﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using th.co.toyota.stm.fas.common;


namespace LFD02170_PrintOutVisualizeAssetLocationMap
{
    /// <summary>
    /// Create By Nuttapon Prapipach
    /// </summary>
    class LFD02170DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02170DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];

            _db = new DataConnection(_strConn);
            if (!_db.TestConnection())
            {

                throw (new Exception("Cannot connect Database"));
            }
        }
      

        public DataTable GetSystemData(string _category, string _subCategory,string _code)
        {
            
            DataTable dt = new DataTable();
            try
            {

              
                SqlCommand cmd = new SqlCommand("sp_Common_GetSystemValues");
                cmd.Parameters.AddWithValue("@CATEGORY", _category);
                cmd.Parameters.AddWithValue("@SUB_CATEGORY", _subCategory);
                cmd.Parameters.AddWithValue("@CODE", _code);

                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


        public DataTable GetSystemData(string _category, string _subCategory)
        {
           
            DataTable dt = new DataTable();
            try
            {

                
                SqlCommand cmd = new SqlCommand("sp_Common_GetSystem");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CATEGORY", _category);
                cmd.Parameters.AddWithValue("@SUB_CATEGORY", _subCategory);


                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


        public DataTable GetCostCenter(string _company, string _costCode)
        {
            
            DataTable dt = new DataTable();
            try
            {
               
                SqlCommand cmd = new SqlCommand("sp_LFD02170_GetCostCenter");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COMPANY", _company);
                cmd.Parameters.AddWithValue("@COST_CODE", _costCode);
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


        public DataTable GetSumAsset(string _company, string _costCode)
        {
           
            DataTable dt = new DataTable();
            try
            {
                
                SqlCommand cmd = new SqlCommand("sp_LFD02170_SumAsset");       
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COMPANY", _company);
                cmd.Parameters.AddWithValue("@COST_CODE", _costCode);
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


        public DataTable GetAssetLocationResult(string _company, string _costCode)
        {
             
            DataTable dt = new DataTable();
            try
            {
                
                SqlCommand cmd = new SqlCommand("sp_LFD02170_GetAssetLocationResult");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COMPANY", _company);
                cmd.Parameters.AddWithValue("@COST_CODE", _costCode);
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


        public DataTable GetMapPathLocation(string _company, string _costCode)
        {
            
            DataTable dt = new DataTable();
            try
            {
               
                SqlCommand cmd = new SqlCommand("sp_LFD02170_GetMapPathLocation");
                cmd.Parameters.AddWithValue("@COMPANY", _company);
                cmd.Parameters.AddWithValue("@COST_CODE", _costCode);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }

        public DataTable GetCostCodeList(string _empCode)
        {

            DataTable dt = new DataTable();
            try
            {

                SqlCommand cmd = new SqlCommand("sp_LFD02170_GetCostCodeList");
                cmd.Parameters.AddWithValue("@EMP_CODE", _empCode);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


        public DataTable GetRemark(string _company)
        {
          
            DataTable dt = new DataTable();
            try
            {
                
                SqlCommand cmd = new SqlCommand("sp_LFD02170_GetRemark");              
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COMPANY", _company);
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }

       

        public DataTable GetMinorCategory(string _company, string _costCode)
        {

            DataTable dt = new DataTable();
            try
            {

                SqlCommand cmd = new SqlCommand("sp_LFD02170_GetMinorCategory");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COMPANY", _company);
                cmd.Parameters.AddWithValue("@COST_CODE", _costCode);
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }


    }
}
