﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;


namespace LFD02170_PrintOutVisualizeAssetLocationMap
{
    /// <summary>
    /// Create By Nuttapon Prapipach
    /// </summary>
    class LFD02170BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02170DAO _dbconn = null;
        private DetailLogData _log = null;

        IFormatProvider _culture = new CultureInfo("en-US", true);
        public string zipSourceDirectory = string.Empty;
        #endregion

        public LFD02170BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
            BLoggingData.ProcessStatus = eLogLevel.Error ;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Print out visualize Asset location map report";
                }
                BLogging = new BatchLogging();//Test connect data base

                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                this.BusinessLogic();

            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void BusinessLogic()
        {
            try
            {


                BLoggingData.ProcessStatus = this.GenerateReport();
                if (BLoggingData.ProcessStatus == eLogLevel.Error)
                {

                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private XtraReport GetXtraReportObject(string _layoutSection, string costcode, string revision, string pathLogo, string pathMap, DataTable dtLocation, DataTable dtSubType, DataTable dtCostCenter, DataTable dtRemark, DataTable dtAssetType,DataTable dtMinorCategory)
        {
            Console.WriteLine("GetXtraReportObject");
            var xtraReport = XtraReport.FromFile(_layoutSection, true);
            
            xtraReport.PaperKind = System.Drawing.Printing.PaperKind.A3;
            xtraReport.Landscape = true;

           
            ((XRLabel)xtraReport.FindControl("xrlblRevision", true)).Text = revision;//12/04/2017
            //((XRLabel)xtraReport.FindControl("xrlblPrintDate", true)).Text = DateTime.Now.ToString();
            var pathLogo_New = this.GetPathLogo(_dbconn, dtCostCenter.Rows[0]["COMPANY"].ToString());
            string _CompanyName = this.GetCompanyName(_dbconn, dtCostCenter.Rows[0]["COMPANY"].ToString());
            ((XRPictureBox)xtraReport.FindControl("xrPicLogo", true)).ImageUrl = pathLogo_New;//@"D:\FixedAssets\Pic\Logo.png";
  
            if (dtCostCenter.Rows.Count > 0)
            {
                //ALCDP002	Plant 2 Delivery & Packing-KD Engine	2017-03-10 11:56:33.677
                ((XRLabel)xtraReport.FindControl("xrlblCostCode", true)).Text = string.Format("{0}", dtCostCenter.Rows[0]["COST_CODE"]); //ALCDP002
                ((XRLabel)xtraReport.FindControl("xrlblCostName", true)).Text = string.Format("{0}", dtCostCenter.Rows[0]["COST_NAME"]); //Plant 2 Delivery & Packing-KD Engine
                ((XRLabel)xtraReport.FindControl("xrlblAsOf", true)).Text = string.Format("{0}", dtCostCenter.Rows[0]["AS_OF"]);//2017-03-10 11:56
                ((XRLabel)xtraReport.FindControl("xrlblSumary", true)).Text = string.Format("{0}", dtLocation.Rows.Count);
                ((XRLabel)xtraReport.FindControl("xrlblCompany", true)).Text = _CompanyName;
            }


            string atpBOI = string.Empty;
            string atpBOILicense = string.Empty;
            string atpLicense = string.Empty;
            string atpNormal = string.Empty;


            string atpBOI_P = string.Empty;
            string atpBOILicense_P = string.Empty;
            string atpLicense_P = string.Empty;
            string atpNormal_P = string.Empty;
            foreach (DataRow dr in dtAssetType.Rows)
            {
                if (string.Format("{0}", dr["CODE"]) == "BOI")
                    atpBOI = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "BOI_LICENSE")
                    atpBOILicense = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "LICENSE")
                    atpLicense = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "NORMAL")
                    atpNormal = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "BOI_P")
                    atpBOI_P = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "BOI_LICENSE_P")
                    atpBOILicense_P = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "LICENSE_P")
                    atpLicense_P = string.Format("{0}", dr["value"]);
                else if (string.Format("{0}", dr["CODE"]) == "NORMAL_P")
                    atpNormal_P = string.Format("{0}", dr["value"]);

            }


            //------Star Actual------/

            //var headerMain = xtraReport.Bands["GroupHeader2"];
            var headerMain = new GroupHeaderBand();

            var itemBG = new XRPictureBox();
            itemBG.ImageUrl = pathMap;//@"D:\FixedAssets\Pic\Location.jpg";
            //itemBG.ImageUrl = @"D:\FixedAssets\Pic\Location.jpg";
            //itemBG.HeightF = 562;//A4
            //itemBG.WidthF = 1000;//A4
            itemBG.HeightF = 810;
            itemBG.WidthF = 1440;
           
            itemBG.Sizing = ImageSizeMode.StretchImage;
            itemBG.LocationF = new System.Drawing.PointF(80, 0);

            for (int i = 0; i < dtLocation.Rows.Count; i++)
            {
                string iText = string.Format("{0}", dtLocation.Rows[i]["DISPLAY"]);
                string iType = string.Format("{0}", dtLocation.Rows[i]["ASSET_TYPE"]);
                float iX = float.Parse(string.Format("{0}", dtLocation.Rows[i]["X"]));
                float iY = float.Parse(string.Format("{0}", dtLocation.Rows[i]["Y"]));

                int iDegree = 0;
                if (!string.IsNullOrEmpty(dtLocation.Rows[i]["DEGREE"].ToString()))
                {
                    iDegree = Int16.Parse(string.Format("{0}", dtLocation.Rows[i]["DEGREE"]));
                }


                XRLabel lblItem = new XRLabel();
                lblItem.Name = string.Format("lblItem_{0}", i);

                lblItem.Text = iText;
                lblItem.Font = new System.Drawing.Font(new FontFamily("Arial"), 10, FontStyle.Bold, GraphicsUnit.Pixel);
                float ratio = float.Parse(string.Format("{0}", 1.8));//A4 = 1.25
                float sX = iX * ratio;
                float sY = iY * ratio;
                lblItem.LocationF = new System.Drawing.PointF(sX + 84, sY + 10);
                lblItem.WidthF = 56;

                XRPictureBox picItem = new XRPictureBox();



                picItem.Name = string.Format("picItem_{0}", i);
                if (iDegree == 0)
                {

                    if (iType.Equals("BOILICENSE"))
                    {
                        lblItem.BackColor = Color.Red;
                        lblItem.ForeColor = Color.White;
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                       DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                        lblItem.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
                     //   picItem.ImageUrl = atpBOILicense;//@"D:\FixedAssets\Pic\BOILicense.png";
                    }

                    else if (iType.Equals("LICENSE"))
                    {
                        lblItem.BackColor = Color.Yellow;
                        lblItem.ForeColor = Color.Black;
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                       DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                        lblItem.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
                    //    picItem.ImageUrl = atpLicense;//@"D:\FixedAssets\Pic\License.png";
                    }


                    else if (iType.Equals("BOI"))
                    {
                        lblItem.BackColor = Color.Black;
                        lblItem.ForeColor = Color.White;
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                       DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                    //    lblItem.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
                  //      picItem.ImageUrl = atpBOI;//@"D:\FixedAssets\Pic\BOI.png";
                    }
                    else if (iType.Equals("NORMAL"))
                    {
                        lblItem.BackColor = Color.White;
                       // picItem.ImageUrl = atpNormal;//@"D:\FixedAssets\Pic\Normal.png";
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                      DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                       
                    }



                    //if (iType.Equals("BOILICENSE"))
                    //    picItem.ImageUrl = @"D:\FixedAssets\Pic\BOILicense.png";
                    //else if (iType.Equals("LICENSE"))
                    //    picItem.ImageUrl = @"D:\FixedAssets\Pic\License.png";
                    //else if (iType.Equals("BOI"))
                    //    picItem.ImageUrl = @"D:\FixedAssets\Pic\BOI.png";
                    //else if (iType.Equals("NORMAL"))
                    //    picItem.ImageUrl = @"D:\FixedAssets\Pic\Normal.png";
                    //lblItem.BorderColor = Color.Black;

                    // Make its left and right borders visible. 
                   
                    //lblItem.Borders = BorderSide.All;
                    // Set its border width (in pixels). 
                    lblItem.BorderWidth = 1;
                    picItem.WidthF = 45;
                    picItem.HeightF = 45;
                    picItem.Sizing = ImageSizeMode.StretchImage;
                    picItem.LocationF = new System.Drawing.PointF(sX + 80, sY);
                    lblItem.TextAlignment = TextAlignment.MiddleCenter;
                    lblItem.HeightF = 40;
                    lblItem.WidthF = 50;
                }
                else if (iDegree == 90)
                {
                    if (iType.Equals("BOILICENSE"))
                    {
                        lblItem.BackColor = Color.Red;
                        lblItem.ForeColor = Color.White;
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                      DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                        lblItem.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
                        picItem.ImageUrl = atpBOILicense_P;//@"D:\FixedAssets\Pic\BOILicense.png";
                    }

                    else if (iType.Equals("LICENSE"))
                    {
                        picItem.ImageUrl = atpLicense_P;//@"D:\FixedAssets\Pic\License.png";
                        lblItem.BackColor = Color.Yellow;
                        lblItem.ForeColor = Color.Black;
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                      DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                        lblItem.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
                    }

                    else if (iType.Equals("BOI"))
                    {
                        lblItem.BackColor = Color.Black;
                        lblItem.ForeColor = Color.White;
                        lblItem.BorderColor = Color.Black;
                        picItem.ImageUrl = atpBOI_P;//@"D:\FixedAssets\Pic\BOI.png";
                    }

                    else if (iType.Equals("NORMAL"))
                    {
                        picItem.ImageUrl = atpNormal_P;//@"D:\FixedAssets\Pic\Normal.png";
                        lblItem.BackColor = Color.White;
                        lblItem.Borders = DevExpress.XtraPrinting.BorderSide.Left |
                      DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
                      

                    }


                    picItem.WidthF = 45;
                    picItem.HeightF = 45;
                    picItem.Sizing = ImageSizeMode.StretchImage;
                    picItem.LocationF = new System.Drawing.PointF(sX + 80, sY);
                    lblItem.BorderColor = Color.Black;


                    // Set its border width (in pixels). 
                    lblItem.BorderWidth = 1;
                    lblItem.LocationF = new System.Drawing.PointF(sX + 88, sY + 36);
                    lblItem.TextAlignment = TextAlignment.MiddleCenter;
                    lblItem.HeightF = 50;
                    lblItem.WidthF = 40;
                    lblItem.Angle = 90;
                }


                headerMain.Controls.AddRange(new XRControl[] { lblItem, picItem });
            }

            headerMain.Controls.AddRange(new XRControl[] { itemBG });

            //for (int i = 0; i < dtSubType.Rows.Count; i++)
            //{
            //    XRTableRow tr = new XRTableRow();
            //    XRTableCell tc1 = new XRTableCell();
            //    //tc1.Text = string.Format("{0}", dtSubType.Rows[i]["SUB_TYPE"]);
            //    tc1.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            //    tc1.TextAlignment = TextAlignment.MiddleCenter;
            //    XRTableCell tc2 = new XRTableCell();
            //    tc2.Text = string.Format("{0}", dtSubType.Rows[i]["ITEMS"]);
            //    tc2.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            //    tc2.TextAlignment = TextAlignment.MiddleRight;
            //    tr.Cells.Add(tc1);
            //    tr.Cells.Add(tc2);

            //    tr.BackColor = Color.Snow;

            //    if (string.Format("{0}", dtSubType.Rows[i]["SUB_TYPE"]) == "Total")
            //    {


            //        tr.BorderColor = Color.DarkGray;
            //        tr.BackColor = Color.LightSkyBlue;
            //    }


            //    tr.BorderWidth = 1;
            //    tr.BorderColor = Color.Black;
            //    tr.Borders = BorderSide.All;
            //    ((XRTable)xtraReport.FindControl("xrTbSubType", true)).Rows.Add(tr);
            //}

            if (dtRemark.Rows.Count > 0)
            {

                if (dtRemark.Rows.Count == 1)
                {
                    ((XRLabel)xtraReport.FindControl("xrlblRemark1", true)).Text = string.Format("{0}", dtRemark.Rows[0]["Value"]); //Mock up map asset to location
                }
                else if (dtRemark.Rows.Count == 2)
                {
                    ((XRLabel)xtraReport.FindControl("xrlblRemark1", true)).Text = string.Format("{0}", dtRemark.Rows[0]["Value"]); //Mock up map asset to location
                    ((XRLabel)xtraReport.FindControl("xrlblRemark2", true)).Text = string.Format("{0}", dtRemark.Rows[1]["Value"]);

                }
                else if (dtRemark.Rows.Count == 3)
                {
                    ((XRLabel)xtraReport.FindControl("xrlblRemark1", true)).Text = string.Format("{0}", dtRemark.Rows[0]["Value"]); //Mock up map asset to location
                    ((XRLabel)xtraReport.FindControl("xrlblRemark2", true)).Text = string.Format("{0}", dtRemark.Rows[1]["Value"]);
                    ((XRLabel)xtraReport.FindControl("xrlblRemark3", true)).Text = string.Format("{0}", dtRemark.Rows[2]["Value"]);

                }
            }

            xtraReport.Bands.Add(headerMain);
            //xtraReport.DataSource = dtMinorCategory;
            SetXtraSubReportObject(xtraReport, dtMinorCategory);


            //------End Actual------/


            //------------Start FOR TEST-----------------//

            //var itemBG1 = new XRPictureBox();
            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));
            //itemBG1.ImageUrl = @"D:\FixedAssets\Pic\map.png";
            //itemBG1.HeightF = 562;
            //itemBG1.WidthF = 1000;
            //itemBG1.Sizing = ImageSizeMode.StretchImage;
            //itemBG1.LocationF = new System.Drawing.PointF(50, 0);



            //var labelItem1 = new XRLabel();
            ////labelItem1.DataBindings.Add("Text", null, "Item1");
            ////labelItem1.LeftF = 100;
            //labelItem1.Text = "01:STZY-0091569777";
            //labelItem1.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            //labelItem1.LocationF = new System.Drawing.PointF(75, 310);
            ////labelItem3.ForeColor = System.Drawing.Color.White;
            //labelItem1.WidthF = 80;

            //var itemIMG1 = new XRPictureBox();
            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));
            //itemIMG1.ImageUrl = @"D:\FixedAssets\Pic\License.png";
            //itemIMG1.WidthF = 120;
            //itemIMG1.HeightF = 40;
            //itemIMG1.Sizing = ImageSizeMode.StretchImage;
            //itemIMG1.LocationF = new System.Drawing.PointF(70, 300);




            //var labelItem2 = new XRLabel();
            ////labelItem1.DataBindings.Add("Text", null, "Item1");
            ////labelItem1.LeftF = 100;
            //labelItem2.Text = "01:STZY-009100569771";
            //labelItem2.LocationF = new System.Drawing.PointF(705, 260);
            //labelItem2.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            ////labelItem3.ForeColor = System.Drawing.Color.White;
            //labelItem2.WidthF = 80;

            //var itemIMG2 = new XRPictureBox();
            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));
            //itemIMG2.ImageUrl = @"D:\FixedAssets\Pic\BOILicense.png";
            //itemIMG2.WidthF = 120;
            //itemIMG2.HeightF = 40;
            //itemIMG2.Sizing = ImageSizeMode.StretchImage;
            //itemIMG2.LocationF = new System.Drawing.PointF(700, 250);







            //var labelItem3 = new XRLabel();
            ////labelItem1.DataBindings.Add("Text", null, "Item1");
            ////labelItem1.LeftF = 100;
            //labelItem3.Text = "01:STZY-0091569778";
            //labelItem3.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            //labelItem3.LocationF = new System.Drawing.PointF(75, 170);
            ////labelItem3.ForeColor = System.Drawing.Color.White;
            //labelItem3.WidthF = 80;

            //var itemIMG3 = new XRPictureBox();
            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));
            //itemIMG3.ImageUrl = @"D:\FixedAssets\Pic\BOI.png";
            //itemIMG3.WidthF = 120;
            //itemIMG3.HeightF = 40;
            //itemIMG3.Sizing = ImageSizeMode.StretchImage;
            //itemIMG3.LocationF = new System.Drawing.PointF(70, 160);



            //var labelItem4 = new XRLabel();
            ////labelItem1.DataBindings.Add("Text", null, "Item1");
            ////labelItem1.LeftF = 100;
            //labelItem4.Text = "01:STZY-0091569773";
            //labelItem4.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            //labelItem4.LocationF = new System.Drawing.PointF(75, 210);
            ////labelItem3.ForeColor = System.Drawing.Color.White;
            //labelItem4.WidthF = 80;

            //var itemIMG4 = new XRPictureBox();
            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));
            //itemIMG4.ImageUrl = @"D:\FixedAssets\Pic\Normal.png";
            //itemIMG4.WidthF = 120;
            //itemIMG4.HeightF = 40;
            //itemIMG4.Sizing = ImageSizeMode.StretchImage;
            //itemIMG4.LocationF = new System.Drawing.PointF(70, 200);


            //var labelItem5 = new XRLabel();
            ////labelItem1.DataBindings.Add("Text", null, "Item1");
            ////labelItem1.LeftF = 100;
            //labelItem5.Text = "01:STZY-0091569774";
            //labelItem5.Font = new System.Drawing.Font(new FontFamily("Arial"), 9, FontStyle.Bold, GraphicsUnit.Pixel);
            //labelItem5.LocationF = new System.Drawing.PointF(305, 110);
            ////labelItem3.ForeColor = System.Drawing.Color.White;
            //labelItem5.WidthF = 80;

            //var itemIMG5 = new XRPictureBox();
            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));
            //itemIMG5.ImageUrl = @"D:\FixedAssets\Pic\Normal.png";
            //itemIMG5.HeightF = 120;
            //itemIMG5.HeightF = 40;
            //itemIMG5.Sizing = ImageSizeMode.StretchImage;
            //itemIMG5.LocationF = new System.Drawing.PointF(300, 100);


            ////itemIMG.DataBindings.Add(new XRBinding("Text", null, "Item1"));






            //var header = new GroupHeaderBand();

            //header.Controls.AddRange(new XRControl[] { labelItem1, itemIMG1 });

            //header.Controls.AddRange(new XRControl[] { labelItem2, itemIMG2 });

            //header.Controls.AddRange(new XRControl[] { labelItem3, itemIMG3 });

            ////header.Controls.AddRange(new XRControl[] { labelItem1, itemIMG1 });
            //header.Controls.AddRange(new XRControl[] { labelItem4, itemIMG4 });

            //header.Controls.AddRange(new XRControl[] { labelItem5, itemIMG5 });
            //header.Controls.AddRange(new XRControl[] { itemBG1 });


            //xtraReport.Bands.Add(header);


            //------------End FOR TEST-----------------//

            return xtraReport;
        }


        void SetXtraSubReportObject(XtraReport MainReport, DataTable dt)
        {
            //Get sub report lay out section name
            string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["SubReportLayoutSection"]; //follow your config

            var subReport = (XRSubreport)MainReport.FindControl("MINOR_CATEGORY_SUB", true);

            var ReferenceReport = XtraReport.FromFile(cfgLayoutSection, true);

            ReferenceReport.DataSource = dt;
            subReport.ReportSource = ReferenceReport;
        
            //subReport.Report.DataSource = dt;
            //subReport.Report.Visible = true;

        }

        private bool GenerateFile(string _layoutDirectory, string _directoryName, string _fileName, string costcode, string revision, string pathLogo, string pathMap, DataTable dtLocation, DataTable dtSubType, DataTable dtCostCenter, DataTable dtRemark, DataTable dtAssetType,DataTable dtMinorCategory)
        {
            try
            {
                if (dtCostCenter.Rows.Count == 0)
                    return false;


                var xTraReport = GetXtraReportObject(_layoutDirectory, costcode, revision, pathLogo, pathMap, dtLocation, dtSubType, dtCostCenter, dtRemark, dtAssetType,dtMinorCategory);


                _log4Net.WriteInfoLogFile("GenerateFile");
                //var xTraReport = GetXtraReportObject(_layoutDirectory, costcode, revision, pathLogo, pathMap, dtLocation, dtSubType, dtCostCenter, dtRemark, dtAssetType);
                _log4Net.WriteInfoLogFile("GenerateFile");
//>>>>>>> 5649de5a6c3e477617bbc6d362b02bbb327f81e1
                var pdfBytes = Export2PDF(xTraReport, _directoryName, _fileName);
                return true;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                throw ex;
            }
        }

        private eLogLevel GenerateReport()
        {
            eLogLevel _rs = eLogLevel.Error;

            string _messageCode = "MSTD0000BERR  : ";
            try
            {
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                //if (!System.IO.File.Exists(cfgLayoutSection))
                //{
                //    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                //}
                string zipFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                //Get PDF file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, DateTime.Now);
                oFileName = oFileName.Replace(".pdf", string.Empty);




                _dbconn = new LFD02170DAO();


                string BatchParam = BLoggingData.Arguments.Replace("'", "");

                string[] sp = BatchParam.Split('|');

                string empcode = string.Empty;
                string costcode = string.Empty;
                string company_code = string.Empty;
                
                if (sp.Length.Equals(3))
                {
                   
                    empcode = string.Format("{0}", sp[0]);
                    company_code = string.Format("{0}", sp[1]);
                    costcode = string.Format("{0}", sp[2]);
                }
                else if (sp.Length.Equals(1))
                {
                    empcode = string.Format("{0}", sp[0]);
                }
                  
               


                DataTable dtLocation = new DataTable();
                DataTable dtSummary = new DataTable();
                DataTable dtRemark = new DataTable();
                DataTable dtCostCenter = new DataTable();
                DataTable dtAssetType = new DataTable();
                DataTable dtMinorCategory = new DataTable();

                string pathLogo, pathMap, revision;

                if (costcode.Equals(string.Empty))
                {

                    DataTable dtCost = this.GetCostCodeList(_dbconn, empcode);

                    if (dtCost.Rows.Count > 0)
                    {
                        zipSourceDirectory = Path.Combine(oDirectoryName, "LFD02170BO_ZipFolder");
                        if (!Directory.Exists(zipSourceDirectory))
                        {
                            try
                            {
                                Directory.CreateDirectory(zipSourceDirectory);
                            }
                            catch (Exception ex)
                            {
                                InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);

                            }
                        }


                        foreach (DataRow dr in dtCost.Rows)
                        {

                            costcode = string.Format("{0}", dr["COST_CODE"]);
                            company_code = string.Format("{0}", dr["COMPANY"]);

                            Console.WriteLine(costcode);
                            
                            dtLocation = this.GetAssetLocationResult(_dbconn, company_code, costcode);
                            dtSummary = this.GetSummary(_dbconn, company_code, costcode);
                            dtRemark = this.GetRemark(_dbconn, company_code);
                            dtCostCenter = this.GetCostCenter(_dbconn, company_code, costcode);
                            dtAssetType = this.GetAssetTypePath(_dbconn);
                            pathLogo = this.GetPathLogo(_dbconn, company_code);
                            pathMap = this.GetMapPathLocation(_dbconn, company_code, costcode);
                            revision = this.GetRevision(_dbconn, company_code);

                            dtMinorCategory = this.GetMinorCategory(_dbconn, company_code, costcode);

                            //Get PDF file name
                            string oFileNamei = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                            oFileNamei = string.Format(oFileNamei, DateTime.Now);
                            oFileNamei = oFileNamei.Replace(".pdf", string.Empty);


                            //Gernerate PDF File
                            GenerateFile(cfgLayoutSection, zipSourceDirectory, oFileNamei, costcode, revision, pathLogo, pathMap, dtLocation, dtSummary, dtCostCenter, dtRemark, dtAssetType, dtMinorCategory);

                        }


  

                        // zip process 
                        zipFileName = zipFileName.Replace(".pdf", ".zip");
                        zipFileName = string.Format(zipFileName, DateTime.Now);
                        string destinationPath_ZipPath =  System.IO.Path.Combine(oDirectoryName , zipFileName);
                        bool ZipSuccess = false;
                        ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                        if (ZipSuccess)
                            _rs = eLogLevel.Information;
                         
                        DetailLogData _detail = new DetailLogData();
                        _detail.AppID = BLoggingData.AppID;
                        _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, destinationPath_ZipPath);
                        _detail.Level = eLogLevel.Information;
                        BLogging.InsertDetailLog(_detail);

                        // Insert File Download
                        BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);



                        //// Insert log : File {1} is generated to {0}
                        //DetailLogData _detail = new DetailLogData();
                        //_detail.AppID = BLoggingData.AppID;
                        //_detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".pdf");
                        //_detail.Level = eLogLevel.Information;
                        //BLogging.InsertDetailLog(_detail);

                        //// Insert File Download
                        //BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".pdf");
                        //result = 1;



                    }


                }
                else
                {
                    

                    dtLocation = this.GetAssetLocationResult(_dbconn, company_code, costcode);
                    dtSummary = this.GetSummary(_dbconn, company_code, costcode);
                    dtRemark = this.GetRemark(_dbconn, company_code);
                    dtCostCenter = this.GetCostCenter(_dbconn, company_code, costcode);
                    dtAssetType = this.GetAssetTypePath(_dbconn);
                    pathLogo = this.GetPathLogo(_dbconn, company_code);
                    pathMap = this.GetMapPathLocation(_dbconn, company_code, costcode);
                    revision = this.GetRevision(_dbconn, company_code);

                    dtMinorCategory = this.GetMinorCategory(_dbconn, company_code, costcode);


                    //Gernerate PDF File
                    bool _s = GenerateFile(cfgLayoutSection, oDirectoryName, oFileName, costcode, revision, pathLogo, pathMap, dtLocation, dtSummary, dtCostCenter, dtRemark, dtAssetType, dtMinorCategory);

                    DetailLogData _detail = new DetailLogData();
                    _detail.AppID = BLoggingData.AppID;
                    if (!_s)
                    {

                        _detail.Description = CommonMessageBatch.DATA_NOT_FOUND;
                        _detail.Level = eLogLevel.Error;
                        BLogging.InsertDetailLog(_detail);
                        _rs = eLogLevel.Error;
                        return _rs;
                    }
                    // Insert log : File {1} is generated to {0}
                    
                
                    _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".pdf");
                    _detail.Level = eLogLevel.Information;
                    BLogging.InsertDetailLog(_detail);
                    _log4Net.WriteInfoLogFile(oDirectoryName);

                    _log4Net.WriteInfoLogFile(oFileName);
                    // Insert File Download
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".pdf");
                    _rs = eLogLevel.Information;

                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return _rs ;
        }

        byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            _log4Net.WriteInfoLogFile("Export2PDF");
            _log4Net.WriteInfoLogFile(DirectoryName);
            _log4Net.WriteInfoLogFile(FileName);

            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }

        private string GetPathLogo(LFD02170DAO DAO , string _company)
        {
            string path = string.Empty;
            DataTable dt = DAO.GetSystemData("REPORT_CONFIG", "LOGO_" + _company, "LFD02170");

            if (dt.Rows.Count > 0)
            {
                path = string.Format("{0}", dt.Rows[0]["Value"]);

            }

            return path;
        }
        private string GetCompanyName(LFD02170DAO DAO, string _company)
        {
            string path = string.Empty;
            DataTable dt = DAO.GetSystemData("REPORT_CONFIG", "COMPANY_NAME", _company);

            if (dt.Rows.Count > 0)
            {
                path = string.Format("{0}", dt.Rows[0]["Value"]);

            }

            return path;
        }

        private string GetRevision(LFD02170DAO DAO, string _company)
        {
            string result = string.Empty;

            //LFD02170DAO DAO = new LFD02170DAO();

            DataTable dt = DAO.GetSystemData("REPORT_CONFIG", "HEADER-MSG_"+ _company, "LFD02170");

            if (dt.Rows.Count > 0)
            {
                result = string.Format("{0}", dt.Rows[0]["Value"]);

            }

            return result;
        }

        private DataTable GetAssetTypePath(LFD02170DAO DAO)
        {

            DataTable dt = DAO.GetSystemData("SYSTEM_CONFIG", "ASSET_TYPE_PATH");

            return dt;
        }

        private DataTable GetSummary(LFD02170DAO DAO, string _company, string _costCode)
        {
            string result = string.Empty;

            //LFD02170DAO DAO = new LFD02170DAO();

            DataTable dt = DAO.GetSumAsset(_company, _costCode);

            /*
            int sum = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ITEMS"] !=null)
                {
                    if (string.Format("{0}", dt.Rows[i]["ITEMS"]) != "")
                    {
                        sum += Convert.ToInt32(string.Format("{0}", dt.Rows[i]["ITEMS"]));
                    }
                }
            }
            */
          //  dt.Rows.Add("Total", sum);


            return dt;
        }

        private DataTable GetAssetLocationResult(LFD02170DAO DAO, string _company, string _costCode)
        {

            DataTable dt = DAO.GetAssetLocationResult(_company, _costCode);

            return dt;
        }

        private DataTable GetCostCenter(LFD02170DAO DAO, string _company, string _costCode)
        {

            DataTable dt = DAO.GetCostCenter(_company, _costCode);

            return dt;
        }

        private string GetMapPathLocation(LFD02170DAO DAO, string _company, string _costCode)
        {
            string result = string.Empty;

            //LFD02170DAO DAO = new LFD02170DAO();

            DataTable dt = DAO.GetMapPathLocation(_company, _costCode);

            if (dt.Rows.Count == 0)
                return string.Empty;

            return string.Format("{0}", dt.Rows[0]["MAP_PATH"]);

        }

        private DataTable GetRemark(LFD02170DAO DAO, string _company)
        {

            DataTable dt = DAO.GetRemark(_company);


            return dt;
        }

        private DataTable GetCostCodeList(LFD02170DAO DAO, string _empCode)
        {

            DataTable dt = DAO.GetCostCodeList(_empCode);


            return dt;
        }

        private DataTable GetMinorCategory(LFD02170DAO DAO, string _company, string _costCode)
        {

            DataTable dt = DAO.GetMinorCategory(_company, _costCode);

            return dt;
        }

        private bool ZipDataToFile(string sourcePathDirectory, string destinationPath_ZipPath)
        {
            bool _valueReturn;
            string _messageCode = "MSTD0000BERR  : ";
            _valueReturn = false;
            try
            {

                 destinationPath_ZipPath = string.Format(destinationPath_ZipPath, DateTime.Now);
                //-------------------
                ZipFile.CreateFromDirectory(sourcePathDirectory, destinationPath_ZipPath);
                _valueReturn = true;
                Directory.Delete(sourcePathDirectory, true);
            }
            catch (Exception ex)
            {
                InsertLog(_messageCode + "Cannot zip file because :" + ex.Message);
            }
            return _valueReturn;
        }

        private void InsertLog(string Log_Description)
        {
            _log = new DetailLogData();
            _log.AppID = BLoggingData.AppID;
            _log.Status = eLogStatus.Processing;
            _log.Level = eLogLevel.Error;
            _log.Description = Log_Description;
            BLogging.InsertDetailLog(_log);

            _log4Net.WriteErrorLogFile(Log_Description);
        }

    }
}
