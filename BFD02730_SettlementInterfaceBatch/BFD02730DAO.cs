﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
namespace th.co.toyota.stm.fas
{
    public class SettlementAssetList
    {
        public int RECORD_ID { get; set; }
        public string DOC_NO { get; set; }
        public string COMPANY { get; set; }
        public string AUC_NO { get; set; }
        public string AUC_SUB { get; set; }
        public string INVOICE_NO { get; set; }
        public string FISCAL_YEAR { get; set; }
        public int INV_LINE { get; set; }
        public string AUC_AMOUNT { get; set; }
        public string REFERENCE { get; set; }
        public int SEQ_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string SETTLE_AMOUNT { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public string BLANK { get; set; }
    }

    class BFD02730DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public BFD02730DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];           
        }

        public List<SettlementAssetList> GetSettlementAssetList(BatchLoggingData _data)
        {
            //DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02730_GetSettlementAssetList");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AppID", _data.AppID);
                cmd.Parameters.AddWithValue("@User", _data.ReqBy);
                cmd.CommandType = CommandType.StoredProcedure;

                return _db.executeDataToList<SettlementAssetList>(cmd);

                //dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            //return dt;
        }

        public void UpdateAssetStatus()
        {
          
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02730_UpdateAssetStatus");
                cmd.CommandType = CommandType.StoredProcedure;
                _db.Execute(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}

public class MSG
{
    public const string FileSuccess = "MSTD4005AINF"; //MSTD4005AINF	 File {0} is successfully generated in {1}.	
    public const string BatchBegin = "MSTD7000BINF"; //MSTD7000BINF : {0} Begin
    public const string BatchEndSuccessfully = "MSTD7001BINF"; //MSTD7001BINF : {0} End successfully
    public const string BatchEndError = "MSTD7002BINF";  //MSTD7002BINF :  {0} End with error {1}
    public const string DataNotFound_NoParams = "MCOM2100BWRN"; // No data found
    public const string DataNotFound = "MSTD7054BERR"; //{0} Data not found from {1}		
}