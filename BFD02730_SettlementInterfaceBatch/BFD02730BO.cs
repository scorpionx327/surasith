﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTP;
using System.ComponentModel;

namespace th.co.toyota.stm.fas
{
    class BFD02730BO
    {
        #region "Variable Declare"

        public BatchLoggingData bLoggingData = null;
        public BatchLogging bLogging = null;

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";

        private Log4NetFunction _log4Net = new Log4NetFunction();
        private BFD02730DAO _dbconn = null;
        private DetailLogData _log = null;
        #endregion

        public BFD02730BO()
        {
            bLoggingData = new BatchLoggingData();
            bLogging = new BatchLogging();


            bLoggingData.BatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            bLoggingData.BatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];


            // Set Batch Q information
            bLoggingData.Status = eLogStatus.Queue;
            bLoggingData.Description = "Run by schedule";
            bLoggingData.ReqBy = "System";


        }
        public void Processing()
        {
            MailSending _mail = null;
            try
            {
                _mail = new MailSending();


                bLogging = new BatchLogging();//Test connect data base
                _dbconn = new BFD02730DAO();

                this.StartBatchQ();

                this.BusinessProcess();

            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = bLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    bLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
            }
            finally
            {
                try
                {
                    this.SetBatchQEnd();
                    _mail.SendEmailToAdministratorInSystemConfig(bLoggingData.BatchID);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }


        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion

        public DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private void GenerateFile(string cfgFileName, string cfgLayoutSection, List<SettlementAssetList> assets)
        {
            GenerateFileClass cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

            cls.HeaderText = string.Format(cls.HeaderText, DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-US")), "00434"); //#HPLANIIYYYYMMDDhhmmss
            cls.FooterText = string.Format(cls.FooterText, (assets.Count + 2).ToString().PadLeft(7, '0')); //#T000072

            var dt = ToDataTable(assets);
            cls.DataSource = dt;

            cls.Execute();

            InsertLog(MSG.FileSuccess, bLoggingData.BatchID, cls.FullFileName);//File {0} is successfully generated in {1}.
            bLoggingData.ProcessStatus = FTPFile(cls.FullFileName, cfgFileName, cls.FileName);

            cls.MoveFileToArchive(bLoggingData.ProcessStatus == eLogLevel.Information);

            //Delay for 1 second
            System.Threading.Thread.Sleep(1000);
        }

        private void BusinessProcess()
        {
            try
            {
                //3) Generate Interface .txt File (Common Process)
                string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                var settlementAssets = _dbconn.GetSettlementAssetList(bLoggingData);
                if (settlementAssets.Count == 0)
                {
                    GenerateFile(cfgFileName, cfgLayoutSection, settlementAssets);
                    InsertLog(MSG.DataNotFound_NoParams);
                }
                else
                {
                    //GROUP ASSET BY COMPANY AND AUC_NO
                    var assetGroups = settlementAssets.GroupBy(x => x.AUC_NO);
                    var rowLimit = 499; //limit row per file

                    var sentAssets = new List<SettlementAssetList>();
                    foreach (var assetGroup in assetGroups)
                    {
                        var newAssets = assetGroup.ToList();//get list
                        //Check is current sent asset + new assset more than row limit
                        //then send file
                        if (sentAssets.Count > 0 && ((sentAssets.Count + newAssets.Count) > rowLimit))
                        {
                            GenerateFile(cfgFileName, cfgLayoutSection, sentAssets);
                            sentAssets.Clear();
                        }

                        foreach (var newAsset in newAssets)
                        {
                            if (sentAssets.Count >= rowLimit)
                            {
                                GenerateFile(cfgFileName, cfgLayoutSection, sentAssets);
                                sentAssets.Clear();
                            }
                            //add new asset
                            newAsset.RECORD_ID = sentAssets.Count + 1;
                            sentAssets.Add(newAsset);
                        }
                    }

                    if (sentAssets.Count > 0)
                    {
                        GenerateFile(cfgFileName, cfgLayoutSection, sentAssets);
                    }
                }

                //Update Data
                if (bLoggingData.ProcessStatus == eLogLevel.Information)
                {
                    _dbconn.UpdateAssetStatus();
                }


            }
            catch (Exception ex)
            {
                bLoggingData.ProcessStatus = eLogLevel.Error;
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private eLogLevel FTPFile(string fullFileName, string cfgFileName, string fileName)
        {
            // return eLogLevel.Information; //Test

            eLogLevel _rs = eLogLevel.Warning;

            string ftpFile = "Tranfer(FTP) file : " + fileName;
            try
            {
                InsertLog(MSG.BatchBegin, ftpFile); //MSTD7000BINF : {0} Begin

                string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["LayoutSectionFTP"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSectionFTP")));
                }

                ConfigurationManager cfg = new ConfigurationManager(cfgFileName);
                System.Collections.Hashtable ht = cfg.GetChildNodes(cfgLayoutSectionFTP);
                foreach (string cfgFTP in ht.Keys)
                {
                    try
                    {
                        FTP ftp = new FTP(cfgFileName, cfgLayoutSectionFTP + @"/" + cfgFTP);
                        ftp.UploadFileName = fullFileName;
                        ftp.Execute();

                        _rs = eLogLevel.Information;

                        this.InsertLog(MSG.BatchEndSuccessfully, ftpFile); //MSTD7001BINF : {0} End successfully
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc);
                        _rs = eLogLevel.Error;
                        if (exc.HResult.Equals(-2146231997))
                        {
                            this.InsertLog(MSG.DataNotFound, "FTP", "configuration"); //MSTD7054BERR : {0} Data not found from {1}	
                            throw;
                        }
                        throw exc;
                    }
                }
            }
            catch (Exception ex)
            {
                _rs = eLogLevel.Error;
                this.InsertLog(MSG.BatchEndError, ftpFile, string.Empty); //MSTD7002BINF :  {0} End with error {1}
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return _rs;
        }
        private void InsertLog(string messageCode, params string[] param)
        {
            try
            {
                _log = new DetailLogData();
                _log.AppID = bLoggingData.AppID;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.MessageCode = messageCode;
                bLogging.InsertDetailLog(_log, param);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
    }
}