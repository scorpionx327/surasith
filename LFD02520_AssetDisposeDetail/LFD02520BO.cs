﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using System.Text.RegularExpressions;

namespace LFD02520_AssetDisposeDetail
{
    class LFD02520BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02520DAO _dbconn = null;
        private DetailLogData _log = null;

        private string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config
        private string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config
        private string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
        private string zipSourceDirectory = string.Empty;

        IFormatProvider _culture = new CultureInfo("en-US", true);
        #endregion

        public LFD02520BO()
        {
            BLoggingData = new BatchLoggingData();         
        }

        public void Processing()
        {
            MailSending mail = null;
            BLoggingData.IProcessStatus = _iProcessStatus;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Asset dispose detail with approval and asset status check report";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02520DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                GetAssetDisposeData();
                BLoggingData.IProcessStatus = _iProcessStatus;
            }
            catch (Exception ex) //cannot connect
            {
                
                _log4Net.WriteErrorLogFile(ex.Message, ex);

                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = ex.Message;
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    string ErrMsg = string.Format(CommonMessageBatch.MSTD0067AERR, System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message);
                    _log4Net.WriteErrorLogFile(ErrMsg, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.Send();
                }
                catch (Exception ex)
                {
                    string ErrMsg = string.Format(CommonMessageBatch.MSTD0067AERR, System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message);
                    _log4Net.WriteErrorLogFile(ErrMsg, ex);
                }
            }
        }

        private void GetAssetDisposeData()
        {
            try
            {
                string zipFileName = oFileName;
                string OutputFolderName = oDirectoryName;

                string[] _p = BLoggingData.Arguments.Split('|');
                _dbconn = new LFD02520DAO();
                DataSet ds = new DataSet();

                string DOC_NO;

                if (_p.Length == 1)
                {
                    DOC_NO = _p[0];

                    ds = _dbconn.GetAssetDisposeData(DOC_NO);

                    if (CountDataAllTable(ds) > 0)
                    {
                        PrepareDataSet(ds);
                        _iProcessStatus = GenerateReport(ds);
                    }
                    else
                    {
                        _log = new DetailLogData();
                        _log.AppID = BLoggingData.AppID;
                        _log.Status = eLogStatus.Processing;
                        _log.Level = eLogLevel.Error;
                        _log.Favorite = false;
                        _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                        BLogging.InsertDetailLog(_log);
                    }
                }
                else
                {
                    for (int i = 0; i < _p.Length; i++)
                    {
                        DOC_NO = _p[i].Replace("'", string.Empty);
                        ds = _dbconn.GetAssetDisposeData(DOC_NO);
                        if (CountDataAllTable(ds) > 0)
                        {
                            PrepareDataSet(ds);
                            GenerateMultipleReport(ds, DOC_NO);
                        }
                    }
                    // zip process 
                    zipFileName = zipFileName.Replace(".pdf", ".zip");
                    zipFileName = string.Format(zipFileName, DateTime.Now);
                    string destinationPath_ZipPath = OutputFolderName + @"\" + zipFileName;
                    bool ZipSuccess = false;
                    ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                    if (ZipSuccess)
                    { _iProcessStatus = 1; }
                    // Insert log : File {1} is generated to {0}
                    DetailLogData _detail = new DetailLogData();
                    _detail.AppID = BLoggingData.AppID;
                    _detail.Description = string.Format(SendingBatch.I_GENFILE_END, OutputFolderName, zipFileName);
                    _detail.Level = eLogLevel.Information;
                    BLogging.InsertDetailLog(_detail);

                    // Insert File Download
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        XtraReport GetXtraReportObject(string LayoutSection, DataSet dataSource)
        {
            var xtraReport = XtraReport.FromFile(LayoutSection, true);
            xtraReport.DataSource = dataSource;

            string LayoutDetailP1 = System.Configuration.ConfigurationManager.AppSettings["LayoutDetailP1"];
            string LayoutDetail = System.Configuration.ConfigurationManager.AppSettings["LayoutDetail"];
            string LayoutAPPR01 = System.Configuration.ConfigurationManager.AppSettings["LayoutAPPR01"];
            string LayoutAPPR02 = System.Configuration.ConfigurationManager.AppSettings["LayoutAPPR02"];
            string LayoutAPPR03 = System.Configuration.ConfigurationManager.AppSettings["LayoutAPPR03"];
            string LayoutAPPR04 = System.Configuration.ConfigurationManager.AppSettings["LayoutAPPR04"];
            string LayoutPhoto = System.Configuration.ConfigurationManager.AppSettings["LayoutPhoto"];

            string LayoutENV = System.Configuration.ConfigurationManager.AppSettings["LayoutENV"];
            string LayoutFAM = System.Configuration.ConfigurationManager.AppSettings["LayoutFAM"];


            //Get sub report template path      
            CreateSubReportObject(xtraReport, dataSource.Copy(), "RequestDetail", LayoutDetailP1);
            CreateSubReportObject(xtraReport, dataSource.Copy(), "RequestApproval", LayoutAPPR01);
            CreateSubReportObject(xtraReport, dataSource.Copy(), "BOIApproval", LayoutAPPR02);
            CreateSubReportObject(xtraReport, dataSource.Copy(), "StoreApproval", LayoutAPPR03);
            CreateSubReportObject(xtraReport, dataSource.Copy(), "MachineApproval", LayoutAPPR04);

            if (dataSource.Tables["DISP_DETAIL_REQUEST"].Rows.Count > 0)
            {
                //Get sub report template path [Detail Page2 and next]       
                CreateSubReportObject(xtraReport, dataSource.Copy(), "RequestDetailNext", LayoutDetail);
            }


            if (dataSource.Tables["DISP_DETAIL_FAM"].Rows.Count > 0)
            {
                  
                CreateSubReportObject(xtraReport, dataSource.Copy(), "FAMAssetDetail", LayoutFAM);
            }

            if (dataSource.Tables["DISP_DETAIL_ENV"].Rows.Count > 0)
            {

                CreateSubReportObject(xtraReport, dataSource.Copy(), "ENVAssetDetail", LayoutENV);
            }

            if (dataSource.Tables["DISP_ASSET_PHOTO"].Rows.Count > 0)
            {
                //Get sub report template path [PHOTO]       
                CreateSubReportObject(xtraReport, dataSource.Copy(), "PhotoDetail", LayoutPhoto);
            }



            return xtraReport;
        }

        void CreateSubReportObject(XtraReport MainReport, DataSet DataSource, string SubReportName, string LayoutTemplate)
        {
            var subReportObj = (XRSubreport)MainReport.FindControl(SubReportName, true);
            var subReport = XtraReport.FromFile(LayoutTemplate, true);

            subReport.DataSource = DataSource;
            subReportObj.ReportSource = subReport;
        }



        DataSet AddBlankRow(DataSet DataSet, string TableName, int limit)
        {

            if (DataSet.Tables[TableName].Rows.Count < limit)
            {
                int diff = limit - DataSet.Tables[TableName].Rows.Count;
                for (int ii = 0; ii < diff; ii++)
                {
                    DataSet.Tables[TableName].Rows.Add();
                }
            }
            return DataSet;
        }

        void PrepareDataSet(DataSet DataSet)
        {
            //DetailPage1
            AddBlankRow(DataSet, "DISP_DETAIL_REQUEST_P1", 3);

            //RequestApproval
            AddBlankRow(DataSet, "DISP_CRNT_APPROVER", 6);

            //BOIApproval
            AddBlankRow(DataSet, "DISP_BOI_APPROVER", 3);

            //StoreApproval
            AddBlankRow(DataSet, "DISP_STORE_APPROVER", 3);

            //MachineApproval
            AddBlankRow(DataSet, "DISP_FA_APPROVER", 3);
        }

        void GenerateFile(DataSet ds, string DirectoryName, string FileName, string LayoutDirectory)
        {
            try
            {
                var xTraReport = GetXtraReportObject(LayoutDirectory, ds);
                var pdfBytes = Export2PDF(xTraReport, DirectoryName, FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        int GenerateReport(DataSet ds)
        {
            int result = -1;
            string _messageCode = "MSTD0000BERR  : ";
            string fileExtension = ".pdf";
            string destinationPath_ZipPath = string.Empty;
            string zipFileName = string.Empty;

            string counterMeasureAttachment = string.Empty;

            List<string> boiAtt = new List<string>();
            List<string> storeAtt = new List<string>();
            List<string> pdfAtt = new List<string>();

            try
            {
                string LayoutTemplate = cfgLayoutSection;  //Report template name
                string OutputFolderName = oDirectoryName;  //Output directory name
                string OutputFileName = oFileName; //Output file name

                if (string.IsNullOrEmpty(LayoutTemplate))
                {
                    string ErrMsg = string.Format(CommonMessage.E_CONFIG, "LayoutSection");
                    throw (new Exception(ErrMsg));
                }
                if (!System.IO.File.Exists(LayoutTemplate))
                {
                    string ErrMsg = string.Format(CommonMessageBatch.MSTD0013AERR, string.Concat("<", LayoutTemplate, ">"));
                    throw (new System.IO.FileNotFoundException(ErrMsg));
                }


                //Rename file (add datetime in output file)
                OutputFileName = string.Format(OutputFileName, DateTime.Now);
                OutputFileName = OutputFileName.Replace(".pdf", string.Empty);

                foreach (DataRow row in ds.Tables["DISP_BOI_ATTACH"].Rows)
                {
                    boiAtt.Add(row[1].ToString());
                }

                foreach (DataRow row in ds.Tables["DISP_STORE_ATTACH"].Rows)
                {
                    storeAtt.Add(row[1].ToString());
                }

                foreach (DataRow row in ds.Tables["DISP_ASSET_PDF"].Rows)
                {
                    pdfAtt.Add(row[1].ToString());
                }

                object counter_measure = ds.Tables["DISP_DETAIL_REPORT"].Rows[0]["LOSS_COUNTER_MEASURE"];
                if (counter_measure != DBNull.Value)
                {
                    counterMeasureAttachment = ds.Tables["DISP_DETAIL_REPORT"].Rows[0]["LOSS_COUNTER_MEASURE"].ToString();
                }

                if ((boiAtt.Count > 0) || (storeAtt.Count > 0) || (pdfAtt.Count > 0) || (!string.IsNullOrEmpty(counterMeasureAttachment)))
                {
                    zipSourceDirectory = Path.Combine(OutputFolderName, "LFD02520BO_ZipFolder");
                    if (!Directory.Exists(zipSourceDirectory))
                    {
                        try
                        {
                            Directory.CreateDirectory(zipSourceDirectory);
                        }
                        catch (Exception ex)
                        {
                            InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);
                        }

                        foreach (string attachment in boiAtt)
                        {
                            if (File.Exists(attachment))
                                File.Copy(attachment, zipSourceDirectory + "\\" + Path.GetFileName(attachment));
                        }

                        foreach (string attachment in storeAtt)
                        {
                            if (File.Exists(attachment))
                                File.Copy(attachment, zipSourceDirectory + "\\" + Path.GetFileName(attachment));
                        }

                        foreach (string attachment in pdfAtt)
                        {
                            if (File.Exists(attachment))
                                File.Copy(attachment, zipSourceDirectory + "\\" + Path.GetFileName(attachment));
                        }

                        if (File.Exists(counterMeasureAttachment))
                        {
                            File.Copy(counterMeasureAttachment, zipSourceDirectory + "\\" + Path.GetFileName(counterMeasureAttachment));
                        }
                        //Gernerate Report File (PDF)
                        GenerateFile(ds,
                            zipSourceDirectory,
                            OutputFileName,
                            LayoutTemplate);

                        // zip process 
                        zipFileName = oFileName;
                        zipFileName = zipFileName.Replace(".pdf", ".zip");
                        zipFileName = string.Format(zipFileName, DateTime.Now);
                        destinationPath_ZipPath = OutputFolderName + @"\" + zipFileName;
                        bool ZipSuccess = false;
                        ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                        if (ZipSuccess)
                        { _iProcessStatus = 1; }
                        fileExtension = ".zip";
                    }
                }
                else
                {
                    //Gernerate Report File (PDF)
                    GenerateFile(ds,

                        OutputFolderName,
                        OutputFileName,
                        LayoutTemplate);
                }



                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                // Insert File Download
                if (fileExtension.Equals(".pdf"))
                {
                    _detail.Description = string.Format(SendingBatch.I_GENFILE_END, OutputFolderName, OutputFileName + fileExtension);
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, OutputFolderName + @"\" + OutputFileName + fileExtension);
                }
                else
                {
                    _detail.Description = string.Format(SendingBatch.I_GENFILE_END, OutputFolderName, zipFileName);
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);
                }
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                result = 1;
            }
            catch (Exception ex)
            {
                Directory.Delete(zipSourceDirectory);
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                throw ex;
            }
            return result;
        }

        private int GenerateMultipleReport(DataSet ds, string docno)
        {
            int result = -1;
            string _messageCode = "MSTD0000BERR  : ";
            try
            {
                string LayoutTemplate = cfgLayoutSection;  //Report template name
                string OutputFolderName = oDirectoryName;  //Output directory name
                string OutputFileName = Regex.Replace(docno, @"[^0-9a-zA-Z\._]", string.Empty) + "_" + oFileName; //Output file name

                string boiAttachment = string.Empty;
                string storeAttachment = string.Empty;
                string pdfAttachment = string.Empty;
                string counterMeasureAttachment = string.Empty;

                List<string> boiAtt = new List<string>();
                List<string> storeAtt = new List<string>();
                List<string> pdfAtt = new List<string>();


                if (string.IsNullOrEmpty(LayoutTemplate))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(LayoutTemplate))
                {
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, LayoutTemplate));
                }

                //Rename file (add datetime in output file)
                OutputFileName = string.Format(OutputFileName, DateTime.Now);
                OutputFileName = OutputFileName.Replace(".pdf", string.Empty);
                zipSourceDirectory = Path.Combine(OutputFolderName, "LFD02520BO_ZipFolder");
                if (!Directory.Exists(zipSourceDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(zipSourceDirectory);
                    }
                    catch (Exception ex)
                    {
                        InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);
                    }
                }


                foreach (DataRow row in ds.Tables["DISP_BOI_ATTACH"].Rows)
                {
                    boiAtt.Add(row[1].ToString());
                }

                foreach (DataRow row in ds.Tables["DISP_STORE_ATTACH"].Rows)
                {
                    storeAtt.Add(row[1].ToString());
                }

                foreach (DataRow row in ds.Tables["DISP_ASSET_PDF"].Rows)
                {
                    pdfAtt.Add(row[1].ToString());
                }

                object counter_measure = ds.Tables["DISP_DETAIL_REPORT"].Rows[0]["LOSS_COUNTER_MEASURE"];
                if (counter_measure != DBNull.Value)
                {
                    counterMeasureAttachment = ds.Tables["DISP_DETAIL_REPORT"].Rows[0]["LOSS_COUNTER_MEASURE"].ToString();
                }

                if ((boiAtt.Count > 0) || (storeAtt.Count > 0) || (pdfAtt.Count > 0) || (!string.IsNullOrEmpty(counterMeasureAttachment)))
                {
                    string zipSubSourceDirectory = Path.Combine(OutputFolderName, "LFD02520BO_ZipFolder\\LFD02520BO_ZipFolder_Att");
                    if (!Directory.Exists(zipSubSourceDirectory))
                    {
                        try
                        {
                            Directory.CreateDirectory(zipSubSourceDirectory);
                        }
                        catch (Exception ex)
                        {
                            InsertLog(_messageCode + "Cannot create directory :" + zipSubSourceDirectory + "(zip Directory) because  : " + ex.Message);
                        }
                    }

                    foreach (string attachment in boiAtt)
                    {
                        if (File.Exists(attachment))
                            File.Copy(attachment, zipSubSourceDirectory + "\\" + Path.GetFileName(attachment));
                    }

                    foreach (string attachment in storeAtt)
                    {
                        if (File.Exists(attachment))
                            File.Copy(attachment, zipSubSourceDirectory + "\\" + Path.GetFileName(attachment));
                    }

                    foreach (string attachment in pdfAtt)
                    {
                        if (File.Exists(attachment))
                            File.Copy(attachment, zipSubSourceDirectory + "\\" + Path.GetFileName(attachment));
                    }

                    if (File.Exists(counterMeasureAttachment))
                    {
                        File.Copy(counterMeasureAttachment, zipSubSourceDirectory + "\\" + Path.GetFileName(counterMeasureAttachment));
                    }


                    //Gernerate Report File (PDF)
                    GenerateFile(ds,
                        zipSubSourceDirectory,
                        OutputFileName,
                        LayoutTemplate);


                    // zip process 
                    string zipFileName = OutputFileName + ".zip";
                    //zipFileName = zipFileName.Replace(".pdf", ".zip");
                    zipFileName = string.Format(zipFileName, DateTime.Now);
                    string destinationPath_ZipPath = OutputFolderName + @"\LFD02520BO_ZipFolder\" + zipFileName;
                    bool ZipSuccess = false;
                    ZipSuccess = ZipDataToFile(zipSubSourceDirectory, destinationPath_ZipPath);

                    if (ZipSuccess)
                    { _iProcessStatus = 1; }
                }
                else
                {
                    //No Attached file
                    GenerateFile(ds,
                        zipSourceDirectory,
                        OutputFileName,
                        LayoutTemplate);
                }



                _log4Net.WriteErrorLogFile("Generated :" + OutputFileName);

                ////Gernerate PDF File
                //GenerateFile(ds, zipSourceDirectory, OutputFileName, LayoutTemplate);
                result = 1;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }

        int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if ((ds != null) && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                {
                    CountRow += dt.Rows.Count;
                }
            }
            return CountRow;
        }

        byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }

        private void InsertLog(string Log_Description)
        {
            _log = new DetailLogData();
            _log.AppID = BLoggingData.AppID;
            _log.Status = eLogStatus.Processing;
            _log.Level = eLogLevel.Error;
            _log.Description = Log_Description;
            BLogging.InsertDetailLog(_log);

            _log4Net.WriteErrorLogFile(Log_Description);
        }

        private bool ZipDataToFile(string sourcePathDirectory, string destinationPath_ZipPath)
        {
            bool _valueReturn;
            string _messageCode = "MSTD0000BERR  : ";
            _valueReturn = false;
            try
            {

                destinationPath_ZipPath = string.Format(destinationPath_ZipPath, DateTime.Now);
                //-------------------
                ZipFile.CreateFromDirectory(sourcePathDirectory, destinationPath_ZipPath);
                _valueReturn = true;
                Directory.Delete(sourcePathDirectory, true);
            }
            catch (Exception ex)
            {
                InsertLog(_messageCode + "Cannot zip file because :" + ex.Message);
            }
            return _valueReturn;
        }
    }

}
