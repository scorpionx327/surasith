﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace LFD02520_AssetDisposeDetail
{
    class LFD02520DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02520DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }                   

        public DataSet GetAssetDisposeData(string DOC_NO)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02520_GetAssetDisposeData");
                cmd.Parameters.AddWithValue("@DOC_NO", DOC_NO);

                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);

                ds.DataSetName = "ReportDataSource";
                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0)
                        table.TableName = "DISP_HEADMSG";
                    else if (ii == 1)
                        table.TableName = "DISP_SP_APPR01";
                    else if (ii == 2)
                        table.TableName = "DISP_SP_APPR02";
                    else if (ii == 3)
                        table.TableName = "DISP_SP_APPR03";
                    else if (ii == 4)
                        table.TableName = "DISP_SP_APPR04";
                    else if (ii == 5)
                        table.TableName = "DISP_CRNT_APPROVER";
                    else if (ii == 6)
                        table.TableName = "DISP_BOI_APPROVER";
                    else if (ii == 7)
                        table.TableName = "DISP_STORE_APPROVER";
                    else if (ii == 8)
                        table.TableName = "DISP_FA_APPROVER";
                    else if (ii == 9)
                        table.TableName = "DISP_DETAIL_REQUEST_P1";
                    else if (ii == 10)
                        table.TableName = "DISP_DETAIL_REQUEST";
                    else if (ii == 11)
                        table.TableName = "DISP_SUMMARY";
                    else if (ii == 12)
                        table.TableName = "DISP_DETAIL_REPORT";
                    else if (ii == 13)
                        table.TableName = "DISP_ASSET_PHOTO";
                    else if (ii == 14)
                        table.TableName = "DISP_ASSET_PDF";
                    else if (ii == 15)
                        table.TableName = "DISP_BOI_ATTACH";
                    else if (ii == 16)
                        table.TableName = "DISP_STORE_ATTACH";
                    else if (ii == 17)
                        table.TableName = "DISP_DETAIL_FAM";
                    else if (ii == 18)
                        table.TableName = "DISP_DETAIL_ENV";

                    ii++;
                }

                int rowCount = ds.Tables["DISP_DETAIL_REQUEST_P1"].Rows.Count;
                if (rowCount > 0)
                    return ds;
                else
                    return null;
            
            }

            catch (Exception ex)
            {
                throw (ex);
            }

            finally
            {
                _db.Close();
            }

            //return ds;
        }


    }
}
