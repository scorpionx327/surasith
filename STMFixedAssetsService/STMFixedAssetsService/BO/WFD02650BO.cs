﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.WFD02650;

namespace th.co.toyota.stm.fas.BO
{
    public class WFD02650BO
    {
        private WFD02650DAO dao;
        public WFD02650BO() { dao = new WFD02650DAO(); }

        public bool Login(string EMP_CODE, string COMPANY, common.Log4NetFunction _log)
        {
            try
            {
                
                bool _F = dao.Login(EMP_CODE.Trim(), COMPANY.Trim());
                _log.WriteInfoLogFile("Login with " + EMP_CODE + " / result " + _F);
                return _F;
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("Login");
                _log.WriteErrorLogFile(ex.Message, ex);

                return false;
            }
        }

        public WFD02650_PlanModel DownloadPlanData(string EMP_CODE, common.Log4NetFunction _log)
        {
            try
            {
                WFD02650_PlanModel model = new WFD02650_PlanModel();

                model.ServerDateTime = DateTime.Now;
                model.Employees = dao.GetEmployeeWorkData(EMP_CODE);
                model.StockTakeH = dao.GetStockTakeHData(model.Employees[0].IS_FAADMIN);
                model.StockTakeDs = dao.GetStockTakeDDatas(string.Join(",", model.Employees.GroupBy(m => m.SV_EMP_CODE).Select(m => m.Key).ToArray()), model.StockTakeH.STOCK_TAKE_KEY, model.Employees.Select(m => m.IS_FAADMIN).First());
                model.StockTakeDPerSVs = dao.GetStockTakeDPerSVDatas(string.Join(",", model.Employees.GroupBy(m => m.SV_EMP_CODE).Select(m => m.Key).ToArray()), model.StockTakeH.STOCK_TAKE_KEY, model.Employees.Select(m => m.IS_FAADMIN).First());

                foreach (var d in model.StockTakeDPerSVs)
                {
                    d.DATE_FROM = d.DATE_FROM.Value.Date + new TimeSpan(int.Parse(d.TIME_START.Split(':')[0]), int.Parse(d.TIME_START.Split(':')[1]), 0);
                    d.DATE_TO = d.DATE_TO.Value.Date + new TimeSpan(int.Parse(d.TIME_END.Split(':')[0]), int.Parse(d.TIME_END.Split(':')[1]), 0);
                    if (string.IsNullOrEmpty(d.IS_LOCK.Trim())) d.IS_LOCK = "Y";
                }

                return model;
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("DownloadPlanData");
                _log.WriteErrorLogFile(ex.Message, ex);

                return null;
            }
        }
        public List<WFD02650_EmployeeModel> DownloadPlan_Employee(string EMP_CODE, common.Log4NetFunction _log)
        {
            try
            {
                return dao.GetEmployeeWorkData(EMP_CODE);
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("DownloadPlan_Employee");
                _log.WriteErrorLogFile(ex.Message, ex);

                return null;
            }
        }
        public TB_R_STOCK_TAKE_H DownloadPlan_Header(string IS_FAADMIN, common.Log4NetFunction _log)
        {
            try
            {
                return dao.GetStockTakeHData(IS_FAADMIN);
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("DownloadPlan_Header");
                _log.WriteErrorLogFile(ex.Message, ex);

                return null;
            }
        }
        public List<TB_R_STOCK_TAKE_D> DownloadPlan_Details(string STOCK_TAKE_KEY, string SV_EMP_CODE, string IS_FAADMIN, common.Log4NetFunction _log)
        {
            try
            {
                return dao.GetStockTakeDDatas(SV_EMP_CODE, STOCK_TAKE_KEY, IS_FAADMIN);
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("DownloadPlan_Details");
                _log.WriteErrorLogFile(ex.Message, ex);

                return null;
            }
        }
        public List<TB_R_STOCK_TAKE_D_PER_SV> DownloadPlan_DetailsPerSV(string SV_EMP_CODE, string STOCK_TAKE_KEY, string IS_FAADMIN, common.Log4NetFunction _log)
        {
            try
            {
                return dao.GetStockTakeDPerSVDatas(SV_EMP_CODE, STOCK_TAKE_KEY, IS_FAADMIN);
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("DownloadPlan_DetailsPerSV");
                _log.WriteErrorLogFile(ex.Message, ex);

                return null;
            }
        }

        public bool AddStockTakeInvalidScanData(WFD02650_InvalidScanModel data, common.Log4NetFunction _log)
        {
            try
            {
                dao.AddStockTakeInvalidScanData(new TB_R_STOCK_TAKE_INVALID_CHECKING()
                {
                    EMP_CODE = data.EMP_CODE,
                    SCAN_DATE = CVCDT(data.SCAN_DATE),
                    COST_CODE = data.COST_CODE
                }, data.BARCODE);
                return true;
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("AddStockTakeInvalidScanData");
                _log.WriteErrorLogFile(ex.Message, ex);

                return false;
            }
        }

        public bool UpdatePlan(string STOCK_TAKE_KEY, string EMP_CODE, List<TB_R_STOCK_TAKE_D> StockTakeDDatas, common.Log4NetFunction _log)
        {
            WFD02650DAO d = new WFD02650DAO();
            d.beginTransaction();
            try
            {
                d.UpdateStockTakeHPlanStatus(STOCK_TAKE_KEY, EMP_CODE);
                foreach (var s in StockTakeDDatas)
                {
                    d.UpdateStockTakeDScanData(s);
                }

                d.commitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("UpdatePlan");
                _log.WriteErrorLogFile(ex.Message, ex);

                d.rollbackTransaction();
                return false;
            }
        }

        public bool UpdateStockTakeDScanData(WFD02650_ScanItemModel data, common.Log4NetFunction _log)
        {
            try
            {
                dao.UpdateStockTakeDScanData(new TB_R_STOCK_TAKE_D()
                {

                    STOCK_TAKE_KEY = data.STOCK_TAKE_KEY,
                    ASSET_NO = data.ASSET_NO,
                    EMP_CODE = data.EMP_CODE,
                    CHECK_STATUS = data.CHECK_STATUS,
                    COUNT_TIME = data.COUNT_TIME,
                    SCAN_DATE = CVCDT(data.SCAN_DATE),
                    START_COUNT_TIME = CVCDT(data.START_COUNT_TIME),
                    END_COUNT_TIME = CVCDT(data.END_COUNT_TIME)

                });
                //_log.WriteInfoLogFile("UpdateStockTakeDScanData OK");
                return true;
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("UpdateStockTakeDScanData");
                _log.WriteErrorLogFile(ex.Message, ex);
                return false;
            }
        }

        public bool LockEmp(string STOCK_TAKE_KEY, string SV_EMP_CODE,common.Log4NetFunction _log)
        {
            try
            {
                dao.LockEmp(new TB_R_STOCK_TAKE_D_PER_SV()
                {
                    STOCK_TAKE_KEY = STOCK_TAKE_KEY,
                    SV_EMP_CODE = SV_EMP_CODE,
                });
                return true;
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("LockEmp");
                _log.WriteErrorLogFile(ex.Message, ex);
                return false;
            }
        }

        private Nullable<DateTime> CVCDT(WFD02650_ServerDateTime cd)
        {
            if (cd == null) return null;
            else return DateTime.ParseExact(cd.Value, cd.Format, new CultureInfo(cd.CultureStr));
        }

        public int CheckStockPlanChange(string STOCK_TAKE_KEY, string UPDATE_DATE, common.Log4NetFunction _log)
        {
            dao.beginTransaction();

            try
            {
                return dao.CheckStockPlanChange(STOCK_TAKE_KEY, UPDATE_DATE);
            }
            catch (Exception ex)
            {
                _log.WriteInfoLogFile("CheckStockPlanChange");
                _log.WriteErrorLogFile(ex.Message, ex);
                return -1;
            }
        }
    }
}