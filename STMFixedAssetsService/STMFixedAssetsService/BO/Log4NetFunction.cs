﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.common
{
    public class Log4NetFunction
    {
        protected static readonly ILog fLog = LogManager.GetLogger("Log4NetConfiguration");
        public Log4NetFunction()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //
            // TODO: Add constructor logic here
            //
            log4net.Config.XmlConfigurator.Configure();
        }
        public void WriteDebugLogFile(string _error)
        {
            fLog.Debug(_error);
        }
        public void WriteInfoLogFile(string _error)
        {
            fLog.Info(_error);
        }
        public void WriteErrorLogFile(string _error, Exception innerex)
        {
            fLog.Error(_error, innerex);
        }
        public void WriteErrorLogFile(string _error)
        {
            fLog.Error(_error);
        }
    }
}
