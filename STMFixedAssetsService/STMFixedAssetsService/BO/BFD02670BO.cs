﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.BFD02670;
using th.co.toyota.stm.fas.DAO;

namespace th.co.toyota.stm.fas.BO
{
    public class BFD02670BO
    {

        public bool SaveUploadPlan(List<BFD02670_ScanModel> scans, List<BFD02670_InvalidModel> invalids, string employeeNo)
        {
            BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("START SaveUploadPlan");
            dao.beginTransaction();
            try
            {
                List<TB_R_STOCK_TAKE_D> dDatas = genStockTake_ds(scans);
                //List<TB_R_STOCK_TAKE_INVALID_CHECKING> invalidCheckings = genInvalidateChecking(invalids, dDatas[0]);
                List<TB_R_STOCK_TAKE_INVALID_CHECKING> invalidCheckings = genInvalidateChecking(invalids);
                //dao.insertLog("PREPARED");
                //dao.insertLog(dDatas.Count.ToString());
                if (dDatas.Count > 0)
                {
                    dao.UpdateStockTake_H_Plan_Status(dDatas[0].STOCK_TAKE_KEY, employeeNo, "S");

                    foreach (var d in dDatas)
                    {
                        dao.UpdateStockTake_D(d);
                    }
                }

                foreach (var i in invalidCheckings)
                {
                    dao.AddStockTakeInvalidScanData(i);
                }

                dao.commitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dao.rollbackTransaction();
                return false;
            }
        }

        public bool SaveUploadScanBySpecificRec(List<BFD02670_ScanModel> scans)
        {
            BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("START SaveUploadScanBySpecificRec");
            dao.beginTransaction();
            try
            {
                List<TB_R_STOCK_TAKE_D> dDatas = genStockTake_ds(scans);
                //dao.insertLog("PREPARED");
                //dao.insertLog(dDatas.Count.ToString());
                if (dDatas.Count > 0)
                {
                    foreach (var d in dDatas)
                    {
                        dao.UpdateStockTake_D(d);
                    }
                }

                dao.commitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dao.rollbackTransaction();
                return false;
            }
        }

        public bool SaveUploadInvalidBySpecificRec(List<BFD02670_InvalidModel> invalids)
        {
            BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("START SaveUploadInvalidBySpecificRec");
            dao.beginTransaction();
            try
            {
                List<TB_R_STOCK_TAKE_INVALID_CHECKING> invalidCheckings = genInvalidateChecking(invalids);
                //dao.insertLog("PREPARED");

                foreach (var i in invalidCheckings)
                {
                    dao.AddStockTakeInvalidScanData(i);
                }

                dao.commitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                dao.rollbackTransaction();
                return false;
            }
        }

        private List<TB_R_STOCK_TAKE_D> genStockTake_ds(List<BFD02670_ScanModel> scans)
        {
            List<TB_R_STOCK_TAKE_D> datas = new List<TB_R_STOCK_TAKE_D>();

            foreach (var d in scans)
            {
                datas.Add(new TB_R_STOCK_TAKE_D()
                {
                    STOCK_TAKE_KEY = d.STOCK_TAKE_KEY,
                    ASSET_NO = d.ASSET_NO,
                    EMP_CODE = d.EMP_CODE,
                    CHECK_STATUS = d.CHECK_STATUS,
                    SCAN_DATE = CVCDT(d.DATE),
                    START_COUNT_TIME = CVCDT(d.START_COUNT_TIME),
                    END_COUNT_TIME = CVCDT(d.END_COUNT_TIME),
                    COUNT_TIME = d.COUNT_TIME
                });
            }

            return datas;
        }
        //private List<TB_R_STOCK_TAKE_INVALID_CHECKING> genInvalidateChecking(List<BFD02670_InvalidModel> invalids, TB_R_STOCK_TAKE_D item)
        private List<TB_R_STOCK_TAKE_INVALID_CHECKING> genInvalidateChecking(List<BFD02670_InvalidModel> invalids)
        {
            List<TB_R_STOCK_TAKE_INVALID_CHECKING> datas = new List<TB_R_STOCK_TAKE_INVALID_CHECKING>();

            foreach (var d in invalids)
            {
                datas.Add(new TB_R_STOCK_TAKE_INVALID_CHECKING()
                {
                    ASSET_NO = d.BARCODE, // User asset no to keep barcode data
                    EMP_CODE = d.EMP_CODE,
                    SCAN_DATE = CVCDT(d.SCAN_DATE),
                    //STOCK_TAKE_KEY = item.STOCK_TAKE_KEY,
                    COST_CODE = d.COST_CODE
                });
            }

            return datas;
        }

        private Nullable<DateTime> CVCDT(BFD02670_ServerDateTime cd)
        {
            if (cd == null) return null;
            else return DateTime.ParseExact(cd.Value, cd.Format, new CultureInfo(cd.CultureStr));
        }

    }
}