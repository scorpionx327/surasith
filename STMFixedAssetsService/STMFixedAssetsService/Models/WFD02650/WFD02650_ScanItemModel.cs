﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02650
{
    public class WFD02650_ScanItemModel
    {
        public string STOCK_TAKE_KEY { get; set; }
        public string ASSET_NO { get; set; }
        public string EMP_CODE { get; set; }
        public string CHECK_STATUS { get; set; }
        public int COUNT_TIME { get; set; }
        public WFD02650_ServerDateTime SCAN_DATE { get; set; }
        public WFD02650_ServerDateTime START_COUNT_TIME { get; set; }
        public WFD02650_ServerDateTime END_COUNT_TIME { get; set; }
    }
}
