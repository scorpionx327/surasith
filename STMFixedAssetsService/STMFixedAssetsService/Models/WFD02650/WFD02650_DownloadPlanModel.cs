﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.Models.WFD02650
{
    public class WFD02650_PlanModel
    {
        public DateTime ServerDateTime { get; set; }
        public List<WFD02650_EmployeeModel> Employees { get; set; }
        public TB_R_STOCK_TAKE_H StockTakeH { get; set; }
        public List<TB_R_STOCK_TAKE_D> StockTakeDs { get; set; }
        public List<TB_R_STOCK_TAKE_D_PER_SV> StockTakeDPerSVs { get; set; }
    }
}
