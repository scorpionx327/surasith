﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02650
{
    public class WFD02650_EmployeeModel
    {
        public string EMP_NO { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        public string IS_FAADMIN { get; set; }
        public string IS_SV { get; set; }
        public string SV_EMP_CODE { get; set; }
        public string UPDATE_DATE { get; set; }
    }
}