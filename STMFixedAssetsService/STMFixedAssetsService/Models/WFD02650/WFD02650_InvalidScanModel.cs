﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02650
{
    public class WFD02650_InvalidScanModel
    {
        public string COST_CODE { get; set; }
        public string BARCODE { get; set; }
        public string EMP_CODE { get; set; }
        public WFD02650_ServerDateTime SCAN_DATE { get; set; }
    }
}
