﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02650
{
    public class WFD02650_ServerDateTime
    {
        public string Value { get; set; }
        public string Format { get; set; }
        public string CultureStr { get; set; }
    }
}
