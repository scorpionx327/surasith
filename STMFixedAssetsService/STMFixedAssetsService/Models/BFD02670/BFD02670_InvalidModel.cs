﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.BFD02670
{
    public class BFD02670_InvalidModel
    {
        public string COST_CODE { get; set; }
        public string EMP_CODE { get; set; }
        public string BARCODE { get; set; }
        public BFD02670_ServerDateTime SCAN_DATE { get; set; }
    }
}
