﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.BFD02670
{
    public class BFD02670_ScanModel
    {
        public string STOCK_TAKE_KEY { get; set; }
        public string ASSET_NO { get; set; }
        public string EMP_CODE { get; set; }
        public string CHECK_STATUS { get; set; }
        public BFD02670_ServerDateTime DATE { get; set; }
        public BFD02670_ServerDateTime START_COUNT_TIME { get; set; }
        public BFD02670_ServerDateTime END_COUNT_TIME { get; set; }
        public Nullable<int> COUNT_TIME { get; set; }
    }
}
