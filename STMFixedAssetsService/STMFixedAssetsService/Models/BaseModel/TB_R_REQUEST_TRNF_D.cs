//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace th.co.toyota.stm.fas.Models.BaseModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_R_REQUEST_TRNF_D
    {
        public string DOC_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string COST_CODE { get; set; }
        public Nullable<decimal> COST { get; set; }
        public Nullable<System.DateTime> DATE_IN_SERVICE { get; set; }
        public Nullable<decimal> BOOK_VALUE { get; set; }
        public Nullable<decimal> UNIT { get; set; }
        public Nullable<decimal> ACCUM_DEPREC { get; set; }
        public string TRNF_REASON { get; set; }
        public string BOI_NO { get; set; }
        public string BOI_CHECK_FLAG { get; set; }
        public string BOI_ATTACH_DOC { get; set; }
        public string DELETE_FLAG { get; set; }
        public Nullable<System.DateTime> CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public Nullable<System.DateTime> UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
    }
}
