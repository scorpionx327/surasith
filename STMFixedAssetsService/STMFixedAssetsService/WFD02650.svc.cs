﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using th.co.toyota.stm.fas.BO;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.WFD02650;
using th.co.toyota.stm.fas.common ;
namespace th.co.toyota.stm.fas
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WFD02650" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WFD02650.svc or WFD02650.svc.cs at the Solution Explorer and start debugging.
    public class WFD02650 : IWFD02650
    {
        private static Log4NetFunction _log;
        public bool UserLogin(string EMP_CODE,string COMPANY)
        {
            _log = new Log4NetFunction();

            bool _f = new WFD02650BO().Login(EMP_CODE, COMPANY, _log);
            return _f;
        }

        public WFD02650_PlanModel DownloadPlanData(string EMP_CODE)
        {
            _log = new Log4NetFunction();

            WFD02650_PlanModel _f = new WFD02650BO().DownloadPlanData(EMP_CODE, _log);
            return _f;
        }


        public bool AddStockTakeInvalidScanData(WFD02650_InvalidScanModel data)
        {
            _log = new Log4NetFunction();

            bool _f = new WFD02650BO().AddStockTakeInvalidScanData(data, _log);
            _log = null;
            return _f;
        }

        public bool UpdatePlan(string STOCK_TAKE_KEY, string EMP_CODE, List<TB_R_STOCK_TAKE_D> StockTakeDDatas)
        {
            _log = new Log4NetFunction();

            bool _f = new WFD02650BO().UpdatePlan(STOCK_TAKE_KEY, EMP_CODE, StockTakeDDatas, _log);
            _log = null;
            return _f;
        }
        public bool UpdateStockTakeDScanData(WFD02650_ScanItemModel data)
        {
            _log = new Log4NetFunction();

            bool _f = new WFD02650BO().UpdateStockTakeDScanData(data, _log);
            _log = null;
            return _f;
        }

        public bool LockEmp(string STOCK_TAKE_KEY, string SV_EMP_CODE)
        {
            _log = new Log4NetFunction();

            bool _f = new WFD02650BO().LockEmp(STOCK_TAKE_KEY, SV_EMP_CODE, _log);
            return _f;
        }

        public int CheckStockPlanChange(string STOCK_TAKE_KEY, string UPDATE_DATE)
        {
            _log = new Log4NetFunction();

            int _f = new WFD02650BO().CheckStockPlanChange(STOCK_TAKE_KEY, UPDATE_DATE, _log);
            return _f;
        }

        public bool TestConnection()
        {
            return true;
        }


        #region Download plan ย่อยทีละอัน

        public List<WFD02650_EmployeeModel> DownloadPlan_Employee(string EMP_CODE)
        {
            _log = new Log4NetFunction();

            List < WFD02650_EmployeeModel > _f = new WFD02650BO().DownloadPlan_Employee(EMP_CODE, _log);
            return _f;
        }

        public TB_R_STOCK_TAKE_H DownloadPlan_Header(string IS_FAADMIN)
        {
            _log = new Log4NetFunction();

            TB_R_STOCK_TAKE_H _f = new WFD02650BO().DownloadPlan_Header(IS_FAADMIN, _log);
            return _f;
        }

        public List<TB_R_STOCK_TAKE_D> DownloadPlan_Details(string STOCK_TAKE_KEY, string SV_EMP_CODE, string IS_FAADMIN)
        {
            _log = new Log4NetFunction();

            List < TB_R_STOCK_TAKE_D > _f = new WFD02650BO().DownloadPlan_Details(STOCK_TAKE_KEY, SV_EMP_CODE, IS_FAADMIN, _log);
            return _f;
        }

        public List<TB_R_STOCK_TAKE_D_PER_SV> DownloadPlan_DetailsPerSV(string SV_EMP_CODE, string STOCK_TAKE_KEY, string IS_FAADMIN)
        {
            _log = new Log4NetFunction();

            List < TB_R_STOCK_TAKE_D_PER_SV > _f = new WFD02650BO().DownloadPlan_DetailsPerSV(STOCK_TAKE_KEY, SV_EMP_CODE, IS_FAADMIN, _log);
            return _f;
        }

        public WFD02650_ServerDateTime GetServerDateTime()
        {
            WFD02650_ServerDateTime res = new WFD02650_ServerDateTime();
            var dt = DateTime.Now;
            res.Format = "dd-MM-yyyy HH:mm:ss";
            res.CultureStr = "en-Us";
            res.Value = dt.ToString(res.Format, new CultureInfo(res.CultureStr));
            return res;
        }

        #endregion
    }
}
