﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using th.co.toyota.stm.fas.BO;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.BFD02670;

namespace th.co.toyota.stm.fas
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BFD02670" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BFD02670.svc or BFD02670.svc.cs at the Solution Explorer and start debugging.
    public class BFD02670 : IBFD02670
    {
        public bool Upload(List<BFD02670_ScanModel> scans, List<BFD02670_InvalidModel> invalids, string employeeNo)
        {
            //BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("Called Upload");
            BFD02670BO bo = new BFD02670BO();
            return bo.SaveUploadPlan(scans, invalids, employeeNo);
        }

        public bool UploadScanBySpecificRec(List<BFD02670_ScanModel> scans)
        {
            //BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("Called UploadBySpecificRec");
            BFD02670BO bo = new BFD02670BO();
            return bo.SaveUploadScanBySpecificRec(scans);
        }

        public bool UploadInvalidBySpecificRec(List<BFD02670_InvalidModel> invalids)
        {
            //BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("Called UploadBySpecificRec");
            BFD02670BO bo = new BFD02670BO();
            return bo.SaveUploadInvalidBySpecificRec(invalids);
        }

        public void UpdateStockHStatusS(string stock_take_key, string employeeNo)
        {
            BFD02670DAO dao = new BFD02670DAO();
            //dao.insertLog("Called UpdateStockHStatusS");
            dao.UpdateStockTake_H_Plan_Status(stock_take_key, employeeNo, "S");
        }

        public bool CheckLockWithServerDate(string stock_take_key, string employeeNo)
        {
            BFD02670DAO dao = new BFD02670DAO();
            return dao.CheckLockWithServerDate(stock_take_key, employeeNo);
        }
    }
}
