﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.WFD02650;

namespace th.co.toyota.stm.fas
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWFD02650" in both code and config file together.
    [ServiceContract]
    public interface IWFD02650
    {
        [OperationContract]
        bool UserLogin(string EMP_CODE, string COMPANY);

        [OperationContract]
        WFD02650_PlanModel DownloadPlanData(string EMP_CODE);

        [OperationContract]
        bool AddStockTakeInvalidScanData(WFD02650_InvalidScanModel data);

        [OperationContract]
        bool UpdatePlan(string STOCK_TAKE_KEY, string EMP_CODE, List<TB_R_STOCK_TAKE_D> StockTakeDDatas);

        [OperationContract]
        bool LockEmp(string STOCK_TAKE_KEY, string EMP_CODE);

        [OperationContract]
        int CheckStockPlanChange(string STOCK_TAKE_KEY, string UPDATE_DATE);

        [OperationContract]
        bool UpdateStockTakeDScanData(WFD02650_ScanItemModel data);

        [OperationContract]
        bool TestConnection();

        [OperationContract]
        List<WFD02650_EmployeeModel> DownloadPlan_Employee(string EMP_CODE);
        [OperationContract]
        TB_R_STOCK_TAKE_H DownloadPlan_Header(string IS_FAADMIN);
        [OperationContract]
        List<TB_R_STOCK_TAKE_D> DownloadPlan_Details(string STOCK_TAKE_KEY, string SV_EMP_CODE, string IS_FAADMIN);
        [OperationContract]
        List<TB_R_STOCK_TAKE_D_PER_SV> DownloadPlan_DetailsPerSV(string SV_EMP_CODE, string STOCK_TAKE_KEY, string IS_FAADMIN);
        [OperationContract]
        WFD02650_ServerDateTime GetServerDateTime();
    }
}
