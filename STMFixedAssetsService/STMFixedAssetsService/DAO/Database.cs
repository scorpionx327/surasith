﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace th.co.toyota.stm.fas.DAO
{
    public class Database : SqlServerProvider
    {
        public Database() : base(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)
        {
        }
    }
}