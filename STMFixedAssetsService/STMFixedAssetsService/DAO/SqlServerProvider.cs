﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace th.co.toyota.stm.fas.DAO
{
    public class SqlServerProvider : DatabaseProvider
    {
        public SqlServerProvider(string connectionString)
            : base(connectionString)
        {

        }

        #region Open / Close Connection

        public void openConnection()
        {
            if (_connection.State != System.Data.ConnectionState.Open)
                _connection.Open();
        }

        public void closeConnection()
        {
            if (_connection.State != System.Data.ConnectionState.Closed)
                //_connection.Dispose();
                _connection.Close();
        }

        #endregion

        #region Transaction

        public void beginTransaction()
        {
            try
            {
                openConnection();
                if (_connection.State == System.Data.ConnectionState.Open)
                    _transaction = _connection.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void commitTransaction()
        {
            try
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void rollbackTransaction()
        {
            try
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _transaction.Rollback();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Execute data

        public void execute(string storeProcedureName, List<SqlParameter> parameters)
        {
            SqlDataAdapter ad = new SqlDataAdapter(storeProcedureName, _connection);
            ad.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            if (this._transaction != null)
            {
                ad.SelectCommand.Transaction = this._transaction;
            }

            for (int i = 0; i < parameters.Count; i++)
                ad.SelectCommand.Parameters.Add(parameters[i]);

            try
            {
                this.openConnection();

                ad.SelectCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    this.closeConnection();
            }

        }

        public List<T> executeDataToList<T>(string storeProcedureName)
        {
            return executeDataToList<T>(storeProcedureName, this._params);
        }
        public List<T> executeDataToList<T>(string storeProcedureName, List<SqlParameter> parameters)
        {
            List<T> result = new List<T>();
            SqlDataAdapter ad = new SqlDataAdapter(storeProcedureName, _connection);
            SqlDataReader myReader = null;
            ad.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            if (this._transaction != null)
            {
                ad.SelectCommand.Transaction = this._transaction;
            }

            for (int i = 0; i < parameters.Count; i++)
                ad.SelectCommand.Parameters.Add(parameters[i]);

            try
            {
                this.openConnection();

                myReader = ad.SelectCommand.ExecuteReader();
                result = _DataReaderMapToList<T>(myReader);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    this.closeConnection();
                if (!myReader.IsClosed)
                    myReader.Close();
            }

            return result;
        }

        public List<T> executeQueryDataToList<T>(string queryString)
        {
            List<T> result = new List<T>();
            SqlDataAdapter ad = new SqlDataAdapter(queryString, _connection);
            SqlDataReader myReader = null;
            ad.SelectCommand.CommandType = System.Data.CommandType.Text;

            if (this._transaction != null)
            {
                ad.SelectCommand.Transaction = this._transaction;
            }

            try
            {
                this.openConnection();

                myReader = ad.SelectCommand.ExecuteReader();
                result = _DataReaderMapToList<T>(myReader);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    this.closeConnection();
            }

            return result;
        }

        public T executeScalar<T>(string storeProcedureName)
        {
            return this.executeScalar<T>(storeProcedureName, this.Parameters);
        }
        public T executeScalar<T>(string storeProcedureName, List<SqlParameter> param)
        {
            SqlCommand cmm = new SqlCommand(storeProcedureName, _connection);
            if (_transaction != null) cmm.Transaction = _transaction;
            cmm.CommandType = CommandType.StoredProcedure;
            T retValue = default(T);

            for (int i = 0; i < param.Count; i++)
            {
                cmm.Parameters.Add(param[i]);
            }
            try
            {
                openConnection();

                retValue = (T)cmm.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                //if (_transaction == null) //Fixed Max Pool Size 2017/08/22
                    closeConnection();
            }

            return retValue;
        }

        public T executeScalarQuery<T>(string query)
        {
            SqlCommand cmm = new SqlCommand(query, _connection);
            if (_transaction != null) cmm.Transaction = _transaction;
            cmm.CommandType = CommandType.Text;
            T retValue = default(T);

            try
            {
                openConnection();

                retValue = (T)cmm.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    closeConnection();
            }

            return retValue;
        }

        public DataTable executeDataToDataTable(string storeProcedureName)
        {
            return executeDataToDataTable(storeProcedureName, null);
        }
        public DataTable executeDataToDataTable(string storeProcedureName, List<SqlParameter> parameters)
        {
            SqlDataAdapter ad = new SqlDataAdapter(storeProcedureName, _connection);
            DataTable dt = new DataTable();
            ad.SelectCommand.CommandType = CommandType.StoredProcedure;

            if (this._transaction != null)
            {
                ad.SelectCommand.Transaction = this._transaction;
            }


            if (parameters != null && parameters.Count > 0)
            {
                for (int i = 0; i < parameters.Count; i++)
                {
                    ad.SelectCommand.Parameters.Add(parameters[i]);
                }
            }

            try
            {
                openConnection();

                ad.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    closeConnection();
            }

            return dt;
        }
        public DataTable executeDataToDataTableWithPaging(string storeProcedureName, ref int rowCount)
        {
            return executeDataToDataTableWithPaging(storeProcedureName, this.Parameters, ref rowCount);
        }
        public DataTable executeDataToDataTableWithPaging(string storeProcedureName, List<SqlParameter> parameters, ref int rowCount)
        {
            SqlDataAdapter ad = new SqlDataAdapter(storeProcedureName, _connection);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ad.SelectCommand.CommandType = CommandType.StoredProcedure;

            if (this._transaction != null)
            {
                ad.SelectCommand.Transaction = this._transaction;
            }

            for (int i = 0; i < parameters.Count; i++)
            {
                ad.SelectCommand.Parameters.Add(parameters[i]);
            }
            try
            {
                openConnection();

                ad.Fill(ds);
                if (ds.Tables.Count > 1)
                {
                    dt = ds.Tables[0];
                    rowCount = (int)ds.Tables[1].Rows[0][0];
                }
                else
                {
                    rowCount = 0;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    closeConnection();
            }

            return dt;
        }

        public DataSet executeDataToDataSet(string storeProcedureName, List<SqlParameter> parameters)
        {
            SqlDataAdapter ad = new SqlDataAdapter(storeProcedureName, _connection);
            DataSet ds = new DataSet();

            ad.SelectCommand.CommandType = CommandType.StoredProcedure;

            if (this._transaction != null)
            {
                ad.SelectCommand.Transaction = this._transaction;
            }

            bool haveData = false;
            for (int i = 0; i < parameters.Count; i++)
            {
                ad.SelectCommand.Parameters.Add(parameters[i]);
            }
            try
            {
                openConnection();

                ad.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    haveData = true;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    closeConnection();
            }

            if (haveData)
                return ds;
            else
                return null;
        }

        #endregion
    }
}