﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace LFD02420_AssetTransferDetail
{
    class LFD02420DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02420DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataSet GetDataAssetTransferDetailReport(string DOC_NO)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02420_GetDataAssetTransferDetailReport");

                cmd.Parameters.AddWithValue("@DOC_NO", DOC_NO);                      
                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);

                ds.DataSetName = "ReportDataSource";

                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0)
                        table.TableName = "TRASFER_REQUEST";                    
                    if (ii == 1)
                        table.TableName = "ASSET_DETAIL";                    
                    if (ii == 2)
                        table.TableName = "TRANSFER_APPROVER";                    
                    if (ii == 3)
                        table.TableName = "BOI_APPROVER";                    
                    if (ii == 4)
                        table.TableName = "NEW_VP_APPROVER";
                    if (ii == 5)
                        table.TableName = "NEW_APPROVER";
                    if (ii == 6)
                        table.TableName = "ASSET_DETAIL_2";

                    ii++;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }
    }
}
