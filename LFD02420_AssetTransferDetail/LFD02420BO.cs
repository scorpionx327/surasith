﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02420_AssetTransferDetail
{
    class LFD02420BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02420DAO _dbconn = null;
        private DetailLogData _log = null;

        IFormatProvider _culture = new CultureInfo("en-US", true);
        public string zipSourceDirectory = string.Empty;
        #endregion

        public LFD02420BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
            BLoggingData.IProcessStatus = _iProcessStatus;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Asset transfer detail with approval and asset status check report";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02420DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                GetDataAssetTransferDetailReport();
                BLoggingData.IProcessStatus = _iProcessStatus;
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(_batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void GetDataAssetTransferDetailReport()
        {
            try
            {
                string zipFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                string BatchParam = BLoggingData.Arguments.Replace("'", "");

                string[] _p = BatchParam.Split('|');

                _dbconn = new LFD02420DAO();
                DataSet ds = new DataSet();
                string DOC_NO;
                if (_p.Length == 1)
                {
                    DOC_NO = _p[0];

                    ds = _dbconn.GetDataAssetTransferDetailReport(DOC_NO);

                    if (CountDataAllTable(ds) > 0)
                    {
                        PrepareDataSet(ds);
                        _iProcessStatus = GenerateReport(ds);
                    }
                    else
                    {
                        _log = new DetailLogData();
                        _log.AppID = BLoggingData.AppID;
                        _log.Status = eLogStatus.Processing;
                        _log.Level = eLogLevel.Error;
                        _log.Favorite = false;
                        _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                        BLogging.InsertDetailLog(_log);
                    }
                }
                else
                {
                    for (int i = 0; i < _p.Length ; i++)
                    {
                        DOC_NO = _p[i];
                        ds = _dbconn.GetDataAssetTransferDetailReport(DOC_NO);
                        if (CountDataAllTable(ds) > 0)
                        {
                            PrepareDataSet(ds);
                            GenerateMultipleReport(ds);
                        }
                    }
                    // zip process 
                         zipFileName = zipFileName.Replace(".pdf", ".zip");
                       zipFileName = string.Format(zipFileName, DateTime.Now);
                    string destinationPath_ZipPath = oDirectoryName + @"\" + zipFileName;
                        bool ZipSuccess = false;
                        ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                        if (ZipSuccess)
                        { _iProcessStatus = 1; }
                        // Insert log : File {1} is generated to {0}
                        DetailLogData _detail = new DetailLogData();
                        _detail.AppID = BLoggingData.AppID;
                        _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, destinationPath_ZipPath);
                        _detail.Level = eLogLevel.Information;
                         BLogging.InsertDetailLog(_detail);

                        // Insert File Download
                        BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);
               
                  
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }



        XtraReport GetXtraReportObject(string LayoutSection, DataSet dataSource)
        {
            var xtraReport = XtraReport.FromFile(LayoutSection, true);
            xtraReport.DataSource = dataSource;

            string AssetDetailLayoutSection = System.Configuration.ConfigurationManager.AppSettings["AssetDetailLayoutSection"];
            string TransApproveLayoutSection = System.Configuration.ConfigurationManager.AppSettings["ApproverLayoutSection"];
            string VPApproveLayoutSection = System.Configuration.ConfigurationManager.AppSettings["VPApproverLayoutSection"];
            string AssetDetail2LayoutSection = System.Configuration.ConfigurationManager.AppSettings["AssetDetail2LayoutSection"];

            //Get sub report template path [Asset Detail]
            CreateSubReportObject(xtraReport, PrepareAssetDetailDataSet(dataSource.Copy()), "AssetDetailReport", AssetDetailLayoutSection);

            //Get sub report template path [Transfer Approver]       
            CreateSubReportObject(xtraReport, dataSource.Copy(), "TransApproval", TransApproveLayoutSection);

            //Get sub report template path [BOI Approver]       
            CreateSubReportObject(xtraReport, PrepareApproveDataSet(dataSource.Copy(), "BOI_APPROVER"), "BOIApproval", TransApproveLayoutSection);

            //Get sub report template path [VP Approver]       
            CreateSubReportObject(xtraReport, PrepareApproveDataSet(dataSource.Copy(), "NEW_VP_APPROVER"), "VPApproval", VPApproveLayoutSection);

            //Get sub report template path [Other Approver]       
            CreateSubReportObject(xtraReport, PrepareApproveDataSet(dataSource.Copy(), "NEW_APPROVER"), "OtherApproval", VPApproveLayoutSection);

            if (dataSource.Tables["ASSET_DETAIL_2"].Rows.Count > 0)
            { 
                //Get sub report template path [Asset Detail 2]       
                CreateSubReportObject(xtraReport, dataSource.Copy(), "AssetDetail2", AssetDetail2LayoutSection);               
            }
            return xtraReport;
        }


        void CreateSubReportObject(XtraReport MainReport, DataSet DataSource, string SubReportName, string cfgLayoutSection)
        {          
            var subReportObj = (XRSubreport)MainReport.FindControl(SubReportName, true);

            var subReport = XtraReport.FromFile(cfgLayoutSection, true);
            subReport.DataSource = DataSource;

            subReportObj.ReportSource = subReport;
        }
        DataSet PrepareAssetDetailDataSet(DataSet DataSet) {

            DataTable DataTableTop5 = DataSet.Tables["ASSET_DETAIL"].AsEnumerable().Take(5).CopyToDataTable();
            DataTableTop5.TableName = "ASSET_DETAIL_5";

            DataTable DataTableAll = DataSet.Tables["ASSET_DETAIL"].AsEnumerable().CopyToDataTable();
            DataTableAll.TableName = "ASSET_DETAIL_ALL";

            DataSet.Tables.Clear();

            DataSet.Tables.Add(DataTableTop5);
            DataSet.Tables.Add(DataTableAll);         

            return AddBlankRow(DataSet, "ASSET_DETAIL_5", 5); ;
        }
        DataSet PrepareApproveDataSet(DataSet DataSet, string Table2Rename) {
            DataSet.Tables.Remove("TRANSFER_APPROVER");
            DataSet.Tables[Table2Rename].TableName = "TRANSFER_APPROVER";
            return DataSet;
        }
        DataSet AddBlankRow(DataSet DataSet, string TableName, int limit) {

            if (DataSet.Tables[TableName].Rows.Count < limit) {
                int diff = limit - DataSet.Tables[TableName].Rows.Count;
                for (int ii = 0; ii < diff; ii++){
                    DataSet.Tables[TableName].Rows.Add();
                }
            }                           
            return DataSet;
        }
        void PrepareDataSet(DataSet DataSet) {         
            //------------------Transfer Approver-----------------------//
            AddBlankRow(DataSet, "TRANSFER_APPROVER", 3);
            //------------------BOI Approver-----------------------//
            AddBlankRow(DataSet, "BOI_APPROVER", 5);
            //------------------VP Approver-----------------------//
            AddBlankRow(DataSet, "NEW_VP_APPROVER", 1);
            //------------------Other Approver-----------------------//
            AddBlankRow(DataSet, "NEW_APPROVER", 5);
        }
        void GenerateFile(DataSet ds,  string DirectoryName, string FileName, string LayoutDirectory)
        {
            try
            {
                var xTraReport = GetXtraReportObject(LayoutDirectory, ds );
                var pdfBytes = Export2PDF(xTraReport, DirectoryName, FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        int GenerateReport(DataSet ds)
        {
            int result = -1;
            try
            {
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(cfgLayoutSection))
                {
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                }

                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                //Get PDF file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, DateTime.Now);
                oFileName = oFileName.Replace(".pdf", string.Empty);

                //Gernerate PDF File
                GenerateFile(ds,
                    oDirectoryName,
                    oFileName,
                    cfgLayoutSection);

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".pdf");
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".pdf");
                result = 1;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }

        private int GenerateMultipleReport(DataSet ds)
        {
            int result = -1;
            string _messageCode = "MSTD0000BERR  : ";
            try
            {
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(cfgLayoutSection))
                {
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                }

                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                //Get PDF file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, DateTime.Now);
                oFileName = oFileName.Replace(".pdf", string.Empty);

                zipSourceDirectory = Path.Combine(oDirectoryName, "LFD02420BO_ZipFolder");
                if (!Directory.Exists(zipSourceDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(zipSourceDirectory);
                    }
                    catch (Exception ex)
                    {
                          InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);

                    }
                }
                //Gernerate PDF File
                GenerateFile(ds,zipSourceDirectory,oFileName, cfgLayoutSection);
                result = 1;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }

        int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if ((ds != null) && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                { CountRow += dt.Rows.Count; }
            }
            return CountRow;
        }
        byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }


        private void InsertLog(string Log_Description)
        {
            _log = new DetailLogData();
            _log.AppID = BLoggingData.AppID;
            _log.Status = eLogStatus.Processing;
            _log.Level = eLogLevel.Error;
            _log.Description = Log_Description;
            BLogging.InsertDetailLog(_log);

            _log4Net.WriteErrorLogFile(Log_Description);
        }

        private bool ZipDataToFile(string sourcePathDirectory, string destinationPath_ZipPath)
        {
            bool _valueReturn;
            string _messageCode = "MSTD0000BERR  : ";
            _valueReturn = false;
            try
            {

                destinationPath_ZipPath = string.Format(destinationPath_ZipPath, DateTime.Now);
                //-------------------
                ZipFile.CreateFromDirectory(sourcePathDirectory, destinationPath_ZipPath);
                _valueReturn = true;
                Directory.Delete(sourcePathDirectory, true);
            }
            catch (Exception ex)
            {
                InsertLog(_messageCode + "Cannot zip file because :" + ex.Message);
            }
            return _valueReturn;
        }




    }
}
