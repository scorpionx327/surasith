﻿using System;
using System.Runtime.InteropServices;
using th.co.toyota.stm.fas.common;

namespace th.co.toyota.stm.fas
{
    class BFD02511Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleTitle(string _title);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main(string[] args)
        {
            Log4NetFunction _log4net = new Log4NetFunction();
            string _modName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            string _procName = System.IO.Path.GetFileNameWithoutExtension(_modName);
            System.Diagnostics.Process[] _appProc = System.Diagnostics.Process.GetProcessesByName(_procName);



            if (_appProc.Length > 1)
            {
                Console.WriteLine("There is another instances running. This instance will be closed automatically.");
                return;
            }

            /************************************   Check Multiple Running    ****************************************/
            string _cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
            if (!System.IO.File.Exists(_cfgFileName))
            {
                // message
                Console.WriteLine("Not found Configuration File : {0}", _cfgFileName);
                return;
            }



            if (!BatchLogging.IsConnected())
            {
                _log4net.WriteErrorLogFile(MessageCommonBatch.E_CANNOT_CONNECT_DB);
                return;
            }



            try
            {
                if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"]))
                {
                    throw (new Exception("No found [BatchName] in Configuration "));
                }
                if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"]))
                {
                    throw (new Exception("No found [BatchID] in Configuration "));
                }

                var _cls = new BFD02511BO();

                if (args.Length == 3 && common.Interface.Util.IsInt(args[0])) // manual
                {
                    _cls.bLoggingData.AppID = Convert.ToInt32(args[0]);
                    _cls.bLoggingData.BatchID = args[1];

                    if (System.Configuration.ConfigurationManager.AppSettings["BatchID"] != _cls.bLoggingData.BatchID)
                    {
                        //Log4Net
                        _log4net.WriteErrorLogFile("Invalid Batch ID");
                        return;
                    }

                    if (args[2].Split('|').Length < 4)
                    {
                        _log4net.WriteErrorLogFile(string.Format("Batch Parameter#1 [{0}] shoud be Company#GUID#UploadFilename#user", args[2]));
                        return;
                    }
                    var _p = args[2].Split('|');
                    _cls.UploadParameter = new BFD02511BO.Parameter()
                    {
                        Company = _p[0],
                        GUID = _p[1],
                        UploadFileName = _p[2],
                        RequestAssetRetirement = _p[3],
                        User = _p[4]

                    };

                }
                else //Not Allow Auto
                {
                    //Log4net
                    _log4net.WriteErrorLogFile(string.Format("Invalid Batch Parameter#2 [{0}]", args));
                    return;
                }

                SetConsoleTitle(_cls.bLoggingData.BatchID);
                _cls.Processing();
            }
            catch (Exception ex)
            {
                _log4net.WriteErrorLogFile(ex.Message);
                return;
            }
        }
    }
}
