﻿using System;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Collections.Generic;

namespace th.co.toyota.stm.fas
{
    class BFD01160DAO : Database
    {
        
        public BFD01160DAO()
		{
            //
            // TODO: Add constructor logic here
            //

            
		}

        public bool TestConnection()
        {
            try
            {
                //Test Connection
                openConnection();
                closeConnection();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
       
        public List<TB_R_NOTIFICATION> GetNotificationList()
        {
            var _rs = executeDataToList<TB_R_NOTIFICATION>("sp_BFD01160_GetNotificationList");
            return _rs;
        }
        public void UpdateNotification(TB_R_NOTIFICATION _data)
        {
            List<SqlParameter> _params = new List<SqlParameter>();
            _params.Add(new SqlParameter("@ID", _data.ID));
            _params.Add(new SqlParameter("@SendFlag", _data.SEND_FLAG));
            _params.Add(new SqlParameter("@ResultFlag", _data.RESULT_FLAG));
            _params.Add(new SqlParameter("@UpdateUser", _data.UPDATE_BY));

            execute("sp_BFD01160_UpdateNotification", _params);
        }

        public List<TB_M_SYSTEM> GetSystemConfiguration(string _Category, string _SubCategory)
        {
            List<SqlParameter> _params = new List<SqlParameter>();
            _params.Add(new SqlParameter("@CATEGORY", _Category));
            _params.Add(new SqlParameter("@SUB_CATEGORY", _SubCategory));

            var _rs = executeDataToList<TB_M_SYSTEM>("sp_Common_GetSystem", _params);
            return _rs;

        }
        
    }
    
   
}
