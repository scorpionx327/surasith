﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Xml.Linq;
using System.Text;
using System.Configuration;

namespace th.co.toyota.stm.fas
{
    class BFD01160BO
    {
        public string sBatchName = "Notification Sending Batch";
        private string sBatchID = "BFD01260";
        private bool bSuccessFlag = false;
        
        private Log4NetFunction log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();

        
        private BFD01160DAO dbconn = null;
        
        public BFD01160BO()
        {
                 
        }
        
        public void Processing(string pram)
        {
            try
            {
                log4Net.WriteInfoLogFile("BFD01160_NotificationSendingBatch Begin");//{0} Begin MSTD7000BINF

                dbconn = new BFD01160DAO();

                if (!dbconn.TestConnection())
                {
                    log4Net.WriteInfoLogFile("Failed to connect to Database");//MSTD8001SERR
                    mail.AddErrorMessage("Failed to connect to Database");//MSTD8001SERR

                    return;
                }

                var _rs = dbconn.GetNotificationList();
                Console.WriteLine("Total {0} Records", _rs.Count);

                if (_rs.Count == 0)
                {
                    //Ex.[2017/01/05 19:12:40.023][INFO][Fixed Assets-APP][user1][BFD01260][MSTD0059AERR : No data found]
                    log4Net.WriteInfoLogFile("No data found");//MSTD0059AERR
                    return;
                }

                var _cfg = dbconn.GetSystemConfiguration("SYSTEM_EMAIL", "EMAILTO_PIC");
                if (_cfg.FindAll(m => m.CODE == "SMTP_SERVER").Count > 0)
                    mail.MailSmtpServer = _cfg.Find(m => m.CODE == "SMTP_SERVER").VALUE;

                if (_cfg.FindAll(m => m.CODE == "SMTP_PORT").Count > 0)
                    mail.MailSmtpPort = Convert.ToInt32(_cfg.Find(m => m.CODE == "SMTP_PORT").VALUE);

                if (_cfg.FindAll(m => m.CODE == "MAIL_FROM").Count > 0)
                    mail.MailFrom = _cfg.Find(m => m.CODE == "MAIL_FROM").VALUE;

                if (_cfg.FindAll(m => m.CODE == "MAIL_FROM_DISPLAY").Count > 0)
                    mail.DisplayName = _cfg.Find(m => m.CODE == "MAIL_FROM_DISPLAY").VALUE;

                if (_cfg.FindAll(m => m.CODE == "SMTP_USER").Count > 0)
                    mail.MailSmtpUser = _cfg.Find(m => m.CODE == "SMTP_USER").VALUE;

                if (_cfg.FindAll(m => m.CODE == "SMTP_PASSWORD").Count > 0)
                    mail.MailSmtpPassword = _cfg.Find(m => m.CODE == "SMTP_PASSWORD").VALUE;


                bSuccessFlag = true;

                string _emailFormat = string.Empty;
                try
                {
                    _emailFormat = System.IO.File.ReadAllText(ConfigurationManager.AppSettings["EmailFormatFile"]);

                }
                catch(Exception ex)
                {
                    log4Net.WriteErrorLogFile(ex.Message, ex);
                }

                int _cnt = _rs.Count;
                int _i  = 0;

                _rs.ForEach(delegate (TB_R_NOTIFICATION _item)
                {
                    _i++;
                    Console.WriteLine("{0}/{1}", _i, _cnt);
                    this.SendNotification(_item, _emailFormat);

                    System.Threading.Thread.Sleep(2000);

                });

                //Incase No error in Send Notification Function => Success Flag should be true

            }
            catch (Exception ex) //cannot connect
            {
                bSuccessFlag = false;
                //Log
                log4Net.WriteErrorLogFile(ex.Message);
                mail.AddErrorMessage(ex.Message);
            }
            finally
            {
                //Success & Error
                log4Net.WriteInfoLogFile(bSuccessFlag ? "BFD01160_NotificationSendingBatch End successfully" : "BFD01260_NotificationSendingBatch End with error");

                //This function will send incase found a lot of data
                mail.SendEmailToAdministratorInSystemConfig(this.sBatchName);
            }

        }
        
        
        private void SendNotification(TB_R_NOTIFICATION _data,string _emailFormat)
        {
            //string headerEmail = "";
            //string footerEmail = "";
            Console.WriteLine("To : {0} Title : {1} ", _data.TO, _data.TITLE);
            mail.MailTo = _data.TO;
            mail.Mailcc = _data.CC;
            mail.MailBcc = _data.BCC;
            mail.MailBody = string.Format("{0}{1}{1}{1}{2}", _data.MESSAGE, Environment.NewLine, _data.FOOTER);
            mail.MailSubject = _data.TITLE;

            if (!string.IsNullOrEmpty(_emailFormat))
            {
                mail.MailBody = _emailFormat.Replace("<<<BODY>>>", mail.MailBody);
            }

            //using (System.IO.StreamWriter file =
            //new System.IO.StreamWriter(string.Format(@"D:\TFAST\{0:HHmmssnnn}.html",DateTime.Now), true))
            //{
            //    file.WriteLine(mail.MailBody);
            //}

            int _counter = 0, _limit = 3;
            bool _successFlag = false;
            while (_counter <= 3)
            {
                _counter++;
                try
                {
                    mail.Send();
                    _successFlag = true;
                    break;
                }
                catch (Exception ex)
                {
                    if (_counter > _limit)
                    {
                        bSuccessFlag = false;
                        //Add
                        log4Net.WriteErrorLogFile(string.Format("To : {0} => Error Message : {1}", _data.TO, ex.Message));
                        mail.AddErrorMessage(string.Format("To : {0} => Error Message : {1}", _data.TO, ex.Message));
                        break;
                    }
                }
            }

            _data.SEND_FAIL_NUMBER = _counter - 1;

            _data.SEND_FLAG = "Y";
            _data.RESULT_FLAG = _successFlag ? "Y" : "N";
            _data.UPDATE_BY = "System";

            dbconn.UpdateNotification(_data);
        }

        //private string getHeaderEmail() {
        //    try
        //    {
        //        var dt = string.Format("{0:dd.MM.yyyy}",DateTime.Now) ;
        //        string configvalue1 = ConfigurationManager.AppSettings["HeaderEmail"];
        //        string result = "";
        //        XDocument doc = XDocument.Load(configvalue1);
        //        var authors = doc.Descendants("emailtemplate");
        //        foreach (var author in authors)
        //        {
        //            foreach (XElement level2Element in author.Elements("header"))
        //            {
        //                result = result + level2Element.Value;
        //            }
        //        }
        //        //try
        //        //{
        //        //    result = result.Replace("{0}", dt);
        //        //}
        //        //catch (Exception ex) {

        //        //}
        //        return result.ToString();
        //    }
        //    catch (Exception ex) {
        //        return "";
        //    }            
        //}
        //private string getFooterEmail()
        //{
        //    try
        //    {
        //        string configvalue1 = ConfigurationManager.AppSettings["FooterEmail"];
        //        string result = "";
        //        XDocument doc = XDocument.Load(configvalue1);
        //        var authors = doc.Descendants("emailtemplate");
        //        foreach (var author in authors)
        //        {
        //            foreach (XElement level2Element in author.Elements("footer"))
        //            {
        //                result = result + level2Element.Value;
        //            }
        //        }
        //        return result.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

    }
}
