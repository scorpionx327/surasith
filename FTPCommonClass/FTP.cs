﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using System.Net;
using System.IO;
using th.co.toyota.stm.fas.common.FTPCommonClass;
using System.Collections;
using th.co.toyota.stm.fas.common.Interface;
namespace th.co.toyota.stm.fas.common.FTP
{
    public class FTP
    {

        FTPModel _ftpConfig = null ;
        public FTP(string _configFileName, string _configSection)
        {
           // Read parameter from section
            ConfigurationManagerFTP _cfg = new ConfigurationManagerFTP(_configFileName);
            // System.Collections.ArrayList _ar = _cfg.GetNodesListAndAttribute(_configSection);
            System.Collections.Hashtable _ht = _cfg.GetAttributeList(_configSection);
            //<FTP1 FTPHost="www.vinpoint.co.th" FTPPort="2002" UserName="ftpguest@vinpoint.co.th" Password="1234@abcd" FTPPath="/H20" />

            ArrayList _ar = _cfg.GetNodesListAndAttribute(_configSection + "/ColumnValidation/Column");


            if (!_ht.ContainsKey("FTPHost"))
                throw (new Exception(string.Format("Not found <{0}>", "FTPHost")));
            if (!_ht.ContainsKey("FTPPort"))
                throw (new Exception(string.Format("Not found <{0}>", "FTPPort")));
            if (!_ht.ContainsKey("UserName"))
                throw (new Exception(string.Format("Not found <{0}>", "UserName")));
            if (!_ht.ContainsKey("Password"))
                throw (new Exception(string.Format("Not found <{0}>", "Password")));

            _ftpConfig = new FTPModel();
            _ftpConfig.FTPHost = string.Format("{0}", _ht["FTPHost"]);
            _ftpConfig.Port = Convert.ToInt32(_ht["FTPPort"]);
            _ftpConfig.UserName = string.Format("{0}", _ht["UserName"]);
            _ftpConfig.Password = string.Format("{0}", _ht["Password"]);
            _ftpConfig.ServerPath = string.Format("{0}", _ht["FTPPath"]);
            _ftpConfig.ClientPath = string.Format("{0}", _ht["ClientPath"]);
            _ftpConfig.FilePrefix = string.Format("{0}", _ht["FilePrefix"]);

            if (_ht.ContainsKey("ArchivePath"))
                _ftpConfig.ArchivePath = string.Format("{0}", _ht["ArchivePath"]);



        }

        public FTP(string _configFileName, string _configSection,string _layoutName)
        {
            try
            {
                   ConfigurationManager _cfg = new ConfigurationManager(_configFileName);
                    Hashtable _ht = _cfg.GetChildNodes(_configSection+"/" + _layoutName);

                    if (Util.IsNullorEmpty(_ht, "FTPHost"))
                        throw (new Exception(string.Format("Not found <{0}>", "FTPHost")));
                    if (Util.IsNullorEmpty(_ht, "FTPPort"))
                        throw (new Exception(string.Format("Not found <{0}>", "FTPPort")));
                    if (Util.IsNullorEmpty(_ht, "UserName"))
                        throw (new Exception(string.Format("Not found <{0}>", "UserName")));
                    if (Util.IsNullorEmpty(_ht, "Password"))
                        throw (new Exception(string.Format("Not found <{0}>", "Password")));
                    if (Util.IsNullorEmpty(_ht, "ClientPath"))
                        throw (new Exception(string.Format("Not found <{0}>", "ClientPath")));

                    _ftpConfig = new FTPModel();

                    _ftpConfig.FTPHost = Util.GetStringFromHash(_ht, "FTPHost");  //string.Format("{0}", _ht["FTPHost"]);
                    _ftpConfig.UserName = Util.GetStringFromHash(_ht, "UserName"); //string.Format("{0}", _ht["UserName"]);
                    _ftpConfig.Password = Util.GetStringFromHash(_ht, "Password"); // string.Format("{0}", _ht["Password"]);

                    if (!Util.IsNullorEmpty(_ht, "FTPPort"))
                    {
                        _ftpConfig.Port = Convert.ToInt32(Util.GetStringFromHash(_ht, "FTPPort")); //Convert.ToInt32(_ht["FTPPort"]);
                    }
                    else
                    {
                        _ftpConfig.Port = 21;
                    }
            
                    if (!Util.IsNullorEmpty(_ht, "FTPPath"))//string.Format("{0}", _ht["FTPPath"]);
                    {
                        _ftpConfig.ServerPath = Util.GetStringFromHash(_ht, "FTPPath"); 
                    }
                    if (!Util.IsNullorEmpty(_ht, "ClientPath"))//string.Format("{0}", _ht["ClientPath"]);
                    {
                        _ftpConfig.ClientPath = Util.GetStringFromHash(_ht, "ClientPath"); 
                    }

                    if (!Util.IsNullorEmpty(_ht, "FilePrefix"))//string.Format("{0}", _ht["FilePrefix"
                    {
                        _ftpConfig.FilePrefix = Util.GetStringFromHash(_ht, "FilePrefix");
                    _ftpConfig.FilePrefix = _ftpConfig.FilePrefix.Replace("*", string.Empty);
                    }

                    if( Util.GetStringFromHash(_ht, "OnlyLatestFile").ToString().ToLower().Equals("true"))//string.Format("{0}", _ht["OnlyLatestFile"]);
                    {
                        _ftpConfig.OnlyLatestFile = true;
                    }
                    else
                    {
                        _ftpConfig.OnlyLatestFile = false;
                    }

                    if (_ht.ContainsKey("ArchivePath"))
                        _ftpConfig.ArchivePath = string.Format("{0}", _ht["ArchivePath"]);


            }
            catch
            {
                throw (new BatchLoggingException("Layout ftp <{0}> incorrect", _layoutName));
            }
        }
        public FTP()
        {
            //Get DB
            _ftpConfig = new FTPModel();
        }
        public FTP(FTPModel _model)
        {
            _ftpConfig = _model;
        }

        public string UploadFileName { get; set; }
        private int LoggingID = -1 ;
      
        public bool Execute()
        {
         
            _ftpConfig.ClientPath = this.UploadFileName;
            string _tmpFileName = string.Empty;
            for (int i = 1; i <= 3; i++)
            {
                using (FtpLib.FtpConnection ftp = new FtpLib.FtpConnection(_ftpConfig.FTPHost, _ftpConfig.Port, _ftpConfig.UserName, _ftpConfig.Password))
                {
                    ftp.Open(); /* Open the FTP connection */
                    AddLog(string.Format("FTP Login. to [{0}], Username=[{1}]", _ftpConfig.FTPHost, _ftpConfig.UserName));

                    ftp.Login(); /* Login using previously provided credentials */
                    AddLog(string.Format("FTP is connected."));
                    try
                    {
                        string _fileName = new System.IO.FileInfo(_ftpConfig.ClientPath).Name ;
                        ftp.SetCurrentDirectory(_ftpConfig.ServerPath);
                        AddLog(string.Format("Start Transfer File {0}", _ftpConfig.ClientPath));
                        ftp.PutFile(_ftpConfig.ClientPath, _fileName);
                        
                        //Download file
                        _tmpFileName = System.IO.Path.GetTempFileName() ;
                        ftp.GetFile(_fileName,_tmpFileName, false);

                        long _fLocalSize = new System.IO.FileInfo(_ftpConfig.ClientPath).Length;
                        long _fServerSize = new System.IO.FileInfo(_tmpFileName).Length;

                        if(_fServerSize != _fLocalSize && i < 3) //last time no deleted
                        {
                            //delete server & continue ;
                            ftp.RemoveFile(_ftpConfig.ServerPath);
                            AddLog(string.Format("File size is missmatch, System will delete file and try again"));
                            continue;
                        }


                        AddLog(string.Format("Transfer is completed"));
                        ftp.Close();

                        return true;
                       
                    }catch(Exception ex){
                        AddLog("Transfer is fail => "+ ex.Message);
                    }
                    finally
                    {
                        //DeleteFile
                        try
                        {
                           
                            if (!string.IsNullOrEmpty(_tmpFileName) && System.IO.File.Exists(_tmpFileName))
                                System.IO.File.Delete(_tmpFileName);
                        }
                        catch (Exception ex) { }
                    }

                }
               
                
            }
            return false;
        }
        public bool DownloadFileFromFTP()
        {
              _FTP localFTP = new _FTP(_ftpConfig.FTPHost, _ftpConfig.UserName, _ftpConfig.Password, _ftpConfig.ServerPath);
               string _tmpFileName = string.Empty;

            if (!localFTP.IsConnected())
            {
                throw (new BatchLoggingException("Connection Failed, Connection  <{0}> not found", _ftpConfig.FTPHost));
            }

            List<_FTPX.DirectoryItem> returnValue = new List<_FTPX.DirectoryItem>();

            returnValue = localFTP.GetFileList(_ftpConfig);

            if (returnValue.Count <= 0)
            {
                throw (new BatchLoggingException("File ID {0} does not exist in server", _ftpConfig.FilePrefix));
            }
            
            if (_ftpConfig.OnlyLatestFile)
            {// only latest file 
                _tmpFileName = returnValue[returnValue.Count - 1].Name;
                localFTP.Download(_ftpConfig.ServerPath + "/" + _tmpFileName, _ftpConfig.ClientPath + "/" + _tmpFileName);


                long _fLocalSize = new System.IO.FileInfo(_ftpConfig.ClientPath + "/" + _tmpFileName).Length;
                long _fServerSize = localFTP.GetFtpFileSize(_ftpConfig.ServerPath + "/" + _tmpFileName);
                if (_fServerSize != _fLocalSize && _fServerSize != -1) 
                {
                    //delete file in local path;
                    if (System.IO.File.Exists(_ftpConfig.ClientPath + "/" + _tmpFileName))
                    {
                        System.IO.File.Delete(_tmpFileName);
                    }
                    throw (new BatchLoggingException(string.Format("File size is missmatch, System will delete file and try again")));
                }
            }  
            else
            { // all file
                foreach (_FTPX.DirectoryItem arFilename in returnValue)
                {
                    _tmpFileName = arFilename.Name;
                    localFTP.Download(_ftpConfig.ServerPath + "/" + _tmpFileName, _ftpConfig.ClientPath + "/" + _tmpFileName);

                    long _fLocalSize = new System.IO.FileInfo(_ftpConfig.ClientPath + "/" + _tmpFileName).Length;
                    long _fServerSize = localFTP.GetFtpFileSize( _tmpFileName);
                    if (_fServerSize != _fLocalSize)
                    {
                        foreach (_FTPX.DirectoryItem arTargetDeletfile in returnValue)   //delete file in local path;
                        {
                            if (System.IO.File.Exists(_ftpConfig.ClientPath + "/" + arFilename.Name ))
                            {
                                System.IO.File.Delete(_tmpFileName);
                            }
                        }
                        throw (new BatchLoggingException(string.Format("File size is missmatch, System will delete file and try again")));
                    }
                }
            }
            //Add by Surasith T.
            localFTP.MoveFile(_ftpConfig);

            return true;
            
        }


        private void AddLog(eLogLevel _lvl, string _msg)
        {
            Console.WriteLine(_msg);
            if (this.LoggingID == -1)
                return;

            BatchLogging _log = new BatchLogging();
            DetailLogData _detail = new DetailLogData() ;
            _detail.AppID = this.LoggingID ;
            _detail.Description = _msg ;
            _detail.Favorite = false ;
            _detail.Level = _lvl;
            _detail.Status = eLogStatus.Processing ;
            _log.InsertDetailLog(_detail);
        }
        private void AddLog(string _msg)
        {
            AddLog(eLogLevel.Information, _msg);
        }
    }
}
