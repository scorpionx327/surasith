﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace th.co.toyota.stm.fas.common.FTPCommonClass
{
    public class ConfigurationManagerFTP
    {
        private XmlDocument _doc = new XmlDocument();
        private string ConfigurationFileName = string.Empty;

        public ConfigurationManagerFTP(string _configFileName)
        {
            this.ConfigurationFileName = _configFileName;
            this.LoadConfigDocument();
        }
        private void LoadConfigDocument()
        {
            try
            {
                _doc.Load(this.ConfigurationFileName);
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw (new Exception("Configuration file not found", e)); //config not found

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Hashtable GetAttributeList(string _nodeName)
        {
            Hashtable _ht = new Hashtable();
            try
            {
                XmlNode _node = _doc.SelectSingleNode(_nodeName);
                if (_node == null)
                {
                    throw (new Exception(string.Format("Configuration <{0}> not found", _nodeName)));
                }

                foreach (XmlAttribute _attr in _node.Attributes)
                {
                    if (!_ht.ContainsKey(_attr.Name))
                        _ht.Add(_attr.Name, _attr.Value);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return _ht;
        }
        public object GetAttributeValue(string _nodeName, string _attrName)
        {
            Hashtable _ht = new Hashtable();
            _ht = GetAttributeList(_nodeName);
            if (_ht.ContainsKey(_attrName))
            {
                return _ht[_attrName];
            }
            return null;
        }


        public Hashtable GetChildNodes(string _nodeName)
        {
            XmlNode _node = _doc.SelectSingleNode(_nodeName);
            XmlNodeList _childs = _node.ChildNodes;

            Hashtable _ht = new Hashtable();
            foreach (XmlNode _nd in _childs)
            {
                if (!_ht.ContainsKey(_nd.Name))
                    _ht.Add(_nd.Name, _nd.InnerText);
            }

            return _ht;

        }
        public ArrayList GetNodesListAndAttribute(string _nodeName)
        {
            ArrayList _ar = new ArrayList();

            try
            {
                XmlNodeList _nodes = _doc.SelectNodes(_nodeName);

                foreach (XmlNode _node in _nodes)
                {
                    Hashtable _ht = new Hashtable();
                    foreach (XmlAttribute _attr in _node.Attributes)
                    {
                        if (!_ht.ContainsKey(_attr.Name))
                            _ht.Add(_attr.Name, _attr.Value);
                    }
                    _ar.Add(_ht);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return _ar;
        }

        public static string GetAppSetting(string _key)
        {
            return ConfigurationSettings.AppSettings[_key];
        }

    }
}
