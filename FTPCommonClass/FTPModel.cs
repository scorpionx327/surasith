﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.common.FTP
{
    public class FTPModel
    {
        public string FTPHost { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsConnected { get; set; }
        public string ServerPath { get; set; }
        public string FilePrefix { get; set; }
        public string ClientPath { get; set; }
        public bool OnlyLatestFile { get; set; }  // If True is mean get only ,false is mean get all , default set all

        public string ArchivePath { get; set; } //Leave it's blank incase no require move from source table
    }
}
