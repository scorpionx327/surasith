﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace th.co.toyota.stm.fas.common.FTP
{
    internal class _FTP
    {
        private string host = null;
        private string user = null;
        private string pass = null;
        private string dir = null;
        public _FTP(string hostIP, string userName, string password, string _dir) { host = hostIP; user = userName; pass = password; dir = _dir; }
        public bool IsConnected()
        {
            using (FtpLib.FtpConnection ftp = new FtpLib.FtpConnection(host, user, pass))
            {
                try
                {
                    Console.WriteLine("IsConnected");
                    ftp.Open(); /* Open the FTP connection */

                    ftp.Login(); /* Login using previously provided credentials */
                    ftp.Close();
                    Console.WriteLine("IsConnected OK");
                    return true ;
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
               
        }
        public List<_FTPX.DirectoryItem> GetFileList(FTPModel _fModel)
        {
            using (FtpLib.FtpConnection ftp = new FtpLib.FtpConnection(host, user, pass))
            {
                string[] File_Prefix = _fModel.FilePrefix.Split('|');
                List<_FTPX.DirectoryItem> rs = new List<_FTPX.DirectoryItem>();
                try
                {
                    ftp.Open(); /* Open the FTP connection */

                    Console.WriteLine("GetFileList");

                    ftp.Login(); /* Login using previously provided credentials */

                    ftp.SetCurrentDirectory(dir);

                    var _fileList = ftp.GetFiles();

                    Console.WriteLine("GetFileList Count {0}", _fileList.Count());
                    foreach (var _file in _fileList)
                    {
                        Console.WriteLine(_file.Name);
                        for (int i = 0; i < File_Prefix.Length; i++)
                        {
                            if (!File_Prefix[i].ToString().Equals(string.Empty))
                            {
                                if (_file.Name.StartsWith(File_Prefix[i].ToString()))
                                {
                                    rs.Add(new _FTPX.DirectoryItem { Name = _file.Name });
                                }
                            }
                            else
                            {
                                rs.Add(new _FTPX.DirectoryItem { Name = _file.Name });
                            }
                        }
                    }

                    return rs;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ftp.Close();
                }
            }
            

        }

        public void Download(string remoteFile, string localFile)
        {
            using (FtpLib.FtpConnection ftp = new FtpLib.FtpConnection(host, user, pass))
            {
                try
                {
                    Console.WriteLine("Download {0}", remoteFile);
                    Console.WriteLine("SetCurrentDirectory {0}", dir);
                    Console.WriteLine("localFile {0}", localFile);

                    ftp.Open(); /* Open the FTP connection */

                    ftp.Login(); /* Login using previously provided credentials */

                    ftp.SetCurrentDirectory(dir);


                    ftp.GetFile(remoteFile, localFile, false);

                    ftp.Close();
                 
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public long GetFtpFileSize(string remoteFile)
        {
            using (FtpLib.FtpConnection ftp = new FtpLib.FtpConnection(host, user, pass))
            {
                try
                {
                    ftp.Open(); /* Open the FTP connection */

                    ftp.Login(); /* Login using previously provided credentials */

                    ftp.SetCurrentDirectory(dir);
                    
                    var _ttl = ftp.GetFileSize(remoteFile);

                    Console.WriteLine("GetFileSize remoteFile {0}", remoteFile);
                    Console.WriteLine("GetFileSize {0}", _ttl);
                    ftp.Close();

                    return _ttl;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public void MoveFile(FTPModel _fModel)
        {
            if (string.IsNullOrEmpty(_fModel.ArchivePath))
                return;

            using (FtpLib.FtpConnection ftp = new FtpLib.FtpConnection(host, user, pass))
            {
                try
                {
                    ftp.Open(); /* Open the FTP connection */

                 
                    ftp.Login(); /* Login using previously provided credentials */

                    ftp.SetCurrentDirectory(dir);

                    var _fList = GetFileList(_fModel);
                    foreach (var s in _fList)
                    {
                        string _newFileName = _fModel.ArchivePath + "/" + s.Name;

                        if (ftp.FileExists(_newFileName))
                        {
                            _newFileName = _fModel.ArchivePath + "/" + s.Name + string.Format("_{0:yyyyMMddHHmmss}", DateTime.Now);
                        }
                        Console.WriteLine("MoveFile {0} to {1}", s.Name, _newFileName);
                        ftp.RenameFile(s.Name, _newFileName);
                    }

                

                    ftp.Close();

                
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
        }
    }
    internal class _FTPX
    {
        private string host = null;
        private string user = null;
        private string pass = null;
        private FtpWebRequest ftpRequest = null;
        private FtpWebResponse ftpResponse = null;
        private Stream ftpStream = null;
        private int bufferSize = 2048;

        /* Construct Object */
        public _FTPX(string hostIP, string userName, string password) { host = hostIP; user = userName; pass = password; }

        /* Download File */
        public void Download(string remoteFile, string localFile)
        {
            try
            {
                /* Create an FTP Request */
                //  ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + remoteFile);
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + host + "/" + remoteFile));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Get the FTP Server's Response Stream */
                ftpStream = ftpResponse.GetResponseStream();
                /* Open a File Stream to Write the Downloaded File */
                FileStream localFileStream = new FileStream(localFile, FileMode.Create);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[bufferSize];
                int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                /* Download the File by Writing the Buffered Data Until the Transfer is Complete */
                try
                {
                    while (bytesRead > 0)
                    {
                        localFileStream.Write(byteBuffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); throw; }
            return;
        }

        /* Upload File */
        public void Upload(string remoteFile, string localFile)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + remoteFile);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpRequest.GetRequestStream();
                /* Open a File Stream to Read the File for Upload */
                FileStream localFileStream = new FileStream(localFile, FileMode.Create);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[bufferSize];
                int bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
                try
                {
                    while (bytesSent != 0)
                    {
                        ftpStream.Write(byteBuffer, 0, bytesSent);
                        bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); throw; }
            return;
        }

        /* Delete File */
        public void Delete(string deleteFile)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)WebRequest.Create(host + "/" + deleteFile);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return;
        }

        /* Rename File */
        public void Rename(string currentFileNameAndPath, string newFileName)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)WebRequest.Create(host + "/" + currentFileNameAndPath);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.Rename;
                /* Rename the File */
                ftpRequest.RenameTo = newFileName;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return;
        }

        /* Create a New Directory on the FTP Server */
        public void CreateDirectory(string newDirectory)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)WebRequest.Create(host + "/" + newDirectory);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return;
        }

        /* Get the Date/Time a File was Created */
        public string getFileCreatedDateTime(string fileName)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + fileName);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpResponse.GetResponseStream();
                /* Get the FTP Server's Response Stream */
                StreamReader ftpReader = new StreamReader(ftpStream);
                /* Store the Raw Response */
                string fileInfo = null;
                /* Read the Full Response Stream */
                try { fileInfo = ftpReader.ReadToEnd(); }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
                /* Return File Created Date Time */
                return fileInfo;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            /* Return an Empty string Array if an Exception Occurs */
            return "";
        }

        /* Get the Size of a File */
        public string getFileSize(string fileName)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + fileName);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.GetFileSize;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpResponse.GetResponseStream();
                /* Get the FTP Server's Response Stream */
                StreamReader ftpReader = new StreamReader(ftpStream);
                /* Store the Raw Response */
                string fileInfo = null;
                /* Read the Full Response Stream */
                try { while (ftpReader.Peek() != -1) { fileInfo = ftpReader.ReadToEnd(); } }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
                /* Return File Size */
                return fileInfo;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            /* Return an Empty string Array if an Exception Occurs */
            return "";
        }

        /* List Directory Contents File/Folder Name Only */
        public string[] directoryListSimple(string directory)
        {
            try
            {
                /* Create an FTP Request */
                if (string.IsNullOrEmpty(directory))
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://" + host);
                else
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://" + host + "/" + directory);

                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpResponse.GetResponseStream();
                /* Get the FTP Server's Response Stream */
                StreamReader ftpReader = new StreamReader(ftpStream);
                /* Store the Raw Response */
                string directoryRaw = null;
                /* Read Each Line of the Response and Append a Pipe to Each Line for Easy Parsing */
                try { while (ftpReader.Peek() != -1) { directoryRaw += ftpReader.ReadLine() + "|"; } }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
                /* Return the Directory Listing as a string Array by Parsing 'directoryRaw' with the Delimiter you Append (I use | in This Example) */
                try { string[] directoryList = directoryRaw.Split("|".ToCharArray()); return directoryList; }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            }
            catch (Exception ex) {
                // Console.WriteLine(ex.ToString());
                throw (ex);
            }
            /* Return an Empty string Array if an Exception Occurs */
            return new string[] { "" };
        }

        /* List Directory Contents in Detail (Name, Size, Created, etc.) */
        public string[] directoryListDetailed(string directory)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + directory);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpResponse.GetResponseStream();
                /* Get the FTP Server's Response Stream */
                StreamReader ftpReader = new StreamReader(ftpStream);
                /* Store the Raw Response */
                string directoryRaw = null;
                /* Read Each Line of the Response and Append a Pipe to Each Line for Easy Parsing */
                try { while (ftpReader.Peek() != -1) { directoryRaw += ftpReader.ReadLine() + "|"; } }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
                /* Return the Directory Listing as a string Array by Parsing 'directoryRaw' with the Delimiter you Append (I use | in This Example) */
                try { string[] directoryList = directoryRaw.Split("|".ToCharArray()); return directoryList; }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            /* Return an Empty string Array if an Exception Occurs */
            return new string[] { "" };
        }

        //public bool IsConnected()
        //{
        //    try
        //    {
        //        directoryListSimple(string.Empty);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        public bool IsConnected()
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + host + "/"));
            request.Proxy = WebRequest.DefaultWebProxy;
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Credentials = new NetworkCredential(user, pass);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            try
            {

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                //set your flag
                return true;
            }
            catch (Exception ex)
            {
                throw (new BatchLoggingException("Connection ftp server:{0} is failed, error message:{1} ", request.RequestUri.AbsolutePath, ex.Message));
                //return false;

            }
        }

        private DateTime GetFtpFileDate(string url, ICredentials credential)
        {
            FtpWebRequest rd = (FtpWebRequest)WebRequest.Create(url);
            rd.Proxy = WebRequest.DefaultWebProxy;
            rd.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            rd.Credentials = credential;

            FtpWebResponse response = (FtpWebResponse)rd.GetResponse();
            DateTime lmd = response.LastModified;
            response.Close();

            return lmd;
        }
        public struct DirectoryItem
        {
            public Uri BaseUri;

            public string AbsolutePath
            {
                get
                {
                    return string.Format("{0}/{1}", BaseUri, Name);
                }
            }

            public bool IsDirectory;
            public string Name;
        }
        string _test;
        public List<_FTPX.DirectoryItem> GetFileList(FTPModel _fModel)
        {
            Console.WriteLine("GetFileList");
          //  Console.WriteLine("ftp://" + host + "/" + _fModel.ServerPath);
          //  Console.WriteLine(_fModel.ServerPath);
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://" + host + "/%2F" + _fModel.ServerPath);//(new Uri("ftp://" + host + "/" + _fModel.ServerPath));
            request.Proxy = WebRequest.DefaultWebProxy;
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Credentials = new NetworkCredential(user, pass);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = true;
            Console.WriteLine("request.RequestUri.AbsolutePath");
            Console.WriteLine(request.RequestUri.AbsolutePath);

            List<DirectoryItem> returnValue = new List<DirectoryItem>();
            string[] list = null;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                _test = reader.ReadToEnd();
                Console.WriteLine(_test);

            }
            Console.WriteLine("Check OK");
            // Incase Prefix file is multiple  separate by |
            string[] File_Prefix = _fModel.FilePrefix.Split('|');

            foreach (string line in list)
            {
                // Windows FTP Server Response Format
                // DateCreated    IsDirectory    Name
                string data = line;

                // Parse date
               // string date = data.Substring(0, 17);
               // DateTime dateTime = DateTime.Parse(date);
                data = data.Remove(0, 24);

                // Parse <DIR>
                string dir = data.Substring(0, 5);
                bool isDirectory = dir.Equals("<dir>", StringComparison.InvariantCultureIgnoreCase);
                data = data.Remove(0, 5);
                data = data.Remove(0, 10);

                // Parse name
                string name = data;

                // Create directory info
                DirectoryItem item = new DirectoryItem();
                item.BaseUri = new Uri("ftp://" + host + "/" + _fModel.ServerPath);
              //  item.DateCreated = dateTime;
                item.IsDirectory = isDirectory;
                item.Name = name;
                for(int i=0; i< File_Prefix.Length;i++ )
                {
                    if (!File_Prefix[i].ToString().Equals(string.Empty))
                    {
                        if (name.StartsWith(File_Prefix[i].ToString()))
                        {
                            returnValue.Add(item);
                        }
                    }
                    else
                    {
                        returnValue.Add(item);
                    }
                }             
            }
         
            return returnValue;

        }

        public long GetFtpFileSize(string remoteDirectory , string fileName)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + host + "/" + remoteDirectory + "/" + fileName));
            request.Proxy = WebRequest.DefaultWebProxy;
            request.Credentials = new NetworkCredential(user, pass);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            long size = response.ContentLength;
            response.Close();
            return size;
        }


        public void DownloadFtpFile(string sourcePath, string DesctinationPath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + host + "/" + sourcePath));
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(user, pass);
            request.UseBinary = true;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                using (Stream rs = response.GetResponseStream())
                {
                    using (FileStream ws = new FileStream(DesctinationPath, FileMode.Create))
                    {
                        byte[] buffer = new byte[2048];
                        int bytesRead = rs.Read(buffer, 0, buffer.Length);

                        while (bytesRead > 0)
                        {
                            ws.Write(buffer, 0, bytesRead);
                            bytesRead = rs.Read(buffer, 0, buffer.Length);
                        }
                    }
                }
            }
        }

        public void MoveFile(FTPModel _fModel)
        {
            if (string.IsNullOrEmpty(_fModel.ArchivePath))
                return;

            //If Not exists, Created
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + _fModel.FTPHost + "/" + _fModel.ArchivePath));
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(user, pass);
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    // Okay.   
                }
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        // Directory not found.   
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + _fModel.FTPHost + "/" + _fModel.ArchivePath));
                        request.Method = WebRequestMethods.Ftp.MakeDirectory;
                        request.Credentials = new NetworkCredential(user, pass);
                        using (var resp = (FtpWebResponse)request.GetResponse())
                        {
                            Console.WriteLine(resp.StatusCode);
                        }

                    }
                }
            }


            var _List = GetFileList(_fModel);
            foreach (DirectoryItem _s in _List)
            {

                //Check Existrs
                string _newFileName = _fModel.ArchivePath + "/" + _s.Name; 
                if (this.Exists(_fModel, _s.Name))
                {
                    _newFileName = _fModel.ArchivePath + "/" + _s.Name + string.Format("_{0:yyyyMMddHHmmss}",DateTime.Now);
                }
                
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(_s.AbsolutePath);
                request.Method = WebRequestMethods.Ftp.Rename;
                request.Credentials = new NetworkCredential(user, pass);
                request.UsePassive = true;

                request.RenameTo = _newFileName;
                request.GetResponse().Close();
            }
        }
        private bool Exists(FTPModel _fModel, string _fName)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("ftp://{0}/{1}/{2}", _fModel.FTPHost, _fModel.ArchivePath, _fName)));
            request.Credentials = new NetworkCredential(user, pass);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    //Does not exist
                    return false;
                }
            }
            return true;
        }
    }

}
