﻿using System;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace th.co.toyota.stm.fas
{
    class BFD02411DAO
    {
        private DataConnection dbconn = null;

        public BFD02411DAO()
        {
            //
            // TODO: Add constructor logic here
            //

            dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            if (!dbconn.TestConnection())
            {

                throw (new Exception("Cannot connect Database"));
            }

        }
        public void Close()
        {
            dbconn.Close();
        }
        public eLogLevel Execute(int _AppID, BFD02411BO.Parameter _data)
        {
            SqlCommand _cmd = new SqlCommand("sp_BFD02411_UploadTransferBatch");
            _cmd.CommandType = System.Data.CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@AppID", _AppID);
            _cmd.Parameters.AddWithValue("@Company", _data.Company);
            _cmd.Parameters.AddWithValue("@GUID", _data.GUID);
            _cmd.Parameters.AddWithValue("@User", _data.User);

            object _obj = dbconn.Execute(_cmd);
            return (eLogLevel)Convert.ToInt32(_obj);


        }
    }
}
