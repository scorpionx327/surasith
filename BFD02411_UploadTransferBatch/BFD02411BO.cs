﻿using System;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTP;

namespace th.co.toyota.stm.fas
{
    class BFD02411BO
    {
        Log4NetFunction _log4net = new Log4NetFunction();
        static eLogLevel _rs;

        public class Parameter
        {

            public string GUID { get; set; }
            public string Company { get; set; }
            public string FlowType { get; set; }
            public string User { get; set; }
            public string UploadFileName { get; set; }
        }

        public Parameter UploadParameter { get; set; }

        // private string strMailBodyPath = string.Empty;

        private Log4NetFunction log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();

        public BatchLoggingData bLoggingData = null;

        public BatchLogging bLogging = null;

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";

        private BFD02411DAO _dbconn = null;

        private DetailLogData _detailLogData = new DetailLogData();
        ReceiveFileClass _cls = null;
        public BFD02411BO()
        {
            bLogging = new BatchLogging();
            bLoggingData = new BatchLoggingData();

            // Set Batch Q information            
            bLoggingData.BatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            bLoggingData.BatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];

        }


        public void Processing()
        {
            try
            {
                //open connection
                bool _success = false;

                _dbconn = new BFD02411DAO();

                _detailLogData = new DetailLogData() { AppID = this.bLoggingData.AppID };

                this.StartBatchQ();

                _success = this.ReceivingProcess(); // Common Receiving file on local path
                if (!_success)
                {
                    //No email send
                    //mail.AppID = string.Format("{0}", BLoggingData.AppID); //assign when send email
                    return;
                }

                bLoggingData.ProcessStatus = this.BusinessProcess();
                //if (bLoggingData.ProcessStatus == eLogLevel.Error)
                //{
                //    mail.AppID = string.Format("{0}", bLoggingData.AppID); //assign when send email
                //}
                //2019.12.03
                if (_cls != null)
                    _cls.MoveFile(bLoggingData.ProcessStatus);

            }
            catch (Exception ex) //cannot connect
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                if ((ex is SqlException))
                {
                    return;
                }

                try
                {
                    _detailLogData.Status = eLogStatus.Processing;
                    _detailLogData.Level = eLogLevel.Error;
                    _detailLogData.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    bLogging.InsertDetailLog(_detailLogData);
                }
                catch (Exception ex1)
                {
                    //log4net
                    log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex1.Message, ex1);
                }


            }
            finally
            {
                try
                {
                    // Move file
                    this.SetBatchQEnd();
                    //if mail list > 0 send email
                    //Incomplete

                    _dbconn.Close();//Close connection

                    // mail.SendEmailToAdministratorInSystemConfig(this.strBatchName);
                }
                catch (Exception ex)
                {
                    //log4net
                    log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                }
            }

        }

        #region "BatchQ Method"

        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(bLoggingData);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(bLoggingData);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }

        }
        #endregion

        #region "Receiving Method"
        private bool ReceivingProcess()
        {
            

            try
            {
                string _cfgLayoutSection = ConfigurationManager.GetAppSetting("LayoutSection"); //follow your config
                if (_cfgLayoutSection == null)
                {
                    // message
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, "LayoutSection"));
                }

                _cls = new ReceiveFileClass(ConfigFileName, _cfgLayoutSection);
                _cls.ExecuteExcelFile(this.UploadParameter.UploadFileName);

                foreach (MessageResult _rs in _cls.ProcessResultList)
                {
                    _detailLogData.Status = eLogStatus.Processing;
                    _detailLogData.Level = _rs.MessageResultType;
                    _detailLogData.Description = _rs.Message;
                    bLogging.InsertDetailLog(_detailLogData);
                }

            }
            catch (BatchLoggingException bLoggingex)
            {
                _log4net.WriteErrorLogFile(bLoggingex.Message);
                _detailLogData.Status = eLogStatus.Processing;
                if (bLoggingex.arErrorList == null)
                {
                    _detailLogData.Description = bLoggingex.Message;
                    _detailLogData.Level = eLogLevel.Error;
                    bLogging.InsertDetailLog(_detailLogData);
                }
                else
                {
                    foreach (MessageResult msg in bLoggingex.arErrorList)
                    {
                        _detailLogData.Description = msg.Message;
                        _detailLogData.Level = msg.MessageResultType;
                        bLogging.InsertDetailLog(_detailLogData);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return true;
        }
        #endregion

        #region "FTP Receiving Method"
        // private bool ReceivingFileFromFTP(string cfgFileName)
        //{
        //    try
        //    {
        //        string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["LayoutSectionFTP"]; //follow your config

        //        if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
        //        {
        //            throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutOracalFTP")));
        //        }
        //        FTP ftp = new FTP(cfgFileName, cfgLayoutSectionFTP, "LayoutOracalFTP");
        //        ftp.DownloadFileFromFTP();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
        //        throw (ex);

        //    }
        //    return true;
        //}
        #endregion

        #region "BusinessProcess Method"

        /** Allow to Edit */
        private eLogLevel BusinessProcess()
        {
            try
            {
                _rs = _dbconn.Execute(bLoggingData.AppID, this.UploadParameter);
            }
            catch (Exception ex)
            {
                _log4net.WriteErrorLogFile(ex.Message);
            }
            return _rs;

        }

        //private string ReadConfigEmail(string fileConfigPath)
        //{
        //    string _strReturn = string.Empty;
        //    const int iBufferSize = 128;
        //    using (System.IO.FileStream _fileStream = System.IO.File.OpenRead(fileConfigPath))
        //    using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.ASCII, true, iBufferSize))
        //    {
        //        string _line = string.Empty;
        //        while ((_line = _rd.ReadLine()) != null)
        //        {
        //            if(! _line.Equals (string.Empty))
        //            {
        //                _strReturn = _strReturn + _line + " ";
        //            }                   
        //        }
        //        _rd.Close();
        //    }

        //    return _strReturn;
        //}

        /** End Allow */
        #endregion
    }
}
