﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace LFD02330_ExportReprintRequest
{
    class LFD02330DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02330DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataTable GetReprintRequestData(string _guid)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_LFD02330_GetReprintRequestData");
                cmd.Parameters.AddWithValue("@GUID", _guid);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }
    }
}
