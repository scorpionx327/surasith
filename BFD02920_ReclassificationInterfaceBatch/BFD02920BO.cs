﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTP;


namespace th.co.toyota.stm.fas
{
    class BFD02920BO
    {
        #region "Variable Declare"
      
        public BatchLoggingData bLoggingData = null;
        public BatchLogging bLogging = null;
        
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private BFD02920DAO _dbconn = null;
        private DetailLogData _log = null;
        #endregion

        public BFD02920BO()
        {
            bLoggingData = new BatchLoggingData();
            bLogging = new BatchLogging();


            bLoggingData.BatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            bLoggingData.BatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];


            // Set Batch Q information
            bLoggingData.Status = eLogStatus.Queue;
            bLoggingData.Description = "Run by schedule";
            bLoggingData.ReqBy = "System";
            

        }
        public void Processing()
        {
            MailSending _mail = null;
            try
            {
                _mail = new MailSending();


                bLogging = new BatchLogging();//Test connect data base
                _dbconn = new BFD02920DAO();

               this.StartBatchQ();

                this.BusinessProcess();
               
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = bLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    bLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
            }
            finally
            {
                try
                {
                    this.SetBatchQEnd();
                    _mail.SendEmailToAdministratorInSystemConfig(bLoggingData.BatchID);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        
        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion


        private void BusinessProcess()
        {
            try
            {
             
                DataTable dt = new DataTable();
                dt = _dbconn.GetReclassificationAssetList(bLoggingData);
            
                //3) Generate Interface .txt File (Common Process)
                string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                _cls.HeaderText = string.Format(_cls.HeaderText, DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-US")),"00488"); //#HPLANIIYYYYMMDDhhmmss
                _cls.FooterText = string.Format(_cls.FooterText, (dt.Rows.Count + 2).ToString().PadLeft(7, '0')); //#T000072
                if (dt != null && dt.Rows.Count > 0)
                {
                    _cls.DataSource = dt;
                }
                else
                {
                    this.InsertLog(MSG.DataNotFound_NoParams);
                }
                _cls.Execute();

                this.InsertLog(MSG.FileSuccess, bLoggingData.BatchID, _cls.FullFileName);//File {0} is successfully generated in {1}.
                bLoggingData.ProcessStatus = this.FTPFile(_cls.FullFileName, cfgFileName, _cls.FileName);

                //Update Data
                if (bLoggingData.ProcessStatus == eLogLevel.Information)
                {
                    _dbconn.UpdateAssetStatus();
                }

                _cls.MoveFileToArchive(bLoggingData.ProcessStatus == eLogLevel.Information);

            }
            catch (Exception ex)
            {
                bLoggingData.ProcessStatus = eLogLevel.Error;
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        
        private eLogLevel FTPFile(string fullFileName, string cfgFileName, string fileName)
        {
            // return eLogLevel.Information; //Test

            eLogLevel _rs = eLogLevel.Warning;

            string ftpFile = "Tranfer(FTP) file : " + fileName;
            try
            {
                InsertLog(MSG.BatchBegin, ftpFile); //MSTD7000BINF : {0} Begin

                string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["LayoutSectionFTP"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSectionFTP")));
                }

                ConfigurationManager cfg = new ConfigurationManager(cfgFileName);
                System.Collections.Hashtable ht = cfg.GetChildNodes(cfgLayoutSectionFTP);
                foreach (string cfgFTP in ht.Keys)
                {
                    try
                    {
                        FTP ftp = new FTP(cfgFileName, cfgLayoutSectionFTP + @"/" + cfgFTP);
                        ftp.UploadFileName = fullFileName;
                        ftp.Execute();

                        _rs = eLogLevel.Information;

                        this.InsertLog(MSG.BatchEndSuccessfully, ftpFile); //MSTD7001BINF : {0} End successfully
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc);
                        _rs = eLogLevel.Error;
                        if (exc.HResult.Equals(-2146231997))
                        {
                            this.InsertLog(MSG.DataNotFound, "FTP", "configuration"); //MSTD7054BERR : {0} Data not found from {1}	
                            throw;
                        }
                        throw exc;
                    }
                }
            }
            catch (Exception ex)
            {
                _rs = eLogLevel.Error;
                this.InsertLog(MSG.BatchEndError, ftpFile, string.Empty); //MSTD7002BINF :  {0} End with error {1}
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return _rs;
        }
        private void InsertLog(string messageCode, params string[] param)
        {
            try
            {
                _log = new DetailLogData();
                _log.AppID = bLoggingData.AppID;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.MessageCode = messageCode;
                bLogging.InsertDetailLog(_log, param);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
    }
}