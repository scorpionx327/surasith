﻿using System;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Collections.Generic;

namespace th.co.toyota.stm.fas
{
    class BFD01161DAO
    {
        private DataConnection dbconn = null;

        public BFD01161DAO()
		{
            //
            // TODO: Add constructor logic here
            //
            dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            if (!dbconn.TestConnection())
            {
                throw (new Exception("Cannot connect Database"));
            }


        }
        public void Close()
        {
            dbconn.Close();
        }

        public eLogLevel InsertNotification(BatchLoggingData _data)
        {
            SqlCommand _cmd = new SqlCommand("sp_BFD01160_InsertNotification");
            _cmd.CommandType = CommandType.StoredProcedure;

            _cmd.Parameters.AddWithValue("@AppID", _data.AppID);
            _cmd.Parameters.AddWithValue("@User", _data.ReqBy);

            return (eLogLevel)Convert.ToInt32(dbconn.Execute(_cmd));



        }

       
    }
    
   
}
