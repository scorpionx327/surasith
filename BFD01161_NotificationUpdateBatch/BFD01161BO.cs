﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.Models.BaseModel;



namespace th.co.toyota.stm.fas
{
    class BFD01161BO
    {
        public string sBatchName = "Notification Update Batch";
        private string sBatchID = "BFD01161";
       
        private Log4NetFunction log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();

        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
      
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
       

        private BFD01161DAO dbconn = null;
        
        public BFD01161BO()
        {
            BLoggingData = new BatchLoggingData();
            BLogging = new BatchLogging();

            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"])))
            {
                sBatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            }
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"])))
            {
                sBatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            }

            // 
            BLoggingData = new BatchLoggingData();
            BLoggingData.BatchID = sBatchID;
            BLoggingData.Status = eLogStatus.Processing ;
            BLoggingData.Description = "Run by schedule";
            BLoggingData.ReqBy = "System";
            BLoggingData.BatchName = sBatchName;
            
        }
        
        public void Processing()
        {
            try
            {
                
                dbconn = new BFD01161DAO();

                BLoggingData.AppID = BLogging.InsertBatchQ(BLoggingData);

                BLogging.StartBatchQ(BLoggingData);

                log4Net.WriteInfoLogFile(string.Format("App ID = {0}",BLoggingData.AppID));

                string User = BLoggingData.ReqBy;

                //dbconn.InsertAllNotification("STM", "System");

                var _rs = dbconn.InsertNotification(BLoggingData);

                BLoggingData.Status = _rs == eLogLevel.Information ? eLogStatus.Successfully : eLogStatus.Error ;
                BLoggingData.ProcessStatus = eLogLevel.Information;

            }
            catch (Exception ex) //cannot connect
            {
                BLoggingData.ProcessStatus = eLogLevel.Error;
                BLoggingData.Status = eLogStatus.Error;
                //Log
                log4Net.WriteErrorLogFile(ex.Message);
                mail.AddErrorMessage(ex.Message);
            }
            finally
            {
                BLogging.SetBatchQEnd(BLoggingData);
                
                //This function will send incase found a lot of data
                mail.SendEmailToAdministratorInSystemConfig(this.sBatchName);
            }

        }
      
    }
}
