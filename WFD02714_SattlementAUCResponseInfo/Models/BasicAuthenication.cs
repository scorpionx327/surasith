﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
 
namespace WFD02714_SattlementAUCResponseInfo.Models
{
    public class BasicAuthenication : AuthorizationFilterAttribute
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            //Check client passed any value in header or not
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            else
            {
                //Get the Hear values
                string authenticationToken = actionContext.Request.Headers.Authorization.Parameter;
                //Decoded the authenticationToken values becouse client passed the user namd and password in encoded form
                string decodedAuthenticationToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
                //Split the user name and password by : because client passed the user name and password as"userNameValue:Passwordvalue"
                string[] usernamePasswordArray = decodedAuthenticationToken.Split(':');
                string username = usernamePasswordArray[0];
                string password = usernamePasswordArray[1];

                try
                {
                   
                    _log.Debug(string.Format("Authenication Key = [{0}]", authenticationToken));
                    _log.Debug(string.Format("Username = [{0}]", username));
                    _log.Debug(string.Format("Password = [{0}]", password));


                    var _business = new th.co.toyota.stm.fas.BusinessObject.WFD02714BO();
                    //validate from the database for this user name or password.
                    if (_business.ValidateUser(username, password))
                    {
                        Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(username), null);
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    }
                }catch(Exception ex)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
        }
    }
}
 