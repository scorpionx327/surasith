using System;
using System.Reflection;

namespace WFD02714_SattlementAUCResponseInfo.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}