﻿using System.Web;
using System.Web.Mvc;

namespace WFD02714_SattlementAUCResponseInfo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
