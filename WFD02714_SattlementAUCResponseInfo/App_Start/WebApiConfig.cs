﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WFD02714_SattlementAUCResponseInfo
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
        //    config.Filters.Add(new BasicAuthenicateAttribute());
            // Web API routes
            config.MapHttpAttributeRoutes();

            var _cfg = System.Configuration.ConfigurationManager.AppSettings["Path"];
           
           
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: _cfg+"{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "AUCInvoiceInfo" }
            );
            
        }
    }
}
