﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using th.co.toyota.stm.fas.Models.SFAS.WFD02714;
using WFD02714_SattlementAUCResponseInfo.Models;

namespace WFD02714_SattlementAUCResponseInfo.Controllers
{
    public class AUCInvoiceInfoController : ApiController
    {
        [BasicAuthenication]
        public async Task<HttpStatusCodeResult> Post([FromBody]List<WFD02714ResponseAUCFromSAPModel> value)
        {
            try
            {
                if (value == null)
                {
                    _log.Info("No data found");
                    return new HttpStatusCodeResult(HttpStatusCode.NoContent);
                }
                _log.Info("Get AUC Info");
                var _json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                _log.Info(_json);

                var _bo = new th.co.toyota.stm.fas.BusinessObject.WFD02714BO();

                _bo.UpdateAUCInfo(value);
                


                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Get()
        {
            return "TFAST";
        }

    }
}
