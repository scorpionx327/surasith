﻿using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02690_ExportStockTakingResult
{
    class LFD02690BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
      
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02690DAO _dbconn = null;
        private DetailLogData _log = null;

        public string zipSourceDirectory = string.Empty;
        #endregion

        public LFD02690BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
            BLoggingData.ProcessStatus = eLogLevel.Error;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Export stock taking result";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02690DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                //  GetStockTakingData();
                BLoggingData.ProcessStatus = eLogLevel.Error;
                BusinessProcess();
                   }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void BusinessProcess()
        {

            string _messageCode = "MSTD0000BERR  : ";

            string cfgFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet1"];
            string messageDesc = string.Empty;

            string _year;
            string _company;
            string _round;
            string _assetlocaltion;

            try
            {
                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');
                string apl_ID = string.Empty;
                apl_ID = _p[0];

                // Incase multiple file 
                BFD02690Model _val = new BFD02690Model();
                _val = getConfiguration(cfgFileName);
                string zipFileName = _val.FilePreFix;
                string oDirectoryName = _val.FileLocation;

                // zip process 
                zipFileName = zipFileName.Replace(".xlsx", ".zip");
                zipFileName = string.Format(zipFileName, DateTime.Now);
                string destinationPath_ZipPath = oDirectoryName + @"\" + zipFileName;
                bool ZipSuccess = false;

                zipSourceDirectory = Path.Combine(oDirectoryName, "BFD02690_ZipFolder_" + apl_ID);

                if (!Directory.Exists(zipSourceDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(zipSourceDirectory);
                    }
                    catch (Exception ex)
                    {
                        InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);

                    }
                }


                DataTable dtGetParam = new DataTable();
                dtGetParam = _dbconn.GetParam(apl_ID);
                // Incase 1 file 
                if (dtGetParam != null && dtGetParam.Rows.Count > 0)
                {
                    _company = string.Empty;
                    _year = string.Empty;
                    _round = string.Empty;
                    _assetlocaltion = string.Empty;

                    if (dtGetParam.Rows.Count == 1)
                    {
                        _company = string.Format("{0}", dtGetParam.Rows[0]["COMPANY"]);
                        _year = string.Format("{0}", dtGetParam.Rows[0]["YEAR"]);
                        _round = string.Format("{0}", dtGetParam.Rows[0]["ROUND"]);
                        _assetlocaltion = string.Format("{0}", dtGetParam.Rows[0]["ASSET_LOCATION"]);

                        GetStockTakingData(_year, _round, _assetlocaltion, _company);

                        BLoggingData.ProcessStatus = eLogLevel.Information ;
                        _dbconn.RemoveParam(_year, _round, _assetlocaltion, apl_ID);
                    }
                    else
                    {
                        for (int i = 0; i < dtGetParam.Rows.Count; i++)
                        {
                            _company = string.Empty;
                            _round = string.Empty;
                            _assetlocaltion = string.Empty;

                            _company = string.Format("{0}", dtGetParam.Rows[0]["COMPANY"]);
                            _year = string.Format("{0}", dtGetParam.Rows[i]["YEAR"]);
                            _round = string.Format("{0}", dtGetParam.Rows[i]["ROUND"]);
                            _assetlocaltion = string.Format("{0}", dtGetParam.Rows[i]["ASSET_LOCATION"]);

                            GenerateMultipleReport(_year, _round, _assetlocaltion, zipSourceDirectory, _year + _round + _assetlocaltion, _company);

                            _dbconn.RemoveParam(_year, _round, _assetlocaltion, apl_ID);

                        }

                        ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                        if (ZipSuccess)
                        {
                            BLoggingData.ProcessStatus = eLogLevel.Information;
                        }
                        // Insert log : File {1} is generated to {0}
                        DetailLogData _detail = new DetailLogData();
                        _detail.AppID = BLoggingData.AppID;
                        _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, destinationPath_ZipPath);
                        _detail.Level = eLogLevel.Information;
                        BLogging.InsertDetailLog(_detail);

                        // Insert File Download
                        BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);
                        BLoggingData.ProcessStatus = eLogLevel.Information;
                    }

                }

            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            finally
            {
                if (Directory.Exists(zipSourceDirectory))
                {
                    Directory.Delete(zipSourceDirectory, true);
                }

            }
        }


        private void GetStockTakingData(string PERIOD_YEAR, string PERIOD_ROUND, string ASSET_LOCATION, string COMPANY)
        {
            try
            {
                //string[] _p = BLoggingData.Arguments.Split('|');

                DataSet ds = new DataSet();

                //string PERIOD_YEAR = _p[0];
                //string PERIOD_ROUND = _p[1];
                //string ASSET_LOCATION = _p[2];               

                    ds = _dbconn.GetStockTakingData(PERIOD_YEAR, ASSET_LOCATION, PERIOD_ROUND, COMPANY);
        
                if (CountDataAllTable(ds) != 0)
                {
                    int TbNo = 0;
                    string templateFileName = null;
                    List<string> lstPathFilename = new List<string>();

                    foreach (DataTable dt in ds.Tables)
                    {
                        if (TbNo == 0)
                            templateFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet1"];

                        if (TbNo == 1)
                            templateFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet2"];

                        if (TbNo == 2)
                            templateFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet3"];




                            string TempExcelName = GenerateReport(dt, templateFileName, ASSET_LOCATION);
                        lstPathFilename.Add(TempExcelName);

                        TbNo++;
                    }

                    this.MergeExcelFile(lstPathFilename);
                    BLoggingData.ProcessStatus = eLogLevel.Information;
                }
                else
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }


        private void GenerateMultipleReport(string PERIOD_YEAR, string PERIOD_ROUND, string ASSET_LOCATION, string zipFullPath, string runningFileName, string Company)
        {
            try
            {

                DataSet ds = new DataSet();

                ds = _dbconn.GetStockTakingData(PERIOD_YEAR, ASSET_LOCATION, PERIOD_ROUND , Company);

                if (CountDataAllTable(ds) != 0)
                {
                    int TbNo = 0;
                    string templateFileName = null;
                    List<string> lstPathFilename = new List<string>();

                    foreach (DataTable dt in ds.Tables)
                    {
                        if (TbNo == 0)
                            templateFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet1"];

                        if (TbNo == 1)
                            templateFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet2"];

                        if (TbNo == 2)
                            templateFileName = System.Configuration.ConfigurationSettings.AppSettings["TemplateSheet3"];



                        string TempExcelName = GenerateMultipleReport(dt, templateFileName, zipFullPath, runningFileName, ASSET_LOCATION);
                        lstPathFilename.Add(TempExcelName);

                        TbNo++;
                    }

                    this.MergeFileWithOutRegisDownloadFile(lstPathFilename);
                    BLoggingData.ProcessStatus = eLogLevel.Information;
                }
                else
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }



        private int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if ((ds != null) && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                { CountRow += dt.Rows.Count; }
            }
            return CountRow;
        }
        private string GenerateMultipleReport(DataTable _dt, string configFileName, string zipFullPath, string runningFileName,string ASSET_LOCATION)
        {
            string result = null;
            try
            {
                string cfgFileName = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                cfgFileName = System.IO.Path.Combine(cfgFileName, configFileName + ".config");

                string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config               

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }

                GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                if (ASSET_LOCATION.Equals("O"))
                { _cls.ExcelTemplateFileName = _cls.ExcelTemplateFileName.Replace(".xlsx", "_O.xlsx"); }
                _cls.DataSource = _dt;
                _cls.ExecuteByFixPath(zipFullPath, runningFileName);

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, _cls.DirectoryName, _cls.FileName);
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                result = (zipFullPath + @"\" + _cls.FileName);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }
        private string GenerateReport(DataTable _dt, string configFileName,string ASSET_LOCATION)
        {
            string result = null;
            try
            {
                string cfgFileName = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                cfgFileName = System.IO.Path.Combine(cfgFileName, configFileName + ".config");

                string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config               

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }

                GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                if(ASSET_LOCATION.Equals("O"))
                { _cls.ExcelTemplateFileName = _cls.ExcelTemplateFileName.Replace(".xlsx", "_O.xlsx"); }
                
                _cls.DataSource = _dt;
                _cls.Execute();

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, _cls.DirectoryName, _cls.FileName);
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                result = (_cls.DirectoryName + @"\" + _cls.FileName);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }


        private void MergeExcelFile(List<string> lstPathFilename)
        {
            try
            {
                FileInfo outputFile = new FileInfo(lstPathFilename[0]);
                ExcelPackage pckMerge = new ExcelPackage(outputFile);
                for (int inx = 1; inx < lstPathFilename.Count; inx++)
                {

                    if (!string.IsNullOrEmpty(lstPathFilename[inx]))
                    {
                        FileInfo fileCopy = new FileInfo(lstPathFilename[inx]);
                        ExcelPackage pckSheet = new ExcelPackage(fileCopy);

                        string sheetName = pckSheet.Workbook.Worksheets[1].Name;

                        ExcelWorksheet sheetCopy = pckSheet.Workbook.Worksheets.Copy(sheetName, string.Format("SheetX{0}", inx.ToString()));

                        //Add sheet
                        pckMerge.Workbook.Worksheets.Add(sheetName, sheetCopy);
                    }
                }

                pckMerge.Save();

                // Delete File
                for (int inx = 1; inx < lstPathFilename.Count; inx++)
                {
                    if (!string.IsNullOrEmpty(lstPathFilename[inx]))
                    {
                        FileInfo file = new FileInfo(lstPathFilename[inx]);
                        file.Delete();
                    }
                }

                string outputDirectoryName = lstPathFilename[0].Substring(0, lstPathFilename[0].LastIndexOf(@"\"));
                string outputFilename = lstPathFilename[0].Substring(lstPathFilename[0].LastIndexOf(@"\") + 1, lstPathFilename[0].Length - outputDirectoryName.Length - 1);

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, outputDirectoryName, outputFilename);
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, outputDirectoryName + @"\" + outputFilename);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }


        private void MergeFileWithOutRegisDownloadFile(List<string> lstPathFilename)
        {
            try
            {
                FileInfo outputFile = new FileInfo(lstPathFilename[0]);
                ExcelPackage pckMerge = new ExcelPackage(outputFile);
                for (int inx = 1; inx < lstPathFilename.Count; inx++)
                {

                    if (!string.IsNullOrEmpty(lstPathFilename[inx]))
                    {
                        FileInfo fileCopy = new FileInfo(lstPathFilename[inx]);
                        ExcelPackage pckSheet = new ExcelPackage(fileCopy);

                        string sheetName = pckSheet.Workbook.Worksheets[1].Name;

                        ExcelWorksheet sheetCopy = pckSheet.Workbook.Worksheets.Copy(sheetName, string.Format("SheetX{0}", inx.ToString()));

                        //Add sheet
                        pckMerge.Workbook.Worksheets.Add(sheetName, sheetCopy);
                    }
                }
    
                pckMerge.Save();

                // Delete File
                for (int inx = 1; inx < lstPathFilename.Count; inx++)
                {
                    if (!string.IsNullOrEmpty(lstPathFilename[inx]))
                    {
                        FileInfo file = new FileInfo(lstPathFilename[inx]);
                        file.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }


        private void InsertLog(string messageCode, params string[] param)
        {
            try
            {
                _log = new DetailLogData();
                _log.AppID = BLoggingData.AppID;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.MessageCode = messageCode;
                BLogging.InsertDetailLog(_log, param);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }


        private bool ZipDataToFile(string sourcePathDirectory, string destinationPath_ZipPath)
        {
            bool _valueReturn;
            string _messageCode = "MSTD0000BERR  : ";
            _valueReturn = false;
            try
            {

                destinationPath_ZipPath = string.Format(destinationPath_ZipPath, DateTime.Now);
                //-------------------
                ZipFile.CreateFromDirectory(sourcePathDirectory, destinationPath_ZipPath);
                _valueReturn = true;
                Directory.Delete(sourcePathDirectory, true);
            }
            catch (Exception ex)
            {
                InsertLog(_messageCode + "Cannot zip file because :" + ex.Message);
            }
            return _valueReturn;
        }


        private BFD02690Model getConfiguration(string configFileName)
        {
            string cfgFileName = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            cfgFileName = System.IO.Path.Combine(cfgFileName, configFileName + ".config");

            string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config               

            if (string.IsNullOrEmpty(cfgLayoutSection))
            {
                throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
            }

            ConfigurationManager _cfg = new ConfigurationManager(cfgFileName);
            Hashtable _htChild = _cfg.GetChildNodes(cfgLayoutSection);
            Hashtable _ht = _cfg.GetAttributeList(cfgLayoutSection);

            BFD02690Model _val = new BFD02690Model();
            _val.FileLocation = Util.GetStringFromHash(_htChild, "DirectoryName");
            _val.FilePreFix = Util.GetStringFromHash(_htChild, "FilePrefix");
            return _val;
        }
    }

    public class BFD02690Model
    {
        public string FileLocation { get; set; }
        public string FilePreFix { get; set; }


    }
}
