﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
using System.Configuration;

namespace LFD02690_ExportStockTakingResult
{
    class LFD02690DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02690DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataSet GetStockTakingData(string PeriodYear, string AssetLocation, string PeriodRound, string Company)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                var DataTable1 = new DataTable();
                var DataTable2 = new DataTable();
                var DataTable3 = new DataTable();



                if (AssetLocation.Equals("O"))
                {

                    DataTable1 = GetDataTableSummaryReport_O(PeriodYear, AssetLocation, PeriodRound, Company);
                    DataTable2 = GetDataTableDetailByAssetNo_O(PeriodYear, AssetLocation, PeriodRound, Company);
                    DataTable3 = GetAssetIncorrectLocation_O(PeriodYear, AssetLocation, PeriodRound, Company);
                }
                else
                {
                    DataTable1 = GetDataTableSummaryReport(PeriodYear, AssetLocation, PeriodRound, Company);
                    DataTable2 = GetDataTableDetailByAssetNo(PeriodYear, AssetLocation, PeriodRound, Company);
                    DataTable3 = GetAssetIncorrectLocation(PeriodYear, AssetLocation, PeriodRound, Company);

                }

                //var DataTable1 = GetDataTableSummaryReport(PeriodYear, AssetLocation, PeriodRound);
                //var DataTable2 = GetDataTableDetailByAssetNo(PeriodYear, AssetLocation, PeriodRound);
                //var DataTable3 = GetAssetIncorrectLocation(PeriodYear, AssetLocation, PeriodRound);




                ds = new DataSet();
                ds.Tables.Add(DataTable1);
                ds.Tables.Add(DataTable2);
                ds.Tables.Add(DataTable3);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }

        public DataTable GetParam(string apl_id)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02680_GetParam");
                cmd.Parameters.Add(new SqlParameter("@APL_ID", SqlDbType.Decimal)).Value = Decimal.Parse(apl_id);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }

        public void RemoveParam(string _year, string _round, string _assetlocaltion, string apl_id)
        {
               
             SqlCommand _cmd = new SqlCommand("sp_LFD02690_RemoveParam");
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@YEAR", _year);
            _cmd.Parameters.AddWithValue("@ROUND", _round);
            _cmd.Parameters.AddWithValue("@ASSET_LOCATION", _assetlocaltion );
            _cmd.Parameters.AddWithValue("@APL_ID", Decimal.Parse(apl_id));
            _db.ExecuteNonQuery(_cmd);
        }

        private DataTable GetDataTableSummaryReport(string PeriodYear, string AssetLocation, string PeriodRound,string Company)
        {
            SqlCommand cmd = new SqlCommand("sp_LFD02690_GetStockTakingSummaryReport");

            cmd.Parameters.AddWithValue("@COMPANY", Company);
            cmd.Parameters.AddWithValue("@PERIOD_YEAR", PeriodYear);
            cmd.Parameters.AddWithValue("@PERIOD_ROUND", PeriodRound);
            cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);

            cmd.CommandType = CommandType.StoredProcedure;
            var dataTable = _db.GetData(cmd);

            return dataTable;
        }

        private DataTable GetDataTableDetailByAssetNo(string PeriodYear, string AssetLocation, string PeriodRound, string Company)
        {
            SqlCommand cmd = new SqlCommand("sp_LFD02690_GetStockTakingDetailReport");

            cmd.Parameters.AddWithValue("@COMPANY", Company);
            cmd.Parameters.AddWithValue("@PERIOD_YEAR", PeriodYear);
            cmd.Parameters.AddWithValue("@PERIOD_ROUND", PeriodRound);
            cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);

            cmd.CommandType = CommandType.StoredProcedure;
            var dataTable = _db.GetData(cmd);

            return dataTable;
        }

        private DataTable GetAssetIncorrectLocation(string PeriodYear, string AssetLocation, string PeriodRound, string Company) {
            SqlCommand cmd = new SqlCommand("sp_LFD02690_GetInvalidLocationAssetReport");

            cmd.Parameters.AddWithValue("@COMPANY", Company);
            cmd.Parameters.AddWithValue("@PERIOD_YEAR", PeriodYear);
            cmd.Parameters.AddWithValue("@PERIOD_ROUND", PeriodRound);
            cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);

            cmd.CommandType = CommandType.StoredProcedure;
            var dataTable = _db.GetData(cmd);

            return dataTable;
        }



        private DataTable GetDataTableSummaryReport_O(string PeriodYear, string AssetLocation, string PeriodRound, string Company)
        {
            SqlCommand cmd = new SqlCommand("sp_LFD02690_GetStockTakingSummaryReport");

            cmd.Parameters.AddWithValue("@COMPANY", Company);
            cmd.Parameters.AddWithValue("@PERIOD_YEAR", PeriodYear);
            cmd.Parameters.AddWithValue("@PERIOD_ROUND", PeriodRound);
            cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);

            cmd.CommandType = CommandType.StoredProcedure;
            var dataTable = _db.GetData(cmd);

            return dataTable;
        }

        private DataTable GetDataTableDetailByAssetNo_O(string PeriodYear, string AssetLocation, string PeriodRound, string Company)
        {
            SqlCommand cmd = new SqlCommand("sp_LFD02690_GetStockTakingDetailReport");

            cmd.Parameters.AddWithValue("@COMPANY", Company);
            cmd.Parameters.AddWithValue("@PERIOD_YEAR", PeriodYear);
            cmd.Parameters.AddWithValue("@PERIOD_ROUND", PeriodRound);
            cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);

            cmd.CommandType = CommandType.StoredProcedure;
            var dataTable = _db.GetData(cmd);

            return dataTable;
        }

        private DataTable GetAssetIncorrectLocation_O(string PeriodYear, string AssetLocation, string PeriodRound, string Company)
        {
            SqlCommand cmd = new SqlCommand("sp_LFD02690_GetInvalidLocationAssetReport");
            cmd.Parameters.AddWithValue("@COMPANY", Company);
            cmd.Parameters.AddWithValue("@PERIOD_YEAR", PeriodYear);
            cmd.Parameters.AddWithValue("@PERIOD_ROUND", PeriodRound);
            cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);

            cmd.CommandType = CommandType.StoredProcedure;
            var dataTable = _db.GetData(cmd);

            return dataTable;
        }
    }
}
