﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02540_SummaryFAdisposeListByPeriod
{
    class LFD02540DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02540DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }

        public DataSet GetDisposalRequestSummaryReport(DateTime START_PERIOD, DateTime END_PERIOD, string COMPANY_CODE)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02540_GetDisposalRequestSummaryReport");
                cmd.Parameters.AddWithValue("@START_PERIOD", START_PERIOD);
                cmd.Parameters.AddWithValue("@END_PERIOD", END_PERIOD);
                cmd.Parameters.AddWithValue("@COMPANY_CODE", COMPANY_CODE);

                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);

                //ds = TempGetDisposalRequestSummaryReport(START_PERIOD, END_PERIOD, USER_ID);
                //Comment Loop นี้

                ds.DataSetName = "ReportDataSource";
                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0)
                    {
                        table.TableName = "Content";
                    }
                    if (ii == 1)
                    {
                        table.TableName = "Summary";
                    }
                    ii++;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }

        public string GetLogoURL(string CATEGORY, string SUB_CATEGORY, string CODE)
        {
            string result = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return result;
                }

                SqlCommand cmd = new SqlCommand("sp_Common_GetSystemValues");
                cmd.Parameters.AddWithValue("@CATEGORY", CATEGORY);
                cmd.Parameters.AddWithValue("@SUB_CATEGORY", SUB_CATEGORY);
                cmd.Parameters.AddWithValue("@CODE", CODE);

                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = _db.GetData(cmd);

                result = (dt.Rows.Count > 0 ? dt.Rows[0]["VALUE"].ToString() : null);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return result;
        }

        DataSet TempGetDisposalRequestSummaryReport(DateTime START_PERIOD, DateTime END_PERIOD, string USER_ID)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "ReportDataSource";

            DataTable dt = new DataTable();
            dt.TableName = "Content";
            dt.Clear();
            dt.Columns.Add("ITEM_NO", typeof(Int32));
            dt.Columns.Add("COST_CENTER");
            dt.Columns.Add("ASSET_NO");
            dt.Columns.Add("ASSET_SUB");
            dt.Columns.Add("DESCRIPTION");
            dt.Columns.Add("DATE_IN_SERVICE");
            dt.Columns.Add("USEFULLLIFE", typeof(Int32));
            dt.Columns.Add("DATE_RETIRED");
            dt.Columns.Add("QTY", typeof(Int32));
            dt.Columns.Add("COST", typeof(decimal));
            dt.Columns.Add("REMAINING_COST", typeof(decimal));
            dt.Columns.Add("RETIREMENT_COST", typeof(decimal));
            dt.Columns.Add("DETAIL_DATE");
            dt.Columns.Add("FULL_FLAG");
            dt.Columns.Add("PROCEED_OF_SALE", typeof(decimal));
            dt.Columns.Add("SOLD_TO");
            dt.Columns.Add("INVOICE_NO");
            dt.Columns.Add("TRANSACTION_NO");

            for (int i = 1; i <= 1000; i++)
            {
                DataRow DataRow = dt.NewRow();

                DataRow["ITEM_NO"] = i;
                DataRow["COST_CENTER"] = "E02DM003";
                DataRow["ASSET_NO"] = "KMC-A01N0000500";
                DataRow["ASSET_SUB"] = "KMC-A01N0000501";
                DataRow["DESCRIPTION"] = "Machine ST-0067";
                DataRow["DATE_IN_SERVICE"] = "13-Sep-2002";
                DataRow["USEFULLLIFE"] = 11;
                DataRow["DATE_RETIRED"] = "14-Sep-2013";               
                DataRow["QTY"] = 1;
                DataRow["COST"] = 1000000;
                DataRow["REMAINING_COST"] = 999999;
                DataRow["RETIREMENT_COST"] = 50000;
                DataRow["DETAIL_DATE"] = "14-Sep-2013";
                DataRow["FULL_FLAG"] = "FULL";
                DataRow["PROCEED_OF_SALE"] = 10;
                DataRow["SOLD_TO"] = "ABC";
                DataRow["INVOICE_NO"] = "715155";
                DataRow["TRANSACTION_NO"] = "888005";

                dt.Rows.Add(DataRow);
            }

            ds.Tables.Add(dt);

            DataTable dt2 = new DataTable();
            dt2.Clear();
            dt2.TableName = "Summary";

            dt2.Columns.Add("COST_CENTER");
            dt2.Columns.Add("QTY", typeof(decimal));
            dt2.Columns.Add("COST", typeof(decimal));
            dt2.Columns.Add("REMAINING_COST", typeof(decimal));
            dt2.Columns.Add("RETIREMENT_COST", typeof(decimal));
            dt2.Columns.Add("PROCEED_OF_SALE", typeof(decimal));


            for (int i = 1; i <= 100; i++)
            {
                DataRow DataRow = dt2.NewRow();

                DataRow["COST_CENTER"] = "EO2DM003";
                DataRow["QTY"] =10;
                DataRow["COST"] = 5000000;
                DataRow["REMAINING_COST"] = 4999990;
                DataRow["RETIREMENT_COST"] = 10;
                DataRow["PROCEED_OF_SALE"] = 100;
           

                dt2.Rows.Add(DataRow);
            }

            ds.Tables.Add(dt2);

            return ds;
        }
    }
}
