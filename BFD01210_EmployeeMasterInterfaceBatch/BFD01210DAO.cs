﻿using System;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace th.co.toyota.stm.fas
{
    class BFD01210DAO
    {
        private  DataConnection dbconn = null ;
        
        public BFD01210DAO()
        {
            //
            // TODO: Add constructor logic here
            //

            dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            if (!dbconn.TestConnection())
            {

                throw (new Exception("Cannot connect Database"));
            }

        }
        public void Close()
        {
            dbconn.Close();
        }
        public eLogLevel Execute(BatchLoggingData _data)
        {
            SqlCommand _cmd = new SqlCommand("sp_BFD01210_ReceiveEmployeeMasterIFBatch");
            _cmd.CommandType = System.Data.CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@AppID", _data.AppID);
            _cmd.Parameters.AddWithValue("@UserID", _data.ReqBy);

            object _obj = dbconn.Execute(_cmd);
            return (eLogLevel)Convert.ToInt32(_obj);

        }
    }
}
