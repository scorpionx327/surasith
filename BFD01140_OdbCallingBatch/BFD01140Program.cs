﻿
using System;
using System.Runtime.InteropServices;
using th.co.toyota.stm.fas.common;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace COMB204_OdbCallingBatch
{
    class BFD01140Program
    {
        [DllImport("Kernel32.dll")]
        static extern bool SetConsoleTitle(string _title);

        private static Log4NetFunction _log;
        private static MailSending _mailFunction;

        [STAThread]
        static void Main(string[] args)
        {
            Log4NetFunction log = CreateLogFunctionInstance();

            string errorMsg = string.Empty;
            try
            {
                if (!HasAnotherProcessRunning(log))
                {
                //    MailFunction mail = CreateMailFunctionInstance();

                    bool isCallConfigSuccess = false;
                   // CallConfigurationExt(log, mail, out isCallConfigSuccess);
                    CallConfigurationExt(log,  out isCallConfigSuccess);

                    if (isCallConfigSuccess)
                    {
                        // set title
                        SetConsoleTitle(ConfigurationExt.BatchName);

                        BFD01140BO mainClass = new BFD01140BO();

                        if (!mainClass.CheckDBConnected()) // Check Database Connection
                        {
                            errorMsg = "Cannot Connect database.";
                            Console.WriteLine(errorMsg);
                            Console.WriteLine(string.Empty);
                            if (log != null)
                                log.WriteDebugLogFile(errorMsg);
                        }
                        else
                        {
                            Console.WriteLine("Connect database completed.");
                            Console.WriteLine(string.Empty);
                            //
                            // start : business logic process
                            //
                            mainClass.Process();
                            //
                            // end : business logic process
                            //
                        } // end if
                    } // end if 
                } // end if 

                // 2014-09-26 : Delay console to close.
                CountDownToClose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception Error: {0}", ex.Message);
                if (log != null)
                    log.WriteErrorLogFile(errorMsg);

                CountDownToClose();
            }
        }
        //private static void CallConfigurationExt(Log4NetFunction log, MailFunction mail, out bool IsCallSuccess)
        private static void CallConfigurationExt(Log4NetFunction log,  out bool IsCallSuccess)
        {
            try
            {
                IsCallSuccess = false;

                ConfigurationExt.Set();
                string errorMsg = ConfigurationExt.GetErrorList();

                if (!string.IsNullOrWhiteSpace(errorMsg))
                {
                    if (log != null)
                        log.WriteDebugLogFile(errorMsg);

                    //if (mail != null)
                    //    mail.Send(errorMsg);

                    Console.WriteLine("Error ConfigurationExt : " + errorMsg);
                }

                IsCallSuccess = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //private static MailFunction CreateMailFunctionInstance()
        //{
        //    try
        //    {
        //        _mailFunction = _mailFunction != null ? _mailFunction : new MailFunction();
        //        return _mailFunction;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error MailFunction : " + ex.Message);
        //        return null;
        //    }
        //}

        private static Log4NetFunction CreateLogFunctionInstance()
        {
            try
            {
                _log = _log != null ? _log : new Log4NetFunction();
                return _log;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Log4NetFunction : " + ex.Message);
                return null;
            }
        }

        private static bool HasAnotherProcessRunning(Log4NetFunction log)
        {
            try
            {
                string errorMsg = string.Empty;
                string modName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
                string procName = System.IO.Path.GetFileNameWithoutExtension(modName);

                System.Diagnostics.Process[] appProc = System.Diagnostics.Process.GetProcessesByName(procName);
                //if (appProc.Length > 1)
                //{
                //    // 2014-09-26 : Show message on console
                //    errorMsg = "There is another instances running. This instance will be closed automatically.";
                //    Console.WriteLine(errorMsg);


                //    // 2014-09-24 : Add message to log file
                //    if (log != null)
                //        log.WriteDebugLogFile(errorMsg);

                //    return true;
                //}

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void CountDownToClose()
        {
            System.Threading.Thread.Sleep(10000);
        }

    } // end class
}