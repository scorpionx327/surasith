﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace COMB204_OdbCallingBatch
{
    public class BFD01140DAO
    {
        
		private Log4NetFunction log = new Log4NetFunction() ;
		private DataConnection dbconn ;
        public BFD01140DAO()
		{
			//
			// TODO: Add constructor logic here
			//
			dbconn = new DataConnection(ConfigurationExt.ConnectionString) ;
            if (!dbconn.TestConnection())
            {
                string msg = "Can't connect database server.\r\n" +
            "Please contact administrator for solving the following error.\r\n" +
            "Try to set ConnectionString in config file or Check database connection.\r\n";
                //throw(new Exception(STMError.E_CANNOT_CONNECT_DB)) ;
                throw (new Exception(msg));
            }
			
		}
		
		public bool CheckWaitingBatchIsExists()
		{
            SqlCommand _cmd = new SqlCommand("sp_BFD01140_CheckWaitingBatchIsExists");
			_cmd.CommandType = CommandType.StoredProcedure ;

			DataTable _dt = dbconn.GetData(_cmd) ;
			if(_dt.Rows.Count == 0)
				return false ;

			return Convert.ToInt32(_dt.Rows[0][0]) > 0 ;
		}
		private Hashtable GetBatchParams(DataTable _dt)
		{
			Hashtable _ht = new Hashtable() ;
			foreach(DataRow _dr in _dt.Rows)
			{
				if(!_ht.ContainsKey(_dr["REQ_ID"]))
				{
					_ht.Add(_dr["REQ_ID"],new ArrayList()) ;
				}
				ArrayList _ar = _ht[_dr["REQ_ID"]] as ArrayList ;
				BatchParam _b = new BatchParam() ;
				_b.Index = Convert.ToInt32(_dr["SEQ"]) ;
				_b.ParameterName = string.Format("{0}",_dr["PARAMETERNAME"]) ;
				_b.ParameterValues = string.Format("{0}",_dr["PARAMETERVALUE"]) ;
				_ar.Add(_b) ;
			}

			return _ht ;
		}
		public ArrayList GetWaitingBatch()
		{
            SqlCommand _cmd = new SqlCommand("sp_BFD01140_GetWaitingBatchList");
			_cmd.CommandType = CommandType.StoredProcedure ;

			DataSet _ds = dbconn.GetDataSet(_cmd,"Result") ;
			DataTable _dt = _ds.Tables[0] ;

			Hashtable _htParams = this.GetBatchParams(_ds.Tables[1]) ;

			Hashtable _ht = new Hashtable() ;
			foreach (DataRow _dr in _dt.Rows)
			{
				string _grp = string.Format("{0}",_dr["GRP"]) ;
				if(!_ht.ContainsKey(_grp))
				{
					_ht.Add(_grp, new ArrayList()) ;
				}
				ArrayList _ar = _ht[_grp] as ArrayList ;
				BatchInfo _f = new BatchInfo() ;
				_f.ReqID = string.Format("{0}", _dr["REQ_ID"]) ;
				_f.BatchID = string.Format("{0}", _dr["BATCH_ID"]) ;
				_f.BatchName = string.Format("{0}", _dr["BATCH_NAME"]) ;
				_f.Priority = string.Format("{0}", _dr["PRIORITY"]) ;
				_f.GroupID = string.Format("{0}", _dr["GRP"]) ;
				_f.ExecPath = string.Format("{0}", _dr["EXEC_PATH"]) ;
				_f.Status = string.Format("{0}", _dr["STATUS"]) ;
				if(_htParams.ContainsKey(_dr["REQ_ID"]))
				{
					_f.BatchParamList = (_htParams[_dr["REQ_ID"]] as ArrayList) ;
				}
				_ar.Add(_f) ;
				
			}
			SortedList _stG = new SortedList() ;
		
			foreach(DictionaryEntry _e in _ht)
			{
				ArrayList _a = _e.Value as ArrayList ;
				BatchInfo _f1 = _a[0] as BatchInfo ;
				if(!_stG.ContainsKey(_f1.Priority))
				{
					_stG.Add(_f1.Priority, new SortedList()) ;
				}
				SortedList _st = _stG[_f1.Priority] as SortedList ;
				_st.Add(_f1.ReqID, _f1) ;
			}
			ArrayList _arBatchList = new ArrayList() ;
			foreach(SortedList _s in _stG.Values)
			{
				foreach(BatchInfo _f2 in _s.Values)
				{
					_arBatchList.Add(_f2) ;
				}
			}
			
			return _arBatchList ;
		}

        internal string GetErrorMessage(string msgCode)
        {
            try
            {
                SqlCommand _cmd = new SqlCommand("sp_Common_GetMessage");
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.Parameters.Add("@MESSAGE_CODE", SqlDbType.VarChar, 12);
                _cmd.Parameters["@MESSAGE_CODE"].Value = msgCode;
                DataTable _dt = dbconn.GetData(_cmd);

                if (_dt != null && _dt.Rows.Count > 0)
                {
                    return _dt.Rows[0]["MESSAGE_TEXT"] != DBNull.Value ? Convert.ToString(_dt.Rows[0]["MESSAGE_TEXT"]) : null;
                }
                else
                {
                    throw new Exception(string.Format("Cannot find message code : [{0}] in master data.", msgCode));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
