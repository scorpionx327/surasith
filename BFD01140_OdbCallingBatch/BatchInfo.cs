﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMB204_OdbCallingBatch
{
    /// <summary>
    /// Summary description for BatchInfo.
    /// </summary>
    public class BatchInfo
    {
        public BatchInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public string BatchID;
        public string BatchName;
        public string GroupID;
        public string Priority;
        public string ReqID;
        //		public string ReqBy ;ddd
        public string Status;
        public string ExecPath;
        public System.Collections.ArrayList BatchParamList = new System.Collections.ArrayList();
        public string GetParamList()
        {
            string _s = string.Empty;
            foreach (BatchParam _p in this.BatchParamList) //alreadsort
            {
                _s += string.Format("{0}|", _p.ParameterValues);
            }

            if (_s.Length > 0)
                _s = _s.Substring(0, _s.Length - 1);

            return _s;
        }
    }
    public class BatchParam
    {
        public int Index;
        public string ParameterName;
        public string ParameterValues;
    }
    public class ConfigurationExt
    {
        public static string GetErrorList()
        {
            return sError;
        }
        private static string sError = string.Empty;
        public static void Set()
        {
            ConfigurationExt.sError = string.Empty;

            //Check
            string[] _params = new string[] { "ConnectionString", "BatchName" };

            System.Text.StringBuilder _sb = new System.Text.StringBuilder();
            foreach (string _cfg in _params)
            {
                string _v = Get(_cfg);
                if (string.Format("{0}", _v).Trim() == string.Empty)
                {
                    //_sb.AppendFormat(STMError.E_CONFIG, _cfg);
                    _sb.AppendFormat("Not found {0} in configuration", _cfg);
                    _sb.Append(Environment.NewLine);
                }
            }
            sError = _sb.ToString();
            ConfigurationExt.ConnectionString = Get("ConnectionString");
            ConfigurationExt.BatchName = Get("BatchName");
        }
        public static string ConnectionString;
        public static string BatchName;
        public static string Get(string _config)
        {

            return System.Configuration.ConfigurationManager.AppSettings[_config]; 
        }
    }
}
