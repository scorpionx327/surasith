﻿using System;
using System.Collections;
using th.co.toyota.stm.fas.common;

namespace COMB204_OdbCallingBatch
{
    internal class BFD01140BO
    {
        #region -- local variable --
        private Log4NetFunction _log;
      //  private MailFunction _mail;
        private BFD01140DAO _proc;
        private BatchLogging _blog;
        #endregion

        #region -- constructor --
        public BFD01140BO()
        {
            try
            {
                _log = new Log4NetFunction();
            //    _mail = new MailFunction();
            }
            catch (Exception ex)
            {
                if (_log != null)
                    _log.WriteErrorLogFile(ex.Message);
                throw (ex);
            }
        }
        #endregion        


        public bool CheckDBConnected()
        {
            try
            {
                _proc = new BFD01140DAO();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogFileAndMail(ex.Message);
                return false;
            }
            return true;

        }

        public void Process()
        {
            try
            {
                _blog = new BatchLogging();
                if (!_proc.CheckWaitingBatchIsExists()) // case : 'No have batch q', count batch q from database
                {                   
                    #region -- no batch in quence --
                    string errorMsg = _proc.GetErrorMessage("MCOM2002BINF");                   
                    errorMsg = string.Format("{0} : {1}", "MCOM2002BINF", errorMsg);
                    System.Console.WriteLine(errorMsg); // 2014-09-24
                    _log.WriteDebugLogFile(errorMsg);
                    var logData = new DetailLogData()
                    {
                        AppID = -1,
                        Description = errorMsg,
                        Favorite = false,
                        Level = eLogLevel.Information,
                        MessageCode = "MCOM2002BINF",
                        Status = eLogStatus.Processing,
                        User = "System"
                    };
                    _blog.InsertDetailLog(logData);
                    #endregion
                }
                else  // case : 'have batch q'
                {
                    ArrayList arRequestBatch = _proc.GetWaitingBatch(); // get all batch q from database
                    #region -- loop each batch q --
                    foreach (BatchInfo batchQItem in arRequestBatch)
                    {
                        //if (batchQItem.Status == "P")
                        //{
                        //    // old code is 'R'
                        //    continue;
                        //}
                        //else
                        {
                            ExecuteBatch(batchQItem);
                        }
                    } // end foreach 
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        #region -- private method --

        private void ExecuteBatch(BatchInfo batchQItem)
        {
            try
            {
                #region -- declare and init variable --
                BatchLoggingData batchLogData = new BatchLoggingData();
                DetailLogData detailLogData = new DetailLogData();

                batchLogData.AppID = Convert.ToInt32(batchQItem.ReqID);
                batchLogData.BatchID = batchQItem.BatchID;
                detailLogData.AppID = Convert.ToInt32(batchQItem.ReqID);
                detailLogData.Status = eLogStatus.Error; // initial error status

                string errorMsg = string.Empty;
                #endregion

                #region -- [DR] : update [TB_BATCH_Q].[STATUS]='P'
                batchLogData.Status = eLogStatus.Processing;
                //_blog.UpdateBatchQStatus(batchLogData);
                #endregion

                if (!System.IO.File.Exists(batchQItem.ExecPath))
                {
                    #region -- [DR] : validate exe path
                    errorMsg = _proc.GetErrorMessage("MCOM2004BERR");
                    errorMsg = string.Format(errorMsg, string.IsNullOrWhiteSpace(batchQItem.ExecPath) ? string.Empty : batchQItem.ExecPath, batchQItem.BatchID);
                    errorMsg = string.Format("{0} : {1}", "MCOM2004BERR", errorMsg);

                    Console.WriteLine("\t" + errorMsg);

                    _log.WriteErrorLogFile(errorMsg);

                    detailLogData.Description = errorMsg;
                    detailLogData.Status = eLogStatus.Error;
                    detailLogData.Level = eLogLevel.Error;
                    _blog.InsertDetailLog(detailLogData);
                    #endregion
                }
                else
                {
                    #region -- [DR] : start batch --
                    // add log 
                    errorMsg = _proc.GetErrorMessage("MCOM2003BINF");
                    errorMsg = string.Format(errorMsg, batchQItem.BatchID, batchQItem.BatchName, batchQItem.ReqID);
                    errorMsg = string.Format("{0} : {1}", "MCOM2003BINF", errorMsg);

                    Console.WriteLine("\t" + errorMsg);

                    _log.WriteDebugLogFile(errorMsg);

                    detailLogData.Status = eLogStatus.Processing;
                    detailLogData.Description = errorMsg;
                    detailLogData.Level = eLogLevel.Information;
                    _blog.InsertDetailLog(detailLogData);
                    #endregion                    

                    try
                    {
                        bool canStart = false;
                        // If cannot execute file then throw exception and keep log.
                        #region -- execute batch process --
                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        process.StartInfo.FileName = batchQItem.ExecPath;
                        string paramList = batchQItem.GetParamList();
                        paramList = !string.IsNullOrWhiteSpace(paramList) ? paramList : string.Empty;
                        if (paramList.Contains("|"))
                        {
                            process.StartInfo.Arguments = string.Format("{0} {1} \"{2}\"", batchQItem.ReqID, batchQItem.BatchID, paramList);
                        }
                        else
                        {
                            process.StartInfo.Arguments = string.Format("{0} {1} {2}", batchQItem.ReqID, batchQItem.BatchID, paramList);
                        }
                        _log.WriteInfoLogFile("BatchID :" + batchLogData.BatchID);
                        _log.WriteInfoLogFile("Process Path :" + process.StartInfo.FileName);
                        _log.WriteInfoLogFile("Param :" + process.StartInfo.Arguments);
                        canStart = process.Start();
                        batchLogData.PID = process.Id.ToString();
                        _blog.UpdateBatchQStatus(batchLogData);

                        #endregion

                        if (!canStart)
                        {
                            // case : 'batch cannot start'
                            #region -- case : 'batch cannot start' --
                            // MST1008AERR 
                            // {0} : BATCH_ID
                            // {1} : Exeception Message
                            errorMsg = _proc.GetErrorMessage("MST1008AERR");
                            errorMsg = string.Format(errorMsg, batchQItem.BatchID, string.Format("Cannot start [{0}] batch.", batchQItem.BatchID));
                            errorMsg = string.Format("{0} : {1}", "MCOM2003BINF", errorMsg);

                            Console.WriteLine("\t" + errorMsg);

                            _log.WriteDebugLogFile(errorMsg);
                            detailLogData.Status = eLogStatus.Processing;
                            detailLogData.Description = errorMsg;
                            detailLogData.Level = eLogLevel.Error;
                            _blog.InsertDetailLog(detailLogData);
                            #endregion
                        }
                        else
                        {

                     

                            #region -- [DR] : end batch --
                            // Note : {0}=Batch ID, {1}=Batch Name
                            errorMsg = _proc.GetErrorMessage("MCOM2005BINF");
                            errorMsg = string.Format(errorMsg, batchQItem.BatchID, batchQItem.BatchName);
                            errorMsg = string.Format("{0} : {1}", "MCOM2005BINF", errorMsg);

                            Console.WriteLine("\t" + errorMsg);

                            _log.WriteDebugLogFile(errorMsg);
                            detailLogData.Description = errorMsg;
                            detailLogData.Status = eLogStatus.Successfully;
                            detailLogData.Level = eLogLevel.Information;
                            _blog.InsertDetailLog(detailLogData);
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        // case : 'batch cannot start'
                        #region -- case : 'batch cannot start' --
                        // MST1008AERR 
                        // {0} : BATCH_ID
                        // {1} : Exeception Message
                        errorMsg = _proc.GetErrorMessage("MST1008AERR");
                        errorMsg = string.Format(errorMsg, batchQItem.BatchID, ex.Message);
                        errorMsg = string.Format("{0} : {1}", "MCOM2003BINF", errorMsg);

                        Console.WriteLine("\t" + errorMsg);

                        _log.WriteDebugLogFile(errorMsg);
                        detailLogData.Status = eLogStatus.Processing;
                        detailLogData.Description = errorMsg;
                        detailLogData.Level = eLogLevel.Error;
                        _blog.InsertDetailLog(detailLogData);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LogFileAndMail(string _message)
        {
            try
            {

                if (_log == null)
                    _log = new Log4NetFunction();

                _log.WriteDebugLogFile(_message);

                //if (_mail == null)
                //    _mail = new MailFunction();
                //_mail.Send(_message);

                Console.WriteLine(_message);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}
