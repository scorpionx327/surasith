﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GenerateAssetNoAPI.Controllers
{
    public class RequestAssetModel
    {
        public string COMPANY { get; set; }
        public string DOC_NO { get; set; }
        public int LINE_NO { get; set; }
        public string PARENT_ASSET_NO { get; set; }
        public string ASSET_MAINSUB { get; set; }
        public string INDICATOR_VALIDATE { get; set; }
        public string ASSET_CLASS { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_DESC { get; set; }
        public string ADTL_ASSET_DESC { get; set; }
        public string SERIAL_NO { get; set; }
        public string INVEN_NO { get; set; }
        public string BASE_UOM { get; set; }
        public string LAST_INVEN_DATE { get; set; }
        public string INVEN_INDICATOR { get; set; }
        public string INVEN_NOTE { get; set; }
        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string LOCATION { get; set; }
        public string ROOM { get; set; }
        public string LICENSE_PLATE { get; set; }
        public string WBS_PROJECT { get; set; }
        public string INVEST_REASON { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public string ASSET_STATUS { get; set; }
        public string MACHINE_LICENSE { get; set; }
        public string BOI_NO { get; set; }
        public string FINAL_ASSET_CLASS { get; set; }
        public string ASSET_SUPPER_NO { get; set; }
        public string MANUFACT_ASSET { get; set; }
        public string ASSET_TYPE_NAME { get; set; }
        public string WBS_BUDGET { get; set; }
        public string PLATE_TYPE { get; set; }
        public string BARCODE_SIZE { get; set; }
        public string LEASE_AGREE_DATE { get; set; }
        public string LEASE_START_DATE { get; set; }
        public string LEASE_LENGTH_YEAR { get; set; }
        public string LEASE_LENGTH_PERD { get; set; }
        public string LEASE_TYPE { get; set; }
        public string LEASE_SUPPLEMENT { get; set; }
        public string LEASE_NUM_PAYMENT { get; set; }
        public string LEASE_PAY_CYCLE { get; set; }
        public string LEASE_ADV_PAYMENT { get; set; }
        public string LEASE_PAYMENT { get; set; }
        public string LEASE_ANU_INT_RATE { get; set; }
        public string DPRE_AREA_01 { get; set; }
        public string DPRE_KEY_01 { get; set; }
        public string PLAN_LIFE_YEAR_01 { get; set; }
        public string PLAN_LIFE_PERD_01 { get; set; }
        public string SCRAP_VALUE_01 { get; set; }
        public string DPRE_AREA_15 { get; set; }
        public string DPRE_KEY_15 { get; set; }
        public string PLAN_LIFE_YEAR_15 { get; set; }
        public string PLAN_LIFE_PERD_15 { get; set; }
        public string SCRAP_VALUE_15 { get; set; }
        public string DPRE_AREA_31 { get; set; }
        public string DPRE_KEY_31 { get; set; }
        public string PLAN_LIFE_YEAR_31 { get; set; }
        public string PLAN_LIFE_PERD_31 { get; set; }
        public string SCRAP_VALUE_31 { get; set; }
        public string DPRE_AREA_41 { get; set; }
        public string DPRE_KEY_41 { get; set; }
        public string PLAN_LIFE_YEAR_41 { get; set; }
        public string PLAN_LIFE_PERD_41 { get; set; }
        public string SCRAP_VALUE_41 { get; set; }
        public string DPRE_AREA_81 { get; set; }
        public string DPRE_KEY_81 { get; set; }
        public string PLAN_LIFE_YEAR_81 { get; set; }
        public string PLAN_LIFE_PERD_81 { get; set; }
        public string SCRAP_VALUE_81 { get; set; }
        public string DPRE_AREA_91 { get; set; }
        public string DPRE_KEY_91 { get; set; }
        public string PLAN_LIFE_YEAR_91 { get; set; }
        public string PLAN_LIFE_PERD_91 { get; set; }
        public string SCRAP_VALUE_91 { get; set; }

    }
    public class ResultInfo
    {
        public string TFASTRequestnumber { get; set; }
        public string TFASTRequestItem { get; set; }
        public string CompanyCode { get; set; }
        public string AssetNumber { get; set; }
        public string Assetsubnumber { get; set; }
        public string Status { get; set; }
        public string MessageType { get; set; }
        public string MessageID { get; set; }
        public string MessageNo { get; set; }
        public string Message { get; set; }
        
    }
    public class AssetMasterController : ApiController
    {

        // GET: api/assetMaster/
        [HttpPost]
        public IHttpActionResult Post([FromBody]List<RequestAssetModel> data)
        {
            return Ok("Success");
        }

    }

    public class AUCInvoiceController : ApiController
    {

        // GET: api/assetMaster/
        [HttpPost]
        public IHttpActionResult Post([FromBody]List<RequestAssetModel> data)
        {
            return Ok("Success");
        }

    }
}
