﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace th.co.toyota.stm.fas.common
{
    public class BatchLogging
    {
        private DataConnection dbconn = null;
        public BatchLogging()
        {
            
            dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            if (!dbconn.TestConnection())
            {

                throw (new Exception("Cannot connect Database"));
            }
        }
        public static bool IsConnected()
        {
            var dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            return dbconn.TestConnection();

        }

        public int InsertBatchQ(BatchLoggingData _data) //ok
        {
            try
            {
                SqlCommand _cmd = new SqlCommand("sp_Common_InsertBatchQ");
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.Parameters.AddWithValue("@BatchID", _data.BatchID);
                _cmd.Parameters.AddWithValue("@ReqBy", _data.ReqBy);
                _cmd.Parameters.AddWithValue("@Status", _data.Status.ToString().Substring(0, 1)); //default P
                _cmd.Parameters.AddWithValue("@Description", _data.Description);

                object _obj = dbconn.Execute(_cmd);
                return Convert.ToInt32(_obj);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void UpdateBatchQStatus(BatchLoggingData _data) //ok
        {
            try
            {
                BatchLoggingData bLogingData = this.GetRequestBatchInfo(_data.AppID);
                if (string.Format("{0}", _data.ReqBy).Trim() == string.Empty)
                {
                    _data.ReqBy = bLogingData.ReqBy;
                }
                string status = string.Format("{0}", _data.Status).Substring(0, 1).ToUpper();
                SqlCommand _cmd = new SqlCommand("sp_Common_UpdateBatchQ");
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.Parameters.AddWithValue("@ReqID", _data.AppID);
                _cmd.Parameters.AddWithValue("@Status", status);
                _cmd.Parameters.AddWithValue("@PID", _data.PID);
                dbconn.ExecuteNonQuery(_cmd);

                //if (_data.Status == eLogStatus.Successfully || _data.Status == eLogStatus.Error)
                //{
                //    this.EndBatchOperation(_data.BatchID, _data.ReqBy);
                //}

                //Add new by Patchrin - Run other batch to continue
                if (!status.Equals("P"))
                {
                    if (GetNextBatchQ(_data.AppID))
                    {
                        string strBatchCallingID = "COMB204";
                        string callingBatchPath = GetCallingBatchPart(strBatchCallingID);
                        if (!callingBatchPath.Equals(string.Empty))
                        {
                            if (System.IO.File.Exists(callingBatchPath))
                            {
                                try
                                {
                                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                                    process.StartInfo.FileName = callingBatchPath;
                                    process.Start();
                                }
                                catch (Exception exProcess)
                                {
                                    throw exProcess;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        private void InsertLog(int appID, string messageCode, params string[] param)
        {
            try
            {
                //MSTD8002SERR	Undefine Error : {0}	
                DetailLogData _log = new DetailLogData();
                _log.AppID = appID;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.MessageCode = messageCode;
                BatchLogging BLogging = new BatchLogging();
                BLogging.InsertDetailLog(_log, param);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool GetNextBatchQ(int _AppID)
        {
            bool result = false;
            try
            {
                SqlCommand _cmd = new SqlCommand("SELECT TOP 1 BATCH_ID, APP_ID FROM TB_BATCH_Q WHERE [STATUS] <> 'S' AND APP_ID > @AppID");
                _cmd.CommandType = CommandType.Text;
                _cmd.Parameters.AddWithValue("@AppID", _AppID);
                DataTable _dt = dbconn.GetData(_cmd);
                if (_dt != null && _dt.Rows.Count > 0)
                {
                    result = _dt.Rows[0]["BATCH_ID"] == null ? false : true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public String GetCallingBatchPart(string _batchID)
        {
            string strReturn = string.Empty;
            try
            {
                SqlCommand _cmd = new SqlCommand("sp_Common_GetBatchInfo");
                _cmd.CommandType = System.Data.CommandType.StoredProcedure;
                _cmd.Parameters.AddWithValue("@BATCH_ID", _batchID);
                DataSet ds = dbconn.GetDataSet(_cmd);
                DataTable dt = ds.Tables[ds.Tables.Count - 1];
                if (dt.Rows.Count > 0)
                {
                    if (!(dt.Rows[0] == null | System.DBNull.Value.Equals(dt.Rows[0]["EXEC_PATH"])))
                    {
                        strReturn = Convert.ToString(dt.Rows[0]["EXEC_PATH"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturn;
        }
        public BatchLoggingData GetRequestBatchInfo(int _AppID)
        {
            SqlCommand _cmd = new SqlCommand("SELECT * FROM TB_BATCH_Q WHERE APP_ID = @AppID");
            _cmd.CommandType = CommandType.Text;
            _cmd.Parameters.AddWithValue("@AppID", _AppID);
            DataTable _dt = dbconn.GetData(_cmd);
            if (_dt.Rows.Count == 0)
            {
                return new BatchLoggingData();
            }
            BatchLoggingData _data = new BatchLoggingData();
            DataRow _dr = _dt.Rows[0];
            _data.AppID = _AppID;

            _data.BatchID = string.Format("{0}", _dr["BATCH_ID"]);
            _data.ReqBy = string.Format("{0}", _dr["UPDATE_BY"]);

            switch (string.Format("{0}", _dr["STATUS"]).Trim())
            {
                case "E":
                    _data.Status = eLogStatus.Error;
                    break;
                case "S":
                    _data.Status = eLogStatus.Successfully;
                    break;
                case "P":
                    _data.Status = eLogStatus.Processing;
                    break;
            }

            return _data;
        }
        private string sDetailReqUser = string.Empty;
        public void InsertDetailLog(DetailLogData _detail)
        {
            if (sDetailReqUser == string.Empty)
            {
                BatchLoggingData _data = this.GetRequestBatchInfo(_detail.AppID);
                sDetailReqUser = _data.ReqBy;
            }
            SqlCommand _cmd = new SqlCommand("sp_Common_InsertDetailLog");
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@APP_ID", _detail.AppID);
            _cmd.Parameters.AddWithValue("@STATUS", string.Format("{0}", _detail.Status).Substring(0, 1));
            _cmd.Parameters.AddWithValue("@FAVORITE_FLAG", (_detail.Favorite ? "Y" : "N"));
            _cmd.Parameters.AddWithValue("@LEVEL", string.Format("{0}", _detail.Level).Substring(0, 1));
            _cmd.Parameters.AddWithValue("@MESSAGE", _detail.Description);
            _cmd.Parameters.AddWithValue("@User", sDetailReqUser);
            dbconn.ExecuteNonQuery(_cmd);
        }

        public void InsertDetailLog(DetailLogData detail, params string[] param)
        {
            try
            {
                string parameter = string.Join("|", param);

                if (sDetailReqUser == string.Empty)
                {
                    BatchLoggingData _data = this.GetRequestBatchInfo(detail.AppID);
                    sDetailReqUser = _data.ReqBy;
                }
                SqlCommand _cmd = new SqlCommand("sp_Common_InsertLog_With_Param");
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.Parameters.AddWithValue("@APP_ID", detail.AppID);
                _cmd.Parameters.AddWithValue("@STATUS", string.Format("{0}", detail.Status).Substring(0, 1));
                _cmd.Parameters.AddWithValue("@MESSAGE_CODE", detail.MessageCode);
                _cmd.Parameters.AddWithValue("@MESSAGE_PARAM", parameter);
                _cmd.Parameters.AddWithValue("@strUserID", sDetailReqUser);
                _cmd.Parameters.AddWithValue("@FAVORITE_FLAG", 'N');
                dbconn.ExecuteNonQuery(_cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public System.Collections.ArrayList GetErrorDetailLog(int _ReqID)
        //{
        //    SqlCommand _cmd = new SqlCommand("sp_Common_GetErrorDetailLog");
        //    _cmd.CommandType = CommandType.StoredProcedure;
        //    _cmd.Parameters.AddWithValue("@ReqID", _ReqID);
        //    DataTable _dt = dbconn.GetData(_cmd);

        //    System.Collections.ArrayList _ar = new System.Collections.ArrayList();
        //    foreach (DataRow _dr in _dt.Rows)
        //    {
        //        _ar.Add(string.Format("{0}", _dr["MESSAGE"]));
        //    }
        //    return _ar;
        //}
        public eLogStatus GetBatchEndStatus(int _AppID)
        {
            SqlCommand _cmd = new SqlCommand("sp_Common_GetStatusDetailLog");
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@ReqID", _AppID);
            DataTable _dt = dbconn.GetData(_cmd);
            if (_dt.Rows.Count == 0)
                return eLogStatus.Successfully;

            DataRow _dr = _dt.Rows[0];
            if (Convert.ToInt32(_dr["E"]) > 0)
                return eLogStatus.Error;

            return eLogStatus.Successfully;


        }
        private void EndBatchOperation(string batchID, string userID)
        {
            SqlCommand _cmd = new SqlCommand("sp_PL2B601_EndBatchOperation");
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@BATCH_ID", batchID);
            _cmd.Parameters.AddWithValue("@USER_ID", userID);
            dbconn.ExecuteNonQuery(_cmd);
        }
        public void Close()
        {
            dbconn.Close();
        }

        public void AddDownloadFile(int _AppID, string _userID, string _BatchID, string _FileName)
        {
            SqlCommand _cmd = new SqlCommand("sp_Common_InsertFileDownload");
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@AppID", _AppID);
            _cmd.Parameters.AddWithValue("@BatchID", _BatchID);
            _cmd.Parameters.AddWithValue("@FileName", _FileName);
            _cmd.Parameters.AddWithValue("@User", _userID);
            dbconn.ExecuteNonQuery(_cmd);
        }

        public void StartBatchQ(BatchLoggingData BLoggingData)
        {
            try
            {
                DetailLogData _log = null;
                _log = new DetailLogData();
                BLoggingData.Status = eLogStatus.Processing;
                if (BLoggingData.AppID > 0)
                {
                    UpdateBatchQStatus(BLoggingData);
                }
                else
                {
                    _log.AppID = InsertBatchQ(BLoggingData);
                }
                _log.AppID = BLoggingData.AppID;
                _log.Level = eLogLevel.Information;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.Description = string.Format(MessageCommonBatch.MSTD7000BINF, BLoggingData.BatchName);
                InsertDetailLog(_log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void _SetBatchQEnd(BatchLoggingData BLoggingData)
        //{
        //    try
        //    {
        //        DetailLogData _log = null;
        //        _log = new DetailLogData();

        //        _log.AppID = BLoggingData.AppID;
        //        _log.Favorite = false;
        //        _log.Status = BLoggingData.Status;
        //        switch (BLoggingData.IProcessStatus)
        //        {
        //            case 1:
        //                _log.Level = eLogLevel.Information;
        //                _log.Description = string.Format(MessageCommonBatch.MSTD7001BINF, BLoggingData.BatchName);
        //                break;
        //            case 0:
        //                _log.Level = eLogLevel.Warning;
        //                _log.Description = string.Format(MessageCommonBatch.MSTD7003BINF, BLoggingData.BatchName, string.Empty);
        //                break;
        //            case -1:
        //                _log.Level = eLogLevel.Error;
        //                _log.Description = string.Format(MessageCommonBatch.MSTD7002BINF, BLoggingData.BatchName, string.Empty);
        //                break;
        //        }

        //        InsertDetailLog(_log);

        //        BLoggingData.Status = GetBatchEndStatus(BLoggingData.AppID);
        //        UpdateBatchQStatus(BLoggingData);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void SetBatchQEnd(BatchLoggingData BLoggingData)
        {
            try
            {
                DetailLogData _log = null;
                _log = new DetailLogData();
          
                _log.AppID = BLoggingData.AppID;
                _log.Favorite = false;
                _log.Status = BLoggingData.Status;
                _log.Level = BLoggingData.ProcessStatus;
                switch (BLoggingData.ProcessStatus)
                {
                    case  eLogLevel.Information :
                        _log.Description = string.Format(MessageCommonBatch.MSTD7001BINF, BLoggingData.BatchName);
                        break;
                    case eLogLevel.Warning:
                        _log.Description = string.Format(MessageCommonBatch.MSTD7003BINF, BLoggingData.BatchName, string.Empty);
                        break;
                    case eLogLevel.Error :
                        _log.Description = string.Format(MessageCommonBatch.MSTD7002BINF, BLoggingData.BatchName, string.Empty);
                        break;
                }

                InsertDetailLog(_log);

                BLoggingData.Status = GetBatchEndStatus(BLoggingData.AppID);
                UpdateBatchQStatus(BLoggingData);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public class BatchLoggingData
    {
        private int _AppID = -1;
        private string _BatchID = string.Empty;
        private string _BatchName = string.Empty;
        private string _ReqBy = string.Empty;
        private string _Description = string.Empty;
        private string _pid = string.Empty;
        private DateTime _ReqDateTime;
        private string _Arguments = string.Empty;
        private eLogStatus _Status = eLogStatus.Processing;
        private DateTime _UpdateDate;
        private int _iProcessStatus = -1;

        public int AppID
        {
            get { return _AppID; }
            set { _AppID = value; }
        }
        public string BatchID
        {
            get { return _BatchID; }
            set { _BatchID = value; }
        }
        public string BatchName
        {
            get { return _BatchName; }
            set { _BatchName = value; }
        }
        public DateTime ReqDateTime
        {
            get { return _ReqDateTime; }
        }
        public string Arguments
        {
            set { _Arguments = value; }
            get { return _Arguments; }
        }
        //public int IProcessStatus Please change to Process Status
        //{
        //    get { return _iProcessStatus; }
        //    set { _iProcessStatus = value; }
        //}
        public eLogLevel ProcessStatus
        {
            get;set;
        }
        public eLogStatus Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string ReqBy
        {
            get { return _ReqBy; }
            set { _ReqBy = value; }
        }
        public DateTime UpdateDate
        {
            get { return _UpdateDate; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string PID
        {
            get { return _pid; }
            set { _pid = value; }
        }

        public BatchLoggingData()
        {
        }

    }
    public class DetailLogData
    {
        public DetailLogData() { }
        public int AppID = -1;
        public eLogLevel Level = eLogLevel.Information;
        public string MessageCode;
        public string Description;
        public string User;
        public eLogStatus Status = eLogStatus.Processing;
        public bool Favorite;
    }
    public enum eLogStatus
    {
        Queue,
        Processing,
        Error,
        Successfully
    }
    public enum eLogLevel
    {
        Error,
        Information,
        Warning
    }
    public class DetailLogFileData
    {
        public int AppID;
        public string BatchID;
        public string FilePath;
        public string CreateBy;
    }
    public class MessageCommonBatch
    {
        public const string MSTD7000BINF = "MSTD7000BINF : {0} Begin";
        public const string MSTD7001BINF = "MSTD7001BINF : {0} End successfully";
        public const string MSTD7003BINF = "MSTD7003BINF : {0} End with warning {1}";
        public const string MSTD7002BINF = "MSTD7002BINF : {0} End with error {1}";

        public const string E_CANNOT_CONNECT_DB = "Cannot Connect Database";
    }
}