﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace th.co.toyota.stm.fas.common
{
    public class DataConnection
    {
        private SqlConnection dbConn;
        private SqlTransaction dbTrans;

        private string _ConnectionString = string.Empty;
        private int _ConnectionTimeOut = 0;
        public static string ConnectionString = string.Empty;
        public DataConnection(string _connectionString)
        {
            //
            // TODO: Add constructor logic here
            //

            if (_connectionString == null)
            {
                throw (new Exception("Not found ConnectionString in Configuration file"));
            }
            this._ConnectionString = _connectionString;
            ConnectionString = _connectionString;

        }
        public DataConnection()
        {

        }
        public int ConnectionTimeout
        {
            get { return this._ConnectionTimeOut; }
            set { this._ConnectionTimeOut = value; }
        }
        public DataTable GetData(SqlCommand _cmd)
        {
            try
            {
                if (_cmd.Connection == null)
                {
                    _cmd.CommandTimeout = this.ConnectionTimeout;
                    _cmd.Connection = dbConn;
                }
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                DataTable _dt = new DataTable();
                SqlDataAdapter _da = new SqlDataAdapter(_cmd);
                _da.Fill(_dt);
                dbConn.Close();
                return _dt;
            }
            catch (Exception ex)
            {
                //log4net
                throw (ex);
            }
        }
        public DataSet GetDataSet(SqlCommand _cmd, string _refTable)
        {
            try
            {
                if (_cmd.Connection == null)
                {
                    _cmd.CommandTimeout = this.ConnectionTimeout;
                    _cmd.Connection = dbConn;
                }
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(_cmd);
                da.Fill(ds, _refTable);
                dbConn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                //log4net
                throw (ex);
            }
        }
        public DataSet GetDataSet(SqlCommand _cmd)
        {
            try
            {
                if (_cmd.Connection == null)
                {
                    _cmd.CommandTimeout = this.ConnectionTimeout;
                    _cmd.Connection = dbConn;
                }
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(_cmd);
                da.Fill(ds);
                dbConn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                //log4net
                throw (ex);
            }
        }
        public void ExecuteNonQuery(SqlCommand _cmd)
        {
            try
            {
                if (_cmd.Connection == null)
                {
                    _cmd.Connection = dbConn;
                    _cmd.CommandTimeout = this.ConnectionTimeout;
                }
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //log4net
                throw (ex);
            }
        }
        public object Execute(SqlCommand _cmd)
        {
            try
            {
                if (_cmd.Connection == null)
                {
                    _cmd.Connection = dbConn;
                    _cmd.CommandTimeout = this.ConnectionTimeout;
                }
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                return _cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                //log4net
                throw (ex);
            }
        }
        public DataTable GetData(string _sql)
        {
            try
            {
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();

                DataTable _dt = new DataTable();
                SqlDataAdapter _da = new SqlDataAdapter(_sql, dbConn);

                _da.Fill(_dt);
                dbConn.Close();
                return _dt;
            }
            catch (Exception ex)
            {
                //log4net
                throw (ex);
            }

        }
        public bool TestConnection()
        {
            try
            {
                dbConn = new SqlConnection(_ConnectionString);
                dbConn.Open();

                return true;
            }
            catch
            {
                return false;
            }
        }
        public void Close()
        {
            if (dbConn.State != ConnectionState.Closed)
                dbConn.Close();
        }
        public static DataTable Exec(SqlCommand _cmd)
        {
            try
            {
                if (_cmd.Connection == null)
                {
                    _cmd.Connection = new SqlConnection(ConnectionString);
                }
                if (_cmd.Connection.State != ConnectionState.Open)
                    _cmd.Connection.Open();

                DataTable _dt = new DataTable();
                SqlDataAdapter _da = new SqlDataAdapter(_cmd);
                _da.Fill(_dt);
                _cmd.Connection.Close();
                return _dt;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                _cmd.Connection.Close();
            }

        }

        // Transaction
        public void BeginTransaction()
        {
            if (dbConn == null || dbConn.State == ConnectionState.Closed)
            {
                dbConn.Open();
            }
            dbTrans = dbConn.BeginTransaction();
        }
        public void Commit()
        {
            if (dbTrans != null)
            {
                dbTrans.Commit();
            }
        }
        public void Rollback()
        {
            if (dbTrans != null)
            {
                dbTrans.Rollback();
            }
        }
        public void CloseTransaction()
        {
            try
            {
                if (dbTrans != null)
                    dbTrans.Dispose();
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                dbTrans = null;
                dbConn = null;
            }
        }
        public object ExecuteTrans(SqlCommand _cmd)
        {
            try
            {
                _cmd.CommandType = CommandType.StoredProcedure;
                _cmd.Connection = dbConn;
                _cmd.Transaction = dbTrans;
                return _cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public object IfNull(object _x)
        {
            if (_x == null || string.IsNullOrEmpty(string.Format("{0}", _x)))
                return DBNull.Value;

            return _x;
        }

        private bool ColumnExists(SqlDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i) == columnName)
                {
                    return true;
                }
            }

            return false;
        }

        #region Covert value 

        private static System.DateTime? GetDate(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDateTime(objRow[fieldName]);
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        private static decimal GetDecimal(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDecimal(objRow[fieldName]);
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }
        private static double GetDouble(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDouble(objRow[fieldName]);
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static int GetInt(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return int.Parse(objRow[fieldName].ToString());
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static string GetString(SqlDataReader objRow, string fieldName, bool isWithNullable)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToString(objRow[fieldName]);
                }
            }
            catch
            {
                if (isWithNullable)
                { return null; }
                return string.Empty;
            }
            if (isWithNullable)
            { return null; }
            return string.Empty;
        }
        #endregion

        protected List<T> _DataReaderMapToList<T>(SqlDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (System.Reflection.PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (ColumnExists(dr, prop.Name))
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            //  -- prop.SetValue(obj, dr[prop.Name], null);  
                            if (prop.PropertyType == typeof(String))
                            {
                                prop.SetValue(obj, GetString(dr, prop.Name, false), null);
                                continue;
                            }
                            if (prop.PropertyType == typeof(Decimal) || (prop.PropertyType == typeof(Decimal?)))
                            {
                                prop.SetValue(obj, GetDecimal(dr, prop.Name), null);
                                continue;
                            }
                            if (prop.PropertyType == typeof(Double) || (prop.PropertyType == typeof(Double?)))
                            {
                                prop.SetValue(obj, GetDouble(dr, prop.Name), null);
                                continue;
                            }
                            //DateTime
                            if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                            {
                                prop.SetValue(obj, GetDate(dr, prop.Name), null);
                                continue;
                            }
                            //Int
                            if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?))
                            {
                                prop.SetValue(obj, GetInt(dr, prop.Name), null);
                                continue;
                            }
                        }
                    }
                }
                list.Add(obj);
            }
            return list;
        }
        public List<T> executeDataToList<T>(SqlCommand _cmd)
        {

            if (_cmd.Connection == null)
            {
                _cmd.CommandTimeout = this.ConnectionTimeout;
                _cmd.Connection = dbConn;
            }
            

            List<T> result = new List<T>();
            SqlDataAdapter ad = new SqlDataAdapter(_cmd);
            SqlDataReader myReader = null;
            ad.SelectCommand.CommandType = System.Data.CommandType.StoredProcedure;

            
            //for (int i = 0; i < _cmd.Parameters.Count; i++)
            //    ad.SelectCommand.Parameters.Add(_cmd.Parameters[i]);

            try
            {
                if (dbConn.State != ConnectionState.Open)
                    dbConn.Open();
                
                myReader = ad.SelectCommand.ExecuteReader();
                result = _DataReaderMapToList<T>(myReader);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (myReader != null && !myReader.IsClosed)
                    myReader.Close();
            }

            return result;
        }
    }

  
}