﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.common
{
    [Serializable]
    public class BatchLoggingException : Exception, ISerializable
    {
        private DateTime m_dtTimestamp;
        private string m_strMachineName;
        public System.Collections.ArrayList arErrorList = null;
        public BatchLoggingException()
            : base()
        {
            arErrorList = null;
        }

        public BatchLoggingException(string strMessage)
            : base(strMessage)
        {
            arErrorList = null;
        }
        public BatchLoggingException(string format, params object[] _args)
            : base(string.Format(format, _args))
        {
            arErrorList = null;
        }
        public BatchLoggingException(string strMessage, System.Collections.ArrayList _arErrorList)
            : base(strMessage)
        {
            arErrorList = new System.Collections.ArrayList();
            arErrorList.AddRange(_arErrorList);
        }

        public BatchLoggingException(Exception e)
            : base(e.Message, e)
        {
            arErrorList = null;
        }

        //deserialization contructor
        public BatchLoggingException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

            m_dtTimestamp = info.GetDateTime("Timestamp");
            m_strMachineName = info.GetString("MachineName");
        }

        public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Timestamp", m_dtTimestamp);
            info.AddValue("MachineName", m_strMachineName);
        }

    }
}
