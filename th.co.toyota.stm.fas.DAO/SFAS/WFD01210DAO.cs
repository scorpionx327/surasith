﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01210;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD01210DAO : Database
    {
        public bool GetFAadminFlag(string emp_code)
        {
            string FAAdminFlag = string.Empty;
            bool IsFAAdmin = false;
            try
            {


            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", System.Data.SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
            FAAdminFlag= executeScalar<string>("sp_FD01210DAO_GetFAAdminFlag", param);
            
            if (FAAdminFlag.Equals("Y"))
            {
                IsFAAdmin = true;
            }
            else
            {
                IsFAAdmin = false;
            }
            }
            catch
            {
                IsFAAdmin = false;
            }

            return IsFAAdmin;
        }

        //public bool GetAECUserFlag(string emp_code)
        //{
        //    try
        //    {
        //        List<SqlParameter> param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("@EMP_CODE", System.Data.SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
        //        var FAAdminFlag = executeScalar<string>("sp_FD01210DAO_GetAECUserFlag", param);

        //        return  string.Format("{0}",FAAdminFlag) == "Y";
              
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public string GetSystemAdminFlag(string RoleID )
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@User", System.Data.SqlDbType.VarChar) { Value = (object)RoleID ?? DBNull.Value });
            return executeScalar<string>("sp_Common_GetODBBatchCallingPath", param);
        }
        public List<TB_M_ORGANIZATION> GetOrganizationDAO()
        {
            return executeDataToList<TB_M_ORGANIZATION>("sp_Common_GetAllOrganization");
        }
        public List<TB_M_ORGANIZATION> GetDivisionNameDAO()
        {
            return executeDataToList<TB_M_ORGANIZATION>("sp_WFD01210_GetDivisionName");
        }
        public List<TB_M_ORGANIZATION> GetDepartmentNameDAO(string _DivName)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@pDivName", SqlDbType.VarChar) { Value = (object)_DivName ?? DBNull.Value });
            return executeDataToList<TB_M_ORGANIZATION>("sp_WFD01210_GetDepartmentName", param);
        }
        public List<TB_M_ORGANIZATION> GetSectionNameDAO(string _DivName, string _DeptName)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@pDivName", SqlDbType.VarChar) { Value = (object)_DivName ?? DBNull.Value });
            param.Add(new SqlParameter("@pDeptName", SqlDbType.VarChar) { Value = (object)_DeptName ?? DBNull.Value });
           
            return executeDataToList<TB_M_ORGANIZATION>("sp_WFD01210_GetSectionName", param);
        }
        public List<WFD01210PositionModel> GetPositionDAO()
        {
            return executeDataToList<WFD01210PositionModel>("sp_WFD01210_GetPosition");
        }
        public List<WFD01210EmployeeModel> GetSearchUserProfileDAO(WFD01210SearchModel Model, ref PaginationModel page)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@DIV_NAME", SqlDbType.VarChar) { Value = (object)Model.DIV_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@DEPT_NAME", SqlDbType.VarChar) { Value = (object)Model.DEPT_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@SEC_NAME", SqlDbType.VarChar) { Value = (object)Model.SEC_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)Model.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_NAME", SqlDbType.VarChar) { Value = (object)Model.EMP_NAME ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_LASTNAME", SqlDbType.VarChar) { Value = (object)Model.EMP_LASTNAME ?? DBNull.Value });
            param.Add(new SqlParameter("@POST_CODE", SqlDbType.VarChar) { Value = (object)Model.POST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)Model.ROLE ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)Model.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@IN_CHARGE_COST_CENTER", SqlDbType.VarChar) { Value = (object)Model.IN_CHARGE_COST_CENTER ?? DBNull.Value });

            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)Model.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_DIV_NAME", SqlDbType.VarChar) { Value = (object)Model.SUB_DIV_NAME ?? DBNull.Value });
            param.Add(new SqlParameter("@LINE", SqlDbType.VarChar) { Value = (object)Model.LINE ?? DBNull.Value });
            param.Add(new SqlParameter("@JOB", SqlDbType.VarChar) { Value = (object)Model.JOB ?? DBNull.Value });
            param.Add(new SqlParameter("@RESPONSIBILITY", SqlDbType.VarChar) { Value = (object)Model.RESPONSIBILITY ?? DBNull.Value });
            param.Add(new SqlParameter("@SPECIAL_ROLE", SqlDbType.VarChar) { Value = (object)Model.SPECIAL_ROLE ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });

            param.Add(new SqlParameter("@M_SYS_EMP_CODE", SqlDbType.VarChar) { Value = (object)Model.M_SYS_EMP_CODE ?? DBNull.Value });

            param.Add(TotalItem);
            var datas = executeDataToList<WFD01210EmployeeModel>("sp_WFD01210_SearchUserProfile", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        public List<WFD01210CCBaseOnOrganizeModel> GetCCBaseOnOrganizeDAO(WFD01210SearchModel Model, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)Model.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);
            var datas = executeDataToList<WFD01210CCBaseOnOrganizeModel>("sp_WFD01210_GetCostCenter", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        public void DeleteEmployeeDAO(WFD01210SearchModel data, string UserLogin)
        {
            List<SqlParameter> param = new List<SqlParameter>();
         
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)UserLogin ?? DBNull.Value });
            execute("sp_WFD01210_DeleteEmployee", param);
        }
        public void DeleteEmployeeSVCostCenterDAO(WFD01210SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            execute("sp_WFD01210_DeleteEmployeeSVCostCenter", param);
        }
        public List<TB_M_COST_CENTER> GetCostCenterDAO(WFD01210SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            return executeDataToList<TB_M_COST_CENTER>("sp_WFD01210_GetCostCenterAll",param);
        }
        public WFD01210EmployeeDetailModel GetEmployeeByCodeDAO(WFD01210SearchModel data )
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@SYS_EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

            return executeDataToList<WFD01210EmployeeDetailModel>("sp_WFD01210_GetEmployeeByCode", param).FirstOrDefault();
        }
        public List<WFD01210DelegateUserModel> GetDelegateUserDAO(WFD01210SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EmpCode", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            return executeDataToList<WFD01210DelegateUserModel>("sp_WFD01210_GetDelegateUserList", param);
        }

        public List<WFD01210AECUserModel> GetAECUserDAO(WFD01210SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            return executeDataToList<WFD01210AECUserModel>("sp_WFD01210_GetAECUserList", param);
        }
        public List<WFD01210EmployeeModel> GetCompayList(WFD01210SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            return executeDataToList<WFD01210EmployeeModel>("sp_Common_GetCompanyList", param);
        }

        public void InitializeCostCenterPopup(string _EmpCode, string _Company, string _LoginUser)
        {
            //sp_WFD01210InitialCostCenterPopup
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = _EmpCode });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = _Company });
            param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = _LoginUser });

            execute("sp_WFD01210InitialCostCenterPopup", param);
        }
        private void InsertEmployeeDAO(WFD01210SaveEmployeeModel data,string UserLogin)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE.ToUpper().Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY.ToUpper().Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_TITLE", SqlDbType.VarChar) { Value = (object)data.EMP_TITLE.Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_NAME", SqlDbType.VarChar) { Value = (object)data.EMP_NAME.Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_LASTNAME", SqlDbType.VarChar) { Value = (object)data.EMP_LASTNAME.Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@ORG_CODE", SqlDbType.VarChar) { Value = (object)data.ORG_CODE.Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            //param.Add(new SqlParameter("@POST_CODE", SqlDbType.VarChar) { Value = (object)data.POST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@POST_NAME", SqlDbType.VarChar) { Value = (object)data.POST_NAME ?? DBNull.Value });
            param.Add(new SqlParameter("@EMAIL", SqlDbType.VarChar) { Value = (object)data.EMAIL ?? DBNull.Value });
            param.Add(new SqlParameter("@TEL_NO", SqlDbType.VarChar) { Value = (object)data.TEL_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@SIGNATURE_PATH", SqlDbType.VarChar) { Value = (object)data.SIGNATURE_PATH ?? DBNull.Value });
            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ROLE ?? DBNull.Value });
            param.Add(new SqlParameter("@SPEC_DIV", SqlDbType.VarChar) { Value = (object)data.SPEC_DIV ?? DBNull.Value });
            param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)data.FAADMIN ?? DBNull.Value });
            param.Add(new SqlParameter("@DELEGATE_PIC", SqlDbType.VarChar) { Value = (object)data.DELEGATE_PIC ?? DBNull.Value });
            param.Add(new SqlParameter("@DELEGATE_FROM", SqlDbType.VarChar) { Value = (object)data.DELEGATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@DELEGATE_TO", SqlDbType.VarChar) { Value = (object)data.DELEGATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)UserLogin ?? DBNull.Value });
           
            param.Add(new SqlParameter("@AEC_USER", SqlDbType.VarChar) { Value = (object)data.AEC_User ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_MANAGER", SqlDbType.VarChar) { Value = (object)data.AEC_Manager ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_GENERAL_MANAGER", SqlDbType.VarChar) { Value = (object)data.AEC_GENERAL_MANAGER ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_USER_LIST", SqlDbType.VarChar) { Value = (object)data.AEC_USER_LIST ?? DBNull.Value });
            param.Add(new SqlParameter("@FASupervisor", SqlDbType.VarChar) { Value = (object)data.FASupervisor ?? DBNull.Value });
            param.Add(new SqlParameter("@FAWindow", SqlDbType.VarChar) { Value = (object)data.FAWindow ?? DBNull.Value });

            //param.Add(new SqlParameter("@JOB_CODE", SqlDbType.VarChar) { Value = (object)data.JOB ?? DBNull.Value });
            param.Add(new SqlParameter("@JOB_NAME", SqlDbType.VarChar) { Value = (object)data.JOB_NAME ?? DBNull.Value });

            execute("sp_WFD01210_InsertEmployee", param);
        }

        public void InsertEmployee(WFD01210SaveEmployeeModel data, string UserLogin)
        {
            try
            {
                beginTransaction();
                
                //this.InsertInChargeCostCenterDAO(data,UserLogin);
                //this.InsertInChargeResCostCenterDAO(data, UserLogin);
                this.InsertEmployeeDAO(data, UserLogin);
                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        
        public void UpdateEmployee(WFD01210SaveEmployeeModel data, string UserLogin)
        {
            try
            {
                beginTransaction();

                //this.InsertInChargeCostCenterDAO(data, UserLogin);
                //this.InsertInChargeResCostCenterDAO(data, UserLogin);

                this.UpdateEmployeeFADAO(data, UserLogin);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void InsertInChargeCostCenterDAO(WFD01210SaveEmployeeModel _data, string _User)
        {
            List<SqlParameter> param = new List<SqlParameter>();
           
            param.Add(new SqlParameter("@COST_CODE_LIST", SqlDbType.VarChar) { Value = (object)_data.IN_CHARGE_COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)_data.SYS_EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)_User ?? DBNull.Value  });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)_data.COMPANY ?? DBNull.Value });
            execute("sp_WFD01210_Insert_COSTCENTER", param);
            
        }

        public void InsertInChargeResCostCenterDAO(WFD01210SaveEmployeeModel _data, string _User)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COST_CODE_LIST", SqlDbType.VarChar) { Value = (object)_data.IN_RESPONSE_CHARGE_COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)_data.SYS_EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)_User ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)_data.COMPANY ?? DBNull.Value });
            execute("sp_WFD01210_Insert_FASUP_COSTCENTER", param);

        }

        private void UpdateEmployeeFADAO(WFD01210SaveEmployeeModel data, string UserLogin)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@SYS_EMP_CODE", SqlDbType.VarChar) { Value = (object)data.SYS_EMP_CODE.ToUpper().Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_TITLE", SqlDbType.VarChar) { Value = (object)data.EMP_TITLE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_NAME", SqlDbType.VarChar) { Value = (object)data.EMP_NAME ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_LASTNAME", SqlDbType.VarChar) { Value = (object)data.EMP_LASTNAME ?? DBNull.Value });
            param.Add(new SqlParameter("@ORG_CODE", SqlDbType.VarChar) { Value = (object)data.ORG_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            //param.Add(new SqlParameter("@POST_CODE", SqlDbType.VarChar) { Value = (object)data.POST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@POST_NAME", SqlDbType.VarChar) { Value = (object)data.POST_NAME ?? DBNull.Value });
            param.Add(new SqlParameter("@EMAIL", SqlDbType.VarChar) { Value = (object)data.EMAIL ?? DBNull.Value });
            param.Add(new SqlParameter("@TEL_NO", SqlDbType.VarChar) { Value = (object)data.TEL_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@SIGNATURE_PATH", SqlDbType.VarChar) { Value = (object)data.SIGNATURE_PATH ?? DBNull.Value });
            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ROLE ?? DBNull.Value });
            param.Add(new SqlParameter("@SPEC_DIV", SqlDbType.VarChar) { Value = (object)data.SPEC_DIV ?? DBNull.Value });
            param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)data.FAADMIN ?? DBNull.Value });
            param.Add(new SqlParameter("@DELEGATE_PIC", SqlDbType.VarChar) { Value = (object)data.DELEGATE_PIC ?? DBNull.Value });
            param.Add(new SqlParameter("@DELEGATE_FROM", SqlDbType.VarChar) { Value = (object)data.DELEGATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@DELEGATE_TO", SqlDbType.VarChar) { Value = (object)data.DELEGATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)UserLogin ?? DBNull.Value });
            //param.Add(new SqlParameter("@JOB", SqlDbType.VarChar) { Value = (object)data.JOB ?? DBNull.Value });
            param.Add(new SqlParameter("@JOB_NAME", SqlDbType.VarChar) { Value = (object)data.JOB_NAME ?? DBNull.Value });

            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@SP_ROLE_COMPANY", SqlDbType.VarChar) { Value = (object)data.SP_ROLE_COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_USER", SqlDbType.VarChar) { Value = (object)data.AEC_User ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_MANAGER", SqlDbType.VarChar) { Value = (object)data.AEC_Manager ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_GENERAL_MANAGER", SqlDbType.VarChar) { Value = (object)data.AEC_GENERAL_MANAGER ?? DBNull.Value });
            param.Add(new SqlParameter("@AEC_USER_LIST", SqlDbType.VarChar) { Value = (object)data.AEC_USER_LIST ?? DBNull.Value });
            param.Add(new SqlParameter("@FASupervisor", SqlDbType.VarChar) { Value = (object)data.FASupervisor ?? DBNull.Value });
            param.Add(new SqlParameter("@FAWindow", SqlDbType.VarChar) { Value = (object)data.FAWindow ?? DBNull.Value });

            execute("sp_WFD01210_UpdateEmployeeFAS", param);

        }
        //private void UpdateEmployeeUsersDAO(WFD01210SaveEmployeeModel data, string UserLogin)
        //{
        //    List<SqlParameter> param = new List<SqlParameter>();
        //    param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
        //    param.Add(new SqlParameter("@TEL_NO", SqlDbType.VarChar) { Value = (object)data.TEL_NO ?? DBNull.Value });
        //    param.Add(new SqlParameter("@SIGNATURE_PATH", SqlDbType.VarChar) { Value = (object)data.SIGNATURE_PATH ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ROLE ?? DBNull.Value });
        //    param.Add(new SqlParameter("@SPEC_DIV", SqlDbType.VarChar) { Value = (object)data.SPEC_DIV ?? DBNull.Value });
        //    param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)data.FAADMIN ?? DBNull.Value });
        //    param.Add(new SqlParameter("@DELEGATE_PIC", SqlDbType.VarChar) { Value = (object)data.DELEGATE_PIC ?? DBNull.Value });
        //    param.Add(new SqlParameter("@DELEGATE_FROM", SqlDbType.Date) { Value = (object)data.DELEGATE_FROM ?? DBNull.Value });
        //    param.Add(new SqlParameter("@DELEGATE_TO", SqlDbType.Date) { Value = (object)data.DELEGATE_TO ?? DBNull.Value });
        //    param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)UserLogin ?? DBNull.Value });
        //    execute("sp_WFD01210_UpdateEmployeeHRMS", param);

        //}
        public List<TB_M_ORGANIZATION> SearchtOrganizationDAO(WFD01210SearchModel data, ref PaginationModel page)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });
            param.Add(new SqlParameter("@ORG_CODE", SqlDbType.VarChar) { Value = (object)data.ORG_CODE ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });

            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(TotalItem);
            var datas = executeDataToList<TB_M_ORGANIZATION>("sp_WFD01210_SearchOrganizeByOrgCode", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //OK Surasith
        public List<WFD01210InChargeModel> GetInChargeDAO(WFD01210SearchModel data, string _Login)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });
            param.Add(new SqlParameter("@NO_ASSET_FLAG", SqlDbType.VarChar) { Value = (object)data.NO_ASSET_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = _Login });
            return executeDataToList<WFD01210InChargeModel>("sp_WFD01210_GetCostCenter_In_Charge", param);
        }
        //OK Surasith
        public List<WFD01210InChargeModel> GetResponsibleInChargeDAO(WFD01210SearchModel data, string _Login)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });
            param.Add(new SqlParameter("@NO_ASSET_FLAG", SqlDbType.VarChar) { Value = (object)data.NO_ASSET_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = _Login });
            return executeDataToList<WFD01210InChargeModel>("sp_WFD01210_GetRespCostCenter_In_Charge", param);
        }
        //OK
        public List<WFD01210InChargeModel> GetInChargeTableDAO(WFD01210SearchModel data, string _Login)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@IS_RESPONSE_COST_CENTER", SqlDbType.VarChar) { Value = (object)data.IS_RESPONSE_COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = _Login });
            return executeDataToList<WFD01210InChargeModel>("sp_WFD01210_GetIn_Charge", param);
        }
        //OK
        public List<WFD01210SVCostCenterModel> CheckSVCostCenterDAO(string IN_CHARGE_COST_CENTER, string UserLogin)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)IN_CHARGE_COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)UserLogin ?? DBNull.Value });
            return executeDataToList<WFD01210SVCostCenterModel>("sp_WFD01210_Check_In_Charge", param);
        }
        //OK
        public List<CostCenterAutoCompleteModels> GetCostCenterAutoComplete(string keyword, string empcode,string isfasup)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@FASup", base.IfNull(isfasup)));
            return executeDataToList<CostCenterAutoCompleteModels>("sp_WFD01210_GetCostCenter_AutoComplete", param);
        }

        public WFD01210EmployeeDetailModel GetEmployeeByEmpCodeDAO(WFD01210SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@SYS_EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            return executeDataToList<WFD01210EmployeeDetailModel>("sp_WFD01110_GetEmployeeByCode", param).FirstOrDefault();
        }


        public string GetAllowSearchDAO(string _Company, string _SysEmpCode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)_Company ?? DBNull.Value });
            param.Add(new SqlParameter("@SYS_EMP_CODE", SqlDbType.VarChar) { Value = (object)_SysEmpCode ?? DBNull.Value });
            return executeScalar<string>("sp_WFD01210_Check_Allow_Search", param);

        }

    }
}
