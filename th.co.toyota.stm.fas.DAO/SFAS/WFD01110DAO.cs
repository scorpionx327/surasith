﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01110;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD01110DAO : Database
    {

        public WFD01110DAO() : base()
        {

        }
        public List<WFD01110TransferModel> GetTransferDAO(WFD01110TransferModel model, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)model.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@TRNF_MAIN_TYPE", SqlDbType.VarChar) { Value = (object)model.TRNF_MAIN_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110TransferModel>("sp_WFD01110_GetTransfer", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        public List<WFD01110AssetNewModel> GetAssetNewDAO(WFD01110AssetNewModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110AssetNewModel>("sp_WFD01110_GetAssetNew", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        
        public List<WFD01110SettlementModel> GetSettlementDAO(WFD01110SettlementModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110SettlementModel>("sp_WFD01110_GetSettlement", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //Reclassification
        public List<WFD01110ReclassificationModel> GetReclassificationDAO(WFD01110ReclassificationModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110ReclassificationModel>("sp_WFD01110_GetReclassification", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //Asset Change
        public List<WFD01110AssetChangeModel> GetAssetChangeDAO(WFD01110AssetChangeModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110AssetChangeModel>("sp_WFD01110_GetAssetChange", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //Reprint
        public List<WFD01110AssetChangeModel> GetReprintDAO(WFD01110AssetChangeModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110AssetChangeModel>("sp_WFD01110_GetReprint", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //Retirement
        public List<WFD01110RetirementModel> GetRetirementDAO(WFD01110RetirementModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@DISP_TYPE", SqlDbType.VarChar) { Value = (object)con.DISP_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110RetirementModel>("sp_WFD01110_GetRetirement", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //Imparment
        public List<WFD01110ImpairmentModel> GetImpairmentDAO(WFD01110ImpairmentModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110ImpairmentModel>("sp_WFD01110_GetImpairment", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //Physical Count
        public List<WFD01110PhysicalCountModel> GetPhysicalCountDAO(WFD01110PhysicalCountModel con, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110PhysicalCountModel>("sp_WFD01110_GetPhysicalCount", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        
        public List<WFD01110OnProcessModel> GetOnProcessDAO(WFD01110SearchRequestModel data, string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = EMP_CODE });
            param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110OnProcessModel>("sp_WFD01110_GetOnProcess", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }


        public List<WFD01110OnHandAECModel> GetOnHandAEC(WFD01110SearchRequestModel data, string EMP_CODE, ref PaginationModel page)
        {
            page.Totalitem = 0;
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = EMP_CODE });
            param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01110OnHandAECModel>("sp_WFD01110_GetOnHandAEC", param);

            if (datas == null || datas.Count == 0)
                return new List<WFD01110OnHandAECModel>() ;

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        public WFD01110ToDoListModel GetToDoListDAO(string emp_code)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
            var datas = executeDataToList<WFD01110ToDoListModel>("sp_WFD01110_GetToDoList", param);
            return datas.FirstOrDefault();
        }

        public WFD01110ToDoListFASupModel GetToDoListFASubDAO(string emp_code)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
            var datas = executeDataToList<WFD01110ToDoListFASupModel>("sp_WFD01110_GetToDoListFASup", param);
            return datas.FirstOrDefault();
        }

        public WFD01110TotalPerTabModel GetTotalPerTab(string emp_code)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
            var datas = executeDataToList<WFD01110TotalPerTabModel>("sp_WFD01110_GetTotalPerTab", param);
            return datas.FirstOrDefault();
        }

        public List<WFD01110TotalRequestSummary> GetRequestSummary(string emp_code)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
            var datas = executeDataToList<WFD01110TotalRequestSummary>("sp_WFD01110_GetRequestSummaryAEC", param);
            return datas;
        }

        public void SaveAnnouncementDAO(WFD01110SaveAnnouncementModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ANNOUNCE_DESC", SqlDbType.VarChar) { Value = (object)data.ANNOUNCE_DESC ?? DBNull.Value });
            param.Add(new SqlParameter("@ANNOUNCE_URL", SqlDbType.VarChar) { Value = (object)data.ANNOUNCE_URL ?? DBNull.Value });
            param.Add(new SqlParameter("@DISPLAY_FLAG", SqlDbType.VarChar) { Value = (object)data.DISPLAY_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@ANNOUNCE_TYPE", SqlDbType.VarChar) { Value = (object)data.ANNOUNCE_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY ?? DBNull.Value });
            execute("sp_WFD01110_SaveAnnouncement", param);
        }
        public List<WFD01110AnnouncementModel> GetAnnouncementDAO(WFD01110SearchRequestModel con)
        {
            StringBuilder builder = new StringBuilder();

            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)con.COMPANY ?? DBNull.Value });

            return executeDataToList<WFD01110AnnouncementModel>("sp_WFD01110_GetAnnouncement", param);
        }
        public string CheckStockTakeDAO(string Login)
        {
            string value = "";
            
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)Login ?? DBNull.Value });
            var data = executeDataToList<WFD01110StockTakeKeyModel>("sp_WFD01110_CheckStockPlan", param).FirstOrDefault();
            if (data != null)
            {
                value = data.STOCK_TAKE_KEY;
            }

            return value;
           
          
        }
        public List<WFD01110StockTakeModel> GetStockTakeDAO(string EMP_CODE, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);
            var datas = executeDataToList<WFD01110StockTakeModel>("sp_WFD01110_GetStockTake", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());
            return datas;
        }


        public List<WFD01110BaseModel> GetCompanyList(string EMP_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            var datas = executeDataToList<WFD01110BaseModel>("sp_WFD01110_GetCompanyList", param);
            return datas;
        }

    }
}