﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.WFD02710;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.WFD02115;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02710DAO : RequestDetailDAO
    {
        public void InitialAssetList(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            execute("sp_WFD02710_InitialAssetList", param);
        }


        public List<WFD02710AUCModel> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });

            param.Add(TotalItem);

            var datas = executeDataToList<WFD02710AUCModel>("sp_WFD02710_GetAssetList", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());



            return datas;
        }
        public void PrepareAverage(WFD02710AUCModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            execute("sp_WFD02710_PrepareAverage", param);

        }

        //
        public List<WFD02710TargetModel> GetTargetAssetList(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            var datas = executeDataToList<WFD02710TargetModel>("sp_WFD02710_GetTargetAssetList", param);
            return datas;
        }

        public List<WFD02710TargetModel> getCurrentActiveSettlement(WFD01170MainModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.RequestHeaderData.USER_BY) });
            var datas = executeDataToList<WFD02710TargetModel>("sp_WFD02710_GetActiveSettlement", param);
            return datas;
        }

        public void ClearAssetList(WFD0BaseRequestDocModel data)
        {
            // Allow only Submit 
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            execute("sp_WFD02710_ClearAssetList", param);
        }

        public List<MessageModel> AssetValidation(WFD01170MainModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            //param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.RequestHeaderData.DOC_NO != null ? data.RequestHeaderData.DOC_NO : data.RequestHeaderData.TEMP_DOC_NO) });
            return executeDataToList<MessageModel>("sp_WFD02710_ValidationAsset", param);
        }
        public List<MessageModel> GenerateFlowValidation(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            //param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO != null ? data.DOC_NO : data.TEMP_DOC_NO) });
            return executeDataToList<MessageModel>("sp_WFD02710_GenerateFlowValidation", param);
        }
        
        public List<MessageModel> SubmitValidation(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO != null ? data.DOC_NO : data.TEMP_DOC_NO) });
            return executeDataToList<MessageModel>("sp_WFD02710_SubmitValidation", param);
        }
        public List<MessageModel> ApproveValidation(WFD0BaseRequestDocModel _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02710_ApproveValidation", param);

        }


        public void InsertAsset(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });

                execute("sp_WFD02710_InsertAsset", param);


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void DeleteAsset(WFD0BaseRequestDetailModel data, string invNo, string invLine)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@INV_NO", SqlDbType.VarChar) { Value = base.IfNull(invNo) });
                param.Add(new SqlParameter("@INV_LINE", SqlDbType.VarChar) { Value = base.IfNull(invLine) });

                execute("sp_WFD02710_DeleteAsset", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void DeleteTargetAssets(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });


                execute("sp_WFD02710_DeleteTargetAssets", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void InsertNewToTargetAssets(WFD02710SaveModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@COMPANY", base.IfNull(data.COMPANY)));
                param.Add(new SqlParameter("@DOC_NO", base.IfNull(data.DOC_NO)));
                param.Add(new SqlParameter("@ASSET_NO", base.IfNull(data.ASSET_NO)));
                param.Add(new SqlParameter("@ASSET_SUB", base.IfNull(data.ASSET_SUB)));
                param.Add(new SqlParameter("@LINE_NO", base.IfNull(data.LINE_NO)));
                param.Add(new SqlParameter("@COST_CODE", base.IfNull(data.COST_CODE)));
                param.Add(new SqlParameter("@RESP_COST_CODE", base.IfNull(data.RESP_COST_CODE)));
                param.Add(new SqlParameter("@GUID", base.IfNull(data.GUID)));


                execute("sp_WFD02710_InsertNewToTargetAssets", param);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            //
        }
        public void InsertExistsToTargetAssets(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });

                execute("sp_WFD02710_InsertExistsToTargetAssets", param);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            //
        }
        public void PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID }); //Submit mode
            param.Add(new SqlParameter("@ACT_ROLE", SqlDbType.VarChar) { Value = data.ACT_ROLE }); //Submit mode
            param.Add(new SqlParameter("@FLOW_TYPE", SqlDbType.VarChar) { Value = data.FLOW_TYPE });


            execute("sp_WFD02710_PrepareGenerateFlow", param);

        }

        public void UpdateTargetAsset(List<WFD02710TargetModel> lst)
        {
            try
            {
                foreach (var data in lst)
                {

                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                    param.Add(new SqlParameter("@CAPDATE", SqlDbType.VarChar) { Value = base.IfNull(data.CAP_DATE) });
                    execute("sp_WFD02710_UpdateTargetAsset", param);
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void UpdateSettlement(WFD02710AUCModel data, string _User, bool isAvgCost)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_NO) });
            param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_SUB) });
            param.Add(new SqlParameter("@SAP_DOC", SqlDbType.VarChar) { Value = base.IfNull(data.SAP_DOC) });
            param.Add(new SqlParameter("@PO_NO", SqlDbType.VarChar) { Value = base.IfNull(data.PO_NO) });

            param.Add(new SqlParameter("@INV_NO", SqlDbType.VarChar) { Value = base.IfNull(data.INV_NO) });
            param.Add(new SqlParameter("@INV_LINE", SqlDbType.VarChar) { Value = base.IfNull(data.INV_LINE) });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.TARGET_ASSET_NO) });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.TARGET_ASSET_SUB) });
            param.Add(new SqlParameter("@SETTLE_AMT", SqlDbType.Decimal) { Value = base.IfNull(data.SETTLE_AMOUNT) });
            param.Add(new SqlParameter("@ADJ_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.ADJ_TYPE) });
            param.Add(new SqlParameter("@ISAVG_COST", SqlDbType.Bit) { Value = isAvgCost ? 1 : 0 });

            execute("sp_WFD02710_UpdateSettlementAmount", param);
        }

        public void UpdateSettlementGroup(WFD02710AUCModel data, string _User, bool isAvgCost)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            //param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
            //param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            //param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_NO) });
            //param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_SUB) });
            //param.Add(new SqlParameter("@SAP_DOC", SqlDbType.VarChar) { Value = base.IfNull(data.SAP_DOC) });
            //param.Add(new SqlParameter("@PO_NO", SqlDbType.VarChar) { Value = base.IfNull(data.PO_NO) });

            //param.Add(new SqlParameter("@INV_NO", SqlDbType.VarChar) { Value = base.IfNull(data.INV_NO) });
            //param.Add(new SqlParameter("@INV_LINE", SqlDbType.VarChar) { Value = base.IfNull(data.INV_LINE) });
            //param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.TARGET_ASSET_NO) });
            //param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.TARGET_ASSET_SUB) });
            //param.Add(new SqlParameter("@SETTLE_AMT", SqlDbType.Decimal) { Value = base.IfNull(data.SETTLE_AMOUNT) });
            //param.Add(new SqlParameter("@ADJ_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.ADJ_TYPE) });
            //param.Add(new SqlParameter("@ISAVG_COST", SqlDbType.Bit) { Value = isAvgCost ? 1 : 0 });

            //execute("sp_WFD02710_UpdateSettlementAmount", param);
        }


        public void UpdateSettlementSummary(WFD02710AUCModel data, string _User)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_NO) });
            param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_SUB) });

            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.TARGET_ASSET_NO) });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.TARGET_ASSET_SUB) });
            param.Add(new SqlParameter("@SETTLE_AMT", SqlDbType.VarChar) { Value = base.IfNull(data.SETTLE_AMOUNT) });
            param.Add(new SqlParameter("@REMAIN_AMT", SqlDbType.VarChar) { Value = base.IfNull(data.AUC_REMAIN) });
            param.Add(new SqlParameter("@ADJ_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.ADJ_TYPE) });

            execute("sp_WFD02710_UpdateSettlementAmountSummary", param);
        }

        public void Submit(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Submit(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Free function
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02710_SubmitRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@FuncID", SqlDbType.VarChar) { Value = Constants.BatchID.WFD02710 });
                execute("sp_WFD01170_InitialDefaultBOIDocument", param);

               
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.TEMP_DOC_NO) });
                execute("sp_WFD02710_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Reject(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02710_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02710_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Approve(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Approve(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02710_ApproveRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02710_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Acknowledge(WFD01170MainModel _data, string _status)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_data, _status);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02710_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02710_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        //
        public void SaveDraft(WFD0BaseRequestDocModel _data)
        {
            try
            {
                beginTransaction();

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER_BY) });
                execute("sp_WFD02710_SaveDraft", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public WFD02710AUCModel GetDraft(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });


            var datas = executeDataToList<WFD02710AUCModel>("sp_WFD02710_GetDraft", param);

            return datas.FirstOrDefault();


            //sp_WFD02710_GetDraft
        }
        public void DeleteDraft(WFD0BaseRequestDocModel _data)
        {
            try
            {
                beginTransaction();

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER_BY) });
                execute("sp_WFD02710_DeleteDraft", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Resend(WFD01170MainModel _header, List<WFD0BaseRequestDetailModel> _list, string user)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_header, Constants.DocumentStatus.Resend);

                List<SqlParameter> param = new List<SqlParameter>();
                foreach (var item in _list)
                {
                    param.Clear();
                    //param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(item.DOC_NO) });
                    param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(item.ASSET_NO) });
                    param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(item.ASSET_SUB) });
                    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(user) });

                    execute("sp_WFD02710_ResendDetail", param);
                }

                param.Clear();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_header.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(user) });

                execute("sp_WFD02710_ResendHeader", param);

                ////Document No
                ////param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                //param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
                ////param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.SelectedList) });
                //param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER) });

                //execute("sp_WFD02710_Resend", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void UpdateCapDate(string guid, string docNo, string tempAssetNo, string tempSubNo, DateTime? capDate)
        {
            try
            {
                beginTransaction();

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(guid) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(docNo) });
                param.Add(new SqlParameter("@TEMP_ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(tempAssetNo) });
                param.Add(new SqlParameter("@TEMP_SUB_NO", SqlDbType.VarChar) { Value = tempSubNo });//tempSubNo NEVER BE NULL
                param.Add(new SqlParameter("@CAPDATE", SqlDbType.Date) { Value = base.IfNull(capDate) });

                execute("sp_WFD02710_UpdateCapDate", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void CopyToNewSettlement(WFD01170MainModel _data)
        {

            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02710_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02710_ClearAssetList", param);

                param.Clear();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@NEW_GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.NewGuid) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02710_CopyToNewSettlement", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
    }
}
