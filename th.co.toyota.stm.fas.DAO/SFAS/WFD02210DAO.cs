﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.WFD02210;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02210DAO : RequestDetailDAO
    {
        private string RequestType { get { return "C"; } }
        public List<WFD02210SearchModel> GetAssetCIP(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY,this.RequestType) });
            param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
            param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_BY", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD02210SearchModel>("sp_WFD02210_GetAssetCIP", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());
            


            return datas;
        }

        public void ClearAssetCIP(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
          
            execute("sp_WFD02210_ClearAssetCIP", param);

           
        }

        public void InsertAssetCIP(WFD02210Model data)
        {
            try
            {
                for (int i = 0; i < data.ASSET_LIST.Count; i++)
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].ASSET_NO ?? DBNull.Value });
                    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].COST_CODE ?? DBNull.Value });
                    param.Add(new SqlParameter("@COMPLETE_DATE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].COMPLETE_DATE ?? DBNull.Value });
                    param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
                    param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });

                    execute("sp_WFD02210_InsertAssetCIP", param);
                  }
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void ApproveAssetCIP(WFD02210Model data)
        {
            
            try
            {
                var param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
                execute("sp_WFD02210_DeleteAssetFlagY", param);


                for (int i = 0; i < data.ASSET_LIST.Count; i++)
                {
                    param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].ASSET_NO ?? DBNull.Value });
                    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].COST_CODE ?? DBNull.Value });
                    param.Add(new SqlParameter("@COMPLETE_DATE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].COMPLETE_DATE ?? DBNull.Value });
                    param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

                    execute("sp_WFD02210_ApproveAssetCIP", param);
                    
                }
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        public void ApproveCIP(WFD0BaseRequestDocModel _data, WFD02210Model _dataCIP, string _Action, bool _submit)
        {
            /*
            beginTransaction();
            try
            {
                //Approve Request
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
                param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.EMP_CODE ?? DBNull.Value });
                param.Add(new SqlParameter("@DELEGATE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DELEGATE ?? DBNull.Value });
                param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.FA ?? DBNull.Value });
                param.Add(new SqlParameter("@HIGHER", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.HIGHER ?? DBNull.Value });
                param.Add(new SqlParameter("@ACTION", SqlDbType.VarChar) { Value = _Action });
                param.Add(new SqlParameter("@Comment", SqlDbType.VarChar) { Value = _data.CommentInfo.CommentText });
                param.Add(new SqlParameter("@AttachFile", SqlDbType.VarChar) { Value = _data.CommentInfo.AttachFileName });
                param.Add(new SqlParameter("@SubmitFlag", SqlDbType.VarChar) { Value = _submit ? "Y" : "N" });
                execute("sp_WFD01270_ApproveRequest", param);

                //Approve Asset
                param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_dataCIP.DOC_NO ?? GetTempDocNo(_dataCIP.USER_BY, this.RequestType) });
                execute("sp_WFD02210_DeleteAssetFlagY", param);

                for (int i = 0; i < _dataCIP.ASSET_LIST.Count; i++)
                {
                    param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_dataCIP.DOC_NO ?? GetTempDocNo(_dataCIP.USER_BY, this.RequestType) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)_dataCIP.ASSET_LIST[i].ASSET_NO ?? DBNull.Value });
                    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)_dataCIP.ASSET_LIST[i].COST_CODE ?? DBNull.Value });
                    param.Add(new SqlParameter("@COMPLETE_DATE", SqlDbType.VarChar) { Value = (object)_dataCIP.ASSET_LIST[i].COMPLETE_DATE ?? DBNull.Value });
                    param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)_dataCIP.EMP_CODE ?? DBNull.Value });
                    execute("sp_WFD02210_ApproveAssetCIP", param);
                }

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
            */
        }

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        //public void RejectAssetCIP(WFD02210Model data)
        //{

        //    try
        //    {
        //        //Delete Flag Y
        //        var param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
        //        //param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].ASSET_NO ?? DBNull.Value });
        //        //param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].COST_CODE ?? DBNull.Value });
        //        param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

        //        execute("sp_WFD02210_RejectAssetCIP", param);


        //        //for (int i = 0; i < data.ASSET_LIST.Count; i++)
        //        //{
        //        //    var param = new List<SqlParameter>();
        //        //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
        //        //    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].ASSET_NO ?? DBNull.Value });
        //        //    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[i].COST_CODE ?? DBNull.Value });
        //        //    param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

        //        //    execute("sp_WFD02210_RejectAssetCIP", param);

        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        public void RejectCIP(WFD01270MainModel _data, WFD02210Model _dataCIP, string _Action, bool _submit)
        {
            beginTransaction();
            try
            {
                //Reject Request
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
                param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.EMP_CODE ?? DBNull.Value });
                param.Add(new SqlParameter("@DELEGATE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DELEGATE ?? DBNull.Value });
                param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.FA ?? DBNull.Value });
                param.Add(new SqlParameter("@HIGHER", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.HIGHER ?? DBNull.Value });
                param.Add(new SqlParameter("@ACTION", SqlDbType.VarChar) { Value = _Action });
                param.Add(new SqlParameter("@Comment", SqlDbType.VarChar) { Value = _data.CommentInfo.CommentText });
                param.Add(new SqlParameter("@AttachFile", SqlDbType.VarChar) { Value = _data.CommentInfo.AttachFileName });
                param.Add(new SqlParameter("@SubmitFlag", SqlDbType.VarChar) { Value = _submit ? "Y" : "N" });
                execute("sp_WFD01270_ApproveRequest", param);

                //Reject Asset
                //Delete Flag Y
                param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_dataCIP.DOC_NO ?? GetTempDocNo(_dataCIP.USER_BY, this.RequestType) });
                param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)_dataCIP.EMP_CODE ?? DBNull.Value });
                execute("sp_WFD02210_RejectAssetCIP", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void DeleteAssetCIP(WFD02210Model data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            try
            {
                if (data.ASSET_LIST == null || data.ASSET_LIST.Count == 0)
                {
                    return;
                }

                for (int i = 0; i < data.ASSET_LIST.Count; i++)
                {
                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[0].ASSET_NO ?? DBNull.Value });
                    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.ASSET_LIST[0].COST_CODE ?? DBNull.Value });
                    param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
                    param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });

                    execute("sp_WFD02210_DeleteAssetCIP", param);
                }
                
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        
        //
        public void SubmitAssetCIP(WFD02210Model data, string _DocumentList)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
                param.Add(new SqlParameter("@DocNoMatch", SqlDbType.VarChar) { Value = _DocumentList });
                param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });

                execute("sp_WFD02210_SubmitAssetCIP", param);
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<MessageModel> ValidationBeforeApprove(WFD02210Model data)
        {
            List<MessageModel> datas = new List<MessageModel>();
            try
            {
                string _guid = data.GUID;
                if (string.IsNullOrEmpty(_guid))
                {
                    _guid = data.DOC_NO;
                    data.GUID = data.DOC_NO;
                }                    

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _guid });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
                execute("sp_WFD02210_InsertAssetToTempSelected", param);

                return this.CommonAssetValidation(new Models.WFD01270.WFD01270Model {
                     DOC_NO = data.DOC_NO,
                     USER_BY = data.USER_BY,
                     REQUEST_TYPE = this.RequestType,
                     GUID = data.GUID,
                     UPDATE_DATE = data.UPDATE_DATE

                });
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void InsertAssetToTempSelected(WFD02210Model data)
        {
            try
            {
                string _guid = data.GUID;
                if (string.IsNullOrEmpty(_guid))
                {
                    _guid = data.DOC_NO;
                    data.GUID = data.DOC_NO;
                }

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _guid });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
                execute("sp_WFD02210_InsertAssetToTempSelected", param);
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
