﻿using System;
using System.Collections.Generic;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01180;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD01180DAO : RequestDetailDAO
    {
        private string RequestType { get { return "D"; } }

        public List<WFD01180MSystemModel> GetAssetClass(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            var datas = executeDataToList<WFD01180MSystemModel>("sp_WFD01180_GetAssetClass");

            return datas;
        }

        public List<WFD01180MSystemModel> GetMinorCategory(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            var datas = executeDataToList<WFD01180MSystemModel>("sp_WFD01180_GetMinorCategory", param);

            return datas;
        }

        public List<WFD01180MSystemModel> GetCompany(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            var datas = executeDataToList<WFD01180MSystemModel>("sp_WFD01180_GetCompany");

            return datas;
        }

        public List<WFD01180MSystemModel> GetPlateType(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            var datas = executeDataToList<WFD01180MSystemModel>("sp_WFD01180_GetPlateType");

            return datas;
        }

        public List<WFD01180MSystemModel> GetPlateSize(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            var datas = executeDataToList<WFD01180MSystemModel>("sp_WFD01180_GetPlateSize");

            return datas;
        }

        public List<WFD01180MSystemModel> CHKAssetClass(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            var datas = executeDataToList<WFD01180MSystemModel>("sp_WFD01180_CHKAssetClass");

            return datas;
        }

        public List<WFD01180DepreciationModel> GetDepreciation(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });
            var datas = executeDataToList<WFD01180DepreciationModel>("sp_WFD01180_GetDepreciation", param);

            return datas;
        }

        public List<WFD01180DepreciationModel> ChkDepreciation(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });
            var datas = executeDataToList<WFD01180DepreciationModel>("sp_WFD01180_CHKDepreciation", param);

            return datas;
        }

        public void InsDeprecistion(WFD01180DepreciationModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
                param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
                param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });
                param.Add(new SqlParameter("@STICKER_TYPE", SqlDbType.VarChar) { Value = (object)data.STICKER_TYPE ?? DBNull.Value });
                param.Add(new SqlParameter("@STICKER_SIZE", SqlDbType.VarChar) { Value = (object)data.STICKER_SIZE ?? DBNull.Value });
                param.Add(new SqlParameter("@INVENTORY_INDICATOR", SqlDbType.VarChar) { Value = (object)data.INVENTORY_INDICATOR ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_01", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_01", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_01", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_01", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_01", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_01 ?? DBNull.Value });

                param.Add(new SqlParameter("@DEACT_DEPRE_AREA_15", SqlDbType.VarChar) { Value = (object)data.DEACT_DEPRE_AREA_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@DPRE_KEY_15", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_15", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_15", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_15", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_15", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_15 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_31", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_31", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_31", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_31", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_31", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_31 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_41", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_41", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_41", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_41", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_41", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_41 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_81", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_81", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_81", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_81", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_81", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_81 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_91", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_91", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_91", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_91", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_91", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_91 ?? DBNull.Value });

                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });


                execute("sp_WFD01180_InsDeprecistion", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void UpdDeprecistion(WFD01180DepreciationModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
                param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
                param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });
                param.Add(new SqlParameter("@STICKER_TYPE", SqlDbType.VarChar) { Value = (object)data.STICKER_TYPE ?? DBNull.Value });
                param.Add(new SqlParameter("@STICKER_SIZE", SqlDbType.VarChar) { Value = (object)data.STICKER_SIZE ?? DBNull.Value });
                param.Add(new SqlParameter("@INVENTORY_INDICATOR", SqlDbType.VarChar) { Value = (object)data.INVENTORY_INDICATOR ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_01", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_01", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_01", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_01", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_01 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_01", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_01 ?? DBNull.Value });

                param.Add(new SqlParameter("@DEACT_DEPRE_AREA_15", SqlDbType.VarChar) { Value = (object)data.DEACT_DEPRE_AREA_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@DPRE_KEY_15", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_15", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_15", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_15", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_15 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_15", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_15 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_31", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_31", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_31", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_31", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_31 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_31", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_31 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_41", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_41", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_41", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_41", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_41 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_41", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_41 ?? DBNull.Value });


                param.Add(new SqlParameter("@DPRE_KEY_81", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_81", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_81", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_81", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_81 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_81", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_81 ?? DBNull.Value });

                param.Add(new SqlParameter("@DPRE_KEY_91", SqlDbType.VarChar) { Value = (object)data.DPRE_KEY_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_YEAR_91", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_YEAR_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_PERD_91", SqlDbType.Int) { Value = (object)data.USEFUL_LIFE_PERD_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_91", SqlDbType.VarChar) { Value = (object)data.USEFUL_LIFE_TYPE_91 ?? DBNull.Value });
                param.Add(new SqlParameter("@SCRAP_VALUE_91", SqlDbType.Decimal) { Value = (object)data.SCRAP_VALUE_91 ?? DBNull.Value });

                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });


                execute("sp_WFD01180_UpdDeprecistion", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void DelDeprecistion(WFD01180DepreciationModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
                param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
                param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });

                execute("sp_WFD01180_DelDeprecistion", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
