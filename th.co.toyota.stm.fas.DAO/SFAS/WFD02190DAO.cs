﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02190;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02190DAO : Database
    {
        public WFD02190DAO() : base()
        {

        }

        public List<WFD02190Model> Search(WFD02190SearchModel _data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@EMP_CODE", base.IfNull(_data.EMP_CODE)));
            param.Add(new SqlParameter("@ActRole", base.IfNull(_data.ACT_ROLE)));
            param.Add(new SqlParameter("@REQUEST_TYPE", base.IfNull(_data.REQUEST_TYPE)));
            param.Add(new SqlParameter("@COMPANY", base.IfNull(_data.COMPANY)));
            param.Add(new SqlParameter("@ASSET_NO", base.IfNull(_data.ASSET_NO)));
            param.Add(new SqlParameter("@ASSET_SUB", base.IfNull(_data.ASSET_SUB)));
            param.Add(new SqlParameter("@ASSET_NAME", base.IfNull(_data.ASSET_NAME)));
            param.Add(new SqlParameter("@ASSET_CLASS", base.IfNull(_data.ASSET_CLASS)));
            param.Add(new SqlParameter("@ASSET_GROUP", base.IfNull(_data.ASSET_GROUP)));
            param.Add(new SqlParameter("@WBS_PROJECT", base.IfNull(_data.WBS_PROJECT)));
            param.Add(new SqlParameter("@WBS_BUDGET", base.IfNull(_data.WBS_BUDGET)));
            param.Add(new SqlParameter("@ASSET_PLATE_NO", base.IfNull(_data.ASSET_PLATE_NO)));
            param.Add(new SqlParameter("@LOCATION", base.IfNull(_data.LOCATION)));

            param.Add(new SqlParameter("@ONLY_PARENT_ASSETS", base.IfNull(_data.ONLY_PARENT_ASSETS)));
            param.Add(new SqlParameter("@COST_CODE", base.IfNull(_data.COST_CODE)));
            param.Add(new SqlParameter("@RESP_COST_CODE", base.IfNull(_data.RESP_COST_CODE)));
            param.Add(new SqlParameter("@DATE_IN_SERVICE_START", base.IfNull(_data.DATE_IN_SERVICE_START)));
            param.Add(new SqlParameter("@DATE_IN_SERVICE_TO", base.IfNull(_data.DATE_IN_SERVICE_TO)));
            param.Add(new SqlParameter("@INVEST_REASON", base.IfNull(_data.INVEST_REASON)));
            param.Add(new SqlParameter("@SearchOption", base.IfNull(_data.searchOption)));

            param.Add(new SqlParameter("@GUID", base.IfNull(_data.GUID)));
            param.Add(new SqlParameter("@pageNum", base.IfNull(page.CurrentPage)));
            param.Add(new SqlParameter("@pageSize", base.IfNull(page.ItemPerPage)));
            param.Add(new SqlParameter("@sortColumnName", base.IfNull(page.OrderColIndex)));
            param.Add(new SqlParameter("@orderType", base.IfNull(page.OrderType)));

            //param.Add(new SqlParameter("@HasCostValue", base.IfNull(page.OrderType),));
            param.Add(new SqlParameter("@HasCostValue", SqlDbType.Bit) { Value = IfNull(_data.HasCostValue) });

            param.Add(TotalItem);

            var datas = executeDataToList<WFD02190Model>("sp_WFD02190_SearchData", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        public bool SaveAssetsData(WFD02190SaveModel data)
        {
            try
            {
                for (int i = 0; i < data.DETIAL_ASSET.Count; i++)
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.DETIAL_ASSET[i].ASSET_NO ?? DBNull.Value });
                    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.DETIAL_ASSET[i].COST_CODE ?? DBNull.Value });
                    param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });

                    execute("sp_WFD02190_InsertData", param);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool SaveAssetsDataByGUID(WFD02190SaveModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
                param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });

                var datas = executeDataToList<WFD02190SearchModel>("sp_WFD02190_InsertDisposalData", param);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool SaveTempDispAssetsData(WFD02190SaveModel data)
        {
            try
            {
                for (int i = 0; i < data.DETIAL_ASSET.Count; i++)
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.DETIAL_ASSET[i].ASSET_NO ?? DBNull.Value });
                    param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.DETIAL_ASSET[i].COST_CODE ?? DBNull.Value });
                    param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });

                    var datas = executeDataToList<WFD02190SearchModel>("sp_WFD02190_InsertTempDataDisp", param);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public List<MessageModel> CheckParentAndChildDisposal(WFD02190SaveModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });

            return executeDataToList<MessageModel>("sp_WFD02190_CheckParentAndChildDisposal", param);
        }
    }


}
