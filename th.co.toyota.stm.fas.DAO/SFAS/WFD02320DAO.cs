﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD02320;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02320DAO : Database
    {
        public WFD02320DAO() : base()
        {

        }
        public List<WFD02320PrintQModel> Search(WFD02320SearchModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });

            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.NVarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.NVarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD02320PrintQModel>("sp_WFD02320_GetPrintTagQue", param);

            page.Totalitem = TotalItem.Value != DBNull.Value ? int.Parse(TotalItem.Value.ToString()) : 0;

            return datas;
        }

        public WFD02320PrintQDCModel GetPrintDetail(string company, string costcode, string resp_cost_code, string createby, string find, string printall)
        {
            WFD02320PrintQDCModel PrintQDCModel = new WFD02320PrintQDCModel();

            List<SqlParameter> param = new List<SqlParameter>();
            if (!string.IsNullOrEmpty(company))
            {
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)company ?? DBNull.Value });
            }
            if (!string.IsNullOrEmpty(costcode))
            {
                param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)costcode ?? DBNull.Value });
            }
            if (!string.IsNullOrEmpty(resp_cost_code))
            {
                param.Add(new SqlParameter("@RESP_COST_CODE", SqlDbType.VarChar) { Value = (object)resp_cost_code ?? DBNull.Value });
            }

            if (!string.IsNullOrEmpty(createby))
            {
                param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)createby ?? DBNull.Value });
            }
            if(!string.IsNullOrEmpty(find))
            {
              param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)find ?? DBNull.Value });
            }       
            param.Add(new SqlParameter("@PrintAll", SqlDbType.VarChar) { Value = (object)printall ?? DBNull.Value });
            var datas = executeDataToDataSet("sp_WFD02320_GetPrintDetail", param);

            if (datas.Tables[0] != null && datas.Tables[0].Rows.Count > 0)
            {
                PrintQDCModel.printQDialog = datas.Tables[0].ToList<WFD02320PrintQDialogModel>();
            }

            if (datas.Tables[1] != null && datas.Tables[1].Rows.Count > 0)
            {
                PrintQDCModel.printAllCount = datas.Tables[1].ToList<WFD02320PrintAllCountModel>().First(); 
            }
            if (datas.Tables[2] != null && datas.Tables[2].Rows.Count > 0)
            {
                PrintQDCModel.AssetList = datas.Tables[2].ToList<WFD02320PrintQDetailsModel>();
            }

            
            return PrintQDCModel;
        }
        //public List<SystemConfigurationModels> GetPrintChange(string BARCODE_SIZE)
        //{
        //    List<SqlParameter> param = new List<SqlParameter>();
        //    param.Add(new SqlParameter("@barcode", SqlDbType.VarChar) { Value = (object)BARCODE_SIZE ?? DBNull.Value });
        //    var datas = executeDataToList<SystemConfigurationModels>("sp_WFD02320_GetChangePrinter", param);


        //    return datas;
        //}
        //public List<WFD02320PrintAllCountModel> GetPrinAllCount(string BARCODE_SIZE)
        //{
        //    var datas = executeDataToList<WFD02320PrintAllCountModel>("sp_WFD02320_GetChangePrinter");


        //    return datas;
        //}
    }
}
