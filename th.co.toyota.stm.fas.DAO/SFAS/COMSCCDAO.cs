﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.COMSCC;

namespace th.co.toyota.stm.fas.DAO
{
    public class COMSCCDAO : Database
    {
        public COMSCCDAO() : base()
        {

        }
        public COMSCCModel getDefaultCostCode(string EMP_CODE)
        {
            COMSCCModel _valReturn = new COMSCCModel();
            _valReturn.COST_CODE = "";
            _valReturn.COST_NAME = "";

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });

            var _temp = executeDataToList<COMSCCModel>("sp_Common_GetAssetCostCenter", param);
             
            if(_temp!=null & _temp.Count>0)
            {
                if(_temp.Count ==1 )
                {
                    _valReturn = _temp[0];
                }
                else
                {
                    _valReturn.COST_CODE = "";
                    _valReturn.COST_NAME = "";
                }
            }

            return _valReturn;
        }

        public COMSCCModel getDetailCostCode(string COST_CODE)
        {
            COMSCCModel _valReturn = new COMSCCModel();
            _valReturn.COST_CODE = "";
            _valReturn.COST_NAME = "";

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)COST_CODE ?? DBNull.Value });

            var _temp = executeDataToList<COMSCCModel>("sp_Common_GetDetailCostCenter", param);

            if (_temp != null & _temp.Count > 0)
            {
                if (_temp.Count == 1)
                {
                    _valReturn = _temp[0];
                }
                else
                {
                    _valReturn.COST_CODE = "";
                    _valReturn.COST_NAME = "";
                }
            }

            return _valReturn;
        }
        public List<COMSCCModel> SearchFW(COMSCCDefaultScreenModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ALL_FLAG", SqlDbType.VarChar) { Value = (object)data.ALL_COST_CENTER ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_REQUEST", SqlDbType.VarChar) { Value = (object)data.PAGE_REQUEST ?? DBNull.Value });
            //param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            //param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            //param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<COMSCCModel>("sp_COMSCC_SearchData", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        public List<COMSCCModel> SearchFS(COMSCCDefaultScreenModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ALL_FLAG", SqlDbType.VarChar) { Value = (object)data.ALL_COST_CENTER ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_REQUEST", SqlDbType.VarChar) { Value = (object)data.PAGE_REQUEST ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            //param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            //param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            //param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<COMSCCModel>("sp_COMSCC_SearchFASUpData", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
    }
    
}
