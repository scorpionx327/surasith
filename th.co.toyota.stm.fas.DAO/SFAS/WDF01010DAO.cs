﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using System.Configuration;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD01010;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.DAO
{
    public class WDF01010DAO : Database
    {
        public WDF01010DAO() : base()
        {

        }

        public List<WDF01010FunctionModel> GetFunctionDAO()
        {
            return executeDataToList<WDF01010FunctionModel>("sp_WFD01010_GetFunction");
        }

        public List<WFD01010ResultSearchModel> GetSearchLogginDAO(WFD01010DefaultScreenModel Data,ref PaginationModel Page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            param.Add(new SqlParameter("@MODUAL_ID", SqlDbType.VarChar) { Value = (object)Data.SYSTEM_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@BATCH_ID", SqlDbType.VarChar) { Value = (object)Data.FUNCTION_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = (object)Data.STATUS ?? DBNull.Value });
            param.Add(new SqlParameter("@APP_ID", SqlDbType.VarChar) { Value = (object)Data.APP_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)Data.USER_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@START_TIME", SqlDbType.VarChar) { Value = (object)Data.DATE_START ?? DBNull.Value });
            param.Add(new SqlParameter("@END_TIME", SqlDbType.VarChar) { Value = (object)Data.DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@FAVORITE_FLAG", SqlDbType.VarChar) { Value = (object)Data.FAVORITE_FLAG ?? DBNull.Value });

            param.Add(new SqlParameter("@CURRENT_PAGE", SqlDbType.Int) { Value = (object)Page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)Page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)Page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_TYPE", SqlDbType.VarChar) { Value = (object)Page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var result = executeDataToList<WFD01010ResultSearchModel>("sp_WDF01010_SearchData", param);
            Page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return result;
        }

        public List<WDF01010DetailModel> GetDetailLogginDAO(WFD01010DefaultScreenModel Data, ref PaginationModel Page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            param.Add(new SqlParameter("@APP_ID", SqlDbType.Int) { Value = (object)Data.APP_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@LEVEL", SqlDbType.VarChar) { Value = (object)Data.LEVEL ?? DBNull.Value });           
            param.Add(new SqlParameter("@CURRENT_PAGE", SqlDbType.Int) { Value = (object)Page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)Page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)Page.OrderColIndex ?? DBNull.Value });
            param.Add(TotalItem);

            var result = executeDataToList<WDF01010DetailModel>("sp_WDF01010_GetDetails", param);
            Page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return result;
        }
    }
}
