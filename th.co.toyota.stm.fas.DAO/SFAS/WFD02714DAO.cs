﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.SFAS.WFD02714;

namespace th.co.toyota.stm.fas.DAO.SFAS
{
    public class WFD02714DAO : RequestDetailDAO
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void UpdateAUCInfo(List<WFD02714ResponseAUCFromSAPModel> _list)
        {
            try
            {
                beginTransaction();

                WFD02714ResponseAUCFromSAPModel _grp = new WFD02714ResponseAUCFromSAPModel();

                //var _grpList = new List<WFD02714ResponseAUCFromSAPModel>();
                //foreach (var _data in _list)
                //{
                //    if(_grpList.FindAll(x => x.aucNo == _data.aucNo && x.aucSubNo == _data.aucSubNo && 
                //    x.requestNo == _data.requestNo && x.companyCode == _data.companyCode).Count > 0)
                //    {
                //        continue;
                //    }
                //    _grpList.Add(new WFD02714ResponseAUCFromSAPModel()
                //    {
                //         aucNo = _data.aucNo, aucSubNo = _data.aucSubNo,
                //    });
                //}

                foreach (var _data in _list)
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    //if ( _grp.aucNo != _data.aucNo || 
                    //    _grp.aucSubNo != _data.aucSubNo || 
                    //    _grp.requestNo != _data.requestNo ||
                    //    _grp.companyCode != _data.companyCode){

                    //    param.Clear();
                    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.requestNo) });
                    //    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(_data.companyCode) });
                    //    param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.aucNo) });
                    //    param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(_data.aucSubNo) });
                    //    execute("sp_WFD02714_ClearAUCInfo", param);

                    //}
                    
                    _log.Info("UpdateAUCInfo :");
                    _log.Info(_data);
                    param.Clear();
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.requestNo) });
                    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(_data.companyCode) });
                    param.Add(new SqlParameter("@AUC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.aucNo) });
                    param.Add(new SqlParameter("@AUC_SUB", SqlDbType.VarChar) { Value = base.IfNull(_data.aucSubNo) });
                    param.Add(new SqlParameter("@PO_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.poNo) });
                    param.Add(new SqlParameter("@SAP_DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.SAPDocNo) });
                    param.Add(new SqlParameter("@FISCAL_YEAR", SqlDbType.VarChar) { Value = base.IfNull(_data.fiscalYear) });
                    param.Add(new SqlParameter("@INV_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.invoiceNo) });
                    param.Add(new SqlParameter("@INV_ITEMNO", SqlDbType.VarChar) { Value = base.IfNull(_data.invoiceItemNo) });
                    param.Add(new SqlParameter("@INV_DESC", SqlDbType.VarChar) { Value = base.IfNull(_data.invoiceDesc) });
                    param.Add(new SqlParameter("@INV_AMT", SqlDbType.VarChar) { Value = base.IfNull(_data.invoiceAmt) });
                    param.Add(new SqlParameter("@Message", SqlDbType.VarChar) { Value = base.IfNull(_data.returnMessage) });

                    var _rs = executeDataToList<Result>("sp_WFD02714_UpdateAUCInfo", param);
                    if(_rs.Count == 0)
                    {
                        _log.Error("No result");
                    }
                    switch (_rs[0].RESULT)
                    {
                        case "1":
                            _log.Info("Update existing Invoice");
                            break;
                        case "2":
                            _log.Info("Update first Invoice of AUC");
                            break;

                        case "3":
                            _log.Error("No Data update");
                            break;
                        case "4":
                            _log.Info("Add Invoice to AUC");
                            break;

                    }
                }
          
                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }

            //
           
        }
    }
    public class Result
    {
        public string RESULT { get; set; }
    }
}
