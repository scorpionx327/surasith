﻿using System;
using System.Collections.Generic;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02510;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02510DAO : RequestDetailDAO
    {
        private string RequestType { get { return "D"; } }

        public void InitialAssetList(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            execute("sp_WFD02510_InitialAssetList", param);
        }
        public List<WFD02510HeaderModel> GetDataHeader(WFD0BaseRequestDocModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });

                var datas = executeDataToList<WFD02510HeaderModel>("sp_WFD02510_GetDisposeHeader", param);

                return datas ;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        
        public void ClearAssetList(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            
            execute("sp_WFD02510_ClearAssetList", param);

        }
        public List<MessageModel> AssetValidation(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02510_ValidationAsset", param);
        }
        public List<MessageModel> GenerateFlowValidation(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02510_GenerateFlowValidation", param);
        }
        //
        public List<MessageModel> SubmitValidation(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02510_SubmitValidation", param);
        }
        public List<MessageModel> ApproveValidation(WFD0BaseRequestDocModel _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
            return executeDataToList<MessageModel>("sp_WFD02510_ApproveValidation", param);

        }

        public void InsertAsset(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });

                execute("sp_WFD02510_InsertAsset", param);


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });


                execute("sp_WFD02510_DeleteAsset", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
     

        public void PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID }); //Submit mode
            param.Add(new SqlParameter("@ACT_ROLE", SqlDbType.VarChar) { Value = data.ACT_ROLE }); //Submit mode
            

            execute("sp_WFD02510_PrepareGenerateFlow", param);

        }

     
        

       public List<WFD02510AssetModel> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(TotalItem);

            //var datas = executeDataToList<WFD02510AssetModel>("sp_WFD02A00_GetAssetList", param);
            var datas = executeDataToList<WFD02510AssetModel>("sp_WFD02510_GetAssetList", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());



            return datas;
        }

      

        public List<WFD02510AttachDocModel> GetAttachDoc(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            var datas = executeDataToList<WFD02510AttachDocModel>("sp_WFD02510_Get_Attach_Doc");

            return datas;
        }

      
        public void UpdateAsset(WFD02510HeaderModel _header, string _user)
        {
            try
            {
                //Header First 

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(_header.COMPANY) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_header.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@DISP_TYPE", SqlDbType.VarChar) { Value = base.IfNull(_header.DISP_TYPE) });
                param.Add(new SqlParameter("@DISP_MASS_REASON", SqlDbType.VarChar) { Value = base.IfNull(_header.DISP_MASS_REASON) });
                param.Add(new SqlParameter("@DISP_MASS_ATTACH_DOC", SqlDbType.VarChar) { Value = base.IfNull(_header.DISP_MASS_ATTACH_DOC) });
                param.Add(new SqlParameter("@MINUTE_OF_BIDDING", SqlDbType.VarChar) { Value = base.IfNull(_header.MINUTE_OF_BIDDING) });
                param.Add(new SqlParameter("@HIGHER_APP_ATTACH", SqlDbType.VarChar) { Value = base.IfNull(_header.HIGHER_APP_ATTACH) });
                param.Add(new SqlParameter("@ROUTE_TYPE", SqlDbType.VarChar) { Value = base.IfNull(_header.ROUTE_TYPE) });
                param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = base.IfNull(_header.USER_BY) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_header.GUID) });
                param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = base.IfNull(_header.ACT_ROLE) });
                param.Add(new SqlParameter("@CURRENT_APPR_INDX", SqlDbType.Int) { Value = base.IfNull(_header.CURRENT_APPR_INDX) });

                
                execute("sp_WFD02510_UpdateDisposeHeader", param);


                if (_header.DetailList == null)
                    return;


                foreach (var data in _header.DetailList)
                {

                    param = new List<SqlParameter>();
                    param.Clear();

                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                    param.Add(new SqlParameter("@COST", SqlDbType.VarChar) { Value = base.IfNull(data.COST) });
                    param.Add(new SqlParameter("@RETIRE", SqlDbType.VarChar) { Value = base.IfNull(data.RETIRE) });
                    param.Add(new SqlParameter("@QTY", SqlDbType.VarChar) { Value = base.IfNull(data.QTY) });
                    param.Add(new SqlParameter("@DISPOSAL_BY", SqlDbType.VarChar) { Value = base.IfNull(data.DISPOSAL_BY) });
                    param.Add(new SqlParameter("@RETIRE_COST", SqlDbType.VarChar) { Value = base.IfNull(data.RETIRE_COST) });
                    param.Add(new SqlParameter("@BOOK_VALUE", SqlDbType.VarChar) { Value = base.IfNull(data.BOOK_VALUE) });
                    param.Add(new SqlParameter("@RETIRE_BOOK_VALUE", SqlDbType.VarChar) { Value = base.IfNull(data.RETIRE_BOOK_VALUE) });
                    param.Add(new SqlParameter("@ACCUM_DEPREC", SqlDbType.VarChar) { Value = base.IfNull(data.ACCUM_DEPREC) });
                    param.Add(new SqlParameter("@USEFUL_LIFE", SqlDbType.VarChar) { Value = base.IfNull(data.USEFUL_LIFE) });
                    param.Add(new SqlParameter("@DISP_REASON", SqlDbType.VarChar) { Value = base.IfNull(data.DISP_REASON) });
                    param.Add(new SqlParameter("@DISP_REASON_OTHER", SqlDbType.VarChar) { Value = base.IfNull(data.DISP_REASON_OTHER) });
                    param.Add(new SqlParameter("@BOI_NO", SqlDbType.VarChar) { Value = base.IfNull(data.BOI_NO) });
                    param.Add(new SqlParameter("@INVEST_REASON", SqlDbType.VarChar) { Value = base.IfNull(data.INVEST_REASON) });
                    param.Add(new SqlParameter("@STORE_CHECK_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.STORE_CHECK_FLAG) });
                    param.Add(new SqlParameter("@SOLD_TO", SqlDbType.VarChar) { Value = base.IfNull(data.SOLD_TO) });
                    param.Add(new SqlParameter("@PRICE", SqlDbType.VarChar) { Value = base.IfNull(data.PRICE) });
                    param.Add(new SqlParameter("@INVOICE_NO", SqlDbType.VarChar) { Value = base.IfNull(data.INVOICE_NO) });
                    param.Add(new SqlParameter("@DETAIL_DATE", SqlDbType.VarChar) { Value = base.IfNull(data.DETAIL_DATE) });
                    param.Add(new SqlParameter("@DETAIL_REMARK", SqlDbType.VarChar) { Value = base.IfNull(data.DETAIL_REMARK) });
                    param.Add(new SqlParameter("@REMOVAL_COST", SqlDbType.VarChar) { Value = base.IfNull(data.REMOVAL_COST) });
                    param.Add(new SqlParameter("@USER_MISTAKE", SqlDbType.VarChar) { Value = base.IfNull(data.USER_MISTAKE) });
                    param.Add(new SqlParameter("@COMPENSATION", SqlDbType.VarChar) { Value = base.IfNull(data.COMPENSATION) });
                    param.Add(new SqlParameter("@RECEIVE_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.RECEIVE_FLAG) });
                    param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = base.IfNull(data.STATUS) });
                    param.Add(new SqlParameter("@FLOW_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.FLOW_TYPE) });
                    param.Add(new SqlParameter("@DISP_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.DISP_TYPE) });
                    param.Add(new SqlParameter("@DISP_MASS_REASON", SqlDbType.VarChar) { Value = base.IfNull(data.DISP_MASS_REASON) });
                    param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
                    param.Add(new SqlParameter("@ADJ_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.ADJ_TYPE) });
                    param.Add(new SqlParameter("@RETIRE_QTY", SqlDbType.Decimal) { Value = base.IfNull(data.RETIRE_QTY) });
                    param.Add(new SqlParameter("@EDIT_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.EDIT_FLAG) });

                    execute("sp_WFD02510_UpdateAsset", param);
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    

        public void Submit(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Submit(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Free function
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02510_SubmitRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@FuncID", SqlDbType.VarChar) { Value = Constants.BatchID.WFD02510 });
                execute("sp_WFD01170_InitialDefaultBOIDocument", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02510_SubmitwithApproveAsset", param);

                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02510_ClearAssetList", param);



                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Reject(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02510_RejectRequest", param);

                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02510_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void CopyToNewRetirement(WFD01170MainModel _data)
        {

            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02510_RejectRequest", param);

                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02510_ClearAssetList", param);

                param.Clear();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@NEW_GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.NewGuid) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02510_CopyToNewRetirement", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Approve(WFD01170MainModel _data)
        {
            try
            {
               beginTransaction();

                base._Approve(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02510_ApproveRequest", param);

               commitTransaction();
               closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Resend(WFD01170MainModel _header, WFD01170SelectedAssetNoModel _data)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_header, Constants.DocumentStatus.Resend);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
                param.Add(new SqlParameter("@ASSET_LIST", SqlDbType.VarChar) { Value = base.IfNull(_data.SelectedList) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER) });

                execute("sp_WFD02510_Resend", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Acknowledge(WFD01170MainModel _data, string _status)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_data, _status);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02510_RejectRequest", param);

                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02510_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
    }
}
