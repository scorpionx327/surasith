﻿using System;
using System.Collections.Generic;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02310;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02810DAO : RequestDetailDAO
    {
        public void InitialAssetList(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            execute("sp_WFD02810_InitialAssetList", param);
        }

        public List<WFD02810Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            
			param.Add(TotalItem);

            var datas = executeDataToList<WFD02810Model>("sp_WFD02810_GetAssetList", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());



            return datas;
        }

        public DataTable GetOriginalAsset(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToDataTable("sp_WFD02810_GetOriginalCompare", param);
        }
        public DataTable GetTargetAsset(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToDataTable("sp_WFD02810_GetTargetCompare", param);
        }
        public List<WFD02810FieldChangeModel> GetFieldMapping(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            
            return executeDataToList<WFD02810FieldChangeModel>("sp_WFD02810_GetFieldMapping", param);
        }

        //

        public void ClearAssetList(WFD0BaseRequestDocModel data)
        {
            // Allow only Submit 
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            execute("sp_WFD02810_ClearAssetList", param);
        }

        public void DeleteAsset(WFD02810Model data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) }); 
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                
                execute("sp_WFD02810_DeleteAsset", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        public void PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID }); //Submit mode
            param.Add(new SqlParameter("@ACT_ROLE", SqlDbType.VarChar) { Value = data.ACT_ROLE }); //Submit mode
            param.Add(new SqlParameter("@FLOW_TYPE", SqlDbType.VarChar) { Value = data.FLOW_TYPE });


            execute("sp_WFD02810_PrepareGenerateFlow", param);

        }
        public List<MessageModel> CheckValidation(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02810_ValidationAsset", param);
        }
        public void Submit(WFD01170MainModel _data, List<WFD02810FieldChangeModel> _fieldMappingList)
        {
            try
            {
                beginTransaction();

                base._Submit(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Free function
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@FlowType", SqlDbType.VarChar) { Value = _data.RequestHeaderData.FLOW_TYPE });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02810_SubmitRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@FuncID", SqlDbType.VarChar) { Value = Constants.BatchID.WFD02810 });
                execute("sp_WFD01170_InitialDefaultBOIDocument", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02810_SubmitwithApproveAsset", param);


                foreach (var _comment in _fieldMappingList)
                {
                    param.Clear();
                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = _comment.ASSET_NO});
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = _comment.ASSET_SUB });
                    param.Add(new SqlParameter("@COLUMN_NAME", SqlDbType.VarChar) { Value = _comment.DISPLAY_NAME });
                    param.Add(new SqlParameter("@OLD", SqlDbType.VarChar) { Value = _comment.OLD });
                    param.Add(new SqlParameter("@NEW", SqlDbType.VarChar) { Value = _comment.NEW });
                    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                    param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = _comment.LINE_NO });
                    execute("sp_WFD02810_InsertChangeComment", param);
                }
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02810_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void InsertChangeCommentResubmit(WFD01170MainModel _data, List<WFD02810FieldChangeModel> _fieldMappingList)
        {
            try
            {
                beginTransaction();

                List<SqlParameter> param = new List<SqlParameter>();


                foreach (var _comment in _fieldMappingList)
                {
                    param.Clear();
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = _data.RequestHeaderData.DOC_NO });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = _comment.ASSET_NO });
                    param.Add(new SqlParameter("@COLUMN_NAME", SqlDbType.VarChar) { Value = _comment.DISPLAY_NAME });
                    param.Add(new SqlParameter("@OLD", SqlDbType.VarChar) { Value = _comment.OLD });
                    param.Add(new SqlParameter("@NEW", SqlDbType.VarChar) { Value = _comment.NEW });
                    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                    param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = _comment.LINE_NO });
                    execute("sp_WFD02810_InsertChangeCommentResubmit", param);
                }
                //If complete
                param.Clear();

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Reject(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_RejectRequest", param);
				//If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02810_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void CopyToNewInfoChange(WFD01170MainModel _data)
        {

            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02810_ClearAssetList", param);

                param.Clear();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@NEW_GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.NewGuid) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_CopyToNewInfoChange", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Approve(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Approve(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_ApproveRequest", param);
				
				//If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02810_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Resend(WFD01170MainModel _header, WFD01170SelectedAssetNoModel _data)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_header, Constants.DocumentStatus.Resend);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
                param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.SelectedList) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER) });

                execute("sp_WFD02810_Resend", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Acknowledge(WFD01170MainModel _data, string _status)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_data, _status);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02810_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void CopyToNewReclass(WFD01170MainModel _data)
        {

            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02810_ClearAssetList", param);

                param.Clear();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                //param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@NEW_GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.NewGuid) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02810_CopyToNewInfoChange", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

    }
    
}
