﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.SFAS.WFD02650;

namespace th.co.toyota.stm.fas.DAO.SFAS
{
    public class WFD02650DAO : Database
    {
        public WFD02650DAO() : base() { }

        public bool Login(string EMP_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });

            return executeScalar<bool>("sp_WFD02650_Login", param);
        }

        public List<WFD02650_EmployeeModel> GetEmployeeWorkData(string EMP_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });

            return executeDataToList<WFD02650_EmployeeModel>("sp_WFD02650_GetEmployeeWorkData", param);
        }

        public TB_R_STOCK_TAKE_H GetStockTakeHData(string IS_FA_ADMIN)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@IS_FA_ADMIN", SqlDbType.VarChar) { Value = (object)IS_FA_ADMIN ?? DBNull.Value });

            return executeDataToList<TB_R_STOCK_TAKE_H>("sp_WFD02650_GetStockPlanHData", param).First();
        }

        public List<TB_R_STOCK_TAKE_D> GetStockTakeDDatas(string SV_EMP_CODES, string STOCK_TAKE_KEY, string IS_FAADMIN)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@SV_EMP_CODES", SqlDbType.VarChar) { Value = (object)SV_EMP_CODES ?? DBNull.Value });
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@IS_FAADMIN", SqlDbType.VarChar) { Value = (object)IS_FAADMIN ?? DBNull.Value });

            return executeDataToList<TB_R_STOCK_TAKE_D>("sp_WFD02650_GetStockPlanDData", param);
        }

        public List<TB_R_STOCK_TAKE_D_PER_SV> GetStockTakeDPerSVDatas(string SV_EMP_CODES, string STOCK_TAKE_KEY, string IS_FAADMIN)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@SV_EMP_CODES", SqlDbType.VarChar) { Value = (object)SV_EMP_CODES ?? DBNull.Value });
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@IS_FAADMIN", SqlDbType.VarChar) { Value = (object)IS_FAADMIN ?? DBNull.Value });

            return executeDataToList<TB_R_STOCK_TAKE_D_PER_SV>("sp_WFD02650_GetStockPlanDPerSVData", param);
        }

        public void AddStockTakeInvalidScanData(TB_R_STOCK_TAKE_INVALID_CHECKING data, string BARCODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@BARCODE", SqlDbType.VarChar) { Value = (object)BARCODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@SCAN_DATE", SqlDbType.DateTime) { Value = (object)data.SCAN_DATE ?? DBNull.Value });

            execute("sp_WFD02650_AddInvalidStockTakeScan", param);
        }

        public void UpdateStockTakeHPlanStatus(string STOCK_TAKE_KEY, string UPDATE_BY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.NVarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.NVarChar) { Value = (object)UPDATE_BY ?? DBNull.Value });

            execute("sp_WFD02650_UpdateStockTakeHPlanStatus", param);
        }

        public void UpdateStockTakeDScanData(TB_R_STOCK_TAKE_D data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)data.STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@CHECK_STATUS", SqlDbType.VarChar) { Value = (object)data.CHECK_STATUS ?? DBNull.Value });
            param.Add(new SqlParameter("@SCAN_DATE", SqlDbType.DateTime) { Value = (object)data.SCAN_DATE ?? DBNull.Value });
            param.Add(new SqlParameter("@START_COUNT_TIME", SqlDbType.DateTime) { Value = (object)data.START_COUNT_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@END_COUNT_TIME", SqlDbType.DateTime) { Value = (object)data.END_COUNT_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@COUNT_TIME", SqlDbType.Int) { Value = (object)data.COUNT_TIME ?? DBNull.Value });

            execute("sp_WFD02650_UpdateStockTakeDScanData", param);
        }
    }
}
