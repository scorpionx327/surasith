﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02115;
using th.co.toyota.stm.fas.Models.WFD01180;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02115DAO : Database
    {
        public WFD02115DAO() : base()
        {

        }

        public List<WBSMasterAutoCompleteModels> GetWBSBudgetAUCAutoComplete(string company, string empcode, string isFilterByFiscalYear)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@Company", base.IfNull(company)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@IsFilterByFiscalYear", base.IfNull(isFilterByFiscalYear)));

            return executeDataToList<WBSMasterAutoCompleteModels>("sp_WFD02115_GetWBSAUCBudget_AutoComplete", param);
        }
        public List<WBSMasterAutoCompleteModels> GetWBSBudgetRMAAutoComplete(string company,  string empcode, string isFilterByFiscalYear)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@Company", base.IfNull(company)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@IsFilterByFiscalYear", base.IfNull(isFilterByFiscalYear)));

            return executeDataToList<WBSMasterAutoCompleteModels>("sp_WFD02115_GetWBSRMABudget_AutoComplete", param);
        }
        public List<WBSMasterAutoCompleteModels> GetWBSProjectAutoComplete(string company,  string empcode, string flowType, string wbsBudget)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@Company", base.IfNull(company)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@FlowType", base.IfNull(flowType)));
            param.Add(new SqlParameter("@WBSBudget", base.IfNull(wbsBudget)));
            return executeDataToList<WBSMasterAutoCompleteModels>("sp_WFD02115_GetWBSProject_AutoComplete", param);
        }

        public void SaveAssetsData(WFD02115SaveModel data)
        {
            try
            {
                
                List<SqlParameter> param = new List<SqlParameter>();


                param.Add(new SqlParameter("@COMPANY", base.IfNull(data.COMPANY)));

                param.Add(new SqlParameter("@DOC_NO", base.IfNull(data.DOC_NO )));
                param.Add(new SqlParameter("@LINE_NO", base.IfNull(data.LINE_NO )));
                param.Add(new SqlParameter("@PARENT_ASSET_NO", base.IfNull(data.PARENT_ASSET_NO )));
                param.Add(new SqlParameter("@ASSET_MAINSUB", base.IfNull(data.ASSET_MAINSUB )));
                param.Add(new SqlParameter("@INDICATOR_VALIDATE", base.IfNull(data.INDICATOR_VALIDATE )));
                param.Add(new SqlParameter("@ASSET_CLASS", base.IfNull(data.ASSET_CLASS )));
                param.Add(new SqlParameter("@ASSET_NO", base.IfNull(data.ASSET_NO )));
                param.Add(new SqlParameter("@ASSET_SUB", base.IfNull(data.ASSET_SUB )));
                param.Add(new SqlParameter("@ASSET_DESC", base.IfNull(data.ASSET_DESC )));
                param.Add(new SqlParameter("@ADTL_ASSET_DESC", base.IfNull(data.ADTL_ASSET_DESC )));
                param.Add(new SqlParameter("@SERIAL_NO", base.IfNull(data.SERIAL_NO )));
                param.Add(new SqlParameter("@INVEN_NO", base.IfNull(data.INVEN_NO )));
                param.Add(new SqlParameter("@BASE_UOM", base.IfNull(data.BASE_UOM )));
                param.Add(new SqlParameter("@LAST_INVEN_DATE", base.IfNull(data.LAST_INVEN_DATE )));
                param.Add(new SqlParameter("@INVEN_INDICATOR", base.IfNull(data.INVEN_INDICATOR )));
                param.Add(new SqlParameter("@INVEN_NOTE", base.IfNull(data.INVEN_NOTE )));
                param.Add(new SqlParameter("@COST_CODE", base.IfNull(data.COST_CODE )));
                param.Add(new SqlParameter("@RESP_COST_CODE", base.IfNull(data.RESP_COST_CODE )));
                param.Add(new SqlParameter("@LOCATION", base.IfNull(data.LOCATION )));
                param.Add(new SqlParameter("@ROOM", base.IfNull(data.ROOM )));
                param.Add(new SqlParameter("@LICENSE_PLATE", base.IfNull(data.LICENSE_PLATE )));
                param.Add(new SqlParameter("@WBS_PROJECT", base.IfNull(data.WBS_PROJECT )));
                param.Add(new SqlParameter("@INVEST_REASON", base.IfNull(data.INVEST_REASON )));
                param.Add(new SqlParameter("@MINOR_CATEGORY", base.IfNull(data.MINOR_CATEGORY )));
                param.Add(new SqlParameter("@ASSET_STATUS", base.IfNull(data.ASSET_STATUS )));
                param.Add(new SqlParameter("@MACHINE_LICENSE", base.IfNull(data.MACHINE_LICENSE )));
                param.Add(new SqlParameter("@BOI_NO", base.IfNull(data.BOI_NO )));
                param.Add(new SqlParameter("@FINAL_ASSET_CLASS", base.IfNull(data.FINAL_ASSET_CLASS )));
                param.Add(new SqlParameter("@ASSET_SUPPER_NO", base.IfNull(data.ASSET_SUPPER_NO)));

                param.Add(new SqlParameter("@MANUFACT_ASSET", base.IfNull(data.MANUFACT_ASSET)));
                param.Add(new SqlParameter("@LOCATION_REMARK", base.IfNull(data.LOCATION_REMARK)));
                param.Add(new SqlParameter("@ASSET_TYPE_NAME", base.IfNull(data.ASSET_TYPE_NAME )));
                param.Add(new SqlParameter("@WBS_BUDGET", base.IfNull(data.WBS_BUDGET )));
                param.Add(new SqlParameter("@BUDGET", base.IfNull(data.BUDGET)));


                param.Add(new SqlParameter("@PLATE_TYPE", base.IfNull(data.PLATE_TYPE )));
                param.Add(new SqlParameter("@BARCODE_SIZE", base.IfNull(data.BARCODE_SIZE )));
                param.Add(new SqlParameter("@LEASE_AGREE_DATE", base.IfNull(data.LEASE_AGREE_DATE )));
                param.Add(new SqlParameter("@LEASE_START_DATE", base.IfNull(data.LEASE_START_DATE )));
                param.Add(new SqlParameter("@LEASE_LENGTH_YEAR", base.IfNull(data.LEASE_LENGTH_YEAR )));
                param.Add(new SqlParameter("@LEASE_LENGTH_PERD", base.IfNull(data.LEASE_LENGTH_PERD )));
                param.Add(new SqlParameter("@LEASE_TYPE", base.IfNull(data.LEASE_TYPE )));
                param.Add(new SqlParameter("@LEASE_SUPPLEMENT", base.IfNull(data.LEASE_SUPPLEMENT )));
                param.Add(new SqlParameter("@LEASE_NUM_PAYMENT", base.IfNull(data.LEASE_NUM_PAYMENT )));
                param.Add(new SqlParameter("@LEASE_PAY_CYCLE", base.IfNull(data.LEASE_PAY_CYCLE )));
                param.Add(new SqlParameter("@LEASE_ADV_PAYMENT", base.IfNull(data.LEASE_ADV_PAYMENT )));
                param.Add(new SqlParameter("@LEASE_PAYMENT", base.IfNull(data.LEASE_PAYMENT )));
                param.Add(new SqlParameter("@LEASE_ANU_INT_RATE", base.IfNull(data.LEASE_ANU_INT_RATE )));

                param.Add(new SqlParameter("@DPRE_AREA_01", base.IfNull(data.DPRE_AREA_01 )));
                param.Add(new SqlParameter("@DPRE_KEY_01", base.IfNull(data.DPRE_KEY_01 )));
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_01", base.IfNull(getUsefulLifeTypeCode(data.USEFUL_LIFE_TYPE_01))));
                param.Add(new SqlParameter("@PLAN_LIFE_YEAR_01", base.IfNull(data.PLAN_LIFE_YEAR_01 )));
                param.Add(new SqlParameter("@PLAN_LIFE_PERD_01", base.IfNull(data.PLAN_LIFE_PERD_01 )));
                param.Add(new SqlParameter("@SCRAP_VALUE_01", base.IfNull(data.SCRAP_VALUE_01 )));

                param.Add(new SqlParameter("@DEACT_DEPRE_AREA_15", base.IfNull(data.DEACT_DEPRE_AREA_15)));
                param.Add(new SqlParameter("@DPRE_AREA_15", base.IfNull(data.DPRE_AREA_15 )));
                param.Add(new SqlParameter("@DPRE_KEY_15", base.IfNull(data.DPRE_KEY_15 )));
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_15", base.IfNull(getUsefulLifeTypeCode(data.USEFUL_LIFE_TYPE_15))));
                param.Add(new SqlParameter("@PLAN_LIFE_YEAR_15", base.IfNull(data.PLAN_LIFE_YEAR_15 )));
                param.Add(new SqlParameter("@PLAN_LIFE_PERD_15", base.IfNull(data.PLAN_LIFE_PERD_15 )));
                param.Add(new SqlParameter("@SCRAP_VALUE_15", base.IfNull(data.SCRAP_VALUE_15 )));


                param.Add(new SqlParameter("@DPRE_AREA_31", base.IfNull(data.DPRE_AREA_31 )));
                param.Add(new SqlParameter("@DPRE_KEY_31", base.IfNull(data.DPRE_KEY_31 )));
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_31", base.IfNull(getUsefulLifeTypeCode(data.USEFUL_LIFE_TYPE_31))));
                param.Add(new SqlParameter("@PLAN_LIFE_YEAR_31", base.IfNull(data.PLAN_LIFE_YEAR_31 )));
                param.Add(new SqlParameter("@PLAN_LIFE_PERD_31", base.IfNull(data.PLAN_LIFE_PERD_31 )));
                param.Add(new SqlParameter("@SCRAP_VALUE_31", base.IfNull(data.SCRAP_VALUE_31 )));


                param.Add(new SqlParameter("@DPRE_AREA_41", base.IfNull(data.DPRE_AREA_41 )));
                param.Add(new SqlParameter("@DPRE_KEY_41", base.IfNull(data.DPRE_KEY_41 )));
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_41", base.IfNull(getUsefulLifeTypeCode(data.USEFUL_LIFE_TYPE_41))));
                param.Add(new SqlParameter("@PLAN_LIFE_YEAR_41", base.IfNull(data.PLAN_LIFE_YEAR_41 )));
                param.Add(new SqlParameter("@PLAN_LIFE_PERD_41", base.IfNull(data.PLAN_LIFE_PERD_41 )));
                param.Add(new SqlParameter("@SCRAP_VALUE_41", base.IfNull(data.SCRAP_VALUE_41 )));


                param.Add(new SqlParameter("@DPRE_AREA_81", base.IfNull(data.DPRE_AREA_81 )));
                param.Add(new SqlParameter("@DPRE_KEY_81", base.IfNull(data.DPRE_KEY_81 )));
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_81", base.IfNull(getUsefulLifeTypeCode(data.USEFUL_LIFE_TYPE_81))));
                param.Add(new SqlParameter("@PLAN_LIFE_YEAR_81", base.IfNull(data.PLAN_LIFE_YEAR_81 )));
                param.Add(new SqlParameter("@PLAN_LIFE_PERD_81", base.IfNull(data.PLAN_LIFE_PERD_81 )));
                param.Add(new SqlParameter("@SCRAP_VALUE_81", base.IfNull(data.SCRAP_VALUE_81 )));


                param.Add(new SqlParameter("@DPRE_AREA_91", base.IfNull(data.DPRE_AREA_91 )));
                param.Add(new SqlParameter("@DPRE_KEY_91", base.IfNull(data.DPRE_KEY_91 )));
                param.Add(new SqlParameter("@USEFUL_LIFE_TYPE_91", base.IfNull(getUsefulLifeTypeCode(data.USEFUL_LIFE_TYPE_91))));
                param.Add(new SqlParameter("@PLAN_LIFE_YEAR_91", base.IfNull(data.PLAN_LIFE_YEAR_91 )));
                param.Add(new SqlParameter("@PLAN_LIFE_PERD_91", base.IfNull(data.PLAN_LIFE_PERD_91 )));
                param.Add(new SqlParameter("@SCRAP_VALUE_91", base.IfNull(data.SCRAP_VALUE_91 )));


                param.Add(new SqlParameter("@GUID", base.IfNull(data.GUID )));
                param.Add(new SqlParameter("@User", base.IfNull(data.CREATE_BY )));
                param.Add(new SqlParameter("@Cnt", base.IfNull(data.Cnt )));

                execute("sp_WFD02115_SaveData", param);
             
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void SaveAssetsReclassData(WFD02115SaveModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@ASSET_NO", base.IfNull(data.ASSET_NO)));
                param.Add(new SqlParameter("@ASSET_SUB", base.IfNull(data.ASSET_SUB)));
                param.Add(new SqlParameter("@ASSET_DESC", base.IfNull(data.ASSET_DESC)));
                param.Add(new SqlParameter("@ASSET_CLASS", base.IfNull(data.ASSET_CLASS)));

                param.Add(new SqlParameter("@GUID", base.IfNull(data.GUID)));
                param.Add(new SqlParameter("@LINE_NO", base.IfNull(data.LINE_NO)));
                param.Add(new SqlParameter("@User", base.IfNull(data.CREATE_BY)));

                execute("sp_WFD02115_SaveDataReclass", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public WFD02115SaveModel GetAssetsData(WFD02115SaveModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = (object)data.LINE_NO ?? DBNull.Value });

            var _rs = executeDataToList<WFD02115SaveModel>("sp_WFD02115_GetData", param);

            if (_rs == null || _rs.Count == 0)
                return null;
            
            return _rs.First() ;
        }

        public string GetDocumentStatus(WFD02115SaveModel data)
        {
            string ret = "";
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = (object)data.LINE_NO ?? DBNull.Value });
            return executeScalar<string>("sp_WFD02115_GetDocumentStatus", param);
        }


        public WFD02115SaveModel GetWBSData(WFD02115WBSModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@WBS_CODE", SqlDbType.VarChar) { Value = (object)data.WBS ?? DBNull.Value });
            param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = (object)data.COMPANY_CODE ?? DBNull.Value });
            
                       var _rs = executeDataToList<WFD02115SaveModel>("sp_WFD02115_GetWBSData", param);

            if (_rs == null || _rs.Count == 0)
                return null;

            return _rs.First();
        }
        public WFD02115SaveModel GetFixedAssetMaster(WFD02115SaveModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });

            var _rs = executeDataToList<WFD02115SaveModel>("sp_WFD02115_GetFixedAssetMaster", param);

            if (_rs == null || _rs.Count == 0)
                return null;

            return _rs.First();
        }

        public WFD02115SaveModel GetOriginalAssetsData(WFD02115SaveModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = (object)data.LINE_NO ?? DBNull.Value });

            var _rs = executeDataToList<WFD02115SaveModel>("sp_WFD02115_GetOriginalData", param);

            if (_rs == null || _rs.Count == 0)
                return null;

            return _rs.First();
        }

        public string GetLastLineNo(WFD02115SaveModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            var _rs = executeScalar<string>("sp_WFD02115_GetLastNoFromAssetTemp", param);
            return _rs;

        }

        //
        public WFD02115WBSModel GetDeriveWBSBudget(WFD02115WBSModel _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY_CODE", SqlDbType.VarChar) { Value = (object)_data.COMPANY_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@WBS", SqlDbType.VarChar) { Value = (object)_data.WBS ?? DBNull.Value });

            return executeDataToList<WFD02115WBSModel>("sp_WFD02115_GetDerivedWBSInfo", param).FirstOrDefault();
         
        }

        public TB_M_DEPRECIATION GetDefaultDepreciation(TB_M_DEPRECIATION _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)_data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)_data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)_data.MINOR_CATEGORY ?? DBNull.Value });
            return executeDataToList<TB_M_DEPRECIATION>("sp_WFD02115_GetDepreciation", param).FirstOrDefault();

        }

        public List<WFD02115DepreciationModel> GetDepreciationAutocomplete(string company)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@Company", base.IfNull(company)));

            return executeDataToList<WFD02115DepreciationModel>("sp_WFD02115_GetDepreciationAutocomplete", param);
        }

        private string getUsefulLifeTypeCode(string val)
        {
            string ret = "";
            if ("N/A".Equals(val))
            {
                ret = "NA";
            }
            else if ("Base on Contract".Equals(val))
            {
                ret = "BC";
            }
            else if ("Year/Month".Equals(val))
            {
                ret = "VL";
            }
            else
            {
                ret = "";
            }
            return ret;
        }
    }


}
