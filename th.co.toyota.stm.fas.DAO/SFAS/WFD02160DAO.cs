﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02160;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02160DAO : Database
    {
        public WFD02160DAO() : base()
        {

        }

        public void UpdateMapLayout(WFD02160MapLocConditionModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@MAP_PATH", SqlDbType.VarChar) { Value = (object)data.MAP_PATH ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });

           execute("sp_WFD02160_UpdateMapLocation", param);
        }


        public void DeleteMapLayout(WFD02160MapLocConditionModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });

            execute("sp_WFD02160_DeleteMapLayout", param);
        }


        public void UpdateMapLayout(WFD02160AssetModel data)
        {
                int? x_point=null;
                int? y_point = null;
                int? degree = null;     //Pitiphat CR-B-011 201709


            if (!string.IsNullOrEmpty(data.X_POINT))
            {
                data.X_POINT = data.X_POINT.Replace("px", "");
                x_point =(int)double.Parse(data.X_POINT);
            }

            if (!string.IsNullOrEmpty(data.Y_POINT))
            {
                data.Y_POINT = data.Y_POINT.Replace("px", "");
                y_point = (int)double.Parse(data.Y_POINT);
            }

            //Pitiphat CR-B-011 201709
            if (!string.IsNullOrEmpty(data.DEGREE))
            {
                if (data.DEGREE.Trim() == "270")
                {
                    degree = 90;
                }
                else
                {
                    degree = (int)double.Parse(data.DEGREE);
                }
            }

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@POINT_X", SqlDbType.Int) { Value = (object)x_point ?? DBNull.Value });
            param.Add(new SqlParameter("@POINT_Y", SqlDbType.Int) { Value = (object)y_point ?? DBNull.Value });
            param.Add(new SqlParameter("@DEGREE", SqlDbType.Int) { Value = (object)degree ?? DBNull.Value });                 //Pitiphat CR-B-011 201709
            param.Add(new SqlParameter("@IS_MAPPING", SqlDbType.VarChar) { Value = (object)data.SELECTED ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@SEQ", SqlDbType.VarChar) { Value = (object)data.SEQ ?? DBNull.Value });

            execute("sp_WFD02160_UpdateMapLayOut", param);
        }

        public bool SaveMapLayout(List<WFD02160AssetModel> data)
        {
            beginTransaction();
            try
            {
                foreach (var _tdata in data)  // Loop add print tag
                {
                    UpdateMapLayout(_tdata);
                }

                commitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw ex;
            }
        }



        public WFD02160MapLocModel ViewMapLocation(WFD02160MapLocConditionModel data)
        {
            WFD02160MapLocModel _return = null;
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            


            var _returnList = executeDataToList<WFD02160MapLocModel>("sp_WFD02160_ViewMapLocation", param);
               if(_returnList.Count>0)
                 {
                    _return = _returnList.First();
                    }
            return _return;
        }


        public List<WFD02160AssetLoc> GetAssetLoc(WFD02160MapLocConditionModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
              var ListAssetNo = executeDataToList<WFD02160AssetLoc>("sp_WFD02160_ViewAssetLoc", param).ToList();
              return ListAssetNo;
            }
        public List<SystemModel> GetMinorCategory(WFD02160MapLocConditionModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_LFD02160_GetMinorCategory", param);//sp_WFD02160_GetMinorCategory
        }


        public List<WFD02160AssetModel> ViewMapPoint(WFD02160MapLocConditionModel data, bool _AdminFlag)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = _AdminFlag ? "Y" : "N" });
            return executeDataToList<WFD02160AssetModel>("sp_WFD02160_ViewMapPoint", param);
        }
    }
}
