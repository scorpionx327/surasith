﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD02120;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02120DAO : Database
    {
        public WFD02120DAO() : base()
        {

        }

        public List<WFD02120FixedAssetResultModel> GetFixedAssetAutoList(WFD02120SearhConditionModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            ////Search condition model
            param.Add(new SqlParameter("@EMP_CODE", base.IfNull(data.UserId)));
            param.Add(new SqlParameter("@COMPANY", base.IfNull(data.Company)));
            param.Add(new SqlParameter("@SEARCH_MODE", base.IfNull(data.SearchMode)));

            //Pagging
            param.Add(new SqlParameter("@CURRENCT_PAGE", base.IfNull(page.CurrentPage)));
            param.Add(new SqlParameter("@PAGE_SIZE", base.IfNull(page.ItemPerPage)));
            param.Add(new SqlParameter("@ORDER_BY", base.IfNull(page.OrderColIndex)));
            param.Add(new SqlParameter("@orderType", base.IfNull(page.OrderType)));
            //param.Add(new SqlParameter("@TOTAL_ITEM", SqlDbType.Int) { Direction = ParameterDirection.Output });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD02120FixedAssetResultModel>("sp_WFD02120_GetFixedAssetAuto", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        public List<WFD02120FixedAssetResultModel> GetFixedAssetList(WFD02120SearhConditionModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };


            ////Search condition model
            param.Add(new SqlParameter("@EMP_CODE", base.IfNull(data.UserId)));
            param.Add(new SqlParameter("@COMPANY", base.IfNull(data.Company)));
            param.Add(new SqlParameter("@ASSET_NO", base.IfNull(data.AssetNo)));
            param.Add(new SqlParameter("@ASSET_SUB", base.IfNull(data.AssetSub)));
            param.Add(new SqlParameter("@ASSET_NAME", base.IfNull(data.AssetName)));



            param.Add(new SqlParameter("@ASSET_CLASS", base.IfNull(data.AssetClass)));
            param.Add(new SqlParameter("@ASSET_GROUP", base.IfNull(data.AssetGroup)));
            param.Add(new SqlParameter("@COST_CODE", base.IfNull(data.CostCenterCode)));
            param.Add(new SqlParameter("@RESP_COST_CODE", base.IfNull(data.ResponsibleCostCenter)));
            param.Add(new SqlParameter("@LOCATION", base.IfNull(data.Location)));
            param.Add(new SqlParameter("@WBS_PROJECT", base.IfNull(data.WBSProject)));
            param.Add(new SqlParameter("@WBS_BUDGET", base.IfNull(data.WBSBudget)));
            param.Add(new SqlParameter("@BOI", base.IfNull(data.BOI)));
            param.Add(new SqlParameter("@CAPITAL_DATE_FROM", base.IfNull(data.CapitalizeDateFrom)));
            param.Add(new SqlParameter("@CAPITAL_DATE_TO", base.IfNull(data.CapitalizeDateTo)));
            param.Add(new SqlParameter("@ASSET_PLATE_NO", base.IfNull(data.AssetPlateNo)));
            param.Add(new SqlParameter("@ASSET_STATUS", base.IfNull(data.AssetStatus)));
            param.Add(new SqlParameter("@PRINT_STATUS", base.IfNull(data.PrintStatus)));
            param.Add(new SqlParameter("@PHOTO_STATUS", base.IfNull(data.PhotoStatus)));
            param.Add(new SqlParameter("@MAP_STATUS", base.IfNull(data.MapStatus)));
            param.Add(new SqlParameter("@COST_PENDING", base.IfNull(data.CostPending)));

            //Pagging
            param.Add(new SqlParameter("@CURRENCT_PAGE", base.IfNull(page.CurrentPage)));
            param.Add(new SqlParameter("@PAGE_SIZE", base.IfNull(page.ItemPerPage)));
            param.Add(new SqlParameter("@ORDER_BY", base.IfNull(page.OrderColIndex)));
            param.Add(new SqlParameter("@orderType", base.IfNull(page.OrderType)));
            //param.Add(new SqlParameter("@TOTAL_ITEM", SqlDbType.Int) { Direction = ParameterDirection.Output });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD02120FixedAssetResultModel>("sp_WFD02120_GetFixedAsset", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        public void ExecAddPrintTag(List<WFD02120FixedAssetResultModel> data)
        {
            beginTransaction();
            try
            {
                foreach (WFD02120FixedAssetResultModel _tdata in data)  // Loop add print tag
                {
                    AddPrintTag(_tdata);
                }
                commitTransaction();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        private void AddPrintTag(WFD02120FixedAssetResultModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });

            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

            param.Add(new SqlParameter("@PRINT_LOCATION", SqlDbType.VarChar) { Value = (object)data.PRINT_LOCATION ?? DBNull.Value });
            // Add param Print Status for checking assets in new condition modified by Pawares M. 20180706
            param.Add(new SqlParameter("@PRINT_STATUS", SqlDbType.Char) { Value = (object)data.PRINT_STATUS ?? DBNull.Value });
            execute("sp_WFD02120_AddPrintTag", param);
        }

        public int ExecAddPrintReportRow(List<WFD02120FixedAssetResultModel> data, string userId)
        {
            beginTransaction();
            try
            {
                FixAssetReportModel apl = executeDataToList<FixAssetReportModel>("sp_WFD2120_GetNewAPL_ID").First();

                foreach (WFD02120FixedAssetResultModel _tdata in data)  // Loop add print tag
                {
                    addPrintReportByRow(_tdata, userId, apl.APL_ID);
                }
                commitTransaction();
                return apl.APL_ID;
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
            return 0;
        }

        private void addPrintReportByRow(WFD02120FixedAssetResultModel data, string userId, int aplId)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APL_ID", SqlDbType.VarChar) { Value = (object)aplId ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CENTER", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

            execute("sp_Common_InsertFixedAssetReport", param);
        }

    }

    public class FixAssetReportModel
    {
        public int APL_ID { get; set; }
        public string ASSET_NO { get; set; }
        public string COST_CENTER { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
    }

}
