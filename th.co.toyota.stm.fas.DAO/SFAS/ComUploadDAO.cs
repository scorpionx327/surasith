﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.DAO 
{
    public class ComUploadDAO : Database
    {
        public void InsertFileAttchDoc(FileDownloadModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                param.Add(new SqlParameter("@DOC_UPLOAD", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_UPLOAD) });                
                param.Add(new SqlParameter("@FILE_NAME", SqlDbType.VarChar) { Value = base.IfNull(data.FILE_NAME) });
                param.Add(new SqlParameter("@FILE_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.FILE_TYPE) });
                param.Add(new SqlParameter("@FILE_NAME_ORIGINAL", SqlDbType.VarChar) { Value = base.IfNull(data.FILE_NAME_ORIGINAL) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
                param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = data.LINE_NO == "null" ? "1" : base.IfNull(data.LINE_NO) });
                execute("sp_ComUpload_InsertFileAttchDoc", param);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void DeleteFileAttchDoc(FileDownloadModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                //param.Add(new SqlParameter("@TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.TYPE) });
                param.Add(new SqlParameter("@DOC_UPLOAD", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_UPLOAD) });
                param.Add(new SqlParameter("@FILE_NAME", SqlDbType.VarChar) { Value = base.IfNull(data.FILE_NAME) });
                param.Add(new SqlParameter("@FILE_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.FILE_TYPE) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
                param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = base.IfNull(data.LINE_NO) });
                execute("sp_ComUpload_DeleteFileAttchDoc", param);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<FileDownloadModel> GetFileAttchDoc(FileDownloadModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@FUNCTION_ID", SqlDbType.VarChar) { Value = base.IfNull(data.FUNCTION_ID) });
            param.Add(new SqlParameter("@TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.TYPE) });
            param.Add(new SqlParameter("@DOC_UPLOAD", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_UPLOAD) });
            param.Add(new SqlParameter("@FIRST_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.FIRST_FLAG) });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = base.IfNull(data.UPDATE_DATE) });
            param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = base.IfNull(data.LINE_NO) });
            var datas = executeDataToList<FileDownloadModel>("sp_ComUpload_GetFileAttchDoc", param);

            return datas;
        }
    }
}
