﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.SFAS.WFD01150;

namespace th.co.toyota.stm.fas.DAO.SFAS
{
    public class WFD01150DAO : RequestDetailDAO
    {
        public WFD01150DAO() : base() { }


        public WFD01150SearchApproveModel SearchApproveByRequestType(string COMPANY, string REQUEST_FLOW_TYPE, ref PaginationModel pagin)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter totalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.VarChar) { Value = (object)REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)pagin.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)pagin.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)pagin.OrderColIndex ?? DBNull.Value });
            param.Add(totalItem);

            DataSet ds = executeDataToDataSet("sp_WFD01150_SearchApproveByRequestType", param);

            pagin.Totalitem = (int)totalItem.Value;

            WFD01150SearchApproveModel model = new WFD01150SearchApproveModel();
            model.Rows = genSearchApproveRows(ds, COMPANY);
            model.FlowMasterDatas = genFlowApproveMasterDatas(ds);

            return model;
        }
        private List<WFD01150SearchApproveRowModel> genSearchApproveRows(DataSet ds,string _Company)
        {
            List<WFD01150SearchApproveRowModel> datas = new List<WFD01150SearchApproveRowModel>();
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                datas.Add(new WFD01150SearchApproveRowModel()
                {
                    FIXED_ASSET_CATEGORY = r["FIXED_ASSET_CATEGORY"].ToString(),
                    APPRV_ID = int.Parse(r["APPRV_ID"].ToString()),
                    NO = int.Parse(r["NO"].ToString()),
                    REQUEST_TYPE = r["REQUEST_TYPE"].ToString(),
                    ASSET_CLASS = string.Format("{0}", r["ASSET_CLASS"]),
                    UPDATE_BY = string.Format("{0}", r["UPDATE_BY"]),
                    UPDATE_DATE = string.Format("{0}", r["UPDATE_DATE"]),
                    COMPANY = string.Format("{0}", _Company),
                    Flows = genSearchApproveFlowDatas(ds, int.Parse(r["APPRV_ID"].ToString()))
                });
            }

            return datas;
        }
        private List<WFD01150SearchApproveFlowModel> genSearchApproveFlowDatas(DataSet ds, int APPRV_ID)
        {
            var rows = (from r in ds.Tables[1].AsEnumerable()
                        where r.Field<int>("APPRV_ID") == APPRV_ID
                        select r);

            List<WFD01150SearchApproveFlowModel> datas = new List<WFD01150SearchApproveFlowModel>();
            foreach (DataRow r in rows)
            {
                datas.Add(new WFD01150SearchApproveFlowModel()
                {
                    INDX = int.Parse(r["INDX"].ToString()),
                    NO = int.Parse(r["NO"].ToString()),
                    ROLE_CODE = r["ROLE_CODE"].ToString(),
                    ROLE_NAME = r["ROLE_NAME"].ToString(),
                    APPRV_ID = int.Parse(r["APPRV_ID"].ToString())
                });
            }

            return datas;
        }
        private List<WFD01150FlowMasterModel> genFlowApproveMasterDatas(DataSet ds)
        {
            List<WFD01150FlowMasterModel> datas = new List<WFD01150FlowMasterModel>();

            foreach (DataRow r in ds.Tables[2].Rows)
            {
                datas.Add(new WFD01150FlowMasterModel()
                {
                    CODE = int.Parse(r["CODE"].ToString()),
                    REQUEST_TYPE = r["REQUEST_TYPE"].ToString()
                });
            }

            return datas;
        }

        public List<SystemModel> GetApproveRoleList()
        {
            List<SqlParameter> param = new List<SqlParameter>();

            return executeDataToList<SystemModel>("sp_WFD01150_GetApproveRoleList", param);
        }
        public List<WFD01150ApproveDModel> GetApproveDDatas(int APPROVE_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPROVE_ID", SqlDbType.Int) { Value = (object)APPROVE_ID ?? DBNull.Value });

            return executeDataToList<WFD01150ApproveDModel>("sp_WFD01150_GetApproveD", param);
        }

        public List<SystemModel> GetDefaultFlow(string RequestFlowType)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.NVarChar) { Value = (object)RequestFlowType ?? DBNull.Value });

            return executeDataToList<SystemModel>("sp_WFD01150_GetDefaultFlow", param);
        }
        public List<WFD01150ApproveEmailDModel> GetHeaderEmail(WFD01150EditScreenModel _para)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.NVarChar) { Value = (object)_para.REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@APPROVE_ID", SqlDbType.Int) { Value = (object)_para.APPROVE_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)_para.CurrentSeq ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)_para.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)_para.USER ?? DBNull.Value });
            return executeDataToList<WFD01150ApproveEmailDModel>("sp_WFD01150_GetHeaderEmail", param);
        }
        public List<WFD01150ApproveEmailDModel> GetDetailEmail(WFD01150EditScreenModel _para)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.NVarChar) { Value = (object)_para.REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@APPROVE_ID", SqlDbType.Int) { Value = (object)_para.APPROVE_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)_para.CurrentSeq ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)_para.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)_para.USER ?? DBNull.Value });
            return executeDataToList<WFD01150ApproveEmailDModel>("sp_WFD01150_GetDetailEmail", param);
        }
        public TB_M_APPROVE_H GetApproveH(int APPROVE_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPROVE_ID", SqlDbType.Int) { Value = (object)APPROVE_ID ?? DBNull.Value });

            return executeDataToList<TB_M_APPROVE_H>("sp_WFD01150_GetApproveH", param).First();
        }

        public List<string> CheckDupplicateApproveH(string COMPANY, string REQUEST_FLOW_TYPE, string ASSET_CLASS_STR)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.NVarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.NVarChar) { Value = (object)REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_CLASS_STR", SqlDbType.NVarChar) { Value = (object)ASSET_CLASS_STR ?? DBNull.Value });

            DataTable dt = executeDataToDataTable("sp_WFD01150_CheckDupplicateApproveH", param);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.AsEnumerable()
                           .Select(r => r.Field<string>("ASSET_CLASS"))
                           .ToList();
            }
            else return new List<string>();
        }

        #region Add Data

        public int AddApproveH(TB_M_APPROVE_H data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY ?? DBNull.Value });

            return executeScalar<int>("sp_WFD01150_AddApproveH", param);
        }
        public void AddApproveD(WFD01150ApproveDModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)data.INDX ?? DBNull.Value });
            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ROLE ?? DBNull.Value });
            param.Add(new SqlParameter("@APPRV_GROUP", SqlDbType.VarChar) { Value = (object)data.APPRV_GROUP ?? DBNull.Value });
            param.Add(new SqlParameter("@DIVISION", SqlDbType.VarChar) { Value = (object)data.DIVISION ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUIRE_FLAG", SqlDbType.VarChar) { Value = (object)data.REQUIRE_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@CONDITION_CODE", SqlDbType.VarChar) { Value = (object)data.CONDITION_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@OPERATOR", SqlDbType.VarChar) { Value = (object)data.OPERATOR ?? DBNull.Value });
            param.Add(new SqlParameter("@VALUE1", SqlDbType.VarChar) { Value = (object)data.VALUE1 ?? DBNull.Value });
            param.Add(new SqlParameter("@VALUE2", SqlDbType.VarChar) { Value = (object)data.VALUE2 ?? DBNull.Value });
            param.Add(new SqlParameter("@FINISH_FLOW", SqlDbType.VarChar) { Value = (object)data.FINISH_FLOW ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_DEL_ITEM", SqlDbType.VarChar) { Value = (object)data.ALLOW_DEL_ITEM ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_REJECT", SqlDbType.VarChar) { Value = (object)data.ALLOW_REJECT ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_SEL_APPRV", SqlDbType.VarChar) { Value = (object)data.ALLOW_SEL_APPRV ?? DBNull.Value });
            param.Add(new SqlParameter("@REJECT_TO", SqlDbType.VarChar) { Value = (object)data.REJECT_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@HIGHER_APPR", SqlDbType.VarChar) { Value = (object)data.HIGHER_APPR ?? DBNull.Value });

            param.Add(new SqlParameter("@KPI_LEADTIME", SqlDbType.VarChar) { Value = (object)data.LEAD_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@GEN_FILE", SqlDbType.VarChar) { Value = (object)data.GENERATE_FILE ?? DBNull.Value });
            param.Add(new SqlParameter("@NOTI_MODE", SqlDbType.VarChar) { Value = (object)data.NOTI_BY_EMAIL ?? DBNull.Value });
            param.Add(new SqlParameter("@OP_MODE", SqlDbType.VarChar) { Value = (object)data.OPERATION ?? DBNull.Value });


            execute("sp_WFD01150_InsertApproveD", param);
        }

        #endregion

        #region Update data

        public void UpdateApproveH(TB_M_APPROVE_H data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.DateTime) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.UPDATE_BY ?? DBNull.Value });

            execute("sp_WFD01150_UpdateApproveH", param);
        }
        public void UpdateApproveD(WFD01150ApproveDModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)data.INDX ?? DBNull.Value });
            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ROLE ?? DBNull.Value });
            param.Add(new SqlParameter("@APPRV_GROUP", SqlDbType.VarChar) { Value = (object)data.APPRV_GROUP ?? DBNull.Value });
            param.Add(new SqlParameter("@DIVISION", SqlDbType.VarChar) { Value = (object)data.DIVISION ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUIRE_FLAG", SqlDbType.VarChar) { Value = (object)data.REQUIRE_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@CONDITION_CODE", SqlDbType.VarChar) { Value = (object)data.CONDITION_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@OPERATOR", SqlDbType.VarChar) { Value = (object)data.OPERATOR ?? DBNull.Value });
            param.Add(new SqlParameter("@VALUE1", SqlDbType.VarChar) { Value = (object)data.VALUE1 ?? DBNull.Value });
            param.Add(new SqlParameter("@VALUE2", SqlDbType.VarChar) { Value = (object)data.VALUE2 ?? DBNull.Value });
            param.Add(new SqlParameter("@FINISH_FLOW", SqlDbType.VarChar) { Value = (object)data.FINISH_FLOW ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_DEL_ITEM", SqlDbType.VarChar) { Value = (object)data.ALLOW_DEL_ITEM ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_REJECT", SqlDbType.VarChar) { Value = (object)data.ALLOW_REJECT ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_SEL_APPRV", SqlDbType.VarChar) { Value = (object)data.ALLOW_SEL_APPRV ?? DBNull.Value });
            param.Add(new SqlParameter("@REJECT_TO", SqlDbType.VarChar) { Value = (object)data.REJECT_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@HIGHER_APPR", SqlDbType.VarChar) { Value = (object)data.HIGHER_APPR ?? DBNull.Value });

            param.Add(new SqlParameter("@KPI_LEADTIME", SqlDbType.VarChar) { Value = (object)data.LEAD_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@GEN_FILE", SqlDbType.VarChar) { Value = (object)data.GENERATE_FILE ?? DBNull.Value });
            param.Add(new SqlParameter("@NOTI_MODE", SqlDbType.VarChar) { Value = (object)data.NOTI_BY_EMAIL ?? DBNull.Value });
            param.Add(new SqlParameter("@OP_MODE", SqlDbType.VarChar) { Value = (object)data.OPERATION ?? DBNull.Value });


            execute("sp_WFD01150_UpdateApproveD", param);
        }
        public void UpdateApproveEmailDetail(TB_M_APPROVE_H data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_FLOW_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.UPDATE_BY ?? DBNull.Value });

            execute("sp_WFD01150_UpdateApproveEmailDetail", param);
        }
        public void InsertApproveEmailDetail(TB_M_APPROVE_H data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.UPDATE_BY ?? DBNull.Value });

            execute("sp_WFD01150_InsertApproveEmailDetail", param);
        }
        public void SaveTempApproveEmailDetail(List<WFD01150ApproveEmailDModel> data, string User)
        {
            for (int i = 0; i < data.Count; i++)
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data[i].APPRV_ID ?? DBNull.Value });
                param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)data[i].INDX ?? DBNull.Value });
                param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data[i].ROLE ?? DBNull.Value });
                param.Add(new SqlParameter("@E_INDX", SqlDbType.Int) { Value = (object)data[i].E_INDX ?? DBNull.Value });
                param.Add(new SqlParameter("@E_ROLE", SqlDbType.VarChar) { Value = (object)data[i].E_ROLE ?? DBNull.Value });
                param.Add(new SqlParameter("@EMAIL_APR_TO", SqlDbType.VarChar) { Value = (object)data[i].EMAIL_APR_TO ?? DBNull.Value });
                param.Add(new SqlParameter("@EMAIL_APR_CC", SqlDbType.VarChar) { Value = (object)data[i].EMAIL_APR_CC ?? DBNull.Value });
                param.Add(new SqlParameter("@EMAIL_REJ_TO", SqlDbType.VarChar) { Value = (object)data[i].EMAIL_REJ_TO ?? DBNull.Value });
                param.Add(new SqlParameter("@EMAIL_REJ_CC", SqlDbType.VarChar) { Value = (object)data[i].EMAIL_REJ_CC ?? DBNull.Value });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data[i].GUID ?? DBNull.Value });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)User ?? DBNull.Value });
                execute("sp_WFD01150_SaveTempApproveEmailDetail", param);
            }
        }
        public void DeleteTempApproveEmailDetail(WFD01150ApproveEmailDModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPROVE_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)data.INDX ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            execute("sp_WFD01150_DeleteTempApproveEmailDetail", param);
        }
        public void SaveTempApproveD(TB_M_APPROVE_D data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)data.INDX ?? DBNull.Value });
            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ROLE ?? DBNull.Value });
            param.Add(new SqlParameter("@APPRV_GROUP", SqlDbType.VarChar) { Value = (object)data.APPRV_GROUP ?? DBNull.Value });
            param.Add(new SqlParameter("@DIVISION", SqlDbType.VarChar) { Value = (object)data.DIVISION ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUIRE_FLAG", SqlDbType.VarChar) { Value = (object)data.REQUIRE_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@CONDITION_CODE", SqlDbType.VarChar) { Value = (object)data.CONDITION_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@OPERATOR", SqlDbType.VarChar) { Value = (object)data.OPERATOR ?? DBNull.Value });
            param.Add(new SqlParameter("@VALUE1", SqlDbType.VarChar) { Value = (object)data.VALUE1 ?? DBNull.Value });
            param.Add(new SqlParameter("@VALUE2", SqlDbType.VarChar) { Value = (object)data.VALUE2 ?? DBNull.Value });
            param.Add(new SqlParameter("@GENERATE_FILE", SqlDbType.VarChar) { Value = (object)data.GENERATE_FILE ?? DBNull.Value });
            param.Add(new SqlParameter("@FINISH_FLOW", SqlDbType.VarChar) { Value = (object)data.FINISH_FLOW ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_DEL_ITEM", SqlDbType.VarChar) { Value = (object)data.ALLOW_DEL_ITEM ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_REJECT", SqlDbType.VarChar) { Value = (object)data.ALLOW_REJECT ?? DBNull.Value });
            param.Add(new SqlParameter("@ALLOW_SEL_APPRV", SqlDbType.VarChar) { Value = (object)data.ALLOW_SEL_APPRV ?? DBNull.Value });
            param.Add(new SqlParameter("@REJECT_TO", SqlDbType.VarChar) { Value = (object)data.REJECT_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@HIGHER_APPR", SqlDbType.VarChar) { Value = (object)data.HIGHER_APPR ?? DBNull.Value });

            param.Add(new SqlParameter("@LEAD_TIME", SqlDbType.VarChar) { Value = (object)data.LEAD_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@NOTI_BY_EMAIL", SqlDbType.VarChar) { Value = (object)data.NOTI_BY_EMAIL ?? DBNull.Value });
            param.Add(new SqlParameter("@OPERATION", SqlDbType.VarChar) { Value = (object)data.OPERATION ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)data.USER ?? DBNull.Value });

            execute("sp_WFD01150_SaveTempApproveD", param);
        }
        #endregion

        #region Delete data

        public void DeleteApprove(WFD01150SearchApproveRowModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APPRV_ID", SqlDbType.Int) { Value = (object)data.APPRV_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.NVarChar) { Value = (object)data.UPDATE_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });

            execute("sp_WFD01150_DeleteApprove", param);
        }

        #endregion

    }
}