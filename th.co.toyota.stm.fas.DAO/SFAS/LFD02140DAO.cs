﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Shared;
using System.Linq;
using th.co.toyota.stm.fas.Models.SFAS.LFD02140;
using th.co.toyota.stm.fas.Models.WFD02320;

namespace th.co.toyota.stm.fas.DAO
{
    public class LFD02140DAO : Database
    {
        public LFD02140DAO() : base()
        {

        }
        public string GetPrintTagReport(string BARCODE_SIZE)
        {
            string path = "";
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)"REPORT_PATH" ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)"BARCODE" ?? DBNull.Value });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)BARCODE_SIZE ?? DBNull.Value });
            var result = executeDataToList<SystemConfigurationModels>("sp_Common_GetSystemValues", param);
            if (result.Count > 0 && result != null)
            {
                path = result.Select(x => x.VALUE).FirstOrDefault();
            }
            return path;
        }

        public string GetPrintTagReportCover(string BARCODE_SIZE)
        {
            string path = "";
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)"REPORT_PATH" ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)"BARCODE_COVER" ?? DBNull.Value });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)BARCODE_SIZE ?? DBNull.Value });
            var result = executeDataToList<SystemConfigurationModels>("sp_Common_GetSystemValues", param);
            if (result.Count > 0 && result != null)
            {
                path = result.Select(x => x.VALUE).FirstOrDefault();
            }
            return path;
        }

        public List<LFD02140AssetMasterModel> GetAsset(LFD02140AssetMasterModel _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)_data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)_data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@BARCODE_SIZE", SqlDbType.VarChar) { Value = (object)_data.BARCODE_SIZE ?? DBNull.Value });
            param.Add(new SqlParameter("@PRINT_LOCATION", SqlDbType.VarChar) { Value = (object)_data.PRINTLOCATION ?? DBNull.Value });
            return executeDataToList<LFD02140AssetMasterModel>("sp_LFD02140_GetAssetH", param);
        }

        public void PrintTagSuccess(LFD02140AssetMasterModel _data, string EMP_CODE)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)_data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)_data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)_data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)_data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@RESP_COST_CODE", SqlDbType.VarChar) { Value = (object)_data.RESP_COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            execute("sp_WFD02320_PrintTagSuccess", param);
        }
       
        public void PrintTagProcess(decimal? app_id, string status, string message_code
            , string message_param, string emp_code, string favorite_flag)
        {

            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@APP_ID", SqlDbType.Int) { Value = (object)app_id ?? DBNull.Value });
            param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = (object)status ?? DBNull.Value });
            param.Add(new SqlParameter("@MESSAGE_CODE", SqlDbType.VarChar) { Value = (object)message_code ?? DBNull.Value });
            param.Add(new SqlParameter("@MESSAGE_PARAM", SqlDbType.VarChar) { Value = (object)message_param ?? DBNull.Value });
            param.Add(new SqlParameter("@strUserID", SqlDbType.VarChar) { Value = (object)emp_code ?? DBNull.Value });
            param.Add(new SqlParameter("@FAVORITE_FLAG", SqlDbType.VarChar) { Value = (object)favorite_flag ?? DBNull.Value });
            execute("sp_Common_InsertLog_With_Param", param);

        }

        // Update Barcode before print by Pawares M. 20180619  
        //public void UpdateBarcode(string asset_no)
        //{

        //    List<SqlParameter> param = new List<SqlParameter>();
        //    param.Add(new SqlParameter("@AssetNo", SqlDbType.VarChar) { Value = (object)asset_no ?? DBNull.Value });
        //    execute("sp_WFD02110_UpdateAssetBarcode", param);
        //}
    }
}
