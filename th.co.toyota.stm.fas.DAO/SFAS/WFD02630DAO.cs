﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD02630;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02630DAO : Database
    {
        public WFD02630DAO() : base()
        {

        }
        public List<WFD02630YearModel> GetYearDAO(string company)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@company", SqlDbType.VarChar) { Value = (object)company ?? DBNull.Value });
            return executeDataToList<WFD02630YearModel>("sp_WFD02630_GetYear", param);
        }
        public List<WFD02630RoundModel> GetRoundDAO(string company, string year)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@company", SqlDbType.VarChar) { Value = (object)company ?? DBNull.Value });
            param.Add(new SqlParameter("@year", SqlDbType.VarChar) { Value = (object)year ?? DBNull.Value });
            return executeDataToList<WFD02630RoundModel>("sp_WFD02630_GetRound", param);
        }
        public List<WFD02630AssetLocationModel> GetAssetLocationDAO(string company, string year, string round)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@company", SqlDbType.VarChar) { Value = (object)company ?? DBNull.Value });
            param.Add(new SqlParameter("@year", SqlDbType.VarChar) { Value = (object)year ?? DBNull.Value });
            param.Add(new SqlParameter("@round", SqlDbType.VarChar) { Value = (object)round ?? DBNull.Value });
            return executeDataToList<WFD02630AssetLocationModel>("sp_WFD02630_GetAssetLocation", param);
        }

        public List<WFD02630HolidayModel> GetHolidayDAO(WFD02630SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@company", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@year", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@round", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@asset_location", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            return executeDataToList<WFD02630HolidayModel>("sp_WFD02630_GetHoliday", param);
        }
        public WFD02630FixedAssetModel GetStockList(WFD02630SearchModel data)
        {
            WFD02630FixedAssetModel FixedAssetModel = new WFD02630FixedAssetModel();

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@ISFAADMIN", SqlDbType.VarChar) { Value = (object)data.ISFAADMIN ?? DBNull.Value });
            DataSet dsAsset = executeDataToDataSet("sp_WFD02630_GetSTManageMonitoring", param);

            if (dsAsset.Tables[0] != null && dsAsset.Tables[0].Rows.Count > 0)
            {// DateAsOf            
                FixedAssetModel.DateAsOf = dsAsset.Tables[0].ToList<WFD2630DataAsOfModel>().First();
            }

            if (dsAsset.Tables[1] != null && dsAsset.Tables[1].Rows.Count > 0)
            {//OverAll             
                FixedAssetModel.OverAll = dsAsset.Tables[1].ToList<WFD2630OverAllModel>().First();
            }

            if (dsAsset.Tables[2] != null && dsAsset.Tables[2].Rows.Count > 0)
            {// Calendar         
                FixedAssetModel.Calendar = dsAsset.Tables[2].ToList<WFD02630CalendarModel>();
            }
            if (dsAsset.Tables[3] != null && dsAsset.Tables[3].Rows.Count > 0)
            {

                FixedAssetModel.TargetDate = dsAsset.Tables[3].ToList<WFD2630TargetDateModel>().First();
            }

            return FixedAssetModel;
        }
        public List<WFD02630GetDefaultDropDown> GetDefaultScreenDAO(WFD02630SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@IS_FaAdmin", SqlDbType.VarChar) { Value = (object)data.ISFAADMIN ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            return executeDataToList<WFD02630GetDefaultDropDown>("sp_WFD02630_GetDefaultDropDown", param);
        }


        public bool CheckCreateRequestStock(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION,string EMP_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            DataTable dt = executeDataToDataTable("sp_WFD02630_CheckCreateRequestStock", param);
            bool result = false;
            if (dt.Rows[0]["RESULT"].ToString() == "True")
            {
                return true;
            }

            return result;
        }

    }
}
