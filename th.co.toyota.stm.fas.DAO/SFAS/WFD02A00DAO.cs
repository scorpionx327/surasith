﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.WFD02210;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02A00DAO : RequestDetailDAO
    {

        public void InitialAssetList(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            execute("sp_WFD02A00_InitialAssetList", param);
        }
        public List<WFD02A00Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            
			param.Add(TotalItem);

            var datas = executeDataToList<WFD02A00Model>("sp_WFD02A00_GetAssetList", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());
            


            return datas;
        }

        public void ClearAssetList(WFD0BaseRequestDocModel data)
        {
            // Allow only Submit 
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            execute("sp_WFD02A00_ClearAssetList", param);  
        }
        public List<MessageModel> CheckValidation(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02A00_ValidationAsset", param);
        }

        public List<MessageModel> ApproveValidation(WFD0BaseRequestDocModel _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
            return executeDataToList<MessageModel>("sp_WFD02A00_ApproveValidation", param);

        }
        public void InsertAsset(WFD02A00Model data)
        {
            try
            {
               
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@IMPAIRMENT_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.IMPAIRMENT_FLAG) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });

                execute("sp_WFD02A00_InsertAsset", param);
                  
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                
                
                execute("sp_WFD02A00_DeleteAsset", param);
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        //public List<MessageModel> AddAssetListValidation(string assetNos)
        //{
        //    try
        //    {
        //        List<SqlParameter> param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("@ASSET_NOS", SqlDbType.VarChar) { Value = base.IfNull(assetNos) });

        //        return executeDataToList<MessageModel>("sp_WFD02A00_IM_AddAssetListValidation", param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}
        public void PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID }); //Submit mode
            param.Add(new SqlParameter("@ACT_ROLE", SqlDbType.VarChar) { Value = data.ACT_ROLE }); //Submit mode
            param.Add(new SqlParameter("@FLOW_TYPE", SqlDbType.VarChar) { Value = data.FLOW_TYPE });


            execute("sp_WFD02A00_PrepareGenerateFlow", param);

        }
        public void UpdateAsset(List<WFD02A00Model> lst,string _user )
        {
            try
            {
                foreach (var data in lst)
                {

                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                    param.Add(new SqlParameter("@POSTING_DATE", SqlDbType.VarChar) { Value = base.IfNull(data.POSTING_DATE) });
                    param.Add(new SqlParameter("@ACC_PRINCIPLE", SqlDbType.VarChar) { Value = base.IfNull(data.ACC_PRINCIPLE) });
                    param.Add(new SqlParameter("@REASON", SqlDbType.VarChar) { Value = base.IfNull(data.REASON) });
                    param.Add(new SqlParameter("@AMOUNT_POSTED", SqlDbType.VarChar) { Value = base.IfNull(data.AMOUNT_POSTED) });
                    param.Add(new SqlParameter("@PERCENTAGE", SqlDbType.VarChar) { Value = base.IfNull(data.PERCENTAGE) });
                    param.Add(new SqlParameter("@IMPAIRMENT_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.IMPAIRMENT_FLAG) });
                    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _user  });
                    param.Add(new SqlParameter("@ADJ_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.ADJ_TYPE) });
                    //param.Add(new SqlParameter("@TOTALCOST", SqlDbType.VarChar) { Value = base.IfNull(data.TOTALCOST) });
                    execute("sp_WFD02A00_UpdateAsset", param);
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        

        public void Submit(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Submit(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                 //Free function
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02A00_SubmitRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@FuncID", SqlDbType.VarChar) { Value = Constants.BatchID.WFD02A00 });
                execute("sp_WFD01170_InitialDefaultBOIDocument", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02810_SubmitwithApproveAsset", param);


                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02A00_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Reject(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02A00_RejectRequest", param);
				//If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02A00_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        
        public void CopyToNewImpairment(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02A00_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02A00_ClearAssetList", param);

                param.Clear();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@NEW_GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.NewGuid) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02A00_CopyToNewImpairment", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Approve(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Approve(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02A00_ApproveRequest", param);
				//If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02A00_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Acknowledge(WFD01170MainModel _data, string _status)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_data, _status);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02A00_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02A00_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        public void Resend(WFD01170MainModel _header, WFD01170SelectedAssetNoModel _data)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_header, Constants.DocumentStatus.Resend);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
                param.Add(new SqlParameter("@ASSET_LIST", SqlDbType.VarChar) { Value = base.IfNull(_data.SelectedList) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER) });

                execute("sp_WFD02A00_Resend", param);
               
                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void UpdateAmount(WFD02A00Model data, string _user)
        {
            try
            {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                    //param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                    //param.Add(new SqlParameter("@ACC_PRINCIPLE", SqlDbType.VarChar) { Value = base.IfNull(data.ACC_PRINCIPLE) });
                    //param.Add(new SqlParameter("@REASON", SqlDbType.VarChar) { Value = base.IfNull(data.REASON) });
                    param.Add(new SqlParameter("@AMOUNT_POSTED", SqlDbType.VarChar) { Value = base.IfNull(data.AMOUNT_POSTED) });
                    param.Add(new SqlParameter("@PERCENTAGE", SqlDbType.VarChar) { Value = base.IfNull(data.PERCENTAGE) });
                    param.Add(new SqlParameter("@IMPAIRMENT_FLAG", SqlDbType.VarChar) { Value = base.IfNull(data.IMPAIRMENT_FLAG) });
                    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _user });
                    param.Add(new SqlParameter("@ADJ_TYPE", SqlDbType.VarChar) { Value = base.IfNull(data.ADJ_TYPE) });
                    param.Add(new SqlParameter("@TOTALCOST", SqlDbType.VarChar) { Value = base.IfNull(data.TOTALCOST) });
                    execute("sp_WFD02A00_UpdateAmount", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
