﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01130;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD01130DAO : Database
    {
        public WFD01130DAO() : base()
        {

        }

        public List<WFD01130RequestHistoryModel> GetRequestHistory(WFD01130SearchModel Model, ref PaginationModel page)
        {
          
            string chkdate = "0";
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)Model.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)Model.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)Model.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@IS_SYS_ADMIN", SqlDbType.VarChar) { Value = (object)Model.IS_SYS_ADMIN ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)Model.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@TRANSACTION_TYPE", SqlDbType.VarChar) { Value = (object)Model.TRANSACTION_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@DATE_SERVICE_FROM", SqlDbType.VarChar) { Value = (object)Model.DATE_SERVICE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@DATE_SERVICE_TO", SqlDbType.VarChar) { Value = (object)(Model.DATE_SERVICE_TO + " 23:59:59") ?? DBNull.Value });
            if (!string.IsNullOrEmpty(Model.DATE_SERVICE_FROM) && !string.IsNullOrEmpty(Model.DATE_SERVICE_TO))
            {
                chkdate = "1";
            }
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)Model.DOCUMENT_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = (object)Model.STATUS ?? DBNull.Value });
            param.Add(new SqlParameter("@chkDate", SqlDbType.VarChar) { Value = chkdate });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);
            var datas = executeDataToList<WFD01130RequestHistoryModel>("sp_WFD01130_GetRequestHistory", param);
            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
       public WFD01130AssetModel GetAssetName(string ASSET_NO, string COMPANY, string ASSET_SUB)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)ASSET_SUB ?? DBNull.Value });
            var datas = executeDataToList<WFD01130AssetModel>("sp_WFD01130_GetAssetName", param);

            return datas.FirstOrDefault();
        }
    }
}
