﻿using System;
using System.Collections.Generic;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD02130;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

namespace th.co.toyota.stm.fas.DAO
{
   public class WFD02130DAO : Database
    {
        public WFD02130DAO() : base()
        {

        }
        public WFD02130_TB_M_ASSETS_H GetFixedAssetList(WFD02130UpdateDataModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            var rs = executeDataToList<WFD02130_TB_M_ASSETS_H>("sp_WFD02130_GetAsset", param);

            if (rs == null || rs.Count == 0)
                return new WFD02130_TB_M_ASSETS_H() ;

            return rs.FirstOrDefault();
        }

        //public List<WFD02130_TB_M_ASSETS_D> GetFixedAssetDetailList(WFD02130UpdateDataModel data, ref PaginationModel page)
        //{

        //    List<SqlParameter> param = new List<SqlParameter>();
        //    SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
        //    {
        //        Direction = ParameterDirection.Output
        //    };

        //    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });

        //    //Pagging
        //    param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
        //    param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ORDER_TYPE", SqlDbType.NVarChar) { Value = (object)page.OrderType ?? DBNull.Value });
        //    param.Add(TotalItem);


        //    var FixedAssetDetails =  executeDataToList<WFD02130_TB_M_ASSETS_D>("sp_WFD02130_GetAssetDetail", param);
        //    page.Totalitem = int.Parse(TotalItem.Value.ToString());

        //    return FixedAssetDetails;
        //}

        //public List<WFD02130_TB_M_ASSETS_FINANCE> GetFixedAssetFinance(WFD02130UpdateDataModel data, ref PaginationModel page)
        //{

        //    List<SqlParameter> param = new List<SqlParameter>();
        //    SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
        //    {
        //        Direction = ParameterDirection.Output
        //    };

        //    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });

        //    //Pagging
        //    param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
        //    param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ORDER_TYPE", SqlDbType.NVarChar) { Value = (object)page.OrderType ?? DBNull.Value });
        //    param.Add(TotalItem);


        //    var FixedAssetDetails = executeDataToList<WFD02130_TB_M_ASSETS_FINANCE>("sp_WFD02130_GetAssetFinance", param);
        //    page.Totalitem = int.Parse(TotalItem.Value.ToString());

        //    return FixedAssetDetails;
        //}

        public void UpdateAssetInfo(WFD02130_TB_M_ASSETS_H data, string _User)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });

                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
                param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

                param.Add(new SqlParameter("@EMP_NAME", SqlDbType.VarChar) { Value = (object)data.EMP_NAME ?? DBNull.Value });
                param.Add(new SqlParameter("@PLATE_TYPE", SqlDbType.VarChar) { Value = (object)data.PLATE_TYPE ?? DBNull.Value });
                param.Add(new SqlParameter("@BARCODE_SIZE", SqlDbType.VarChar) { Value = (object)data.BARCODE_SIZE ?? DBNull.Value });
                param.Add(new SqlParameter("@PRINT_LOCATION", SqlDbType.VarChar) { Value = (object)data.PRINT_LOCATION ?? DBNull.Value });
                param.Add(new SqlParameter("@STOCKTAKE_FREQUENCY", SqlDbType.VarChar) { Value = (object)data.STOCKTAKE_FREQUENCY ?? DBNull.Value });
                param.Add(new SqlParameter("@PROCESS_STATUS", SqlDbType.VarChar) { Value = (object)data.PROCESS_STATUS ?? DBNull.Value });
                param.Add(new SqlParameter("@EMP_REMARK", SqlDbType.VarChar) { Value = (object)data.EMP_REMARK ?? DBNull.Value });
                param.Add(new SqlParameter("@LOCATION_REMARK", SqlDbType.VarChar) { Value = (object)data.LOCATION_REMARK ?? DBNull.Value });
                param.Add(new SqlParameter("@IMP_INVOICE_1", SqlDbType.VarChar) { Value = (object)data.IMP_INVOICE_1 ?? DBNull.Value });
                param.Add(new SqlParameter("@IMP_INVOICE_2", SqlDbType.VarChar) { Value = (object)data.IMP_INVOICE_2 ?? DBNull.Value });
                param.Add(new SqlParameter("@IMP_DATE", SqlDbType.VarChar) { Value = (object)data.IMP_DATE ?? DBNull.Value });
                param.Add(new SqlParameter("@AORGOR_1", SqlDbType.VarChar) { Value = (object)data.AORGOR_1 ?? DBNull.Value });
                param.Add(new SqlParameter("@AORGOR_2", SqlDbType.VarChar) { Value = (object)data.AORGOR_2 ?? DBNull.Value });
                param.Add(new SqlParameter("@Free_Text", SqlDbType.VarChar) { Value = (object)data.Free_Text ?? DBNull.Value });
                param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
                param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = _User });

                execute("sp_WFD02130_UpdateAssetInfo", param);
            }
            catch (Exception ex)
            {

                throw (ex);
            }
           
        }

        public WFD02130_TB_M_EMPLOYEE GetPrintLocation(WFD02130UpdateDataModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            var PrintLocation = executeDataToList<WFD02130_TB_M_EMPLOYEE>("sp_WFD02130_GetPrintLocation", param).FirstOrDefault();

            return PrintLocation;
        }

        public void AddPrintTagAndUpdateBarCodeSize(WFD02130UpdateDataModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@PRINT_LOCATION", SqlDbType.VarChar) { Value = (object)data.PRINT_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@BARCODE_SIZE", SqlDbType.VarChar) { Value = (object)data.BARCODE_SIZE ?? DBNull.Value });
            param.Add(new SqlParameter("@PLATE_TYPE", SqlDbType.VarChar) { Value = (object)data.PLATE_TYPE ?? DBNull.Value });

            execute("sp_WFD02130_AddPrintTagAndUpdateBarCodeSize", param);
        }

        public void UpdateTAG_Photo(WFD02130UpdateDataModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@TAG_PHOTO", SqlDbType.VarChar) { Value = (object)data.TAG_PHOTO ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
            execute("sp_WFD02130_UpdateTAG_Photo", param);
        }
        //public void UpdateBOIDoc(WFD02130BOIModel data)
        //{
        //    List<SqlParameter> param = new List<SqlParameter>();
        //    param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
        //    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
        //    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
        //    param.Add(new SqlParameter("@FileName", SqlDbType.VarChar) { Value = (object)data.FileName ?? DBNull.Value });
        //    param.Add(new SqlParameter("@FullFileName", SqlDbType.VarChar) { Value = (object)data.FullFileName ?? DBNull.Value });
        //    param.Add(new SqlParameter("@FileType", SqlDbType.VarChar) { Value = (object)data.FileType ?? DBNull.Value });

        //    param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
        //    execute("sp_WFD02130_UpdateBOIDoc", param);


        //}
        public string UpdateBOIDoc(FileDownloadModel data, string user)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = (object)user ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_UPLOAD", SqlDbType.VarChar) { Value = (object)data.DOC_UPLOAD ?? DBNull.Value });

            return executeScalar<string>("sp_WFD02130_UpdateBOIDoc", param);
        }
        public void DeleteBOIDoc(WFD02130BOIModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
            execute("sp_WFD02130_DeleteBOIDoc", param);


        }

        //
        public string GetPermissionUploadBOIDoc(string USER_LOGON)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@USER_LOGON", SqlDbType.VarChar) { Value = (object)USER_LOGON ?? DBNull.Value });

            return executeScalar<string>("sp_WFD02130_GetPermissionUploadBOIDoc", param);
        }

        
    }
}
