﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.WFD01270;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01170;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.DAO
{
    //public class WFD01270DAO : Database
    //{
    //    #region Permission
        
    //    //public string CheckPhonNoSVNew(WFD01270Model data)
    //    //{
    //    //    string _return = "N";
    //    //    List<SqlParameter> param = new List<SqlParameter>();

    //    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

    //    //    var datas = executeDataToList<WFD01270ApproverModel>("sp_WFD02410_CheckPhonNoSVNew", param);
    //    //    if (datas != null && datas.Count > 0)
    //    //    {
    //    //        _return = "Y";
    //    //    }
    //    //    return _return;
    //    //}
        
    //    #endregion

    //    #region Requestor
    //    //public WFD01270Requestor GetRequestor(WFD01270Model data)
    //    //{
    //    //    List<SqlParameter> param = new List<SqlParameter>();
    //    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });

    //    //    return executeDataToList<WFD01270Requestor>("sp_WFD01270_GetRequestor", param).First();
    //    //}
    //    public WFD01270Model GetDocumentInfo(string docno)
    //    {
    //        List<SqlParameter> param = new List<SqlParameter>();
    //        param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = docno });
         
    //        return executeDataToList<WFD01270Model>("sp_WFD01270_GetDocumentInfo", param).First();
    //    }
    //    public List<BaseSearchModel> GetAssetListHistory(string docno)
    //    {
    //        List<SqlParameter> param = new List<SqlParameter>();
    //        param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = docno });

    //        return executeDataToList<BaseSearchModel>("sp_WFD01270_GetAssetListHistory", param);
             
    //    }
    //    #endregion
        
    //    #region Approver

    //    //public List<MessageModel> ValidateGenerateApprover(WFD01270Model data)
    //    //{
    //    //    List<MessageModel> datas = new List<MessageModel>();
    //    //    try
    //    //    {
    //    //        List<SqlParameter> param = new List<SqlParameter>();
    //    //        param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
    //    //        datas = executeDataToList<MessageModel>("sp_WFD01270_ValidateGenerateApprover", param);
    //    //        return datas;
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        return null;
    //    //    }
    //    //}

    //    //public void GenerateApprover(WFD01270Model data, string _RequestFlowType, bool _GenerateRouteFlag)
    //    //{
    //    //    List<SqlParameter> param = new List<SqlParameter>();

    //    //    if (_GenerateRouteFlag) // New
    //    //    {
    //    //        param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = data.COMPANY });
    //    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? string.Format("{0}/{1}", data.USER_BY, data.REQUEST_TYPE) });
    //    //        param.Add(new SqlParameter("@REQUEST_FLOW_TYPE", SqlDbType.VarChar) { Value = _RequestFlowType });
    //    //        param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@CostCodeTo", SqlDbType.VarChar) { Value = (object)data.COST_CENTER_TO ?? DBNull.Value });

    //    //        execute("sp_WFD01270_GenerateApproveRoute", param);
    //    //    }
    //    //    param.Clear();
    //    //    param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = data.COMPANY });
    //    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? string.Format("{0}/{1}", data.USER_BY, data.REQUEST_TYPE) });
    //    //    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@CostCodeTo", SqlDbType.VarChar) { Value = (object)data.COST_CENTER_TO ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@EmpCode", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });


    //    //    execute("sp_WFD01270_GenerateApproverList", param);

    //    //}
       
    //    public List<WFD01270ApproverModel> GetApprover(WFD01270Model data)
    //    {
    //        List<SqlParameter> param = new List<SqlParameter>();
            
    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? string.Format("{0}/{1}", data.USER_BY , data.REQUEST_TYPE) });
          
    //        var datas = executeDataToList<WFD01270ApproverModel>("sp_WFD01270_GetApprover", param);

    //        return datas;
    //    }

    //    public List<WFD01270ApproverModel> GetListApprover(WFD01270Model data)
    //    {
    //        List<SqlParameter> param = new List<SqlParameter>();
    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? string.Format("{0}/{1}", data.USER_BY, data.REQUEST_TYPE) });


    //        var datas = executeDataToList<WFD01270ApproverModel>("sp_WFD01270_GetListApprover", param);

    //        return datas;
    //    }

        

    //    //public List<WFD01270ApproverModel> GetApproverByDocNo(WFD01270Model data)
    //    //{
    //    //    List<SqlParameter> param = new List<SqlParameter>();
    //    //    param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });

    //    //    var datas = executeDataToList<WFD01270ApproverModel>("sp_WFD01270_GetApproverByDocNo", param);

    //    //    return datas;
    //    //}

    //    //public List<WFD01270ListApproverModel> GetListApproverByDocNo(WFD01270Model data)
    //    //{
    //    //    List<SqlParameter> param = new List<SqlParameter>();
    //    //    param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
    //    //    param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });

    //    //    var datas = executeDataToList<WFD01270ListApproverModel>("sp_WFD01270_GetListApproverByDocNo", param);

    //    //    return datas;
    //    //}
    //    #endregion
            
    //    #region Submit


    //    //public void SaveRequestData(WFD01270MainModel _data)
    //    //{

    //    //    //InsertTempRequestHeaderData(_data);
    //    //    UploadApproverName(_data);
           
    //    //}

    //    //private void InsertTempRequestHeaderData(WFD01270MainModel _data)// Main And Requertor
    //    //{
          
    //    //    try
    //    //    {
    //    //        List<SqlParameter> param = new List<SqlParameter>();
    //    //        param.Add(new SqlParameter("@COMPANY",  base.IfNull(_data.RequestHeaderData.COMPANY)));
    //    //        param.Add(new SqlParameter("@GUID", base.IfNull(_data.RequestHeaderData.GUID)));
    //    //        param.Add(new SqlParameter("@EMP_CODE", base.IfNull(_data.RequestHeaderData.EMP_CODE)));
    //    //        param.Add(new SqlParameter("@REQUEST_TYPE", base.IfNull(_data.RequestHeaderData.REQUEST_TYPE)));
    //    //        param.Add(new SqlParameter("@APPROVE_FLOW_TYPE", base.IfNull(_data.RequestHeaderData.APPROVE_FLOW_TYPE)));
    //    //        param.Add(new SqlParameter("@PHONE_NO", base.IfNull(_data.Requestor.PHONE_NO)));
    //    //        param.Add(new SqlParameter("@STATUS", base.IfNull(_data.Requestor.STATUS)));
    //    //        param.Add(new SqlParameter("@DOC_NO", base.IfNull(_data.RequestHeaderData.DOC_NO)));
    //    //        param.Add(new SqlParameter("@USER_BY", base.IfNull(_data.RequestHeaderData.USER_BY)));

    //    //        executeScalar<string>("sp_WFD01270_InsertHeaderData", param);
                
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        throw (ex);
    //    //    }
            
    //    //}

    //    //public void UploadApproverName(WFD01270MainModel data)
    //    //{
    //    //    try
    //    //    {
    //    //        if (data.ApproverList == null)
    //    //            return;
    //    //        for (int i = 0; i < data.ApproverList.Count; i++)
    //    //        {
    //    //            List<SqlParameter> param = new List<SqlParameter>();
    //    //            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.RequestHeaderData.DOC_NO ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)data.ApproverList[i].INDX ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@ROLE", SqlDbType.VarChar) { Value = (object)data.ApproverList[i].ROLE ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.ApproverList[i].EMP_CODE ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@DIVISION", SqlDbType.VarChar) { Value = (object)data.ApproverList[i].DIVISION ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.RequestHeaderData.REQUEST_TYPE ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)data.RequestHeaderData.EMP_CODE ?? DBNull.Value });
    //    //            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.RequestHeaderData.GUID ?? DBNull.Value });

    //    //            var datas = executeScalar<string>("sp_WFD01270_SubmitApproverData", param);
                    
    //    //        }
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        throw (ex);
    //    //    }

    //    //}

    //    //public void SaveCommentInfo(WFD01270MainModel _data)
    //    //{
          
    //    //    try
    //    //    {
    //    //        List<SqlParameter> param = new List<SqlParameter>();
    //    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)_data.CommentInfo.Index ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.EMP_CODE ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = (object)_data.CommentInfo.CommentText ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = (object)_data.CommentInfo.AttachFileName  ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.USER_BY ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.REQUEST_TYPE ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@COMMENT_SATAUS", SqlDbType.VarChar) { Value = (object)_data.CommentInfo.CommentStatus ?? DBNull.Value });
               

    //    //        var datas = executeScalar<string>("sp_WFD01270_SubmitCommentInfoData", param);
                
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        throw (ex);
    //    //    }

    //    //}
    //    #endregion

    //    public void AdjustDisposeApproveFlow(string _docNo)
    //    {
    //        try
    //        {
    //            List<SqlParameter> param = new List<SqlParameter>();
    //            param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = _docNo });
                
    //            execute("sp_WFD01270_AdjustApproveFlowDisposal", param);

    //        }
    //        catch (Exception ex)
    //        {
    //            throw (ex);
    //        }
    //    }

    //    //public List<WFD01270DocumentMappingModel> UpdateRequestData(WFD01270MainModel _data, string _ApproveType)// Main And Requertor
    //    //{

    //    //    try
    //    //    {
                
    //    //        List<SqlParameter> param = new List<SqlParameter>();
    //    //        param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.GUID ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = _ApproveType  });
    //    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
    //    //        return executeDataToList<WFD01270DocumentMappingModel>("sp_WFD01270_SubmitMainData", param);
                
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        throw (ex);
    //    //    }

    //    //}


    //    //
    //    //public void ApproveRequest(WFD01270MainModel _data, string _Action,bool _submit)// Main And Requertor
    //    //{

    //    //    try
    //    //    {
    //    //        if (_Action == "A")
    //    //        {
    //    //            UploadApproverName(_data);
    //    //        }

    //    //        List<SqlParameter> param = new List<SqlParameter>();
    //    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.EMP_CODE ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@DELEGATE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DELEGATE ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.FA ?? DBNull.Value });

    //    //        param.Add(new SqlParameter("@HIGHER", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.HIGHER ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@ACTION", SqlDbType.VarChar) { Value = _Action });

    //    //        param.Add(new SqlParameter("@Comment", SqlDbType.VarChar) { Value = _data.CommentInfo.CommentText });
    //    //        param.Add(new SqlParameter("@AttachFile", SqlDbType.VarChar) { Value = _data.CommentInfo.AttachFileName });
    //    //        param.Add(new SqlParameter("@SubmitFlag", SqlDbType.VarChar) { Value = _submit ? "Y" : "N" });
    //    //        execute("sp_WFD01270_ApproveRequest", param);

    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        throw (ex);
    //    //    }

    //    //}

    //    //public List<MessageModel> ValidationConcurrency(WFD01270MainModel _data)// Main And Requertor
    //    //{
    //    //    List<MessageModel> datas = new List<MessageModel>();
    //    //    try
    //    //    {
    //    //        List<SqlParameter> param = new List<SqlParameter>();
    //    //        param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.GUID ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@RequestType", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.REQUEST_TYPE ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
    //    //        param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.UPDATE_DATE ?? DBNull.Value });

    //    //        datas = executeDataToList<MessageModel>("sp_WFD01270_ValidationConcurrency", param);
    //    //        return datas;
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        throw (ex);
    //    //    }          
            
    //    //}

    //    //public List<WFD01270CommentHistoryModel> GetTempDocCommentHistory(string DOC_NO)
    //    //{
    //    //    List<SqlParameter> param = new List<SqlParameter>();
    //    //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)DOC_NO ?? DBNull.Value });

    //    //    var datas = executeDataToList<WFD01270CommentHistoryModel>("sp_WFD01270_GetTempDocCommentHistory", param);

    //    //    return datas;
    //    //}

    //    public void UpdateFileCommentHistory(WFD01170CommentHistoryModel _data)// Main And Requertor
    //    {
    //        try
    //        {
    //            List<SqlParameter> param = new List<SqlParameter>();
    //            param.Add(new SqlParameter("@COMMENT_HISTORY_ID", SqlDbType.VarChar) { Value = (object)_data.COMMENT_HISTORY_ID ?? DBNull.Value });
    //            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.DOC_NO ?? DBNull.Value });
    //            param.Add(new SqlParameter("@INDX", SqlDbType.Int) { Value = (object)_data.INDX ?? DBNull.Value });
    //            param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = (object)_data.ATTACH_FILE ?? DBNull.Value });

    //            execute("sp_WFD01270_UpdateFileCommentHistory", param);
    //        }
    //        catch (Exception ex)
    //        {
    //            throw (ex);
    //        }

    //    }
    //}

    public class RequestDetailDAO : Database
    {
       
        protected void _Submit(WFD01170MainModel _data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                // Change Approver Name Incase Allow to change
                if (!string.IsNullOrEmpty(_data.ApproverList))
                {
                    _data.ApproverList = _data.ApproverList.Replace("null", string.Empty); // data concat from jquery
                }

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@ApproverList", SqlDbType.VarChar) { Value = _data.ApproverList });
                param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD01170_SubmitApprover", param);
                
                //Request Header Data, If copy not allow to edit
                param.Clear();
                param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@PHONE_NO", SqlDbType.VarChar) { Value = _data.RequestHeaderData.PHONE_NO });
                param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.CommentText) });
                param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.AttachFileName) });
                execute("sp_WFD01170_SubmitRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD01170_SubmitAttachDoc", param);


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        protected void _Reject(WFD01170MainModel _data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                
                // Login Information
                param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                param.Add(new SqlParameter("@IS_DELEGATE", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsDelegate) });
                param.Add(new SqlParameter("@IS_HIGHER", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsHigher) });
                param.Add(new SqlParameter("@IS_INSTEAD", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsInstead) });

                // Comment Info
                param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.CommentText) });
                param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.AttachFileName) });

                param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.UPDATE_DATE) });

                execute("sp_WFD01170_RejectRequest", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        protected void _Approve(WFD01170MainModel _data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                param.Clear();
                param.Add(new SqlParameter("@GUID", base.IfNull(_data.RequestHeaderData.GUID)));
                param.Add(new SqlParameter("@DOC_NO", base.IfNull(_data.RequestHeaderData.DOC_NO)));
                param.Add(new SqlParameter("@ApproverList", base.IfNull(_data.ApproverList)));
                param.Add(new SqlParameter("@User", base.IfNull(_data.RequestHeaderData.USER_BY)));
                param.Add(new SqlParameter("@UPDATE_DATE", base.IfNull(_data.RequestHeaderData.UPDATE_DATE)));

                execute("sp_WFD01170_UpdateApprover", param); //shoud be check user authoization

                //Document No
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });

                // Login Information
                param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                param.Add(new SqlParameter("@IS_DELEGATE", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsDelegate) });
                param.Add(new SqlParameter("@IS_HIGHER", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsHigher) });
                param.Add(new SqlParameter("@IS_INSTEAD", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsInstead) });

                // Comment Info
                param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.CommentText) });
                param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.AttachFileName) });

                param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.UPDATE_DATE) });

                execute("sp_WFD01170_ApproveRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@DOC_NO", base.IfNull(_data.RequestHeaderData.DOC_NO)));
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD01170_ApproveAttachDoc", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void _UpdateRequestStatus(WFD01170MainModel _data, string _Status)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();

                //Document No
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });

                // Login Information
                param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                param.Add(new SqlParameter("@IS_DELEGATE", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsDelegate) });
                param.Add(new SqlParameter("@IS_HIGHER", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsHigher) });
                param.Add(new SqlParameter("@IS_INSTEAD", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.IsInstead) });

                param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = _Status });

                // Comment Info
                param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.CommentText) });
                param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.AttachFileName) });

                param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.UPDATE_DATE) });

                
                execute("sp_WFD01170_UpdateRequestStatus", param);
            
            }
            catch (Exception ex)
            { 
                throw (ex);
            }
        }

        
    }

    public class WFD01170DAO : Database
    {
        public WFD01170UserPermissionModel GetPermission(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
            var _rs = executeDataToList<WFD01170UserPermissionModel>("sp_WFD01170_GetPermission", param);

            if (_rs.Count == 0)
                return null;

            return _rs.FirstOrDefault();
        }

        public WFD01170UserPermissionModel GetPermissionRetirement(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.USER_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)data.REQUEST_TYPE ?? DBNull.Value });
            var _rs = executeDataToList<WFD01170UserPermissionModel>("sp_WFD01170_GetPermissionRetirement", param);

            if (_rs.Count == 0)
                return null;

            return _rs.FirstOrDefault();
        }
        

        public TB_R_REQUEST_H GetRequestHeader(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });


            var _rs = executeDataToList<TB_R_REQUEST_H>("sp_WFD01170_GetRequestHeader", param);
            if (_rs.Count == 0)
                return null ;

            return _rs.First();
        }
        //
        public void InitializeApproverList(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            
            execute("sp_WFD01170_InitializeApproverList", param);
 
        }

        public WFD01170Requestor GetRequestor(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY_SELECTED) });

            return executeDataToList<WFD01170Requestor>("sp_WFD01170_GetRequestor", param).First();
        }
        public List<SystemModel> GetRoleOfRequestor(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            param.Add(new SqlParameter("@DEFAULT", SqlDbType.VarChar) { Value = base.IfNull(data.ACT_ROLE) });
            return executeDataToList<SystemModel>("sp_WFD01170_GetRoleOfRequeestor", param);
        }
        public bool IsAllowCreateDocument(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            var _rs = executeScalar<string>("sp_WFD01170_AllowCreateRequest", param).FirstOrDefault();

            return string.Format("{0}", _rs) == "Y";
        }
        public void GenerateApproverFlow(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();            
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });            
            execute("sp_WFD01170_GenerateApproverFlow", param);

            param.Clear();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            //Incase Success
            execute("sp_WFD01170_GeneratePersonList", param);
            
        }
        //sp_WFD01170_ResetFlow
        public void ResetApproverFlow(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            execute("sp_WFD01170_ResetFlow", param);
        }
        public List<WFD01170ApproverModel> GetApproverFlow(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //submit
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //actual
            return executeDataToList<WFD01170ApproverModel>("sp_WFD01170_GetApproverFlow", param);
        }
        public List<WFD01170ApproverModel> GetApproverPerson(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //submit
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //actual
            return executeDataToList<WFD01170ApproverModel>("sp_WFD01170_GetApproverPersonList", param);
        }

        public List<WFD0BaseRequestDocModel> GetDocumentList(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID });
            return executeDataToList<WFD0BaseRequestDocModel>("sp_WFD01170_GetTotalDocuments", param);

        }

        #region Workflow
        public List<WFD01170WorkflowTrackingProcessModel> GetWorkflowTrackingProcess(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) } );

            var datas = executeDataToList<WFD01170WorkflowTrackingProcessModel>("sp_WFD01170_GetWorkflowTrackingProcess", param);

            return datas;
        }
        #endregion

        #region Comment

        public List<WFD01170CommentHistoryModel> GetCommentHistory(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) } );

            var datas = executeDataToList<WFD01170CommentHistoryModel>("sp_WFD01170_GetCommentHistory", param);

            return datas;
        }
        #endregion


        public void SaveApprover(WFD01170MainModel _data)
        {
            if (!string.IsNullOrEmpty(_data.ApproverList))
            {
                _data.ApproverList = _data.ApproverList.Replace("null", string.Empty); // data concat from jquery
            }
            List<SqlParameter> param = new List<SqlParameter>();
            param.Clear();
            param.Add(new SqlParameter("@GUID", base.IfNull(_data.RequestHeaderData.GUID)));
            param.Add(new SqlParameter("@DOC_NO", base.IfNull(_data.RequestHeaderData.DOC_NO)));
            param.Add(new SqlParameter("@ApproverList", base.IfNull(_data.ApproverList)));
            param.Add(new SqlParameter("@User", base.IfNull(_data.RequestHeaderData.USER_BY)));
            param.Add(new SqlParameter("@UPDATE_DATE", base.IfNull(_data.RequestHeaderData.UPDATE_DATE)));

            execute("sp_WFD01170_UpdateApprover", param);

        }

        //dbo.fn_IsReadyGenFile('STM-A201909/0005')
        public bool IsReadyGenFile(string _DocNo)
        {
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value =_DocNo });
            var _rs = executeScalar<string>("sp_WFD01170_IsReadyGenFile", param);
          
            return string.Format("{0}", _rs) == "Y";
        }

        public void UpdateGenStatusForNewAssetRequest(WFD01170SelectedAssetNoModel _data, string _User)
        {
            if (!string.IsNullOrEmpty(_data.SelectedList))
            {
                _data.SelectedList = _data.SelectedList.Replace("null", string.Empty); // data concat from jquery
            }
            List<SqlParameter> param = new List<SqlParameter>();
            try
            {
                beginTransaction();
                
                param.Clear();
                param.Add(new SqlParameter("@GUID", base.IfNull(_data.GUID)));
                param.Add(new SqlParameter("@DOC_NO ", base.IfNull(_data.DOC_NO)));
                param.Add(new SqlParameter("@LINE_NO", base.IfNull(_data.SelectedList)));
                param.Add(new SqlParameter("@User", _User));

                execute("sp_WFD01170_SetGenStatusRequestAssetNo", param);
                
              
                commitTransaction();
                closeConnection();
            }catch(Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        
        public void AECUpdateRequestAfterSendSAP(WFD01170MainModel _data, string _RejectStatus)
        {
            try
            {
                beginTransaction();
                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });

                // Login Information
                param.Add(new SqlParameter("@LOGIN_USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                param.Add(new SqlParameter("@REJECT_FLAG", SqlDbType.VarChar) { Value = _RejectStatus });

                // Comment Info
                param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.CommentText) });
                param.Add(new SqlParameter("@ATTACH_FILE", SqlDbType.VarChar) { Value = base.IfNull(_data.CommentInfo.AttachFileName) });

                param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.UPDATE_DATE) });

                execute("sp_WFD01170_AECUpdateRequestAfterSendSAP", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
            
        }
    }
}
