﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD02610;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.WFD01120;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02630;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD01120DAO : Database
    {
        public WFD01120DAO() : base()
        {

        }       

        public List<SystemModel> GetReportType()
        {           
            return executeDataToList<SystemModel>("sp_WFD01120_GetReportType");
        }

        public List<SystemConfigurationModels> GetReportName(string ReportType)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)ReportType ?? DBNull.Value });

            return executeDataToList<SystemConfigurationModels>("sp_WFD01120_GetReportName", param);
        }

        public List<SystemModel> GetReportNameForCombo()
        {
            List<SqlParameter> param = new List<SqlParameter>();

            return executeDataToList<SystemModel>("sp_WFD01120_GetReportNameByComBo", param);
        }

        public List<CostCenterModels> LoadCostCenterByEmployeeCode(string employeecode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)employeecode ?? DBNull.Value });
            return executeDataToList<CostCenterModels>("sp_WFD01120_GetCostCenter", param);
        }       

        public List<WFD01120ChangeRequestDetail> GetChangeRequestDetail(string Company, string RequestType, string DocNo, Nullable<DateTime> PeriodFrom, Nullable<DateTime> PeriodTo, string EmpCode,ref PaginationModel Page)
        {           
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)Company ?? DBNull.Value });
            param.Add(new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar) { Value = (object)RequestType ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)DocNo ?? DBNull.Value });
            param.Add(new SqlParameter("@PERIOD_FROM", SqlDbType.Date) { Value = (PeriodFrom.HasValue ? (object)(PeriodFrom.Value.Date) : DBNull.Value)});
            param.Add(new SqlParameter("@PERIOD_TO", SqlDbType.Date) { Value = (PeriodTo.HasValue ? (object)(PeriodTo.Value.Date) : DBNull.Value) });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EmpCode ?? DBNull.Value });
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)Page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)Page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)Page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)Page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD01120ChangeRequestDetail>("sp_WFD01120_GetChangeRequestDetail", param);

            Page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        //private string ConvertDate2String(DateTime value) {
        //    return string.Format("{0}-{1}-{2} {3}:{4}:{5}", value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second);
        //}
        public List<WFD02630GetDefaultDropDown> GetStockTakingReportDAO(string EMP_CODE, string IS_FAADMIN)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@IS_FaAdmin", SqlDbType.VarChar) { Value = (object)IS_FAADMIN ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            return executeDataToList<WFD02630GetDefaultDropDown>("sp_WFD02630_GetDefaultDropDown", param);
        }
    }
}
