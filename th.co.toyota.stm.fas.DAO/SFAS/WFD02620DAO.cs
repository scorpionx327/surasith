﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD02620;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02620DAO : Database 
    {
        public WFD02620DAO() : base()
        {
        }

        public WFD02620DefaultScreenModel GetDefaultSreenData()
        {
            return executeDataToList<WFD02620DefaultScreenModel>("sp_WFD02620_GetDefaultScreenData").First();
        }

        public WFD02620YearRoundAssetLocModel GetParamWithoutAssetLocation(string COMPANY, string YEAR, string ROUND)
        {

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });

            return executeDataToList<WFD02620YearRoundAssetLocModel>("sp_WFD02620_GetParamWithoutAssetLocation", param).First();
        }


        
        public WFD02620SettingModel GetSettingSectionData(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            return executeDataToList<WFD02620SettingModel>("sp_WFD02620_GetStockTakeHSettingData", param).First();
        }
        public List<WFD02620StockTakeDModel> GetStockTakeDDatas(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            return executeDataToList<WFD02620StockTakeDModel>("sp_WFD02620_GetStockTakeDDatas", param);
        }
        public List<WFD02620StockTakeDPerSVModel> GetStockTakeDPerSVDatas(string COMPANY,string YEAR, string ROUND, string ASSET_LOCATION)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            return executeDataToList<WFD02620StockTakeDPerSVModel>("sp_WFD02620_GetStockTakeDPerSVDatas", param);
        }
        public List<WFD02620HolidayModel> GetStockTakeHolidayDatas(string STOCK_TAKE_KEY,string COMPANY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            return executeDataToList<WFD02620HolidayModel>("sp_WFD02620_GetStockTakeHoliday", param);
        }

        public WFD02620MasterSettingModel GetMasterSettingData()
        {
            return executeDataToList<WFD02620MasterSettingModel>("sp_WFD02620_GetMasterSettingData").First();
        }

        public string AddStockTakeRequest(WFD02620SettingModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@DATA_AS_OF", SqlDbType.Date) { Value = (object)data.DATA_AS_OF ?? DBNull.Value });
            //param.Add(new SqlParameter("@SUB_TYPE", SqlDbType.VarChar) { Value = (object)data.SUB_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@TARGET_DATE_FROM", SqlDbType.Date) { Value = (object)data.TARGET_DATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@TARGET_DATE_TO", SqlDbType.Date) { Value = (object)data.TARGET_DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@QTY_OF_HANDHELD", SqlDbType.Int) { Value = (object)data.QTY_OF_HANDHELD ?? DBNull.Value });
            param.Add(new SqlParameter("@BREAK_TIME_MINUTE", SqlDbType.Int) { Value = (object)data.BREAK_TIME_MINUTE ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_STATUS", SqlDbType.VarChar) { Value = (object)data.ASSET_STATUS ?? DBNull.Value });

            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CENTER", SqlDbType.VarChar) { Value = (object)data.COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@RESPONSIBLE_COST_CENTER", SqlDbType.VarChar) { Value = (object)data.RESPONSIBLE_COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@BOI", SqlDbType.VarChar) { Value = (object)data.BOI ?? DBNull.Value });
            param.Add(new SqlParameter("@INVENTORY_CHECK", SqlDbType.VarChar) { Value = (object)data.INVENTORY_CHECK ?? DBNull.Value });

            return executeScalar<string>("sp_WFD02620_AddStockTakeRequest", param);
        }

        public void EditStockTakeRequest(WFD02620SettingModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@DATA_AS_OF", SqlDbType.Date) { Value = (object)data.DATA_AS_OF ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_TYPE", SqlDbType.VarChar) { Value = (object)data.SUB_TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@TARGET_DATE_FROM", SqlDbType.Date) { Value = (object)data.TARGET_DATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@TARGET_DATE_TO", SqlDbType.Date) { Value = (object)data.TARGET_DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@QTY_OF_HANDHELD", SqlDbType.Int) { Value = (object)data.QTY_OF_HANDHELD ?? DBNull.Value });
            param.Add(new SqlParameter("@BREAK_TIME_MINUTE", SqlDbType.Int) { Value = (object)data.BREAK_TIME_MINUTE ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_STATUS", SqlDbType.VarChar) { Value = (object)data.ASSET_STATUS ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.DateTime) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)data.STOCK_TAKE_KEY ?? DBNull.Value });

            param.Add(new SqlParameter("@ASSET_CLASS", SqlDbType.VarChar) { Value = (object)data.ASSET_CLASS ?? DBNull.Value });
            param.Add(new SqlParameter("@MINOR_CATEGORY", SqlDbType.VarChar) { Value = (object)data.MINOR_CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CENTER", SqlDbType.VarChar) { Value = (object)data.COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@RESPONSIBLE_COST_CENTER", SqlDbType.VarChar) { Value = (object)data.RESPONSIBLE_COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@BOI", SqlDbType.VarChar) { Value = (object)data.BOI ?? DBNull.Value });
            param.Add(new SqlParameter("@INVENTORY_CHECK", SqlDbType.VarChar) { Value = (object)data.INVENTORY_CHECK ?? DBNull.Value });

            execute("sp_WFD02620_EditStockTakeRequest", param);
        }
        public List<string> ValidateAnnounce(string STOCK_TAKE_KEY, string COMPANY)
        {
            List<string> res = new List<string>();
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.NVarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            var dt = executeDataToDataTable("sp_WFD02620_AnnounceValidate", param);

            if (dt != null)
                foreach (DataRow row in dt.Rows)
                    res.Add(row[0].ToString());
            else
                return new List<string>();
            return res;
        }
        public List<string> IsOtherPlanInProcess(string STOCK_TAKE_KEY, string COMPANY)
        {
            List<string> res = new List<string>();
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.NVarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            var dt = executeDataToDataTable("sp_WFD02620_CheckOtherPlanIsActive", param);

            if (dt != null)
                foreach (DataRow row in dt.Rows)
                    res.Add(row[0].ToString());
            else
                return new List<string>();
            return res;
        }
        public void SaveAssignmentFAAdmin(WFD02620SettingModel data, WFD02620StockTakeDPerSVModel svData)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)svData.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@FA_DATE_FROM", SqlDbType.Date) { Value = (object)svData.FA_DATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@FA_DATE_TO", SqlDbType.Date) { Value = (object)svData.FA_DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@DATE_FROM", SqlDbType.Date) { Value = (object)svData.DATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@DATE_TO", SqlDbType.Date) { Value = (object)svData.DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@TIME_START", SqlDbType.VarChar) { Value = (object)svData.TIME_START ?? DBNull.Value });
            param.Add(new SqlParameter("@TIME_END", SqlDbType.VarChar) { Value = (object)svData.TIME_END ?? DBNull.Value });
            param.Add(new SqlParameter("@TIME_MINUTE", SqlDbType.Int) { Value = (object)svData.TIME_MINUTE ?? DBNull.Value });
            param.Add(new SqlParameter("@USAGE_HANDHELD", SqlDbType.Int) { Value = (object)svData.USAGE_HANDHELD ?? DBNull.Value });

            execute("sp_WFD02620_SaveAssignmentFAAdmin", param);
        }
        public void SaveStockTakeHoliday(string COMPANY, string STOCK_TAKE_KEY, string HOLIDAY_DATES)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@HOLIDAY_DATES", SqlDbType.NVarChar) { Value = (object)HOLIDAY_DATES ?? DBNull.Value });
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            execute("sp_WFD02620_SaveStockTakeHoliday", param);
        }
        public void UpdateStockTakePerSVDatas(List<WFD02620StockTakeDPerSVModel> datas)
        {
            beginTransaction();
            try
            {
                foreach (var d in datas)
                {
                    UpdateStockTakePerSVData(d);
                }

                commitTransaction();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw ex;
            }
        }
        public void UpdateStockTakePerSVData(WFD02620StockTakeDPerSVModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)data.STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@FA_DATE_FROM", SqlDbType.Date) { Value = (object)data.FA_DATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@FA_DATE_TO", SqlDbType.Date) { Value = (object)data.FA_DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@DATE_FROM", SqlDbType.Date) { Value = (object)data.DATE_FROM ?? DBNull.Value });
            param.Add(new SqlParameter("@DATE_TO", SqlDbType.Date) { Value = (object)data.DATE_TO ?? DBNull.Value });
            param.Add(new SqlParameter("@TIME_START", SqlDbType.VarChar) { Value = (object)data.TIME_START ?? DBNull.Value });
            param.Add(new SqlParameter("@TIME_END", SqlDbType.VarChar) { Value = (object)data.TIME_END ?? DBNull.Value });
            param.Add(new SqlParameter("@TIME_MINUTE", SqlDbType.Int) { Value = (object)data.TIME_MINUTE ?? DBNull.Value });
            param.Add(new SqlParameter("@USAGE_HANDHELD", SqlDbType.Int) { Value = (object)data.USAGE_HANDHELD ?? DBNull.Value });
            param.Add(new SqlParameter("@TOTAL_ASSET", SqlDbType.Int) { Value = (object)data.TOTAL_ASSET ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.UPDATE_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@UPDATE_DATE", SqlDbType.DateTime) { Value = (object)data.UPDATE_DATE ?? DBNull.Value });
            param.Add(new SqlParameter("@IS_LOCK", SqlDbType.NVarChar) { Value = (object)data.IS_LOCK ?? DBNull.Value });

            execute("sp_WFD02620_UpdateStockTakePerSV", param);
        }
        public void ResetAssignment(string UPDATE_BY,string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.NVarChar) { Value = (object)UPDATE_BY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            execute("sp_WFD02620_ResetAssignment", param);
        }

        public void CompletedStockTakePlan(string STOCK_TAKE_KEY, string COMPANY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.NVarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            execute("sp_WFD02620_CompletedStockTakePlan", param);
        }

        public List<AnnounceEmailModel> GetEmailDatas(string STOCK_TAKE_KEY, string COMPANY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.NVarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            return executeDataToList<AnnounceEmailModel>("sp_WFD02620_GetEmailDatas", param);
        }
        public string GetManagerEmailOfSV(string SV_EMP_CODE, string COMPANY)
        {
            var managerEmail = string.Empty;
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.NVarChar) { Value = (object)SV_EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });

            try
            {
                managerEmail = executeScalar<string>("sp_WFD02620_GetManagerEmailOfSV", param);
            }
            catch
            {
                managerEmail = string.Empty;
            }
            return managerEmail;
        }
        public bool ValidateCreateStockTakePlan(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });
            return executeScalar<bool>("sp_WFD02620_ValidateCreateStockTakePlan", param);
        }

        public List<WFD02620CostCenterModel> GetCostCenterForCombo()
        {
            List<SqlParameter> param = new List<SqlParameter>();

            return executeDataToList<WFD02620CostCenterModel>("sp_WFD02620_GetCostCenter", param);
        }

        public List<WFD02620CostCenterModel> GetResponsibleCostCenterForCombo()
        {
            List<SqlParameter> param = new List<SqlParameter>();

            return executeDataToList<WFD02620CostCenterModel>("sp_WFD02620_GetResponsibleCostCenter", param);
        }
    }
}