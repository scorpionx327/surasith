﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.SFAS.WFD021A4;

namespace th.co.toyota.stm.fas.DAO.SFAS
{
    public class WFD021A4DAO : RequestDetailDAO
    {
        public void UpdateAssetNo(WFD021A4ResponseRequestNo _data)
        {
            try
            {
                beginTransaction();


                List<SqlParameter> param = new List<SqlParameter>();
                foreach (var _asset in _data.ASSETS)
                {
                    param.Clear();
                    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.requestNo) });
                    param.Add(new SqlParameter("@LINE_NO", SqlDbType.VarChar) { Value = base.IfNull(_asset.requestItem) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(_asset.assetNo) });
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(_asset.assetSubNo) });
                    param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = base.IfNull(_asset.status) });
                    param.Add(new SqlParameter("@MESSAGE_TYPE", SqlDbType.VarChar) { Value = base.IfNull(_asset.status) });
                    param.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar) { Value = base.IfNull(_asset.messagelist) });
                    execute("sp_WFD021A4_UpdateAssetNo", param);
                }

                //Update document Status
                param.Clear();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.requestNo) });
                execute("sp_WFD021A4_UpdateRequestDocument", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }

            //
           
        }
    }
}
