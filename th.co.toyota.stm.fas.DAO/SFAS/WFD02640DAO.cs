﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.WFD02640;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02640DAO : RequestDetailDAO
    {
        private string RequestType { get { return "S"; } }
        public WFD02640DAO() : base()
        {

        }
        public TB_M_EMPLOYEE GetEmployeeDAO(string EMP_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            return executeDataToList<TB_M_EMPLOYEE>("sp_Common_getEmpByCode", param).FirstOrDefault();
        }
        public string GetActiveStatusPerSV(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION, string SV_EMP_CODE, string USER_LOGON)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@SV_EMP_CODE", SqlDbType.VarChar) { Value = (object)SV_EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGON", SqlDbType.VarChar) { Value = (object)USER_LOGON ?? DBNull.Value });

            return executeScalar<string>("sp_WFD02640_GetActiveStatusPerSV", param);
        }

        public string GetEmpTitle(string company, string year, string round, string assetLocation, string empCode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)company ?? DBNull.Value });

            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)year ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)round ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)assetLocation ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)empCode ?? DBNull.Value });
            var _rs = executeScalar<object>("sp_WFD02640_GetEmpTitle", param);

            return string.Format("{0}", _rs);
        }


        public WFD02640SearchModel GetInitialDataByDocNo(string DocNo)
        {
        
               List<SqlParameter> param = new List<SqlParameter>();
              param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = (object)DocNo ?? DBNull.Value });

            return executeDataToList<WFD02640SearchModel>("sp_WFD02640_GetInitialDataByDocNo", param).FirstOrDefault();
        }
            

        public WFD02640ScreenModel GetScreenOnload(WFD02640SearchModel data)
        {
            WFD02640ScreenModel ScreenModel = new WFD02640ScreenModel();

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@ISFAADMIN", SqlDbType.VarChar) { Value = (object)data.ISFAADMIN ?? DBNull.Value });
            param.Add(new SqlParameter("@SV_CODE", SqlDbType.VarChar) { Value = (object)data.SV_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOCUMENT ?? DBNull.Value });
            DataSet ds = executeDataToDataSet("sp_WFD02640_GetDetailStockTakingMonitoringDetail", param);
            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {         
                ScreenModel.SummaryTime = ds.Tables[0].ToList<WFD02640SummaryTimeModel>().First();
            }

            if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
            {          
                ScreenModel.SummaryAsset = ds.Tables[1].ToList<WFD02640SummaryAssetModel>().First();
            }

            if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
            {     
                ScreenModel.SummaryDetail = ds.Tables[2].ToList<WFD02640SummaryDetailModel>();
            }

            return ScreenModel;
        }
        public void SaveNoticaEmail(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.NVarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.NVarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.NVarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@SV_CODE", SqlDbType.VarChar) { Value = (object)data.SV_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });            
            execute("sp_WFD02640_NoticeEmail", param);
        }

        public void SetInitialDataIncaseReSubmit(string doc_no)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DocNo", SqlDbType.NVarChar) { Value = (object)doc_no ?? DBNull.Value });
            execute("sp_WFD02640_InitialDataIncaseReSubmit", param);
        }

        public void SaveTempRequestStockDAO(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOCUMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = data.COST_CENTER });
            param.Add(new SqlParameter("@COMMENT", SqlDbType.VarChar) { Value = (object)data.COMMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@ATTACHMENT", SqlDbType.VarChar) { Value = (object)data.ATTACHMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPLETE_FLAG", SqlDbType.VarChar) { Value = (object)data.COMPLETE_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@SELECTED", SqlDbType.VarChar) { Value = (object)data.SELECTED ?? DBNull.Value });
            param.Add(new SqlParameter("@SV_EMP_CODE", SqlDbType.VarChar) { Value = (object)data.SV_CODE ?? DBNull.Value });
            execute("sp_WFD02640_Insert_Temp_RequestStock", param);
        }

        public void UpdateAssetList(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOCUMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = data.COST_CENTER });
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPLETE_FLAG", SqlDbType.VarChar) { Value = (object)data.COMPLETE_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@SELECTED", SqlDbType.VarChar) { Value = (object)data.SELECTED ?? DBNull.Value });
            param.Add(new SqlParameter("@SV_EMP_CODE", SqlDbType.VarChar) { Value = (object)data.SV_CODE ?? DBNull.Value });

            execute("sp_WFD02640_UpdateAssetList", param);
        }

        public void InsertAssetList(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOCUMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@SELECTED", SqlDbType.VarChar) { Value = (object)data.SELECTED ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });

            execute("sp_WFD02640_InsertAssetList", param);
        }
        public void ClearAssetList(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOCUMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            execute("sp_WFD02640_ClearAssetList", param);
        }


        
        public void UpdateFileToTempStock(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOCUMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CENTER ?? DBNull.Value });
            param.Add(new SqlParameter("@ATTACHMENT", SqlDbType.VarChar) { Value = (object)data.ATTACHMENT ?? DBNull.Value });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
            execute("sp_WFD02640_UpdateFileToTempStock", param);
        }
        //public List<MessageModel> ValidationBeforeApprove(WFD02640Model data)
        //{
        //    List<MessageModel> datas = new List<MessageModel>();
        //    try
        //    {
        //        string _guid = data.GUID;
        //        if (string.IsNullOrEmpty(_guid))
        //            _guid = data.DOC_NO;
        //       // error here
        //        List<SqlParameter> param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? GetTempDocNo(data.USER_BY, this.RequestType) });
        //        var _list = executeDataToList<WFD02640SearchModel>("sp_WFD02640_GetStockTakingRequest", param);

        //        //Delete Assets from temp
        //        param.Clear();
        //        param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _guid });
        //        execute("sp_WFD02640_ClearSelectedAssetList", param);
        //        foreach (var _item in _list)
        //        {
        //            this.InsertAssetList(_item);
        //        }

        //        //param.Clear();
        //        //param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _guid });
        //        //datas = executeDataToList<MessageModel>("sp_WFD01270_ValidationAsset", param);


        //        return datas;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        //public void Reject(WFD02640Model data)
        //{
        //    try
        //    {
        //          List<SqlParameter> param = new List<SqlParameter>();
        //          param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });

        //        execute("sp_WFD02640_RejectStockTaking", param);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        //public void RejectStockTaking(WFD01270MainModel _data, WFD02640Model _dataStockTaking, string _Action, bool _submit)
        //{
        //    beginTransaction();
        //    try
        //    {
        //        //Reject Request
        //        List<SqlParameter> param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DOC_NO ?? DBNull.Value });
        //        param.Add(new SqlParameter("@APPROVE_BY", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.EMP_CODE ?? DBNull.Value });
        //        param.Add(new SqlParameter("@DELEGATE", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.DELEGATE ?? DBNull.Value });
        //        param.Add(new SqlParameter("@FAADMIN", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.FA ?? DBNull.Value });
        //        param.Add(new SqlParameter("@HIGHER", SqlDbType.VarChar) { Value = (object)_data.RequestHeaderData.HIGHER ?? DBNull.Value });
        //        param.Add(new SqlParameter("@ACTION", SqlDbType.VarChar) { Value = _Action });
        //        param.Add(new SqlParameter("@Comment", SqlDbType.VarChar) { Value = _data.CommentInfo.CommentText });
        //        param.Add(new SqlParameter("@AttachFile", SqlDbType.VarChar) { Value = _data.CommentInfo.AttachFileName });
        //        param.Add(new SqlParameter("@SubmitFlag", SqlDbType.VarChar) { Value = _submit ? "Y" : "N" });
        //        execute("sp_WFD01270_ApproveRequest", param);

        //        //Reject Asset
        //        param = new List<SqlParameter>();
        //        param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = (object)_dataStockTaking.DOC_NO ?? DBNull.Value });
        //        execute("sp_WFD02640_RejectStockTaking", param);

        //        commitTransaction();
        //        closeConnection();
        //    }
        //    catch (Exception ex)
        //    {
        //        rollbackTransaction();
        //        throw (ex);
        //    }
        //}
        public void PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID }); //Submit mode
            param.Add(new SqlParameter("@ACT_ROLE", SqlDbType.VarChar) { Value = data.ACT_ROLE }); //Submit mode
            execute("sp_WFD02640_PrepareGenerateFlow", param);

        }
        public void Approve(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Approve(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02640_ApproveRequest", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Reject(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02640_RejectRequest", param);

              
                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public string ValidateSubmitIsRequireComment(WFD02640Model data)
        {
           
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });
  
               

              string _str = executeScalar<string>("sp_WFD02640_ValidateSubmitStockTaking", param);
                if(!string.IsNullOrEmpty(_str))
                {
                    return _str;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return string.Empty;
        }

        public string ValidateSubmitIsRequireCommentWithDelay(WFD02640Model data)
        {

            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DocNo", SqlDbType.VarChar) { Value = (object)data.DOC_NO ?? DBNull.Value });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = (object)data.GUID ?? DBNull.Value });

                string _str = executeScalar<string>("sp_WFD02640_ValidateSubmitStockTakingWithDelay", param);
                if (!string.IsNullOrEmpty(_str))
                {
                    return _str;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return string.Empty;
        }

        public void Submit(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Submit(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Free function
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER_BY", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02640_SubmitRequest", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);


            }
        }
     
        public string GetSpecialPermission(WFD02640SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            var _dt = executeDataToDataTable("sp_WFD02640_GetActiveStatusPerSV", param);

            if (_dt == null || _dt.Rows.Count == 0)
                return "Y"; //Show view Mode

            return string.Format("{0}", _dt.Rows[0]["COMPLETE_FLAG"]);



         
        }


        public bool CheckCreateRequestStock(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION, string EMP_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            DataTable dt = executeDataToDataTable("sp_WFD02640_CheckCreateRequestStockPerSV", param);
            bool result = false;
            if (dt.Rows[0]["RESULT"].ToString() == "True")
            {
                return true;
            }

            return result;
        }
        
    }
}
