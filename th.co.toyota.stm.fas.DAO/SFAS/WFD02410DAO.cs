﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.WFD02410;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.COMSCC;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02410DAO : RequestDetailDAO
    {
        public void InitialAssetList(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });
            execute("sp_WFD02410_InitialAssetList", param);
        }
        public List<WFD02410NewAssetOwnerModel> GetNewAssetOwnerTransfer(WFD02410HeaderModel data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) });
                param.Add(new SqlParameter("@REQUEST_EMP_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.REQUEST_EMP_CODE) });
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@NEW_COST_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.NEW_COST_CODE) });
                param.Add(new SqlParameter("@NEW_RESP_COST_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.NEW_RESP_COST_CODE) });

                var datas = executeDataToList<WFD02410NewAssetOwnerModel>("sp_WFD02410_GetNewAssetOwner", param);

                return datas;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<WFD02410Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD02410Model>("sp_WFD02410_GetAssetList", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());



            return datas;
        }

        public void ClearAssetList(WFD0BaseRequestDocModel data)
        {
            // Allow only Submit 
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            execute("sp_WFD02410_ClearAssetList", param);
        }
		public List<MessageModel> AssetValidation(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02410_ValidationAsset", param);
        }
        public List<MessageModel> GenerateFlowValidation(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02410_GenerateFlowValidation", param);
        }
        //
        public List<MessageModel> SubmitValidation(WFD0BaseRequestDocModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
            return executeDataToList<MessageModel>("sp_WFD02410_SubmitValidation", param);
        }
        public List<MessageModel> ApproveValidation(WFD01170MainModel _data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
            param.Add(new SqlParameter("@ACT_MARK", SqlDbType.VarChar) { Value = base.IfNull(_data.UserPermissionData.ActionMark) });
            return executeDataToList<MessageModel>("sp_WFD02410_ApproveValidation", param);

        }
	

        public void InsertAsset(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(data.USER_BY) });

                execute("sp_WFD02410_InsertAsset", param);


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            try
            {

                List<SqlParameter> param = new List<SqlParameter>();

                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(data.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });


                execute("sp_WFD02410_DeleteAsset", param);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            //
            List<SqlParameter> param = new List<SqlParameter>();

            param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = data.GUID }); //Submit mode
            param.Add(new SqlParameter("@ACT_ROLE", SqlDbType.VarChar) { Value = data.ACT_ROLE }); //Submit mode
            param.Add(new SqlParameter("@FLOW_TYPE", SqlDbType.VarChar) { Value = data.FLOW_TYPE });


            execute("sp_WFD02410_PrepareGenerateFlow", param);

        }

        
        public void UpdateAsset(WFD02410HeaderModel _header, string _user)
        {
            try
            {
                //Header First 

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_header.GUID) }); //Submit mode
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_header.DOC_NO) }); //approve mode
                param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(_header.COMPANY) });
                param.Add(new SqlParameter("@NEW_COST_CODE", SqlDbType.VarChar) { Value = base.IfNull(_header.NEW_COST_CODE) });
                param.Add(new SqlParameter("@NEW_RESP_COST_CODE", SqlDbType.VarChar) { Value = base.IfNull(_header.NEW_RESP_COST_CODE) });
                param.Add(new SqlParameter("@FLOW_TYPE", SqlDbType.VarChar) { Value = base.IfNull(_header.FLOW_TYPE) });
                param.Add(new SqlParameter("@TRNF_TYPE", SqlDbType.VarChar) { Value = base.IfNull(_header.TRNF_TYPE) });

                param.Add(new SqlParameter("@RECEIVER", SqlDbType.VarChar) { Value = base.IfNull(_header.RECEIVER) });
                param.Add(new SqlParameter("@TEL_NO", SqlDbType.VarChar) { Value = base.IfNull(_header.TEL_NO) });

                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_user) });
                execute("sp_WFD02410_UpdateTransferHeader", param);

                if (_header.DetailList == null)
                    return;

                foreach (var data in _header.DetailList)
                {

                    param = new List<SqlParameter>();
                    param.Clear();
                    param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(data.GUID) }); //Submit mode
                    param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
                    param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_NO) });
                    param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = base.IfNull(data.ASSET_SUB) });
                    param.Add(new SqlParameter("@TRNF_REASON", SqlDbType.VarChar) { Value = base.IfNull(data.TRNF_REASON) });
                    param.Add(new SqlParameter("@TRNF_REASON_OTHER", SqlDbType.VarChar) { Value = base.IfNull(data.TRNF_REASON_OTHER) });
                    param.Add(new SqlParameter("@TRNF_EFFECTIVE_DT", SqlDbType.VarChar) { Value = base.IfNull(data.TRNF_EFFECTIVE_DT) });
                    param.Add(new SqlParameter("@PLANT", SqlDbType.VarChar) { Value = base.IfNull(data.PLANT) });
                    param.Add(new SqlParameter("@LOCATION_CD", SqlDbType.VarChar) { Value = base.IfNull(data.LOCATION_CD) });
                    param.Add(new SqlParameter("@FUTURE_USE", SqlDbType.VarChar) { Value = base.IfNull(data.FUTURE_USE) });
                    param.Add(new SqlParameter("@WBS_PROJECT", SqlDbType.VarChar) { Value = base.IfNull(data.WBS_PROJECT) });
                    param.Add(new SqlParameter("@TO_EMPLOYEE", SqlDbType.VarChar) { Value = base.IfNull(data.TO_EMPLOYEE) });
                    param.Add(new SqlParameter("@LOCATION_DETAIL", SqlDbType.VarChar) { Value = base.IfNull(data.LOCATION_DETAIL) });
                    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_user) });
                    execute("sp_WFD02410_UpdateAsset", param);


                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public void Submit(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Submit(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Free function
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02410_SubmitRequest", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@FuncID", SqlDbType.VarChar) { Value = Constants.BatchID.WFD02410 });
                execute("sp_WFD01170_InitialDefaultBOIDocument", param);

                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = _data.RequestHeaderData.GUID });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = _data.RequestHeaderData.USER_BY });
                execute("sp_WFD02410_SubmitwithApproveAsset", param);
                
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02410_ClearAssetList", param);

               

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Reject(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02410_RejectRequest", param);
				//If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02410_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }
        
        public void CopyToNewTransfer(WFD01170MainModel _data)
        {

            try
            {
                beginTransaction();

                base._Reject(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02410_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02410_ClearAssetList", param);

                param.Clear();
                //Document No
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@NEW_GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.NewGuid) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02410_CopyToNewTransfer", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Approve(WFD01170MainModel _data)
        {
            try
            {
                beginTransaction();

                base._Approve(_data);

                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02410_ApproveRequest", param);

                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02410_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public void Acknowledge(WFD01170MainModel _data,string _status)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_data, _status);


                List<SqlParameter> param = new List<SqlParameter>();
                //Document No
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.DOC_NO) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.USER_BY) });
                execute("sp_WFD02410_RejectRequest", param);
                //If complete
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.RequestHeaderData.GUID) });
                execute("sp_WFD02410_ClearAssetList", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        public List<WBSMasterAutoCompleteModels> GetDropdownWBSCosProject(WFD02410Model data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            return executeDataToList<WBSMasterAutoCompleteModels>("sp_WFD02410_GetWBSCosProject", param);
        }
        public List<SystemModel> GetDropdownToEmployee(WFD02410NewAssetOwnerModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COST_CENTER", SqlDbType.VarChar) { Value = base.IfNull(data.COST_CODE) });
            param.Add(new SqlParameter("@COMPANY_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            return executeDataToList<SystemModel>("sp_WFD02410_GetToEmployee", param);
        }
        public List<SystemModel> GetDropdownLocation(WFD02410Model data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY_CODE", SqlDbType.VarChar) { Value = base.IfNull(data.COMPANY) });
            return executeDataToList<SystemModel>("sp_WFD02410_GetLocation", param);
        }

        public void Resend(WFD01170MainModel _header, WFD01170SelectedAssetNoModel _data, string _User)
        {
            try
            {
                beginTransaction();

                base._UpdateRequestStatus(_header, Constants.DocumentStatus.Resend);

                List<SqlParameter> param = new List<SqlParameter>();
              
                param.Clear();
                param.Add(new SqlParameter("@GUID", SqlDbType.VarChar) { Value = base.IfNull(_data.GUID) });
                param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
                param.Add(new SqlParameter("@ASSET_LIST", SqlDbType.VarChar) { Value = base.IfNull(_data.SelectedList) });
                param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER) });

                execute("sp_WFD02410_Resend", param);

                commitTransaction();
                closeConnection();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw (ex);
            }
        }

        //public string IsRequireUploadTMC(WFD0BaseRequestDocModel _data )
        //{
        //    List<SqlParameter> param = new List<SqlParameter>();
        //    param.Add(new SqlParameter("@DOC_NO", SqlDbType.VarChar) { Value = base.IfNull(_data.DOC_NO) });
        //    param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = base.IfNull(_data.USER_BY) });
        //    return executeScalar<string>("sp_WFD02410_IsRequireUploadTMC", param);

           
        //}

        public List<COMSCCModel> SearchCostCode(COMSCCDefaultScreenModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });
            
            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });

            param.Add(TotalItem);

            var datas = executeDataToList<COMSCCModel>("sp_WFD02410_SearchDestinationCostCenter", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
        public List<COMSCCModel> SearchResponseCostCode(COMSCCDefaultScreenModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@FIND", SqlDbType.VarChar) { Value = (object)data.FIND ?? DBNull.Value });

            param.Add(new SqlParameter("@pageNum", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@pageSize", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@sortColumnName", SqlDbType.VarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@orderType", SqlDbType.VarChar) { Value = (object)page.OrderType ?? DBNull.Value });

            param.Add(TotalItem);

            var datas = executeDataToList<COMSCCModel>("sp_WFD02410_SearchDestinationRespCostCenter", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }
    }
}
