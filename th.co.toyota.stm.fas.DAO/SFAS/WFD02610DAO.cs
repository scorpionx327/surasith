﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using System.Configuration;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD02610;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.DAO
{
    public class WFD02610DAO : Database
    {
        public WFD02610DAO() : base()
        {
            
        }

        public WFD02610DefaultScreenModel GetDefaultScreenData(WFD02610DefaultScreenModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            return executeDataToList<WFD02610DefaultScreenModel>("sp_WFD02610_getDefaultScreenData", param).First();
        }

        public WFD02610FinishPlanModel GetFinishPlanData(WFD02610SearchModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });

            return executeDataToList<WFD02610FinishPlanModel>("sp_WFD02610_getFinishPlanData", param).First();
        }

        public List<WFD02610SearchModel> Search(string UserLogin , string IsFAAdmin , WFD02610DefaultScreenModel data, ref PaginationModel page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            param.Add(new SqlParameter("@USER_LOGIN", SqlDbType.VarChar) { Value = (object)UserLogin  ?? DBNull.Value });
            param.Add(new SqlParameter("@ISFAADMIN", SqlDbType.VarChar) { Value = (object)IsFAAdmin ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_TYPE", SqlDbType.NVarChar) { Value = (object)page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var datas = executeDataToList<WFD02610SearchModel>("sp_WFD02610_SearchStockTakeRequestData", param);

            page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return datas;
        }

        public void FinishPlan(WFD02610FinishPlanConditionModel data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@REMARK", SqlDbType.NVarChar) { Value = (object)data.REMARK ?? DBNull.Value });
            param.Add(new SqlParameter("@TOTAL_NOT_CHECK_ASSETS", SqlDbType.Int) { Value = (object)data.TOTAL_NOT_CHECK_ASSETS ?? DBNull.Value });
            param.Add(new SqlParameter("@USER", SqlDbType.VarChar) { Value = (object)data.USER ?? DBNull.Value });

            execute("sp_WFD02610_FinishPlan", param);
        }

        public bool IsApprovedAllCostCenter(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)ASSET_LOCATION ?? DBNull.Value });

            DataTable dt = executeDataToDataTable("sp_WFD02610_IsApprovedAllCostCenter", param);
            return int.Parse(dt.Rows[0]["RESULT"].ToString()) == 1 ? true : false;
        }

        public void AddStockExportData(TB_T_STOCK_EXPORT data, decimal _aplID)
        {
            List<SqlParameter> param = new List<SqlParameter>();

        
            param.Add(new SqlParameter("@BATCH_ID", SqlDbType.NVarChar) { Value = (object)data.BATCH_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@YEAR", SqlDbType.VarChar) { Value = (object)data.YEAR ?? DBNull.Value });
            param.Add(new SqlParameter("@ROUND", SqlDbType.VarChar) { Value = (object)data.ROUND ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.VarChar) { Value = (object)data.ASSET_LOCATION ?? DBNull.Value });
            param.Add(new SqlParameter("@APL_ID", SqlDbType.Decimal) { Value = (object)_aplID ?? DBNull.Value });

            execute("sp_WFD02610_AddStockExportData", param);

            
        }
        public decimal GetNewAPL_IDForExportToTemp()
        {
            return executeScalar<decimal>("sp_WFD02610_GetNewAPL_ID");
        }
    }
}