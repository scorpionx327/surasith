﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.DAO.SFAS
{
    public class BFD02670DAO : Database
    {
        public BFD02670DAO() : base() { }
        
        public void UpdateStockTake_H_Plan_Status(string STOCK_TAKE_KEY, string EMP_CODE, string PLAN_STATUS)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@PLAN_STATUS", SqlDbType.VarChar) { Value = (object)PLAN_STATUS ?? DBNull.Value });

            execute("sp_BFD02670_UpdateStockTake_H_Plan_Status", param);
        }

        public void UpdateStockTake_D(TB_R_STOCK_TAKE_D data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", SqlDbType.VarChar) { Value = (object)data.COMPANY ?? DBNull.Value });
            param.Add(new SqlParameter("@STOCK_TAKE_KEY", SqlDbType.VarChar) { Value = (object)data.STOCK_TAKE_KEY ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_NO", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@ASSET_SUB", SqlDbType.VarChar) { Value = (object)data.ASSET_SUB ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@CHECK_STATUS", SqlDbType.VarChar) { Value = (object)data.CHECK_STATUS ?? DBNull.Value });
            param.Add(new SqlParameter("@SCAN_DATE", SqlDbType.DateTime) { Value = (object)data.SCAN_DATE ?? DBNull.Value });
            param.Add(new SqlParameter("@START_COUNT_TIME", SqlDbType.DateTime) { Value = (object)data.START_COUNT_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@END_COUNT_TIME", SqlDbType.DateTime) { Value = (object)data.END_COUNT_TIME ?? DBNull.Value });
            param.Add(new SqlParameter("@COUNT_TIME", SqlDbType.Int) { Value = (object)data.COUNT_TIME ?? DBNull.Value });

            execute("sp_WFD02650_UpdateStockTakeDScanData", param);
        }

        public void AddStockTakeInvalidScanData(TB_R_STOCK_TAKE_INVALID_CHECKING data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@BARCODE", SqlDbType.VarChar) { Value = (object)data.ASSET_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@EMP_CODE", SqlDbType.VarChar) { Value = (object)data.EMP_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@COST_CODE", SqlDbType.VarChar) { Value = (object)data.COST_CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@SCAN_DATE", SqlDbType.DateTime) { Value = (object)data.SCAN_DATE ?? DBNull.Value });

            execute("sp_WFD02650_AddInvalidStockTakeScan", param);
        }
    }
}