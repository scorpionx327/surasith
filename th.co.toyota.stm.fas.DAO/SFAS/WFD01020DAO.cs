﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.SFAS.WFD01020;
using th.co.toyota.stm.fas.Models.Shared;

namespace th.co.toyota.stm.fas.DAO.SFAS
{
    public class WFD01020DAO : Database
    {
        public WFD01020DAO() : base()
        {

        }

        public List<WFD01020ComboBoxValueModel> GetCategory()
        {
            return executeDataToList<WFD01020ComboBoxValueModel>("sp_WFD01020_LoadSystemMasterCategory");
        }

        public List<WFD01020ComboBoxValueModel> GetSubCategory(string category)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)category ?? DBNull.Value });
            return executeDataToList<WFD01020ComboBoxValueModel>("sp_WFD01020_LoadSystemMasterSubCategory", param);
        }

        public List<WFD01020DataModel> SearchSystemMaster(string Category, string SubCategory, ref PaginationModel Page)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            SqlParameter TotalItem = new SqlParameter("@TOTAL_ITEM", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
           
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)Category ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SubCategory ?? DBNull.Value });           
            param.Add(new SqlParameter("@CURRENCT_PAGE", SqlDbType.Int) { Value = (object)Page.CurrentPage ?? DBNull.Value });
            param.Add(new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = (object)Page.ItemPerPage ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_BY", SqlDbType.NVarChar) { Value = (object)Page.OrderColIndex ?? DBNull.Value });
            param.Add(new SqlParameter("@ORDER_TYPE", SqlDbType.VarChar) { Value = (object)Page.OrderType ?? DBNull.Value });
            param.Add(TotalItem);

            var result = executeDataToList<WFD01020DataModel>("sp_WFD01020_SearchSystemMaster", param);
            Page.Totalitem = int.Parse(TotalItem.Value.ToString());

            return result;
        }

        public WFD01020DataModel GetSystemMaster(WFD01020DataModel data)
        {          
            List <SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)data.CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)data.SUB_CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)data.CODE ?? DBNull.Value });
            return executeDataToList<WFD01020DataModel>("sp_WFD01020_GetSystemMaster", param).FirstOrDefault();
        }

        public void InsertSystemMaster(WFD01020DataModel data)
        {
            if (string.IsNullOrWhiteSpace(data.REMARKS))
            {
                data.REMARKS = "";
            }

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)data.CATEGORY.ToUpper().Trim() });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)data.SUB_CATEGORY.Trim() });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)data.CODE.Trim() });
            param.Add(new SqlParameter("@VALUE", SqlDbType.VarChar) { Value = (object)data.VALUE.Trim() });
            param.Add(new SqlParameter("@REMARKS", SqlDbType.VarChar) { Value = (object)data.REMARKS.Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@ACTIVE_FLAG", SqlDbType.Char) { Value = (object)data.ACTIVE_FLAG });        
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY });          
            execute("sp_WFD01020_InsertSystemMaster", param);
        }

        public void UpdateSystemMaster(WFD01020DataModel data)
        {
            if (string.IsNullOrWhiteSpace(data.REMARKS))
            {
                data.REMARKS = "";
            }

            List <SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)data.CATEGORY.ToUpper().Trim() });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)data.SUB_CATEGORY.Trim() });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)data.CODE.Trim() });
            param.Add(new SqlParameter("@VALUE", SqlDbType.VarChar) { Value = (object)data.VALUE.Trim() });
            param.Add(new SqlParameter("@REMARKS", SqlDbType.VarChar) { Value = (object)data.REMARKS.Trim() ?? DBNull.Value });
            param.Add(new SqlParameter("@ACTIVE_FLAG", SqlDbType.VarChar) { Value = (object)data.ACTIVE_FLAG });                   
            param.Add(new SqlParameter("@UPDATE_BY", SqlDbType.VarChar) { Value = (object)data.UPDATE_BY });
            execute("sp_WFD01020_UpdateSystemMaster", param);
        }

        public DataTable GetSystemMasterList(string _CATEGORY, string _SUB_CATEGORY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = _CATEGORY });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = _SUB_CATEGORY });
            DataTable dt = new DataTable();
            dt = executeDataToDataTable("sp_WFD01020_GetSystemMasterList", param);
            return dt;
        }
    }
}
