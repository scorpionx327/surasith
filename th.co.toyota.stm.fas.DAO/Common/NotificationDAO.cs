﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.DAO.Common
{
    public class NotificationDAO : Database
    {
        public NotificationDAO() : base() { }

        public void InsertDatas(List<TB_R_NOTIFICATION> datas)
        {
            beginTransaction();
            try
            {
                foreach (var d in datas) InsertData(d);

                commitTransaction();
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw ex;
            }
        }

        public void InsertData(TB_R_NOTIFICATION data)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@TO", SqlDbType.VarChar) { Value = (object)data.TO ?? DBNull.Value });
            param.Add(new SqlParameter("@CC", SqlDbType.VarChar) { Value = (object)data.CC ?? DBNull.Value });
            param.Add(new SqlParameter("@BCC", SqlDbType.VarChar) { Value = (object)data.BCC ?? DBNull.Value });
            param.Add(new SqlParameter("@TITLE", SqlDbType.VarChar) { Value = (object)data.TITLE ?? DBNull.Value });
            param.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar) { Value = (object)data.MESSAGE ?? DBNull.Value });
            param.Add(new SqlParameter("@FOOTER", SqlDbType.VarChar) { Value = (object)data.FOOTER ?? DBNull.Value });
            param.Add(new SqlParameter("@TYPE", SqlDbType.VarChar) { Value = (object)data.TYPE ?? DBNull.Value });
            param.Add(new SqlParameter("@START_PERIOD", SqlDbType.DateTime) { Value = (object)data.START_PERIOD ?? DBNull.Value });
            param.Add(new SqlParameter("@END_PERIOD", SqlDbType.DateTime) { Value = (object)data.END_PERIOD ?? DBNull.Value });
            param.Add(new SqlParameter("@SEND_FLAG", SqlDbType.VarChar) { Value = (object)data.SEND_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@RESULT_FLAG", SqlDbType.VarChar) { Value = (object)data.RESULT_FLAG ?? DBNull.Value });
            param.Add(new SqlParameter("@REF_FUNC", SqlDbType.VarChar) { Value = (object)data.REF_FUNC ?? DBNull.Value });
            param.Add(new SqlParameter("@REF_DOC_NO", SqlDbType.VarChar) { Value = (object)data.REF_DOC_NO ?? DBNull.Value });
            param.Add(new SqlParameter("@CREATE_BY", SqlDbType.VarChar) { Value = (object)data.CREATE_BY ?? DBNull.Value });

            execute("sp_Common_InsertNotification", param);
        }
    }
}
