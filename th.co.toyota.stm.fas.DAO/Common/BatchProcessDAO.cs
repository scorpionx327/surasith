﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.DAO.Common
{
    public class BatchProcessDAO : Database 
    {
        public BatchProcessDAO() : base() { }

        public decimal insertBatchQ(StartBatchModel data)
        {
            beginTransaction();
            try
            {
                decimal appId = _insertBatchQ(data.BatchID, data.UserId, data.Description);
                if (data.Parameters != null && data.Parameters.Length > 0)
                {
                    for (int i = 0; i < data.Parameters.Length; i++)
                    {
                        _insertBatchParameter(appId, data.Parameters[i].ParamSeq, data.Parameters[i].ParamName, data.Parameters[i].ParamValue);
                    }
                }
                commitTransaction();
                return appId;
            }
            catch (Exception ex)
            {
                rollbackTransaction();
                throw ex;
            }
        }
        public decimal insertBatchNoTransaction(StartBatchModel data)
        {
            //beginTransaction();
            try
            {
                decimal appId = _insertBatchQ(data.BatchID, data.UserId, data.Description);
                if (data.Parameters != null && data.Parameters.Length > 0)
                {
                    for (int i = 0; i < data.Parameters.Length; i++)
                    {
                        _insertBatchParameter(appId, data.Parameters[i].ParamSeq, data.Parameters[i].ParamName, data.Parameters[i].ParamValue);
                    }
                }
                //commitTransaction();
                return appId;
            }
            catch (Exception ex)
            {
                //rollbackTransaction();
                throw ex;
            }
        }
        private decimal _insertBatchQ(string BatchID, string ReqBy, string Description)
        {
            string Status = "Q";
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@BatchID", SqlDbType.VarChar) { Value = (object)BatchID ?? DBNull.Value });
            param.Add(new SqlParameter("@ReqBy", SqlDbType.VarChar) { Value = (object)ReqBy ?? DBNull.Value });
            param.Add(new SqlParameter("@Status", SqlDbType.VarChar) { Value = (object)Status ?? DBNull.Value });
            param.Add(new SqlParameter("@Description", SqlDbType.VarChar) { Value = (object)Description ?? DBNull.Value });

            return executeScalar<decimal>("sp_Common_InsertBatchQ", param);
        }
        private void _insertBatchParameter(decimal P_APP_ID, int? P_PARAM_SEQ, string P_PARAM_NAME, string P_PARAM_VALUE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@P_APP_ID", SqlDbType.Decimal) { Value = (object)P_APP_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@P_PARAM_SEQ", SqlDbType.Int) { Value = (object)P_PARAM_SEQ ?? DBNull.Value });
            param.Add(new SqlParameter("@P_PARAM_NAME", SqlDbType.NVarChar) { Value = (object)P_PARAM_NAME ?? DBNull.Value });
            param.Add(new SqlParameter("@P_PARAM_VALUE", SqlDbType.NVarChar) { Value = (object)P_PARAM_VALUE ?? DBNull.Value });
            execute("sp_Common_InsertBatchQParam", param);
        }

        public decimal GetUserBatchAppId(string BATCH_ID, string USER_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@BATCH_ID", SqlDbType.VarChar) { Value = (object)BATCH_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar) { Value = (object)USER_ID ?? DBNull.Value });

            DataTable dt = executeDataToDataTable("sp_common_GetUserBatchWorkingAppId", param);
            if (dt != null && dt.Rows.Count > 0) return decimal.Parse(dt.Rows[0]["APP_ID"].ToString());
            else return -1;
        }

        public BatchMessageStatusData GetBatchStatus(decimal APP_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APP_ID", SqlDbType.Decimal) { Value = (object)APP_ID ?? DBNull.Value });
            return executeDataToList<BatchMessageStatusData>("sp_common_GetBatchStatus", param).First();
        }

        public void InsertLog(decimal APP_ID,  string STATUS, string LEVEL, string MESSAGE, string UserId)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APP_ID", SqlDbType.Decimal) { Value = (object)APP_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@STATUS", SqlDbType.VarChar) { Value = (object)STATUS ?? DBNull.Value });
            param.Add(new SqlParameter("@FAVORITE_FLAG", SqlDbType.VarChar) { Value = "N" });
            param.Add(new SqlParameter("@LEVEL", SqlDbType.VarChar) { Value = (object)LEVEL ?? DBNull.Value });
            param.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar) { Value = (object)MESSAGE ?? DBNull.Value });
            param.Add(new SqlParameter("@User", SqlDbType.VarChar) { Value = (object)UserId ?? DBNull.Value });
            execute("sp_Common_InsertDetailLog", param);
        }

        public void UpdateBatchStatus(decimal APP_ID, string STATUS)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@ReqID", SqlDbType.Decimal) { Value = (object)APP_ID ?? DBNull.Value });
            param.Add(new SqlParameter("@Status", SqlDbType.Char) { Value = (object)STATUS ?? DBNull.Value });

            execute("sp_Common_UpdateBatchQ", param);
        }

        public  string GetODBBatchCallingPath()
        {
            List<SqlParameter> param = new List<SqlParameter>();

            return executeScalar<string>("sp_Common_GetODBBatchCallingPath", param);
        }

        public int GetBatchProcessId(decimal APP_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APP_ID", SqlDbType.Decimal) { Value = (object)APP_ID ?? DBNull.Value });
            var dt = executeDataToDataTable("sp_Common_GetBatchProcessId", param);

            if (dt != null && dt.Rows.Count > 0)
            {
                int n;
                string pid = dt.Rows[0]["PID"].ToString();
                if (int.TryParse(pid, out n))
                    return int.Parse(pid);
            }

            return -1;
        }
    }
}