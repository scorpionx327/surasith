﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.DAO.Common
{
    public class MessageDAO : Database
    {
        public MessageDAO() : base()
        {

        }

        public MessageModel GetMessage(string MESSAGE_CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@MESSAGE_CODE", SqlDbType.VarChar) { Value = (object)MESSAGE_CODE ?? DBNull.Value });
            return executeDataToList<MessageModel>("sp_Common_GetMessage", param).First();
        }
    }
}