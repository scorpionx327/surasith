﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.BaseModel;

namespace th.co.toyota.stm.fas.DAO.Common
{
    public class SystemDAO : Database
    {
        public SystemDAO() : base()
        {
        }

        public List<SystemModel> SelectSystemDatas(SYSTEM_CATEGORY CATEGORY, SYSTEM_SUB_CATEGORY SUB_CATEGORY)
        {
            return SelectSystemDatas(CATEGORY.ToString(), SUB_CATEGORY.ToString());
        }
        public List<SystemModel> SelectSystemDatas(SYSTEM_CATEGORY CATEGORY, string SUB_CATEGORY)
        {
            return SelectSystemDatas(CATEGORY.ToString(), SUB_CATEGORY);
        }
        public List<SystemModel> SelectSystemDatas(string CATEGORY, string SUB_CATEGORY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_Common_GetSystem", param);
        }

        public List<SystemModel> SelectSystemDatasOrderByCode(string CATEGORY, string SUB_CATEGORY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_Common_GetSystemOrderByCode", param);
        }

        public List<SystemModel> SelectSystemDatasOrderByRemark(string CATEGORY, string SUB_CATEGORY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_Common_GetSystemOrderByRemark", param);
        }
        public SystemModel SelectSystemValue(SYSTEM_CATEGORY CATEGORY, SYSTEM_SUB_CATEGORY SUB_CATEGORY, string CODE)
        {
            return SelectSystemValue(CATEGORY.ToString(), SUB_CATEGORY.ToString(), CODE);
        }
        public SystemModel SelectSystemValue(SYSTEM_CATEGORY CATEGORY, string SUB_CATEGORY, string CODE)
        {
            return SelectSystemValue(CATEGORY.ToString(), SUB_CATEGORY, CODE);
        }
        public SystemModel SelectSystemValue(string CATEGORY, string SUB_CATEGORY, string CODE)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)CODE ?? DBNull.Value });

            return executeDataToList<SystemModel>("sp_Common_GetSystemValues", param).First();
        }

        //Add By Surasith T. 2019-06-23
        public List<SystemModel> GetSystemMasterAutoComplete(string CATEGORY, string SUB_CATEGORY,string CodeFlag, string keyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@CodeFlag", SqlDbType.VarChar) { Value = (object)CodeFlag ?? DBNull.Value });
            param.Add(new SqlParameter("@keyword", SqlDbType.VarChar) { Value = (object)keyword ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_Common_GetSystem_AutoComplete", param);
        }
        public List<SystemModel> GetSystemMasterAutoCompleteWhereRemark(string CATEGORY, string SUB_CATEGORY, string CODE, string CodeFlag, string Remark)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)CODE ?? DBNull.Value });
            param.Add(new SqlParameter("@CodeFlag", SqlDbType.VarChar) { Value = (object)CodeFlag ?? DBNull.Value });
            param.Add(new SqlParameter("@REMARKS", SqlDbType.VarChar) { Value = (object)Remark ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_Common_GetSystemMasterAutoCompleteWhereRemark", param);
        }
        public List<SystemModel> GetAUCAssetClass(string CodeFlag, string Company, string keyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CodeFlag", SqlDbType.VarChar) { Value = (object)CodeFlag ?? DBNull.Value });
            param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = (object)Company ?? DBNull.Value });
            param.Add(new SqlParameter("@keyword", SqlDbType.VarChar) { Value = (object)keyword ?? DBNull.Value });

            return executeDataToList<SystemModel>("sp_Common_GetAUCAssetClass_AutoComplete", param);
        }
        public List<SystemModel> GetFinalAssetClass(string CodeFlag, string Company, string keyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CodeFlag", SqlDbType.VarChar) { Value = (object)CodeFlag ?? DBNull.Value });
            param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = (object)Company ?? DBNull.Value });
            param.Add(new SqlParameter("@keyword", SqlDbType.VarChar) { Value = (object)keyword ?? DBNull.Value });

            return executeDataToList<SystemModel>("sp_Common_GetFinalAssetClass_AutoComplete", param);
        }
        public List<SystemModel> GetAssetClass(string CodeFlag, string Company, string keyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CodeFlag", SqlDbType.VarChar) { Value = (object)CodeFlag ?? DBNull.Value });
            param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = (object)Company ?? DBNull.Value });
            param.Add(new SqlParameter("@keyword", SqlDbType.VarChar) { Value = (object)keyword ?? DBNull.Value });

            return executeDataToList<SystemModel>("sp_Common_GetAssetClass_AutoComplete", param);
        }
        public List<SystemModel> GetMinorCategoryByAssetClass(string CodeFlag, string Company, string AssetClass)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CodeFlag", SqlDbType.VarChar) { Value = (object)CodeFlag ?? DBNull.Value });
            param.Add(new SqlParameter("@Company", SqlDbType.VarChar) { Value = (object)Company ?? DBNull.Value });
            param.Add(new SqlParameter("@AssetClass", SqlDbType.VarChar) { Value = (object)AssetClass ?? DBNull.Value });

            return executeDataToList<SystemModel>("sp_Common_GetMinorCategory", param);
        }
        //
        public List<WBSMasterAutoCompleteModels> GetWBSBudgetAutoComplete(string company, string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@Company", base.IfNull(company)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));

            return executeDataToList<WBSMasterAutoCompleteModels>("sp_Common_GetWBSBudget_AutoComplete", param);
        }
        public List<WBSMasterAutoCompleteModels> GetWBSProjectAutoComplete(string company, string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@Company", base.IfNull(company)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<WBSMasterAutoCompleteModels>("sp_Common_GetWBSProject_AutoComplete", param);
        }
        public List<FixAssetsAutoCompleteModels> GetFixedAssetAutoComplete(string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<FixAssetsAutoCompleteModels>("sp_Common_GetFixedAssets_AutoComplete", param);
        }
        public List<CostCenterAutoCompleteModels> GetCostCenterAutoComplete(string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<CostCenterAutoCompleteModels>("sp_Common_GetCostCenter_AutoComplete", param);
        }
        public List<CostCenterAutoCompleteModels> GetResponseCostCenterAutoComplete(string keyword,  string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<CostCenterAutoCompleteModels>("sp_Common_GetRespCostCenter_AutoComplete", param);
        }

        

        public List<DepreciationModel> SelectDepreciationDatas(string COMPANY, string ASSET_CLASS, string MINOR_CATEGORY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@COMPANY", base.IfNull(COMPANY)));
            param.Add(new SqlParameter("@ASSET_CLASS", base.IfNull(ASSET_CLASS)));
            param.Add(new SqlParameter("@MINOR_CATEGORY", base.IfNull(MINOR_CATEGORY)));
            return executeDataToList<DepreciationModel>("sp_Common_GetDepreciation", param);
        }

        public List<SystemModel> SelectSpecialRoleDatas(string CATEGORY, string SUB_CATEGORY)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)CATEGORY ?? DBNull.Value });
            param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)SUB_CATEGORY ?? DBNull.Value });
            return executeDataToList<SystemModel>("sp_Common_GetSpecialRole", param);
        }

        public List<OrganizationAutoCompleteModels> GetDivisionAutoComplete(string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<OrganizationAutoCompleteModels>("sp_Common_GetDivision_AutoComplete", param);
        }

        public List<OrganizationAutoCompleteModels> GetSubDivistionAutoComplete(string keyword, string empcode, string _divKeyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@DivKeyword", base.IfNull(_divKeyword)));
            return executeDataToList<OrganizationAutoCompleteModels>("sp_Common_GetSubDivision_AutoComplete", param);
        }

        public List<OrganizationAutoCompleteModels> GetDepartmentAutoComplete(string keyword, string empcode, string _subDivKeyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
           param.Add(new SqlParameter("@SubDivKeyword", base.IfNull(_subDivKeyword)));
            return executeDataToList<OrganizationAutoCompleteModels>("sp_Common_GetDepartment_AutoComplete", param);
        }

        public List<OrganizationAutoCompleteModels> GetGetSectionAutoComplete(string keyword, string empcode, string _depKeyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@DepKeyword", base.IfNull(_depKeyword)));

            return executeDataToList<OrganizationAutoCompleteModels>("sp_Common_GetSection_AutoComplete", param);
        }

        public List<OrganizationAutoCompleteModels> GetGetLineAutoComplete(string keyword, string empcode, string _secKeyword)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@SecKeyword", base.IfNull(_secKeyword)));
            return executeDataToList<OrganizationAutoCompleteModels>("sp_Common_GetLine_AutoComplete", param);
        }

        public List<EmployeeAutoCompleteModels> GetGetPositionAutoComplete(string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<EmployeeAutoCompleteModels>("sp_Common_GetPosition_AutoComplete", param);
        }

        public List<EmployeeAutoCompleteModels> GetJobAutoComplete(string keyword, string empcode)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            return executeDataToList<EmployeeAutoCompleteModels>("sp_Common_GetJob_AutoComplete", param);
        }

        public List<TB_M_EMPLOYEE> GetEmployeeAutoComplete(string keyword, string empcode, string company)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@keyword", base.IfNull(keyword)));
            param.Add(new SqlParameter("@EmpCode", base.IfNull(empcode)));
            param.Add(new SqlParameter("@Company", base.IfNull(company)));
            return executeDataToList<TB_M_EMPLOYEE>("sp_Common_Get_Employee_AutoComplete", param);
        }

    }
}