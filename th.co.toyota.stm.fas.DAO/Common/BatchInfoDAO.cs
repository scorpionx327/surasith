﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.DAO.Common
{
    public class BatchInfoDAO : Database 
    {
        public BatchInfoDAO() : base() { }


        public BatchInfoModel GetBatchInfoData(string BATCH_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@BATCH_ID", SqlDbType.VarChar) { Value = (object)BATCH_ID ?? DBNull.Value });
            return executeDataToList<BatchInfoModel>("sp_Common_GetBatchInfo", param).First();
        }
    }
}
