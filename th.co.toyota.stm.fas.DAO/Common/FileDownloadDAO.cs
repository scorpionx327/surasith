﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.DAO.Common
{
    public class FileDownloadDAO : Database
    {
        
        public FileDownloadModel GetFileDownloadFromAppId(decimal APP_ID)
        {
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter("@APP_ID", SqlDbType.Decimal) { Value = (object)APP_ID ?? DBNull.Value });
            return executeDataToList<FileDownloadModel>("sp_Common_GetFileDownloadFromAppId", param).First();
        }
    }
}
