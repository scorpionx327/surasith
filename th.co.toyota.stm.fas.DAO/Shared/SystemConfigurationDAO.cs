﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.Shared;
using System.Data.SqlClient;
using System.Data;
namespace th.co.toyota.stm.fas.DAO.Shared
{
    public class SystemConfigurationDAO : Database
    {
        public SystemConfigurationDAO() : base()
        {

        }      
        public List<SystemConfigurationModels> GetSystemData(SystemConfigurationModels data)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@CATEGORY", SqlDbType.VarChar) { Value = (object)data.CATEGORY ?? DBNull.Value });
                param.Add(new SqlParameter("@SUB_CATEGORY", SqlDbType.VarChar) { Value = (object)data.SUB_CATEGORY ?? DBNull.Value });
                param.Add(new SqlParameter("@CODE", SqlDbType.VarChar) { Value = (object)data.CODE ?? DBNull.Value });

                return executeDataToList<SystemConfigurationModels>("sp_Common_GetSystemValues", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
