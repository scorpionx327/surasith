﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace th.co.toyota.stm.fas.DAO.Shared
{
    public class Database : SqlServerProvider
    {
        public Database() : base(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString)
        {
        }

        public object IfNull(object _x)
        {
            if (_x == null || string.IsNullOrEmpty(string.Format("{0}",_x)))
                return DBNull.Value;

            return _x;
        }
    }
}