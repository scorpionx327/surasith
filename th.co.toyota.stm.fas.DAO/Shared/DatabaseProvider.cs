﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.DAO.Shared
{
    public abstract class DatabaseProvider
    {
        #region Protected variables
        protected SqlConnection _connection;
        protected SqlTransaction _transaction;
        protected string _connectionString;
        protected List<SqlParameter> _params;
        #endregion

        public DatabaseProvider(string connectionString)
        {
            _connectionString = connectionString;
            _connection = new SqlConnection(_connectionString);
            _params = new List<SqlParameter>();
            _transaction = null;
        }

        #region Public property

        public SqlConnection Connection
        {
            get { return _connection; }
        }

        public SqlTransaction Transaction
        {
            get { return _transaction; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        public List<SqlParameter> Parameters
        {
            get
            {
                if (_params == null)
                    _params = new List<SqlParameter>();
                return _params;
            }
            set
            {
                _params = value;
            }
        }

        #endregion

        #region Generate return data

        protected DBReturnData _generateReturnData(DB_RETURN_TYPE returnType, Exception exception)
        {
            DBReturnData data = new DBReturnData();

            data.type = returnType;
            data.exception = exception;
            data.message = _generateExeptionMessage(exception);

            return data;
        }
        protected DBReturnData _generateReturnData(DB_RETURN_TYPE returnType, string message)
        {
            DBReturnData data = new DBReturnData();

            data.type = returnType;
            data.exception = null;
            data.message = message;

            return data;
        }
        protected string _generateExeptionMessage(Exception exception)
        {
            return exception.Message;
        }

        #endregion

        #region Convert data
        protected T _DataReaderMap<T>(SqlDataReader dr)
        {
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
            }
            return obj;
        }
        
        protected List<T> _DataReaderMapToList<T>(SqlDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (ColumnExists(dr, prop.Name))
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                             //  -- prop.SetValue(obj, dr[prop.Name], null);  
                            if (prop.PropertyType == typeof(String))
                            {
                                prop.SetValue(obj, GetString(dr, prop.Name, false), null);
                                continue;
                            }
                            if (prop.PropertyType == typeof(Decimal) || (prop.PropertyType == typeof(Decimal?)))
                            {
                                prop.SetValue(obj, GetDecimal(dr, prop.Name), null);
                                continue;
                            }
                            if (prop.PropertyType == typeof(Double) || (prop.PropertyType == typeof(Double?)))
                            {
                                prop.SetValue(obj, GetDouble(dr, prop.Name), null);
                                continue;
                            }
                            //DateTime
                            if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                            {
                                prop.SetValue(obj, GetDate(dr, prop.Name), null);
                                continue;
                            }
                            //Int
                            if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?))
                            {
                                prop.SetValue(obj, GetInt(dr, prop.Name), null);
                                continue;
                            }
                        }
                    }
                }
                list.Add(obj);
            }
            return list;
        }

        private bool ColumnExists(SqlDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i) == columnName)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
        #region Covert value 

        private static System.DateTime? GetDate(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDateTime(objRow[fieldName]);
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        private static decimal GetDecimal(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDecimal(objRow[fieldName]);
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }
        private static double GetDouble(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDouble(objRow[fieldName]);
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static int GetInt(SqlDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return int.Parse(objRow[fieldName].ToString());
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static string GetString(SqlDataReader objRow, string fieldName, bool isWithNullable)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToString(objRow[fieldName]);
                }
            }
            catch
            {
                if (isWithNullable)
                { return null; }
                return string.Empty;
            }
            if (isWithNullable)
            { return null; }
            return string.Empty;
        }
        #endregion
    }


    public enum DB_RETURN_TYPE { NONE = 1, SUCCESS = 2, ERROR = 3 }
    public class DBReturnData
    {
        public DB_RETURN_TYPE type { get; set; }
        public Exception exception { get; set; }
        public string message { get; set; }
    }


}
