﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace th.co.toyota.stm.fas
{
    class BFD0261ABO
    {
        private string strBatchName = "Upload Stock Taking Batch";
       // private string strBatchID = "BFD0261A";
        private int iProcessStatus = -1;

        protected Log4NetFunction _log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();

        public BatchLoggingData BatchQModel = null;

        public BatchLogging bLogging = null;
        private BFD0261ADAO dbconn = null;
        private DetailLogData LogModel = new DetailLogData();

        public string UploadFileName = string.Empty;
        private string ConfigurationFileName = string.Empty;
        public BFD0261ABO()
        {
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"])))
            {
                strBatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            }
            BatchQModel = new BatchLoggingData();
             BatchQModel.BatchName = strBatchName;
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"])))
            {
                BatchQModel.BatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            }
        }
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        public void Processing(BFD0261AModel _data)
        {
            try
            {
                //open connection
                bLogging = new BatchLogging();
                dbconn = new BFD0261ADAO();
                LogModel.AppID = BatchQModel.AppID;
                _log4Net = new Log4NetFunction();

                StartBatchQ();
                bool _success = this.ReceivingProcess();

                if (!_success)
                {
                    //add to email list
                    //mail.AppID = string.Format("{0}", bLoggingData.AppID);
                    return;
                }

                BatchQModel.ProcessStatus = this.BusinessProcess(_data);


            }
            catch (Exception ex) //cannot connect
            {
                //log4net
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                //add to email list
                //mail.AppID = string.Format("{0}", bLoggingData.AppID);

                if (!(ex is SqlException))
                {
                    try
                    {
                        LogModel.Status = eLogStatus.Processing;
                        LogModel.Level = eLogLevel.Error;
                        LogModel.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                        bLogging.InsertDetailLog(LogModel);
                    }
                    catch (Exception ex1)
                    {
                        //log4net
                        _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex1.Message, ex1);
                    }

                }
                return;
            }
            finally
            {
                try
                {
                    SetBatchQEnd();
                    //if mail list > 0 send email
                    //Incomplete
                    mail.SendEmailToAdministratorInSystemConfig(this.strBatchName);
                }
                catch (Exception ex)
                {
                    //log4net
                    _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                }

            }

        }

        #region "BatchQ Method"


        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(BatchQModel);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(BatchQModel);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion

        private bool ReceivingProcess()
        {
            ReceiveFileClass _cls = null;

            try
            {
                string _cfgLayoutSection = ConfigurationManager.GetAppSetting("LayoutSection"); //follow your config
                if (_cfgLayoutSection == null)
                {
                    // message
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, "LayoutSection"));
                }

                _cls = new ReceiveFileClass(ConfigFileName, _cfgLayoutSection);
                _cls.ExecuteExcelFile(this.UploadFileName);
                foreach (MessageResult _rs in _cls.ProcessResultList)
                {
                    LogModel.Status = eLogStatus.Processing;
                    LogModel.Level = _rs.MessageResultType;
                    LogModel.Description = _rs.Message;
                    bLogging.InsertDetailLog(LogModel);
                }

            }
            catch (BatchLoggingException bLoggingex)
            {
                LogModel.Status = eLogStatus.Processing;
                if (bLoggingex.arErrorList == null)
                {
                    LogModel.Description = bLoggingex.Message;
                    LogModel.Level = eLogLevel.Error;//Add 2014.09.02 Kanjana.c
                    bLogging.InsertDetailLog(LogModel);
                    _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + bLoggingex.Message);
                }
                else
                {
                    foreach (MessageResult msg in bLoggingex.arErrorList)
                    {
                        LogModel.Description = msg.Message;
                        LogModel.Level = msg.MessageResultType;
                        bLogging.InsertDetailLog(LogModel);

                        _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + msg.Message);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return true;
        }

        /** Allow to Edit */
        private eLogLevel BusinessProcess(BFD0261AModel _data)
        {
            return dbconn.UploadStockTaking(BatchQModel.AppID, BatchQModel.ReqBy,_data);
        }
        /** End Allow */
    }
}
