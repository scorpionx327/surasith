﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;


namespace th.co.toyota.stm.fas
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleTitle(string _title);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main(string[] args)
        {
            string _modName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            string _procName = System.IO.Path.GetFileNameWithoutExtension(_modName);
            System.Diagnostics.Process[] _appProc = System.Diagnostics.Process.GetProcessesByName(_procName);
            if (_appProc.Length > 1)
            {
                Console.WriteLine("There is another instances running. This instance will be closed automatically.");
                return;
            }
            /************************************   Check Multiple Running    ****************************************/
            string _cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
            if (!System.IO.File.Exists(_cfgFileName))
            {
                // message
                Console.WriteLine("Not found Configuration File : {0}", _cfgFileName);
                return;
            }


            BFD0261ABO _cls = new BFD0261ABO();
            BFD0261AModel _261AModel = new BFD0261AModel();

            Log4NetFunction _log4net = new Log4NetFunction();
            if (args.Length == 3 && Util.IsInt(args[0])) // manual
            {
                _cls.BatchQModel.AppID = Convert.ToInt32(args[0]);
                _cls.BatchQModel.BatchID = args[1];
                //_cls.UploadFileName = args[2];
               _cls.BatchQModel.Arguments = args[2];


                _log4net.WriteErrorLogFile(String.Format(" _cls.BatchQModel.AppID:{0} , _cls.BatchQModel.BatchID:{1}, Batch param:{2}", _cls.BatchQModel.AppID, _cls.BatchQModel.BatchID,_cls.BatchQModel.Arguments));

                string BatchParam = _cls.BatchQModel.Arguments.Replace("'", "");

                string[] param = BatchParam.Split('|');

                _261AModel.UploadFileName = param[0];
                _cls.UploadFileName = param[0];
                _261AModel.IsUpdate = param[1];
                _261AModel.UpdateBy = param[2];
                _261AModel.StockYear = param[3];
                _261AModel.StockRound = param[4];
                _261AModel.StockAssetLocation = param[5];
                _261AModel.QTyOfHanheld = param[6];
                _261AModel.TargetDateFrom = param[7];
                _261AModel.TargetDateTo = param[8];
                _261AModel.BreakTime = param[9];
            }
            else //Not Allow Auto
            {
                //Log4net
                _log4net.WriteErrorLogFile("Invalid Batch Parameter");
                return;
            }


            string _batchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            if (_batchID != _cls.BatchQModel.BatchID)
            {
                //Log4Net
                _log4net.WriteErrorLogFile("Invalid Batch ID");
                return;
            }
            SetConsoleTitle(_batchID);
            _cls.Processing(_261AModel);
        }
    }
}
