﻿using System;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using System.Data;

namespace th.co.toyota.stm.fas
{
    class BFD0261ADAO
    {
        private  DataConnection dbconn = null ;

        public BFD0261ADAO()
		{
			//
			// TODO: Add constructor logic here
			//

			dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]) ;
			if(!dbconn.TestConnection())
			{
				
				throw(new Exception("Cannot connect Database")) ;
			}	
							
		}
        public void Close()
        {
            dbconn.Close();
        }
        public eLogLevel UploadStockTaking(int _ReqID, string _ReqUser,BFD0261AModel _model)
        {
            SqlCommand _cmd = new SqlCommand("sp_BFD0261A_UploadStockTakingBatch");
            _cmd.CommandType = System.Data.CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@AppID", _ReqID);
            _cmd.Parameters.AddWithValue("@UserID", _ReqUser);
            _cmd.Parameters.AddWithValue("@IsUpdate", _model.IsUpdate);
            _cmd.Parameters.AddWithValue("@UpdateBy", _model.UpdateBy);
            _cmd.Parameters.AddWithValue("@StockYear", _model.StockYear);
            _cmd.Parameters.AddWithValue("@StockRound", _model.StockRound);
            _cmd.Parameters.AddWithValue("@StockAssetLocation", _model.StockAssetLocation);
            _cmd.Parameters.AddWithValue("@QTyOfHanheld", _model.QTyOfHanheld);
            _cmd.Parameters.AddWithValue("@TargetDateFrom", _model.TargetDateFrom);
            _cmd.Parameters.AddWithValue("@TargetDateTo", _model.TargetDateTo);
            _cmd.Parameters.AddWithValue("@BreakTime", _model.BreakTime);

            
            DataSet ds = dbconn.GetDataSet(_cmd);
            DataTable dt = ds.Tables[ds.Tables.Count - 1];

            return (eLogLevel)(dt.Rows[0][0]);
        }
    }

    public class BFD0261AModel
    {
        public string UploadFileName { get; set; }
        public string IsUpdate { get; set; }
        public string UpdateBy { get; set; }
        public string StockYear { get; set; }
        public string StockRound { get; set; }
        public string StockAssetLocation { get; set; }
        public string QTyOfHanheld { get; set; }
        public string TargetDateFrom { get; set; }
        public string TargetDateTo { get; set; }
        public string BreakTime { get; set; }
      }

}
