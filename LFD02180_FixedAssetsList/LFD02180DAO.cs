﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
using System.Configuration;

namespace LFD02180_FixedAssetsList
{
    public class LFD02180DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02180DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }     
        public DataTable GetFixedAssetResult(WFD02120SearhConditionModel data, string _User)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }

                var param = new List<SqlParameter>();
                SqlCommand cmd = new SqlCommand("sp_LFD02180_GetDataFixedAsset"); 

                cmd.Parameters.AddWithValue("@COMPANY", _db.IfNull(data.Company));
                cmd.Parameters.AddWithValue("@ASSET_NO", _db.IfNull(data.AssetNo));
                cmd.Parameters.AddWithValue("@ASSET_SUB", _db.IfNull(data.AssetSub));
                cmd.Parameters.AddWithValue("@ASSET_NAME", _db.IfNull(data.AssetName));
                cmd.Parameters.AddWithValue("@ASSET_CLASS", _db.IfNull(data.AssetClass));
                cmd.Parameters.AddWithValue("@ASSET_GROUP", _db.IfNull(data.AssetGroup));
                cmd.Parameters.AddWithValue("@COST_CODE", _db.IfNull(data.CostCenterCode));
                cmd.Parameters.AddWithValue("@RESP_COST_CODE", _db.IfNull(data.ResponsibleCostCenter));
                cmd.Parameters.AddWithValue("@LOCATION", _db.IfNull(data.Location));
                cmd.Parameters.AddWithValue("@WBS_PROJECT", _db.IfNull(data.WBSProject));
                cmd.Parameters.AddWithValue("@WBS_BUDGET", _db.IfNull(data.WBSBudget));
                cmd.Parameters.AddWithValue("@BOI", _db.IfNull(data.BOI));
                cmd.Parameters.AddWithValue("@CAPITAL_DATE_FROM", _db.IfNull(data.CapitalizeDateFrom));
                cmd.Parameters.AddWithValue("@CAPITAL_DATE_TO", _db.IfNull(data.CapitalizeDateTo));
                cmd.Parameters.AddWithValue("@ASSET_PLATE_NO", _db.IfNull(data.AssetPlateNo));
                cmd.Parameters.AddWithValue("@ASSET_STATUS", _db.IfNull(data.AssetStatus));
                cmd.Parameters.AddWithValue("@PRINT_STATUS", _db.IfNull(data.PrintStatus));
                cmd.Parameters.AddWithValue("@PHOTO_STATUS", _db.IfNull(data.PhotoStatus));
                cmd.Parameters.AddWithValue("@MAP_STATUS", _db.IfNull(data.MapStatus));
                cmd.Parameters.AddWithValue("@COST_PENDING", _db.IfNull(data.CostPending)); 
                cmd.Parameters.AddWithValue("@USER_ID", _User);
                cmd.CommandType = CommandType.StoredProcedure;
                var _dt = _db.GetData(cmd);
                return _dt;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            
        }
    }

    public class WFD02120SearhConditionModel
    {
        public string Company { get; set; }
        public string AssetNo { get; set; }
        public string AssetSub { get; set; }
        public string AssetName { get; set; }
        public string AssetClass { get; set; }
        public string AssetGroup { get; set; }
        public string CostCenterCode { get; set; }
        public string ResponsibleCostCenter { get; set; }
        public string Location { get; set; }
        public string WBSProject { get; set; }
        public string WBSBudget { get; set; }
        public string BOI { get; set; }
        public string CapitalizeDateFrom { get; set; }
        public string CapitalizeDateTo { get; set; }
        public string AssetPlateNo { get; set; }
        public string AssetStatus { get; set; }

        public string PrintStatus { get; set; }
        public string PhotoStatus { get; set; }
        public string MapStatus { get; set; }
        public int? CostPending { get; set; }

        public bool IsAECUser { get; set; }

    }
}
