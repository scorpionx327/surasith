﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas
{
    public class RequestHeaderModel
    {
        public RequestHeaderModel()
        {
            this.ASSETS = new List<RequestDetailModel>();
        }
        public string requestNo { get; set; }
        public List<RequestDetailModel> ASSETS { get; set; }
    }
    public class RequestDetailModel
    {
        public string requestNo { get; set; }
        public string requestItem { get; set; }
        public string companyCode { get; set; }
        public string assetNo { get; set; }
        public string assetSubNo { get; set; }

        public string status { get; set; }
        public string message { get; set;}
       
    }
    public class SystemMaster
    {
        public string CATEGORY { get; set; }
        public string SUB_CATEGORY { get; set; }
        public string CODE { get; set; }
        public string VALUE { get; set; }
        public string REMARKS { get; set; }
        public string ACTIVE_FLAG { get; set; }

    }

    public class SAPConfiguration
    {
        public string URL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class AUCModel
    {
        public string DOC_NO { get; set; }
        public string SEQ_NO { get; set; }
        public string COMPANY { get; set; }
        public string AUC_NO { get; set; }
        public string AUC_SUB { get; set; }
    }
}
