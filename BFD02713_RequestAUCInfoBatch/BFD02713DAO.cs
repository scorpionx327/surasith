﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
namespace th.co.toyota.stm.fas
{
    class BFD02713DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public BFD02713DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];           
        }

        public SAPConfiguration GetSAPApiUrl()
        {
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }

                SqlCommand cmd = new SqlCommand("sp_Common_GetSystemValues");
                cmd.Parameters.AddWithValue("@CATEGORY", "SYSTEM_CONFIG");
                cmd.Parameters.AddWithValue("@SUB_CATEGORY", "SAP_REQUEST_AUC_INFO");
               
                cmd.CommandType = CommandType.StoredProcedure;
                var rs = _db.executeDataToList<SystemMaster>(cmd);
                if (rs == null || rs.Count == 0)
                    return null ;

                var _data = new SAPConfiguration();

                if(rs.Find(x => x.CODE.ToUpper() == "URL") != null)
                {
                    _data.URL = rs.Find(x => x.CODE.ToUpper() == "URL").VALUE;
                }
                if (rs.Find(x => x.CODE.ToUpper() == "USERNAME") != null)
                {
                    _data.UserName = rs.Find(x => x.CODE.ToUpper() == "USERNAME").VALUE;
                }
                if (rs.Find(x => x.CODE.ToUpper() == "PASSWORD") != null)
                {
                    _data.Password = rs.Find(x => x.CODE.ToUpper() == "PASSWORD").VALUE;
                }

                return _data;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }


        public List<AUCModel> GetAssetsList(int _AppID) //all quece
        {  
            try
            {

                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02713_GetAssetList");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AppID", _AppID);
                var rs = _db.executeDataToList<AUCModel>(cmd);
                return rs;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
           
        }

        public void UpdateSentStatus(RequestHeaderModel _grpDoc, int appId)
        {
          
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }

                SqlCommand cmd = new SqlCommand("sp_BFD02713_UpdateRequestStatus");
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var _data in _grpDoc.ASSETS)
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@DOC_NO", _data.requestNo);
                    cmd.Parameters.AddWithValue("@AUC_NO", _data.assetNo);
                    cmd.Parameters.AddWithValue("@AUC_SUB", _data.assetSubNo);
                    cmd.Parameters.AddWithValue("@STATUS", _data.status);
                    _db.Execute(cmd);
                }

                cmd = new SqlCommand("sp_BFD02713_InitialAssetList");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@AppID", appId);

                _db.Execute(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}

public class MSG
{
    public const string FileSuccess = "MSTD4005AINF"; //MSTD4005AINF	 File {0} is successfully generated in {1}.	
    public const string BatchBegin = "MSTD7000BINF"; //MSTD7000BINF : {0} Begin
    public const string BatchEndSuccessfully = "MSTD7001BINF"; //MSTD7001BINF : {0} End successfully
    public const string BatchEndError = "MSTD7002BINF";  //MSTD7002BINF :  {0} End with error {1}
    public const string DataNotFound_NoParams = "MCOM2100BWRN"; // No data found
    public const string DataNotFound = "MSTD7054BERR"; //{0} Data not found from {1}
}