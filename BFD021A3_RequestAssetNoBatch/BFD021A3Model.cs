﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas
{
    public class RequestHeaderModel
    {
        public RequestHeaderModel()
        {
            this.ASSETS = new List<RequestAssetModel>();
        }
        public string requestNo { get; set; }

        public List<RequestAssetModel> ASSETS { get; set; }


    }
    public class SAPConfiguration
    {
        public string URL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class RequestAssetModel
    {
        public string requestNo { get; set; }
        public string requestItem { get; set; }
        public string companyCode { get; set; }
        public string refMainAssetNo { get; set; }
        public string mainOrSubInd { get; set; }
        public string WBSCostCenterValInd { get; set; }
        public string assetClass { get; set; }
        public string assetDesc { get; set; }
        public string assetDescAdditional { get; set; }
        public string serialNo { get; set; }
        public string inventoryNo { get; set; }
        public string baseUoM { get; set; }
        public string lastInvDate { get; set; }
        public string inventoryInd { get; set; }
        public string inventoryNote { get; set; }
        public string costCenter { get; set; }
        public string costCenterRespAsset { get; set; }
        public string plant { get; set; }
        public string assetLocation { get; set; }
        public string room { get; set; }
        public string licensePlate { get; set; }
        public string WBSDepre { get; set; }
        public string investmentReason { get; set; }
        public string evalGroup1 { get; set; }
        public string evalGroup2 { get; set; }
        public string evalGroup3 { get; set; }
        public string evalGroup4 { get; set; }
        public string evalGroup5 { get; set; }
        public string assetSuperNo { get; set; }
        public string assetManu { get; set; }
        public string WBSBudget { get; set; }
        public string leaseAgreeDate { get; set; }
        public string leaseStartDate { get; set; }
        public string leaseYear { get; set; }
        public string leasePeriod { get; set; }
        public string leaseSupplText { get; set; }
        public string leasePaymentNo { get; set; }
        public string leasePaymentCycle { get; set; }
        public string leaseAdvPayment { get; set; }
        public string leasePayment { get; set; }
        public string leaseAnnualIntRate { get; set; }
        public string depreArea01 { get; set; }
        public string depreKey01 { get; set; }
        public string planUseLifeYear01 { get; set; }
        public string planUseLifePeriod01 { get; set; }
        public string scrapVal01 { get; set; }
        public string deactiveDepreAreaInd15 { get; set; }
        public string depreArea15 { get; set; }
        public string depreKey15 { get; set; }
        public string planUseLifeYear15 { get; set; }
        public string planUseLifePeriod15 { get; set; }
        public string scrapVal15 { get; set; }
        public string depreArea31 { get; set; }
        public string depreKey31 { get; set; }
        public string planUseLifeYear31 { get; set; }
        public string planUseLifePeriod31 { get; set; }
        public string scrapVal31 { get; set; }
        public string depreArea41 { get; set; }
        public string depreKey41 { get; set; }
        public string planUseLifeYear41 { get; set; }
        public string planUseLifePeriod41 { get; set; }
        public string scrapVal41 { get; set; }
        public string depreArea81 { get; set; }
        public string depreKey81 { get; set; }
        public string planUseLifeYear81 { get; set; }
        public string planUseLifePeriod81 { get; set; }
        public string scrapVal81 { get; set; }
        public string depreArea91 { get; set; }
        public string depreKey91 { get; set; }
        public string planUseLifeYear91 { get; set; }
        public string planUseLifePeriod91 { get; set; }
        public string scrapVal91 { get; set; }
    }

    public class SystemMaster
    {
        public string CATEGORY { get; set; }
        public string SUB_CATEGORY { get; set; }
        public string CODE { get; set; }
        public string VALUE { get; set; }
        public string REMARKS { get; set; }
        public string ACTIVE_FLAG { get; set; }

    }
}
