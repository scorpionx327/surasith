﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTP;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;

namespace th.co.toyota.stm.fas
{
    class BFD021A3BO
    {
        #region "Variable Declare"
      
        public BatchLoggingData bLoggingData = null;
        public BatchLogging bLogging = null;
        
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private BFD02A30DAO _dbconn = null;
        private DetailLogData _log = null;
        #endregion

        public BFD021A3BO()
        {
            bLoggingData = new BatchLoggingData();
            bLogging = new BatchLogging();


            bLoggingData.BatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            bLoggingData.BatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            
        }
        public void Processing()
        {
            Console.WriteLine("Application Start");
            MailSending _mail = null;
            try
            {
                _mail = new MailSending();


                bLogging = new BatchLogging();//Test connect data base
                _dbconn = new BFD02A30DAO();

               this.StartBatchQ();

               this.BusinessProcess();

                bLoggingData.ProcessStatus = eLogLevel.Information;


            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = bLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    bLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
            }
            finally
            {
                try
                {
                    this.SetBatchQEnd();
                    _mail.SendEmailToAdministratorInSystemConfig(bLoggingData.BatchID);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        
        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion


        private void BusinessProcess()
        {
            try
            {
                //Get Configuration
                var _sapConfig = _dbconn.GetSAPApiUrl();
                if(_sapConfig == null)
                {
                    _log4Net.WriteErrorLogFile("Not found SAL Configuration Category = SYSTEM_CONFIG, Sub Category = SAP_REQUEST_ASSET_MASTER_INFO");
                    return;
                }
                Console.WriteLine("Connect to : {0}", _sapConfig.URL);
                Console.WriteLine("User Name : {0}", _sapConfig.UserName);
                Console.WriteLine("Password : {0}", _sapConfig.Password);
                var _rsList = _dbconn.GetAssetsList();
                if (_rsList.Count == 0)
                {
                    _log4Net.WriteInfoLogFile("No Data Found");
                    return ;
                }
                Console.WriteLine("Asset Count : {0}", _rsList.Count );

                bLogging.InsertDetailLog(new DetailLogData()
                {
                    AppID = bLoggingData.AppID,
                    Level = eLogLevel.Information,
                    Favorite = true,
                    User = bLoggingData.ReqBy,
                    Description = string.Format("Found {0} assets for request asset no", _rsList.Count)
                });

             

                //Create Group for sending
                int _ItemPerGroup = Convert.ToInt32(ConfigurationManager.GetAppSetting("ITEM_PER_GROUP"));
                List<RequestHeaderModel> _grpList = new List<RequestHeaderModel>();
                
                var _temp = new RequestHeaderModel(); //Invoice No

                foreach (var _item in _rsList)
                {

                    if(_temp.requestNo != _item.requestNo || _grpList[_grpList.Count-1].ASSETS.Count > _ItemPerGroup)
                        _grpList.Add(new RequestHeaderModel() { requestNo = _item.requestNo });

                    if(_grpList.Count == 0)
                    {
                        _grpList.Add(new RequestHeaderModel() { requestNo = _item.requestNo });
                    }
                    _temp.requestNo = _item.requestNo;

                     var _currentGrp = _grpList[_grpList.Count - 1];
                    _currentGrp.ASSETS.Add(_item);
                    
                }

                foreach(var _grp in _grpList)
                {
                    _ExecuteWebService(_sapConfig, _grp);
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                bLoggingData.ProcessStatus = eLogLevel.Error;
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private bool _ExecuteWebService(SAPConfiguration _sapConfig, RequestHeaderModel _data)
        {

            try
            {
                //Write File for sending.

                var _json = JsonConvert.SerializeObject(_data);

                _log4Net.WriteInfoLogFile("Send Data :");
                _log4Net.WriteInfoLogFile(_json);


                _log4Net.WriteInfoLogFile("Call HttpAPI.PostAsync : _sapConfig.URL = " + _sapConfig.URL + ", _sapConfig.UserName = " + _sapConfig.UserName + ", _sapConfig.Password = " + _sapConfig.Password);
                var _rs = HttpAPI.PostAsync<bool>(_sapConfig.URL, _sapConfig.UserName, _sapConfig.Password, _data);

                bLogging.InsertDetailLog(new DetailLogData()
                {
                    AppID = bLoggingData.AppID,
                    Level = eLogLevel.Information,
                    Favorite = true,
                    User = bLoggingData.ReqBy,
                    Description = string.Format("Request Asset No => {0} ({1} records)", _rs ? "success" : "fail", _data.ASSETS.Count)
                });

                if (_rs)
                {
                    Console.WriteLine("UpdateSentStatus");
                    _dbconn.UpdateSentStatus(_data);
                }
                else
                {
                    _log4Net.WriteInfoLogFile("HttpAPI.PostAsync return NULL");
                }

                return false;
            }
            catch (Exception ex)
            {
                Exception _exInner = ex.InnerException == null ? ex : ex.InnerException; 
                _log4Net.WriteErrorLogFile(_exInner.Message, _exInner);
                throw (ex);
            }
          
        }
     
    }

    public static class HttpAPI
    {
        public static bool TestConnection()
        {
           
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["MainUrl"]);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    HttpResponseMessage response = client.GetAsync(client.BaseAddress + "values/").Result;
                    return response.IsSuccessStatusCode;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
        private static HttpClient CreateNewClientInstance()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["MainUrl"]);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public static T GetAsync<T>(string _url)
        {

            HttpResponseMessage response = CreateNewClientInstance().GetAsync(_url).Result;


            if (!response.IsSuccessStatusCode)
            {
                throw (new Exception("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase));
            }
            var rs = response.Content.ReadAsAsync<T>();

            return rs.Result;

        }
        static HttpClient client = new HttpClient();
        public static bool PostAsync<T>(string _url, string _UserName, string _Password, object _data)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //                client.DefaultRequestHeaders.Add("Authorization", string.Format("Bearer {0}", _Token));
                
                var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}",_UserName, _Password));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));


                var response = client.PostAsJsonAsync(client.BaseAddress, _data);

                return response.Result.IsSuccessStatusCode;
                
            }
        }
    }
}