﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace LFD02240_ExportCIPRequest
{
    class LFD02240DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02240DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataTable GetCIPRequestData(string _DocNO)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_LFD02240_GetCIPRequestData");
                cmd.Parameters.AddWithValue("@DOC_NO", _DocNO);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }
    }
}