﻿using System;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using System.Globalization;
using System.Data;
using System.Collections;

namespace th.co.toyota.stm.fas
{
    class BFD01220BO
    {
        private string strBatchName = "Receiving Employee Master Interface Batch";
        private string strBatchID = "BFD01220";
        private string strMailBodyPath = string.Empty;

        private Log4NetFunction log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();

        public BatchLoggingData BatchQModel = null;

        public BatchLogging bLogging = null;
        private BFD01220DAO _dataAccess = null;
        private DetailLogData LogModel = new DetailLogData();

        public string UploadFileName = string.Empty;
        private string ConfigurationFileName = string.Empty;

        //private string SuccessPath = string.Empty;
        //private string FailPath = string.Empty;
        //public string FullFileName = string.Empty;

        public BFD01220BO()
        {
             bLogging = new BatchLogging();

            // Set Batch Q information
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"])))
            {
                strBatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            }
            BatchQModel = new BatchLoggingData();
            BatchQModel.BatchID = strBatchID;
            BatchQModel.Status = eLogStatus.Queue;
            BatchQModel.Description = "Run by schedule";
            BatchQModel.ReqBy = "System";
            BatchQModel.BatchName = strBatchName;
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"])))
            {
                BatchQModel.BatchID  = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            }            
        }

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        public void Processing()
        {
            try
            {

                   //open connection
                bool _success = false;

                  _dataAccess = new BFD01220DAO();
                LogModel.AppID = BatchQModel.AppID; 

                StartBatchQ();
                mail.AppID = string.Empty;
                Console.WriteLine("Connect to HMRS");
                _success = this.ReceiveFileFromSharePath(ConfigFileName); // receiving file from ftp
                if (!_success)
                {
                    // Cannot receive file from FTP 
                    //add to email list
                    mail.AppID = string.Format("{0}", LogModel.AppID);
                    return;
                }
                Console.WriteLine("Transfer File Completed");
 
                _success = ReceivingProcess(); // Common Receiving file on local path
                if (!_success)
                {
                    mail.AppID = string.Format("{0}", LogModel.AppID);
                    //add to email list
                    //mail.AppID = string.Format("{0}", bLoggingData.AppID);
                    return;
                }
          
                BatchQModel.IProcessStatus = this.BusinessProcess();             
            }
            catch (Exception ex) //cannot connect
            {
                mail.AppID = string.Format("{0}", LogModel.AppID);

                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                if (!(ex is SqlException))
                {
                    try
                    {
                        LogModel.Status = eLogStatus.Processing;
                        LogModel.Level = eLogLevel.Error;
                        LogModel.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                        bLogging.InsertDetailLog(LogModel);
                    }
                    catch (Exception ex1)
                    {
                        //log4net
                        log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex1.Message, ex1);
                    }

                }
                return;
            }
            finally
            {
                try
                {
                    SetBatchQEnd();
                    //if mail list > 0 send email
                    //Incomplete


                    mail.SendEmailToAdministratorInSystemConfig(this.strBatchName);
                    _dataAccess.Close();//Close connection
                }
                catch (Exception ex)
                {
                    //log4net
                    log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                }
            }

        }

        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(BatchQModel);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(BatchQModel);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion

        #region "Receiving Method"
          private bool ReceivingProcess()
        {
            bool _valueReturn = false;
            try
            {
                string _LayoutSection = string.Empty;
                _valueReturn = ReceivingProcess("Organization"); //Org   
                if(_valueReturn)
                { 
                _valueReturn = ReceivingProcess("Employee"); //Employee    
                }
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return _valueReturn;
        }
         private bool ReceivingProcess(string _LayoutSection)
        {
            ReceiveFileClass _cls = null;

            try
            {
                Console.WriteLine("ReceivingProcess {0}", _LayoutSection);
                string _cfgLayoutSection = ConfigurationManager.GetAppSetting(_LayoutSection); //follow your config
                if (_cfgLayoutSection == null)
                {
                    // message
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, _LayoutSection));
                }

                _cls = new ReceiveFileClass(ConfigFileName, _cfgLayoutSection);
                _cls.Execute();


                //_cls.Execute(this.UploadFileName);
                foreach (MessageResult _rs in _cls.ProcessResultList)
                {
                    Console.WriteLine(_rs.Message);
                    LogModel.Status = eLogStatus.Processing;
                    LogModel.Level = _rs.MessageResultType;
                    LogModel.Description = _rs.Message;
                    bLogging.InsertDetailLog(LogModel);
                }

            }
            catch (BatchLoggingException bLoggingex)
            {
                Console.WriteLine(bLoggingex.ToString());
                LogModel.Status = eLogStatus.Processing;
                if (bLoggingex.arErrorList == null)
                {
                    LogModel.Description = bLoggingex.Message;
                    LogModel.Level = eLogLevel.Error;
                    bLogging.InsertDetailLog(LogModel);

                    if (bLoggingex.Message.StartsWith("No data in file"))
                    {
                        BatchQModel.IProcessStatus = 0;
                    }
                }
                else
                {
                    foreach (MessageResult msg in bLoggingex.arErrorList)
                    {
                        LogModel.Description = msg.Message;
                        LogModel.Level = msg.MessageResultType;
                        bLogging.InsertDetailLog(LogModel);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return true;
        }
        #endregion

        #region "FTP Receiving Method"

        private bool ReceiveFileFromSharePath(string cfgFileName)
        {
          
            try
            {
                string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["FileShareList"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "FileShareList")));
                }
               
                DownloadFile(cfgFileName, cfgLayoutSectionFTP, "Organization");
                DownloadFile(cfgFileName, cfgLayoutSectionFTP, "Employee"); // Check exist file in server --->   
                
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);

            }
            return true;
        }
        #endregion
        #region "BusinessProcess Method"

        /** Allow to Edit */
        private Int16 BusinessProcess()
        {
            Int16 _valuReturn;
            _valuReturn= _dataAccess.ReceiveIFEmpBatch(BatchQModel.AppID, BatchQModel.ReqBy);

            return _valuReturn;
        }

      
        /** End Allow */
        #endregion

        private bool DownloadFile(string _configFileName, string _configSection, string _layoutName)
        {
            ConfigurationManager _cfg = new ConfigurationManager(_configFileName);
            Hashtable _ht = _cfg.GetChildNodes(_configSection + "/" + _layoutName);

            if (Util.IsNullorEmpty(_ht, "FilePath"))
                throw (new Exception(string.Format("Not found <{0}>", "FilePath")));
            if (Util.IsNullorEmpty(_ht, "FilePrefix"))
                throw (new Exception(string.Format("Not found <{0}>", "FilePrefix")));
            if (Util.IsNullorEmpty(_ht, "ClientPath"))
                throw (new Exception(string.Format("Not found <{0}>", "ClientPath")));

           
            string _FilePath = Util.GetStringFromHash(_ht, "FilePath");
            string _FilePrefix = Util.GetStringFromHash(_ht, "FilePrefix");
           // _FilePrefix = _FilePrefix.Replace("*", string.Empty);

            string _ClientPath = Util.GetStringFromHash(_ht, "ClientPath");

            bool _OnlyLatestFile = false ;
            if (Util.GetStringFromHash(_ht, "OnlyLatestFile").ToString().ToLower().Equals("true"))//string.Format("{0}", _ht["OnlyLatestFile"]);
            {
                _OnlyLatestFile = true;
            }

            string _ArchivePath = string.Empty;

            if (_ht.ContainsKey("ArchivePath"))
                _ArchivePath = string.Format("{0}", _ht["ArchivePath"]);
            
            
            try
            {
                // Try to Connected
                bool _b = System.IO.Directory.Exists(_FilePath);
                if (!_b)
                    throw (new Exception(string.Format("Cannot connect to Path : {0}", _FilePath)));

                // Check File Exists
                string[] _fs = System.IO.Directory.GetFiles(_FilePath, _FilePrefix);
                if(_fs.Length == 0)
                {
                    throw (new Exception(string.Format("No file match with {0} in {1}", _FilePrefix, _FilePath)));
                }


                if (_OnlyLatestFile)
                {
                    string _SourceFileName = _fs[_fs.Length - 1];

                    string _TargetFileName = System.IO.Path.Combine(_ClientPath, new System.IO.FileInfo(_SourceFileName).Name);
                    
                    System.IO.File.Copy(_SourceFileName, _TargetFileName, true);

                    if (!string.IsNullOrEmpty(_ArchivePath))
                    {
                        System.Threading.Thread.Sleep(1000);

                        _TargetFileName = System.IO.Path.Combine(_ArchivePath, new System.IO.FileInfo(_SourceFileName).Name);
                        System.IO.File.Move(_SourceFileName, _TargetFileName);
                    }

                    return true ;

                }

                // All File
                for(int i = 0; i < _fs.Length;i++)
                {
                    string _SourceFileName = _fs[i];

                    string _TargetFileName = System.IO.Path.Combine(_ClientPath, new System.IO.FileInfo(_SourceFileName).Name);

                    System.IO.File.Copy(_SourceFileName, _TargetFileName, true);

                }
                if (string.IsNullOrEmpty(_ArchivePath))
                    return true;

                System.Threading.Thread.Sleep(1000);
                for (int i = 0; i < _fs.Length; i++)
                {
                    string _SourceFileName = _fs[i];

                    string _TargetFileName = System.IO.Path.Combine(_ArchivePath, new System.IO.FileInfo(_SourceFileName).Name);

                    System.IO.File.Move(_SourceFileName, _TargetFileName);

                }

                return true; 

            }
            catch (Exception ex)
            {
                throw (ex);
            }


           
        }
    }
}
