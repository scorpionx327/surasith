﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
namespace th.co.toyota.stm.fas
{
    class BFD02680DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public BFD02680DAO()
        {
            _strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];           
        }

        public DataTable GetParam(string apl_id)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02680_GetParam");
                cmd.Parameters.Add(new SqlParameter("@APL_ID", SqlDbType.Decimal)).Value = Decimal.Parse(apl_id);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }

        public DataTable RemoveParam(string _year, string _round, string _assetlocaltion, string apl_id)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02680_GetExportStock");
                cmd.Parameters.Add(new SqlParameter("@YEAR", SqlDbType.Text)).Value = _year;
                cmd.Parameters.Add(new SqlParameter("@ROUND", SqlDbType.Text)).Value = _round;
                cmd.Parameters.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.Text)).Value = _assetlocaltion;
                cmd.Parameters.Add(new SqlParameter("@APL_ID", SqlDbType.Decimal)).Value = Decimal.Parse(apl_id);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }

        public DataTable GetExportStock(string _year, string _round, string _assetlocaltion, string apl_id)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02680_GetExportStock");
                cmd.Parameters.Add(new SqlParameter("@YEAR", SqlDbType.Text)).Value = _year;
                cmd.Parameters.Add(new SqlParameter("@ROUND", SqlDbType.Text)).Value = _round;
                cmd.Parameters.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.Text)).Value = _assetlocaltion;
                cmd.Parameters.Add(new SqlParameter("@APL_ID", SqlDbType.Decimal)).Value = Decimal.Parse(apl_id);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }

        public bool GetPlanStutas(string _year, string _round, string _assetlocaltion)
        {
            bool result = false;
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return result;
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02680_GetPlanStatus");
                cmd.Parameters.Add(new SqlParameter("@YEAR", SqlDbType.Text)).Value = _year;
                cmd.Parameters.Add(new SqlParameter("@ROUND", SqlDbType.Text)).Value = _round;
                cmd.Parameters.Add(new SqlParameter("@ASSET_LOCATION", SqlDbType.Text)).Value = _assetlocaltion;
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);

                if (dt.Rows.Count > 0)
                {
                    if (!dt.Rows[0][0].ToString().Equals("F"))
                    {
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return result;
        }

    }
}

public class MSG
{
    public const string FileSuccess = "MSTD4005AINF"; //MSTD4005AINF	 File {0} is successfully generated in {1}.	
    public const string BatchBegin = "MSTD7000BINF"; //MSTD7000BINF : {0} Begin
    public const string BatchEndSuccessfully = "MSTD7001BINF"; //MSTD7001BINF : {0} End successfully
    public const string BatchEndError = "MSTD7002BINF";  //MSTD7002BINF :  {0} End with error {1}
    public const string DataNotFound_NoParams = "MCOM2100BWRN"; // No data found
    public const string DataNotFound = "MSTD7054BERR"; //{0} Data not found from {1}
}