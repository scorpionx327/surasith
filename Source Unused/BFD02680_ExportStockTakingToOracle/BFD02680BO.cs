﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using System.IO;
using System.IO.Compression;
using System.Collections;

namespace th.co.toyota.stm.fas
{
    class BFD02680BO
    {
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private BFD02680DAO _dbconn = null;
        private DetailLogData _log = null;
        public string zipSourceDirectory = string.Empty;


        public BFD02680BO()
        {
            BLoggingData = new BatchLoggingData();
        }
        public void Processing()
        {
            MailSending mail = null;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationSettings.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Export Stock Taking To Oracle";
                }


                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new BFD02680DAO();
                BLoggingData.BatchName = _batchName;
                StartBatchQ();

                //string BatchParam = BLoggingData.Arguments.Replace("'", "");
                //string[] _p = BatchParam.Split('|');
                _dbconn = new BFD02680DAO();
                //DataTable dt = new DataTable(); ---Move to check when generation file per year/round
                //if (_dbconn.GetPlanStutas(_p[0], _p[1], _p[2]))//Check Plan Status != F
                //{

                //        string.Format("Stock Taking Result for Period {0} / {1} not finish", _p[0], _p[1]);

                //        _log = new DetailLogData();
                //        _log.AppID = BLoggingData.AppID;
                //        _log.Status = eLogStatus.Processing;
                //        _log.Level = eLogLevel.Error;
                //        _log.Favorite = false;
                //        _log.Description = string.Format("Stock Taking Result for Period {0} / {1} not finish", _p[0], _p[1]);
                //        BLogging.InsertDetailLog(_log);
                //        return;

                //}
                BLoggingData.IProcessStatus = -1;

                BusinessProcess();
                switch (_iProcessStatus)
                {
                    case 1:
                        //return 1 is mean end with success 
                        break;
                    case 0:
                        //return 0 is mean end with warning 
                        break;
                    case -1:
                        //return -1 is mean end with error
                       // mail.AppID = string.Format("{0}", BLoggingData.AppID);
                        return;
                };
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
                try
                {
                   // mail..AppID = string.Format("{0}", BLoggingData.AppID);
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
            }
            finally
            {
                try
                {
                    SetBatchQEnd();
                    mail.SendEmailToAdministratorInSystemConfig(BLoggingData.BatchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }
        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = BLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                BLogging.StartBatchQ(BLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                BLogging.SetBatchQEnd(BLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion
        private void BusinessProcess()
        {

            string _messageCode = "MSTD0000BERR  : ";
            string _msgNoFinish = "MFAS1301AWRR : ";

            string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
            string messageDesc = string.Empty;

            string _year; 
            string _round;
            string _assetlocaltion;

            try
            {
                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');
                string apl_ID = string.Empty;
                apl_ID = _p[0];

                // Incase multiple file 
                BFD02680Model _val = new BFD02680Model();
                _val = getConfiguration(cfgFileName);
                string zipFileName = _val.FilePreFix;
                string oDirectoryName = _val.FileLocation;

                // zip process 
                zipFileName = zipFileName.Replace(".csv", ".zip");
                zipFileName = string.Format(zipFileName, DateTime.Now);
                string destinationPath_ZipPath = oDirectoryName + @"\" + zipFileName;
                bool ZipSuccess = false;

                zipSourceDirectory = Path.Combine(oDirectoryName, "BFD02680_ZipFolder_"+ apl_ID);


                if (!Directory.Exists(zipSourceDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(zipSourceDirectory);
                    }
                    catch (Exception ex)
                    {
                        InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);

                    }
                }



                DataTable dtGetParam = new DataTable();
                dtGetParam = _dbconn.GetParam(apl_ID);
                // Incase 1 file 
                if (dtGetParam != null && dtGetParam.Rows.Count > 0)
                {
                    _year = string.Empty;
                    _round = string.Empty ;
                    _assetlocaltion = string.Empty;

                    if (dtGetParam.Rows.Count==1)
                    {
                        _year = string.Format("{0}", dtGetParam.Rows[0]["YEAR"]);
                        _round = string.Format("{0}", dtGetParam.Rows[0]["ROUND"]);
                        _assetlocaltion = string.Format("{0}", dtGetParam.Rows[0]["ASSET_LOCATION"]);

                        
                        if (_dbconn.GetPlanStutas(_year, _round, _assetlocaltion))//Check Plan Status != F
                        {

                            //string.Format("Stock Taking Result for Period {0} / {1} not finish", _year, _round);

                            _log = new DetailLogData();
                            _log.AppID = BLoggingData.AppID;
                            _log.Status = eLogStatus.Processing;
                            _log.Level = eLogLevel.Warning;
                            _log.Favorite = false;
                            _log.Description = string.Format("{0}Stock Taking Result for Period {1} / {2} not finish", _msgNoFinish, _year, _round);
                            BLogging.InsertDetailLog(_log);
                            _dbconn.RemoveParam(_year, _round, _assetlocaltion, apl_ID);
                            return;
                        }else {
                            GenerateResultFile(_year, _round, _assetlocaltion, apl_ID);
                            BLoggingData.IProcessStatus = 1;
                        }
                    
                    }
                    else
                    {
                        for (int i = 0; i < dtGetParam.Rows.Count; i++)
                        {
                            _year = string.Empty;
                            _round = string.Empty;
                            _assetlocaltion = string.Empty;

                            _year = string.Format("{0}", dtGetParam.Rows[i]["YEAR"]);
                            _round = string.Format("{0}", dtGetParam.Rows[i]["ROUND"]);
                            _assetlocaltion = string.Format("{0}", dtGetParam.Rows[i]["ASSET_LOCATION"]);

                            if (_dbconn.GetPlanStutas(_year, _round, _assetlocaltion))//Check Plan Status != F
                            {

                              //  string.Format("Stock Taking Result for Period {0} / {1} not finish", _year, _round);

                                _log = new DetailLogData();
                                _log.AppID = BLoggingData.AppID;
                                _log.Status = eLogStatus.Processing;
                                _log.Level = eLogLevel.Warning;
                                _log.Favorite = false;
                                _log.Description = string.Format("{0} : Stock Taking Result for Period {1} / {2} not finish", _msgNoFinish, _year, _round);
                                BLogging.InsertDetailLog(_log);

                                _dbconn.RemoveParam(_year, _round, _assetlocaltion, apl_ID);
                            }
                            else
                            {
                             GenerateMultipleResultFile(_year, _round, _assetlocaltion, apl_ID, zipSourceDirectory, _year+_round+_assetlocaltion);
                            }
                        }

                        ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                        if (ZipSuccess)
                        { _iProcessStatus = 1; }
                        // Insert log : File {1} is generated to {0}
                        DetailLogData _detail = new DetailLogData();
                        _detail.AppID = BLoggingData.AppID;
                        _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, destinationPath_ZipPath);
                        _detail.Level = eLogLevel.Information;
                        BLogging.InsertDetailLog(_detail);

                        // Insert File Download
                        BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);
                        BLoggingData.IProcessStatus = 1;
                    }

                }

            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            finally
            {
                if (Directory.Exists(zipSourceDirectory))
                {
                    Directory.Delete(zipSourceDirectory, true);
                }

            }
        }


        private void GenerateResultFile(string _year, string _round, string _assetlocaltion, string apl_id)
        {
            string messageDesc = string.Empty;
            try
            {

                _dbconn = new BFD02680DAO();
                DataTable dt = new DataTable();
                dt = _dbconn.GetExportStock(_year, _round,_assetlocaltion, apl_id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //3) Generate Interface File (Common Process)
                    string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                    string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config
                    if (string.IsNullOrEmpty(cfgLayoutSection))
                    {
                        throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                    }
                    GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                    _cls.DataSource = dt;
                    _cls.Execute();


                    // Insert log : File {1} is generated to {0}
                    DetailLogData _detail = new DetailLogData();
                    _detail.AppID = BLoggingData.AppID;
                    _detail.Description = string.Format(SendingBatch.I_GENFILE_END, _cls.DirectoryName, _cls.FileName);
                    _detail.Level = eLogLevel.Information;
                    BLogging.InsertDetailLog(_detail);

                    // Insert File Download
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, _cls.DirectoryName + @"\" + _cls.FileName);

                    BLoggingData.IProcessStatus = 1;
                }
                else
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private void GenerateMultipleResultFile(string _year, string _round, string _assetlocaltion,string apl_ID,string zipFullPath, string runningFileName)
        {
            string messageDesc = string.Empty;
            try
            {

                _dbconn = new BFD02680DAO();
                DataTable dt = new DataTable();
                dt = _dbconn.GetExportStock(_year, _round, _assetlocaltion, apl_ID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //3) Generate Interface File (Common Process)
                    string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                    string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config
                    if (string.IsNullOrEmpty(cfgLayoutSection))
                    {
                        throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                    }
                    GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                    _cls.DataSource = dt;
                    _cls.ExecuteByFixPath(zipFullPath,runningFileName);

                }
                else
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Warning;
                    _log.Favorite = false;
                    _log.Description = string.Format("{0}, Stock Taking Result for Period {1} / {2} ",   string.Format(CommonMessageBatch.DATA_NOT_FOUND),_year,_round) ;
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private void InsertLog(string messageCode, params string[] param)
        {
            try
            {
                _log = new DetailLogData();
                _log.AppID = BLoggingData.AppID;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.MessageCode = messageCode;
                BLogging.InsertDetailLog(_log, param);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private bool ZipDataToFile(string sourcePathDirectory, string destinationPath_ZipPath)
        {

            bool _valueReturn;
            string _messageCode = "MSTD0000BERR  : ";

            _valueReturn = false;
            try
            {

                destinationPath_ZipPath = string.Format(destinationPath_ZipPath, DateTime.Now);
                //-------------------
                ZipFile.CreateFromDirectory(sourcePathDirectory, destinationPath_ZipPath);
                _valueReturn = true;
                Directory.Delete(sourcePathDirectory, true);
            }
            catch (Exception ex)
            {
                InsertLog(_messageCode + "Cannot zip file because :" + ex.Message);
            }
            return _valueReturn;
        }


        private BFD02680Model getConfiguration(string cfgFileName)
        {
            string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config
            ConfigurationManager _cfg = new ConfigurationManager(cfgFileName);
            Hashtable _htChild = _cfg.GetChildNodes(cfgLayoutSection);
            Hashtable _ht = _cfg.GetAttributeList(cfgLayoutSection);

            BFD02680Model _val = new BFD02680Model();
            _val.FileLocation= Util.GetStringFromHash(_htChild, "DirectoryName");
            _val.FilePreFix = Util.GetStringFromHash(_htChild, "FilePrefix");
            return _val;
        }     
    }

    public class BFD02680Model
    {
        public string FileLocation { get; set; }
        public string FilePreFix { get; set; }
    }
  }