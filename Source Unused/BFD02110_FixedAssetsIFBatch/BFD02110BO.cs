﻿using System;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTPCommonClass;
using th.co.toyota.stm.fas.common.FTP;
using System.Globalization;
using System.Data;
using System.Collections;
using System.IO;
using System.Text;

namespace th.co.toyota.stm.fas
{
    class BFD02110BO
    {
        private string strBatchName = "Fixed Assets Interface Batch";
        private string strBatchID = "BFD02110";
        private string strMailBodyPath = string.Empty;

        private Log4NetFunction log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();
        public BatchLoggingData BatchQModel = null;

        public BatchLogging bLogging = null;
        private BFD02110DAO _dataAccess = null;
        private DetailLogData LogModel = new DetailLogData();

        public string UploadFileName = string.Empty;
        private string ConfigurationFileName = string.Empty;

        private string SuccessPath = string.Empty;
        private string FailPath = string.Empty;
        private string OriginPath = string.Empty;
        private string FullFileName = string.Empty;
        private string FileName = string.Empty;

        public BFD02110BO()
        {
             bLogging = new BatchLogging();

            // Set Batch Q information
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"])))
            {
                strBatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            }
            BatchQModel = new BatchLoggingData();
            BatchQModel.BatchID = strBatchID;
            BatchQModel.Status = eLogStatus.Queue;
            BatchQModel.Description = "Run by schedule";
            BatchQModel.ReqBy = "System";
            BatchQModel.BatchName = strBatchName;
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"])))
            {
                BatchQModel.BatchID  = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            }            
        }

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        public void Processing()
        {
            try
            {
                //open connection
                bool _success = false;

                  _dataAccess = new BFD02110DAO();
                LogModel.AppID = BatchQModel.AppID; 

                StartBatchQ();

                _success = this.ReceivingFileFromFTP(ConfigFileName); // receiving file from ftp
                if (!_success)
                {
                    // Cannot receive file from FTP 
                    //add to email list
                    mail.AppID = string.Format("{0}", BatchQModel.AppID);
                    return;
                }

                #region "Split file process"
                SplitIFAssetFile(ConfigFileName);

                #endregion
                _success = ReceivingProcess(); // Common Receiving file on local path
                if (!_success)
                {
                    Util.MoveFile(this.FullFileName, this.FailPath);
                    //add to email list
                    mail.AppID = string.Format("{0}", BatchQModel.AppID);
                    return;
                }
                else
                {
                    Util.MoveFile(this.FullFileName, this.SuccessPath);
                }

                BatchQModel.IProcessStatus = this.BusinessProcess();             
            }
            catch (Exception ex) //cannot connect
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                if (!(ex is SqlException))
                {
                    try
                    {
                        Util.MoveFile(this.FullFileName, this.FailPath);
                        LogModel.Status = eLogStatus.Processing;
                        LogModel.Level = eLogLevel.Error;
                        LogModel.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                        bLogging.InsertDetailLog(LogModel);
                    }
                    catch (Exception ex1)
                    {
                        //log4net
                        log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex1.Message, ex1);
                    }

                }
                mail.AppID = string.Format("{0}", BatchQModel.AppID);
                return;
            }
            finally
            {
                try
                {
                    SetBatchQEnd();
                    //if mail list > 0 send email
                    //Incomplete
                
                    mail.SendEmailToAdministratorInSystemConfig(this.strBatchName);
                    _dataAccess.Close();//Close connection
                }
                catch (Exception ex)
                {
                    //log4net
                    log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                }
            }

        }

        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(BatchQModel);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(BatchQModel);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion

        #region "Receiving Method"
          private bool ReceivingProcess()
        {
            bool _valueReturn = false;
            try
            {
                string _LayoutSection = string.Empty;
             
                _valueReturn = ReceivingProcess("LayoutSectionAssetD"); //Asset D
                _valueReturn = ReceivingProcess("LayoutSectionAssetF"); //Asset Finance
                _valueReturn = ReceivingProcess("LayoutSectionAssetH"); //Asset H
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return _valueReturn;
        }

         private bool ReceivingProcess(string _LayoutSection)
        {
            ReceiveFileClass _cls = null;

            try
            {
                string _cfgLayoutSection = ConfigurationManager.GetAppSetting(_LayoutSection); //follow your config
                if (_cfgLayoutSection == null)
                {
                    // message
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, _LayoutSection));
                }

                _cls = new ReceiveFileClass(ConfigFileName, _cfgLayoutSection);
                _cls.Execute();

                this.SuccessPath = _cls.SuccessPath;
                this.FailPath = _cls.FailPath;
                this.OriginPath = _cls.FileLocation;

                //Clear Temp Files, common function auto send a file to another path.      
                if (System.IO.File.Exists(this.SuccessPath + "\\" + _cls.FileName))
                {
                    Util.DeleteFile(this.SuccessPath + "\\" + _cls.FileName, false);
                }
                else if (System.IO.File.Exists(this.FailPath + "\\" + _cls.FileName))
                {
                    Util.DeleteFile(this.FailPath + "\\" + _cls.FileName, false);
                }
                else if (System.IO.File.Exists(this.OriginPath + "\\" + _cls.FileName))
                {
                    Util.DeleteFile(this.OriginPath + "\\" + _cls.FileName, false);
                }

                //_cls.Execute(this.UploadFileName);
                foreach (MessageResult _rs in _cls.ProcessResultList)
                {
                    LogModel.Status = eLogStatus.Processing;
                    LogModel.Level = _rs.MessageResultType;
                    LogModel.Description = _rs.Message;
                    bLogging.InsertDetailLog(LogModel);
                }

            }
            catch (BatchLoggingException bLoggingex)
            {
                LogModel.Status = eLogStatus.Processing;
                if (bLoggingex.arErrorList == null)
                {
                    LogModel.Description = bLoggingex.Message;
                    LogModel.Level = eLogLevel.Error;
                    bLogging.InsertDetailLog(LogModel);

                    if (bLoggingex.Message.StartsWith("No data in file"))
                    {
                        BatchQModel.IProcessStatus = 0;
                    }
                }
                else
                {
                    foreach (MessageResult msg in bLoggingex.arErrorList)
                    {
                        LogModel.Description = msg.Message;
                        LogModel.Level = msg.MessageResultType;
                        bLogging.InsertDetailLog(LogModel);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return true;
        }
        #endregion

        #region "Generate Asset I/F File "
        private bool Splitfileprocess(string full_file_Name)
        {
            bool _valueReturn = false;
            try
            {
                string _LayoutSection = string.Empty;
                DataTable dtAssetH = new DataTable();
                DataTable dtAssetD = new DataTable();
                DataTable dtAssetFinance = new DataTable();

                ArrayList _ar = ReadFileASCII(full_file_Name);
                ArrayList _arAssetH = new ArrayList(); 
                ArrayList _arAssetD = new ArrayList(); 
                ArrayList _arAssetFinance = new ArrayList(); 
                for (int i = 0; i < _ar.Count - 1; i++)
                {
                    string[] _s = _ar[i].ToString().Split('|');

                    if (_s[0].ToString() == "B") // Asset H
                    {
                        _arAssetH.Add(_ar[i]);
                     }
                    if (_s[0].ToString() == "A") // Asset D
                    {
                        _ar[i] = i.ToString() + "|" + _ar[i].ToString();
                        _arAssetD.Add(_ar[i]);
                    }
                    if (_s[0].ToString() == "S") // Asset Finance
                    {
                        _ar[i] = i.ToString() + "|" + _ar[i].ToString();
                        _arAssetFinance.Add(_ar[i]);
                    }
                }
                dtAssetH = ConvertArrayListToDataTable(_arAssetH);
                dtAssetD = ConvertArrayListToDataTable(_arAssetD);
                dtAssetFinance = ConvertArrayListToDataTable(_arAssetFinance);

           
                 GenerateInterfaceFile(dtAssetD, "LayoutSectionGenAssetD"); //Asset D
                 GenerateInterfaceFile(dtAssetFinance, "LayoutSectionGenAssetFinance"); //Asset Finance    
                 GenerateInterfaceFile(dtAssetH, "LayoutSectionGenAssetH"); //Asset H


                _valueReturn = true;
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return _valueReturn;
        }

        public void SplitIFAssetFile(string cfgFileName)
        {

             string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
            ConfigurationManager _cfg = new ConfigurationManager(cfgFileName);
            Hashtable _htChild = _cfg.GetChildNodes(cfgLayoutSection );
            Hashtable _ht = _cfg.GetAttributeList(cfgLayoutSection);

            string FileLocation = Util.GetStringFromHash(_htChild, "DIR");
                string FilePreFix = Util.GetStringFromHash(_htChild, "FILEPREFIX"); 
                string  checkHeader =  Util.GetStringFromHash(_ht, "withHeader");
                string checkFooter =  Util.GetStringFromHash(_ht, "withFooter");
                string HeaderPrefix = string.Empty;
                string FooterPrefix = string.Empty; 

            if (checkHeader.ToLower().Equals("true"))
                   HeaderPrefix = Util.GetStringFromHash(_ht, "HeaderPrefix");
                if (checkFooter.ToLower().Equals("true"))
                 FooterPrefix = Util.GetStringFromHash(_ht, "FooterPrefix");


                string[] _fs1 = System.IO.Directory.GetFiles(FileLocation, FilePreFix);

            // Sorting by update date---> 
            Hashtable _htf = new Hashtable();

            if (_fs1.Length.Equals(0))
            {
                //this.Execute(this.FilePreFix);
            }
            else
            {
                foreach (string _s in _fs1)//user For  1 - - _fs1.count 
                {
                    try
                    {
                        System.IO.FileInfo _f = new System.IO.FileInfo(_s);

                        if (!_htf.ContainsKey(_f.FullName))
                        {
                            this.FullFileName = _f.FullName;
                            this.FileName = _f.Name;
                            this.SuccessPath = System.IO.Path.Combine(_f.Directory.Parent.FullName, "Success");
                            this.FailPath = System.IO.Path.Combine(_f.Directory.Parent.FullName, "Error");
                            if (validateInterfaceFile(_f.FullName, HeaderPrefix, FooterPrefix))
                            {
                                 Splitfileprocess(_f.FullName);
                                //Util.DeleteFile(_f.FullName,false );
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }
            }
        }

        private bool validateInterfaceFile(string FileName,string HeaderPrefix, string FooterPrefix)
        {
            ArrayList _ar = ReadFileASCII(FileName);
            int TotalRowsOfAsscii = _ar.Count;

            if ((!string.IsNullOrEmpty(HeaderPrefix)) && _ar.Count > 0)
            {
                if (!string.Format("{0}", _ar[0]).StartsWith(HeaderPrefix)) //first line
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, HeaderPrefix)));

                
            }
            if ((!string.IsNullOrEmpty(FooterPrefix)) && _ar.Count > 0)
            {
                if (!string.Format("{0}", _ar[_ar.Count - 1]).StartsWith(FooterPrefix)) //last line
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_PREFIX_MISSMATCH, FooterPrefix)));
                }
                //Check row count
                string strTotalRowsOfAsscii = TotalRowsOfAsscii.ToString("0000000");
                string strTotalRowsOnTailer = _ar[_ar.Count - 1].ToString().Substring(3, 7);
                if (!strTotalRowsOfAsscii.Equals(strTotalRowsOnTailer))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_TOTALCOUNT_MISSMATCH, FileName)));
                }
           
            }

            if (_ar.Count == 0 || _ar.Count == 2)
            {
                log4Net.WriteErrorLogFile(string.Format("No data in file <{0}>", this.FileName));
                throw (new BatchLoggingException(string.Format("No data in file <{0}>", this.FileName)));
            }

            return true;
        }


        private ArrayList ReadFileASCII(string full_file_name)
        {
            ArrayList _ar = new ArrayList();
            const int iBufferSize = 128;
            using (System.IO.FileStream _fileStream = System.IO.File.OpenRead(full_file_name))
            //using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.UTF8, true, iBufferSize))
           // using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.GetEncoding(874), true, iBufferSize))
            using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.GetEncoding(874), true, iBufferSize))
            {
                string _line = string.Empty;
                while ((_line = _rd.ReadLine()) != null)
                {
                    _ar.Add(_line);
                }
                _rd.Close();
            }
            return _ar;
        }



        private DataTable ConvertArrayListToDataTable(ArrayList _ar)
        {
            DataTable dt = new DataTable();
            // create columns

            for (int i = 0; i < _ar.Count ; i++)
            {
                string[] _s = _ar[i].ToString().Split('|');
                if (i == 0)
                {
                    for (int j = 0; j < _s.Length ; j++)
                    {
                        dt.Columns.Add(j.ToString());
                    }
                    // Add 'BANK  column for |
                   dt.Columns.Add("BANK");
                 }
                DataRow row = dt.NewRow();
                for (int j = 0; j < _s.Length ; j++)
                {
                    row[j.ToString()] = _s[j];
                }
                dt.Rows.Add(row);
            }

            return dt;
        }

        private void GenerateInterfaceFile(DataTable dt, string _LayoutSection)
        {
            try
            {
                  if (dt != null && dt.Rows.Count > 0)
                {
                    //3) Generate Interface .txt File (Common Process)
                    string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                    string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings[_LayoutSection]; //follow your config
                    if (string.IsNullOrEmpty(cfgLayoutSection))
                    {
                        throw (new Exception(string.Format(CommonMessage.E_CONFIG, _LayoutSection)));
                    }
                    GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                    _cls.HeaderText = string.Format(_cls.HeaderText, DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-US")), "00280"); 
                    _cls.FooterText = string.Format(_cls.FooterText, (dt.Rows.Count + 2).ToString().PadLeft(7, '0')); 
                    _cls.DataSource = dt;
                    _cls.Execute();
                }
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        #endregion


        #region "FTP Receiving Method"

        private bool ReceivingFileFromFTP(string cfgFileName)
        {
            try
            {
                string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["LayoutSectionFTP"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutOracalFTP")));
                }
                FTP ftp = new FTP(cfgFileName, cfgLayoutSectionFTP, "LayoutOracalFTP");
                ftp.DownloadFileFromFTP();              
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);

            }
            return true;
        }
        #endregion
        #region "BusinessProcess Method"

        /** Allow to Edit */
        private Int16 BusinessProcess()
        {
            Int16 _valuReturn;
            _valuReturn= _dataAccess.ReceiveIFAssetsBatch(BatchQModel.AppID, BatchQModel.ReqBy);

            return _valuReturn;
        }


        /** End Allow */
        #endregion



    }
}
