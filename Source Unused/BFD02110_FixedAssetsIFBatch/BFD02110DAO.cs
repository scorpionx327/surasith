﻿using System;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace th.co.toyota.stm.fas
{
    class BFD02110DAO
    {
        private  DataConnection dbconn = null ;

        public BFD02110DAO()
		{
			//
			// TODO: Add constructor logic here
			//

			dbconn = new DataConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]) ;
			if(!dbconn.TestConnection())
			{
				
				throw(new Exception("Cannot connect Database")) ;
			}	
							
		}
        public void Close()
        {
                 dbconn.Close();
        }
        public Int16 ReceiveIFAssetsBatch(int _AppID, string _ReqUser)
        {
            SqlCommand _cmd = new SqlCommand("sp_BFD02110_ReceiveIFAssetsBatch");
            _cmd.CommandType = System.Data.CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@AppID", _AppID);
            _cmd.Parameters.AddWithValue("@UserID", _ReqUser);

            DataSet ds = dbconn.GetDataSet(_cmd);
            DataTable dt = ds.Tables[ds.Tables.Count - 1];

            return Convert.ToInt16(dt.Rows[0][0]);
        }
      }
}
