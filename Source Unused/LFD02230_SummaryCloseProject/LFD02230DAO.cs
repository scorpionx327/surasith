﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace LFD02230_SummaryCloseProject
{
    class LFD02230DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02230DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }                   

        public DataSet GetSummaryCIPRequestReport(DateTime START_PERIOD, DateTime END_PERIOD, string USER_ID)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection()){
                    return ds;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02230_GetSummaryCIPRequestReport");
                cmd.Parameters.AddWithValue("@START_PERIOD", START_PERIOD);
                cmd.Parameters.AddWithValue("@END_PEIORD", END_PERIOD);
                cmd.Parameters.AddWithValue("@USER_ID", USER_ID);

                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);
                //ds = TempGetSummaryCIPRequestReport(START_PERIOD, END_PERIOD, USER_ID);     

                ds.DataSetName = "ReportDataSource";
                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0)
                    {
                        table.TableName = "Content";
                    }
                    if (ii == 1)
                    {
                        table.TableName = "Summary";
                    }
                    ii++;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }

        public string GetLogoURL(string CATEGORY, string SUB_CATEGORY, string CODE)
        {
            string result = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return result;
                }              

                SqlCommand cmd = new SqlCommand("sp_Common_GetSystemValues");
                cmd.Parameters.AddWithValue("@CATEGORY", CATEGORY);
                cmd.Parameters.AddWithValue("@SUB_CATEGORY", SUB_CATEGORY);
                cmd.Parameters.AddWithValue("@CODE", CODE);

                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = _db.GetData(cmd);

                result = (dt.Rows.Count > 0 ? dt.Rows[0]["VALUE"].ToString(): null);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return result;         
        }

        DataSet TempGetSummaryCIPRequestReport(DateTime START_PERIOD, DateTime END_PERIOD, string USER_ID)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "ReportDataSource";

            DataTable dt = new DataTable();
            dt.TableName = "Content";
            dt.Clear();
            dt.Columns.Add("ITEM_NO");
            dt.Columns.Add("COST_CENTER");
            dt.Columns.Add("CIP_NO");
            dt.Columns.Add("ASSET_NO");
            dt.Columns.Add("ASSET_DECRIPTION");
            dt.Columns.Add("DATE_IN_SERVICE");
            dt.Columns.Add("ENTRY_PERIOD");
            dt.Columns.Add("APPROVER");
            dt.Columns.Add("POSITION_DEPARTMENT");

            for (int i = 1; i <= 1000; i++)
            {
                DataRow DataRow = dt.NewRow();
                DataRow["ITEM_NO"] = i;
                DataRow["COST_CENTER"] = "EO2DM003";
                DataRow["CIP_NO"] = "C2017/0001";
                DataRow["ASSET_NO"] = "KMC-A01N0005000";
                DataRow["ASSET_DECRIPTION"] = "ติดตั้งระบบบำบัดฝุ่นแดง";
                DataRow["DATE_IN_SERVICE"] = "13-Sep-2004";
                DataRow["ENTRY_PERIOD"] = "Jan-2017";
                DataRow["APPROVER"] = "Monot 12-Jan-2017";
                DataRow["POSITION_DEPARTMENT"] = "DM/Engine no.3";
                dt.Rows.Add(DataRow);
            }

            ds.Tables.Add(dt);

            DataTable dt2 = new DataTable();
            dt2.Clear();
            dt2.TableName = "Summary";
            dt2.Columns.Add("COST_CENTER_CODE");
            dt2.Columns.Add("COST_CENTER_NAME");
            dt2.Columns.Add("COUNT_CIP", typeof(Int32));          

            for (int i = 1; i <= 100; i++)
            {
                DataRow DataRow = dt2.NewRow();
                DataRow["COST_CENTER_CODE"] = "EO2DM003";
                DataRow["COST_CENTER_NAME"] = "Gasolin ZR G5 Crankshaft";
                DataRow["COUNT_CIP"] = 100;
                dt2.Rows.Add(DataRow);
            }

            ds.Tables.Add(dt2);

            return ds;
        }
    }
}
