﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Compression;

using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02230_SummaryCloseProject
{
    class LFD02230BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02230DAO _dbconn = null;
        private DetailLogData _log = null;
      
        IFormatProvider _culture = new CultureInfo("en-US", true);      
        #endregion

        public LFD02230BO()
        {
            BLoggingData = new BatchLoggingData();         
        }

        public void Processing()
        {
            MailSending mail = null;
            BLoggingData.IProcessStatus = _iProcessStatus;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Summary close project report by time period report";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02230DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                GetSummaryCIPRequestReport();
                BLoggingData.IProcessStatus = _iProcessStatus;
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void GetSummaryCIPRequestReport()
        {
            try
            {

                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');

                _dbconn = new LFD02230DAO();
                DataSet ds = new DataSet();
           
                DateTime START_PERIOD = DateTime.ParseExact(_p[0], "yyyyMMdd", _culture);
                DateTime END_PERIOD = DateTime.ParseExact(_p[1], "yyyyMMdd", _culture);

                string USER_ID = _p[2];            

                ds = _dbconn.GetSummaryCIPRequestReport(START_PERIOD, END_PERIOD, USER_ID);

                if (CountDataAllTable(ds) > 0)
                {
                    _iProcessStatus = GenerateReport(ds, START_PERIOD, END_PERIOD);                   
                }
                else
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                { CountRow += dt.Rows.Count; }
            }
            return CountRow;
        }

        private int GenerateReport(DataSet ds, DateTime Start_Period, DateTime End_Period)
        {
            int result = -1;
            try
            {               
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
          
                if (string.IsNullOrEmpty(cfgLayoutSection)){
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(cfgLayoutSection)){
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                }

                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  
                            
                //Get zip file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, DateTime.Now);
                oFileName = oFileName.Replace(".zip", string.Empty);
                
                //Gernerate Zip File
                GenerateFile(ds, 
                    Start_Period, 
                    End_Period,
                    oDirectoryName, 
                    oFileName, 
                    cfgLayoutSection);
                
                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".zip");
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".zip");
                result = 1;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }

        XtraReport GetXtraReportObject(string LayoutSection, DataSet dataSource, DateTime startPeriod, DateTime endPeriod)
        {
            var xtraReport = XtraReport.FromFile(LayoutSection, true);

            string Param1 = startPeriod.ToString("dd-MMM-yyyy");
            string Param2 = endPeriod.ToString("dd-MMM-yyyy");

            xtraReport.DataSource = dataSource;

            xtraReport.Parameters["Start_Period"].Value = Param1;
            xtraReport.Parameters["Start_Period"].Visible = true;

            xtraReport.Parameters["End_Period"].Value = Param2;
            xtraReport.Parameters["End_Period"].Visible = true;

            LoadImageLogoCompany(xtraReport);

            var subDataSource = dataSource.Copy();
            SetXtraSubReportObject(xtraReport, subDataSource, Param1, Param2);
           
            return xtraReport;
        }
        void SetXtraSubReportObject(XtraReport MainReport, DataSet dataSource, string startPeriod, string endPeriod)
        {
            //Get sub report lay out section name
            string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["SubReportLayoutSection"]; //follow your config

            var subReport = (XRSubreport)MainReport.FindControl("SUMMARY_CIP_CLOSED", true);

            var ReferenceReport = XtraReport.FromFile(cfgLayoutSection, true);

            ReferenceReport.DataSource = dataSource;
            ReferenceReport.Parameters["Start_Period"].Value = startPeriod;
            ReferenceReport.Parameters["Start_Period"].Visible = true;

            ReferenceReport.Parameters["End_Period"].Value = endPeriod;
            ReferenceReport.Parameters["End_Period"].Visible = true;

            LoadImageLogoCompany(ReferenceReport);

            subReport.ReportSource = ReferenceReport;          
        }

        void LoadImageLogoCompany(XtraReport MainReport) {       
            string url = _dbconn.GetLogoURL("PRINT_TAG", "LOGO", "LFD02230");

            var ImageLogo = (XRPictureBox)MainReport.FindControl("Logo", true);
            if (ImageLogo != null){
                // Set its image.
                ImageLogo.ImageUrl = url;
            }                    
        }

        byte[] Export2XLSx(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".xlsx";

            // Get its XLSx export options.
            XlsxExportOptions xlsxOptions = xtraReport.ExportOptions.Xlsx;

            // Set XLSx-specific export options.
            xlsxOptions.ShowGridLines = true;
            xlsxOptions.TextExportMode = TextExportMode.Value;

            // Export the report to XLSx.
            xtraReport.ExportToXlsx(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }
        byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;
         
            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }
        void CreateZipFile(string DirectoryName, string FileName, byte[] PdfBytes, byte[] XlsxBytes)
        {
            var zipPath = DirectoryName + @"\" + FileName + ".zip";

            using (var fileStream = new FileStream(zipPath, FileMode.CreateNew))
            {
                using (var archive = new ZipArchive(fileStream, ZipArchiveMode.Create, true))
                {                                     
                    var zipArchiveEntryPdf = archive.CreateEntry(FileName + ".pdf", CompressionLevel.Fastest);
                    using (var zipStream = zipArchiveEntryPdf.Open()) { 
                        zipStream.Write(PdfBytes, 0, PdfBytes.Length);
                    }

                    var zipArchiveEntryXlsx = archive.CreateEntry(FileName + ".xlsx", CompressionLevel.Fastest);
                    using (var zipStream = zipArchiveEntryXlsx.Open()) {
                        zipStream.Write(XlsxBytes, 0, XlsxBytes.Length);
                    }                   
                }
            }           
        }
        void GenerateFile(DataSet ds, DateTime Start_Period, DateTime End_Period, string DirectoryName, string FileName, string LayoutDirectory)
        {
            try
            {
                var xTraReport = GetXtraReportObject(LayoutDirectory, ds, Start_Period, End_Period);
              
                var xlsxBytes = Export2XLSx(xTraReport, DirectoryName, FileName);
                var pdfBytes = Export2PDF(xTraReport, DirectoryName, FileName);

                CreateZipFile(DirectoryName, FileName, pdfBytes, xlsxBytes);
                DeleteJunkFiles(DirectoryName, FileName);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        void DeleteJunkFiles(string DirectoryName, string FileName)
        {                   
            foreach (string file in Directory.GetFiles(DirectoryName))
            {                
                var item = new FileInfo(file);
                if (item.Name.StartsWith(FileName) && (item.Name.ToLower().EndsWith(".pdf") || item.Name.ToLower().EndsWith(".xlsx"))) {                    
                    item.Delete();
                }             
            }                       
        }     
    }
}
