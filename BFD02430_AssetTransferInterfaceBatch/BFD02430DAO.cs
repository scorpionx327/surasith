﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
namespace th.co.toyota.stm.fas
{
    class BFD02430DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public BFD02430DAO()
        {
            _strConn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];           
        }


        public DataTable GetAssetTransfer(int appID, string updateBy)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02430_GetAssetTransfer");
                //cmd.Parameters.Add(new SqlParameter("@APPID", SqlDbType.Int)).Value = appID;
                //cmd.Parameters.Add(new SqlParameter("@USER", SqlDbType.Text)).Value = updateBy;
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }

        public void  UpdateAssetHis()
        {
                int result = -1;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return;
                }
                    SqlCommand cmd = new SqlCommand("sp_BFD02430_UpdateAssetHis");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                object obj = _db.Execute(cmd);
                result = Convert.ToInt16(obj);



            }
            catch (Exception ex)
            {
                throw (ex);
            }
            
        }
    }
}

public class MSG
{
    public const string FileSuccess = "MSTD4005AINF"; //MSTD4005AINF	 File {0} is successfully generated in {1}.	
    public const string BatchBegin = "MSTD7000BINF"; //MSTD7000BINF : {0} Begin
    public const string BatchEndSuccessfully = "MSTD7001BINF"; //MSTD7001BINF : {0} End successfully
    public const string BatchEndError = "MSTD7002BINF";  //MSTD7002BINF :  {0} End with error {1}
    public const string DataNotFound = "MSTD7054BERR";  //MSTD7054BERR	: {0} Data not found from {1}		
}