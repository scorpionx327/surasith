﻿using System;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTPCommonClass;
using th.co.toyota.stm.fas.common.FTP;
using System.Globalization;
using System.Data;
using System.Collections;


namespace th.co.toyota.stm.fas
{
    class BFD02430BO
    {

        private string strBatchName = "Asset Transfer Interface Batch";
        private string strBatchID = "BFD02430";

        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private BFD02430DAO _dbconn = null;
        private DetailLogData _log = null;



        public BFD02430BO()
        {
            BLoggingData = new BatchLoggingData();
            BLogging = new BatchLogging();

            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["BatchName"])))
            {
                strBatchName = System.Configuration.ConfigurationSettings.AppSettings["BatchName"];
            }
            // Set Batch Q information
            BLoggingData = new BatchLoggingData();
            BLoggingData.BatchID = strBatchID;
            BLoggingData.Status = eLogStatus.Queue;
            BLoggingData.Description = "Run by schedule";
            BLoggingData.ReqBy = "System";
            BLoggingData.BatchName = strBatchName;
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["BatchID"])))
            {
                BLoggingData.BatchID = System.Configuration.ConfigurationSettings.AppSettings["BatchID"];
            }
        
      }
        public void Processing()
        {
            MailSending mail = null;
            try
            {
                mail = new MailSending();
                _batchName = BLoggingData.BatchName; // System.Configuration.ConfigurationSettings.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Generate Asset Transfer Interface";
                }

                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new BFD02430DAO();

                StartBatchQ();

                BusinessProcess();
                switch (_iProcessStatus)
                {
                    case 1:
                        //return 1 is mean end with success 
                        break;
                    case 0:
                        //return 0 is mean end with warning 
                        break;
                    case -1:
                        //return -1 is mean end with error
                       // mail.AppID = string.Format("{0}", BLoggingData.AppID);
                        return;
                };
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                   // mail..AppID = string.Format("{0}", BLoggingData.AppID);
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
            }
            finally
            {
                try
                {
                    SetBatchQEnd();
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }
            #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = BLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                BLogging.StartBatchQ(BLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                BLogging.SetBatchQEnd(BLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion

        private void BusinessProcess()
        {
            string messageDesc = string.Empty;
            try
            {
                _dbconn = new BFD02430DAO();
                DataTable dt = new DataTable();
                dt = _dbconn.GetAssetTransfer(BLoggingData.AppID, BLoggingData.ReqBy);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                    //3) Generate Interface .txt File (Common Process)
                    string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                    string cfgLayoutSection = System.Configuration.ConfigurationSettings.AppSettings["LayoutSection"]; //follow your config
                    if (string.IsNullOrEmpty(cfgLayoutSection))
                    {
                        throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                    }
                    GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                    //string spacePad = System.Configuration.ConfigurationSettings.AppSettings["SpacePad"];
                    //if (string.IsNullOrEmpty(spacePad))
                    //{
                    //    spacePad = "0";
                    //}
                    //int iSpacePad = Convert.ToInt32(spacePad);
                    //_cls.HeaderText = string.Format("#HPLANII{0}", DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-US"))).PadRight(iSpacePad, ' '); //#HPLANIIYYYYMMDDhhmmss
                    _cls.HeaderText = string.Format(_cls.HeaderText, DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-US")),"00320"); //#HPLANIIYYYYMMDDhhmmss
                    //_cls.FooterText = string.Format(_cls.FooterText, (dt.Rows.Count + 2).ToString().PadLeft(6, '0')).PadRight(iSpacePad, ' '); //#T000072
                    _cls.FooterText = string.Format(_cls.FooterText, (dt.Rows.Count + 2).ToString().PadLeft(7, '0')); //#T000072
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        _cls.DataSource = dt;
                                   }
                            else
                            {
                            BLoggingData.IProcessStatus = 0; //End with warning  no record to export
                        }
                   _cls.Execute();

                    //4) Get generated Interface file from File Generation folder, and store the file into Send Folder in order to send to  system.

                     InsertLog(MSG.FileSuccess, _batchName, _cls.FullFileName);//File {0} is successfully generated in {1}.
                    _iProcessStatus = FTPFile(_cls.FullFileName, cfgFileName, _cls.FileName);
                //  _iProcessStatus = 1;
                //Update Data
                if (_iProcessStatus > 0)
                {
                    _dbconn.UpdateAssetHis();
                }

                BLoggingData.IProcessStatus = 1;


            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private int FTPFile(string fullFileName, string cfgFileName, string fileName)
        {
            int result = -1;
            string ftpFile = "Tranfer(FTP) file : " + fileName;
            try
            {
                InsertLog(MSG.BatchBegin, ftpFile); //MSTD7000BINF : {0} Begin

                string cfgLayoutSectionFTP = System.Configuration.ConfigurationSettings.AppSettings["LayoutSectionFTP"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSectionFTP")));
                }

                ConfigurationManager cfg = new ConfigurationManager(cfgFileName);
                System.Collections.Hashtable ht = cfg.GetChildNodes(cfgLayoutSectionFTP);
                foreach (string cfgFTP in ht.Keys)
                {
                    try
                    {
                        FTP ftp = new FTP(cfgFileName, cfgLayoutSectionFTP + @"/" + cfgFTP);
                        ftp.UploadFileName = fullFileName;
                        ftp.Execute();

                        result = 1;
                        InsertLog(MSG.BatchEndSuccessfully, ftpFile); //MSTD7001BINF : {0} End successfully
                    }
                    catch (Exception exc)
                    {
                        if (exc.HResult.Equals(-2146231997))
                        {
                            InsertLog(MSG.DataNotFound, "FTP", "configuration"); //MSTD7054BERR : {0} Data not found from {1}	
                            throw;
                        }
                        throw exc;
                    }
                }
            }
            catch (Exception ex)
            {
                InsertLog(MSG.BatchEndError, ftpFile, string.Empty); //MSTD7002BINF :  {0} End with error {1}
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }
        private void InsertLog(string messageCode, params string[] param)
        {
            try
            {
                _log = new DetailLogData();
                _log.AppID = BLoggingData.AppID;
                _log.Status = eLogStatus.Processing;
                _log.Favorite = false;
                _log.MessageCode = messageCode;
                BLogging.InsertDetailLog(_log, param);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
    }
}