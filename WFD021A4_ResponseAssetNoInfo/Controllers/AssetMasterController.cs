﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using th.co.toyota.stm.fas.Models.SFAS.WFD021A4;
using WFD021A4_ResponseAssetNoInfo.Models;

namespace WFD021A4_ResponseAssetNoInfo.Controllers
{
    public class AssetMasterController : ApiController
    {
        [BasicAuthenication]
        public async Task<HttpStatusCodeResult> Post([FromBody]WFD021A4ResponseRequestNo value)
        {
            try
            {
                if(value == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NoContent);
                }
                var _bo = new th.co.toyota.stm.fas.BusinessObject.WFD021A4BO();
                _bo.UpdateAsseetNo(value);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        public string Get()
        {
            return "TFAST";
        }

    }
}
