using System;
using System.Reflection;

namespace WFD021A4_ResponseAssetNoInfo.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}