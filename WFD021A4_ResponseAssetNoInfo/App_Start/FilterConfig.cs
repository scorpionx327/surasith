﻿using System.Web;
using System.Web.Mvc;

namespace WFD021A4_ResponseAssetNoInfo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
