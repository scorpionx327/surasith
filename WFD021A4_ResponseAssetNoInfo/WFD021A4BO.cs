﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.DAO.SFAS;
using th.co.toyota.stm.fas.Models.SFAS.WFD021A4 ;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD021A4BO
    {

        private WFD021A4DAO dao;
        public WFD021A4BO()
        {
            dao = new WFD021A4DAO();
        }
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool ValidateUser(string _UserName, string _Password)
        {
            var _sys = (new DAO.Common.SystemDAO().SelectSystemDatas("SYSTEM_CONFIG", "SAP_RESPONSE_ASSET_MASTER_INFO"));
            if (_sys.Find(x => x.CODE.ToUpper() == "USERNAME") == null)
                return false;

            if (_sys.Find(x => x.CODE.ToUpper() == "PASSWORD") == null)
                return false;

            var _user = _sys.Find(x => x.CODE.ToUpper() == "USERNAME").VALUE;
            var _pwd = _sys.Find(x => x.CODE.ToUpper() == "PASSWORD").VALUE;

            if(_user == _UserName && _pwd == _Password)
            {
                return true ;
            }
            return false;


        }

        public void UpdateAsseetNo(WFD021A4ResponseRequestNo _data)
        {

            try
            {
                _log.Info("Receive Asset No Info");

                if(_data == null)
                {
                    _log.Info("No request data");
                    return;
                }

                var _json = Newtonsoft.Json.JsonConvert.SerializeObject(_data);
                _log.Info(_json);

                if (_data.ASSETS == null || _data.ASSETS.Count == 0)
                {
                    _log.Info("No Asset data");
                    return;
                }

                dao.UpdateAssetNo(_data);
                //Update Log
                _log.Info("Update successfully");
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
        }
        
    }
}
