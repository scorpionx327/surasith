﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
namespace th.co.toyota.stm.fas
{
    class BFD02530DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public BFD02530DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];           
        }


        public DataTable GetRetirementAssetList(BatchLoggingData _data)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02530_GetAssetDispose");
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }

        public void UpdateAssetStatus()
        {
          
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }
                SqlCommand cmd = new SqlCommand("sp_BFD02530_UpdateAssetStatus");
                cmd.CommandType = CommandType.StoredProcedure;
               _db.Execute(cmd);
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}

public class MSG
{
    public const string FileSuccess = "MSTD4005AINF"; //MSTD4005AINF	 File {0} is successfully generated in {1}.	
    public const string BatchBegin = "MSTD7000BINF"; //MSTD7000BINF : {0} Begin
    public const string BatchEndSuccessfully = "MSTD7001BINF"; //MSTD7001BINF : {0} End successfully
    public const string BatchEndError = "MSTD7002BINF";  //MSTD7002BINF :  {0} End with error {1}
    public const string DataNotFound_NoParams = "MCOM2100BWRN"; // No data found
    public const string DataNotFound = "MSTD7054BERR"; //{0} Data not found from {1}
}