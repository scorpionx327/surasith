﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices;
using th.co.toyota.stm.fas.common;

namespace th.co.toyota.stm.fas
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleTitle(string _title);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main(string[] args)
        {
            Log4NetFunction _log4net = new Log4NetFunction();
            string _modName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            string _procName = System.IO.Path.GetFileNameWithoutExtension(_modName);
            System.Diagnostics.Process[] _appProc = System.Diagnostics.Process.GetProcessesByName(_procName);
            //if (_appProc.Length > 1)
            //{
            //    Console.WriteLine("There is another instances running. This instance will be closed automatically.");
            //    return;
            //}
            /************************************   Check Multiple Running    ****************************************/
            string _cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
            if (!System.IO.File.Exists(_cfgFileName))
            {
                // message
                Console.WriteLine("Not found Configuration File : {0}", _cfgFileName);
                return;
            }
            
            try
            {
                //Configuration Checking => BatchID, BatchName [Mandatory]
                if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"]))
                {
                    throw (new Exception("No found [BatchName] in Configuration "));
                }
                if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"]))
                {
                    throw (new Exception("No found [BatchID] in Configuration "));
                }
                
                var cls = new LFD02A02BO();

                if (args.Length >= 3) // manual
                {
                    cls.bLoggingData.AppID = Convert.ToInt32(args[0]);
                    cls.bLoggingData.BatchID = args[1];
                    cls.bLoggingData.Arguments = args[2];
                }
                else //Not Allow Auto
                {
                    //Log4net
                    _log4net.WriteErrorLogFile("Invalid Batch Parameter");
                    return;
                }
                if (System.Configuration.ConfigurationManager.AppSettings["BatchID"] != cls.bLoggingData.BatchID)
                {
                    //Log4Net
                    _log4net.WriteErrorLogFile("Invalid Batch ID");
                    return;
                }

                SetConsoleTitle(cls.bLoggingData.BatchID);
                
                cls.Processing();
            }
            catch (Exception ex)
            {
                    _log4net.WriteErrorLogFile(ex.Message);
                    return;
            }

        }
    }
}