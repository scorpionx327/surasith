﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTP;


namespace th.co.toyota.stm.fas
{
    class LFD02A02BO
    {
        #region "Variable Declare"
      
        public BatchLoggingData bLoggingData = null;
        public BatchLogging bLogging = null;

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02A02DAO _dbconn = null;
        private DetailLogData _log = null;
        #endregion

        public LFD02A02BO()
        {
            bLoggingData = new BatchLoggingData();
            bLogging = new BatchLogging();

            bLoggingData.BatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            bLoggingData.BatchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            

        }

        public void Processing()
        {
            MailSending _mail = null;
            try
            {
                _mail = new MailSending();
               

                bLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02A02DAO();

               this.StartBatchQ();

               this.BusinessProcess();
               
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = bLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    bLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
            }
            finally
            {
                try
                {
                    this.SetBatchQEnd();
                    _mail.SendEmailToAdministratorInSystemConfig(bLoggingData.BatchID);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);
            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                bLogging.SetBatchQEnd(bLoggingData);
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        #endregion
        private void BusinessProcess()
        {
            try
            {
                bLoggingData.ProcessStatus = eLogLevel.Error;

                string[] _p = bLoggingData.Arguments.Split('|'); //Guid|DocNo
                if(_p.Length != 2)
                {
                    _log = new DetailLogData();
                    _log.AppID = bLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = "Batch Parameter shoud be GUID|DOCNO";
                    bLogging.InsertDetailLog(_log);
                    return;
                }

                _dbconn = new LFD02A02DAO();
                DataTable dt = new DataTable();
                //dt = _dbconn.GetImpairementAssetList(bLoggingData.AppID, _p[0]);
                dt = _dbconn.GetImpairementAssetList(  _p[0]);
                if (dt == null || dt.Rows.Count == 0)
                {
                    _log = new DetailLogData();
                    _log.AppID = bLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    bLogging.InsertDetailLog(_log);
                    return;
                }

                //Generate File 
                this.GenerateReport(dt);

                bLoggingData.ProcessStatus = eLogLevel.Information;

            }
            catch (Exception ex)
            {
                bLoggingData.ProcessStatus = eLogLevel.Error;
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private void GenerateReport(DataTable _dt)
        {
          
            try
            {
                string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);
                _cls.DataSource = _dt;
                _cls.Execute();

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = bLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, _cls.DirectoryName, _cls.FileName);
                _detail.Level = eLogLevel.Information;
                bLogging.InsertDetailLog(_detail);

                // Insert File Download
                bLogging.AddDownloadFile(bLoggingData.AppID, bLoggingData.ReqBy, bLoggingData.BatchID, _cls.DirectoryName + @"\" + _cls.FileName);
             
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
           
        }        
    }
}