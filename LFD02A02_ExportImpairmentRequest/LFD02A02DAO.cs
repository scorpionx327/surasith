﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
namespace th.co.toyota.stm.fas
{
    class LFD02A02DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02A02DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataTable GetImpairementAssetList(  string _GUID)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    throw (new Exception("Cannot connect database"));
                }
                SqlCommand cmd = new SqlCommand("sp_LFD02A02_ExportImpairementReport");
           //     cmd.Parameters.AddWithValue("@AppID", _AppID);
                cmd.Parameters.AddWithValue("@GUID", _GUID);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }
    }
}