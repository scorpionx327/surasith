﻿using System;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace LFD021C0_SummaryCloseProject
{
    class LFD021C0DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD021C0DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataTable GetReprintRequestData(string COMPANY, string COST_CODE)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_LFD02112_ExportAssetsPlateForOutsource");
                cmd.Parameters.AddWithValue("@COMPANY", COMPANY);
                cmd.Parameters.AddWithValue("@COST_CODE", COST_CODE);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }
    }
}
