﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02660_PrintOutFAUpdateStatus
{
    class LFD02660BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02660DAO _dbconn = null;
        private DetailLogData _log = null;

        IFormatProvider _culture = new CultureInfo("en-US", true);
        public string zipSourceDirectory = string.Empty;
        #endregion

        public LFD02660BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
            //BLoggingData._iProcessStatus = _iProcessStatus;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Print out FA update scan status(countuncount) withwithout cover page report";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02660DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                GetStockTakingResult();
                //BLoggingData.IProcessStatus = _iProcessStatus;
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void GetStockTakingResult()
        {
            string _messageCode = "MSTD0000BERR  : ";
            try
            {
                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');

                _dbconn = new LFD02660DAO();
                DataSet ds = new DataSet();
                

                string Year = _p[0];
                string Round = _p[1];
                string AssetLocation = _p[2];
                string CostCenterCode = _p[3];
                string DocumentNo = _p[4];
                string Company = _p[5];


                if (CostCenterCode != "")
                {
                    ds = _dbconn.GetStockTakingResult(Year, Round, AssetLocation, CostCenterCode, DocumentNo, Company);

                    if (CountDataAllTable(ds) > 0)
                    {
                        _iProcessStatus = GenerateReport(ds, _p.ToList<string>());
                    }
                    else
                    {
                        _log = new DetailLogData();
                        _log.AppID = BLoggingData.AppID;
                        _log.Status = eLogStatus.Processing;
                        _log.Level = eLogLevel.Error;
                        _log.Favorite = false;
                        _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                        BLogging.InsertDetailLog(_log);
                    }

                }
                else
                {
                    //Get lay out section name
                    string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                    if (string.IsNullOrEmpty(cfgLayoutSection))
                    {
                        throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                    }
                    if (!System.IO.File.Exists(cfgLayoutSection))
                    {
                        throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                    }

                    //Get output directory name
                    string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  
                    //Get PDF file name

                    string zipFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;


                    zipSourceDirectory = Path.Combine(oDirectoryName, "LFD02660BO_ZipFolder");
                    if (!Directory.Exists(zipSourceDirectory))
                    {
                        try
                        {
                            Directory.CreateDirectory(zipSourceDirectory);
                        }
                        catch (Exception ex)
                        {
                            InsertLog(_messageCode + "Cannot create directory :" + zipSourceDirectory + "(zip Directory) because  : " + ex.Message);

                        }
                    }

                    DataTable dt = _dbconn.GetCostDoc(Year, Round, AssetLocation);

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            CostCenterCode = string.Format("{0}", dr["COST_CODE"]);
                            DocumentNo = string.Format("{0}", dr["DOC_NO"]);

                            ds = _dbconn.GetStockTakingResult(Year, Round, AssetLocation, CostCenterCode, DocumentNo, Company);

                            string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                            oFileName = string.Format(oFileName, DateTime.Now);
                            oFileName = oFileName.Replace(".pdf", string.Empty);

                            if (CountDataAllTable(ds) > 0)
                            {
                                _p[3] = CostCenterCode;
                                _p[4] = DocumentNo;
                            
                                GenerateFile(ds, _p.ToList<string>(), zipSourceDirectory, oFileName, cfgLayoutSection);

                            }

                        }
                        


                        // zip process 
                        zipFileName = zipFileName.Replace(".pdf", ".zip");
                        zipFileName = string.Format(zipFileName, DateTime.Now);
                        string destinationPath_ZipPath = oDirectoryName + @"\" + zipFileName;
                        bool ZipSuccess = false;
                        ZipSuccess = ZipDataToFile(zipSourceDirectory, destinationPath_ZipPath);

                        if (ZipSuccess) _iProcessStatus = 1;

                        DetailLogData _detail = new DetailLogData();
                        _detail.AppID = BLoggingData.AppID;
                        _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, destinationPath_ZipPath);
                        _detail.Level = eLogLevel.Information;
                        BLogging.InsertDetailLog(_detail);

                        // Insert File Download
                        BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, destinationPath_ZipPath);


                    }

                }
               
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        XtraReport GetXtraReportObject(string LayoutSection, DataSet dataSource, List<string> Parameters)
        {
            var xtraReport = XtraReport.FromFile(LayoutSection, true);
            xtraReport.DataSource = dataSource;
            return xtraReport;
        }

        void GenerateFile(DataSet ds, List<string> Parameters, string DirectoryName, string FileName, string LayoutDirectory)
        {
            try
            {
                var xTraReport = GetXtraReportObject(LayoutDirectory, ds, Parameters);
                var pdfBytes = Export2PDF(xTraReport, DirectoryName, FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        int GenerateReport(DataSet ds, List<string> Parameters)
        {
            int result = -1;
            try
            {
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(cfgLayoutSection))
                {
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                }

                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                //Get PDF file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, DateTime.Now);
                oFileName = oFileName.Replace(".pdf", string.Empty);

                //Gernerate PDF File
                GenerateFile(ds,
                    Parameters,
                    oDirectoryName,
                    oFileName,
                    cfgLayoutSection);

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".pdf");
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".pdf");
                result = 1;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            return result;
        }
        int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if ((ds != null) && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                { CountRow += dt.Rows.Count; }
            }
            return CountRow;
        }
        byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }

        private bool ZipDataToFile(string sourcePathDirectory, string destinationPath_ZipPath)
        {
            bool _valueReturn;
            string _messageCode = "MSTD0000BERR  : ";
            _valueReturn = false;
            try
            {

                destinationPath_ZipPath = string.Format(destinationPath_ZipPath, DateTime.Now);
                //-------------------
                ZipFile.CreateFromDirectory(sourcePathDirectory, destinationPath_ZipPath);
                _valueReturn = true;
                Directory.Delete(sourcePathDirectory, true);
            }
            catch (Exception ex)
            {
                InsertLog(_messageCode + "Cannot zip file because :" + ex.Message);
            }
            return _valueReturn;
        }

        private void InsertLog(string Log_Description)
        {
            _log = new DetailLogData();
            _log.AppID = BLoggingData.AppID;
            _log.Status = eLogStatus.Processing;
            _log.Level = eLogLevel.Error;
            _log.Description = Log_Description;
            BLogging.InsertDetailLog(_log);

            _log4Net.WriteErrorLogFile(Log_Description);
        }
    }
}
