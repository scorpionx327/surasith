﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using th.co.toyota.stm.fas.common;

namespace LFD02660_PrintOutFAUpdateStatus
{
    class LFD02660DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02660DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataSet GetStockTakingResult(string Year, string Round, string AssetLocation, string CostCenterCode, string DocNo, string Company)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02660_GetFAUpdateScan");

                cmd.Parameters.AddWithValue("@YEAR", Year);                              
                cmd.Parameters.AddWithValue("@ROUND", Round);
                cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);
                cmd.Parameters.AddWithValue("@COST_CENTER_CODE", CostCenterCode);                
                if (!DocNo.Equals(string.Empty)) {
                    cmd.Parameters.AddWithValue("@DOCUMENT_NO", DocNo);
                }         
                cmd.Parameters.AddWithValue("@COMPANY", Company);

                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);

                ds.DataSetName = "ReportDataSource";
                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0)
                    {
                        table.TableName = "STOCK_TAKING_RESULT";
                    }
                    ii++;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }

        public DataTable GetCostDoc(string Year, string Round, string AssetLocation)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02660_GetCostDoc");

                cmd.Parameters.AddWithValue("@YEAR", Year);
                cmd.Parameters.AddWithValue("@ROUND", Round);
                cmd.Parameters.AddWithValue("@ASSET_LOCATION", AssetLocation);


                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return dt;
        }
        
    }
}
