﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
using System.Configuration;

namespace LFD021A2_FixedAssetsList
{
    public class LFD021A2DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD021A2DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }     
        public DataTable GetFixedAssetResult(WFD02120SearhConditionModel data)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }

                var param = new List<SqlParameter>(); 
				SqlCommand cmd = new SqlCommand("sp_LFD021A2_GetFixedAssetRequest"); 
                cmd.Parameters.AddWithValue("@GUID", _db.IfNull(data.GUID));
                cmd.CommandType = CommandType.StoredProcedure;
          
                var _dt = _db.GetData(cmd);
                return _dt;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            
        }
    }

    public class WFD02120SearhConditionModel
    {
        public string Company { get; set; }
        public string AssetNo { get; set; }
        public string AssetSub { get; set; }
        public string AssetName { get; set; }
        public string AssetClass { get; set; }
        public string AssetGroup { get; set; }
        public string CostCenterCode { get; set; }
        public string ResponsibleCostCenter { get; set; }
        public string Location { get; set; }
        public string WBSProject { get; set; }
        public string WBSBudget { get; set; }
        public string BOI { get; set; }
        public string CapitalizeDateFrom { get; set; }
        public string CapitalizeDateTo { get; set; }
        public string AssetPlateNo { get; set; }
        public string AssetStatus { get; set; }

        public string PrintStatus { get; set; }
        public string PhotoStatus { get; set; }
        public string MapStatus { get; set; }
        public int? CostPending { get; set; }
 
        public bool IsAECUser { get; set; }
        public string GUID { get; set; }

    }

}
