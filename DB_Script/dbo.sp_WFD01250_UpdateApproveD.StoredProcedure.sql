DROP PROCEDURE [dbo].[sp_WFD01250_UpdateApproveD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 10-03-2017
-- Description:	Update approve d data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_UpdateApproveD]
	@APPRV_ID	int ,
	@INDX	int ,
	@ROLE	varchar(3) ,
	@APPRV_GROUP	varchar(8) ,
	@DIVISION	varchar(6) ,
	@REQUIRE_FLAG	varchar(8) ,
	@CONDITION_CODE	varchar(3) ,
	@OPERATOR	varchar(2) ,
	@VALUE1	varchar(20) ,
	@VALUE2	varchar(20) ,
	@FINISH_FLOW	varchar(1) ,
	@ALLOW_DEL_ITEM	varchar(1) ,
	@ALLOW_REJECT	varchar(1) ,
	@ALLOW_SEL_APPRV	varchar(1) ,
	@REJECT_TO	varchar(3) ,
	@HIGHER_APPR	varchar(1) ,
	@KPI_LEADTIME	INT =NULL,
	@GEN_FILE		VARCHAR(1),
	@NOTI_MODE		VARCHAR(1),
	@OP_MODE		VARCHAR(1) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF( SELECT COUNT(1) FROM TB_M_APPROVE_D WHERE APPRV_ID=@APPRV_ID and INDX=@INDX) > 0
	BEGIN

		UPDATE [dbo].[TB_M_APPROVE_D]
		   SET 
			  ROLE = @ROLE
			  ,APPRV_GROUP = @APPRV_GROUP
			  ,DIVISION = @DIVISION
			  ,REQUIRE_FLAG = @REQUIRE_FLAG
			  ,CONDITION_CODE = @CONDITION_CODE
			  ,OPERATOR = @OPERATOR
			  ,VALUE1 = @VALUE1
			  ,VALUE2 = @VALUE2
			  ,FINISH_FLOW = @FINISH_FLOW
			  ,ALLOW_DEL_ITEM = @ALLOW_DEL_ITEM
			  ,ALLOW_REJECT = @ALLOW_REJECT
			  ,ALLOW_SEL_APPRV = @ALLOW_SEL_APPRV
			  ,REJECT_TO = @REJECT_TO
			  ,HIGHER_APPR = @HIGHER_APPR
			  ,GENERATE_FILE	= @GEN_FILE
			   ,LEAD_TIME		= @KPI_LEADTIME
			   ,NOTI_BY_EMAIL	= @NOTI_MODE
			   ,OPERATION		= @OP_MODE
		 WHERE APPRV_ID = @APPRV_ID and INDX = @INDX

	END
	ELSE
	BEGIN

		INSERT INTO [dbo].[TB_M_APPROVE_D]
			   ([APPRV_ID]
			   ,[INDX]
			   ,[ROLE]
			   ,[APPRV_GROUP]
			   ,[DIVISION]
			   ,[REQUIRE_FLAG]
			   ,[CONDITION_CODE]
			   ,[OPERATOR]
			   ,[VALUE1]
			   ,[VALUE2]
			   ,[FINISH_FLOW]
			   ,[ALLOW_DEL_ITEM]
			   ,[ALLOW_REJECT]
			   ,[ALLOW_SEL_APPRV]
			   ,[REJECT_TO]
			   ,[HIGHER_APPR]
			   ,GENERATE_FILE
			   ,LEAD_TIME
			   ,NOTI_BY_EMAIL
			   ,OPERATION
			   )
		 VALUES
			   (@APPRV_ID
			   ,@INDX
			   ,@ROLE
			   ,@APPRV_GROUP
			   ,@DIVISION
			   ,@REQUIRE_FLAG
			   ,@CONDITION_CODE
			   ,@OPERATOR
			   ,@VALUE1
			   ,@VALUE2
			   ,@FINISH_FLOW
			   ,@ALLOW_DEL_ITEM
			   ,@ALLOW_REJECT
			   ,@ALLOW_SEL_APPRV
			   ,@REJECT_TO
			   ,@HIGHER_APPR
			   ,@GEN_FILE
			   ,@KPI_LEADTIME
			   ,@NOTI_MODE
			   ,@OP_MODE
			   )

	END

    


END
GO
