DROP PROCEDURE [dbo].[sp_BFD0261A_InsertDataFromStagging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_BFD0261A_InsertDataFromStagging]
(
	@UserID VARCHAR(8),
    @StockYear VARCHAR(4),
    @StockRound VARCHAR(2),
    @StockAssetLocation VARCHAR(1), --I is In house,O is out-source
    @QTyOfHanheld VARCHAR(8),
    @TargetDateFrom VARCHAR(8), --format YYYYMMDD
    @TargetDateTo VARCHAR(8), --format YYYYMMDD
    @BreakTime VARCHAR(8),
	@ERROR_MESSAGE	VARCHAR(MAX) OUT ,
	@COMPANY T_COMPANY = null,
	@DATE_IN_SERVICE datetime = null,
	@ASSET_CLASS T_ASSET_CLASS = null

)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;
-- 1  Insert into   TB_R_STOCK_TAKE_H
INSERT INTO [TB_R_STOCK_TAKE_H]
           ([STOCK_TAKE_KEY] ,[YEAR],[ROUND]  ,[ASSET_LOCATION],[DATA_AS_OF]
           ,[SUB_TYPE] ,[TARGET_DATE_FROM]   ,[TARGET_DATE_TO] ,[QTY_OF_HANDHELD]
           ,[BREAK_TIME_MINUTE] ,[ASSET_STATUS]  ,[PLAN_STATUS]
           ,[ALL_ASSET_TOTAL] ,[MC_TOTAL] ,[EC_TOTAL] ,[PC_TOTAL] ,[LC_TOTAL]
           ,[REMARK] ,[CREATE_DATE] ,[CREATE_BY] ,[UPDATE_DATE]  ,[UPDATE_BY],  COMPANY , ASSET_CLASS )
     VALUES
           (CONCAT(@StockYear,@StockRound,@StockAssetLocation) ,@StockYear ,@StockRound,@StockAssetLocation
            ,NULL --[DATA_AS_OF]
            , NULL -- <SUB_TYPE, varchar(150),>
           ,CONVERT(DATE,@TargetDateFrom,112) 
           ,CONVERT(DATE,@TargetDateTo,112)  
           ,CONVERT(INT,@QTyOfHanheld) --<QTY_OF_HANDHELD, int,>
           ,CONVERT(INT,@BreakTime) --<BREAK_TIME_MINUTE, int,>
           ,NULL  -- <ASSET_STATUS, varchar(1),> Both/parent/chield
           ,'D' --- <PLAN_STATUS, varchar(1),>
           ,NULL --<ALL_ASSET_TOTAL, int,>
           ,NULL --<MC_TOTAL, int,>
           ,NULL --<EC_TOTAL, int,>
           ,NULL --<PC_TOTAL, int,>
           ,NULL --<LC_TOTAL, int,>
           ,NULL --<REMARK, nvarchar(500),>
            , GETDATE(), @UserID,  GETDATE(), @UserID, @COMPANY , @ASSET_CLASS)

IF (@StockAssetLocation='I') 
BEGIN

 EXEC @Error= sp_BFD0261A_Insert_Update_IncaseInHouse @UserID ,    @StockYear ,  @StockRound ,  @StockAssetLocation, 
											 @QTyOfHanheld ,    @TargetDateFrom ,@TargetDateTo , @BreakTime,	@ERROR_MESSAGE OUT 

END
IF (@StockAssetLocation='O') 
BEGIN
 EXEC @Error= sp_BFD0261A_Insert_Update_IncaseOutSource @UserID ,    @StockYear ,  @StockRound ,  @StockAssetLocation, 
											 @QTyOfHanheld ,    @TargetDateFrom ,@TargetDateTo , @BreakTime,	@ERROR_MESSAGE OUT 

END

	RETURN @Error ;
END TRY
BEGIN CATCH
			       PRINT concat('ERROR Message:',ERROR_MESSAGE())
				   SET @ERROR_MESSAGE=ERROR_MESSAGE();
				RETURN 1;
END CATCH







GO
