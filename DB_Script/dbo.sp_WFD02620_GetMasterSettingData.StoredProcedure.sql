DROP PROCEDURE [dbo].[sp_WFD02620_GetMasterSettingData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 16-02-2017
-- Description:	Get system master setting for WFD02620 Stock take requrest screen
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetMasterSettingData]
	-- Add the parameters for the stored procedure here
AS
	DECLARE @CATEGORY nvarchar(10) = 'STOCK_TAKE'
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		(SELECT top 1 VALUE FROM TB_M_SYSTEM WHERE CATEGORY=@CATEGORY and SUB_CATEGORY='BREAK_TIME') as BREAK_TIME
		, (SELECT top 1 VALUE FROM TB_M_SYSTEM WHERE CATEGORY=@CATEGORY and SUB_CATEGORY='DISP_PERIOD') as DISP_PERIOD
		, (SELECT top 1 VALUE FROM TB_M_SYSTEM WHERE CATEGORY=@CATEGORY and SUB_CATEGORY='PLAN' and CODE='END_HRS') as END_HRS
		, (SELECT top 1 VALUE FROM TB_M_SYSTEM WHERE CATEGORY=@CATEGORY and SUB_CATEGORY='PLAN' and CODE='START_HRS') as START_HRS

END




GO
