DROP PROCEDURE [dbo].[sp_WFD02320_GetPrintTagQue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 06/02/2017
-- Description:	WFD02320
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02320_GetPrintTagQue]
		@COMPANY	VARCHAR(255) = NULL, -- -- Concat with #
		@FIND VARCHAR(60), 
		@pageNum INT,
		@pageSize INT,
		@sortColumnName VARCHAR(50),
		@orderType VARCHAR(5),
		@TOTAL_ITEM		int output
AS
BEGIN

	SET @FIND = dbo.fn_GetSearchCriteria(@FIND)

    --begin modify by thanapon: fixed paging and sorting
	IF(@sortColumnName IS NULL )
		SET @sortColumnName ='1';
	IF(@orderType IS NULL )
		SET @orderType ='asc';
 
	DECLARE @FROM	INT, @TO	INT

	SET @FROM	= (@pageSize * (@pageNum-1)) + 1;
	SET @TO		= @pageSize * (@pageNum);

	SET NOCOUNT ON;
 	
	SELECT		rp.COMPANY,
				rp.CREATE_BY as REQUEST_BY_CODE,
    			CONCAT(em.EMP_TITLE , ' ', em.EMP_NAME , ' ' ,em.EMP_LASTNAME) as REQUEST_BY_NAME,
    			rp.COST_CODE as COST_CENTER,
    			cc.COST_NAME as COST_CENTER_NAME,
				rp.RESP_COST_CODE as RESP_COST_CODE,
				rc.COST_NAME as RESP_COST_NAME,
    			COUNT(ASSET_NO) as TOTAL_PRINT_ITEM
	INTO		#TB_T_RESULT
    FROM		[dbo].[TB_R_PRINT_Q] rp WITH(NOLOCK)
    LEFT JOIN	TB_M_COST_CENTER cc 
	ON			rp.COMPANY = cc.COMPANY AND rp.COST_CODE = cc.COST_CODE
	 LEFT JOIN	TB_M_COST_CENTER rc 
	ON			rp.COMPANY = rc.COMPANY AND rp.RESP_COST_CODE = rc.COST_CODE
    LEFT JOIN	TB_M_EMPLOYEE em 
	ON			rp.CREATE_BY = em.SYS_EMP_CODE
    WHERE	EXISTS(SELECT 1 FROM dbo.fn_GetMultipleCompanyList(@COMPANY) COM WHERE COM.COMPANY_CODE = rp.COMPANY) AND
	(		rp.COST_CODE		like ISNULL(@FIND,rp.COST_CODE ) or
			rp.RESP_COST_CODE	like ISNULL(@FIND,rp.RESP_COST_CODE ) or
			cc.COST_NAME		like ISNULL(@FIND,cc.COST_NAME ) or
			rp.CREATE_BY		like ISNULL(@FIND,rp.CREATE_BY ) or
			em.EMP_NAME			like ISNULL(@FIND,em.EMP_NAME )
	)
	GROUP	BY 
			rp.COMPANY,
			rp.CREATE_BY, 
			em.EMP_TITLE ,
			em.EMP_NAME ,
			em.EMP_LASTNAME , 
			rp.COMPANY,
			rp.COST_CODE , 
			cc.COST_NAME,
			rp.RESP_COST_CODE,
			rc.COST_NAME ;
	SET @TOTAL_ITEM = @@ROWCOUNT

	PRINT @FROM
	PRINT @TO
    --begin modify by thanapon: fixed paging and sorting
  
   --begin modify by thanapon: fixed paging and sorting
	SELECT	ROWNUMBER,
			COMPANY,
			REQUEST_BY_CODE,
    		REQUEST_BY_NAME,
    		COST_CENTER,
    		COST_CENTER_NAME,
			RESP_COST_CODE,
			RESP_COST_NAME,
    		TOTAL_PRINT_ITEM
	FROM	(
			SELECT	CASE WHEN @sortColumnName = '1' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
						 WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)
					 
						 WHEN @sortColumnName = '2' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_BY_CODE asc)  
						 WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_BY_CODE desc)   
					 
						 WHEN @sortColumnName = '3' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_BY_NAME asc)  
						 WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_BY_NAME desc)

						 WHEN @sortColumnName = '4' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER asc)
						 WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER desc)
						 WHEN @sortColumnName = '5' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER_NAME asc)
						 WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER_NAME desc)

						 WHEN @sortColumnName = '6' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY RESP_COST_CODE asc)  
						 WHEN @sortColumnName = '6' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY RESP_COST_CODE desc)  
					 
					
						 WHEN @sortColumnName = '7' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY TOTAL_PRINT_ITEM asc) 
						 WHEN @sortColumnName = '7' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TOTAL_PRINT_ITEM desc)  
						 ELSE ROW_NUMBER() OVER(ORDER BY REQUEST_BY_CODE ASC)
					END AS ROWNUMBER,
					COMPANY,
					REQUEST_BY_CODE,
    				REQUEST_BY_NAME,
    				COST_CENTER,
    				COST_CENTER_NAME,
					RESP_COST_CODE,
					RESP_COST_NAME,
    				TOTAL_PRINT_ITEM
			FROM	#TB_T_RESULT
	) d
	WHERE	d.ROWNUMBER >= @FROM and 
			d.ROWNUMBER <= @TO
	ORDER		BY	
			d.ROWNUMBER

	DROP TABLE 	#TB_T_RESULT
  --end modify by thanapon: fixed paging and sorting

END
GO
