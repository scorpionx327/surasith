DROP PROCEDURE [dbo].[sp_WFD01210_GetRespCostCenter_In_Charge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 10/03/2017
-- Description:	Get Cost center In-charge
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetRespCostCenter_In_Charge]
@FIND			VARCHAR(60),
@NO_ASSET_FLAG	VARCHAR(1) = 'N', -- 2017-06-23 Surasith T. Display Cost Center that no assets include when Y
@EMP_CODE		T_SYS_USER
AS
BEGIN
	-- Display Left Side of screen

	SET @NO_ASSET_FLAG = ISNULL(@NO_ASSET_FLAG, 'N')

	DECLARE	@COMPANY	T_COMPANY
	SELECT	@COMPANY	= COMPANY
	FROM	TB_M_EMPLOYEE E
	WHERE	E.SYS_EMP_CODE	= @EMP_CODE

	SET		@FIND = dbo.fn_GetSearchCriteria(@FIND)
	SELECT	C.COST_CODE,
			C.COST_NAME,
			CASE WHEN H.RESP_COST_CODE IS NULL THEN 'N' ELSE 'Y' END AS STYLE -- Set Inactive
	FROM	TB_M_SYSTEM	 SS
	INNER	JOIN
			TB_M_COST_CENTER C
	ON		SS.CATEGORY		= 'RESPONSE_COST_CENTER' AND 
			SS.SUB_CATEGORY	= C.COMPANY AND
			SS.[VALUE]		= C.COST_CODE AND
			SS.SUB_CATEGORY	= @COMPANY AND
			SS.ACTIVE_FLAG	= 'Y'
	LEFT	JOIN
	(
			SELECT	H.COMPANY,
					H.RESP_COST_CODE
			FROM	TB_M_ASSETS_H H
			WHERE	H.COMPANY		= @COMPANY AND
					H.[STATUS]		= 'Y' AND
					H.INHOUSE_FLAG	= 'Y'
			GROUP	BY
					H.COMPANY,
					H.RESP_COST_CODE
	) H
	ON		C.COMPANY	= H.COMPANY AND
			C.COST_CODE	= H.RESP_COST_CODE	
	LEFT	JOIN
			TB_M_FASUP_COST_CENTER FS
	ON		C.COMPANY	= FS.COMPANY AND 
			C.COST_CODE = FS.COST_CODE AND
			FS.EMP_CODE	= @EMP_CODE
	
	WHERE	C.COMPANY	= @COMPANY AND
			FS.COST_CODE IS NULL AND
			(
				( @NO_ASSET_FLAG = 'Y' AND H.RESP_COST_CODE IS NOT NULL) OR
				( @NO_ASSET_FLAG = 'N' AND H.RESP_COST_CODE IS NULL )
			
			)
			AND	C.[STATUS] = 'Y'
			AND(
					(C.COST_CODE LIKE @FIND or @FIND IS NULL )
					OR (C.COST_NAME LIKE @FIND or @FIND IS NULL )
				)
			
	ORDER	BY	
			C.COST_CODE
END



GO
