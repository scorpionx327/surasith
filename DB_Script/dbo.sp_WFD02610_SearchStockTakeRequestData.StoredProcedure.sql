DROP PROCEDURE [dbo].[sp_WFD02610_SearchStockTakeRequestData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-01-31
-- Description:	Sear stock take request data 
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02610_SearchStockTakeRequestData]
	-- Add the parameters for the stored procedure here
	 @USER_LOGIN		T_USER
	, @ISFAADMIN			varchar(1) --'Y' is FA Addmin , N is normal User 
	, @YEAR				varchar(4)
	, @ROUND			varchar(2)
	, @ASSET_LOCATION	varchar(1)
	, @CURRENCT_PAGE	int
	, @PAGE_SIZE		int
	, @ORDER_BY			NVARCHAR(50)
	, @ORDER_TYPE		NVARCHAR(4)
	, @COMPANY		T_COMPANY
	, @TOTAL_ITEM		int output

AS
	DECLARE @FROM	INT
			, @TO	INT
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @FROM = (@PAGE_SIZE * (@CURRENCT_PAGE-1)) + 1;
	SET @TO = @PAGE_SIZE * (@CURRENCT_PAGE);

	IF @ORDER_TYPE IS NULL
	BEGIN
		SET @ORDER_TYPE='asc'
	END

	SELECT
		convert(bit, 0) as SELECTED
		, h.COMPANY
		, h.STOCK_TAKE_KEY
		, h.YEAR	
		, h.ROUND
		, h.ASSET_LOCATION	
		, (	SELECT COUNT(DISTINCT COST_CODE) 
			FROM TB_R_STOCK_TAKE_D d 
			WHERE d.STOCK_TAKE_KEY=h.STOCK_TAKE_KEY
		)  as TOTAL_COST_CENTER
		, h.ALL_ASSET_TOTAL as TOTAL_ASSETS
		, (	SELECT COUNT(1) 
			FROM TB_R_STOCK_TAKE_D d 
				WHERE d.YEAR=h.YEAR and d.ROUND=h.ROUND and d.ASSET_LOCATION=h.ASSET_LOCATION and d.CHECK_STATUS='Y'
		) as SCAN_RESULT	
		, DATEDIFF(d, h.TARGET_DATE_FROM, h.TARGET_DATE_TO) + 1 as PLAN_DAY	
		, ISNULL(DATEDIFF(d
			, (SELECT MIN(SCAN_DATE) FROM TB_R_STOCK_TAKE_D d WHERE d.YEAR=h.YEAR and d.ROUND=h.ROUND and d.ASSET_LOCATION=h.ASSET_LOCATION)
			, (SELECT MAX(SCAN_DATE) FROM TB_R_STOCK_TAKE_D d WHERE d.YEAR=h.YEAR and d.ROUND=h.ROUND and d.ASSET_LOCATION=h.ASSET_LOCATION)
		)	+ 1, 0) as ACTUAL_DAY	
		, h.REMARK
		, h.PLAN_STATUS
	INTO #WFD02610_SEARCH_TABLE
	FROM TB_R_STOCK_TAKE_H h
	WHERE (h.YEAR like '%' + @YEAR + '%' or @YEAR IS NULL) and (h.ROUND like '%' + @ROUND + '%' or @ROUND IS NULL) 
		and (h.ASSET_LOCATION=@ASSET_LOCATION or @ASSET_LOCATION IS NULL)

	IF @YEAR IS NULL AND @ROUND IS NULL
	BEGIN
		SET @ORDER_BY = '0'
	END

	SELECT
		@TOTAL_ITEM = count(1)
	FROM #WFD02610_SEARCH_TABLE

	SELECT	
		d.COMPANY
		,d.SELECTED
		,d.STOCK_TAKE_KEY
		,d.YEAR	
		,d.ROUND	
		,d.ASSET_LOCATION	
		,( select top 1 VALUE from TB_M_SYSTEM WHERE CATEGORY='COMMON' and SUB_CATEGORY = 'ASSET_LOCATION' and CODE=d.ASSET_LOCATION ) as ASSET_LOCATION_NAME
		,d.TOTAL_COST_CENTER	
		,d.TOTAL_ASSETS	
		,d.SCAN_RESULT	
		,d.PLAN_DAY	
		,d.ACTUAL_DAY	
		,d.REMARK
		,d.PLAN_STATUS
		,d.PLAN_STATUS_TEXT
	FROM (
		SELECT
			ROW_NUMBER() OVER (ORDER BY 
									CASE WHEN @ORDER_BY = '1' and @ORDER_TYPE='asc' THEN data.YEAR
										END ASC,											 
									CASE WHEN @ORDER_BY = '1' and @ORDER_TYPE='desc' THEN data.YEAR
										END DESC,
									CASE WHEN @ORDER_BY = '2' and @ORDER_TYPE='asc' THEN data.ROUND
										END ASC,											 
									CASE WHEN @ORDER_BY = '2' and @ORDER_TYPE='desc' THEN data.ROUND
										END DESC,
									CASE WHEN @ORDER_BY = '3' and @ORDER_TYPE='asc' THEN data.ASSET_LOCATION
										END ASC,											 
									CASE WHEN @ORDER_BY = '3' and @ORDER_TYPE='desc' THEN data.ASSET_LOCATION
										END DESC,
									CASE WHEN @ORDER_BY = '4' and @ORDER_TYPE='asc' THEN data.PLAN_STATUS
										END ASC,											 
									CASE WHEN @ORDER_BY = '4' and @ORDER_TYPE='desc' THEN data.PLAN_STATUS
										END DESC,
									CASE WHEN @ORDER_BY = '5' and @ORDER_TYPE='asc' THEN data.TOTAL_ASSETS
										END ASC,											 
									CASE WHEN @ORDER_BY = '5' and @ORDER_TYPE='desc' THEN data.TOTAL_ASSETS
										END DESC,
									CASE WHEN @ORDER_BY = '6' and @ORDER_TYPE='asc' THEN data.TOTAL_COST_CENTER
										END ASC,											 
									CASE WHEN @ORDER_BY = '6' and @ORDER_TYPE='desc' THEN data.TOTAL_COST_CENTER
										END DESC,
									CASE WHEN @ORDER_BY = '7' and @ORDER_TYPE='asc' THEN data.TOTAL_ASSETS
										END ASC,											 
									CASE WHEN @ORDER_BY = '7' and @ORDER_TYPE='desc' THEN data.TOTAL_ASSETS
										END DESC,
									CASE WHEN @ORDER_BY = '8' and @ORDER_TYPE='asc' THEN data.SCAN_RESULT
										END ASC,											 
									CASE WHEN @ORDER_BY = '8' and @ORDER_TYPE='desc' THEN data.SCAN_RESULT
										END DESC,
									CASE WHEN @ORDER_BY = '9' and @ORDER_TYPE='asc' THEN data.PLAN_DAY
										END ASC,											 
									CASE WHEN @ORDER_BY = '9' and @ORDER_TYPE='desc' THEN data.PLAN_DAY
										END DESC,
									CASE WHEN @ORDER_BY = '10' and @ORDER_TYPE='asc' THEN data.ACTUAL_DAY
										END ASC,											 
									CASE WHEN @ORDER_BY = '10' and @ORDER_TYPE='desc' THEN data.ACTUAL_DAY
										END DESC,
									CASE WHEN @ORDER_BY = '11' and @ORDER_TYPE='asc' THEN data.REMARK
										END ASC,											 
									CASE WHEN @ORDER_BY = '11' and @ORDER_TYPE='desc' THEN data.REMARK
										END DESC,
									CASE WHEN @ORDER_BY = '0' and @ORDER_TYPE='asc' THEN S.REMARKS
										END ASC,											 
									CASE WHEN @ORDER_BY = '0' and @ORDER_TYPE='desc' THEN S.REMARKS
										END DESC,

									data.YEAR, data.ASSET_LOCATION, data.ROUND)as NO
			,data.COMPANY
			,data.SELECTED
			,data.STOCK_TAKE_KEY
			,data.YEAR	
			,data.ROUND	
			,data.ASSET_LOCATION
			,dbo.fn_GetSystemMaster('SYSTEM_CONFIG','STOCK_TAKING_STATUS', data.PLAN_STATUS) PLAN_STATUS_TEXT
			,data.TOTAL_COST_CENTER	
			,isnull(data.TOTAL_ASSETS,0) TOTAL_ASSETS
			,data.SCAN_RESULT	
			,data.PLAN_DAY	
			,data.ACTUAL_DAY	
			,data.REMARK
			,data.PLAN_STATUS
		FROM #WFD02610_SEARCH_TABLE data
		left join TB_M_SYSTEM S on S.CATEGORY = 'SYSTEM_CONFIG'
			      and s.SUB_CATEGORY = 'STOCK_TAKING_STATUS' and s.CODE = data.PLAN_STATUS
	) d
	WHERE d.NO >= @FROM and d.NO <= @TO

	DROP TABLE #WFD02610_SEARCH_TABLE
	
END



GO
