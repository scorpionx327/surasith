DROP PROCEDURE [dbo].[sp_WFD02620_GetDefaultScreenData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-10
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetDefaultScreenData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	select
	(select top 1 VALUE FROM TB_M_SYSTEM where CATEGORY='STOCK_TAKE' and SUB_CATEGORY='BREAK_TIME'  and CODE='1') as BREAK_TIME_STR
	, (select top 1 VALUE FROM TB_M_SYSTEM where CATEGORY='STOCK_TAKE' and SUB_CATEGORY='PLAN'  and CODE='START_HRS') as START_HRS_STR
	, (select top 1 VALUE FROM TB_M_SYSTEM where CATEGORY='STOCK_TAKE' and SUB_CATEGORY='PLAN'  and CODE='END_HRS') as END_HRS_STR

END




GO
