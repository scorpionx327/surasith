DROP PROCEDURE [dbo].[sp_WFD02630_GetDefaultDropDown]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 13/03/2017
-- Description:	get default ddl
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02630_GetDefaultDropDown]
			@IS_FaAdmin varchar(1),
			@EMP_CODE T_USER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
			SELECT TOP 1 H.STOCK_TAKE_KEY,H.YEAR,H.ROUND,H.ASSET_LOCATION 
			 FROM TB_R_STOCK_TAKE_H H left join TB_M_SYSTEM S on S.CATEGORY = 'SYSTEM_CONFIG'
			      and s.SUB_CATEGORY = 'STOCK_TAKING_STATUS' and s.CODE = H.PLAN_STATUS
			WHERE H.PLAN_STATUS <>'D' AND
			      ((@IS_FAADMIN = 'N' AND H.ASSET_LOCATION = 'I') 
				   OR @IS_FAADMIN = 'Y')
										 --EXISTS ( SELECT 1 
											--		FROM TB_R_STOCK_TAKE_D_PER_SV D 
											--		 WHERE D.STOCK_TAKE_KEY = H.STOCK_TAKE_KEY
														-- AND ((@IS_FAADMIN = 'Y' ) OR (D.EMP_CODE = @EMP_CODE ) )

													-- )
			ORDER BY S.REMARKS ASC, H.CREATE_DATE DESC
END




GO
