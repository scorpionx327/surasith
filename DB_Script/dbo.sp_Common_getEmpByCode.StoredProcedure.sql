DROP PROCEDURE [dbo].[sp_Common_getEmpByCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 02/03/2017
-- Description:	Get emp * by emp_code
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_getEmpByCode] 
	@EMP_CODE varchar(8)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select * from TB_M_EMPLOYEE where EMP_CODE = @EMP_CODE
END



GO
