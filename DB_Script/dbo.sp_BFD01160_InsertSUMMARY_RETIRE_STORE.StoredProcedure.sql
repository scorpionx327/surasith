DROP PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_RETIRE_STORE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_RETIRE_STORE]
@User	VARCHAR(8)
AS
BEGIN
	DECLARE @ID int,	
		@TO varchar(1000),
		@CC varchar(1000),
		@BCC varchar(1000),
		@TITLE varchar(255),
		@MESSAGE nvarchar(max),
		@Dear nvarchar(max),
		@FOOTER varchar(250),
		@TYPE varchar(1),
		@START_PERIOD datetime,
		@END_PERIOD datetime,
		@SEND_FLAG varchar(1),
		@RESULT_FLAG varchar(1),
		@REF_FUNC varchar(8),
		@REF_DOC_NO T_DOC_NO

		--SET @MESSAGE = 'Dear Name of store <br>';

		SET @MESSAGE = CONCAT(@MESSAGE,('<table style="border:1px solid black;">
											<thead style="height:25px; background-color:rgba(236, 240, 245, 1);"> 
											  <tr>
												<th style="min-width: 120px;">Asset No</th>									
												<th style="min-width: 50px;">Sub</th>			
												<th style="min-width: 200px;">Description</th>								
												<th style="min-width: 200px;">Additional Description</th>					
												<th style="min-width: 150px;">Asset Plate No</th>							
												<th style="min-width: 100px;">Cost Center</th>								
												<th style="min-width: 200px;">Cost Center Name</th>							
												<th style="min-width: 100px;">Requestor</th>								
												<th style="min-width: 150px;">FA-Supervisor</th>							
											  </tr>
											</thead>'));

		DECLARE @StartDate datetime;
		DECLARE @EndDate datetime;
		select @StartDate = DATEADD(day, -1, [value]),@EndDate = [value]
		From TB_M_SYSTEM																
		Where CATEGORY = 'NOTIFICATION'																
			 AND SUB_CATEGORY = 'SUMMARY_RETIRE_STORE'															
			 AND CODE = 'NEXTTIME'		
-----------------------------------------------------------------------------------------------------------
		DECLARE @ASSET_NO varchar(100),@ASSET_SUB varchar(10),@ASSET_NAME varchar(500),@ADTL_ASSET_NAME varchar(500),@INVOICE_NO varchar(100),@ASSET_COST_CODE varchar(100)
				,@COST_NAME varchar(100),@Requestor varchar(100),@FASupervisor varchar(100)

		DECLARE cursor_results CURSOR FOR 
		select --rh.DOC_NO,rh.REQUEST_DATE,
			dd.ASSET_NO,dd.ASSET_SUB,h.ASSET_NAME,h.ADTL_ASSET_NAME,dd.INVOICE_NO,rh.ASSET_COST_CODE--,dd.COST_CODE
			,cc.COST_NAME,concat(rh.EMP_NAME,' ',rh.EMP_LASTNAME) Requestor
			--,h.MINOR_CATEGORY
			,concat(ra.EMP_NAME,' ',ra.EMP_LASTNAME) FASupervisor
			--,rh.EMP_CODE,ra.APPR_STATUS
		from TB_R_REQUEST_H rh
			inner join TB_R_REQUEST_DISP_D dd on rh.DOC_NO = dd.DOC_NO and rh.COMPANY = dd.COMPANY
			inner join TB_R_REQUEST_APPR ra on rh.DOC_NO = ra.DOC_NO
			inner join TB_M_ASSETS_H h on dd.ASSET_NO = h.ASSET_NO and dd.ASSET_SUB = h.ASSET_SUB
			inner join TB_M_COST_CENTER cc on rh.ASSET_COST_CODE = cc.COST_CODE
		where rh.EMP_CODE in (
					select e.EMP_CODE 
					from TB_M_EMPLOYEE e
						left join TB_M_SP_ROLE r on e.EMP_CODE = r.EMP_CODE
					where r.SP_ROLE= 'STR' 
				)
				and ra.APPR_DATE between @StartDate and @EndDate
				and ra.APPR_STATUS = 'A'
				and ra.APPR_ROLE = 'FA'

		OPEN cursor_results
		FETCH NEXT FROM cursor_results into @ASSET_NO,@ASSET_SUB,@ASSET_NAME,@ADTL_ASSET_NAME,@INVOICE_NO,@ASSET_COST_CODE,@COST_NAME,@Requestor,@FASupervisor
		WHILE @@FETCH_STATUS = 0
		BEGIN 
			SET @MESSAGE = CONCAT(@MESSAGE,'<tr>');							
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 120px;">',@ASSET_NO,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:center;">',@ASSET_SUB,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 200px;">',@ASSET_NAME,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 200px;">',@ADTL_ASSET_NAME,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 150px;">',@INVOICE_NO,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;">',@ASSET_COST_CODE,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 200px;">',@COST_NAME,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;">',@Requestor,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 150px;">',@FASupervisor,'</td>');		
			SET @MESSAGE = CONCAT(@MESSAGE,'</tr>');	

			FETCH NEXT FROM cursor_results into @ASSET_NO,@ASSET_SUB,@ASSET_NAME,@ADTL_ASSET_NAME,@INVOICE_NO,@ASSET_COST_CODE,@COST_NAME,@Requestor,@FASupervisor
		END

		CLOSE cursor_results;
		DEALLOCATE cursor_results;
------------------------------------------------------------------------------------------------------------
		SET @ID = (select isnull(max(ID),0) + 1 from TB_R_NOTIFICATION);
		SET @MESSAGE = CONCAT(@MESSAGE,'</table>');

		--select * from @MyTable
		SET @FOOTER = 'Best Regards,<br>System';

		SET @START_PERIOD = @StartDate;
		SET @END_PERIOD = @EndDate;

		SET @SEND_FLAG = 'N';
		SET @RESULT_FLAG = 'N';
		SET @TYPE = 'I';
		SET @REF_FUNC = '';

	DECLARE @NAME varchar(max),@AEC_MGR_EMP_ID varchar(50),@EMAIL varchar(50),@COMPANY T_COMPANY;
	DECLARE cursor_results CURSOR FOR 
	select concat('คุณ ',e.EMP_NAME_T,' ',e.EMP_LASTNAME_T),e.EMP_CODE,e.EMAIL,e.COMPANY 
	from TB_M_EMPLOYEE e
	left join TB_M_SP_ROLE r on e.EMP_CODE = r.EMP_CODE
	where r.SP_ROLE= 'STR' 


	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @NAME,@AEC_MGR_EMP_ID,@EMAIL,@COMPANY
	WHILE @@FETCH_STATUS = 0
	BEGIN 

	SET @Dear = concat('Dear ',@NAME,' <br>');

	SET @TO = @EMAIL;
	SET @TITLE = CONCAT('TFAST : ',@COMPANY,' Summary Daily of asset retirement');
	INSERT INTO TB_R_NOTIFICATION
           (ID,[TO],CC,BCC,TITLE,[MESSAGE],FOOTER,[TYPE]
           ,START_PERIOD,END_PERIOD,SEND_FLAG,RESULT_FLAG,REF_FUNC,REF_DOC_NO
			,CREATE_DATE,CREATE_BY,UPDATE_DATE,UPDATE_BY)
     VALUES
           (@ID
		   ,@TO
           ,@CC
           ,@BCC
           ,@TITLE
           ,concat(@Dear,@MESSAGE)
           ,@FOOTER
           ,@TYPE
           ,@START_PERIOD
           ,@END_PERIOD
           ,@SEND_FLAG
           ,@RESULT_FLAG
           ,@REF_FUNC
           ,@REF_DOC_NO
           ,getdate()
           ,@User
           ,getdate()
           ,@User)

	--select @TO,@CC,@BCC,@TITLE,@MESSAGE,@FOOTER,@TYPE,@START_PERIOD,@END_PERIOD,@SEND_FLAG,@RESULT_FLAG,@REF_FUNC,@REF_DOC_NO,getdate(),@User,getdate(),@User

	FETCH NEXT FROM cursor_results into @NAME,@AEC_MGR_EMP_ID,@EMAIL,@COMPANY
	END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'SUMMARY_RETIRE_STORE'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	SET @dNextTime = DATEADD(DAY, 1, @dNextTime)

	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= 'System'
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
			--select @MESSAGE
END
GO
