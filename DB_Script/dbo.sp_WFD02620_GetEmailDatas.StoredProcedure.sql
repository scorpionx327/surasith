DROP PROCEDURE [dbo].[sp_WFD02620_GetEmailDatas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-23
-- Description:	Get data for send email
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetEmailDatas]
	-- Add the parameters for the stored procedure here
	@STOCK_TAKE_KEY	nvarchar(7)
AS
	DECLARE @ASSET_LOCATION nvarchar(1);
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP 1 @ASSET_LOCATION=ASSET_LOCATION
	FROM TB_R_STOCK_TAKE_H 
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY;

	if @ASSET_LOCATION = 'I'
	BEGIN

		select 
			h.YEAR
			, h.ROUND
			, h.ASSET_LOCATION
			, ISNULL(s.VALUE, h.ASSET_LOCATION) as ASSET_LOCATION_NAME
			, sv.EMP_CODE
			, sv.EMP_TITLE
			, sv.EMP_NAME
			, sv.EMP_LASTNAME
			, e.EMAIL
		from TB_R_STOCK_TAKE_H h
		inner join TB_R_STOCK_TAKE_D_PER_SV sv on h.STOCK_TAKE_KEY=sv.STOCK_TAKE_KEY and sv.COMPANY=h.COMPANY
		left join TB_M_SYSTEM s on s.CATEGORY='COMMON' and s.SUB_CATEGORY='ASSET_LOCATION' and s.CODE=h.ASSET_LOCATION
		left join TB_M_EMPLOYEE e on sv.EMP_CODE=e.EMP_CODE
		where h.STOCK_TAKE_KEY=@STOCK_TAKE_KEY

	END
	ELSE
	BEGIN
		select 
			SUBSTRING(@STOCK_TAKE_KEY, 0, 5) as YEAR
			, SUBSTRING(@STOCK_TAKE_KEY, 5, 2) as ROUND
			, @ASSET_LOCATION as ASSET_LOCATION
			, ISNULL((	SELECT top 1 s.VALUE FROM TB_M_SYSTEM s 
				WHERE s.CATEGORY='COMMON' and s.SUB_CATEGORY='ASSET_LOCATION' 
					and s.CODE=@ASSET_LOCATION), @ASSET_LOCATION) as ASSET_LOCATION_NAME
			, EMP_CODE
			, EMP_TITLE
			, EMP_NAME
			, EMP_LASTNAME
			, RTRIM(LTRIM(EMAIL)) as EMAIL
		FROM TB_M_EMPLOYEE  e
		WHERE FAADMIN='Y'
		ORDER BY EMP_CODE ASC;

	END

	

END




GO
