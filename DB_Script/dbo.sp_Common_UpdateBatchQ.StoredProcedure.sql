DROP PROCEDURE [dbo].[sp_Common_UpdateBatchQ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Common_UpdateBatchQ]
(
@ReqID			numeric(18, 2),
@Status		 varchar(1),
@PID		  varchar(20)=NULL
)
AS
BEGIN
IF @PID IS NOT NULL
BEGIN
	UPDATE	TB_BATCH_Q
	SET		[STATUS] = @Status,
			UPDATE_DATE =  GETDATE(),
			PID =@PID
	WHERE	APP_ID	 = @ReqID
END
ELSE
BEGIN
	UPDATE	TB_BATCH_Q
	SET		[STATUS] = @Status,
			UPDATE_DATE =  GETDATE()
	WHERE	APP_ID	 = @ReqID
END
	IF(@@ROWCOUNT = 0)
	BEGIN
		DECLARE @msg VARCHAR(100)
		SET @msg = 'Not found Request ID <'+CONVERT(VARCHAR,@ReqID)+'> in TB_BATCH_Q'
		RAISERROR(@msg,16,1)
	END

	IF @Status IN ('S','E')
	BEGIN
		declare @LogID numeric(18,0)
		SELECT @LogID = MAX(LOG_ID) from TB_L_LOGGING where APP_ID = @ReqID
		update TB_L_LOGGING set STATUS = @Status where LOG_ID = @LogID 

	END
END






GO
