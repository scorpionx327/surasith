DROP PROCEDURE [dbo].[sp_Common_GetJob_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetJob_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER
AS
BEGIN
		SELECT JOB_CODE,
				JOB_NAME
		FROM	TB_M_EMPLOYEE E
		WHERE	(	
					JOB_CODE LIKE CONCAT(@keyword,'%') 
					OR
					JOB_CODE LIKE CONCAT(@keyword,'%') 
				) 
END
GO
