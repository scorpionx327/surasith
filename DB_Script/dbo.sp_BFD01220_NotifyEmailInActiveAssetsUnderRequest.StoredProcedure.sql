DROP PROCEDURE [dbo].[sp_BFD01220_NotifyEmailInActiveAssetsUnderRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_BFD01220_NotifyEmailInActiveAssetsUnderRequest]
(
 	@AppID	INT,
	@UserID		VARCHAR(8)
)
AS
BEGIN
	-- Check Asset Master

--Cost Center Expire is Request Document Active and  (SV) 
DECLARE @Subject	 VARCHAR(MAX) 
	DECLARE @Header	  VARCHAR(100)   
	DECLARE @Footer		VARCHAR(100) 

DECLARE @TB_T_EMAIL_SV TABLE
	(
		SYS_EMP_CODE		VARCHAR(18),
		EMP_NAME			VARCHAR(150),
		EMAIL				VARCHAR(50),
		COST_CODE			VARCHAR(10),
		COST_NAME			VARCHAR(20)

	)

		INSERT INTO	@TB_T_EMAIL_SV
		SELECT 	SV.EMP_CODE,EM.EMP_NAME , EM.EMAIL, C.COST_CODE,C.COST_NAME
	FROM	TB_T_COST_CENTER C WITH(NOLOCK)
	INNER	JOIN
	TB_M_ASSETS_H A WITH(NOLOCK)
	ON		A.COMPANY	= C.COMPANY AND
		A.COST_CODE	= C.COST_CODE
	LEFT	JOIN
	TB_M_SV_COST_CENTER SV
	ON		C.COMPANY	= SV.COMPANY AND
		C.COST_CODE	= SV.COST_CODE
		INNER JOIN TB_M_EMPLOYEE EM  WITH(NOLOCK)
		ON SV.EMP_CODE  = EM.SYS_EMP_CODE
		WHERE	C.FLAG	= 'D' and A.[STATUS] = 'Y' and EM.EMAIL IS NOT NULL
		GROUP BY   SV.EMP_CODE,EM.EMP_NAME , EM.EMAIL, C.COST_CODE,C.COST_NAME
		ORder by SV.EMP_CODE

 

	
	set @Subject	  = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD01220_SV')
	set @Header	   = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01220_HEADER_SV')
	set @Footer	  =  dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01220_FOOTER_SV')


	 INSERT INTO TB_R_NOTIFICATION
		SELECT	NEXT VALUE FOR NOTIFICATION_ID OVER(ORDER BY R.EMP_NAME),
				R.EMAIL	AS [TO],
				dbo.fn_GetEmailListofAdministrator()	AS CC,
				NULL		AS BCC,
				@Subject,
				CONCAT(dbo.fn_StringFormat(@Header,R.EMP_NAME) ,CONCAT('<div><table><tbody>', R.COSTCENTER_LIST,'</tbody></table></div>') , @Footer) AS [MESSAGE],
				--dbo.fn_StringFormat(@Body,'Requestor|'+R.URL_LIST)  AS [MESSAGE],
				NULL	AS FOOTER, -- Get From Default
				'I'		AS [TYPE],
				NULL	AS [START_PERIOD],
				NULL	AS [END_PERIOD],
				'N'		AS [SEND_FLAG],
				NULL	AS RESULT_FLAG,
				'BFD01220'	AS REF_FUNC,
				NULL	AS REF_DOC_NO,
				GETDATE(),
				@UserID,
				GETDATE(),
				@UserID
	FROM	
	(	
	
	SELECT DISTINCT	A.SYS_EMP_CODE,	A.EMP_NAME,A.EMAIL,
            STUFF((    SELECT ' <tr>' + CONCAT('<td>',COST_CODE,'</td><td>',COST_NAME,'</td></tr>') AS [text()]                        
                        FROM @TB_T_EMAIL_SV SUB
						WHERE	SUB.SYS_EMP_CODE = A.SYS_EMP_CODE
                        FOR XML PATH('') 
                        ), 1, 1, '' ) AS COSTCENTER_LIST
						FROM @TB_T_EMAIL_SV A
	


	
	) R








	--------------------------------------------------------------------


--Cost Center Expire is Request Document Active and  (FASUP)
DECLARE @TB_T_EMAIL_FASUP TABLE
	(
		SYS_EMP_CODE		VARCHAR(18),
		EMP_NAME			VARCHAR(150),
		EMAIL				VARCHAR(50),
		COST_CODE			VARCHAR(10),
		COST_NAME			VARCHAR(20)

	)

 INSERT INTO	@TB_T_EMAIL_FASUP
SELECT 	 FS.EMP_CODE ,EM.EMP_NAME , EM.EMAIL,C.COST_CODE,C.COST_NAME
FROM	TB_T_COST_CENTER C WITH(NOLOCK)
INNER	JOIN
TB_M_ASSETS_H A WITH(NOLOCK)
ON		A.COMPANY	= C.COMPANY AND
		A.COST_CODE	= C.COST_CODE
	LEFT	JOIN
TB_M_FASUP_COST_CENTER FS
ON		C.COMPANY	= FS.COMPANY AND
		C.COST_CODE	= FS.COST_CODE
INNER JOIN TB_M_EMPLOYEE EM  WITH(NOLOCK)
ON FS.EMP_CODE  = EM.SYS_EMP_CODE
WHERE	C.FLAG	= 'D' and A.[STATUS] = 'Y' and EM.EMAIL IS NOT NULL
GROUP BY   FS.EMP_CODE ,EM.EMP_NAME , EM.EMAIL,C.COST_CODE,C.COST_NAME

set @Subject	  = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD01220_FASUP')
	set @Header	   = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01220_HEADER_FASUP')
	set @Footer	  =  dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01220_FOOTER_FASUP')

	INSERT INTO TB_R_NOTIFICATION
		SELECT	NEXT VALUE FOR NOTIFICATION_ID OVER(ORDER BY R.EMP_NAME),
				R.EMAIL	AS [TO],
				dbo.fn_GetEmailListofAdministrator()	AS CC,
				NULL		AS BCC,
				@Subject,
				CONCAT(dbo.fn_StringFormat(@Header,R.EMP_NAME) ,CONCAT('<div><table><tbody>', R.COSTCENTER_LIST,'</tbody></table></div>') , @Footer) AS [MESSAGE],
				--dbo.fn_StringFormat(@Body,'Requestor|'+R.URL_LIST)  AS [MESSAGE],
				NULL	AS FOOTER, -- Get From Default
				'I'		AS [TYPE],
				NULL	AS [START_PERIOD],
				NULL	AS [END_PERIOD],
				'N'		AS [SEND_FLAG],
				NULL	AS RESULT_FLAG,
				'BFD01220'	AS REF_FUNC,
				NULL	AS REF_DOC_NO,
				GETDATE(),
				@UserID,
				GETDATE(),
				@UserID
	FROM	
	(	
	
	SELECT DISTINCT	A.SYS_EMP_CODE,	A.EMP_NAME,A.EMAIL,
            STUFF((    SELECT ' <tr>' + CONCAT('<td>',COST_CODE,'</td><td>',COST_NAME,'</td></tr>') AS [text()]                        
                        FROM @TB_T_EMAIL_FASUP SUB
						WHERE	SUB.SYS_EMP_CODE = A.SYS_EMP_CODE
                        FOR XML PATH('') 
                        ), 1, 1, '' ) AS COSTCENTER_LIST
						FROM @TB_T_EMAIL_FASUP A
	


	
	) R


	--------------------------------------------------------------------------------------------
--Cost Center Expire is Request Document Active
DECLARE @TB_T_EMAIL_CC_EXPIRE TABLE
	(
		SYS_EMP_CODE		VARCHAR(18),
		EMP_NAME			VARCHAR(150),
		EMAIL				VARCHAR(50),
		COST_CODE			VARCHAR(10),
		COST_NAME			VARCHAR(20)

	)

SELECT  RQ.EMP_CODE,EM.EMP_NAME,EM.EMAIL , T.COST_CODE , T.COST_NAME
FROM	TB_T_COST_CENTER T
INNER	JOIN 
TB_R_REQUEST_H RQ  WITH(NOLOCK)
ON	T.COMPANY = RQ.COMPANY AND
T.COST_CODE	= RQ.COST_CODE
INNER JOIN TB_M_EMPLOYEE EM  WITH(NOLOCK)
ON RQ.EMP_CODE  = EM.SYS_EMP_CODE
WHERE T.FLAG = 'D'  AND  
DBO.FN_ISENDREQUEST(RQ.[STATUS]) = 'N' and EM.EMAIL IS NOT NULL
group by  RQ.EMP_CODE,EM.EMP_NAME,EM.EMAIL , T.COST_CODE , T.COST_NAME

set @Subject	  = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD01220_CCEXP')
	set @Header	   = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01220_HEADER_CCEXP')
	set @Footer	  =  dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01220_FOOTER_CCEXP')

	INSERT INTO TB_R_NOTIFICATION
		SELECT	NEXT VALUE FOR NOTIFICATION_ID OVER(ORDER BY R.EMP_NAME),
				R.EMAIL	AS [TO],
				dbo.fn_GetEmailListofAdministrator()	AS CC,
				NULL		AS BCC,
				@Subject,
				CONCAT(dbo.fn_StringFormat(@Header,R.EMP_NAME) ,CONCAT('<div><table><tbody>', R.COSTCENTER_LIST,'</tbody></table></div>') , @Footer) AS [MESSAGE],
				--dbo.fn_StringFormat(@Body,'Requestor|'+R.URL_LIST)  AS [MESSAGE],
				NULL	AS FOOTER, -- Get From Default
				'I'		AS [TYPE],
				NULL	AS [START_PERIOD],
				NULL	AS [END_PERIOD],
				'N'		AS [SEND_FLAG],
				NULL	AS RESULT_FLAG,
				'BFD01220'	AS REF_FUNC,
				NULL	AS REF_DOC_NO,
				GETDATE(),
				@UserID,
				GETDATE(),
				@UserID
	FROM	
	(	
	
	SELECT DISTINCT	A.SYS_EMP_CODE,	A.EMP_NAME,A.EMAIL,
            STUFF((    SELECT ' <tr>' + CONCAT('<td>',COST_CODE,'</td><td>',COST_NAME,'</td></tr>') AS [text()]                        
                        FROM @TB_T_EMAIL_CC_EXPIRE SUB
						WHERE	SUB.SYS_EMP_CODE = A.SYS_EMP_CODE
                        FOR XML PATH('') 
                        ), 1, 1, '' ) AS COSTCENTER_LIST
						FROM @TB_T_EMAIL_CC_EXPIRE A
	


	
	) R
 
	RETURN

END



--select CONCAT(COMPANY,'.',EMP_CODE) from TB_M_SV_COST_CENTER
--update TB_M_FASUP_COST_CENTER 
--set EMP_CODE =CONCAT(COMPANY,'.',EMP_CODE)
--WHERE EMP_CODE not like 'STM.%'

--select CONCAT(COMPANY,'.',EMP_CODE) from TB_M_FASUP_COST_CENTER
--WHERE EMP_CODE not like 'STM.%'
GO
