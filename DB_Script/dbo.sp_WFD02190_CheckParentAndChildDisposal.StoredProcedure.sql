DROP PROCEDURE [dbo].[sp_WFD02190_CheckParentAndChildDisposal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 09/02/2017
-- Description:	Insert Fixed assets screen
-- =============================================
--declare @TOTAL_ITEM int exec sp_WFD02190_SearchData null,null,null,null,null,null,null,null,null,1,10,null,@TOTAL_ITEM out  
create PROCEDURE [dbo].[sp_WFD02190_CheckParentAndChildDisposal]
(
	@GUID				    CHAR(50)			= NULL,		--Guid	
	@CREATE_BY				VARCHAR(200)		= NULL		--Cost Code	
)
AS
BEGIN
	BEGIN TRY
		DECLARE @CHECK_ERR INT
		IF OBJECT_ID('tempdb..#TB_ASSET') IS NOT NULL 
		DROP TABLE #TB_ASSET
		CREATE TABLE #TB_ASSET
		(		
			COST_CODE			VARCHAR(50),			
			ASSET_NO			VARCHAR(50)
		)
		IF OBJECT_ID('tempdb..#TB_MESSAGE') IS NOT NULL 
		DROP TABLE #TB_MESSAGE
		CREATE TABLE #TB_MESSAGE
		(
			MESSAGE_CODE				VARCHAR(50),
			MESSAGE_TEXT				VARCHAR(255),	
			MESSAGE_TYPE				VARCHAR(50)
		)

		INSERT INTO #TB_ASSET
		(
			ASSET_NO,		COST_CODE
		)
		SELECT 
			ASSET_NO,		COST_CODE  
		FROM TB_T_SELECTED_ASSETS 
		WHERE GUID = @GUID 


		INSERT INTO #TB_ASSET
		(
			ASSET_NO,		COST_CODE
		)
		SELECT 
			ASSET_NO,		COST_CODE  
		FROM TB_T_SELECTED_ASSETS_DISP 
		WHERE GUID = @GUID 


		INSERT INTO #TB_MESSAGE
		(
			MESSAGE_CODE, MESSAGE_TYPE,	MESSAGE_TEXT 
		)
		SELECT 'MFAS0708AERR','ERR',DBO.fn_ReplaceMessage1('MFAS0708AERR',ASSET_NO13)
		FROM 
		(
		SELECT LEFT(ASSET_NO,13) ASSET_NO13,COST_CODE,SUM(CASE WHEN RIGHT(ASSET_NO,2) = '00' THEN 1 ELSE 0 END) PARENT,
		SUM(CASE WHEN RIGHT(ASSET_NO,2) != '00' THEN 1 ELSE 0 END) CHILD
		FROM #TB_ASSET 		
		GROUP BY LEFT(ASSET_NO,13),COST_CODE
		) T
		WHERE T.PARENT <> 0 AND T.CHILD <> 0

		SELECT @CHECK_ERR = COUNT(1) FROM #TB_MESSAGE
		IF(@CHECK_ERR > 0 )
		BEGIN
			DELETE TB_T_SELECTED_ASSETS_DISP 
			WHERE GUID = @GUID 
		END


		SELECT * FROM #TB_MESSAGE

		

	END TRY
	BEGIN CATCH
		THROW		
	END CATCH

END
GO
