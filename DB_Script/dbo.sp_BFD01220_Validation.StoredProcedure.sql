DROP PROCEDURE [dbo].[sp_BFD01220_Validation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ============================================= SFAS =============================================
-- UISS_1.2.4_BFD01240_Cost Center Master Interface batch
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/01 (yyyy/mm/dd)
-- Description:  UISS_1.2.4_BFD01240_Cost Center Master Interface batch
-- Objective  :This process is validate 
- ============================================= SFAS =============================================*/

CREATE PROCEDURE  [dbo].[sp_BFD01220_Validation]
 @AppID			INT,
 @UserID	   VARCHAR(8)
AS
BEGIN
	DECLARE @Msg VARCHAR(250), @return	INT = 1 ;
	SET @Msg = dbo.fn_GetMessage('MSTD0012AERR')

	-- Check company
	INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	SELECT  DISTINCT @AppID, 'P', 'N', 'E',
			dbo.fn_StringFormat(@Msg, CONCAT('Company : ',S.COMPANY,'|System Master (TB_M_SYSTEM)')),'Y', @UserID, GETDATE()
	FROM
			TB_S_COST_CENTER S
	LEFT	JOIN
			TB_M_SYSTEM MS
	ON		MS.CATEGORY		= 'SYSTEM_CONFIG' AND 
			MS.SUB_CATEGORY = 'COMPANY' AND
			MS.CODE			= S.COMPANY AND
			MS.ACTIVE_FLAG	= 'Y'
	WHERE	MS.CODE IS NULL
	IF @@ROWCOUNT > 0
		SET @return = 2

	return @return
END






GO
