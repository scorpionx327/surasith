DROP PROCEDURE [dbo].[sp_WFD01210_GetSectionName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun
-- Create date: 05/03/2017
-- Description:	Get All orgrnization order by
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetSectionName]
@pDivName	VARCHAR(45) = NULL,
@pDeptName	VARCHAR(45) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	SECTION_NAME SEC_NAME
	FROM	TB_M_ORGANIZATION
	WHERE	SECTION_NAME IS NOT NULL AND
			( DIV_NAME = @pDivName OR @pDivName IS NULL ) AND
			( DEPT_NAME = @pDeptName OR @pDeptName IS NULL )
	GROUP	BY
			SECTION_NAME
	ORDER	BY
			SEC_NAME
END




GO
