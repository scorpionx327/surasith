DROP PROCEDURE [dbo].[sp_WFD02810_DeleteAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_ClearAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD02810_DeleteAsset]
(	
	@GUID		T_GUID,
	@DOC_NO		T_DOC_NO,
	@ASSET_NO	T_ASSET_NO,
	@ASSET_SUB	T_ASSET_SUB
)
AS
BEGIN
	
	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_H WHERE DOC_NO = @DOC_NO)
	BEGIN
		UPDATE	TB_R_REQUEST_ASSET_D
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				ASSET_NO	= @ASSET_NO AND
				ASSET_SUB	= @ASSET_SUB

		UPDATE	TB_R_REQUEST_ASSET_CHG
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				ASSET_NO	= @ASSET_NO AND
				ASSET_SUB	= @ASSET_SUB
		
	END

	-- delete if exists in submit mode	
	DELETE	TB_T_REQUEST_ASSET_D
	WHERE	[GUID]		= @GUID AND 
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB

	DELETE	TB_R_REQUEST_ASSET_CHG
	WHERE	[GUID]		= @GUID AND 
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB AND
			DOC_NO		IS NULL
END
GO
