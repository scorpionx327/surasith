DROP PROCEDURE [dbo].[sp_WFD01010_GetFunction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 15/03/2017
-- Description:	get function
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01010_GetFunction]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT FUNCTION_ID,FUNCTION_NAME FROM TB_M_FUNCTION WITH(NOLOCK)
	ORDER BY FUNCTION_NAME
END



GO
