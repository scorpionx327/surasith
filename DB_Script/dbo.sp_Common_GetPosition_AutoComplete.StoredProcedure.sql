DROP PROCEDURE [dbo].[sp_Common_GetPosition_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetPosition_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER
AS
BEGIN
		SELECT POST_CODE,
				POST_NAME
		FROM	TB_M_EMPLOYEE 
		WHERE	(	
					POST_CODE LIKE CONCAT(@keyword,'%') 
					OR
					POST_NAME LIKE CONCAT(@keyword,'%') 
				) 
END
GO
