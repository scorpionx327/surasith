DROP PROCEDURE [dbo].[sp_BFD02680_GetPlanStatus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_BFD02680_GetPlanStatus]
@YEAR				as  VARCHAR(4)	,					
@ROUND				as 	VARCHAR(2)	,					
@ASSET_LOCATION		as	VARCHAR(1)					
 
AS

BEGIN

SELECT 	STH.PLAN_STATUS								
FROM 
	TB_R_STOCK_TAKE_H STH	
WHERE 
	STH.[YEAR] = @YEAR
	AND STH.[ROUND] = @ROUND 
	AND STH.[ASSET_LOCATION] = @ASSET_LOCATION
END



GO
