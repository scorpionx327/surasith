DROP PROCEDURE [dbo].[sp_WFD02620_GetCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================WFD01120===================
-- Author:		<Uten S.>
-- Create date: <2017/03/11> 
-- Description:	for WFD01120 get cost center for general report 
-- =============================================
CREATE PROCEDURE  [dbo].[sp_WFD02620_GetCostCenter]

AS
BEGIN
SET NOCOUNT ON;
   SELECT SV.COST_CODE,CONCAT( SV.COST_CODE,':', M.COST_NAME) AS COST_NAME 
   FROM TB_M_SV_COST_CENTER SV
   INNER JOIN TB_M_COST_CENTER M
   ON SV.COST_CODE = M.COST_CODE
   ORDER BY M.COST_NAME
END



GO
