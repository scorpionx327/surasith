DROP PROCEDURE [dbo].[sp_WFD02210_DeleteAssetFlagY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec WFD02210_ValidationBeforeSubmitAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f14',null,''
CREATE PROCEDURE [dbo].[sp_WFD02210_DeleteAssetFlagY]
(	
	@DOC_NO						VARCHAR(10)			= NULL		--DOC NO
)
AS
BEGIN

	--=============== UPDATE TB_R_REQUEST_CIP ===============--

	UPDATE	H
	SET		PROCESS_STATUS = NULL
	FROM	TB_M_ASSETS_H H
	INNER	JOIN
			TB_R_REQUEST_CIP C
	ON		H.ASSET_NO		= C.ASSET_NO
	WHERE	C.DOC_NO		= @DOC_NO AND
			C.DELETE_FLAG	= 'Y'

	DELETE	H
	FROM	TB_R_ASSET_HIS H
	INNER	JOIN
			TB_R_REQUEST_CIP C
	ON		H.ASSET_NO		= C.ASSET_NO AND
			H.COST_CODE		= C.COST_CODE AND
			H.DOC_NO		= C.DOC_NO
	WHERE	C.DOC_NO		= @DOC_NO AND
			C.DELETE_FLAG	= 'Y'

	DELETE TB_R_REQUEST_CIP
	WHERE	DOC_NO			= @DOC_NO AND 
			DELETE_FLAG		= 'Y'
END


GO
