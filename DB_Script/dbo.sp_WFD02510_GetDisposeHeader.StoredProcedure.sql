DROP PROCEDURE [dbo].[sp_WFD02510_GetDisposeHeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 24/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_GetNewAssetOwner '0806/T', '0806','E03DK208'    
CREATE PROCEDURE [dbo].[sp_WFD02510_GetDisposeHeader]
(	
	@GUID			T_GUID
)
	
AS
BEGIN

	SELECT	COMPANY,
			DOC_NO,
			DISPOSAL_BY,
			DISP_TYPE,
			DISP_MASS_REASON,
			DISP_MASS_ATTACH_DOC,
			MINUTE_OF_BIDDING,
			HIGHER_APP_ATTACH,
			ROUTE_TYPE
	FROM	TB_T_REQUEST_DISP_H H
	WHERE	[GUID]		= @GUID
	
END
GO
