DROP PROCEDURE [dbo].[sp_WFD02210_GetAssetCIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 08/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--truncate table TB_T_SELECTED_ASSETS where GUID = 'tbf1da954-0d12-4988-9a6a-65e2ca593258'
--declare @TOTAL_ITEM int exec sp_WFD02210_GetAssetCIP null,'C2017/4080','0755',null,1,10,null,@TOTAL_ITEM out  
CREATE PROCEDURE [dbo].[sp_WFD02210_GetAssetCIP]
(	
	@GUID					VARCHAR(50)			= NULL,		--GUID
	@DOC_NO					VARCHAR(10)	,		--DOC NO
	@USER_BY				VARCHAR(8)			= NULL,		--DOC NO
	@UPDATE_DATE			VARCHAR(30)			= NULL,		--DOC NO
	@CURRENCT_PAGE			INT					= NULL,		--Current page
	@PAGE_SIZE				INT					= NULL,		--Page size
	@ORDER_BY				NVARCHAR(50)		= NULL,		--Order by
	@TOTAL_ITEM				INT output
)
AS
BEGIN
	IF OBJECT_ID('tempdb..#ASSETS_CIP') IS NOT NULL DROP TABLE #ASSETS_CIP
	
	DELETE TB_T_SELECTED_ASSETS
	WHERE CREATE_DATE		< DATEADD(DAY,-1,GETDATE())

	IF ISNULL(@DOC_NO,'') = CONCAT(@USER_BY,'/C') -- Bew Asset
	BEGIN
		DELETE	TB_R_REQUEST_CIP 
		WHERE	DOC_NO			= @DOC_NO AND
				[GUID]			!= @GUID
	
		DELETE	TB_R_REQUEST_CIP 
		WHERE	DOC_NO			= @DOC_NO AND
				DELETE_FLAG		= 'Y'
		-- Insert Selected To Actual Tables
		INSERT INTO TB_R_REQUEST_CIP
		(
			DOC_NO,					ASSET_NO,				COST_CODE,					CIP_NO, 
			PLAN_AMOUNT,			ACT_AMOUNT,				COMPLETE_DATE,				DELETE_FLAG, 
			CREATE_DATE,			CREATE_BY,				UPDATE_DATE,				UPDATE_BY, 
			GUID					
		)
		SELECT	
			@DOC_NO,				T.ASSET_NO,				ISNULL(T.COST_CODE,''),		ISNULL(H.TAG_NO,''),
			H.PLAN_COST,			H.COST,					NULL,						'N',
			GETDATE(),				@USER_BY,				GETDATE(),					@USER_BY,
			@GUID
		FROM	TB_T_SELECTED_ASSETS T	WITH (NOLOCK)
		INNER JOIN TB_M_ASSETS_H H WITH (NOLOCK) ON T.ASSET_NO		= H.ASSET_NO 
									AND T.GUID			= @GUID 	
		LEFT JOIN TB_R_REQUEST_CIP C WITH (NOLOCK) ON T.COST_CODE		= C.COST_CODE 
									AND T.ASSET_NO		= C.ASSET_NO 
									AND T.GUID			= @GUID 
									AND C.DOC_NO		= @DOC_NO
									AND C.DELETE_FLAG	= 'N'
		WHERE C.ASSET_NO IS NULL
		ORDER BY T.ASSET_NO 

	END

	SELECT	
	@GUID GUID,					DOC_NO,							ISNULL(CIP_NO,'') CIP_NO,			C.ASSET_NO,						H.ASSET_NAME,	
	CONVERT(varchar, CAST(C.PLAN_AMOUNT AS money), 1) PLAN_COST, CONVERT(varchar, CAST(C.ACT_AMOUNT AS money), 1)	 ACTUAL_COST,		C.COST_CODE,						'N' AS ALLOW_DELETE,			dbo.fn_dateFAS(COMPLETE_DATE) COMPLETE_DATE,	 		
	CASE WHEN LEN(H.ASSET_NAME) < 61 THEN H.ASSET_NAME ELSE LEFT(H.ASSET_NAME,50) + '..' END ASSET_NAME_SHOW,
	'N' AS RETIRED_FLAG	
	INTO #ASSETS_CIP
	FROM	TB_R_REQUEST_CIP C	WITH (NOLOCK)
	INNER JOIN TB_M_ASSETS_H H WITH (NOLOCK) ON C.ASSET_NO	= H.ASSET_NO 
	WHERE C.DOC_NO			= @DOC_NO	
		AND C.DELETE_FLAG	= 'N'
	ORDER BY ASSET_NO 

	--- Add By Surasith 2017-03-13
	--- Set Delete Flag Incase Cost Center is changed. or Asset No already expire

	UPDATE	T
	SET		ALLOW_DELETE	= 'Y'
	FROM	#ASSETS_CIP T
	LEFT	JOIN
			TB_M_ASSETS_H H
	ON		T.ASSET_NO	= H.ASSET_NO
	LEFT	JOIN
			TB_M_ASSETS_D D
	ON		T.ASSET_NO	= D.ASSET_NO AND
			T.COST_CODE	= D.COST_CODE
	WHERE	( 
				D.ASSET_NO IS NULL OR -- Cost Center is expired
				H.ASSET_NO IS NULL OR -- Asset No is deleted from master (should not occur)
				ISNULL(H.[STATUS], 'N') = 'N'
			)

	UPDATE	T
	SET		RETIRED_FLAG	= 'Y'
	FROM	#ASSETS_CIP T
	LEFT	JOIN
			TB_M_ASSETS_H H
	ON		T.ASSET_NO	= H.ASSET_NO
	LEFT	JOIN
			TB_M_ASSETS_D D
	ON		T.ASSET_NO	= D.ASSET_NO AND
			T.COST_CODE	= D.COST_CODE
	WHERE	( 
				D.ASSET_NO IS NULL OR -- Cost Center is expired
				H.ASSET_NO IS NULL OR -- Asset No is deleted from master (should not occur)
				ISNULL(H.[STATUS], 'N') = 'N'
			)
	-- End Of Modify

	SELECT @TOTAL_ITEM = COUNT(1)
	FROM #ASSETS_CIP


	SELECT 
	*
	FROM #ASSETS_CIP
	ORDER BY ASSET_NO
	

END


GO
