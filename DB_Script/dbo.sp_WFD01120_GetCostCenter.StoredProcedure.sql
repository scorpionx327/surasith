DROP PROCEDURE [dbo].[sp_WFD01120_GetCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================WFD01120===================
-- Author:		<Uten S.>
-- Create date: <2017/03/11> 
-- Description:	for WFD01120 get cost center for general report 
-- =============================================
CREATE PROCEDURE  [dbo].[sp_WFD01120_GetCostCenter]
(	
	@EMP_CODE	VARCHAR(8)
)
AS
BEGIN
SET NOCOUNT ON;
   SELECT COST_CODE,CONCAT(COST_CODE,':', COST_NAME) AS COST_NAME FROM dbo.FN_FD0COSTCENTERLIST(@EMP_CODE)
   ORDER BY COST_NAME
END



GO
