DROP PROCEDURE [dbo].[sp_WFD01270_ValidationAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01270_ValidationAsset]
(	
	@GUID				VARCHAR(50)		= NULL,
	@RequestType		VARCHAR(1)		= NULL,
	@DOC_NO				T_DOC_NO		= NULL,
	@UPDATE_DATE		VARCHAR(30)		= NULL		
)
AS
BEGIN
	
	-- Request Stock not validate on asset
	IF @RequestType = 'S'
		RETURN

	-- Incase Disposal no need to check inactive assets that exists in document
	DECLARE @ASSETS_UPDATED AS TABLE
	(
		COMPANY		VARCHAR(10),
		ASSET_NO	VARCHAR(12),
		ASSET_SUB	VARCHAR(4)
	)
	IF @RequestType = 'D'
	BEGIN
		INSERT
		INTO	@ASSETS_UPDATED(COMPANY, ASSET_NO, ASSET_SUB)
		SELECT	H.COMPANY, H.ASSET_NO, H.ASSET_SUB 
		FROM	TB_R_ASSET_HIS R
		INNER	JOIN
				TB_M_ASSETS_H H
		ON		R.ASSET_NO	= H.ASSET_NO
		WHERE	DOC_NO		= @DOC_NO AND
				SAP_UPDATE_STATUS = 'Y'
	END

	DECLARE @MESSAGE_CODE	VARCHAR(12)
	



	--==================== Check Asset Status ====================--

	DECLARE @TB_T_ERR_MSG AS TABLE
	(
		MESSAGE_CODE	VARCHAR(25),
		MESSAGE_TYPE	VARCHAR(4),
		MESSAGE_TEXT	VARCHAR(255)
	)
	-- Check Asset No is Retired : MCOM0015AERR = Asset No {0} was retired
	INSERT INTO @TB_T_ERR_MSG
	SELECT	'MCOM0015AERR', 'ERR', DBO.fn_ReplaceMessage2('MCOM0015AERR',T.ASSET_NO, T.ASSET_SUB)
	FROM	TB_T_SELECTED_ASSETS T	 
	LEFT	JOIN
			TB_M_ASSETS_H H
	ON		T.COMPANY	= H.COMPANY AND
			T.ASSET_NO	= H.ASSET_NO AND
			T.ASSET_SUB	= H.ASSET_SUB
	WHERE	T.GUID	= @GUID AND
			( 
				H.ASSET_NO IS NULL OR -- Asset No is deleted from master (should not occur)
				(ISNULL(H.[STATUS], 'N') = 'N' AND 
						ISNULL(H.PROCESS_STATUS,'-') != 'DC' AND -- Allow to continue when status is inactive and exists in disposal process
						NOT EXISTS(SELECT 1 FROM @ASSETS_UPDATED U 
								WHERE	U.COMPANY	= T.COMPANY AND
										U.ASSET_NO	= T.ASSET_NO AND 
										U.ASSET_SUB = T.ASSET_SUB)
						-- Support disposal
				)
			)
			

	-- Other Process Status
	INSERT INTO @TB_T_ERR_MSG
	SELECT	'MFAS1902AERR', 'ERR', DBO.fn_ReplaceMessage2('MFAS1902AERR',A.ASSET_NO,
			CASE A.PROCESS_STATUS	WHEN 'C' THEN 'CIP' 
								WHEN 'D' THEN 'Disposing' 
								WHEN 'T' THEN 'Transfering' 
								ELSE '' END)
	FROM	TB_T_SELECTED_ASSETS T	 
	INNER	JOIN 
			TB_M_ASSETS_H A 
	ON		T.COMPANY	= A.COMPANY AND
			T.ASSET_NO	= A.ASSET_NO AND 
			T.ASSET_SUB = A.ASSET_SUB
	WHERE	T.[GUID]		= @GUID  AND
			(ISNULL(A.PROCESS_STATUS,'') <> '' AND (@RequestType NOT IN ('P','R','L')))
			AND NOT EXISTS (SELECT	1 
							FROM	TB_R_ASSET_HIS HS 
							WHERE	HS.DOC_NO = @DOC_NO AND 
									HS.ASSET_NO = T.ASSET_NO AND 
									HS.ASSET_SUB = T.ASSET_SUB)  -- Case validate Approve
			
			-- Not include P		


	--==================== Check Asset No & Cost Center Matching ====================--
	
	INSERT INTO @TB_T_ERR_MSG
	SELECT	'MCOM0015AERR', 'ERR', DBO.fn_ReplaceMessage3('MCOM0011AERR',C.ASSET_NO,C.COST_CODE, A.COST_CODE)
	FROM	TB_T_SELECTED_ASSETS C
	LEFT	JOIN	TB_M_ASSETS_H A
	ON		C.COMPANY		= A.COMPANY AND
			C.ASSET_NO		= A.ASSET_NO AND
			C.ASSET_SUB		= A.ASSET_SUB
	WHERE	C.[GUID]		= @GUID AND 
			A.ASSET_NO		IS NULL AND
			C.COST_CODE		<> A.COST_CODE

	

	IF @UPDATE_DATE IS NOT NULL
	BEGIN
		INSERT INTO @TB_T_ERR_MSG
		SELECT	'MSTD0114AERR', 'ERR', DBO.fn_ReplaceMessage('MSTD0114AERR')
		FROM	TB_R_REQUEST_H T
		WHERE	DOC_NO			=	@DOC_NO
			AND FORMAT(T.UPDATE_DATE,'yyyy-MM-dd HH:MM:ss.fff')		<> @UPDATE_DATE 
	END
	
	SELECT	*
	FROM	@TB_T_ERR_MSG
END


GO
