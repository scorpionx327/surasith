DROP PROCEDURE [dbo].[sp_WFD02510_DeleteAssetFlagY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02510_DeleteAssetFlagY]
@DOC_NO		T_DOC_NO,
@USER		T_SYS_USER
AS
BEGIN
	-- This store is executed by Approve Operation
	-------------------------------------------------------------
	DELETE
	FROM	TB_T_DELETED_ASSETS
	WHERE	DOC_NO = @DOC_NO
	
	INSERT
	INTO	TB_T_DELETED_ASSETS
	(		COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	COST_CODE)
	select	COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	NULL
	FROM	TB_T_REQUEST_DISPOSE
	WHERE	DOC_NO	= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	EXEC sp_WFD01170_GenerateCommentDeleted @DOC_NO, @USER , 'A'
	-------------------------------------------------------------
	-- Unlock process status when assets is deleted from request
	UPDATE	H
	SET		H.PROCESS_STATUS	= NULL,
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_T_REQUEST_DISPOSE M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO		= @DOC_NO AND
			M.DELETE_FLAG	= 'Y'
	
	

	-- Delete assets from request
	DELETE	TB_T_REQUEST_DISPOSE 
	WHERE	DOC_NO		= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	-------------------------------------------------------------
END
GO
