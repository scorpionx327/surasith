DROP PROCEDURE [dbo].[sp_WFD01250_CheckDupplicateApproveH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 13-03-2017
-- Description:	Get dupplicate asset category base on same request type
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_CheckDupplicateApproveH]
	@COMPANY			T_COMPANY,
	@REQUEST_FLOW_TYPE nvarchar(2),
	@ASSET_CLASS_STR NVARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;

    select 
		ASSET_CLASS 
	from TB_M_APPROVE_H
	WHERE ASSET_CLASS in (SELECT * from STRING_SPLIT(@ASSET_CLASS_STR,','))
		and REQUEST_FLOW_TYPE=@REQUEST_FLOW_TYPE
		and COMPANY = @COMPANY


END
GO
