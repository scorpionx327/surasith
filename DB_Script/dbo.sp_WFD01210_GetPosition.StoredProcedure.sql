DROP PROCEDURE [dbo].[sp_WFD01210_GetPosition]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun
-- Create date: 05/03/2017
-- Description:	Get All orgrnization order by
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetPosition]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
      DISTINCT ( [POST_CODE])
      ,[POST_NAME]
      
  FROM [TB_M_EMPLOYEE]
  where ACTIVEFLAG = 'Y'
  order by POST_NAME
END




GO
