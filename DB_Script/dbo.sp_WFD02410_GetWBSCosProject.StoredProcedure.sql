DROP PROCEDURE [dbo].[sp_WFD02410_GetWBSCosProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 19/07/2019
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_GetLocation 'C20170004','AS0000000100','30000',null,'0179'
CREATE PROCEDURE [dbo].[sp_WFD02410_GetWBSCosProject]
(	
	@COMPANY_CODE				 T_COMPANY	
)
AS
BEGIN
	SELECT 
		WBS_CODE CODE,	
		PROJECT_DEFINITION VALUE
	FROM TB_M_WBS  
	WHERE COMPANY_CODE	= @COMPANY_CODE		AND
	WBS_CODE	LIKE 'D%'

END


GO
