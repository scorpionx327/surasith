DROP PROCEDURE [dbo].[sp_WFD01210_Check_In_Charge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sarun yuanyong
-- Create date: 14/03/2017
-- Description:	check incharge
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_Check_In_Charge]
		@COST_CODE		T_COST_CODE,
		@USER_LOGIN		T_SYS_USER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

	SELECT	COST_CODE,EMP_CODE 
	from	TB_M_SV_COST_CENTER
	where	COST_CODE in (SELECT Value FROM fn_Split(@COST_CODE,',')) AND
			EMP_CODE <> @USER_LOGIN

	if(@@ROWCOUNT>0)
	begin 
	delete TB_T_SV_COST_CENTER
	end
END



GO
