DROP PROCEDURE [dbo].[sp_WFD02610_IsApprovedAllCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 16-03/2017
-- Description:	Count of approved request stock for check finish plan. 
-- If num of approved request stock is not equal num of stock take cost center will cannot finish plan
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02610_IsApprovedAllCostCenter]
	@YEAR varchar(4)
	, @ROUND varchar(2)
	, @ASSET_LOCATION varchar(1)
AS
	--DECLARE  @ASSET_COST_CENTER_TB TABLE (
	--	COST_CODE varchar(8)
	--);
	DECLARE @UNAPPROVED_CNT int;
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#T_STOCK_REQUEST') IS NOT NULL
	BEGIN
			DROP TABLE #T_STOCK_REQUEST
	END

	--INSERT INTO @ASSET_COST_CENTER_TB
	--SELECT COST_CODE
	--FROM TB_R_STOCK_TAKE_D
	--WHERE YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION 
	--GROUP BY COST_CODE

	--select 
	--	@APPROVED_CNT = COUNT(1) 
	--from TB_R_REQUEST_STOCK s
	--LEFT JOIN TB_R_REQUEST_H h on s.DOC_NO=h.DOC_NO
	--WHERE  s.YEAR=@YEAR and s.ROUND=@ROUND 
	--		and s.ASSET_LOCATION=@ASSET_LOCATION 
	--		and s.COST_CODE in (SELECT * from @ASSET_COST_CENTER_TB)
	--		and h.STATUS='60'

	SELECT S.YEAR, S.ROUND,S.ASSET_LOCATION, S.SV_EMP_CODE, S.COST_CODE
	INTO #T_STOCK_REQUEST 
	FROM TB_R_REQUEST_STOCK s
	LEFT JOIN TB_R_REQUEST_H h on s.DOC_NO=h.DOC_NO
	WHERE  s.YEAR=@YEAR and s.ROUND=@ROUND 
			and s.ASSET_LOCATION=@ASSET_LOCATION 
		    and h.STATUS='60'


	IF @ASSET_LOCATION = 'O'
	   BEGIN
	      SELECT    @UNAPPROVED_CNT = COUNT(1)
		  FROM      TB_R_STOCK_TAKE_D_PER_SV SV
		  WHERE     SV.YEAR = @YEAR and SV.ROUND=@ROUND and SV.ASSET_LOCATION = @ASSET_LOCATION
		            AND not exists (select '1' from #T_STOCK_REQUEST Temp where Temp.SV_EMP_CODE = SV.EMP_CODE)
	   END
	ELSE
	   BEGIN
	      SELECT    @UNAPPROVED_CNT = COUNT(1)
		  FROM      TB_R_STOCK_TAKE_D D
		  WHERE     D.YEAR = @YEAR and D.ROUND=@ROUND and D.ASSET_LOCATION = @ASSET_LOCATION
		            AND not exists (select '1' from #T_STOCK_REQUEST Temp where Temp.SV_EMP_CODE = D.SV_EMP_CODE and Temp.COST_CODE = D.COST_CODE)
	 END



	--IF @APPROVED_CNT = (SELECT COUNT(1) FROM @ASSET_COST_CENTER_TB)
	--BEGIN

	--	SELECT 1 as RESULT

	--END
	--ELSE
	--BEGIN

	--	SELECT 0 as RESULT

	--END

	IF @UNAPPROVED_CNT > 0
	BEGIN

	   SELECT 0 as RESULT

	END
	ELSE
	BEGIN

		SELECT 1 as RESULT

	END


	IF OBJECT_ID('tempdb..#T_STOCK_REQUEST') IS NOT NULL
	BEGIN
			DROP TABLE #T_STOCK_REQUEST
	END

END



GO
