DROP PROCEDURE [dbo].[sp_BFD01210_InsertEmployeeInfoChanged]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01210_InsertEmployeeInfoChanged]
AS
BEGIN
	---------------------------------------------------------------------------------------------------------------------------------
	-- Update Employee Info Incase Information is changed.
	DELETE
	FROM	TB_T_EMPLOYEE_CHANGE

	INSERT
	INTO	TB_T_EMPLOYEE_CHANGE
	(		SYS_EMP_CODE, COMPANY, EMP_CODE, EMP_TITLE, EMP_NAME, EMP_LASTNAME, ORG_CODE, 
			ACTIVEFLAG, ORG_CHANGED, INFO_CHANGED)
	SELECT	CONCAT(S.HOME_COMPANY_CODE,'.',S.EMPLOYEE_NO),
			ISNULL(S.HOST_COMPANY_CODE, S.HOME_COMPANY_CODE), 
			S.EMPLOYEE_NO, 
			S.TITLE, 
			S.FIRST_NAME,
			S.LAST_NAME, 
			S.ORG_CD,
			CASE WHEN S.EMP_STATUS = 'R' OR GETDATE() <= ISNULL(EFFECTIVE_END_DATE,'1999-01-01') THEN 'N'
									ELSE 'Y' END,
			'N', -- Calculate Later
			CASE WHEN	S.TITLE			!= E.EMP_TITLE OR
						S.FIRST_NAME	!= E.EMP_NAME OR
						S.LAST_NAME		!= E.EMP_LASTNAME  THEN 'Y' ELSE 'N' END

	FROM	TB_S_EMP_SAP S
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		S.EMPLOYEE_NO		= E.EMP_CODE AND
			S.HOME_COMPANY_CODE	= E.COMPANY -- To be confirm
	WHERE	(
			S.TITLE			!= E.EMP_TITLE OR
			S.FIRST_NAME	!= E.EMP_NAME OR
			S.LAST_NAME		!= E.EMP_LASTNAME OR
			S.ORG_CD		!= E.ORG_CODE OR
			E.ACTIVEFLAG	!= CASE WHEN S.EMP_STATUS = 'R' OR GETDATE() <= ISNULL(EFFECTIVE_END_DATE,'1999-01-01') THEN 'N'
									ELSE 'Y' END
		    )
	---------------------------------------------------------------------------------------------------------------------------------

END
GO
