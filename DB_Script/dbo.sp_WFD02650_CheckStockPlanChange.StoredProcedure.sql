DROP PROCEDURE [dbo].[sp_WFD02650_CheckStockPlanChange]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Phithak Khonnoy
-- Create date: 25-04-2017
-- Description:	Check updatedate before upload
-- =============================================

CREATE PROCEDURE [dbo].[sp_WFD02650_CheckStockPlanChange]
  -- Add the parameters for the stored procedure here
  @STOCK_TAKE_KEY NVARCHAR(7)
  , @UPDATE_Date NVARCHAR(20)
AS
BEGIN
  SET NOCOUNT ON ;



  DECLARE @Flag INT = 0 ; -- 0: Plan not change, 1: Update changed, 2: Plan finished, New Plan Existed, 3: Plan finished, No New Plan



  IF ( SELECT COUNT(1)
		 FROM TB_R_STOCK_TAKE_H
		WHERE STOCK_TAKE_KEY						   = @STOCK_TAKE_KEY
		  AND PLAN_STATUS							   != 'F'
		  AND FORMAT(UPDATE_DATE, 'ddMMyyyyHHmmssfff') != @UPDATE_Date) > 0
  BEGIN
	--SELECT CONVERT(bit, 1) as RESULT
	SET @Flag = 1 ;
  END ;



  --ELSE
  --BEGIN 
  --	SELECT CONVERT(bit, 0) as RESULT
  --END
  IF ( SELECT COUNT(1)
		 FROM TB_R_STOCK_TAKE_H
		WHERE STOCK_TAKE_KEY = @STOCK_TAKE_KEY
		  AND PLAN_STATUS	 = 'F') > 0
  BEGIN
	IF ( SELECT COUNT(1)
		   FROM dbo.TB_R_STOCK_TAKE_H
		  WHERE PLAN_STATUS <> 'F'
			AND PLAN_STATUS <> 'D') > 0
	BEGIN
	  SET @Flag = 2 ; -- Plan finished, New Plan Existed
	END ;
	ELSE
	BEGIN
	  SET @Flag = 3 ; -- Plan finished, No New Plan
	END ;
  END ;



  SELECT @Flag ;
END ;
GO
