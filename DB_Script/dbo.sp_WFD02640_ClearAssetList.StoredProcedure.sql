DROP PROCEDURE [dbo].[sp_WFD02640_ClearAssetList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

    Open DBDiff 0.9.0.0
    http://opendbiff.codeplex.com/

    Script created by G07\surasith on 5/20/2017 at 2:36:38 PM.

    Created on:  FTHLAT2556WIN10
    Source:      FD0DEV_FTH on 10.254.47.62
    Destination: FD0DEVUT on 10.254.47.62

*/


-- =============================================
-- Author:	 Uten Sopradid
-- Create date: 16/03/2017
-- Description:	insert temp
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_ClearAssetList]
	@COMPANY		T_COMPANY,
	@DOC_NO			T_DOC_NO,
	@YEAR			varchar(4)	  ,
	@ROUND			varchar(2)	  ,
	@ASSET_LOCATION varchar(1),
	@GUID			T_GUID
AS
BEGIN
		
		UPDATE	[dbo].[TB_T_REQUEST_STOCK]
		SET		[SELECTED] = 'N'
		WHERE	(	COMPANY			= @COMPANY AND
					YEAR			= @YEAR AND ROUND=@ROUND AND 
					ASSET_LOCATION	= @ASSET_LOCATION AND 
					[GUID]			= ISNULL(@GUID,@DOC_NO)
				 )
		
	/*	DELETE
		FROM	TB_T_REQUEST_STOCK
		WHERE	[GUID] = ISNULL(@GUID,@DOC_NO)
	*/

		DELETE
		FROM	TB_T_SELECTED_ASSETS
		WHERE	[GUID] = ISNULL(@GUID,@DOC_NO)
END
GO
