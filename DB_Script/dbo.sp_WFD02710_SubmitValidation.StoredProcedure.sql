/****** Object:  StoredProcedure [dbo].[sp_WFD02710_SubmitValidation]    Script Date: 10/7/2019 6:53:50 PM ******/
DROP PROCEDURE [dbo].[sp_WFD02710_SubmitValidation]
GO
/****** Object:  StoredProcedure [dbo].[sp_WFD02710_SubmitValidation]    Script Date: 10/7/2019 6:53:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02710_SubmitValidation] 
@GUID	T_GUID
AS
BEGIN
	DECLARE @TB_T_ERR AS TABLE
	(
		MESSAGE_CODE	VARCHAR(25),
		MESSAGE_TYPE	VARCHAR(4),
		MESSAGE_TEXT	VARCHAR(255)
	)
		
	INSERT
	INTO	@TB_T_ERR
	SELECT 'MSTD9012SERR' AS MESSAGE_CODE,
			'ERR' AS MESSAGE_TYPE,
			dbo.fn_ReplaceMessage2('MSTD9012SERR',AUC_NO,INV_NO) AS MESSAGE_TEXT
	FROM	TB_T_REQUEST_SETTLE_H
	WHERE	[GUID] = @GUID AND AUC_REMAIN > 0.00
	
	SELECT	*
	FROM	@TB_T_ERR
END
GO
