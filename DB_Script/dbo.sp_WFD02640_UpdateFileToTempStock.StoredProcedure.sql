DROP PROCEDURE [dbo].[sp_WFD02640_UpdateFileToTempStock]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SUPHACHAI L. 
-- Create date: 07/04/2017
-- Description:	GenerateApprover
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_UpdateFileToTempStock]
(
	@COMPANY		T_COMPANY,
	@DOC_NO			T_DOC_NO,
	@YEAR			varchar(4)	  ,
	@ROUND			varchar(2)	  ,
	@ASSET_LOCATION varchar(1),
	@COST_CODE		T_COST_CODE,
	@ATTACHMENT		varchar(200),
	@GUID			T_GUID
	)
AS
BEGIN
	
	UPDATE	TB_T_REQUEST_STOCK
	SET		ATTACHMENT = @ATTACHMENT
	WHERE	COMPANY			= @COMPANY 
	AND		DOC_NO			= @DOC_NO 
	AND		[YEAR]				= @YEAR 
	AND		[ROUND]				= @ROUND 
	AND		ASSET_LOCATION		= @ASSET_LOCATION 
	AND		COST_CODE			= @COST_CODE 
	
END
GO
