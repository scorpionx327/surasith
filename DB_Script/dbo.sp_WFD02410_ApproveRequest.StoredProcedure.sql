DROP PROCEDURE [dbo].[sp_WFD02410_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02410_ApproveRequest]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN
	
	-- Update First
	EXEC [sp_WFD02410_UpdateAssetApprove] @GUID, @DOC_NO, @USER

	EXEC sp_WFD01170_ApproveAttachDoc @GUID, @DOC_NO, @USER



	IF dbo.fn_IsReadyGenFile(@DOC_NO) = 'Y'
	BEGIN

		UPDATE	TB_R_REQUEST_TRNF_D
		SET		[STATUS]	= 'GEN',
				UPDATE_DATE	= GETDATE(),
				UPDATE_BY	= @USER
		WHERE	DOC_NO		= @DOC_NO AND
				[STATUS]	= 'NEW'
		RETURN
	END

	IF dbo.fn_IsCompleteApprover(@DOC_NO) = 'N'
	RETURN

	UPDATE	TB_R_REQUEST_TRNF_D
	SET		TRNF_EFFECTIVE_DT	= GETDATE(),
			UPDATE_BY			= @USER,
			UPDATE_DATE			= GETDATE()
	WHERE	DOC_NO	= @DOC_NO

	-- Update
	UPDATE	H
	SET		PROCESS_STATUS	= 'TC',
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @USER
	FROM	TB_M_ASSETS_H H WITH (NOLOCK)
	INNER	JOIN 
			TB_R_REQUEST_TRNF_D T WITH (NOLOCK)
	ON		H.COMPANY	= T.COMPANY AND 
			H.ASSET_NO	= T.ASSET_NO -- Set Process Status = 'T' both of Parent and Child
	WHERE	T.DOC_NO	= @DOC_NO

	DECLARE @LatestApproverCode	T_SYS_USER, 
			@LatestApproverName	VARCHAR(68)

	DECLARE @AECCode		T_SYS_USER
	UPDATE	H
	SET		H.DETAIL		= 'Transfer from {0} to {1}', -- Follow transfer type
								
			H.TRANS_DATE	= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= @LatestApproverName, 
			H.AEC_APPR_BY			= @AECCode,
			H.AEC_APPR_DATE			= GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_TRNF_D M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND 
			M.DOC_NO	= H.DOC_NO AND 
			M.ASSET_NO	= H.ASSET_NO AND 
			M.ASSET_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO
	

END
GO
