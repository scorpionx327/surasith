DROP PROCEDURE [dbo].[sp_common_GetBatchStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_common_GetBatchStatus]
	-- Add the parameters for the stored procedure here
	@APP_ID		int
AS
	DECLARE @IS_PROCESSING bit;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	/*
	SELECT 
		STATUS ,
		ISNULL((SELECT TOP 1 MESSAGE FROM  TB_L_LOGGING l WHERE l.APP_ID = b.APP_ID order by LOG_ID desc), 'Waiting . . .') as MESSAGE
	FROM TB_BATCH_Q b WHERE b.APP_ID=@APP_ID
	*/
	DECLARE @Status VARCHAR(1)
	SELECT	@Status =	[STATUS]
	FROM	TB_BATCH_Q
	WHERE	APP_ID	=	@APP_ID

	IF @Status = 'E'
	BEGIN
		-- Check Data Not Found
		DECLARE @NotFound VARCHAR(255)
		SELECT	TOP 1 @NotFound = [MESSAGE] 
		FROM	TB_L_LOGGING 
		WHERE	APP_ID = @APP_ID AND [LEVEL] = 'E' AND [MESSAGE] LIKE 'MSTD0059AERR%'
		ORDER	BY	
				LOG_ID DESC
		IF @@ROWCOUNT > 0
		BEGIN
			SELECT 'N' AS [STATUS], @NotFound [MESSAGE]
			RETURN
		END		
	END

	DECLARE @Message VARCHAR(255)

	SELECT	TOP 1 @Message = [MESSAGE] 
	FROM	TB_L_LOGGING l WHERE l.APP_ID = @APP_ID 
	ORDER	BY	LOG_ID DESC
	
	SET  @Message = ISNULL(@Message, 'Waiting . . .')

	SELECT @Status AS [STATUS], @Message [MESSAGE]

END



GO
