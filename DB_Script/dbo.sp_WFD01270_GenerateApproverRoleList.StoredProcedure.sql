DROP PROCEDURE [dbo].[sp_WFD01270_GenerateApproverRoleList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
-- Create proc [dbo].[sp_WFD01270_GenerateApproverRoleList] "dbo.sp_WFD01270_GenerateApproverRoleList"
--

CREATE PROCEDURE [dbo].[sp_WFD01270_GenerateApproverRoleList]
@DocNo			VARCHAR(10),
@Seq			INT,
@DisplayRole	VARCHAR(3),
@ActRole		VARCHAR(3),
@Level			VARCHAR(4),
@MainFlag		VARCHAR(1),
@RowCount		INT OUT
AS
BEGIN
	-- Prepare Master for comparing
	-- 01 : 
	SELECT	E.EMP_CODE,
			CASE	WHEN @Level = 'COM'  THEN D.CMP_CODE 
					WHEN @Level = 'DIV'  THEN D.DIV_CODE 
					WHEN @Level = 'SDIV' THEN D.SUB_DIV_CODE 
					WHEN @Level = 'DEPT' THEN D.DEPT_CODE 
					WHEN @Level = 'SECT' THEN D.SECTION_CODE 
					WHEN @Level = 'LINE' THEN D.LINE_CODE 
					ELSE '' END AS ORG_CODE
	INTO	#TB_M_EMPLOYEE
	FROM	TB_M_EMPLOYEE E
	INNER	JOIN
			TB_M_ORGANIZATION D
	ON		E.ORG_CODE		= D.ORG_CODE
	WHERE	E.[ROLE]		= @ActRole AND
			E.ACTIVEFLAG	= 'Y' AND
			CASE	WHEN @Level = 'COM'  THEN D.CMP_CODE 
					WHEN @Level = 'DIV'  THEN D.DIV_CODE 
					WHEN @Level = 'SDIV' THEN D.SUB_DIV_CODE 
					WHEN @Level = 'DEPT' THEN D.DEPT_CODE 
					WHEN @Level = 'SECT' THEN D.SECTION_CODE 
					WHEN @Level = 'LINE' THEN D.LINE_CODE 
					ELSE NULL END  IS NOT NULL
			
	
	
	UPDATE	#TB_T_CC
	SET		ORG_CODE = CASE	WHEN @Level = 'COM'  THEN ISNULL(CMP_CODE,'') 
							WHEN @Level = 'DIV'  THEN ISNULL(DIV_CODE,'') 
							WHEN @Level = 'SDIV' THEN ISNULL(SUB_DIV_CODE,'') 
							WHEN @Level = 'DEPT' THEN ISNULL(DEPT_CODE,'') 
							WHEN @Level = 'SECT' THEN ISNULL(SECTION_CODE,'') 
							WHEN @Level = 'LINE' THEN ISNULL(LINE_CODE,'') 
						ELSE '' END
	-- select @Level, * from #TB_T_CC
	-- select @Level, * from #TB_M_EMPLOYEE
	INSERT
	INTO	TB_H_REQUEST_APPR(DOC_NO, INDX, [ROLE], EMP_CODE, MAIN)
	SELECT	DISTINCT
			@DocNo, @Seq, @DisplayRole AS APPV_ROLE,  E.EMP_CODE,
			@MainFlag AS MAIN
	FROM	#TB_M_EMPLOYEE E
	INNER	JOIN
			#TB_T_CC CC
	ON		E.ORG_CODE	= CC.ORG_CODE
	LEFT	JOIN
			TB_H_REQUEST_APPR H
	ON		H.DOC_NO	= @DocNo AND
			H.INDX		= @Seq AND
			H.EMP_CODE	= E.EMP_CODE
	WHERE	EXISTS(SELECT 1 FROM #TB_T_CC_TARGET T WHERE T.COST_CODE = CC.COST_CODE) AND -- Support Transfer Cost Center / Stock Taking
			H.EMP_CODE		IS NULL
	
	SET @RowCount = @@ROWCOUNT

	DROP TABLE #TB_M_EMPLOYEE

END
GO
