DROP PROCEDURE [dbo].[sp_WFD02210_SubmitAssetCIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_InsertAssetCIP 'C20170004','AS0000000100','30000',null,'0179'
CREATE PROCEDURE [dbo].[sp_WFD02210_SubmitAssetCIP]
(	
	@GUID						VARCHAR(50)			= NULL,
	@DocNoMatch					VARCHAR(MAX)		= NULL,
	@USER_BY					VARCHAR(8)			= NULL
)
AS
BEGIN

		-- Convert
		DECLARE @TB_T_DOC AS TABLE
		(
			DOC_NO		VARCHAR(10),
			ASSET_CATE	VARCHAR(2),
			REQUEST_TYPE	VARCHAR(1)
		)
		INSERT
		INTO	@TB_T_DOC
		SELECT	DOC_NO, ASSET_CATE, REQUEST_TYPE FROM dbo.fn_GenDocList(@DocNoMatch)
		-- dbo.fn_GenDocList('A*1|B*2|A*3|A*4|A*5|A*6|A*7')

		DELETE	T
		FROM	TB_T_SELECTED_ASSETS T 
		INNER	JOIN TB_R_REQUEST_CIP R WITH (NOLOCK)
		 ON		T.ASSET_NO		= R.ASSET_NO AND
				T.[GUID]		= R.[GUID]
		WHERE (R.COMPLETE_DATE IS NULL OR R.DELETE_FLAG = 'Y') AND
				R.[GUID]			= @GUID

		DELETE
		FROM	TB_R_REQUEST_CIP 
		WHERE	[GUID] = @GUID AND 
				(COMPLETE_DATE IS NULL OR DELETE_FLAG = 'Y')

		UPDATE	CIP
		SET		DOC_NO		= T.DOC_NO,
				UPDATE_DATE	= GETDATE(),
				UPDATE_BY	= @USER_BY
		FROM	TB_R_REQUEST_CIP CIP WITH (NOLOCK)
		INNER	JOIN
				TB_T_SELECTED_ASSETS H WITH (NOLOCK) --- Create On Main function
		ON		CIP.ASSET_NO = H.ASSET_NO
		INNER	JOIN
				@TB_T_DOC T
		ON		T.ASSET_CATE = H.ASSET_CATEGORY
		WHERE	CIP.[GUID]	= @GUID


		-- UPDATE PROCESS_STATUS on TB_M_ASSET_H (only selected asset) : 2017.03.08 Nunchaya S.(Tik) 
		UPDATE	H
		SET		PROCESS_STATUS	= 'C',
				UPDATE_DATE		= GETDATE(),
				UPDATE_BY		= @USER_BY
		FROM	TB_M_ASSETS_H H WITH (NOLOCK)
		INNER	JOIN 
				TB_T_SELECTED_ASSETS CIP WITH (NOLOCK)
		ON		H.ASSET_NO		= CIP.ASSET_NO
		WHERE	CIP.[GUID]	= @GUID
		--=============== INSERT INTO TB_R_ASSET_HIS ===============--
		
		INSERT 
		INTO TB_R_ASSET_HIS
		(
				DOC_NO,
				ASSET_NO,		
				COST_CODE,		
				REQUEST_TYPE,	
				[STATUS], 
				CREATE_DATE,	CREATE_BY,		UPDATE_DATE,	UPDATE_BY
		)

		SELECT	C.DOC_NO,		
				C.ASSET_NO,		
				C.COST_CODE,	
				'C',			
				H.[STATUS],
				GETDATE(),		@USER_BY,		GETDATE(),		@USER_BY
		FROM	TB_R_REQUEST_CIP C WITH (NOLOCK)
		INNER	JOIN
				TB_R_REQUEST_H H WITH (NOLOCK)
		ON		C.DOC_NO	= H.DOC_NO
		WHERE	C.[GUID]	= @GUID
		
		-----------------------------------------------------------------------------------
		-- Add New By Surasith T. 2017-06-13
		UPDATE	H
		SET		H.ASSET_COST_CODE = R.COST_CODE
		FROM	TB_R_REQUEST_H H
		INNER	JOIN
		(
				SELECT	ROW_NUMBER() OVER(PARTITION BY R.DOC_NO ORDER BY ASSET_NO) AS INDX,
						R.DOC_NO, COST_CODE
				FROM	TB_R_ASSET_HIS R
				INNER	JOIN
						(SELECT DISTINCT DOC_NO FROM @TB_T_DOC) T
				ON		R.DOC_NO = T.DOC_NO
		)	R
		ON		H.DOC_NO = R.DOC_NO AND R.INDX = 1
		-----------------------------------------------------------------------------------

END


GO
