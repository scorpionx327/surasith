DROP PROCEDURE [dbo].[sp_Common_GetLine_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetLine_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER,
@SecKeyword		varchar(200)

AS
BEGIN
		SELECT LINE_CODE,
				LINE_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE (	
				LINE_CODE LIKE CONCAT(@keyword,'%') 
				OR
				LINE_NAME LIKE CONCAT(@keyword,'%') 
				) 
			AND (	
				SECTION_CODE LIKE CONCAT(@SecKeyword,'%') 
				OR
				SECTION_NAME LIKE CONCAT(@SecKeyword,'%') 
				) 
		GROUP BY LINE_CODE, LINE_NAME
		ORDER BY LINE_CODE ASC
END
GO
