DROP PROCEDURE [dbo].[sp_WFD01150_GetTmpDivH_MAIL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_GetTmpDivH_MAIL]
(
	@APPRV_ID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MyTableVar table(  
		APPRV_ID int,
		SEQ varchar(3),
		[ROLE] varchar(3),
		DIVISION varchar(6),
		GRP varchar(8),
		CONDITION varchar(400),
		REQ varchar(3),
		REJ varchar(3),

		OPERATOR Varchar(400),																																																								
		VALUE1 int,			
		VALUE2 int,				
		ALLOWTOREJECT varchar(3),																																																										
		ALLOWTODELETE varchar(3),
		LEADTIME int,
		ALLOWTOSELECT varchar(3),
		FINISHFLOW varchar(3),
		GENFILE varchar(3),
		HIGHERAPPROVE  varchar(3),
		NOTIFICATIONBYEMAIL   varchar(3),
		OPERATION   varchar(3),
		Isinsert varchar(1)
	); 

	DECLARE @01 varchar(3);
	DECLARE @02 varchar(3);
	DECLARE @20 varchar(3);
	DECLARE @30 varchar(3);
	DECLARE @40 varchar(3);
	DECLARE @41 varchar(3);
	DECLARE @50 varchar(3);
	DECLARE @70 varchar(3);
	DECLARE @71 varchar(3);
	SET @01 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =1)
	SET @02 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =2)
	SET @20 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =20)
	SET @30 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =30)
	SET @40 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =40)
	SET @41 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =41)
	SET @50 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =50)
	SET @70 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =70)
	SET @71 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =71)

	IF(@01 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'01','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'01','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@02 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'02','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'02','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@20 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'20','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'20','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@30 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'30','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'30','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@40 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'40','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'40','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@41 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'41','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'41','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@50 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'50','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'50','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@70 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'70','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'70','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	IF(@71 is not null)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'71','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END
	
	IF(@APPRV_ID = 0)
	BEGIN
		INSERT INTO @MyTableVar select 	@APPRV_ID,'71','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	END

	select * from @MyTableVar
END

GO
