DROP PROCEDURE [dbo].[sp_WFD01130_GetAssetName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 15/02/2017
-- Description:	GetAssetNamebyCode
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01130_GetAssetName]
	-- Add the parameters for the stored procedure here
	@COMPANY T_COMPANY,
	@ASSET_NO T_ASSET_NO,
	@ASSET_SUB T_ASSET_SUB
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ASSET_NO,ASSET_NAME FROM TB_M_ASSETS_H 
	WHERE ASSET_NO=@ASSET_NO
	AND ASSET_SUB=@ASSET_SUB
	AND COMPANY=@COMPANY
END



GO
