DROP PROCEDURE [dbo].[sp_BFD01260_GetNotificationList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01260_GetNotificationList]
AS
BEGIN
	
	UPDATE	TB_R_NOTIFICATION
	SET		SEND_FLAG = 'X'
	WHERE	SEND_FLAG = 'N' AND 
			([TO] IS NULL OR LEN([TO]) = 0)

	DECLARE @FooterMessage VARCHAR(400)
	SET @FooterMessage = dbo.fn_GetSystemMaster('NOTIFICATION',	'EMAIL',	'FOOTER');
	

	SELECT	ID,
			[TO],
			CC,
			BCC,
			TITLE,
			[MESSAGE],
			ISNULL(FOOTER, @FooterMessage) AS FOOTER,
			[TYPE],
			START_PERIOD,
			END_PERIOD,
			SEND_FLAG,
			RESULT_FLAG,
			REF_FUNC,
			REF_DOC_NO,
			--SEND_FAIL_NUMBER,
			CREATE_DATE,
			CREATE_BY,
			UPDATE_DATE,
			UPDATE_BY
	FROM	TB_R_NOTIFICATION
	WHERE	SEND_FLAG = 'N' AND
			(	[TYPE] = 'I' OR
				[TYPE] = 'S' AND GETDATE() BETWEEN START_PERIOD AND END_PERIOD
			)
			
	ORDER	BY
			ID
END



GO
