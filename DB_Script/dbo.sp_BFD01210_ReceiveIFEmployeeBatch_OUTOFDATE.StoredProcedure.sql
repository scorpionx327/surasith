DROP PROCEDURE [dbo].[sp_BFD01210_ReceiveIFEmployeeBatch_OUTOFDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD01210_ReceiveIFEmployeeBatch_OUTOFDATE]
(
 	@AppID	INT,
	@UserID		VARCHAR(8)
)
AS
BEGIN TRY
	DECLARE	@cnt	INT

	SELECT	@cnt = COUNT(*)
	FROM	TB_S_EMP_SAP
	IF(@cnt = 0)
	BEGIN
		EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MCOM0020AWRN', 'Receive Employee',@UserID
		SELECT 2 AS SUCCESS ; -- Warning
		RETURN;
	END

    DECLARE @rs INT
	EXEC @rs = sp_BFD01210_Validation @AppID, @UserID
	
	IF @rs = 2
	RETURN

	--BEGIN TRANSACTION T1
		
		DECLARE @ERROR_MESSAGE VARCHAR(MAX)

		-- Keep Original TB_M_EMP_SAP
		EXEC sp_BFD01210_InsertEmployeeOriginalSAP

		EXEC sp_BFD01210_InsertEmployeeInfoChanged

		EXEC sp_BFD01210_InsertUpdateEmployeeMaster @AppID, @UserID

		-- Update Org Change Flag 2017-05-23 Surasith T.
		UPDATE	T
		SET		ORG_CHANGED = 'Y'
		FROM	TB_M_EMPLOYEE M
		INNER	JOIN
				TB_T_EMPLOYEE_CHANGE T
		ON		M.SYS_EMP_CODE = T.SYS_EMP_CODE
		WHERE	dbo.fn_IsOrganizeChanged(M.[ROLE], M.ORG_CODE, T.ORG_CODE) ='Y'
		

		-- Delete FA Window and FA Supervisor
		EXEC sp_BFD01210_DeleteFASupFAWindow @AppID, @UserID

		-- Email to Requestor Incase Approver(W) is resign / Org.Code is changed.
		EXEC sp_BFD01210_ApproverResign @AppID, @UserID

		-- Rename employee in Approve Flow
		EXEC sp_WFD01170_UpdateRequestwhenEmployeeInfoChanged 


	--	COMMIT TRANSACTION T1

		EXEC sp_Common_GenerateCostCenterMapping NULL -- Surasith
		EXEC sp_Common_GenerateCostCenterRole NULL -- Add new By Surasith

		EXEC [sp_BFD01210_WriteWarningMessage]   @AppID,@UserID

		SELECT 1 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning

	RETURN;

END TRY
BEGIN CATCH
	/*
	IF @@TRANCOUNT <> 0
	BEGIN
		ROLLBACK TRANSACTION T1
	END
	*/
	--===========================================================Write log =====================================
     DECLARE @errMessage nVARCHAR(1000);
     SET @errMessage= CONCAT('Receiving Employee Master Interface batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
	 exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@errMessage,@UserID,'Y'
	--===========================================================End Write log =====================================
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    SELECT @ErrorMessage = ERROR_MESSAGE();
    SELECT @ErrorSeverity = ERROR_SEVERITY();
    SELECT @ErrorState = ERROR_STATE();
    RAISERROR (@ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
                );
	SELECT 0 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
	RETURN;

END CATCH
GO
