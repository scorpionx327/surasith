DROP PROCEDURE [dbo].[sp_WFD02910_RejectRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02910_RejectRequest]
(	
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN


	DECLARE @DocStatus VARCHAR(2)

	SELECT	@DocStatus = [STATUS] 
	FROM	TB_R_REQUEST_H
	WHERE	DOC_NO		= @DOC_NO

	-- Undo Status Flag Y incase user reject.
	UPDATE	TB_R_REQUEST_RECLAS
	SET		DELETE_FLAG = NULL
	WHERE	DOC_NO = @DOC_NO
	
	UPDATE	TB_R_REQUEST_ASSET_D
	SET		DELETE_FLAG = NULL
	WHERE	DOC_NO = @DOC_NO

	-------------------------------------------------------------------------------------
	-- Reject by AEC
	-- Undo Status from MC -> M (To check again)
	UPDATE	H
	SET		H.PROCESS_STATUS	= 'C',
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_RECLAS M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO			= @DOC_NO AND
			H.PROCESS_STATUS	= 'CC'


		
	DECLARE @format VARCHAR(255) = 'Reject Reclassification : From {0} To {1}'

	DECLARE @LatestApproverCode	T_SYS_USER, @LatestApproverName	VARCHAR(68)
	DECLARE @AECCode		T_SYS_USER
	UPDATE	H
	SET		H.DETAIL				=  dbo.fn_StringFormat(@format, CONCAT(M.ASSET_CLASS,'|',M.NEW_ASSET_CLASS)),
			H.TRANS_DATE			= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= NULL, -- @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= NULL, -- @LatestApproverName, 
			H.AEC_APPR_BY			= NULL, -- @AECCode,
			H.AEC_APPR_DATE			= NULL, -- GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_RECLAS M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO

END
GO
