DROP PROCEDURE [dbo].[sp_WFD02410_UpdateBOIOnSubmitAssetTransfer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_SubmitAssetTransfer 'T20170004','AS0000000100','30000',null,'0179'
CREATE PROCEDURE [dbo].[sp_WFD02410_UpdateBOIOnSubmitAssetTransfer]
(	
	@DocNoMatch	VARCHAR(MAX)		= NULL,
	@AssetNo	VARCHAR(15),			
	@BOIDoc		VARCHAR(255),
	@BOICheck	VARCHAR(1)
)
AS
BEGIN
	
		-- Convert
		DECLARE @TB_T_DOC AS TABLE
		(
			DOC_NO		VARCHAR(10),
			ASSET_CATE	VARCHAR(2),
			REQUEST_TYPE	VARCHAR(1)
		)
		INSERT
		INTO	@TB_T_DOC
		SELECT	DOC_NO, ASSET_CATE, REQUEST_TYPE FROM dbo.fn_GenDocList(@DocNoMatch)
		-- dbo.fn_GenDocList('A*1|B*2|A*3|A*4|A*5|A*6|A*7')


		UPDATE	TRAF
		SET		BOI_ATTACH_DOC	= @BOIDoc,
				BOI_CHECK_FLAG	= @BOICheck
		FROM	TB_R_REQUEST_TRNF_D TRAF
		WHERE	EXISTS(SELECT 1 FROM @TB_T_DOC WHERE DOC_NO = TRAF.DOC_NO) AND ASSET_NO = @AssetNo
		
	
	
END


GO
