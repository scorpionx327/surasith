DROP PROCEDURE [dbo].[sp_BFD02110_UpperAndTrimDataInStagingTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  For convert data in stagging table 
- ============================================= SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD02110_UpperAndTrimDataInStagingTable]
AS
BEGIN 


-- Remove duplicate key in staging table.
-- Asset Detail
PRINT '-- Delete duplicate key in Asset Detail (Keys: ASSET_NO, COST_CENTER)'
DELETE FROM TB_S_ASSETS_D
WHERE  EXISTS ( SELECT *
				 FROM ( SELECT ROW_NUMBER() 
									OVER(PARTITION BY ASSET_NO	, 
													  SUBSTRING(EXPENSE_ACCOUNT, 15, 8) 
										 ORDER BY CASE
										          WHEN EMP_CODE IS NULL THEN '0'
												  ELSE '1' END DESC, UNIT DESC,EMP_CODE ASC    
												  ) AS ROWNUMBER ,
								ROW_NO --,
										--ASSET_NO, 
										--UNIT, 
										--EMP_NAME, 
										--EMP_CODE,
										--EXPENSE_ACCOUNT, 
										--LOCATION
						FROM	TB_S_ASSETS_D ) table1
				  WHERE	ROWNUMBER >'1'
				  AND table1.ROW_NO = TB_S_ASSETS_D.ROW_NO 
				)

-- Asset Header 
UPDATE  TB_S_ASSETS_H 
SET     ASSET_NO						= UPPER(LTRIM(RTRIM(ASSET_NO)))
      , ASSET_NAME 					    = CASE 
	                                      WHEN ISNULL(ASSET_NAME , '') <> '' THEN LTRIM(RTRIM( ASSET_NAME ))
										  ELSE NULL END 
      , MAJOR_MINOR_CATEGORY 			= CASE 
	                                      WHEN ISNULL(MAJOR_MINOR_CATEGORY , '') <> '' THEN LTRIM(RTRIM( MAJOR_MINOR_CATEGORY ))
 										  ELSE NULL END 
      , TAG_NO 							= CASE 
	                                      WHEN ISNULL(TAG_NO , '') <> '' THEN LTRIM(RTRIM( TAG_NO ))
										  ELSE NULL END 
      , SERIAL_NO 						= CASE 
	                                      WHEN ISNULL(SERIAL_NO , '') <> '' THEN LTRIM(RTRIM( SERIAL_NO ))
										  ELSE NULL END 
      , ASSET_TYPE 						= CASE 
	                                      WHEN ISNULL(ASSET_TYPE , '') <> '' THEN LTRIM(RTRIM( ASSET_TYPE ))
 										  ELSE NULL END 
      , MODEL 							= CASE 
	                                      WHEN ISNULL(MODEL , '') <> '' THEN LTRIM(RTRIM( MODEL ))
										  ELSE NULL END 
      , PLAN_COST 						= CASE 
	                                      WHEN ISNULL(PLAN_COST , '') <> '' THEN LTRIM(RTRIM( PLAN_COST ))
										  ELSE NULL END 
      , SUB_TYPE 						= CASE 
	                                      WHEN ISNULL(SUB_TYPE , '') <> '' THEN LTRIM(RTRIM( SUB_TYPE ))
 										  ELSE NULL END 
      , BARCODE_SIZE 					= CASE 
	                                      WHEN ISNULL(BARCODE_SIZE , '') <> '' THEN LTRIM(RTRIM( BARCODE_SIZE ))
										  ELSE NULL END 
      , CLASS_SACCESS 					= CASE 
	                                      WHEN ISNULL(CLASS_SACCESS , '') <> '' THEN LTRIM(RTRIM( CLASS_SACCESS ))
										  ELSE NULL END 
      , BOI_NO 							= CASE 
	                                      WHEN ISNULL(BOI_NO , '') <> '' THEN LTRIM(RTRIM( BOI_NO ))
										  ELSE NULL END 
      , PROJECT_NAME 					= CASE 
	                                      WHEN ISNULL(PROJECT_NAME , '') <> '' THEN LTRIM(RTRIM( PROJECT_NAME ))
										  ELSE NULL END 
      , TOOLING_COST 					= CASE 
	                                      WHEN ISNULL(TOOLING_COST , '') <> '' THEN LTRIM(RTRIM( TOOLING_COST ))
										  ELSE NULL END 
      , MACHINE_LICENSE 				= CASE 
	                                      WHEN ISNULL(MACHINE_LICENSE , '') <> '' THEN LTRIM(RTRIM( MACHINE_LICENSE ))
										  ELSE NULL END 
      , PRODUCTION_PART_NO 					= CASE 
	                                      WHEN ISNULL(PRODUCTION_PART_NO , '') <> '' THEN LTRIM(RTRIM( PRODUCTION_PART_NO ))
										  ELSE NULL END 
      , FA_BOOK 						= CASE 
	                                      WHEN ISNULL(FA_BOOK , '') <> '' THEN LTRIM(RTRIM( FA_BOOK ))
										  ELSE NULL END 
      , COST 							= CASE 
	                                      WHEN ISNULL(COST , '') <> '' THEN LTRIM(RTRIM( COST ))
 										  ELSE NULL END 
      , SALVAGE_VALUE 					= CASE 
	                                      WHEN ISNULL(SALVAGE_VALUE , '') <> '' THEN LTRIM(RTRIM( SALVAGE_VALUE ))
										  ELSE NULL END 
      , NBV 							= CASE 
	                                      WHEN ISNULL(NBV , '') <> '' THEN LTRIM(RTRIM( NBV ))
 										  ELSE NULL END 
      , YTD_DEPRECIATION 				= CASE 
	                                      WHEN ISNULL(YTD_DEPRECIATION , '') <> '' THEN LTRIM(RTRIM( YTD_DEPRECIATION ))
 										  ELSE NULL END 
      , ACCUMULATE_DEPRECIATION 		= CASE 
	                                      WHEN ISNULL(ACCUMULATE_DEPRECIATION , '') <> '' THEN LTRIM(RTRIM( ACCUMULATE_DEPRECIATION ))
										  ELSE NULL END 
      , UOP_CAPACITY 					= CASE 
	                                      WHEN ISNULL(UOP_CAPACITY , '') <> '' THEN LTRIM(RTRIM( UOP_CAPACITY ))
										  ELSE NULL END 
      , UOP_LTD_PRODUCTION 				= CASE 
	                                      WHEN ISNULL(UOP_LTD_PRODUCTION , '') <> '' THEN LTRIM(RTRIM( UOP_LTD_PRODUCTION ))
										  ELSE NULL END 
      , DATE_IN_SERVICE 				= CASE 
	                                      WHEN ISNULL(DATE_IN_SERVICE , '') <> '' THEN LTRIM(RTRIM( DATE_IN_SERVICE ))
 										  ELSE NULL END 
      , STL_LIFE_YEAR 					= CASE 
	                                      WHEN ISNULL(STL_LIFE_YEAR , '') <> '' THEN LTRIM(RTRIM( STL_LIFE_YEAR ))
										  ELSE NULL END 
      , STL_REMAIN_LIFE_YEAR 			= CASE 
	                                      WHEN ISNULL(STL_REMAIN_LIFE_YEAR , '') <> '' THEN LTRIM(RTRIM( STL_REMAIN_LIFE_YEAR ))
										  ELSE NULL END 
      , RETIREMENT_DATE 				= CASE 
	                                      WHEN ISNULL(RETIREMENT_DATE , '') <> '' THEN  LTRIM(RTRIM( RETIREMENT_DATE ))
										  ELSE NULL END
      , TRANSFER_DATE 					= CASE 
	                                      WHEN ISNULL(TRANSFER_DATE , '') <> '' THEN  LTRIM(RTRIM( TRANSFER_DATE ))
										  ELSE NULL END
      , ENTRY_PERIOD 					    = CASE 
	                                      WHEN ISNULL(ENTRY_PERIOD , '') <> '' THEN LTRIM(RTRIM( ENTRY_PERIOD ))
										  ELSE NULL END

-- Asset Detials (Asset/Cost center)
UPDATE TB_S_ASSETS_D
SET    ROW_NO							= LTRIM(RTRIM( ROW_NO ))
      , ASSET_NO   					    = UPPER(LTRIM(RTRIM(ASSET_NO)))
      , UNIT   					        = CASE 
	                                      WHEN ISNULL(UNIT , '') <> '' THEN LTRIM(RTRIM( UNIT))
										  ELSE NULL END
      , EMP_NAME   					     = CASE 
	                                       WHEN ISNULL(EMP_NAME , '') <> '' THEN LTRIM(RTRIM( EMP_NAME))
										   ELSE NULL END
      , EMP_CODE   					     = CASE 
	                                       WHEN ISNULL(EMP_CODE , '') <> '' THEN UPPER(LTRIM(RTRIM( EMP_CODE)))
										   ELSE NULL END
      , EXPENSE_ACCOUNT   				 = CASE 
	                                       WHEN ISNULL(EXPENSE_ACCOUNT , '') <> '' THEN LTRIM(RTRIM( EXPENSE_ACCOUNT))
 										   ELSE NULL END
      ,  [LOCATION]      				 = CASE 
	                                       WHEN ISNULL([LOCATION] , '') <> '' THEN LTRIM(RTRIM([LOCATION])) 
	  									   ELSE NULL END
-- Asset Finance 
UPDATE TB_S_ASSETS_FINANCE
SET     ROW_NO  							=LTRIM(RTRIM( ROW_NO ))
      , ASSET_NO  							=UPPER(LTRIM(RTRIM( ASSET_NO )))
      , INVOICE_NO 							=UPPER(LTRIM(RTRIM( INVOICE_NO )))
      , INVOICE_DATE 						= CASE 
	                                          WHEN ISNULL(INVOICE_DATE , '') <> '' THEN LTRIM(RTRIM(INVOICE_DATE))
											  ELSE NULL END
      , INVOICE_LINE 						= CASE 
	                                          WHEN ISNULL(INVOICE_LINE , '') <> '' THEN LTRIM(RTRIM( INVOICE_LINE ))
	  									       ELSE NULL END
      , [DESCRIPTION]						= CASE 
	                                          WHEN ISNULL([DESCRIPTION] , '') <> '' THEN LTRIM(RTRIM( [DESCRIPTION] ))
	  									      ELSE NULL END
      , SUPPLIER_NAME 						= CASE 
	                                          WHEN ISNULL(SUPPLIER_NAME , '') <> '' THEN LTRIM(RTRIM( SUPPLIER_NAME ))
	  									        ELSE NULL END
      , SUPPLIER_NO 						= CASE 
	                                          WHEN ISNULL(SUPPLIER_NO , '') <> '' THEN LTRIM(RTRIM( SUPPLIER_NO ))
	  									       ELSE NULL END
      , INVOICE_COST 						= CASE 
	                                          WHEN ISNULL(INVOICE_COST , '') <> '' THEN LTRIM(RTRIM( INVOICE_COST ))
 	  									      ELSE NULL END
      , PO_NO 							    =UPPER(LTRIM(RTRIM( PO_NO )))
END 






GO
