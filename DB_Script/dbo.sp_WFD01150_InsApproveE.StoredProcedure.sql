DROP PROCEDURE [dbo].[sp_WFD01150_InsApproveE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_InsApproveE]
		@APPRV_ID int,
        @INDX int,
        @ROLE varchar(3),
        @E_INDX int,
        @E_ROLE varchar(3),
        @EMAIL_APR_TO char(1),
        @EMAIL_APR_CC char(1),
        @EMAIL_REJ_TO char(1),
        @EMAIL_REJ_CC char(1)
AS
BEGIN TRY 
	SET NOCOUNT ON;
	--DECLARE @APPRV_ID	int 
	--SET @APPRV_ID = (select max(APPRV_ID) + 1 from TB_M_APPROVE_H);

	INSERT INTO TB_M_APPROVE_E
           (APPRV_ID
           ,INDX
           ,[ROLE]
           ,E_INDX
           ,E_ROLE
           ,EMAIL_APR_TO
           ,EMAIL_APR_CC
           ,EMAIL_REJ_TO
           ,EMAIL_REJ_CC)
     VALUES
           (@APPRV_ID
           ,@INDX
           ,@ROLE
           ,@E_INDX
           ,@E_ROLE
           ,@EMAIL_APR_TO
           ,@EMAIL_APR_CC
           ,@EMAIL_REJ_TO
           ,@EMAIL_REJ_CC)
END TRY
BEGIN CATCH
	THROW
END CATCH
GO
