DROP PROCEDURE [dbo].[sp_WFD01110_GetOnHandAEC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Surasith T.
-- Create date: 21/09/2019
-- Description:	get transfer tab in home page
/*
Nipon 2019-08-14 16:38:10.537
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetOnHandAEC]
	-- Add the parameters for the stored procedure here
	@COMPANY VARCHAR(50)	= null, -- not use T_COMPANY varchar so out of range.
	@EMP_CODE T_SYS_USER,
	@REQUEST_TYPE	VARCHAR(20) = NULL, -- List
	@pageNum INT,
    @pageSize INT,
    @sortColumnName VARCHAR(50),
	@orderType VARCHAR(5),
	@TOTAL_ITEM int output
AS
BEGIN
	
	IF NOT EXISTS(SELECT 1 FROM TB_M_EMPLOYEE WHERE SYS_EMP_CODE = @EMP_CODE AND FAADMIN = 'Y')
	BEGIN
		SET @TOTAL_ITEM = 0
		RETURN ;
	END
	-- get AEC Code
	DECLARE @ACR VARCHAR(3)
	SET @ACR = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','ACR_CODE','ACR')




	SELECT	CASE WHEN @sortColumnName = '0' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 WHEN @sortColumnName = '1' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 
				 WHEN @sortColumnName = '2' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY DOC_NO asc)  
				 WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DOC_NO desc)  
				 WHEN @sortColumnName = '3' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUEST_TYPE asc)
				 WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_TYPE desc)
				 
				 WHEN @sortColumnName = '4' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY LAST_APPRV_DATE asc)
				 WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY LAST_APPRV_DATE desc)
				 WHEN @sortColumnName = '5' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY DIFF_DATE asc)
				 WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DIFF_DATE desc)

				 WHEN @sortColumnName = '6' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR asc) 
				 WHEN @sortColumnName = '6' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR desc)  
				 WHEN @sortColumnName = '7' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS asc) 
				 WHEN @sortColumnName = '7' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS desc)  
				 WHEN @sortColumnName = '8' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE asc) 
				 WHEN @sortColumnName = '8' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE desc)  
				 ELSE ROW_NUMBER() OVER(ORDER BY DOC_NO ASC)  
			END AS ROWNUMBER,
			T.*
	INTO	#TB_T_RESULT
	FROM	
	(
		SELECT	R.DOC_NO,
				R.COMPANY,
				R.REQUEST_TYPE, 
				dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_TYPE',R.REQUEST_TYPE) REQUEST_TYPE_DESC, 
				MAX(W.APPR_DATE) LAST_APPRV_DATE,
				DATEDIFF(DAY,MAX(W.APPR_DATE), GETDATE()) AS DIFF_DATE,
				CONCAT(R.EMP_TITLE, ' ', R.EMP_NAME, ' ', R.EMP_LASTNAME)   AS REQUESTOR,
				dbo.fn_GetCurrentStatus(R.DOC_NO)			AS CURRENT_STATUS,
				R.REQUEST_DATE
		FROM	[dbo].[TB_R_REQUEST_H] R
		INNER	JOIN 
				TB_R_REQUEST_APPR A
		on		R.DOC_NO	= A.DOC_NO
		INNER	JOIN
				TB_R_REQUEST_APPR W
		ON		R.DOC_NO	= W.DOC_NO
		WHERE	dbo.fn_IsEndRequest(R.[STATUS]) = 'N' AND
				A.APPR_ROLE		= @ACR  AND
				A.APPR_STATUS	= 'W' AND
				( EXISTS(	SELECT	1 
							FROM	dbo.fn_GetMultipleCompanyList(@COMPANY) C 
							WHERE	C.COMPANY_CODE = r.COMPANY ) OR @COMPANY IS NULL ) AND
				( EXISTS(	SELECT	1 
							FROM	dbo.fn_GetMultipleList(@REQUEST_TYPE) C 
							WHERE	C.value = r.REQUEST_TYPE ) OR @REQUEST_TYPE IS NULL )
				-- Add Emp code Condition
		GROUP	BY
				R.DOC_NO,
				R.COMPANY,
				
				R.EMP_CODE,
				R.EMP_TITLE,
				R.EMP_NAME,
				R.EMP_LASTNAME,
				R.REQUEST_DATE,
				R.REQUEST_TYPE
	
				
	)	T
	SET @TOTAL_ITEM = @@ROWCOUNT


	IF(@sortColumnName IS NULL )
	BEGIN
		SET @sortColumnName ='0';
	END
	IF(@orderType IS NULL )
	BEGIN
		SET @orderType ='asc';
	END

	DECLARE @FROM	INT , @TO	INT

	SET @FROM	= (@pageSize * (@pageNum-1)) + 1;
	SET @TO		= @pageSize * (@pageNum);						

	--begin modify by thanapon: fixed paging and sorting
	SELECT	d.ROWNUMBER,
			d.DOC_NO,
			d.COMPANY,
			d.REQUEST_TYPE, 
			d.REQUEST_TYPE_DESC, 
			dbo.fn_dateFAS(LAST_APPRV_DATE) AS LAST_APPRV_DATE,
			DIFF_DATE,
			REQUESTOR,
			CURRENT_STATUS,
			dbo.fn_dateFAS(REQUEST_DATE) AS REQUEST_DATE

	FROM	#TB_T_RESULT d
	
	WHERE	d.ROWNUMBER >= @FROM and 
			d.ROWNUMBER <= @TO
	ORDER	BY 
			d.ROWNUMBER
	
	IF OBJECT_ID('tempdb..#TB_T_RESULT') IS NOT NULL 
		DROP TABLE #TB_T_RESULT 
	IF OBJECT_ID('tempdb..#TB_T_SCOPE') IS NOT NULL 
		DROP TABLE #TB_T_SCOPE 

END
GO
