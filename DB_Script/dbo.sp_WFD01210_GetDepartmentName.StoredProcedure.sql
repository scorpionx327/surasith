DROP PROCEDURE [dbo].[sp_WFD01210_GetDepartmentName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun
-- Create date: 05/03/2017
-- Description:	Get All orgrnization order by
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetDepartmentName]
@pDivName	VARCHAR(45) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	DEPT_NAME
	FROM	TB_M_ORGANIZATION
	WHERE	DEPT_NAME IS NOT NULL AND 
			( DIV_NAME = @pDivName OR @pDivName IS NULL )
	GROUP	BY
			DEPT_NAME
	ORDER	BY
			DEPT_NAME
END




GO
