DROP PROCEDURE [dbo].[sp_WFD01020_InsertSystemMaster]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jatuporn Sornsiriong
-- Create date: 14/03/2017
-- Description:	Insert Sys Master
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01020_InsertSystemMaster]
	 @CATEGORY		VARCHAR(40)
	,@SUB_CATEGORY	VARCHAR(40)
	,@CODE			VARCHAR(40)
	,@VALUE			VARCHAR(400)
	,@REMARKS		VARCHAR(200)
	,@ACTIVE_FLAG	CHAR(1)	
	,@CREATE_BY		VARCHAR(8)	
AS
BEGIN

INSERT INTO [dbo].[TB_M_SYSTEM]
           ([CATEGORY]
           ,[SUB_CATEGORY]
           ,[CODE]
           ,[VALUE]
           ,[REMARKS]
           ,[ACTIVE_FLAG]
           ,[CREATE_DATE]
           ,[CREATE_BY]
           ,[UPDATE_DATE]
           ,[UPDATE_BY])
     VALUES
           (@CATEGORY
           ,@SUB_CATEGORY
           ,@CODE
           ,@VALUE
           ,@REMARKS
           ,@ACTIVE_FLAG
           ,GETDATE()
           ,@CREATE_BY
           ,GETDATE()
           ,@CREATE_BY)

END





GO
