DROP PROCEDURE [dbo].[sp_WFD01270_UpdateRequestwhenEmployeeInfoChanged]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_UpdateRequestwhenEmployeeInfoChanged]
AS
BEGIN
	
	IF NOT EXISTS(SELECT 1 FROM TB_T_EMPLOYEE_CHANGE) 
	BEGIN
		RETURN
	END

	-------------------------------------------------------------------------------------------------
	-- Update Employee Info (Name)
	UPDATE	A
	SET		A.EMP_TITLE		= E.EMP_TITLE,
			A.EMP_NAME		= E.EMP_NAME,
			A.EMP_LASTNAME	= E.EMP_LASTNAME,
			A.UPDATE_DATE	= GETDATE(),
			A.UPDATE_BY		= 'Rename'
	FROM	TB_R_REQUEST_H H
	INNER	JOIN
			TB_R_REQUEST_APPR A
	ON		H.DOC_NO = A.DOC_NO
	INNER	JOIN
			TB_T_EMPLOYEE_CHANGE E
	ON		A.EMP_CODE = E.EMP_CODE
	WHERE	H.[STATUS] NOT IN ('60','70','75','90') AND -- Finish Complete, Reject
			A.APPR_STATUS IN ('W','P') AND
			E.ACTIVEFLAG = 'Y' AND E.INFO_CHANGED = 'Y'

	-------------------------------------------------------------------------------------------------
	-- Update Employee Info (Name)

	UPDATE	RA
	SET		RA.EMP_TITLE	= E.EMP_TITLE,
			RA.EMP_NAME		= E.EMP_NAME,
			RA.EMP_LASTNAME	= E.EMP_LASTNAME
	FROM	TB_R_REQUEST_H H
	INNER	JOIN
			TB_R_REQUEST_APPR A
	ON		H.DOC_NO = A.DOC_NO
	INNER	JOIN
			TB_H_REQUEST_APPR RA
	ON		A.DOC_NO	= RA.DOC_NO AND
			A.INDX		= RA.INDX
	INNER	JOIN
			TB_T_EMPLOYEE_CHANGE E
	ON		A.EMP_CODE	= E.EMP_CODE
	WHERE	H.[STATUS]		NOT IN ('60','70','75','90') AND -- Finish Complete, Reject
			A.APPR_STATUS	IN ('W','P') AND
			E.ACTIVEFLAG	= 'Y' AND E.INFO_CHANGED = 'Y'
	-------------------------------------------------------------------------------------------------
	-- Delete From List when User resign or Org.Changed.
	
	DELETE	RA
	FROM	TB_R_REQUEST_H H
	INNER	JOIN
			TB_R_REQUEST_APPR A
	ON		H.DOC_NO = A.DOC_NO
	INNER	JOIN
			TB_H_REQUEST_APPR RA
	ON		A.DOC_NO	= RA.DOC_NO AND
			A.INDX		= RA.INDX
	INNER	JOIN
			TB_T_EMPLOYEE_CHANGE E
	ON		RA.EMP_CODE = E.EMP_CODE
	WHERE	H.[STATUS]		NOT IN ('60','70','75','90') AND -- Finish Complete, Reject
			A.APPR_STATUS	IN ('W','P') AND
			( E.ACTIVEFLAG	= 'N' OR E.ORG_CHANGED = 'Y')
	-------------------------------------------------------------------------------------------------
	
	DELETE
	FROM	TB_T_EMPLOYEE_CHANGE

END
GO
