DROP PROCEDURE [dbo].[sp_WFD01170_RejectRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01170_RejectRequest]
@DOC_NO				T_DOC_NO,
@LOGIN_USER			T_SYS_USER,
@IS_DELEGATE		VARCHAR(1), -- Y => Delegate User take action
@IS_HIGHER			VARCHAR(1), -- Y => Manager take action instead
@IS_INSTEAD			VARCHAR(1), -- Y => ACR User take action instead approve user		
-- Comment
@COMMENT			VARCHAR(200)		= NULL,			
@ATTACH_FILE		VARCHAR(200)		= NULL
AS
BEGIN
	-- Get current data
	DECLARE	@WaitingIndx	INT, 
			@RequestType	VARCHAR(1),  @cDocStatus VARCHAR(2),
			@RejectTo INT, 
			@ApproveUser T_SYS_USER
	
	SELECT	@RequestType = REQUEST_TYPE, @cDocStatus = [STATUS]
	FROM	TB_R_REQUEST_H 
	WHERE	DOC_NO = @DOC_NO

	SELECT	@WaitingIndx	= INDX, 
			@ApproveUser	= EMP_CODE, 
			@RejectTo		= REJECT_TO
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND
			[APPR_STATUS]	= 'W'
	IF @@ROWCOUNT = 0
	RETURN


	EXEC sp_WFD01170_InsertComment @DOC_NO, @WaitingIndx, @ApproveUser, @COMMENT, @ATTACH_FILE, @LOGIN_USER, @RequestType, 'R'
	
	-----------------------------------------------------------------------------------
	UPDATE	TB_R_REQUEST_APPR -- Should be update Actual Role
	SET		APPR_STATUS		= 'W',
			APPR_DATE		= NULL,
			UPDATE_DATE		= GETDATE()
	WHERE	DOC_NO			= @DOC_NO AND
			INDX			= @RejectTo

	UPDATE	TB_R_REQUEST_APPR 
	SET		APPR_STATUS		= 'P',
			APPR_DATE		= NULL,
			UPDATE_DATE		= GETDATE()
	WHERE	DOC_NO			= @DOC_NO AND
			INDX			> @RejectTo

	-----------------------------------------------------------------------------------
	-- Update Document Status
	DECLARE @iCurrent		INT,
			@iGenFile		INT, -- Index for change status
			@iFinishFlow	INT,	 -- Index for change status
			@iStart		INT
	SET		@iCurrent = @RejectTo

	SELECT	@iGenFile	= MAX(INDX) -- should 1 record
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO		= @DOC_NO AND 
			GEN_FILE	= 'Y'

	SELECT	@iStart	= MIN(INDX) -- should 1 record
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO		= @DOC_NO


	SELECT	@iFinishFlow	= MAX(INDX) -- should 1 record
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND 
			FINISH_FLOW		= 'Y'

	UPDATE	TB_R_REQUEST_H
	SET		[STATUS]	= CASE	WHEN [STATUS] >= '30' AND @iCurrent >= @iGenFile THEN '20' -- ACM reject to ACR
								WHEN [STATUS] >= '20' AND @iCurrent >= @iFinishFlow THEN '10' -- ACR reject to Manager
								WHEN [STATUS] <= '20'  AND @iStart < @WaitingIndx THEN '10' -- Manager Reject to Requesetor (not change)
								WHEN [STATUS] = '10'  AND @iStart = @WaitingIndx THEN '90'  --Requesttor Reject
								WHEN [STATUS] = '80'  THEN '90'  -- Requestore Reject
								ELSE [STATUS] END, -- Manager Reject to Requesetor (not change)
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= @LOGIN_USER
	WHERE	DOC_NO		= @DOC_NO


	IF @iStart = @WaitingIndx 
	BEGIN
		UPDATE	TB_R_REQUEST_APPR 
		SET		APPR_STATUS		= 'R',
				APPR_DATE		= NULL,
				UPDATE_DATE		= GETDATE()
		WHERE	DOC_NO			= @DOC_NO AND
				INDX			= @iStart
	END

END
GO
