DROP PROCEDURE [dbo].[sp_WFD02640_ValidateSubmitStockTaking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
User by : Uten S.
Create Date : 2017-04-10 
*/

CREATE PROCEDURE [dbo].[sp_WFD02640_ValidateSubmitStockTaking]
(	
	    @DocNo			T_DOC_NO,
		@GUID			T_GUID
)
AS
BEGIN
 DECLARE @Cnt INT,  @CCList VARCHAR(MAX)

 SET @Cnt=0;
  IF EXISTS( SELECT DOC_NO
					 FROM TB_R_REQUEST_STOCK S
					 WHERE S.DOC_NO =@DocNo )
  BEGIN
			-- Incase Resumit
			 
             SELECT	@Cnt=COUNT(1) FROM TB_T_REQUEST_STOCK T
			 -- 2017-06-08 Surasith
			 -- Add Complete Flag and Selected Condition for support when system should check loss condition
			 WHERE  DOC_NO = @DocNo AND COMPLETE_FLAG ='N' AND SELECTED='Y'
			 AND ( COMMENT IS NULL OR ATTACHMENT IS NULL)

			 -- Add Complete Flag and Selected Condition for support when system should check loss condition
			SELECT	@CCList = COALESCE(@CCList + ',', '') + COST_CODE 
			FROM	TB_T_REQUEST_STOCK T
			WHERE	DOC_NO = @DocNo AND COMPLETE_FLAG ='N' AND SELECTED='Y'
					AND ( COMMENT IS NULL OR ATTACHMENT IS NULL)
			GROUP	BY COST_CODE

			
  END
  ELSE
  BEGIN	    
			 --Incase New   
			SELECT @Cnt=COUNT(1) FROM TB_T_REQUEST_STOCK T
			WHERE [GUID] = @GUID AND COMPLETE_FLAG ='N' AND SELECTED='Y'
			AND ( COMMENT IS NULL OR ATTACHMENT IS NULL)

			SELECT	@CCList = COALESCE(@CCList + ',', '') + COST_CODE 
			FROM		TB_T_REQUEST_STOCK T
			WHERE [GUID] = @GUID AND COMPLETE_FLAG ='N' AND SELECTED='Y'
			AND ( COMMENT IS NULL OR ATTACHMENT IS NULL)
			GROUP	BY COST_CODE

  END


  IF @Cnt = 0
  BEGIN
	SELECT '' AS COST_CODE --  N is no request
	RETURN 
  END

  SELECT	@CCList AS COST_CODE
END
GO
