DROP PROCEDURE [dbo].[sp_Common_GetFileDownloadFromAppId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_GetFileDownloadFromAppId]
	-- Add the parameters for the stored procedure here
	@APP_ID	numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		*
	FROM TB_R_FILE_DOWNLOAD
	WHERE APP_ID=@APP_ID

END



GO
