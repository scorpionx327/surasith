DROP PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_AEC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_AEC]
@User	VARCHAR(8)
AS
BEGIN
	DECLARE @ID int,	
		@TO varchar(1000),
		@CC varchar(1000),
		@BCC varchar(1000),
		@TITLE varchar(255),
		@MESSAGE nvarchar(max),
		@FOOTER varchar(250),
		@TYPE varchar(1),
		@START_PERIOD datetime,
		@END_PERIOD datetime,
		@SEND_FLAG varchar(1),
		@RESULT_FLAG varchar(1),
		@REF_FUNC varchar(8),
		@REF_DOC_NO T_DOC_NO

	--DECLARE @MyTable  table(REQUEST_TYPE varchar(400),
	--				TDEN int,
	--				TMT int,
	--				STM int,
	--				Total int
	--			); 


		SET @MESSAGE = 'Dear AEC Manage <br>';
		SET @MESSAGE = CONCAT(@MESSAGE,('<style>
				table {
				  font-family: arial, sans-serif;
				  border-collapse: collapse;
				  width: 100%;
				}

				td, th {
				  border: 1px solid #dddddd;
				  text-align: left;
				  padding: 8px;
				}

				tr:nth-child(even) {
				  background-color: #dddddd;
				}
				</style>

				<table>
				  <tr>
					<th>REQUEST_TYPE</th>
					<th>TDEN</th>
					<th>TMT</th>
					<th>STM</th>
					<th>Total</th>
				  </tr>'));

	DECLARE @StartDate datetime;
	DECLARE @EndDate datetime;
	select @StartDate = DATEADD(day, -1, [value]),@EndDate = [value]
	From TB_M_SYSTEM																
	Where CATEGORY = 'NOTIFICATION'																
		 AND SUB_CATEGORY = 'SUMMARY_AEC'															
		 AND CODE = 'NEXTTIME'		

	DECLARE @VALUE varchar(400),@CODE varchar(40);
	DECLARE cursor_results CURSOR FOR 
	select m.[VALUE],m.CODE 
	from TB_M_SYSTEM m 
	where m.CATEGORY = 'SYSTEM_CONFIG' 
		and m.SUB_CATEGORY = 'REQUEST_FLOW_TYPE'

	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @VALUE,@CODE
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		DECLARE @TDEM int
		DECLARE @TMT int
		DECLARE @STM int
		DECLARE @Total int

			SET @TDEM = (select count(ra.DOC_NO)																								
			from TB_R_REQUEST_H	rh
				left outer join TB_M_EMPLOYEE e on rh.EMP_CODE = e.EMP_CODE
				left outer join TB_R_REQUEST_APPR ra on rh.DOC_NO = ra.DOC_NO
			where e.FAADMIN = 'Y'
				and ra.APPR_ROLE = 'ACR'
				and ra.APPR_DATE between @StartDate and @EndDate
				and rh.REQUEST_TYPE = @CODE
				and rh.COMPANY = 'TDEM');

			SET @TMT = (select count(ra.DOC_NO)																								
			from TB_R_REQUEST_H	rh
				left outer join TB_M_EMPLOYEE e on rh.EMP_CODE = e.EMP_CODE
				left outer join TB_R_REQUEST_APPR ra on rh.DOC_NO = ra.DOC_NO
			where e.FAADMIN = 'Y'
				and ra.APPR_ROLE = 'ACR'
				and ra.APPR_DATE between @StartDate and @EndDate
				and rh.REQUEST_TYPE = @CODE
				and rh.COMPANY = 'TMT');

			SET @STM = (select count(ra.DOC_NO)																								
				from TB_R_REQUEST_H	rh
					left outer join TB_M_EMPLOYEE e on rh.EMP_CODE = e.EMP_CODE
					left outer join TB_R_REQUEST_APPR ra on rh.DOC_NO = ra.DOC_NO
				where e.FAADMIN = 'Y'
					and ra.APPR_ROLE = 'ACR'
					and ra.APPR_DATE between @StartDate and @EndDate
					and rh.REQUEST_TYPE = @CODE
					and rh.COMPANY = 'STM');

			SET @Total = @TDEM + @TMT + @STM;

			SET @MESSAGE = CONCAT(@MESSAGE,'<tr>');
			SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@VALUE,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@TDEM,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@TMT,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@STM,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@Total,'</td>');
			SET @MESSAGE = CONCAT(@MESSAGE,'</tr>');

			--insert into @MyTable select @VALUE,@TDEM,@TMT,@STM,@Total
	FETCH NEXT FROM cursor_results into @VALUE,@CODE
	END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;

	SET @ID = (select isnull(max(ID),0) + 1 from TB_R_NOTIFICATION);
	SET @MESSAGE = CONCAT(@MESSAGE,'</table>');

	--select * from @MyTable
	SET @FOOTER = 'Best Regards,<br>System';

	SET @START_PERIOD = @StartDate;
	SET @END_PERIOD = @EndDate;

	SET @SEND_FLAG = 'N';
	SET @RESULT_FLAG = 'N';
	SET @TYPE = 'I';
	SET @REF_FUNC = '';

	DECLARE @AEC_MGR_EMP_ID varchar(50),@EMAIL varchar(50),@COMPANY T_COMPANY;
	DECLARE cursor_results CURSOR FOR 
	select e.AEC_MGR_EMP_ID,e.EMAIL,e.COMPANY
	from TB_M_EMPLOYEE e
	where e.FAADMIN = 'Y'
	group by e.AEC_MGR_EMP_ID,e.EMAIL,e.COMPANY


	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @AEC_MGR_EMP_ID,@EMAIL,@COMPANY
	WHILE @@FETCH_STATUS = 0
	BEGIN 

	SET @TO = @EMAIL;
	SET @TITLE = CONCAT('TFAST : ',@COMPANY,' Summary Daily Requests');
	INSERT INTO TB_R_NOTIFICATION
           (ID,[TO],CC,BCC,TITLE,[MESSAGE],FOOTER,[TYPE]
           ,START_PERIOD,END_PERIOD,SEND_FLAG,RESULT_FLAG,REF_FUNC,REF_DOC_NO
			,CREATE_DATE,CREATE_BY,UPDATE_DATE,UPDATE_BY)
     VALUES
           (@ID
		   ,@TO
           ,@CC
           ,@BCC
           ,@TITLE
           ,@MESSAGE
           ,@FOOTER
           ,@TYPE
           ,@START_PERIOD
           ,@END_PERIOD
           ,@SEND_FLAG
           ,@RESULT_FLAG
           ,@REF_FUNC
           ,@REF_DOC_NO
           ,getdate()
           ,@User
           ,getdate()
           ,@User)

	--select @TO,@CC,@BCC,@TITLE,@MESSAGE,@FOOTER,@TYPE,@START_PERIOD,@END_PERIOD,@SEND_FLAG,@RESULT_FLAG,@REF_FUNC,@REF_DOC_NO,getdate(),@User,getdate(),@User

	FETCH NEXT FROM cursor_results into @AEC_MGR_EMP_ID,@EMAIL,@COMPANY
	END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'SUMMARY_AEC'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	SET @dNextTime = DATEADD(DAY, 1, @dNextTime)

	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= 'System'
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
	--select @MESSAGE
END
GO
