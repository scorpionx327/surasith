DROP PROCEDURE [dbo].[sp_BFD01260_UpdateFixedAssets]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_BFD01260_UpdateFixedAssets] 
 	@AppID		INT,
	@UserID		T_SYS_USER
AS
BEGIN

	-- Update Existing Record
	UPDATE	M
	SET		ASSET_NAME				= S.ASSET_NAME,
			ADTL_ASSET_NAME			= S.ADTL_ASSET_NAME,
			ASSET_CLASS				= S.ASSET_CLASS,
			ASSET_GROUP				= S.ASSET_GROUP,
			SERIAL_NO				= S.SERIAL_NO,
	--		TAG_NO					= S.TAG_NO,  << TFAST
			BOI_NO					= S.BOI_NO,
			MACHINE_LICENSE			= S.MACHINE_LICENSE,
			MINOR_CATEGORY			= S.MINOR_CATEGORY,
			DATE_IN_SERVICE			= CONVERT(DATE,REPLACE(S.DATE_IN_SERVICE,'.','/'),103),
	--		PLATE_TYPE				= S.PLATE_TYPE,  << TFAST
	--		BARCODE_SIZE			= S.BARCODE_SIZE,  << TFAST
	--		STOCKTAKE_FREQUENCY		= S.STOCKTAKE_FREQUENCY,  << TFAST
	--		EMP_CODE				= S.EMP_CODE,  << TFAST
	--		EMP_NAME				= S.EMP_NAME,  << TFAST
	--		EMP_REMARK				= S.EMP_REMARK,  << TFAST
	--		LOCATION_REMARK			= S.LOCATION_REMARK,  << TFAST
			RETIREMENT_DATE			= CONVERT(DATE,REPLACE(S.RETIREMENT_DATE,'.','/'),103),
	--		TRANSFER_DATE			= S.TRANSFER_DATE,  << TFAST
	--		ENTRY_PERIOD			= S.ENTRY_PERIOD,  << TFAST
	--		BARCODE					= S.BARCODE,  << TFAST
	--		BOI_FLAG				= S.BOI_FLAG,  << TFAST
	--		MAP_STATUS				= S.MAP_STATUS,  << TFAST
	--		PHOTO_STATUS			= S.PHOTO_STATUS,  << TFAST
	--		PRINT_STATUS			= S.PRINT_STATUS,  << TFAST
	--		STATUS					= S.STATUS,  << TFAST
	--		PROCESS_STATUS			= S.PROCESS_STATUS,  << TFAST
	--		PRINT_COUNT				= S.PRINT_COUNT,  << TFAST
	--		TAG_PHOTO				= S.TAG_PHOTO,  << TFAST
	--		INHOUSE_FLAG			= S.INHOUSE_FLAG,  << TFAST
			AC_DETERMINE			= S.AC_DETERMINE,
			INVEN_NO				= S.INVEN_NO,
			QUANTITY				= S.QUANTITY,
			BASE_UOM				= S.BASE_UOM,
			LAST_INVEN_DATE			= CONVERT(DATE,REPLACE(S.LAST_INVEN_DATE,'.','/'),103),
			INVEN_INDICATOR			= S.INVEN_INDICATOR,
			INVEN_NOTE				= S.INVEN_NOTE,
			WBS_PROJECT				= S.WBS_PROJECT,
			WBS_BUDGET				= S.WBS_BUDGET,
			COST_CODE				= S.COST_CODE,
			RESP_COST_CODE			= S.RESP_COST_CODE,
			PLANT					= S.PLANT,
			[LOCATION]				= S.[LOCATION],
			ROOM					= S.ROOM,
			LICENSE_PLATE			= S.LICENSE_PLATE,
			ASSET_STATUS			= S.ASSET_STATUS,
			FINAL_ASSET_CLASS		= S.FINAL_ASSET_CLASS,
			ASSET_SUPER_NO			= S.ASSET_SUPER_NO,
			INVEST_REASON			= S.INVEST_REASON,
			VENDOR_CODE				= S.VENDOR_CODE,
			VENDOR_NAME				= S.VENDOR_NAME,
			MANUFACT_ASSET			= S.MANUFACT_ASSET,
			ORG_ASSET				= S.ORG_ASSET,
			ORG_ASSET_SUB			= S.ORG_ASSET_SUB,
			LEASE_AGREE_DATE		= CONVERT(DATE,REPLACE(S.LEASE_AGREE_DATE,'.','/'),103),
			LEASE_START_DATE		= CONVERT(DATE,REPLACE(S.LEASE_START_DATE,'.','/'),103),
			LEASE_LENGTH_YEAR		= S.LEASE_LENGTH_YEAR,
			LEASE_LENGTH_PERD		= S.LEASE_LENGTH_PERD,
			LEASE_TYPE				= S.LEASE_TYPE,
			LEASE_SUPPLEMENT		= S.LEASE_SUPPLEMENT,
			LEASE_NUM_PAYMENT		= S.LEASE_NUM_PAYMENT,
			LEASE_PAY_CYCLE			= S.LEASE_PAY_CYCLE,
			LEASE_ADV_PAYMENT		= S.LEASE_ADV_PAYMENT,
			LEASE_PAYMENT			= S.LEASE_PAYMENT,
			LEASE_ANU_INT_RATE		= S.LEASE_ANU_INT_RATE,
			DPRE_AREA_01			= S.DPRE_AREA_01,
			DPRE_KEY_01				= S.DPRE_KEY_01,
			USEFUL_LIFE_YEAR_01		= S.PLAN_LIFE_YEAR_01,
			USEFUL_LIFE_PERD_01		= S.PLAN_LIFE_PERD_01,
			DPRE_START_DATE_01		= CONVERT(DATE,REPLACE(S.DPRE_START_DATE_01,'.','/'),103),
			SCRAP_VALUE_01			= S.SCRAP_VALUE_01,
			SCRAP_VALUE_PERCENT_APC_01	= S.SCRAP_VALUE_PERCENT_APC_01,
			CUMU_ACQU_PRDCOST_01	= S.CUMU_ACQU_PRDCOST_01,
			ACCU_DPRE_01			= S.ACCU_DPRE_01,
			DPRE_CUR_YEAR_01		= S.DPRE_CUR_YEAR_01,
			NET_VALUE_01			= S.NET_VALUE_01,
			CAPACITY_UOP_01			= S.CAPACITY_UOP_01,
			LTD_PROD_UOP_01			= S.LTD_PROD_UOP_01,
			DEACTIVATE_15			= S.DEACTIVATE_15,
			DPRE_AREA_15			= S.DPRE_AREA_15,
			DPRE_KEY_15				= S.DPRE_KEY_15,
			USEFUL_LIFE_YEAR_15		= S.PLAN_LIFE_YEAR_15,
			USEFUL_LIFE_PERD_15		= S.PLAN_LIFE_PERD_15,
			DPRE_START_DATE_15		= CONVERT(DATE,REPLACE(S.DPRE_START_DATE_15,'.','/'),103),
			SCRAP_VALUE_15			= S.SCRAP_VALUE_15,
			SCRAP_VALUE_PERCENT_APC_15	= S.SCRAP_VALUE_PERCENT_APC_15,
			CUMU_ACQU_PRDCOST_15	= S.CUMU_ACQU_PRDCOST_15,
			ACCU_DPRE_15			= S.ACCU_DPRE_15,
			DPRE_CUR_YEAR_15		= S.DPRE_CUR_YEAR_15,
			NET_VALUE_15			= S.NET_VALUE_15,
			CAPACITY_UOP_15			= S.CAPACITY_UOP_15,
			LTD_PROD_UOP_15			= S.LTD_PROD_UOP_15,
			DPRE_AREA_31			= S.DPRE_AREA_31,
			DPRE_KEY_31				= S.DPRE_KEY_31,
			USEFUL_LIFE_YEAR_31		= S.PLAN_LIFE_YEAR_31,
			USEFUL_LIFE_PERD_31		= S.PLAN_LIFE_PERD_31,
			DPRE_START_DATE_31		= CONVERT(DATE,REPLACE(S.DPRE_START_DATE_31,'.','/'),103),
			SCRAP_VALUE_31			= S.SCRAP_VALUE_31,
			SCRAP_VALUE_PERCENT_APC_31	= S.SCRAP_VALUE_PERCENT_APC_31,
			CUMU_ACQU_PRDCOST_31	= S.CUMU_ACQU_PRDCOST_31,
			ACCU_DPRE_31			= S.ACCU_DPRE_31,
			DPRE_CUR_YEAR_31		= S.DPRE_CUR_YEAR_31,
			NET_VALUE_31			= S.NET_VALUE_31,
			CAPACITY_UOP_31			= S.CAPACITY_UOP_31,
			LTD_PROD_UOP_31			= S.LTD_PROD_UOP_31,
			DPRE_AREA_41			= S.DPRE_AREA_41,
			DPRE_KEY_41				= S.DPRE_KEY_41,
			USEFUL_LIFE_YEAR_41		= S.PLAN_LIFE_YEAR_41,
			USEFUL_LIFE_PERD_41		= S.PLAN_LIFE_PERD_41,
			DPRE_START_DATE_41		= CONVERT(DATE,REPLACE(S.DPRE_START_DATE_41,'.','/'),103),
			SCRAP_VALUE_41			= S.SCRAP_VALUE_41,
			SCRAP_VALUE_PERCENT_APC_41	= S.SCRAP_VALUE_PERCENT_APC_41,
			CUMU_ACQU_PRDCOST_41	= S.CUMU_ACQU_PRDCOST_41,
			ACCU_DPRE_41			= S.ACCU_DPRE_41,
			DPRE_CUR_YEAR_41		= S.DPRE_CUR_YEAR_41,
			NET_VALUE_41			= S.NET_VALUE_41,
			CAPACITY_UOP_41			= S.CAPACITY_UOP_41,
			LTD_PROD_UOP_41			= S.LTD_PROD_UOP_41,
			DPRE_AREA_81			= S.DPRE_AREA_81,
			DPRE_KEY_81				= S.DPRE_KEY_81,
			USEFUL_LIFE_YEAR_81		= S.PLAN_LIFE_YEAR_81,
			USEFUL_LIFE_PERD_81		= S.PLAN_LIFE_PERD_81,
			DPRE_START_DATE_81		= CONVERT(DATE,REPLACE(S.DPRE_START_DATE_81,'.','/'),103),
			SCRAP_VALUE_81			= S.SCRAP_VALUE_81,
			SCRAP_VALUE_PERCENT_APC_81	= S.SCRAP_VALUE_PERCENT_APC_81,
			CUMU_ACQU_PRDCOST_81	= S.CUMU_ACQU_PRDCOST_81,
			ACCU_DPRE_81			= S.ACCU_DPRE_81,
			DPRE_CUR_YEAR_81		= S.DPRE_CUR_YEAR_81,
			NET_VALUE_81			= S.NET_VALUE_81,
			CAPACITY_UOP_81			= S.CAPACITY_UOP_81,
			LTD_PROD_UOP_81			= S.LTD_PROD_UOP_81,
			DPRE_AREA_91			= S.DPRE_AREA_91,
			DPRE_KEY_91				= S.DPRE_KEY_91,
			USEFUL_LIFE_YEAR_91		= S.PLAN_LIFE_YEAR_91,
			USEFUL_LIFE_PERD_91		= S.PLAN_LIFE_PERD_91,
			DPRE_START_DATE_91		= CONVERT(DATE,REPLACE(S.DPRE_START_DATE_91,'.','/'),103),
			SCRAP_VALUE_91			= S.SCRAP_VALUE_91,
			SCRAP_VALUE_PERCENT_APC_91	= S.SCRAP_VALUE_PERCENT_APC_91,
			CUMU_ACQU_PRDCOST_91	= S.CUMU_ACQU_PRDCOST_91,
			ACCU_DPRE_91			= S.ACCU_DPRE_91,
			DPRE_CUR_YEAR_91		= S.DPRE_CUR_YEAR_91,
			NET_VALUE_91			= S.NET_VALUE_91,
			CAPACITY_UOP_91			= S.CAPACITY_UOP_91,
			LTD_PROD_UOP_91			= S.LTD_PROD_UOP_91,
			UPDATE_DATE				= GETDATE(),
			UPDATE_BY				= @UserID

	FROM	TB_M_ASSETS_H M
	INNER	JOIN
			TB_S_ASSETS_H S
	ON		M.COMPANY	= S.COMPANY AND
			M.ASSET_NO	= S.ASSET_NO AND
			M.ASSET_SUB = S.ASSET_SUB



	
END
GO
