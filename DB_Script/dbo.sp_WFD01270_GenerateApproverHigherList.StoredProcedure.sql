DROP PROCEDURE [dbo].[sp_WFD01270_GenerateApproverHigherList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_GenerateApproverHigherList]
@Company	T_COMPANY,
@DOC_NO		T_DOC_NO,
@Role	VARCHAR(3), -- Current Role
@Seq	INT
AS
BEGIN
		DECLARE @RoleMappingRoleC VARCHAR(40), @RolePrioritySC VARCHAR(40)
		SET @RoleMappingRoleC	= 'ROLE_MAPPING'
		SET @RolePrioritySC		= 'ROLE_PRIORITY'

		-- Get Current Role Index from Master
		DECLARE @Length INT, @PreLength INT
		DECLARE @PriorityIndex INT, @HigherRole VARCHAR(3)
		SET @PriorityIndex = CONVERT(INT,dbo.fn_GetSystemMaster(@RoleMappingRoleC,@RolePrioritySC,@Role))

		-- Test
		DECLARE	@TB_T_CHECKED_ROLE AS TABLE
		(	
			APPR_ROLE	VARCHAR(3) 
		)
		DECLARE @LoopCnt INT = 0
		WHILE(EXISTS(	SELECT	1 
						FROM	TB_M_SYSTEM  M
						LEFT	JOIN
								@TB_T_CHECKED_ROLE T
						ON		M.CODE			= T.APPR_ROLE
						WHERE	T.APPR_ROLE IS NULL AND
								CATEGORY		= @RoleMappingRoleC AND 
								SUB_CATEGORY	= @RolePrioritySC AND 
								[VALUE]			>= @PriorityIndex AND
								ACTIVE_FLAG		= 'Y' ))
		BEGIN
			
			 

			SET @LoopCnt = @LoopCnt + 1
			IF @LoopCnt > 20
				BREAK

			SELECT	TOP 1
					@HigherRole		= CODE,
					@PriorityIndex	= CONVERT(INT, [VALUE])
			FROM	TB_M_SYSTEM M
			LEFT	JOIN
					@TB_T_CHECKED_ROLE T
			ON		M.CODE			= T.APPR_ROLE
			WHERE	T.APPR_ROLE IS NULL AND
					CATEGORY		= @RoleMappingRoleC AND 
					SUB_CATEGORY	= @RolePrioritySC AND 
					[VALUE]			>= @PriorityIndex AND
					ACTIVE_FLAG		= 'Y'
			ORDER	BY [VALUE]


			IF @HigherRole IS NULL
				BREAK

			IF @PriorityIndex = NULL
				BREAK 
			
			INSERT INTO @TB_T_CHECKED_ROLE(APPR_ROLE) VALUES(@HigherRole)
			
			-- Get organization level
			DECLARE @DirectOrgLevel VARCHAR(40),@InDirectOrgLevel VARCHAR(40)
			SET @DirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@Role)
			SET @InDirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_INDIRECT'),@Role)


			DECLARE @GCnt INT = 0, @Cnt INT = 0
			-- @Role is Main Role from Approver, @Length is length of higer role
			EXEC sp_WFD01270_GenerateApproverRoleList @DOC_NO, @Seq, @Role, @HigherRole, @DirectOrgLevel, 'M', @Cnt OUT
			SET @GCnt = @Cnt

			EXEC sp_WFD01270_GenerateApproverRoleList @DOC_NO, @Seq, @Role, @HigherRole, @InDirectOrgLevel, 'R', @Cnt OUT
			SET @GCnt = @GCnt + @Cnt

			-- Stop when system can get the approver, Incase no get approver from next approver list
			IF @GCnt > 0
				BREAK ;
		END

		

		

		

END
GO
