DROP PROCEDURE [dbo].[sp_WFD01270_GenerateApproverHigherRoleSpecialList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_WFD01270_GenerateApproverHigherRoleSpecialList]
@Company	T_COMPANY,
@DocNo	T_DOC_NO,
@Seq	INT,
@Role	VARCHAR(3),
@RowCount	INT OUT,
@Div	VARCHAR(6)
AS
BEGIN
	
	DECLARE @HigherRoleList VARCHAR(400)
	SET @HigherRoleList = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPROVE_HIGHER_MATCH',@Role)

	IF ISNULL(@HigherRoleList,'') = ''
	BEGIN
		--PRINT 'No @HigherRoleList'
		SET @RowCount = 0 
		RETURN
	END

	DECLARE @TB_HIGHER_LIST AS TABLE
	(
		INDX INT, [ROLE] VARCHAR(3), USED VARCHAR(1),SEQ INT
	)

	INSERT INTO	@TB_HIGHER_LIST
	SELECT	position, [value], 'N'  ,SEQ
	From dbo.FN_SPLIT(@HigherRoleList,'|') M
	Inner Join (select CODE,CONVERT(INT,REMARKS) AS SEQ  from TB_M_SYSTEM  WHERE SUB_CATEGORY = 'ROLE_CODE' ) R
	on M.[value] = R.CODE
	ORDER BY SEQ
	
	DECLARE @TargetRole VARCHAR(3), @Length INT
	
	DECLARE @EffCnt INT, @DirectOrgLevel VARCHAR(40)

	WHILE EXISTS(SELECT 1 FROM @TB_HIGHER_LIST WHERE USED = 'N' )
	BEGIN
		SELECT TOP 1 @TargetRole = [ROLE] FROM @TB_HIGHER_LIST  WHERE USED = 'N' ORDER BY INDX
		UPDATE @TB_HIGHER_LIST SET USED = 'Y' WHERE [ROLE] = @TargetRole

		PRINT @TargetRole
		
		SET @DirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@Role)

		--EXEC sp_WFD01270_GenerateApproverRoleList @DocNo, @Seq, @Role, @TargetRole, @Length,  'H', @EffCnt OUT
		EXEC sp_WFD01270_GenerateApproverRoleSpecialList  @Company, @DocNo, @Seq, @Role, @TargetRole, @Length, 'H', @EffCnt OUT , @Div
		
		IF @@ERROR > 0 OR @EffCnt > 0
		BEGIN
			SET @RowCount = @EffCnt
			BREAK
		END

		SET @RowCount = @EffCnt
	END

END
GO
