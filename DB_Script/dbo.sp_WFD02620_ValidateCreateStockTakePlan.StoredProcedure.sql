DROP PROCEDURE [dbo].[sp_WFD02620_ValidateCreateStockTakePlan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 08-03-2017
-- Description:	Valdiate before create stock take plane
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_ValidateCreateStockTakePlan]
	-- Add the parameters for the stored procedure here
	@YEAR					varchar(4)
	, @ROUND				varchar(2)
	, @ASSET_LOCATION		varchar(1)
AS
	DECLARE @MESSAGE NVARCHAR(250);
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- ///////////////////////////////////////////////////////////////////////		
	-- Validate add data

	--In case check Duplicate, System will check data in TB_R_STOCK_TAKE_H by follow key:								
	--	- Stock Taking Round(Year)							
	--	- Stock Taking Round(Round)							
	--	- Asset Location							

	IF(SELECT COUNT(1) FROM TB_R_STOCK_TAKE_H WHERE ASSET_LOCATION=@ASSET_LOCATION and YEAR=@YEAR and ROUND=@ROUND) > 0
	BEGIN


		SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MSTD0010AERR'

		SET @MESSAGE = '[CUSTOM]' + 'MSTD0010AERR|' + REPLACE(
							REPLACE(
								REPLACE(@MESSAGE, '{0}', 'Year/Round')
								, '{1}', @YEAR + '/' + @ROUND + ' and Asset Location = ' 
									+ (	SELECT TOP 1 VALUE FROM TB_M_SYSTEM 
										WHERE CATEGORY='COMMON' and SUB_CATEGORY = 'ASSET_LOCATION' and CODE=@ASSET_LOCATION ))
							, '{2}', 'TB_R_STOCK_TAKE_H');
			
		RAISERROR (@MESSAGE, -- Message text.
				16, -- Severity.
				1 -- State.
				);
		return;

	END


	--System not allow create new plan,
	--Incase old plan not finish
	---> check  by Asset Location (Inhouse /out source)
	ELSE IF(SELECT COUNT(1) FROM TB_R_STOCK_TAKE_H WHERE ASSET_LOCATION=@ASSET_LOCATION and PLAN_STATUS <> 'F') > 0
	BEGIN
		
		SET @MESSAGE = '[CUSTOM]MSTD0000AERR|System not allow create new plan, Incase old plan not finish'
		RAISERROR (@MESSAGE, -- Message text.
				16, -- Severity.
				1 -- State.
				);
		return;

	END

	SELECT CONVERT(BIT, 1) as RESULT

END




GO
