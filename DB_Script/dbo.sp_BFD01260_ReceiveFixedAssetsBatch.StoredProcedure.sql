DROP PROCEDURE [dbo].[sp_BFD01260_ReceiveFixedAssetsBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_BFD01260_ReceiveFixedAssetsBatch] 
 	@AppID		INT,
	@UserID		T_SYS_USER
AS
BEGIN
	
	IF NOT EXISTS(SELECT 1 FROM TB_S_ASSETS_H)
	BEGIN
		
		EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MCOM0020AWRN', 'Receiving Fixed Assets Interface batch',@UserID
		SELECT 2 AS SUCCESS ; -- Warning
		RETURN;
	END

	BEGIN TRY

		BEGIN TRANSACTION T1
		-- Keep New List
		SELECT	COMPANY, ASSET_NO, ASSET_SUB, COST_CODE
		INTO	#TB_T_NEW_ASSET
		FROM	TB_M_ASSETS_H
		WHERE	1 = 2
		
		Print 'Insert new asset to temp'
		INSERT
		INTO	#TB_T_NEW_ASSET(COMPANY, ASSET_NO, ASSET_SUB,COST_CODE)
		SELECT	S.COMPANY, S.ASSET_NO, S.ASSET_SUB, S.COST_CODE
		FROM	TB_S_ASSETS_H S
		LEFT	JOIN
				TB_M_ASSETS_H M
		ON		S.COMPANY	= M.COMPANY AND
				S.ASSET_NO	= M.ASSET_NO AND
				S.ASSET_SUB	= M.ASSET_SUB
		WHERE	M.ASSET_NO IS NULL
	
		-- Update Existing Record
		Print 'Update existing'
		EXEC sp_BFD01260_UpdateFixedAssets @AppID, @UserID

	
		-- Insert New Record
		Print 'Insert new assets'
		EXEC sp_BFD01260_InsertFixedAssets @AppID, @userID

		-- Update Inactive incase not exists.
		Print 'Set Inactive'
		UPDATE	M
		SET		M.[STATUS]	= 'N'
		FROM	TB_M_ASSETS_H M
		LEFT	JOIN
				TB_S_ASSETS_H S
		ON		M.COMPANY	= S.COMPANY AND
				M.ASSET_NO	= S.ASSET_NO AND
				M.ASSET_SUB = S.ASSET_SUB
		WHERE	S.ASSET_NO IS NULL OR (M.RETIREMENT_DATE IS NOT NULL AND M.RETIREMENT_DATE <= GETDATE() AND M.[STATUS] = 'Y')

		Print 'Remove asset location'
		-- Remove from Map when Assets is inactive
		DELETE	L
		FROM	TB_M_ASSETS_LOC L
		INNER	JOIN
				TB_M_ASSETS_H M
		ON		L.COMPANY	= M.COMPANY AND
				L.ASSET_NO	= M.ASSET_NO AND
				M.ASSET_NO	= '0000'
		WHERE	M.[STATUS] = 'N' 

		-- Insert new assets into Map
		Print 'add asset location'
		INSERT
		INTO	TB_M_ASSETS_LOC
		(
				COMPANY,		ASSET_NO,		COST_CODE,
				X,				Y,				SEQ,			DEGREE,
				CREATE_DATE,	CREATE_BY,		UPDATE_DATE,	UPDATE_BY
		)
		SELECT	A.COMPANY,		A.ASSET_NO,		A.COST_CODE,
				NULL,			NULL,			NULL,			NULL,
				GETDATE(),		@UserID,		GETDATE(),		@UserID
		FROM	#TB_T_NEW_ASSET A


		DROP TABLE #TB_T_NEW_ASSET

		-- Map Field From Request

		-- Keep Document
		DELETE FROM TB_T_UNLOCK_ASSET

		-- Unlock Status
		Print 'sp_BFD01260_CheckUnlockImpairmentRequest'
		EXEC sp_BFD01260_CheckUnlockImpairmentRequest @AppID, @UserID

		-- Reclass
		Print 'sp_BFD01260_CheckUnlockReclassRequest'
		EXEC sp_BFD01260_CheckUnlockReclassRequest @AppID, @UserID

		-- Retirement
		Print 'sp_BFD01260_CheckUnlockRetirementRequest'
		EXEC sp_BFD01260_CheckUnlockRetirementRequest @AppID, @UserID

		-- Info Change
		Print 'sp_BFD01260_CheckUnlockRetirementRequest'
		EXEC sp_BFD01260_CheckUnlockRetirementRequest @AppID, @UserID

		-- Transfer
		Print 'sp_BFD01260_CheckUnlockTransferRequest'
		EXEC sp_BFD01260_CheckUnlockTransferRequest @AppID, @UserID

		-- Settlement


		-- Clear Assets Status
		UPDATE	M
		SET		M.PROCESS_STATUS = NULL,
				M.UPDATE_DATE	= GETDATE(),
				M.UPDATE_BY		= @UserID
		FROM	TB_M_ASSETS_H M
		INNER	JOIN
				TB_T_UNLOCK_ASSET T
		ON		M.COMPANY	= T.COMPANY AND 
				M.ASSET_NO	= T.ASSET_NO AND 
				M.ASSET_SUB = T.ASSET_SUB

		-- Update Item Seq
		EXEC sp_BFD01260_UpdateItemSeq @AppID, @UserID
	
		COMMIT TRANSACTION T1
		SELECT 1 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning

	END TRY
	BEGIN CATCH
		
		DECLARE @ErrMsg VARCHAR(255)
		SET @ErrMsg= CONCAT('Receiving Fixed Assets Interface batch','|',' ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
		PRINT @ErrMsg
		IF @@TRANCOUNT <> 0
		BEGIN
			ROLLBACK TRANSACTION T1
		END
		
		EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrMsg,@UserID,'Y'

		SELECT 0 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
		RETURN;

	END CATCH

END
GO
