DROP PROCEDURE [dbo].[sp_WFD02640_Insert_Temp_RequestStock]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 16/03/2017
-- Description:	insert temp
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_Insert_Temp_RequestStock]
	@COMPANY		T_COMPANY, 
	@DOC_NO			T_DOC_NO   ,
	@YEAR			varchar(4)	  ,
	@ROUND			varchar(2)	  ,
	@ASSET_LOCATION varchar(1),
	@COST_CODE		T_COST_CODE,
	@COMMENT		varchar(200) = null,
	@ATTACHMENT		varchar(200)  = null,
	@USER_LOGIN		T_SYS_USER,
	@GUID			T_GUID,
	@COMPLETE_FLAG	VARCHAR(1),
	@SELECTED		varchar(1),
	@SV_EMP_CODE    T_SYS_USER
AS
BEGIN


DECLARE @CntNoScan INT


IF(@ASSET_LOCATION='I')
BEGIN
    SELECT  @CntNoScan = COUNT(*)
	FROM	TB_R_STOCK_TAKE_D
	WHERE	COMPANY			= @COMPANY AND
			[YEAR]			= @YEAR AND 
			[ROUND]			= @ROUND AND 
			ASSET_LOCATION	= @ASSET_LOCATION AND 
			COST_CODE		= @COST_CODE AND 
			CHECK_STATUS	= 'N'
	
END
ELSE
BEGIN-- OUT SOURCE --> @COST_CODE IS MEAN supplier code
    SELECT	@CntNoScan = COUNT(*)
	FROM	TB_R_STOCK_TAKE_D
	WHERE	COMPANY			= @COMPANY AND
			[YEAR]			= @YEAR AND 
			[ROUND]			= @ROUND AND 
			ASSET_LOCATION	= @ASSET_LOCATION AND 
			SV_EMP_CODE		= @COST_CODE AND 
			CHECK_STATUS	= 'N'
END

IF(@CntNoScan IS NOT NULL)
BEGIN
         IF(@CntNoScan >0)
		 BEGIN
		    SET @COMPLETE_FLAG='N'
		 END
		 ELSE
		 BEGIN
		   SET @COMPLETE_FLAG='Y'
		 END
END
ELSE
BEGIN
   SET @COMPLETE_FLAG='N'
END


IF NOT EXISTS(SELECT 1 FROM TB_T_REQUEST_STOCK WHERE  COMPANY = @COMPANY AND DOC_NO=@DOC_NO AND COST_CODE=@COST_CODE)
BEGIN

		INSERT INTO [dbo].[TB_T_REQUEST_STOCK]
				   (COMPANY, [DOC_NO]   ,[YEAR]
				   ,[ROUND]   ,[ASSET_LOCATION]
				   ,[COST_CODE]		   ,[COMMENT]
				   ,[ATTACHMENT]	   ,[CREATE_DATE]
				   ,[CREATE_BY]			   ,[GUID]
				   ,[COMPLETE_FLAG]	   ,[SELECTED]
				   ,[SV_EMP_CODE])
			 VALUES
				   (@COMPANY, @DOC_NO		  ,@YEAR	   ,@ROUND			 
				   ,@ASSET_LOCATION    ,@COST_CODE	   ,@COMMENT		 
				   ,@ATTACHMENT	  ,GETDATE()   ,@USER_LOGIN
				   ,ISNULL(@GUID,@DOC_NO)  ,ISNULL(@COMPLETE_FLAG,'N')   ,@SELECTED
				   ,@SV_EMP_CODE)
END
ELSE
BEGIN

			UPDATE [dbo].[TB_T_REQUEST_STOCK]
			   SET [DOC_NO] =			@DOC_NO			
				  ,[YEAR]	=			@YEAR			
				  ,[ROUND] =			@ROUND			
				  ,[ASSET_LOCATION] =	@ASSET_LOCATION 
				  ,[COST_CODE] =		@COST_CODE	
				  ,[COMPLETE_FLAG]	=	ISNULL(@COMPLETE_FLAG,'N')
				  ,[COMMENT] =			@COMMENT
				  ,[ATTACHMENT] =		ISNULL(@ATTACHMENT,[ATTACHMENT])		
				  ,[CREATE_DATE] =		GETDATE()
				  ,[CREATE_BY] =		@USER_LOGIN
				  ,[GUID] =				ISNULL(@GUID,@DOC_NO)
				  ,[SELECTED] =@SELECTED
				  ,[SV_EMP_CODE] = @SV_EMP_CODE
			 where  COMPANY		= @COMPANY AND
					DOC_NO		= @DOC_NO AND 
					COST_CODE	= @COST_CODE
END
END
GO
