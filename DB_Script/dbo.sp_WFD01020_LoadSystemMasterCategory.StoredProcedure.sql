DROP PROCEDURE [dbo].[sp_WFD01020_LoadSystemMasterCategory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jatuporn Sornsiriwong
-- Create date: 13/03/2017
-- Description:	Load distinct category
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01020_LoadSystemMasterCategory]	
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT DISTINCT CATEGORY AS Value FROM TB_M_SYSTEM	
END





GO
