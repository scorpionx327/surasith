DROP PROCEDURE [dbo].[sp_Common_GetSubDivision_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetSubDivision_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER,
@DivKeyword		varchar(8) = null
AS
BEGIN
		/*SELECT	SUB_DIV_CODE,
				SUB_DIV_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(	
					SUB_DIV_CODE LIKE CONCAT(@keyword,'%') 
					OR
					SUB_DIV_NAME LIKE CONCAT(@keyword,'%') 
				) 
		AND     (	
					DIV_CODE LIKE CONCAT(@DivKeyword,'%') 
					OR
					DIV_NAME LIKE CONCAT(@DivKeyword,'%') 
				) 
		GROUP BY SUB_DIV_CODE, SUB_DIV_NAME
		ORDER BY SUB_DIV_CODE	*/

		SELECT	SUB_DIV_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(						
					SUB_DIV_NAME LIKE CONCAT(@keyword,'%') 
				) 
		AND     (						
					DIV_NAME LIKE CONCAT(@DivKeyword,'%') 
				) 
		GROUP BY SUB_DIV_NAME
		ORDER BY SUB_DIV_NAME
				
END
GO
