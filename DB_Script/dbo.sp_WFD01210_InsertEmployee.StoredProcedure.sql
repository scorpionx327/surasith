DROP PROCEDURE [dbo].[sp_WFD01210_InsertEmployee]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 08/03/2017
-- Description:	Insert EMP
/*
Nipon 2019-07-30 09:17:50.803 add field from TabUserProfile.cshtml
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_InsertEmployee]
	 @EMP_CODE			varchar(8)
	,@EMP_TITLE			varchar(8)
	,@EMP_NAME			varchar(30)
	,@EMP_LASTNAME		varchar(30)
	,@ORG_CODE			varchar(21)
	,@COST_CODE			varchar(8)
	,@POST_CODE			varchar(4)
	,@POST_NAME			varchar(50)
	,@EMAIL				varchar(50)
	,@TEL_NO			varchar(20)
	,@SIGNATURE_PATH	varchar(100)
	,@ROLE				varchar(3)
	,@SPEC_DIV			varchar(100)
	,@FAADMIN			varchar(1) = 'N'
	,@DELEGATE_PIC		varchar(8)
	,@DELEGATE_FROM		varchar(30)
	,@DELEGATE_TO		varchar(30)
	,@USER_LOGIN        varchar(8)
	,@COMPANY			T_COMPANY
	,@AEC_USER			varchar(1) = null
	,@FA_Window			varchar(1) = null

	,@AEC_MANAGER 		varchar(1) = null

	,@AEC_GENERAL_MANAGER 		varchar(1) = null
	,@AEC_USER_LIST 		varchar(MAX) = null
	,@FASupervisor				varchar(1) = null
	,@FAWindow					varchar(1) = null

	,@JOB_CODE			varchar(4)
	,@JOB_NAME			varchar(50)
	
AS
BEGIN
	begin try

	begin transaction t1
	--select @AEC_MGR_EMP_ID = CONCAT(@COMPANY,'.',@EMP_CODE)

	declare @SYS_EMP_CODE T_SYS_USER
	set @SYS_EMP_CODE = CONCAT(@COMPANY,'.',@EMP_CODE)


	if(@AEC_USER='Y')
		select @FAADMIN='Y'

	INSERT INTO [dbo].[TB_M_EMPLOYEE]
           (SYS_EMP_CODE
		   ,COMPANY
		   ,[EMP_CODE]       
           ,[EMP_TITLET]
           ,[EMP_TITLE]
           ,[EMP_NAME]
           ,[EMP_LASTNAME]
           ,[EMP_NAME_T]
           ,[EMP_LASTNAME_T]
           ,[ORG_CODE]
           ,[COST_CODE]
           ,[COST_NAME]
           ,[POST_CODE]
           ,[POST_NAME]
		   -- ,JOB No have field Job.
           ,[EMAIL]
           ,[EFF_START_DATE]
           ,[RESIGNED_DATE]
           ,[EMP_TYPE]
           ,[SOURCE]
           ,[TEL_NO]
           ,[SIGNATURE_PATH]
           ,[ROLE]
           ,[SPEC_DIV]
           ,[ACTIVEFLAG]
           ,[PRINT_LOCATION]
           ,[FAADMIN]---y/n
           ,[DELEGATE_PIC]
           ,[DELEGATE_FROM]
           ,[DELEGATE_TO]
           ,[CREATE_DATE]
           ,[CREATE_BY]
           ,[UPDATE_DATE]
           ,[UPDATE_BY]
		   ,[JOB_CODE]
		   ,[JOB_NAME]
		   )
     VALUES
           (
		   --CONCAT(@COMPANY,'.',@EMP_CODE)
		   @SYS_EMP_CODE
		   ,@COMPANY
		    ,@EMP_CODE		
		   ,null	
		   ,@EMP_TITLE		
		   ,@EMP_NAME		
		   ,@EMP_LASTNAME	
		   ,null	
		   ,null
		   ,@ORG_CODE		
		   ,@COST_CODE		
		   ,(select top 1 COST_NAME from TB_M_COST_CENTER where  COST_CODE = @COST_CODE)
		   ,@POST_CODE		
		   ,@POST_NAME		
		   ,@EMAIL			
		   ,null
		   ,null	
		   ,null		
		   ,'FAS'		
		   ,@TEL_NO		
		   ,@SIGNATURE_PATH
		   ,@ROLE
		   ,@SPEC_DIV		
		   ,'Y'
		   ,null
		   ,@FAADMIN
		   ,@DELEGATE_PIC	
		   ,dbo.fn_ConvertStringToDate(@DELEGATE_FROM)
		   ,dbo.fn_ConvertStringToDate(@DELEGATE_TO)
		   ,GETDATE()
		   ,@USER_LOGIN
		   ,GETDATE()
		   ,@USER_LOGIN
		   ,@JOB_CODE
		   ,@JOB_NAME
		   )

	

	--When select cost center for AEC User	
	/*if(@AEC_USER = 'true')
	begin 

		INSERT INTO TB_M_AEC_USER_COST_CENTER (COMPANY, EMP_CODE, COST_CODE,CREATE_DATE,CREATE_BY)
		SELECT @COMPANY,@EMP_CODE,@COST_CODE,GETDATE(),@USER_LOGIN
	end*/

	create table #Temp
	(
		 SP_ROLE_CODE varchar(3), 
		 SP_ROLE varchar(3)
	)

	--AEC User (List by company) If AEC User - Checked					
	if(@AEC_USER='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='03',SP_ROLE='ACR'
	end

	if(@AEC_MANAGER='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='04',SP_ROLE='ACM' 
	end

	if(@AEC_GENERAL_MANAGER='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='09',SP_ROLE='AGM' 
	end

	if(@FAWindow='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='01',SP_ROLE='FW' 
	end

	if(@FASupervisor='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='02',SP_ROLE='FS' 
	end

	begin 
		delete TB_M_SP_ROLE where EMP_CODE= @SYS_EMP_CODE --@EMP_CODE
		INSERT INTO TB_M_SP_ROLE (EMP_CODE, SP_ROLE_COMPANY,SP_ROLE_CODE,SP_ROLE, CREATE_DATE, CREATE_BY,UPDATE_BY)
		SELECT @SYS_EMP_CODE,@COMPANY,t.SP_ROLE_CODE,t.SP_ROLE,GETDATE(),@USER_LOGIN,@USER_LOGIN from #Temp t

		drop table #Temp
	end

	IF EXISTS (SELECT * FROM TB_T_SV_COST_CENTER where EMP_CODE=@SYS_EMP_CODE) 
	BEGIN
		delete from [TB_M_SV_COST_CENTER] where EMP_CODE = @SYS_EMP_CODE

		INSERT INTO [dbo].[TB_M_SV_COST_CENTER]
				([COMPANY]
				,[COST_CODE]
				,[EMP_CODE]
				,[CREATE_DATE]
				,[CREATE_BY]
				,[UPDATE_DATE]
				,[UPDATE_BY])
		select
				@COMPANY
				,temp.COST_CODE
				,@SYS_EMP_CODE --,@EMP_CODE
				,GETDATE()
				,@USER_LOGIN
				,GETDATE()
				,@USER_LOGIN
		from TB_T_SV_COST_CENTER temp
		where temp.EMP_CODE= @SYS_EMP_CODE --@EMP_CODE

		delete TB_T_SV_COST_CENTER
	END

	IF EXISTS (SELECT * FROM TB_T_FASUP_COST_CENTER where EMP_CODE=@SYS_EMP_CODE) 
	BEGIN

		delete from [TB_M_FASUP_COST_CENTER] where EMP_CODE = @SYS_EMP_CODE

		INSERT INTO [dbo].[TB_M_FASUP_COST_CENTER]
				([COMPANY]
				,[COST_CODE]
				,[EMP_CODE]
				,[CREATE_DATE]
				,[CREATE_BY]
				,[UPDATE_DATE]
				,[UPDATE_BY])
		select
				@COMPANY
				,temp.RES_COST_CODE
				,@SYS_EMP_CODE --,@EMP_CODE
				,GETDATE()
				,@USER_LOGIN
				,GETDATE()
				,@USER_LOGIN
		from TB_T_FASUP_COST_CENTER temp
		where temp.EMP_CODE= @SYS_EMP_CODE --@EMP_CODE

		delete TB_T_FASUP_COST_CENTER
	END

	-- Update Manager
	-- Update Manager
	UPDATE	TB_M_EMPLOYEE -- clear current
	SET		AEC_MGR_EMP_ID = NULL
	WHERE	AEC_MGR_EMP_ID = @SYS_EMP_CODE

	UPDATE	M -- asssign new 
	SET		M.AEC_MGR_EMP_ID = @SYS_EMP_CODE
	FROM	TB_M_EMPLOYEE M
	INNER	JOIN
			dbo.fn_GetMultipleList(@AEC_USER_LIST) T
	ON		M.SYS_EMP_CODE	= T.value


	EXEC sp_Common_GenerateCostCenterMapping @SYS_EMP_CODE --@EMP_CODE
	EXEC sp_Common_GenerateCostCenterRole @SYS_EMP_CODE --@EMP_CODE

	commit transaction t1
	end try
	begin catch
		rollback transaction t1 ;
		throw ;
	end catch
END
GO
