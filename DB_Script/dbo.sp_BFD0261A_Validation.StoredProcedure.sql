DROP PROCEDURE [dbo].[sp_BFD0261A_Validation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)

- ============================================= SFAS =============================================*/

CREATE PROCEDURE  [dbo].[sp_BFD0261A_Validation]
  @AppID			INT,
  @UserID	   VARCHAR(8),
  @IsUpdate VARCHAR(1),
  @StockYear VARCHAR(4),
  @StockRound VARCHAR(2),
  @StockAssetLocation VARCHAR(1)
AS
BEGIN TRY
	DECLARE @PLAN_STATUS		VARCHAR(50)
	DECLARE @Error BIT;
	SET @Error =0;
DECLARE @Msg VARCHAR(500)
      --Master Check		 MSTD0012AERR -->	MSTD0012AERR :  {0} does not exist in {1}
	    SET		@Msg = dbo.fn_GetMessage('MSTD0012AERR')
	    INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  		SELECT  @AppID, 'P', 'N', 'E',
				dbo.fn_StringFormat(@Msg , CONCAT('Asset No : ',r.ASSET_NO,'|','Asset Master (TB_M_ASSETS_H)')),'Y' , @UserID, GETDATE()
		FROM    TB_S_STOCK_TAKE r
		WHERE  NOT EXISTS (SELECT 1 
					  FROM TB_M_ASSETS_H AssetH
					  WHERE r.ASSET_NO=AssetH.ASSET_NO
							)

	IF @@ROWCOUNT > 0	SET	@Error = 1

									
--Master Check	 MCOM0015AERR	--> MCOM0015AERR : Asset No {0} was retired											
	    SET		@Msg = dbo.fn_GetMessage('MCOM0015AERR')
	   INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  		SELECT  @AppID, 'P', 'N', 'E',
				dbo.fn_StringFormat(@Msg , r.ASSET_NO),'Y' , @UserID, GETDATE()
		FROM    TB_S_STOCK_TAKE r
		WHERE EXISTS (SELECT 1 
					  FROM TB_M_ASSETS_H AssetH
					  WHERE r.ASSET_NO=AssetH.ASSET_NO
						    and AssetH.STATUS='N'
							)
	IF @@ROWCOUNT > 0	SET	@Error = 1
										
--Check Location MFAS1202AERR : There is many asset location (Inhouse/Outsource). System allow to upload data with same location.	 													
	DECLARE	@cnt	INT
		SELECT @cnt =COUNT(*) 
		FROM	(	SELECT AssetH.INHOUSE_FLAG
					FROM    TB_S_STOCK_TAKE r INNER JOIN  TB_M_ASSETS_H AssetH  ON r.ASSET_NO=AssetH.ASSET_NO
					GROUP BY AssetH.INHOUSE_FLAG
					) s

   IF(@cnt > 1)
	 BEGIN
		EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MFAS1202AERR', NULL,@UserID
		SET	@Error = 1
	END

IF (@StockAssetLocation ='I')  ---- Check incase In-house should be have sv user
BEGIN
	   SET		@Msg = dbo.fn_GetMessage('MCOM0022AERR')
	   INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  		SELECT  @AppID, 'P', 'N', 'E',
				dbo.fn_StringFormat(@Msg , r.ASSET_NO),'Y' , @UserID, GETDATE()
	   FROM (
	  			SELECT S.ASSET_NO ,S.COST_CODE,SvCost.EMP_CODE
				FROM (
						 SELECT AssetH.ASSET_NO, (SELECT TOP (1) AssetD.COST_CODE   
		 								FROM TB_M_ASSETS_D AssetD left join TB_M_COST_CENTER CC on AssetD.COST_CODE = CC.COST_CODE
										WHERE AssetH.ASSET_NO=AssetD.ASSET_NO and CC.STATUS = 'Y' ORDER BY UNIT DESC 
										) AS COST_CODE
						 FROM TB_M_ASSETS_H AssetH 
						 WHERE EXISTS( SELECT 1 FROM TB_S_STOCK_TAKE S_StockTake 
									   WHERE AssetH.ASSET_NO=S_StockTake.ASSET_NO)	
						 )S LEFT JOIN TB_M_SV_COST_CENTER SvCost ON S.COST_CODE=SvCost.COST_CODE
				 )r
			WHERE ISNULL(r.EMP_CODE,'')=''

    	IF @@ROWCOUNT > 0	SET	@Error = 1
END 

--IF (@StockAssetLocation ='O') ---- Check incase Out-source should be have supplier no.
--BEGIN
--    SET		@Msg = dbo.fn_GetMessage('MCOM0023AERR')
--	   INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
--  		SELECT  @AppID, 'P', 'N', 'E',
--				dbo.fn_StringFormat(@Msg , r.ASSET_NO),'Y' , @UserID, GETDATE()
--	   FROM (
--	  			 SELECT AssetH.ASSET_NO,SUPPLIER_NO
--						 FROM TB_M_ASSETS_H AssetH 
--						 WHERE EXISTS( SELECT 1 FROM TB_S_STOCK_TAKE S_StockTake 
--									   WHERE AssetH.ASSET_NO=S_StockTake.ASSET_NO)	
--				 )r
--			WHERE ISNULL(r.SUPPLIER_NO,'')=''

--    	IF @@ROWCOUNT > 0	SET	@Error = 1
--END


IF (@IsUpdate='N')
BEGIN
 --- check Duplicate in Master  MSTD0010AERR
   SET		@Msg = dbo.fn_GetMessage('MSTD0010AERR')
  	  INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  		SELECT  @AppID, 'P', 'N', 'E',
				dbo.fn_StringFormat(@Msg , CONCAT('Stock take period ','|', @StockYear,'/',@StockRound ,' Asset Location :',@StockAssetLocation,'|','Stock Take Result Table.')),'Y', @UserID, GETDATE()
		FROM    TB_R_STOCK_TAKE_H r
		WHERE [YEAR]=@StockYear AND [ROUND]=@StockRound AND ASSET_LOCATION=@StockAssetLocation

	IF @@ROWCOUNT > 0	SET	@Error = 1

END
IF (@IsUpdate='Y')
BEGIN
 --- check Asset Location notmatch
 -- select  dbo.fn_GetMessage('MFAS1205AERR')
  --MFAS1205AERR : There is asset location (Inhouse/Outsource) mismatch in result table.
				IF EXISTS(SELECT   ASSET_LOCATION
								 FROM	 (	SELECT  CASE   WHEN AssetH.INHOUSE_FLAG ='Y' THEN 'I'
												   ELSE 'O' END ASSET_LOCATION 
											FROM    TB_S_STOCK_TAKE r INNER JOIN  TB_M_ASSETS_H AssetH  ON r.ASSET_NO=AssetH.ASSET_NO
											GROUP BY AssetH.INHOUSE_FLAG
											) s
								  WHERE NOT EXISTS( SELECT 1 
												   FROM TB_R_STOCK_TAKE_H StockH
												   WHERE StockH.[YEAR]=@StockYear 
												   AND  StockH.[ROUND]=@StockRound 
												   AND  StockH.ASSET_LOCATION=@StockAssetLocation
												   AND  s.ASSET_LOCATION = @StockAssetLocation)) 
				BEGIN 
					EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MFAS1205AERR', NULL,@UserID
					SET	@Error = 1;
				END

--MFAS1601AERR : Can not upload data to server Because Period year :{0}, round : {1}, plan not equal draft.
--Suphachai L. 2017-05-16				
				SELECT @PLAN_STATUS = PLAN_STATUS 
				FROM TB_R_STOCK_TAKE_H
				WHERE YEAR					= @StockYear
					AND ROUND				= @StockRound
					AND ASSET_LOCATION		= @StockAssetLocation
				
				IF ISNULL(@PLAN_STATUS,'') <> 'D'
				BEGIN
					SET		@Msg = dbo.fn_GetMessage('MFAS1601AERR')
					INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  					SELECT  @AppID, 'P', 'N', 'E',
							dbo.fn_StringFormat(@Msg , CONCAT(@StockYear,'|',@StockRound,'|',@StockAssetLocation)),'Y' , @UserID, GETDATE()
					SET	@Error = 1;
				END
END



	RETURN @Error ;
END TRY
BEGIN CATCH
			PRINT concat('ERROR Message:',ERROR_MESSAGE())
END CATCH







GO
