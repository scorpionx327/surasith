DROP PROCEDURE [dbo].[sp_WFD02620_SaveAssignmentFAAdmin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 15-02-2017
-- Description:	Save assignment by fa admin
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_SaveAssignmentFAAdmin]
	@CREATE_BY				T_USER
	, @YEAR					varchar(4)
	, @ROUND				varchar(2)
	, @ASSET_LOCATION		varchar(1)
	, @EMP_CODE				T_SYS_USER
	, @FA_DATE_FROM			date	
	, @FA_DATE_TO			date	
	, @DATE_FROM			date	
	, @DATE_TO				date	
	, @TIME_START			varchar(5)	
	, @TIME_END				varchar(5)	
	, @TIME_MINUTE			int	
	, @USAGE_HANDHELD		int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	-- /////////////////////////////////////////////
	-- Update TB_R_STOCK_TAKE_D_PER_SV

	UPDATE TB_R_STOCK_TAKE_D_PER_SV
		SET FA_DATE_FROM = @FA_DATE_FROM
			,FA_DATE_TO = @FA_DATE_TO
			,DATE_FROM = @DATE_FROM
			,DATE_TO = @DATE_TO
			,TIME_START = @TIME_START
			,TIME_END = @TIME_END
			,TIME_MINUTE = @TIME_MINUTE
			,USAGE_HANDHELD = @USAGE_HANDHELD
			,UPDATE_BY = @CREATE_BY
			--,UPDATE_DATE = GETDATE() Remove By Surasith t. 2017-05-17 Becuase system check concurrency, FA Admin can re-assign then error is occur when update_date is changed.
	WHERE YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION and EMP_CODE=@EMP_CODE
		



END




GO
