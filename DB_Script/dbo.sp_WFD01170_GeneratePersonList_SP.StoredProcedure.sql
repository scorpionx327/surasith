DROP PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList_SP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList_SP]
@Indx		INT,
@Company	T_COMPANY,
@Role		VARCHAR(3),
@GUID		T_GUID = NULL
AS
BEGIN
	INSERT
	INTO	TB_H_REQUEST_APPR(
			DOC_NO,
			INDX,
			[ROLE],
			ACTUAL_ROLE,
			EMP_CODE,
			EMP_TITLE,
			EMP_NAME,
			EMP_LASTNAME,
			EMAIL,
			MAIN,
			ORG_CODE,
			[GUID]
	)
	SELECT	'',
			@Indx,
			@Role,
			E.[ROLE],
			E.SYS_EMP_CODE,
			EMP_TITLE,
			EMP_NAME,
			EMP_LASTNAME,
			EMAIL,
			'M',
			ORG_CODE,
			@GUID
	FROM	TB_M_EMPLOYEE E WHERE EXISTS(SELECT 1 FROM TB_M_SP_ROLE SP 
											WHERE		SP.EMP_CODE			= E.SYS_EMP_CODE  AND 
														SP.SP_ROLE			= @Role AND
													(	SP.SP_ROLE_COMPANY	= @Company OR @Role = 'ACR') 
										)
END

--select * from TB_H_REQUEST_APPR where GUID = '7d83531c-ca32-43f1-98c1-9b56425102ad'
GO
