DROP PROCEDURE [dbo].[sp_WFD02320_GetChangePrinter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 07/02/2017
-- Description:	GET CHANGE PRINTER
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02320_GetChangePrinter] 
	@barcode varchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select distinct VALUE,
	CATEGORY, 
	SUB_CATEGORY, 
	CODE, 
	REMARKS, 
	ACTIVE_FLAG, 
	CREATE_BY, 
	CREATE_DATE, 
	UPDATE_BY, 
	UPDATE_DATE
	 from TB_M_SYSTEM
where CATEGORY = 'PRINT_CONFIG' and SUB_CATEGORY like 'PRINTER_%' and CODE = @barcode
order by value
END



GO
