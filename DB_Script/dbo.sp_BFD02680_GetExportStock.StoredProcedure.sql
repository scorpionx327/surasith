DROP PROCEDURE [dbo].[sp_BFD02680_GetExportStock]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_BFD02680_GetExportStock]
@YEAR				as  VARCHAR(4)	,					
@ROUND				as 	VARCHAR(2)	,					
@ASSET_LOCATION		as	VARCHAR(1)	,
@APL_ID numeric(18,2)				
 
AS

BEGIN

 DELETE
FROM TB_T_STOCK_EXPORT
WHERE     APL_ID =@APL_ID AND BATCH_ID='BFD02680' 
	  AND YEAR=@YEAR AND ROUND =@ROUND 
	   AND ASSET_LOCATION=@ASSET_LOCATION



SELECT 
	STD.ASSET_NO	as	FA_ID , 										
	REPLACE(AH.ASSET_NAME,',','')	as	FA_NAME , -- 2017.05.02 : CSV not support comma
	STD.COST_CODE	as 	CC_ID ,										
	STD.COST_NAME	as	CC_NAME ,										
	STD.PLANT_CD	as 	LOC_ID ,									
	STD.SUB_TYPE	as 	SUB_TYPE ,									
	AH.SERIAL_NO	as	SR_NO ,								
	CONVERT(varchar(10),AH.DATE_IN_SERVICE,112)	as	REG_DATE ,										
	AH.BOI_FLAG	as	BOI_STS ,
	--(case when AH.STATUS = 'N' and AH.RETIREMENT_DATE IS NOT NULL then  'Y' else 'N' end )	as SALE_STS,
	(case when AH.STATUS = 'N' and AH.RETIREMENT_DATE IS NOT NULL then  'T' else 'F' end )	as SALE_STS, -- 2017.05.02 : Change follow by IS		
	dbo.fn_GetBarcodeSize(AH.BARCODE_SIZE,'A') as 	STK_SIZE,																				
	AH.BARCODE_SIZE as	STK_SIZE	,									
	AH.TAG_PHOTO	as 	FA_PIC ,										
	'A1'			as	PHY_ID ,									
	NULL        as	MOD_DATE ,									
	'-'			as	LOC_NAME ,									
	CONVERT(varchar(10),STD.UPDATE_DATE,112) as	PHY_DATE										
FROM 
	TB_R_STOCK_TAKE_H STH
	INNER JOIN TB_R_STOCK_TAKE_D STD ON STH.STOCK_TAKE_KEY = STD.STOCK_TAKE_KEY
	INNER JOIN TB_M_ASSETS_H  AH ON STD.ASSET_NO = AH.ASSET_NO

WHERE 
	STH.[YEAR] = @YEAR
	AND STH.[ROUND] = @ROUND 
	AND STH.[ASSET_LOCATION] = @ASSET_LOCATION
	AND STH.[PLAN_STATUS] = 'F'
	AND STD.SCAN_DATE is not null
ORDER BY STD.ASSET_NO



END




GO
