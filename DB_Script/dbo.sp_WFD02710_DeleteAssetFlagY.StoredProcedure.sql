DROP PROCEDURE [dbo].[sp_WFD02710_DeleteAssetFlagY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02710_DeleteAssetFlagY]
@DOC_NO		T_DOC_NO,
@USER		T_SYS_USER
AS
BEGIN
	-- This store is executed by Approve Operation
	-------------------------------------------------------------
	DELETE
	FROM	TB_T_DELETED_ASSETS
	WHERE	DOC_NO = @DOC_NO
	INSERT
	INTO	TB_T_DELETED_ASSETS
	(		COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	COST_CODE)
	select	COMPANY,	DOC_NO,	AUC_NO,		AUC_SUB,	NULL
	FROM	TB_R_REQUEST_SETTLE_H
	WHERE	DOC_NO	= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	EXEC sp_WFD01170_GenerateCommentDeleted @DOC_NO, @USER, 'A'
	-----------------------------------------------------------
	--

	DECLARE @TB_T_TARGET_ASSET TABLE
	(
		COMPANY		T_COMPANY,
		ASSET_NO	T_ASSET_NO,
		ASSET_SUB	T_ASSET_SUB
	)
	INSERT
	INTO	@TB_T_TARGET_ASSET
	SELECT	COMPANY,	ASSET_NO,		ASSET_SUB -- TARGET ASSET 
	FROM	TB_R_REQUEST_SETTLE_H
	WHERE	DOC_NO	= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	DELETE	T	-- Delete from list when target asset is refer to other asset
	FROM	@TB_T_TARGET_ASSET T
	INNER	JOIN
			TB_R_REQUEST_SETTLE_H M
	ON		M.COMPANY	= T.COMPANY AND
			M.ASSET_NO	= T.ASSET_NO AND
			M.ASSET_SUB	= T.ASSET_SUB
	WHERE	DOC_NO	= @DOC_NO AND
			ISNULL(DELETE_FLAG,'N')	= 'N'		

	INSERT
	INTO	@TB_T_TARGET_ASSET
	SELECT	COMPANY,	AUC_NO,		AUC_SUB -- AUC
	FROM	TB_R_REQUEST_SETTLE_H
	WHERE	DOC_NO	= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	-- Unlock process status when assets is deleted from request
	UPDATE	H
	SET		H.PROCESS_STATUS	= NULL,
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN -- Deleted
			@TB_T_TARGET_ASSET M 
	ON		M.COMPANY		= H.COMPANY AND
			M.ASSET_NO		= H.ASSET_NO AND 
			M.ASSET_SUB		= H.ASSET_SUB
		
	

	-- Delete assets from request
	DELETE	TB_R_REQUEST_SETTLE_H
	WHERE	DOC_NO		= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	-------------------------------------------------------------
END
GO
