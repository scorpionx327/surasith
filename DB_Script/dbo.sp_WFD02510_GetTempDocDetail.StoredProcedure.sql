DROP PROCEDURE [dbo].[sp_WFD02510_GetTempDocDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD02510_GetTempDocDetail]
(
	@DOC_NO						VARCHAR(10)
)
	
AS
BEGIN
	SELECT 
	DOC_NO,							ASSET_NO,										PHOTO_ATTACH,
	BOI_ATTACH_DOC
	FROM TB_R_REQUEST_DISP_D D
	WHERE D.DOC_NO		 = @DOC_NO
	AND  (PHOTO_ATTACH LIKE 'TEMP%' OR BOI_ATTACH_DOC LIKE 'TEMP%')


END





GO
