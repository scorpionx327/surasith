DROP PROCEDURE [dbo].[sp_WFD01110_GetAnnouncement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SARUN YUANYONG
-- Create date: 21/02/2017
-- Description:	GET Announcement [DISPLAY_FLAG] Y
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetAnnouncement]
(
	@COMPANY T_COMPANY = null
)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

			SELECT [COMPANY]
				  ,[ANNOUNCE_ID]
				  ,[ANNOUNCE_DESC]
				  ,[ANNOUNCE_URL]
				  ,[DISPLAY_FLAG]
				  ,[ANNOUNCE_TYPE]
				  ,[CREATE_DATE]
				  ,[CREATE_BY]
			  FROM [TB_R_ANNOUNCEMENT]
			  WHERE [DISPLAY_FLAG] = 'Y'
			  AND (COMPANY in (select COMPANY_CODE FROM dbo.fn_GetMultipleCompanyList(@COMPANY)) or @COMPANY is null)
END



GO
