DROP PROCEDURE [dbo].[sp_BFD021A1_UploadFixedAssetRequestBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD021A1_UploadFixedAssetRequestBatch]
@AppID		INT,
@Company	T_COMPANY,
@GUID		T_GUID,
@User		T_SYS_USER
AS
BEGIN
	
	-- Validation
	DECLARE @ErrorCnt	INT = 0
	EXEC sp_BFD02A10_UploadFixedAssetRequesValidation @AppID, @Company, @GUID, @User, @ErrorCnt OUT
	IF @ErrorCnt > 0
	BEGIN
		SELECT 0 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
		RETURN;
	END
	
	BEGIN TRY
		-- Insert
		DELETE
		FROM	TB_T_REQUEST_ASSET_D
		WHERE	[GUID] = @GUID

	
		INSERT
		INTO	TB_T_REQUEST_ASSET_D (
				COMPANY,			DOC_NO,			ASSET_NO,			ASSET_SUB,
				--ACC_PRINCIPLE,		DPRE_AREA,		DOCUMENT_DATE,		POSTING_DATE,
				--REASON,				AMOUNT_POSTED,	[PERCENTAGE],		IMPAIRMENT_FLAG,
				--BF_CUMU_ACQU_PRDCOST_01,
				[STATUS],			[GUID],			DELETE_FLAG,
				CREATE_DATE,		CREATE_BY,		UPDATE_DATE,		UPDATE_BY
		)
		SELECT	M.COMPANY,			NULL,			M.ASSET_NO,			M.ASSET_SUB,
				--NULL,				NULL,			NULL,				NULL,
				--S.REASON,			S.AMOUNT,		NULL,				NULL,
				--M.CUMU_ACQU_PRDCOST_01,
				'NEW',				@GUID,			NULL,
				GETDATE(),			@User,			GETDATE(),			@User
		FROM	TB_M_ASSETS_H M
		INNER	JOIN
				TB_S_REQUEST_ASSET_D S
		ON		S.COMPANY	= M.COMPANY AND
				S.ASSET_NO	= M.ASSET_NO AND
				S.ASSET_SUB	= M.ASSET_SUB

		SELECT 1 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
		RETURN;

	END TRY
	BEGIN CATCH
		DECLARE @Msg VARCHAR(255)
		SET @Msg = ERROR_MESSAGE();
		
		EXEC sp_Common_InsertDetailLog @AppID, 'P', 'N', 'E', @Msg, @User
		
		SELECT 0 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
		RETURN;

	END CATCH
END
GO
