DROP PROCEDURE [dbo].[sp_WFD02410_UpdateAssetApprove]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02410_UpdateAssetApprove]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN

	EXEC sp_WFD02410_DeleteAssetFlagY @DOC_NO, @USER

	UPDATE	R
	SET		RESP_COST_CODE		=	D.RESP_COST_CODE,
			TRNF_REASON			=	D.TRNF_REASON,
			PLANT				=	D.PLANT,
			LOCATION_CD			=	D.LOCATION_CD,
			LOCATION_NAME		=	D.LOCATION_NAME,
			FUTURE_USE			=	D.FUTURE_USE,
			WBS_PROJECT			=	D.WBS_PROJECT,
			TO_EMPLOYEE			=	D.TO_EMPLOYEE,
			LOCATION_DETAIL		=	D.LOCATION_DETAIL,
			TRNF_EFFECTIVE_DT	=	D.TRNF_EFFECTIVE_DT,
			UPDATE_DATE			=	GETDATE(),
			UPDATE_BY			=	@USER
	FROM	TB_R_REQUEST_TRNF_D R WITH(NOLOCK)
	INNER	JOIN
			TB_T_REQUEST_TRNF_D D WITH(NOLOCK)
	ON		R.COMPANY			= D.COMPANY AND
			R.ASSET_NO			= D.ASSET_NO AND
			R.ASSET_SUB			= D.ASSET_SUB
	WHERE	D.[GUID]			= @GUID

END
GO
