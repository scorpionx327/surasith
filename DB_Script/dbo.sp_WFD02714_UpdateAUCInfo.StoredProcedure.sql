DROP PROCEDURE [dbo].[sp_WFD02714_UpdateAUCInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02714_UpdateAUCInfo]
@DOC_NO			T_DOC_NO,
@COMPANY		T_COMPANY,
@AUC_NO			T_ASSET_NO,
@AUC_SUB		T_ASSET_SUB,
@PO_NO			VARCHAR(10),
@SAP_DOC_NO		VARCHAR(10),
@FISCAL_YEAR	VARCHAR(4),
@INV_NO			VARCHAR(16),
@INV_ITEMNO		INT,
@INV_DESC		VARCHAR(50),
@INV_AMT		DECIMAL(23,2)

AS
BEGIN

	
	-- Incase update existsing invoice no
	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_SETTLE_H WHERE
			DOC_NO		= @DOC_NO	AND
			COMPANY		= @COMPANY AND
			AUC_NO		= @AUC_NO AND
			AUC_SUB		= @AUC_SUB AND
			PO_NO		= @PO_NO AND
			INV_NO		= @INV_NO AND
			INV_LINE	= @INV_ITEMNO
	)
	BEGIN
		UPDATE	TB_R_REQUEST_SETTLE_H
		SET		INV_DESC	= @INV_DESC,
				AUC_AMOUNT	= @INV_AMT,
				[STATUS]	= 'RESP',  -- Request, Response, New, Gen, Success, Error 
				UPDATE_DATE	= GETDATE(),
				UPDATE_BY	= 'SAPAUC'
		WHERE
				DOC_NO		= @DOC_NO AND
				COMPANY		= @COMPANY AND
				AUC_NO		= @AUC_NO AND
				PO_NO		= @PO_NO AND
				INV_NO		= @INV_NO AND
				INV_LINE	= @INV_ITEMNO

		RETURN
	END 
	DECLARE @Cnt	INT
	SELECT	@Cnt = COUNT(1)
	FROM	TB_R_REQUEST_SETTLE_H
	WHERE	DOC_NO		= @DOC_NO	AND
			COMPANY		= @COMPANY AND
			AUC_NO		= @AUC_NO AND
			AUC_SUB		= @AUC_SUB

	IF @Cnt = 0
		RETURN

	-- Incase found 1, user add auc then request invoice
	IF @Cnt = 1
	BEGIN
		UPDATE	TB_R_REQUEST_SETTLE_H
		SET		PO_NO		= @PO_NO,
				INV_NO		= @INV_NO,
				INV_LINE	= @INV_ITEMNO,
				INV_DESC	= @INV_DESC,
				AUC_AMOUNT	= @INV_AMT,
				[STATUS]	= 'RESP',  -- Request, Response, New, Gen, Success, Error 
				UPDATE_DATE	= GETDATE(),
				UPDATE_BY	= 'SAPAUC'
		WHERE
				DOC_NO		= @DOC_NO AND
				COMPANY		= @COMPANY AND
				AUC_NO		= @AUC_NO AND
				AUC_SUB		= @AUC_SUB
		RETURN
	END

	-- add new invoice line
	INSERT
	INTO	TB_R_REQUEST_SETTLE_H
	(		COMPANY,			DOC_NO,			SEQ_NO,			AUC_NO,			AUC_SUB,			AUC_NAME,
			FISCAL_YEAR,		SAP_DOC,		PO_NO,			INV_NO,			INV_LINE,			INV_DESC,
			AUC_AMOUNT,			AUC_REMAIN,		ASSET_NO,		ASSET_SUB,
			SETTLE_AMOUNT,		CAP_DATE,		[STATUS],		[GUID],			DELETE_FLAG,
			CREATE_DATE,		CREATE_BY,		UPDATE_DATE,	UPDATE_BY )
	SELECT	TOP 1
			COMPANY,			DOC_NO,			SEQ_NO,			AUC_NO,			AUC_SUB,			AUC_NAME,
			@FISCAL_YEAR,		@SAP_DOC_NO,	@PO_NO,			@INV_NO,		@INV_ITEMNO,		@INV_DESC,
			@INV_AMT,			@INV_AMT,		NULL,			NULL,
			NULL,				NULL,			'RESP',			[GUID],			DELETE_FLAG,
			GETDATE(),			'SAPAUC',		GETDATE(),	'SAPAUC'
	FROM	TB_R_REQUEST_SETTLE_H
	WHERE	DOC_NO		= @DOC_NO AND
			COMPANY		= @COMPANY AND
			AUC_NO		= @AUC_NO AND
			AUC_SUB		= @AUC_SUB
	ORDER	BY
			INV_NO, INV_LINE
END
GO
