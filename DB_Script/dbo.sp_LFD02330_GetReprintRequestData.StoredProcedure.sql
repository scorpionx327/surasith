DROP PROCEDURE [dbo].[sp_LFD02330_GetReprintRequestData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suphachai L.>
-- Create date: <2017/01/31> เวลา 12:00
-- Description:	<Export reprint request screen to excel>
-- =============================================
--exec sp_LFD02330_GetReprintRequestData 'ad97bed1-4497-4a71-829e-d5d5cc174432'
CREATE PROCEDURE [dbo].[sp_LFD02330_GetReprintRequestData]
(	
	@GUID						T_GUID
)
AS
BEGIN
--SET NOCOUNT ON;

	declare @COUNT_DOC as int

	select @COUNT_DOC=count(*) from TB_R_REQUEST_H where [GUID]	= @GUID 


	if (@COUNT_DOC>0)
	begin
		SELECT distinct H.DOC_NO,															
		dbo.fn_GetCurrentStatus(H.DOC_NO) AS CURRENT_STATUS, --S.VALUE CURRENT_STATUS,				
		dbo.fn_dateFAS(H.REQUEST_DATE) REQUEST_DATE,
		--H.COMPANY,
		H.EMP_CODE,													
		ISNULL(H.EMP_TITLE,'') + ' ' + H.EMP_NAME + ' ' + H.EMP_LASTNAME EMP_NAME,	
		H.PHONE_NO TEL_NO,			
		H.COST_CODE + ' : ' + C.COST_NAME COST_NAME,								
		H.POS_CODE + ' : ' + H.POS_NAME POS_NAME,					
		H.DEPT_NAME,																
		H.SECT_NAME,
		R.ASSET_NO,
		R.ASSET_SUB,
		A.ASSET_NAME,
		A.ASSET_CLASS + M.VALUE ASSET_CLASS,
		--Comment by sutathawat
		--CASE WHEN H.REQUEST_TYPE = 'R' THEN 'Y' ELSE '' END DAMAGE,	
		--CASE WHEN H.REQUEST_TYPE = 'L' THEN 'Y' ELSE '' END LOSS,		
		R.DAMAGE_FLAG DAMAGE,
		R.LOSS_FLAG LOSS,				
		R.PRINT_COUNT
		FROM TB_T_REQUEST_REPRINT R 
		left JOIN 
			TB_R_REQUEST_H H
		ON		H.DOC_NO		= R.DOC_NO 
		left	JOIN 
			TB_M_COST_CENTER C 
		ON		R.COST_CODE		= C.COST_CODE AND 
			R.COMPANY		= C.COMPANY	
		left	JOIN TB_M_ASSETS_H A ON R.ASSET_NO				= A.ASSET_NO AND R.COMPANY=A.COMPANY AND R.ASSET_SUB=A.ASSET_SUB
		left	join TB_M_SYSTEM M ON A.ASSET_CLASS = M.CODE AND 
			M.CATEGORY = 'ASSET_CLASS' AND 
			SUB_CATEGORY='ASSET_CLASS'
	
		WHERE	R.[GUID]	= @GUID 
		ORDER	BY R.ASSET_NO 
	end
	else
	begin 
		SELECT  distinct R.DOC_NO,															
		dbo.fn_GetCurrentStatus(R.DOC_NO) AS CURRENT_STATUS, --S.VALUE CURRENT_STATUS,				
		dbo.fn_dateFAS(R.CREATE_DATE) REQUEST_DATE,
		--E.COMPANY,
		E.EMP_CODE  EMP_CODE,													
		ISNULL(E.EMP_TITLE,'') + ' ' + E.EMP_NAME + ' ' + E.EMP_LASTNAME EMP_NAME,	
		E.TEL_NO TEL_NO,			
		E.COST_CODE + ' : ' + C.COST_NAME COST_NAME,								
		E.POST_CODE + ' : ' + E.POST_NAME POS_NAME,					
		O.DEPT_NAME  DEPT_NAME,																
		O.SECTION_NAME  SECT_NAME,
		R.ASSET_NO,
		R.ASSET_SUB,
		A.ASSET_NAME,
		A.ASSET_CLASS + M.VALUE ASSET_CLASS,
		--Comment by sutathawat
		--CASE WHEN H.REQUEST_TYPE = 'R' THEN 'Y' ELSE '' END DAMAGE,	
		--CASE WHEN H.REQUEST_TYPE = 'L' THEN 'Y' ELSE '' END LOSS,		
		R.DAMAGE_FLAG DAMAGE,
		R.LOSS_FLAG LOSS,				
		R.PRINT_COUNT
		FROM TB_T_REQUEST_REPRINT R  
		left join
		TB_M_EMPLOYEE E on
		E.SYS_EMP_CODE=R.CREATE_BY
		left join 
		TB_M_ORGANIZATION O on
		E.ORG_CODE=O.ORG_CODE 
		left	JOIN 
			TB_M_COST_CENTER C 
		ON		R.COST_CODE		= C.COST_CODE AND 
			R.COMPANY		= C.COMPANY	
		left	JOIN TB_M_ASSETS_H A ON R.ASSET_NO				= A.ASSET_NO AND R.COMPANY=A.COMPANY AND R.ASSET_SUB=A.ASSET_SUB
		left	join TB_M_SYSTEM M ON A.ASSET_CLASS = M.CODE AND 
			M.CATEGORY = 'ASSET_CLASS' AND 
			SUB_CATEGORY='ASSET_CLASS'
	
		WHERE	R.[GUID]	= @GUID 
		ORDER	BY R.ASSET_NO 
	end 


	
END
GO
