DROP PROCEDURE [dbo].[sp_WFD01110_GetRetirement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Surasith T.
-- Create date: 21/09/2019
-- Description:	get transfer tab in home page
/*
Nipon 2019-08-14 16:38:10.537
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetRetirement]
	-- Add the parameters for the stored procedure here
	@COMPANY VARCHAR(50)	= null, -- not use T_COMPANY varchar so out of range.
	@EMP_CODE T_SYS_USER,
	@DISP_TYPE	VARCHAR(10)	= NULL,
	@pageNum INT,
    @pageSize INT,
    @sortColumnName VARCHAR(50),
	@orderType VARCHAR(5),
	@TOTAL_ITEM int output
AS
BEGIN
	-- Prepare #Temp for keep emp code, delegate, role of login user
	CREATE TABLE #TB_T_SCOPE(EMP_CODE VARCHAR(25), COMPANY VARCHAR(10), SP_ROLE VARCHAR(3))
	EXEC sp_WFD01110_SetScope @EMP_CODE

	SELECT	CASE WHEN @sortColumnName = '0' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 WHEN @sortColumnName = '1' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 
				 WHEN @sortColumnName = '2' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY DOC_NO asc)  
				 WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DOC_NO desc)  
				 WHEN @sortColumnName = '3' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY DISP_TYPE asc)
				 WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DISP_TYPE desc)
				 WHEN @sortColumnName = '4' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY ASSET_CNT asc)
				 WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_CNT desc)
				 
				 WHEN @sortColumnName = '5' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY TOTAL_NET_BOOK asc)
				 WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TOTAL_NET_BOOK desc)

				 WHEN @sortColumnName = '6' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COST_CODE asc) 
				 WHEN @sortColumnName = '6' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CODE desc) 
				 WHEN @sortColumnName = '7' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR asc) 
				 WHEN @sortColumnName = '7' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR desc)  
				 WHEN @sortColumnName = '8' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS asc) 
				 WHEN @sortColumnName = '8' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS desc)  
				 WHEN @sortColumnName = '9' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE asc) 
				 WHEN @sortColumnName = '9' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE desc)
				 ELSE ROW_NUMBER() OVER(ORDER BY DOC_NO ASC)    
			END AS ROWNUMBER,
			T.*
	INTO	#TB_T_RESULT
	FROM	
	(
	   SELECT	R.DOC_NO,
				R.COMPANY,
				H.DISP_TYPE,
				COUNT(1) AS ASSET_CNT,
				SUM(D.RETIRE_BOOK_VALUE) AS TOTAL_NET_BOOK,
				R.ASSET_COST_CODE AS COST_CODE,
				CONCAT(R.EMP_TITLE, ' ', R.EMP_NAME, ' ', R.EMP_LASTNAME)   AS REQUESTOR,
				dbo.fn_GetCurrentStatus(R.DOC_NO)			AS CURRENT_STATUS,
				R.REQUEST_DATE,
				R.REQUEST_TYPE
		FROM	[dbo].[TB_R_REQUEST_H] R
		INNER	JOIN 
				TB_R_REQUEST_APPR A
		on		R.DOC_NO = a.DOC_NO 
		INNER	JOIN  
				TB_R_REQUEST_DISP_H H
		ON		R.DOC_NO = H.DOC_NO
		LEFT	JOIN  
				TB_R_REQUEST_DISP_D D
		ON		R.DOC_NO = D.DOC_NO

		WHERE	R.REQUEST_TYPE	= 'D' AND 
				dbo.fn_IsEndRequest(R.[STATUS]) = 'N' AND
				A.APPR_STATUS	= 'W' AND 
				( EXISTS(	SELECT	1 
							FROM	dbo.fn_GetMultipleCompanyList(@COMPANY) C 
							WHERE	C.COMPANY_CODE = r.COMPANY ) OR @COMPANY IS NULL ) AND
				( EXISTS(	SELECT	1 
							FROM	dbo.fn_GetMultipleList(@DISP_TYPE) C 
							WHERE	C.[value] = h.DISP_TYPE ) OR @DISP_TYPE IS NULL ) AND
				-- Filter
				( EXISTS(	SELECT	1
							FROM	#TB_T_SCOPE T
							WHERE	T.EMP_CODE	= A.EMP_CODE ) OR
				  EXISTS(	SELECT	1
							FROM	#TB_T_SCOPE T
							WHERE	T.COMPANY	= R.COMPANY AND T.SP_ROLE = A.APPR_ROLE AND A.NOTIFICATION_MODE = 'G' )
				) 
				-- Add Emp code Condition
		GROUP	BY
				R.DOC_NO,
				R.COMPANY,
				R.ASSET_COST_CODE,
				R.EMP_CODE,
				R.EMP_TITLE,
				R.EMP_NAME,
				R.EMP_LASTNAME,
				H.DISP_TYPE,
				R.REQUEST_DATE,
				R.REQUEST_TYPE,				
				R.ASSET_COST_CODE
				
	)	T
	SET @TOTAL_ITEM = @@ROWCOUNT


	IF(@sortColumnName IS NULL )
	BEGIN
		SET @sortColumnName ='0';
	END
	IF(@orderType IS NULL )
	BEGIN
		SET @orderType ='asc';
	END

	DECLARE @FROM	INT , @TO	INT

	SET @FROM	= (@pageSize * (@pageNum-1)) + 1;
	SET @TO		= @pageSize * (@pageNum);						

	--begin modify by thanapon: fixed paging and sorting
	SELECT	d.DOC_NO,
			d.COMPANY,
			d.DISP_TYPE,
			d.ASSET_CNT,
			d.TOTAL_NET_BOOK,
			d.COST_CODE,
			d.REQUESTOR,
			d.REQUEST_TYPE,
			d.CURRENT_STATUS,
			dbo.fn_dateFAS(REQUEST_DATE) AS REQUEST_DATE,
			C.COST_NAME  AS COST_NAME_HINT
	FROM	#TB_T_RESULT d
	LEFT	JOIN
			TB_M_COST_CENTER C
	ON		d.COMPANY		= C.COMPANY AND
			d.COST_CODE		= C.COST_CODE
	
	WHERE	d.ROWNUMBER >= @FROM and 
			d.ROWNUMBER <= @TO
	ORDER	BY 
			d.ROWNUMBER
	
	IF OBJECT_ID('tempdb..#TB_T_RESULT') IS NOT NULL 
		DROP TABLE #TB_T_RESULT 
	IF OBJECT_ID('tempdb..#TB_T_SCOPE') IS NOT NULL 
		DROP TABLE #TB_T_SCOPE 

	
END
GO
