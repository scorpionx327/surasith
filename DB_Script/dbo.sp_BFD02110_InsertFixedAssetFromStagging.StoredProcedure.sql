DROP PROCEDURE [dbo].[sp_BFD02110_InsertFixedAssetFromStagging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Uten Sopradid
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  For insert Fixed Assets data in result table
- ============================================= SFAS =============================================*/
CREATE PROCEDURE [dbo].[sp_BFD02110_InsertFixedAssetFromStagging]
                                                            (
                                                                    @UserID        VARCHAR(8),
                                                                    @ERROR_MESSAGE VARCHAR(MAX) OUT
                                                            ) AS BEGIN TRY
        DECLARE 
                @Error BIT;
                SET @Error =0;
                -- Create temp TEMP_ASSETS
                IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
                BEGIN
                        DROP TABLE #TEMP_ASSETS
             END
  ---  0 Create structure of temp table
  SELECT ASSET_NO
  INTO   #TEMP_ASSETS
  FROM   TB_S_ASSETS_H
  WHERE  1<>1
  INSERT INTO #TEMP_ASSETS 
              ( 
                          ASSET_NO 
              )
  SELECT S_Asset.ASSET_NO
  FROM   TB_S_ASSETS_H S_Asset
  WHERE  NOT EXISTS 
         ( SELECT 1
         FROM    TB_M_ASSETS_H AssetH
         WHERE   AssetH.ASSET_NO = S_Asset.ASSET_NO
         )
         PRINT '-- 1  Insert into Asset Header'
  INSERT INTO TB_M_ASSETS_H 
              ( 
                          [ASSET_NO]                , 
                          [ASSET_NAME]              , 
                          [SUB_TYPE]                , 
                          [MODEL]                   , 
                          [SERIAL_NO]               , 
                          [TAG_NO]                  , 
                          [CLASS_SACCESS]           , 
                          [BOI_NO]                  , 
                          [PROJECT_NAME]            , 
                          [PLAN_COST]               , 
                          [PRODUCTION_PART_NO]      , 
                          [FA_BOOK]                 , 
                          [MACHINE_LICENSE]         , 
                          [ASSET_CATEGORY]          , 
                          [MINOR_CATEGORY]          , 
                          [ASSET_TYPE]              , 
                          [DATE_IN_SERVICE]         , 
                          [COST]                    , 
                          [NBV]                     , 
                          [TOOLING_COST]            , 
                          [BARCODE_SIZE]            , 
                          [UOP_CAPACITY]            , 
                          [UOP_LTD_PRODUCTION]      , 
                          [STL_LIFE_YEAR]           , 
                          [STL_REMAIN_LIFE_YEAR]    , 
                          [SALVAGE_VALUE]           , 
                          [YTD_DEPRECIATION]        , 
                          [ACCUMULATE_DEPRECIATION] , 
                          [RETIREMENT_DATE]         , 
                          [TRANSFER_DATE]           , 
                          [ENTRY_PERIOD]            , 
                          --[SUPPLIER_NO]             , --Update after insert success
                          --[SUPPLIER_NAME]           , --Update after insert success
                          --[LOCATION_NAME]           , --Update after insert success
                          [BARCODE]                 , 
                          [BOI_FLAG]                , 
                          [LICENSE_FLAG]            , 
                          [FULLY_DP_FLAG]           , 
                          [MAP_STATUS]              , 
                          [PHOTO_STATUS]            , 
                          [PRINT_STATUS]            , 
                          [STATUS]                  , 
                          [PROCESS_STATUS]          , 
                          [PRINT_COUNT]             , 
                          [TAG_PHOTO]               , 
                          [INHOUSE_FLAG]            , 
                          [LAST_SCAN_DATE]          , 
                          [TRNF_BOI_DECISION]       , 
                          [TRNF_BOI_ATTACH]         , 
                          [DISP_BOI_DECISION]       , 
                          [DISP_BOI_ATTACH]         , 
                          [CREATE_DATE]             , 
                          [CREATE_BY]               , 
                          [UPDATE_DATE]             , 
                          [UPDATE_BY] 
              )
  SELECT [ASSET_NO]			, 
         [ASSET_NAME]		, 
         [SUB_TYPE]			, 
         [MODEL]			, 
         [SERIAL_NO]		, 
         [TAG_NO]			, 
         [CLASS_SACCESS]	, 
         [BOI_NO]			, 
         [PROJECT_NAME]		, 
         CASE
                WHEN IsNumeric(S_Asset.[PLAN_COST])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[PLAN_COST]) AS DECIMAL(18,2))
                ELSE NULL 
         END                                         AS [PLAN_COST]      , 
         [PRODUCTION_PART_NO]											 , 
         [FA_BOOK]														 , 
         [MACHINE_LICENSE]												 , 
         LEFT(S_Asset.MAJOR_MINOR_CATEGORY,2)        AS [ASSET_CATEGORY] , 
         SUBSTRING(S_Asset.MAJOR_MINOR_CATEGORY,4,3) AS [MINOR_CATEGORY] , 
         [ASSET_TYPE]                , 
         CASE 
                WHEN ISNULL(S_Asset.[DATE_IN_SERVICE],'') <> '' 
                THEN CONVERT(DATETIME,S_Asset.[DATE_IN_SERVICE],106)
                ELSE NULL 
         END AS [DATE_IN_SERVICE] , 
         CASE 
                WHEN IsNumeric(S_Asset.[COST])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.COST) AS DECIMAL(18,2))
                ELSE NULL 
         END AS [COST] , 
         CASE 
                WHEN IsNumeric(S_Asset.[NBV])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[NBV]) AS DECIMAL(18,2))
                ELSE NULL 
         END [NBV] , 
         CASE 
                WHEN IsNumeric(S_Asset.[TOOLING_COST])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[TOOLING_COST]) AS DECIMAL(18,2))
                ELSE NULL 
         END                                               AS [TOOLING_COST] , 
         dbo.fn_GetBarcodeSize(S_Asset.[BARCODE_SIZE],'O') AS [BARCODE_SIZE]
         --CASE --- If 1 set 'L', 2 set 'M', 3 set 'S'
         -- WHEN [BARCODE_SIZE]='1' THEN 'L'
         -- WHEN [BARCODE_SIZE]='2' THEN 'M'
         -- WHEN [BARCODE_SIZE]='3' THEN 'S'
         -- ELSE NULL END AS [BARCODE_SIZE]
         , 
         CASE 
                WHEN IsNumeric(S_Asset.[UOP_CAPACITY])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[UOP_CAPACITY]) AS DECIMAL(10,2))
                ELSE NULL 
         END AS [UOP_CAPACITY] , 
         CASE 
                WHEN IsNumeric(S_Asset.[UOP_LTD_PRODUCTION])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[UOP_LTD_PRODUCTION]) AS DECIMAL(10,2))
                ELSE NULL 
         END AS [UOP_LTD_PRODUCTION] , 
         CASE 
                WHEN IsNumeric(S_Asset.[STL_LIFE_YEAR])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[STL_LIFE_YEAR]) AS DECIMAL(10,2))
                ELSE NULL 
         END AS [STL_LIFE_YEAR] , 
         CASE 
                WHEN IsNumeric(S_Asset.[STL_REMAIN_LIFE_YEAR])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[STL_REMAIN_LIFE_YEAR]) AS DECIMAL(10,2))
                ELSE NULL 
         END [STL_REMAIN_LIFE_YEAR] , 
         CASE 
                WHEN IsNumeric(S_Asset.[SALVAGE_VALUE])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[SALVAGE_VALUE]) AS DECIMAL(10,2))
                ELSE NULL 
         END AS [SALVAGE_VALUE] , 
         CASE 
                WHEN IsNumeric(S_Asset.[YTD_DEPRECIATION])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[YTD_DEPRECIATION]) AS DECIMAL(10,2))
                ELSE NULL 
         END AS [YTD_DEPRECIATION] , 
         CASE 
                WHEN IsNumeric(S_Asset.[ACCUMULATE_DEPRECIATION])=1 
                THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[ACCUMULATE_DEPRECIATION]) AS DECIMAL(36,2))
                ELSE NULL 
         END AS [ACCUMULATE_DEPRECIATION] , 
         CASE 
                WHEN ISNULL(S_Asset.[RETIREMENT_DATE],'') <> '' 
                THEN CONVERT(DATETIME,S_Asset.[RETIREMENT_DATE],106)
                ELSE NULL 
         END AS [RETIREMENT_DATE] , 
         CASE 
                WHEN ISNULL(S_Asset.[TRANSFER_DATE],'') <> '' 
                THEN CONVERT(DATETIME,S_Asset.[TRANSFER_DATE],106)
                ELSE NULL 
         END AS [TRANSFER_DATE] , 
         CASE 
                WHEN ISNULL(S_Asset.[ENTRY_PERIOD],'') <> '' 
                THEN CONVERT(DATETIME,S_Asset.[ENTRY_PERIOD],106)
                ELSE NULL 
         END AS            [ENTRY_DATE] , 

/*  -------------- Update after insert data ----------------
         ( SELECT  TOP (1) SUPPLIER_NO
         FROM     TB_S_ASSETS_FINANCE S_AssetFinance
         WHERE    S_Asset.ASSET_NO=S_AssetFinance.ASSET_NO
		 AND      ISNULL(SUPPLIER_NO, '') != ''
		 ORDER BY CONVERT(decimal(18,2), INVOICE_COST) DESC 
         ) 
         AS                [SUPPLIER_NO] , 
         ( SELECT  TOP (1) SUPPLIER_NAME
         FROM     TB_S_ASSETS_FINANCE S_AssetFinance
         WHERE    S_Asset.ASSET_NO=S_AssetFinance.ASSET_NO
         AND      ISNULL(SUPPLIER_NO, '') != ''
		 ORDER BY CONVERT(decimal(18,2), INVOICE_COST) DESC
         ) 
         AS                         [SUPPLIER_NAME] , 
         ( SELECT  TOP (1) [LOCATION]
         FROM     TB_S_ASSETS_D S_AssetD
         WHERE    S_Asset.ASSET_NO=S_AssetD.ASSET_NO
         ORDER BY EMP_CODE DESC, UNIT DESC
         ) 
                                                    AS [LOCATION_NAME] , 
*/
         dbo.fn_GetBarCodeOfAsset(S_Asset.ASSET_NO) AS [BARCODE]       , 
         CASE -- If digit 8th of Asset No. (B.1(ASSET_NO)) = 'B', set to 'Y' otherwise set to 'N'
                WHEN SUBSTRING(S_Asset.ASSET_NO,8,1) = 'B' 
                THEN 'Y'
                ELSE 'N' 
         END AS [BOI_FLAG] , 
         CASE --If E.13(MACHINE_LICENSE) is not blank, set to 'Y' otherwise set to 'N'                              	
                WHEN ISNULL(S_Asset.MACHINE_LICENSE,'') <> '' 
                THEN 'Y'
                ELSE 'N' 
         END AS [LICENSE_FLAG] , 
              CASE  -- If E.17(COST) <= (E.25(SALVAGE_VALUE) + E.27(ACCUMULATE_DEPRECIATION)), set to 'Y', otherwise set to 'N'
								WHEN CAST(dbo.fn_GetStringOfNumber(S_Asset.COST) AS decimal(18,2)) < = (CAST(dbo.fn_GetStringOfNumber(S_Asset.SALVAGE_VALUE)AS decimal(18,2))  + CAST(dbo.fn_GetStringOfNumber(S_Asset.ACCUMULATE_DEPRECIATION) AS decimal(18,2))) THEN 'Y'
								ELSE 'N' END   AS [FULLY_DP_FLAG]  , 
         'N'  AS [MAP_STATUS]     , 
         'N'  AS [PHOTO_STATUS]   , 
         'N'  AS [PRINT_STATUS]   , 
			-- Change Spec Surasith 2017-06-16 Set Default is 'N'
			'N' [STATUS],

		 -- Update 2017-05-12 : Change from Y to check retirement date
		 --CASE WHEN ISNULL(S_Asset.[RETIREMENT_DATE],'') <> '' THEN 'N'
		--					       ELSE 'Y' END  AS [STATUS], 



         NULL   AS [PROCESS_STATUS] , 
         0    AS [PRINT_COUNT]    , 
         NULL AS [TAG_PHOTO]      , 
		 'Y'  AS [INHOUSE_FLAG]      ,  -- Update as below 
         NULL AS [LAST_SCAN_DATE]    , 
         NULL AS [TRNF_BOI_DECISION] , 
         NULL AS [TRNF_BOI_ATTACH]   , 
         NULL AS [DISP_BOI_DECISION] , 
         NULL AS [DISP_BOI_ATTACH]   , 
         GETDATE()                   , 
         @UserID                     , 
         GETDATE()                   , 
         @UserID
  FROM   TB_S_ASSETS_H S_Asset
  WHERE  EXISTS  ( SELECT 1 
					 FROM    #TEMP_ASSETS T_Asset
					 WHERE   T_Asset.ASSET_NO = S_Asset.ASSET_NO 
					 )
		 --- Incase Plan cost is not nummeric
	     AND ASSET_NO NOT IN ( SELECT ASSET_NO
		                       FROM TB_S_ASSETS_H
							     WHERE [PLAN_COST] IS NOT NULL 
							           AND (      IsNumeric([PLAN_COST])=0 
									           OR ltrim(rtrim([PLAN_COST]))='-')
											   )
---------------------------------------------------------------------------------------------
-- Update I/H Flag of new assets default is Y
UPDATE	M
SET		INHOUSE_FLAG = 'N'
FROM	TB_M_ASSETS_H M 
INNER	JOIN
		#TEMP_ASSETS T
ON		M.ASSET_NO = T.ASSET_NO
WHERE	ASSET_CATEGORY IN (	SELECT	[VALUE] 
							FROM	TB_M_SYSTEM
							WHERE	CATEGORY		= 'ADMIN_ASSET' AND 
									SUB_CATEGORY	= 'ASSET_CATEGORY' AND 
									ACTIVE_FLAG		= 'Y' )

-- Update Status of CIP
DECLARE @CIPCode VARCHAR(40)
SET @CIPCode = dbo.fn_GetSystemMaster('FAS_TYPE','ASSET_TYPE','CIP')

UPDATE	M
SET		PRINT_STATUS	= 'Y',
		MAP_STATUS		= 'Y',
		PHOTO_STATUS	= 'Y'
FROM	TB_M_ASSETS_H M 
INNER	JOIN
		#TEMP_ASSETS T
ON		M.ASSET_NO = T.ASSET_NO
WHERE	M.ASSET_TYPE = @CIPCode AND INHOUSE_FLAG = 'Y'
---------------------------------------------------------------------------------------------










  -------UPDATE  [LOCATION_NAME]    
   UPDATE M
   SET M.LOCATION_NAME = D.LOCATION
   FROM TB_M_ASSETS_H M INNER JOIN (    SELECT ASSET_NO, LOCATION 
										  FROM (SELECT ROW_NUMBER() OVER(PARTITION BY ASSET_NO	, 
																							  SUBSTRING(EXPENSE_ACCOUNT, 15, 8) 
																				 ORDER BY CASE
																						  WHEN EMP_CODE IS NULL THEN '0'
																						  ELSE '1' END DESC, UNIT DESC,EMP_CODE ASC    
																						  ) AS ROWNUMBER ,
																			ASSET_NO, LOCATION
																FROM	TB_S_ASSETS_D 
																) AssetD
														  WHERE	ROWNUMBER = '1'
									  )D ON M.ASSET_NO=D.ASSET_NO
   WHERE  EXISTS  ( SELECT 1 
					 FROM    #TEMP_ASSETS T_Asset
					 WHERE   T_Asset.ASSET_NO = M.ASSET_NO 
					 )



         PRINT '  -- 2  Delete Asset Details ( Asset Details / Asset Finance )' 
		 
		 PRINT '     -- 2.1 Delete Asset Details '
  DELETE AssetD
  FROM   TB_M_ASSETS_D AssetD
  WHERE  EXISTS 
         ( SELECT 1 
         FROM    #TEMP_ASSETS T_Asset
         WHERE   T_Asset.ASSET_NO = AssetD.ASSET_NO 
         )
         PRINT '	  --- Delete 2.2 Asset Finance'
  DELETE AssetFinance
  FROM   TB_M_ASSETS_FINANCE AssetFinance
  WHERE  EXISTS 
         ( SELECT 1 
         FROM    #TEMP_ASSETS T_Asset
         WHERE   T_Asset.ASSET_NO = AssetFinance.ASSET_NO 
         )
         PRINT '-- 3  Insert Asset Detaisl '
  INSERT INTO TB_M_ASSETS_D 
              ( 
                          ASSET_NO        , 
                          COST_CODE       , 
                          EMP_CODE        , 
                          EMP_NAME        , 
                          EXPENSE_ACCOUNT , 
                          UNIT            , 
                          CREATE_DATE     , 
                          CREATE_BY       , 
                          UPDATE_DATE     , 
                          UPDATE_BY 
              )
  SELECT ASSET_NO                                                , 
         SUBSTRING(ISNULL(EXPENSE_ACCOUNT,''),15,8) AS COST_CODE , 
         EMP_CODE                                                , 
         EMP_NAME                                                , 
         EXPENSE_ACCOUNT                                         , 
         CASE
                WHEN UNIT IS NOT NULL 
                THEN CAST(dbo.fn_GetStringOfNumber(UNIT) AS DECIMAL(18,4))
                ELSE NULL 
         END AS UNIT , 
         GETDATE()   , 
         @UserID     , 
         GETDATE()   , 
         @UserID
  FROM   TB_S_ASSETS_D S_Asset
  WHERE  EXISTS 
         ( SELECT 1 
         FROM    #TEMP_ASSETS T_Asset
         WHERE   T_Asset.ASSET_NO = S_Asset.ASSET_NO 
         )
         PRINT '-- 4  Insert Asset Finance '
  INSERT INTO TB_M_ASSETS_FINANCE 
              (            ROW_NO,
                          ASSET_NO      , 
                          INVOICE_NO    , 
                          INVOICE_DATE  , 
                          INVOICE_LINE  , 
                          SUPPLIER_NO   , 
                          SUPPLIER_NAME , 
                          [DESCRIPTION] , 
                          INVOICE_COST  , 
                          PO_NO         , 
                          CREATE_DATE   , 
                          CREATE_BY     , 
                          UPDATE_DATE   , 
                          UPDATE_BY 
              )
  SELECT     ROW_NO, ASSET_NO   , 
         INVOICE_NO , 
         CASE
                WHEN ISNULL(S_Asset.INVOICE_DATE,'') <> '' 
                THEN CONVERT(DATE,S_Asset.INVOICE_DATE,106)
                ELSE NULL 
         END AS INVOICE_DATE , 
         INVOICE_LINE        , 
         SUPPLIER_NO         , 
         SUPPLIER_NAME       , 
         [DESCRIPTION]       , 
         CASE
                WHEN IsNumeric(INVOICE_COST)=1THEN CAST(dbo.fn_GetStringOfNumber(INVOICE_COST) AS DECIMAL(18,2))
                ELSE NULL 
         END AS INVOICE_COST , 
         PO_NO               , 
         GETDATE()           , 
         @UserID             , 
         GETDATE()           , 
         @UserID
  FROM   TB_S_ASSETS_FINANCE S_Asset
  WHERE  EXISTS 
         ( SELECT 1 
         FROM    #TEMP_ASSETS T_Asset
         WHERE   T_Asset.ASSET_NO = S_Asset.ASSET_NO 
         )
         PRINT '-- 5 Insert  TB_M_ASSETS_LOC'
  INSERT INTO TB_M_ASSETS_LOC
              ( 
                          [ASSET_NO]    , 
                          [X]           , 
                          [Y]           , 
                          [CREATE_DATE] , 
                          [CREATE_BY]   , 
                          [UPDATE_DATE] , 
                          [UPDATE_BY] 
              )
  SELECT [ASSET_NO] , 
         NULL       , 
         NULL       , 
         GETDATE()  , 
         @UserID    , 
         GETDATE()  , 
         @UserID
  FROM   #TEMP_ASSETS T_Asset
    WHERE NOT EXISTS(SELECT 1 
				    FROM TB_M_ASSETS_LOC Loc
					WHERE T_Asset.ASSET_NO=Loc.ASSET_NO)
         -- Create temp TEMP_ASSETS
         IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL BEGIN
  DROP TABLE #TEMP_ASSETS
END RETURN @Error ;
 
END TRY BEGIN CATCH
-- Create temp TEMP_ASSETS
IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
        BEGIN
                DROP TABLE #TEMP_ASSETS
     END PRINT concat('ERROR Message:',ERROR_MESSAGE()) SET @ERROR_MESSAGE=ERROR_MESSAGE();
 
RETURN 1;
END CATCH




GO
