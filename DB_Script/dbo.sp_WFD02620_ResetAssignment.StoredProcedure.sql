DROP PROCEDURE [dbo].[sp_WFD02620_ResetAssignment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 15-02-2017
-- Description:	Reser assignment
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_ResetAssignment]
	 @UPDATE_BY			T_SYS_USER
	, @YEAR				varchar(4)
	, @ROUND			varchar(2)
	, @ASSET_LOCATION	varchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE	TB_R_STOCK_TAKE_D_PER_SV
		SET DATE_FROM = NULL
			,DATE_TO = NULL
			,TIME_START = NULL
			,TIME_END = NULL
			,TIME_MINUTE = NULL	
			,USAGE_HANDHELD = 0
			,UPDATE_BY = @UPDATE_BY
			,UPDATE_DATE = GETDATE()
			,FA_DATE_FROM = NULL
			,FA_DATE_TO = NULL
	WHERE YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION

	 -- update plan status to draft 
	 UPDATE TB_R_STOCK_TAKE_H
	 SET PLAN_STATUS ='D'
	 	  ,UPDATE_BY = @UPDATE_BY
		--	,UPDATE_DATE = GETDATE() Don't Update
	WHERE YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION


END




GO
