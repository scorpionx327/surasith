DROP PROCEDURE [dbo].[sp_Common_InsertFileDownload]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[sp_Common_InsertFileDownload]
(
@AppID			INT,
@BatchID		VARCHAR(25),
@FileName		VARCHAR(255),
@User		VARCHAR(8)
)
AS
BEGIN

DECLARE @DocNo VARCHAR(25) 
SET @DocNo = @BatchID + RIGHT(CONCAT('00000000000000000000',@AppID),14)
PRINT @FileName

INSERT INTO dbo.TB_R_FILE_DOWNLOAD
           (APP_ID ,DOC_NO ,FILE_STATUS ,FILE_PATH ,CREATE_BY ,CREATE_DATE)
     VALUES
           (@AppID ,@DocNo ,'D',@FileName,@User,GETDATE())

END







GO
