DROP PROCEDURE [dbo].[sp_Common_GetBatchProcessId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 18-03-2017
-- Description:	Get process id from batch queue by app id
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_GetBatchProcessId]
	-- Add the parameters for the stored procedure here
	@APP_ID	numeric(18, 0)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	TOP 1 PID FROM TB_BATCH_Q
	WHERE APP_ID=@APP_ID

END



GO
