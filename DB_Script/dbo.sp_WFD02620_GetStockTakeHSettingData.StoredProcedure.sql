DROP PROCEDURE [dbo].[sp_WFD02620_GetStockTakeHSettingData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-14
-- Description:	Get setting section data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetStockTakeHSettingData]
	-- Add the parameters for the stored procedure here
	 @COMPANY			T_COMPANY
	, @YEAR				varchar(4)
	, @ROUND			varchar(2)
	, @ASSET_LOCATION	varchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		top 1
		h.CREATE_BY
		, h.STOCK_TAKE_KEY
		, ISNULL(LTRIM(RTRIM(e.EMP_NAME)), '') as EMP_NAME
		, ISNULL(SUBSTRING(LTRIM(RTRIM(e.EMP_LASTNAME)), 0, 2) + '.', '') as EMP_LASTNAME
		, h.COMPANY
		, h.YEAR
		, h.ROUND
		, h.ASSET_LOCATION
		, h.QTY_OF_HANDHELD
		--, h.SUB_TYPE
		, h.TARGET_DATE_FROM
		, h.TARGET_DATE_TO
		, h.DATA_AS_OF
		, h.BREAK_TIME_MINUTE
		, ASSET_STATUS
		, h.UPDATE_DATE
		, h.PLAN_STATUS

		, h.ASSET_CLASS
		, h.MINOR_CATEGORY
		, h.COST_CENTER
		, h.RESPONSE_CC
		, h.INVEST_REASON
		, h.INVEN_INDICATOR



	
	FROM [dbo].[TB_R_STOCK_TAKE_H] h
	LEFT JOIN TB_M_EMPLOYEE e on h.CREATE_BY=e.EMP_CODE
	WHERE  h.COMPANY=@COMPANY and h.YEAR=@YEAR and h.ROUND=@ROUND and h.ASSET_LOCATION=@ASSET_LOCATION

END




GO
