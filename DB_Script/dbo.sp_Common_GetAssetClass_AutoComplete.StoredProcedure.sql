DROP PROCEDURE [dbo].[sp_Common_GetAssetClass_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetAssetClass_AutoComplete]
@CodeFlag		VARCHAR(1),
@Company		VARCHAR(100),
@keyword		VARCHAR(40)
AS
BEGIN
	

	SELECT	MS.CODE, MS.[VALUE]
	FROM	TB_M_DEPRECIATION M
	INNER	JOIN
			TB_M_SYSTEM MS
	ON		M.ASSET_CLASS	= MS.CODE AND
			MS.SUB_CATEGORY = 'ASSET_CLASS' AND
			MS.CATEGORY		= 'ASSET_CLASS'
	WHERE	EXISTS(SELECT 1 FROM dbo.fn_GetMultipleCompanyList(@Company) F WHERE F.COMPANY_CODE = M.COMPANY) AND
			(	(@CodeFlag = 'Y' AND CODE			LIKE CONCAT(@keyword,'%')) OR
				(@CodeFlag <> 'Y' AND [VALUE]		LIKE CONCAT(@keyword,'%'))
			)
	GROUP	BY
			MS.CODE, MS.[VALUE]
	ORDER	BY
			CASE @CodeFlag WHEN 'Y' THEN CODE ELSE [VALUE] END
END
GO
