DROP PROCEDURE [dbo].[sp_WFD02620_GetResponsibleCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================WFD01120===================
-- Author:		<Uten S.>
-- Create date: <2017/03/11> 
-- Description:	for WFD01120 get cost center for general report 
-- =============================================
CREATE PROCEDURE  [dbo].[sp_WFD02620_GetResponsibleCostCenter]

AS
BEGIN
SET NOCOUNT ON;
   SELECT FA.COST_CODE,CONCAT( FA.COST_CODE,':', M.COST_NAME) AS COST_NAME 
   FROM TB_M_FASUP_COST_CENTER FA
   INNER JOIN TB_M_COST_CENTER M
   ON FA.COST_CODE = M.COST_CODE
   ORDER BY M.COST_NAME
END
GO
