DROP PROCEDURE [dbo].[sp_WFD01180_GetMinorCategory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01180_GetMinorCategory]
(
		@COMPANY T_COMPANY= NULL		--COMPANY
)
AS
BEGIN
	SET NOCOUNT ON;

	--select null,null
	--UNION all
	select CODE,[VALUE] from TB_M_SYSTEM 
	where CATEGORY ='EVA_GRP_1' 
		and SUB_CATEGORY IN (SELECT REPLACE(REPLACE(REPLACE(Value,'[',''),']',''),'"','') FROM fn_Split(@COMPANY, ','))
		AND ACTIVE_FLAG = 'Y'
END
GO
