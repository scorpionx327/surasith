DROP PROCEDURE [dbo].[sp_WFD01150_GetApprove_H]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_GetApprove_H]
(
		@COMPANY T_COMPANY,
		@REQUEST_FLOW_TYPE varchar(2)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MyTableVar table(  
							APPRV_ID int NOT NULL,  
							ASSET_CLASS T_ASSET_CLASS NOT NULL,  
							ASSET_CLASS_NAME varchar(400) NOT NULL, 
							T01 varchar(3),
							T02 varchar(3),
							T20 varchar(3),
							T30 varchar(3),
							T40 varchar(3),
							T41 varchar(3),
							T50 varchar(3),
							T70 varchar(3),
							T71 varchar(3)
					); 

	DECLARE @APPRV_ID INT,@ASSET_CLASS T_ASSET_CLASS,@ASSET_CLASS_NAME varchar(400);
	DECLARE cursor_results CURSOR FOR 
		select H.APPRV_ID,
			H.ASSET_CLASS,
			MC.VALUE as ASSET_CLASS_NAME
		from TB_M_APPROVE_H H
		inner join tb_m_system MC on H.ASSET_CLASS = MC.CODE 
			and mc.category  = 'ASSET_CLASS' 
			and MC.sub_category = 'ASSET_CLASS' 
			and MC.active_flag = 'Y' 
		where (H.COMPANY = @COMPANY or @COMPANY is null)
			and (H.REQUEST_FLOW_TYPE = @REQUEST_FLOW_TYPE or @REQUEST_FLOW_TYPE is null);
 
	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @APPRV_ID,@ASSET_CLASS,@ASSET_CLASS_NAME
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		DECLARE @01 varchar(3);
		DECLARE @02 varchar(3);
		DECLARE @20 varchar(3);
		DECLARE @30 varchar(3);
		DECLARE @40 varchar(3);
		DECLARE @41 varchar(3);
		DECLARE @50 varchar(3);
		DECLARE @70 varchar(3);
		DECLARE @71 varchar(3);
		SET @01 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =1)
		SET @02 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =2)
		SET @20 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =20)
		SET @30 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =30)
		SET @40 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =40)
		SET @41 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =41)
		SET @50 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =50)
		SET @70 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =70)
		SET @71 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =71)

		INSERT INTO @MyTableVar(APPRV_ID,ASSET_CLASS,ASSET_CLASS_NAME,T01,T02,T20,T30,T40,T41,T50,T70,T71)
		VALUES(@APPRV_ID,@ASSET_CLASS,@ASSET_CLASS_NAME,isnull(@01,''),isnull(@02,''),isnull(@20,''),isnull(@30,''),isnull(@40,''),isnull(@41,''),isnull(@50,''),isnull(@70,''),isnull(@71,''))
	FETCH NEXT FROM cursor_results into @APPRV_ID,@ASSET_CLASS,@ASSET_CLASS_NAME
	END
 
	CLOSE cursor_results;
	DEALLOCATE cursor_results;

	select	ROW_NUMBER() OVER(ORDER BY APPRV_ID) AS NO,
			APPRV_ID ,  
			ASSET_CLASS,  
			ASSET_CLASS_NAME, 
			T01,
			T02,
			T20,
			T30,
			T40,
			T41,
			T50,
			T70,
			T71
	from @MyTableVar

END
GO
