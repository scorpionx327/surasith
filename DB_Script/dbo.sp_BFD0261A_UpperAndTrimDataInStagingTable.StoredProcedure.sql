DROP PROCEDURE [dbo].[sp_BFD0261A_UpperAndTrimDataInStagingTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)

- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_BFD0261A_UpperAndTrimDataInStagingTable]
AS
BEGIN 
UPDATE  TB_S_STOCK_TAKE 
SET     ASSET_NO						= UPPER(LTRIM(RTRIM(ASSET_NO)))
END 








GO
