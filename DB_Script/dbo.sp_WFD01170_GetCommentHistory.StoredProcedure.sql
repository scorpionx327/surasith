DROP PROCEDURE [dbo].[sp_WFD01170_GetCommentHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD01170_GetCommentHistory]
(
	@DOC_NO			T_DOC_NO
)
	
AS
BEGIN
	DECLARE @PATH		VARCHAR(400)
	SET @PATH = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','IMAGE_PATH','IMAGE_UPLOAD')

	SELECT	COMMENT_HISTORY_ID, 
			DOC_NO,			
			INDX,			
			C.EMP_CODE,					
			CONCAT(E.EMP_NAME, ' ', SUBSTRING(E.EMP_LASTNAME,1,1),'.') AS EMP_NAME, 
			STATUS,			
			COMMENT,		
			ATTACH_FILE,		
			dbo.fn_dateTimeFAS(C.CREATE_DATE) CREATE_DATE, 
			C.CREATE_BY
	FROM	TB_R_REQUEST_COMMENT C WITH (NOLOCK)
	LEFT	JOIN 
			TB_M_EMPLOYEE E ON C.CREATE_BY = E.SYS_EMP_CODE 
	WHERE	DOC_NO = @DOC_NO
	ORDER	BY 
			COMMENT_HISTORY_ID

END
GO
