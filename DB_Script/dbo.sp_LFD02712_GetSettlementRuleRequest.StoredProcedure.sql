DROP PROCEDURE [dbo].[sp_LFD02712_GetSettlementRuleRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nipon>
-- Create date: 2019-09-05 11:50:45.940
-- Description:	<Export Settlement Rule Request screen to excel>
-- =============================================

--exec sp_LFD02712_GetSettlementRuleRequest 'stm','96ab731e-d28a-4a19-ae1c-deb4a6e3f214' 
CREATE PROCEDURE [dbo].[sp_LFD02712_GetSettlementRuleRequest]
(	
	--@COMPANY	T_COMPANY,
	@GUID		T_GUID
)
AS	
	declare @COUNT_DOC as int

	select @COUNT_DOC=count(*) from TB_R_REQUEST_H where [GUID]	= @GUID 


	if (@COUNT_DOC>0)
	begin
		-- Get from submit mode
		SELECT	distinct H.DOC_NO,
			dbo.fn_GetCurrentStatus(H.DOC_NO) AS CURRENT_STATUS,
			dbo.fn_dateFAS(H.REQUEST_DATE) REQUEST_DATE,
			H.COMPANY,
			H.EMP_CODE,		
			ISNULL(H.EMP_TITLE,'') + ' ' + H.EMP_NAME + ' ' + H.EMP_LASTNAME EMP_NAME,	
			H.PHONE_NO TEL_NO,	
			H.COST_CODE + ' : ' + C.COST_NAME COST_NAME,	
			H.POS_CODE + ' : ' + H.POS_NAME POS_NAME,					
			H.DEPT_NAME,																
			H.SECT_NAME,
			R.ASSET_NO,
			R.ASSET_SUB,
			A.ASSET_NAME,
			A.ASSET_CLASS + M.VALUE ASSET_CLASS,
			--ASSET LIST
			R.AUC_NO,
			R.AUC_SUB,
			R.PO_NO,
			R.INV_NO,
			R.INV_LINE,
			R.INV_DESC,
			R.AUC_AMOUNT,
			--R.CAP_DATE,
			D.CAP_DATE,
			R.SETTLE_AMOUNT
		FROM TB_R_REQUEST_H H 	
		INNER JOIN TB_T_REQUEST_SETTLE_H R ON H.DOC_NO = R.DOC_NO 
		left JOIN TB_T_REQUEST_SETTLE_D D ON R.DOC_NO = D.DOC_NO 
		and D.TEMP_ASSET_NO=R.ASSET_NO 
		LEFT JOIN TB_M_COST_CENTER C ON H.COST_CODE = C.COST_CODE 
			AND H.COMPANY = C.COMPANY
		LEFT JOIN TB_M_ASSETS_H A ON R.COMPANY = H.COMPANY 
			AND R.ASSET_NO = A.ASSET_NO 
			AND R.ASSET_SUB = A.ASSET_SUB
		LEFT JOIN TB_M_SYSTEM M ON A.ASSET_CLASS = M.CODE 
			AND M.CATEGORY = 'ASSET_CLASS' 
			AND SUB_CATEGORY='ASSET_CLASS'
		WHERE	R.[GUID] = @GUID 
		--AND  R.COMPANY = @COMPANY
	end 
	else
	begin
		-- Get from submit mode
		SELECT	distinct R.DOC_NO,
			dbo.fn_GetCurrentStatus(R.DOC_NO) AS CURRENT_STATUS,
			dbo.fn_dateFAS(R.CREATE_DATE) REQUEST_DATE,
			R.COMPANY,
			E.EMP_CODE   EMP_CODE,		
			ISNULL(E.EMP_TITLE,'') + ' ' + E.EMP_NAME + ' ' + E.EMP_LASTNAME EMP_NAME,	
			E.TEL_NO TEL_NO,	
			E.COST_CODE + ' : ' + C.COST_NAME COST_NAME,	
			E.POST_CODE + ' : ' + E.POST_NAME POS_NAME,					
			O.DEPT_NAME  DEPT_NAME,																
			O.SECTION_NAME  SECT_NAME,
			R.ASSET_NO,
			R.ASSET_SUB,
			A.ASSET_NAME,
			A.ASSET_CLASS + M.VALUE ASSET_CLASS,
			--ASSET LIST
			R.AUC_NO,
			R.AUC_SUB,
			R.PO_NO,
			R.INV_NO,
			R.INV_LINE,
			R.INV_DESC,
			R.AUC_AMOUNT,
			--R.CAP_DATE,
			D.CAP_DATE,
			R.SETTLE_AMOUNT
		FROM  TB_T_REQUEST_SETTLE_H R  
		left JOIN TB_T_REQUEST_SETTLE_D D ON R.DOC_NO = D.DOC_NO 
		and D.TEMP_ASSET_NO=R.ASSET_NO  
		left join
		TB_M_EMPLOYEE E on
		E.SYS_EMP_CODE=R.CREATE_BY
		left join 
		TB_M_ORGANIZATION O on
		E.ORG_CODE=O.ORG_CODE 
		LEFT JOIN TB_M_COST_CENTER C ON E.COST_CODE = C.COST_CODE 
			AND R.COMPANY = C.COMPANY
	 
		LEFT JOIN TB_M_ASSETS_H A ON  
		    R.ASSET_NO = A.ASSET_NO 
			AND R.ASSET_SUB = A.ASSET_SUB
		LEFT JOIN TB_M_SYSTEM M ON A.ASSET_CLASS = M.CODE 
			AND M.CATEGORY = 'ASSET_CLASS' 
			AND SUB_CATEGORY='ASSET_CLASS'
		WHERE	R.[GUID] = @GUID 
		--AND  R.COMPANY = @COMPANY

	end 
	

GO
