DROP PROCEDURE [dbo].[sp_WFD01250_SearchApproveByRequestType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 08-03-2917
-- Description:	Search approve by request type
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_SearchApproveByRequestType]
	-- Add the parameters for the stored procedure here
	@COMPANY			T_COMPANY,
	@REQUEST_FLOW_TYPE varchar(2)
	, @CURRENCT_PAGE	int
	, @PAGE_SIZE		int
	, @ORDER_BY			NVARCHAR(50)
	, @TOTAL_ITEM		int output
AS
	DECLARE @FROM	INT
			, @TO	INT
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @FROM = (@PAGE_SIZE * (@CURRENCT_PAGE-1)) + 1;
	SET @TO = @PAGE_SIZE * (@CURRENCT_PAGE);

	SELECT	@TOTAL_ITEM = COUNT(1)
	FROM	TB_M_APPROVE_H H
	LEFT	JOIN 
			TB_M_SYSTEM S 
	ON		H.ASSET_CLASS		= S.CODE AND
			S.CATEGORY			= 'ASSET_CLASS' AND 
			S.SUB_CATEGORY		= 'ASSET_CLASS' AND
			S.ACTIVE_FLAG		= 'Y'
	WHERE	H.COMPANY			= @COMPANY AND
			H.REQUEST_FLOW_TYPE	= @REQUEST_FLOW_TYPE

	SELECT
		*
	FROM (
		SELECT
			ROW_NUMBER() over (ORDER BY CASE @ORDER_BY
											WHEN '3' THEN data.FIXED_ASSET_CATEGORY
											ELSE data.APPRV_ID
										END)as NO
			, data.APPRV_ID
			, data.REQUEST_TYPE
			, data.ASSET_CLASS
			, data.FIXED_ASSET_CATEGORY
			, data.UPDATE_BY
			, data.UPDATE_DATE
		FROM (
			SELECT
				  h.APPRV_ID
				, h.REQUEST_TYPE
				, h.ASSET_CLASS
				, s.VALUE as FIXED_ASSET_CATEGORY
				, h.UPDATE_BY
				, h.UPDATE_DATE
			FROM TB_M_APPROVE_H h
			LEFT	JOIN 
			TB_M_SYSTEM S 
			ON		H.ASSET_CLASS		= S.CODE AND
					S.CATEGORY			= 'ASSET_CLASS' AND 
					S.SUB_CATEGORY		= 'ASSET_CLASS' AND
					S.ACTIVE_FLAG		= 'Y'
			WHERE	H.COMPANY			= @COMPANY AND
					H.REQUEST_FLOW_TYPE	= @REQUEST_FLOW_TYPE
			) data 
			) d
	WHERE d.NO >= @FROM and d.NO <= @TO

	
	-- Common Flow
	select		ROW_NUMBER() over (ORDER BY d.INDX) as NO
				, d.APPRV_ID
				, d.INDX
				, d.ROLE as ROLE_CODE
				, r.VALUE as ROLE_NAME
	from		TB_M_APPROVE_D d
	left		join 
				TB_M_SYSTEM af on af.CATEGORY='APPROVE_FLOW' 
	and			af.SUB_CATEGORY	=	@REQUEST_FLOW_TYPE and 
				CONVERT(int, af.CODE)=d.INDX
	left join	TB_M_SYSTEM r 
	on			r.CATEGORY='RESPONSIBILITY' and 
				r.SUB_CATEGORY='ROLE_CODE' and 
				r.CODE=d.ROLE
	WHERE d.APPRV_ID in (SELECT h.APPRV_ID FROM TB_M_APPROVE_H h WHERE h.COMPANY = @COMPANY AND h.REQUEST_FLOW_TYPE = @REQUEST_FLOW_TYPE)



	select 
		SUB_CATEGORY as REQUEST_TYPE, CONVERT(int, CODE) as CODE
	from TB_M_SYSTEM
	where CATEGORY='APPROVE_FLOW' and SUB_CATEGORY=@REQUEST_FLOW_TYPE
	order by SUB_CATEGORY, CONVERT(int, CODE)
	
END
GO
