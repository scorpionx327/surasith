DROP PROCEDURE [dbo].[sp_LFD02170_GetCostCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_GetCostCode]
@EMP_CODE VARCHAR(8)
AS
BEGIN	
	SET NOCOUNT ON;

	    SELECT COST_CODE FROM  dbo.FN_FD0COSTCENTERLIST (@EMP_CODE)
		ORDER BY COST_CODE 

END


GO
