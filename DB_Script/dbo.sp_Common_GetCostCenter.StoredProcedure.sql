DROP PROCEDURE [dbo].[sp_Common_GetCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suphachai L.>
-- Create date: <2017/01/31> เวลา 12:00
-- Description:	<Export reprint request screen to excel>
-- =============================================
--exec sp_LFD02330_GetReprintRequestData 'P2017/0001'
CREATE PROCEDURE [dbo].[sp_Common_GetCostCenter]
(	
	@EMPLOYEE_CODE	VARCHAR(10) NULL
)
AS
BEGIN
SET NOCOUNT ON;

	SELECT [COST_CODE]
      ,[COST_NAME]
    
      ,[MAP_PATH]
      ,[ENABLE_FLAG]
      ,[EFFECTIVE_FROM]
      ,[EFFECTIVE_TO]
      ,[STATUS]
   
      ,[CREATE_DATE]
      ,[CREATE_BY]
      ,[UPDATE_DATE]
      ,[UPDATE_BY]
  FROM [TB_M_COST_CENTER] C
  WHERE	C.COST_CODE IN (SELECT CC.COST_CODE FROM dbo.FN_FD0COSTCENTERLIST(@EMPLOYEE_CODE) CC)


END



GO
