DROP PROCEDURE [dbo].[sp_WFD01270_GetTempDocCommentHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD01270_GetTempDocCommentHistory]
(
	@DOC_NO						VARCHAR(10)
)
	
AS
BEGIN
	DECLARE @PATH		VARCHAR(400)
	SELECT TOP 1 @PATH = VALUE FROM TB_M_SYSTEM 
	WHERE CATEGORY = 'SYSTEM_CONFIG' AND SUB_CATEGORY = 'IMAGE_PATH' AND CODE = 'IMAGE_UPLOAD'

	SELECT COMMENT_HISTORY_ID, 
	DOC_NO,			INDX,			C.EMP_CODE,						E.EMP_NAME + ' ' + SUBSTRING(E.EMP_LASTNAME,1,1) + '.' EMP_NAME, 
	STATUS,			COMMENT,		ATTACH_FILE AS ATTACH_FILE,		CONVERT(VARCHAR(23),FORMAT(C.CREATE_DATE,'dd-MMM-yyyy HH:mm:ss')) CREATE_DATE, 
	C.CREATE_BY,	CASE STATUS 
						WHEN 'S' THEN 'glyphicon glyphicon-floppy-disk bg-aqua' 
						WHEN 'A' THEN 'glyphicon glyphicon-ok bg-aqua' 
						WHEN 'R' THEN 'glyphicon glyphicon-remove bg-maroon' 
					END  ICON_DISPLAY

	FROM TB_R_REQUEST_COMMENT C WITH (NOLOCK)
	LEFT JOIN TB_M_EMPLOYEE E ON CASE WHEN C.EMP_CODE = 'FA' THEN C.CREATE_BY ELSE C.EMP_CODE END = E.EMP_CODE 
	WHERE DOC_NO = @DOC_NO
	AND  ATTACH_FILE LIKE 'TEMP%'
	ORDER BY COMMENT_HISTORY_ID

END





GO
