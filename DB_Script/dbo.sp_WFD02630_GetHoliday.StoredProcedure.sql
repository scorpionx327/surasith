DROP PROCEDURE [dbo].[sp_WFD02630_GetHoliday]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 27/02/2017
-- Description:Get Holiday
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02630_GetHoliday]
@year varchar(4),
@round varchar(2),
@asset_location varchar(1),
@company T_COMPANY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

				  SELECT [STOCK_TAKE_KEY]
						 ,[HOLIDATE_DATE]
				  FROM [TB_R_STOCK_TAKE_HOLIDAY]
				  WHERE STOCK_TAKE_KEY = CONCAT(@year,@round,@asset_location)
				  AND COMPANY=@company
END



GO
