DROP PROCEDURE [dbo].[sp_WFD01110_CheckStockPlan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 21/02/2017
-- Description:	System will check status of plan is not draft/finish plan in Stock Take Header table[TB_R_STOCK_TAKE_H]
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_CheckStockPlan]
@EMP_CODE	T_SYS_USER
 AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	DECLARE @COMPANY	T_COMPANY, @FAADMIN VARCHAR(1)
	SELECT	@COMPANY = COMPANY, @FAADMIN = FAADMIN
	FROM	TB_M_EMPLOYEE
	WHERE	SYS_EMP_CODE = @EMP_CODE 
		

	SET NOCOUNT ON;
	declare @bufferday int
	set @bufferday =  convert(int,(select value from TB_M_SYSTEM where CATEGORY = 'STOCK_TAKE' 
	and SUB_CATEGORY = 'DISP_PERIOD'
	and CODE = 1 and ACTIVE_FLAG = 'Y')); 

	select STOCK_TAKE_KEY from TB_R_STOCK_TAKE_H
	where dateadd(day,-@bufferday,TARGET_DATE_FROM ) <= GETDATE()
			and dateadd(day,+@bufferday,TARGET_DATE_TO) >= GETDATE()
			And ASSET_LOCATION = 'I' ---(only Inhouse)
			and PLAN_STATUS in ('C','S')
			and ( COMPANY = @COMPANY OR @FAADMIN = 'Y')
END
GO
