DROP PROCEDURE [dbo].[sp_WFD02190_InsertTempDataDisp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 09/02/2017
-- Description:	Insert Fixed assets screen
-- =============================================
--declare @TOTAL_ITEM int exec sp_WFD02190_SearchData null,null,null,null,null,null,null,null,null,1,10,null,@TOTAL_ITEM out  
create PROCEDURE [dbo].[sp_WFD02190_InsertTempDataDisp]
(
	@GUID				    CHAR(50)			= NULL,		--Sub Type	
	@ASSET_NO				VARCHAR(15)			= NULL,		--Asset No	
	@COST_CODE				VARCHAR(8)			= NULL,		--Cost Code	
	@CREATE_BY				VARCHAR(200)		= NULL		--Cost Code	
)
AS
BEGIN
	BEGIN TRY
		DECLARE @ITEM_NO INT

		SELECT @ITEM_NO = COUNT(1) + 1 FROM TB_T_SELECTED_ASSETS_DISP
		WHERE GUID	= @GUID

		DECLARE @ASSET_CATE VARCHAR(2)
		SELECT @ASSET_CATE = ASSET_CATEGORY 
		FROM TB_M_ASSETS_H WHERE ASSET_NO = @ASSET_NO

		IF @COST_CODE IS NULL
		BEGIN
			SELECT TOP 1 @COST_CODE = COST_CODE FROM TB_M_ASSETS_D WHERE ASSET_NO = @ASSET_NO
		END


		INSERT INTO TB_T_SELECTED_ASSETS_DISP
		(
			GUID,			ASSET_NO,		ITEM_NO,						COST_CODE,		CREATE_BY,			CREATE_DATE, ASSET_CATEGORY
		)
		VALUES
		(
			@GUID,			@ASSET_NO,		CONVERT(VARCHAR(15),@ITEM_NO),	@COST_CODE,		@CREATE_BY,			GETDATE(), @ASSET_CATE	
		)
	END TRY
	BEGIN CATCH


	END CATCH

END
GO
