DROP PROCEDURE [dbo].[sp_WFD02510_InitialAssetList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02510_InitialAssetList]
(	
	@DOC_NO		T_DOC_NO,
	@GUID		T_GUID,
	@User		T_SYS_USER
)
AS
BEGIN

	-- Copy data from actual to temporary
	DELETE
	FROM	TB_T_REQUEST_DISPOSE
	WHERE	CREATE_DATE < DATEADD(DAY, -1, GETDATE())

	DELETE
	FROM	TB_T_REQUEST_DISP_H
	WHERE	CREATE_DATE < DATEADD(DAY, -1, GETDATE())

	INSERT
	INTO	TB_T_REQUEST_DISP_H
	(
			COMPANY,
			DOC_NO,
			DISPOSAL_BY,
			DISP_TYPE, -- Sale / Scrap / Donat
			DISP_MASS_REASON,
			DISP_MASS_ATTACH_DOC,
			MINUTE_OF_BIDDING,
			HIGHER_APP_ATTACH,
			ROUTE_TYPE,
			[GUID],
			CREATE_DATE
	)
	SELECT	COMPANY,
			DOC_NO,
			DISPOSAL_BY,
			DISP_TYPE,
			DISP_MASS_REASON,
			DISP_MASS_ATTACH_DOC,
			MINUTE_OF_BIDDING,
			HIGHER_APP_ATTACH,
			ROUTE_TYPE,
			@GUID,
			CREATE_DATE

	FROM	TB_R_REQUEST_DISP_H
	WHERE	DOC_NO	= @DOC_NO
	-- Detail
	INSERT
	INTO	TB_T_REQUEST_DISPOSE
	(		
			DOC_NO,
			DISP_TYPE,
			COMPANY,
			ASSET_NO,
			ASSET_SUB,
			ASSET_NAME,
			DATE_IN_SERVICE,
			RETIRE,
			QTY,
			COST,
			BOOK_VALUE,
			RETIRE_COST,
			USER_MISTAKE,
			RETIRE_BOOK_VALUE,
			ACCUM_DEPREC,
			USEFUL_LIFE,
			PHOTO_ATTACH,
			DISP_REASON,
			BOI_NO,
			BOI_CHECK_FLAG,
			BOI_ATTACH_DOC,
			STORE_CHECK_FLAG,
			RETIREMENT_TYPE,
			SOLD_TO,
			PRICE,
			INVOICE_NO,
			DETAIL_DATE,
			REMOVAL_COST,
			DETAIL_ATTACH,
			DETAIL_REMARK,
			DISP_REASON_OTHER,
			LOSS_COUNTER_MEASURE,
			USER_MISTAKE_FLAG,
			COMPENSATION,
			DELETE_FLAG,
			PROCESS_STATUS,
			[GUID],
			USER_DISPOSE,
			CREATE_DATE,
			CREATE_BY
	)
	SELECT	R.DOC_NO,
			H.DISP_TYPE,
			R.COMPANY,
			R.ASSET_NO,
			R.ASSET_SUB,
			M.ASSET_NAME,
			R.DATE_IN_SERVICE,
			R.RETIRE,
			R.QTY,
			R.COST,
			R.BOOK_VALUE,
			R.RETIRE_COST,
			R.USER_MISTAKE,
			R.RETIRE_BOOK_VALUE,
			R.ACCUM_DEPREC,
			R.USEFUL_LIFE,
			NULL PHOTO_ATTACH,
			R.DISP_REASON,
			R.BOI_NO,
			R.BOI_CHECK_FLAG,
			NULL BOI_ATTACH_DOC,
			R.STORE_CHECK_FLAG,
			R.RETIREMENT_TYPE,
			R.SOLD_TO,
			R.PRICE,
			R.INVOICE_NO,
			DETAIL_DATE,
			REMOVAL_COST,
			NULL DETAIL_ATTACH,
			DETAIL_REMARK,
			DISP_REASON_OTHER,
			NULL LOSS_COUNTER_MEASURE,
			R.USER_MISTAKE,
			R.COMPENSATION,
			R.DELETE_FLAG,
			R.[STATUS],
			@GUID,
			NULL USER_DISPOSE,
			GETDATE(),
			R.CREATE_BY
	FROM	TB_R_REQUEST_DISP_D R
	INNER	JOIN
			TB_R_REQUEST_DISP_H H
	ON		R.DOC_NO	= H.DOC_NO
	INNER	JOIN
			TB_M_ASSETS_H M
	ON		R.COMPANY	= M.COMPANY AND 
			R.ASSET_NO	= M.ASSET_NO AND
			R.ASSET_SUB	= M.ASSET_SUB
	WHERE	R.DOC_NO = @DOC_NO AND
			(DELETE_FLAG	!= 'Y' OR DELETE_FLAG IS NULL)
	


END
GO
