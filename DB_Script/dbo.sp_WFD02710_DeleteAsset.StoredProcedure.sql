/****** Object:  StoredProcedure [dbo].[sp_WFD02710_DeleteAsset]    Script Date: 10/4/2019 3:31:49 PM ******/
DROP PROCEDURE [dbo].[sp_WFD02710_DeleteAsset]
GO
/****** Object:  StoredProcedure [dbo].[sp_WFD02710_DeleteAsset]    Script Date: 10/4/2019 3:31:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_ClearAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD02710_DeleteAsset]
(	
	@GUID		T_GUID,
	@DOC_NO		T_DOC_NO,
	@COMPANY	T_COMPANY,
	@AUC_NO	T_ASSET_NO,
	@AUC_SUB	T_ASSET_SUB
)
AS
BEGIN
	
	--IF EXISTS(SELECT 1 FROM TB_R_REQUEST_H WHERE DOC_NO = @DOC_NO)
	--BEGIN
	
		UPDATE	TB_R_REQUEST_SETTLE_H
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				COMPANY		= @COMPANY AND
				AUC_NO		= @AUC_NO AND
				AUC_SUB		= @AUC_SUB
		
	--END

	-- delete if exists in submit mode
	DELETE	T
	FROM	TB_T_SELECTED_ASSETS T
	WHERE	T.[GUID]		= @GUID AND 
			T.COMPANY		= @COMPANY AND
			NOT EXISTS(	SELECT	1 
						FROM	TB_R_REQUEST_SETTLE_H D
						WHERE	D.COMPANY = @COMPANY AND 
								D.[GUID]	= @GUID AND
								D.ASSET_NO	= T.ASSET_NO AND 
								D.ASSET_SUB	= T.ASSET_SUB )

	DELETE	TB_T_REQUEST_SETTLE_H
	WHERE	[GUID]		= @GUID AND 
			COMPANY		= @COMPANY AND
			AUC_NO		= @AUC_NO AND
			AUC_SUB		= @AUC_SUB

END
GO
