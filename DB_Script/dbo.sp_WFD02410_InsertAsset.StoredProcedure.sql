DROP PROCEDURE [dbo].[sp_WFD02410_InsertAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02410_InsertAsset]
(	
	@COMPANY	T_COMPANY,
	@ASSET_NO	T_ASSET_NO,
	@ASSET_SUB	T_ASSET_SUB,
	@GUID		T_GUID,
	@USER		T_SYS_USER
)
AS
BEGIN TRY 
	-- Insert into temp
	DELETE
	FROM	TB_T_SELECTED_ASSETS
	WHERE	COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			[ASSET_SUB] = @ASSET_SUB AND
			[GUID]		= @GUID

	INSERT
	INTO	TB_T_SELECTED_ASSETS
	(		[GUID],
			COMPANY,
			ASSET_NO,
			ASSET_SUB,
			ITEM_NO,
			COST_CODE,
			ASSET_CLASS,
			CREATE_DATE,
			CREATE_BY
			)
	SELECT	@GUID,
			@COMPANY,
			@ASSET_NO,
			@ASSET_SUB,
			1,
			COST_CODE,
			ASSET_CLASS,
			GETDATE(),
			@USER
	FROM	TB_M_ASSETS_H
	WHERE	COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB
	
	-- Insert into temp impair
	INSERT
	INTO	TB_T_REQUEST_TRNF_D (
			COMPANY,				DOC_NO,				ASSET_NO,				ASSET_SUB,
			COST_CODE,				RESP_COST_CODE,		COST,					BOOK_VALUE,
			DATE_IN_SERVICE,		TRNF_REASON,		BOI_NO,					INVEST_REASON,
			PLANT,					LOCATION_CD,		LOCATION_NAME,			WBS_PROJECT,
			TO_EMPLOYEE,			LOCATION_DETAIL,	TRNF_EFFECTIVE_DT,
			[STATUS],				[GUID],				DELETE_FLAG,
			CREATE_DATE,			CREATE_BY,			UPDATE_DATE,			UPDATE_BY
	)
	SELECT	COMPANY,				NULL,				ASSET_NO,				ASSET_SUB,
			COST_CODE,				RESP_COST_CODE,		CUMU_ACQU_PRDCOST_01,	NET_VALUE_01,
			DATE_IN_SERVICE,		'' TRNF_REASON,		BOI_NO,					INVEST_REASON,
			PLANT,					NULL,				NULL,					WBS_PROJECT,
			NULL,					NULL,				NULL,
			'NEW',					@GUID,				NULL,
			GETDATE(),				@USER,				GETDATE(),			@USER
	FROM	TB_M_ASSETS_H
	WHERE	COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB
END TRY
BEGIN CATCH
	THROW
END CATCH
GO
