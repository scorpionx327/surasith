DROP PROCEDURE [dbo].[sp_LFD02170_SumAssetBySubType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_SumAssetBySubType]
@COST_CODE VARCHAR (8)
AS
BEGIN	
	SET NOCOUNT ON;

       SELECT M.VALUE AS MACHINE_LICENSE,ISNULL(A.ITEMS,0) AS ITEMS
	   FROM TB_M_SYSTEM M
	   LEFT JOIN 
	   (
	    SELECT COUNT(1) AS ITEMS ,H.MACHINE_LICENSE
        FROM TB_M_ASSETS_H H
	    inner join TB_M_ASSETS_D D
		On H.ASSET_NO  =  D.ASSET_NO
		WHERE D.COST_CODE = @COST_CODE --'SMESP002'
		AND H.STATUS='Y' 
		AND H.ASSET_SUB = '0000'
		AND H.INHOUSE_FLAG = 'Y'
		GROUP BY H.MACHINE_LICENSE ) A
		ON M.CODE = A.MACHINE_LICENSE
		WHERE M.CATEGORY = 'SYSTEM_CONFIG' AND M.SUB_CATEGORY = 'MINOR_CATEGORY'
		--AND exists (select '1' from tb_m_system S where s.CATEGORY = 'TO_DO_LIST' and s.SUB_CATEGORY = 'MAP_LOCATION' and s.code like 'SUB_TYPE_%'
		--           and s.VALUE = M.CODE)
		ORDER BY M.REMARKS ASC

END


--select * from TB_M_SYSTEM where SUB_CATEGORY like 'MAP_LOCATION%'




GO
