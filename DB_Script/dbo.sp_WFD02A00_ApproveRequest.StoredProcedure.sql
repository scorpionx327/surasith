DROP PROCEDURE [dbo].[sp_WFD02A00_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02A00_ApproveRequest]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN
	
	EXEC sp_WFD02A00_UpdateAssetApprove @GUID, @DOC_NO, @USER
	
	/*
	Approve by AEC
	*/
	-- To be change
	IF dbo.fn_IsReadyGenFile(@DOC_NO) = 'N'
	RETURN

	UPDATE	TB_R_REQUEST_IMPAIRMENT
	SET		[STATUS]	= 'GEN',
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= @USER
	WHERE	DOC_NO		= @DOC_NO AND
			ASSET_NO	IS NOT NULL AND
			[STATUS]	= 'NEW'

	-------------------------------------------------------------------------------------
	-- Approve by AEC
	-- Execute when Status is change from 10 -> 20 (MGR Approve)
	UPDATE	H
	SET		H.PROCESS_STATUS	= 'MC',
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_IMPAIRMENT M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO			= @DOC_NO AND
			H.PROCESS_STATUS	= 'M'


	
		
	DECLARE @format VARCHAR(255) = '{0}: Asset No {1} Sub {2} from cost {3} to {4}'

	DECLARE @LatestApproverCode	T_SYS_USER, @LatestApproverName	VARCHAR(68)
	DECLARE @AECCode		T_SYS_USER
	UPDATE	H
	SET		H.DETAIL		= dbo.fn_StringFormat(@format, 
									CONCAT(IIF(M.IMPAIRMENT_FLAG ='IM','Impairment', 'Reverse Impairment'),'|',
											M.ASSET_NO, '|', M.ASSET_SUB, '|', M.BF_CUMU_ACQU_PRDCOST_01,'|', M.AMOUNT_POSTED)),
			H.TRANS_DATE	= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= @LatestApproverName, 
			H.AEC_APPR_BY			= @AECCode,
			H.AEC_APPR_DATE			= GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_IMPAIRMENT M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND 
			M.DOC_NO	= H.DOC_NO AND 
			M.ASSET_NO	= H.ASSET_NO AND 
			M.ASSET_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO

END
GO
