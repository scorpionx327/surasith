DROP PROCEDURE [dbo].[sp_WFD02620_GetStockTakeHoliday]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 15-02-2017
-- Description:	Get stock take holiday datas
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetStockTakeHoliday]
	@STOCK_TAKE_KEY		varchar(7)
	, @COMPANY			T_COMPANY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM TB_R_STOCK_TAKE_HOLIDAY 
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY AND COMPANY = @COMPANY

END




GO
