DROP PROCEDURE [dbo].[sp_common_GetUserBatchWorkingAppId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE PROCEDURE [dbo].[sp_common_GetUserBatchWorkingAppId]
	-- Add the parameters for the stored procedure here
	@BATCH_ID		varchar(20)
	, @USER_ID		varchar(20)
AS
	DECLARE @IS_PROCESSING bit;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT APP_ID FROM TB_BATCH_Q WHERE BATCH_ID=@BATCH_ID and CREATE_BY=@USER_ID and (STATUS='Q' or STATUS='P')
	 
END


GO
