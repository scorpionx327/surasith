DROP PROCEDURE [dbo].[sp_WFD01270_SubmitCommentInfoData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 22/02/2017
-- Description:	Insert Comment Info
-- =============================================
--declare @ERROR_STATUS VARCHAR(10) exec sp_WFD01270_SubmitCommentInfoData 'P2017/0001','1','0001',null,null,null,@ERROR_STATUS OUT  SELECT @ERROR_STATUS

CREATE PROCEDURE [dbo].[sp_WFD01270_SubmitCommentInfoData]
(
	@DOC_NO					T_DOC_NO			= NULL,		
	@INDX					INT					= NULL,		
	@EMP_CODE				T_SYS_USER			= NULL,		
	@COMMENT				VARCHAR(200)		= NULL,			
	@ATTACH_FILE			VARCHAR(200)		= NULL,		
	@USER_BY				VARCHAR(10)			= NULL,		
	@REQUEST_TYPE			VARCHAR(1)			= NULL,
	@GUID					VARCHAR(50)			= NULL,
	@COMMENT_SATAUS			VARCHAR(1)			= NULL
	
)
AS
BEGIN
	
	BEGIN TRY

		IF ISNULL(@INDX,0) = 0 
		BEGIN
			SELECT	TOP 1 @INDX = INDX FROM TB_R_REQUEST_APPR WITH (NOLOCK)
			WHERE	DOC_NO = @DOC_NO AND EMP_CODE = @EMP_CODE AND APPR_STATUS != 'P' 
			ORDER	BY INDX
		END

	
		IF ISNULL(@COMMENT,'') = ''
		BEGIN
			SELECT @COMMENT =  CASE @COMMENT_SATAUS  
						WHEN 'R' THEN 'Rejected' 
						WHEN 'S' THEN 'Submit' 		
						WHEN 'D' THEN 'Deleted' 				
						END
		END

		DECLARE @FA VARCHAR(3)
		SET @FA = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPRV_ROLE_CODE','ACR')
		PRINT 'test [sp_WFD01270_SubmitCommentInfoData]'
		INSERT INTO TB_R_REQUEST_COMMENT
		(
			COMMENT_HISTORY_ID,
			DOC_NO,				INDX,					EMP_CODE,				EMP_NAME,
			STATUS,				COMMENT,				ATTACH_FILE,
			CREATE_DATE,		CREATE_BY,				GUID
		)
		SELECT
			NEXT VALUE FOR COMMENT_HISTORY_ID, 
			@DOC_NO,			@INDX,					@EMP_CODE,				
			CONCAT(ISNULL(E.EMP_TITLE,'') ,' ', ISNULL(E.EMP_NAME,'') ,' ', LEFT(ISNULL(E.EMP_LASTNAME,''),1) , '.'),			
			@COMMENT_SATAUS,	@COMMENT,				@ATTACH_FILE, 
			GETDATE(),			@USER_BY,				@GUID
		FROM TB_M_EMPLOYEE E
		WHERE E.EMP_CODE	= IIF(@EMP_CODE = @FA, @USER_BY, @EMP_CODE)  

	END TRY
	BEGIN CATCH
		THROW ;

	END CATCH

END
GO
