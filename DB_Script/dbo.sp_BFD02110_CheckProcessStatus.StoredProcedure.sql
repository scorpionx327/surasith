DROP PROCEDURE [dbo].[sp_BFD02110_CheckProcessStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  For check  Process Status of Asset No. for write warning log
- ============================================= SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD02110_CheckProcessStatus]
( 
	@AppID	INT,
	@UserID		VARCHAR(8)
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

  DECLARE @Msg VARCHAR(500)

  /*--   Case Retriement Date is not blank (Dispose) */
     SET		@Msg = dbo.fn_GetMessage('MFAS1105BWRN')

	 INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	 SELECT  @AppID, 'P', 'N', 'W',
				dbo.fn_StringFormat(@Msg , r.ASSET_NO ),'Y', @UserID, GETDATE()
		FROM    (	SELECT  AssetH.ASSET_NO 
					FROM    TB_M_ASSETS_H AssetH 
					INNER	JOIN   (		SELECT	ASSET_NO,RETIREMENT_DATE, ENTRY_PERIOD 
											FROM	TB_S_ASSETS_H
											WHERE	ISNULL(RETIREMENT_DATE,'') <> ''
											) S_Asset   
					ON		AssetH.ASSET_NO=S_Asset.ASSET_NO AND 
							AssetH.PROCESS_STATUS = 'D'
					--Add By Surasith T. 2017-07-06
					WHERE	CONVERT(DATE, ISNULL(AssetH.RETIREMENT_DATE,'1900-01-01')) < CONVERT(DATE, ISNULL(S_Asset.RETIREMENT_DATE,'1900-01-01'))
						 ) r

 /*--   Case Transfer Date is not blank (Transfer) */
     SET		@Msg = dbo.fn_GetMessage('MFAS1105BWRN')

	 INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	 SELECT  @AppID, 'P', 'N', 'W',
				dbo.fn_StringFormat(@Msg , r.ASSET_NO ),'Y', @UserID, GETDATE()
		FROM    (  SELECT  AssetH.ASSET_NO
					FROM    TB_M_ASSETS_H AssetH 
					INNER JOIN   (  SELECT ASSET_NO,TRANSFER_DATE, ENTRY_PERIOD 
										FROM TB_S_ASSETS_H
										WHERE ISNULL(TRANSFER_DATE,'') <> ''
										) S_Asset   
					 ON AssetH.ASSET_NO=S_Asset.ASSET_NO AND AssetH.PROCESS_STATUS='T'
					 WHERE	CONVERT(DATE, ISNULL(AssetH.TRANSFER_DATE,'1900-01-01')) < CONVERT(DATE, ISNULL(S_Asset.TRANSFER_DATE,'1900-01-01'))
					 ) r


/*--   Case Asset Type change from 'CIP' to 'Capitalized'*/
     SET		@Msg = dbo.fn_GetMessage('MFAS1105BWRN')

	 INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	 SELECT  @AppID, 'P', 'N', 'W',
				dbo.fn_StringFormat(@Msg , r.ASSET_NO ),'Y', @UserID, GETDATE()
		FROM    ( SELECT  AssetH.ASSET_NO
					FROM    TB_M_ASSETS_H AssetH 
					INNER JOIN  ( SELECT ASSET_NO,ENTRY_PERIOD 
										FROM TB_S_ASSETS_H
										WHERE UPPER(ASSET_TYPE) ='CAPITALIZED'
										) S_Asset   
					 ON AssetH.ASSET_NO=S_Asset.ASSET_NO  AND   UPPER(AssetH.ASSET_TYPE)='CIP' AND AssetH.PROCESS_STATUS='C') r
	

/*---- Incase Plan Cost is not numeric */
 SET @Msg = dbo.fn_GetMessage('MFAS1106BWRN')
	 INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	 SELECT  @AppID, 'P', 'N', 'W',
				dbo.fn_StringFormat(@Msg , r.ASSET_NO ),'Y', @UserID, GETDATE()
     FROM    (   SELECT ASSET_NO
							FROM TB_S_ASSETS_H
								WHERE [PLAN_COST] IS NOT NULL 
									AND (      IsNumeric([PLAN_COST])=0 
											OR ltrim(rtrim([PLAN_COST]))='-')
							 )r


	RETURN @Error ;
END TRY
BEGIN CATCH
 			PRINT concat('ERROR Message:',ERROR_MESSAGE())
END CATCH










GO
