DROP PROCEDURE [dbo].[sp_WFD02640_GetCostCenterStock]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 08/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--truncate table TB_T_SELECTED_ASSETS where GUID = 'tbf1da954-0d12-4988-9a6a-65e2ca593258'
--declare @TOTAL_ITEM int exec sp_WFD02210_GetAssetCIP '2fbc0621-3dad-4cc0-a304-36f5a3c89148',null,'0008',null,1,10,null,@TOTAL_ITEM out  
CREATE PROCEDURE [dbo].[sp_WFD02640_GetCostCenterStock]
(	
	@DOC_NO						VARCHAR(10)			= NULL,		--DOC NO	
	@YEAR						VARCHAR(4)			= NULL,
	@ROUND						VARCHAR(2)			= NULL,
	@ASSET_LOCATION				VARCHAR(1)			= NULL,
	@COST_CODE					VARCHAR(8)			= NULL,	
	@USER_BY					VARCHAR(50)			= NULL,
	@UPDATE_DATE				VARCHAR(50)			= NULL,
	@EMP_CODE					VARCHAR(30),
	@ISFAADMIN					VARCHAR(1), --- Y is FA Admin , N is SV User , Normal user not access 
	@GUID						VARCHAR(50)			= NULL		--DOC NO
)
AS
BEGIN
	DECLARE @TEMP_DOC_NO	VARCHAR(10)
	DECLARE @COUNT_TEMP		INT
	DECLARE @TEMP_GUID		VARCHAR(50)

	IF OBJECT_ID('tempdb..#STOCK_TIME') IS NOT NULL DROP TABLE #STOCK_TIME
	IF OBJECT_ID('tempdb..#TOTAL_ASSET') IS NOT NULL DROP TABLE #TOTAL_ASSET
	IF OBJECT_ID('tempdb..#DETAIL_STOCK') IS NOT NULL DROP TABLE #DETAIL_STOCK
		
	IF ISNULL(@DOC_NO,'') = '' AND ISNULL(@GUID,'') != ''
	BEGIN
		SET @TEMP_DOC_NO = @USER_BY + '/1'

		SELECT @COUNT_TEMP = COUNT(1) 
		FROM TB_T_REQUEST_STOCK
		WHERE DOC_NO			= @TEMP_DOC_NO

		IF @COUNT_TEMP > 0
		BEGIN
			SELECT TOP 1 @TEMP_GUID = GUID 
			FROM TB_T_REQUEST_STOCK
			WHERE DOC_NO			= @TEMP_DOC_NO

			IF ISNULL(@TEMP_GUID,'') <> @GUID	-- ของเก่า			
			BEGIN
				DELETE TB_T_REQUEST_STOCK 
				WHERE DOC_NO			= @TEMP_DOC_NO
			END
		END
			INSERT INTO TB_T_REQUEST_STOCK
			(

				DOC_NO,					YEAR,					ROUND,						ASSET_LOCATION, 
				COST_CODE,				COMMENT,				ATTACHMENT, 
				CREATE_DATE,			CREATE_BY,				
				GUID			
			)
			SELECT 
				@TEMP_DOC_NO,			@YEAR,					@ROUND,						@ASSET_LOCATION,
				COST_CODE,				'',						'',						
				GETDATE(),				@USER_BY,		
				@GUID			
			FROM TB_R_STOCK_TAKE_D D
			WHERE SV_EMP_CODE = @EMP_CODE
			GROUP BY COST_CODE

		SET @DOC_NO = @TEMP_DOC_NO
	END

END


GO
