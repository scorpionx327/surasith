DROP PROCEDURE [dbo].[sp_WFD02640_GetDetailStockTakingMonitoringDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 02/03/2017
-- Description:	Get detail Stock Taking Monitoring Detail
-- =============================================
--EXEC [sp_WFD02640_GetDetailStockTakingMonitoringDetail] '2018','05','I'
--EXEC [sp_WFD02640_GetDetailStockTakingMonitoringDetail] '','','','','','S2017/0035'
CREATE PROCEDURE [dbo].[sp_WFD02640_GetDetailStockTakingMonitoringDetail]
	@COMPANY	T_COMPANY,
	@YEAR VARCHAR(4)= NULL,
	@ROUND VARCHAR(2)= NULL,
	@ASSET_LOCATION VARCHAR(1)= NULL,
	@EMP_CODE VARCHAR(30)= NULL,
	@SV_CODE VARCHAR(30)= NULL,
	@DOC_NO T_DOC_NO = NULL,
	@ISFAADMIN VARCHAR(1)= NULL --- Y is FA Admin , N is SV User , Normal user not access
AS
BEGIN
  ----==============================  Declare status 
	
		DECLARE @PathAttachMent VARCHAR(MAX);



		IF (@DOC_NO IS NOT NULL)
		BEGIN
			SELECT	@COMPANY	= COMPANY,
					@YEAR		= [YEAR],
					@ROUND		= [ROUND],
					@ASSET_LOCATION = ASSET_LOCATION
			FROM	TB_R_REQUEST_STOCK 
			WHERE	DOC_NO=@DOC_NO;

			SELECT	@SV_CODE =SV_EMP_CODE
			FROM	(
					SELECT	stockD.SV_EMP_CODE 
					FROM	TB_R_STOCK_TAKE_D stockD
					INNER	JOIN  
							TB_R_REQUEST_STOCK  r 
					ON      stockD.COMPANY				= r.COMPANY
							AND		stockD.[YEAR]		= r.YEAR 
							AND		stockD.[ROUND]		=  r.ROUND 
							AND		stockD.ASSET_LOCATION = r.ASSET_LOCATION 
							AND		stockD.SV_EMP_CODE	= r.SV_EMP_CODE
				
					WHERE	stockD.COMPANY	= @COMPANY
							AND		stockD.YEAR = @YEAR 
							AND		stockD.ROUND   =  @ROUND 
							AND		stockD.ASSET_LOCATION = @ASSET_LOCATION 
							AND		r.DOC_NO=@DOC_NO
                      
					GROUP BY stockD.SV_EMP_CODE
			) t
		END 
	

		DECLARE @StockKey VARCHAR(17) = CONCAT(@YEAR ,@ROUND , @ASSET_LOCATION)


		SET @PathAttachMent =''  -- get from system config 

	    DECLARE @Status_Complete AS VARCHAR(20)				= 	 'Complete'
		DECLARE @Status_Complete_WithDelay AS VARCHAR(20)	=     'Complete with Delay'
		DECLARE @Status_Delay AS VARCHAR(20)				=     'Delay'
		DECLARE @Status_OnProgress AS VARCHAR(20)			=      'On Progress'
		DECLARE @Status_Complete_WithLose AS VARCHAR(20)	=      'Complete with Loss'
		DECLARE @Status_Not_Yet_Scan AS VARCHAR(20)			=      'not_yet_scan'

		DECLARE @Status_EVA_CIRCLE VARCHAR(10) = 'CIRCLE'
		DECLARE @Status_EVA_TRIANGLE VARCHAR(10) = 'TRIANGLE'
		DECLARE @Status_EVA_CROSS VARCHAR(10) = 'CROSS'


		BEGIN /* ==============================  CREATE Temp TABLE ============================== */
				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_H') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_H
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D_PER_SV') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D_PER_SV
				END

				---- Temp data for retrun to screen 
				IF OBJECT_ID('tempdb..#T_TIME_PROCESS') IS NOT NULL
				BEGIN
						DROP TABLE #T_TIME_PROCESS
				END

				IF OBJECT_ID('tempdb..#T_DATA_PER_SV') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_PER_SV
				END

				IF OBJECT_ID('tempdb..#T_DATA_PER_COST_CODE') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_PER_COST_CODE
				END
		END
 
 		BEGIN /* ============================== Insert data to Temp TABLE ============================== */
			--Stock Header
			SELECT	* 
			INTO	#T_STOCK_TAKE_H 
			FROM	TB_R_STOCK_TAKE_H stockH
			WHERE   stockH.COMPANY			= @COMPANY
			AND		stockH.YEAR				= @YEAR 
			AND		stockH.ROUND			=  @ROUND 
			AND		stockH.ASSET_LOCATION	= @ASSET_LOCATION 
			AND		stockH.STOCK_TAKE_KEY	= @StockKey
		--	select 'test#1',* from #T_STOCK_TAKE_H
				--Stock Per SV
					SELECT  STOCK_TAKE_KEY  , YEAR  , ROUND , ASSET_LOCATION 	  , EMP_CODE 
							, EMP_TITLE 		  , EMP_NAME   , EMP_LASTNAME 
							,CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),DATE_FROM,112),' ', TIME_START,':00' )) AS DATE_FROM
							,CASE 
							WHEN (DATE_TO IS NOT NULL) AND (TIME_END IS NOT NULL) THEN CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),DATE_TO,112),' ', TIME_END,':00' ))
							WHEN (DATE_TO IS NOT NULL) AND (TIME_END IS NULL) THEN CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),DATE_TO,112),' ','23:59:59' ))
							ELSE NULL END AS DATE_TO
							, TIME_MINUTE 	  , USAGE_HANDHELD 	  , IS_LOCK  , TOTAL_ASSET  
					INTO #T_STOCK_TAKE_D_PER_SV
					FROM TB_R_STOCK_TAKE_D_PER_SV stockSV
					WHERE	stockSV.COMPANY	= @COMPANY
					AND		stockSV.YEAR = @YEAR 
					AND		stockSV.ROUND   =  @ROUND 
					AND		stockSV.ASSET_LOCATION = @ASSET_LOCATION 
					AND		stockSV.STOCK_TAKE_KEY= @StockKey
					AND		stockSV.EMP_CODE = @SV_CODE

					SELECT stockD.STOCK_TAKE_KEY, stockD.[YEAR],stockD.[ROUND],stockD.ASSET_LOCATION
						    ,stockD.SV_EMP_CODE,  stockD.COST_CODE,stockD.SUPPLIER_NAME AS COST_NAME,
							stockD.CHECK_STATUS,stockD.SCAN_DATE,requestStock.DOC_NO 
							,requestStock.COMMENT , requestStock.ATTACHMENT
							,stockD.ASSET_NO
					INTO #T_STOCK_TAKE_D
					FROM TB_R_STOCK_TAKE_D  stockD  
					LEFT JOIN TB_R_REQUEST_STOCK requestStock  
					ON		stockD.COMPANY	= requestStock.COMPANY
								AND stockD.YEAR = requestStock.YEAR 
						        AND stockD.ROUND   =  requestStock.ROUND 
								AND stockD.ASSET_LOCATION = requestStock.ASSET_LOCATION  
								AND stockD.COST_CODE =requestStock.COST_CODE
								AND stockD.SV_EMP_CODE =requestStock.SV_EMP_CODE
                    WHERE 1<>1


				--Stock Detail
	IF(@ASSET_LOCATION='I')
	BEGIN
						INSERT INTO #T_STOCK_TAKE_D
		 				SELECT stockD.STOCK_TAKE_KEY, stockD.[YEAR],stockD.[ROUND],stockD.ASSET_LOCATION
						    ,stockD.SV_EMP_CODE,  stockD.COST_CODE,TCost.COST_NAME,
							stockD.CHECK_STATUS,stockD.SCAN_DATE,requestStock.DOC_NO 
							,requestStock.COMMENT , requestStock.ATTACHMENT
							,stockD.ASSET_NO
					FROM TB_R_STOCK_TAKE_D  stockD  
					LEFT JOIN TB_M_COST_CENTER  TCost ON stockD.COST_CODE=TCost.COST_CODE
					LEFT JOIN (
						-- In worst case system insert 1 cost center to 2 doc no (Surasith)
						SELECT	COMPANY, YEAR, ROUND, ASSET_LOCATION, COST_CODE, COMMENT, DOC_NO, ATTACHMENT,SV_EMP_CODE,
								ROW_NUMBER() OVER (PARTITION BY YEAR, ROUND, ASSET_LOCATION, COST_CODE ORDER BY DOC_NO DESC) AS INDX
						FROM	TB_R_REQUEST_STOCK r
						WHERE	r.COMPANY	= @COMPANY
								AND r.YEAR = @YEAR 
								AND r.ROUND   =  @ROUND 
								AND r.ASSET_LOCATION = @ASSET_LOCATION 
					) requestStock  
					ON		stockD.COMPANY = requestStock.COMPANY
								AND	stockD.YEAR = requestStock.YEAR 
						        AND stockD.ROUND   =  requestStock.ROUND 
								AND stockD.ASSET_LOCATION = requestStock.ASSET_LOCATION  
								AND stockD.COST_CODE =requestStock.COST_CODE
								AND stockD.SV_EMP_CODE =requestStock.SV_EMP_CODE AND requestStock.INDX = 1
										
					WHERE  stockD.COMPANY	= @COMPANY
					AND		stockD.YEAR = @YEAR 
					AND    stockD.ROUND   =  @ROUND 
					AND    stockD.ASSET_LOCATION = @ASSET_LOCATION 
					AND    stockD.STOCK_TAKE_KEY= @StockKey
					AND EXISTS( SELECT 1 
								FROM #T_STOCK_TAKE_D_PER_SV t
								WHERE stockD.STOCK_TAKE_KEY=t.STOCK_TAKE_KEY
								ANd stockD.SV_EMP_CODE=t.EMP_CODE )

		--	select 'test#2',* from #T_STOCK_TAKE_D
    END	
	ELSE
BEGIN
	                INSERT INTO #T_STOCK_TAKE_D
		 			SELECT stockD.STOCK_TAKE_KEY, stockD.[YEAR],stockD.[ROUND],stockD.ASSET_LOCATION
						    ,stockD.SV_EMP_CODE,  stockD.SV_EMP_CODE AS COST_CODE,stockD.SUPPLIER_NAME AS COST_NAME,
							stockD.CHECK_STATUS,stockD.SCAN_DATE,requestStock.DOC_NO 
							,requestStock.COMMENT , requestStock.ATTACHMENT
							,stockD.ASSET_NO
					FROM TB_R_STOCK_TAKE_D  stockD  
					LEFT JOIN (
						-- In worst case system insert 1 cost center to 2 doc no (Surasith)
						SELECT	COMPANY, YEAR, ROUND, ASSET_LOCATION, COST_CODE, COMMENT, DOC_NO, ATTACHMENT,SV_EMP_CODE,
								ROW_NUMBER() OVER (PARTITION BY YEAR, ROUND, ASSET_LOCATION, SV_EMP_CODE ORDER BY DOC_NO DESC) AS INDX
						FROM	TB_R_REQUEST_STOCK r
						WHERE	r.COMPANY = @COMPANY
								AND		r.YEAR = @YEAR 
								AND    r.ROUND   =  @ROUND 
								AND    r.ASSET_LOCATION = @ASSET_LOCATION 
					)   
					requestStock  
					ON          stockD.COMPANY = requestStock.COMPANY
								AND    stockD.YEAR   =    requestStock.YEAR 
						        AND   stockD.ROUND   =   requestStock.ROUND 
								AND   stockD.ASSET_LOCATION = requestStock.ASSET_LOCATION  
								AND    stockD.SV_EMP_CODE =requestStock.SV_EMP_CODE AND INDX = 1
					WHERE  stockD.COMPANY	= @COMPANY
					AND		stockD.YEAR = @YEAR 
					AND    stockD.ROUND   =  @ROUND 
					AND    stockD.ASSET_LOCATION = @ASSET_LOCATION 
					AND    stockD.STOCK_TAKE_KEY= @StockKey
					AND EXISTS( SELECT 1 
								FROM #T_STOCK_TAKE_D_PER_SV t
								WHERE stockD.STOCK_TAKE_KEY=t.STOCK_TAKE_KEY
								ANd stockD.SV_EMP_CODE=t.EMP_CODE )
		--	select 'test#3',* from #T_STOCK_TAKE_D
END		
	
									
		END


BEGIN --- ============== Cal data in cost ===============



			   DECLARE @START_DATE DATETIME  --- Date start on plan per sv
               DECLARE @TARGET_DATE DATETIME --- Date end on plan per sv
			   SELECT TOP 1 @TARGET_DATE=DATE_TO,@START_DATE= DATE_FROM  FROM #T_STOCK_TAKE_D_PER_SV 
		
    -------- Clone table, Without Check because get when return data 
   			   SELECT    StockD.COST_CODE AS COST_CENTER_CODE,StockD.COST_NAME AS COST_CENTER_NAME
					   , StockD.COMMENT  AS PERSEN   ---Temp field
					   , StockD.COMMENT  AS TOTAL ---temp field
					   , StockSv.TOTAL_ASSET AS ACTUAL_SCAN   ---temp field
					   , StockSv.TOTAL_ASSET AS TOTAL_ASSET
					   , stockSV.DATE_TO AS TARGET_DATE_TO 
					   , stockSV.DATE_TO AS MAX_SCAN_DATE 
				       , stockSV.EMP_LASTNAME AS [STATUS]  ---temp field
					   , StockD.COMMENT  AS [DESCRIPTION] ---temp field
					   , StockD.COMMENT  AS  CLASS_NAME ---temp field
					   , StockD.COMMENT 
					   , StockD.ATTACHMENT
					   , StockD.DOC_NO 
					   ,stockSV.EMP_CODE AS SV_EMP_CODE
			  INTO #T_DATA_PER_COST_CODE
			 FROM #T_STOCK_TAKE_D_PER_SV stockSV INNER JOIN #T_STOCK_TAKE_D StockD
			 ON stockSV.STOCK_TAKE_KEY=StockD.STOCK_TAKE_KEY 
			 WHERE 1<> 1


					 INSERT INTO #T_DATA_PER_COST_CODE  (COST_CENTER_CODE,COST_CENTER_NAME,TOTAL_ASSET,COMMENT,ATTACHMENT,DOC_NO
					               ,TARGET_DATE_TO,SV_EMP_CODE)
					 SELECT StockD.COST_CODE,StockD.COST_NAME,COUNT(ASSET_NO) AS TOTAL_ASSET
							,StockD.COMMENT,StockD.ATTACHMENT,StockD.DOC_NO,@TARGET_DATE,StockD.SV_EMP_CODE
					 FROM #T_STOCK_TAKE_D StockD
					 WHERE ISNULL(StockD.DOC_NO,'') = ISNULL(@DOC_NO,ISNULL(StockD.DOC_NO,'') )
					 GROUP BY StockD.COST_CODE,StockD.COST_NAME,StockD.COMMENT,StockD.ATTACHMENT,StockD.DOC_NO,StockD.SV_EMP_CODE

						UPDATE   stockT
							SET       stockT.ACTUAL_SCAN    = ISNULL(dActual.ACTUAL_SCAN,0)
									, stockT.MAX_SCAN_DATE  = CASE 
															  WHEN (DOC_NO IS NOT NULL)   THEN ISNULL(dActual.MAX_SCAN_DATE,@START_DATE)
															  WHEN ISNULL(dActual.ACTUAL_SCAN,0) < ISNULL(stockT.TOTAL_ASSET,0)   THEN  CONVERT(DATETIME,GETDATE()) 
															  WHEN ISNULL(dActual.ACTUAL_SCAN,0) = ISNULL(stockT.TOTAL_ASSET,0) AND ISNULL(stockT.TOTAL_ASSET,0)> 0 THEN dActual.MAX_SCAN_DATE
															  ELSE CONVERT(DATETIME,GETDATE())   END
					   FROM #T_DATA_PER_COST_CODE stockT	
					   Left JOIN
								 ( SELECT COUNT(ASSET_NO) AS ACTUAL_SCAN ,COST_CODE ,ISNULL(MAX(SCAN_DATE),GETDATE()) AS MAX_SCAN_DATE --if no scan set to current date
									   FROM #T_STOCK_TAKE_D stockD
									   WHERE CHECK_STATUS='Y'
									   GROUP BY COST_CODE
									   )dActual  
						ON stockT.COST_CENTER_CODE=dActual.COST_CODE

					--	select 'test#x',* from #T_DATA_PER_COST_CODE


				       UPDATE   stockT
					   SET    	 stockT.PERSEN= NULL
								,stockT.TOTAL = CONCAT(ISNULL(stockT.ACTUAL_SCAN,0), '/', ISNULL(stockT.TOTAL_ASSET,0))
								,[STATUS]=NULL
					   FROM #T_DATA_PER_COST_CODE stockT 
					
					    
	                     UPDATE #T_DATA_PER_COST_CODE
						     SET  PERSEN =CONCAT(Cast(Cast(CAST(ISNULL(ACTUAL_SCAN,0) AS decimal)/ CAST(ISNULL(TOTAL_ASSET,0) AS decimal)*100 as decimal(18,2)) as varchar(6)) , '%' )
						       ,CLASS_NAME   =CASE  WHEN  (DOC_NO IS NOT NULL) THEN @Status_Complete
												   ELSE @Status_OnProgress
							 END

	                       UPDATE #T_DATA_PER_COST_CODE
						      SET  [DESCRIPTION] =CONCAT (PERSEN,' ', CLASS_NAME)
							        ,[STATUS]   =  CASE -- Update progress  On progress,Complete , Complete with Delay , Complete with Lose
  											         --- Already summit 
													  WHEN  (DOC_NO IS NOT NULL)  AND (ACTUAL_SCAN < TOTAL_ASSET)   THEN @Status_Complete_WithLose
												      WHEN  (DOC_NO IS NOT NULL)  AND (ACTUAL_SCAN = TOTAL_ASSET)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_Complete
													  WHEN  (DOC_NO IS NOT NULL)  AND (ACTUAL_SCAN = TOTAL_ASSET)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)  THEN @Status_Complete_WithDelay
												      --- no summit 
												      WHEN (DOC_NO IS NULL)  AND (ACTUAL_SCAN < TOTAL_ASSET) AND (MAX_SCAN_DATE <= TARGET_DATE_TO)   THEN @Status_OnProgress
													  WHEN (DOC_NO IS NULL)  AND (ACTUAL_SCAN < TOTAL_ASSET) AND (MAX_SCAN_DATE > TARGET_DATE_TO)    THEN @Status_Delay
													  WHEN (DOC_NO IS NULL)  AND (ACTUAL_SCAN = TOTAL_ASSET) AND (MAX_SCAN_DATE <= TARGET_DATE_TO)   THEN @Status_Complete
												      WHEN (DOC_NO IS NULL)  AND (ACTUAL_SCAN = TOTAL_ASSET) AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_Delay
                                                          ELSE  @Status_OnProgress
										   END


   
END -- =============end = Cal data in cost ===============


                 ---- Clone table
				 SELECT tCost.PERSEN , tCost.CLASS_NAME AS CLASS, tCost.[STATUS] AS ICON, tCost.TOTAL AS [DESCRIPTION]
						,tCost.ACTUAL_SCAN, tCost.TOTAL_ASSET,tCost.TARGET_DATE_TO,tCost.MAX_SCAN_DATE,'Y' AS ALL_SEND_REQUEST
				    INTO  #T_DATA_PER_SV
				 FROM #T_DATA_PER_COST_CODE tCost
				 WHERE 1<>1

				 				-- Surasith T. 2017-06-15
				-- Filter Cost Center that login user no permission to access
				IF (@ASSET_LOCATION='I')
				BEGIN
			    	DELETE	T
				    FROM	#T_DATA_PER_COST_CODE T
				    LEFT	JOIN
				     		dbo.FN_FD0COSTCENTERLIST(@EMP_CODE) S
				    ON	  	T.COST_CENTER_CODE = S.COST_CODE AND S.COMPANY	= @COMPANY
				    WHERE	S.COST_CODE IS NULL 
					        and @DOC_NO is null   -- add by pla for delete cost center only link from 2.6.3
				END


				
				 INSERT INTO #T_DATA_PER_SV (ACTUAL_SCAN, TOTAL_ASSET,TARGET_DATE_TO,MAX_SCAN_DATE,ALL_SEND_REQUEST)
				 SELECT SUM(ACTUAL_SCAN) , 
						SUM(TOTAL_ASSET),
						@TARGET_DATE,MAX(MAX_SCAN_DATE),
						'Y' AS ALL_SEND_REQUEST
				 FROM  #T_DATA_PER_COST_CODE
				

				UPDATE #T_DATA_PER_SV
				SET ALL_SEND_REQUEST =  CASE
									   WHEN  (SELECT COUNT(*) AS NOT_SEND 
											   FROM #T_DATA_PER_COST_CODE
											   WHERE DOC_NO IS NULL)> 0 THEN 'N'
										ELSE 'Y' END
				
			--	select 'test#5',* from #T_DATA_PER_SV
 ----------------------------------------------------------------------------------------------------------------
				  UPDATE #T_DATA_PER_SV
				  SET 	    PERSEN =CONCAT(Cast(Cast(CAST(ISNULL(ACTUAL_SCAN,0) AS decimal)/ CAST(TOTAL_ASSET AS decimal)*100 as decimal(18,2)) as varchar(6)) , '%' )
						  ,[DESCRIPTION]   =CONCAT( ISNULL(ACTUAL_SCAN,0),'/', ISNULL(TOTAL_ASSET,0))
						 -- ,CLASS   = CASE -- Update progress  On progress,Complete , Complete with Delay , Complete with Lose
							--					   WHEN (ALL_SEND_REQUEST='Y') AND (ACTUAL_SCAN = TOTAL_ASSET)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)   THEN @Status_Complete
							--					   WHEN (ALL_SEND_REQUEST='Y') AND (ACTUAL_SCAN = TOTAL_ASSET)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)    THEN @Status_Complete_WithDelay
							--					   WHEN (ALL_SEND_REQUEST='Y') AND (ACTUAL_SCAN < TOTAL_ASSET)  THEN @Status_Complete_WithLose 
							--					   --- not send
							--					      WHEN (ALL_SEND_REQUEST='N')  AND (ACTUAL_SCAN < TOTAL_ASSET) AND (MAX_SCAN_DATE <= TARGET_DATE_TO)   THEN @Status_OnProgress
							--						  WHEN (ALL_SEND_REQUEST='N')  AND (ACTUAL_SCAN < TOTAL_ASSET) AND (MAX_SCAN_DATE > TARGET_DATE_TO)    THEN @Status_Delay
							--						  WHEN (ALL_SEND_REQUEST='N')  AND (ACTUAL_SCAN = TOTAL_ASSET) AND (MAX_SCAN_DATE <= TARGET_DATE_TO)   THEN @Status_Complete
							--					      WHEN (ALL_SEND_REQUEST='N')  AND (ACTUAL_SCAN = TOTAL_ASSET) AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_Delay
							--					     ELSE @Status_OnProgress END
						 --,ICON   = CASE -- Update progress   TRIANGLE		 -- CROSS		 -- CIRCLE
							--					   WHEN (ALL_SEND_REQUEST='Y') AND (ACTUAL_SCAN = TOTAL_ASSET)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_EVA_CIRCLE
							--					   WHEN (ALL_SEND_REQUEST='Y') AND (ACTUAL_SCAN = TOTAL_ASSET)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_EVA_TRIANGLE
							--					   WHEN (ALL_SEND_REQUEST='Y') AND (ACTUAL_SCAN < TOTAL_ASSET)  THEN @Status_EVA_CROSS												  
							--					   ELSE ''  END
						

						DECLARE @CLASS_ALL AS Varchar(50) = '';
						DECLARE @IMAGE_TYPE AS Varchar(20) = '';
						
						
						SELECT @CLASS_ALL =  CASE WHEN count(STATUS) > 0 THEN @Status_Complete ELSE @CLASS_ALL END,
						       @IMAGE_TYPE = CASE WHEN count(STATUS) > 0 THEN @Status_EVA_CIRCLE ELSE @IMAGE_TYPE END
						FROM #T_DATA_PER_COST_CODE
						WHERE STATUS in (@Status_Complete)

						SELECT @CLASS_ALL =  CASE WHEN count(STATUS) > 0 THEN @Status_OnProgress ELSE @CLASS_ALL END,
						         @IMAGE_TYPE = CASE WHEN count(STATUS) > 0 THEN @Status_EVA_CIRCLE ELSE @IMAGE_TYPE END
						FROM #T_DATA_PER_COST_CODE
						WHERE STATUS in (@Status_OnProgress,@Status_Not_Yet_Scan)
						
						SELECT @CLASS_ALL =  CASE WHEN count(STATUS) > 0 THEN @Status_Delay ELSE @CLASS_ALL END,
						         @IMAGE_TYPE = CASE WHEN count(STATUS) > 0 THEN @Status_EVA_TRIANGLE ELSE @IMAGE_TYPE END
						FROM #T_DATA_PER_COST_CODE
						WHERE STATUS in (@Status_Delay,@Status_Complete_WithDelay)

						SELECT @CLASS_ALL =  CASE WHEN count(STATUS) > 0 THEN @Status_Complete_WithLose ELSE @CLASS_ALL END,
						       @IMAGE_TYPE = CASE WHEN count(STATUS) > 0 THEN @Status_EVA_CROSS ELSE @IMAGE_TYPE END
						FROM #T_DATA_PER_COST_CODE
						WHERE STATUS = @Status_Complete_WithLose

						 UPDATE #T_DATA_PER_SV
						 SET CLASS = @CLASS_ALL, ICON=@IMAGE_TYPE

	              --- Clone table  time
	             SELECT PERSEN,CLASS,TARGET_DATE_TO AS MIN_SCAN_DATE,MAX_SCAN_DATE
				         ,TOTAL_ASSET AS TOTAL_TIME_MINUTE ---temp field
				        ,TOTAL_ASSET AS  USED_TIME_MINUTE  --temp field
						,ALL_SEND_REQUEST
						,CLASS AS [STATUS] --- Is same sv
				 INTO #T_TIME_PROCESS
				 FROM #T_DATA_PER_SV 
				 WHERE 1<>1
	             

				DECLARE @MaxDate DATETIME,
						@TotalWorkingMinute INT

				SELECT	TOP 1 @MaxDate = ISNULL(MAX_SCAN_DATE,GETDATE())
				FROM	#T_DATA_PER_SV


				SET @StockKey = CONCAT(@YEAR ,@ROUND , @ASSET_LOCATION)

				EXEC sp_WFD02640_GetTotalWorkingTime @START_DATE, @MaxDate, @StockKey,  @TotalWorkingMinute OUT

				

				 INSERT INTO #T_TIME_PROCESS ([STATUS],MIN_SCAN_DATE,MAX_SCAN_DATE,USED_TIME_MINUTE,ALL_SEND_REQUEST)
				 SELECT CLASS,@START_DATE, 
				         ISNULL(MAX_SCAN_DATE,GETDATE()),
						 @TotalWorkingMinute
						 -- DATEDIFF(MINUTE, @START_DATE,ISNULL(MAX_SCAN_DATE,GETDATE()))
					   ,ALL_SEND_REQUEST
				 FROM #T_DATA_PER_SV
	
			

  			  UPDATE #T_TIME_PROCESS
				 SET TOTAL_TIME_MINUTE = (SELECT TOP 1 TIME_MINUTE FROM #T_STOCK_TAKE_D_PER_SV)
				     --,MIN_SCAN_DATE = ISNULL(( SELECT  MIN(SCAN_DATE) AS MIN_SCAN_DATE
									--							   FROM #T_STOCK_TAKE_D stockD
									--							   WHERE CHECK_STATUS='Y'
									--							   GROUP BY SV_EMP_CODE   ),MIN_SCAN_DATE)


				
                UPDATE #T_TIME_PROCESS
				SET PERSEN = CASE 
							WHEN USED_TIME_MINUTE > TOTAL_TIME_MINUTE THEN '100.00%'
							WHEN USED_TIME_MINUTE > 0 AND TOTAL_TIME_MINUTE > 0 AND USED_TIME_MINUTE <= TOTAL_TIME_MINUTE THEN CONCAT(Cast(Cast(CAST(USED_TIME_MINUTE AS decimal)/ CAST(TOTAL_TIME_MINUTE AS decimal)*100 as decimal(18,2)) as varchar(6)) , '%' )
					        ELSE '0.00%' END
				  --- Clase is use same SV Data 
				  ,CLASS = CASE WHEN USED_TIME_MINUTE > TOTAL_TIME_MINUTE THEN @Status_Delay
							WHEN ALL_SEND_REQUEST ='N'  AND USED_TIME_MINUTE > 0 AND TOTAL_TIME_MINUTE > 0 AND USED_TIME_MINUTE <= TOTAL_TIME_MINUTE THEN @Status_OnProgress
							WHEN ALL_SEND_REQUEST ='Y' AND  USED_TIME_MINUTE <= TOTAL_TIME_MINUTE THEN @Status_Complete
					        ELSE @Status_OnProgress END
							
			


 
					  --UPDATE #T_DATA_PER_SV  
					  --SET ICON   = CASE 
							--					   WHEN   (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_EVA_CIRCLE
							--					   WHEN   (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_EVA_TRIANGLE
			
							--					   ELSE 'kk'  END

						
						
	

	     ----------------------- Return value  --------------------
		 	-- select '90.50%' AS PERSEN , @Status_Complete AS [STATUS] , @Status_Complete AS [CLASS]
			 SELECT PERSEN,[STATUS],CLASS
			 FROM #T_TIME_PROCESS 


  
         --SELECT '90.50%' AS PERSEN, @STATUS_COMPLETE AS CLASS, @STATUS_EVA_CROSS AS ICON,'399/400' AS DESCRIPTION
		     SELECT PERSEN,CLASS,ICON,[DESCRIPTION]
			 FROM  #T_DATA_PER_SV 



		/*-- SELECT 'ACP0000Z' AS COST_CENTER_CODE,'Name of ACP0000Z' AS COST_CENTER_NAME
         -- ,'90.45%' AS PERSEN,'104/100' AS TOTAL,'Complete' AS STATUS,'90.45% On process' AS DESCRIPTION,'comment' AS COMMENT
		 -- ,'' AS ATTACHMENT
		 -- ,'' AS CHECKED  */
		 IF(@ASSET_LOCATION='I')
		 BEGIN
		       SELECT  COST_CENTER_CODE, COST_CENTER_NAME , PERSEN, TOTAL,[STATUS],[DESCRIPTION]
				     ,COMMENT   , ATTACHMENT  
					 , CASE   WHEN DOC_NO IS NOT NULL THEN 'Y'
					ELSE 'N' END AS CHECKED 
				FROM #T_DATA_PER_COST_CODE
         END
		 ELSE
		 BEGIN
		 	 SELECT  COST_CENTER_CODE, COST_CENTER_NAME , PERSEN, TOTAL,[STATUS],[DESCRIPTION]
				     ,COMMENT   , ATTACHMENT  
					 , CASE   WHEN DOC_NO IS NOT NULL THEN 'Y'
					ELSE 'N' END AS CHECKED 
				FROM #T_DATA_PER_COST_CODE
		 END



		BEGIN /* ==============================  CREATE Temp TABLE ============================== */
				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_H') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_H
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D_PER_SV') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D_PER_SV
				END

				---- Temp data for retrun to screen 
				IF OBJECT_ID('tempdb..#T_TIME_PROCESS') IS NOT NULL
				BEGIN
						DROP TABLE #T_TIME_PROCESS
				END

				IF OBJECT_ID('tempdb..#T_DATA_PER_SV') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_PER_SV
				END

				IF OBJECT_ID('tempdb..#T_DATA_PER_COST_CODE') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_PER_COST_CODE
				END
		END


END
GO
