DROP PROCEDURE [dbo].[sp_WFD01110_GetStockTake]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 21/02/2017
-- Description:	Get Stocktake table
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetStockTake]
	@EMP_CODE T_SYS_USER
	, @pageNum INT
  , @pageSize INT
   , @sortColumnName VARCHAR(50)
	, @orderType VARCHAR(5)
	, @TOTAL_ITEM		int output
AS
BEGIN
  --begin modify by thanapon: fixed paging and sorting
  IF(@sortColumnName IS NULL )
  BEGIN
   SET @sortColumnName ='0';
  END
  IF(@orderType IS NULL )
  BEGIN
   SET @orderType ='asc';
  END
  
  DECLARE @FROM	INT
			  , @TO	INT

	SET @FROM = (@pageSize * (@pageNum-1)) + 1;
	SET @TO = @pageSize * (@pageNum);
  --end modify by thanapon: fixed paging and sorting
  
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @T Table (STOCK_TAKE_KEY varchar(7))
Insert @T Exec sp_WFD01110_CheckStockPlan @EMP_CODE
		select 
		@TOTAL_ITEM = count(*)
		from [TB_R_STOCK_TAKE_INVALID_CHECKING] H
	    join TB_R_STOCK_TAKE_D D 
		on H.STOCK_TAKE_KEY = D.STOCK_TAKE_KEY 
		and d.ASSET_NO = h.ASSET_NO
		left join TB_M_EMPLOYEE e 
		on h.EMP_CODE = e.EMP_CODE
		--where D.EMP_CODE <> @EMP_CODE
		where d.CHECK_STATUS = 'N'
		and d.STOCK_TAKE_KEY in (Select * from @T);


WITH Paging_Transfer AS
  (
    --begin modify by thanapon: fixed paging and sorting
    SELECT ASSET_NO, ASSET_NAME, COST_CENTER_OWNER, COST_CENTER_FOUND, FOUND_BY
        ,(CASE WHEN @sortColumnName = '0' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_NO asc)  
               WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_NO desc)   
               WHEN @sortColumnName = '1' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_NAME asc)  
               WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_NAME desc)  
               WHEN @sortColumnName = '2' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER_OWNER asc)  
               WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER_OWNER desc)
               WHEN @sortColumnName = '3' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER_FOUND asc)
               WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CENTER_FOUND desc)
               WHEN @sortColumnName = '4' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY FOUND_BY asc)
               WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY FOUND_BY desc)  
        END) AS ROWNUMBER
        --end modify by thanapon: fixed paging and sorting
    FROM (  
  		select 
    		  D.ASSET_NO
    		  ,D.ASSET_NAME
    		  ,CONCAT(D.COST_CODE,':',D.COST_NAME) COST_CENTER_OWNER
    		  ,CONCAT(H.COST_CODE,':',H.COST_NAME) COST_CENTER_FOUND
    		  ,CONCAT(e.EMP_NAME,' ', e.EMP_LASTNAME) FOUND_BY -- Add space between name & lastname : 2017.06.15
    		from [TB_R_STOCK_TAKE_INVALID_CHECKING] H
    		join TB_R_STOCK_TAKE_D D 
			on H.STOCK_TAKE_KEY = D.STOCK_TAKE_KEY 
			and d.ASSET_NO = h.ASSET_NO
    		left join TB_M_EMPLOYEE e 
			on h.EMP_CODE = e.EMP_CODE
    		--where D.EMP_CODE <> @EMP_CODE
			where 1=1
			and d.CHECK_STATUS = 'N'
    		and d.STOCK_TAKE_KEY in (Select * from @T)
		) T
		)
    
      --begin modify by thanapon: fixed paging and sorting
  SELECT *
  FROM Paging_Transfer d
  WHERE  d.ROWNUMBER >= @FROM and d.ROWNUMBER <= @TO
  ORDER BY d.ROWNUMBER
  --end modify by thanapon: fixed paging and sorting
END
GO
