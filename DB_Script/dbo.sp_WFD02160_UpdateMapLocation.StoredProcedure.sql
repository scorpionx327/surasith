DROP PROCEDURE [dbo].[sp_WFD02160_UpdateMapLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====WFD02160 : Fixed Asset Map Location ==================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/06 (yyyy/mm/dd)
*/
CREATE PROCEDURE [dbo].[sp_WFD02160_UpdateMapLocation]
	(	@COMPANY		T_COMPANY = NULL,
		@COST_CODE		T_COST_CODE = NULL,		
		@MAP_PATH		VARCHAR(200) = NULL,
		@UPDATE_BY		VARCHAR(8),
		@UPDATE_DATE	VARCHAR(50)											
	)
AS
BEGIN TRY
	
--IF Update date not sam
   --    IF NOT EXISTS( 
			--SELECT  1 FROM TB_M_COST_CENTER
			--WHERE COST_CODE =@COST_CODE  AND FORMAT(UPDATE_DATE,'ddMMyyyyHHmmssfff') =@UPDATE_DATE)
   --          BEGIN
			--		 --SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'
			--			--SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
			--			DECLARE @Msg VARCHAR(500)

   --                   SET		@Msg = CONCAT('[CUSTOM]MCOM0008AERR|' ,dbo.fn_GetMessage('MCOM0008AERR'));

			--			RAISERROR (@Msg, -- Message text.
			--				   16, -- Severity.
			--				   1 -- State.
			--				   );
			--			RETURN;
			--   END

			 UPDATE CostCode
				 SET CostCode.MAP_PATH=@MAP_PATH
					 , CostCode.UPDATE_BY=@UPDATE_BY
					 , CostCode.UPDATE_DATE=GETDATE()
			 FROM TB_M_COST_CENTER CostCode
			 WHERE COMPANY = @COMPANY AND CostCode.COST_CODE=@COST_CODE
END TRY
BEGIN CATCH
	
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH
GO
