DROP PROCEDURE [dbo].[sp_LFD02660_GetFAUpdateScan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nuttapon
-- Create date: 24-10-2017
-- Description:	Add REASON and Plan Start  and Plan End
-- =============================================
-- Nipon 2019/08/29 add field 
--EXEC sp_LFD02660_GetFAUpdateScan '2017','35','I','SMETR002'
CREATE PROCEDURE [dbo].[sp_LFD02660_GetFAUpdateScan]
(
	@YEAR VARCHAR(4),								
	@ROUND VARCHAR(2),																		
	@ASSET_LOCATION VARCHAR(1),																			
	@COST_CENTER_CODE VARCHAR(30),																		
	@DOCUMENT_NO VARCHAR(10) = NULL,	
	@COMPANY T_COMPANY
)
AS
BEGIN
SET NOCOUNT ON;

	DECLARE @bSubmitFlag VARCHAR(1) = 'Y'
	IF ISNULL(@DOCUMENT_NO,'') = ''
	BEGIN
		SELECT	TOP 1 @DOCUMENT_NO = DOC_NO 
		FROM	TB_R_REQUEST_STOCK 
		WHERE	[YEAR] = @YEAR AND [ROUND] = @ROUND AND ASSET_LOCATION = @ASSET_LOCATION  
		        AND @COST_CENTER_CODE = IIF(@ASSET_LOCATION = 'I', COST_CODE, SV_EMP_CODE)
				


	END

	IF ISNULL(@DOCUMENT_NO,'') = ''
	BEGIN
		SET @bSubmitFlag = 'N'
	END 
	
	DECLARE @IS_APPROVE INT = 0,
			@LOGO_PATH VARCHAR(550) = NULL,
			@PERIOD_DESC VARCHAR(10) = NULL,
			@EVA_TYPE CHAR(1) = NULL,
			@STOCK_TAKING_CODE VARCHAR(8) = NULL,
			@STOCK_TAKING_NAME VARCHAR(50) = NULL,
			@FOUND INT = 0,
			@NOTFOUND INT = 0,
			@MIN_DATE DATETIME = NULL,		
			@MAX_DATE DATETIME = NULL,			
			@COUNTING_TIME INT = 0,
			@SPENDING_TIME VARCHAR(10) = NULL,
			@COMMENT VARCHAR(1000) = NULL,			
			@GM_USER VARCHAR(500) = NULL,			
			@GM_DATE DATETIME = NULL,			
			@DGM_USER VARCHAR(500) = NULL,			
			@DGM_DATE DATETIME = NULL,			
			@DM_USER VARCHAR(500) = NULL,			
			@DM_DATE DATETIME = NULL,			
			@SV_USER VARCHAR(500) = NULL,			
			@SV_DATE DATETIME = NULL,		
			@GL_USER VARCHAR(500) = NULL,			
			@GL_DATE DATETIME = NULL,
			
			@GM_ROLE VARCHAR(500) = NULL,
			@DGM_ROLE VARCHAR(500) = NULL,
			@DM_ROLE VARCHAR(500) = NULL,
			@SV_ROLE VARCHAR(500) = NULL,
					
			@TEMP_GL_USER VARCHAR(500) = NULL,
			@TEMP_MAX_DATE DATETIME = NULL,
			@TEMP_SV_DATE DATETIME = NULL,
			@PLAN_START DATETIME = NULL,
			@REASON  VARCHAR(500) = NULL,
			@COUNT_TIME_NO_BREAK INT = 0

	-------------------------Find Stock taking result-----------------------------
	IF OBJECT_ID('tempdb..#TEMP_STOCK_TAKING_RESULT') IS NOT NULL
	BEGIN
		DROP TABLE #TEMP_STOCK_TAKING_RESULT
	END	
	SELECT DISTINCT 
			IIF(STOCK.CHECK_STATUS = 'Y', 1, 0) AS FOUND_TYPE, 			
            STOCK.SUB_TYPE,
			STOCK.ASSET_NO AS ASSET_NO, 
			STOCK.ASSET_NAME AS ASSET_NAME,
			STOCK.COUNT_TIME AS COUNT_TIME,
			STOCK.SCAN_DATE AS SCAN_DATE,
			STOCK.START_COUNT_TIME AS START_COUNT_TIME,
			STOCK.END_COUNT_TIME AS END_COUNT_TIME,
			STOCK.SV_EMP_CODE AS SV_EMP_CODE,
			STOCK.COST_CODE AS COST_CODE,
			STOCK.EMP_CODE As EMP_CODE

	INTO #TEMP_STOCK_TAKING_RESULT
	  FROM TB_R_STOCK_TAKE_D STOCK
	 WHERE STOCK.[YEAR] = @YEAR
		and STOCK.COMPANY=@COMPANY
	   AND STOCK.[ROUND] = @ROUND
	   AND STOCK.ASSET_LOCATION = @ASSET_LOCATION
	   --AND @COST_CENTER_CODE = IIF(@ASSET_LOCATION = 'I', STOCK.COST_CODE, @COST_CENTER_CODE)
	   AND @COST_CENTER_CODE = IIF(@ASSET_LOCATION = 'I', STOCK.COST_CODE, STOCK.SV_EMP_CODE)
	   ---com
	--------------------------------Find Request is approve---------------------------------	
	/*SELECT @IS_APPROVE = COUNT(*) FROM TB_R_REQUEST_H
	WHERE [STATUS]  = '60' AND DOC_NO= @DOCUMENT_NO;*/

	SELECT @IS_APPROVE = COUNT(*) FROM TB_R_STOCK_TAKE_H
	WHERE ( @bSubmitFlag = 'Y') AND COMPANY=@COMPANY AND [YEAR] = @year and [ROUND] = @ROUND and [ASSET_LOCATION] = @ASSET_LOCATION;
	
	
	IF (@IS_APPROVE > 0) 
	BEGIN
		--------------------------------Find Logo path-----------------------------------	
		SELECT @LOGO_PATH = VALUE
		  FROM TB_M_SYSTEM
		  WHERE CATEGORY = 'PRINT_TAG' 
		  AND SUB_CATEGORY = 'LOGO-'+@COMPANY
		  AND ACTIVE_FLAG='Y'
		  --AND CODE = 'LFD02660'

		  /*
		  select value from tb_m_system where category='PRINT_TAG' and sub_category='LOGO-' + [company]  + and active_flag='Y'
		  */

		--------------------------------Find Period Desc---------------------------------
		SET @PERIOD_DESC =  RIGHT('00'+ISNULL(@ROUND,''),2) + '/' + @YEAR; 
			
		--Find Cost Center or Supplier
		IF (@ASSET_LOCATION = 'I')
			BEGIN
				SELECT @STOCK_TAKING_CODE = COST_CODE, @STOCK_TAKING_NAME = COST_NAME
				FROM TB_M_COST_CENTER 
				WHERE COST_CODE = @COST_CENTER_CODE	
				AND COMPANY=@COMPANY

			END
		ELSE
			BEGIN
				SELECT @STOCK_TAKING_CODE = EMP_CODE, @STOCK_TAKING_NAME = EMP_NAME
				FROM TB_R_STOCK_TAKE_D_PER_SV 
				WHERE [YEAR] = @YEAR	
				AND [ROUND] = @ROUND
				AND [ASSET_LOCATION] = 'O'
				AND [EMP_CODE] = @COST_CENTER_CODE
				AND TB_R_STOCK_TAKE_D_PER_SV.COMPANY=@COMPANY

			END
 
		-------------------------------------Find Count Found---------------------------------------
		SELECT @FOUND = COUNT(*) 
		  FROM #TEMP_STOCK_TAKING_RESULT 
		 WHERE FOUND_TYPE = 1;

		------------------------------------Find Count Not found------------------------------------
		SELECT @NOTFOUND = COUNT(*) 
		  FROM #TEMP_STOCK_TAKING_RESULT 
		 WHERE FOUND_TYPE = 0; 		
	
		----------------------------------------Find Min date---------------------------------------
		SELECT @MIN_DATE = MIN([START_COUNT_TIME]) 
		  FROM #TEMP_STOCK_TAKING_RESULT; 	

		-----------------------------------------Find Max date--------------------------------------
		SELECT @MAX_DATE = MAX([END_COUNT_TIME]) 
		  FROM #TEMP_STOCK_TAKING_RESULT; 

		-------------------------------------------Find Sum Counting time---------------------------
		SELECT @COUNTING_TIME = SUM([COUNT_TIME]) 
		  FROM #TEMP_STOCK_TAKING_RESULT;

		SELECT @SPENDING_TIME = 
					RIGHT('0'+CAST(@COUNTING_TIME / 3600 AS VARCHAR),2) +':'+ 
					RIGHT('0'+CAST((@COUNTING_TIME / 60) % 60 AS VARCHAR),2) +':'+
					RIGHT('0'+CAST(@COUNTING_TIME % 60 AS VARCHAR),2); 

		--SELECT @COUNT_TIME_NO_BREAK = SUM([COUNT_TIME_NO_BREAK]) 
		--  FROM #TEMP_STOCK_TAKING_RESULT;
		
		--SELECT @SPENDING_TIME = 
		--		RIGHT('0'+CAST(@COUNT_TIME_NO_BREAK / 3600 AS VARCHAR),2) +':'+ 
		--		RIGHT('0'+CAST((@COUNT_TIME_NO_BREAK / 60) % 60 AS VARCHAR),2) +':'+
		--		RIGHT('0'+CAST(@COUNT_TIME_NO_BREAK % 60 AS VARCHAR),2); 

	    --------------------------------------Find EVA----------------------------------------------
		DECLARE @SV_CODE VARCHAR(8);

		SELECT @TEMP_MAX_DATE = MAX(END_COUNT_TIME),
		       @SV_CODE = MAX(SV_EMP_CODE)
		  FROM #TEMP_STOCK_TAKING_RESULT;				

		SELECT @TEMP_SV_DATE = CASE
			WHEN (SV.DATE_TO IS NOT NULL) AND (SV.TIME_END IS NOT NULL) THEN
			CONVERT(DATETIME,
					CONCAT(CONVERT(VARCHAR(8), SV.DATE_TO, 112),
							' ',
							SV.TIME_END,
							':00'))
			WHEN (SV.DATE_TO IS NOT NULL) AND (SV.TIME_END IS NULL) THEN
			CONVERT(DATETIME,
					CONCAT(CONVERT(VARCHAR(8), SV.DATE_TO, 112),
							' ',
							'23:59:59'))
			ELSE
			NULL
		END,
	      	@PLAN_START = CASE
			WHEN (SV.DATE_FROM IS NOT NULL) AND (SV.TIME_START IS NOT NULL) THEN
			CONVERT(DATETIME,
					CONCAT(CONVERT(VARCHAR(8), SV.DATE_FROM, 112),
							' ',
							SV.TIME_START,
							':00'))
			WHEN (SV.DATE_FROM IS NOT NULL) AND (SV.TIME_START IS NULL) THEN
			CONVERT(DATETIME,
					CONCAT(CONVERT(VARCHAR(8), SV.DATE_FROM, 112),
							' ',
							'00:00:00'))
			ELSE
			NULL
		END

		FROM TB_R_STOCK_TAKE_D_PER_SV SV
			WHERE SV.[YEAR] = @YEAR
			AND SV.[ROUND] = @ROUND
			AND SV.ASSET_LOCATION = @ASSET_LOCATION
			AND COMPANY=@COMPANY
			AND SV.EMP_CODE = @SV_CODE;

		--print @TEMP_MAX_DATE
		--print @TEMP_SV_DATE
		--print 'xxx'

		SELECT	@REASON = R.COMMENT 
		FROM	TB_R_REQUEST_STOCK R 
		WHERE	[YEAR] = @YEAR AND [ROUND] = @ROUND AND ASSET_LOCATION = @ASSET_LOCATION  
		        AND @COST_CENTER_CODE = IIF(@ASSET_LOCATION = 'I', COST_CODE, SV_EMP_CODE)

		IF (@NOTFOUND > 0)
			BEGIN
				SET @EVA_TYPE = 'X'
				SET @REASON = 'LOSS : '+@REASON
			END
	    ELSE IF (@TEMP_MAX_DATE > @TEMP_SV_DATE)
			BEGIN
				SET @EVA_TYPE = 'A'
				SET @REASON = 'DELAY : '+@REASON
			END
		ELSE
			BEGIN
				SET @EVA_TYPE = 'O'
				SET @REASON = '-'
			END	
		
		-----------------------------------Find Comment---------------------------------------------	
		SELECT @COMMENT = CONCAT(@COMMENT,'|','') + ltrim(rtrim(COMMENT))
			FROM (
					SELECT RTRIM(TB_M_EMPLOYEE.EMP_NAME) + ' ' + 
							   LEFT(TB_M_EMPLOYEE.EMP_LASTNAME, 1) + '. : ' + 
							   TB_R_REQUEST_COMMENT.COMMENT AS COMMENT	, INDX , COMMENT_HISTORY_ID
					FROM TB_R_REQUEST_H										
					INNER JOIN TB_R_REQUEST_COMMENT ON TB_R_REQUEST_H.DOC_NO = TB_R_REQUEST_COMMENT.DOC_NO 
					LEFT JOIN TB_M_EMPLOYEE ON TB_R_REQUEST_COMMENT.EMP_CODE = TB_M_EMPLOYEE.SYS_EMP_CODE 
					WHERE TB_R_REQUEST_H.DOC_NO = @DOCUMENT_NO
					AND REQUEST_TYPE = 'S'
			 ) A
        ORDER BY COMMENT_HISTORY_ID, INDX asc --Fixed Sorting

		----------------------------------Find User GM-------------------------1-----------
		--SELECT @GM_USER = RTRIM(EMP_NAME) + ' ' + LEFT(EMP_LASTNAME, 1) + '.', 
		--	 @GM_DATE = APPR_DATE
		--  FROM TB_R_REQUEST_APPR
		--  WHERE DOC_NO = @DOCUMENT_NO
		--   AND [ROLE] = 'GM'
			IF OBJECT_ID('tempdb..#T_USER') IS NOT NULL
			BEGIN
				DROP TABLE #T_USER
			END	

		DECLARE @INDX INT
		DECLARE @USER1 VARCHAR(200)
		DECLARE @USER1_DATE DateTime
		DECLARE @ACTUAL_ROLE VARCHAR(200)

		SELECT TOP 4  INDX,
		case when R.APPR_STATUS = 'A' then RTRIM(EMP_NAME) + ' ' + LEFT(EMP_LASTNAME, 1) + '.'
		     Else ''
			 END AS M_USER,
        case when R.APPR_STATUS = 'A' then APPR_DATE
		     Else Null
			 END AS APPR_DATE,
			 S.value AS ACTUAL_ROLE
		INTO #T_USER
		FROM TB_R_REQUEST_APPR R left join TB_M_SYSTEM S on S.CATEGORY = 'RESPONSIBILITY' and S.SUB_CATEGORY = 'ROLE_CODE'
		--and S.CODE = R.ROLE
		WHERE DOC_NO = @DOCUMENT_NO 
		ORDER BY INDX DESC 

		SET @INDX=1;

		DECLARE tt_cursor CURSOR FOR
  			     SELECT  M_USER ,   APPR_DATE, ACTUAL_ROLE
				 FROM #T_USER
				 ORDER BY INDX ASC 
		OPEN tt_cursor
		FETCH NEXT FROM tt_cursor
			  INTO @USER1,@USER1_DATE, @ACTUAL_ROLE
		 WHILE @@FETCH_STATUS = 0
			BEGIN 
		      IF(@INDX=1)
			  BEGIN
			 	SET	@SV_USER = @USER1;
	            SET	@SV_DATE = @USER1_DATE;
				SET @SV_ROLE = @ACTUAL_ROLE;
			  END
			  IF(@INDX=2)
			  BEGIN
			  	SET	@DM_USER  = @USER1;
	            SET	@DM_DATE = @USER1_DATE;
				SET @DM_ROLE = @ACTUAL_ROLE;
			  END
			  IF(@INDX=3)
			  BEGIN
			  	SET	@DGM_USER = @USER1;
	            SET	@DGM_DATE = @USER1_DATE;
	            SET	@DGM_ROLE = @ACTUAL_ROLE;
			  END
			  IF(@INDX=4)
			  BEGIN
			  	SET	@GM_USER  = @USER1;
	            SET	@GM_DATE = @USER1_DATE;
				SET	@GM_ROLE = @ACTUAL_ROLE;
			  END
			  SET @INDX=@INDX+1;
			FETCH NEXT FROM tt_cursor
			INTO @USER1,@USER1_DATE, @ACTUAL_ROLE
		   END 
		 CLOSE tt_cursor
		 DEALLOCATE tt_cursor

/**================================================ backup =======================================

		     SELECT @USER1 = Request.M_USER,@USER1_DATE=APPR_DATE ,@INDX=INDX
		    FROM  ( 
				     SELECT TOP 1 RTRIM(EMP_NAME) + ' ' + LEFT(EMP_LASTNAME, 1) + '.'  AS M_USER,   APPR_DATE,INDX
				     FROM TB_R_REQUEST_APPR
					 WHERE DOC_NO = @DOCUMENT_NO 
			         ORDER BY INDX DESC 
					 )Request

        
			----------------------------------Find User DGM---------------------------2---------
				  SELECT @USER1 = Request.M_USER,@USER1_DATE=APPR_DATE ,@INDX=INDX
					FROM  ( 
								 SELECT TOP 1 RTRIM(EMP_NAME) + ' ' + LEFT(EMP_LASTNAME, 1) + '.'  AS M_USER,   APPR_DATE,INDX
						 FROM TB_R_REQUEST_APPR
						 WHERE DOC_NO = @DOCUMENT_NO  
						 ORDER BY INDX DESC 
					 )Request
	  
		----------------------------------Find User DM-----------------------------3-------
                SELECT @USER1 = Request.M_USER,@USER1_DATE=APPR_DATE ,@INDX=INDX
					FROM  ( 
								 SELECT TOP 1 RTRIM(EMP_NAME) + ' ' + LEFT(EMP_LASTNAME, 1) + '.'  AS M_USER,   APPR_DATE,INDX
						 FROM TB_R_REQUEST_APPR
						 WHERE DOC_NO = @DOCUMENT_NO 
					
						 ORDER BY INDX DESC 
					 )Request
	

		----------------------------------Find User SV-----------------------------4-------
               SELECT @USER1 = Request.M_USER,@USER1_DATE=APPR_DATE ,@INDX=INDX
					FROM  ( 
					SELECT TOP 1 RTRIM(EMP_NAME) + ' ' + LEFT(EMP_LASTNAME, 1) + '.'  AS M_USER,   APPR_DATE,INDX
						 FROM TB_R_REQUEST_APPR
						 WHERE DOC_NO = @DOCUMENT_NO 
					
						 ORDER BY INDX DESC 
					 )Request
=============================================== backup =======================================*/


	IF OBJECT_ID('tempdb..#INDX') IS NOT NULL
	BEGIN
		DROP TABLE #INDX
	END	

		----------------------------------Find User GL----------------------------------		
		--SELECT @GL_USER = CONCAT(@GL_USER,'|','') + ltrim(rtrim(EMP_NAME))
		--	FROM (
		--			SELECT TOP 2 RTRIM(E.EMP_NAME) + ' ' +
		--				 LEFT(E.EMP_LASTNAME, 1) + '.' AS EMP_NAME,
		--				 D.SCAN_DATE AS SCAN_DATE						
		--			FROM #TEMP_STOCK_TAKING_RESULT D
		--			INNER JOIN TB_M_EMPLOYEE E ON D.EMP_CODE = E.EMP_CODE				
		--	 ) A
  --      ORDER BY SCAN_DATE DESC		

	  SELECT @GL_USER = CONCAT(@GL_USER,' ','') + ltrim(rtrim(EMP_NAME))	
      From (select top 2 RTRIM(E.EMP_NAME) + ' ' + LEFT(E.EMP_LASTNAME, 1) + '.' AS EMP_NAME, 
            max(Scan_date) as SCAN_DATE 
	        from #TEMP_STOCK_TAKING_RESULT D
            inner join TB_M_EMPLOYEE E on D.EMP_CODE = E.SYS_EMP_CODE
            group by E.EMP_NAME, e.EMP_LASTNAME
			) A
       ORDER BY SCAN_DATE DESC		

		  SELECT @GL_DATE = MAX(SCAN_DATE) 
		  FROM #TEMP_STOCK_TAKING_RESULT;				  		
	END	   
	

	   --###### START FIX CR ADD PHOTO  ######
	  DECLARE  @ASSET_PHOTO_PATH VARCHAR(50)

	  SELECT @ASSET_PHOTO_PATH =  CONCAT([VALUE],@COMPANY,'\') FROM TB_M_SYSTEM WHERE CATEGORY= 'SYSTEM_CONFIG' AND SUB_CATEGORY='UPLOAD_PATH' AND CODE='ASSET_PATH'

	  --[PATH PHOTO] = select value + [company] + '/' and  sub_category='UPLOAD_PATH' and Code='ASSET_PATH'
	   --###### END FIX CR ADD PHOTO  ######

	------------------------------Retrieve Stock Taking Result------------------------------
	   DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)
	
	SELECT @LOGO_PATH AS "LOGO_PATH",
       @PERIOD_DESC AS "PERIOD",
       @EVA_TYPE AS "EVA",
       @STOCK_TAKING_CODE AS "COST_CENTER_CODE",
       @STOCK_TAKING_NAME AS "COST_CENTER_NAME",
       @ASSET_LOCATION AS "ASSET_LOCATION",
       @FOUND AS "FOUND",
       @NOTFOUND AS "NOT_FOUND",
       @FOUND + @NOTFOUND AS "TOTAL",
       @MIN_DATE AS "CHECKING_DATE_FROM",
       @MIN_DATE AS "CHECKING_TIME_FROM",
       @MAX_DATE AS "CHECKING_DATE_TO",
       @MAX_DATE AS "CHECKING_TIME_TO",
       @SPENDING_TIME AS "SPENDING_TIME",
       IIF(LEFT(@COMMENT, 1) = '|',
           RIGHT(@COMMENT, LEN(@COMMENT) - 1),
           @COMMENT) AS "COMMENT",
       @GM_USER AS "GM_USER",
       @GM_DATE AS "GM_DATE",
       @DGM_USER AS "DGM_USER",
       @DGM_DATE AS "DGM_DATE",
       @DM_USER AS "DM_USER",
       @DM_DATE AS "DM_DATE",
       @SV_USER AS "SV_USER",
       @SV_DATE AS "SV_DATE",
       IIF(LEFT(@GL_USER, 1) = '|',
           RIGHT(@GL_USER, LEN(@GL_USER) - 1),
           @GL_USER) AS "GL_USER",
       @GL_DATE AS "GL_DATE",
       STOCK.FOUND_TYPE AS "FOUND_TYPE",    
       S.REMARKS        as "SUB_TYPE",
       STOCK.ASSET_NO   AS "ASSET_NO",
       STOCK.ASSET_NAME AS "ASSET_NAME",
       @IS_APPROVE      AS "GEN_COVER_PAGE",     
       S.value AS SUB_TYPE_NAME,
       CASE
         WHEN STOCK.FOUND_TYPE = 0 THEN
          'Not Found'
         WHEN STOCK.FOUND_TYPE = 1 THEN
          'Found'
       END AS FOUND_TYPE_NAME,
       
       @GM_ROLE  AS "GM_ROLE",
       @DGM_ROLE AS "DGM_ROLE",
       @DM_ROLE  AS "DM_ROLE",
       @SV_ROLE  AS "SV_ROLE",
	   @MIN_DATE AS "ACTUAL_START",
       @MAX_DATE AS "ACTUAL_FINISH",
	   @PLAN_START AS "PLAN_START",
       @TEMP_SV_DATE AS "PLAN_FINISH",
	   @REASON AS "REASON",

	   --###### START FIX CR ADD PHOTO  ######
	   STOCK.SCAN_DATE AS  SCAN_DATE,
	   CONCAT(A.MANUFACT_ASSET,'                 ',A.LOCATION_REMARK) AS LOCATION_NAME,--conc
	   CONCAT(@ASSET_PHOTO_PATH,A.TAG_PHOTO) AS ASSET_PHOTO,
	   A.MINOR_CATEGORY MINOR_CATEGORY

	   --###### END FIX CR ADD PHOTO  ######	
   

  FROM #TEMP_STOCK_TAKING_RESULT STOCK
  LEFT JOIN TB_M_SYSTEM S ON S.CATEGORY = 'FAS_TYPE'
                         AND S.sub_category = 'SUB_TYPE'
                         AND S.code = STOCK.SUB_TYPE
		--###### START FIX CR ADD PHOTO  ######
  left JOIN TB_M_ASSETS_H A ON  STOCK.ASSET_NO = A.ASSET_NO AND A.COMPANY=@COMPANY 
	   --###### END FIX CR ADD PHOTO  ######					
						

END
GO
