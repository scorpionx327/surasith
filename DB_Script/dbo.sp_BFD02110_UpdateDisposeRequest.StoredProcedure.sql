DROP PROCEDURE [dbo].[sp_BFD02110_UpdateDisposeRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  For update Request History Table : Dispose Request
- ============================================= SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD02110_UpdateDisposeRequest]
(
	 @UserID		VARCHAR(8),
	 @ERROR_MESSAGE	VARCHAR(MAX) OUT
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

-- Create temp TEMP_ASSETS 
IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_ASSETS
END

-- 1 Create structure of temp table 
 	 SELECT ASSET_NO, RETIREMENT_DATE, ENTRY_PERIOD, PROCESS_STATUS, ACCUMULATE_DEPRECIATION
	 INTO	#TEMP_ASSETS 
	 FROM	TB_M_ASSETS_H
	 WHERE	1<>1

	 -- Get Asset that Complete Disposal
	INSERT	
	INTO	#TEMP_ASSETS(ASSET_NO, RETIREMENT_DATE, ENTRY_PERIOD, PROCESS_STATUS, ACCUMULATE_DEPRECIATION)
	SELECT  AssetH.ASSET_NO,S_Asset.RETIREMENT_DATE,S_Asset.ENTRY_PERIOD,AssetH.PROCESS_STATUS,
			 CASE	WHEN  IsNumeric(S_Asset.[ACCUMULATE_DEPRECIATION])=1  
					THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[ACCUMULATE_DEPRECIATION]) AS decimal(36,2))
					ELSE NULL END
		
	FROM    TB_M_ASSETS_H AssetH 
	INNER	JOIN
			TB_S_ASSETS_H S_Asset   
	ON		AssetH.ASSET_NO = S_Asset.ASSET_NO
	WHERE	AssetH.PROCESS_STATUS='DC' AND
			IIF(S_Asset.RETIREMENT_DATE IS NOT NULL, 
				CONVERT(DATE,S_Asset.RETIREMENT_DATE,106), '1900-01-01') > IIF(AssetH.RETIREMENT_DATE IS NOT NULL, 
																				CONVERT(DATE,AssetH.RETIREMENT_DATE,106), '1900-01-01')
	
  --2 Update history of request																	
        UPDATE AssetHis
		SET	 AssetHis.POSTING_DATE =CASE 
										WHEN T_Assets.RETIREMENT_DATE IS NOT NULL THEN CONVERT(DATE,T_Assets.RETIREMENT_DATE,106)
										ELSE NULL END 
		     ,AssetHis.ENTRY_PERIOD = CASE 
										WHEN T_Assets.ENTRY_PERIOD IS NOT NULL THEN CONVERT(DATE,T_Assets.ENTRY_PERIOD,106)
										ELSE NULL END 
			,AssetHis.ORACLE_UPDATE_STATUS='Y'
			,AssetHis.ACCUM_DEPREC = T_Assets.ACCUMULATE_DEPRECIATION  -- Add By Surasith Change Update Accum DP for display in summary report
			,AssetHis.UPDATE_BY=@UserID
			,AssetHis.UPDATE_DATE=GETDATE() 
		FROM	TB_R_ASSET_HIS AssetHis
		INNER	JOIN #TEMP_ASSETS T_Assets  ON AssetHis.ASSET_NO=T_Assets.ASSET_NO
		WHERE       AssetHis.ORACLE_UPDATE_STATUS='N'
			  AND	 AssetHis.REQUEST_TYPE='D' 
			  AND	 AssetHis.[STATUS] NOT IN ('90','99') 
			 
 --3 Clear data in asset location 
	    UPDATE AssetH
		SET	 AssetH.LOCATION_NAME = NULL 
			,AssetH.PROCESS_STATUS = NULL 
		FROM TB_M_ASSETS_H AssetH
		INNER JOIN #TEMP_ASSETS T_Assets  ON AssetH.ASSET_NO=T_Assets.ASSET_NO
	
--4 Clear data in asset location 
	UPDATE AssetLoc
	SET	 AssetLoc.X = NULL 
		,AssetLoc.Y = NULL 
		,AssetLoc.UPDATE_BY=@UserID
		,AssetLoc.UPDATE_DATE=GETDATE() 
	FROM TB_M_ASSETS_LOC AssetLoc
	INNER JOIN #TEMP_ASSETS T_Assets  ON AssetLoc.ASSET_NO=T_Assets.ASSET_NO

	-- Create temp TEMP_ASSETS 
	IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
	BEGIN
			DROP TABLE #TEMP_ASSETS
	END

	RETURN @Error ;
END TRY
BEGIN CATCH
			-- Create temp TEMP_ASSETS 
		IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
		BEGIN
				DROP TABLE #TEMP_ASSETS
		END
			PRINT concat('ERROR Message:',ERROR_MESSAGE())
			SET @ERROR_MESSAGE=ERROR_MESSAGE();
				RETURN 1;
END CATCH










GO
