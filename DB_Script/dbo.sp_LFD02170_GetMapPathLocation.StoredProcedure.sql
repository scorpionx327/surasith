DROP PROCEDURE [dbo].[sp_LFD02170_GetMapPathLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_GetMapPathLocation]
@COMPANY	T_COMPANY,
@COST_CODE	T_COST_CODE
AS
BEGIN	
	SET NOCOUNT ON;
	DECLARE @PATH VARCHAR(400)

	SELECT	@PATH=[VALUE]  
	FROM	TB_M_SYSTEM 
	WHERE	CATEGORY		= 'SYSTEM_CONFIG' AND 
			SUB_CATEGORY	= 'MAP_PATH'  AND 
			CODE			= 'COST_CENTER'


	SELECT	CONCAT(@PATH,'\',MAP_PATH) AS MAP_PATH
	FROM	TB_M_COST_CENTER
	WHERE	COMPANY		= @COMPANY AND
			COST_CODE	= @COST_CODE AND  
			[STATUS]	= 'Y'
		

END
GO
