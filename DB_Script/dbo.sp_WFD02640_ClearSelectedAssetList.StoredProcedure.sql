DROP PROCEDURE [dbo].[sp_WFD02640_ClearSelectedAssetList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02640_ClearSelectedAssetList]
@GUID	T_GUID
AS
BEGIN
	DELETE
	FROM	TB_T_SELECTED_ASSETS
	WHERE	[GUID] = @GUID
END
GO
