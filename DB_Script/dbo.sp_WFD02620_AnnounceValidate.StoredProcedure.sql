DROP PROCEDURE [dbo].[sp_WFD02620_AnnounceValidate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_AnnounceValidate]
	-- Add the parameters for the stored procedure here
	@STOCK_TAKE_KEY	nvarchar(7)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	select 
	CASE WHEN ASSET_LOCATION = 'O' THEN 'Supplier :'  ELSE 'S/V Name : ' END  +	 RTRIM(ISNULL(EMP_TITLE,'')) + ' ' + RTRIM(ISNULL(EMP_NAME,'')) + ' ' + RTRIM(ISNULL(EMP_LASTNAME,''))  as  NAME
	from TB_R_STOCK_TAKE_D_PER_SV 
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY and (DATE_FROM IS NULL or DATE_TO is null)

END




GO
