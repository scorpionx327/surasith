DROP PROCEDURE [dbo].[sp_BFD01160_InsertNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertNotification]
@AppID	INT,
@User	VARCHAR(8)
AS
BEGIN
	DECLARE @ErrorMessage nVARCHAR(1000) ;
	-- Incase there are an error. system continue
	BEGIN TRY
		-- CIP
		BEGIN TRY
			EXEC sp_BFD01260_InsertCIPNotification @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert CIP Notification','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
			--RAISERROR(@ErrorMessage, 16,1)
		END CATCH

		-- Insert Cost Center Notification
		BEGIN TRY
			EXEC sp_BFD01260_InsertCostCenterNotification @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Cost Center Notification','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH


		-- Insert Mark Location Notification
		BEGIN TRY
			EXEC sp_BFD01260_InsertMarkLocationNotification @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Mark Location Notification','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert PrintTag Notification
		BEGIN TRY
			EXEC sp_BFD01260_InsertPrintTagNotification @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert PrintTag Notification','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert PrintTag Notification Outsource
		BEGIN TRY
			EXEC sp_BFD01260_InsertPrintTagNotificationOutsource @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert PrintTag Notification Outsource','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert Stock Taking Notification
		BEGIN TRY
			EXEC sp_BFD01260_InsertStockTakingNotification @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Stock Taking Notification','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert Stock Taking Notification Outsource
		BEGIN TRY
			EXEC sp_BFD01260_InsertStockTakingNotificationOutsource @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Stock Taking Notification Outsouce','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert Upload Photo Notification
		BEGIN TRY
			EXEC sp_BFD01260_InsertUploadPhotoNotification @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Upload Photo Notification','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert Upload Photo Notification Outsource
		BEGIN TRY
			EXEC sp_BFD01260_InsertUploadPhotoNotificationOutsource @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Upload Photo Notification Outsource','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH

		-- Insert Found Asset Loss after create DocNo 
		BEGIN TRY
			EXEC sp_BFD01260_InsertStockTakingNotificationLoss @User
		END TRY
		BEGIN CATCH
			SET @ErrorMessage= CONCAT('Insert Found Asset Loss after create DocNo','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
			EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		END CATCH


		SELECT CASE WHEN @ErrorMessage IS NULL THEN 1 ELSE 0 END ; -- SUCCESS/Warning
	    RETURN;
	END TRY

	BEGIN CATCH
		--===========================================================Write log =====================================
		 SET @ErrorMessage= CONCAT('Insert Notification Batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
		 EXEC sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ErrorMessage, @User,'Y'
		--===========================================================End Write log =====================================
		SELECT -1 -- ERROR;
		RETURN;

	END CATCH
END



GO
