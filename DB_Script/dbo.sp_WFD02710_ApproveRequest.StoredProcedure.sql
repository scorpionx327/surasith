DROP PROCEDURE [dbo].[sp_WFD02710_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02710_ApproveRequest]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN
	
	-- Update First
	EXEC [sp_WFD02710_UpdateAssetApprove] @GUID, @DOC_NO, @USER

	IF dbo.fn_IsReadyGenFile(@DOC_NO) = 'N'
	RETURN

	UPDATE	TB_R_REQUEST_SETTLE_H
	SET		[STATUS]	= 'GEN',
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= @USER
	WHERE	DOC_NO		= @DOC_NO AND
			[STATUS]	= 'NEW'

	-- Update
	UPDATE	H
	SET		PROCESS_STATUS	= 'KC',
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @USER
	FROM	TB_M_ASSETS_H H WITH (NOLOCK)
	INNER	JOIN 
			TB_R_REQUEST_SETTLE_H T WITH (NOLOCK)
	ON		H.COMPANY	= T.COMPANY AND 
			H.ASSET_NO	= T.AUC_NO AND
			H.ASSET_SUB	= T.AUC_SUB
	WHERE	T.DOC_NO	= @DOC_NO

	UPDATE	H
	SET		PROCESS_STATUS	= 'KC',
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @USER
	FROM	TB_M_ASSETS_H H WITH (NOLOCK)
	INNER	JOIN 
			TB_T_REQUEST_SETTLE_H T WITH (NOLOCK)
	ON		H.COMPANY	= T.COMPANY AND 
			H.ASSET_NO	= T.ASSET_NO AND
			H.ASSET_SUB	= T.ASSET_SUB
	WHERE	T.DOC_NO	= @DOC_NO


	DECLARE @LatestApproverCode	T_SYS_USER, 
			@LatestApproverName	VARCHAR(68)

	DECLARE @AECCode		T_SYS_USER
	UPDATE	H
	SET		H.DETAIL		= 'Settlement',
			H.TRANS_DATE	= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= @LatestApproverName, 
			H.AEC_APPR_BY			= @AECCode,
			H.AEC_APPR_DATE			= GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_SETTLE_H M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND 
			M.DOC_NO	= H.DOC_NO AND 
			M.AUC_NO	= H.ASSET_NO AND 
			M.AUC_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO
	

END
GO
