DROP PROCEDURE [dbo].[sp_WFD02410_GetLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_GetLocation 'C20170004','AS0000000100','30000',null,'0179'
CREATE PROCEDURE [dbo].[sp_WFD02410_GetLocation]
(	
	@COMPANY_CODE				 T_COMPANY	
)
AS
BEGIN
	SELECT * 
	FROM TB_M_SYSTEM 
	WHERE CATEGORY	= 'LOCATION'		AND
	SUB_CATEGORY	= @COMPANY_CODE		AND
	ACTIVE_FLAG		= 'Y'

END


GO
