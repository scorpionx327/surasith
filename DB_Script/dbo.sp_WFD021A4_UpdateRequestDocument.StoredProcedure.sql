DROP PROCEDURE [dbo].[sp_WFD021A4_UpdateRequestDocument]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD021A4_UpdateRequestDocument]
@DOC_NO			T_DOC_NO
AS
BEGIN
	
	DECLARE @RequestType	VARCHAR(1), @Status VARCHAR(2)
	SELECT	@RequestType = REQUEST_TYPE ,
			@Status		 = [STATUS]
	FROM	TB_R_REQUEST_H
	WHERE	DOC_NO = @DOC_NO

	IF EXISTS(	SELECT	1 
				FROM	TB_M_SYSTEM 
				WHERE	CATEGORY		= 'SYSTEM_CONFIG' AND 
						SUB_CATEGORY	= 'AUTO_GEN_FLOW' AND 
						ACTIVE_FLAG		= 'Y' AND 
						[VALUE]			= @RequestType )
	BEGIN
		RETURN
	END

	DECLARE @Cnt INT, @Success INT, @Error INT
	
	SELECT	@Cnt		= COUNT(*),
			@Success	= SUM(CASE [STATUS] WHEN 'SUCCESS' THEN 1 ELSE 0 END),
			@Error		= SUM(CASE [STATUS] WHEN 'ERROR' THEN 1 ELSE 0 END)
	FROM	TB_R_REQUEST_ASSET_D
	WHERE	DOC_NO		= @DOC_NO
	
	SET  @Cnt		= ISNULL(@Cnt,0)
	SET  @Success	= ISNULL(@Success,0)
	SET  @Error		= ISNULL(@Error,0)

	IF @Success + @Error != @Cnt
	BEGIN
		RETURN -- It's mean, SAP send partial result of request no.
	END

	IF (@Cnt = @Success AND @Success > 0)
		SET @Status = '60'
	ELSE IF (@Cnt = @Error AND @Cnt > 0)
		SET @Status = '50'
	ELSE IF @Cnt > @Error AND @Error > 0
		SET @Status = '40'
	

	UPDATE	TB_R_REQUEST_H
	SET		[STATUS]	= @Status,
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= 'System'
	WHERE	DOC_NO		= @DOC_NO AND
			[STATUS]	IN ('30','45')
	
	-- Set Requesetor is Next Approve

	DECLARE @RequestorIndx INT, @LatestIndx INT, @AECIndx INT
	SELECT	@RequestorIndx	= MIN(INDX),
			@LatestIndx		= MAX(INDX)
	FROM	TB_R_REQUEST_APPR 
	WHERE	DOC_NO			= @DOC_NO ;
	
	SELECT	@AECIndx		= MAX(INDX)
	FROM	TB_R_REQUEST_APPR 
	WHERE	DOC_NO			= @DOC_NO AND
			APPR_ROLE		= dbo.fn_GetSystemMaster('SYSTEM_CONFIG','ACR_CODE','ACR')
	
	DECLARE @indx INT = (@LatestIndx + 1)
	IF @Status = '60'
	BEGIN
		
		EXEC sp_WFD021A4_AddNewApprover @DOC_NO, @RequestorIndx, @indx, 'Y'
		RETURN
	END
	ELSE IF ( @LatestIndx != @AECIndx ) -- Last is AEC
	BEGIN
		EXEC sp_WFD021A4_AddNewApprover @DOC_NO, @RequestorIndx, @indx, 'N'
		RETURN
	END
	ELSE
	BEGIN
		EXEC sp_WFD021A4_UpdateApprover @DOC_NO, @AECIndx
		RETURN
	END


END
GO
