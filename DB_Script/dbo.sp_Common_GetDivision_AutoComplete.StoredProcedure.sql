DROP PROCEDURE [dbo].[sp_Common_GetDivision_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetDivision_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER
AS
BEGIN
		/*SELECT	DIV_CODE,
				DIV_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(	
					ORG_CODE LIKE CONCAT(@keyword,'%') 
					OR
					DIV_NAME LIKE CONCAT(@keyword,'%') 
				) 
		GROUP BY DIV_CODE,DIV_NAME
		ORDER BY DIV_CODE ASC	*/

		SELECT	DIV_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(						
					DIV_NAME LIKE CONCAT(@keyword,'%') 
				) 
		GROUP BY DIV_NAME
		ORDER BY DIV_NAME ASC		
END
GO
