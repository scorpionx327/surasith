DROP PROCEDURE [dbo].[sp_WFD01270_ValidationConcurrency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01270_ValidationConcurrency]
(	
	@GUID				VARCHAR(50)		= NULL,
	@RequestType		VARCHAR(1)		= NULL,
	@DOC_NO				VARCHAR(10)		= NULL,
	@UPDATE_DATE		VARCHAR(30)		= NULL		
)
AS
BEGIN

	DECLARE @MESSAGE_CODE	VARCHAR(12)
	
	--==================== Check Asset Status ====================--

	DECLARE @TB_T_ERR_MSG AS TABLE
	(
		MESSAGE_CODE	VARCHAR(25),
		MESSAGE_TYPE	VARCHAR(4),
		MESSAGE_TEXT	VARCHAR(255)
	)

	IF @UPDATE_DATE IS NOT NULL
	BEGIN
		INSERT INTO @TB_T_ERR_MSG
		SELECT	'MSTD0114AERR', 'ERR', DBO.fn_ReplaceMessage('MSTD0114AERR')
		FROM	TB_R_REQUEST_H T
		WHERE	DOC_NO			=	@DOC_NO
			AND FORMAT(T.UPDATE_DATE,'yyyy-MM-dd HH:MM:ss.fff')		<> @UPDATE_DATE 
	END
	
	SELECT	*
	FROM	@TB_T_ERR_MSG
END


GO
