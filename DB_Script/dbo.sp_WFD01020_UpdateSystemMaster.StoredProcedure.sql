DROP PROCEDURE [dbo].[sp_WFD01020_UpdateSystemMaster]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jatuporn Sornsiriong
-- Create date: 14/03/2017
-- Description:	Update Sys Master
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01020_UpdateSystemMaster]
	 @CATEGORY		VARCHAR(40)
	,@SUB_CATEGORY	VARCHAR(40)
	,@CODE			VARCHAR(40)
	,@VALUE			VARCHAR(400)
	,@REMARKS		VARCHAR(200)
	,@ACTIVE_FLAG	CHAR(1)	
	,@UPDATE_BY		VARCHAR(8)	
AS
BEGIN

UPDATE [dbo].[TB_M_SYSTEM]
   SET [VALUE] = @VALUE
      ,[REMARKS] = @REMARKS
      ,[ACTIVE_FLAG] = @ACTIVE_FLAG
      ,[UPDATE_DATE] = GETDATE()
      ,[UPDATE_BY] = @UPDATE_BY
 WHERE [CATEGORY] = @CATEGORY
 AND [SUB_CATEGORY] = @SUB_CATEGORY
 AND [CODE] = @CODE

END





GO
