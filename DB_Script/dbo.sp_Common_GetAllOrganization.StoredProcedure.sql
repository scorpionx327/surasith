DROP PROCEDURE [dbo].[sp_Common_GetAllOrganization]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun
-- Create date: 05/03/2017
-- Description:	Get All orgrnization order by
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_GetAllOrganization]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from TB_M_ORGANIZATION 
END




GO
