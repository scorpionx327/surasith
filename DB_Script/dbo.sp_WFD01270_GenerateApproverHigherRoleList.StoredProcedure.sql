DROP PROCEDURE [dbo].[sp_WFD01270_GenerateApproverHigherRoleList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
-- Create proc [dbo].[sp_WFD01270_GenerateApproverRoleList] "dbo.sp_WFD01270_GenerateApproverRoleList"
--

CREATE PROCEDURE [dbo].[sp_WFD01270_GenerateApproverHigherRoleList]
@Company	T_COMPANY,
@DocNo		T_DOC_NO,
@Seq	INT,
@Role	VARCHAR(3),
@RowCount	INT OUT
AS
BEGIN
	
	DECLARE @HigherRoleList VARCHAR(400)
	SET @HigherRoleList = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPROVE_HIGHER_MATCH',@Role)

	IF ISNULL(@HigherRoleList,'') = ''
	BEGIN
		--PRINT 'No @HigherRoleList'
		SET @RowCount = 0 
		RETURN
	END

	DECLARE @TB_HIGHER_LIST AS TABLE
	(
		INDX INT, [ROLE] VARCHAR(3), USED VARCHAR(1)
	)

	INSERT INTO	@TB_HIGHER_LIST
	SELECT	position, [value], 'N'  From dbo.FN_SPLIT(@HigherRoleList,'|')
	-- DM => DGM|GM|VP

	
	DECLARE @TargetRole VARCHAR(3)
	DECLARE @DirectOrgLevel VARCHAR(40)
	DECLARE @EffCnt INT

	WHILE EXISTS(SELECT 1 FROM @TB_HIGHER_LIST WHERE USED = 'N')
	BEGIN
		SELECT TOP 1 @TargetRole = [ROLE] FROM @TB_HIGHER_LIST  WHERE USED = 'N' ORDER BY INDX
		UPDATE @TB_HIGHER_LIST SET USED = 'Y' WHERE [ROLE] = @TargetRole

		PRINT @TargetRole
		SET @DirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@TargetRole)

		
		EXEC sp_WFD01270_GenerateApproverRoleList @DocNo, @Seq, @Role, @TargetRole, @DirectOrgLevel,  'H', @EffCnt OUT
		
		IF @@ERROR > 0 OR @EffCnt > 0
		BEGIN
			BREAK
		END
	END

END
GO
