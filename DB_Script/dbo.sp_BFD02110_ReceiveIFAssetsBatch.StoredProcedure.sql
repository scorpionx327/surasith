DROP PROCEDURE [dbo].[sp_BFD02110_ReceiveIFAssetsBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- UISS_2.1.1_BFD02110_Fixed assets interface batch
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  UISS_2.1.1_BFD02110_Fixed assets interface batch
			 and DN_FAS_2.1.1_BFD02110_Fixed Assets Interface Batch
-- Objective  :This process is main function for Fixed Assets Interface batch
REMARK : Value return
	1  : end with success
	0  : end with warning
	-1 : end with error 
	exec sp_BFD02110_ReceiveIFAssetsBatch -1,'uten'
-- =============================================  SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD02110_ReceiveIFAssetsBatch] 
(
 	@AppID	INT,
	@UserID		VARCHAR(8)
)
AS
BEGIN TRY
	DECLARE	@cnt				INT
	DECLARE	@cntAssetH			INT
	DECLARE	@cntAssetD			INT
	DECLARE	@cntAssetFinance	INT

	DECLARE @ERROR_MESSAGE VARCHAR(MAX);

	SELECT	@cntAssetH = COUNT(*)
	FROM	TB_S_ASSETS_H

	SELECT	@cntAssetD = COUNT(*)
	FROM	TB_S_ASSETS_D

	SELECT	@cntAssetFinance = COUNT(*)
	FROM	TB_S_ASSETS_FINANCE

	SET @cnt=ISNULL(@cntAssetH,0) + ISNULL(@cntAssetD,0) + ISNULL(@cntAssetFinance,0) ;

	IF(@cnt = 0)
	BEGIN
	     EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MCOM0020AWRN', 'Receiving Fixed Assets Interface batch',@UserID
		SELECT 0; -- 1 is mean end with warning
		RETURN;
	END
	-- for set trim and upper data in stagging table 
	EXEC sp_BFD02110_UpperAndTrimDataInStagingTable 

     --- Validate check 
	DECLARE @Error BIT
	--PRINT '-------- Validate check 
     --EXEC @Error=sssss @AppID,@UserID 
	--IF ( @Error = 1 ) 
	--   BEGIN
	--      GOTO ERROR;
	--   END

	-- For check  Process Status of Asset No. for write warning log
	EXEC sp_BFD02110_CheckProcessStatus @AppID,@UserID


BEGIN TRANSACTION T1

  print  '----- 4: Update Request History Table : Transfer Request'
      EXEC   @Error=sp_BFD02110_UpdateTransferRequest @UserID ,@ERROR_MESSAGE OUT
	  IF ( @Error = 1 ) 
	   BEGIN
	      GOTO ERROR;
	   END

  print  '----- 5: Update Request History Table : Dispose Request'
          EXEC   @Error=sp_BFD02110_UpdateDisposeRequest @UserID ,@ERROR_MESSAGE OUT
	IF ( @Error = 1 ) 
	   BEGIN
	      GOTO ERROR;
	   END
  print  '----- 6: Update Request History Table : CIP Request'
         EXEC   @Error= sp_BFD02110_UpdateCIPRequest @UserID ,@ERROR_MESSAGE OUT
	  IF ( @Error = 1 ) 
	   BEGIN
	      GOTO ERROR;
	   END

  print  '---   3: Update Fixed Assets data in result table'
	  EXEC @Error= sp_BFD02110_UpdateFixedAssetFromStagging @UserID,@ERROR_MESSAGE OUT

	 IF ( @Error = 1 ) 
	   BEGIN
	      GOTO ERROR;
	   END
	   
	  print  '  ----  2: Insert Fixed Assets data into result table'
      EXEC  @Error= sp_BFD02110_InsertFixedAssetFromStagging @UserID,@ERROR_MESSAGE OUT
	  IF ( @Error = 1 ) 
	   BEGIN
	      GOTO ERROR;
	   END

	   -- [SUPPLIER_NO]      , [SUPPLIER_NAME]     

   UPDATE M
   SET M.SUPPLIER_NO = D.SUPPLIER_NO
	  ,M.SUPPLIER_NAME=D.SUPPLIER_NAME
   FROM TB_M_ASSETS_H M INNER JOIN (           SELECT ASSET_NO,SUPPLIER_NO,SUPPLIER_NAME
												 FROM ( SELECT ROW_NUMBER() 
																	OVER(PARTITION BY ASSET_NO	
																		 ORDER BY CASE
																				  WHEN SysConfig.VALUE IS NULL THEN '0'
																				  ELSE '1' END ASC,
																				  INVOICE_COST DESC,
																				  SUPPLIER_NO ASC) AS ROWNUMBER ,
																 ROW_NO,ASSET_NO,SUPPLIER_NO,SUPPLIER_NAME
														FROM	TB_S_ASSETS_FINANCE AssetF LEFT JOIN  (  
																									SELECT [VALUE]
																									FROM [TB_M_SYSTEM] WHERE CATEGORY='SYSTEM_CONFIG'
																									 AND SUB_CATEGORY='DUMMY_SUPPLIER_NO' 
																									 )SysConfig
													   ON ISNULL(AssetF.SUPPLIER_NO,'') = ISNULL(SysConfig.VALUE,'')
																 )tb_FirtRow
												  WHERE	  ROWNUMBER =1 
									  )D ON M.ASSET_NO=D.ASSET_NO
 AND EXISTS (SELECT 1 
					FROM TB_S_ASSETS_H  S
					WHERE M.ASSET_NO = S.ASSET_NO
					)

	   -- Update list of PO to Asset Header by Surasith T. 2017-04-12
	   UPDATE	M
		SET		PO_NO_LIST = STUFF((
								SELECT DISTINCT ', ' + ISNULL(PO_NO,'') 
								FROM TB_M_ASSETS_FINANCE FF 
								WHERE FF.ASSET_NO = M.ASSET_NO AND PO_NO IS NOT NULL
								FOR XML PATH ('')
							),1,1,'')
		FROM	TB_M_ASSETS_H M
		--------------------------------------------------------------------------------
		-- Keep to Support Disposal 2017-06-16
		UPDATE	M
		SET		TOTAL_UNIT	= ISNULL(D.UNIT,0),
				M.[STATUS]	= CASE	WHEN ISNULL(D.UNIT,0) = 0 THEN 'N' 
									WHEN PROCESS_STATUS = 'DC' THEN [STATUS] -- Support Disposal
									ELSE 'Y' END
		FROM	TB_M_ASSETS_H M
		LEFT	JOIN
		(
			SELECT	ASSET_NO, SUM(UNIT) AS UNIT
			FROM	TB_M_ASSETS_D
			GROUP	BY
					ASSET_NO
		) D
		ON	M.ASSET_NO = D.ASSET_NO
		--------------------------------------------------------------------------------
		-- Suphachai L. 2017-05-18
		EXEC   @Error= sp_BFD02110_UpdateItemSeq @UserID ,@ERROR_MESSAGE OUT
		IF ( @Error = 1 ) 
		BEGIN
			GOTO ERROR;
		END

	COMMIT TRANSACTION T1
	SELECT 1;
	RETURN;

ERROR: 
	if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END
	   SET @ERROR_MESSAGE= CONCAT('Receiving Fixed Assets Interface batch','|',' ERROR_MESSAGE :',@ERROR_MESSAGE,'|',NULL,'|',NULL)
	
	 	 exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@ERROR_MESSAGE,@UserID,'Y'

		SELECT -1;
		RETURN;

END TRY
BEGIN CATCH

	 if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END
--===========================================================Write log =====================================
     DECLARE @errMessage nVARCHAR(1000);
     SET @errMessage= CONCAT('Receiving Fixed Assets Interface batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
	 exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@errMessage,@UserID,'Y'
--===========================================================End Write log =====================================
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		SELECT -1;
		RETURN;
END CATCH









GO
