DROP PROCEDURE [dbo].[sp_WFD01250_GetApproveH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 10-03-2017
-- Description:	Get approve h data for edit
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_GetApproveH]
	@APPROVE_ID	int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM TB_M_APPROVE_H h
	WHERE APPRV_ID=@APPROVE_ID

END




GO
