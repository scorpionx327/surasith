DROP PROCEDURE [dbo].[sp_WFD02190_SearchData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 08/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
-- declare @TOTAL_ITEM int exec sp_WFD02190_SearchData NULL,'1432',null,null,'Y',null,null,null,null,null,null,null,null,'T','Y',1,200,null,@TOTAL_ITEM out  

CREATE PROCEDURE [dbo].[sp_WFD02190_SearchData]
(
	@EMP_CODE				T_SYS_USER,
	@REQUEST_TYPE			VARCHAR(1),
    @COMPANY				T_COMPANY,		
    @ASSET_NO				T_ASSET_NO		= NULL,
    @ASSET_SUB				T_ASSET_SUB		= NULL,
    @ASSET_NAME				VARCHAR(200)	= NULL,
    @ASSET_CLASS			T_ASSET_CLASS	= NULL,
    @ASSET_GROUP			VARCHAR(3)		= NULL,
    @WBS_PROJECT			VARCHAR(24)		= NULL,
    @WBS_BUDGET				VARCHAR(24)		= NULL,
    @ASSET_PLATE_NO			VARCHAR(25)		= NULL,
	@LOCATION				VARCHAR(40)		= NULL,

    @ONLY_PARENT_ASSETS		VARCHAR(1),				-- Y,N

    @COST_CODE				T_COST_CODE		= NULL,
	@RESP_COST_CODE			T_COST_CODE		= NULL,
    @DATE_IN_SERVICE_START	VARCHAR(10)		= NULL, --dd.MM.yyyy
    @DATE_IN_SERVICE_TO		VARCHAR(10)		= NULL, --dd.MM.yyyy
	@INVEST_REASON			VARCHAR(2)		= NULL,
	@SearchOption			VARCHAR(7)		= NULL,
    @GUID					T_GUID,
	@pageNum				INT				= NULL,
	@pageSize				INT				= NULL,
	@sortColumnName			VARCHAR(50)		= NULL,
	@orderType				VARCHAR(5)      = NULL,
	@TOTAL_ITEM				INT output
)
AS
BEGIN
	
	
	IF @pageNum IS NULL OR @pageNum < 1
		SET @pageNum = 1
	IF @pageSize IS NULL
		SET @pageSize = 10

	IF @orderType IS NULL
		SET @orderType = 'asc'


	SET @DATE_IN_SERVICE_START	= ISNULL(@DATE_IN_SERVICE_START,'01.01.1900')
	SET @DATE_IN_SERVICE_TO		= ISNULL(@DATE_IN_SERVICE_TO,'31.12.9999')

	SET	@ASSET_SUB = CASE WHEN @ONLY_PARENT_ASSETS = 'Y' THEN '0000' ELSE @ASSET_SUB END
	
	-- Set User Permission
	DECLARE @IsAECUser VARCHAR(1) = dbo.fn_IsAECUser(@EMP_CODE) ;
	DECLARE @IsBOIUser VARCHAR(1) = dbo.fn_IsBOIUser(@EMP_CODE) ;




	-- Create temp TEMP_LIST_ASSETS 
	IF OBJECT_ID('tempdb..#FN_FD0_COSTCENTER_LIST') IS NOT NULL
	BEGIN
		DROP TABLE #FN_FD0_COSTCENTER_LIST
	END

	DECLARE @FN_FD0_COSTCENTER_LIST TABLE
	(
		COMPANY		T_COMPANY,
		COST_CODE	T_COST_CODE
	)

	
	IF @IsAECUser = 'N'
	BEGIN
		INSERT 
		INTO	@FN_FD0_COSTCENTER_LIST(COMPANY, COST_CODE)
		SELECT	COMPANY, COST_CODE 
		FROM	dbo.FN_FD0COSTCENTERLIST_REQ(@EMP_CODE)

	END

	PRINT '#0'
	---------------------------------------------------------------------------------
	-- Selected Asset
	DECLARE @TB_T_SELECTED_ASSETS AS TABLE
	(
			COMPANY		T_COMPANY,
			ASSET_NO	T_ASSET_NO,
			ASSET_SUB	T_ASSET_SUB
	)

	INSERT
	INTO	@TB_T_SELECTED_ASSETS
	SELECT	COMPANY, ASSET_NO, ASSET_SUB 
	FROM	TB_T_SELECTED_ASSETS 
	WHERE	[GUID] = @GUID AND 
			@REQUEST_TYPE NOT IN ('C') -- RECLASS : Allow to select duplicate item
	
	---------------------------------------------------------------------------------
	-- Child Asset for Transfer Request checking
	/* DECLARE @TB_T_IN_PROC AS TABLE
	(
		ASSET_NO	T_ASSET_NO,
		ASSET_SUB	T_ASSET_SUB
	)
	
	INSERT
	INTO	@TB_T_IN_PROC
	SELECT	ASSET_NO, ASSET_SUB
	FROM	TB_M_ASSETS_H 
	WHERE	ISNULL(PROCESS_STATUS,'') != '' AND 
			RIGHT(ASSET_NO, 2) <> '00' AND
			@REQUEST_TYPE = 'T'*/
	
	---------------------------------------------------------------------------------
	-- Main Query
	PRINT '#1'
		
	SELECT	CASE 
			
             WHEN @sortColumnName = '1' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_NO asc)
             WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_NO desc)
			 
			 WHEN @sortColumnName = '2' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_SUB asc)
             WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_SUB desc)

             WHEN @sortColumnName = '3' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_NAME asc)
             WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_NAME desc)           
			
			 WHEN @sortColumnName = '4' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_CLASS asc)
             WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_CLASS desc)           
			
			 WHEN @sortColumnName = '5' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.COST_CODE asc)
             WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.COST_CODE desc)           
			
			 WHEN @sortColumnName = '6' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.WBS_PROJECT asc)
             WHEN @sortColumnName = '6' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.WBS_PROJECT desc)           
			
				
             WHEN @sortColumnName = '8' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.MINOR_CATEGORY asc) 
             WHEN @sortColumnName = '8' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.MINOR_CATEGORY desc) 

			 WHEN @sortColumnName = '9' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.DATE_IN_SERVICE asc) 
             WHEN @sortColumnName = '9' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY H.DATE_IN_SERVICE desc) 

             WHEN @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_NO asc) 
             WHEN @orderType = 'desc'	THEN ROW_NUMBER() OVER(ORDER BY H.ASSET_NO desc)
			END AS [NO],
			H.COMPANY,
			H.ASSET_NO,
			H.ASSET_SUB,
			H.ASSET_NAME,
			H.ASSET_CLASS,
			H.COST_CODE,
			H.RESP_COST_CODE,
			H.WBS_PROJECT,
			H.INVEST_REASON,
			H.MINOR_CATEGORY, 
			H.BOI_NO,
			H.[LOCATION],
			dbo.fn_dateFAS(H.DATE_IN_SERVICE) DATE_IN_SERVICE,
			H.PROCESS_STATUS
			
	INTO	#TB_RAW_DATA
	FROM	TB_M_ASSETS_H H	
	LEFT	JOIN
			@TB_T_SELECTED_ASSETS A
	ON		H.COMPANY			= A.COMPANY AND
			H.ASSET_NO			= A.ASSET_NO AND
			H.ASSET_SUB			= A.ASSET_SUB
	WHERE	(	@IsAECUser = 'Y' OR
				EXISTS(SELECT 1 FROM @FN_FD0_COSTCENTER_LIST F WHERE H.COST_CODE = F.COST_CODE AND H.COMPANY = F.COMPANY) OR
				EXISTS(SELECT 1 FROM TB_M_FASUP_COST_CENTER RF WHERE H.RESP_COST_CODE = RF.COST_CODE AND H.COMPANY = RF.COMPANY AND RF.EMP_CODE = @EMP_CODE)
			)	AND
			A.ASSET_NO			IS NULL AND
			H.COMPANY			= @COMPANY AND
			H.ASSET_NO			LIKE	ISNULL(@ASSET_NO,		H.ASSET_NO) AND
			H.ASSET_SUB			=		ISNULL(@ASSET_SUB,		H.ASSET_SUB) AND
		(	H.ASSET_NAME		LIKE	@ASSET_NAME OR @ASSET_NAME IS NULL ) AND			-- Support field is null
			H.ASSET_CLASS		LIKE	ISNULL(@ASSET_CLASS,	H.ASSET_CLASS) AND
			H.ASSET_GROUP		=		ISNULL(@ASSET_GROUP,	H.ASSET_GROUP) AND
		(	H.WBS_PROJECT		LIKE @WBS_PROJECT OR @WBS_PROJECT IS NULL ) AND		-- Field can be null
		(	H.WBS_BUDGET		LIKE @WBS_BUDGET  OR @WBS_BUDGET IS NULL ) AND		-- Field can be null
			H.COST_CODE			LIKE	ISNULL(@COST_CODE,		H.COST_CODE) AND
		(	H.RESP_COST_CODE	LIKE	@RESP_COST_CODE OR @RESP_COST_CODE IS NULL) AND			-- Support field is null
		(	H.INVEN_NO			LIKE	@ASSET_PLATE_NO OR @ASSET_PLATE_NO IS NULL ) AND	-- Support field is null
		(	H.INVEST_REASON			LIKE	@INVEST_REASON OR @INVEST_REASON IS NULL ) AND -- Support field is null
		(	H.[LOCATION]		LIKE	@LOCATION OR @LOCATION IS NULL ) AND				-- Support field is null
			H.DATE_IN_SERVICE 
								BETWEEN dbo.fn_ConvertStringToDate(@DATE_IN_SERVICE_START) AND 
										dbo.fn_ConvertStringToDate(@DATE_IN_SERVICE_TO)
	
	
	EXEC sp_WFD02190_CustomSearchResult @GUID, @SearchOption
	
	SELECT	@TOTAL_ITEM = COUNT(1)
	FROM	#TB_RAW_DATA


	DECLARE @FROM	INT , @TO	INT

	PRINT '#2'

	SET @FROM = (@pageSize * (@pageNum-1)) + 1;
	SET @TO = @pageSize * (@pageNum);
	
	SELECT	M.[NO],
			M.COMPANY,
			M.ASSET_NO,
			M.ASSET_SUB,
			M.ASSET_NAME,
			M.ASSET_CLASS,
			AC.[VALUE] AS ASSET_CLASS_HINT,
			M.COST_CODE,			
			CC.COST_NAME AS COST_NAME_HINT,
			M.RESP_COST_CODE,
			RC.COST_NAME AS RESP_COST_NAME_HINT,
			M.WBS_PROJECT,
			WBS.[DESCRIPTION] AS WBS_NAME_HINT,
			M.BOI_NO,
			M.MINOR_CATEGORY, 
			MC.[VALUE] AS MINOR_CATEGORY_HINT,
			M.INVEST_REASON,
			BOI.[VALUE] AS INVEST_REASON_HINT,
			M.[LOCATION],
			M.DATE_IN_SERVICE,
			M.PROCESS_STATUS,
			CONVERT(vARCHAR(25),'') AS REF_DOC_NO
	INTO	#TB_RESULT
	FROM	#TB_RAW_DATA M
	INNER	JOIN
			TB_M_COST_CENTER CC
	ON		M.COMPANY = CC.COMPANY AND M.COST_CODE = CC.COST_CODE
	LEFT	JOIN
			TB_M_COST_CENTER RC
	ON		M.COMPANY = RC.COMPANY AND M.RESP_COST_CODE = RC.COST_CODE
	LEFT	JOIN -- should be inner join
			TB_M_WBS WBS
	ON		M.COMPANY = WBS.COMPANY_CODE AND M.WBS_PROJECT = WBS.WBS_CODE
	LEFT	JOIN
			TB_M_SYSTEM AC
	ON		AC.CATEGORY = 'ASSET_CLASS' AND AC.SUB_CATEGORY = 'ASSET_CLASS' AND AC.CODE = M.ASSET_CLASS
	LEFT	JOIN
			TB_M_SYSTEM MC
	ON		MC.CATEGORY = 'EVA_GRP_1' AND MC.SUB_CATEGORY = @COMPANY AND MC.CODE = M.MINOR_CATEGORY				-- Suphachai 2019-09-24
	LEFT	JOIN
			TB_M_SYSTEM BOI
	ON		BOI.CATEGORY = 'INVEST_REASON' AND BOI.SUB_CATEGORY = 'INVEST_REASON' AND BOI.CODE = M.INVEST_REASON				-- Suphachai 2019-09-24
	WHERE	M.[NO] >= @FROM and 
			M.[NO] <= @TO
	
	-- Get Document History
	UPDATE	R
	SET		R.REF_DOC_NO = T.DOC_NO
	FROM	#TB_RESULT R
	INNER	JOIN
	(
		SELECT	H.DOC_NO,
				H.COMPANY,
				H.ASSET_NO,
				H.ASSET_SUB,
				ROW_NUMBER() OVER(PARTITION BY H.COMPANY, H.ASSET_NO, H.ASSET_SUB ORDER BY CREATE_DATE DESC) AS INDX
		FROM	TB_R_ASSET_HIS H
		INNER	JOIN
				#TB_RESULT R
		ON		H.COMPANY	= R.COMPANY AND
				H.ASSET_NO	= R.ASSET_NO AND
				H.ASSET_SUB	= R.ASSET_SUB
		WHERE	R.[PROCESS_STATUS] IS NOT NULL AND
				LEFT(R.PROCESS_STATUS,1) = H.REQUEST_TYPE 
	) T
	ON		R.COMPANY	= T.COMPANY AND
			R.ASSET_NO	= T.ASSET_NO AND
			R.ASSET_SUB	= T.ASSET_SUB
	WHERE	T.INDX		= 1


	SELECT	*,
			CASE	WHEN	@SearchOption = 'WFD01130' THEN 'Y'
					WHEN	@SearchOption IN ('P','AU','RM','ST') THEN 'Y'
					WHEN	T.PROCESS_STATUS IS NULL THEN 'Y'
					ELSE 'N' END ALLOW_CHECK,

			dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_TYPE', LEFT(T.PROCESS_STATUS,1)) [STATUS]
	FROM	#TB_RESULT T
	ORDER	BY
			[NO]

	IF OBJECT_ID('tempdb..#TB_RESULT') IS NOT NULL 
		DROP TABLE #TB_RESULT
	
	IF OBJECT_ID('tempdb..#TB_RAW_DATA') IS NOT NULL 
		DROP TABLE #TB_RAW_DATA

	


END
GO
