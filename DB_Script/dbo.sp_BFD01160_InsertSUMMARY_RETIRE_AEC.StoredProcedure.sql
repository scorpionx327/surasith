DROP PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_RETIRE_AEC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_RETIRE_AEC]
@User	VARCHAR(8)
AS
BEGIN
	DECLARE @ID int,	
		@TO varchar(1000),
		@CC varchar(1000),
		@BCC varchar(1000),
		@TITLE varchar(255),
		@MESSAGE nvarchar(max),
		@Dear nvarchar(max),
		@FOOTER varchar(250),
		@TYPE varchar(1),
		@START_PERIOD datetime,
		@END_PERIOD datetime,
		@SEND_FLAG varchar(1),
		@RESULT_FLAG varchar(1),
		@REF_FUNC varchar(8),
		@REF_DOC_NO T_DOC_NO

		--SET @MESSAGE = 'Dear AEC Manage <br>';

		SET @MESSAGE = CONCAT(@MESSAGE,('<table style="border:1px solid black;">
						<thead style="height:25px; background-color:rgba(236, 240, 245, 1);"> 
						  <tr>
							<th style="min-width: 100px;">Retirement Type</th>
							<th style="min-width: 100px;">Asset Class</th>
							<th style="min-width: 50px;text-align:center;" colspan="2">TDEM</th>
							<th style="min-width: 50px;text-align:center;" colspan="2">TMT</th>
							<th style="min-width: 50px;text-align:center;" colspan="2">STM</th>
						  </tr>
						  <tr>
							<th style="min-width: 100px;"></th>
							<th style="min-width: 100px;"></th>
							<th style="min-width: 50px;text-align:right;">QTY</th>
							<th style="min-width: 50px;text-align:right;">Cost</th>
							<th style="min-width: 50px;text-align:right;">QTY</th>
							<th style="min-width: 50px;text-align:right;">Cost</th>
							<th style="min-width: 50px;text-align:right;">QTY</th>
							<th style="min-width: 50px;text-align:right;">Cost</th>
						  </tr>
					  </thead>'));

		DECLARE @StartDate datetime;
		DECLARE @EndDate datetime;
		select @StartDate = DATEADD(day, -1, [value]),@EndDate = [value]
		From TB_M_SYSTEM																
		Where CATEGORY = 'NOTIFICATION'																
			 AND SUB_CATEGORY = 'SUMMARY_RETIRE_AEC'															
			 AND CODE = 'NEXTTIME'	
-----------------------------------------------------------------------------------------------------------
		DECLARE @RETIREMENT_TYPE varchar(100),@RESP_COST_CODE varchar(100),@COMPANYT varchar(100) 
			,@QTY_TDEM decimal(18,2),@Cost_TDEM decimal(18,2)
			,@QTY_TMT decimal(18,2),@Cost_TMT decimal(18,2)
			,@QTY_STM decimal(18,2),@Cost_STM decimal(18,2);

		DECLARE cursor_results CURSOR FOR 
		select dd.RETIREMENT_TYPE, dd.RESP_COST_CODE, rh.COMPANY
			,(case when rh.COMPANY = 'TDEM' then sum(dd.QTY) else 0 end) QTY_TDEM  
			,(case when rh.COMPANY = 'TDEM' then sum(dd.COST) else 0 end) COST_TDEM 
			,(case when rh.COMPANY = 'TMT' then sum(dd.QTY) else 0 end) QTY_TMT 
			,(case when rh.COMPANY = 'TMT' then sum(dd.COST) else 0 end) COST_TMT 
			,(case when rh.COMPANY = 'STM' then sum(dd.QTY) else 0 end) QTY_STM 
			,(case when rh.COMPANY = 'STM' then sum(dd.COST) else 0 end) COST_STM 
		from TB_R_REQUEST_H rh
			left outer join TB_R_REQUEST_DISP_D dd on rh.DOC_NO = dd.DOC_NO
				and rh.COMPANY = dd.COMPANY
			left outer join TB_R_REQUEST_APPR ra on rh.DOC_NO = ra.DOC_NO
		where rh.EMP_CODE in (
								select e.EMP_CODE from TB_M_EMPLOYEE e
								where e.FAADMIN = 'Y'
							)
			and ra.APPR_ROLE = 'ACR'
			and ra.APPR_STATUS = 'A'
			and ra.APPR_DATE between @StartDate and @EndDate
			and rh.REQUEST_TYPE = 'D'
		group by dd.RETIREMENT_TYPE, dd.RESP_COST_CODE, rh.COMPANY

		OPEN cursor_results
		FETCH NEXT FROM cursor_results into @RETIREMENT_TYPE,@RESP_COST_CODE,@COMPANYT,@QTY_TDEM,@Cost_TDEM,@QTY_TMT,@Cost_TMT,@QTY_STM,@Cost_STM
		WHILE @@FETCH_STATUS = 0
		BEGIN 
			SET @MESSAGE = CONCAT(@MESSAGE,'<tr>');
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;">',@RETIREMENT_TYPE,'</td>');								
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;">',@RESP_COST_CODE,'</td>');								
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@QTY_TDEM,'</td>');				
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@Cost_TDEM,'</td>');					
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@QTY_TMT,'</td>');				
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@Cost_TMT,'</td>');						
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@QTY_STM,'</td>');				
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@Cost_STM,'</td>');
			SET @MESSAGE = CONCAT(@MESSAGE,'</tr>');	


			FETCH NEXT FROM cursor_results into @RETIREMENT_TYPE,@RESP_COST_CODE,@COMPANYT,@QTY_TDEM,@Cost_TDEM,@QTY_TMT,@Cost_TMT,@QTY_STM,@Cost_STM
		END

		CLOSE cursor_results;
		DEALLOCATE cursor_results;
------------------------------------------------------------------------------------------------------------
		SET @MESSAGE = CONCAT(@MESSAGE,'</table>');

		--select * from @MyTable
		SET @FOOTER = 'Best Regards,<br>System';

		SET @START_PERIOD = @StartDate;
		SET @END_PERIOD = @EndDate;

		SET @SEND_FLAG = 'N';
		SET @RESULT_FLAG = 'N';
		SET @TYPE = 'I';
		SET @REF_FUNC = '';

	DECLARE @NAME varchar(max),@AEC_MGR_EMP_ID varchar(50),@EMAIL varchar(50),@COMPANY T_COMPANY;
	DECLARE cursor_results CURSOR FOR 
	select concat('คุณ ',e.EMP_NAME_T,' ',e.EMP_LASTNAME_T),e.AEC_MGR_EMP_ID,e.EMAIL,e.COMPANY
	from TB_M_EMPLOYEE e
	where e.FAADMIN = 'Y'
	group by EMP_NAME_T,e.EMP_LASTNAME_T,e.AEC_MGR_EMP_ID,e.EMAIL,e.COMPANY


	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @NAME,@AEC_MGR_EMP_ID,@EMAIL,@COMPANY
	WHILE @@FETCH_STATUS = 0
	BEGIN 
	SET @ID = (select isnull(max(ID),0) + 1 from TB_R_NOTIFICATION);
	SET @Dear = concat('Dear ',@NAME,' <br>');

	SET @TO = @EMAIL;
	SET @TITLE = CONCAT('TFAST : ',@COMPANY,' Summary Daily Requests');
	INSERT INTO TB_R_NOTIFICATION
           (ID,[TO],CC,BCC,TITLE,[MESSAGE],FOOTER,[TYPE]
           ,START_PERIOD,END_PERIOD,SEND_FLAG,RESULT_FLAG,REF_FUNC,REF_DOC_NO
			,CREATE_DATE,CREATE_BY,UPDATE_DATE,UPDATE_BY)
     VALUES
           (@ID
		   ,@TO
           ,@CC
           ,@BCC
           ,@TITLE
           ,concat(@Dear,@MESSAGE)
           ,@FOOTER
           ,@TYPE
           ,@START_PERIOD
           ,@END_PERIOD
           ,@SEND_FLAG
           ,@RESULT_FLAG
           ,@REF_FUNC
           ,@REF_DOC_NO
           ,getdate()
           ,@User
           ,getdate()
           ,@User)

	--select @TO,@CC,@BCC,@TITLE,@MESSAGE,@FOOTER,@TYPE,@START_PERIOD,@END_PERIOD,@SEND_FLAG,@RESULT_FLAG,@REF_FUNC,@REF_DOC_NO,getdate(),@User,getdate(),@User

	FETCH NEXT FROM cursor_results into @NAME,@AEC_MGR_EMP_ID,@EMAIL,@COMPANY
	END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'SUMMARY_RETIRE_AEC'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	SET @dNextTime = DATEADD(DAY, 1, @dNextTime)

	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= 'System'
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
			--select @MESSAGE
END
GO
