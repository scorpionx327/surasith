DROP PROCEDURE [dbo].[sp_WFD02210_RejectAssetCIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec WFD02210_ValidationBeforeSubmitAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f14',null,''
CREATE PROCEDURE [dbo].[sp_WFD02210_RejectAssetCIP]
(	
	@DOC_NO						VARCHAR(10)			= NULL,		--DOC NO	
	--@ASSET_NO					VARCHAR(15)			= NULL,
	--@COST_CODE					VARCHAR(8)			= NULL,
	--@COMPLETE_DATE				VARCHAR(10)			= NULL,
	@APPROVE_BY					VARCHAR(50)			= NULL
)
AS
BEGIN

	BEGIN TRY 

		DECLARE @NEW_STATUS VARCHAR(2) = 'C'
		
		DECLARE @DocStatus VARCHAR(2)
		SELECT	@DocStatus = [STATUS] 
		FROM	TB_R_REQUEST_H
		WHERE	DOC_NO = @DOC_NO

		
	
		IF (@DocStatus != '90' AND @DocStatus != '99' AND @DocStatus != '80')
		BEGIN
			RETURN
		END


		IF OBJECT_ID('tempdb..#TB_T') IS NOT NULL
		DROP TABLE #TB_T

		SELECT	ASSET_NO, COST_CODE, 'N' AS USED
		INTO	#TB_T 
		FROM	TB_R_REQUEST_CIP WITH(NOLOCK)
		WHERE	DOC_NO = @DOC_NO

		WHILE(EXISTS(SELECT 1 FROM #TB_T WHERE USED = 'N'))
		BEGIN

			DECLARE @ASSET_NO VARCHAR(15), @COST_CODE VARCHAR(8)
	
			SELECT	TOP 1 @ASSET_NO = ASSET_NO, @COST_CODE = COST_CODE 
			FROM	#TB_T WHERE USED = 'N'

			UPDATE	#TB_T SET USED = 'Y' WHERE @ASSET_NO = ASSET_NO AND @COST_CODE = COST_CODE


			DECLARE @CIPDescription VARCHAR(255) = NULL
			IF @DocStatus = '80' -- Set this value on TB_R_ASSET_HIS.DETAIL only completed reject
			BEGIN
				SELECT	@CIPDescription = CONCAT('Reject, Close In Project : Project : {' , CIP_NO , '}')
				FROM	TB_R_REQUEST_CIP 
				WHERE	ASSET_NO = @ASSET_NO AND DOC_NO = @DOC_NO AND COST_CODE = @COST_CODE

				SET @NEW_STATUS = NULL
			END

			EXEC sp_WFD01270_ApproveAsset				
					@DOC_NO,
					@ASSET_NO,
					@COST_CODE,
					@DocStatus,
					'C',
					@NEW_STATUS,
					@CIPDescription,
					@APPROVE_BY
		END -- While

		IF OBJECT_ID('tempdb..#TB_T') IS NOT NULL
		DROP TABLE #TB_T

	END TRY
	BEGIN CATCH
		THROW
	END CATCH

END


GO
