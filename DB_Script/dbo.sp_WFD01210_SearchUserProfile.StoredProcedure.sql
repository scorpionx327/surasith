DROP PROCEDURE [dbo].[sp_WFD01210_SearchUserProfile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 05/03/2017
-- Description:	Search User Profile
/*
Nipon 2019-07-24 Add field.
*/
-- =============================================

CREATE PROCEDURE [dbo].[sp_WFD01210_SearchUserProfile]
	@DIV_NAME varchar(45),
	@DEPT_NAME varchar(45),
	@SEC_NAME varchar(45),
	@EMP_CODE varchar(10), -- Include %%
	@EMP_NAME varchar(32), -- Include %%
	@EMP_LASTNAME varchar(32), -- Include %%
	@POST_CODE varchar(4),
	@ROLE varchar(3),
	@COST_CODE T_COST_CODE, -- Include %%
	@IN_CHARGE_COST_CENTER varchar(12), -- Include %%

	 @COMPANY T_COMPANY =null,
	 @SUB_DIV_NAME varchar(100) =null,
	 @LINE varchar(45) =null,
	 @JOB varchar(50) =null,
	 @RESPONSIBILITY varchar(400) =null,
	 @SPECIAL_ROLE varchar(400) =null

	, @pageNum INT
    , @pageSize INT
    , @sortColumnName VARCHAR(50)
	, @orderType VARCHAR(5)
	, @TOTAL_ITEM		int output
AS
BEGIN
SET NOCOUNT ON;

	SET @EMP_CODE = dbo.fn_GetSearchCriteria(@EMP_CODE)
	SET @EMP_NAME = dbo.fn_GetSearchCriteria(@EMP_NAME)
	SET @EMP_LASTNAME = dbo.fn_GetSearchCriteria(@EMP_LASTNAME)
	SET @COST_CODE = dbo.fn_GetSearchCriteria(@COST_CODE)
	SET @IN_CHARGE_COST_CENTER = dbo.fn_GetSearchCriteria(@IN_CHARGE_COST_CENTER)
	
	-- Change Div Code, Dept Code, Sect Code to Name

	SELECT
		@TOTAL_ITEM = COUNT(1)
	from TB_M_EMPLOYEE E (nolock)
			left join TB_M_ORGANIZATION O (nolock) on E.ORG_CODE = O.ORG_CODE
		--	left join TB_M_SV_COST_CENTER C on C.EMP_CODE = E.EMP_CODE
		--	left join TB_M_SYSTEM S (nolock) on E.ROLE = S.CODE and S.CATEGORY ='ROLE_MAPPING' and S.SUB_CATEGORY='ROLE_PRIORITY'
		--	AND (S.VALUE = @RESPONSIBILITY OR @RESPONSIBILITY IS NULL)
		--	left join TB_M_SP_ROLE R (nolock) on E.EMP_CODE=R.EMP_CODE
			where  (O.DIV_NAME = @DIV_NAME OR @DIV_NAME IS NULL)
			AND (O.DEPT_NAME = @DEPT_NAME OR @DEPT_NAME IS NULL)
		    AND (O.SECTION_NAME = @SEC_NAME OR @SEC_NAME IS NULL)
			AND (E.EMP_CODE like @EMP_CODE OR @EMP_CODE IS NULL)
			AND (E.EMP_NAME like @EMP_NAME OR @EMP_NAME IS NULL)
			AND (E.EMP_LASTNAME like @EMP_LASTNAME OR @EMP_LASTNAME IS NULL)
			AND (E.POST_CODE = @POST_CODE OR @POST_CODE IS NULL)
			AND (E.ROLE = @ROLE OR @ROLE IS NULL)
			AND (E.COST_CODE like @COST_CODE OR @COST_CODE IS NULL)
			AND ( @IN_CHARGE_COST_CENTER IS NULL or exists (select '1' from TB_M_SV_COST_CENTER SV where sv.EMP_CODE = e.EMP_CODE and sv.COST_CODE like @IN_CHARGE_COST_CENTER  ))
		--	AND (C.COST_CODE like @IN_CHARGE_COST_CENTER OR @IN_CHARGE_COST_CENTER IS NULL)
			AND (E.COMPANY in (select COMPANY_CODE FROM dbo.fn_GetMultipleCompanyList(@COMPANY)) or @COMPANY is null)
			AND (O.SUB_DIV_NAME = @SUB_DIV_NAME OR @SUB_DIV_NAME IS NULL)
			--AND (O.LINE_CODE = @LINE OR @LINE IS NULL)
			AND (O.LINE_NAME = @LINE OR @LINE IS NULL)
			AND (E.JOB_CODE = @JOB OR @JOB IS NULL)
			
			AND E.ACTIVEFLAG ='Y';


  WITH Paging AS
  (
    SELECT E.COMPANY
			,E.JOB_NAME JOB 
			,E.ROLE
			--,E.ROLE+R.SP_ROLE RESPONSIBILITY -- Old version code --> ,S.VALUE as RESPONSIBILITY.(Nipon)
			,STUFF(( SELECT ',' + S.VALUE AS [text()]                        
				FROM TB_M_SP_ROLE R (nolock)
				INNER JOIN TB_M_SYSTEM S (nolock) on R.SP_ROLE = S.CODE and S.CATEGORY ='RESPONSIBILITY' and S.SUB_CATEGORY='DISPLAY_SPECIAL_ROLE'
				WHERE E.SYS_EMP_CODE = R.EMP_CODE 					
				FOR XML PATH('') 
				), 1, 1, '' )
				AS RESPONSIBILITY
			,O.SUB_DIV_NAME
			,O.SECTION_NAME
			,O.LINE_NAME
			,E.SYS_EMP_CODE  
			,E.EMP_CODE
			,E.EMP_TITLE
			,E.EMP_TITLE+E.EMP_NAME+' '+E.EMP_LASTNAME EMP_NAME
			,E.EMP_NAME AS FIRST_NAME
			,E.EMP_LASTNAME
			,E.POST_CODE
			,E.POST_NAME
			,E.COST_CODE
			,E.COST_NAME
			,O.DIV_NAME
			,O.DEPT_NAME
			,E.EMAIL
			,E.SIGNATURE_PATH
			,E.UPDATE_DATE
			, CAST(ROW_NUMBER() OVER 
			 (ORDER BY 
			  CASE WHEN @sortColumnName = '1' and @orderType='asc' THEN 
					(E.COMPANY +','+ ISNULL(O.DIV_NAME,'') + ',' + E.EMP_NAME)
					END ASC,											 
			 CASE WHEN @sortColumnName = '1' and @orderType='desc' THEN
					(E.COMPANY +','+ ISNULL(O.DIV_NAME,'') + ',' + E.EMP_NAME)
					END DESC,
			 CASE WHEN @sortColumnName = '2' and @orderType='asc' THEN E.EMP_CODE 
					END ASC,
			 CASE WHEN @sortColumnName = '2' and @orderType='desc' THEN E.EMP_CODE 
					END DESC,
			 CASE WHEN @sortColumnName = '3' and @orderType='asc' THEN E.EMP_TITLE
					END ASC,
			 CASE WHEN @sortColumnName = '3' and @orderType='desc' THEN E.EMP_TITLE
					END DESC,
			 CASE WHEN @sortColumnName = '4' and @orderType='asc' THEN E.EMP_NAME
					END ASC,			  
			 CASE WHEN @sortColumnName = '4' and @orderType='desc' THEN E.EMP_NAME
					END DESC,
			 CASE WHEN @sortColumnName = '5' and @orderType='asc' THEN E.EMP_LASTNAME
					END ASC,			  
			 CASE WHEN @sortColumnName = '5' and @orderType='desc' THEN E.EMP_LASTNAME
					END DESC,
			 CASE WHEN @sortColumnName = '6' and @orderType='asc' THEN E.POST_CODE
					END ASC,			  
			 CASE WHEN @sortColumnName = '6' and @orderType='desc' THEN E.POST_CODE
					END DESC,
			 CASE WHEN @sortColumnName = '7' and @orderType='asc' THEN E.POST_NAME
					END ASC,			  
			 CASE WHEN @sortColumnName = '7' and @orderType='desc' THEN E.POST_NAME
					END DESC,
		--	 CASE WHEN @sortColumnName = '8' and @orderType='asc' THEN  S.VALUE
		--			END ASC,										    
		--	 CASE WHEN @sortColumnName = '8' and @orderType='desc' THEN S.VALUE
		--			END DESC,										    
			 CASE WHEN @sortColumnName = '9' and @orderType='asc' THEN  E.COST_CODE
					END ASC,										    
			 CASE WHEN @sortColumnName = '9' and @orderType='desc' THEN E.COST_CODE
					END DESC,										    
			 CASE WHEN @sortColumnName = '10' and @orderType='asc' THEN  O.DIV_NAME
					END ASC,										    
			 CASE WHEN @sortColumnName = '10' and @orderType='desc' THEN O.DIV_NAME
					END DESC,
			 CASE WHEN @sortColumnName = '11' and @orderType='asc' THEN  O.DEPT_NAME
					END ASC,									    
			 CASE WHEN @sortColumnName = '11' and @orderType='desc' THEN O.DEPT_NAME
					END DESC,
             CASE WHEN @sortColumnName = '12' and @orderType='asc' THEN  E.EMAIL
					END ASC,									    	 
			 CASE WHEN @sortColumnName = '12' and @orderType='desc' THEN E.EMAIL
					END DESC ,
			 CASE WHEN @sortColumnName = '13' and @orderType='asc' THEN  E.SIGNATURE_PATH
					END ASC,									    	 
			 CASE WHEN @sortColumnName = '13' and @orderType='desc' THEN E.SIGNATURE_PATH
					END DESC,
			 CASE WHEN @sortColumnName is null THEN
					(E.COMPANY +','+ ISNULL(O.DIV_NAME,'') + ',' + E.EMP_NAME)
					END ASC
				 ) AS INT) AS RowNumber
			from TB_M_EMPLOYEE E (nolock)
			left join TB_M_ORGANIZATION O (nolock) on E.ORG_CODE = O.ORG_CODE
		--	left join TB_M_SV_COST_CENTER C on C.EMP_CODE = E.EMP_CODE
		--	left join TB_M_SYSTEM S (nolock) on E.ROLE = S.CODE and S.CATEGORY ='ROLE_MAPPING' and S.SUB_CATEGORY='ROLE_PRIORITY'
		--	AND (S.VALUE = @RESPONSIBILITY OR @RESPONSIBILITY IS NULL)
		--	left join TB_M_SP_ROLE R (nolock) on E.EMP_CODE=R.EMP_CODE
			where  (O.DIV_NAME = @DIV_NAME OR @DIV_NAME IS NULL)
			AND (O.DEPT_NAME = @DEPT_NAME OR @DEPT_NAME IS NULL)
		    AND (O.SECTION_NAME = @SEC_NAME OR @SEC_NAME IS NULL)
			AND (E.EMP_CODE like @EMP_CODE OR @EMP_CODE IS NULL)
			AND (E.EMP_NAME like @EMP_NAME OR @EMP_NAME IS NULL)
			AND (E.EMP_LASTNAME like @EMP_LASTNAME OR @EMP_LASTNAME IS NULL)
			AND (E.POST_CODE = @POST_CODE OR @POST_CODE IS NULL)
			AND (E.ROLE = @ROLE OR @ROLE IS NULL)
			AND (E.COST_CODE like @COST_CODE OR @COST_CODE IS NULL)
			AND ( @IN_CHARGE_COST_CENTER IS NULL or exists (select '1' from TB_M_SV_COST_CENTER SV where sv.EMP_CODE = e.EMP_CODE and sv.COST_CODE like @IN_CHARGE_COST_CENTER  ))
		--	AND (C.COST_CODE like @IN_CHARGE_COST_CENTER OR @IN_CHARGE_COST_CENTER IS NULL)
			AND (E.COMPANY in (select COMPANY_CODE FROM dbo.fn_GetMultipleCompanyList(@COMPANY)) or @COMPANY is null)
			AND (O.SUB_DIV_NAME = @SUB_DIV_NAME OR @SUB_DIV_NAME IS NULL)
			--AND (O.LINE_CODE = @LINE OR @LINE IS NULL)
			AND (O.LINE_NAME = @LINE OR @LINE IS NULL)
			AND (E.JOB_CODE = @JOB OR @JOB IS NULL)
			AND E.ACTIVEFLAG ='Y'			

		
	)
  SELECT *
  FROM Paging
  WHERE RowNumber BETWEEN (@pageNum - 1) * @pageSize + 1 
   AND @pageNum * @pageSize
   --ORDER BY 	COMPANY,	DIV_NAME,	FIRST_NAME
   
END





GO
