DROP PROCEDURE [dbo].[sp_WFD01270_RollbackFlagY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec WFD02210_ValidationBeforeSubmitAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f14',null,''
CREATE PROCEDURE [dbo].[sp_WFD01270_RollbackFlagY]
(	
	@DOC_NO						VARCHAR(10),
	@REQUEST_TYPE				VARCHAR(1)
)
AS
BEGIN

	IF @REQUEST_TYPE = 'C'
	BEGIN
		UPDATE TB_R_REQUEST_CIP
		SET		DELETE_FLAG		= 'N'
		WHERE	DOC_NO			= @DOC_NO AND 
				DELETE_FLAG		= 'Y'
	END

	IF @REQUEST_TYPE IN ('P','R','L')
	BEGIN
		UPDATE TB_R_REQUEST_REPRINT
		SET		DELETE_FLAG		= 'N'
		WHERE	DOC_NO			= @DOC_NO AND 
				DELETE_FLAG		= 'Y'
	END

	IF @REQUEST_TYPE = 'T'
	BEGIN
		UPDATE TB_R_REQUEST_TRNF_D
		SET		DELETE_FLAG		= 'N'
		WHERE	DOC_NO			= @DOC_NO AND 
				DELETE_FLAG		= 'Y'
	END

	IF @REQUEST_TYPE = 'D'
	BEGIN
		UPDATE TB_R_REQUEST_DISP_D
		SET		DELETE_FLAG		= 'N'
		WHERE	DOC_NO			= @DOC_NO AND 
				DELETE_FLAG		= 'Y'
	END
END


GO
