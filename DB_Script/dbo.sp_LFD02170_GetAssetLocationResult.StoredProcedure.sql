DROP PROCEDURE [dbo].[sp_LFD02170_GetAssetLocationResult]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_LFD02170_GetAssetLocationResult 'E03DM102 '
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_GetAssetLocationResult]
	@COMPANY	T_COMPANY,
	@COST_CODE	T_COST_CODE
AS
BEGIN	
SET NOCOUNT ON;

	 SELECT				AssetH.ITEM_SEQ ITEMS,
						FORMAT(AssetH.ITEM_SEQ,'00') AS DISPLAY,
						CASE	WHEN (Loc.X IS NOT NULL) AND   (Loc.Y IS NOT NULL)  THEN 'Y'
								ELSE 'N' END AS SELECTED ,
						CASE	WHEN AssetH.BOI_FLAG = 'Y' and AssetH.MACHINE_LICENSE is not null THEN 'BOILICENSE'
								WHEN AssetH.BOI_FLAG = 'Y'  THEN 'BOI'
								WHEN AssetH.MACHINE_LICENSE is not null THEN 'LICENSE'
								ELSE 'NORMAL' END AS ASSET_TYPE,
						-- ,CASE 
					 --   WHEN AssetH.BOI_FLAG = 'Y' and AssetH.MACHINE_LICENSE is not null THEN 'BOILICENSE'
						--WHEN AssetH.BOI_FLAG = 'Y'  THEN 'BOILICENSE'
						--WHEN AssetH.MACHINE_LICENSE is not null THEN 'BOILICENSE'
						--ELSE 'BOILICENSE' END AS ASSET_TYPE
						Loc.X AS X ,
						Loc.Y  AS Y,
						Loc.COST_CODE,
						AssetH.MAP_STATUS , 
						AssetH.MACHINE_LICENSE,
						AssetH.ASSET_NO,
						CONVERT(INT,ISNULL(Loc.SEQ,0)) RECORD_ID,
						Loc.DEGREE
		
		FROM (		SELECT	COMPANY,
							ASSET_NO,
							MAP_STATUS , 
							--S.REMARKS AS SUB_TYPE_SORT,
							H.BOI_FLAG,
							H.MACHINE_LICENSE,
							H.ITEM_SEQ
					FROM	TB_M_ASSETS_H H 
					/*LEFT	JOIN 
						   ( SELECT		CODE,REMARKS -- Remark is field for sorting  
							 FROM		TB_M_SYSTEM 
							 WHERE		CATEGORY		= 'SYSTEM_CONFIG' AND 
										SUB_CATEGORY	= 'MINOR_CATEGORY' AND 
										ACTIVE_FLAG		= 'Y' 
							 ) S 
							ON		H.MINOR_CATEGORY = S.CODE*/
						   WHERE	H.MINOR_CATEGORY IN (	SELECT	[VALUE] 
															FROM	TB_M_SYSTEM
															WHERE	CATEGORY		= 'LOC_MAP' AND 
																	SUB_CATEGORY	= CONCAT('MINOR_CATEGORY_',@COMPANY) AND 
																	ACTIVE_FLAG		= 'Y' 
														) AND	
									H.ASSET_GROUP	= 'RMA' AND
									H.[STATUS]		= 'Y' AND
									H.ASSET_SUB		= '0000' AND
									H.INHOUSE_FLAG	= 'Y' AND
								(	H.COST_CODE		= @COST_CODE OR EXISTS (SELECT	1 
																			FROM 	TB_M_ASSETS_LOC L
																			WHERE	L.ASSET_NO=H.ASSET_NO AND L.COMPANY = H.COMPANY AND
																					L.COST_CODE = @COST_CODE AND L.COMPANY = @COMPANY
																		) 
								)
															
			  ) AssetH 
		INNER 
		JOIN		TB_M_ASSETS_LOC Loc 
		ON			Loc.ASSET_NO	= AssetH.ASSET_NO AND 
					LOC.COMPANY		= AssetH.COMPANY
		WHERE		AssetH.COMPANY	= @COMPANY AND
					MAP_STATUS		='Y' AND 
					Loc.COST_CODE	is not null	   
		ORDER BY	CONVERT(INT,ISNULL(Loc.SEQ,0)) DESC

--- select data from temp 
---- Loop join set running per sub type
--    IF CURSOR_STATUS('global','tt_cursor')>=-1
--		BEGIN
--						CLOSE tt_cursor   
--						DEALLOCATE tt_cursor
--		END


--   DECLARE @SUB_TYPE VARCHAR(3) 

--   DECLARE tt_cursor CURSOR FOR
--   --Notify email incase, Cost Center expire date     
--		    SELECT SUB_TYPE
--			FROM #T_MAP
--			GROUP BY SUB_TYPE
--    OPEN tt_cursor
--    FETCH NEXT FROM tt_cursor
--		  INTO @SUB_TYPE
--	 WHILE @@FETCH_STATUS = 0
--		BEGIN 
--		            UPDATE T1
--					SET T1.ITEMS=T2.ITEMS
--					FROM #T_MAP T1
--					INNER JOIN  (   SELECT ROW_NUMBER() OVER(ORDER BY ITEMS ASC ) AS ITEMS,ASSET_NO
--								    FROM #T_MAP 
--									WHERE SUB_TYPE=@SUB_TYPE
--									)T2 ON T1.ASSET_NO=T2.ASSET_NO
--		FETCH NEXT FROM tt_cursor
--		INTO @SUB_TYPE
--	   END 
--	 CLOSE tt_cursor
--	 DEALLOCATE tt_cursor

--	 	 IF CURSOR_STATUS('global','tt_cursor')>=-1
--		BEGIN
--						CLOSE tt_cursor   
--						DEALLOCATE tt_cursor
--		END
-----// 
		--SELECT    RECORD_ID  ,COST_CODE   -- ,L.ASSET_NO,M.MODEL
		--         ,X_POINT AS X,Y_POINT AS Y    -- ,LICENSE_FLAG,BOI_FLAG
		--         ,FORMAT(ITEMS,'00') +':'+SUBSTRING(MACHINE_NUMBER,1,12) AS DISPLAY
		--         ,ASSET_TYPE
		--FROM #T_MAP
		--WHERE MAP_STATUS ='Y'
		--ORDER BY ITEMS ASC;


IF OBJECT_ID('tempdb..#T_MAP') IS NOT NULL
BEGIN
    DROP TABLE #T_MAP
END

END
GO
