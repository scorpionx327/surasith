DROP PROCEDURE [dbo].[sp_WFD01210_UpdateRequestWhenEmployeeInfoChanged]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =============================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/06 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE [dbo].[sp_WFD01210_UpdateRequestWhenEmployeeInfoChanged]
(	  
	 @UserID		T_SYS_USER,
	 @OLD_ORG_CODE	T_ORG_CODE,
	 @NEW_ORG_CODE	T_ORG_CODE,
	 @ROLE			varchar(3),
	 @ACTIVE		VARCHAR(1),
	 @EMP_CODE      T_SYS_USER
)
AS
BEGIN TRY
	
	IF (dbo.fn_IsOrganizeChanged(@ROLE,@OLD_ORG_CODE, @NEW_ORG_CODE) != 'Y' AND @ACTIVE = 'Y')
	BEGIN
		RETURN
	END
	-- Get Document of User that Resign / Org. Change 

	------------------------------------------------------------------------------------------------------------------
	-- Delete Org Code from approver list (W, P)
	DELETE	H
	FROM	TB_H_REQUEST_APPR H
	INNER	JOIN
			TB_R_REQUEST_APPR A
	ON		A.DOC_NO	= H.DOC_NO AND
			A.INDX		= H.INDX
	INNER	JOIN
			TB_R_REQUEST_H R
	ON		A.DOC_NO	= R.DOC_NO
	WHERE	R.[STATUS]	NOT IN ('60','70','75','90') AND
			H.EMP_CODE	= @EMP_CODE AND
			A.APPR_STATUS	IN ('W','P')
	------------------------------------------------------------------------------------------------------------------
	-- Delete Resign Employee from approver list (W, P)
	UPDATE	A
	SET		ACTUAL_ROLE		= '',
			EMP_CODE		= NULL,
			EMP_TITLE		= NULL,
			EMP_NAME		= NULL,
			EMP_LASTNAME	= NULL,
			EMAIL			= NULL,
			POS_CODE		= NULL,
			POS_NAME		= NULL,
			DEPT_CODE		= NULL,
			DEPT_NAME		= NULL,
			ORG_CODE		= NULL
	FROM	TB_R_REQUEST_APPR A
	INNER	JOIN
			TB_R_REQUEST_H R
	ON		A.DOC_NO	= R.DOC_NO
	WHERE	R.[STATUS]	NOT IN ('60','70','75','90') AND
			A.EMP_CODE		= @EMP_CODE AND 
			A.APPR_STATUS	IN ('W','P')

END TRY
BEGIN CATCH
	

END CATCH
GO
