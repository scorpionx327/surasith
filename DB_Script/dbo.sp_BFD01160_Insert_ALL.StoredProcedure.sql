DROP PROCEDURE [dbo].[sp_BFD01160_Insert_ALL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_Insert_ALL]
	@Company	T_COMPANY,
	@User		VARCHAR(8)
AS
BEGIN
	exec sp_BFD01160_InsertSUMMARY_AEC @User
	exec sp_BFD01160_InsertSUMMARY_BOI @User
	exec sp_BFD01160_InsertSUMMARY_KPI @User
	exec sp_BFD01160_InsertSUMMARY_RETIRE_AEC @User
	exec sp_BFD01160_InsertSUMMARY_RETIRE_STORE @User
	exec sp_BFD01160_InsertSUMMARY_UPLOAD_MAP @Company,@User
	--exec sp_BFD01260_InsertSUMMARY_UPLOAD_PHOTO @Company,@User
	exec sp_BFD01160_InsertSUMMARY_UPLOAD_PHOTO_IH @Company,@User 
	exec sp_BFD01160_InsertSUMMARY_UPLOAD_PHOTO_OS @Company,@User 
	--exec sp_BFD01260_InsertSUMMARY_PRINT_TAG @Company,@User 
	exec sp_BFD01160_InsertSUMMARY_PRINT_TAG_IH @Company,@User
	exec sp_BFD01160_InsertSUMMARY_PRINT_TAG_OS @Company,@User
END
GO
