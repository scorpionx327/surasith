DROP PROCEDURE [dbo].[sp_LFD02912_Export_Reclassification_Request]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LFD02912_Export_Reclassification_Request]
(
	@DOC_NO T_DOC_NO
)
AS
BEGIN
	SELECT	R.COMPANY,
		--R.DOC_NO,
		R.ASSET_NO,
		R.ASSET_SUB,
		R.ASSET_CLASS,
		dbo.fn_dateFAS(M.DATE_IN_SERVICE),
		--FORMAT(M.DATE_IN_SERVICE,'dd.MM.yyyy'),
		--dbo.fn_GetSystemMaster('ASSET_CLASS', 'ASSET_CLASS', R.ASSET_CLASS) AS ASSET_CLASS_HINT,															
		M.ASSET_NAME,
		R.COST_VALUE,	
		R.NEW_ASSET_NO,		
		R.NEW_ASSET_SUB,		
		R.NEW_ASSET_CLASS,
		--dbo.fn_GetSystemMaster('ASSET_CLASS', 'ASSET_CLASS', R.NEW_ASSET_CLASS) AS NEW_ASSET_CLASS_HINT,
		R.NEW_ASSET_NAME ,
		R.AMOUNT_POSTED,																				
		R.POSTING_DATE,																				
		R.INVEST_REASON,																				
		R.REASON,																				
		R.STATUS,																				
		R.DOCUMENT_DATE,																																							
		R.PARTIAL_FLAG,																				
		R.ASSET_VALUE_DATE	
	FROM TB_T_REQUEST_RECLAS R
	INNER JOIN TB_M_ASSETS_H M ON R.COMPANY = M.COMPANY 
			AND R.ASSET_NO = M.ASSET_NO 
			AND R.ASSET_SUB = M.ASSET_SUB
	WHERE (R.DOC_NO = @DOC_NO OR @DOC_NO IS NULL OR LEN(@DOC_NO) = 0)
	ORDER BY R.LINE_NO
END

GO
