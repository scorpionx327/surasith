DROP PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList_MGR_HigherDirect]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from TB_M_SYSTEM where CATEGORY like 'SYSTEM_CONFIG' and SUB_CATEGORY LIKE 'STM_APPROVE_INDIRECT%'
CREATE PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList_MGR_HigherDirect]
@Indx		INT,
@Company	T_COMPANY,
@Role		VARCHAR(3),
@Division	VARCHAR(6),
@GUID		T_GUID = NULL,
@RowCount	INT OUT
AS
BEGIN
	SET @RowCount = 0
	
	DECLARE @HigherRoleList VARCHAR(400)
	SET @HigherRoleList = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_HIGHER_MATCH'),@Role)

	IF ISNULL(@HigherRoleList,'') = ''
	BEGIN
		--PRINT 'No @HigherRoleList'
		SET @RowCount = 0 
		RETURN
	END

	DECLARE @TB_HIGHER_LIST AS TABLE
	(
		INDX INT, [ROLE] VARCHAR(3), USED VARCHAR(1)
	)

	INSERT INTO	@TB_HIGHER_LIST
	SELECT	position, [value], 'N'  From dbo.FN_SPLIT(@HigherRoleList,'|')

	DECLARE @TargetRole VARCHAR(3)
	DECLARE @DirectOrgLevel VARCHAR(40)
	DECLARE @EffCnt INT

	WHILE EXISTS(SELECT 1 FROM @TB_HIGHER_LIST WHERE USED = 'N')
	BEGIN
		SELECT TOP 1 @TargetRole = [ROLE] FROM @TB_HIGHER_LIST  WHERE USED = 'N' ORDER BY INDX
		UPDATE @TB_HIGHER_LIST SET USED = 'Y' WHERE [ROLE] = @TargetRole

		PRINT	@TargetRole
		SET		@DirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@TargetRole)

		EXEC sp_WFD01170_GeneratePersonList_MGR @Indx, @Company, @TargetRole, 'H', @GUID, @EffCnt OUT
		IF @@ERROR > 0 OR @EffCnt > 0
		BEGIN
			BREAK
		END
	END


END
GO
