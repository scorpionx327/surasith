DROP PROCEDURE [dbo].[sp_WFD02160_ViewMapLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====WFD02160 : Fixed Asset Map Location ==================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/11 (yyyy/mm/dd)
 exec sp_WFD02160_ViewMapLocation '42000'
*/
CREATE PROCEDURE [dbo].[sp_WFD02160_ViewMapLocation]
( 
	@COMPANY	T_COMPANY,
	@COST_CODE	T_COST_CODE,
	@EMP_CODE	T_SYS_USER
 )
AS
BEGIN TRY
	DECLARE @MAP_LAYOUT_PATH VARCHAR(MAX)

	SELECT TOP 1 @MAP_LAYOUT_PATH=VALUE 
	FROM TB_M_SYSTEM
	WHERE CATEGORY='SYSTEM_CONFIG' AND SUB_CATEGORY='MAP_PATH' AND CODE='COST_CENTER'


	SELECT COST_CODE,
	       CASE 
			WHEN MAP_PATH IS NOT NULL THEN CONCAT(@MAP_LAYOUT_PATH,'\',ISNULL(MAP_PATH,'')) 
			ELSE '' END AS MAP_PATH ,
			CASE 
			WHEN MAP_PATH IS NOT NULL THEN CONCAT(@MAP_LAYOUT_PATH,'\',ISNULL(MAP_PATH,'')) 
			ELSE '' END AS MAP_LOCATION_FULL_PATH
			,FORMAT(UPDATE_DATE,'ddMMyyyyHHmmssfff') AS UPDATE_DATE
	FROM TB_M_COST_CENTER 
	WHERE ( COST_CODE=@COST_CODE AND 
			COMPANY = @COMPANY AND
		     COST_CODE IN ((SELECT COST_CODE FROM dbo.FN_FD0COSTCENTERLIST(@EMP_CODE))
			)
		  )


END TRY
BEGIN CATCH
	
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH
GO
