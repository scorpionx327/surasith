DROP PROCEDURE [dbo].[sp_WFD02630_GetYear]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 22/02/2017
-- Description:	Get year
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02630_GetYear]
@company T_COMPANY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	  
      	SELECT H.[YEAR]
				 FROM TB_R_STOCK_TAKE_H H
				WHERE H.COMPANY=@company AND -- Filter Company
						H.PLAN_STATUS <>'D'
				       AND
											 EXISTS ( SELECT 1 
														FROM TB_R_STOCK_TAKE_D_PER_SV D 
														 WHERE D.STOCK_TAKE_KEY = H.STOCK_TAKE_KEY
															--AND ((@IS_FAADMIN = 'Y' ) OR (D.EMP_CODE = @EMP_CODE ) )
														)
				
		 Group by [YEAR]
		  ORDER BY H.[YEAR] asc
END
GO
