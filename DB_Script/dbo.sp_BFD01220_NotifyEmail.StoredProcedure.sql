DROP PROCEDURE [dbo].[sp_BFD01220_NotifyEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ============================================= SFAS =============================================
-- UISS_1.2.4_BFD01240_Cost Center Master Interface batch
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/01 (yyyy/mm/dd)
-- Description:  UISS_1.2.4_BFD01240_Cost Center Master Interface batch
-- Objective  :This process is validate 
- ============================================= SFAS =============================================*/

CREATE PROCEDURE  [dbo].[sp_BFD01220_NotifyEmail]
(
     @AppID	INT,
    @MailBody   NVARCHAR(MAX),
	@UserID		VARCHAR(8)
)
AS
BEGIN TRY

     ------------- Incase no data 
     DECLARE	@cnt	INT
     SELECT	 @cnt = COUNT(*)
	 FROM TB_R_REQUEST_H requstH INNER JOIN TB_M_COST_CENTER CostCenter
	 ON  CostCenter.COST_CODE=requstH.COST_CODE
	 WHERE requstH.[STATUS]  < 60  AND CostCenter.[STATUS]='N'

	IF(@cnt = 0)
	BEGIN
	     SELECT 1;
	     RETURN;
	END
	------------- Incase have data system will send email

   SET @MailBody=RTRIM(LTRIM(@MailBody)) ;

	DECLARE @to VARCHAR(255) 
	DECLARE @cc VARCHAR(255) 
	DECLARE @bcc VARCHAR(255) 
	DECLARE @subject VARCHAR(255)
    DECLARE @body NVARCHAR(MAX)

	DECLARE @Error BIT;
	SET @Error =0;

 --- Set mail To  ,get from 
	SELECT @to= CONCAT(@to+';','') + ltrim(rtrim(EMAIL))
	FROM TB_M_EMPLOYEE 
	WHERE FAADMIN='Y'
	ORDER BY EMP_CODE ASC;



    SET @cc = NULL;
	SET @bcc = NULL;

	--- Set mail subject
	SELECT @subject=VALUE
	FROM TB_M_SYSTEM 
	WHERE CATEGORY='SYSTEM_EMAIL' AND SUB_CATEGORY='SUBJECT' AND CODE='BFD01240' AND ACTIVE_FLAG='Y'
   
 declare @textcontentHeader varchar(500),@textcontentBody nvarchar(max)
   --set @textcontentHeader='</BR></BR><table border="1"> <tr>
			--					<td align="center"><b>NO</b></td>
			--					<td align="center"><b>Request No</b></td>
			--					<td align="center"><b>Request Type</b></td>
			--					<td align="center"><b>Status</b></td>
			--					<td align="center"><b>S/V  PIC</b></td>
			--					<td align="center"><b>Cost Center</b></td>
			--					<td align="center"><b>Cost Center Name</b></td>
			--					</tr>'
   set @textcontentBody=''

	DECLARE @RN INT
	DECLARE @DOC_NO VARCHAR(10)
	DECLARE @REQUEST_TYPE VARCHAR(400)
	DECLARE @DESC VARCHAR(500)
	DECLARE @SVName VARCHAR(60)
	DECLARE @COST_CODE VARCHAR(8)
	DECLARE @COST_NAME VARCHAR(240)

	IF CURSOR_STATUS('global','tt_cursor')>=-1
		BEGIN
						CLOSE tt_cursor   
						DEALLOCATE tt_cursor
		END

   DECLARE tt_cursor CURSOR FOR
   --Notify email incase, Cost Center expire date     
		   SELECT	  ROW_NUMBER() OVER (ORDER BY requstH.DOC_NO) AS RN-- Running No
					 , requstH.DOC_NO  -- AS 'Request No'
					 ,ISNULL(sysMaster.VALUE,requstH.REQUEST_TYPE) AS REQUEST_TYPE --Request Type
					 ,ISNULL(requstH.[DESC],'') AS ReqestDesc -- Status
					 ,CONCAT( requstH.EMP_NAME , '  ' , LEFT(EMP_LASTNAME,1) ,'.') AS SV_NAME
					 ,requstH.COST_CODE ,ISNULL(CostCenter.COST_NAME,'') AS COST_NAME
		  FROM TB_R_REQUEST_H requstH INNER JOIN TB_M_COST_CENTER CostCenter
		  ON  CostCenter.COST_CODE=requstH.COST_CODE
		  LEFT JOIN (   SELECT CODE,VALUE FROM TB_M_SYSTEM
						 WHERE CATEGORY='RPT_REQ_TYPE' AND SUB_CATEGORY='REQ_TYPE' 
					  )  sysMaster
		 ON  sysMaster.CODE=requstH.REQUEST_TYPE
		 WHERE requstH.[STATUS]  < 60  AND CostCenter.[STATUS]='N'
    OPEN tt_cursor
    FETCH NEXT FROM tt_cursor
		  INTO @RN,@DOC_NO,@REQUEST_TYPE,@DESC,@SVName,  @COST_CODE,@COST_NAME
	 WHILE @@FETCH_STATUS = 0
		BEGIN 
		   set @textcontentBody = CONCAT( @textcontentBody ,'<tr><td  align="right">' , @RN ,'</td>'
										  ,'<td align="center">' + @DOC_NO + '</td>'
										  ,'<td align="left">' + @REQUEST_TYPE + '</td>'
										  ,'<td align="left">' + @DESC + '</td>'
										   ,'<td align="left">' + @SVName + '</td>'
										   ,'<td align="center">' + @COST_CODE + '</td>'
										  ,'<td align="left">' + @COST_NAME + '</td>'
										  ,'</tr>')

		FETCH NEXT FROM tt_cursor
		INTO @RN,@DOC_NO,@REQUEST_TYPE,@DESC,@SVName,  @COST_CODE,@COST_NAME
	   END 
	 CLOSE tt_cursor
	 DEALLOCATE tt_cursor

	    SET @body=CONCAT(LTRIM(RTRIM(@textcontentHeader)) , @textcontentBody  ,'</table></br></br></body></html>');

	    SET @MailBody=CONCAT(@MailBody , @body)

	  INSERT INTO [TB_R_NOTIFICATION]
           ([ID]   ,[TO]    ,[CC]    ,[BCC]  ,[TITLE] ,[MESSAGE] ,[FOOTER]    ,[TYPE]
           ,[START_PERIOD]  ,[END_PERIOD]    ,[SEND_FLAG],[RESULT_FLAG]
           ,[REF_FUNC]   ,[REF_DOC_NO] ,[CREATE_DATE]
           ,[CREATE_BY] ,[UPDATE_DATE] ,[UPDATE_BY])
     VALUES
           (NEXT VALUE FOR NOTIFICATION_ID ,@to ,@cc    ,@bcc ,@subject    ,@MailBody ,NULL   ,'I' -- (Immediately) 
			 ,NULL,NULL  ,'N'  ,NULL ,'BFD01240'  ,NULL    ,GETDATE()  ,@UserID ,GETDATE()  ,@UserID)

	SELECT 1;
	RETURN;
END TRY
BEGIN CATCH
			IF CURSOR_STATUS('global','tt_cursor')>=-1
					BEGIN
									CLOSE tt_cursor   
									DEALLOCATE tt_cursor
					END
--===========================================================Write log =====================================
     DECLARE @errMessage nVARCHAR(1000);
     SET @errMessage= CONCAT('Receiving Cost Center Master Interface batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
	 exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@errMessage,@UserID,'Y'
--===========================================================End Write log =====================================
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		SELECT -1;
		RETURN;
END CATCH
GO
