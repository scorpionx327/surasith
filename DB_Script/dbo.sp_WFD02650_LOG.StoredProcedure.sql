DROP PROCEDURE [dbo].[sp_WFD02650_LOG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Check user login
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_LOG]
	@LOG nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into temp_log_handheld
	values (@LOG, GETDATE())
	
END
GO
