DROP PROCEDURE [dbo].[sp_WFD02650_LockEmp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Phithak Khonnoy
-- Create date: 2017-02-22
-- Description:	Update Locked Employee SV
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_LockEmp]
	
	@STOCK_TAKE_KEY varchar(7) 
	,@SV_EMP_CODE varchar(8)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE TB_R_STOCK_TAKE_D_PER_SV   
 		SET IS_LOCK = 'Y'
		, LOCKED = 'Y'
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY
 			--and EMP_CODE=@EMP_CODE
			and EMP_CODE = @SV_EMP_CODE
			--(select
			--	CASE 
			--		WHEN (SELECT COUNT(1) FROM TB_M_SV_COST_CENTER sv WHERE sv.EMP_CODE=e.EMP_CODE) > 0 THEN e.EMP_CODE
			--		ELSE SV.EMP_CODE
			--	END
			--FROM TB_M_EMPLOYEE e
		 --   inner join TB_M_SV_COST_CENTER SV on SV.COST_CODE = e.COST_CODE
			--WHERE e.EMP_CODE=@EMP_CODE)
			and LOCKED is null
END
GO
