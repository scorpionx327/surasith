DROP PROCEDURE [dbo].[sp_WFD02620_CompletedStockTakePlan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-23
-- Description:	Update stock take plan to completed (C)
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_CompletedStockTakePlan]
	-- Add the parameters for the stored procedure here
	@STOCK_TAKE_KEY nvarchar(7)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE TB_R_STOCK_TAKE_H SET PLAN_STATUS='C'
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY


END




GO
