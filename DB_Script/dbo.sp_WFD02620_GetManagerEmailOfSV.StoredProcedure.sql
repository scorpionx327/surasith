DROP PROCEDURE [dbo].[sp_WFD02620_GetManagerEmailOfSV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Uten Sopradid
-- Create date: 2017-04-07
-- Description:	Get data for send email
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetManagerEmailOfSV]
	@EMP_CODE	T_USER
AS
BEGIN

SELECT  dbo.fn_GetEmail_cc_ListofManager(@EMP_CODE);

END




GO
