DROP PROCEDURE [dbo].[sp_WFD02620_UpdateStockTakePerSV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-22
-- Description:	Update stock take per sv data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_UpdateStockTakePerSV]
	
	@STOCK_TAKE_KEY varchar(7) 
	,@EMP_CODE varchar(8) 
	,@FA_DATE_FROM date 
	,@FA_DATE_TO date 
	,@DATE_FROM date 
	,@DATE_TO date 
	,@TIME_START varchar(5) 
	,@TIME_END varchar(5) 
	,@TIME_MINUTE int 
	,@USAGE_HANDHELD int 
	,@TOTAL_ASSET int 
	,@UPDATE_BY varchar(8) 
	,@UPDATE_DATE datetime
	,@IS_LOCK nvarchar(1)

AS
	DECLARE @MESSAGE NVARCHAR(250);
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- ///////////////////////////////////////////////////////////////////////		
		-- Validate add data

		--In case of Concurrent Checking, 
						

		IF(	SELECT COUNT(1) FROM TB_R_STOCK_TAKE_D_PER_SV 
			WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY and LTRIM(RTRIM(EMP_CODE))=LTRIM(RTRIM(@EMP_CODE)) and UPDATE_DATE <> @UPDATE_DATE) > 0
				--and (DATE_FROM <> @DATE_FROM or DATE_TO <> @DATE_TO or TIME_START <> @TIME_START or TIME_END <> @TIME_END)) > 0
		BEGIN


			SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'

			SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
			
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;

		END

	UPDATE TB_R_STOCK_TAKE_D_PER_SV   
 		SET FA_DATE_FROM=@FA_DATE_FROM
 			, FA_DATE_TO=@FA_DATE_TO
 			, DATE_FROM=@DATE_FROM
 			, DATE_TO=@DATE_TO
 			, TIME_START=@TIME_START
 			, TIME_END=@TIME_END
 			, TIME_MINUTE=@TIME_MINUTE
 			, USAGE_HANDHELD=@USAGE_HANDHELD
 			, TOTAL_ASSET=@TOTAL_ASSET
 			, UPDATE_DATE=GETDATE()
 			, UPDATE_BY=@UPDATE_BY
			, IS_LOCK=@IS_LOCK
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY
 			and EMP_CODE=@EMP_CODE


END




GO
