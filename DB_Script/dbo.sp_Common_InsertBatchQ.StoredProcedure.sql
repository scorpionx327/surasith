DROP PROCEDURE [dbo].[sp_Common_InsertBatchQ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[sp_Common_InsertBatchQ]
(
@BatchID	VARCHAR(10),
@ReqBy		VARCHAR(8),
@Status		 VARCHAR(1),
@Description  VARCHAr(255)
)
AS
BEGIN



	INSERT INTO TB_BATCH_Q(APP_ID,BATCH_ID, [STATUS],[DESCRIPTION], CREATE_BY,  CREATE_DATE, UPDATE_BY, UPDATE_DATE)
    	VALUES (NEXT VALUE FOR BatchQ_APP_ID,   @BatchID,@Status,@Description,@ReqBy,GETDATE(),@ReqBy,GETDATE())
    
	   --SELECT @@IDENTITY

 --Add by Uten 16/04/2015-----
	  DECLARE @IDENT_APP_ID numeric(18,0);
 
	SELECT		TOP 1 @IDENT_APP_ID = APP_ID
	FROM		TB_BATCH_Q 
	WHERE		BATCH_ID = @BatchID 
	AND			CREATE_BY = @ReqBy 
	ORDER BY	CREATE_DATE DESC


	SELECT @IDENT_APP_ID;
--Add by Uten 16/04/2015-----


END



GO
