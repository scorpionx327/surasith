DROP PROCEDURE [dbo].[sp_WFD02910_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02910_ApproveRequest]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN

	EXEC sp_WFD02910_UpdateAssetApprove @GUID, @DOC_NO, @USER
	
	/*
	Approve by AEC
	*/
	IF dbo.fn_IsReadyGenFile(@DOC_NO) = 'N'
	RETURN



	UPDATE	TB_R_REQUEST_RECLAS
	SET		[STATUS]	= 'GEN',
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= @USER
	WHERE	DOC_NO		= @DOC_NO AND
			ASSET_NO	IS NOT NULL AND
			[STATUS]	= 'NEW'

	-------------------------------------------------------------------------------------
	-- Approve by AEC
	-- Execute when Status is change from 10 -> 20 (MGR Approve)
	UPDATE	H
	SET		H.PROCESS_STATUS	= 'CC',
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_RECLAS M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO			= @DOC_NO AND
			H.PROCESS_STATUS	= 'C'


	
		
	DECLARE @format VARCHAR(255) = 'Reclassification : From {0} To {1}'

	DECLARE @LatestApproverCode	T_SYS_USER, @LatestApproverName	VARCHAR(68)
	DECLARE @AECCode		T_SYS_USER
	
	-- Reclassification : From {current asset} To {new asset}
	UPDATE	H
	SET		H.DETAIL		= dbo.fn_StringFormat(@format, CONCAT(M.ASSET_CLASS,'|',M.NEW_ASSET_CLASS)),
			H.TRANS_DATE	= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= @LatestApproverName, 
			H.AEC_APPR_BY			= @AECCode,
			H.AEC_APPR_DATE			= GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_RECLAS M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND 
			M.DOC_NO	= H.DOC_NO AND
			M.ASSET_NO	= H.ASSET_NO AND 
			M.ASSET_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO



	EXEC sp_WFD01170_ApproveAttachDoc @GUID, @DOC_NO, @USER
END
GO
