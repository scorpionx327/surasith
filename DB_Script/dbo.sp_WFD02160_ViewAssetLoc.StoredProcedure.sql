DROP PROCEDURE [dbo].[sp_WFD02160_ViewAssetLoc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====WFD02160 : Fixed Asset Map Location ==================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/11 (yyyy/mm/dd)
 exec sp_WFD02160_ViewMapLocation '42000'
*/
CREATE PROCEDURE [dbo].[sp_WFD02160_ViewAssetLoc]
( 
	@COMPANY	T_COMPANY,	
	@COST_CODE	T_COST_CODE
 )
AS
BEGIN TRY
		SELECT	L.ASSET_NO,
				L.COST_CODE,
				L.X AS X_POINT,
				L.Y AS Y_POINT 
		FROM	TB_M_ASSETS_LOC L
		INNER	JOIN
				TB_M_ASSETS_H H
		ON		L.ASSET_NO	= H.ASSET_NO AND
				L.COMPANY	= H.COMPANY AND
				L.COST_CODE	= H.COST_CODE
		WHERE	H.ASSET_SUB = '0000' AND 	
				L.COST_CODE = @COST_CODE -- Add By Surasith T. 2017-06-08 Display Asset that match with Selected CC								
	END TRY
BEGIN CATCH
	
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH
GO
