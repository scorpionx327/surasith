DROP PROCEDURE [dbo].[sp_WFD02640_GetActiveStatusPerSV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai L
-- Create date: 31/03/2017
-- Description:	return Y = ทุก Cost Center ทำ Request, N = ยังไม่ได้ทำทุก Cost Center
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_GetActiveStatusPerSV]
	@COMPANY		T_COMPANY,
	@YEAR			varchar(4)	  ,
	@ROUND			varchar(2)	  ,
	@ASSET_LOCATION varchar(1),
	@SV_EMP_CODE		varchar(30),
	@USER_LOGON  varchar(8)
AS
BEGIN
	DECLARE @All_AlreadySubmit AS VARCHAR(1)
	DECLARE @IsMangerView AS VARCHAR(1)
	SET		@IsMangerView='N'

IF(@ASSET_LOCATION='I')
BEGIN
	IF( EXISTS ( SELECT		1
				   FROM		TB_R_STOCK_TAKE_D stockD
				   WHERE    stockD.COMPANY	= @COMPANY AND
							stockD.YEAR		= @YEAR     AND    
							stockD.ROUND	=  @ROUND AND  
							stockD.ASSET_LOCATION = @ASSET_LOCATION AND  
							SV_EMP_CODE		= @SV_EMP_CODE AND 
							NOT EXISTS( SELECT 1
									  FROM TB_R_REQUEST_STOCK  rStock
									  WHERE stockD.COMPANY			= rStock.COMPANY AND
											stockD.YEAR				= rStock.YEAR AND    
											stockD.ROUND			= rStock.ROUND  AND  
											stockD.ASSET_LOCATION	= rStock.ASSET_LOCATION and 
											stockD.COST_CODE		= rStock.COST_CODE)
								)
						  )
	BEGIN
		  SET @All_AlreadySubmit ='N' --SELECT 'N' AS COMPLETE_FLAG     -- 	return N is mean haves some Cost Center no submit request 
	END 
	ELSE
	BEGIN
		  SET @All_AlreadySubmit ='Y'  --SELECT 'Y' AS COMPLETE_FLAG   -- 	return Y  is mean all Cost Center already submit request
END
END
ELSE
BEGIN
			IF( EXISTS ( SELECT 1
						   FROM TB_R_STOCK_TAKE_D stockD
						   WHERE	stockD.COMPANY			= @COMPANY AND
									stockD.YEAR				= @YEAR     AND    
									stockD.ROUND			= @ROUND AND  
									stockD.ASSET_LOCATION	= @ASSET_LOCATION AND  
									SV_EMP_CODE=@SV_EMP_CODE AND 
									NOT EXISTS( SELECT	1
												FROM	TB_R_REQUEST_STOCK  rStock
												WHERE   stockD.COMPANY			= rStock.COMPANY AND  
														stockD.YEAR				= rStock.YEAR AND    
														stockD.ROUND			= rStock.ROUND AND  
														stockD.ASSET_LOCATION	= rStock.ASSET_LOCATION and 
														stockD.SV_EMP_CODE		= rStock.SV_EMP_CODE)
										)
								  )
			BEGIN
				  SET @All_AlreadySubmit ='N' --SELECT 'N' AS COMPLETE_FLAG     -- 	return N is mean haves some Cost Center no submit request 
			END 
			ELSE
			BEGIN
				  SET @All_AlreadySubmit ='Y'  --SELECT 'Y' AS COMPLETE_FLAG   -- 	return Y  is mean all Cost Center already submit request
			END

END

IF(@ASSET_LOCATION='I')
BEGIN
		IF EXISTS (SELECT EMP_CODE FROM TB_R_STOCK_TAKE_D_PER_SV SV
						WHERE    SV.COMPANY	= @COMPANY 
							AND	SV.YEAR = @YEAR     
							AND  SV.ROUND   =  @ROUND 
							AND  SV.ASSET_LOCATION = @ASSET_LOCATION 
							AND  SV.EMP_CODE=@SV_EMP_CODE
							AND  SV.EMP_CODE IN(
												 SELECT SV.EMP_CODE 
												  FROM TB_T_COST_CENTER_ROLE R INNER JOIN TB_M_SV_COST_CENTER SV
												  ON R.COST_CODE = SV.COST_CODE AND R.COMPANY = SV.COMPANY
												 WHERE EXISTS (SELECT '1' FROM TB_M_SYSTEM S 
																WHERE S.CATEGORY = 'SYSTEM_CONFIG'
																AND S.SUB_CATEGORY = 'STOCK_TAKE_MANAGER' 
																AND S.CODE = R.APPV_ROLE 
																AND S.ACTIVE_FLAG = 'Y')
																AND  R.EMP_CODE = @USER_LOGON)
																)
		  BEGIN
		   SET  @IsMangerView ='Y'
		  END
END

--- Return data.
SELECT CASE
	   WHEN  @All_AlreadySubmit = 'Y' OR  @IsMangerView ='Y' THEN 'Y'
	   ELSE 'N' END AS COMPLETE_FLAG

END
GO
