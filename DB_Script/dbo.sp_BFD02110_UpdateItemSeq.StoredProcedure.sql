DROP PROCEDURE [dbo].[sp_BFD02110_UpdateItemSeq]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Suphachai Leetrakool 
-- Create date:  2017/05/18 (yyyy/mm/dd)
-- Description:  For update Item Seq
- ============================================= SFAS =============================================*/
CREATE PROCEDURE [dbo].[sp_BFD02110_UpdateItemSeq]
(
	 @UserID		VARCHAR(8),
	 @ERROR_MESSAGE	VARCHAR(MAX) OUT
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

	UPDATE TB_M_ASSETS_D
	SET ITEM_SEQ = T.ITEM_SEQ
	FROM
	(
		SELECT  ROW_NUMBER()  OVER (PARTITION BY COST_CODE ORDER BY S.REMARKS ASC, 
			CASE WHEN  S.CODE ='M/C' THEN H.MODEL ELSE H.ASSET_NO  END ASC,
			H.ASSET_NO ASC ) ITEM_SEQ,
			H.ASSET_NO,		D.COST_CODE,	H.MODEL,S.CODE
		FROM TB_M_ASSETS_H H
		INNER JOIN TB_M_ASSETS_D D ON H.ASSET_NO = D.ASSET_NO 
		LEFT JOIN 
		(
			SELECT CODE,			REMARKS -- Remark is field for sorting  
			FROM TB_M_SYSTEM 
			WHERE CATEGORY			= 'FAS_TYPE' 
			AND SUB_CATEGORY		= 'SUB_TYPE'
			AND ACTIVE_FLAG			= 'Y' 
		) S ON H.SUB_TYPE			=  S.CODE 
		WHERE H.STATUS				= 'Y' 	
		AND RIGHT(H.ASSET_NO, 2)	= '00'
		AND INHOUSE_FLAG            = 'Y' --Inhouse group by cost center	
	) T
	WHERE SUBSTRING(TB_M_ASSETS_D.ASSET_NO,0,LEN(TB_M_ASSETS_D.ASSET_NO) - 1) = SUBSTRING(T.ASSET_NO,0,LEN(T.ASSET_NO) - 1)
	AND TB_M_ASSETS_D.COST_CODE		= T.COST_CODE 
	AND LEN(TB_M_ASSETS_D.ASSET_NO) >= 2



	/* Update Item Seq for outsource*/
	UPDATE TB_M_ASSETS_D
	SET ITEM_SEQ = T.ITEM_SEQ
	FROM
	(
		SELECT  ROW_NUMBER()  OVER (PARTITION BY SUPPLIER_NO ORDER BY S.REMARKS ASC, 
			CASE WHEN  S.CODE ='M/C' THEN H.MODEL ELSE H.ASSET_NO  END ASC,
			H.ASSET_NO ASC ) ITEM_SEQ,
			H.ASSET_NO,		D.COST_CODE,	H.MODEL,S.CODE
		FROM TB_M_ASSETS_H H
		INNER JOIN TB_M_ASSETS_D D ON H.ASSET_NO = D.ASSET_NO 
		LEFT JOIN 
		(
			SELECT CODE,			REMARKS -- Remark is field for sorting  
			FROM TB_M_SYSTEM 
			WHERE CATEGORY			= 'FAS_TYPE' 
			AND SUB_CATEGORY		= 'SUB_TYPE'
			AND ACTIVE_FLAG			= 'Y' 
		) S ON H.SUB_TYPE			=  S.CODE 
		WHERE H.STATUS				= 'Y' 	
		AND RIGHT(H.ASSET_NO, 2)	= '00'
		AND INHOUSE_FLAG            = 'N' --Outsource group by supplier no.	
	) T
	WHERE SUBSTRING(TB_M_ASSETS_D.ASSET_NO,0,LEN(TB_M_ASSETS_D.ASSET_NO) - 1) = SUBSTRING(T.ASSET_NO,0,LEN(T.ASSET_NO) - 1)
--	AND TB_M_ASSETS_D.COST_CODE		= T.COST_CODE 
	AND LEN(TB_M_ASSETS_D.ASSET_NO) >= 2

	RETURN @Error ;
END TRY
BEGIN CATCH
	SET @ERROR_MESSAGE=ERROR_MESSAGE();
	RETURN 1;
END CATCH










GO
