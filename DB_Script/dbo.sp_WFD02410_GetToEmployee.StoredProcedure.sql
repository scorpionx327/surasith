DROP PROCEDURE [dbo].[sp_WFD02410_GetToEmployee]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 19/07/2019
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_GetLocation 'C20170004','AS0000000100','30000',null,'0179'
CREATE PROCEDURE [dbo].[sp_WFD02410_GetToEmployee]
(	
	@COST_CENTER				T_COST_CODE,
	@COMPANY_CODE				T_COMPANY
)
AS
BEGIN
	SELECT 
		EMP_CODE CODE,	
		CONCAT(EMP_TITLE,' ',EMP_NAME,' ',EMP_LASTNAME) VALUE
	FROM TB_M_EMPLOYEE  
	WHERE COMPANY		= @COMPANY_CODE		AND
	COST_CODE			= @COST_CENTER

END


GO
