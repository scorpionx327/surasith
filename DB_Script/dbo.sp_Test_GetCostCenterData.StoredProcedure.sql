DROP PROCEDURE [dbo].[sp_Test_GetCostCenterData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Test_GetCostCenterData]
	-- Add the parameters for the stored procedure here
	@CURRENCT_PAGE	int
	, @PAGE_SIZE		int
	, @ORDER_BY			NVARCHAR(50)
	, @ORDER_TYPE		nvarchar(5)
	, @TOTAL_ITEM		int output
AS
	DECLARE @FROM	INT
			, @TO	INT
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @FROM = (@PAGE_SIZE * (@CURRENCT_PAGE-1)) + 1;
	SET @TO = @PAGE_SIZE * (@CURRENCT_PAGE);

	SELECT @TOTAL_ITEM = COUNT(1) FROM _tb_m_cost_center

	SELECT
		*
	FROM (
		SELECT
			ROW_NUMBER() OVER (ORDER BY	
									CASE WHEN @ORDER_BY = '0' and @ORDER_TYPE='asc' THEN COST_CODE
											END ASC,
									 CASE WHEN @ORDER_BY = '0' and @ORDER_TYPE='desc' THEN COST_CODE
											END DESC,
									 CASE WHEN @ORDER_BY = '1' and @ORDER_TYPE='asc' THEN COST_NAME
											END ASC,
									 CASE WHEN @ORDER_BY = '1' and @ORDER_TYPE='desc' THEN COST_NAME
											END DESC,
									 CASE WHEN @ORDER_BY = '2' and @ORDER_TYPE='asc' THEN CREATE_DATE
											END ASC,
									 CASE WHEN @ORDER_BY = '2' and @ORDER_TYPE='desc' THEN CREATE_DATE
											END DESC,
									 CASE WHEN @ORDER_BY = '3' and @ORDER_TYPE='asc' THEN CREATE_BY
											END ASC,
									 CASE WHEN @ORDER_BY = '3' and @ORDER_TYPE='desc' THEN CREATE_BY
											END DESC
							) as NO
			, COST_CODE, COST_NAME, CREATE_DATE, CREATE_BY
		FROM _tb_m_cost_center
	) as d
	WHERE d.NO >= @FROM and d.NO <= @TO

END



GO
