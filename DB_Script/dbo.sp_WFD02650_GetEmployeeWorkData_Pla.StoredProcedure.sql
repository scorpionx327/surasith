DROP PROCEDURE [dbo].[sp_WFD02650_GetEmployeeWorkData_Pla]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 02-03-2017
-- Description:	Login from handheld
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_GetEmployeeWorkData_Pla]
	-- Add the parameters for the stored procedure here
	@EMP_CODE varchar(8)
AS
	DECLARE @STOCK_TALE_KEY nvarchar(7)
BEGIN
	SET NOCOUNT ON;

	IF( SELECT TOP 1 FAADMIN FROM TB_M_EMPLOYEE WHERE EMP_CODE=@EMP_CODE ) = 'Y'
	BEGIN

		SELECT TOP 1 @STOCK_TALE_KEY = STOCK_TAKE_KEY FROM TB_R_STOCK_TAKE_H
		WHERE ASSET_LOCATION = 'O' and PLAN_STATUS in ('C','S')
		order by CREATE_DATE desc

		IF @STOCK_TALE_KEY IS NULL
		BEGIN
			
			SELECT TOP 1 @STOCK_TALE_KEY = STOCK_TAKE_KEY FROM TB_R_STOCK_TAKE_H
			WHERE ASSET_LOCATION = 'I' and PLAN_STATUS in ('C','S')
			order by CREATE_DATE desc

		END

		SELECT 
			top 1
			e.EMP_CODE as EMP_NO
			,e.EMP_TITLE
			,e.EMP_NAME
			,e.EMP_LASTNAME
			,e.FAADMIN as IS_FAADMIN
			,'N' as IS_SV
			,'' as SV_EMP_CODE
		INTO #tmpTable 
		FROM TB_M_EMPLOYEE e
		WHERE EMP_CODE=@EMP_CODE

		SELECT
			(SELECT TOP 1  EMP_NO FROM #tmpTable) as EMP_NO
			,(SELECT TOP 1  EMP_TITLE FROM #tmpTable) as EMP_TITLE
			,(SELECT TOP 1  EMP_NAME FROM #tmpTable) as EMP_NAME
			,(SELECT TOP 1  EMP_LASTNAME FROM #tmpTable) as EMP_LASTNAME
			,sv.EMP_CODE as COST_CODE
			,sv.EMP_NAME as COST_NAME
			,(SELECT TOP 1  IS_FAADMIN FROM #tmpTable) as IS_FAADMIN 
			,'N' as IS_SV
			,sv.EMP_CODE as SV_EMP_CODE
		FROM TB_R_STOCK_TAKE_D_PER_SV sv 
		WHERE sv.STOCK_TAKE_KEY=@STOCK_TALE_KEY

		DROP TABLE #tmpTable

	END
	ELSE BEGIN

		SELECT
			EMP_NO
			,EMP_TITLE
			,EMP_NAME
			,EMP_LASTNAME
			,COST_CODE
			,COST_NAME
			,IS_FAADMIN
			, CASE	WHEN IS_SV > 0 THEN 'Y'
					ELSE 'N'
				END as IS_SV
			, CASE	WHEN IS_SV > 0 THEN EMP_NO
			  ELSE SV_EMP_CODE
			  END AS SV_EMP_CODE
		FROM (
			select
				e.EMP_CODE as EMP_NO
				, e.EMP_TITLE as EMP_TITLE
				, e.EMP_NAME as EMP_NAME
				, e.EMP_LASTNAME as EMP_LASTNAME
				, e.COST_CODE as COST_CODE
				, e.COST_NAME as COST_NAME
				, e.FAADMIN as IS_FAADMIN
				, (SELECT COUNT(1) FROM TB_M_SV_COST_CENTER sv WHERE sv.EMP_CODE=e.EMP_CODE) as IS_SV
		
				, SV.EMP_CODE as SV_EMP_CODE
			FROM TB_M_EMPLOYEE e
		    inner join TB_M_SV_COST_CENTER SV on SV.COST_CODE = e.COST_CODE
			--INNER JOIN (
			--	select	
			--		@EMP_CODE as EMP_CODE_COST
			--		, cost.COST_CODE
			--		, cost.COST_NAME
			--		, c.EMP_CODE as SV_EMP_CODE
			----	from [dbo].[FN_FD0COSTCENTERLIST](@EMP_CODE) cost
			----	INNER JOIN 
			--	TB_M_SV_COST_CENTER  c on cost.COST_CODE=c.COST_CODE 
			--	GROUP BY  cost.COST_CODE , cost.COST_NAME , c.EMP_CODE 
			--) c on e.EMP_CODE=c.EMP_CODE_COST
			WHERE e.EMP_CODE=@EMP_CODE
		) data
	
	END

END




GO
