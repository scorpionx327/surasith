DROP PROCEDURE [dbo].[sp_WFD01250_DeleteApprove]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 13/03/2017
-- Description:	Delete approve data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_DeleteApprove]
	@APPRV_ID		int
	, @UPDATE_BY	nvarchar(8)
	, @UPDATE_DATE	datetime
AS
	DECLARE @MESSAGE NVARCHAR(250);
BEGIN
	SET NOCOUNT ON;

	if( SELECT COUNT(1) FROM TB_M_APPROVE_H WHERE APPRV_ID=@APPRV_ID and UPDATE_DATE != @UPDATE_DATE) > 0
	BEGIN

			SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'

			SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
			
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;

	END
	ELSE
	BEGIN


		DELETE TB_M_APPROVE_D WHERE APPRV_ID=@APPRV_ID;
		DELETE TB_M_APPROVE_H WHERE APPRV_ID=@APPRV_ID;

	END 

END





GO
