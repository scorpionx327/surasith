DROP PROCEDURE [dbo].[sp_LFD02690_GetStockTakingSummaryReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_LFD02690_GetStockTakingSummaryReport](
	-- Add the parameters for the stored procedure here
		@COMPANY	T_COMPANY = NULL,									
		@PERIOD_YEAR	VARCHAR(4),									
		@PERIOD_ROUND	VARCHAR(2),
		@ASSET_LOCATION VARCHAR(1)	
	)
AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE @STATUSPLAN VARCHAR(1);

	SELECT @STATUSPLAN = PLAN_STATUS
	FROM TB_R_STOCK_TAKE_H
	WHERE [YEAR] = @PERIOD_YEAR and [ROUND]=@PERIOD_ROUND and ASSET_LOCATION = @ASSET_LOCATION and COMPANY=@COMPANY


	IF @ASSET_LOCATION = 'I'
	BEGIN

     SELECT TD.COMPANY COMPANY,
	   --TD.PLANT_CD AS PLANT_CD,
    --   TD.PLANT_NAME AS PLANT_NAME,
       TD.COST_CODE AS COST_CENTER,
       TD.COST_NAME AS COST_NAME,
       COUNT(1) AS TOTAL_ITEMs,
       COUNT(CASE
               WHEN TD.CHECK_STATUS = 'Y' THEN
                1
             END) AS COUNTED_ITEMs,
       COUNT(CASE
               WHEN TD.CHECK_STATUS != 'Y' THEN
                1
             END) AS REMAIN_ITEMs,
       
       MIN(CASE
             WHEN TD.START_COUNT_TIME IS NOT NULL THEN
			  [dbo].fn_dateFAS( TD.START_COUNT_TIME)
           END) AS "START_DATE",
       MIN(CASE
             WHEN TD.START_COUNT_TIME IS NOT NULL THEN
              TD.START_COUNT_TIME
           END) AS "START_TIME",
       IIF(COUNT(CASE
                   WHEN TD.CHECK_STATUS != 'Y' THEN
                    1
                 END) = 0 or (@STATUSPLAN = 'F'),
		     [dbo].fn_dateFAS(MAX(TD.END_COUNT_TIME)),
           null) AS "FINISH_DATE",

       IIF(COUNT(CASE
                   WHEN TD.CHECK_STATUS != 'Y' THEN
                    1
                 END) = 0 or (@STATUSPLAN = 'F'),
           MAX(TD.END_COUNT_TIME),
           null) AS "FINISH_TIME",
       
       IIF(COUNT(CASE
                   WHEN TD.CHECK_STATUS != 'Y' THEN
                    1
                 END) = 0 or (@STATUSPLAN = 'F'),
           
           CONVERT(INT,ROUND(DATEDIFF(second,
                          MIN(CASE
                                WHEN TD.START_COUNT_TIME IS NOT NULL THEN
                                 TD.START_COUNT_TIME
                              END),
                          
                          MAX(TD.END_COUNT_TIME)) / 60.0,
                 0))
           
          ,
           null) AS ACTUAL_TIME,
       CONVERT(INT,ROUND(SUM(TD.COUNT_TIME)/60.0,0)) AS COUNT_TIME
   FROM TB_R_STOCK_TAKE_H TH

  LEFT JOIN TB_R_STOCK_TAKE_D TD ON TH.STOCK_TAKE_KEY = TD.STOCK_TAKE_KEY
                                AND TH.[YEAR] = TD.[YEAR]
                                AND TH.[ROUND] = TD.[ROUND]
                                AND TH.ASSET_LOCATION = TD.ASSET_LOCATION

  LEFT JOIN TB_R_STOCK_TAKE_INVALID_CHECKING TC ON TD.STOCK_TAKE_KEY =
                                                   TC.STOCK_TAKE_KEY
                                               AND TD.ASSET_NO =
                                                   TC.ASSET_NO
  LEFT JOIN TB_M_EMPLOYEE TE ON TC.EMP_CODE = TE.EMP_CODE

  LEFT JOIN TB_M_ASSETS_H AH ON TD.ASSET_NO = AH.ASSET_NO
							AND TD.ASSET_SUB=AH.ASSET_SUB
							AND TD.COMPANY=AH.COMPANY

  WHERE 
	TH.[YEAR] = @PERIOD_YEAR
	AND TH.[ROUND] = @PERIOD_ROUND
	AND TH.[ASSET_LOCATION] = @ASSET_LOCATION

 GROUP BY --TD.PLANT_CD, TD.PLANT_NAME, 
 TD.COST_CODE, TD.COST_NAME,TD.COMPANY
 ORDER BY --TD.PLANT_CD, 
 TD.COST_CODE

 END
 ELSE -- For outsource
 BEGIN
   SELECT TD.COMPANY COMPANY,
	   --TD.PLANT_CD AS PLANT_CD,
    --   TD.PLANT_NAME AS PLANT_NAME,
       TD.SV_EMP_CODE AS COST_CENTER,
       TD.SUPPLIER_NAME AS COST_NAME,
       COUNT(1) AS TOTAL_ITEMs,
       COUNT(CASE
               WHEN TD.CHECK_STATUS = 'Y' THEN
                1
             END) AS COUNTED_ITEMs,
       COUNT(CASE
               WHEN TD.CHECK_STATUS != 'Y' THEN
                1
             END) AS REMAIN_ITEMs,
       
       MIN(CASE
             WHEN TD.START_COUNT_TIME IS NOT NULL THEN
              TD.START_COUNT_TIME
           END) AS "START_DATE",
       MIN(CASE
             WHEN TD.START_COUNT_TIME IS NOT NULL THEN
              TD.START_COUNT_TIME
           END) AS "START_TIME",
       IIF(COUNT(CASE
                   WHEN TD.CHECK_STATUS != 'Y' THEN
                    1
                 END) = 0 or (@STATUSPLAN = 'F'),
           MAX(TD.END_COUNT_TIME),
           null) AS "FINISH_DATE",
       IIF(COUNT(CASE
                   WHEN TD.CHECK_STATUS != 'Y' THEN
                    1
                 END) = 0 or (@STATUSPLAN = 'F'),
           MAX(TD.END_COUNT_TIME),
           null) AS "FINISH_TIME",
       
       IIF(COUNT(CASE
                   WHEN TD.CHECK_STATUS != 'Y' THEN
                    1
                 END) = 0 or (@STATUSPLAN = 'F'),
           
           CONVERT(INT,ROUND(DATEDIFF(second,
                          MIN(CASE
                                WHEN TD.START_COUNT_TIME IS NOT NULL THEN
                                 TD.START_COUNT_TIME
                              END),
                          
                          MAX(TD.END_COUNT_TIME)) / 60.0,
                 0))
           
          ,
           null) AS ACTUAL_TIME,
       CONVERT(INT,ROUND(SUM(TD.COUNT_TIME)/60.0,0)) AS COUNT_TIME
   FROM TB_R_STOCK_TAKE_H TH

  LEFT JOIN TB_R_STOCK_TAKE_D TD ON TH.STOCK_TAKE_KEY = TD.STOCK_TAKE_KEY
                                AND TH.[YEAR] = TD.[YEAR]
                                AND TH.[ROUND] = TD.[ROUND]
                                AND TH.ASSET_LOCATION = TD.ASSET_LOCATION

  LEFT JOIN TB_R_STOCK_TAKE_INVALID_CHECKING TC ON TD.STOCK_TAKE_KEY =
                                                   TC.STOCK_TAKE_KEY
                                               AND TD.ASSET_NO =
                                                   TC.ASSET_NO
  LEFT JOIN TB_M_EMPLOYEE TE ON TC.EMP_CODE = TE.EMP_CODE

  LEFT JOIN TB_M_ASSETS_H AH ON TD.ASSET_NO = AH.ASSET_NO
							AND TD.ASSET_SUB=AH.ASSET_SUB
							AND TD.COMPANY=AH.COMPANY

  WHERE 
	TH.[YEAR] = @PERIOD_YEAR
	AND TH.[ROUND] = @PERIOD_ROUND
	AND TH.[ASSET_LOCATION] = @ASSET_LOCATION
	AND TH.COMPANY=@COMPANY

 GROUP BY --TD.PLANT_CD, TD.PLANT_NAME, 
 TD.SV_EMP_CODE, TD.SUPPLIER_NAME,TD.COMPANY
 ORDER BY --TD.PLANT_CD, 
 TD.SV_EMP_CODE

 END

END



GO
