DROP PROCEDURE [dbo].[sp_WFD02620_GetParamWithoutAssetLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Uten Sopradid
-- Create date: 2017-04-04
-- Description:	Get latest round incase add plan without asset location
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetParamWithoutAssetLocation]
(	
    @YEAR				varchar(4)
	, @ROUND			varchar(2)
)
AS
BEGIN

	SELECT top 1  h.YEAR , h.ROUND	, h.ASSET_LOCATION

	FROM TB_R_STOCK_TAKE_H h
	WHERE h.YEAR=@YEAR and h.ROUND=@ROUND 
	ORDER BY CREATE_DATE desc;

END





GO
