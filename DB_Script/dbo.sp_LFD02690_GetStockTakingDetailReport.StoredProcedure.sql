DROP PROCEDURE [dbo].[sp_LFD02690_GetStockTakingDetailReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_LFD02690_GetStockTakingDetailReport](
	-- Add the parameters for the stored procedure here
		@COMPANY T_COMPANY = null,
		@PERIOD_YEAR	VARCHAR(4),									
		@PERIOD_ROUND	VARCHAR(2),
		@ASSET_LOCATION VARCHAR(1)	
	)
AS
BEGIN	
	SET NOCOUNT ON;

	IF @ASSET_LOCATION = 'I'
	BEGIN

       SELECT TD.COMPANY COMPANY,
	   TD.COST_CODE,
       TD.COST_NAME,
	   TD.ASSET_CLASS,
	   TD.ASSET_NO,
	   TD.ASSET_SUB,
	   TD.ASSET_NAME,
	   AH.INVEN_NO ASSET_PLATE_NO,
       dbo .fn_dateFAS(TD.SCAN_DATE) AS SCAN_DATE,
       CONVERT(VARCHAR(5), TD.SCAN_DATE, 108) AS SCAN_TIME
      FROM TB_R_STOCK_TAKE_H TH

      LEFT JOIN TB_R_STOCK_TAKE_D TD ON TH.STOCK_TAKE_KEY = TD.STOCK_TAKE_KEY
                                AND TH.[YEAR] = TD. [YEAR]
                                AND TH.[ROUND] = TD.[ROUND]
                                AND TH.ASSET_LOCATION = TD.ASSET_LOCATION

      LEFT JOIN TB_R_STOCK_TAKE_INVALID_CHECKING TC ON TD.STOCK_TAKE_KEY =
                                                   TC.STOCK_TAKE_KEY
                                               AND TD.ASSET_NO =
                                                   TC.ASSET_NO
     LEFT JOIN TB_M_EMPLOYEE TE ON TC.EMP_CODE = TE.EMP_CODE

     LEFT JOIN TB_M_ASSETS_H AH ON TD.ASSET_NO = AH.ASSET_NO
								AND AH.COMPANY=TD.COMPANY
								AND AH.ASSET_SUB=TD.ASSET_SUB

     WHERE TH.[YEAR] = @PERIOD_YEAR
       AND TH.[ROUND] = @PERIOD_ROUND
       AND TH.ASSET_LOCATION = @ASSET_LOCATION
	   AND TH.COMPANY=@COMPANY

     ORDER BY TD.COST_CODE,  TD.ASSET_NO														

    END
	ELSE
	BEGIN

	 SELECT TD.COMPANY COMPANY,
	   TD.COST_CODE,
       TD.COST_NAME,
	   TD.ASSET_CLASS,
	   TD.ASSET_NO,
	   TD.ASSET_SUB,
	   TD.ASSET_NAME,
	   AH.INVEN_NO ASSET_PLATE_NO,
       dbo .fn_dateFAS(TD.SCAN_DATE) AS SCAN_DATE,
       CONVERT(VARCHAR(5), TD.SCAN_DATE, 108) AS SCAN_TIME
      FROM TB_R_STOCK_TAKE_H TH

      LEFT JOIN TB_R_STOCK_TAKE_D TD ON TH.STOCK_TAKE_KEY = TD.STOCK_TAKE_KEY
                                AND TH.[YEAR] = TD. [YEAR]
                                AND TH.[ROUND] = TD.[ROUND]
                                AND TH.ASSET_LOCATION = TD.ASSET_LOCATION

      LEFT JOIN TB_R_STOCK_TAKE_INVALID_CHECKING TC ON TD.STOCK_TAKE_KEY =
                                                   TC.STOCK_TAKE_KEY
                                               AND TD.ASSET_NO =
                                                   TC.ASSET_NO
      LEFT JOIN TB_M_EMPLOYEE TE ON TC.EMP_CODE = TE.EMP_CODE

      LEFT JOIN TB_M_ASSETS_H AH ON TD.ASSET_NO = AH.ASSET_NO
										AND AH.COMPANY=TD.COMPANY
										AND AH.ASSET_SUB=TD.ASSET_SUB

      WHERE TH.[YEAR] = @PERIOD_YEAR
       AND TH.[ROUND] = @PERIOD_ROUND
       AND TH.ASSET_LOCATION = @ASSET_LOCATION
	   AND TH.COMPANY=@COMPANY

       ORDER BY TD.COST_CODE, 
	   TD.ASSET_NO	

	END

END



GO
