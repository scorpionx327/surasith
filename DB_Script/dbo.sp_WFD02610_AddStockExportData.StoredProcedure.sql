DROP PROCEDURE [dbo].[sp_WFD02610_AddStockExportData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-03-27
-- Description:	Add stock export data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02610_AddStockExportData]
	-- Add the parameters for the stored procedure here
	@BATCH_ID			nvarchar(10) ,
	@YEAR				varchar(4) ,
	@ROUND				varchar(2) ,
	@ASSET_LOCATION		varchar(1) ,
	@APL_ID				numeric(18, 0),
	@COMPANY			T_COMPANY
AS
BEGIN

	INSERT INTO [dbo].[TB_T_STOCK_EXPORT]
           ([APL_ID]
           ,[BATCH_ID]
           ,[YEAR]
           ,[ROUND]
		   ,[COMPANY]
           ,[ASSET_LOCATION])
     VALUES
           (@APL_ID
           ,@BATCH_ID
           ,@YEAR
           ,@ROUND
		   ,@COMPANY
           ,@ASSET_LOCATION)

END


GO
