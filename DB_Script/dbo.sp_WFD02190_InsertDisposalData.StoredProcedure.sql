DROP PROCEDURE [dbo].[sp_WFD02190_InsertDisposalData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 09/02/2017
-- Description:	Insert Fixed assets screen
-- =============================================
--declare @TOTAL_ITEM int exec sp_WFD02190_SearchData null,null,null,null,null,null,null,null,null,1,10,null,@TOTAL_ITEM out  
create PROCEDURE [dbo].[sp_WFD02190_InsertDisposalData]
(
	@GUID				    CHAR(50)			= NULL,		--Sub Type	
	@CREATE_BY				VARCHAR(200)		= NULL		--Cost Code	
)
AS
BEGIN
	BEGIN TRY
		DECLARE @ITEM_NO INT

		SELECT @ITEM_NO = COUNT(1) + 1 FROM TB_T_SELECTED_ASSETS
		WHERE GUID	= @GUID

		INSERT INTO TB_T_SELECTED_ASSETS
		(
			GUID,			ASSET_NO,		COST_CODE,		CREATE_BY,			CREATE_DATE,	ASSET_CATEGORY,		ITEM_NO						
		)
		SELECT
			@GUID,			A.ASSET_NO,		A.COST_CODE,	@CREATE_BY,			GETDATE(),		H.ASSET_CATEGORY,	CONVERT(VARCHAR(15),@ITEM_NO + ROW_NUMBER() OVER(ORDER BY A.ASSET_NO))	
		FROM TB_T_SELECTED_ASSETS_DISP A
		INNER JOIN TB_M_ASSETS_H H ON A.ASSET_NO = H.ASSET_NO 
		WHERE GUID = @GUID


		DELETE TB_T_SELECTED_ASSETS_DISP
		WHERE GUID = @GUID 
		
	END TRY
	BEGIN CATCH


	END CATCH

END
GO
