DROP PROCEDURE [dbo].[sp_WFD02320_GetPrintDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 07/02/2017
-- Description:	Print tag queue dialog
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02320_GetPrintDetail]
	@COMPANY	VARCHAR(255) = NULL,
	@COST_CODE	T_COST_CODE = NULL,
	@RESP_COST_CODE	T_COST_CODE = NULL,
	@CREATE_BY	T_SYS_USER= NULL,
	@FIND		Varchar(max) = NULL,
	@PrintAll	Varchar(1)   --Y = PrintAll, N = PrintTag
AS
BEGIN

    DECLARE @TOTAL_ITEM int;
	DECLARE @COST_CENTER Varchar(300);
	DECLARE @REQUESTOR Varchar(300);
	DECLARE @RESP_COST_CENTER VARCHAR(300);
	DECLARE @CntCost INT
	DECLARE @CntRequest INT

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 /* ==============================  CREATE Temp TABLE ============================== */

	 IF OBJECT_ID('tempdb..#T_Print_Detail') IS NOT NULL
     BEGIN
		DROP TABLE #T_Print_Detail
	 END

	SELECT	IIF(Q.COMPANY = 'P', '', Q.COMPANY) COMPANY,
			Q.COST_CODE, 
			CC.COST_NAME, 
			Q.RESP_COST_CODE,
			rc.COST_NAME AS RESP_COST_NAME,
			Q.CREATE_BY, 
			EM.EMP_CODE, -- For display
			EM.EMP_NAME, 
			Q.ASSET_NO,
			Q.ASSET_SUB,
			IIF(Q.PLATE_TYPE = 'P', '', Q.BARCODE_SIZE) BARCODE_SIZE,
			IIF(Q.PLATE_TYPE = 'P', '', Q.PRINT_LOCATION) PRINT_LOCATION, 
			Q.PLATE_TYPE
	INTO	#T_Print_Detail
	FROM	[dbo].[TB_R_PRINT_Q] Q
	LEFT	JOIN 
			TB_M_COST_CENTER cc 
	ON		Q.COMPANY = CC.COMPANY AND Q.COST_CODE = CC.COST_CODE
	LEFT	JOIN 
			TB_M_COST_CENTER rc 
	ON		Q.COMPANY = rc.COMPANY AND Q.RESP_COST_CODE = rc.COST_CODE

    LEFT	JOIN
			TB_M_EMPLOYEE em 
	ON		Q.CREATE_BY = em.SYS_EMP_CODE
	WHERE	(@PrintAll = 'N' and	Q.COMPANY		 = ISNULL(@COMPANY, Q.COMPANY) AND
									Q.COST_CODE		 = ISNULL(@COST_CODE,Q.COST_CODE) and 
									Q.RESP_COST_CODE = ISNULL(@RESP_COST_CODE,Q.COST_CODE) and 
									Q.CREATE_BY		 = ISNULL(@CREATE_BY,Q.CREATE_BY))
			OR 
			(@printAll = 'Y' and (	EXISTS(SELECT 1 FROM dbo.fn_GetMultipleCompanyList(@COMPANY) COM 
													WHERE COM.COMPANY_CODE = q.COMPANY) AND
									Q.COST_CODE			LIKE ISNULL(@FIND,Q.COST_CODE ) OR
    								CC.COST_NAME		LIKE ISNULL(@FIND,CC.COST_NAME ) OR
									Q.RESP_COST_CODE	LIKE ISNULL(@FIND, Q.RESP_COST_CODE ) OR
    								Q.CREATE_BY			LIKE ISNULL(@FIND,Q.CREATE_BY ) OR
    								EM.EMP_NAME			LIKE ISNULL(@FIND,EM.EMP_NAME )))	
	---- summary Header
	SELECT	@TOTAL_ITEM = COUNT(1)
	FROM	#T_Print_Detail

	----- Summary Cost Center
	SELECT	@CntCost = COUNT(DISTINCT CONCAT(COMPANY,'-', COST_CODE))		  
	FROM	#T_Print_Detail

	IF( @CntCost>1)
	BEGIN
		SET @COST_CENTER = CONCAT(@CntCost , ' COST CENTER')
	END
	ELSE
	BEGIN
		SELECT TOP 1  @COST_CENTER = concat(COST_CODE, ' - ', COST_NAME)
		FROM	#T_Print_Detail
	END

	----- Summary Responsible Cost Center
	SELECT	@CntCost = COUNT(DISTINCT CONCAT(COMPANY,'-', RESP_COST_CODE))		  
	FROM	#T_Print_Detail

	IF( @CntCost>1)
	BEGIN
		SET @COST_CENTER = CONCAT(@CntCost , ' COST CENTER')
	END
	ELSE
	BEGIN
		SELECT TOP 1  @RESP_COST_CENTER = concat(RESP_COST_CODE, ' - ', RESP_COST_NAME)
		FROM	#T_Print_Detail
	END
	----- Summary Create By
		   
    SELECT @CntRequest=COUNT(distinct CREATE_BY)		  
	FROM  #T_Print_Detail
	      
	IF( @CntRequest>1)
	BEGIN
		SET @REQUESTOR = CONCAT(@CntRequest, ' REQUESTOR')
	END
	ELSE
	BEGIN
		SELECT TOP 1 @REQUESTOR = CONCAT(EMP_CODE, ' - ', EMP_NAME)
		FROM  #T_Print_Detail
	END
	---- Show Print Dialog

	SELECT	Q.BARCODE_SIZE,
			V.[VALUE] as BARCODE_SIZE_DESC,
			P.[VALUE] AS PLATE_TYPE_DESC,
			Q.PLATE_TYPE,
			-- Get Company location from configuration
			T.REMARKS AS PRINT_COMPANY_LOCATION,
			Q.PRINT_LOCATION,
			T.[VALUE] as [LOCATION],
			S.[VALUE] as PRINTER_NAME,
			COUNT(1) as ITEM_QTY
	FROM	#T_Print_Detail Q
	LEFT	JOIN TB_M_SYSTEM T 
	ON		Q.PRINT_LOCATION	= T.CODE AND 
			T.CATEGORY			= 'PRINT_CONFIG' AND T.SUB_CATEGORY	= 'PRINTER_LOCATION' AND ACTIVE_FLAG = 'Y'
	LEFT	JOIN 
			TB_M_SYSTEM S 
	ON		S.CODE				= Q.BARCODE_SIZE AND S.REMARKS = '1' AND
			S.CATEGORY			= 'PRINT_CONFIG' and S.SUB_CATEGORY = Concat('PRINTER_', Q.PRINT_LOCATION) AND S.ACTIVE_FLAG = 'Y'
	LEFT	JOIN TB_M_SYSTEM V 
	ON		V.CODE				= Q.BARCODE_SIZE AND
			V.CATEGORY			= 'FAS_TYPE' and V.SUB_CATEGORY ='BARCODE_SIZE'
	LEFT	JOIN TB_M_SYSTEM P
	ON		P.CODE				= Q.PLATE_TYPE AND
			P.CATEGORY			= 'FAS_TYPE' and P.SUB_CATEGORY ='PLATE_TYPE'

	LEFT	JOIN TB_M_COST_CENTER cc 
	ON		Q.COMPANY			= CC.COMPANY AND 
			Q.COST_CODE			= cc.COST_CODE
    LEFT	JOIN  TB_M_EMPLOYEE em 
	ON		Q.CREATE_BY			= em.SYS_EMP_CODE
	
	GROUP	BY 
			Q.BARCODE_SIZE,
			V.[VALUE],
			Q.PLATE_TYPE,
			P.[VALUE],
			T.REMARKS,
			Q.PRINT_LOCATION,
			T.[VALUE],
			S.[VALUE]
	ORDER	BY 
			V.[VALUE],
			P.[VALUE],
			[LOCATION],
			PRINTER_NAME

	
	  -- Select Summary for Header
	SELECT	@TOTAL_ITEM			AS ITEM_COUNT,
			@RESP_COST_CENTER	AS RESP_COST_COUNT,
			@COST_CENTER		AS COST_COUNT  ,
			@REQUESTOR			AS CREATEDBY_COUNT

	  ---Select for print tag in dialog
	SELECT  pq.COMPANY,
			pq.COST_CODE, 
			pq.RESP_COST_CODE,
			pq.COST_NAME,
			pq.CREATE_BY, 
			pq.EMP_NAME,
	        pq.ASSET_NO,
			pq.ASSET_SUB,
			pq.BARCODE_SIZE,
			pq.PLATE_TYPE,
			pq.PRINT_LOCATION,
	        left(ah.[ASSET_NAME],50) AS ASSET_NAME,
			FORMAT(ah.[DATE_IN_SERVICE],'yyyyMMdd HH:mm:ss.ffffff') AS [DATE_IN_SERVICE],
			ah.[PRINT_COUNT],
			ah.BARCODE,
			T.VALUE AS LOCATION_NAME
	from	#T_Print_Detail pq 
	left	join TB_M_ASSETS_H ah 
	on		ah.ASSET_NO		= pq.ASSET_NO AND
			ah.COMPANY		= pq.COMPANY AND
			ah.ASSET_SUB	= pq.ASSET_SUB	
	left	join TB_M_SYSTEM T 
	on		pq.PRINT_LOCATION = T.CODE AND 
			T.CATEGORY = 'PRINT_CONFIG' and T.SUB_CATEGORY = 'PRINTER_LOCATION' and ACTIVE_FLAG = 'Y'
	order	by 
			pq.COST_CODE asc, 
			pq.ASSET_NO asc

			
			  
	  /* ==============================  Clear temp table ============================== */
	   IF OBJECT_ID('tempdb..#T_Print_Detail') IS NOT NULL
     BEGIN
		DROP TABLE #T_Print_Detail
	 END

	
END
GO
