DROP PROCEDURE [dbo].[sp_WFD01210_UpdateEmployeeHRMS_OUTOFDATE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 08/03/2017
-- Description:	Update Employee source HRMS
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_UpdateEmployeeHRMS_OUTOFDATE]
	 @EMP_CODE			varchar(8)
	,@TEL_NO			varchar(20)
	,@SIGNATURE_PATH	varchar(100)
	,@ROLE				varchar(3)
	,@SPEC_DIV			varchar(100)
	,@FAADMIN			varchar(1)
	,@DELEGATE_PIC		varchar(8)
	,@DELEGATE_FROM		date
	,@DELEGATE_TO		date
	,@USER_LOGIN        varchar(8)
AS
BEGIN
UPDATE [dbo].[TB_M_EMPLOYEE]
   SET
       [TEL_NO] = @TEL_NO
      ,[SIGNATURE_PATH] = @SIGNATURE_PATH
      ,[ROLE] = @ROLE
      ,[SPEC_DIV] = @SPEC_DIV
      ,[FAADMIN] = @FAADMIN
      ,[DELEGATE_PIC] = @DELEGATE_PIC
      ,[DELEGATE_FROM] = @DELEGATE_FROM
      ,[DELEGATE_TO] =@DELEGATE_TO
      ,[UPDATE_DATE] = GETDATE()
      ,[UPDATE_BY] = @USER_LOGIN
 WHERE 
	EMP_CODE = @EMP_CODE


	EXEC sp_Common_GenerateCostCenterMapping @EMP_CODE
	
	EXEC sp_Common_GenerateCostCenterRole @EMP_CODE


END



GO
