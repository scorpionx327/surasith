DROP PROCEDURE [dbo].[sp_WFD02410_GetTempDocDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD02410_GetTempDocDetail]
(
	@DOC_NO						VARCHAR(10)
)
	
AS
BEGIN
	SELECT 
	DOC_NO,							ASSET_NO,										BOI_ATTACH_DOC
	FROM TB_R_REQUEST_TRNF_D D WITH(NOLOCK)
	WHERE D.DOC_NO		 = @DOC_NO
	AND  (BOI_ATTACH_DOC LIKE 'TEMP%')


END






GO
