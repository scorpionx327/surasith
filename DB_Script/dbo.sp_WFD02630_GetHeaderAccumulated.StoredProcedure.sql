DROP PROCEDURE [dbo].[sp_WFD02630_GetHeaderAccumulated]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 22/02/2017
-- Description:	Get Date Header
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02630_GetHeaderAccumulated]
@year varchar(4),
@round varchar(2),
@asset_location varchar(1)

AS
BEGIN

select h.UPDATE_DATE,h.PLAN_STATUS from TB_R_STOCK_TAKE_H	h
where h.YEAR = @year and h.[ROUND] = @round
and h.ASSET_LOCATION=@asset_location
							

END



GO
