DROP PROCEDURE [dbo].[sp_WFD01250_InsertApproveD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 10-03-2017
-- Description:	Insert approve d data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_InsertApproveD]
	@APPRV_ID	int ,
	@INDX	int ,
	@ROLE	varchar(3) ,
	@APPRV_GROUP	varchar(8) ,
	@DIVISION	varchar(6) ,
	@REQUIRE_FLAG	varchar(8) ,
	@CONDITION_CODE	varchar(3) ,
	@OPERATOR	varchar(2) ,
	@VALUE1	varchar(20) ,
	@VALUE2	varchar(20) ,
	@FINISH_FLOW	varchar(1) ,
	@ALLOW_DEL_ITEM	varchar(1) ,
	@ALLOW_REJECT	varchar(1) ,
	@ALLOW_SEL_APPRV	varchar(1) ,
	@REJECT_TO	varchar(3) ,
	@HIGHER_APPR	varchar(1),
	@KPI_LEADTIME	INT =NULL,
	@GEN_FILE		VARCHAR(1),
	@NOTI_MODE		VARCHAR(1),
	@OP_MODE		VARCHAR(1) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    INSERT INTO [dbo].[TB_M_APPROVE_D]
           ([APPRV_ID]
           ,[INDX]
           ,[ROLE]
           ,[APPRV_GROUP]
           ,[DIVISION]
           ,[REQUIRE_FLAG]
           ,[CONDITION_CODE]
           ,[OPERATOR]
           ,[VALUE1]
           ,[VALUE2]
           ,[FINISH_FLOW]
           ,[ALLOW_DEL_ITEM]
           ,[ALLOW_REJECT]
           ,[ALLOW_SEL_APPRV]
           ,[REJECT_TO]
           ,[HIGHER_APPR]
		   ,GENERATE_FILE
		   ,LEAD_TIME
		   ,NOTI_BY_EMAIL
		   ,OPERATION
          )
     VALUES
           (@APPRV_ID
           ,@INDX
           ,@ROLE
           ,@APPRV_GROUP
           ,@DIVISION
           ,@REQUIRE_FLAG
           ,@CONDITION_CODE
           ,@OPERATOR
           ,@VALUE1
           ,@VALUE2
           ,@FINISH_FLOW
           ,@ALLOW_DEL_ITEM
           ,@ALLOW_REJECT
           ,@ALLOW_SEL_APPRV
           ,@REJECT_TO
           ,@HIGHER_APPR
		   ,@GEN_FILE
		   ,@KPI_LEADTIME
		   ,@NOTI_MODE
		   ,@OP_MODE
           )


END
GO
