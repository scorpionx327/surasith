DROP PROCEDURE [dbo].[sp_WFD02510_UpdateAssetApprove]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02510_UpdateAssetApprove]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN

	EXEC sp_WFD02510_DeleteAssetFlagY @DOC_NO, @USER

	UPDATE	R
	SET		R.RETIRE		=	T.RETIRE,			
			R.QTY			=	T.QTY,
			R.COST			=	T.COST,
			R.BOOK_VALUE	=	T.BOOK_VALUE,
			R.RETIREMENT_TYPE	= T.RETIREMENT_TYPE,
			R.ADJ_TYPE	= T.ADJ_TYPE,
			R.RETIRE_QTY	= T.RETIRE_QTY,
			R.RETIRE_COST	= T.RETIRE_COST,
			R.RETIRE_BOOK_VALUE	= T.RETIRE_BOOK_VALUE,
			R.ACCUM_DEPREC	=	T.ACCUM_DEPREC,
			R.USEFUL_LIFE	= T.USEFUL_LIFE,
			R.DISP_REASON	= T.DISP_REASON,
			R.DISP_REASON_OTHER	= T.DISP_REASON_OTHER,
			R.BOI_NO		= T.BOI_NO,
			R.BOI_CHECK_FLAG	= T.BOI_CHECK_FLAG,
			R.STORE_CHECK_FLAG	= T.STORE_CHECK_FLAG,
			R.SOLD_TO		= T.SOLD_TO,
			R.PRICE			= T.PRICE,
			R.INVOICE_NO	= T.INVOICE_NO,
			R.DETAIL_DATE	= T.DETAIL_DATE,
			R.DETAIL_REMARK	= T.DETAIL_REMARK,
			R.REMOVAL_COST	= T.REMOVAL_COST,
			R.USER_MISTAKE	= T.USER_MISTAKE,
			R.COMPENSATION	= T.COMPENSATION,
			R.RECEIVE_FLAG	= T.RECEIVE_FLAG
					
	FROM	TB_R_REQUEST_DISP_D R WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_DISP_D T WITH(NOLOCK)
	ON		R.COMPANY			= T.COMPANY AND
			R.ASSET_NO			= T.ASSET_NO AND
			R.ASSET_SUB			= T.ASSET_SUB
	WHERE	T.[GUID]			= @GUID

END
GO
