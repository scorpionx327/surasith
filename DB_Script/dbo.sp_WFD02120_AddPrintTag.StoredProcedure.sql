DROP PROCEDURE [dbo].[sp_WFD02120_AddPrintTag]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_WFD02120_AddPrintTag]
(	@COMPANY		T_COMPANY,
	@ASSET_NO		T_ASSET_NO,
	@ASSET_SUB		T_ASSET_SUB,
	@EMP_CODE		T_SYS_USER,																
	@PRINT_LOCATION  VARCHAR(8) = NULL	,
	
	-- TODO TEST	
	@PRINT_STATUS CHAR(1) = NULL,
	@MANUAL VARCHAR(6) = 'MANUAL',
	@FIRST_PRINT VARCHAR(11) = 'FIRST_PRINT'	,				
	@PRINT_COUNT INT = NULL
	
	)
AS
BEGIN TRY
BEGIN TRANSACTION T1
        --- Update Print Location 
		UPDATE	TB_M_EMPLOYEE 
		SET		PRINT_LOCATION =@PRINT_LOCATION
		WHERE	SYS_EMP_CODE=@EMP_CODE

     
		--- Delete old print q
		 --- Delete old print q
		 DELETE TB_R_PRINT_Q 
		 WHERE  COMPANY		= @COMPANY AND
				ASSET_NO	= @ASSET_NO AND 
				ASSET_SUB	= @ASSET_SUB

		   
		-- TODO TEST
		-- TEST NEW column by Pawares
		---     Print Queue insert q
		-- SELECT @DOC_NO = AssetHis.DOC_NO FROM TB_R_ASSET_HIS AssetHis WHERE ASSET_NO = @ASSET_NO

		DECLARE @DOC_NO	T_DOC_NO
		SELECT	TOP(1)@DOC_NO = (RePrt.DOC_NO) 
		FROM	TB_R_REQUEST_REPRINT RePRT 
		WHERE	COMPANY		= @COMPANY AND
				ASSET_NO	= @ASSET_NO AND 
				ASSET_SUB	= @ASSET_SUB
		ORDER	BY UPDATE_DATE DESC

		IF(@PRINT_STATUS = 'Y')
			BEGIN
				---     Print Queue insert q

				INSERT 
				INTO	TB_R_PRINT_Q  
				(		COMPANY,
						ASSET_NO,
						ASSET_SUB,
						COST_CODE,
						RESP_COST_CODE,
						BARCODE_SIZE,
						PLATE_TYPE,
						PRINT_LOCATION,
						CREATE_DATE,
						CREATE_BY,
						REMARK
				)
				SELECT	COMPANY,
						ASSET_NO,
						ASSET_SUB,
						COST_CODE,
						RESP_COST_CODE,
						BARCODE_SIZE,
						PLATE_TYPE,
						@PRINT_LOCATION,
						GETDATE(),
						@EMP_CODE,
						ISNULL(@DOC_NO, @MANUAL)
				FROM	TB_M_ASSETS_H
				WHERE	COMPANY		= @COMPANY AND
						ASSET_NO	= @ASSET_NO AND 
						ASSET_SUB	= @ASSET_SUB
			END
		ELSE 
		BEGIN
				SELECT	@PRINT_COUNT = AssetH.PRINT_COUNT 
				FROM	TB_M_ASSETS_H AssetH 
				WHERE	COMPANY		= @COMPANY AND
						ASSET_NO	= @ASSET_NO AND 
						ASSET_SUB	= @ASSET_SUB

				INSERT 
				INTO	TB_R_PRINT_Q  
				(		COMPANY,
						ASSET_NO,
						ASSET_SUB,
						COST_CODE,
						RESP_COST_CODE,
						BARCODE_SIZE,
						PLATE_TYPE,
						PRINT_LOCATION,
						CREATE_DATE,
						CREATE_BY,
						REMARK
				)
				SELECT	COMPANY,
						ASSET_NO,
						ASSET_SUB,
						COST_CODE,
						RESP_COST_CODE,
						BARCODE_SIZE,
						PLATE_TYPE,
						@PRINT_LOCATION,
						GETDATE(),
						@EMP_CODE,
						CASE WHEN @PRINT_COUNT > 0  THEN  ISNULL(@DOC_NO, @MANUAL)
							 ELSE @FIRST_PRINT END
				FROM	TB_M_ASSETS_H
				WHERE	COMPANY		= @COMPANY AND
						ASSET_NO	= @ASSET_NO AND 
						ASSET_SUB	= @ASSET_SUB
		END

	  --- Update Print Count and Status
		UPDATE	TB_M_ASSETS_H
		SET		PRINT_STATUS	= 'Y',
				PRINT_COUNT		= ISNULL(PRINT_COUNT,0) + 1,
				UPDATE_BY		= @EMP_CODE,
				UPDATE_DATE		= GETDATE()
		WHERE	COMPANY			= @COMPANY AND
				ASSET_NO		= @ASSET_NO AND 
				ASSET_SUB		= @ASSET_SUB AND
				PRINT_STATUS= 'N'

	--	MOVE TO PRINT TAG QUEUE SCREEN by Pawares M. 20180618	
		EXEC sp_WFD02110_UpdateAssetBarcode @COMPANY, @ASSET_NO, @ASSET_SUB

	COMMIT TRANSACTION T1
	 RETURN ;

END TRY
BEGIN CATCH
	 if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END

	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		SELECT -1;
		RETURN;
END CATCH
GO
