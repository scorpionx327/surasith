DROP PROCEDURE [dbo].[sp_LFD02170_GetRemark]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_GetRemark]
@COMPANY	T_COMPANY
AS
BEGIN	
	SET NOCOUNT ON;

		SELECT	CODE,VALUE
		FROM	TB_M_SYSTEM
		WHERE	CATEGORY = 'REGISTRATION' AND  SUB_CATEGORY = CONCAT('PRINT_MAP_',@COMPANY) AND  ACTIVE_FLAG = 'Y'
		ORDER	BY CODE 

END
GO
