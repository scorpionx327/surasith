DROP PROCEDURE [dbo].[sp_BFD01160_InsertCostCenterNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertCostCenterNotification]
@User	VARCHAR(8)
AS
BEGIN

	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'COST_CENTER_NO_SV'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	--------------------------------------------------------------------------------------------------------
	-- Every 25th, Get Next Period
	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	IF(@dNextTime > GETDATE())
		RETURN

	-- Sending to Admin
	
	SELECT	CC.COST_CODE, CC.COST_NAME, 'N' AS FLAG
	INTO	#TB_T_COST_CODE
	FROM	TB_M_COST_CENTER CC
	INNER	JOIN
	(
			-- Change spec : User request to send CC that assets is active and I/H only
			SELECT	COST_CODE
			FROM	TB_M_ASSETS_H H
			INNER	JOIN
					TB_M_ASSETS_D D
			ON		H.ASSET_NO = D.ASSET_NO
			WHERE	H.[STATUS]		= 'Y' AND
					H.INHOUSE_FLAG	= 'Y'
			GROUP	BY
					COST_CODE
	) A
	ON	CC.COST_CODE = A.COST_CODE
	LEFT	JOIN
			TB_M_SV_COST_CENTER SV
	ON		CC.COST_CODE = SV.COST_CODE
	WHERE	CC.[STATUS] = 'Y' AND
			CC.PARENT_FLAG = 'N' AND -- Add 2017-04-26 Surasith T. Remove Parent CC from notification
			SV.COST_CODE IS NULL

	IF @@ROWCOUNT = 0
		GOTO SETNEXT
	
	--------------------------------------------------------------------------------------------------------
	-- Sending a Email
	DECLARE @TB_T_EMP TABLE(EMP_CODE VARCHAR(8), SHORT_NAME VARCHAR(68), EMAIL VARCHAR(50))

	DELETE
	FROM	@TB_T_EMP

	-- Get Administrator
	INSERT
	INTO	@TB_T_EMP(EMP_CODE, SHORT_NAME, EMAIL)
	SELECT	E.EMP_CODE, dbo.fn_GetShortENName(E.EMP_CODE), E.EMAIL
	FROM	TB_M_EMPLOYEE E
	WHERE	E.ACTIVEFLAG	= 'Y' AND
			E.FAADMIN		= 'Y'
			
	DECLARE @BodyTemplate VARCHAR(400), @SubjectTemplate VARCHAR(400), 
			@CC VARCHAR(8), @CCName VARCHAR(50), @CCList VARCHAR(MAX)

	SET @SubjectTemplate	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD02160_COST_CENTER_NO_SV')
	SET @BodyTemplate		= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD02160_COST_CENTER_NO_SV')

	--While Loop CC

	WHILE(EXISTS(SELECT 1 FROM #TB_T_COST_CODE WHERE FLAG = 'N'))
	BEGIN

		SELECT	TOP 1 @CC = COST_CODE, @CCName = COST_NAME 
		FROM	#TB_T_COST_CODE WHERE FLAG = 'N'
		ORDER	BY COST_CODE

		UPDATE #TB_T_COST_CODE SET FLAG = 'Y' WHERE COST_CODE = @CC

		SET @CCList = CONCAT('<TR>','<TD>',@CC,'</TD>','<TD>',@CCName,'<TD>','</TR>')


	END --WHILE(EXISTS(SELECT 1 FROM #TB_T_COST_CODE WHERE FLAG = 'N'))



	INSERT
	INTO	TB_R_NOTIFICATION
	(		ID,
			[TO],
			CC,
			BCC,
			TITLE,
			[MESSAGE],
			FOOTER,
			[TYPE],
			START_PERIOD,
			END_PERIOD,
			SEND_FLAG,
			RESULT_FLAG,
			REF_FUNC,
			REF_DOC_NO,
			CREATE_DATE,
			CREATE_BY
	)

	SELECT	NEXT VALUE FOR NOTIFICATION_ID,
			T.EMAIL		AS [TO],
			NULL		AS CC,
			NULL		AS BCC,
			@SubjectTemplate AS TITLE,
			dbo.fn_StringFormat(@BodyTemplate, CONCAT(	T.SHORT_NAME,'|', @CCList)) AS [MESSAGE],
			NULL	AS FOOTER, -- Get From Default
			'I'		AS [TYPE],
			NULL	AS [START_PERIOD],
			NULL	AS [END_PERIOD],
			'N'		AS [SEND_FLAG],
			NULL	AS RESULT_FLAG,
			'BFD02160'	AS REF_FUNC,
			NULL	AS REF_DOC_NO,
			GETDATE(),
			@User
	FROM	@TB_T_EMP T
	

	--------------------------------------------------------------------------------------------------------
	-- Update Next Time
SETNEXT:	
	DECLARE @s VARCHAR(400)
	SET @s = dbo.fn_GetSystemMaster(@Category,@SubCategory,'EVERY_DATE')
	SET @dNextTime = DATEADD(DAY, CONVERT(INT, @s), @dNextTime)
	
	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @User
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
		
	IF OBJECT_ID('tempdb..#TB_T_COST_CODE') IS NOT NULL	
		DROP TABLE #TB_T_COST_CODE
	
END
GO
