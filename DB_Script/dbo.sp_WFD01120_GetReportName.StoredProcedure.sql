DROP PROCEDURE [dbo].[sp_WFD01120_GetReportName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_WFD01120_GetReportName]
(
@CATEGORY VARCHAR(40)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT CATEGORY, SUB_CATEGORY, CODE, VALUE, REMARKS, 
	ACTIVE_FLAG, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE
  FROM TB_M_SYSTEM WHERE UPPER(CATEGORY) = UPPER(@CATEGORY)  AND ACTIVE_FLAG = 'Y' 
  ORDER BY VALUE
END


--Created By   : Jatuporn Sornsiriwong
--Created Date : 3/2/2017 11.42
--Description  : Get Report Name when Report type change


GO
