DROP PROCEDURE [dbo].[sp_WFD01110_GetPhysicalCount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Surasith T.
-- Create date: 21/09/2019
-- Description:	get transfer tab in home page
/*
Nipon 2019-08-14 16:38:10.537
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetPhysicalCount]
	-- Add the parameters for the stored procedure here
	@COMPANY VARCHAR(50)	= null, -- not use T_COMPANY varchar so out of range.
	@EMP_CODE T_SYS_USER,
	@pageNum INT,
    @pageSize INT,
    @sortColumnName VARCHAR(50),
	@orderType VARCHAR(5),
	@TOTAL_ITEM int output
AS
BEGIN
	-- Prepare #Temp for keep emp code, delegate, role of login user
	CREATE TABLE #TB_T_SCOPE(EMP_CODE VARCHAR(25), COMPANY VARCHAR(10), SP_ROLE VARCHAR(3))
	EXEC sp_WFD01110_SetScope @EMP_CODE

	-- GetList of Document No

	SELECT	R.COMPANY, 
			R.DOC_NO, 
			CONCAT(R.EMP_TITLE, ' ', R.EMP_NAME, ' ', R.EMP_LASTNAME)   AS REQUESTOR,
			R.REQUEST_DATE,
			R.REQUEST_TYPE,
			dbo.fn_GetCurrentStatus(R.DOC_NO) CURRENT_STATUS,
			CONVERT(VARCHAR(4),'') [YEAR],
			0 AS [ROUND],
			0 AS ASSET_CNT,
			CONVERT(VARCHAR(1),'') ASSET_LOCATION,
			CONVERT(VARCHAR(MAX),'') COST_CODE
	INTO	#TB_REQUEST
	FROM	TB_R_REQUEST_H R
	INNER	JOIN 
			TB_R_REQUEST_APPR A
	ON		R.DOC_NO = a.DOC_NO
	WHERE	R.REQUEST_TYPE	= 'S' AND 
			dbo.fn_IsEndRequest(R.[STATUS]) = 'N' AND
			A.APPR_STATUS	= 'W' AND 
			( EXISTS(	SELECT	1 
						FROM	dbo.fn_GetMultipleCompanyList(@COMPANY) C 
						WHERE	C.COMPANY_CODE = r.COMPANY ) OR @COMPANY IS NULL ) AND
			-- Filter
			( EXISTS(	SELECT	1
						FROM	#TB_T_SCOPE T
						WHERE	T.EMP_CODE	= A.EMP_CODE ) OR
				EXISTS(	SELECT	1
						FROM	#TB_T_SCOPE T
						WHERE	T.COMPANY	= R.COMPANY AND T.SP_ROLE = A.APPR_ROLE AND A.NOTIFICATION_MODE = 'G' )
			) 

	-- Get Cost Center & Asset Count & Period
	UPDATE	T
	SET		T.[YEAR]			= TT.[YEAR],
			T.[ROUND]			= TT.[ROUND],
			T.ASSET_LOCATION	= TT.ASSET_LOCATION,
			T.COST_CODE			= TT.COST_CODE_LIST 
	FROM	#TB_REQUEST T
	INNER	JOIN
	(
		SELECT	M.DOC_NO,
				M.COMPANY,
				S.[YEAR], 
				S.[ROUND], 
				s.ASSET_LOCATION,
			   (	STUFF((SELECT CAST('#' + S.COST_CODE AS VARCHAR(MAX)) 
					FROM	TB_R_REQUEST_STOCK S 
					WHERE	(M.DOC_NO = S.DOC_NO) 
					FOR XML PATH ('')), 1, 2, '')
				) AS COST_CODE_LIST
		FROM	#TB_REQUEST M 
		INNER	JOIN
				TB_R_REQUEST_STOCK S ON M.DOC_NO = S.DOC_NO
		GROUP	BY  
				M.DOC_NO,
				M.COMPANY,
				S.[YEAR], 
				S.[ROUND], 
				s.ASSET_LOCATION
	) TT
	ON	T.DOC_NO = TT.DOC_NO
	UPDATE	T
	SET		T.ASSET_CNT	= TT.ASSET_CNT
	FROM	#TB_REQUEST T
	INNER	JOIN
	(
		SELECT	M.DOC_NO,
				COUNT(1) AS ASSET_CNT
		FROM	#TB_REQUEST M 
		INNER	JOIN
				TB_R_REQUEST_STOCK S ON M.DOC_NO = S.DOC_NO
		INNER	JOIN
				TB_R_STOCK_TAKE_D D
		ON		S.COMPANY			= D.COMPANY AND
				S.[YEAR]			= D.[YEAR] AND
				S.[ROUND]			= D.[ROUND] AND
				S.ASSET_LOCATION	= D.ASSET_LOCATION AND
				S.COST_CODE			= D.COST_CODE
		GROUP	BY
				M.DOC_NO
	) TT
	ON	T.DOC_NO = TT.DOC_NO


	SELECT	CASE WHEN @sortColumnName = '0' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 WHEN @sortColumnName = '1' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 
				 WHEN @sortColumnName = '2' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY [YEAR] asc)  
				 WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY [YEAR] desc)  
				 WHEN @sortColumnName = '3' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY [ROUND] asc)
				 WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY [ROUND] desc)
				 WHEN @sortColumnName = '4' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY [ASSET_LOCATION] asc)  
				 WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY [ASSET_LOCATION] desc)  
				 WHEN @sortColumnName = '5' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY ASSET_CNT asc)
				 WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_CNT desc)
				 

				 WHEN @sortColumnName = '6' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COST_CODE asc) 
				 WHEN @sortColumnName = '6' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CODE desc) 
				 WHEN @sortColumnName = '7' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR asc) 
				 WHEN @sortColumnName = '7' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR desc)  
				 WHEN @sortColumnName = '8' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS asc) 
				 WHEN @sortColumnName = '8' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS desc)  
				 WHEN @sortColumnName = '9' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE asc) 
				 WHEN @sortColumnName = '9' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE desc)  
				 ELSE ROW_NUMBER() OVER(ORDER BY DOC_NO ASC)  
			END AS ROWNUMBER,
			T.*
	INTO	#TB_T_RESULT 
	FROM	#TB_REQUEST T
	SET @TOTAL_ITEM = @@ROWCOUNT


	IF(@sortColumnName IS NULL )
	BEGIN
		SET @sortColumnName ='0';
	END
	IF(@orderType IS NULL )
	BEGIN
		SET @orderType ='asc';
	END

	DECLARE @FROM	INT , @TO	INT

	SET @FROM	= (@pageSize * (@pageNum-1)) + 1;
	SET @TO		= @pageSize * (@pageNum);						

	--begin modify by thanapon: fixed paging and sorting
	SELECT	d.ROWNUMBER,
			d.COMPANY, 
			d.DOC_NO, 
			d.REQUESTOR,		
			dbo.fn_dateFAS(REQUEST_DATE) AS REQUEST_DATE,
			d.CURRENT_STATUS,
			d.[YEAR],
			d.[ROUND],
			d.ASSET_CNT,
			d.ASSET_LOCATION,
			d.COST_CODE,
			d.REQUEST_TYPE
	FROM	#TB_T_RESULT d

	WHERE	d.ROWNUMBER >= @FROM and 
			d.ROWNUMBER <= @TO
	ORDER	BY 
			d.ROWNUMBER
	
	IF OBJECT_ID('tempdb..#TB_REQUEST') IS NOT NULL 
		DROP TABLE #TB_REQUEST 
	IF OBJECT_ID('tempdb..#TB_T_SCOPE') IS NOT NULL 
		DROP TABLE #TB_T_SCOPE 

	
END
GO
