DROP PROCEDURE [dbo].[sp_WFD02320_GetPrintDetail_KHUN_PLA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 07/02/2017
-- Description:	Print tag queue dialog
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02320_GetPrintDetail_KHUN_PLA]
	@COST_CODE Varchar(8) = NULL,
	@CREATE_BY Varchar(8) = NULL,
	@FIND Varchar(max) = NULL,
	@PrintAll Varchar(1)   --Y = PrintAll, N = PrintTag
AS
BEGIN


    DECLARE @TOTAL_ITEM int;
	DECLARE @COST_CENTER Varchar(300);
	DECLARE @REQUESTOR Varchar(300);
	DECLARE @CntCost INT
	DECLARE @CntRequest INT

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 /* ==============================  CREATE Temp TABLE ============================== */

	 IF OBJECT_ID('tempdb..#T_Print_Detail') IS NOT NULL
     BEGIN
		DROP TABLE #T_Print_Detail
	 END

	  select 
	   Q.COST_CODE, CC.COST_NAME, Q.CREATE_BY, CONCAT(em.EMP_TITLE , ' ', em.EMP_NAME , ' ' ,em.EMP_LASTNAME) AS EMP_NAME,
	   Q.ASSET_NO,Q.BARCODE_SIZE,Q.PRINT_LOCATION
	  INTO #T_Print_Detail
	  FROM [dbo].[TB_R_PRINT_Q] Q
	  left join TB_M_COST_CENTER cc on Q.COST_CODE = cc.COST_CODE
      left join  TB_M_EMPLOYEE em on Q.CREATE_BY = em.EMP_CODE
	  where (@PrintAll = 'N' and Q.COST_CODE = ISNULL(@COST_CODE,Q.COST_CODE) and Q.CREATE_BY = ISNULL(@CREATE_BY,Q.CREATE_BY))
	  or (@printAll = 'Y' and 
	      (Q.COST_CODE like ISNULL(@FIND,Q.COST_CODE ) or
    	  cc.COST_NAME like ISNULL(@FIND,cc.COST_NAME ) or
    	  Q.CREATE_BY like ISNULL(@FIND,Q.CREATE_BY ) or
    	  em.EMP_NAME like ISNULL(@FIND,em.EMP_NAME )))

	
	---- summary Header
	       select @TOTAL_ITEM = count(ASSET_NO)
	       FROM  #T_Print_Detail
	 
	      SELECT @CntCost=COUNT(distinct cost_code)		  
	       FROM  #T_Print_Detail
		   

		   IF( @CntCost>1)
		   BEGIN
		     SET @COST_CENTER = CONCAT(@CntCost , ' COST CENTER')
		   END
		   ELSE
		   BEGIN
		    SELECT TOP 1  @COST_CENTER = concat(COST_CODE, ' - ', COST_NAME)
            FROM  #T_Print_Detail
		   END
		   
           SELECT @CntRequest=COUNT(distinct CREATE_BY)		  
	       FROM  #T_Print_Detail
	      
		   IF( @CntRequest>1)
		   BEGIN
		     SET @REQUESTOR = CONCAT(@CntRequest, ' REQUESTOR')
		   END
		   ELSE
		    BEGIN
		      select TOP 1 @REQUESTOR = concat(CREATE_BY, ' - ', EMP_NAME)
		      FROM  #T_Print_Detail
			END


	select 
	  V.VALUE as BARCODE_SIZE
	 ,Q.PRINT_LOCATION
	 ,T.VALUE as LOCATION
	 ,S.VALUE as PRINTER_NAME
	 --,(Select [VALUE] from TB_M_SYSTEM,TB_R_PRINT_Q where TB_M_SYSTEM.CATEGORY = 'PRINT_CONFIG' and TB_M_SYSTEM.SUB_CATEGORY = 'PRINTER_LOCATION' and CODE = TB_R_PRINT_Q.PRINT_LOCATION) as LOCATION
	 --,(Select [VALUE] from TB_M_SYSTEM,TB_R_PRINT_Q where TB_M_SYSTEM.CATEGORY = 'PRINT_CONFIG' and TB_M_SYSTEM.SUB_CATEGORY = Concat('PRINTER_', TB_R_PRINT_Q.PRINT_LOCATION) and TB_M_SYSTEM.CODE = TB_R_PRINT_Q.BARCODE_SIZE and TB_M_SYSTEM.REMARKS = 1 ) as PRINTER_NAME
	 ,COUNT(ASSET_NO) as ITEM_QTY
	  FROM [dbo].[TB_R_PRINT_Q] Q
	  left join TB_M_SYSTEM T on Q.PRINT_LOCATION = T.CODE AND T.CATEGORY = 'PRINT_CONFIG' and T.SUB_CATEGORY = 'PRINTER_LOCATION' and ACTIVE_FLAG = 'Y'
	  left join TB_M_SYSTEM S on S.CATEGORY = 'PRINT_CONFIG' and S.SUB_CATEGORY = Concat('PRINTER_', Q.PRINT_LOCATION) and S.CODE = Q.BARCODE_SIZE and S.REMARKS = 1  and S.ACTIVE_FLAG = 'Y' 
	  left join TB_M_SYSTEM V on V.CATEGORY = 'FAS_TYPE' and V.SUB_CATEGORY ='BARCODE_SIZE' and V.CODE = Q.BARCODE_SIZE
	  left join TB_M_COST_CENTER cc on Q.COST_CODE = cc.COST_CODE
      left join  TB_M_EMPLOYEE em on Q.CREATE_BY = em.EMP_CODE
	  where (@PrintAll = 'N' and Q.COST_CODE = ISNULL(@COST_CODE,Q.COST_CODE) and Q.CREATE_BY = ISNULL(@CREATE_BY,Q.CREATE_BY))
	  or (@printAll = 'Y' and 
	      (Q.COST_CODE like ISNULL(@FIND,Q.COST_CODE ) or
    	  cc.COST_NAME like ISNULL(@FIND,cc.COST_NAME ) or
    	  Q.CREATE_BY like ISNULL(@FIND,Q.CREATE_BY ) or
    	  em.EMP_NAME like ISNULL(@FIND,em.EMP_NAME )))

	 -- Q.COST_CODE in(SELECT Value FROM fn_Split(@COST_CODE, ',')) and Q.CREATE_BY in(SELECT Value FROM fn_Split(@CREATE_BY, ','))
	  group by V.VALUE,Q.PRINT_LOCATION,T.VALUE,S.VALUE
	  order by V.VALUE,LOCATION,PRINTER_NAME


	  -- Select Summary for Header
	  select @TOTAL_ITEM,@COST_CENTER,@REQUESTOR

	  ---Select for print tag in dialog
	  select * from #T_Print_Detail order by COST_CODE, ASSET_NO
	
	  
	  /* ==============================  Clear temp table ============================== */
	   IF OBJECT_ID('tempdb..#T_Print_Detail') IS NOT NULL
     BEGIN
		DROP TABLE #T_Print_Detail
	 END

	
END



GO
