DROP PROCEDURE [dbo].[sp_WFD01170_GetRequestor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetRequestor NULL, '0001'    
CREATE PROCEDURE [dbo].[sp_WFD01170_GetRequestor]
(
	@DOC_NO				T_DOC_NO = NULL,
	@EMP_CODE			T_SYS_USER
)
	
AS
BEGIN		
	--DECLARE @STATUS			VARCHAR(2)
	DECLARE @STATUS_DOC		VARCHAR(100)

	SET @STATUS_DOC = dbo.fn_GetCurrentStatus(@DOC_NO)

	-- New Requeset
	IF ISNULL(@DOC_NO,'') = ''
	BEGIN
		SELECT 
		'' DOC_NO,				
		DBO.fn_dateFAS(GETDATE()) REQUEST_DATE,						
		'' REQUEST_TYPE,
		EMP_TITLE,				
		SYS_EMP_CODE,
		EMP_CODE,													
		CONCAT(EMP_TITLE, ' ',EMP_NAME, ' ',EMP_LASTNAME) EMP_NAME, 
		EMP_LASTNAME,			
		E.COST_CODE,												
		E.COST_CODE + '-' + C.COST_NAME COST_NAME,
		POST_CODE POS_CODE, 	
		CONCAT(POST_CODE, ':', POST_NAME) POS_NAME,	O.DEPT_CODE,										
		O.DEPT_NAME,			
		O.SECTION_CODE SECT_CODE,										
		O.SECTION_NAME SECT_NAME,											
		E.TEL_NO PHONE_NO,		
		@STATUS_DOC [STATUS],--'Wait for submit ' STATUS
		NULL UPDATE_DATE
		FROM	TB_M_EMPLOYEE E
		INNER	JOIN TB_M_ORGANIZATION O ON E.ORG_CODE	= O.ORG_CODE
		INNER	JOIN TB_M_COST_CENTER C ON E.COST_CODE	= C.COST_CODE 
		WHERE	SYS_EMP_CODE = @EMP_CODE
	END
	ELSE
	BEGIN
		
		SELECT TOP 1 @STATUS_DOC = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','DOC_STATUS',H.[STATUS]) 
		FROM	TB_R_REQUEST_H H WITH (NOLOCK)
		WHERE	H.DOC_NO			= @DOC_NO 
				-- H.STATUS		NOT IN ('00','10','70')



		SELECT	H.COMPANY,
				H.DOC_NO,					
				DBO.fn_dateFAS(REQUEST_DATE) REQUEST_DATE,					
				REQUEST_TYPE, 
				EMP_CODE SYS_EMP_CODE,
				REQUEST_ROLE,
				EMP_TITLE,				
				EMP_CODE,													
				CONCAT(EMP_TITLE, ' ',EMP_NAME, ' ',EMP_LASTNAME) EMP_NAME, 
				EMP_LASTNAME,			
				H.COST_CODE,												
				CONCAT(H.COST_CODE, '-', C.COST_NAME) COST_NAME,
				POS_CODE,			    
				CONCAT(POS_CODE, ':', POS_NAME)  POS_NAME,	DEPT_CODE,											
				DEPT_NAME, 				
				SECT_CODE,													
				SECT_NAME,											
				PHONE_NO,				
				ISNULL(@STATUS_DOC,H.[STATUS]) STATUS,
				FORMAT(H.UPDATE_DATE,'yyyy-MM-dd HH:MM:ss.fff') UPDATE_DATE
		FROM	TB_R_REQUEST_H H WITH (NOLOCK)
		LEFT	JOIN TB_M_COST_CENTER C 
		ON		H.COST_CODE		= C.COST_CODE 		
		WHERE	H.DOC_NO = @DOC_NO

	END

END
GO
