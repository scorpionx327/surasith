/****** Object:  UserDefinedFunction [dbo].[fn_ConvertStringToDateTime]    Script Date: 2019/10/14 8:49:54 PM ******/
DROP FUNCTION [dbo].[fn_ConvertStringToDateTime]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ConvertStringToDateTime]    Script Date: 2019/10/14 8:49:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ConvertStringToDateTime](@str VARCHAR(19))
returns datetime
as
begin
	return  CONVERT(DATETIME,@str,104) 
end

GO
