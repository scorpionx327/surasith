DROP PROCEDURE [dbo].[sp_WFD02620_EditStockTakeRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 15-02-2017
-- Description:	Edit stock take request process data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_EditStockTakeRequest]
	-- Add the parameters for the stored procedure here
	 @CREATE_BY				T_USER
	, @COMPANY				T_COMPANY
	, @YEAR					varchar(4)
	, @ROUND				varchar(2)
	, @ASSET_LOCATION		varchar(1)
	, @DATA_AS_OF			date
	, @TARGET_DATE_FROM		date
	, @TARGET_DATE_TO		date
	, @QTY_OF_HANDHELD		int
	, @BREAK_TIME_MINUTE	int
	, @ASSET_STATUS			nvarchar(2)		
	, @UPDATE_DATE			datetime
	, @STOCK_TAKE_KEY		varchar(7)
	
	, @ASSET_CLASS			T_ASSET_CLASS
	, @MINOR_CATEGORY		T_MINOR_CATEGORY
	, @COST_CENTER			varchar(200)
	, @RESPONSIBLE_COST_CENTER	varchar(200)
	, @BOI					varchar(8)
	, @INVENTORY_CHECK		varchar(1)

AS
	DECLARE	@TransactionName	nvarchar(25) = 'addstocktaketransaction'

	
	DECLARE @tb_Holidays TABLE (
		DT	DATE
	);

	DECLARE @MESSAGE NVARCHAR(250);

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN @TransactionName
	BEGIN TRY


		-- ///////////////////////////////////////////////////////////////////////		
		-- Validate add data

		--In case of Concurrent Checking, 
						

		IF(SELECT COUNT(1) FROM TB_R_STOCK_TAKE_H WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY and UPDATE_DATE <> @UPDATE_DATE) > 0
		BEGIN


			SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'

			SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
			
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;

		END

		 -- Generate holiday list
		-- Generate holiday list
		DECLARE @DT DATE;
		SET @DT=@TARGET_DATE_FROM;
		WHILE @DT<=@TARGET_DATE_TO
		BEGIN

			IF(DATEPART(WEEKDAY, @DT) in (1,7))
			BEGIN
				INSERT INTO @tb_Holidays select @DT as DT
			END

			SET @DT=DATEADD(day,1,@DT)

		END

		-- ///////////////////////////////////////////////////////////////////////////
		-- Remove detail data before insert

		DELETE FROM TB_R_STOCK_TAKE_D WHERE COMPANY =@COMPANY and YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION

		DELETE FROM TB_R_STOCK_TAKE_D_PER_SV WHERE COMPANY =@COMPANY and YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION

		DELETE FROM TB_R_STOCK_TAKE_HOLIDAY WHERE COMPANY =@COMPANY and STOCK_TAKE_KEY=@STOCK_TAKE_KEY
		

		-- ///////////////////////////////////////////////////////////////////////////
		-- Insert Data
		 UPDATE StockH
		 SET  stockH.DATA_AS_OF = @DATA_AS_OF,
		      StockH.[TARGET_DATE_FROM] = @TARGET_DATE_FROM ,
			  StockH.[TARGET_DATE_TO] = @TARGET_DATE_TO,
			  StockH.[QTY_OF_HANDHELD] = @QTY_OF_HANDHELD , 
			  StockH.[BREAK_TIME_MINUTE]  = @BREAK_TIME_MINUTE , 
			  StockH.ASSET_STATUS = @ASSET_STATUS,

			  stockH.ASSET_CLASS = @ASSET_CLASS,
			  stockH.MINOR_CATEGORY = @MINOR_CATEGORY,
			  stockH.COST_CENTER = @COST_CENTER,
			  stockH.RESPONSE_CC = @RESPONSIBLE_COST_CENTER,
			  stockH.INVEST_REASON = @BOI,
			  stockH.INVEN_INDICATOR = @INVENTORY_CHECK,

			  StockH.[PLAN_STATUS]  = 'D' , 
			  StockH.UPDATE_BY  = @CREATE_BY , 
			  StockH.UPDATE_DATE = GETDATE()
		 FROM TB_R_STOCK_TAKE_H StockH 
		 WHERE StockH.STOCK_TAKE_KEY =CONCAT(@COMPANY,@YEAR,@ROUND,@ASSET_LOCATION)

	--- Insert Detail table 
	 IF (@ASSET_LOCATION='I') -- Insert Case
	BEGIN
	  exec sp_WFD02620_Insert_Update_IncaseInHouse @CREATE_BY, @YEAR ,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_STATUS,@MINOR_CATEGORY
	END
	 IF (@ASSET_LOCATION='O') -- Insert Case
	BEGIN
	  exec sp_WFD02620_Insert_Update_IncaseOutSource @CREATE_BY, @YEAR ,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_STATUS,@MINOR_CATEGORY
	END		

		-- TB_R_STOCK_TAKE_HOLIDAY
		INSERT INTO TB_R_STOCK_TAKE_HOLIDAY
		SELECT  @STOCK_TAKE_KEY as STOCK_TAKE_KEY, DT as HOLIDATE_DATE, @COMPANY as COMPANY FROM @tb_Holidays

		IF( SELECT COUNT(1) FROM TB_R_STOCK_TAKE_D WHERE STOCK_TAKE_KEY =CONCAT(@COMPANY,@YEAR,@ROUND,@ASSET_LOCATION) ) <= 0
		BEGIN		
			

			SET @MESSAGE = '[CUSTOM]MSTD0000AERR|There is no asset data for stock taking'
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );

			return;			
		END

		COMMIT;

		select @STOCK_TAKE_KEY as STOCK_TAKE_KEY

	END TRY
	BEGIN CATCH

		

		ROLLBACK;

		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			   @ErrorSeverity = ERROR_SEVERITY(),
			   @ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );

	END CATCH




END





GO
