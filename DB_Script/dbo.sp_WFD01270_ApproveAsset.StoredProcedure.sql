DROP PROCEDURE [dbo].[sp_WFD01270_ApproveAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_ApproveAsset]
(	
	@DOC_NO			VARCHAR(10)			= NULL,	
	@ASSET_NO		VARCHAR(15)			= NULL,
	@COST_CODE		VARCHAR(8)			= NULL,
	@STATUS			VARCHAR(2),
	@PROC_STATUS	VARCHAR(2),
	@NEW_STATUS		VARCHAR(2),
	@Desc			VARCHAR(255),
	@APPROVE_BY		VARCHAR(50)			= NULL
)
AS
BEGIN
	--=============== UPDATE TB_M_ASSETS_H ===============--
	DECLARE	@BOI_DICISION	VARCHAR(1)			= NULL
	DECLARE	@BOI_ATTACH		VARCHAR(200)		= NULL
	DECLARE @PURPOSE		VARCHAR(40)			= NULL
	

	DECLARE @FullyParentRetired VARCHAR(1) = 'N', @RecvToTent VARCHAR(1) = 'N', @FullyRetired VARCHAR(1) = 'N'
	
	IF @PROC_STATUS = 'T' AND @STATUS = '60' -- For Transfer
	BEGIN
		SELECT	@BOI_DICISION = BOI_CHECK_FLAG,
				@BOI_ATTACH = BOI_ATTACH_DOC
		FROM	TB_R_REQUEST_TRNF_D
		WHERE	DOC_NO		= @DOC_NO AND 
				ASSET_NO	= @ASSET_NO
	END

	IF @PROC_STATUS = 'D' -- Fixed 'D' From Main Proc
	BEGIN
		SELECT	@BOI_DICISION	= CASE WHEN @STATUS = '60' THEN BOI_CHECK_FLAG ELSE NULL END,
				@BOI_ATTACH		= CASE WHEN @STATUS = '60' THEN BOI_ATTACH_DOC ELSE NULL END,
				@PURPOSE		= CASE WHEN @STATUS = '70' THEN PURPOSE ELSE NULL END,
				-- Surasith T. support dispose Parent & Child
				@FullyParentRetired = CASE	WHEN  dbo.fn_IsFullRetired(ORIGINAL_QTY, QTY, COST, COST_DISP,  RETIRE) = 'Y' AND RIGHT(ASSET_NO,2) = '00' 
											THEN 'Y' ELSE 'N' END,
				@FullyRetired = dbo.fn_IsFullRetired(ORIGINAL_QTY, QTY, COST, COST_DISP,  RETIRE),
				@RecvToTent		= CASE WHEN @STATUS = '70' THEN RECEIVE_FLAG ELSE '-' END -- Support FA Receive to TENT and Status change to Inactive
		FROM	TB_R_REQUEST_DISP_D
		WHERE	DOC_NO			= @DOC_NO AND
				ASSET_NO		= @ASSET_NO
	END
	
	UPDATE TB_M_ASSETS_H
	SET		PROCESS_STATUS		= @NEW_STATUS,
 			UPDATE_DATE			= GETDATE(),
			UPDATE_BY			= @APPROVE_BY  
	WHERE	(	ASSET_NO			= @ASSET_NO OR --Support Fully Y (In Current)
				(PROCESS_STATUS = 'T' AND LEFT(ASSET_NO,13)	= LEFT(@ASSET_NO,13)) OR -- Parent & Child
				-- Surasith T. support dispose Parent & Child
				(PROCESS_STATUS IN ('D') AND (@FullyParentRetired = 'Y' AND LEFT(ASSET_NO,13)	= LEFT(@ASSET_NO,13)) )
			)	AND	(
				PROCESS_STATUS		= @PROC_STATUS	OR
				PROCESS_STATUS		= CONCAT(@PROC_STATUS,'C') -- Add for FA reject after status = 60
			)
	
	UPDATE TB_M_ASSETS_H
	SET		[STATUS]			=	CASE	WHEN @RecvToTent = 'Y' THEN 'N' -- FA change from Blank to Tent
											WHEN @RecvToTent = 'N' THEN 'Y' -- FA cancel Tent
											ELSE [STATUS] END,
 			UPDATE_DATE			= GETDATE(),
			UPDATE_BY			= @APPROVE_BY  
	WHERE	PROCESS_STATUS = 'DC' AND @STATUS = '70' AND
			(
				( @FullyParentRetired = 'Y' AND LEFT(ASSET_NO,13)	= LEFT(@ASSET_NO,13) )OR 
				( @FullyParentRetired = 'N' AND ASSET_NO = @ASSET_NO )
			) AND @PROC_STATUS = 'D' AND @STATUS = '70' AND @FullyRetired = 'Y'
			
			

	IF @STATUS = '60' -- Update only 1 time (Approved)
		BEGIN
		UPDATE TB_M_ASSETS_H
		SET		-- BOI will update only when status 60 Refer Document from new request Surasith T. 2017-04-27
				TRNF_BOI_DECISION	= ISNULL(@BOI_DICISION, TRNF_BOI_DECISION),			--CASE WHEN @NEW_STATUS = 'TC' THEN @BOI_DICISION ELSE TRNF_BOI_DECISION END,  -- 
				TRNF_BOI_ATTACH		= ISNULL(@BOI_ATTACH, TRNF_BOI_ATTACH),				--CASE WHEN @NEW_STATUS = 'TC' THEN @BOI_ATTACH   ELSE TRNF_BOI_ATTACH END,  --
				DISP_BOI_DECISION	= ISNULL(@BOI_DICISION, DISP_BOI_DECISION),			--CASE WHEN @NEW_STATUS = 'DC' THEN @BOI_DICISION ELSE DISP_BOI_DECISION END,--
				DISP_BOI_ATTACH		= ISNULL(@BOI_ATTACH, DISP_BOI_ATTACH),				--CASE WHEN @NEW_STATUS = 'DC' THEN @BOI_ATTACH   ELSE DISP_BOI_ATTACH END,--
				BOI_UPDATE_DATE		= CASE	WHEN @BOI_ATTACH IS NULL AND (TRNF_BOI_ATTACH IS NOT NULL OR DISP_BOI_DECISION IS NOT NULL) 
											THEN GETDATE()
											WHEN @BOI_ATTACH IS NOT NULL AND (TRNF_BOI_ATTACH IS NULL OR DISP_BOI_DECISION IS NULL) 
											THEN GETDATE()
											ELSE BOI_UPDATE_DATE END,
				UPDATE_DATE			= GETDATE(),
				UPDATE_BY			= @APPROVE_BY  
		WHERE	ASSET_NO			= @ASSET_NO AND
				PROCESS_STATUS		= @NEW_STATUS
				-- No need to update boi incase fully
	END
			 --@PROC_STATUS Modify by Surasith T. 2017-04-27 	
			
	--=============== UPDATE TB_R_ASSET_HIS ===============--
	DECLARE @sName VARCHAR(68) 
	SET @sName = dbo.fn_GetShortENName(@APPROVE_BY)
	
	DECLARE @FA_FLAG		VARCHAR(1)			= NULL
	-- Case status = 70 : Ready to send : Approve by FA 
	-- In case dispose, set FA Verify code when that FA input purpose info.
	IF @STATUS = '70' AND @PROC_STATUS = 'D' AND @PURPOSE IS NOT NULL
		SET @FA_FLAG = '1'
	-- Other case, set FA Verify code after have FA click verify
	IF @STATUS = '70' AND @PROC_STATUS <>'D'
		SET @FA_FLAG = '1'

	UPDATE	R			
	SET		TRANS_DATE				= GETDATE(),
			DETAIL					= @Desc,
			-- Case status = 60 : Completed Approval : Approve by Finish Flow Approver
			LATEST_APPROVE_CODE		= CASE WHEN @STATUS = '60' THEN @APPROVE_BY ELSE LATEST_APPROVE_CODE END,
			LATEST_APPROVE_NAME		= CASE WHEN @STATUS = '60' THEN @sName ELSE LATEST_APPROVE_NAME END,

			ORACLE_UPDATE_STATUS	= CASE WHEN @STATUS = '60' THEN 'N' ELSE ORACLE_UPDATE_STATUS END,

			-- Case status = 70 : Update FA Verify Code & Date
			FA_VERIFY_CODE			= CASE WHEN @FA_FLAG IS NOT NULL AND ORACLE_SENT_DATE IS NULL THEN @APPROVE_BY ELSE FA_VERIFY_CODE END,
			FA_VERIFY_DATE			= CASE WHEN @FA_FLAG IS NOT NULL AND ORACLE_SENT_DATE IS NULL THEN GETDATE() ELSE FA_VERIFY_DATE END,
			[STATUS]					= @Status,		
			UPDATE_DATE				= GETDATE(),
			UPDATE_BY				= @APPROVE_BY
	FROM	TB_R_ASSET_HIS R
	WHERE	(	R.ASSET_NO				= @ASSET_NO OR 
				(@PROC_STATUS = 'T' AND LEFT(ASSET_NO,13)	= LEFT(@ASSET_NO,13)) OR
				-- Surasith T. support dispose Parent & Child
				(@PROC_STATUS = 'D' AND (@FullyParentRetired = 'Y' AND LEFT(ASSET_NO,13)	= LEFT(@ASSET_NO,13)) )
			) AND
			R.COST_CODE				= @COST_CODE AND
			R.DOC_NO				= @DOC_NO

END



GO
