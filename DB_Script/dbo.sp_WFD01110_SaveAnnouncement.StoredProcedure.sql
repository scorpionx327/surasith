DROP PROCEDURE [dbo].[sp_WFD01110_SaveAnnouncement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 20/02/2017
-- Description:	Save Announcement
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_SaveAnnouncement]
	 @COMPANY					 T_COMPANY
	,@ANNOUNCE_DESC				 varchar(8000)
	,@ANNOUNCE_URL				 varchar(500) 
	,@DISPLAY_FLAG				 varchar(1)
	,@ANNOUNCE_TYPE				 varchar(1)
	,@CREATE_BY				     varchar(8)
AS
BEGIN
		UPDATE [dbo].[TB_R_ANNOUNCEMENT]
		SET [DISPLAY_FLAG] = 'N'
		WHERE COMPANY=@COMPANY

		INSERT INTO [dbo].[TB_R_ANNOUNCEMENT]
				([ANNOUNCE_ID]
				,[COMPANY]
				,[ANNOUNCE_DESC]
				,[ANNOUNCE_URL]
				,[DISPLAY_FLAG]
				,[ANNOUNCE_TYPE]
				,[CREATE_DATE]
				,[CREATE_BY])
			VALUES
				(
				NEXT VALUE FOR ANNOUNCE_ID
				,@COMPANY
				,@ANNOUNCE_DESC 
				,@ANNOUNCE_URL
				,@DISPLAY_FLAG
				,@ANNOUNCE_TYPE
				,GETDATE()
				,@CREATE_BY)

END



GO
