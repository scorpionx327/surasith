DROP PROCEDURE [dbo].[sp_BFD02670_UpdateStockTake_D_b]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 07-03-2017
-- Description:	Update stock take d data
-- =============================================
CREATE PROCEDURE [dbo].[sp_BFD02670_UpdateStockTake_D_b]
	@STOCK_TAKE_KEY varchar(7) 
	,@ASSET_NO varchar(15) 
	,@EMP_CODE varchar(8) 
	,@CHECK_STATUS varchar(19) 
	,@SCAN_DATE datetime 
	,@START_COUNT_TIME datetime 
	,@END_COUNT_TIME datetime 
	,@COUNT_TIME int 

AS
BEGIN

	UPDATE TB_R_STOCK_TAKE_D 
		SET EMP_CODE = CASE CHECK_STATUS
							WHEN 'N' THEN @EMP_CODE
							ELSE EMP_CODE
						END
			, CHECK_STATUS = CASE CHECK_STATUS
								WHEN 'N' THEN @CHECK_STATUS
								ELSE CHECK_STATUS
							END
			, SCAN_DATE=@SCAN_DATE
			, START_COUNT_TIME=@START_COUNT_TIME
			, END_COUNT_TIME=@END_COUNT_TIME
			, COUNT_TIME=@COUNT_TIME
			, UPDATE_BY=@EMP_CODE
			, UPDATE_DATE=GETDATE()
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY and ASSET_NO=@ASSET_NO

END




GO
