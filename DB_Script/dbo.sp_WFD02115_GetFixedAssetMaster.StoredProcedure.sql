DROP PROCEDURE [dbo].[sp_WFD02115_GetFixedAssetMaster]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02115_GetFixedAssetMaster]
@COMPANY	T_COMPANY,
@ASSET_NO	T_ASSET_NO,
@ASSET_SUB	T_ASSET_SUB
AS
BEGIN
	SELECT		M.COMPANY,				
				M.ASSET_CLASS,			ASSET_NO,			ASSET_SUB,			ASSET_NAME AS ASSET_DESC,
				CASE WHEN ASSET_SUB = '0000' THEN 'M' ELSE 'S' END AS ASSET_MAINSUB,
				ADTL_ASSET_NAME AS ADTL_ASSET_DESC,		
										SERIAL_NO,			INVEN_NO,			BASE_UOM,				LAST_INVEN_DATE,
				INVEN_INDICATOR,		INVEN_NOTE,			M.COST_CODE,		RESP_COST_CODE,			[LOCATION],
				ROOM,					LICENSE_PLATE,		WBS_PROJECT,		INVEST_REASON,			MINOR_CATEGORY,
				ASSET_STATUS,			MACHINE_LICENSE,	BOI_NO,				FINAL_ASSET_CLASS,		MANUFACT_ASSET,
				NULL ASSET_TYPE_NAME,	WBS_BUDGET,			PLATE_TYPE,			BARCODE_SIZE,
				LEASE_AGREE_DATE,		LEASE_START_DATE,	LEASE_LENGTH_YEAR,	LEASE_LENGTH_PERD,		LEASE_TYPE,
				LEASE_SUPPLEMENT,		LEASE_NUM_PAYMENT,	LEASE_PAY_CYCLE,	LEASE_ADV_PAYMENT,		LEASE_PAYMENT,
				LEASE_ANU_INT_RATE,
				DPRE_AREA_01,			DPRE_KEY_01,		USEFUL_LIFE_YEAR_01 AS PLAN_LIFE_YEAR_01,	
															USEFUL_LIFE_PERD_01 AS PLAN_LIFE_PERD_01,		SCRAP_VALUE_01,
				DPRE_AREA_15,			DPRE_KEY_15,		USEFUL_LIFE_YEAR_15,	
															USEFUL_LIFE_PERD_01 AS PLAN_LIFE_PERD_15,		SCRAP_VALUE_15,

				DPRE_AREA_31,			DPRE_KEY_31,		USEFUL_LIFE_YEAR_31,	
															USEFUL_LIFE_PERD_31 AS PLAN_LIFE_PERD_31,		SCRAP_VALUE_31,

				DPRE_AREA_41,			DPRE_KEY_41,		USEFUL_LIFE_YEAR_41,	
															USEFUL_LIFE_PERD_41 AS PLAN_LIFE_PERD_41,		SCRAP_VALUE_41,

				DPRE_AREA_81,			DPRE_KEY_81,		USEFUL_LIFE_YEAR_81,	
															USEFUL_LIFE_PERD_81 AS PLAN_LIFE_PERD_81,		SCRAP_VALUE_81,

				DPRE_AREA_91,			DPRE_KEY_91,		USEFUL_LIFE_YEAR_91,	
															USEFUL_LIFE_PERD_91 AS PLAN_LIFE_PERD_91,		SCRAP_VALUE_91,
				CUMU_ACQU_PRDCOST_01,									
				CC.COST_NAME, 
				RC.COST_NAME RESP_COST_NAME,
				CASE WHEN VW.ASSET_CLASS IS NULL THEN 'RMA' ELSE 'AUC' END AS ASSET_GROUP	
	FROM	TB_M_ASSETS_H M
	LEFT	JOIN
			TB_M_COST_CENTER CC
	ON		M.COMPANY	= CC.COMPANY AND M.COST_CODE = CC.COST_CODE
	LEFT	JOIN
			TB_M_COST_CENTER RC
	ON		M.COMPANY	= RC.COMPANY AND M.RESP_COST_CODE = RC.COST_CODE
	LEFT	JOIN
			VW_AUC_ASSET_CLASS VW
	ON		M.COMPANY	= VW.COMPANY AND M.ASSET_CLASS = VW.ASSET_CLASS
	WHERE	M.COMPANY		= @COMPANY AND
			M.ASSET_NO	= @ASSET_NO AND
			M.ASSET_SUB	= @ASSET_SUB

END
GO
