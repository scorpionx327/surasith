DROP PROCEDURE [dbo].[sp_Common_GetCostCenter_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetCostCenter_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER
AS
BEGIN
		SELECT	COST_CODE,
				COST_NAME,
				COST_CENTER_DESC,
				DEPARTMENT
		FROM	TB_M_COST_CENTER 
		WHERE	(	COST_CODE LIKE CONCAT(@keyword,'%') OR
					COST_NAME LIKE CONCAT(@keyword,'%') 
				) AND
				[STATUS] = 'Y'
END
GO
