DROP PROCEDURE [dbo].[sp_WFD02710_InsertAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02710_InsertAsset]
(	
	@COMPANY	T_COMPANY,
	@ASSET_NO	T_ASSET_NO,
	@ASSET_SUB	T_ASSET_SUB,
	@GUID		T_GUID,
	@USER		T_SYS_USER
)
AS
BEGIN TRY 
	-- Insert into temp
	DELETE
	FROM	TB_T_SELECTED_ASSETS
	WHERE	COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			[ASSET_SUB] = @ASSET_SUB AND
			[GUID]		= @GUID

	INSERT
	INTO	TB_T_SELECTED_ASSETS
	(		[GUID],
			COMPANY,
			ASSET_NO,
			ASSET_SUB,
			ITEM_NO,
			COST_CODE,
			ASSET_CLASS,
			CREATE_DATE,
			CREATE_BY
			)
	SELECT	@GUID,
			@COMPANY,
			@ASSET_NO,
			@ASSET_SUB,
			1,
			COST_CODE,
			ASSET_CLASS,
			GETDATE(),
			@USER
	FROM	TB_M_ASSETS_H
	WHERE	COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB
	

	DECLARE @Latest	INT
	SELECT @Latest = MAX(SEQ_NO) FROM TB_T_REQUEST_SETTLE_H WHERE GUID = @GUID
	SET @Latest = ISNULL(@Latest,0) 
	INSERT
	INTO	TB_T_REQUEST_SETTLE_H
	(
			COMPANY,			DOC_NO,			SEQ_NO,			
			AUC_NO,				AUC_SUB,		AUC_COST_CODE,	AUC_RESP_COST_CODE,		DATE_IN_SERVICE,
			AUC_NAME,			FISCAL_YEAR,	SAP_DOC,		PO_NO,			INV_NO,
			INV_LINE,			INV_DESC,		AUC_AMOUNT,		AUC_REMAIN,		
			ASSET_NO,			ASSET_SUB,		
			SETTLE_AMOUNT,			
			[STATUS],			[GUID],			DELETE_FLAG,
			CREATE_DATE,		CREATE_BY,		UPDATE_DATE,	UPDATE_BY
	)
	SELECT	COMPANY,			@USER+'/9999' DOC_NO,		@Latest + 1	,			
			ASSET_NO,			ASSET_SUB,			COST_CODE,		RESP_COST_CODE,		DATE_IN_SERVICE, 
			ASSET_NAME,			NULL FISCAL_YEAR,	NULL SAP_DOC,	NULL AS PO_NO,		NULL AS INV_NO,
			NULL INV_LINE,		NULL INV_DESC,		NULL AUC_AMOUNT,NULL AS AUC_REMAIN,
			NULL ASSET_NO,		NULL ASSET_SUB,			
			NULL SETTLE_AMOUNT,
			'NEW',				@GUID,			'N',				
			GETDATE(),			@USER,			GETDATE(),		@USER
	FROM	TB_M_ASSETS_H
	WHERE	COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB
END TRY
BEGIN CATCH
	THROW
END CATCH
GO
