DROP PROCEDURE [dbo].[sp_WFD02650_Login]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Check user login
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_Login]
	@EMP_CODE varchar(8),
	@COMPANY T_COMPANY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(SELECT COUNT(1) FROM TB_M_EMPLOYEE WHERE EMP_CODE=@EMP_CODE and COMPANY = @COMPANY) > 0
	BEGIN
		SELECT CONVERT(bit, 1) as RESULT
	END
	ELSE
	BEGIN 
		SELECT CONVERT(bit, 0) as RESULT
	END
END





GO
