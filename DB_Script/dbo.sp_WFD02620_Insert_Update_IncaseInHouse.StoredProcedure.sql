DROP PROCEDURE [dbo].[sp_WFD02620_Insert_Update_IncaseInHouse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_WFD02620_Insert_Update_IncaseInHouse]
(
	
	 @STOCK_TAKE_KEY		VARCHAR(17)
	, @COMPANY				T_COMPANY
    , @YEAR					VARCHAR(4)
    , @ROUND				VARCHAR(2)
    , @ASSETLOCATION		VARCHAR(1) --I is In house,O is out-source 
	, @ASSET_NO				T_ASSET_NO
	, @ASSET_NAME			varchar(50)
	, @ASSET_CLASS			T_ASSET_CLASS
	, @MINOR_CATEGORY		T_MINOR_CATEGORY
	, @COST_CODE			T_COST_CODE
	, @COST_NAME			varchar(20)
	, @EMP_CODE				T_SYS_USER
	, @LOCATION				varchar(10)
	, @CUMU_ACQU_PRDCOST_01	varchar(14)
	, @SERIAL_NO			varchar(18)
	, @MODEL				varchar(40)
	, @DATA_AS_OF			DATETIME
	, @ASSET_STATUS			VARCHAR(1)
	, @USERID				T_USER
	
	
	

	--@STOCK_TAKE_KEY,@COMPANY,@YEAR,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_NO,@ASSET_NAME,@COST_CODE,@COST_NAME,@EMP_CODE,@LOCATION,@SERIAL_NO,@MODEL,@CUMU_ACQU_PRDCOST_01

			--STOCK_TAKE_KEY		varchar(17)
			--,COMPANY				varchar(10)
			--,YEAR					varchar(4)
			--,@ROUND				varchar(2)
			--,@ASSET_LOCATION		varchar(1)
			--,@ASSET_NO			varchar(12)
			--,@ASSET_NAME			varchar(50)
			--,@ASSET_CLASS			varchar(8)
			--,@MINOR_CATEGORY		varchar(4)
			--,@COST_CODE			varchar(10)	
			--,@COST_NAME			varchar(20)
			--,@EMP_CODE			varchar(50)	
			--,@LOCATION			varchar(10)
			--,@CUMU_ACQU_PRDCOST_01	varchar(14)
			--,@SERIAL_NO			varchar(18)
			--,@MODEL				varchar(40)
			--,@DATA_AS_OF			DATETIME
			--,@ASSET_STATUS		VARCHAR(1)
		
	
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

IF OBJECT_ID('tempdb..#TEMP_MINOR_CATEGORY') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_SUPTYPE
END
	SELECT MINOR_CATEGORY
	 INTO #TEMP_MINOR_CATEGORY
	FROM TB_M_ASSETS_H
	WHERE 1<>1 

	    INSERT INTO #TEMP_MINOR_CATEGORY(MINOR_CATEGORY)
		SELECT VALUE AS MINOR_CATEGORY
		FROM STRING_SPLIT (@MINOR_CATEGORY, '#') 

-- 2  Insert into   TB_R_STOCK_TAKE_D
 


		INSERT INTO [dbo].[TB_R_STOCK_TAKE_D]
           ([STOCK_TAKE_KEY]
		   ,[COMPANY]
		   ,[YEAR]
		   ,[ROUND]
		   ,[ASSET_LOCATION]
           ,[SV_EMP_CODE]
		   ,[ASSET_NO]
		   ,[ASSET_NAME]
		   ,[EMP_CODE]
		   ,[COST_CODE]
		   ,[COST_NAME]
		   ,[BARCODE]
		   ,[CHECK_STATUS]
           ,[SCAN_DATE]
		   ,[START_COUNT_TIME]
		   ,[END_COUNT_TIME]
		   ,[COUNT_TIME]
           ,[LOCATION_NAME]
		   ,[SERIAL_NO]
		   ,[MODEL]
		   ,[PRODUCTION_PART_NO]
           ,[TOOLING_COST]
		   ,[CUMU_ACQU_PRDCOST_01]
           ,[ASSET_CLASS]
		   ,[MINOR_CATEGORY]
		   ,[INVEST_REASON]
		   ,[ASSET_STATUS]
		   ,[ASSET_STATUS_REASON]
           ,[ASSET_PLATE_STATUS]
		   ,[BOI_PLATE_STATUS]
		   ,[REMARK]
           ,[CREATE_DATE]
		   ,[CREATE_BY]
		   ,[UPDATE_DATE]
		   ,[UPDATE_BY])
		   
		SELECT 
		@STOCK_TAKE_KEY --[STOCK_TAKE_KEY] 
		,@COMPANY		--[COMPANY] 
		,@YEAR			--[YEAR] 
		,@ROUND			--[ROUND] 
		,@ASSETLOCATION --[ASSETLOCATION] 
		,NULL 		    --[SV_EMP_CODE]
		,ASSET_NO  	    --[ASSET_NO]
		,ASSET_NAME     --[ASSET_NAME]
		,@EMP_CODE		--[EMP_CODE]
		,@COST_CODE     --[COST_CODE]
		,@COST_NAME	    --[COST_NAME]
		,AssetH.BARCODE	--[BARCODE]
		,'N'		    --[CHECK_STATUS]
		,NULL		    --[SCAN_DATE]
		,NULL		    --[START_COUNT_TIME]
		,NULL		    --[END_COUNT_TIME]
		,NULL	        --[COUNT_TIME]
		,NULL		    --[LOCATION_NAME]
		,@SERIAL_NO		--[SERIAL_NO]
		,@MODEL		    --[MODEL]
		,NULL   	    --[PRODUCTION_PART_NO]
		,NULL		    --[TOOLING_COST]
		,AssetH.CUMU_ACQU_PRDCOST_01	--[CUMU_ACQU_PRDCOST_01]
		,AssetH.ASSET_CLASS				--[ASSET_CLASS]
		,AssetH.MINOR_CATEGORY		    --[MINOR_CATEGORY]
		,NULL		    --[INVEST_REASON]
		,NULL		    --[ASSET_STATUS]
		,NULL		    --[ASSET_STATUS_REASON]
		,NULL   	    --[ASSET_PLATE_STATUS]
		,NULL   	    --[BOI_PLATE_STATUS]
		,NULL			--[REMARK]
		,GETDATE()	    --[CREATE_DATE]
		,@UserID	    --[CREATE_BY]
		,GETDATE()	    --[UPDATE_DATE]
		,@UserID	    --[UPDATE_BY]

		--@STOCK_TAKE_KEY,@COMPANY,@YEAR,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_NO,@ASSET_NAME,@COST_CODE,@COST_NAME,@EMP_CODE,@LOCATION,@SERIAL_NO,@MODEL,@CUMU_ACQU_PRDCOST_01

		 FROM TB_M_ASSETS_H AssetH
		 WHERE AssetH.DATE_IN_SERVICE <= @DATA_AS_OF
		AND ASSETH.STATUS='Y'
		AND ASSETH.INHOUSE_FLAG='Y'
		AND ((RIGHT(ASSETH.ASSET_NO, 2)  = '00' AND @ASSET_STATUS='P')
				OR (RIGHT(ASSETH.ASSET_NO, 2)  <> '00' AND @ASSET_STATUS='C')
				OR @ASSET_STATUS='B' )
	    AND AssetH.MINOR_CATEGORY IN ( SELECT MINOR_CATEGORY FROM #TEMP_MINOR_CATEGORY )
















 

 ---- 2.1 Update Cost center detial 
 -- UPDATE StockD
 --  SET StockD.COST_NAME =M_Cost.COST_NAME
 --  --, StockD.PLANT_CD =M_Cost.PLANT_CD
 --  --, StockD.PLANT_NAME =M_Cost.PLANT_NAME   
 -- FROM TB_R_STOCK_TAKE_D StockD
 -- LEFT JOIN TB_M_COST_CENTER M_Cost ON StockD.COST_CODE=M_Cost.COST_CODE
 -- WHERE StockD.STOCK_TAKE_KEY =CONCAT(@COMPANY,@YEAR,@ROUND,@ASSETLOCATION)

---- 2.2 Update SV User 
-- UPDATE StockD
--   SET StockD.SV_EMP_CODE =SV_User.EMP_CODE
--  FROM TB_R_STOCK_TAKE_D StockD
--  LEFT JOIN TB_M_SV_COST_CENTER SV_User ON StockD.COST_CODE=SV_User.COST_CODE
--  WHERE StockD.STOCK_TAKE_KEY =CONCAT(@COMPANY,@YEAR,@ROUND,@ASSETLOCATION)

 ----- Delete asset no assign sv
 --DELETE TB_R_STOCK_TAKE_D WHERE ISNULL(SV_EMP_CODE,'')=''




   

-- --3  Insert into   TB_R_STOCK_TAKE_D_PER_SV
-- INSERT INTO TB_R_STOCK_TAKE_D_PER_SV
--           ([STOCK_TAKE_KEY] ,[COMPANY],[YEAR] ,[ROUND]   ,[ASSET_LOCATION]  ,[EMP_CODE]  ,[EMP_TITLE]
--           ,[EMP_NAME]  ,[EMP_LASTNAME]    ,[FA_DATE_FROM] ,[FA_DATE_TO]
--           ,[DATE_FROM]  ,[DATE_TO]  ,[TIME_START]  ,[TIME_END]
--           ,[TIME_MINUTE]   ,[USAGE_HANDHELD]   ,[IS_LOCK]  ,[TOTAL_ASSET]  
--		    ,[CREATE_DATE]   ,[CREATE_BY]   ,[UPDATE_DATE]    ,[UPDATE_BY])
--SELECT  CONCAT(@COMPANY,@YEAR,@ROUND,@ASSETLOCATION),@COMPANY ,@YEAR ,@ROUND,@ASSETLOCATION
--            , SV_EMP_CODE--<EMP_CODE, varchar(8),>
--            ,NULL -- <EMP_TITLE, varchar(8),>
--            ,NULL -- <EMP_NAME, varchar(30),>
--            ,NULL -- <EMP_LASTNAME, nvarchar(30),>
--            ,NULL --<FA_DATE_FROM, date,>
--            ,NULL --<FA_DATE_TO, date,>
--            ,NULL --<DATE_FROM, date,>
--            ,NULL --<DATE_TO, date,>
--            ,NULL --<TIME_START, varchar(5),>
--            ,NULL --<TIME_END, varchar(5),>
--            ,0 --<TIME_MINUTE, int,>
--            ,NULL --<USAGE_HANDHELD, int,>
--            ,'N' -- <IS_LOCK, varchar(1),>
--            ,COUNT(ASSET_NO ) --<TOTAL_ASSET, int,>
--            ,GETDATE(), @UserID,  GETDATE(), @UserID 
--FROM TB_R_STOCK_TAKE_D StockD
--WHERE StockD.STOCK_TAKE_KEY =CONCAT(@COMPANY,@YEAR,@ROUND,@ASSETLOCATION)
--GROUP BY SV_EMP_CODE
---- 3.1 Update SV Emp_Code 
--UPDATE StockSV
-- SET StockSV.EMP_TITLE=Emp.EMP_TITLE
-- ,StockSV.EMP_NAME=Emp.EMP_NAME
-- ,StockSV.EMP_LASTNAME=Emp.EMP_LASTNAME
--FROM TB_R_STOCK_TAKE_D_PER_SV StockSV LEFT JOIN TB_M_EMPLOYEE Emp ON StockSV.EMP_CODE=Emp.EMP_CODE
--WHERE StockSV.STOCK_TAKE_KEY =CONCAT(@COMPANY,@YEAR,@ROUND,@ASSETLOCATION)

 --4 Update  value in  TB_R_STOCK_TAKE_H	
  --DECLARE @ALL_ASSET_TOTAL INT  = 0
  --DECLARE @MC_TOTAL INT = 0
  --DECLARE @EC_TOTAL INT = 0 
  --DECLARE @PC_TOTAL INT = 0
  --DECLARE @LC_TOTAL INT = 0

--SELECT @ALL_ASSET_TOTAL =COUNT(*) 
--FROM TB_R_STOCK_TAKE_D  StockD
--WHERE StockD.STOCK_TAKE_KEY = @STOCK_TAKE_KEY

--SELECT @MC_TOTAL =COUNT(*) 
--FROM TB_R_STOCK_TAKE_D  StockD
--WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
--AND SUB_TYPE ='M/C'

--SELECT @EC_TOTAL =COUNT(*) 
--FROM TB_R_STOCK_TAKE_D  StockD
--WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
--AND SUB_TYPE ='E/C'

--SELECT @PC_TOTAL =COUNT(*) 
--FROM TB_R_STOCK_TAKE_D  StockD
--WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
--AND SUB_TYPE ='P/C'

--SELECT @LC_TOTAL =COUNT(*) 
--FROM TB_R_STOCK_TAKE_D  StockD
--WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
--AND SUB_TYPE ='L/C'

 --UPDATE StockH
 --SET  
	--  StockH.TOTAL = @ALL_ASSET_TOTAL --, 
 --    --StockH.MC_TOTAL = @MC_TOTAL , 
 --    --StockH.EC_TOTAL = @EC_TOTAL , 
 --    --StockH.PC_TOTAL = @PC_TOTAL , 
 --    --StockH.LC_TOTAL = @LC_TOTAL 
 --FROM TB_R_STOCK_TAKE_H StockH 
 --WHERE StockH.STOCK_TAKE_KEY = @STOCK_TAKE_KEY

 IF OBJECT_ID('tempdb..#TEMP_SUPTYPE') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_SUPTYPE
END
	RETURN @Error ;
END TRY
BEGIN CATCH
			       PRINT concat('ERROR Message:',ERROR_MESSAGE())
				
END CATCH







GO
