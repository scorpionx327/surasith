DROP PROCEDURE [dbo].[sp_BFD0261A_UpdateDataFromStagging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_BFD0261A_UpdateDataFromStagging]
(
	@UserID VARCHAR(8),
    @StockYear VARCHAR(4),
    @StockRound VARCHAR(2),
    @StockAssetLocation VARCHAR(1), --I is In house,O is out-source
    @QTyOfHanheld VARCHAR(8),
    @TargetDateFrom VARCHAR(8), --format YYYYMMDD
    @TargetDateTo VARCHAR(8), --format YYYYMMDD
    @BreakTime VARCHAR(8),
	@ERROR_MESSAGE	VARCHAR(MAX) OUT 
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;
-- 1  Insert into   TB_R_STOCK_TAKE_H
 UPDATE StockH
 SET  StockH.[TARGET_DATE_FROM] = CONVERT(DATE,@TargetDateFrom,112),
      StockH.[TARGET_DATE_TO] = CONVERT(DATE,@TargetDateTo,112),
	  StockH.[QTY_OF_HANDHELD] = CONVERT(INT,@QTyOfHanheld) , 
	  StockH.[BREAK_TIME_MINUTE]  = CONVERT(INT,@BreakTime) , 
      StockH.[PLAN_STATUS]  = 'D' , 
      StockH.UPDATE_BY  = @UserID , 
      StockH.UPDATE_DATE = GETDATE()
 FROM TB_R_STOCK_TAKE_H StockH 
 WHERE StockH.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)


-- 2  Insert into   TB_R_STOCK_TAKE_D
  -- DELETE temp 
    DELETE TB_R_STOCK_TAKE_D 
	WHERE STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
 --3  Insert into   TB_R_STOCK_TAKE_D_PER_SV
     DELETE TB_R_STOCK_TAKE_D_PER_SV 
	 WHERE STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)

IF (@StockAssetLocation='I') 
BEGIN

 EXEC @Error= sp_BFD0261A_Insert_Update_IncaseInHouse @UserID ,    @StockYear ,  @StockRound ,  @StockAssetLocation, 
											 @QTyOfHanheld ,    @TargetDateFrom ,@TargetDateTo , @BreakTime,	@ERROR_MESSAGE OUT 

END
IF (@StockAssetLocation='O') 
BEGIN
 EXEC @Error= sp_BFD0261A_Insert_Update_IncaseOutSource @UserID ,    @StockYear ,  @StockRound ,  @StockAssetLocation, 
											 @QTyOfHanheld ,    @TargetDateFrom ,@TargetDateTo , @BreakTime,	@ERROR_MESSAGE OUT 

END
  

	RETURN @Error ;
END TRY
BEGIN CATCH
			       PRINT concat('ERROR Message:',ERROR_MESSAGE())
				   SET @ERROR_MESSAGE=ERROR_MESSAGE();
				RETURN 1;
END CATCH







GO
