DROP PROCEDURE [dbo].[sp_WFD02620_GetStockTakeDDatas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetStockTakeDDatas]
	-- Add the parameters for the stored procedure here
	@COMPANY T_COMPANY
	,@YEAR	varchar(4)	
	, @ROUND	varchar(2)	
	, @ASSET_LOCATION	varchar(1)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		d.STOCK_TAKE_KEY
		,d.YEAR
		,d.ROUND
		,d.ASSET_LOCATION
		,d.SV_EMP_CODE
		,d.ASSET_NO
		,d.ASSET_NAME
		,d.EMP_CODE
		,d.COST_CODE
		,d.COST_NAME
		,d.PLANT_CD
		,d.PLANT_NAME
		,d.SUPPLIER_NAME
		,d.BARCODE
		,d.SUB_TYPE
		,d.CHECK_STATUS
		,d.SCAN_DATE
		,d.START_COUNT_TIME
		,d.END_COUNT_TIME
		,d.COUNT_TIME
		,d.LOCATION_NAME
		,d.CREATE_DATE
		,d.CREATE_BY
		,d.UPDATE_DATE
		,d.UPDATE_BY
		,s.REMARKS as ORDER_SUB_TYPE
	FROM TB_R_STOCK_TAKE_D d
	LEFT JOIN TB_M_SYSTEM s on s.CATEGORY='FAS_TYPE' and s.SUB_CATEGORY='SUB_TYPE'  and RTRIM(d.SUB_TYPE)=RTRIM(s.VALUE)
	WHERE d.YEAR=@YEAR and d.ROUND=@ROUND and d.ASSET_LOCATION=@ASSET_LOCATION

END




GO
