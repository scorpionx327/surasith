DROP PROCEDURE [dbo].[sp_WFD01270_CheckDifferentDivisionTransfer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_CheckDifferentDivisionTransfer]
@GUID		VARCHAR(50),
@CostCodeTo	VARCHAR(8),
@Role		VARCHAR(3),
@Cnt		INT OUT
AS
BEGIN
	
	DECLARE	@TB_S_ORG TABLE(ORG_CODE	VARCHAR(21))
	DECLARE	@TB_T_ORG TABLE(ORG_CODE	VARCHAR(21))

	DECLARE @CostCodeFromList VARCHAR(MAX)

	SELECT	@CostCodeFromList =  COALESCE(@CostCodeFromList + '|', '') + COST_CODE 
	FROM	(	SELECT	DISTINCT COST_CODE 
				FROM	TB_T_SELECTED_ASSETS 
				WHERE	[GUID] = @GUID
			) X
	--SELECT @CostCodeFromList
	INSERT
	INTO	@TB_S_ORG 
	SELECT	ORG_CODE
	FROM	[dbo].[fn_FD0OrgCodeOfCostCode](@CostCodeFromList)

	INSERT
	INTO	@TB_T_ORG 
	SELECT	ORG_CODE
	FROM	[dbo].[fn_FD0OrgCodeOfCostCode](@CostCodeTo)

	--SELECT * FROM @TB_S_ORG
	--SELECT * FROM @TB_T_ORG
	
	-- Get Length Of Division
	DECLARE @Length INT
	SET @Length = CONVERT(INT, dbo.fn_GetSystemMaster('SYSTEM_CONFIG','DIFF_DIVISION', @Role))
	--SELECT @Length
	SELECT	@Cnt = COUNT(*)
	FROM	@TB_S_ORG S
	LEFT	JOIN
			@TB_T_ORG T
	ON		LEFT(S.ORG_CODE, @Length) = LEFT(T.ORG_CODE, @Length)
	WHERE	T.ORG_CODE IS NULL

	--SELECT *
	--FROM	@TB_S_ORG S
	--LEFT	JOIN
	--		@TB_T_ORG T
	--ON		LEFT(S.ORG_CODE, @Length) = LEFT(T.ORG_CODE, @Length)
	--WHERE	T.ORG_CODE IS NULL
	
END


GO
