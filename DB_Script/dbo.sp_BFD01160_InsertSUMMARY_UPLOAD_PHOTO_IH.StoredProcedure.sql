DROP PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_UPLOAD_PHOTO_IH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_UPLOAD_PHOTO_IH]
	@Company	T_COMPANY,
	@User		VARCHAR(8)
AS
BEGIN
	DECLARE @ID int,	
		@TO varchar(1000),
		@CC varchar(1000),
		@BCC varchar(1000),
		@TITLE varchar(255),
		@MESSAGE nvarchar(max),
		@FOOTER varchar(250),
		@TYPE varchar(1),
		@START_PERIOD datetime,
		@END_PERIOD datetime,
		@SEND_FLAG varchar(1),
		@RESULT_FLAG varchar(1),
		@REF_FUNC varchar(8),
		@REF_DOC_NO T_DOC_NO

	DECLARE @VALUE_INDICATOR varchar(1)
	select @VALUE_INDICATOR = [VALUE] From TB_M_SYSTEM																
	Where CATEGORY = 'NOTIFICATION_CONFIG'																
		 and SUB_CATEGORY = 'PHOTO_CONDITION_STM'	
		 and CODE = 'CHECK_INVEN_INDICATOR'
		 and ACTIVE_FLAG = 'Y'

	DECLARE @VALUE_MINORCATE varchar(1)
	select @VALUE_MINORCATE = [VALUE] From TB_M_SYSTEM																
	Where CATEGORY = 'NOTIFICATION_CONFIG'																
		 and SUB_CATEGORY = 'PHOTO_CONDITION_STM'	
		 and CODE = 'CHECK_MINORCATE'
		 and ACTIVE_FLAG = 'Y'	

	select T.* INTO #TMP 
	from (
			select top 100 h.ASSET_NO, h.ASSET_NAME, h.COST_CODE,cc.COST_NAME, h.RESP_COST_CODE,Resp_cc.COST_NAME RESP_COST_NAME, h.LOCATION
			from TB_M_ASSETS_H h
			inner join TB_M_COST_CENTER cc on h.COST_CODE = cc.COST_CODE
			inner join TB_M_COST_CENTER Resp_cc on h.RESP_COST_CODE = Resp_cc.COST_CODE 
			where h.COMPANY = @Company
				and h.PHOTO_STATUS = 'N'
				and (
					(h.INVEN_INDICATOR = 'Y' and @VALUE_MINORCATE = 'N') 
					or 
					(@VALUE_MINORCATE = 'Y' and (CONCAT(h.ASSET_CLASS,'|',h.MINOR_CATEGORY) 
									in (SELECT VALUE FROM TB_M_SYSTEM m WHERE CATEGORY = 'NOTIFICATION_CONFIG' 
										and m.SUB_CATEGORY = 'PHOTO_UPLOAD_MINOR_STM'
										and m.ACTIVE_FLAG = 'Y'
										)
									)
								)
					)
			group by h.ASSET_NO, h.ASSET_NAME, h.COST_CODE,cc.COST_NAME, h.RESP_COST_CODE,Resp_cc.COST_NAME, h.LOCATION 
		) T

		DECLARE @EMP_CODE varchar(50),@EMAIL varchar(50),@NAME varchar(100)
		DECLARE cursor_results CURSOR FOR 
		select e.EMP_CODE,e.EMAIL,concat(e.EMP_NAME,' ',e.EMP_LASTNAME) n
		from TB_M_SV_COST_CENTER scc
			inner join TB_M_EMPLOYEE e on scc.EMP_CODE = e.EMP_CODE
		where scc.COST_CODE in (select COST_CODE from #TMP)
		group by e.EMP_CODE,e.EMAIL,e.EMP_NAME,e.EMP_LASTNAME

		OPEN cursor_results
		FETCH NEXT FROM cursor_results into @EMP_CODE,@EMAIL,@NAME
		WHILE @@FETCH_STATUS = 0
			BEGIN 
				SET @MESSAGE = CONCAT('Dear ',@NAME,' <br>');

				SET @MESSAGE = CONCAT(@MESSAGE,('<table style="border:1px solid black;">
												<thead style="height:25px; background-color:rgba(236, 240, 245, 1);"> 
												  <tr>
													<th style="min-width: 100px;">Asset No</th>	
													<th style="min-width: 100px;">Description</th>	
													<th style="min-width: 100px;">Cost Center</th>	
													<th style="min-width: 100px;">Cos Center Name</th>					
												  </tr>
												</thead>'));

				DECLARE @ASSET_NO varchar(100), @ASSET_NAME varchar(200), @COST_CODE varchar(100),@COST_NAME varchar(100), @RESP_COST_CODE varchar(100),@RESP_COST_NAME varchar(100)
				DECLARE cursor_results2 CURSOR FOR 
					select t.ASSET_NO, t.ASSET_NAME, t.COST_CODE,t.COST_NAME, t.RESP_COST_CODE,t.RESP_COST_NAME
					from #TMP  t
					OPEN cursor_results2
					FETCH NEXT FROM cursor_results2 into @ASSET_NO, @ASSET_NAME, @COST_CODE,@COST_NAME, @RESP_COST_CODE,@RESP_COST_NAME
					WHILE @@FETCH_STATUS = 0
						BEGIN 
							SET @MESSAGE = CONCAT(@MESSAGE,'<tr>');							
							SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;text-align:left;">',@ASSET_NO,'</td>');	
							SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:left;">',@ASSET_NAME,'</td>');	
							SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;text-align:center;">',@COST_CODE,'</td>');	
							SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:center;">',@COST_NAME,'</td>');		
							SET @MESSAGE = CONCAT(@MESSAGE,'</tr>');	

							FETCH NEXT FROM cursor_results2 into @ASSET_NO, @ASSET_NAME, @COST_CODE,@COST_NAME, @RESP_COST_CODE,@RESP_COST_NAME
						END

					CLOSE cursor_results2;
					DEALLOCATE cursor_results2;
				SET @ID = (select isnull(max(ID),0) + 1 from TB_R_NOTIFICATION);
				SET @MESSAGE = CONCAT(@MESSAGE,'</table>');

				SET @FOOTER = 'Best Regards,<br>System';
				SET @SEND_FLAG = 'N';
				SET @RESULT_FLAG = 'N';
				SET @TYPE = 'S';
				SET @REF_FUNC = '';

				SET @TO = @EMAIL;
				SET @TITLE = concat('TFAST : ',@Company,' Upload Asset Photo Notification');
				INSERT INTO TB_R_NOTIFICATION
					   (ID,[TO],CC,BCC,TITLE,[MESSAGE],FOOTER,[TYPE]
					   ,START_PERIOD,END_PERIOD,SEND_FLAG,RESULT_FLAG,REF_FUNC,REF_DOC_NO
						,CREATE_DATE,CREATE_BY,UPDATE_DATE,UPDATE_BY)
				 VALUES
					   (@ID
					   ,@TO
					   ,@CC
					   ,@BCC
					   ,@TITLE
					   ,@MESSAGE
					   ,@FOOTER
					   ,@TYPE
					   ,@START_PERIOD
					   ,@END_PERIOD
					   ,@SEND_FLAG
					   ,@RESULT_FLAG
					   ,@REF_FUNC
					   ,@REF_DOC_NO
					   ,getdate()
					   ,@User
					   ,getdate()
					   ,@User)
				FETCH NEXT FROM cursor_results into @EMP_CODE,@EMAIL,@NAME
			END

		CLOSE cursor_results;
		DEALLOCATE cursor_results;
	Drop table #TMP
END
GO
