DROP PROCEDURE [dbo].[sp_WFD01210_GetDelegateUserList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01210_GetDelegateUserList]
@EmpCode	VARCHAR(8)
AS
BEGIN
	
	DECLARE @OrgCode VARCHAR(21), @Role VARCHAR(3), @Length INT, @PreLength INT

	SELECT	@OrgCode = ORG_CODE,
			@Role = [ROLE]
	FROM	TB_M_EMPLOYEE
	WHERE	EMP_CODE = '0019' --@EmpCode

	-- Get Length Of Role

	SET @Length = CONVERT(INT, dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPROVE_CHECK_LENGTH',@Role))
	SET @Length = ISNULL(@Length,21)
	SET @PreLength = @Length - 3

	SELECT	EMP_CODE, Concat(EMP_NAME, ' ' ,EMP_LASTNAME)as FULL_NAME, ORG_CODE
	FROM	TB_M_EMPLOYEE
	WHERE	[ROLE] = @Role AND -- Same Role
			LEFT(ORG_CODE, @PreLength) = LEFT(@OrgCode, @PreLength) AND -- Under same leader
			EMP_CODE != @EmpCode -- Other Person
	ORDER	BY FULL_NAME

END

-- EXEC sp_XXXXGetDelegateUserList '0157'

-- select * from TB_M_EMPLOYEE where ROLE = 'DM'


GO
