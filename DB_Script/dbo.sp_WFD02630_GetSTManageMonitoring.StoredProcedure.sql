DROP PROCEDURE [dbo].[sp_WFD02630_GetSTManageMonitoring]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02630_GetSTManageMonitoring] 
(   @COMPANY T_COMPANY,
	@YEAR VARCHAR(4),
	@ROUND VARCHAR(2),
	@ASSET_LOCATION VARCHAR(1),
	@EMP_CODE VARCHAR(30),
	@ISFAADMIN VARCHAR(1) --- Y is FA Admin , N is SV User , Normal user not access 
)
AS
BEGIN
		
		-- Should be CONCAT(@COMPANY, @YEAR ,@ROUND , @ASSET_LOCATION)
		DECLARE @StockTakeKey VARCHAR(17) = CONCAT(@YEAR ,@ROUND , @ASSET_LOCATION)

          ----==============================  Declare status 
	    DECLARE @Status_Complete AS VARCHAR(20)				= 	 'Complete'
		DECLARE @Status_Complete_WithDelay AS VARCHAR(20)	=     'Complete with Delay'
		DECLARE @Status_Delay AS VARCHAR(20)				=     'Delay'
		DECLARE @Status_OnProgress AS VARCHAR(20)			=      'On Progress'
		DECLARE @Status_Complete_WithLose AS VARCHAR(20)	=      'Complete with Loss'
		DECLARE @Status_Not_Yet_Scan AS VARCHAR(20)			=      'not_yet_scan'

		 DECLARE @Status_EVA_CIRCLE VARCHAR(10) = 'CIRCLE'
		 DECLARE @Status_EVA_TRIANGLE VARCHAR(10) = 'TRIANGLE'
		 DECLARE @Status_EVA_CROSS VARCHAR(10) = 'CROSS'
		 

		BEGIN /* ==============================  CREATE Temp TABLE ============================== */
				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_H') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_H
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D_PER_SV') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D_PER_SV
				END

				---- Temp data for retrun to screen 
				IF OBJECT_ID('tempdb..#T_STOCK_CALENDAR') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_CALENDAR
				END

				IF OBJECT_ID('tempdb..#T_DATA_AS_OF') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_AS_OF
				END

				IF OBJECT_ID('tempdb..#T_DATA_OVER_ALL') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_OVER_ALL
				END
		END
 
		BEGIN /* =================  INSERT DATA TO Temp table ====Header /Detail / Per SV ========================== */
		  --Stock Header
				SELECT	* 
				INTO	#T_STOCK_TAKE_H 
			   FROM		TB_R_STOCK_TAKE_H stockH
			   WHERE    stockH.COMPANY	= @COMPANY
				AND		stockH.YEAR = @YEAR 
				AND		stockH.ROUND   =  @ROUND 
				AND		stockH.ASSET_LOCATION = @ASSET_LOCATION 
				AND		stockH.STOCK_TAKE_KEY= @StockTakeKey

		--		select 'test1',* from #T_STOCK_TAKE_H

		 --Stock Per SV
			   SELECT  STOCK_TAKE_KEY  , YEAR  , ROUND , ASSET_LOCATION 	  , EMP_CODE 
					  , EMP_TITLE 		  , EMP_NAME   , EMP_LASTNAME ,DATE_FROM
					  ,CASE 
						WHEN (DATE_TO IS NOT NULL) AND (TIME_END IS NOT NULL) THEN CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),DATE_TO,112),' ', TIME_END,':00' ))
						WHEN (DATE_TO IS NOT NULL) AND (TIME_END IS NULL) THEN CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),DATE_TO,112),' ','23:59:59' ))
						ELSE NULL END AS DATE_TO
					  , TIME_MINUTE 	  , USAGE_HANDHELD 	  , IS_LOCK  , TOTAL_ASSET  
			   INTO		#T_STOCK_TAKE_D_PER_SV
			   FROM		TB_R_STOCK_TAKE_D_PER_SV stockSV
			   WHERE    stockSV.COMPANY	= @COMPANY
				AND		stockSV.YEAR = @YEAR 
				AND		stockSV.ROUND   =  @ROUND 
				AND		stockSV.ASSET_LOCATION = @ASSET_LOCATION 
				AND		stockSV.STOCK_TAKE_KEY= @StockTakeKey
				--AND (    (@ISFAADMIN='Y')  OR 
				--		((@ISFAADMIN='N') AND ( stockSV.EMP_CODE = @EMP_CODE))
				--     )---- only access data

			--	select 'test2',* from #T_STOCK_TAKE_D_PER_SV


		 --Stock Detail
			   SELECT	stockD.STOCK_TAKE_KEY, stockD.[YEAR],stockD.[ROUND],stockD.ASSET_LOCATION,stockD.SV_EMP_CODE,
						stockD.COST_CODE,stockD.CHECK_STATUS,stockD.SCAN_DATE ,requestStock.DOC_NO,ASSET_NO
			   INTO		#T_STOCK_TAKE_D
			   FROM		TB_R_STOCK_TAKE_D  stockD  
			   LEFT		JOIN 
						TB_R_REQUEST_STOCK requestStock 
			   ON		stockD.COMPANY = requestStock.COMPANY AND 
						stockD.YEAR = requestStock.YEAR  AND    
						stockD.ROUND   =  requestStock.ROUND AND 
						stockD.ASSET_LOCATION = requestStock.ASSET_LOCATION  AND 
						stockD.COST_CODE =requestStock.COST_CODE AND 
						stockD.SV_EMP_CODE = requestStock.SV_EMP_CODE
			   WHERE	stockD.COMPANY	= @COMPANY
				AND		stockD.YEAR = @YEAR 
				AND		stockD.ROUND   =  @ROUND 
				AND		stockD.ASSET_LOCATION = @ASSET_LOCATION 
				AND		stockD.STOCK_TAKE_KEY= @StockTakeKey
				AND EXISTS( SELECT 1 
							FROM #T_STOCK_TAKE_D_PER_SV t
							WHERE stockD.STOCK_TAKE_KEY=t.STOCK_TAKE_KEY)
			--- For filter only cost center that related to user
		     AND EXISTS (SELECT 1 FROM dbo.FN_FD0COSTCENTERLIST(@EMP_CODE) CC where CC.COST_CODE = stockD.COST_CODE AND CC.COMPANY = @COMPANY)
			--SELECT 'test#4',* FROM dbo.FN_FD0COSTCENTERLIST(@EMP_CODE)
			-- select 'test#3',* from #T_STOCK_TAKE_D
		END

		BEGIN /* =================  Cal data for generate canlendar ========================== */
			  BEGIN --- ==============    CAL SV in Calendar ===============
			   SELECT        stockSV.STOCK_TAKE_KEY AS STOCK_TAKE_KEY
					   , CONCAT(LTRIM(RTRIM(stockSV.EMP_NAME)),' ', LEFT(stockSV.EMP_LASTNAME,1),'.') AS TITLE
					   ,EMP_LASTNAME AS [DESCRIPTION] ---temp field
					   ,TOTAL_ASSET AS ACTUAL_SCAN   ---temp field
					   ,TOTAL_ASSET AS TOTAL_ASSET
					   ,stockSV.DATE_FROM AS START_PLAN 
					   ,stockSV.DATE_TO AS END_PLAN 
					   ,stockSV.DATE_TO AS MAX_SCAN_DATE 
					   , CONCAT('?RequestType=S&YEAR=',@YEAR ,'&ROUND=',@ROUND,'&ASSET_LOCATION=',@ASSET_LOCATION,'&EMP_CODE=',stockSV.EMP_CODE ) AS URL_LINK 
					   , EMP_LASTNAME AS CLASS_NAME  ---temp field
					   , StockSV.EMP_CODE AS EMP_CODE
					   , EMP_LASTNAME AS IS_SEND_REQUEST  ---temp field If send all cost center  system set 'Y' otherwise set 'N'
					   , NULL AS IS_CHILD_COMP
		 INTO #T_STOCK_CALENDAR
		 FROM #T_STOCK_TAKE_D_PER_SV stockSV


				  -- Set initial data 
				   UPDATE 	 stockT
				   SET      stockT.ACTUAL_SCAN		= 0
							,stockT.DESCRIPTION		= NULL
							,stockT.CLASS_NAME		= NULL
							,stockT.MAX_SCAN_DATE	= NULL
							,stockT.IS_SEND_REQUEST = 'Y'
							,stockT.IS_CHILD_COMP = NULL
				   FROM #T_STOCK_CALENDAR stockT

				   UPDATE stockT
				   SET TOTAL_ASSET = ISNULL(TotalAsset.TOTAL_ASSET,0)
				   FROM #T_STOCK_CALENDAR stockT
				   Left JOIN 
							( SELECT COUNT(*) AS TOTAL_ASSET ,SV_EMP_CODE ,STOCK_TAKE_KEY
							   FROM #T_STOCK_TAKE_D stockD
							   GROUP BY SV_EMP_CODE,STOCK_TAKE_KEY
							   )TotalAsset 
				   ON stockT.EMP_CODE=TotalAsset.SV_EMP_CODE AND stockT.STOCK_TAKE_KEY=TotalAsset.STOCK_TAKE_KEY

				  -- SELECT 'test4',COUNT(*) AS TOTAL_ASSET ,SV_EMP_CODE ,STOCK_TAKE_KEY
					--		   FROM #T_STOCK_TAKE_D stockD
					--		   GROUP BY SV_EMP_CODE,STOCK_TAKE_KEY

				  -- select 'test3',* from #T_STOCK_CALENDAR
				   -- delete SV with no asset after filter by cost center
				   DELETE FROM #T_STOCK_CALENDAR where TOTAL_ASSET = 0


				   --Update IS_SEND_REQUEST
				  UPDATE 	 stockT
				   SET    stockT.IS_SEND_REQUEST = 'N'
				   FROM #T_STOCK_CALENDAR stockT
				   INNER JOIN (                          
						SELECT
                            SV_EMP_CODE,
                            --DOC_NO,
							SUM(CASE WHEN DOC_NO IS NOT NULL THEN 1 ELSE 0 END) CNT,
							SUM(CASE WHEN DOC_NO IS NULL THEN 1 ELSE 0 END) NO_CNT
                        FROM
                            #T_STOCK_TAKE_D
                      --  WHERE
                      --      DOC_NO IS NULL
                        GROUP BY
                            SV_EMP_CODE
                          --  DOC_NO
						HAVING SUM(CASE WHEN DOC_NO IS NULL THEN 1 ELSE 0 END) > 0 
                    ) SendRequest
				  ON stockT.EMP_CODE=SendRequest.SV_EMP_CODE

				IF OBJECT_ID('tempdb..#T_CHILD') IS NOT NULL
				BEGIN
						DROP TABLE #T_CHILD
				END

				SELECT
                      SV_EMP_CODE,
                            COST_CODE,
                            SUM(   CASE
                                       WHEN DOC_NO IS NOT NULL
                                           THEN 1
                                       ELSE
                                           0
                                   END
                               ) s_CNT,
                            SUM(   CASE
                                       WHEN DOC_NO IS NULL
                                           THEN 1
                                       ELSE
                                           0
                                   END
                               ) s_NO_CNT,
                            SUM(   CASE
                                       WHEN CHECK_STATUS = 'N'
         THEN 1
                                  ELSE
                                           0
                                   END
                               ) s_CNT_LOSS
							   INTO #T_CHILD
                        FROM
                            #T_STOCK_TAKE_D
                        GROUP BY
                            SV_EMP_CODE,
                            COST_CODE
                        --  DOC_NO
						 
                        HAVING
                            SUM(   CASE
                                       WHEN DOC_NO IS NULL
                                           THEN 1
                                       ELSE
                                           0
                                   END
                               ) = 0

			UPDATE
                stockT
            SET
                stockT.IS_CHILD_COMP = SendRequest.cnt_loss
            FROM
                #T_STOCK_CALENDAR stockT
                INNER JOIN
                    (
					SELECT SV_EMP_CODE,SUM(s_CNT) AS cnt,SUM(s_NO_CNT) AS no_cnt, MAX(s_CNT_LOSS) AS cnt_loss FROM  #T_CHILD  GROUP BY SV_EMP_CODE
                    ) SendRequest
                        ON stockT.EMP_CODE = SendRequest.SV_EMP_CODE;

					IF (@ASSET_LOCATION = 'O')
					BEGIN
						UPDATE
							stockT
						SET
							stockT.IS_SEND_REQUEST = 'Y'
						FROM
							#T_STOCK_CALENDAR stockT
						WHERE stockT.IS_CHILD_COMP IS NOT NULL
					END

				IF OBJECT_ID('tempdb..#T_CHILD') IS NOT NULL
				BEGIN
						DROP TABLE #T_CHILD
				END


			   --Update Actual scan/Max scan date
   				 UPDATE 	 stockT
					   SET stockT.ACTUAL_SCAN=ISNULL(StockIsScan.IS_SCAN,0),
						   stockT.MAX_SCAN_DATE=CASE 
												WHEN (ISNULL(StockIsScan.IS_SCAN,0) < stockT.TOTAL_ASSET) AND (ISNULL(StockIsScan.MAX_SCAN_DATE,GETDATE()) < GETDATE()) THEN GETDATE()
												ELSE ISNULL(StockIsScan.MAX_SCAN_DATE,GETDATE()) END
				FROM #T_STOCK_CALENDAR stockT
				Left JOIN 
							( SELECT COUNT(*) AS IS_SCAN ,SV_EMP_CODE ,STOCK_TAKE_KEY,MAX(SCAN_DATE) AS MAX_SCAN_DATE
							   FROM #T_STOCK_TAKE_D stockD
							   WHERE CHECK_STATUS='Y' AND SCAN_DATE IS NOT NULL
							   GROUP BY SV_EMP_CODE,STOCK_TAKE_KEY
							   )StockIsScan  
				ON stockT.EMP_CODE=StockIsScan.SV_EMP_CODE AND stockT.STOCK_TAKE_KEY=StockIsScan.STOCK_TAKE_KEY


			   --- Update description 
				UPDATE 	 stockT
					   SET stockT.[DESCRIPTION]= CONCAT( Cast(Cast(CAST(stockT.ACTUAL_SCAN AS decimal)/ CAST(stockT.TOTAL_ASSET AS decimal)*100 as decimal(18,2)) as varchar(8)) + '%' 
														 , ' (', stockT.ACTUAL_SCAN, '/', stockT.TOTAL_ASSET, ')')
				FROM #T_STOCK_CALENDAR stockT

		/*   ------------------------- Case Eva of stock calendar
		--Case	All Found	On Time		Submit to Request	Status 
		--1		Yes			Yes				No				Complete
		--2		Yes			Yes				Yes				Complete
		--3		Yes			No				No				delay
		--4		Yes			No				Yes				Complete with delay
		--5		No			Yes				No				On process
		--6		No			Yes				Yes				Complete with lose
		--7		No			No				No				delay
		--8		No			No				Yes				Complete with lose
		 9  no scan and  on time ---> on process
		 10 no scan and over time ---> delay 
		 */
		   ---- Update class name ( In case not yet scan ,On process , Delay , complete , complete with lose 
					   UPDATE 	stockT
					   SET 
					   stockT.CLASS_NAME= CASE 
			   						 --- Find all asset
									 WHEN   (stockT.ACTUAL_SCAN = stockT.TOTAL_ASSET)  AND ( MAX_SCAN_DATE <=END_PLAN  ) AND ( IS_SEND_REQUEST='N' ) THEN @Status_Complete
									 WHEN   (stockT.ACTUAL_SCAN = stockT.TOTAL_ASSET)  AND ( MAX_SCAN_DATE <=END_PLAN  ) AND ( IS_SEND_REQUEST='Y' ) THEN @Status_Complete
									 WHEN   (stockT.ACTUAL_SCAN = stockT.TOTAL_ASSET)  AND ( MAX_SCAN_DATE > END_PLAN  ) AND ( IS_SEND_REQUEST='N' ) THEN @Status_Delay
									 WHEN   (stockT.ACTUAL_SCAN = stockT.TOTAL_ASSET)  AND ( MAX_SCAN_DATE > END_PLAN  ) AND ( IS_SEND_REQUEST='Y' ) THEN @Status_Complete_WithDelay
									 --- Some asset not found

										WHEN (stockT.ACTUAL_SCAN < stockT.TOTAL_ASSET)
                                             AND (MAX_SCAN_DATE <= END_PLAN)
                                             AND (IS_SEND_REQUEST = 'N')
                                             AND ( IS_CHILD_COMP = 0 )
                                            THEN @Status_OnProgress
                                        WHEN (stockT.ACTUAL_SCAN < stockT.TOTAL_ASSET)
                                             AND (MAX_SCAN_DATE <= END_PLAN)
                                             AND (IS_SEND_REQUEST = 'N')
                                             AND (IS_CHILD_COMP > 0)
                                            THEN @Status_Complete_WithLose
                                        WHEN (stockT.ACTUAL_SCAN < stockT.TOTAL_ASSET)
                                             AND (MAX_SCAN_DATE <= END_PLAN)
                                             AND (IS_SEND_REQUEST = 'Y')
                                            THEN @Status_Complete_WithLose
                                        WHEN (stockT.ACTUAL_SCAN < stockT.TOTAL_ASSET)
                                             AND (MAX_SCAN_DATE > END_PLAN)
                                             AND (IS_SEND_REQUEST = 'N')
											 AND (IS_CHILD_COMP > 0)
                                            THEN @Status_Complete_WithLose
                                        WHEN (stockT.ACTUAL_SCAN < stockT.TOTAL_ASSET)
                                             AND (MAX_SCAN_DATE > END_PLAN)
                                             AND (IS_SEND_REQUEST = 'N')
                                            THEN @Status_Delay
                                        WHEN (stockT.ACTUAL_SCAN < stockT.TOTAL_ASSET)
                                             AND (MAX_SCAN_DATE > END_PLAN)
                                             AND (IS_SEND_REQUEST = 'Y')
                                            THEN @Status_Complete_WithLose

									 --  no scan
									WHEN     (stockT.ACTUAL_SCAN = 0 )  AND (MAX_SCAN_DATE IS NULL)	AND ( GETDATE() <=END_PLAN  )  			         THEN @Status_Not_Yet_Scan
									WHEN     (stockT.ACTUAL_SCAN = 0 )  AND (MAX_SCAN_DATE IS NULL)	AND ( GETDATE() > END_PLAN  )  			         THEN @Status_Delay
									END
					 FROM #T_STOCK_CALENDAR stockT

		END
			  BEGIN --======================--- Clone structure table ==================================== 
					 --1. --  DATE_AS_OF , ACTUAL , TOTAL ,100%  , class   ---> data as of  -- Clone structure table 
					 SELECT   s_cal.END_PLAN AS  DATE_AS_OF,s_cal.ACTUAL_SCAN AS ACTUAL, s_cal.TOTAL_ASSET AS TOTAL
						  ,s_cal.[DESCRIPTION] AS PROGRESS_BAR_PERCEN,s_cal.CLASS_NAME AS CLASS_NAME, s_cal.MAX_SCAN_DATE AS MAX_SCAN_DATE
						  ,  CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),sh.TARGET_DATE_TO,112),' ','23:59:59' ))   AS TARGET_DATE_TO
						  ,s_cal.IS_SEND_REQUEST 
				 INTO  #T_DATA_AS_OF
				 FROM  #T_STOCK_CALENDAR s_cal  INNER JOIN #T_STOCK_TAKE_H  sH ON s_cal.STOCK_TAKE_KEY=sH.STOCK_TAKE_KEY
				 WHERE 1<>1 
 

					--2.  --- Data Over all -->  ACTUAL , TOTAL ,100% ,class, icon     -- Clone structure table 
					 SELECT    ACTUAL_SCAN AS ACTUAL, TOTAL_ASSET AS TOTAL
				  ,[DESCRIPTION] AS PROGRESS_BAR_PERCEN,CLASS_NAME AS CLASS_NAME,CLASS_NAME AS IMAGE_TYPE
				  ,s_cal.MAX_SCAN_DATE AS MAX_SCAN_DATE,CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),sh.TARGET_DATE_TO,112),' ','23:59:59' )) AS TARGET_DATE_TO,s_cal.IS_SEND_REQUEST 
		 INTO  #T_DATA_OVER_ALL
		 FROM  #T_STOCK_CALENDAR s_cal  INNER JOIN #T_STOCK_TAKE_H  sH ON s_cal.STOCK_TAKE_KEY=sH.STOCK_TAKE_KEY
		 WHERE 1<>1 
 
			 END 


			 IF EXISTS (SELECT 1 FROM #T_STOCK_TAKE_H  WHERE PLAN_STATUS ='F')
				BEGIN --- Finish plan

				   BEGIN --================================== -Data as OF ============================== 
					  INSERT INTO  #T_DATA_AS_OF ( DATE_AS_OF, ACTUAL,  TOTAL,TARGET_DATE_TO)
					 SELECT  UPDATE_DATE AS DATE_AS_OF,ALL_ASSET_TOTAL AS ACTUAL, ALL_ASSET_TOTAL AS TOTAL,
					  CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),TARGET_DATE_TO,112),' ','23:59:59' )) AS TARGET_DATE_TO
					 FROM #T_STOCK_TAKE_H

					-- Update Actual scan
					 UPDATE #T_DATA_AS_OF
						 SET ACTUAL= ( SELECT ACTUAL_SCAN
										 FROM
											  ( SELECT ISNULL(SUM(ACTUAL_SCAN),0) AS ACTUAL_SCAN ,STOCK_TAKE_KEY
												  FROM #T_STOCK_CALENDAR
												  GROUP BY STOCK_TAKE_KEY
											  )dt
										  )
							 ,TOTAL= ( SELECT TOTAL_ASSET
										 FROM
											  ( SELECT ISNULL(SUM(TOTAL_ASSET),0) AS TOTAL_ASSET ,STOCK_TAKE_KEY
												  FROM #T_STOCK_CALENDAR
												  GROUP BY STOCK_TAKE_KEY
											  )dt2
										  )

						-- Update Max scan date
						 UPDATE #T_DATA_AS_OF
							 SET MAX_SCAN_DATE= ( SELECT MAX_SCAN_DATE
											 FROM
												  ( SELECT MAX(MAX_SCAN_DATE) AS MAX_SCAN_DATE ,STOCK_TAKE_KEY
													  FROM #T_STOCK_CALENDAR
													  GROUP BY STOCK_TAKE_KEY
												  )dt
											  )

						/* find whether any SV is delay */
						DECLARE @Delay as INT;
						SELECT @Delay = count(CLASS_NAME)
						FROM #T_STOCK_CALENDAR
						WHERE CLASS_NAME in (@Status_Delay,@Status_Complete_WithDelay)
					


						-- Update progress
						 UPDATE #T_DATA_AS_OF
						 SET PROGRESS_BAR_PERCEN =CASE WHEN TOTAL = 0 THEN '0.00%'
						                          ELSE CONCAT(Cast(Cast(CAST(ACTUAL AS decimal)/ CAST(TOTAL AS decimal)*100 as decimal(18,2)) as varchar(8)) , '%' ) END
								  ,CLASS_NAME   =CASE -- Update progress  On progress,Complete , Complete with Delay , Complete with Lose
												   --WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_Complete
												   --WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_Complete_WithDelay
												   WHEN  (ACTUAL = TOTAL)  AND @Delay = 0 THEN @Status_Complete
												   WHEN  (ACTUAL = TOTAL)  AND @Delay > 0   THEN @Status_Complete_WithDelay
												   WHEN  (ACTUAL < TOTAL)  THEN @Status_Complete_WithLose
										   END
		 

					END

				   BEGIN -- =============================== Data Over all =================================
						  INSERT INTO #T_DATA_OVER_ALL(ACTUAL,TOTAL,TARGET_DATE_TO,MAX_SCAN_DATE)
						  SELECT  ACTUAL, TOTAL,TARGET_DATE_TO,MAX_SCAN_DATE
						  FROM #T_DATA_AS_OF
						
						-- Update progress
						 UPDATE #T_DATA_OVER_ALL
						 SET PROGRESS_BAR_PERCEN = CASE WHEN TOTAL = 0 THEN '0.00%' 
						                           ELSE CONCAT(Cast(Cast(CAST(ACTUAL AS decimal)/ CAST(TOTAL AS decimal)*100 as decimal(18,2)) as varchar(8)) , '%' ) END
								  ,CLASS_NAME   = CASE -- Update progress  On progress,Complete , Complete with Delay , Complete with Lose
												   --WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_Complete
												   --WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_Complete_WithDelay
												   WHEN  (ACTUAL = TOTAL)  AND @Delay = 0  THEN @Status_Complete
												   WHEN  (ACTUAL = TOTAL)  AND @Delay > 0   THEN @Status_Complete_WithDelay
												   WHEN  (ACTUAL < TOTAL)  THEN @Status_Complete_WithLose END
							   ,IMAGE_TYPE   = CASE -- Update progress   TRIANGLE		 -- CROSS		 -- CIRCLE
												   --WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_EVA_CIRCLE
												   --WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_EVA_TRIANGLE
												   WHEN  (ACTUAL = TOTAL)  AND @Delay = 0  THEN @Status_EVA_CIRCLE
												   WHEN  (ACTUAL = TOTAL)  AND @Delay > 0   THEN @Status_EVA_TRIANGLE
												   WHEN  (ACTUAL < TOTAL)  THEN @Status_EVA_CROSS
												   ELSE '' 	   END

				   END 
	
				END
			ELSE
				BEGIN ---- Check as of with current date  
				   BEGIN --================================== -Data as OF ============================== 
					  INSERT INTO  #T_DATA_AS_OF ( DATE_AS_OF, ACTUAL,  TOTAL,TARGET_DATE_TO)
					  SELECT  GETDATE() AS DATE_AS_OF,ALL_ASSET_TOTAL AS ACTUAL, ALL_ASSET_TOTAL AS TOTAL,
					   CONVERT(DATETIME,CONCAT(CONVERT(VARCHAR(8),TARGET_DATE_TO,112),' ','23:59:59' )) AS TARGET_DATE_TO
					  FROM #T_STOCK_TAKE_H

					-- Update Actual scan
					 UPDATE #T_DATA_AS_OF
						 SET ACTUAL= ISNULL(( SELECT ACTUAL_SCAN
												 FROM
														  ( SELECT ISNULL(SUM(ACTUAL_SCAN),0) AS ACTUAL_SCAN ,STOCK_TAKE_KEY
															  FROM #T_STOCK_CALENDAR
														--	  WHERE END_PLAN <=GETDATE() 
															  GROUP BY STOCK_TAKE_KEY
														  )dt
													  ),0)

					  UPDATE #T_DATA_AS_OF
						 SET TOTAL= ISNULL(( SELECT TOTAL_ASSET
												 FROM
														  ( SELECT ISNULL(SUM(TOTAL_ASSET),0) AS TOTAL_ASSET ,STOCK_TAKE_KEY
															  FROM #T_STOCK_CALENDAR
															  
														  -- Comment for new condition by Pawares M. 20180617	  
													      -- WHERE END_PLAN <=GETDATE()
														  
														  -- New condition for Accum Plan as of today by Pawares M. 20180617
															 WHERE CAST(END_PLAN AS date) <= CAST(GETDATE() AS date) 
															  GROUP BY STOCK_TAKE_KEY
														  )dt
													  ),0)

						-- Update Max scan date
						 UPDATE #T_DATA_AS_OF
							 SET MAX_SCAN_DATE= ( SELECT CASE 
							                             WHEN (ACTUAL < TOTAL) and (MAX_SCAN_DATE < GETDATE()) THEN GETDATE()
														 ELSE MAX_SCAN_DATE END
											 FROM
												  (SELECT MAX(MAX_SCAN_DATE) AS MAX_SCAN_DATE ,STOCK_TAKE_KEY
													  FROM #T_STOCK_CALENDAR
													--  WHERE END_PLAN <=GETDATE() 
													  GROUP BY STOCK_TAKE_KEY
												  )dt
											  )
						/* set class name based on SV status (select the worst stuats)
						       @Status_Complete
								@Status_Delay
								@Status_Complete_WithDelay
								@Status_OnProgress
								@Status_Complete_WithLose
								 @Status_Not_Yet_Scan*/
								
						DECLARE @CLASS_ALL AS Varchar(50) = '';
						DECLARE @IMAGE_TYPE AS Varchar(20) = '';
						PRINT '02'
						
						SELECT @CLASS_ALL =  CASE WHEN count(CLASS_NAME) > 0 THEN @Status_Complete ELSE @CLASS_ALL END,
						       @IMAGE_TYPE = CASE WHEN count(CLASS_NAME) > 0 THEN @Status_EVA_CIRCLE ELSE @IMAGE_TYPE END
						FROM #T_STOCK_CALENDAR
						WHERE CLASS_NAME in (@Status_Complete)

						SELECT @CLASS_ALL =  CASE WHEN count(CLASS_NAME) > 0 THEN @Status_OnProgress ELSE @CLASS_ALL END,
						         @IMAGE_TYPE = CASE WHEN count(CLASS_NAME) > 0 THEN @Status_EVA_CIRCLE ELSE @IMAGE_TYPE END
						FROM #T_STOCK_CALENDAR
						WHERE CLASS_NAME in (@Status_OnProgress,@Status_Not_Yet_Scan)
						
						SELECT @CLASS_ALL =  CASE WHEN count(CLASS_NAME) > 0 THEN @Status_Delay ELSE @CLASS_ALL END,
						         @IMAGE_TYPE = CASE WHEN count(CLASS_NAME) > 0 THEN @Status_EVA_TRIANGLE ELSE @IMAGE_TYPE END
						FROM #T_STOCK_CALENDAR
						WHERE CLASS_NAME in (@Status_Delay,@Status_Complete_WithDelay)

						SELECT @CLASS_ALL =  CASE WHEN count(CLASS_NAME) > 0 THEN @Status_Complete_WithLose ELSE @CLASS_ALL END,
						       @IMAGE_TYPE = CASE WHEN count(CLASS_NAME) > 0 THEN @Status_EVA_CROSS ELSE @IMAGE_TYPE END
						FROM #T_STOCK_CALENDAR
						WHERE CLASS_NAME = @Status_Complete_WithLose
						
						PRINT '03'

						 UPDATE #T_DATA_AS_OF SET CLASS_NAME = @CLASS_ALL
					--	 select 'X',CONCAT(Cast(Cast(CAST(ACTUAL AS decimal)/ CAST(TOTAL AS decimal)*100 as decimal(18,2)) as varchar(6)) , '%' ) , ACTUAL,TOTAL  from #T_DATA_AS_OF
						-- Update progress
					--	return
		 				 UPDATE #T_DATA_AS_OF
						 SET PROGRESS_BAR_PERCEN =CASE 
												  WHEN  TOTAL=0 THEN '0.00%'
												 ELSE CONCAT(Cast(Cast(CAST(ACTUAL AS decimal)/ CAST(TOTAL AS decimal)*100 as decimal(18,2)) as varchar(8)) , '%' ) END
								  --,CLASS_NAME   = CASE -- Update progress  On progress,Complete , Complete with Delay , Complete with Lose
										--		   WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)	THEN @Status_Complete
										--		   WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)		THEN @Status_Delay
										--		   WHEN  (ACTUAL < TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)	THEN @Status_OnProgress
										--		   WHEN  (ACTUAL < TOTAL)  AND  MAX_SCAN_DATE IS NULL				THEN @Status_OnProgress
										--		    WHEN  (ACTUAL < TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)		THEN @Status_Delay
										--   END
					

					END

				   BEGIN -- =============================== Data Over all =================================
				   PRINT '04'
				   declare @IS_SEND_REQUEST varchar(1)
				   
				   /* Display EVA even not submit request*/
				   --SELECT top 1  @IS_SEND_REQUEST=IS_SEND_REQUEST FROM #T_STOCK_CALENDAR
				   --WHERE IS_SEND_REQUEST='N'

				   --IF (@IS_SEND_REQUEST is null)
				   --begin 
				       set @IS_SEND_REQUEST='Y'
				   --end 

						  INSERT INTO #T_DATA_OVER_ALL(ACTUAL,TOTAL,TARGET_DATE_TO,MAX_SCAN_DATE)
						  SELECT  ACTUAL, TOTAL,TARGET_DATE_TO,MAX_SCAN_DATE
						  FROM #T_DATA_AS_OF

						UPDATE #T_DATA_OVER_ALL
						 SET TOTAL= ISNULL(( SELECT TOTAL_ASSET
												 FROM
														  ( SELECT ISNULL(SUM(TOTAL_ASSET),0) AS TOTAL_ASSET ,STOCK_TAKE_KEY
															  FROM #T_STOCK_CALENDAR
															  GROUP BY STOCK_TAKE_KEY
														  )dt
													  ),0)



						UPDATE #T_DATA_OVER_ALL SET CLASS_NAME = @CLASS_ALL,IMAGE_TYPE = @IMAGE_TYPE
						PRINT '01'
						-- Update progress
						 UPDATE #T_DATA_OVER_ALL
						 SET PROGRESS_BAR_PERCEN =CASE WHEN TOTAL = 0 THEN '0.00%'
						                          ELSE CONCAT(Cast(Cast(CAST(ACTUAL AS decimal)/ CAST(TOTAL AS decimal)*100 as decimal(18,2)) as varchar(8)) , '%' ) END
								  --,CLASS_NAME   = CASE -- Update progress  On progress,Complete , Complete with Delay , Complete with Lose
										--		   WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_Complete
										--		   WHEN  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_Delay
										--		   WHEN  (ACTUAL < TOTAL)  AND  (MAX_SCAN_DATE <= TARGET_DATE_TO)   THEN @Status_OnProgress 
										--		   WHEN  (ACTUAL < TOTAL)  AND  (MAX_SCAN_DATE IS NULL )   THEN @Status_OnProgress
										--		    WHEN  (ACTUAL < TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_Delay 
										--		   END
							   --,IMAGE_TYPE   = CASE -- Update progress   TRIANGLE		 -- CROSS		 -- CIRCLE
										--		   WHEN ( @IS_SEND_REQUEST='Y' ) AND  (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE <= TARGET_DATE_TO)  THEN @Status_EVA_CIRCLE
										--		   WHEN ( @IS_SEND_REQUEST='Y' ) AND (ACTUAL = TOTAL)  AND (MAX_SCAN_DATE > TARGET_DATE_TO)   THEN @Status_EVA_TRIANGLE
										--		   WHEN ( @IS_SEND_REQUEST='Y' ) AND (ACTUAL < TOTAL)  THEN @Status_EVA_CROSS
										--		   ELSE '' 	 END
				   END 
			END 
			
		
		--return For lender calendar
			--------- 1. Data As Of
 				  SELECT FORMAT(DATE_AS_OF,'dd-MMM-yyyy') AS DATE_AS_OF , ACTUAL,  TOTAL ,PROGRESS_BAR_PERCEN, CLASS_NAME
				  FROM  #T_DATA_AS_OF
			------   2. Over all 
				 SELECT   ACTUAL,  TOTAL ,PROGRESS_BAR_PERCEN, CLASS_NAME,IMAGE_TYPE
				 FROM #T_DATA_OVER_ALL 
			
			 --------- 3. For lender calendar
				 SELECT	 title,[description], FORMAT(START_PLAN,'yyyy/MM/dd') AS [start] , FORMAT(END_PLAN,'yyyy/MM/dd') AS [end]
					 , URL_LINK as [url],class_name as className,'' as borderColor, IS_SEND_REQUEST
				  FROM #T_STOCK_CALENDAR
				  --WHERE ((@ASSET_LOCATION = 'I') AND (@ISFAADMIN='N'))
				  --      OR (@ISFAADMIN='Y')

				  --WHERE (    (@ISFAADMIN='Y')  OR 
						--((@ISFAADMIN='N') AND ( EMP_CODE = @EMP_CODE))
						--OR
						--((@ISFAADMIN='N') AND EMP_CODE in (select SV.EMP_CODE from FN_FD0COSTCENTERLIST(@EMP_CODE) R Inner join TB_M_SV_COST_CENTER SV
      --                                                     on R.COST_CODE = sv.COST_CODE
                          --     where exists (select '1' from TB_M_SYSTEM S where S.CATEGORY = 'SYSTEM_CONFIG'
                          --and S.SUB_CATEGORY = 'STOCK_TAKE_MANAGER' and S.CODE = R.APPV_ROLE and S.ACTIVE_FLAG = 'Y')
                          --and  R.emp_code = @EMP_CODE))


				    -- )---- only a
			---------- 4. Date From
				SELECT FORMAT(TARGET_DATE_FROM,'yyyy/MM/dd') as TARGET_DATE_FROM,FORMAT(TARGET_DATE_TO,'yyyy/MM/dd')as TARGET_DATE_TO 
				FROM TB_R_STOCK_TAKE_H
				WHERE YEAR = @YEAR	AND ROUND = @ROUND	AND ASSET_LOCATION = @ASSET_LOCATION

			
		END 

		BEGIN /* ==============================  Clear temp table ============================== */
				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_H') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_H
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D
				END

				IF OBJECT_ID('tempdb..#T_STOCK_TAKE_D_PER_SV') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_TAKE_D_PER_SV
				END

				---- Temp data for retrun to screen 
				IF OBJECT_ID('tempdb..#T_STOCK_CALENDAR') IS NOT NULL
				BEGIN
						DROP TABLE #T_STOCK_CALENDAR
				END

				IF OBJECT_ID('tempdb..#T_DATA_AS_OF') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_AS_OF
				END

				IF OBJECT_ID('tempdb..#T_DATA_OVER_ALL') IS NOT NULL
				BEGIN
						DROP TABLE #T_DATA_OVER_ALL
				END
		END
 
END
GO
