DROP PROCEDURE [dbo].[sp_WFD01270_SubmitApproverData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 09/02/2017
-- Description:	Insert Fixed assets screen
-- =============================================
--declare @ERROR_STATUS VARCHAR(10) exec sp_WFD01270_SubmitApproverData 'C2017/0010','1',null,null,null,null,null,null,null,1,10,null,@TOTAL_ITEM out  
CREATE PROCEDURE [dbo].[sp_WFD01270_SubmitApproverData]
(
	@DOC_NO					T_DOC_NO			= NULL,		--Cost Code	
	@INDX					INT					= NULL,		--Cost Code	
	@ROLE				    VARCHAR(50)			= NULL,		--Sub Type	
	@EMP_CODE				VARCHAR(8)			= NULL,		--Cost Code	
	@DIVISION				VARCHAR(3)			= NULL,		--Cost Code		
	@REQUEST_TYPE			VARCHAR(1)			= NULL,		--Cost Code	
	@USER_BY				T_SYS_USER			= NULL,		--Cost Code	
	@GUID					VARCHAR(50) 
)
AS
BEGIN

	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_APPR WITH (NOLOCK) WHERE DOC_NO = @DOC_NO AND INDX = @INDX AND APPR_STATUS = 'A')
	BEGIN
		RETURN ;
	END

	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_APPR WITH (NOLOCK) WHERE DOC_NO = @DOC_NO AND INDX = @INDX AND EMP_CODE = @EMP_CODE AND EMP_NAME IS NOT NULL) -- Protected Org.Code is changed.
	BEGIN
		RETURN ;
	END

	UPDATE R
		SET ACTUAL_ROLE		= ISNULL(E.[ROLE], R.[APPR_ROLE]), -- when emp code is null
		EMP_CODE			= @EMP_CODE,
		EMP_TITLE			= E.EMP_TITLE,
		EMP_NAME			= E.EMP_NAME,
		EMP_LASTNAME		= E.EMP_LASTNAME,
		EMAIL				= E.EMAIL,
		POS_CODE			= ISNULL(E.POST_CODE, ''), -- when emp code is null
		POS_NAME			= E.POST_NAME,
		DEPT_CODE			= E.DEPT_CODE,
		DEPT_NAME			= E.DEPT_NAME,
		ORG_CODE			= E.ORG_CODE,
		UPDATE_DATE			= GETDATE(),
		UPDATE_BY			= @USER_BY 
	FROM	TB_R_REQUEST_APPR R WITH (NOLOCK)
	LEFT	JOIN --Fix Incident when change to null
			(	SELECT	[ROLE],  EMP_TITLE, EMP_NAME, EMP_LASTNAME, EMAIL, POST_CODE, POST_NAME, E.ORG_CODE,
						DEPT_CODE, DEPT_NAME
				FROM	TB_M_EMPLOYEE E
				LEFT	JOIN 
						TB_M_ORGANIZATION O 
				ON		E.ORG_CODE			= O.ORG_CODE 	
				WHERE	EMP_CODE = @EMP_CODE) E 
	ON		1 = 1
	WHERE	R.DOC_NO	= @DOC_NO AND
			R.INDX		= @INDX
	
	
END


GO
