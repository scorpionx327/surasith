DROP PROCEDURE [dbo].[sp_WFD01250_AddApproveH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 10-03-2017
-- Description:	Add approve h data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_AddApproveH]
	@APPRV_ID	int ,
	@COMPANY		T_COMPANY,
	@REQUEST_FLOW_TYPE	varchar(2) ,
	@ASSET_CLASS	varchar(5) ,
	@CREATE_BY	varchar(8) 
AS
	DECLARE @MESSAGE NVARCHAR(250);
BEGIN
	SET NOCOUNT ON;

	IF ( SELECT COUNT(1) FROM TB_M_APPROVE_H WHERE COMPANY = @COMPANY AND REQUEST_FLOW_TYPE=@REQUEST_FLOW_TYPE and ASSET_CLASS=@ASSET_CLASS ) > 0
	BEGIN

		SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MSTD0010AERR'
									

			SET @MESSAGE = '[CUSTOM]' + 'MSTD0010AERR|' + REPLACE(
								REPLACE(
									REPLACE(@MESSAGE, '{0}', 'Request Flow Type')
									, '{1}', @REQUEST_FLOW_TYPE + ' and Fix Asset Category = ' 
										+ @ASSET_CLASS)
								, '{2}', 'TB_M_APPROVE_H');
			
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;								

	END

	DECLARE @REQUEST_TYPE VARCHAR(1)
	SET @REQUEST_TYPE = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_FLOW_TYPE',@REQUEST_FLOW_TYPE)


	SET @APPRV_ID = NEXT VALUE FOR APPROVE_ID;

	INSERT INTO [dbo].[TB_M_APPROVE_H]
			   (APPRV_ID
			   ,COMPANY
			   ,REQUEST_FLOW_TYPE
               ,REQUEST_TYPE
               ,ASSET_CLASS
               ,CREATE_DATE
               ,CREATE_BY
               ,UPDATE_DATE
               ,UPDATE_BY)
			   VALUES
			   (
				@APPRV_ID
				, @COMPANY
				, @REQUEST_FLOW_TYPE
				, @REQUEST_TYPE
				, @ASSET_CLASS
				, GETDATE()
				, @CREATE_BY
				, GETDATE()
				, @CREATE_BY
				)  
	
	 
	DELETE FROM TB_M_APPROVE_D WHERE APPRV_ID=@APPRV_ID 

	
	select @APPRV_ID

END
GO
