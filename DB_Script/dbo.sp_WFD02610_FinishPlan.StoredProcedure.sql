DROP PROCEDURE [dbo].[sp_WFD02610_FinishPlan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-01-31
-- Description:	Finish stock take plane
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02610_FinishPlan]
	-- Add the parameters for the stored procedure here
	@YEAR				varchar(4)
	, @ROUND			varchar(2)
	, @ASSET_LOCATION	varchar(1)
	, @REMARK			nvarchar(500)
	, @TOTAL_NOT_CHECK_ASSETS	int
	, @USER				T_USER
AS
	DECLARE @transactionName nvarchar(20) = 'finishPlanTransaction'
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN @transactionName
	BEGIN TRY

		IF @TOTAL_NOT_CHECK_ASSETS > 0
		BEGIN

			IF @REMARK IS NULL OR LTRIM(RTRIM(@REMARK)) = ''
			BEGIN

				RAISERROR('Remark is required.', 400, 1);

			END
		END

		UPDATE TB_R_STOCK_TAKE_H
			SET PLAN_STATUS='F' -- set status to F : Finish plan
			   , REMARK=@REMARK
				, UPDATE_BY=@USER, UPDATE_DATE=GETDATE()
		WHERE YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION

		UPDATE TB_M_ASSETS_H 
			SET LAST_SCAN_DATE = d.SCAN_DATE, UPDATE_DATE=GETDATE(), UPDATE_BY=@USER
		FROM TB_R_STOCK_TAKE_D d 
		WHERE TB_M_ASSETS_H.ASSET_NO=d.ASSET_NO and YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION

		COMMIT TRAN @transactionName
	END TRY
	BEGIN CATCH  

		ROLLBACK TRAN @transactionName
		DECLARE @ErrorMessage NVARCHAR(4000);  
		DECLARE @ErrorSeverity INT;  
		DECLARE @ErrorState INT;  
  
		SELECT   
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE();  
  
		-- Use RAISERROR inside the CATCH block to return error  
		-- information about the original error that caused  
		-- execution to jump to the CATCH block.  
		RAISERROR (@ErrorMessage, -- Message text.  
				   @ErrorSeverity, -- Severity.  
				   @ErrorState -- State.  
				   );  
	END CATCH; 

	

END




GO
