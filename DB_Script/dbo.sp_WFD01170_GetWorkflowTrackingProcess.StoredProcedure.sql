DROP PROCEDURE [dbo].[sp_WFD01170_GetWorkflowTrackingProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01170_GetWorkflowTrackingProcess]
(
	@DOC_NO	T_DOC_NO
)
	
AS
BEGIN
	SELECT INDX,			
			DOC_NO,			
			CASE	WHEN A.EMP_CODE IS NOT NULL THEN A.EMP_CODE
					ELSE A.APPR_ROLE 
					END EMP_CODE,

			CASE	WHEN A.EMP_CODE IS NOT NULL THEN CONCAT(E.EMP_NAME , ' ' , LEFT(E.EMP_LASTNAME,1), '.') 
					ELSE MS.[VALUE] 
					END  EMP_NAME, 
			
			A.APPR_ROLE AS [ROLE],			
			APPR_STATUS,
			CASE WHEN APPR_STATUS = 'A' THEN 'Approved' 
				 WHEN APPR_STATUS = 'W' THEN 'Wait' ELSE '-' END APPR_STATUS_NAME,	
				 			
			CASE WHEN APPR_STATUS = 'A' THEN 'apprv'
				 WHEN APPR_STATUS = 'W' THEN 'current' ELSE '' END APPR_STATUS_CSS
	FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)
	LEFT	JOIN
			TB_M_SYSTEM MS
	ON		MS.CATEGORY = 'RESPONSIBILITY' AND MS.SUB_CATEGORY = 'ROLE_CODE' AND MS.CODE = A.APPR_ROLE
	LEFT	JOIN 
			TB_M_EMPLOYEE E ON A.EMP_CODE		= E.SYS_EMP_CODE 
	WHERE	DOC_NO = @DOC_NO
	ORDER	BY INDX

END
GO
