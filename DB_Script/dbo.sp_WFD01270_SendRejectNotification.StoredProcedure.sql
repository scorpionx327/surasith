DROP PROCEDURE [dbo].[sp_WFD01270_SendRejectNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_SendRejectNotification]
@DocNo		VARCHAR(10),
@Approver	VARCHAR(8)
AS
BEGIN
	-- Get Requestor
	-- Get Requestor
	DECLARE @Requestor VARCHAR(8), @RequestType	VARCHAR(1)
	SELECT	@Requestor = EMP_CODE, 
			@RequestType = REQUEST_TYPE
	FROM	TB_R_REQUEST_H WHERE DOC_NO = @DocNo

	-- Get FA

	-- Send Email To Approver
	DECLARE @TO VARCHAR(255),		@CC VARCHAR(255),
			@TITLE VARCHAR(255),	@MSG VARCHAR(MAX), @FOOTER VARCHAR(250),
			@Name	VARCHAR(200)

	SELECT	@TO = EMAIL,  @Name = CONCAT(EMP_TITLE,' ', EMP_NAME,' ', LEFT(EMP_LASTNAME,1), '.')
	FROM	TB_M_EMPLOYEE 
	WHERE	EMP_CODE = @Requestor


	DECLARE @FACode VARCHAR(3)
	SET @FACode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','FA_CODE','FA')

	IF (@Approver = @FACode)
	BEGIN
		SET		@CC =  dbo.fn_GetEmailListofAdministrator() -- Include Delegate
	END
	ELSE
	BEGIN
		SELECT	@CC = EMAIL
		FROM	TB_M_EMPLOYEE 
		WHERE	EMP_CODE = @Approver
	END

	DECLARE @Req VARCHAR(100)

	SET @Req = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_TYPE',@RequestType)
	
	SET @TITLE	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','WFD01270_REJECT')
	SET @MSG	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','WFD01270_REJECT')
	SET @FOOTER = dbo.fn_GetSystemMaster('NOTIFICATION','EMAIL','FOOTER')

	SET @TITLE = dbo.fn_StringFormat(@TITLE, @DocNo )
	SET @MSG = dbo.fn_StringFormat(@MSG, CONCAT(@Req,'|',@RequestType, '|', @DocNo,'|', @Name))

	EXEC sp_Common_InsertNotification 
			@TO, @CC, NULL, @TITLE, @MSG, @FOOTER, 'I', NULL, NULL,'N', 'N', 'WFD01270', @DocNo, @Requestor

END


GO
