DROP PROCEDURE [dbo].[sp_WFD02640_ValidateSubmitStockTakingWithDelay]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
User by : Nuttapon P.
Create Date : 2017-10-17 
*/

--exec [sp_WFD02640_ValidateSubmitStockTakingWithDelay] null,'bd5a2917-5767-46ea-b6c0-fc8dd822eecc'
CREATE PROCEDURE [dbo].[sp_WFD02640_ValidateSubmitStockTakingWithDelay]
(	
	    @DocNo		T_DOC_NO,
		@GUID		T_GUID
)
AS
BEGIN
 DECLARE	@Cnt INT,  @CCList VARCHAR(MAX), @Company T_COMPANY
 DECLARE  @Year VARCHAR(4)
 DECLARE  @Round VARCHAR(2)
 DECLARE  @AssetLocation VARCHAR(1)
 DECLARE  @CostCode T_COST_CODE
 DECLARE  @SV_EMP_CODE	T_SYS_USER
 SET @Cnt=0;
  IF EXISTS( SELECT DOC_NO
					 FROM TB_R_REQUEST_STOCK S
					 WHERE S.DOC_NO =@DocNo )
  BEGIN
			-- Incase Resumit
			 
             SELECT	@Cnt=COUNT(1)  FROM TB_T_REQUEST_STOCK T
			 -- 2017-06-08 Surasith
			 -- Add Complete Flag and Selected Condition for support when system should check loss condition
			 WHERE  DOC_NO = @DocNo AND COMPLETE_FLAG ='Y' AND SELECTED='Y'
			 AND  COMMENT IS NULL 

			 -- Add Complete Flag and Selected Condition for support when system should check loss condition
			SELECT	@CCList = COALESCE(@CCList + ',', '') + COST_CODE 
			FROM	TB_T_REQUEST_STOCK T
			WHERE	DOC_NO = @DocNo AND COMPLETE_FLAG ='Y' AND SELECTED='Y'
					AND  COMMENT IS NULL
			GROUP	BY COST_CODE

	
			 SELECT @Company	= T.COMPANY,
					@Year		= T.[YEAR],
					@Round		= T.[ROUND] ,
					@AssetLocation = T.ASSET_LOCATION ,
					@CostCode	 = T.COST_CODE,
					@SV_EMP_CODE = T.SV_EMP_CODE  
			FROM	TB_T_REQUEST_STOCK T
			WHERE	DOC_NO		= @DocNo AND 
					COMPLETE_FLAG ='Y' AND 
					SELECTED='Y'
			 AND  COMMENT IS NULL 
		
  END
  ELSE
  BEGIN	    
			 --Incase New   
			SELECT	@Cnt=COUNT(1) FROM TB_T_REQUEST_STOCK T
			WHERE [GUID] = @GUID AND COMPLETE_FLAG ='Y' AND SELECTED='Y'
			AND  COMMENT IS NULL 

			SELECT	@CCList = COALESCE(@CCList + ',', '') + COST_CODE 
			FROM		TB_T_REQUEST_STOCK T
			WHERE [GUID] = @GUID AND COMPLETE_FLAG ='Y' AND SELECTED='Y'
			AND  COMMENT IS NULL 
			GROUP	BY COST_CODE

			
			SELECT	@Company = T.COMPANY, 
					@Year = T.[YEAR],@Round = T.[ROUND] ,@AssetLocation = T.ASSET_LOCATION ,@CostCode = T.COST_CODE,@SV_EMP_CODE = T.SV_EMP_CODE   FROM TB_T_REQUEST_STOCK T
			WHERE [GUID] = @GUID AND COMPLETE_FLAG ='Y' AND SELECTED='Y'
			AND  COMMENT IS NULL 
			
  END

  			--Check Delay or Complete
			
		    DECLARE @Actaul Datetime
			DECLARE @Plan Datetime
			DECLARE @Result Varchar(1)
			DECLARE @CostCodeOutSource Varchar(20)

			IF( @AssetLocation = 'O')
			BEGIN
			
			SELECT  @CostCodeOutSource = COST_CODE 
			FROM TB_R_STOCK_TAKE_D TD1
			WHERE	(TD1.COMPANY = @Company) AND 
					(TD1.[YEAR] = @Year) AND 
					(TD1.[ROUND] = @Round) AND 
					(TD1.ASSET_LOCATION = @AssetLocation) AND 
					(TD1.SV_EMP_CODE = @CostCode)

			-- Modified condition by Pawares M. 20181212
						
			--SELECT  @Actaul = ISNULL(MAX(SCAN_DATE),'1900-01-01') FROM TB_R_STOCK_TAKE_D TD
			--WHERE  ( TD.[YEAR] = @Year) AND (TD.[ROUND] = @Round) AND (TD.ASSET_LOCATION = @AssetLocation) AND (TD.COST_CODE = @CostCodeOutSource)
			
			SELECT  @Actaul = ISNULL(MAX(SCAN_DATE),'1900-01-01') 
			FROM	TB_R_STOCK_TAKE_D TD
			WHERE	(TD.COMPANY			= @Company) AND 
					(TD.[YEAR]			= @Year) AND 
					(TD.[ROUND]			= @Round) AND 
					(TD.ASSET_LOCATION	= @AssetLocation) AND 
					(TD.COST_CODE		= @CostCodeOutSource) AND 
					(TD.SV_EMP_CODE		= @SV_EMP_CODE)
			
			END
			ELSE
			BEGIN
			SELECT  @Actaul = ISNULL(MAX(SCAN_DATE),'1900-01-01') FROM TB_R_STOCK_TAKE_D TD
			WHERE	(TD.COMPANY			= @Company) AND 
					(TD.[YEAR]			= @Year) AND 
					(TD.[ROUND]			= @Round) AND 
					(TD.ASSET_LOCATION	= @AssetLocation) AND 
					(TD.COST_CODE		= @CostCode)
			END

			SELECT	@Plan = Convert(DateTime, Convert(varchar,DATE_FROM) +' '+ TIME_END)  
			FROM	TB_R_STOCK_TAKE_D_PER_SV SV
			WHERE	(SV.COMPANY			= @Company) AND 
					(SV.[YEAR]			= @Year) AND 
					(SV.[ROUND]			= @Round) AND 
					(SV.ASSET_LOCATION	= @AssetLocation) AND 
					(SV.EMP_CODE		= @SV_EMP_CODE )
			

			if(@Actaul > @Plan)
			begin
			 Set @Result = 'D' --Delay 
			end
			Else
			begin
			 Set @Result = 'C' --Complete 
			end


			-- 1 D
  IF (@Cnt = 0 or @Result = 'C')
  BEGIN
	SELECT '' AS COST_CODE --  N is no request
	RETURN 
  END

  SELECT	@CCList AS COST_CODE
END
GO
