DROP PROCEDURE [dbo].[sp_COMSCC_SearchFASUpData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--declare @TOTAL_ITEM int exec sp_COMSCC_SearchData null,null,1,10,2,@TOTAL_ITEM out  
CREATE PROCEDURE [dbo].[sp_COMSCC_SearchFASUpData]
(
	@COMPANY		T_COMPANY		= NULL,
	@FIND			VARCHAR(400)	= NULL,		--Text Search
	@EMP_CODE		VARCHAR(8)		= NULL,		--Employee Code
	@ALL_FLAG		VARCHAR(10)		= NULL,
	@pageNum		INT				= NULL,
	@pageSize		INT				= NULL,
	@sortColumnName VARCHAR(50)		= NULL,
	@orderType		VARCHAR(5)      = NULL,
	@TOTAL_ITEM		INT				output
	
)
AS
BEGIN


	SET @FIND = dbo.fn_GetSearchCriteria(@FIND)

	IF OBJECT_ID('tempdb..#SEARCH_COST_CENTER') IS NOT NULL DROP TABLE #SEARCH_COST_CENTER
	CREATE TABLE #SEARCH_COST_CENTER
	(
		COST_CODE			VARCHAR(20),
		COST_NAME			VARCHAR(240),
		FS_CODE				VARCHAR(20),
		FS_NAME				VARCHAR(200),
		[STATUS]			VARCHAR(1)
	)
	IF @FIND = ''
	BEGIN
		SET @FIND = NULL
	END

		
	INSERT	
	INTO	#SEARCH_COST_CENTER (
			COST_CODE,		COST_NAME, FS_CODE, FS_NAME, [STATUS])
	SELECT	C.COST_CODE,	C.COST_NAME,	FS_EMP.EMP_CODE, CONCAT(FS_EMP.EMP_NAME , ' ' , SUBSTRING(ISNULL(FS_EMP.EMP_LASTNAME,' '),1,1) , '.'), C.[STATUS]
	FROM	TB_M_COST_CENTER C
	INNER	JOIN -- Should be concat
			TB_M_FASUP_COST_CENTER FS
	ON		C.COMPANY = FS.COMPANY AND C.COST_CODE = FS.COST_CODE
	LEFT	JOIN 
			TB_M_EMPLOYEE FS_EMP
	ON		FS.EMP_CODE			= FS_EMP.SYS_EMP_CODE	
	WHERE ((C.COST_CODE LIKE @FIND OR @FIND IS NULL) 
		OR (C.COST_NAME LIKE @FIND OR @FIND IS NULL)
		OR (CONCAT(FS_EMP.EMP_NAME ,' ' , SUBSTRING(ISNULL(FS_EMP.EMP_LASTNAME,' '),1,1), '.') LIKE @FIND OR @FIND IS NULL))
	


 --begin modify by thanapon: fixed paging and sorting
  IF(@sortColumnName IS NULL )
  BEGIN
   SET @sortColumnName ='0';
  END
  IF(@orderType IS NULL )
  BEGIN
   SET @orderType ='asc';
  END
  
  DECLARE @FROM	INT
			  , @TO	INT

	SET @FROM = (@pageSize * (@pageNum-1)) + 1;
	SET @TO = @pageSize * (@pageNum);
  --end modify by thanapon: fixed paging and sorting

	SELECT	
	@TOTAL_ITEM = COUNT(1)
	FROM #SEARCH_COST_CENTER;

  SELECT * FROM (
  	SELECT	
  	  COST_CODE,		COST_NAME,		FS_CODE, FS_NAME, [STATUS]
      ,(CASE WHEN @sortColumnName = '0' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COST_CODE asc)  
             WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_CODE desc)   
             WHEN @sortColumnName = '1' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COST_NAME asc)  
             WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COST_NAME desc)  
             WHEN @sortColumnName = '2' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY FS_NAME asc)
             WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY FS_NAME desc)  
        END) AS ROWNUMBER
  	FROM #SEARCH_COST_CENTER 
  ) d
  WHERE  d.ROWNUMBER >= @FROM and d.ROWNUMBER <= @TO
  ORDER BY d.ROWNUMBER


END
GO
