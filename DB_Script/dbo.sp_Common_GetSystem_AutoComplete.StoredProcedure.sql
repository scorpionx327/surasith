DROP PROCEDURE [dbo].[sp_Common_GetSystem_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetSystem_AutoComplete]
@CATEGORY		VARCHAR(40),
@SUB_CATEGORY	VARCHAR(40),
@CodeFlag		VARCHAR(1),
@keyword		VARCHAR(40) = null
AS
BEGIN
	/*
	Nipon 2019 Sep 17
	invest_reason = 'B'set REMARK <> 'Non-BOI'
	invest_reason = 'NB' set REMARK = 'Non-BOI'																				
	*/
	if(@keyword='B')
	BEGIN
		SELECT	CODE, 
				[VALUE], 
				REMARKS
		FROM	TB_M_SYSTEM
		WHERE	CATEGORY		= @CATEGORY AND
				SUB_CATEGORY	= @SUB_CATEGORY AND
				(	(@CodeFlag = 'Y' AND CODE			LIKE CONCAT(@keyword,'%')) OR
					(@CodeFlag <> 'Y' AND [VALUE]		LIKE CONCAT(@keyword,'%'))
				) 
				AND REMARKS <> 'NB'
				AND ACTIVE_FLAG = 'Y'

		ORDER	BY
				CASE @CodeFlag WHEN 'Y' THEN CODE ELSE [VALUE] END
	END
	ELSE
	BEGIN 
		SELECT	CODE, 
				[VALUE], 
				REMARKS
		FROM	TB_M_SYSTEM
		WHERE	CATEGORY		= @CATEGORY AND
				SUB_CATEGORY	= @SUB_CATEGORY AND
				(	(@CodeFlag = 'Y' AND CODE			LIKE CONCAT(@keyword,'%')) OR
					(@CodeFlag <> 'Y' AND [VALUE]		LIKE CONCAT(@keyword,'%'))
				) AND
				ACTIVE_FLAG = 'Y'
		ORDER	BY
				CASE @CodeFlag WHEN 'Y' THEN CODE ELSE [VALUE] END
	END
END
GO
