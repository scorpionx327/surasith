DROP PROCEDURE [dbo].[sp_WFD02650_AddInvalidStockTakeScan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Add stock take invalid scan data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_AddInvalidStockTakeScan]
	@BARCODE varchar(19) ,
	@EMP_CODE varchar(8) ,
	@COST_CODE varchar(20),
	@SCAN_DATE datetime 
AS
	DECLARE @STOCK_TAKE_KEY	varchar(7)
			, @YEAR			varchar(4)
			, @ROUND		varchar(2)
			, @ASSET_LOCATION	varchar(1)
			, @ASSET_NO		varchar(15)
			, @COST_NAME varchar(240) 
			, @PLANT_CD varchar(3) 
			, @PLANT_NAME varchar(240) 
			, @COMPANY T_COMPANY
			, @ASSET_SUB T_ASSET_SUB
BEGIN
	SET NOCOUNT ON;

	--SELECT top 1 @COST_NAME = COST_NAME FROM TB_M_COST_CENTER where COST_CODE = @COST_CODE;

	SELECT top 1
		@COMPANY=COMPANY
		,@STOCK_TAKE_KEY  = STOCK_TAKE_KEY
		, @YEAR = YEAR
		, @ROUND = ROUND
		, @ASSET_LOCATION = ASSET_LOCATION
		, @ASSET_NO = ASSET_NO
		, @ASSET_SUB = ASSET_SUB
	FROM TB_R_STOCK_TAKE_D
	WHERE BARCODE=@BARCODE 
		and STOCK_TAKE_KEY in (SELECT STOCK_TAKE_KEY FROM TB_R_STOCK_TAKE_H  WHERE PLAN_STATUS in ('C','S'))
	order by STOCK_TAKE_KEY DESC ;


	if @STOCK_TAKE_KEY IS NULL
	BEGIN
		
			--RAISERROR ('Cannot find this barcode', -- Message text.
			--	   16, -- Severity.
			--	   1 -- State.
			--	   );
			PRINT 'x'

	END
	ELSE
	BEGIN
	    
		--Start Fix 05/06/2017 Incident
		------SELECT top 1 @COST_NAME = CASE WHEN @ASSET_LOCATION = 'O' THEN D.SUPPLIER_NAME ELSE D.COST_NAME END
		------FROM TB_R_STOCK_TAKE_D D
		------WHERE STOCK_TAKE_KEY = @STOCK_TAKE_KEY and D.ASSET_NO = @ASSET_NO          
		if (@ASSET_LOCATION = 'I')
		begin
			SELECT top 1 @COST_NAME = COST_NAME FROM TB_M_COST_CENTER where COST_CODE = @COST_CODE;
		end
		--else
		--begin
		--	SELECT top 1 @COST_NAME = COST_NAME FROM TB_M_ASSETS_H where COST_NAME = @COST_CODE and INHOUSE_FLAG = 'N';
		--end
		--End Fix


		IF (NOT EXISTS(select 'X' from [dbo].[TB_R_STOCK_TAKE_INVALID_CHECKING] where STOCK_TAKE_KEY = @STOCK_TAKE_KEY and ASSET_NO = @ASSET_NO and EMP_CODE = @EMP_CODE))
		BEGIN

		print '555555555555'
		INSERT INTO [dbo].[TB_R_STOCK_TAKE_INVALID_CHECKING]
			   ([COMPANY]
			   ,[STOCK_TAKE_KEY]
			   ,[YEAR]
			   ,[ROUND]
			   ,[ASSET_LOCATION]
			   ,[ASSET_NO]
			   ,[ASSET_SUB]
			   ,[EMP_CODE]
			   ,[COST_CODE]
			   ,[COST_NAME]
			   ,[PLANT_CD]
			   ,[PLANT_NAME]
			   ,[SCAN_DATE])
		 VALUES
			   (@COMPANY
			   ,@STOCK_TAKE_KEY
			   ,@YEAR
			   ,@ROUND
			   ,@ASSET_LOCATION
			   ,@ASSET_NO
			   ,@ASSET_SUB
			   ,@EMP_CODE
			   ,@COST_CODE
			   ,@COST_NAME
			   ,@PLANT_CD
			   ,@PLANT_NAME
			   ,@SCAN_DATE)
		END

	END

	

END
GO
