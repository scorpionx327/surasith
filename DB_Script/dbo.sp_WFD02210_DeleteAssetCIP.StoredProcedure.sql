DROP PROCEDURE [dbo].[sp_WFD02210_DeleteAssetCIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_DeleteAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f14',null,''
CREATE PROCEDURE [dbo].[sp_WFD02210_DeleteAssetCIP]
(	
	@GUID					VARCHAR(50)			= NULL,		--GUID
	@DOC_NO					VARCHAR(10)			= NULL,		--DOC NO	
	@ASSET_NO				VARCHAR(15)			= NULL,		--Asset no		
	@COST_CODE				VARCHAR(15)			= NULL,		--DOC NO
	@USER_BY				VARCHAR(8)			= NULL,		--DOC NO
	@UPDATE_DATE			VARCHAR(30)			= NULL		--DOC NO
)
AS
BEGIN
	
	DECLARE @COUNT_TEMP		INT
	DECLARE @ROW_DELETE INT
	DECLARE @TEMP_GUID		VARCHAR(50)


	DELETE FROM TB_T_SELECTED_ASSETS 
	WHERE ASSET_NO			= @ASSET_NO
		AND ( GUID			= @GUID )
		AND ( COST_CODE		= @COST_CODE )

	UPDATE	TB_R_REQUEST_CIP 
	SET		DELETE_FLAG		= 'Y'
	WHERE	ASSET_NO			= @ASSET_NO
			AND DOC_NO			= @DOC_NO
			AND ( COST_CODE		= @COST_CODE )
END


GO
