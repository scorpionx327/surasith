DROP PROCEDURE [dbo].[sp_BFD02110_UpdateCIPRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  For update Request History Table : CIP Request
- ============================================= SFAS =============================================*/
CREATE PROCEDURE [dbo].[sp_BFD02110_UpdateCIPRequest]
(
	 @UserID		VARCHAR(8),
	 @ERROR_MESSAGE	VARCHAR(MAX) OUT

)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

-- Create temp TEMP_ASSETS 
IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_ASSETS
END

-- 1 Create structure of temp table 
 	 SELECT ASSET_NO,ENTRY_PERIOD ,ASSET_TYPE ,PROCESS_STATUS
	 INTO #TEMP_ASSETS 
	 FROM TB_M_ASSETS_H
	 WHERE 1<>1

	INSERT INTO  #TEMP_ASSETS(ASSET_NO,ENTRY_PERIOD,ASSET_TYPE ,PROCESS_STATUS)
	SELECT  AssetH.ASSET_NO,S_Asset.ENTRY_PERIOD ,AssetH.ASSET_TYPE ,AssetH.PROCESS_STATUS
	FROM    TB_M_ASSETS_H AssetH 
	INNER JOIN  ( SELECT ASSET_NO,ENTRY_PERIOD 
						FROM TB_S_ASSETS_H
						WHERE UPPER(ASSET_TYPE) ='CAPITALIZED'
						) S_Asset   
	 ON AssetH.ASSET_NO=S_Asset.ASSET_NO
	 WHERE AssetH.PROCESS_STATUS='CC'


  --2 Update history of request																	
        UPDATE AssetHis
		SET	 AssetHis.ENTRY_PERIOD =	CONVERT(DATE, GETDATE()) -- Change Spec 2017-06-14 Oracle not send Entry Period incase CIP changed to Cap (Surasith T.)
										/* CASE 
										WHEN T_Assets.ENTRY_PERIOD IS NOT NULL THEN CONVERT(DATE,T_Assets.ENTRY_PERIOD,106)
										ELSE NULL END */
			,AssetHis.POSTING_DATE=GETDATE()
			,AssetHis.ORACLE_UPDATE_STATUS='Y'
			,AssetHis.UPDATE_BY=@UserID
			,AssetHis.UPDATE_DATE=GETDATE() 
		FROM TB_R_ASSET_HIS AssetHis
		INNER JOIN #TEMP_ASSETS T_Assets  ON AssetHis.ASSET_NO=T_Assets.ASSET_NO
		WHERE       AssetHis.ORACLE_UPDATE_STATUS='N'
			  AND	 AssetHis.REQUEST_TYPE='C' 
			  AND	 AssetHis.[STATUS] NOT IN ('90','99') 

	 
 --3 Update process status back to aviable to 
	    UPDATE AssetH
		SET	 AssetH.PROCESS_STATUS = NULL 
	    FROM TB_M_ASSETS_H AssetH
		INNER JOIN #TEMP_ASSETS T_Assets  ON AssetH.ASSET_NO=T_Assets.ASSET_NO
	
	-- Create temp TEMP_ASSETS 
	IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
	BEGIN
			DROP TABLE #TEMP_ASSETS
	END

	RETURN @Error ;
END TRY
BEGIN CATCH
			-- Create temp TEMP_ASSETS 
		IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
		BEGIN
				DROP TABLE #TEMP_ASSETS
		END
			PRINT concat('ERROR Message:',ERROR_MESSAGE())
			  SET @ERROR_MESSAGE=ERROR_MESSAGE();
				RETURN 1;
END CATCH










GO
