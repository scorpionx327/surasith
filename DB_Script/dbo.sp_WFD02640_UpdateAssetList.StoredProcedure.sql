DROP PROCEDURE [dbo].[sp_WFD02640_UpdateAssetList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Uten S.
-- Create date: 14/04/2017
-- Description:	update selected 
-- =============================================
-- Author:		Nuttapo P.
-- Create date: 25/10/2017
-- Description:	update SV_EMP_CODE 
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_UpdateAssetList]
	@COMPANY		T_COMPANY,
	@DOC_NO			T_DOC_NO,
	@YEAR			varchar(4),
	@ROUND			varchar(2),
	@ASSET_LOCATION varchar(1),
	@COST_CODE		T_COST_CODE,
	@USER_LOGIN		T_SYS_USER,
	@GUID			T_GUID,
	@COMPLETE_FLAG	VARCHAR(1),
	@SELECTED		varchar(1),
	@SV_EMP_CODE	T_SYS_USER
AS
BEGIN


DECLARE @CntNoScan INT


IF(@ASSET_LOCATION='I')
BEGIN
    SELECT  @CntNoScan = COUNT(*)
	FROM	TB_R_STOCK_TAKE_D
	WHERE	COMPANY			= @COMPANY AND
			[YEAR]			= @YEAR AND 
			[ROUND]			= @ROUND AND 
			ASSET_LOCATION	= @ASSET_LOCATION AND 
			COST_CODE		= @COST_CODE AND 
			CHECK_STATUS	= 'N'
	
END
ELSE
BEGIN-- OUT SOURCE --> @COST_CODE IS MEAN supplier code
    SELECT  @CntNoScan = COUNT(*)
	FROM	TB_R_STOCK_TAKE_D
	WHERE	COMPANY			= @COMPANY AND
			[YEAR]			= @YEAR AND 
			[ROUND]			= @ROUND AND 
			ASSET_LOCATION	= @ASSET_LOCATION AND 
			SV_EMP_CODE		= @COST_CODE AND 
			CHECK_STATUS	= 'N'
END

IF(@CntNoScan IS NOT NULL)
BEGIN
         IF(@CntNoScan >0)
		 BEGIN
		    SET @COMPLETE_FLAG='N'
		 END
		 ELSE
		 BEGIN
		   SET @COMPLETE_FLAG='Y'
		 END
END
ELSE
BEGIN
   SET @COMPLETE_FLAG='N'
END


IF NOT EXISTS(SELECT 1 
		FROM	TB_T_REQUEST_STOCK 
		WHERE	COMPANY = @COMPANY AND  
				[GUID] = @GUID AND
				COST_CODE=@COST_CODE)
BEGIN

		INSERT INTO [dbo].[TB_T_REQUEST_STOCK]
				   (COMPANY, [DOC_NO]   ,[YEAR]
				   ,[ROUND]   ,[ASSET_LOCATION]
				   ,[COST_CODE]		   ,[COMMENT]
				   ,[ATTACHMENT]	   ,[CREATE_DATE]
				   ,[CREATE_BY]			   ,[GUID]
				   ,[COMPLETE_FLAG]	   ,[SELECTED]
				   ,[SV_EMP_CODE])
			 VALUES
				   (@COMPANY, @DOC_NO		  ,@YEAR	   ,@ROUND			 
				   ,@ASSET_LOCATION    ,@COST_CODE	   ,NULL		 
				   ,NULL	  ,GETDATE()   ,@USER_LOGIN
				   ,ISNULL(@GUID,@DOC_NO)  ,ISNULL(@COMPLETE_FLAG,'N')   ,@SELECTED,@SV_EMP_CODE)
END
ELSE
BEGIN

			UPDATE [dbo].[TB_T_REQUEST_STOCK]
			   SET [DOC_NO] =			@DOC_NO			
				  ,[YEAR]	=			@YEAR			
				  ,[ROUND] =			@ROUND			
				  ,[ASSET_LOCATION] =	@ASSET_LOCATION 
				  ,[COST_CODE] =		@COST_CODE	
				  ,[COMPLETE_FLAG]	=	ISNULL(@COMPLETE_FLAG,'N')	
				  ,[CREATE_DATE] =		GETDATE()
				  ,[CREATE_BY] =		@USER_LOGIN
				  ,[GUID] =				ISNULL(@GUID,@DOC_NO)
				  ,[SELECTED] =@SELECTED
				  ,[SV_EMP_CODE] = @SV_EMP_CODE
			 where  COMPANY = @COMPANY AND [GUID] = @GUID AND COST_CODE=@COST_CODE
END
END
GO
