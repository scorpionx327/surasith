DROP PROCEDURE [dbo].[sp_WFD02130_GetPermissionUploadBOIDoc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS ===== ======================================
-- Author:     FTH/Suphachai Leetrakool 
-- Create date:  2017/06/08 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/

CREATE PROCEDURE [dbo].[sp_WFD02130_GetPermissionUploadBOIDoc]
(									
	@USER_LOGON VARCHAR(8)
 )
AS
BEGIN TRY
	DECLARE @IsPermission VARCHAR(1)
	DECLARE @IsFA VARCHAR(10)
	SET @IsPermission	=	'N'

	SELECT @IsFA = FAADMIN  FROM TB_M_EMPLOYEE 
	WHERE EMP_CODE = @USER_LOGON AND ACTIVEFLAG = 'Y'

	IF ISNULL(@IsFA,'N') = 'Y'
	BEGIN
		SELECT 'Y' AS IsPermission
		RETURN
	END

	DECLARE @Div VARCHAR(40)
	SET @Div = dbo.fn_GetSystemMaster('RESPONSIBILITY','SPEC_DIV','BOI_PP')
	IF EXISTS( SELECT	1
	FROM	TB_M_EMPLOYEE E
	WHERE	EMP_CODE = @USER_LOGON AND 
			SPEC_DIV LIKE CONCAT('%',@Div,'%') -- SPEC_DIV LIKE CONCAT('%',CONCAT(@Div,'_',@Role),'%')
	)
	BEGIN
		SELECT 'Y' AS IsPermission
		RETURN
	END

	DECLARE @Role VARCHAR(3), @OrgCode VARCHAR(21), @Length INT

	SELECT	@Role		= [ROLE], 
			@OrgCode	= ORG_CODE 
	FROM	TB_M_EMPLOYEE
	WHERE	EMP_CODE	= @USER_LOGON

	SET @Length = CONVERT(INT, dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPROVE_CHECK_LENGTH',@Role))

	IF EXISTS(
	SELECT	1
	FROM	TB_M_ORGANIZATION O
	INNER	JOIN
			TB_M_SYSTEM M
	ON		M.CATEGORY	= 'SYSTEM_CONFIG' AND
			M.SUB_CATEGORY = CONCAT('ORGANIZE_CODE_',@Div) AND
			O.ORG_CODE LIKE M.[VALUE] AND --Support %
			M.ACTIVE_FLAG = 'Y'
	WHERE	LEFT(O.ORG_CODE, @Length) = LEFT(@OrgCode, @Length)
	)
	BEGIN
		SELECT 'Y' AS IsPermission
		RETURN
	END

	SELECT 'N' AS IsPermission

END TRY
BEGIN CATCH
	SELECT 'N' AS IsPermission
END CATCH


GO
