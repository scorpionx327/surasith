DROP PROCEDURE [dbo].[sp_WFD02630_GetAssetLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 22/02/2017
-- Description:	Get Round
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02630_GetAssetLocation]
@year varchar(4),
@round varchar(2),
@company T_COMPANY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		--select CODE, value as ASSET_LOCATION from TB_M_SYSTEM where category ='COMMON'
		--and sub_category ='ASSET_LOCATION' 
		--and CODE in (select ASSET_LOCATION from TB_R_STOCK_TAKE_H 
		--where [year] = @year and [ROUND] = @round group by ASSET_LOCATION)

		  	select CODE, value as ASSET_LOCATION from TB_M_SYSTEM where category ='COMMON'
		and sub_category ='ASSET_LOCATION' 
		and CODE in (   	SELECT ASSET_LOCATION
							 FROM TB_R_STOCK_TAKE_H H
							WHERE H.PLAN_STATUS <>'D' AND  [YEAR] = @YEAR AND [ROUND] = @ROUND AND H.COMPANY=@company
								   AND
														 EXISTS ( SELECT 1 
																	FROM TB_R_STOCK_TAKE_D_PER_SV D 
																	 WHERE D.STOCK_TAKE_KEY = H.STOCK_TAKE_KEY
																		--AND ((@IS_FAADMIN = 'Y' ) OR (D.EMP_CODE = @EMP_CODE ) )
																	)
				
					  Group by ASSET_LOCATION)

END
GO
