USE [FD0TFASTIFT]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLastNoFromAssetTemp]    Script Date: 29/10/2019 19:27:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_getLastNoFromAssetTemp] 
( 
	@GUID		T_GUID 
)

AS
BEGIN
DECLARE @NEWLINENO INT

	SELECT @NEWLINENO = ISNULL(MAX(LINE_NO),0) FROM TB_T_REQUEST_ASSET_D WHERE [GUID] = @GUID

	--Max line no. in ASSET Temp + 1
	SET @NEWLINENO = @NEWLINENO + 1
	
	SELECT CONVERT(VARCHAR(5), @NEWLINENO) 
END
