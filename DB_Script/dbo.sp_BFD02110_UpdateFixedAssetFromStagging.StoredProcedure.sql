DROP PROCEDURE [dbo].[sp_BFD02110_UpdateFixedAssetFromStagging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/16 (yyyy/mm/dd)
-- Description:  For update Fixed Assets data in result table

--exec sp_BFD02110_UpdateFixedAssetFromStagging 'uten'
- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_BFD02110_UpdateFixedAssetFromStagging]
(
	 @UserID		VARCHAR(8),
	 @ERROR_MESSAGE	VARCHAR(MAX) OUT
)
AS
BEGIN TRY


	DECLARE @Error BIT;
	SET @Error =0;
-- Create temp TEMP_ASSETS 
IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_ASSETS
END
---  0 Create structure of temp table 
 	 SELECT ASSET_NO
	 INTO #TEMP_ASSETS 
	 FROM TB_S_ASSETS_H
	 WHERE 1<>1

	INSERT INTO  #TEMP_ASSETS(ASSET_NO)
	SELECT  S_Asset.ASSET_NO
	FROM    TB_S_ASSETS_H S_Asset 
	WHERE EXISTS ( SELECT 1 
					FROM TB_M_ASSETS_H  AssetH
					WHERE AssetH.ASSET_NO = S_Asset.ASSET_NO
					)
	-------------------------------------------------------------------------------------------------------
	-- Keep CIP Asset
	DECLARE @CIPCode VARCHAR(40)
	SET @CIPCode = dbo.fn_GetSystemMaster('FAS_TYPE','ASSET_TYPE','CIP')

	SELECT	ASSET_NO
	INTO	#TB_T_CIP_ASSETS
	FROM	TB_M_ASSETS_H
	WHERE	ASSET_TYPE = @CIPCode AND [STATUS] = 'Y'
	-------------------------------------------------------------------------------------------------------

print '--- 1  Update Asset Header '
    UPDATE  AssetH
       SET      AssetH.[ASSET_NAME]				= S_Asset.[ASSET_NAME]
			   ,AssetH.[SUB_TYPE]				= S_Asset.[SUB_TYPE]
			   ,AssetH.[MODEL]					= S_Asset.[MODEL]
			   ,AssetH.[SERIAL_NO]				= S_Asset.[SERIAL_NO]
			   ,AssetH.[TAG_NO]					= S_Asset.[TAG_NO]
			   ,AssetH.[CLASS_SACCESS]			= S_Asset.[CLASS_SACCESS]
			   ,AssetH.[BOI_NO]					= S_Asset.[BOI_NO] 
			   ,AssetH.[PROJECT_NAME]			= S_Asset.[PROJECT_NAME] 
			   ,AssetH.[PLAN_COST]				= CASE	
												  WHEN  IsNumeric(S_Asset.[PLAN_COST])=1 THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[PLAN_COST]) AS decimal(18,2))
												  ELSE NULL END  
			   , AssetH.[PRODUCTION_PART_NO]    = S_Asset.[PRODUCTION_PART_NO] 
			   ,AssetH.[FA_BOOK]				= S_Asset.[FA_BOOK] 
			   ,AssetH.[MACHINE_LICENSE] 		= S_Asset.[MACHINE_LICENSE] 
			  ,[ASSET_CATEGORY]					= LEFT(S_Asset.MAJOR_MINOR_CATEGORY,2)
			  ,[MINOR_CATEGORY]					= SUBSTRING(S_Asset.MAJOR_MINOR_CATEGORY,4,3)
			  ,[ASSET_TYPE]  					= S_Asset.[ASSET_TYPE] 
			 ,AssetH.[DATE_IN_SERVICE]     		= CASE 
												  WHEN ISNULL(S_Asset.[DATE_IN_SERVICE],'') <> ''   THEN CONVERT(DATETIME,S_Asset.[DATE_IN_SERVICE],106)
												  ELSE NULL END 
			  ,AssetH.[COST]  					= CASE	
												  WHEN   IsNumeric(S_Asset.[COST])=1 THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.COST) AS decimal(18,2))
												   ELSE NULL END  
			 ,AssetH.[NBV]   					= CASE	
												   WHEN   IsNumeric(S_Asset.[NBV])=1 THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[NBV]) AS decimal(18,2)) 
													 ELSE NULL END  
			 ,AssetH.[TOOLING_COST]   		    =  CASE	
												   WHEN  IsNumeric(S_Asset.[TOOLING_COST])=1  THEN  CAST(dbo.fn_GetStringOfNumber(S_Asset.[TOOLING_COST]) AS decimal(18,2)) 
													 ELSE NULL END  
			--,[BARCODE_SIZE] = <BARCODE_SIZE, varchar(1),>
			  ,AssetH.[UOP_CAPACITY]   			= CASE	
												   WHEN  IsNumeric(S_Asset.[UOP_CAPACITY])=1  THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[UOP_CAPACITY]) AS decimal(10,2))
												  ELSE NULL END   
			  ,AssetH.[UOP_LTD_PRODUCTION]      = CASE	
												   WHEN  IsNumeric(S_Asset.[UOP_LTD_PRODUCTION])=1 THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[UOP_LTD_PRODUCTION]) AS decimal(10,2)) 
												 ELSE NULL END  
			  ,AssetH.[STL_LIFE_YEAR]   		= CASE	
												   WHEN  IsNumeric(S_Asset.[STL_LIFE_YEAR])=1  THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[STL_LIFE_YEAR]) AS decimal(10,2)) 
													 ELSE NULL END  
			  ,AssetH.[STL_REMAIN_LIFE_YEAR]   	= CASE	
												   WHEN  IsNumeric(S_Asset.[STL_REMAIN_LIFE_YEAR])=1  THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[STL_REMAIN_LIFE_YEAR]) AS decimal(10,2)) 
													ELSE NULL END  
			  ,AssetH.[SALVAGE_VALUE]   		=  CASE	
												   WHEN  IsNumeric(S_Asset.[SALVAGE_VALUE])=1 THEN  CAST(dbo.fn_GetStringOfNumber(S_Asset.[SALVAGE_VALUE]) AS decimal(10,2)) 
													 ELSE NULL END  
			  ,AssetH.[YTD_DEPRECIATION]   		= CASE	
												   WHEN  IsNumeric(S_Asset.[YTD_DEPRECIATION])=1  THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[YTD_DEPRECIATION]) AS decimal(10,2)) 
												  ELSE NULL END  
		 	 ,AssetH.[ACCUMULATE_DEPRECIATION]  = CASE	
												   WHEN  IsNumeric(S_Asset.[ACCUMULATE_DEPRECIATION])=1  THEN CAST(dbo.fn_GetStringOfNumber(S_Asset.[ACCUMULATE_DEPRECIATION]) AS decimal(36,2))
													ELSE NULL END   
			 ,AssetH.[RETIREMENT_DATE]   		=  CASE 
													  WHEN ISNULL(S_Asset.[RETIREMENT_DATE],'') <> ''   THEN CONVERT(DATETIME,S_Asset.[RETIREMENT_DATE],106)
													  ELSE NULL END  	
			 ,AssetH.[TRANSFER_DATE]   				= CASE 
													  WHEN ISNULL(S_Asset.[TRANSFER_DATE],'') <> ''   THEN CONVERT(DATETIME,S_Asset.[TRANSFER_DATE],106)
													  ELSE NULL END  
			 ,AssetH.[ENTRY_PERIOD]  					= CASE 
													  WHEN ISNULL(S_Asset.[ENTRY_PERIOD],'') <> ''   THEN CONVERT(DATETIME,S_Asset.[ENTRY_PERIOD],106)
													  ELSE NULL END  
			--,[LOCATION_NAME] = <LOCATION_NAME, varchar(200),>
			--,[BARCODE] = <BARCODE, varchar(17),>
			--,[BOI_FLAG] = <BOI_FLAG, char(1),>
			--,[PHOTO_STATUS] = <PHOTO_STATUS, char(1),>
			--,[PRINT_STATUS] = <PRINT_STATUS, char(1),>

			
			--,[PROCESS_STATUS] = <PROCESS_STATUS, varchar(1),>
			--,[PRINT_COUNT] = <PRINT_COUNT, int,>
			--,[TAG_PHOTO] = <TAG_PHOTO, varchar(200),>
		    ,AssetH.[INHOUSE_FLAG] = 'Y' -- CASE  ---'If (Left 2 digits of B.14 = '12') set 'Y' otherwise set 'N'
						--		WHEN LEFT(S_Asset.MAJOR_MINOR_CATEGORY,2) = '12' THEN 'Y'
						--		ELSE 'N' END 
			,AssetH.[MAP_STATUS] = CASE --If E.28(RETIREMENT_DATE) is not null or Asset no. does not exist in I/F or E.29(TRANSFER_DATE) is not null, set to 'N'
							  WHEN ISNULL(S_Asset.[RETIREMENT_DATE],'') <> '' THEN 'N'
							  WHEN ISNULL(S_Asset.[TRANSFER_DATE],'')   <> '' THEN 'N'
							    ELSE AssetH.[MAP_STATUS]   END  
			,AssetH.[LICENSE_FLAG] = CASE   --If E.13(MACHINE_LICENSE) is not blank, set to 'Y' otherwise set to 'N'																															
								WHEN ISNULL(S_Asset.MACHINE_LICENSE,'') <> ''   THEN 'Y'
								 ELSE 'N' END 
			,AssetH.[FULLY_DP_FLAG] = CASE  -- If E.17(COST) <= (E.25(SALVAGE_VALUE) + E.27(ACCUMULATE_DEPRECIATION)), set to 'Y', otherwise set to 'N'
								WHEN CAST(dbo.fn_GetStringOfNumber(S_Asset.COST) AS decimal(18,2)) < = (CAST(dbo.fn_GetStringOfNumber(S_Asset.SALVAGE_VALUE)AS decimal(18,2))  + CAST(dbo.fn_GetStringOfNumber(S_Asset.ACCUMULATE_DEPRECIATION) AS decimal(18,2))) THEN 'Y'
								ELSE 'N' END 
		   ,AssetH.SUPPLIER_NO=(    SELECT TOP (1) SUPPLIER_NO   
									 FROM TB_S_ASSETS_FINANCE AssetFinance
									 WHERE AssetH.ASSET_NO=AssetFinance.ASSET_NO
									 ORDER BY ROW_NO ASC )
		   , AssetH.SUPPLIER_NAME=( 
									SELECT TOP (1) SUPPLIER_NAME   
									 FROM TB_S_ASSETS_FINANCE AssetFinance
									 WHERE AssetH.ASSET_NO=AssetFinance.ASSET_NO
									 ORDER BY ROW_NO ASC 
									 )
			--,[LAST_SCAN_DATE]  = <LAST_SCAN_DATE, datetime,>
			--,[TRNF_BOI_DECISION] = <TRNF_BOI_DECISION, varchar(1),>
			--,[TRNF_BOI_ATTACH] = <TRNF_BOI_ATTACH, varchar(200),>
			--,[DISP_BOI_DECISION] = <DISP_BOI_DECISION, varchar(1),>
			--,[DISP_BOI_ATTACH] = <DISP_BOI_ATTACH, varchar(200),>
			, AssetH.UPDATE_DATE =GETDATE()
			, AssetH.UPDATE_BY =@UserID
   	FROM  TB_M_ASSETS_H  AssetH 
	INNER JOIN  TB_S_ASSETS_H S_Asset  ON  AssetH.ASSET_NO = S_Asset.ASSET_NO 
	WHERE AssetH.ASSET_NO NOT IN ( SELECT ASSET_NO
								   FROM TB_S_ASSETS_H
								   WHERE [PLAN_COST] IS NOT NULL 
										   AND (      IsNumeric([PLAN_COST])=0 
												   OR ltrim(rtrim([PLAN_COST]))='-')
											   )

		--  UPDATE incase  Asset no. does not exist in I/F (Update MAP_STATUS='N' and  STATUS='N)
         UPDATE  AssetH
 		 SET      [STATUS]			= 'N'
				, [MAP_STATUS]		= 'N'
				, [UPDATE_DATE]		= GETDATE()
				, [UPDATE_BY]			= @UserID
				, TOTAL_UNIT		= 0
   		FROM  TB_M_ASSETS_H  AssetH 
		WHERE NOT EXISTS(
						  SELECT 1
						  FROM   TB_S_ASSETS_H S_Asset  
						  WHERE  AssetH.ASSET_NO = S_Asset.ASSET_NO 
						  )

		---------------------------------------------------------------------------------------------
		-- Update I/H Flag of new assets default is Y
		UPDATE	M
		SET		INHOUSE_FLAG = 'N'
		FROM	TB_M_ASSETS_H M 
		INNER	JOIN
				#TEMP_ASSETS T
		ON		M.ASSET_NO = T.ASSET_NO
		WHERE	ASSET_CATEGORY IN (	SELECT	[VALUE] 
									FROM	TB_M_SYSTEM
									WHERE	CATEGORY		= 'ADMIN_ASSET' AND 
											SUB_CATEGORY	= 'ASSET_CATEGORY' AND 
											ACTIVE_FLAG		= 'Y' )
		-------------------------------------------------------------------------------------------------------------------
		-- Update By Surasith : Support BCT IS Incident No.8 2017-05-12
		-- Set Todo List Status = 'N' when asset is change from CIP to Capital

		UPDATE	M
		SET		M.PRINT_STATUS	= 'N',
				M.PHOTO_STATUS	= 'N',
				M.MAP_STATUS	= 'N'
		FROM	TB_M_ASSETS_H M
		INNER	JOIN
				#TB_T_CIP_ASSETS T
		ON		M.ASSET_NO = T.ASSET_NO
		WHERE	M.ASSET_TYPE != @CIPCode AND M.[STATUS] = 'Y' AND M.INHOUSE_FLAG = 'Y'
		-------------------------------------------------------------------------------------------------------------------


print '--- 2  Delete Asset Details ( Asset Details / Asset Finance )'
       -- 2.1  Asset Details 
	    DELETE AssetD
		FROM TB_M_ASSETS_D AssetD
	    WHERE EXISTS ( SELECT 1 FROM #TEMP_ASSETS  T_Asset
					WHERE T_Asset.ASSET_NO = AssetD.ASSET_NO )

	  --- 2.2 Asset Finance
	  	DELETE AssetFinance
		FROM TB_M_ASSETS_FINANCE AssetFinance
	    WHERE EXISTS ( SELECT 1 FROM #TEMP_ASSETS  T_Asset
					WHERE T_Asset.ASSET_NO = AssetFinance.ASSET_NO )

print '--- 3  Insert Asset Detaisl '
		INSERT INTO TB_M_ASSETS_D ( ASSET_NO       , COST_CODE       , EMP_CODE       , EMP_NAME       , EXPENSE_ACCOUNT 
									, LOCATION_NAME
									 , UNIT       , CREATE_DATE      , CREATE_BY      , UPDATE_DATE   , UPDATE_BY  )
		SELECT  ASSET_NO  , SUBSTRING(ISNULL(EXPENSE_ACCOUNT,''),15,8) AS COST_CODE
			  , EMP_CODE       , EMP_NAME       , EXPENSE_ACCOUNT 	
			  , [LOCATION]
			  , CASE 
				WHEN UNIT IS NOT NULL THEN CAST(dbo.fn_GetStringOfNumber(UNIT) AS decimal(36,2)) 
				ELSE NULL END  AS  UNIT    
			  , GETDATE()       , @UserID       ,  GETDATE()     , @UserID
		FROM   TB_S_ASSETS_D S_Asset 
		WHERE EXISTS ( SELECT 1 FROM #TEMP_ASSETS  T_Asset
						WHERE T_Asset.ASSET_NO = S_Asset.ASSET_NO )

print '-- 4  Insert Asset Finance '
	 INSERT INTO TB_M_ASSETS_FINANCE (ROW_NO,ASSET_NO, INVOICE_NO  ,INVOICE_DATE ,INVOICE_LINE   ,SUPPLIER_NO
		,SUPPLIER_NAME   ,[DESCRIPTION] ,INVOICE_COST  ,PO_NO  ,CREATE_DATE   ,CREATE_BY   ,UPDATE_DATE  ,UPDATE_BY )
	SELECT  ROW_NO, ASSET_NO, INVOICE_NO  , CASE 
									  WHEN ISNULL(S_Asset.INVOICE_DATE,'') <> ''   THEN CONVERT(DATE,S_Asset.INVOICE_DATE,106)
									  ELSE NULL END AS INVOICE_DATE ,INVOICE_LINE 
								   , SUPPLIER_NO
		   , SUPPLIER_NAME   ,[DESCRIPTION]  
		    , CASE 
			WHEN IsNumeric(INVOICE_COST)=1  THEN CAST(dbo.fn_GetStringOfNumber(INVOICE_COST) AS decimal(18,2)) 
			ELSE NULL END  AS INVOICE_COST  ,PO_NO  
		   , GETDATE()       , @UserID       ,  GETDATE()     , @UserID
	FROM   TB_S_ASSETS_FINANCE S_Asset 
	WHERE EXISTS ( SELECT 1 FROM #TEMP_ASSETS  T_Asset
						WHERE T_Asset.ASSET_NO = S_Asset.ASSET_NO )
 	
-- Create temp TEMP_ASSETS 
IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_ASSETS
END

	RETURN @Error ;
END TRY
BEGIN CATCH

			-- Create temp TEMP_ASSETS 
			IF OBJECT_ID('tempdb..#TEMP_ASSETS') IS NOT NULL
			BEGIN
					DROP TABLE #TEMP_ASSETS
			END

			PRINT concat('ERROR Message:',ERROR_MESSAGE())
			 SET @ERROR_MESSAGE=ERROR_MESSAGE();
			RETURN 1;
END CATCH











GO
