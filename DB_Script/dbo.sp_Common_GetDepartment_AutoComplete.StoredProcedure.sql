DROP PROCEDURE [dbo].[sp_Common_GetDepartment_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetDepartment_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER,
@SubDivKeyword	varchar(200)
AS
BEGIN
		/*SELECT	DEPT_CODE,
				DEPT_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(	
					DEPT_CODE LIKE CONCAT(@keyword,'%') 
					OR
					DEPT_NAME LIKE CONCAT(@keyword,'%') 
				)
		AND		(
					SUB_DIV_CODE LIKE CONCAT(@SubDivKeyword,'%')
					OR
					SUB_DIV_NAME LIKE CONCAT(@SubDivKeyword,'%')
				)
		GROUP BY DEPT_CODE,DEPT_NAME
		ORDER BY DEPT_CODE ASC*/

		SELECT	DEPT_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(						
					DEPT_NAME LIKE CONCAT(@keyword,'%') 
				)
		AND		(					
					SUB_DIV_NAME LIKE CONCAT(@SubDivKeyword,'%')
				)
		GROUP BY DEPT_NAME
		ORDER BY DEPT_NAME ASC
END
GO
