DROP PROCEDURE [dbo].[sp_WFD02410_GetNewAssetOwner]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 24/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_GetNewAssetOwner '0806/T', '0806','E03DK208'    
CREATE PROCEDURE [dbo].[sp_WFD02410_GetNewAssetOwner]
(
	@DOC_NO					T_DOC_NO = NULL,
	@REQUEST_EMP_CODE		T_SYS_USER,					
	@NEW_COST_CODE			T_COST_CODE,
	@NEW_RESP_COST_CODE		T_COST_CODE
)
	
AS
BEGIN
	
	IF NOT EXISTS(SELECT 1 FROM TB_R_REQUEST_TRNF_H WHERE DOC_NO = @DOC_NO)
	BEGIN
		SELECT	'' DOC_NO,						
				DBO.fn_dateFAS(GETDATE()) REQUEST_DATE,			
				'T' REQUEST_TYPE,
				'' TRNF_TYPE,					
				ISNULL(SM.VALUE,'FW') RESPONSIBILITY,	
				CASE WHEN E.SYS_EMP_CODE IS NOT NULL THEN 
						CONCAT(RTRIM(EMP_TITLE), ' ', RTRIM(E.EMP_NAME), ' ' + 
							RTRIM(E.EMP_LASTNAME)) 
					ELSE NULL END EMP_DISPLAY,
				EMP_TITLE,						
				S.EMP_CODE,										
				EMP_NAME, 
				EMP_LASTNAME,					
				S.COST_CODE,									
				CONCAT(C.COST_CODE, '-', C.COST_NAME) COST_NAME,
				POST_CODE POST_CODE, 			
				CONCAT(POST_CODE, ':', POST_NAME) POST_NAME,							
				O.DEPT_CODE,										
				O.DEPT_NAME,					
				O.SECTION_CODE SECT_CODE,						
				O.SECTION_NAME SECT_NAME,											
				E.TEL_NO NEW_PHONE_NO ,				
				'' STATUS,										
				FORMAT(GETDATE(),'yyyy-MM-dd HH:MM:ss.fff') CREATE_DATE,											
				@REQUEST_EMP_CODE CREATE_BY, 	
				@REQUEST_EMP_CODE UPDATE_BY,					
				FORMAT(GETDATE(),'yyyy-MM-dd HH:MM:ss.fff') UPDATE_DATE		
		FROM	TB_M_SV_COST_CENTER S
		LEFT	JOIN 
				TB_M_EMPLOYEE E 
		ON		S.EMP_CODE		= E.SYS_EMP_CODE 
		LEFT	JOIN 
				TB_M_ORGANIZATION O 
		ON		E.ORG_CODE	= O.ORG_CODE
		LEFT	JOIN 
				TB_M_COST_CENTER C 
		ON		S.COMPANY = C.COMPANY AND S.COST_CODE	= C.COST_CODE 
		LEFT	JOIN 
				TB_M_SYSTEM SM 
		ON		SM.CATEGORY		= 'RESPONSIBILITY' AND 
				SM.SUB_CATEGORY	= 'USERROLE_CODE' AND 
				SM.ACTIVE_FLAG	= 'Y' AND 
				SM.CODE			=	E.[ROLE]
		WHERE	S.COST_CODE		= @NEW_COST_CODE
	END
	ELSE
	BEGIN
		SELECT	T.DOC_NO,						
				DBO.fn_dateFAS(H.REQUEST_DATE) REQUEST_DATE,	
				REQUEST_TYPE, 
				TRNF_TYPE,						
				ISNULL(SM.VALUE,'FW') RESPONSIBILITY,			
				ISNULL(RTRIM(T.EMP_TITLE),'') + ' ' + ISNULL(RTRIM(T.EMP_NAME),'') EMP_DISPLAY,
				T.EMP_TITLE,					
				T.EMP_CODE,										
				T.EMP_NAME, 
				EMP_LASTNAME,					
				T.COST_CODE,									
				T.COST_CODE + '-' + C.COST_NAME COST_NAME,
				POS_CODE POST_CODE,				
				CONCAT(T.POST_CODE, ':', T.POST_NAME) POST_NAME,										
				T.DEPT_CODE,											
				T.DEPT_NAME, 					
				T.SECT_CODE,									
				T.SECT_NAME,											
				T.PHONE_NO NEW_PHONE_NO,						
				H.STATUS,										
				FORMAT(T.CREATE_DATE,'yyyy-MM-dd HH:MM:ss.fff') CREATE_DATE,																			
				T.CREATE_BY, 					
				T.UPDATE_DATE,									
				FORMAT(T.UPDATE_DATE,'yyyy-MM-dd HH:MM:ss.fff') UPDATE_DATE,		

				T.TRNF_MAIN_TYPE, -- RC, CC, EC
				T.TRNF_TYPE -- Mass, Normal

		FROM	TB_R_REQUEST_TRNF_H T
		LEFT	JOIN TB_R_REQUEST_H H 
		ON		T.DOC_NO			= H.DOC_NO 
		LEFT	JOIN TB_M_COST_CENTER C 
		ON		H.COMPANY = C.COMPANY AND T.COST_CODE		= C.COST_CODE 
		LEFT	JOIN TB_M_SYSTEM SM 
		ON		SM.CATEGORY		= 'RESPONSIBILITY' AND 
				SM.SUB_CATEGORY	= 'USERROLE_CODE' AND 
				SM.ACTIVE_FLAG	= 'Y' AND 
				SM.CODE			= T.[ROLE]
		WHERE	T.DOC_NO = @DOC_NO

	END
	


	





END
GO
