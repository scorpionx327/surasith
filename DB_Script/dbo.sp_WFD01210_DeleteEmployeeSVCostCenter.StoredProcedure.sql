DROP PROCEDURE [dbo].[sp_WFD01210_DeleteEmployeeSVCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Phithak Khonnoy
-- Create date: 23/03/2017
-- Description:	Delete Employee in TB_M_SV_COST_CENTER for preparing table
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_DeleteEmployeeSVCostCenter] 
	@EMP_CODE T_USER
AS
BEGIN
		Delete from TB_M_SV_COST_CENTER
		where EMP_CODE = @EMP_CODE;
END




GO
