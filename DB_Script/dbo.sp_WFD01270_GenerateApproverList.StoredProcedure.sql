DROP PROCEDURE [dbo].[sp_WFD01270_GenerateApproverList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_GenerateApproverList]
	@Company		T_COMPANY,
	@DOC_NO			T_DOC_NO,
	@GUID			VARCHAR(50),
	@CostCodeTo		T_COST_CODE	= NULL,
	@EmpCode		T_SYS_USER
AS
BEGIN
	-- Purpose : Generate List of Emp Code of all pending approve role
	
	-- SET NOCOUNT ON

	DELETE	H
	FROM	TB_H_REQUEST_APPR H
	INNER	JOIN
			TB_R_REQUEST_APPR R
	ON		H.DOC_NO = R.DOC_NO AND
			H.INDX	= R.INDX
	WHERE	(R.DOC_NO = @DOC_NO AND R.APPR_STATUS = 'P')
	
	
	----------------------------------------------------------------------------------------------
	-- Get List of Approver Name of each Role

	-- Update Cost Code
	DECLARE @TB_APPV_CC TABLE
	(
		SEQ				INT, -- Sequence For Loop
		APPV_CODE		VARCHAR(3),
		DIVISION		VARCHAR(6),
		COST_CODE		VARCHAR(8),
		FIX_DIV			VARCHAR(1),
		NOTIFICATION_MODE	VARCHAR(1),
		HIGHER_APPR		VARCHAR(1),
		USED_FLAG		VARCHAR(1) -- For Loop
	)
	
	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_APPR WHERE DOC_NO = @DOC_NO AND INDX = '1' 
		AND [APPR_ROLE] = 'FW' AND APPR_STATUS = 'W' )
	BEGIN
		INSERT
		INTO	@TB_APPV_CC
		(
				SEQ, APPV_CODE, DIVISION, COST_CODE, FIX_DIV, HIGHER_APPR, NOTIFICATION_MODE, USED_FLAG
		)
		SELECT	A.INDX, 
				A.APPV_ROLE, A.DIVISION, T.COST_CODE, 'N' AS FIX_DIV, A.HIGHER_APPR, A.NOTIFICATION_MODE, 
					'N' AS USED_FLAG
		FROM	TB_T_SELECTED_ASSETS T WITH (NOLOCK)
		CROSS	JOIN
				(	SELECT INDX, [APPR_ROLE] AS APPV_ROLE, DIVISION, HIGHER_APPR, A.NOTIFICATION_MODE 
					FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)  
					--WHERE DOC_NO = @DOC_NO AND APPR_STATUS = 'P' ) A
					WHERE DOC_NO = @DOC_NO AND APPR_STATUS  in ('P','W')) A
		WHERE	[GUID] = @GUID
		GROUP	BY
				 A.INDX, A.APPV_ROLE, A.DIVISION, T.COST_CODE, HIGHER_APPR, NOTIFICATION_MODE
		PRINT 'Match Case FW'
	END
	ELSE
	BEGIN
		INSERT
		INTO	@TB_APPV_CC
		(
				SEQ, APPV_CODE, DIVISION, COST_CODE, FIX_DIV, HIGHER_APPR, NOTIFICATION_MODE, USED_FLAG
		)
		SELECT	A.INDX, 
				A.APPV_ROLE, A.DIVISION, T.COST_CODE, 'N' AS FIX_DIV, A.HIGHER_APPR, A.NOTIFICATION_MODE,
				'N' AS USED_FLAG
		FROM	TB_T_SELECTED_ASSETS T WITH (NOLOCK)
		CROSS	JOIN
				(	SELECT INDX, [APPR_ROLE] AS APPV_ROLE, DIVISION, HIGHER_APPR , NOTIFICATION_MODE
					FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)  
					WHERE DOC_NO = @DOC_NO AND APPR_STATUS = 'P' ) A
					--WHERE DOC_NO = @DOC_NO AND APPR_STATUS  in ('P','W')) A
		WHERE	[GUID] = @GUID
		GROUP	BY
				 A.INDX, A.APPV_ROLE, A.DIVISION, T.COST_CODE, HIGHER_APPR, NOTIFICATION_MODE
	END


	--INSERT
	--INTO	@TB_APPV_CC
	--(
	--		SEQ, APPV_CODE, DIVISION, COST_CODE, FIX_DIV, HIGHER_APPR, USED_FLAG
	--)
	--SELECT	A.INDX, 
	--		A.APPV_ROLE, A.DIVISION, T.COST_CODE, 'N' AS FIX_DIV, A.HIGHER_APPR, 'N' AS USED_FLAG
	--FROM	TB_T_SELECTED_ASSETS T WITH (NOLOCK)
	--CROSS	JOIN
	--		(	SELECT INDX, [ROLE] AS APPV_ROLE, DIVISION, HIGHER_APPR 
	--			FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)  
	--			WHERE DOC_NO = @DOC_NO AND APPR_STATUS = 'P' ) A
	--			--WHERE DOC_NO = @DOC_NO AND APPR_STATUS  in ('P','W')) A
	--WHERE	[GUID] = @GUID
	--GROUP	BY
	--		 A.INDX, A.APPV_ROLE, A.DIVISION, T.COST_CODE, HIGHER_APPR

	UPDATE	@TB_APPV_CC
	SET		FIX_DIV = 'Y'
	WHERE	DIVISION NOT IN ('CRNT','NEW')

	-- Update Cost Code of To [Incase Transfer]
	UPDATE	@TB_APPV_CC
	SET		COST_CODE = @CostCodeTo
	WHERE	DIVISION = 'NEW'

	

	-- Incase CC no employee ref

	SELECT	SV.COST_CODE, 
			E.ORG_CODE, 
			D.CMP_CODE, 
			D.DIV_CODE, 
			D.SUB_DIV_CODE,
			D.DEPT_CODE, 
			D.SECTION_CODE,
			D.LINE_CODE
	INTO	#TB_T_CC
	FROM	TB_M_SV_COST_CENTER SV 
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		SV.EMP_CODE = E.EMP_CODE
	INNER	JOIN
			TB_M_ORGANIZATION D
	ON		D.ORG_CODE = E.ORG_CODE

	WHERE	EXISTS(SELECT 1 FROM @TB_APPV_CC CC WHERE CC.COST_CODE  = SV.COST_CODE)
	GROUP	BY
			SV.COST_CODE, 
			E.ORG_CODE,
			D.CMP_CODE, 
			D.DIV_CODE, 
			D.SUB_DIV_CODE,
			D.DEPT_CODE, 
			D.SECTION_CODE,
			D.LINE_CODE
	----------------------------------------------------------------------------------
	
	INSERT
	INTO	#TB_T_CC
	(		COST_CODE, 
			ORG_CODE, 
			CMP_CODE, 
			DIV_CODE, 
			SUB_DIV_CODE,
			DEPT_CODE, 
			SECTION_CODE,
			LINE_CODE
	)
	SELECT	E.COST_CODE, 
			E.ORG_CODE,
			D.CMP_CODE, 
			D.DIV_CODE, 
			D.SUB_DIV_CODE,
			D.DEPT_CODE, 
			D.SECTION_CODE,
			D.LINE_CODE
	FROM	TB_M_EMPLOYEE E	
	INNER	JOIN
			TB_M_ORGANIZATION D
	ON		D.ORG_CODE = E.ORG_CODE
	LEFT	JOIN
			#TB_T_CC CC
	ON		CC.ORG_CODE = E.ORG_CODE AND CC.COST_CODE = E.COST_CODE
	WHERE	EXISTS(SELECT 1 FROM @TB_APPV_CC CC WHERE CC.COST_CODE  = E.COST_CODE) AND
			E.ACTIVEFLAG	= 'Y' AND

			-- Add New By Surasith T. 2017-06-15 : User confirm to filter temporary user from approver flow
			NOT EXISTS(	SELECT	1 
					FROM	TB_M_SYSTEM S 
					WHERE	S.CATEGORY		= 'SYSTEM_CONFIG' AND
							S.SUB_CATEGORY	= 'TEMP_EMPLOYEE' AND 
							S.ACTIVE_FLAG		= 'Y' AND 
							E.EMP_CODE		LIKE CONCAT(S.[VALUE],'%')
					) AND
			-- End of Modify
			CC.ORG_CODE IS NULL
	GROUP	BY
			E.COST_CODE, 
			E.ORG_CODE,
			D.CMP_CODE, 
			D.DIV_CODE, 
			D.SUB_DIV_CODE,
			D.DEPT_CODE, 
			D.SECTION_CODE,
			D.LINE_CODE


	-- 
	
	----------------------------------------------------------------------------------
	-- Get FW Code
	DECLARE @FWCode VARCHAR(3)
	SET @FWCode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPRV_ROLE_CODE','FW')
	
	INSERT
	INTO	TB_H_REQUEST_APPR(DOC_NO, INDX, [ROLE],  EMP_CODE, MAIN)

	SELECT	DISTINCT @DOC_NO, SEQ, APPV_CODE,  SV.EMP_CODE, 'M' AS MAIN
	FROM	@TB_APPV_CC CC
	INNER	JOIN
			TB_M_SV_COST_CENTER SV
	ON		CC.COST_CODE = SV.COST_CODE
	WHERE	APPV_CODE = @FWCode AND FIX_DIV = 'N'

	
	
	UPDATE	@TB_APPV_CC SET USED_FLAG = 'Y' WHERE APPV_CODE = @FWCode AND FIX_DIV = 'N'
	
	----------------------------------------------------------------------------------
	CREATE	TABLE	#TB_T_CC_TARGET
	(	
		SEQ		INT,
		COST_CODE	VARCHAR(8)
	)
	

	DECLARE @Seq INT, 
			@Role VARCHAR(3), @Div VARCHAR(6), @Cnt INT = 0
	
	--select * from #TB_T_CC
	WHILE(EXISTS(SELECT 1 FROM @TB_APPV_CC WHERE USED_FLAG = 'N' AND FIX_DIV = 'N' AND NOTIFICATION_MODE = 'P' ))
	BEGIN
		-- Incase Stock Taking, There are a lot of Cost Center per 1 Seq
		

		SELECT	TOP 1	@Seq = SEQ FROM	@TB_APPV_CC WHERE	USED_FLAG = 'N' AND FIX_DIV = 'N' ORDER BY SEQ
		UPDATE	@TB_APPV_CC SET	USED_FLAG = 'Y' WHERE	SEQ = @Seq

	
		SELECT	@Role		= APPV_CODE,
				@Div		= DIVISION
		FROM	@TB_APPV_CC
		WHERE	SEQ			= @Seq

		DELETE
		FROM	#TB_T_CC_TARGET

		INSERT
		INTO	#TB_T_CC_TARGET(SEQ, COST_CODE)
		SELECT	SEQ, COST_CODE
		FROM	@TB_APPV_CC
		WHERE	SEQ	= @Seq
		
		
		
		-- Get organization level
		DECLARE @DirectOrgLevel VARCHAR(40),@InDirectOrgLevel VARCHAR(40)
		SET @DirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@Role)
		SET @InDirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_INDIRECT'),@Role)
		
		DECLARE @GCnt INT = 0,  @MainFoundCnt INT = 0

		EXEC sp_WFD01270_GenerateApproverRoleList @DOC_NO, @Seq, @Role, @Role, @DirectOrgLevel,  'M', @Cnt OUT
		SET @GCnt = @Cnt
		SET @MainFoundCnt = @Cnt
		
		
		EXEC sp_WFD01270_GenerateApproverRoleList @DOC_NO, @Seq, @Role, @Role, @InDirectOrgLevel, 'R', @Cnt OUT
		SET @GCnt = @GCnt + @Cnt
		
		-- select * from TB_H_REQUEST_APPR where DOC_NO = @DOC_NO
		-- Add New By Surasith T. 2017-09-28
		
		IF @MainFoundCnt = 0
		BEGIN
			--PRINT 'NOT FOUND'
			EXEC sp_WFD01270_GenerateApproverHigherRoleList @Company, @DOC_NO, @Seq, @Role, @Cnt OUT
			SET @GCnt = @GCnt + @Cnt
			--PRINT '-----------------------------------'
		END
		-- End of Add New
		
		
		IF @GCnt = 0
		BEGIN			
			EXEC sp_WFD01270_GenerateApproverHigherList @Company, @DOC_NO, @Role,  @Seq		
		END
		
	END -- WHILE(EXISTS(SELECT 1 FROM @TB_APPV_CC WHERE FLAG = 'N'))

	
	------------------------------------------------------------------------------------------------------------------------------
	-- Fix Div
	DECLARE @PreparePIC VARCHAR(3)
	SET @PreparePIC = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPRV_ROLE_CODE','PP')

	DECLARE @AECCode VARCHAR(3)
	SET @AECCode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPRV_ROLE_CODE','ACR')

	WHILE(EXISTS(SELECT 1 FROM @TB_APPV_CC WHERE USED_FLAG = 'N' AND FIX_DIV = 'Y' AND NOTIFICATION_MODE = 'P'))
	BEGIN
		 
		SELECT	TOP 1	@Seq = SEQ FROM	@TB_APPV_CC WHERE	USED_FLAG = 'N' AND FIX_DIV = 'Y' ORDER BY SEQ
		UPDATE	@TB_APPV_CC SET	USED_FLAG = 'Y' WHERE	SEQ = @Seq

		SELECT	@Role		= APPV_CODE,
				@Div		= DIVISION
		FROM	@TB_APPV_CC
		WHERE	SEQ			= @Seq
		--PRINT('----------------------------------------------------------------------------------')
		



		-- Fix Person
		IF @Role IN ( @PreparePIC,@AECCode)
		BEGIN
			
			-- Change Generate Approver of fix div follow to IS request
			-- Get Approver from Employee for all role
			-- Should be keep DIV_ROLE Format

			INSERT
			INTO	TB_H_REQUEST_APPR(DOC_NO, INDX, [ROLE], EMP_CODE, MAIN)
			SELECT	@DOC_NO, @Seq, @Role AS APPV_ROLE,  E.EMP_CODE, 'M'
			FROM	TB_M_EMPLOYEE E 
			INNER	JOIN	TB_M_SP_ROLE R
			ON		E.EMP_CODE = R.EMP_CODE
			WHERE	R.SP_ROLE_COMPANY = @Company AND R.SP_ROLE = CONCAT(@Div,'_', @Role)
			ORDER	BY EMP_CODE
			CONTINUE
		END

		--############################################################SPECIAL ROLE##############################################################################
		
		DECLARE @SCnt INT = 0 ,@iCnt INT = 0
		
		SET @DirectOrgLevel = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@Role)

		EXEC sp_WFD01270_GenerateApproverRoleSpecialList  @Company, @DOC_NO, @Seq, @Role, @Role, @DirectOrgLevel, 'M', @iCnt OUT , @Div
		SET @SCnt = @SCnt + @iCnt

		IF @SCnt = 0
		BEGIN
			--PRINT 'NOT FOUND'
			EXEC sp_WFD01270_GenerateApproverHigherRoleSpecialList @Company, @DOC_NO, @Seq, @Role, @Cnt OUT, @Div
			SET @SCnt = @SCnt + @iCnt
			--PRINT '-----------------------------------'
		END
		-- --############################################################SPECIAL ROLE##############################################################################

	END -- WHILE(EXISTS(SELECT 1 FROM @TB_APPV_CC WHERE USED_FLAG = 'N' AND FIX_DIV = 'Y' ))
	------------------------------------------------------------------------------------------------------------------------------
	
	

	-- FA Admin
	--select * from TB_H_REQUEST_APPR where DOC_NO = @DOC_NO
	/*
	IF EXISTS(SELECT 1 FROM @TB_APPV_CC WHERE  APPV_CODE = @AECCode) AND
		NOT EXISTS(SELECT 1 FROM TB_H_REQUEST_APPR WHERE [ROLE] = @AECCode AND DOC_NO = @DOC_NO AND EMP_CODE IS NOT NULL) 
	BEGIN
		

		INSERT
		INTO	TB_H_REQUEST_APPR(DOC_NO, INDX, [ROLE], EMP_CODE, MAIN)
		SELECT	DISTINCT @DOC_NO, SEQ, APPV_CODE, @AECCode, 'M'
		FROM	@TB_APPV_CC WHERE  APPV_CODE = @AECCode
	--	PRINT @@ROWCOUNT
	--	SELECT	@DOC_NO, SEQ, APPV_CODE, @FACode, 'M'
	--	FROM	@TB_APPV_CC WHERE  APPV_CODE = @FACode
		UPDATE	TB_R_REQUEST_APPR
		SET		EMP_CODE	= @AECCode
		WHERE	[APPR_ROLE]		= @AECCode AND DOC_NO = @DOC_NO
	END
	*/
	--------------------------------------------------------------------------------------------------------------------
	---- First Person
	
	INSERT
	INTO	TB_H_REQUEST_APPR(DOC_NO, INDX, [ROLE], EMP_CODE, MAIN)
	SELECT	A.DOC_NO, A.INDX, A.[APPR_ROLE], A.EMP_CODE, NULL
	FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)
	LEFT	JOIN
			TB_H_REQUEST_APPR H WITH (NOLOCK)
	ON		A.DOC_NO = H.DOC_NO AND
			A.INDX	 = H.INDX
	WHERE	H.[ROLE] IS NULL AND A.DOC_NO = @DOC_NO AND A.APPR_STATUS = 'P' AND
			A.EMP_CODE IS NOT NULL
	
	

	IF NOT EXISTS(SELECT 1 FROM TB_R_REQUEST_APPR WHERE DOC_NO = @DOC_NO AND APPR_STATUS IN ('A','W'))
	BEGIN
		-- Get First Role
		DECLARE @R VARCHAR(3), @Indx INT
		SELECT TOP 1 @R = [APPR_ROLE], @Indx = INDX FROM TB_R_REQUEST_APPR WITH (NOLOCK) WHERE DOC_NO = @DOC_NO ORDER BY INDX
		-- If AEC create request.
		IF EXISTS(SELECT 1 FROM TB_M_SP_ROLE E WHERE EMP_CODE = @EmpCode AND [SP_ROLE] = @AECCode)
		BEGIN -- Set Login is first person
				UPDATE	TB_H_REQUEST_APPR
				SET		EMP_CODE	= @EmpCode
				WHERE	INDX		= @Indx AND DOC_NO = @DOC_NO

				UPDATE	TB_R_REQUEST_APPR
				SET		EMP_CODE	= @EmpCode
				WHERE	INDX		= @Indx AND DOC_NO = @DOC_NO
		END
		ELSE IF EXISTS(SELECT 1 FROM TB_H_REQUEST_APPR WITH (NOLOCK)
					WHERE DOC_NO = @DOC_NO AND INDX = @Indx AND EMP_CODE = @EmpCode)
		BEGIN
				UPDATE	TB_R_REQUEST_APPR
				SET		EMP_CODE	= @EmpCode
				WHERE	INDX		= @Indx AND DOC_NO = @DOC_NO
		END
		ELSE
		BEGIN
			DECLARE @ErrMsg VARCHAR(255)
			SET @ErrMsg = dbo.fn_GetMessage('MFAS1908AERR');
			--print @ErrMsg
			-- No authorize to create document
			THROW 50001, @ErrMsg, 1;
			
		END
	END

	------------------------------------------------------------------------------------------------------------------
	-- Chekc Higher Approve
	UPDATE @TB_APPV_CC SET USED_FLAG = 'N'
	WHILE(EXISTS(SELECT 1 FROM @TB_APPV_CC WHERE USED_FLAG = 'N' AND HIGHER_APPR = 'Y' ))
	BEGIN
		
		SELECT	TOP 1	@Indx = SEQ FROM	@TB_APPV_CC WHERE	USED_FLAG = 'N' AND HIGHER_APPR = 'Y' ORDER BY SEQ
		UPDATE	@TB_APPV_CC SET	USED_FLAG = 'Y' WHERE	SEQ = @Indx

		-- Check Exists Or Not ?
		IF EXISTS(SELECT 1 FROM TB_H_REQUEST_APPR WITH (NOLOCK) WHERE DOC_NO = @DOC_NO AND INDX = @Indx)
			CONTINUE -- No need

	
		-- Load Higher of Same Group
		DECLARE @ApprGrp VARCHAR(3), @NextHigher INT, @Req VARCHAR(1), @RL VARCHAR(3)
		SELECT	TOP 1 @ApprGrp = APPRV_GROUP, 
				@Req = REQUIRE_FLAG,
				@RL	= [APPR_ROLE] 
		FROM	TB_R_REQUEST_APPR WITH (NOLOCK)
		WHERE	DOC_NO = @DOC_NO AND INDX = @Indx
		--IF @Req != 'Y'
		--	CONTINUE


		SELECT	@NextHigher = MIN(H.INDX) 
		FROM	TB_R_REQUEST_APPR R WITH (NOLOCK)
		INNER	JOIN
				TB_H_REQUEST_APPR H WITH (NOLOCK)
		ON		R.DOC_NO = H.DOC_NO AND
				R.INDX	 = H.INDX
		WHERE	R.DOC_NO = @DOC_NO AND R.INDX > @Indx AND 
				APPRV_GROUP = @ApprGrp AND ISNULL(@ApprGrp,'') != ''

	

		
		INSERT
		INTO	TB_H_REQUEST_APPR(DOC_NO, INDX, [ROLE], EMP_CODE, MAIN)
		SELECT	@DOC_NO, @Indx, @RL, EMP_CODE, MAIN
		FROM	TB_H_REQUEST_APPR WITH (NOLOCK)
		WHERE	DOC_NO = @DOC_NO AND INDX = @NextHigher

		CONTINUE
	END

	IF OBJECT_ID('tempdb..#TB_T_CC') IS NOT NULL
	DROP TABLE #TB_T_CC

	IF OBJECT_ID('tempdb..#TB_T_CC_TARGET') IS NOT NULL
	DROP TABLE #TB_T_CC_TARGET
	



	------------------------------------------------------------------------------------------------------------------
	-- UPd
	UPDATE	H
	SET		H.ACTUAL_ROLE	= E.[ROLE],
			H.EMP_TITLE		= E.EMP_TITLE,
			H.EMP_NAME		= E.EMP_NAME,
			H.EMP_LASTNAME	= E.EMP_LASTNAME,
			H.EMAIL			= E.EMAIL,
			H.ORG_CODE		= E.ORG_CODE
	FROM	TB_H_REQUEST_APPR H WITH (NOLOCK)
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		E.EMP_CODE	= H.EMP_CODE
	WHERE	H.DOC_NO	= @DOC_NO
	
END
GO
