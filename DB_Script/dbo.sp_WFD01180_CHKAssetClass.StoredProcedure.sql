DROP PROCEDURE [dbo].[sp_WFD01180_CHKAssetClass]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01180_CHKAssetClass]
AS
BEGIN
	SET NOCOUNT ON;
	select M.CODE,M.[VALUE] from TB_M_SYSTEM M
	where M.CATEGORY = 'ASSET_CLASS' 
		and M.SUB_CATEGORY = 'ASSET_CLASS'
		and (CHARINDEX('Right-of-use',M.VALUE) > 0 OR CHARINDEX('leasing',M.VALUE) > 0)
END
GO
