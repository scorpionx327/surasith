DROP PROCEDURE [dbo].[sp_WFD02650_GetStockPlanHData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 02-03-2017
-- Description:	Get stock plan header data for handheld
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_GetStockPlanHData]
	@IS_FA_ADMIN varchar(1)
AS
BEGIN
	SET NOCOUNT ON;

	IF @IS_FA_ADMIN='Y' 
	BEGIN

		select
			top 1
			*
		from TB_R_STOCK_TAKE_H
		where ASSET_LOCATION =	'O' and PLAN_STATUS in ('C','S')
		order by CREATE_DATE desc

	END
	ELSE
	BEGIN

		select
			top 1
			*
		from TB_R_STOCK_TAKE_H
		where ASSET_LOCATION =	'I' and PLAN_STATUS in ('C','S')
		order by CREATE_DATE desc

	END

	
END





GO
