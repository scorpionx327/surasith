DROP PROCEDURE [dbo].[sp_WFD02310_ClearAssetList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 11/03/2017
-- Description:	Clear Temp Asset Disposal
-- =============================================
--exec sp_WFD02510_ClearAssetDisposal '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD02310_ClearAssetList]
(	
	@GUID		T_GUID
)
AS
BEGIN
	
	-- Insert into temp
	DELETE
	FROM	TB_T_SELECTED_ASSETS
	WHERE	[GUID]		= @GUID

	-- Insert into temp impair
	DELETE
	FROM	TB_T_REQUEST_REPRINT 
	WHERE	[GUID]		= @GUID

END
GO
