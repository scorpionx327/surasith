DROP PROCEDURE [dbo].[sp_BFD01220_ReceiveIFCostCenterBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_BFD01220_ReceiveIFCostCenterBatch]
(
 	@AppID	INT,
	@UserID		VARCHAR(8)
)
AS
BEGIN TRY
	DECLARE	@cnt	INT

	SELECT	@cnt = COUNT(*)
	FROM	TB_S_COST_CENTER 
	IF(@cnt = 0)
	BEGIN
		EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MCOM0020AWRN', 'Receive Cost Center',@UserID
		SELECT 2 AS SUCCESS ; -- Warning
		RETURN;
	END

    DECLARE @rs INT
	EXEC @rs = sp_BFD01220_Validation @AppID, @UserID
	
	IF @rs = 2
	BEGIN
		SELECT 0 AS SUCCESS ; -- Error
		RETURN;
	END
	  

	BEGIN TRANSACTION T1
	
	DELETE FROM TB_T_COST_CENTER

	INSERT
	INTO	TB_T_COST_CENTER
	(		COMPANY, COST_CODE, COST_NAME, ASSET_CNT, ASSET_RESP_CNT, FLAG)
	SELECT	S.COMPANY, S.COST_CODE, S.COST_NAME, 0,0, 'N' -- New
	FROM	TB_S_COST_CENTER S
	LEFT	JOIN
			TB_M_COST_CENTER M
	ON		S.COMPANY	= M.COMPANY AND
			S.COST_CODE = M.COST_CODE
	WHERE	M.COST_CODE IS NULL

	-- 1 Update Existing Cost Center
	UPDATE	M
	SET		M.COST_NAME			= S.COST_NAME,
			M.COST_CENTER_DESC	= S.COST_CENTER_DESC,
			M.ENABLE_FLAG		= S.ENABLE_FLAG,
			M.EFFECTIVE_FROM	= CONVERT(DATE, S.EFFECTIVE_FROM,112),
			M.EFFECTIVE_TO		= CONVERT(DATE, S.EFFECTIVE_TO,112),
			M.PERSON_RESPONSE	= S.PERSON_RESP_NAME,
			M.DEPARTMENT		= S.DEPARTMENT,
			M.COST_CENTER_CATG	= S.COST_CENTER_CATG,
			M.HIERARCHY_AREA	= S.HIERARCHY_AREA,
			M.CURRENCY			= S.CURRENCY,
			M.PROFIT_CENTER		= S.PROFIT_CENTER,
			M.[STATUS]			= CASE WHEN S.ENABLE_FLAG = 'Y' AND 
											GETDATE() BETWEEN	CONVERT(DATE, S.EFFECTIVE_FROM,112) AND 
																CONVERT(DATE, S.EFFECTIVE_TO,112) THEN 'Y' ELSE 'N' END,
			M.UPDATE_DATE		= GETDATE(),
			M.UPDATE_BY 		= @UserID
	FROM	TB_M_COST_CENTER M
	INNER	JOIN
			TB_S_COST_CENTER S
	ON		M.COMPANY	= S.COMPANY AND
			M.COST_CODE	= S.COST_CODE
	
	-- 2 Insert New Cost Center
	INSERT
	INTO	TB_M_COST_CENTER
	(		COMPANY,
			COST_CODE,
			COST_NAME,
			COST_CENTER_DESC,
			MAP_PATH,
			ENABLE_FLAG,
			EFFECTIVE_FROM,
			EFFECTIVE_TO,
			PERSON_RESPONSE,
			DEPARTMENT,
			COST_CENTER_CATG,
			HIERARCHY_AREA,
			CURRENCY,
			PROFIT_CENTER,
			[STATUS],
			CREATE_DATE,			CREATE_BY,			UPDATE_DATE,			UPDATE_BY
	)
	SELECT	S.COMPANY,
			S.COST_CODE,
			S.COST_NAME,
			S.COST_CENTER_DESC,
			NULL MAP_PATH,
			S.ENABLE_FLAG,
			CONVERT(DATE,S.EFFECTIVE_FROM, 112),
			CONVERT(DATE, S.EFFECTIVE_TO, 112),
			S.PERSON_RESP_NAME,
			S.DEPARTMENT,
			S.COST_CENTER_CATG,
			S.HIERARCHY_AREA,
			S.CURRENCY,
			S.PROFIT_CENTER,
			CASE WHEN S.ENABLE_FLAG = 'Y' AND 
						GETDATE() BETWEEN CONVERT(DATE, S.EFFECTIVE_FROM,112) AND 
											CONVERT(DATE, S.EFFECTIVE_TO,112) 
					THEN 'Y' ELSE 'N' END,

			GETDATE(),			@UserID,			GETDATE(),			@UserID
	FROM	TB_S_COST_CENTER S
	LEFT	JOIN
			TB_M_COST_CENTER M
	ON		M.COMPANY	= S.COMPANY AND
			M.COST_CODE	= S.COST_CODE
	WHERE	M.COST_CODE IS NULL

	-- 3 Update Inactive incase no found
	UPDATE	M
	SET		M.[STATUS]		= 'N',
			M.UPDATE_DATE	= GETDATE(),
		    M.UPDATE_BY		= @UserID
	FROM	TB_M_COST_CENTER M
	LEFT	JOIN
			TB_S_COST_CENTER S
	ON		M.COST_CODE = S.COST_CODE AND
			M.COMPANY	= S.COMPANY
	WHERE	S.COST_CODE IS NULL

	-- Keep for send email
	INSERT
	INTO	TB_T_COST_CENTER
	(		COMPANY, COST_CODE, COST_NAME, FLAG)
	SELECT	M.COMPANY, M.COST_CODE, M.COST_NAME, 'D' -- Inactive Assets
	FROM	TB_M_COST_CENTER M
	WHERE	M.[STATUS]	= 'N'



	IF EXISTS(SELECT 1 FROM TB_T_COST_CENTER WHERE FLAG = 'N')
	BEGIN
		EXEC sp_BFD01220_NotifyEmailNewCostCenter @AppID, @UserID
	END

	IF EXISTS(SELECT 1 FROM TB_T_COST_CENTER WHERE FLAG = 'D')
	BEGIN
		EXEC sp_BFD01220_NotifyEmailInActiveAssetsUnderRequest @AppID, @UserID
		EXEC sp_BFD01220_RemainAssetsUnderInActiveCostCenterProcess @AppID, @UserID
	END



   /*

          -- 7  In case Cost Center expire date
			 DECLARE @Msg VARCHAR(500)
	 		DECLARE @Msg_MCOM0009BWRN VARCHAR(8000)

			SELECT @Msg_MCOM0009BWRN= CONCAT(@Msg_MCOM0009BWRN+',','') + COST_CODE
			 FROM TB_M_COST_CENTER CostCenter 
			 WHERE CostCenter.[STATUS]='N' AND -- Incactive
			   EXISTS( SELECT '1'
							 FROM  TB_R_REQUEST_H requstH
							  WHERE CostCenter.COST_CODE=requstH.COST_CODE
									AND  requstH.[STATUS]  < 60 )

			IF (@Msg_MCOM0009BWRN IS NOT NULL)
			BEGIN 
				 SET		@Msg = dbo.fn_GetMessage('MCOM0009BWRN')
				INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  				SELECT  @AppID, 'P', 'N', 'W',
						dbo.fn_StringFormat(@Msg , @Msg_MCOM0009BWRN),'Y', @UserID, GETDATE()
		
			END
			*/
	COMMIT TRANSACTION T1
	SELECT 1 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
	RETURN;


END TRY
BEGIN CATCH

	IF @@TRANCOUNT <> 0
	BEGIN
		ROLLBACK TRANSACTION T1
	END
	--===========================================================Write log =====================================
     DECLARE @errMessage nVARCHAR(1000);
     SET @errMessage= CONCAT('Receiving Cost Center Master Interface batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
	 exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@errMessage,@UserID,'Y'
	--===========================================================End Write log =====================================
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    SELECT @ErrorMessage = ERROR_MESSAGE();
    SELECT @ErrorSeverity = ERROR_SEVERITY();
    SELECT @ErrorState = ERROR_STATE();
    RAISERROR (@ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
                );
	SELECT 0 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
	RETURN;

END CATCH
GO
