DROP PROCEDURE [dbo].[sp_WFD01180_GetAssetClass]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01180_GetAssetClass]
AS
BEGIN
	SET NOCOUNT ON;
	select CODE,[VALUE] from TB_M_SYSTEM where	CATEGORY = 'ASSET_CLASS' AND SUB_CATEGORY = 'ASSET_CLASS' AND ACTIVE_FLAG = 'Y'
END
GO
