DROP PROCEDURE [dbo].[sp_WFD01020_LoadSystemMasterSubCategory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jatuporn Sornsiriwong
-- Create date: 13/03/2017
-- Description:	Load distinct sub category
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01020_LoadSystemMasterSubCategory]	
@CATEGORY VARCHAR(40)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT DISTINCT SUB_CATEGORY AS Value FROM TB_M_SYSTEM WHERE CATEGORY = @CATEGORY
END





GO
