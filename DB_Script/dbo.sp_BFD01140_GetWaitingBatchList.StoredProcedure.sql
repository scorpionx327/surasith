DROP PROCEDURE [dbo].[sp_BFD01140_GetWaitingBatchList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_BFD01140_GetWaitingBatchList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	IF OBJECT_ID('tempdb..#TB_T_BATCH_Q') IS NOT NULL DROP TABLE #TB_T_BATCH_Q
     SELECT
			Q.UPDATE_DATE AS EXEC_DATE
			,Q.APP_ID AS REQ_ID
			,Q.BATCH_ID
			,H.BATCH_NAME
			,H.BATCH_GROUP_ID AS GRP
			,H.EXEC_PATH
			,Q.[STATUS]
			, H.EXEC_PATH AS [PRIORITY]	
	 INTO #TB_T_BATCH_Q		
	FROM [dbo].[TB_BATCH_Q] AS Q
	INNER JOIN [dbo].[TB_BATCH_MASTER_H] AS H 	ON H.BATCH_ID = Q.BATCH_ID
	WHERE 1<> 1


	INSERT INTO #TB_T_BATCH_Q
			(EXEC_DATE
			,REQ_ID
			,BATCH_ID
			,BATCH_NAME
			,GRP
			,EXEC_PATH
			,[STATUS]
			,[PRIORITY])

	SELECT
			Q.UPDATE_DATE AS EXEC_DATE
			,Q.APP_ID AS REQ_ID
			,Q.BATCH_ID
			,H.BATCH_NAME
			,H.BATCH_GROUP_ID AS GRP
			,H.EXEC_PATH
			,Q.[STATUS]
			,NULL AS [PRIORITY]			
	FROM [dbo].[TB_BATCH_Q] AS Q
	INNER JOIN ( 
			SELECT
				MIN(APP_ID) AS APP_ID
			FROM [dbo].[TB_BATCH_Q] AS Q 
			INNER JOIN TB_BATCH_MASTER_H H ON Q.BATCH_ID = H.BATCH_ID
			WHERE UPPER(Q.[STATUS]) = 'Q'
			GROUP BY ISNULL(CONVERT(VARCHAR,H.BATCH_GROUP_ID), Q.BATCH_ID)
			--ORDER BY REQ_ID ASC
	) AS A ON Q.APP_ID = A.APP_ID
	--WHERE UPPER(Q.[STATUS]) = 'Q'
	--ORDER BY Q.APP_ID ASC
	INNER JOIN [dbo].[TB_BATCH_MASTER_H] AS H
			ON H.BATCH_ID = Q.BATCH_ID
	WHERE UPPER(Q.[STATUS]) = 'Q'
	--ORDER BY Q.APP_ID ASC


	SELECT * FROM #TB_T_BATCH_Q ORDER BY REQ_ID ASC



	SELECT 
			P.APP_ID AS REQ_ID
			,P.PARAM_SEQ AS [SEQ]
			,P.PARAM_NAME AS [PARAMETERNAME]
			,P.PARAM_VALUE	 AS [PARAMETERVALUE]
	FROM #TB_T_BATCH_Q AS Q	 
	INNER JOIN [dbo].[TB_BATCH_Q_PARAM] AS P
			ON Q.REQ_ID = P.APP_ID
	ORDER BY Q.REQ_ID ASC, P.PARAM_SEQ ASC




END







GO
