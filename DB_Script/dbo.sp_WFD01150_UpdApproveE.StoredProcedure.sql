DROP PROCEDURE [dbo].[sp_WFD01150_UpdApproveE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_UpdApproveE]
		@APPRV_ID int,
        @INDX int,
        @ROLE varchar(3),
        @E_INDX int,
        @E_ROLE varchar(3),
        @EMAIL_APR_TO char(1),
        @EMAIL_APR_CC char(1),
        @EMAIL_REJ_TO char(1),
        @EMAIL_REJ_CC char(1)
AS
BEGIN TRY 
	SET NOCOUNT ON;

	UPDATE TB_M_APPROVE_E
    SET [ROLE] = @ROLE
        ,E_ROLE = @E_ROLE
           ,EMAIL_APR_TO = @EMAIL_APR_TO
           ,EMAIL_APR_CC = @EMAIL_APR_CC
           ,EMAIL_REJ_TO = @EMAIL_REJ_TO
           ,EMAIL_REJ_CC = @EMAIL_REJ_CC 
	WHERE APPRV_ID = @APPRV_ID
          AND INDX = @INDX
          AND E_INDX = @E_INDX
END TRY
BEGIN CATCH
	THROW
END CATCH
GO
