DROP PROCEDURE [dbo].[sp_WFD01270_GetPermission]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetPermission 'C2017/4064','1432','C' 
CREATE PROCEDURE [dbo].[sp_WFD01270_GetPermission]
(
	@DOC_NO						VARCHAR(10),
	@EMP_CODE					VARCHAR(8),
	@REQUEST_TYPE				VARCHAR(1)
)
	
AS
BEGIN
	
	-- Remark No Allow delegate of Higher

	DECLARE @MODE				VARCHAR(1) = 'N', -- N : No authorize, V : View Only, A:Approve, R : VERIFY
			@DELEGATE			VARCHAR(1) = 'N',
			@MAIN				VARCHAR(1) = 'N',
			@HIGHER				VARCHAR(1) = 'N',
			@FA					VARCHAR(1) = 'N',
			@DELETE_ASSET		VARCHAR(1) = 'N', -- Delete Item	
			@ALLOW_APPROVE		VARCHAR(1) = 'N',
			@ALLOW_REJECT		VARCHAR(1) = 'N',
			@ALLOW_SEL_APPV		VARCHAR(1) = 'N'	
	
	DECLARE @DocStatus		VARCHAR(40)
	SELECT	@DocStatus		= [STATUS] FROM TB_R_REQUEST_H WITH (NOLOCK) WHERE DOC_NO = @DOC_NO
	SET		@DocStatus		= ISNULL(@DocStatus,'00')

	PRINT CONCAT('DOC STATUS :',@DocStatus)

	IF (@DocStatus = '80')
	BEGIN
		SET @MODE = 'V'
		SET @ALLOW_APPROVE = 'N'
		SET @ALLOW_REJECT = 'N'
		GOTO RET
	END

	------------------------------------------------------------------------------------------------
	-- Document No exists

	DECLARE @CurrentApprover VARCHAR(8), @CurrentApproveGrp VARCHAR(3), @CurrentIndx INT, 
			@AllowReject VARCHAR(1), @AllowDelete VARCHAR(1),
			@CurrentSelAppr VARCHAR(1),
			@OrgCode	VARCHAR(21)
	SELECT	@CurrentApprover	= EMP_CODE, 
			@CurrentApproveGrp	= APPRV_GROUP,
			@CurrentIndx		= INDX,
			@AllowReject		= ALLOW_REJECT,
			@AllowDelete		= ALLOW_DEL_ITEM,
			@CurrentSelAppr		= ALLOW_SEL_APPRV,
			@OrgCode			= ORG_CODE
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO = @DOC_NO AND APPR_STATUS = 'W'
	------------------------------------------------------------------------------------------------

	-- Is Admin ?
	DECLARE @Cnt INT, @Cond INT
	SELECT	@Cnt = COUNT(*),
			@Cond = SUM(CASE WHEN EMP_CODE = @EMP_CODE THEN 1 ELSE 0 END)
	FROM	TB_M_EMPLOYEE WHERE FAADMIN = 'Y'  AND (EMP_CODE = @EMP_CODE OR (DELEGATE_PIC = @EMP_CODE AND  GETDATE() BETWEEN DELEGATE_FROM AND DELEGATE_TO))
	IF @Cnt > 0
	BEGIN
		PRINT 'FA'
		SET @FA			= 'Y'
		SET @DELEGATE	= IIF(@Cond = 0, 'Y', 'N')
		SET @ALLOW_APPROVE	= 'Y'
		
		------------------------------------------------------------------------------------------- 
		DECLARE @CurrentRole VARCHAR(3)
		SELECT	TOP 1 @CurrentRole = [APPR_ROLE] 
		FROM	TB_R_REQUEST_APPR WHERE DOC_NO = @DOC_NO AND APPR_STATUS = 'W'
		-- Add Support Case FA Admin can delete/reject follow approve master 2017-05-12
		IF ISNULL(@CurrentRole,'-') = 'FA'
		BEGIN
			SET @ALLOW_REJECT	= @AllowReject
			SET @DELETE_ASSET	= @AllowDelete
			SET @ALLOW_SEL_APPV = @CurrentSelAppr
			GOTO RET
		END

		-------------------------------------------------------------------------------------------
		-- Incase FA Admin Login to other status

		SET @DELETE_ASSET	= IIF(@DocStatus NOT IN ('80'),'Y', @DELETE_ASSET)
		SET @ALLOW_REJECT	= IIF(@DocStatus IN ('10','60','90','99'),'Y', @ALLOW_REJECT) -- Allow FA to Reject when Document not complete
		SET @ALLOW_SEL_APPV = IIF(@DocStatus NOT IN ('60','70','80'),'Y', @ALLOW_SEL_APPV) 
		GOTO RET

	END	

	IF @DOC_NO IS NULL 
	BEGIN
		SET @DocStatus = '00'

		-- Check SV (SV Create a document)
		--IF EXISTS(SELECT 1 FROM TB_M_SV_COST_CENTER WHERE EMP_CODE = @EMP_CODE)
		--BEGIN
		SET @MAIN = 'Y'
		SET @DELETE_ASSET = 'Y'
		SET @ALLOW_SEL_APPV = 'Y'
		GOTO RET
	END

	IF @EMP_CODE = @CurrentApprover
	BEGIN
		SET @MAIN	= 'Y'
		SET @ALLOW_REJECT = @AllowReject
		SET @DELETE_ASSET = @AllowDelete
		SET @ALLOW_SEL_APPV = @CurrentSelAppr
		GOTO RET
	END
	ELSE IF dbo.fn_IsDelegateUser(@CurrentApprover, @EMP_CODE) = 'Y'
	BEGIN
		SET @DELEGATE	= 'Y'
		SET @ALLOW_REJECT = @AllowReject
		SET @DELETE_ASSET = @AllowDelete
		SET @ALLOW_SEL_APPV = @CurrentSelAppr
		GOTO RET
	END

	-- No support delegate of higher
	-- Check Higher
	SELECT	TOP 1
			@AllowReject	= ALLOW_REJECT,
			@AllowDelete	= ALLOW_DEL_ITEM, 
			@CurrentSelAppr = ALLOW_SEL_APPRV,
			@OrgCode		= ORG_CODE
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND 
			APPR_STATUS		= 'P' AND
			APPRV_GROUP		= ISNULL(@CurrentApproveGrp, '-') AND
			INDX			> @CurrentIndx AND
			EMP_CODE		= @EMP_CODE
	
	IF @@ROWCOUNT > 0
	BEGIN
		SET @HIGHER = 'Y'
		SET @ALLOW_REJECT = @AllowReject
		SET @DELETE_ASSET = @AllowDelete
		SET @ALLOW_SEL_APPV = @CurrentSelAppr
		GOTO RET
	END
	-- Otherwise, Exists in Flow but can not approve
	SELECT	TOP 1
			@AllowReject	= ALLOW_REJECT 
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND
			EMP_CODE		= @EMP_CODE
	IF @@ROWCOUNT > 0
	BEGIN
		SET @ALLOW_REJECT = 'N'
		SET @DELETE_ASSET = 'N'
		SET @MODE = 'V'
		GOTO RET 
	END

RET:
	
	-- SET MODE
	IF (@FA = 'Y' OR @MAIN = 'Y' OR @DELEGATE = 'Y' OR @HIGHER = 'Y')
	BEGIN
		SET @MODE = 'A'
		SET @ALLOW_APPROVE	= 'Y' 
	END



	-- Check View Mode or No Authorize
	SELECT	@Cnt = COUNT(1) 
	FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		A.EMP_CODE = E.EMP_CODE
	WHERE	DOC_NO		= @DOC_NO AND 
		(	A.EMP_CODE = @EMP_CODE OR 
			dbo.fn_IsDelegateUser(A.EMP_CODE, @EMP_CODE) = 'Y' -- Delegate User
		)
	IF @MODE = 'N' AND @Cnt > 0 --OR  @REQUEST_TYPE = 'S')
	BEGIN
		SET @MODE = 'V'
	END

	IF @FA = 'Y' AND @DocStatus = '60'
	BEGIN
		IF @REQUEST_TYPE = 'S'	-- Stock Request not FA Verify
		BEGIN
			SET @MODE = 'V'
		END
		ELSE
		BEGIN
			SET @MODE = 'R'
		END		
	END
	-- Special Condition for Dispose
	IF @FA = 'Y' AND @DocStatus = '70' AND @REQUEST_TYPE = 'D'
	BEGIN
		
		SET @MODE = 'R'
		SET @ALLOW_APPROVE = 'Y' 
		SET @DELETE_ASSET = 'N'

		-- If send all to oracle 2017-06-26
		IF NOT EXISTS(SELECT 1 FROM TB_R_ASSET_HIS A WHERE A.DOC_NO = @DOC_NO AND SAP_SENT_DATE IS NULL)
		BEGIN
			SET @MODE = 'V'
		END
	END
	
	IF @DocStatus = '70' AND @REQUEST_TYPE <> 'D'  -- Suphachai L. 2017/04/19
	BEGIN
		SET @MODE = 'V'
		SET @ALLOW_APPROVE = 'N' 
		SET @ALLOW_REJECT = 'N' 
		SET @DELETE_ASSET = 'N'
	END
	--------------------------------------------------------------------------------
	-- Check Organization Changed
	DECLARE @OrgChanged VARCHAR(1) = 'N'

	IF(@FA != 'Y' AND @DELEGATE != 'Y' ) --delegate not in same Organize with PIC (not check)
	BEGIN
		SELECT	@OrgChanged = dbo.fn_IsOrganizeChanged([ROLE], @OrgCode, ORG_CODE)
		FROM	TB_M_EMPLOYEE 
		WHERE	EMP_CODE = @EMP_CODE
	END	
	
	SET @OrgChanged = ISNULL(@OrgChanged, 'N') -- FA, Not found

	IF @OrgChanged = 'Y' --AND @DELEGATE != 'Y'  
	BEGIN
		SET @ALLOW_APPROVE = 'N'
		SET @DELETE_ASSET = 'N'
		SET @ALLOW_SEL_APPV = 'N'
		PRINT 'Org is changed'
	END
	-- Special Condition -------------------------------------
	DECLARE @DIVISION VARCHAR(6)
	SELECT	@DIVISION = DIVISION 
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO = @DOC_NO AND APPR_STATUS = 'W'

	DECLARE @BOIDivision VARCHAR(6), @StoreDivision VARCHAR(6), @FADiv VARCHAR(6)

	SET @BOIDivision = dbo.fn_GetSystemMaster('APPROVE_FLOW','APP_DIVISION','BOI')
	SET @StoreDivision = dbo.fn_GetSystemMaster('APPROVE_FLOW','APP_DIVISION','STORE')

	SET @FADiv = dbo.fn_GetSystemMaster('APPROVE_FLOW','APP_DIVISION','FA')
	
	DECLARE @IS_ROLE_BOI_PP INT, @IS_ROLE_STORE_PP INT, @IS_ROLE_FA_PP INT

	SELECT @IS_ROLE_BOI_PP = COUNT(1) FROM TB_R_REQUEST_APPR
	WHERE DOC_NO = @DOC_NO AND DIVISION = @BOIDivision AND APPR_STATUS = 'W' AND APPR_ROLE = 'PP'

	SELECT @IS_ROLE_STORE_PP = COUNT(1) FROM TB_R_REQUEST_APPR
	WHERE DOC_NO = @DOC_NO AND DIVISION = @StoreDivision AND APPR_STATUS = 'W' AND APPR_ROLE = 'PP'

	SELECT @IS_ROLE_FA_PP = COUNT(1) FROM TB_R_REQUEST_APPR
	WHERE DOC_NO = @DOC_NO AND DIVISION = @FADiv AND APPR_STATUS = 'W' -- AND ROLE = 'PP'

	--
	DECLARE @BOICnt INT, @STORECnt INT, @FACnt INT

	SELECt @BOICnt = COUNT(*) FROM TB_R_REQUEST_APPR
	WHERE DOC_NO = @DOC_NO AND DIVISION = @BOIDivision AND APPR_STATUS IN ('A','W')

	SELECt @STORECnt = COUNT(*) FROM TB_R_REQUEST_APPR
	WHERE DOC_NO = @DOC_NO AND DIVISION = @StoreDivision AND APPR_STATUS IN ('A','W')

	SELECt @FACnt = COUNT(*) FROM TB_R_REQUEST_APPR
	WHERE DOC_NO = @DOC_NO AND DIVISION = @FADiv AND APPR_STATUS IN ('A','W')
	

	DECLARE @IS_REQ VARCHAR(1)
	SET @IS_REQ = 'Y'
	
	SET @IS_REQ = IIF(@MODE IN ('V','R'),'N',@IS_REQ)

	SET @IS_REQ = IIF(@MODE = 'A' AND @REQUEST_TYPE IN ('P','R','L') AND @DocStatus IN ('10','60','80'), 'N',@IS_REQ)
	SET @IS_REQ = IIF(@MODE = 'A' AND @REQUEST_TYPE IN ('C') AND @DocStatus IN ('60','80'), 'N',@IS_REQ)
	SET @IS_REQ = IIF(@MODE = 'A' AND @REQUEST_TYPE IN ('T') AND @DocStatus IN ('10','60','80'), 'N',@IS_REQ)
	SET @IS_REQ = IIF(@MODE = 'A' AND @REQUEST_TYPE IN ('D') AND @DocStatus IN ('10','60','80'), 'N',@IS_REQ)
	 
	DECLARE @IS_BOI_STATE VARCHAR(1)
	SET @IS_BOI_STATE = 'N'

	SET @IS_BOI_STATE = IIF(@MODE = 'A' AND @DIVISION = @BOIDivision AND @IS_ROLE_BOI_PP > 0,'Y',@IS_BOI_STATE)

	DECLARE @IS_STORE_STATE VARCHAR(1)
	SET @IS_STORE_STATE = 'N'

	SET @IS_STORE_STATE = IIF(@MODE = 'A' AND @DIVISION = @StoreDivision AND @IS_ROLE_STORE_PP > 0,'Y',@IS_STORE_STATE)


	DECLARE @IS_FA_STATE VARCHAR(1)
	SET @IS_FA_STATE = 'N'

	SET @IS_FA_STATE = IIF(@MODE = 'A' AND @DIVISION = @FADiv AND @IS_ROLE_FA_PP > 0,'Y',@IS_FA_STATE)


	---Suphachai L.
	IF(@DocStatus IN ('90','99'))
	BEGIN
		IF(@FA = 'Y' OR @MAIN = 'Y')
		BEGIN
			SET @MODE			= 'A'
			SET @ALLOW_REJECT	= 'Y'
		END
		ELSE
		BEGIN
			SET @MODE			= 'V'
			SET @ALLOW_APPROVE	= 'N'
			SET @ALLOW_REJECT	= 'N'
			SET @DELETE_ASSET	= 'N'	
		END
	END

	SELECT	@MODE		MODE, 
			@DELEGATE	DELEGATE,
			@MAIN		MAIN,
			@HIGHER		HIGHER,
			@FA			FA,
			@ALLOW_APPROVE	ALLOW_APPROVE,	-- BUTTON
			@ALLOW_REJECT	ALLOW_REJECT,	-- BUTTON
			@DELETE_ASSET DELETE_ASSET,
			@ALLOW_SEL_APPV  ALLOW_SEL_APPV,
			@DocStatus	DOC_STATUS,
			IIF((ISNULL(@BOICnt,0) > 0 OR @IS_BOI_STATE = 'Y') OR (ISNULL(@STORECnt,0) > 0 OR @IS_STORE_STATE = 'Y'),'N',@IS_REQ)	IS_REQ,
			@DIVISION	CURRENT_STATE,
			@IS_BOI_STATE IS_BOI_STATE,
			@IS_STORE_STATE IS_STORE_STATE,
			IIF(ISNULL(@BOICnt,0) > 0 OR @IS_BOI_STATE = 'Y', 'Y','N') VIEW_BOI_STATE,
			IIF(ISNULL(@STORECnt,0) > 0 OR @IS_STORE_STATE = 'Y', 'Y','N') VIEW_STORE_STATE,

			@IS_FA_STATE IS_FA_STATE,
			IIF(ISNULL(@FACnt,0) > 0 OR @IS_FA_STATE = 'Y', 'Y','N') VIEW_FA_STATE,

			@OrgChanged IS_ORG_CHANGED
			
END
GO
