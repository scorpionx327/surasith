DROP PROCEDURE [dbo].[sp_WFD02910_GenerateAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02910_GenerateAsset]
@GUID				T_GUID,
@DOC_NO				T_DOC_NO,
@LINE_NO			INT,
@USER				T_SYS_USER
AS
BEGIN
	-- this function is execute from screen, manage data on temp table only
	
	UPDATE	TB_T_REQUEST_ASSET_D
	SET		[STATUS]	= 'GEN'
	WHERE	[GUID]		= @GUID AND
			LINE_NO		= @LINE_NO AND
			ASSET_NO	IS NULL

	UPDATE	TB_R_REQUEST_ASSET_D
	SET		[STATUS]	= 'GEN'
	WHERE	DOC_NO		= @DOC_NO AND
			LINE_NO		= @LINE_NO	 AND
			ASSET_NO	IS NULL
	
END
GO
