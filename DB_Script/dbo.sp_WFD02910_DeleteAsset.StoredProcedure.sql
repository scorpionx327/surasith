DROP PROCEDURE [dbo].[sp_WFD02910_DeleteAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_ClearAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD02910_DeleteAsset]
(	
	@GUID		T_GUID,
	@DOC_NO		T_DOC_NO,
	@COMPANY	T_COMPANY,
	@LINE_NO	INT
)
AS
BEGIN
	
	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_H WHERE DOC_NO = @DOC_NO)
	BEGIN
		UPDATE	TB_R_REQUEST_ASSET_D
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				COMPANY		= @COMPANY AND
				LINE_NO		= @LINE_NO

		UPDATE	TB_R_REQUEST_RECLAS
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				COMPANY		= @COMPANY AND
				LINE_NO		= @LINE_NO
		
	END

	-- delete if exists in submit mode
	DELETE	T
	FROM	TB_T_SELECTED_ASSETS T
	WHERE	T.[GUID]		= @GUID AND 
			T.COMPANY		= @COMPANY AND
			NOT EXISTS(	SELECT	1 
						FROM	TB_T_REQUEST_ASSET_D D
						WHERE	D.COMPANY = @COMPANY AND 
								D.[GUID]	= @GUID AND
								D.ASSET_NO	= T.ASSET_NO AND 
								D.ASSET_SUB	= T.ASSET_SUB )
	
	DELETE	TB_T_REQUEST_ASSET_D
	WHERE	[GUID]		= @GUID AND 
			COMPANY		= @COMPANY AND
			LINE_NO		= @LINE_NO

	DELETE	TB_T_REQUEST_RECLAS
	WHERE	[GUID]		= @GUID AND 
			COMPANY		= @COMPANY AND
			LINE_NO		= @LINE_NO

END
GO
