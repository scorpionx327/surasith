DROP PROCEDURE [dbo].[sp_WFD02160_UpdateMapLayOut]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====WFD02160 : Fixed Asset Map Location ==================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/11 (yyyy/mm/dd)
*/
CREATE PROCEDURE [dbo].[sp_WFD02160_UpdateMapLayOut]
	(	@COMPANY		T_COMPANY	= NULL,
		@ASSET_NO		T_ASSET_NO	= NULL,	
	    @COST_CODE		T_COST_CODE	= NULL,
		@POINT_X		INT			= NULL,
		@POINT_Y		INT			= NULL,
		@DEGREE			INT			= NULL,	--Pitiphat CR-B-011 20170908
		@IS_MAPPING		VARCHAR(1), --- Y is mean mapping, have value X and Y 
	    @UPDATE_BY		VARCHAR(8),
		@SEQ			INT			= NULL													
	)
AS

BEGIN TRY
	IF (@IS_MAPPING='Y')
	BEGIN
---1. Update location --> TB_M_ASSETS_LOC 
			UPDATE Loc
			SET			   Loc.X				= @POINT_X 
						 , Loc.Y				= @POINT_Y
						 , Loc.DEGREE			= @DEGREE	--Pitiphat CR-B-011 20170908
						 , Loc.UPDATE_BY		= @UPDATE_BY
						 , Loc.COST_CODE		= UPPER(@COST_CODE) 
						 , Loc.UPDATE_DATE		= GETDATE()
						 , Loc.SEQ				= @SEQ
			FROM	TB_M_ASSETS_LOC Loc
			WHERE	Loc.COMPANY		= @COMPANY AND 
					Loc.ASSET_NO	= @ASSET_NO  

---2. Update Map_Status on TB_M_ASSETS_H 
			UPDATE AssetH
			SET			   AssetH.MAP_STATUS	= 'Y'
						, AssetH.UPDATE_BY		= @UPDATE_BY
						, AssetH.UPDATE_DATE	= GETDATE()
			FROM TB_M_ASSETS_H AssetH
			WHERE AssetH.ASSET_NO				= @ASSET_NO AND
				AssetH.COMPANY	= @COMPANY
	END

	IF (@IS_MAPPING='N')
	BEGIN
    ---1. Update location --> TB_M_ASSETS_LOC 
			UPDATE Loc
			SET			  Loc.X					= NULL 
						, Loc.Y					= NULL
						, Loc.DEGREE			= NULL		--Pitiphat CR-B-011 20170908
						--, Loc.COST_CODE			= NULL		-- No need to update cost code // fix 20190929  Pawares M.
						, Loc.UPDATE_BY			= @UPDATE_BY
						, Loc.UPDATE_DATE		= GETDATE()
						, Loc.SEQ				= NULL
			FROM TB_M_ASSETS_LOC Loc
			WHERE	Loc.ASSET_NO				= @ASSET_NO 
						AND Loc.COST_CODE		= @COST_CODE AND
							Loc.COMPANY			= @COMPANY

		   ---2. Update Map_Status on TB_M_ASSETS_H 
			UPDATE AssetH
		SET			      AssetH.MAP_STATUS		= 'N'
						, AssetH.UPDATE_BY		= @UPDATE_BY
						, AssetH.UPDATE_DATE	= GETDATE()
			FROM TB_M_ASSETS_H AssetH
			WHERE AssetH.ASSET_NO				= @ASSET_NO AND
					AssetH.COMPANY				= @COMPANY
	END

END TRY
BEGIN CATCH
	
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH
GO
