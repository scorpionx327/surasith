DROP PROCEDURE [dbo].[sp_BFD01210_ApproverResign]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [sp_BFD01210_ApproverResign] 99999,'System'
CREATE PROCEDURE [dbo].[sp_BFD01210_ApproverResign]
@AppID	NUMERIC(18,0),
@UserID	T_USER
AS
BEGIN
	DECLARE @TB_T_EMP TABLE
	(
		SYS_EMP_CODE	T_SYS_USER,
		FLAG			VARCHAR(1) -- R Resign, O : Org Change.
	)
	INSERT
	INTO	@TB_T_EMP (SYS_EMP_CODE, FLAG) -- Keep Resign
	SELECT	SYS_EMP_CODE, 'R'
	FROM	TB_M_EMPLOYEE
	WHERE	ACTIVEFLAG = 'N'

	INSERT
	INTO	@TB_T_EMP (SYS_EMP_CODE, FLAG) -- Keep Org Change
	SELECT	C.SYS_EMP_CODE, 'O'
	FROM	TB_T_EMPLOYEE_CHANGE C
	LEFT	JOIN
			@TB_T_EMP T
	ON		C.SYS_EMP_CODE = T.SYS_EMP_CODE 
	WHERE	T.SYS_EMP_CODE IS NULL AND  C.ORG_CHANGED	= 'Y'

	-- Only Name in list (R_APPROVE)
	DECLARE @TB_T_REQUEST TABLE
	(
		REQUEST_TYPE	VARCHAR(1) NULL,
		DOC_NO			T_DOC_NO NULL,
		APPROVER		T_USER NULL,
		EMAIL_TO		VARCHAR(MAX),
		EMAIL_TO_CC		VARCHAR(MAX) NULL,
		SEND_TO			T_USER NULL,
		SEND_NAME		VARCHAR(50) NULL,
		APPR_STATUS		VARCHAR(1)
	)
	
	-- Get Document of User that Resign / Org. Change 
	INSERT
	INTO	@TB_T_REQUEST
	( REQUEST_TYPE,DOC_NO, APPROVER, APPR_STATUS )
	SELECT	R.REQUEST_TYPE ,A.DOC_NO,	A.EMP_CODE, A.APPR_STATUS
	FROM	TB_R_REQUEST_APPR A
	INNER	JOIN
			@TB_T_EMP E
	ON		A.EMP_CODE		= E.SYS_EMP_CODE
	INNER	JOIN
			TB_R_REQUEST_H R
	ON		A.DOC_NO		= R.DOC_NO
	WHERE	A.APPR_STATUS	IN ('W','P') 
	
	INSERT
	INTO	@TB_T_REQUEST
	(		REQUEST_TYPE,DOC_NO, APPROVER, APPR_STATUS )
	SELECT	R.REQUEST_TYPE ,H.DOC_NO,	H.EMP_CODE, NULL AS APPR_STATUS
	FROM	TB_R_REQUEST_APPR A
	INNER	JOIN
			TB_H_REQUEST_APPR H
	ON		A.DOC_NO	= H.DOC_NO AND
			A.INDX		= H.INDX
	INNER	JOIN
			@TB_T_EMP E
	ON		H.EMP_CODE		= E.SYS_EMP_CODE
	LEFT	JOIN
			@TB_T_REQUEST T
	ON		A.DOC_NO = T.DOC_NO AND  A.EMP_CODE = T.APPROVER
	INNER	JOIN
			TB_R_REQUEST_H R
	ON		A.DOC_NO		= R.DOC_NO
			
	WHERE	A.APPR_STATUS	IN ('W','P') AND 
			T.DOC_NO IS NULL

	------------------------------------------------------------------------------------------------------------------
	-- Delete Org Code from approver list (W, P)
	DELETE	H
	FROM	TB_H_REQUEST_APPR H
	INNER	JOIN
			TB_R_REQUEST_APPR A
	ON		A.DOC_NO	= H.DOC_NO AND
			A.INDX		= H.INDX
	INNER	JOIN
			@TB_T_EMP E
	ON		H.EMP_CODE		= E.SYS_EMP_CODE
	WHERE	A.APPR_STATUS	IN ('W','P')
	------------------------------------------------------------------------------------------------------------------
	-- Delete Resign Employee from approver list (W, P)
	UPDATE	A
	SET		ACTUAL_ROLE		= '',
			EMP_CODE		= NULL,
			EMP_TITLE		= NULL,
			EMP_NAME		= NULL,
			EMP_LASTNAME	= NULL,
			EMAIL			= NULL,
			POS_CODE		= NULL,
			POS_NAME		= NULL,
			DEPT_CODE		= NULL,
			DEPT_NAME		= NULL,
			ORG_CODE		= NULL
	FROM	TB_R_REQUEST_APPR A
	INNER	JOIN
			@TB_T_EMP E
	ON		A.EMP_CODE		= E.SYS_EMP_CODE
	WHERE	A.APPR_STATUS	IN ('W','P')
	


	-- Send Notification to requestor
	IF NOT EXISTS(SELECT 1 FROM @TB_T_REQUEST WHERE APPR_STATUS IS NOT NULL)
	BEGIN
		-- No Email Send
		RETURN
	END


	-----------------------------------------------------------------------------------------------------------------------------------------------
	-- Prepare Email to requestor / If requestor resign send to AEC
	
	UPDATE	T
	SET		EMAIL_TO	= E.EMAIL, 
			EMAIL_TO_CC	= dbo.fn_GetEmailListofAdministrator(), 
			SEND_TO		= E.SYS_EMP_CODE,
			SEND_NAME   = E.EMP_NAME  
	FROM	@TB_T_REQUEST T
	INNER	JOIN
			TB_R_REQUEST_H H
	ON		T.DOC_NO	= H.DOC_NO 
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		H.EMP_CODE	= E.SYS_EMP_CODE
	WHERE	E.ACTIVEFLAG = 'Y'

	UPDATE	@TB_T_REQUEST
	SET		EMAIL_TO = dbo.fn_GetEmailListofAdministrator(),
			SEND_TO		= 'AEC',
			SEND_NAME	= 'AEC'
	WHERE	EMAIL_TO IS NULL


	DECLARE @TB_T_PREPARE_EMAIL TABLE
	(	
		SEND_TO		T_USER,
		EMAIL_TO	VARCHAR(MAX),
		EMAIL_TO_CC	VARCHAR(MAX),
		[SUBJECT]	VARCHAR(255),
		[BODY]		VARCHAR(MAX),
		URL_LIST	VARCHAR(MAX)
	)

	DECLARE @Subject	VARCHAR(MAX) = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD01210')
	DECLARE @Body		VARCHAR(MAX) = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01210')
	DECLARE @Url		VARCHAR(MAX) = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01210_URL')
	DECLARE @Header	  VARCHAR(100)  = dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01210_HEADER')
	DECLARE @Footer		VARCHAR(100) =  dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD01210_FOOTER')





	INSERT
	INTO	@TB_T_PREPARE_EMAIL (SEND_TO, EMAIL_TO,EMAIL_TO_CC, [SUBJECT], [BODY], [URL_LIST])

	SELECT	SEND_TO, EMAIL_TO,EMAIL_TO_CC, @Subject, @Body, NULL
	FROM
	(
		SELECT	*, ROW_NUMBER() OVER(PARTITION BY SEND_TO ORDER BY DOC_NO) ROW_INDX
		FROM	@TB_T_REQUEST
	)	T
	WHERE	T.ROW_INDX	= 1
			
	--SELECT DISTINCT	MN.SEND_TO, 
 --           STUFF((    SELECT '  <BR/>' + dbo.fn_StringFormat(@Url,REQUEST_TYPE+'|'+SUB.DOC_NO) AS [text()]                       
 --                       FROM @TB_T_REQUEST SUB
	--					WHERE	SUB.SEND_TO = MN.SEND_TO
 --                       FOR XML PATH('') 
 --                       ), 1, 1, '' )
 --           AS URL_LIST
	--FROM  @TB_T_REQUEST MN



	INSERT  INTO TB_R_NOTIFICATION
	SELECT	NEXT VALUE FOR NOTIFICATION_ID OVER(ORDER BY T.SEND_TO),
				T.EMAIL_TO	AS [TO],
				T.EMAIL_TO_CC	AS CC,
				NULL		AS BCC,
				T.[SUBJECT],
				CONCAT(dbo.fn_StringFormat(@Header,R.SEND_NAME) , R.URL_LIST , @Footer) AS [MESSAGE],
				--dbo.fn_StringFormat(@Body,'Requestor|'+R.URL_LIST)  AS [MESSAGE],
				NULL	AS FOOTER, -- Get From Default
				'I'		AS [TYPE],
				NULL	AS [START_PERIOD],
				NULL	AS [END_PERIOD],
				'N'		AS [SEND_FLAG],
				NULL	AS RESULT_FLAG,
				'BFD01210'	AS REF_FUNC,
				NULL	AS REF_DOC_NO,
				GETDATE(),
				@UserID,
				GETDATE(),
				@UserID
	FROM	@TB_T_PREPARE_EMAIL T
	INNER JOIN
	(	SELECT DISTINCT	MN.SEND_TO, MN.SEND_NAME, 
            STUFF((    SELECT '  <BR/>' + dbo.fn_StringFormat(@Url,REQUEST_TYPE+'|'+SUB.DOC_NO) AS [text()]                       
                        FROM @TB_T_REQUEST SUB
						WHERE	SUB.SEND_TO = MN.SEND_TO
                        FOR XML PATH('') 
                        ), 1, 1, '' )
            AS URL_LIST
	FROM  @TB_T_REQUEST MN) R
	on T.SEND_TO = R.SEND_TO
	PRINT 'OK'

END
GO
