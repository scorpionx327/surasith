DROP PROCEDURE [dbo].[sp_WFD02130_GetPrintLocation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS ===== ======================================
-- Author:     Phithak Khonnoy
-- Create date:  2017/03/18 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/

CREATE PROCEDURE [dbo].[sp_WFD02130_GetPrintLocation]
(									
	@EMP_CODE	T_SYS_USER
 )
AS
BEGIN TRY

	    SELECT Emp.PRINT_LOCATION
	    FROM  TB_M_EMPLOYEE Emp
		WHERE Emp.SYS_EMP_CODE	= @EMP_CODE


END TRY
BEGIN CATCH
			    PRINT concat('ERROR Message:',ERROR_MESSAGE())
				RETURN 1;
END CATCH





GO
