DROP PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList_MGR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- select * from TB_M_SYSTEM where CATEGORY like 'SYSTEM_CONFIG' and SUB_CATEGORY LIKE 'STM_APPROVE_INDIRECT%'
CREATE PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList_MGR]
@Indx		INT,
@Company	T_COMPANY,
@Role		VARCHAR(3),
@Division	VARCHAR(6),
@MainFlag	VARCHAR(1),
@GUID		T_GUID = NULL,
@RowCount	INT OUT
AS
BEGIN
	SET @RowCount = 0
	-- EXEC sp_WFD01170_GeneratePersonList_MGR 1, 'STM','MGR','7d83531c-ca32-43f1-98c1-9b56425102ad'

	DECLARE @Level AS VARCHAR(4)
	IF @MainFlag = 'M'
		SET @Level = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_DIRECT'),@Role)
	ELSE
		SET @Level = dbo.fn_GetSystemMaster('SYSTEM_CONFIG',CONCAT(@Company,'_APPROVE_INDIRECT'),@Role)

	PRINT @Level
	
	INSERT
	INTO	TB_H_REQUEST_APPR(
			DOC_NO,
			INDX,
			[ROLE],
			ACTUAL_ROLE,
			EMP_CODE,
			EMP_TITLE,
			EMP_NAME,
			EMP_LASTNAME,
			EMAIL,
			MAIN,
			ORG_CODE,
			[GUID]
	)
	SELECT	'',
			@Indx,
			@Role,
			E.[ROLE],
			E.SYS_EMP_CODE,
			E.EMP_TITLE,
			E.EMP_NAME,
			E.EMP_LASTNAME,
			E.EMAIL,
			@MainFlag,
			E.ORG_CODE,
			@GUID
	FROM	TB_M_EMPLOYEE E
	INNER	JOIN
			TB_M_ORGANIZATION D
	ON		E.ORG_CODE		= D.ORG_CODE
	LEFT	JOIN
			TB_H_REQUEST_APPR H
	ON		H.INDX			= @Indx AND
			H.EMP_CODE		= E.SYS_EMP_CODE AND
			H.[GUID]		= @GUID
	WHERE	H.EMP_CODE IS NULL AND -- Should not duplicate 
			E.[ROLE]		= @Role AND
			E.ACTIVEFLAG	= 'Y' AND
			EXISTS(	SELECT	1		
					FROM	TB_T_COST_CENTER_ORG_MAPPING T
					WHERE	[GUID]	= @GUID AND
							CASE	WHEN @Level = 'COM'  THEN T.CMP_CODE 
									WHEN @Level = 'DIV'  THEN T.DIV_CODE 
									WHEN @Level = 'SDIV' THEN T.SUB_DIV_CODE 
									WHEN @Level = 'DEPT' THEN T.DEPT_CODE 
									WHEN @Level = 'SECT' THEN T.SECTION_CODE 
									WHEN @Level = 'LINE' THEN T.LINE_CODE 
									ELSE '' 
							END = CASE	WHEN @Level = 'COM'  THEN D.CMP_CODE 
										WHEN @Level = 'DIV'  THEN D.DIV_CODE 
										WHEN @Level = 'SDIV' THEN D.SUB_DIV_CODE 
										WHEN @Level = 'DEPT' THEN D.DEPT_CODE 
										WHEN @Level = 'SECT' THEN D.SECTION_CODE 
										WHEN @Level = 'LINE' THEN D.LINE_CODE 
										ELSE '' 
									END
			)
	SET @RowCount = @@ROWCOUNT
END
GO
