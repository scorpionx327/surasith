DROP PROCEDURE [dbo].[sp_WFD02620_UpdateLockStatusOnScreen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_WFD02620_UpdateLockStatusOnScreen]
		@COMPANY	T_COMPANY,
		@YEAR	varchar(4),
		@ROUND	varchar(2),
		@ASSET_LOCATION	varchar(1)	
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM TB_R_STOCK_TAKE_H 
		WHERE	[COMPANY] = @COMPANY AND 
				[YEAR] = @YEAR AND 
				[ROUND] = @ROUND AND 
				ASSET_LOCATION = @ASSET_LOCATION AND
				PLAN_STATUS IN ('S','C') )
	BEGIN
		RETURN ;
	END
	


	UPDATE	SV
	SET		SV.IS_LOCK = 'Y',
			LOCKED = 'Y'
	FROM	TB_R_STOCK_TAKE_D_PER_SV SV
	WHERE	
			[COMPANY] = @COMPANY AND 
			[YEAR] = @YEAR AND 
			[ROUND] = @ROUND AND 
			ASSET_LOCATION = @ASSET_LOCATION AND
			ISNULL(LOCKED,'N') = 'N' AND IS_LOCK = 'N' AND
			GETDATE() > CASE WHEN DATE_TO IS NOT NULL THEN CONVERT(DATETIME, CONCAT(DATE_TO, ' ', TIME_END)) ELSE NULL END AND
			DATE_TO IS NOT NULL
END
GO
