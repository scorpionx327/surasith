DROP PROCEDURE [dbo].[sp_Common_InsertBatchQParam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_InsertBatchQParam]
	-- Add the parameters for the stored procedure here
	@P_APP_ID numeric(18,0)
	,@P_PARAM_SEQ int
	,@P_PARAM_NAME nvarchar(20)
	,@P_PARAM_VALUE nvarchar(255)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


INSERT INTO [dbo].[TB_BATCH_Q_PARAM]
           ([APP_ID]
           ,[PARAM_SEQ]
           ,[PARAM_NAME]
           ,[PARAM_VALUE])
     VALUES
           (@P_APP_ID
           ,@P_PARAM_SEQ
           ,@P_PARAM_NAME
           ,@P_PARAM_VALUE)


END







GO
