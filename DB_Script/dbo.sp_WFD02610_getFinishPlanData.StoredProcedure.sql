DROP PROCEDURE [dbo].[sp_WFD02610_getFinishPlanData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-01-31
-- Description:	Finish stock take request plan
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02610_getFinishPlanData]
	-- Add the parameters for the stored procedure here
	@YEAR				varchar(4)
	, @ROUND			varchar(2)
	, @ASSET_LOCATION	varchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		top 1
		PLAN_STATUS
		, (select count(1) from TB_R_STOCK_TAKE_D d where d.YEAR=h.YEAR and d.ROUND=h.ROUND and d.ASSET_LOCATION=h.ASSET_LOCATION and d.CHECK_STATUS='N') as UNCHECK_CNT
	FROM TB_R_STOCK_TAKE_H h
	WHERE YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION
END




GO
