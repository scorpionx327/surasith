DROP PROCEDURE [dbo].[sp_WFD01150_GetTmpDivH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_GetTmpDivH]
(
	@APPRV_ID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MyTableVar table(  
		APPRV_ID int,
		SEQ varchar(3),
		[ROLE] varchar(3),
		DIVISION varchar(6),
		GRP varchar(8),
		CONDITION varchar(400),
		REQ varchar(3),
		REJ varchar(3),

		OPERATOR Varchar(400),																																																								
		VALUE1 int,			
		VALUE2 int,				
		ALLOWTOREJECT varchar(3),																																																										
		ALLOWTODELETE varchar(3),
		LEADTIME int,
		ALLOWTOSELECT varchar(3),
		FINISHFLOW varchar(3),
		GENFILE varchar(3),
		REJECT_TO  varchar(3),
		HIGHERAPPROVE  varchar(3),
		NOTIFICATIONBYEMAIL   varchar(3),
		OPERATION   varchar(3),
		Isinsert varchar(1)
	); 
	DECLARE @ROLE_01 varchar(3),@ROLE_02 varchar(3),@ROLE_20 varchar(3),@ROLE_30 varchar(3), @ROLE_40 varchar(3),@ROLE_41 varchar(3),@ROLE_50 varchar(3),@ROLE_70 varchar(3),@ROLE_71 varchar(3)
	SELECT @ROLE_01=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =1

	SELECT @ROLE_02=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =2

	SELECT @ROLE_20=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =20

	SELECT @ROLE_30=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =30

	SELECT @ROLE_40=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =40

	SELECT @ROLE_41=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =41

	SELECT @ROLE_50=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =50

	SELECT @ROLE_70=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =70

	SELECT @ROLE_71=[ROLE]
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =71

	IF(@ROLE_01 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'01',@ROLE_01,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'01','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
	
	IF(@ROLE_02 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'02',@ROLE_02,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'02','','','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END

	IF(@ROLE_20 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'20',@ROLE_20,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END	
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'20','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
	
	IF(@ROLE_30 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'30',@ROLE_30,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'30','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
	
	IF(@ROLE_40 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'40',@ROLE_40,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'40','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
		
	IF(@ROLE_41 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'41',@ROLE_41,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'41','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
		
	IF(@ROLE_50 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'50',@ROLE_50,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'50','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
	
	IF(@ROLE_70 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'70',@ROLE_70,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'70','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END
	
	IF(@ROLE_71 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'71',@ROLE_71,'','','','','','',0,0,'','',0,'','','','','',0,0,'U'
		END
	ELSE
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'71','','','','','','','',0,0,'','',0,'','','','','',0,0,'N'
		END

	select * from @MyTableVar
	--DECLARE @01 varchar(3);
	--DECLARE @02 varchar(3);
	--DECLARE @20 varchar(3);
	--DECLARE @30 varchar(3);
	--DECLARE @40 varchar(3);
	--DECLARE @41 varchar(3);
	--DECLARE @50 varchar(3);
	--DECLARE @70 varchar(3);
	--DECLARE @71 varchar(3);
	--SET @01 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =1)
	--SET @02 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =2)
	--SET @20 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =20)
	--SET @30 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =30)
	--SET @40 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =40)
	--SET @41 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =41)
	--SET @50 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =50)
	--SET @70 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =70)
	--SET @71 = (select [ROLE] from TB_M_APPROVE_D where APPRV_ID = @APPRV_ID and INDX =71)

	--IF(@01 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'01','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'01','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@02 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'02','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'02','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@20 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'20','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'20','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@30 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'30','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'30','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@40 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'40','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'40','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@41 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'41','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'41','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@50 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'50','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'50','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@70 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'70','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'70','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--IF(@71 is not null)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'71','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END
	
	--IF(@APPRV_ID = 0)
	--BEGIN
	--	INSERT INTO @MyTableVar select 	'71','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--END

	--INSERT INTO @MyTableVar select 	'01','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'02','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'20','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'30','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'40','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'41','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'50','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'70','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
	--INSERT INTO @MyTableVar select 	'71','','','','','','','',0,0,'','',0,'','','','',0,0,'N'
END

GO
