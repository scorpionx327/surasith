DROP PROCEDURE [dbo].[sp_Common_UpdateProcessIDInBatchQ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Common_UpdateProcessIDInBatchQ]
(
@ReqID			INT,
@PID		 varchar(20)
)
AS
BEGIN
	UPDATE	TB_BATCH_Q
	SET		PID = @PID,
			UPDATE_DATE =  GETDATE()
	WHERE	APP_ID	 = @ReqID
	
	IF(@@ROWCOUNT = 0)
	BEGIN
		DECLARE @msg VARCHAR(100)
		SET @msg = 'Not found Request ID <'+CONVERT(VARCHAR,@ReqID)+'> in TB_BATCH_Q'
		RAISERROR(@msg,16,1)
	END
END






GO
