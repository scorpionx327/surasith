DROP PROCEDURE [dbo].[sp_WFD02810_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02810_ApproveRequest]
(	
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN

	DELETE	C
	FROM	TB_R_REQUEST_ASSET_CHG C
	INNER	JOIN
			TB_R_REQUEST_ASSET_D D
	ON		C.DOC_NO	= D.DOC_NO AND
			C.LINE_NO	= D.LINE_NO
	WHERE	D.DELETE_FLAG	= 'Y' AND
			D.DOC_NO		= @DOC_NO

	
	EXEC sp_WFD021A0_ApproveRequest @DOC_NO, @USER

	IF dbo.fn_IsReadyGenFile(@DOC_NO) = 'N'
	RETURN

	UPDATE	TB_R_REQUEST_ASSET_CHG
	SET		[STATUS]	= 'GEN', -- Same As TB_R_REQUEST_ASSET_D
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= @USER
	WHERE	DOC_NO		= @DOC_NO AND
			[STATUS]	= 'NEW'
	
	-------------------------------------------------------------------------------------
	-- Approve by AEC
	UPDATE	H
	SET		H.PROCESS_STATUS	= 'GC',
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_IMPAIRMENT M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO			= @DOC_NO AND
			H.PROCESS_STATUS	= 'M'

	DECLARE @LatestApproverCode	T_SYS_USER, @LatestApproverName	VARCHAR(68)
	DECLARE @AECCode		T_SYS_USER
	UPDATE	H
	SET		H.DETAIL		= 'Asset Info change.',
			H.TRANS_DATE	= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= @LatestApproverName, 
			H.AEC_APPR_BY			= @AECCode,
			H.AEC_APPR_DATE			= GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_ASSET_CHG M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND 
			M.DOC_NO	= H.DOC_NO AND 
			M.ASSET_NO	= H.ASSET_NO AND 
			M.ASSET_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO
	
END
GO
