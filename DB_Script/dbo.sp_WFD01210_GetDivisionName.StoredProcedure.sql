DROP PROCEDURE [dbo].[sp_WFD01210_GetDivisionName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun
-- Create date: 05/03/2017
-- Description:	Get All orgrnization order by
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetDivisionName]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	DIV_NAME
	FROM	TB_M_ORGANIZATION
	WHERE	DIV_NAME IS NOT NULL
	GROUP	BY
			DIV_NAME
	ORDER	BY
			DIV_NAME
END




GO
