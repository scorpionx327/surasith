DROP PROCEDURE [dbo].[sp_WFD02160_ViewMapPoint]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====WFD02160 : Fixed Asset Map Location ==================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/11 (yyyy/mm/dd) exec sp_WFD02160_ViewMapPoint 'E03DM102','N'
*/
CREATE PROCEDURE [dbo].[sp_WFD02160_ViewMapPoint]
( 
	@COMPANY	T_COMPANY,
	@COST_CODE	T_COST_CODE,
	@FAADMIN	VARCHAR(1) = 'N'
 )
AS
BEGIN TRY
	DECLARE @CAPITALIZED VARCHAR(40) 
	SET @CAPITALIZED = dbo.fn_GetSystemMaster('FAS_TYPE','ASSET_TYPE','CAPITALIZED')


    UPDATE	L
	SET		SEQ = SEQ_NEW
	FROM	TB_M_ASSETS_LOC L
	INNER	JOIN
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY SEQ) SEQ_NEW,
				ASSET_NO,
				COST_CODE,
				COMPANY
		FROM	TB_M_ASSETS_LOC 
		WHERE	COST_CODE	= @COST_CODE AND
				COMPANY		= @COMPANY
	) T 
	ON	L.COMPANY		= T.COMPANY AND
		L.ASSET_NO		= T.ASSET_NO AND
		L.COST_CODE		= T.COST_CODE

	SELECT		H.ITEM_SEQ ITEMS,
				FORMAT(H.ITEM_SEQ,'00') Display,
				H.COMPANY,
				H.ASSET_NO,
				H.ASSET_NAME,
				H.ASSET_HINT,
				
											--Pitiphat CR-B-011 20170908
				CASE WHEN	L.COST_CODE = @COST_CODE AND 
							(L.X IS NOT NULL) AND
							(L.Y IS NOT NULL)  THEN 'Y'
				ELSE 'N' END AS SELECTED,
				CASE WHEN L.COST_CODE = @COST_CODE THEN L.X ELSE NULL END AS X_POINT,
				CASE WHEN L.COST_CODE = @COST_CODE THEN L.Y ELSE NULL END AS Y_POINT,
				CASE WHEN L.COST_CODE = @COST_CODE THEN L.DEGREE ELSE NULL END AS DEGREE,	--Pitiphat CR-B-011 20170908
				L.COST_CODE,
				H.MAP_STATUS,
				L.SEQ,
				H.MINOR_CATEGORY,
				H.ASSET_MAP_HINT,
				CASE	WHEN ISNULL(H.MACHINE_LICENSE,'') <> '' AND H.BOI_FLAG = 'Y' THEN 'red' 
						WHEN H.BOI_FLAG = 'Y' THEN 'black'
						WHEN ISNULL(H.MACHINE_LICENSE,'') <> '' THEN 'yellow'
						ELSE '' END BG_COLOR
		FROM (	SELECT		H.COMPANY,
							H.COST_CODE,
							H.ASSET_NO,
							H.MAP_STATUS, 
							H.BOI_FLAG,
							H.ASSET_NAME,
							CASE	WHEN H.INVEN_NO IS NULL			THEN CONCAT(H.ASSET_NAME,':',H.LOCATION_REMARK)
									WHEN H.INVEN_NO <> H.ASSET_NO	THEN CONCAT(H.ASSET_NAME,':',H.LOCATION_REMARK,':',H.INVEN_NO)
									ELSE CONCAT(H.ASSET_NAME,':',H.LOCATION_REMARK) END AS ASSET_HINT,

							CASE	WHEN H.INVEN_NO IS NULL			THEN H.ASSET_NO 
									WHEN H.INVEN_NO <> H.ASSET_NO	THEN CONCAT(H.ASSET_NO,':',H.ASSET_NAME,':',H.INVEN_NO)
									ELSE CONCAT(H.ASSET_NO,':',H.ASSET_NAME) END AS ASSET_MAP_HINT,
							H.MINOR_CATEGORY,
							H.MACHINE_LICENSE,	--Pitiphat CR-B-011 20170908
							H.ITEM_SEQ
				FROM		TB_M_ASSETS_H H
				WHERE		H.COMPANY		= @COMPANY AND
							H.[STATUS]		= 'Y' AND -- Active Only
							H.ASSET_GROUP	= 'RMA' AND
							H.ASSET_SUB		= '0000' AND
							H.INHOUSE_FLAG	= 'Y' AND -- Map Location no include FA Assets
							( H.COST_CODE	= @COST_CODE  OR 	EXISTS (SELECT	1 
																	FROM 	TB_M_ASSETS_LOC L
																	WHERE	L.ASSET_NO	= H.ASSET_NO AND 
																			L.COMPANY	= H.COMPANY AND
																			L.COST_CODE	= @COST_CODE ) 
							)
							-- Remain condition
															
		) H 
	   INNER	JOIN 
				TB_M_ASSETS_LOC L 
		ON		L.ASSET_NO	= H.ASSET_NO AND 
				L.COMPANY	= H.COMPANY
		WHERE	L.COMPANY = @COMPANY AND ( L.COST_CODE = @COST_CODE OR L.COST_CODE IS NULL )
		ORDER BY ITEMS

END TRY
BEGIN CATCH
	
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH
GO
