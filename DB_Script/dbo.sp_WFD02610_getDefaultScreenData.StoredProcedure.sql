DROP PROCEDURE [dbo].[sp_WFD02610_getDefaultScreenData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-01
-- Description:	Get default screen data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02610_getDefaultScreenData]
	-- Add the parameters for the stored procedure here
	@COMPANY T_COMPANY = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
		top 1
		 H.COMPANY
		, YEAR
		, ROUND
		, H.ASSET_LOCATION
		, 1 as CURRENCT_PAGE
		, 15 as PAGE_SIZE
		, '' as ORDER_BY
	from TB_R_STOCK_TAKE_H H
	left join TB_M_SYSTEM S on S.CATEGORY = 'COMMON'
			      and s.SUB_CATEGORY = 'ASSET_LOCATION' and s.CODE = H.PLAN_STATUS
	WHERE H.COMPANY = @COMPANY
	ORDER BY S.REMARKS ASC, H.CREATE_DATE DESC

END


GO
