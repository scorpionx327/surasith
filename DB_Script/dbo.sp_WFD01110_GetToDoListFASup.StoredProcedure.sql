DROP PROCEDURE [dbo].[sp_WFD01110_GetToDoListFASup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong				Update by: Surasith T.
-- Create date: 17/02/2017					Update date: 22/09/2019
-- Description:	Get count to do list
 --EXEC sp_WFD01110_GetToDoList '0001'
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetToDoListFASup]
(
	@EMP_CODE	T_SYS_USER
)
AS
BEGIN
	
	IF NOT EXISTS(SELECT 1 FROM TB_M_FASUP_COST_CENTER WHERE EMP_CODE = @EMP_CODE)
	BEGIN
		SELECT	0	COUNT_OF_UPLOAD,
				0	COUNT_OF_PRINT_TAG,
				0	COUNT_OF_MARK_LOCATION
		RETURN
	END

	DECLARE @COUNT_OF_UPLOAD        INT;
	DECLARE @COUNT_OF_PRINT_TAG     INT;
	DECLARE @COUNT_OF_MARK_LOCATION  INT;

	DECLARE @COMPANY T_COMPANY, @COND	VARCHAR(2)

	SELECT		TOP 1 @COMPANY = COMPANY 
	FROM		TB_M_FASUP_COST_CENTER
	WHERE		EMP_CODE	= @EMP_CODE

	
	
	SET @COND	= dbo.fn_GetSystemMaster('TODO_LIST_CONFIG',CONCAT('PRINT_TAG_COND_',@COMPANY),'COND')
	IF	@COND IS NULL SET @COND = 'AM'

	------------ Print Tag
	SELECT	@COUNT_OF_PRINT_TAG = COUNT(1)
	FROM	TB_M_ASSETS_H M
	WHERE	M.COMPANY		= @COMPANY	AND
			M.[STATUS]		= 'Y'		AND -- Active Assets
			M.ASSET_SUB		= '0000'	AND	-- Parent Assets
			M.PRINT_STATUS	= 'N'		AND	-- No print
			(
				( @COND != 'AM'	AND M.INVEN_INDICATOR = 'Y') OR -- Check Inventory Indicator
				( @COND = 'AM'	AND EXISTS(	SELECT	1	-- Check Asset class / Minor Category
						FROM	TB_M_SYSTEM MS
						WHERE	MS.CATEGORY		= 'TODO_LIST_CONFIG' AND
								MS.SUB_CATEGORY	= CONCAT('PRINT_TAG_',@COMPANY) AND
								MS.[VALUE]		= CONCAT(M.ASSET_CLASS,'|',M.MINOR_CATEGORY) AND
								MS.ACTIVE_FLAG	= 'Y'
					)) 
			) AND	
			
			EXISTS(	SELECT	1 -- Check Permissioin
							FROM	TB_M_FASUP_COST_CENTER T
							WHERE	T.EMP_CODE	= @EMP_CODE AND
									T.COMPANY	= M.COMPANY AND 
									T.COST_CODE	= M.RESP_COST_CODE ) 
	
	---------------- Map
	SET @COND	= dbo.fn_GetSystemMaster('TODO_LIST_CONFIG',CONCAT('MAP_LOCATION_COND_',@COMPANY),'COND')
	IF	@COND IS NULL SET @COND = 'AM'

	SELECT	@COUNT_OF_MARK_LOCATION = COUNT(1)
	FROM	TB_M_ASSETS_H M
	WHERE	M.COMPANY		= @COMPANY	AND
			M.[STATUS]		= 'Y'		AND -- Active Assets
			M.ASSET_SUB		= '0000'	AND	-- Parent Assets
			M.MAP_STATUS	= 'N'		AND	-- No print
			(
				( @COND != 'AM'	AND M.INVEN_INDICATOR = 'Y') OR -- Check Inventory Indicator
				( @COND = 'AM'	AND EXISTS(	SELECT	1	-- Check Asset class / Minor Category
						FROM	TB_M_SYSTEM MS
						WHERE	MS.CATEGORY		= 'TODO_LIST_CONFIG' AND
								MS.SUB_CATEGORY	= CONCAT('MAP_LOCATION_',@COMPANY) AND
								MS.[VALUE]		= CONCAT(M.ASSET_CLASS,'|',M.MINOR_CATEGORY) AND
								MS.ACTIVE_FLAG	= 'Y'
					)) 
			) AND	
			EXISTS(	SELECT	1 -- Check Permissioin
							FROM	TB_M_FASUP_COST_CENTER T
							WHERE	T.EMP_CODE	= @EMP_CODE AND
									T.COMPANY	= M.COMPANY AND 
									T.COST_CODE	= M.RESP_COST_CODE ) 

	SET @COND	= dbo.fn_GetSystemMaster('TODO_LIST_CONFIG',CONCAT('PHOTO_UPLOAD_COND_',@COMPANY),'COND')
	IF	@COND IS NULL SET @COND = 'AM'

	--Upload Photo
	SELECT	@COUNT_OF_UPLOAD = COUNT(1)
	FROM	TB_M_ASSETS_H M
	WHERE	M.COMPANY		= @COMPANY	AND
			M.[STATUS]		= 'Y'		AND -- Active Assets
			M.ASSET_SUB		= '0000'	AND	-- Parent Assets
			M.PHOTO_STATUS	= 'N'		AND	-- No print
			(
				( @COND != 'AM'	AND M.INVEN_INDICATOR = 'Y') OR -- Check Inventory Indicator
				( @COND = 'AM'	AND EXISTS(	SELECT	1	-- Check Asset class / Minor Category
						FROM	TB_M_SYSTEM MS
						WHERE	MS.CATEGORY		= 'TODO_LIST_CONFIG' AND
								MS.SUB_CATEGORY	= CONCAT('PHOTO_UPLOAD_',@COMPANY) AND
								MS.[VALUE]		= CONCAT(M.ASSET_CLASS,'|',M.MINOR_CATEGORY) AND
								MS.ACTIVE_FLAG	= 'Y'
					)) 
			) AND
			EXISTS(	SELECT	1 -- Check Permissioin
							FROM	TB_M_FASUP_COST_CENTER T
							WHERE	T.EMP_CODE	= @EMP_CODE AND
									T.COMPANY	= M.COMPANY AND 
									T.COST_CODE	= M.RESP_COST_CODE ) 

	SELECT 
		ISNULL(@COUNT_OF_UPLOAD,0) COUNT_OF_UPLOAD,
		ISNULL(@COUNT_OF_PRINT_TAG,0) COUNT_OF_PRINT_TAG,
		ISNULL(@COUNT_OF_MARK_LOCATION,0) COUNT_OF_MARK_LOCATION
	
END
GO
