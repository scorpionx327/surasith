DROP PROCEDURE [dbo].[sp_Common_InsertDetailLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Update By:Uten Sopradid 
Description : Add new column(DISPLAY_FLAG) 
 --> 'Y' is mean display on log monitoring screen ,
'N' or NULL is mean no display on log monitoring screen
*/
CREATE PROCEDURE  [dbo].[sp_Common_InsertDetailLog]
@APP_ID				numeric(18,0),
@STATUS				VARCHAR(1),
@FAVORITE_FLAG		varchar(1),
@LEVEL				varchar(1),
@MESSAGE			VARCHAR(4000) = NULL,
@User				VARCHAR(8)
AS
BEGIN
	 IF(@STATUS='Q')
	 BEGIN
		 SET @STATUS='P'
	 END

	INSERT	INTO	TB_L_LOGGING(APP_ID, STATUS, FAVORITE_FLAG, LEVEL, MESSAGE,DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
	VALUES(@APP_ID, @STATUS, @FAVORITE_FLAG,@LEVEL,@MESSAGE,'Y', @User, GETDATE())
END





GO
