DROP PROCEDURE [dbo].[sp_WFD01170_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01170_ApproveRequest]
@DOC_NO				T_DOC_NO,
@LOGIN_USER			T_SYS_USER,
@IS_DELEGATE		VARCHAR(1), -- Y => Delegate User take action
@IS_HIGHER			VARCHAR(1), -- Y => Manager take action instead
@IS_INSTEAD			VARCHAR(1), -- Y => ACR User take action instead approve user		
-- Comment
@COMMENT			VARCHAR(200)		= NULL,			
@ATTACH_FILE		VARCHAR(200)		= NULL
AS
BEGIN
	-- Get current data
	DECLARE	@WaitingIndx	INT, 
			@CurrentApprover T_SYS_USER,
			@CurrentRole	VARCHAR(3),
			@RequestType	VARCHAR(1), 
			@OpMode			VARCHAR(1)
	
	SELECT	@RequestType = REQUEST_TYPE
	FROM	TB_R_REQUEST_H 
	WHERE	DOC_NO = @DOC_NO

	SELECT	@WaitingIndx		= INDX, 
			@OpMode				= [OPERATION_MODE],
			@CurrentApprover	= EMP_CODE,
			@CurrentRole		= APPR_ROLE
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO				= @DOC_NO AND
			[APPR_STATUS]		= 'W'

	IF @@ROWCOUNT = 0
	RETURN

	SET @COMMENT = ISNULL(@COMMENT,'Approved')
	
	EXEC sp_WFD01170_InsertComment @DOC_NO, @WaitingIndx, @LOGIN_USER, @COMMENT, @ATTACH_FILE, @LOGIN_USER, @RequestType, 'A'


	UPDATE	TB_R_REQUEST_APPR -- Should be update Actual Role
	SET		APPR_STATUS		= 'A',
			APPR_DATE		= GETDATE(), --Alway set Appr Date because  report need condition to display
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @LOGIN_USER
	WHERE	DOC_NO			= @DOC_NO AND
			INDX			= @WaitingIndx

	IF @OpMode = 'G' AND @LOGIN_USER != ISNULL(@CurrentApprover,'')
	BEGIN
		UPDATE	A
		SET		A.ACTUAL_ROLE	= E.[ROLE],
				A.EMP_CODE		= E.SYS_EMP_CODE,
				A.EMP_TITLE		= E.EMP_TITLE,
				A.EMP_NAME		= E.EMP_NAME,
				A.EMP_LASTNAME	= E.EMP_LASTNAME,
				A.EMAIL			= E.EMAIL,
				A.POS_CODE		= E.POST_CODE,
				A.POS_NAME		= E.POST_NAME,
				A.DEPT_CODE		= D.DEPT_CODE,
				A.DEPT_NAME		= D.DEPT_NAME,
				A.DIV_NAME		= D.DIV_NAME,
				A.ORG_CODE		= E.ORG_CODE
		FROM	TB_R_REQUEST_APPR A
		INNER	JOIN
				TB_M_EMPLOYEE E
		ON		E.SYS_EMP_CODE	= @LOGIN_USER
		INNER	JOIN
				TB_M_ORGANIZATION D
		ON		E.ORG_CODE		= D.ORG_CODE
		WHERE	DOC_NO			= @DOC_NO AND
				INDX			= @WaitingIndx
	END
	-- Reget
	SELECT	@CurrentApprover	= EMP_CODE
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND
			INDX			= @WaitingIndx

	
	-----------------------------------------------------------------------------------
	-- Update Document Status
	DECLARE @iCurrent		INT,
			@iGenFile		INT, -- Index for change status
			@iFinishFlow	INT	 -- Index for change status
	
	SELECT	@iGenFile	= MAX(INDX) -- should 1 record
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO		= @DOC_NO AND 
			GEN_FILE	= 'Y'

	SELECT	@iFinishFlow	= MAX(INDX) -- should 1 record
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND 
			FINISH_FLOW		= 'Y'
	
	SELECT	@iCurrent	= MAX(INDX)
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO		= @DOC_NO AND 
			APPR_STATUS = 'A' -- Approved
	
	UPDATE	TB_R_REQUEST_H
	SET		[STATUS]	= CASE	WHEN [STATUS] <= '20' AND @iCurrent >= @iGenFile THEN '30'
								WHEN [STATUS] <= '10' AND @iCurrent >= @iFinishFlow THEN '20'
								ELSE [STATUS] END, 
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= @LOGIN_USER
	WHERE	DOC_NO		= @DOC_NO
	----------------------------------------------------------------------------------- 
	-- Get Next Approver
	DECLARE @NextIndx			INT, 
			@NextApproverRole	VARCHAR(3),
			@NextApproverCode	T_SYS_USER
	IF NOT EXISTS(SELECT 1 FROM TB_R_REQUEST_APPR 
		WHERE DOC_NO	= @DOC_NO AND APPR_STATUS = 'P')
	BEGIN
		-- Complete All
		RETURN
	END
		
	PRINT 'Send Next approve'
	SELECT	TOP 1 @NextIndx	= INDX, 
			@NextApproverRole = APPR_ROLE, 
			@NextApproverCode = EMP_CODE
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO		= @DOC_NO AND APPR_STATUS = 'P'
	ORDER	BY
			INDX
	
	-- Incase AEC approve -> Find AEC Manager
	DECLARE @AECUser VARCHAR(3), @AECManager VARCHAR(3)
	select * from TB_M_SYSTEM where value = 'ACR'
	SET @AECUser = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','ACR_CODE','ACR')
	SET @AECManager = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','ACR_CODE','ACM')

	IF (@NextApproverRole = @AECManager AND @CurrentRole = @AECUser) AND @NextApproverCode IS NULL
	BEGIN
		-- Find
		SET @NextApproverCode = dbo.fn_GetAECManager(@CurrentApprover) ;

		UPDATE	A
		SET		A.ACTUAL_ROLE	= E.[ROLE],
				A.EMP_CODE		= E.SYS_EMP_CODE,
				A.EMP_TITLE		= E.EMP_TITLE,
				A.EMP_NAME		= E.EMP_NAME,
				A.EMP_LASTNAME	= E.EMP_LASTNAME,
				A.EMAIL			= E.EMAIL,
				A.POS_CODE		= E.POST_CODE,
				A.POS_NAME		= E.POST_NAME,
				A.DEPT_CODE		= D.DEPT_CODE,
				A.DEPT_NAME		= D.DEPT_NAME,
				A.DIV_NAME		= D.DIV_NAME,
				A.ORG_CODE		= E.ORG_CODE
		FROM	TB_R_REQUEST_APPR A
		INNER	JOIN
				TB_M_EMPLOYEE E
		ON		E.SYS_EMP_CODE	= @NextApproverCode
		INNER	JOIN
				TB_M_ORGANIZATION D
		ON		E.ORG_CODE		= D.ORG_CODE
		WHERE	DOC_NO			= @DOC_NO AND
				INDX			= @NextIndx

	END

	-- Set Next Approver
	UPDATE	TB_R_REQUEST_APPR -- Should be update Actual Role
	SET		APPR_STATUS		= 'W',
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @LOGIN_USER
	WHERE	DOC_NO			= @DOC_NO AND
			INDX			= @NextIndx

	

	-- Send Email to Next Approver
	RETURN
END
GO
