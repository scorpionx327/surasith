DROP PROCEDURE [dbo].[sp_Common_GetEMP_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetEMP_AutoComplete]
@keyword		varchar(50),
@EmpCode		T_SYS_USER
AS
BEGIN
		SELECT COMPANY,
				EMP_CODE,
				EMP_TITLE,
				EMP_NAME,
				EMP_LASTNAME,
				ORG_CODE,
				SYS_EMP_CODE
		FROM	TB_M_EMPLOYEE E
		WHERE	(	
					EMP_CODE LIKE CONCAT(@keyword,'%') 
					OR
					EMP_NAME LIKE CONCAT(@keyword,'%') 
				) 
END
GO
