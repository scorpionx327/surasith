DROP PROCEDURE [dbo].[sp_WFD01210_GetCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 06/03/2017
-- Description:	get cost center
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetCostCenter]
	 @EMP_CODE T_USER
	,@pageNum INT
    , @pageSize INT
    , @sortColumnName VARCHAR(50)
	, @orderType VARCHAR(5)
	, @TOTAL_ITEM		int output
AS
BEGIN

	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		   SELECT @TOTAL_ITEM = COUNT(1)
		  FROM [TB_M_COST_CENTER]
		  where  		  COST_CODE IN (SELECT COST_CODE FROM dbo.FN_FD0COSTCENTERLIST ( @EMP_CODE));

WITH Paging AS
  (
      SELECT [COST_CODE]
      ,[COST_NAME]
      ,null [PLANT_NAME]
	  , CAST(ROW_NUMBER() OVER 
			 (ORDER BY 
			  CASE WHEN @sortColumnName = '0' and @orderType='asc' THEN [COST_CODE]
					END ASC,											 
			 CASE WHEN @sortColumnName = '0' and @orderType='desc' THEN [COST_CODE]
					END DESC,
			 CASE WHEN @sortColumnName = '1' and @orderType='asc' THEN [COST_NAME] 
					END ASC,
			 CASE WHEN @sortColumnName = '1' and @orderType='desc' THEN [COST_NAME] 
					END DESC
			 --CASE WHEN @sortColumnName = '2' and @orderType='asc' THEN  [PLANT_NAME]
				--	END ASC,										    
			 --CASE WHEN @sortColumnName = '2' and @orderType='desc' THEN [PLANT_NAME]
				--	END DESC
				 ) AS INT) AS RowNumber
  FROM [TB_M_COST_CENTER]
  where -- STATUS='Y' AND
   COST_CODE IN (SELECT COST_CODE FROM dbo.FN_FD0COSTCENTERLIST ( @EMP_CODE))
  
  )
  SELECT *
  FROM Paging
  WHERE RowNumber BETWEEN (@pageNum - 1) * @pageSize + 1 
   AND @pageNum * @pageSize
END



GO
