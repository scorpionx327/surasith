DROP PROCEDURE [dbo].[sp_Common_GetDetailCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Common_GetDetailCostCenter]
(	
	@COST_CODE	VARCHAR(8) 
)
AS
BEGIN
     SELECT COST_CODE,COST_NAME FROM TB_M_COST_CENTER WHERE COST_CODE = @COST_CODE
END



GO
