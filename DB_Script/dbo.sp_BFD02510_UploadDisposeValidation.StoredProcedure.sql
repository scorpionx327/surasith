DROP PROCEDURE [dbo].[sp_BFD02510_UploadDisposeValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD02510_UploadDisposeValidation]
@AppID		INT,
@Company	T_COMPANY,
@GUID		T_GUID,
@User		T_SYS_USER,
@ErrorCnt	INT OUT
AS
BEGIN
	SET @ErrorCnt = 0
	
	DECLARE @Msg VARCHAR(255)
	SET @Msg = dbo.fn_GetMessage('MFAS0405AERR')
	-- PRINT @Msg
	-- Check company between file and Screen
	IF EXISTS(SELECT 1 FROM TB_S_REQUEST_DISP_D WHERE COMPANY <> @Company)
	BEGIN
		INSERT	TB_L_LOGGING 
		(		APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  		SELECT  @AppID, 'P', 'N', 'W',
				dbo.fn_StringFormat(@Msg , CONCAT(S.COMPANY,'|', @Company)),'Y', @User, GETDATE()
		FROM	TB_S_REQUEST_DISP_D S
		WHERE	S.COMPANY <> @Company
		GROUP	BY
				S.COMPANY

		RETURN 
	END
	
	-- select * from TB_M_MESSAGE where MESSAGE_CODE = 'MSTD0012AERR'
	----------------------------------------------------------------------
	-- Check Asset exists	
	SET @Msg = dbo.fn_GetMessage('MSTD0012AERR')

	INSERT	TB_L_LOGGING 
	(		APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	SELECT  @AppID, 'P', 'N', 'W',
			dbo.fn_StringFormat(@Msg , CONCAT('COMPANY : ', S.COMPANY, 'ASSET NO : ',S.ASSET_NO,'ASSET SUB : ', S.ASSET_SUB,'|Assets Master') ),'Y', @User, GETDATE()
	
	

	FROM	TB_S_REQUEST_DISP_D S
	LEFT	JOIN
			TB_M_ASSETS_H M
	ON		S.COMPANY	= M.COMPANY AND 
			S.ASSET_NO	= M.ASSET_NO AND 
			S.ASSET_SUB = M.ASSET_SUB
	WHERE	M.ASSET_NO IS NULL
	SET	@ErrorCnt = @ErrorCnt + @@ROWCOUNT

	-- MFAS0402AERR	ERR	You have no authoizate to access Cost Center : {0}
	-- MFAS0403AERR	ERR	COMPANY : {0}, ASSET NO : {1}, ASSET SUB : {2} is inactive
	-- MFAS0404AERR	ERR	COMPANY : {0}, ASSET NO : {1}, ASSET SUB : {2} is in {3} request
	DECLARE @MsgMFAS0404AERR VARCHAR(255)
	SET @MsgMFAS0404AERR = dbo.fn_GetMessage('MFAS0404AERR')

	-- Check Asset expire / Inprocess
	INSERT	TB_L_LOGGING 
	 (		APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	 SELECT  @AppID, 'P', 'N', 'W',
				CASE	WHEN M.[STATUS] != 'N' THEN dbo.fn_StringFormat(@Msg, CONCAT(M.COMPANY,'|', M.ASSET_NO,'|', M.ASSET_SUB))
						ELSE dbo.fn_StringFormat(@MsgMFAS0404AERR, CONCAT(M.COMPANY,'|', M.ASSET_NO,'|', M.ASSET_SUB,'|', M.PROCESS_STATUS))
				END,
				'Y', @User, GETDATE()
	FROM	TB_S_REQUEST_DISP_D S
	INNER	JOIN
			TB_M_ASSETS_H M
	ON		S.COMPANY	= M.COMPANY AND 
			S.ASSET_NO	= M.ASSET_NO AND 
			S.ASSET_SUB = M.ASSET_SUB
	WHERE	M.[STATUS]	!= 'Y' OR
			M.PROCESS_STATUS	IS NOT NULL
	SET		@ErrorCnt = @ErrorCnt + @@ROWCOUNT
	
	----------------------------------------------------------------------
	-- Check User authoization
	SET @Msg = dbo.fn_GetMessage('MFAS0402AERR')

	INSERT	TB_L_LOGGING 
	 (		APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	 SELECT DISTINCT @AppID, 'P', 'N', 'W',
				dbo.fn_StringFormat(@Msg, M.COST_CODE),
				'Y', @User, GETDATE()

	FROM	TB_S_REQUEST_DISP_D S
	INNER	JOIN
			TB_M_ASSETS_H M
	ON		S.COMPANY	= M.COMPANY AND 
			S.ASSET_NO	= M.ASSET_NO AND 
			S.ASSET_SUB = M.ASSET_SUB
	WHERE	NOT EXISTS(	SELECT	1 
						FROM	TB_T_COST_CENTER_MAPPING CC 
						WHERE	CC.EMP_CODE = @User AND 
								CC.COST_CODE = M.COST_CODE AND 
								CC.COMPANY = M.COMPANY)

END
GO
