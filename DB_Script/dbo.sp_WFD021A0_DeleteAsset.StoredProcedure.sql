DROP PROCEDURE [dbo].[sp_WFD021A0_DeleteAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_ClearAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD021A0_DeleteAsset]
(	
	@GUID		T_GUID,
	@DOC_NO		T_DOC_NO,
	@LINE_NO	INT

)
AS
BEGIN
	
	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_H WHERE DOC_NO = @DOC_NO)
	BEGIN
		UPDATE	TB_R_REQUEST_ASSET_D
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				LINE_NO		= @LINE_NO
		
	END

	-- delete if exists in submit mode	
	DELETE	TB_T_REQUEST_ASSET_D
	WHERE	[GUID]		= @GUID AND 
			LINE_NO		= @LINE_NO

END
GO
