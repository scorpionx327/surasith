DROP PROCEDURE [dbo].[sp_WFD02910_Resend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02910_Resend]
@GUID		T_GUID,
@DOC_NO		T_DOC_NO,
@LINE_NO	VARCHAR(MAX),
@USER		T_SYS_USER
AS
BEGIN
		-- Update Actual (All Record)
		EXEC sp_WFD02910_UpdateAssetApprove @GUID, @DOC_NO, @USER

		SELECT	LINE_NO
		INTO	#TB_T_LINENO
		FROM	TB_R_REQUEST_RECLAS
		WHERE	1 = 2
	
		INSERT
		INTO	#TB_T_LINENO(LINE_NO)
		SELECT	[value]
		FROM	dbo.FN_SPLIT(@LINE_NO,'|')
		IF @@ROWCOUNT = 0
		BEGIN
			RETURN
		END

		-- Actual Table
		UPDATE	D
		SET		D.[STATUS]				=	'GEN',
				D.UPDATE_DATE			=	GETDATE(),
				D.UPDATE_BY				=	@User
		FROM	TB_R_REQUEST_RECLAS D
		INNER	JOIN
				#TB_T_LINENO	TT
		ON		D.LINE_NO	= TT.LINE_NO
		WHERE	D.DOC_NO	= @DOC_NO

		DROP TABLE #TB_T_LINENO;
END
GO
