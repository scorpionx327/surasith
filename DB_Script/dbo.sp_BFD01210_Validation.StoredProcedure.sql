DROP PROCEDURE [dbo].[sp_BFD01210_Validation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====BFD01220 : Employee Master Interface batch========================================
-- UISS_1.2.2_BFD01220_Employee Master Interface batch
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/06 (yyyy/mm/dd)
-- Description: UISS_1.2.2_BFD01220_Employee Master Interface batch

*/
CREATE PROCEDURE  [dbo].[sp_BFD01210_Validation]
 @AppID			INT,
 @UserID	   VARCHAR(8)
AS
BEGIN TRY

	
	DECLARE @Msg VARCHAR(500)

    SET		@Msg = dbo.fn_GetMessage('MSTD0012AERR')
	INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	SELECT  @AppID, 'P', 'N', 'E',
			dbo.fn_StringFormat(@Msg , CONCAT('ORG_CODE : ',r.ORG_CD,'|',' Organization Master (TB_M_ORGANIZATION)')),'Y', @UserID, GETDATE()
	FROM    TB_S_EMP_SAP r
	WHERE	NOT EXISTS (	SELECT	1 
							FROM	TB_M_ORGANIZATION Org
							WHERE	r.ORG_CD=Org.ORG_CODE )

	DELETE	S
	FROM    TB_S_EMP_SAP S
	WHERE	NOT EXISTS (	SELECT	1 
							FROM	TB_M_ORGANIZATION Org
							WHERE	S.ORG_CD=Org.ORG_CODE )
	IF @@ROWCOUNT > 0
		RETURN 2; -- ERROR

	RETURN 1 ; -- SUCCESS

END TRY
BEGIN CATCH
			PRINT concat('ERROR Message:',ERROR_MESSAGE())
END CATCH
GO
