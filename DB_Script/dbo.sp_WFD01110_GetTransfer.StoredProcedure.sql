DROP PROCEDURE [dbo].[sp_WFD01110_GetTransfer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Surasith T.
-- Create date: 21/09/2019
-- Description:	get transfer tab in home page
/*
Nipon 2019-08-14 16:38:10.537
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01110_GetTransfer]
	-- Add the parameters for the stored procedure here
	@COMPANY VARCHAR(50)=null, -- not use T_COMPANY varchar so out of range.
	@EMP_CODE T_SYS_USER,
	@TRNF_MAIN_TYPE VARCHAR(10)=null,
	@pageNum INT,
    @pageSize INT,
    @sortColumnName VARCHAR(50),
	@orderType VARCHAR(5),
	@TOTAL_ITEM int output
AS
BEGIN
	-- Prepare #Temp for keep emp code, delegate, role of login user
	CREATE TABLE #TB_T_SCOPE(EMP_CODE VARCHAR(25), COMPANY VARCHAR(10), SP_ROLE VARCHAR(3))
	EXEC sp_WFD01110_SetScope @EMP_CODE

	SELECT	CASE WHEN @sortColumnName = '0' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY COMPANY asc)  
				 WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPANY desc)   
				 WHEN @sortColumnName = '1' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY DOC_NO asc)  
				 WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DOC_NO desc)  
				 WHEN @sortColumnName = '2' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY TRNF_MAIN_TYPE asc)
				 WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TRNF_MAIN_TYPE desc)
				 WHEN @sortColumnName = '3' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY ASSET_CNT asc)
				 WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY ASSET_CNT desc)
				 WHEN @sortColumnName = '4' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY TRANSFER_FROM asc) 
				 WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TRANSFER_FROM desc)  
				 WHEN @sortColumnName = '5' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY TRANSFER_TO asc) 
				 WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TRANSFER_TO desc) 
				 WHEN @sortColumnName = '6' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR asc) 
				 WHEN @sortColumnName = '6' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUESTOR desc)  
				 WHEN @sortColumnName = '7' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS asc) 
				 WHEN @sortColumnName = '7' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY CURRENT_STATUS desc)  
				 WHEN @sortColumnName = '8' AND @orderType = 'asc'	THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE asc) 
				 WHEN @sortColumnName = '8' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_DATE desc) 
				 ELSE ROW_NUMBER() OVER(ORDER BY DOC_NO ASC)   
			END AS ROWNUMBER,
			T.*
	INTO	#TB_T_RESULT
	FROM	
	(
	   SELECT	R.DOC_NO,
				R.COMPANY,
				dbo.fn_GetSystemMaster('REQUEST','TRANS_MAIN_TYPE', H.TRNF_MAIN_TYPE)  TRNF_MAIN_TYPE,
				COUNT(1)			AS ASSET_CNT,
				CASE WHEN H.TRNF_MAIN_TYPE = 'RC' THEN MIN(D.RESP_COST_CODE) ELSE R.ASSET_COST_CODE END AS TRANSFER_FROM,
				CASE WHEN H.TRNF_MAIN_TYPE = 'RC' THEN H.RESP_COST_CODE ELSE H.COST_CODE END AS TRANSFER_TO,
				CONCAT(R.EMP_TITLE, ' ', R.EMP_NAME, ' ', R.EMP_LASTNAME)   AS REQUESTOR,
				dbo.fn_GetCurrentStatus(R.DOC_NO) AS CURRENT_STATUS,
				R.REQUEST_DATE,
				R.REQUEST_TYPE
		FROM	[dbo].[TB_R_REQUEST_H] R
		LEFT	JOIN 
				TB_R_REQUEST_APPR A
		on		r.DOC_NO = a.DOC_NO 
		LEFT	JOIN  
				TB_R_REQUEST_TRNF_H H
		ON		R.DOC_NO = h.DOC_NO
		LEFT	JOIN
				TB_R_REQUEST_TRNF_D D
		ON		H.DOC_NO	= D.DOC_NO

		WHERE	R.REQUEST_TYPE	= 'T' AND 
				dbo.fn_IsEndRequest(r.STATUS) = 'N' AND
				A.APPR_STATUS	= 'W' AND 
				( EXISTS(	SELECT	1 
							FROM	dbo.fn_GetMultipleCompanyList(@COMPANY) C 
							WHERE	C.COMPANY_CODE = r.COMPANY ) OR @COMPANY IS NULL ) AND
				( EXISTS(	SELECT	1 
							FROM	dbo.fn_GetMultipleList(@TRNF_MAIN_TYPE) C 
							WHERE	C.[value] = h.TRNF_MAIN_TYPE ) OR @TRNF_MAIN_TYPE IS NULL ) AND
				-- filter
				( EXISTS(	SELECT	1
							FROM	#TB_T_SCOPE T
							WHERE	T.EMP_CODE	= A.EMP_CODE ) OR
				  EXISTS(	SELECT	1
							FROM	#TB_T_SCOPE T
							WHERE	T.COMPANY	= R.COMPANY AND T.SP_ROLE = A.APPR_ROLE AND A.NOTIFICATION_MODE = 'G' )
				) 
				-- Add Emp code Condition
		GROUP	BY
				R.DOC_NO,
				R.COMPANY,
				R.ASSET_COST_CODE,
				R.EMP_CODE,
				R.EMP_TITLE,
				R.EMP_NAME,
				R.EMP_LASTNAME,
				R.[STATUS],
				R.REQUEST_DATE,
				R.REQUEST_TYPE,
				H.TRNF_MAIN_TYPE,				
				R.ASSET_COST_CODE,
				H.COST_CODE,
				H.RESP_COST_CODE
				
	)	T
	SET @TOTAL_ITEM = @@ROWCOUNT


	IF(@sortColumnName IS NULL )
	BEGIN
		SET @sortColumnName ='0';
	END
	IF(@orderType IS NULL )
	BEGIN
		SET @orderType ='asc';
	END

	DECLARE @FROM	INT , @TO	INT

	SET @FROM	= (@pageSize * (@pageNum-1)) + 1;
	SET @TO		= @pageSize * (@pageNum);						

	--begin modify by thanapon: fixed paging and sorting
	SELECT	d.ROWNUMBER,
			d.REQUEST_TYPE,
			d.DOC_NO,
			d.COMPANY,
			d.TRNF_MAIN_TYPE,
			d.ASSET_CNT,
			d.TRANSFER_FROM,
			d.TRANSFER_TO,
			d.REQUESTOR,
			d.CURRENT_STATUS,
			dbo.fn_dateFAS(REQUEST_DATE) AS REQUEST_DATE,
			C.COST_NAME  AS TRANSFER_FROM_HINT,
			CC.COST_NAME AS TRANSFER_TO_HINT

	FROM	#TB_T_RESULT d
	LEFT	JOIN
			TB_M_COST_CENTER C
	ON		d.COMPANY		= C.COMPANY AND
			d.TRANSFER_FROM	= C.COST_CODE
	LEFT	JOIN
			TB_M_COST_CENTER CC
	ON		d.COMPANY		= CC.COMPANY AND
			d.TRANSFER_TO	= CC.COST_CODE

	WHERE	d.ROWNUMBER >= @FROM and 
			d.ROWNUMBER <= @TO
	ORDER	BY 
			d.ROWNUMBER
	
	IF OBJECT_ID('tempdb..#TB_T_RESULT') IS NOT NULL 
		DROP TABLE #TB_T_RESULT 
	IF OBJECT_ID('tempdb..#TB_T_SCOPE') IS NOT NULL 
		DROP TABLE #TB_T_SCOPE 

	
END
GO
