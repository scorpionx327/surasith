DROP PROCEDURE [dbo].[sp_WFD01170_CheckGroupOperation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01170_CheckGroupOperation]
@Company			T_COMPANY,
@DocNo				T_DOC_NO,
@EmpCode			T_SYS_USER,
@CurrentIndx		INT,
@Division			VARCHAR(4),
@CurrentRole		VARCHAR(3),
@Result				VARCHAR(1) OUT
AS
BEGIN
	-- Fixed Role
	DECLARE @FS VARCHAR(3) = 'FS'
	DECLARE @FW	VARCHAR(3) = 'FW'
	DECLARE @AECManager	VARCHAR(3) = 'ACM'
	DECLARE @Cnt INT
	---------------------------------------------------------------------------------------------
	IF @CurrentRole = @FS
	BEGIN
		SELECT	@Cnt = COUNT(1)
		FROM	TB_R_REQUEST_COST_CENTER RC
		WHERE	DOC_NO	 = @DocNo AND 
				DIVISION = @Division AND
				EXISTS(	SELECT	1 
						FROM	TB_M_FASUP_COST_CENTER FS 
						WHERE	FS.COMPANY		= RC.COMPANY AND
								FS.COST_CODE	= RC.RESP_COST_CODE AND
								FS.EMP_CODE		= @EmpCode )
		SET @Result = IIF(ISNULL(@Cnt,0) > 0,'Y','N') 
		RETURN
	END
	---------------------------------------------------------------------------------------------
	IF @CurrentRole = @FW -- Actual, shoud set by PIC because 1 CC under 1 FW
	BEGIN
		SELECT	@Cnt = COUNT(1)
		FROM	TB_R_REQUEST_COST_CENTER RC
		WHERE	DOC_NO	 = @DocNo AND
				DIVISION = @Division AND
				EXISTS(	SELECT	1 
						FROM	TB_M_SV_COST_CENTER FW 
						WHERE	FW.COMPANY		= RC.COMPANY AND
								FW.COST_CODE	= RC.COST_CODE AND
								FW.EMP_CODE		= @EmpCode )
		SET @Result = IIF(ISNULL(@Cnt,0) > 0,'Y','N') 
		RETURN
	END

	

	IF  EXISTS(	SELECT	1 
				FROM	TB_M_SYSTEM 
				WHERE	CATEGORY		= 'RESPONSIBILITY' AND 
						SUB_CATEGORY	= 'ROLE_CODE' AND
						CODE			= @CurrentRole AND ACTIVE_FLAG	= 'Y')
	BEGIN
		IF NOT EXISTS(	SELECT	1 
						FROM	TB_M_EMPLOYEE E 
						WHERE	SYS_EMP_CODE = @EmpCode AND [ROLE] = @CurrentRole )
		BEGIN
			RETURN
		END

		DECLARE @OrgCodeLevelList VARCHAR(40)

		SELECT	@OrgCodeLevelList = [VALUE]
		FROM	TB_M_SYSTEM 
		WHERE	CATEGORY		= 'SYSTEM_CONFIG'	AND
				SUB_CATEGORY	=  CONCAT(@Company,'_APPROVE_DIRECT') AND
				CODE			= @CurrentRole AND ACTIVE_FLAG = 'Y'
		IF @@ROWCOUNT = 0
		RETURN

		-- Check Org.Level by Role
		DECLARE @Level VARCHAR(4)
		SELECT	TOP 1 @Level = s.value
		FROM	dbo.FN_SPLIT(@OrgCodeLevelList,'|') S 
		ORDER	by dbo.fn_GetSystemMaster('','',s.value) DESC
		IF @@ROWCOUNT = 0
		RETURN



		-- All Employee 
		SELECT	@Cnt = COUNT(1)
		FROM	TB_M_EMPLOYEE E INNER JOIN TB_M_ORGANIZATION T
		ON		E.ORG_CODE	= T.ORG_CODE
		WHERE	E.ACTIVEFLAG	= 'Y' AND
				EXISTS(	SELECT	1
						FROM	TB_R_REQUEST_COST_CENTER T 
						WHERE	T.DOC_NO		= @DocNo AND 
								T.DIVISION		= @Division AND
								T.COST_CODE		= E.COST_CODE ) AND
				CASE	WHEN @Level = 'COM'  THEN T.CMP_CODE 
						WHEN @Level = 'DIV'  THEN T.DIV_CODE 
						WHEN @Level = 'SDIV' THEN T.SUB_DIV_CODE 
						WHEN @Level = 'DEPT' THEN T.DEPT_CODE 
						WHEN @Level = 'SECT' THEN T.SECTION_CODE 
						WHEN @Level = 'LINE' THEN T.LINE_CODE  END =
					-- Find Org Code of Login User
					(SELECT CASE	WHEN @Level = 'COM'  THEN T.CMP_CODE 
									WHEN @Level = 'DIV'  THEN T.DIV_CODE 
									WHEN @Level = 'SDIV' THEN T.SUB_DIV_CODE 
									WHEN @Level = 'DEPT' THEN T.DEPT_CODE 
									WHEN @Level = 'SECT' THEN T.SECTION_CODE 
									WHEN @Level = 'LINE' THEN T.LINE_CODE END
					FROM	TB_M_EMPLOYEE E INNER JOIN TB_M_ORGANIZATION T
					ON		E.ORG_CODE	= T.ORG_CODE
					WHERE	E.SYS_EMP_CODE	= @EmpCode )

		IF ISNULL(@Cnt,0) > 0
		SET @Result = 'Y'
		RETURN
	END
	---------------------------------------------------------------------------------------------
	-- Special Approve
	IF EXISTS(	SELECT	1 
				FROM	TB_M_SP_ROLE 
				WHERE	SP_ROLE_COMPANY = @Company AND 
						SP_ROLE			= @CurrentRole AND 
						EMP_CODE		= @EmpCode)
	BEGIN
		SET @Result = 'Y'
		RETURN
	END

END
GO
