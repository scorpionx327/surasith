DROP PROCEDURE [dbo].[sp_WFD02130_UpdateBOIDoc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_WFD02130_UpdateBOIDoc]
	(	@EMP_CODE				T_SYS_USER,
		@COMPANY				T_COMPANY,
		@ASSET_NO				T_ASSET_NO,
		@ASSET_SUB				T_ASSET_SUB,		
		@FileName				VARCHAR(250),
		@FullFileName			VARCHAR(250),
		@FileType				VARCHAR(5),
		@UPDATE_DATE VARCHAR(50)															
	 )
AS
BEGIN TRY
BEGIN TRANSACTION T1
        IF NOT EXISTS( 
			SELECT  1 FROM TB_M_ASSETS_H 
			WHERE	COMPANY	= @COMPANY AND
					ASSET_NO =@ASSET_NO  AND 
					ASSET_SUB	= @ASSET_SUB AND
					FORMAT(UPDATE_DATE,'ddMMyyyyHHmmssfff') =@UPDATE_DATE)
             BEGIN
					 --SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'
						--SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
						DECLARE @Msg VARCHAR(500)

                      SET		@Msg = CONCAT('[CUSTOM]MCOM0008AERR|' ,dbo.fn_GetMessage('MCOM0008AERR'));

						RAISERROR (@Msg, -- Message text.
							   16, -- Severity.
							   1 -- State.
							   );
						RETURN;
			   END
				
				
				DELETE
				FROM	TB_R_REQUEST_ATTACH_DOC
				WHERE	DOC_NO		= @COMPANY AND
						DOC_UPLOAD	= 'BOI' AND
						ASSET_NO	= @ASSET_NO AND
						ASSET_SUB	= @ASSET_SUB
				
			
				INSERT
				INTO	TB_R_REQUEST_ATTACH_DOC
				(		DOC_NO,
						DOC_UPLOAD,
						ASSET_NO,
						ASSET_SUB,
						[GUID],
						LINE_NO,
						SEQ,
						FILE_NAME_ORIGINAL,
						ATTACH_DOC,
						ATTACH_TYPE,
						CREATE_DATE,
						CREATE_BY,
						UPDATE_DATE,
						UPDATE_BY
				)
				VALUES
				(		@COMPANY,
						'BOI',
						@ASSET_NO,
						@ASSET_SUB,
						'WFD02130',
						1,
						1,
						@FileName,
						@FullFileName,
						@FileType,
						GETDATE(),
						@EMP_CODE,
						GETDATE(),
						@EMP_CODE
				)
			 
	COMMIT TRANSACTION T1
	 RETURN ;

END TRY
BEGIN CATCH
	 if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END

	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		SELECT -1;
		RETURN;
END CATCH
GO
