DROP PROCEDURE [dbo].[sp_WFD02A00_UpdateAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02A00_UpdateAsset]
@GUID				T_GUID,
@DOC_NO				T_DOC_NO,
@COMPANY			T_COMPANY,
@ASSET_NO			T_ASSET_NO,
@ASSET_SUB			T_ASSET_SUB,
@ACC_PRINCIPLE		VARCHAR(4),
@REASON				VARCHAR(50),
@AMOUNT_POSTED		DECIMAL(23,2),
@PERCENTAGE			INT,
@IMPAIRMENT_FLAG	VARCHAR(2),
@ADJ_TYPE			VARCHAR(1),
@USER				T_SYS_USER
AS
BEGIN
	-- this function is execute from screen, manage data on temp table only

	UPDATE	TB_T_REQUEST_IMPAIRMENT
	SET		ACC_PRINCIPLE		= @ACC_PRINCIPLE,
			REASON				= @REASON,
			AMOUNT_POSTED		= @AMOUNT_POSTED,
			[PERCENTAGE]		= @PERCENTAGE,
			IMPAIRMENT_FLAG		= @IMPAIRMENT_FLAG,
			ADJ_TYPE			= @ADJ_TYPE,
			UPDATE_DATE			= GETDATE(),
			UPDATE_BY			= @USER
	WHERE	[GUID]				= @GUID AND
			COMPANY				= @COMPANY AND
			ASSET_NO			= @ASSET_NO AND
			ASSET_SUB			= @ASSET_SUB
END
GO
