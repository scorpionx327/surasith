DROP PROCEDURE [dbo].[sp_WFD01210_GetCostCenter_In_Charge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 10/03/2017
-- Description:	Get Cost center In-charge
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetCostCenter_In_Charge]
@FIND			VARCHAR(60),
@NO_ASSET_FLAG	VARCHAR(1) = 'N', -- 2017-06-23 Surasith T. Display Cost Center that no assets include when Y
@EMP_CODE		T_SYS_USER
AS
BEGIN
	SET @NO_ASSET_FLAG = ISNULL(@NO_ASSET_FLAG, 'N')

	DECLARE	@COMPANY	T_COMPANY
	SELECT	@COMPANY	= COMPANY
	FROM	TB_M_EMPLOYEE E
	WHERE	E.SYS_EMP_CODE	= @EMP_CODE

	SET		@FIND = dbo.fn_GetSearchCriteria(@FIND)
	SELECT	NULL PLANT_CD,
			NULL PLANT_NAME,
			C.COST_CODE,
			C.COST_NAME, 
			CASE WHEN H.COST_CODE IS NULL THEN 'N' ELSE 'Y' END AS STYLE -- Set Inactive
	FROM	TB_M_COST_CENTER C
	LEFT	JOIN
	(
			SELECT	H.COMPANY,
					H.COST_CODE
			FROM	TB_M_ASSETS_H H
			WHERE	H.COMPANY		= @COMPANY AND
					H.[STATUS]		= 'Y' AND
					H.INHOUSE_FLAG	= 'Y'
			GROUP	BY
					H.COMPANY,
					H.COST_CODE
	) H
	ON		C.COMPANY	= H.COMPANY AND
			C.COST_CODE	= H.COST_CODE	
	LEFT	JOIN
			TB_M_SV_COST_CENTER FS
	ON		C.COMPANY	= FS.COMPANY AND 
			C.COST_CODE = FS.COST_CODE
	
	WHERE	C.COMPANY	= @COMPANY AND
			FS.COST_CODE IS NULL AND
			(
				( @NO_ASSET_FLAG = 'Y' AND H.COST_CODE IS NOT NULL) OR
				( @NO_ASSET_FLAG = 'N' AND H.COST_CODE IS NULL )
			
			)
			AND	C.[STATUS] = 'Y'
			AND(
					(C.COST_CODE LIKE @FIND or @FIND IS NULL )
					OR (C.COST_NAME LIKE @FIND or @FIND IS NULL )
				)
			
	ORDER	BY	
			C.COST_CODE
END



GO
