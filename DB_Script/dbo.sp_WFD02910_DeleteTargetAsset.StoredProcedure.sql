DROP PROCEDURE [dbo].[sp_WFD02910_DeleteTargetAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_ClearAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD02910_DeleteTargetAsset]
(	
	@GUID		T_GUID,
	@DOC_NO		T_DOC_NO,
	@COMPANY	T_COMPANY,
	@LINE_NO	INT
)
AS
BEGIN
	
	DELETE	TB_T_REQUEST_ASSET_D
	WHERE	[GUID]		= @GUID AND 
			COMPANY		= @COMPANY AND
			LINE_NO		= @LINE_NO -- If Exists

			-- Insert into temp
	
	UPDATE	TB_T_REQUEST_RECLAS
	SET		NEW_ASSET_NO	= NULL,		
			NEW_ASSET_SUB	= NULL,		
			NEW_ASSET_NAME	= NULL,		
			NEW_ASSET_CLASS	= NULL,	
			DOCUMENT_DATE	= NULL,	
			POSTING_DATE	= NULL,	
			[PERCENTAGE]	= NULL,	
			AMOUNT_POSTED	= NULL,	
			PARTIAL_FLAG	= NULL,	
			ASSET_VALUE_DATE= NULL
			-- REASON			= NULL
	WHERE	[GUID]			= @GUID AND 
			COMPANY			= @COMPANY AND
			LINE_NO			= @LINE_NO

END


GO
