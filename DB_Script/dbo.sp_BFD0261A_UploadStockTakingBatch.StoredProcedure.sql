DROP PROCEDURE [dbo].[sp_BFD0261A_UploadStockTakingBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- UISS_2.6.10_Upload Asset No for Stock Taking_batch
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/20 (yyyy/mm/dd)
-- Description:  UISS_2.6.10_Upload Asset No for Stock Taking_batch
-- Objective  :This process is main function upload stock taking batch
REMARK : Value return
	1  : end with success
	0  : end with warning
	-1 : end with error 
-- =============================================  SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD0261A_UploadStockTakingBatch]
(
 			@AppID	INT,
			@UserID		VARCHAR(8),
	        @IsUpdate VARCHAR(1), -- N is newcase, Y is update case
            @UpdateBy VARCHAR(8),
            @StockYear VARCHAR(4),
            @StockRound VARCHAR(2),
            @StockAssetLocation VARCHAR(1), --I is In house,O is out-source
            @QTyOfHanheld VARCHAR(8),
            @TargetDateFrom VARCHAR(8), --format YYYYMMDD
            @TargetDateTo VARCHAR(8), --format YYYYMMDD
            @BreakTime VARCHAR(8) 
)
AS
BEGIN TRY
	DECLARE	@cnt				INT
	DECLARE @ERROR_MESSAGE VARCHAR(MAX) 

	SELECT	@cnt = COUNT(*)
	FROM	TB_S_STOCK_TAKE

	IF(@cnt = 0)
	BEGIN
	     EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MCOM0020AWRN', 'Upload Asset No for Stock Taking batch',@UpdateBy
		SELECT 0; -- 1 is mean end with warning
		RETURN 0;
	END
	-- for set trim and upper data in stagging table 
	EXEC sp_BFD0261A_UpperAndTrimDataInStagingTable 


	 IF (@IsUpdate='N') -- Insert Case
	BEGIN
			
				   SET @StockAssetLocation=(SELECT TOP 1  ASSET_LOCATION
											 FROM	 (	SELECT  CASE   WHEN AssetH.INHOUSE_FLAG ='Y' THEN 'I'
															   ELSE 'O' END ASSET_LOCATION 
														FROM    TB_S_STOCK_TAKE r INNER JOIN  TB_M_ASSETS_H AssetH  ON r.ASSET_NO=AssetH.ASSET_NO
														AND r.ASSET_SUB=AssetH.ASSET_SUB and r.COMPANY=AssetH.COMPANY
														GROUP BY AssetH.INHOUSE_FLAG
														)s )
	END 

     --- Validate check 
	DECLARE @Error BIT
	--PRINT '-------- Validate check 
     EXEC @Error=sp_BFD0261A_Validation @AppID,@UpdateBy ,@IsUpdate,@StockYear,@StockRound,@StockAssetLocation
	IF ( @Error = 1 ) 
	   BEGIN
	      GOTO ERROR;
	   END

BEGIN TRANSACTION T1
 IF (@IsUpdate='N') -- Insert Case
	BEGIN
		
	           EXEC @Error= sp_BFD0261A_InsertDataFromStagging	 @UpdateBy ,    @StockYear ,  @StockRound ,  @StockAssetLocation, 
											 @QTyOfHanheld ,    @TargetDateFrom ,@TargetDateTo , @BreakTime,	@ERROR_MESSAGE OUT 
			   IF ( @Error = 1 ) 
			   BEGIN
				  GOTO ERROR;
			   END
	 END

IF (@IsUpdate='Y') ---Update case
	BEGIN
		 	 EXEC @Error= sp_BFD0261A_UpdateDataFromStagging @UpdateBy ,    @StockYear ,  @StockRound ,  @StockAssetLocation, 
											 @QTyOfHanheld ,    @TargetDateFrom ,@TargetDateTo , @BreakTime,	@ERROR_MESSAGE OUT 
			   IF ( @Error = 1 ) 
			   BEGIN
				  GOTO ERROR;
			   END
	END


	COMMIT TRANSACTION T1
	SELECT 1;
	RETURN 1;

ERROR: 
	if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END
   	
		SELECT -1;
	    RETURN -1 ;

END TRY
BEGIN CATCH

	 if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END
--===========================================================Write log =====================================
     DECLARE @errMessage nVARCHAR(1000);
     SET @errMessage= CONCAT('Upload Asset No for Stock Taking batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
	 exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@errMessage,@UpdateBy,'Y'
--===========================================================End Write log =====================================
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())

		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		SELECT -1;
	    RETURN -1;
END CATCH





GO
