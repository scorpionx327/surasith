DROP PROCEDURE [dbo].[sp_WFD02210_ClearAssetCIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02210_ClearAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f1e24'
CREATE PROCEDURE [dbo].[sp_WFD02210_ClearAssetCIP]
(	
	@GUID					VARCHAR(50)			= NULL,		--GUID
	@DOC_NO					VARCHAR(10)			= NULL
)
AS
BEGIN

		DELETE FROM TB_T_SELECTED_ASSETS 
		WHERE GUID	= @GUID

	
		DELETE TB_R_REQUEST_CIP 
		WHERE DOC_NO			= @DOC_NO
END


GO
