DROP PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_KPI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_KPI]
@User	VARCHAR(8)
AS
BEGIN
	DECLARE @ID int,	
		@TO varchar(1000),
		@CC varchar(1000),
		@BCC varchar(1000),
		@TITLE varchar(255),
		@MESSAGE nvarchar(max),
		@Dear nvarchar(max),
		@FOOTER varchar(250),
		@TYPE varchar(1),
		@START_PERIOD datetime,
		@END_PERIOD datetime,
		@SEND_FLAG varchar(1),
		@RESULT_FLAG varchar(1),
		@REF_FUNC varchar(8),
		@REF_DOC_NO T_DOC_NO

	--SET @MESSAGE = 'Dear {Name of Approver or Delegate user} <br>';

	SET @MESSAGE = CONCAT(@MESSAGE,('<table style="border:1px solid black;">
										<thead style="height:25px; background-color:rgba(236, 240, 245, 1);"> 
										  <tr>
											<th style="min-width: 100px;">Request No.</th>	
											<th style="min-width: 100px;">Request Type</th>	
											<th style="min-width: 100px;">Last Approved By</th>	
											<th style="min-width: 100px;">Last Approved Date</th>	
											<th style="min-width: 100px;">Overdue</th>						
										  </tr>
										</thead>'));

	DECLARE @CurrentDate datetime;
	select @CurrentDate = [value]
		From TB_M_SYSTEM																
		Where CATEGORY = 'NOTIFICATION'																
			 AND SUB_CATEGORY = 'SUMMARY_KPI'															
			 AND CODE = 'NEXTTIME'	
-------------------------------------------------------------------------------------------------------
	DECLARE @DOC_NO varchar(100),@VALUE varchar(100),@LastApprovedBy varchar(100),@APPR_DATE varchar(100),@LEAD_TIME varchar(100),@EMAIL varchar(100) 

	DECLARE cursor_results CURSOR FOR 
		select KPI.DOC_NO,KPI.VALUE,KPI.LastApprovedBy,convert(varchar, KPI.APPR_DATE, 106) APPR_DATE,KPI.LEAD_TIME,KPI.EMAIL 
		from (
			Select top 1 ra.DOC_NO,s.VALUE, concat(ra.EMP_NAME,' ' ,ra.EMP_LASTNAME) LastApprovedBy, ra.APPR_DATE,ra.EMAIL --,ra.INDX,h.REQUEST_TYPE
				,l.LEAD_TIME,l.APPRV_ID,DATEADD(day,l.LEAD_TIME,ra.APPR_DATE) APPR_DATE_DUE
			From TB_R_REQUEST_APPR ra																			
			inner join (
				select ra.DOC_NO, max(ra.INDX) INDX
				from TB_R_REQUEST_APPR ra														
				Where ra.DOC_NO in (
							Select ra.DOC_NO
							From TB_R_REQUEST_APPR ra	
								left outer join TB_R_REQUEST_H h on ra.DOC_NO = h.DOC_NO
							Where ra.APPR_STATUS = 'W'
					)														
					and ra.APPR_STATUS = 'A'														
				Group by ra.DOC_NO	
			) m on ra.DOC_NO = m.DOC_NO And ra.INDX = m.INDX
			inner join TB_R_REQUEST_H h on ra.DOC_NO = h.DOC_NO
			inner join (
				select h.REQUEST_TYPE,d.INDX,d.ROLE,d.LEAD_TIME,h.APPRV_ID
				from TB_M_APPROVE_H h
					left outer join TB_M_APPROVE_D d on h.APPRV_ID = d.APPRV_ID
				where EXISTS (
								Select ra.DOC_NO,h.REQUEST_TYPE,ra.INDX,ra.APPR_ROLE,ra.EMP_CODE
								From TB_R_REQUEST_APPR ra	
									left outer join TB_R_REQUEST_H h on ra.DOC_NO = h.DOC_NO
								Where ra.APPR_STATUS = 'W'
							)
				) l on ra.INDX = l.INDX and h.REQUEST_TYPE = l.REQUEST_TYPE
			inner join TB_M_SYSTEM s on h.REQUEST_TYPE = s.CODE and s.CATEGORY = 'SYSTEM_CONFIG' and s.SUB_CATEGORY = 'REQUEST_TYPE'
			order by l.APPRV_ID desc) KPI
		where DATEDIFF(day,@CurrentDate,KPI.APPR_DATE_DUE) >= 0

	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @DOC_NO,@VALUE,@LastApprovedBy,@APPR_DATE,@LEAD_TIME,@EMAIL
	WHILE @@FETCH_STATUS = 0
		BEGIN 
			SET @MESSAGE = CONCAT(@MESSAGE,'<tr>');							
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;">',@DOC_NO,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:left;">',@VALUE,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 100px;text-align:left;">',@LastApprovedBy,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:center;">',@APPR_DATE,'</td>');	
			SET @MESSAGE = CONCAT(@MESSAGE,'	<td style="min-width: 50px;text-align:right;">',@LEAD_TIME,'</td>');		
			SET @MESSAGE = CONCAT(@MESSAGE,'</tr>');	

			FETCH NEXT FROM cursor_results into @DOC_NO,@VALUE,@LastApprovedBy,@APPR_DATE,@LEAD_TIME,@EMAIL
		END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;
--------------------------------------------------------------------------------------------------------
	SET @ID = (select isnull(max(ID),0) + 1 from TB_R_NOTIFICATION);
	SET @MESSAGE = CONCAT(@MESSAGE,'</table>');

	--select * from @MyTable
	SET @FOOTER = 'Best Regards,<br>System';

	SET @START_PERIOD = @CurrentDate;
	--SET @END_PERIOD = @EndDate;

	SET @SEND_FLAG = 'N';
	SET @RESULT_FLAG = 'N';
	SET @TYPE = 'I';
	SET @REF_FUNC = '';

	SET @Dear = concat('Dear ',@LastApprovedBy,' <br>');

	SET @TO = @EMAIL;
	SET @TITLE = 'TFAST : Document still waiting for approve.';
	INSERT INTO TB_R_NOTIFICATION
           (ID,[TO],CC,BCC,TITLE,[MESSAGE],FOOTER,[TYPE]
           ,START_PERIOD,END_PERIOD,SEND_FLAG,RESULT_FLAG,REF_FUNC,REF_DOC_NO
			,CREATE_DATE,CREATE_BY,UPDATE_DATE,UPDATE_BY)
     VALUES
           (@ID
		   ,@TO
           ,@CC
           ,@BCC
           ,@TITLE
           ,concat(@Dear,@MESSAGE)
           ,@FOOTER
           ,@TYPE
           ,@START_PERIOD
           ,@END_PERIOD
           ,@SEND_FLAG
           ,@RESULT_FLAG
           ,@REF_FUNC
           ,@REF_DOC_NO
           ,getdate()
           ,@User
           ,getdate()
           ,@User)

	--select @TO,@CC,@BCC,@TITLE,@MESSAGE,@FOOTER,@TYPE,@START_PERIOD,@END_PERIOD,@SEND_FLAG,@RESULT_FLAG,@REF_FUNC,@REF_DOC_NO,getdate(),@User,getdate(),@User
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'SUMMARY_KPI'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	SET @dNextTime = DATEADD(DAY, 1, @dNextTime)

	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= 'System'
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
			--select @MESSAGE
END
GO
