DROP PROCEDURE [dbo].[sp_WFD01150_GetTmpApproveFlow]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_GetTmpApproveFlow]
(
	@SEQ varchar(3)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MyTableVar table(  
						SEQ varchar(3),
						APPROVE  varchar(100),
						C01  varchar(1),
						C02  varchar(1),
						C20  varchar(1),
						C30  varchar(1),
						C40  varchar(1),
						C41  varchar(1),
						C50  varchar(1),
						C70  varchar(1),
						C71  varchar(1)
					); 
	
	insert into @MyTableVar select '01','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '01','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '01','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '01','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '02','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '02','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '02','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '02','Reject : cc','','','','','','','','',''
	
	insert into @MyTableVar select '20','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '20','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '20','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '20','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '30','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '30','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '30','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '30','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '40','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '40','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '40','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '40','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '41','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '41','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '41','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '41','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '50','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '50','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '50','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '50','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '70','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '70','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '70','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '70','Reject : cc','','','','','','','','',''

	insert into @MyTableVar select '71','Approve : To','','','','','','','','',''
	insert into @MyTableVar select '71','Approve : cc','','','','','','','','',''
	insert into @MyTableVar select '71','Reject : To','','','','','','','','',''
	insert into @MyTableVar select '71','Reject : cc','','','','','','','','',''


	IF(@SEQ is null)
		BEGIN
			select * from @MyTableVar;
		END
	ELSE
		BEGIN
			select * from @MyTableVar where SEQ = @SEQ;
		END
END
GO
