DROP PROCEDURE [dbo].[sp_WFD02640_CheckCreateRequestStockPerSV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nuttapon Prapipach
-- Create date: 11-10-2017
-- Description: Check Sub Ordinate Create Request Per SV
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_CheckCreateRequestStockPerSV]

	@EMP_CODE	T_SYS_USER,
	@COMPANY	T_COMPANY,
	@YEAR		varchar(4),
	@ROUND		varchar(2) ,
	@ASSET_LOCATION varchar(1) 

AS
BEGIN
	
	DECLARE @RESULT int;

	SELECT	@RESULT = COUNT(1)  
	FROM	TB_R_REQUEST_STOCK A
	 WHERE  COMPANY	= @COMPANY AND
			SV_EMP_CODE = @EMP_CODE AND
			A.YEAR = @YEAR and 
			A.ROUND = @ROUND and 
			A.ASSET_LOCATION = @ASSET_LOCATION 


	IF (@RESULT > 0)
	BEGIN
		SELECT CONVERT(bit, 1) as RESULT
	END
	ELSE
	BEGIN
		SELECT CONVERT(bit, 0) as RESULT
	END

END
GO
