DROP PROCEDURE [dbo].[sp_BFD0122X_InsertAndUpdateOrganizeFromStagging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =============================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/06 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE  [dbo].[sp_BFD0122X_InsertAndUpdateOrganizeFromStagging]
(
	 @UserID		VARCHAR(8),
	 @ERROR_MESSAGE	VARCHAR(MAX) OUT
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;
-- Case Update  
UPDATE  M
   SET  M.ORG_CODE  = S.ORG_CODE		  
      , M.COMP_CODE  =  S.COMP_CODE		  
      , M.GRP_CODE  =  S.GRP_CODE			
      , M.GRP_NAME  =  S.GRP_NAME			
      , M.DIV_CODE  =  S.DIV_CODE			
      , M.DIV_NAME  =  S.DIV_NAME			
      , M.DEPT_CODE  =  S.DEPT_CODE			
      , M.DEPT_NAME  =  S.DEPT_NAME			
      , M.SEC_CODE  =  S.SEC_CODE			
      , M.SEC_NAME  =  S.SEC_NAME			
      , M.LINE_CODE  =  S.LINE_CODE			
      , M.LINE_NAME  =  S.LINE_NAME			
      , M.SUBUNIT_CODE  =  S.SUBUNIT_CODE
      , M.SUBUNIT_NAME  =  S.SUBUNIT_NAME
      , M.SHIFT_CODE  =  S.SHIFT_CODE		
      , M.COST_CODE  =  S.COST_CODE			
      , M.COST_NAME  =  S.COST_NAME			
       , M.UPDATE_DATE  = GETDATE()
       , M.UPDATE_BY  = @UserID
FROM TB_M_ORGANIZATION M INNER JOIN TB_S_ORGANIZATION S ON M.ORG_CODE=S.ORG_CODE


---- Case Insert
INSERT INTO TB_M_ORGANIZATION
           (ORG_CODE  ,COMP_CODE  ,GRP_CODE  ,GRP_NAME       ,DIV_CODE
           ,DIV_NAME           ,DEPT_CODE           ,DEPT_NAME           ,SEC_CODE
           ,SEC_NAME           ,LINE_CODE           ,LINE_NAME           ,SUBUNIT_CODE
           ,SUBUNIT_NAME           ,SHIFT_CODE           ,COST_CODE           ,COST_NAME
           ,CREATE_DATE           ,CREATE_BY           ,UPDATE_DATE           ,UPDATE_BY)
SELECT     ORG_CODE  ,COMP_CODE  ,GRP_CODE  ,GRP_NAME       ,DIV_CODE
           ,DIV_NAME,DEPT_CODE           ,DEPT_NAME           ,SEC_CODE
           ,SEC_NAME           ,LINE_CODE           ,LINE_NAME           ,SUBUNIT_CODE
           ,SUBUNIT_NAME           ,SHIFT_CODE           ,COST_CODE           ,COST_NAME
          , GETDATE()       , @UserID       ,  GETDATE()     , @UserID
FROM TB_S_ORGANIZATION S
WHERE NOT EXISTS (SELECT 1 
				  FROM TB_M_ORGANIZATION M
				  WHERE M.ORG_CODE=S.ORG_CODE)


	RETURN @Error ;
END TRY
BEGIN CATCH

			PRINT concat('ERROR Message:',ERROR_MESSAGE())
			   SET @ERROR_MESSAGE=ERROR_MESSAGE();
				RETURN 1;
END CATCH









GO
