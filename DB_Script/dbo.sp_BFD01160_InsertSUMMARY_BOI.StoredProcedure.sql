DROP PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_BOI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertSUMMARY_BOI]
@User	VARCHAR(8)
AS
BEGIN
	DECLARE @ID int,	
		@TO varchar(1000),
		@CC varchar(1000),
		@BCC varchar(1000),
		@TITLE varchar(255),
		@MESSAGE nvarchar(max),
		@FOOTER varchar(250),
		@TYPE varchar(1),
		@START_PERIOD datetime,
		@END_PERIOD datetime,
		@SEND_FLAG varchar(1),
		@RESULT_FLAG varchar(1),
		@REF_FUNC varchar(8),
		@REF_DOC_NO T_DOC_NO

		SET @MESSAGE = 'Dear BOI Team';
		SET @MESSAGE = CONCAT(@MESSAGE,('<style>
				table {
				  font-family: arial, sans-serif;
				  border-collapse: collapse;
				  width: 100%;
				}

				td, th {
				  border: 1px solid #dddddd;
				  text-align: left;
				  padding: 8px;
				}

				tr:nth-child(even) {
				  background-color: #dddddd;
				}
				</style>

				<table>
				  <tr>
					<th>Tax Privilege</th>
					<th>Asset Class</th>
					<th>Minor Category</th>
					<th>QTY</th>
				  </tr>'));

		
	DECLARE @StartDate datetime;
	DECLARE @EndDate datetime;
	select @StartDate = DATEADD(day, -1, [value]),@EndDate = [value]
	From TB_M_SYSTEM																
	Where CATEGORY = 'NOTIFICATION'																
		 AND SUB_CATEGORY = 'SUMMARY_BOI'															
		 AND CODE = 'NEXTTIME'	

	DECLARE @VALUE1 varchar(400),@VALUE2 varchar(400),@VALUE3 varchar(400),@QTY int;
	DECLARE cursor_results CURSOR FOR 
	select m_BOI.VALUE,m_AC.VALUE,m_MC.VALUE,count(rad.ASSET_NO)
	from TB_R_REQUEST_ASSET_D rad
	left outer join TB_M_SYSTEM m_BOI on rad.BOI_NO = m_BOI.CODE
	left outer join TB_M_SYSTEM m_AC on rad.ASSET_CLASS = m_AC.CODE
	left outer join TB_M_SYSTEM m_MC on rad.MINOR_CATEGORY = m_MC.CODE
	where rad.ASSET_NO IN (
			select h.ASSET_NO
			from TB_R_ASSET_HIS h
			where h.SAP_SENT_DATE between @StartDate and @EndDate
		)
		and rad.INVEST_REASON = 'B'
	group by m_BOI.VALUE,m_AC.VALUE,m_MC.VALUE 

	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @VALUE1,@VALUE2,@VALUE3,@QTY
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		SET @MESSAGE = CONCAT(@MESSAGE,'<tr>');
		SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@VALUE1,'</td>');	
		SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@VALUE2,'</td>');	
		SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@VALUE3,'</td>');	
		SET @MESSAGE = CONCAT(@MESSAGE,'<td>',@QTY,'</td>');	
		SET @MESSAGE = CONCAT(@MESSAGE,'</tr>');

			--insert into @MyTable select @VALUE,@TDEM,@TMT,@STM,@Total
	FETCH NEXT FROM cursor_results into @VALUE1,@VALUE2,@VALUE3,@QTY
	END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;



	SET @ID = (select isnull(max(ID),0) + 1 from TB_R_NOTIFICATION);
	SET @MESSAGE = CONCAT(@MESSAGE,'</table>');

	--select * from @MyTable
	SET @FOOTER = 'Best Regards,<br>System';

	SET @START_PERIOD = @StartDate;
	SET @END_PERIOD = @EndDate;

	SET @SEND_FLAG = 'N';
	SET @RESULT_FLAG = 'N';
	SET @TYPE = 'I';
	SET @REF_FUNC = '';

	DECLARE @EMAIL varchar(50),@COMPANY T_COMPANY;
	DECLARE cursor_results CURSOR FOR 
	select e.EMAIL,e.COMPANY 
	from TB_M_EMPLOYEE e
	left join TB_M_SP_ROLE r on e.EMP_CODE = r.EMP_CODE
	where r.SP_ROLE= 'BOI' 


	OPEN cursor_results
	FETCH NEXT FROM cursor_results into @EMAIL,@COMPANY
	WHILE @@FETCH_STATUS = 0
	BEGIN 

	SET @TO = @EMAIL;
	SET @TITLE = CONCAT('TFAST : ',@COMPANY,' Summary Daily BOI Asset Creation');
	INSERT INTO TB_R_NOTIFICATION
           (ID,[TO],CC,BCC,TITLE,[MESSAGE],FOOTER,[TYPE]
           ,START_PERIOD,END_PERIOD,SEND_FLAG,RESULT_FLAG,REF_FUNC,REF_DOC_NO
			,CREATE_DATE,CREATE_BY,UPDATE_DATE,UPDATE_BY)
     VALUES
           (@ID
		   ,@TO
           ,@CC
           ,@BCC
           ,@TITLE
           ,@MESSAGE
           ,@FOOTER
           ,@TYPE
           ,@START_PERIOD
           ,@END_PERIOD
           ,@SEND_FLAG
           ,@RESULT_FLAG
           ,@REF_FUNC
           ,@REF_DOC_NO
           ,getdate()
           ,@User
           ,getdate()
           ,@User)

	--select @TO,@CC,@BCC,@TITLE,@MESSAGE,@FOOTER,@TYPE,@START_PERIOD,@END_PERIOD,@SEND_FLAG,@RESULT_FLAG,@REF_FUNC,@REF_DOC_NO,getdate(),@User,getdate(),@User

	FETCH NEXT FROM cursor_results into @EMAIL,@COMPANY
	END

	CLOSE cursor_results;
	DEALLOCATE cursor_results;
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'SUMMARY_BOI'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	SET @dNextTime = DATEADD(DAY, 1, @dNextTime)

	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= 'System'
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
	--select @MESSAGE
END
GO
