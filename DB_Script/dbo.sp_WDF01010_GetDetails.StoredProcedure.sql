DROP PROCEDURE [dbo].[sp_WDF01010_GetDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WDF01010_GetDetails]
(
	------Parameter Screen--------
	 @APP_ID	INT
	,@LEVEL		VARCHAR(1) = NULL

	-----Parameter DataGrid--------
	,@CURRENT_PAGE	INT
	,@PAGE_SIZE		INT
	,@ORDER_BY	    NVARCHAR(50)
	,@ORDER_TYPE	VARCHAR(5) = 'desc'
	,@TOTAL_ITEM	INT OUTPUT
)
AS
BEGIN
	DECLARE @FROM INT, @TO	INT
	SET @FROM = (@PAGE_SIZE * (@CURRENT_PAGE-1)) + 1;
	SET @TO = @PAGE_SIZE * (@CURRENT_PAGE);
    
  DECLARE @strQuery NVARCHAR(3000);
  DECLARE @sortingInfo VARCHAR(500);
  
  IF @ORDER_BY = '1'
  	SET @sortingInfo = 'CREATE_DATE ' + @ORDER_TYPE + ', LOG_ID ';
  ELSE IF @ORDER_BY = '2'
  	SET @sortingInfo = 'STATUS ' + @ORDER_TYPE + ', LOG_ID ';
  ELSE IF @ORDER_BY = '3'
  	SET @sortingInfo = 'LEVEL ' + @ORDER_TYPE + ', LOG_ID ';
  ELSE IF @ORDER_BY = '4'
  	SET @sortingInfo = 'MESSAGE ' + @ORDER_TYPE + ', LOG_ID ';
  ELSE
  	SET @sortingInfo = 'LOG_ID ';
    
  SET @strQuery =
  'SELECT * FROM
  	(SELECT ROW_NUMBER() OVER (ORDER BY ' + @sortingInfo + ') AS ROW_NUMER,
          dbo.fn_dateTimeFAS(L.CREATE_DATE) AS LOGGING_TIME,
          (SELECT VALUE 
                FROM	TB_M_SYSTEM S WITH(NOLOCK)
                WHERE	S.CODE = ISNULL(TEMP.STATUS,L.STATUS)
                    AND UPPER(S.CATEGORY) = ''COMMON'' 
                    AND UPPER(S.SUB_CATEGORY) = ''STATUS_LOGGING'') "STATUS_DESC",
          (SELECT VALUE 
                              FROM	TB_M_SYSTEM S WITH(NOLOCK)
                              WHERE	S.CODE = L.LEVEL
                                  AND UPPER(S.CATEGORY) = ''COMMON'' 
                                  AND UPPER(S.SUB_CATEGORY) = ''LEVEL_LOGGING'') "LEVEL_DESC",
          L.MESSAGE
  	 FROM TB_L_LOGGING L WITH(NOLOCK)
	 LEFT JOIN 
	 (
		 SELECT		 
				 MAX(DL.LOG_ID) AS LATEST_LOG_ID,
				 CASE MAX(CASE WHEN LEVEL = ''E'' THEN ''90'' ELSE CASE WHEN LEVEL = ''W'' THEN ''80'' ELSE ''70'' END END) WHEN ''90'' THEN ''E'' WHEN ''80'' THEN ''W'' ELSE ''S'' END AS STATUS
		  FROM TB_L_LOGGING DL WITH(NOLOCK)
		  WHERE DISPLAY_FLAG=''Y'' AND APP_ID = ''' + CAST(@APP_ID AS NVARCHAR(10)) + '''
		  GROUP BY DL.APP_ID
	 ) TEMP ON L.LOG_ID = TEMP.LATEST_LOG_ID
     WHERE L.APP_ID = ''' + CAST(@APP_ID AS NVARCHAR(10)) + '''';
     IF @LEVEL IS NOT NULL
     	SET @strQuery = @strQuery + ' AND L.LEVEL = ''' + @LEVEL + '''';
        
  	SET @strQuery = @strQuery + ') T
  WHERE ROW_NUMER >= '+CAST(@FROM as NVARCHAR(15))+' and ROW_NUMER <= '+CAST(@TO as NVARCHAR(15));
  
  --SET @strQuery = @strQuery + @sortingInfo;
  PRINT @strQuery                 
  EXECUTE sp_executesql @strQuery
  
  --SET		@TOTAL_ITEM = @@ROWCOUNT
  

  SELECT @TOTAL_ITEM  = COUNT(1)
  FROM (  SELECT APP_ID
		  FROM TB_L_LOGGING L WITH(NOLOCK)
		  WHERE  L.APP_ID = CAST(@APP_ID AS NVARCHAR(10))   AND  
		   ISNULL(L.LEVEL,'') = ISNULL(@LEVEL,ISNULL(L.LEVEL,''))
		  )S



END

GO
