DROP PROCEDURE [dbo].[sp_WFD01150_GetTmpDivH_EDIT_MAIL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_GetTmpDivH_EDIT_MAIL]
(
	@APPRV_ID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @MyTableVar table(  
		APPRV_ID int,
		SEQ varchar(3),
		[ROLE] varchar(3),
		DIVISION varchar(6),
		GRP varchar(8),
		CONDITION varchar(400),
		REQ varchar(3),
		REJ varchar(3),

		OPERATOR Varchar(400),																																																								
		VALUE1 int,			
		VALUE2 int,				
		ALLOWTOREJECT varchar(3),																																																										
		ALLOWTODELETE varchar(3),
		LEADTIME int,
		ALLOWTOSELECT varchar(3),
		FINISHFLOW varchar(3),
		GENFILE varchar(3),
		REJECT_TO  varchar(3),
		HIGHERAPPROVE  varchar(3),
		NOTIFICATIONBYEMAIL   varchar(3),
		OPERATION   varchar(3),
		Isinsert varchar(1)
	); 

	DECLARE @ROLE_01 varchar(3),@Division_01 varchar(6),@Grp_01 varchar(8),@Condition_01 varchar(400),@Req_01 varchar(3),@Rej_01 varchar(3)
		,@Operator_01 Varchar(400),@Value1_01 int,@Value2_01 int,@Allow_Reject_01 varchar(3),@Allow_Del_Item_01 varchar(3),@Lead_time_01 int,@Allow_Sel_Apprv_01 varchar(3)
		,@Finish_Flow_01 varchar(3),@Generate_File_01 varchar(3),@Reject_to_01 varchar(3),@Higher_Appr_01 varchar(3),@Noti_by_Email_01  varchar(3),@Operation_01  varchar(3)
	DECLARE @ROLE_02 varchar(3),@Division_02 varchar(6),@Grp_02 varchar(8),@Condition_02 varchar(400),@Req_02 varchar(3),@Rej_02 varchar(3)
		,@Operator_02 Varchar(400),@Value1_02 int,@Value2_02 int,@Allow_Reject_02 varchar(3),@Allow_Del_Item_02 varchar(3),@Lead_time_02 int,@Allow_Sel_Apprv_02 varchar(3)
		,@Finish_Flow_02 varchar(3),@Generate_File_02 varchar(3),@Reject_to_02 varchar(3),@Higher_Appr_02 varchar(3),@Noti_by_Email_02  varchar(3),@Operation_02  varchar(3)
	DECLARE @ROLE_20 varchar(3),@Division_20 varchar(6),@Grp_20 varchar(8),@Condition_20 varchar(400),@Req_20 varchar(3),@Rej_20 varchar(3)
		,@Operator_20 Varchar(400),@Value1_20 int,@Value2_20 int,@Allow_Reject_20 varchar(3),@Allow_Del_Item_20 varchar(3),@Lead_time_20 int,@Allow_Sel_Apprv_20 varchar(3)
		,@Finish_Flow_20 varchar(3),@Generate_File_20 varchar(3),@Reject_to_20 varchar(3),@Higher_Appr_20 varchar(3),@Noti_by_Email_20  varchar(3),@Operation_20  varchar(3)
	DECLARE @ROLE_30 varchar(3),@Division_30 varchar(6),@Grp_30 varchar(8),@Condition_30 varchar(400),@Req_30 varchar(3),@Rej_30 varchar(3)
		,@Operator_30 Varchar(400),@Value1_30 int,@Value2_30 int,@Allow_Reject_30 varchar(3),@Allow_Del_Item_30 varchar(3),@Lead_time_30 int,@Allow_Sel_Apprv_30 varchar(3)
		,@Finish_Flow_30 varchar(3),@Generate_File_30 varchar(3),@Reject_to_30 varchar(3),@Higher_Appr_30 varchar(3),@Noti_by_Email_30  varchar(3),@Operation_30  varchar(3)
	DECLARE @ROLE_40 varchar(3),@Division_40 varchar(6),@Grp_40 varchar(8),@Condition_40 varchar(400),@Req_40 varchar(3),@Rej_40 varchar(3)
		,@Operator_40 Varchar(400),@Value1_40 int,@Value2_40 int,@Allow_Reject_40 varchar(3),@Allow_Del_Item_40 varchar(3),@Lead_time_40 int,@Allow_Sel_Apprv_40 varchar(3)
		,@Finish_Flow_40 varchar(3),@Generate_File_40 varchar(3),@Reject_to_40 varchar(3),@Higher_Appr_40 varchar(3),@Noti_by_Email_40  varchar(3),@Operation_40  varchar(3)
	DECLARE @ROLE_41 varchar(3),@Division_41 varchar(6),@Grp_41 varchar(8),@Condition_41 varchar(400),@Req_41 varchar(3),@Rej_41 varchar(3)
		,@Operator_41 Varchar(400),@Value1_41 int,@Value2_41 int,@Allow_Reject_41 varchar(3),@Allow_Del_Item_41 varchar(3),@Lead_time_41 int,@Allow_Sel_Apprv_41 varchar(3)
		,@Finish_Flow_41 varchar(3),@Generate_File_41 varchar(3),@Reject_to_41 varchar(3),@Higher_Appr_41 varchar(3),@Noti_by_Email_41  varchar(3),@Operation_41  varchar(3)
	DECLARE @ROLE_50 varchar(3),@Division_50 varchar(6),@Grp_50 varchar(8),@Condition_50 varchar(400),@Req_50 varchar(3),@Rej_50 varchar(3)
		,@Operator_50 Varchar(400),@Value1_50 int,@Value2_50 int,@Allow_Reject_50 varchar(3),@Allow_Del_Item_50 varchar(3),@Lead_time_50 int,@Allow_Sel_Apprv_50 varchar(3)
		,@Finish_Flow_50 varchar(3),@Generate_File_50 varchar(3),@Reject_to_50 varchar(3),@Higher_Appr_50 varchar(3),@Noti_by_Email_50  varchar(3),@Operation_50  varchar(3)
	DECLARE @ROLE_70 varchar(3),@Division_70 varchar(6),@Grp_70 varchar(8),@Condition_70 varchar(400),@Req_70 varchar(3),@Rej_70 varchar(3)
		,@Operator_70 Varchar(400),@Value1_70 int,@Value2_70 int,@Allow_Reject_70 varchar(3),@Allow_Del_Item_70 varchar(3),@Lead_time_70 int,@Allow_Sel_Apprv_70 varchar(3)
		,@Finish_Flow_70 varchar(3),@Generate_File_70 varchar(3),@Reject_to_70 varchar(3),@Higher_Appr_70 varchar(3),@Noti_by_Email_70  varchar(3),@Operation_70  varchar(3)
	DECLARE @ROLE_71 varchar(3),@Division_71 varchar(6),@Grp_71 varchar(8),@Condition_71 varchar(400),@Req_71 varchar(3),@Rej_71 varchar(3)
		,@Operator_71 Varchar(400),@Value1_71 int,@Value2_71 int,@Allow_Reject_71 varchar(3),@Allow_Del_Item_71 varchar(3),@Lead_time_71 int,@Allow_Sel_Apprv_71 varchar(3)
		,@Finish_Flow_71 varchar(3),@Generate_File_71 varchar(3),@Reject_to_71 varchar(3),@Higher_Appr_71 varchar(3),@Noti_by_Email_71  varchar(3),@Operation_71  varchar(3)
	
	SELECT @ROLE_01=[ROLE],@Division_01=DIVISION,@Grp_01=APPRV_GROUP,@Condition_01=CONDITION_CODE,@Req_01=REQUIRE_FLAG,@Rej_01=Allow_Reject
		,@Operator_01=Operator,@Value1_01=Value1,@Value2_01=Value2,@Allow_Reject_01=Allow_Reject,@Allow_Del_Item_01=Allow_Del_Item,@Lead_time_01=Lead_time,@Allow_Sel_Apprv_01=Allow_Sel_Apprv
		,@Finish_Flow_01=Finish_Flow,@Generate_File_01=Generate_File,@Reject_to_01 = REJECT_TO,@Higher_Appr_01=Higher_Appr,@Noti_by_Email_01=Noti_by_Email,@Operation_01=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =1

	SELECT @ROLE_02=[ROLE],@Division_02=DIVISION,@Grp_02=APPRV_GROUP,@Condition_02=CONDITION_CODE,@Req_02=REQUIRE_FLAG,@Rej_02=Allow_Reject
		,@Operator_02=Operator,@Value1_02=Value1,@Value2_02=Value2,@Allow_Reject_02=Allow_Reject,@Allow_Del_Item_02=Allow_Del_Item,@Lead_time_02=Lead_time,@Allow_Sel_Apprv_02=Allow_Sel_Apprv
		,@Finish_Flow_02=Finish_Flow,@Generate_File_02=Generate_File,@Reject_to_02 = REJECT_TO,@Higher_Appr_02=Higher_Appr,@Noti_by_Email_02=Noti_by_Email,@Operation_02=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =2

	SELECT @ROLE_20=[ROLE],@Division_20=DIVISION,@Grp_20=APPRV_GROUP,@Condition_20=CONDITION_CODE,@Req_20=REQUIRE_FLAG,@Rej_20=Allow_Reject
		,@Operator_20=Operator,@Value1_20=Value1,@Value2_20=Value2,@Allow_Reject_20=Allow_Reject,@Allow_Del_Item_20=Allow_Del_Item,@Lead_time_20=Lead_time,@Allow_Sel_Apprv_20=Allow_Sel_Apprv
		,@Finish_Flow_20=Finish_Flow,@Generate_File_20=Generate_File,@Reject_to_20 = REJECT_TO,@Higher_Appr_20=Higher_Appr,@Noti_by_Email_20=Noti_by_Email,@Operation_20=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =20

	SELECT @ROLE_30=[ROLE],@Division_30=DIVISION,@Grp_30=APPRV_GROUP,@Condition_30=CONDITION_CODE,@Req_30=REQUIRE_FLAG,@Rej_30=Allow_Reject
		,@Operator_30=Operator,@Value1_30=Value1,@Value2_30=Value2,@Allow_Reject_30=Allow_Reject,@Allow_Del_Item_30=Allow_Del_Item,@Lead_time_30=Lead_time,@Allow_Sel_Apprv_30=Allow_Sel_Apprv
		,@Finish_Flow_30=Finish_Flow,@Generate_File_30=Generate_File,@Reject_to_30 = REJECT_TO,@Higher_Appr_30=Higher_Appr,@Noti_by_Email_30=Noti_by_Email,@Operation_30=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =30

	SELECT @ROLE_40=[ROLE],@Division_40=DIVISION,@Grp_40=APPRV_GROUP,@Condition_40=CONDITION_CODE,@Req_40=REQUIRE_FLAG,@Rej_40=Allow_Reject
		,@Operator_40=Operator,@Value1_40=Value1,@Value2_40=Value2,@Allow_Reject_40=Allow_Reject,@Allow_Del_Item_40=Allow_Del_Item,@Lead_time_40=Lead_time,@Allow_Sel_Apprv_40=Allow_Sel_Apprv
		,@Finish_Flow_40=Finish_Flow,@Generate_File_40=Generate_File,@Reject_to_40 = REJECT_TO,@Higher_Appr_40=Higher_Appr,@Noti_by_Email_40=Noti_by_Email,@Operation_40=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =40

	SELECT @ROLE_41=[ROLE],@Division_41=DIVISION,@Grp_41=APPRV_GROUP,@Condition_41=CONDITION_CODE,@Req_41=REQUIRE_FLAG,@Rej_41=Allow_Reject
		,@Operator_41=Operator,@Value1_41=Value1,@Value2_41=Value2,@Allow_Reject_41=Allow_Reject,@Allow_Del_Item_41=Allow_Del_Item,@Lead_time_41=Lead_time,@Allow_Sel_Apprv_41=Allow_Sel_Apprv
		,@Finish_Flow_41=Finish_Flow,@Generate_File_41=Generate_File,@Reject_to_41 = REJECT_TO,@Higher_Appr_41=Higher_Appr,@Noti_by_Email_41=Noti_by_Email,@Operation_41=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =41

	SELECT @ROLE_50=[ROLE],@Division_50=DIVISION,@Grp_50=APPRV_GROUP,@Condition_50=CONDITION_CODE,@Req_50=REQUIRE_FLAG,@Rej_50=Allow_Reject
		,@Operator_50=Operator,@Value1_50=Value1,@Value2_50=Value2,@Allow_Reject_50=Allow_Reject,@Allow_Del_Item_50=Allow_Del_Item,@Lead_time_50=Lead_time,@Allow_Sel_Apprv_50=Allow_Sel_Apprv
		,@Finish_Flow_50=Finish_Flow,@Generate_File_50=Generate_File,@Reject_to_50 = REJECT_TO,@Higher_Appr_50=Higher_Appr,@Noti_by_Email_50=Noti_by_Email,@Operation_50=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =50

	SELECT @ROLE_70=[ROLE],@Division_70=DIVISION,@Grp_70=APPRV_GROUP,@Condition_70=CONDITION_CODE,@Req_70=REQUIRE_FLAG,@Rej_70=Allow_Reject
		,@Operator_70=Operator,@Value1_70=Value1,@Value2_70=Value2,@Allow_Reject_70=Allow_Reject,@Allow_Del_Item_70=Allow_Del_Item,@Lead_time_70=Lead_time,@Allow_Sel_Apprv_70=Allow_Sel_Apprv
		,@Finish_Flow_70=Finish_Flow,@Generate_File_70=Generate_File,@Reject_to_70 = REJECT_TO,@Higher_Appr_70=Higher_Appr,@Noti_by_Email_70=Noti_by_Email,@Operation_70=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =70

	SELECT @ROLE_71=[ROLE],@Division_71=DIVISION,@Grp_71=APPRV_GROUP,@Condition_71=CONDITION_CODE,@Req_71=REQUIRE_FLAG,@Rej_71=Allow_Reject
		,@Operator_71=Operator,@Value1_71=Value1,@Value2_71=Value2,@Allow_Reject_71=Allow_Reject,@Allow_Del_Item_71=Allow_Del_Item,@Lead_time_71=Lead_time,@Allow_Sel_Apprv_71=Allow_Sel_Apprv
		,@Finish_Flow_71=Finish_Flow,@Generate_File_71=Generate_File,@Reject_to_71 = REJECT_TO,@Higher_Appr_71=Higher_Appr,@Noti_by_Email_71=Noti_by_Email,@Operation_71=Operation
	FROM TB_M_APPROVE_D 
	WHERE APPRV_ID = @APPRV_ID and INDX =71

	IF(@ROLE_01 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'01',@ROLE_01,@Division_01,@Grp_01,@Condition_01,@Req_01,@Rej_01
			,@Operator_01,@Value1_01,@Value2_01,@Allow_Reject_01,@Allow_Del_Item_01,@Lead_time_01,@Allow_Sel_Apprv_01,@Finish_Flow_01,@Generate_File_01,@Reject_to_01,@Higher_Appr_01,@Noti_by_Email_01,@Operation_01,'N'
		END
	
	IF(@ROLE_02 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'02',@ROLE_02,@Division_02,@Grp_02,@Condition_02,@Req_02,@Rej_02
			,@Operator_02,@Value1_02,@Value2_02,@Allow_Reject_02,@Allow_Del_Item_02,@Lead_time_02,@Allow_Sel_Apprv_02,@Finish_Flow_02,@Generate_File_02,@Reject_to_02,@Higher_Appr_02,@Noti_by_Email_02,@Operation_02,'N'
		END

	IF(@ROLE_20 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'20',@ROLE_20,@Division_20,@Grp_20,@Condition_20,@Req_20,@Rej_20
			,@Operator_20,@Value1_20,@Value2_20,@Allow_Reject_20,@Allow_Del_Item_20,@Lead_time_20,@Allow_Sel_Apprv_20,@Finish_Flow_20,@Generate_File_20,@Reject_to_20,@Higher_Appr_20,@Noti_by_Email_20,@Operation_20,'N'
		END	
	
	IF(@ROLE_30 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'30',@ROLE_30,@Division_30,@Grp_30,@Condition_30,@Req_30,@Rej_30
			,@Operator_30,@Value1_30,@Value2_30,@Allow_Reject_30,@Allow_Del_Item_30,@Lead_time_30,@Allow_Sel_Apprv_30,@Finish_Flow_30,@Generate_File_30,@Reject_to_30,@Higher_Appr_30,@Noti_by_Email_30,@Operation_30,'N'
		END
	
	IF(@ROLE_40 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'40',@ROLE_40,@Division_40,@Grp_40,@Condition_40,@Req_40,@Rej_40
			,@Operator_40,@Value1_40,@Value2_40,@Allow_Reject_40,@Allow_Del_Item_40,@Lead_time_40,@Allow_Sel_Apprv_40,@Finish_Flow_40,@Generate_File_40,@Reject_to_40,@Higher_Appr_40,@Noti_by_Email_40,@Operation_40,'N'
		END
		
	IF(@ROLE_41 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'41',@ROLE_41,@Division_41,@Grp_41,@Condition_41,@Req_41,@Rej_41
			,@Operator_41,@Value1_41,@Value2_41,@Allow_Reject_41,@Allow_Del_Item_41,@Lead_time_41,@Allow_Sel_Apprv_41,@Finish_Flow_41,@Generate_File_41,@Reject_to_41,@Higher_Appr_41,@Noti_by_Email_41,@Operation_41,'N'
		END
		
	IF(@ROLE_50 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'50',@ROLE_50,@Division_50,@Grp_50,@Condition_50,@Req_50,@Rej_50
			,@Operator_50,@Value1_50,@Value2_50,@Allow_Reject_50,@Allow_Del_Item_50,@Lead_time_50,@Allow_Sel_Apprv_50,@Finish_Flow_50,@Generate_File_50,@Reject_to_50,@Higher_Appr_50,@Noti_by_Email_50,@Operation_50,'N'
		END
	
	IF(@ROLE_70 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'70',@ROLE_70,@Division_70,@Grp_70,@Condition_70,@Req_70,@Rej_70
			,@Operator_70,@Value1_70,@Value2_70,@Allow_Reject_70,@Allow_Del_Item_70,@Lead_time_70,@Allow_Sel_Apprv_70,@Finish_Flow_70,@Generate_File_70,@Reject_to_70,@Higher_Appr_70,@Noti_by_Email_70,@Operation_70,'N'
		END
	
	IF(@ROLE_71 is not null)
		BEGIN
			INSERT INTO @MyTableVar select 	@APPRV_ID,'71',@ROLE_71,@Division_71,@Grp_71,@Condition_71,@Req_71,@Rej_71
			,@Operator_71,@Value1_71,@Value2_71,@Allow_Reject_71,@Allow_Del_Item_71,@Lead_time_71,@Allow_Sel_Apprv_71,@Finish_Flow_71,@Generate_File_71,@Reject_to_71,@Higher_Appr_71,@Noti_by_Email_71,@Operation_71,'N'
		END
		
	select * from @MyTableVar
END
GO
