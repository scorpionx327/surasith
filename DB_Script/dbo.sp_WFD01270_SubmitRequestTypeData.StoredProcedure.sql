DROP PROCEDURE [dbo].[sp_WFD01270_SubmitRequestTypeData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 09/02/2017
-- Description:	Insert Fixed assets screen
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01270_SubmitRequestTypeData]
(
	@GUID				    VARCHAR(50)			= NULL,		--Sub Type	
	@REQUEST_TYPE			VARCHAR(1)		= NULL,		--Cost Code	
	@RunningType			VARCHAR(1),
	@TempDocNo				VARCHAR(10)			= NULL		--Cost Code	

)
AS
BEGIN
		
		IF NOT EXISTS(SELECT 1 FROM #TB_T_SELECTED_ASSETS)
			RETURN


		INSERT
		INTO	#TB_T_DOC
		SELECT	0, NULL, T.ASSET_CLASS, NULL, 'N' 
		FROM	#TB_T_SELECTED_ASSETS  T
		GROUP	BY
				T.ASSET_CLASS
		
		DECLARE @Company	T_COMPANY
		SELECT	@Company = COMPANY
		FROM	TB_T_REQUEST_H
		WHERE	DOC_NO = @TempDocNo

		EXEC [sp_WFD01270_GenerateDocNo] @Company, @GUID, @REQUEST_TYPE
	
		INSERT INTO TB_R_REQUEST_H 
		(
			COMPANY,		DOC_NO,		REQUEST_DATE,	REQUEST_TYPE,	APPROVE_FLOW_TYPE,
			EMP_CODE,		EMP_TITLE,	EMP_NAME,		EMP_LASTNAME,	COST_CODE,
			POS_CODE,		POS_NAME,	DEPT_CODE,		DEPT_NAME,		SECT_CODE,
			SECT_NAME,		PHONE_NO,	[STATUS],		[DESC],			ASSET_COST_CODE,
			[GUID],
			CREATE_DATE,	CREATE_BY,		UPDATE_DATE,		UPDATE_BY
		)
		SELECT
			H.COMPANY,		D.DOC_NO,			H.REQUEST_DATE,	H.REQUEST_TYPE,		H.APPROVE_FLOW_TYPE,
			H.EMP_CODE,		H.EMP_TITLE,	H.EMP_NAME,		H.EMP_LASTNAME,		H.COST_CODE,
			H.POS_CODE,		H.POS_NAME,		H.DEPT_CODE,	H.DEPT_NAME,		H.SECT_CODE,
			H.SECT_NAME,	H.PHONE_NO,		H.[STATUS],		H.[DESC],			H.ASSET_COST_CODE,
			H.[GUID],
			H.CREATE_DATE,	H.CREATE_BY,		H.UPDATE_DATE,		H.UPDATE_BY
		FROM 
			(SELECT DISTINCT DOC_NO FROM #TB_T_DOC) D
		CROSS JOIN
			TB_T_REQUEST_H H WITH (NOLOCK)
		WHERE	H.DOC_NO = @TempDocNo

		
		IF OBJECT_ID('tempdb..#TB_R_REQUEST_H') IS NOT NULL
			DROP TABLE #TB_R_REQUEST_H
		-- Insert Approver of each Document
	
		EXEC [sp_WFD01270_BreakDownApproverToDocNo] @TempDocNo, @RunningType
		
		
		UPDATE	A
		SET		A.APPR_STATUS	= 'W',
				A.APPR_DATE		= GETDATE()
		FROM	TB_R_REQUEST_APPR A
		INNER	JOIN
		(
			-- Get First Person
			SELECT	ROW_NUMBER() OVER(PARTITION BY A.DOC_NO ORDER BY A.INDX) SEQ, A.DOC_NO, A.INDX
			FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)
			INNER	JOIN
					#TB_T_DOC D
			ON		A.DOC_NO = D.DOC_NO
			WHERE	A.APPR_STATUS = 'P'
		) B
		ON		B.SEQ		= 1 AND
				B.DOC_NO	= A.DOC_NO AND
				B.INDX		= A.INDX
	
		-- Delete not selected approver
		DELETE FROM TB_R_REQUEST_APPR
		WHERE GUID			= @GUID
		AND EMP_CODE IS NULL 
		AND APPR_STATUS NOT IN ('A','W') -- Can remove when screen control correctly
	
		
END
GO
