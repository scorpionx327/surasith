DROP PROCEDURE [dbo].[sp_WFD01210_GetIn_Charge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 10/03/2017
-- Description:	get sv cost save emp
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetIn_Charge]
	@EMP_CODE					T_SYS_USER,
	@IS_RESPONSE_COST_CENTER	varchar(1)
AS
BEGIN
	SET NOCOUNT ON;

	IF(ISNULL(@IS_RESPONSE_COST_CENTER,'') <> '' AND @IS_RESPONSE_COST_CENTER = 'Y')
	BEGIN 
		SELECT	FA.RES_COST_CODE AS COST_CODE,
				C.COST_NAME,
				NULL PLANT_CD,
				NULL PLANT_NAME 
		FROM	TB_T_FASUP_COST_CENTER FA
		LEFT	JOIN 
				TB_M_COST_CENTER C 
		ON		C.COST_CODE = FA.RES_COST_CODE
		WHERE	FA.EMP_CODE	= @EMP_CODE
		GROUP	BY 
				FA.RES_COST_CODE,
				C.COST_NAME
	END
	ELSE
	BEGIN 
		SELECT	SV.COST_CODE,
				C.COST_NAME
		FROM	TB_T_SV_COST_CENTER SV
		LEFT	JOIN 
				TB_M_COST_CENTER C 
		ON		C.COST_CODE = SV.COST_CODE
		WHERE	SV.EMP_CODE	= @EMP_CODE
		GROUP	BY 
				SV.COST_CODE,
				C.COST_NAME
	END
END



GO
