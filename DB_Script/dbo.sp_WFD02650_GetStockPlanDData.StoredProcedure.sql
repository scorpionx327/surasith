DROP PROCEDURE [dbo].[sp_WFD02650_GetStockPlanDData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Get stock take plan d datas
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_GetStockPlanDData]
	-- Add the parameters for the stored procedure here
	@SV_EMP_CODES VARCHAR(MAX)
	, @STOCK_TAKE_KEY VARCHAR(7)
	, @IS_FAADMIN varchar(1)
AS
BEGIN
	SET NOCOUNT ON;

	select
		*
	from TB_R_STOCK_TAKE_D d
	where STOCK_TAKE_KEY=@STOCK_TAKE_KEY  --and d.CHECK_STATUS <> 'Y'
		and (d.SV_EMP_CODE in (SELECT value from STRING_SPLIT (@SV_EMP_CODES, ',') )
				or @IS_FAADMIN = 'Y')

END





GO
