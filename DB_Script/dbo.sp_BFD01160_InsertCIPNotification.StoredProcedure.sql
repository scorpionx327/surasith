DROP PROCEDURE [dbo].[sp_BFD01160_InsertCIPNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertCIPNotification]
@User	VARCHAR(8)
AS
BEGIN
	
	DECLARE @Category VARCHAR(40) = 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40) = 'CIP'
	DECLARE @NextTimeCode VARCHAR(40) = 'NEXTTIME'
	--------------------------------------------------------------------------------------------------------
	-- Every 25th, Get Next Period
	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	IF(@dNextTime > GETDATE())
		RETURN
	--------------------------------------------------------------------------------------------------------	
	-- Get List of Asset 
	SELECT	H.ASSET_NO, H.ASSET_NAME, D.COST_CODE, CC.COST_NAME
	INTO	#TB_T_ASSET
	FROM	TB_M_ASSETS_H H
	INNER	JOIN
			TB_M_ASSETS_D D
	ON		H.ASSET_NO = D.ASSET_NO	
	INNER	JOIN
			TB_M_COST_CENTER CC
	ON		D.COST_CODE = CC.COST_CODE
	WHERE	H.[STATUS] = 'Y' AND
			H.ASSET_TYPE = 'CIP' AND
			H.COST >= H.PLAN_COST
	GROUP	BY
			H.ASSET_NO, H.ASSET_NAME, D.COST_CODE, CC.COST_NAME

	IF @@ROWCOUNT = 0
		GOTO SETNEXT
	
	--------------------------------------------------------------------------------------------------------
	-- Sending a Email

	SELECT	T.COST_CODE, 'N' AS FLAG
	INTO	#TB_T_COST_CODE
	FROM	#TB_T_ASSET T
	GROUP	BY
			T.COST_CODE

	DECLARE @CC VARCHAR(8), @BodyTemplate VARCHAR(400), @SubjectTemplate VARCHAR(400)

	SET @SubjectTemplate	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD02160_CIP')
	SET @BodyTemplate		= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD02160_CIP')


	DECLARE @CCMail VARCHAR(1000)
	SET @CCMail = dbo.fn_GetEmailListofAdministrator();
	PRINT @CCMail

	WHILE(EXISTS(SELECT 1 FROM #TB_T_COST_CODE WHERE FLAG = 'N'))
	BEGIN
		SELECT	TOP 1 @CC = COST_CODE FROM	#TB_T_COST_CODE WHERE FLAG = 'N'
		UPDATE #TB_T_COST_CODE SET FLAG = 'Y' WHERE COST_CODE = @CC


		DECLARE @SVEmail	VARCHAR(50),
				@SVShortName	VARCHAR(68)

		-- Get SV
		SELECT	@SVEmail		= E.EMAIL, 
				@SVShortName	= dbo.fn_GetShortENName(E.EMP_CODE)
		FROM	TB_M_SV_COST_CENTER SV
		INNER	JOIN
				TB_M_EMPLOYEE E
		ON		SV.EMP_CODE		= E.EMP_CODE
		WHERE	SV.COST_CODE	= @CC


		
		INSERT
		INTO	TB_R_NOTIFICATION
		(		ID,
				[TO],
				CC,
				BCC,
				TITLE,
				[MESSAGE],
				FOOTER,
				[TYPE],
				START_PERIOD,
				END_PERIOD,
				SEND_FLAG,
				RESULT_FLAG,
				REF_FUNC,
				REF_DOC_NO,
				CREATE_DATE,
				CREATE_BY
		)
		SELECT	NEXT VALUE FOR NOTIFICATION_ID OVER (ORDER BY T.ASSET_NO),
				@SVEmail	AS [TO],
				@CCMail		AS CC, -- Get CC Mail
				NULL		AS BCC,
				dbo.fn_StringFormat(@SubjectTemplate, T.ASSET_NO) AS TITLE,
				dbo.fn_StringFormat(@BodyTemplate, 
							CONCAT(	@SVShortName,'|', 
									T.ASSET_NO,'|', 
									T.ASSET_NAME,'|', 
									T.COST_CODE,'|', 
									T.COST_NAME,'|')) AS [MESSAGE],
				NULL	AS FOOTER, -- Get From Default
				'I'		AS [TYPE],
				NULL	AS [START_PERIOD],
				NULL	AS [END_PERIOD],
				'N'		AS [SEND_FLAG],
				NULL	AS RESULT_FLAG,
				'BFD02160'	AS REF_FUNC,
				NULL	AS REF_DOC_NO,
				GETDATE(),
				@User
		FROM	#TB_T_ASSET T
		WHERE	T.COST_CODE = @CC


	END -- WHILE(EXISTS(SELECT 1 FROM #TB_T_COST_CODE WHERE FLAG = 'N'))

	
	--------------------------------------------------------------------------------------------------------
	-- Update Next Time
SETNEXT:
	DECLARE @s VARCHAR(400)
	SET @s = dbo.fn_GetSystemMaster(@Category,@SubCategory,'EVERY_MONTH')
	SET @dNextTime = DATEADD(MONTH, CONVERT(INT, @s), @dNextTime)
	
	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @User
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode

	IF OBJECT_ID('tempdb..#TB_T_COST_CODE') IS NOT NULL
		DROP TABLE #TB_T_COST_CODE

	IF OBJECT_ID('tempdb..#TB_T_ASSET') IS NOT NULL
		DROP TABLE #TB_T_ASSET

END
GO
