DROP PROCEDURE [dbo].[sp_WFD01210_GetCostCenterAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 13/03/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_GetCostCenterAll]
(
@COMPANY T_COMPANY = null
)
AS
BEGIN
	SELECT [COST_CODE]
      ,[COST_NAME]
      ,null [PLANT_CD]
      ,null [PLANT_NAME]
      ,[MAP_PATH]
      ,[ENABLE_FLAG]
      ,[EFFECTIVE_FROM]
      ,[EFFECTIVE_TO]
      ,[STATUS]
      ,null [PARENT_FLAG]
      ,[CREATE_DATE]
      ,[CREATE_BY]
      ,[UPDATE_DATE]
      ,[UPDATE_BY]
	FROM [TB_M_COST_CENTER] C (nolock)
	WHERE (C.COMPANY in (select COMPANY_CODE FROM dbo.fn_GetMultipleCompanyList(@COMPANY)) or @COMPANY is null)
	AND [STATUS] = 'Y'
END



GO
