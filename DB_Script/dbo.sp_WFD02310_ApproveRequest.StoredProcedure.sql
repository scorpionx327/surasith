DROP PROCEDURE [dbo].[sp_WFD02310_ApproveRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02310_ApproveRequest]
(	
	@GUID			T_GUID,
	@DOC_NO			T_DOC_NO,
	@USER			T_SYS_USER
)
AS
BEGIN

	DECLARE @DocStatus VARCHAR(2)

	SELECT	@DocStatus = [STATUS] 
	FROM	TB_R_REQUEST_H
	WHERE	DOC_NO		= @DOC_NO


	EXEC sp_WFD02310_DeleteAssetFlagY @DOC_NO, @USER

	-- Nothing to update from Temp to actual


	IF dbo.fn_IsReadyGenFile(@DOC_NO) = 'N'
	RETURN

	-------------------------------------------------------------------------------------
	-- Approve by AEC
	-- Execute when Status is change from 10 -> 20 (MGR Approve)
	UPDATE	H
	SET		H.PRINT_STATUS		= 'N',
			H.PROCESS_STATUS	= NULL,
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER
	FROM	TB_M_ASSETS_H H
	INNER	JOIN
			TB_R_REQUEST_REPRINT M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO	= @DOC_NO

	DECLARE @format VARCHAR(255) = 'Tag Reprint barcode, Print Count {0}'

	DECLARE @LatestApproverCode	T_SYS_USER, @LatestApproverName	VARCHAR(68)
	DECLARE @AECCode		T_SYS_USER

	UPDATE	H
	SET		H.DETAIL		= dbo.fn_StringFormat(@format, M.PRINT_COUNT),
			H.TRANS_DATE	= GETDATE(),
			H.SAP_UPDATE_STATUS		= 'N',	
			-- To be change ----------------------------------------------
			H.LATEST_APPROVE_CODE	= @LatestApproverCode, 
			H.LATEST_APPROVE_NAME	= @LatestApproverName, 
			H.AEC_APPR_BY			= @AECCode,
			H.AEC_APPR_DATE			= GETDATE(),
			--------------------------------------------------------------
			H.UPDATE_DATE	= GETDATE(),
			H.UPDATE_BY		= @USER 
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_H R WITH(NOLOCK)
	ON		H.DOC_NO	= R.DOC_NO
	INNER	JOIN
			TB_R_REQUEST_REPRINT M WITH(NOLOCK)
	ON		M.COMPANY	= H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	R.DOC_NO	= @DOC_NO

END
GO
