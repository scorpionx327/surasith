DROP PROCEDURE [dbo].[sp_WFD01270_ValidateGenerateApprover]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01270_ValidateGenerateApprover]
(	
	@REQUEST_TYPE		VARCHAR(1)	= NULL	,
	@GUID				VARCHAR(50)			= NULL
	
)
AS
BEGIN
	
	DECLARE @Req	VARCHAR(100)
	
	SET @Req = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_TYPE',@REQUEST_TYPE)
	
	-- ERR	Sub Type {0} not exists in Approve Flow Master for {1} Request

	SELECT	'MFAS1903AERR' MESSAGE_CODE, 'ERR' MESSAGE_TYPE, 
			DBO.fn_ReplaceMessage2('MFAS1903AERR', A.VALUE ,@Req )  MESSAGE_TEXT
	FROM	
	(
			SELECT	T.ASSET_CLASS, S.VALUE
			FROM	TB_T_SELECTED_ASSETS T
			INNER JOIN TB_M_SYSTEM S ON T.ASSET_CLASS = S.CODE 
									AND S.CATEGORY			= 'FAS_TYPE'
									AND S.SUB_CATEGORY		= 'ASSET_CATEGORY'
									AND S.ACTIVE_FLAG		= 'Y'
			WHERE	[GUID] = @GUID
			GROUP	BY
					T.ASSET_CLASS, S.VALUE
	)	A
	LEFT	JOIN
			TB_M_APPROVE_H H
	ON	H.ASSET_CLASS	= A.ASSET_CLASS AND
		(	(@REQUEST_TYPE != 'P' AND H.REQUEST_TYPE		= @REQUEST_TYPE) OR
			(@REQUEST_TYPE  = 'P' AND H.REQUEST_TYPE IN ('R','L'))
		)
	WHERE	H.APPRV_ID IS NULL
	GROUP BY A.ASSET_CLASS,A.VALUE
		

END


GO
