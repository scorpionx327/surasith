DROP PROCEDURE [dbo].[sp_WFD02710_ValidationAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02710_ValidationAsset]
(	
	@GUID	T_GUID
)
AS
BEGIN


	DECLARE @MESSAGE_CODE	VARCHAR(12)
	
	--==================== Check Asset Status ====================--

	DECLARE @TB_T_ERR_MSG AS TABLE
	(
		MESSAGE_CODE	VARCHAR(25),
		MESSAGE_TYPE	VARCHAR(4),
		MESSAGE_TEXT	VARCHAR(255)
	)
	

	---- Other Process Status
	--INSERT INTO @TB_T_ERR_MSG
	--SELECT 'MFAS1902AERR', 'ERR', DBO.fn_ReplaceMessage2('MFAS1902AERR',H.ASSET_NO,
	--		CASE H.PROCESS_STATUS	WHEN 'C' THEN 'CIP' 
	--							WHEN 'D' THEN 'Disposing' 
	--							WHEN 'T' THEN 'Transfering' 
	--							ELSE '' END)
	--FROM TB_R_REQUEST_DISP_D D
	--LEFT JOIN TB_M_ASSETS_H H ON LEFT(D.ASSET_NO,13) = LEFT(H.ASSET_NO,13)
	--							AND H.PROCESS_STATUS NOT IN ('P','R','L')
	--WHERE [GUID] = @GUID
	---- AND D.COST = D.COST_DISP 
	--AND dbo.fn_IsFullRetired(D.ORIGINAL_QTY, D.QTY, D.COST, D.COST_DISP,  D.RETIRE) = 'Y'
	--AND RIGHT(D.ASSET_NO,2) = '00'
	--AND H.ASSET_NO IS NOT NULL AND (H.ASSET_NO != LEFT(D.ASSET_NO,13) + '00')

	
	SELECT	*
	FROM	@TB_T_ERR_MSG
END
GO
