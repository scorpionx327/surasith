DROP PROCEDURE [dbo].[sp_BFD01160_InsertStockTakingNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertStockTakingNotification]
@User	VARCHAR(8)
AS
BEGIN
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'STOCK_TAKING'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'
	--------------------------------------------------------------------------------------------------------
	-- Weekly
	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	IF(@dNextTime > GETDATE())
		RETURN

	-- Get Duedate

	DECLARE @s VARCHAR(400)
	SET @s = dbo.fn_GetSystemMaster(@Category,@SubCategory ,'BEFORE_PERIOD')

	DECLARE @iDate INT
	SET @iDate = CONVERT(INT, @s)

	-- Sending to SV
	-- NOTIFICATION	ASSET_TYPE_ALLW_PRINT


	SELECT	SV.EMP_CODE, SV.DATE_FROM, SV.DATE_TO, SV.TIME_START, SV.TIME_END, 
			H.STOCK_TAKE_KEY, 'N' AS FLAG
	INTO	#TB_T_SV
	FROM	TB_R_STOCK_TAKE_H H
	INNER	JOIN
			TB_R_STOCK_TAKE_D_PER_SV SV
	ON		H.STOCK_TAKE_KEY	= SV.STOCK_TAKE_KEY AND
			H.[YEAR]			= SV.[YEAR] AND
			H.[ROUND]			= SV.[ROUND] AND
			H.ASSET_LOCATION	= SV.ASSET_LOCATION
	WHERE	H.PLAN_STATUS		IN ('C','S') AND
			H.ASSET_LOCATION	= 'I' AND
			-- DATEDIFF(DAY, SV.DATE_FROM, GETDATE()) = @iDate AND
			DATEDIFF(DAY, GETDATE(), SV.DATE_FROM) = @iDate AND
			EXISTS(	SELECT	1 
					FROM	TB_R_STOCK_TAKE_D D 
					WHERE	D.STOCK_TAKE_KEY	= SV.STOCK_TAKE_KEY AND
							D.[YEAR]			= SV.[YEAR] AND
							D.[ROUND]			= SV.[ROUND] AND
							D.ASSET_LOCATION	= SV.ASSET_LOCATION AND
							D.SV_EMP_CODE		= SV.EMP_CODE
					)
	GROUP	BY
			SV.EMP_CODE, SV.DATE_FROM, SV.DATE_TO, SV.TIME_START, SV.TIME_END,
			H.STOCK_TAKE_KEY
	IF @@ROWCOUNT = 0
		GOTO SETNEXT
	
	
	DECLARE @BodyTemplate VARCHAR(400), @SubjectTemplate VARCHAR(400)

	SET @SubjectTemplate	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD02160_STOCK_TAKING')
	SET @BodyTemplate		= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD02160_STOCK_TAKING')
	

	INSERT
	INTO	TB_R_NOTIFICATION
	(		ID,
			[TO],
			CC,
			BCC,
			TITLE,
			[MESSAGE],
			FOOTER,
			[TYPE],
			START_PERIOD,
			END_PERIOD,
			SEND_FLAG,
			RESULT_FLAG,
			REF_FUNC,
			REF_DOC_NO,
			CREATE_DATE,
			CREATE_BY
	)
	SELECT	NEXT VALUE FOR NOTIFICATION_ID OVER(ORDER BY E.EMP_CODE),
			E.EMAIL	AS [TO],
			NULL		AS CC,
			NULL		AS BCC,
			dbo.fn_StringFormat(@SubjectTemplate, 
						CONCAT(	@iDate, '|',
								T.DATE_FROM, '|', 
								T.DATE_TO)) AS TITLE,
			dbo.fn_StringFormat(@BodyTemplate, 
						CONCAT( E.EMP_NAME, ' ', LEFT(E.EMP_LASTNAME,1), '.',
								T.STOCK_TAKE_KEY)) AS [MESSAGE],
			NULL	AS FOOTER, -- Get From Default
			'I'		AS [TYPE],
			NULL	AS [START_PERIOD],
			NULL	AS [END_PERIOD],
			'N'		AS [SEND_FLAG],
			NULL	AS RESULT_FLAG,
			'BFD02160'	AS REF_FUNC,
			NULL	AS REF_DOC_NO,
			GETDATE(),
			@User
	FROM	#TB_T_SV T
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		T.EMP_CODE = E.EMP_CODE
	WHERE	E.ACTIVEFLAG = 'Y'
	

	--------------------------------------------------------------------------------------------------------
	-- Update Next Time
SETNEXT:
	SET @s = dbo.fn_GetSystemMaster(@Category,@SubCategory,'EVERY_DATE')
	SET @dNextTime = DATEADD(DAY, CONVERT(INT, @s), @dNextTime)
	
	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @User
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode

	IF OBJECT_ID('tempdb..#TB_T_SV') IS NOT NULL			
	DROP TABLE #TB_T_SV

END

/****** Object:  StoredProcedure [dbo].[sp_BFD01260_InsertStockTakingNotificationOutsource]    Script Date: 22/03/2017 5:44:17 PM ******/
SET ANSI_NULLS ON


GO
