DROP PROCEDURE [dbo].[sp_WFD02510_GetTempDocHeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD02510_GetTempDocHeader]
(
	@DOC_NO						VARCHAR(10)
)
	
AS
BEGIN
	SELECT 
	DOC_NO,							DISP_TYPE,										LOSS_COUNTER_MEASURE, 
	USER_MISTAKE_FLAG,				COMPENSATION,									DISP_MASS_REASON, 
	DISP_MASS_ATTACH_DOC,			MINUTE_OF_BIDDING,								BIDDING_ATTACH_DOC, 
	GUID	
	FROM TB_R_REQUEST_DISP_H H
	WHERE H.DOC_NO		 = @DOC_NO
	AND  (LOSS_COUNTER_MEASURE LIKE 'TEMP%' OR DISP_MASS_ATTACH_DOC LIKE 'TEMP%')


END





GO
