DROP PROCEDURE [dbo].[sp_WFD02640_RejectRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
User by : Uten S.
Create Date : 2017-04-10 
*/

CREATE PROCEDURE [dbo].[sp_WFD02640_RejectRequest]
(	
	@DOC_NO		T_DOC_NO,
	@USER		T_SYS_USER
)
AS
BEGIN
	
	DECLARE @DocStatus VARCHAR(2)
	DECLARE @NEW_STATUS VARCHAR(2) = 'D'

	SELECT	@DocStatus	= [STATUS] 
	FROM	TB_R_REQUEST_H
	WHERE	DOC_NO		= @DOC_NO

	IF @DocStatus	IN ('75','80') -- User close with Error, Rejected
	BEGIN
		---- Clear request stock for re sumbit (incase Reject  )
		DELETE	S
		FROM	TB_R_REQUEST_STOCK S
		WHERE	S.DOC_NO	=	@DOC_NO
	END
	
END
GO
