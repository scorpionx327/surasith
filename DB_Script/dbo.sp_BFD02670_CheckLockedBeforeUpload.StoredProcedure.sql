DROP PROCEDURE [dbo].[sp_BFD02670_CheckLockedBeforeUpload]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 07-03-2017
-- Description:	Update stock take d data
-- =============================================
CREATE PROCEDURE [dbo].[sp_BFD02670_CheckLockedBeforeUpload]
	@STOCK_TAKE_KEY varchar(7) 
	,@EMP_CODE varchar(8) 

AS
BEGIN
	
	DECLARE @SERVERDT DATETIME = GETDATE()
	, @ENDSCANDT DATETIME
	, @LOCKED varchar(1)
	, @IS_LOCK varchar(1);

	SELECT TOP 1 
	@ENDSCANDT = CONVERT(DATETIME, CONVERT(VARCHAR, SV.DATE_TO) + ' ' + SV.TIME_END)
	, @LOCKED = SV.LOCKED
	, @IS_LOCK = SV.IS_LOCK
	from TB_R_STOCK_TAKE_D_PER_SV SV 
	WHERE SV.STOCK_TAKE_KEY = @STOCK_TAKE_KEY AND SV.EMP_CODE = @EMP_CODE;

	IF ((@SERVERDT > @ENDSCANDT AND (@IS_LOCK = 'N' AND ISNULL(@LOCKED, 'N') = 'N')) OR @IS_LOCK = 'Y')

	--IF (@SERVERDT > @ENDSCANDT AND ISNULL(@LOCKED, 'N') = 'N')
	BEGIN
		SELECT CONVERT(bit, 1) as RESULT
	END
	ELSE
	BEGIN
		SELECT CONVERT(bit, 0) as RESULT
	END

END
GO
