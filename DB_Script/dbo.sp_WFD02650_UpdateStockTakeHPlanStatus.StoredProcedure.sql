DROP PROCEDURE [dbo].[sp_WFD02650_UpdateStockTakeHPlanStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Update stock take h plan status
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_UpdateStockTakeHPlanStatus]
	-- Add the parameters for the stored procedure here
	@STOCK_TAKE_KEY NVARCHAR(7)
	, @UPDATE_BY	nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE TB_R_STOCK_TAKE_H	
		SET PLAN_STATUS='S'
			,UPDATE_BY=@UPDATE_BY
			--,UPDATE_DATE=GETDATE()
	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY
	AND PLAN_STATUS != 'S'
	AND PLAN_STATUS != 'F'

END





GO
