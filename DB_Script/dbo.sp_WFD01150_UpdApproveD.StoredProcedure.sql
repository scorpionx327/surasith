DROP PROCEDURE [dbo].[sp_WFD01150_UpdApproveD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01150_UpdApproveD]
	@APPRV_ID int
    ,@INDX int
    ,@ROLE varchar(3)
    ,@APPRV_GROUP varchar(8)
    ,@DIVISION varchar(6)
    ,@REQUIRE_FLAG varchar(8)
    ,@CONDITION_CODE varchar(3)
    ,@OPERATOR varchar(2)
    ,@VALUE1 varchar(20)
    ,@VALUE2 varchar(20)
    ,@GENERATE_FILE varchar(1)
    ,@FINISH_FLOW varchar(1)
    ,@ALLOW_DEL_ITEM varchar(1)
    ,@ALLOW_REJECT varchar(1)
    ,@ALLOW_SEL_APPRV varchar(1)
    ,@REJECT_TO int
    ,@HIGHER_APPR varchar(1)
    ,@LEAD_TIME int
    ,@NOTI_BY_EMAIL varchar(1)
    ,@OPERATION varchar(1)
AS
BEGIN TRY 
	SET NOCOUNT ON;

	UPDATE TB_M_APPROVE_D
	SET [ROLE] = @ROLE
           ,APPRV_GROUP = @APPRV_GROUP
           ,DIVISION = @DIVISION
           ,REQUIRE_FLAG = @REQUIRE_FLAG
           ,CONDITION_CODE = @CONDITION_CODE
           ,OPERATOR = @OPERATOR
           ,VALUE1 = @VALUE1
           ,VALUE2 = @VALUE2
           ,GENERATE_FILE = @GENERATE_FILE
           ,FINISH_FLOW = @FINISH_FLOW
           ,ALLOW_DEL_ITEM = @ALLOW_DEL_ITEM
           ,ALLOW_REJECT = @ALLOW_REJECT
           ,ALLOW_SEL_APPRV = @ALLOW_SEL_APPRV
           ,REJECT_TO = @REJECT_TO
           ,HIGHER_APPR = @HIGHER_APPR
           ,LEAD_TIME = @LEAD_TIME
           ,NOTI_BY_EMAIL = @NOTI_BY_EMAIL
           ,OPERATION = @OPERATION
	WHERE APPRV_ID = @APPRV_ID
		AND INDX = @INDX
END TRY
BEGIN CATCH
	THROW
END CATCH
GO
