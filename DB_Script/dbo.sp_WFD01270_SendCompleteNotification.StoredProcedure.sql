DROP PROCEDURE [dbo].[sp_WFD01270_SendCompleteNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_SendCompleteNotification]

@DocNo	T_DOC_NO
AS
BEGIN
	-- Get Requestor
	DECLARE @Requestor VARCHAR(8), @RequestType	VARCHAR(1), @DocStatus VARCHAR(2)
	SELECT	@Requestor = EMP_CODE, 
			@RequestType = REQUEST_TYPE,
			@DocStatus = [STATUS]
	FROM	TB_R_REQUEST_H WHERE DOC_NO = @DocNo

	-- Get FA

	-- Send Email To Approver
	DECLARE @TO VARCHAR(255),		@CC VARCHAR(255),
			@TITLE VARCHAR(255),	@MSG VARCHAR(MAX), @FOOTER VARCHAR(250),
			@Name	VARCHAR(200)

	SELECT	@TO = EMAIL, @Name = CONCAT(EMP_TITLE,' ', EMP_NAME,' ', LEFT(EMP_LASTNAME,1), '.')
	FROM	TB_M_EMPLOYEE 
	WHERE	EMP_CODE = @Requestor

	
	DECLARE @Req VARCHAR(100)

	SET @Req = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_TYPE',@RequestType)
	

	IF (@DocStatus > '60')
	BEGIN
		SET @TITLE	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','WFD01270_VERIFY')
		SET @MSG	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','WFD01270_VERIFY')
	END
	ELSE
	BEGIN
		SET @TITLE	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','WFD01270_COMPLETE')
		SET @MSG	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','WFD01270_COMPLETE')
	END
	SET @FOOTER = dbo.fn_GetSystemMaster('NOTIFICATION','EMAIL','FOOTER')

	SET @TITLE = dbo.fn_StringFormat(@TITLE, @DocNo )
	SET @MSG = dbo.fn_StringFormat(@MSG, CONCAT(@Req,'|',@RequestType, '|', @DocNo,'|', @Name))



	EXEC sp_Common_InsertNotification 
			@TO, @CC, NULL, @TITLE, @MSG, @FOOTER, 'I', NULL, NULL,'N', 'N', 'WFD01270', @DocNo, @Requestor

END


GO
