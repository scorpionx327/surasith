DROP PROCEDURE [dbo].[sp_BFD01260_UpdateItemSeq]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD02110 : Fixed assets interface batch========================================
-- Author:     FTH/Suphachai Leetrakool 
-- Create date:  2017/05/18 (yyyy/mm/dd)
-- Description:  For update Item Seq
- ============================================= SFAS =============================================*/
CREATE PROCEDURE [dbo].[sp_BFD01260_UpdateItemSeq]
(
	@AppID		INT,
	@UserID		T_SYS_USER
)
AS
	
	-- Clear All
	UPDATE	TB_M_ASSETS_H
	SET		ITEM_SEQ = NULL 

	-- Reassign for I/H
	UPDATE	M
	SET		M.ITEM_SEQ = T.ITEM_SEQ
	FROM	TB_M_ASSETS_H M
	INNER	JOIN
	(
		SELECT  ROW_NUMBER()  OVER (PARTITION BY COMPANY, COST_CODE ORDER BY MINOR_CATEGORY, ASSET_NO ) ITEM_SEQ,
				COMPANY, ASSET_NO, ASSET_SUB
		FROM	TB_M_ASSETS_H H
		WHERE	H.[STATUS]		= 'Y' 	AND 
				H.ASSET_SUB		= '0000' AND
				H.INHOUSE_FLAG	= 'Y' --Inhouse group by cost center	
	)	T
	ON	M.COMPANY	= T.COMPANY AND
		M.ASSET_NO	= T.ASSET_NO AND
		M.ASSET_SUB	= T.ASSET_SUB


	-- No define for OS Assets









GO
