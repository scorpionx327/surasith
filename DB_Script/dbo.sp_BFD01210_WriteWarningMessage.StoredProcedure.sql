DROP PROCEDURE [dbo].[sp_BFD01210_WriteWarningMessage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ========== SFAS =====BFD01220 : Employee Master Interface batch========================================
-- UISS_1.2.2_BFD01220_Employee Master Interface batch
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/06 (yyyy/mm/dd)
-- Description: UISS_1.2.2_BFD01220_Employee Master Interface batch
*/

CREATE PROCEDURE  [dbo].[sp_BFD01210_WriteWarningMessage]
 @AppID			INT,
 @UserID	   VARCHAR(8)
AS
BEGIN TRY

DECLARE @Msg VARCHAR(500)

	----------------------------------------------------------------------
	-- Check Ex. Cannot find role responsibility for Position Code  = T0ZZ		
    SET		@Msg = dbo.fn_GetMessage('MCOM0006BWRN')
	INSERT	TB_L_LOGGING (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
  	SELECT  DISTINCT @AppID, 'P', 'N', 'W',
			dbo.fn_StringFormat(@Msg , CONCAT('Role responsibility for POSITION_DESC =',S.POSITION_DESC)),'Y', @UserID, GETDATE()
	FROM    TB_S_EMP_SAP S
	WHERE	POSITION_DESC IS NOT NULL AND
			NOT EXISTS(	SELECT	1 
						FROM	TB_M_SYSTEM MS
						WHERE	MS.CATEGORY		= 'SYSTEM_CONFIG' AND
								MS.SUB_CATEGORY	= CONCAT('JOB_WORD_MAPPING_',S.HOST_COMPANY_CODE) AND
								S.POSITION_DESC			LIKE	MS.CODE
			)
	
END TRY
BEGIN CATCH
			PRINT concat('ERROR Message:',ERROR_MESSAGE())
END CATCH
GO
