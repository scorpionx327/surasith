DROP PROCEDURE [dbo].[sp_LFD02140_GetAssetH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SARUN YUANYONG
-- Create date: 09-02-2017
-- Description:	Get Asset by costcenter and barcode
-- =============================================
CREATE PROCEDURE [dbo].[sp_LFD02140_GetAssetH]
		@COST_CODE Varchar(69),
		@BARCODE_SIZE Varchar(1),
		@LOCATION_NAME VArchar(400)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--ASSET_NO |company_name |DATE_IN_SERVICE| logo_path| ASSET_NAME |PRINT_COUNT |CONTACT| COSTCENTER |INVEST_REASON |SERIAL_NO| TEL| TOTALPRINT| PRINTLOCATION


			select ah.[ASSET_NO]
				  ,left(ah.[ASSET_NAME],50) AS ASSET_NAME
				  ,pq.[BARCODE_SIZE]
				  ,ah.[DATE_IN_SERVICE]
				  ,ah.[PRINT_COUNT]
				  ,ah.BARCODE
				  ,ah.COMPANY AS COMPANY_NAME
				  ,ah.SERIAL_NO
				  ,E.EMP_NAME AS CONTACT
				  ,E.TEL_NO AS TEL
				  ,E.COST_NAME  AS COSTCENTER
				  ,ah.INVEST_REASON 
				  ,pq.PRINT_LOCATION as PRINTLOCATION
				
			from 
			TB_R_PRINT_Q pq
			left join TB_M_ASSETS_H ah on ah.ASSET_NO = pq.ASSET_NO
			left join TB_M_SYSTEM S on S.CODE = pq.PRINT_LOCATION
			left join TB_M_EMPLOYEE E on pq.CREATE_BY = E.SYS_EMP_CODE 
			where pq.COST_CODE = @COST_CODE and pq.BARCODE_SIZE = @BARCODE_SIZE
			and S.VALUE = @LOCATION_NAME
			
			order by pq.COST_CODE,ah.ASSET_NO


END



GO
