DROP PROCEDURE [dbo].[sp_BFD01250_ReceiveIFWBSBudgetBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[sp_BFD01250_ReceiveIFWBSBudgetBatch]
(
 	@AppID		INT,
	@UserID		VARCHAR(8)
)
AS
BEGIN TRY

	IF NOT EXISTS(SELECT 1 FROM TB_S_WBS_BUDGET)
	BEGIN
	    EXEC sp_Common_InsertLog_With_Param @AppID, 'P','MCOM0020AWRN', 'Receiving WBS Budget Interface Batch',@UserID
		SELECT 2 AS SUCCESS ; -- Warning
		RETURN;
	END
	
	DECLARE @rs INT
	EXEC @rs = sp_BFD01250_Validation @AppID, @UserID
	
	IF @rs = 2
	BEGIN
		SELECT 0 AS SUCCESS ; -- Error
		RETURN;
	END

	BEGIN TRANSACTION T1

	UPDATE	M
	SET		M.BUDGET_AMOUNT	= S.BUDGET_AMOUNT,
			M.ACTUAL		= S.ACTUAL,
			M.COMMITMENT	= S.COMMITMENT,
			M.AVAILABLE		= S.AVAILABLE,
			M.UPDATE_BY		= @UserID,
			M.UPDATE_DATE	= GETDATE()
	FROM	TB_M_WBS_BUDGET M
	INNER	JOIN
			TB_S_WBS_BUDGET S
	ON		M.WBS_BUDGET	= S.WBS_BUDGET AND
			M.FISCAL_YEAR	= S.FISCAL_YEAR

	INSERT
	INTO	TB_M_WBS_BUDGET
	(		COMPANY_CODE,
			WBS_BUDGET,
			FISCAL_YEAR,
			BUDGET_AMOUNT,
			ACTUAL,
			COMMITMENT,
			AVAILABLE,
			CREATE_BY,
			CREATE_DATE,
			UPDATE_BY,
			UPDATE_DATE)
	SELECT	M.COMPANY_CODE,
			S.WBS_BUDGET,
			S.FISCAL_YEAR,
			S.BUDGET_AMOUNT,
			S.ACTUAL,
			S.COMMITMENT,
			S.AVAILABLE,
			@UserID,
			GETDATE(),
			@UserID,
			GETDATE()
	FROM	TB_S_WBS_BUDGET S
	INNER	JOIN
			TB_M_WBS M
	ON		S.WBS_BUDGET	= M.WBS_CODE
	WHERE	NOT EXISTS(	SELECT	1
						FROM	TB_M_WBS_BUDGET  M
						WHERE	M.WBS_BUDGET	= S.WBS_BUDGET AND
								M.FISCAL_YEAR	= S.FISCAL_YEAR
						)


	COMMIT TRANSACTION T1
	SELECT 1 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
	RETURN;

END TRY
BEGIN CATCH

	IF @@TRANCOUNT <> 0
	BEGIN
		ROLLBACK TRANSACTION T1
	END
	--===========================================================Write log =====================================
	DECLARE @errMessage nVARCHAR(1000);
	SET @errMessage= CONCAT('Receiving WBS Budget Interface Batch','|','SQL ERROR -->ERROR_PROCEDURE: ',ERROR_PROCEDURE(),',ERROR_NUMBER :', ERROR_NUMBER(),',  ERROR_MESSAGE :',ERROR_MESSAGE(),'|',NULL,'|',NULL)
	exec sp_Common_InsertLog_With_Param @AppID,'E','MSTD7002BINF',@errMessage,@UserID,'Y'
	--===========================================================End Write log =====================================
		--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;
	SELECT @ErrorMessage = ERROR_MESSAGE();
	SELECT @ErrorSeverity = ERROR_SEVERITY();
	SELECT @ErrorState = ERROR_STATE();
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	SELECT 0 AS SUCCESS; -- -0 is Error, 1 is Success, 2 is Warning
	RETURN;

END CATCH
GO
