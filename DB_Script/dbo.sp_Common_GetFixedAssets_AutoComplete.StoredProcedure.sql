DROP PROCEDURE [dbo].[sp_Common_GetFixedAssets_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Common_GetFixedAssets_AutoComplete]
@keyword		VARCHAR(24),
@Company		VARCHAR(10)
AS
BEGIN
		SELECT	ASSET_NO,
				ASSET_SUB,
				ASSET_NAME,
				INVEN_NO
		FROM	TB_M_ASSETS_H
		WHERE	INVEN_NO LIKE CONCAT(@keyword,'%') AND
				[STATUS] = 'Y'
END
GO
