DROP PROCEDURE [dbo].[sp_WFD02320_PrintTagSuccess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 20/02/2017	
-- Description:	Insert TB_H_PRINT_Q ,TB_L_LOGGING ,Delete TB_R_PRINT_Q 
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02320_PrintTagSuccess]
	@COMPANY	T_COMPANY,
	@ASSET_NO	T_ASSET_NO,
	@ASSET_SUB	T_ASSET_SUB,
	@COST_CODE	T_COST_CODE,
	@RESP_COST_CODE T_COST_CODE,
	@EMP_CODE	T_SYS_USER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

	--INSERT INTO TB_H_PRINT_Q SELECT * FROM TB_R_PRINT_Q r WHERE 
	--	r.ASSET_NO = @ASSET_NO and r.COST_CODE = @COST_CODE;

	--	New request for keep printing EMP_CODE and requeter by Pawares M. 20180620
	INSERT 
	INTO	TB_H_PRINT_Q (
				COMPANY,
				ASSET_NO,
				ASSET_SUB,
				COST_CODE,
				RESP_COST_CODE,
				BARCODE_SIZE,
				PRINT_LOCATION,
				CREATE_DATE,
				REQUESTED_BY,
				CREATED_BY,
				REMARK)
	SELECT		r.COMPANY,
				r.ASSET_NO,
				r.ASSET_SUB, 
				r.COST_CODE,
				r.RESP_COST_CODE, 
				r.BARCODE_SIZE, 
				r.PRINT_LOCATION, 
				GETDATE(), 
				r.CREATE_BY, 
				@EMP_CODE, 
				r.REMARK 
	FROM		TB_R_PRINT_Q r
	WHERE		r.COMPANY	= @COMPANY AND
				r.ASSET_NO	= @ASSET_NO AND
				r.ASSET_SUB	= @ASSET_SUB AND 
				r.COST_CODE = @COST_CODE AND
				r.RESP_COST_CODE= @RESP_COST_CODE ;
    
	--INSERT INTO TB_L_LOGGING ([LEVEL],[MESSAGE],CREATE_BY,CREATE_DATE) VALUES('I',CONCAT('Print tag of Asset No: ',@ASSET_NO,', Cost Center Code : ',@COST_CODE),@EMP_CODE,GETDATE());


	DELETE 
	FROM	TB_R_PRINT_Q
	WHERE	COMPANY	= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB AND 
			COST_CODE = @COST_CODE AND
			RESP_COST_CODE= @RESP_COST_CODE ;

END
GO
