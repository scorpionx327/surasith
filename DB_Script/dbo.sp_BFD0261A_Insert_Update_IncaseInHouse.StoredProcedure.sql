DROP PROCEDURE [dbo].[sp_BFD0261A_Insert_Update_IncaseInHouse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_BFD0261A_Insert_Update_IncaseInHouse]
(
	@UserID VARCHAR(8),
    @StockYear VARCHAR(4),
    @StockRound VARCHAR(2),
    @StockAssetLocation VARCHAR(1), --I is In house,O is out-source
    @QTyOfHanheld VARCHAR(8),
    @TargetDateFrom VARCHAR(8), --format YYYYMMDD
    @TargetDateTo VARCHAR(8), --format YYYYMMDD
    @BreakTime VARCHAR(8),
	@ERROR_MESSAGE	VARCHAR(MAX) OUT 

)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

-- 2  Insert into   TB_R_STOCK_TAKE_D
   INSERT INTO TB_R_STOCK_TAKE_D([STOCK_TAKE_KEY] ,[YEAR] ,[ROUND]    ,[ASSET_LOCATION]    ,[SV_EMP_CODE]
           ,[ASSET_NO]     ,[ASSET_NAME]  ,[EMP_CODE]     ,[COST_CODE]       ,[COST_NAME]      ,[PLANT_CD]
           ,[PLANT_NAME]       
		   --,[SUPPLIER_NAME]       
		   ,[BARCODE]        
		   --,[SUB_TYPE]        
		   ,[CHECK_STATUS]
           ,[SCAN_DATE]           ,[START_COUNT_TIME]           ,[END_COUNT_TIME]           ,[COUNT_TIME]
           --,[LOCATION_NAME]           
		   ,[CREATE_DATE]           ,[CREATE_BY]           ,[UPDATE_DATE]           ,[UPDATE_BY]
		   ,SERIAL_NO
		   ,COMPANY
		   ,ASSET_SUB
		   --,OWN_COST_CODE
		   --,	MODEL,	PRODUCTION_PART_NO,	TOOLING_COST
		   ) -- Add new column Support CR#1 2017-06-01
 SELECT CONCAT(@StockYear,@StockRound,@StockAssetLocation) ,@StockYear ,@StockRound,@StockAssetLocation
           , NULL -- <SV_EMP_CODE, varchar(8),>
            ,AssetH.ASSET_NO  -- <ASSET_NO, varchar(15),>
            ,AssetH.ASSET_NAME --<ASSET_NAME, varchar(200),>
            ,NULL -- <EMP_CODE, varchar(8),>
          , (SELECT TOP (1) COST_CODE   
		 		FROM TB_M_ASSETS_D AssetD WHERE AssetH.ASSET_NO=AssetD.ASSET_NO ORDER BY UNIT DESC 
				) AS COST_CODE --<COST_CODE, varchar(8),>  from detail table 
           , NULL -- <COST_NAME, varchar(50),>
           , NULL -- <PLANT_CD, varchar(3),>
           ,NULL -- <PLANT_NAME, varchar(50),>
           --,SUPPLIER_NAME ---<SUPPLIER_NAME, varchar(50),>
           ,AssetH.BARCODE --<BARCODE, varchar(19),>
           --,SUB_TYPE --<SUB_TYPE, varchar(3),>
           ,'N' --- <CHECK_STATUS, varchar(19),>
           ,NULL -- <SCAN_DATE, datetime,>
           ,NULL --  <START_COUNT_TIME, datetime,>
           ,NULL -- <END_COUNT_TIME, datetime,>
           ,NULL -- <COUNT_TIME, int,>
           --,LOCATION_NAME --<LOCATION_NAME, varchar(200),>
           ,GETDATE(), @UserID,  GETDATE(), @UserID
		   ,AssetH.SERIAL_NO
		   ,AssetH.COMPANY
		   ,AssetH.ASSET_SUB
		   --MODEL,	PRODUCTION_PART_NO,	TOOLING_COST -- Add new column Support CR#1 2017-06-01
 FROM TB_M_ASSETS_H AssetH
 INNER JOIN TB_R_STOCK_TAKE_D StockD on  AssetH.ASSET_NO=StockD.ASSET_NO --AND StockD.ASSET_SUB=AssetH.ASSET_SUB and StockD..COMPANY=AssetH.COMPANY
 INNER JOIN TB_M_COST_CENTER M_Cost on StockD.COST_CODE=M_Cost.COST_CODE
 WHERE EXISTS( SELECT 1 FROM TB_S_STOCK_TAKE S_StockTake WHERE AssetH.ASSET_NO=S_StockTake.ASSET_NO AND S_StockTake.ASSET_SUB=AssetH.ASSET_SUB and S_StockTake.COMPANY=AssetH.COMPANY)				
 -- 2.1 Update Cost center detial 
  UPDATE StockD
   SET StockD.COST_NAME =M_Cost.COST_NAME
   --, StockD.PLANT_CD =M_Cost.PLANT_CD
   --, StockD.PLANT_NAME =M_Cost.PLANT_NAME   
  FROM TB_R_STOCK_TAKE_D StockD
  LEFT JOIN TB_M_COST_CENTER M_Cost ON StockD.COST_CODE=M_Cost.COST_CODE
  WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)

-- 2.2 Update SV User 
 UPDATE StockD
   SET StockD.SV_EMP_CODE =SV_User.EMP_CODE
  FROM TB_R_STOCK_TAKE_D StockD
  LEFT JOIN TB_M_SV_COST_CENTER SV_User ON StockD.COST_CODE=SV_User.COST_CODE
  WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)

 --3  Insert into   TB_R_STOCK_TAKE_D_PER_SV
 INSERT INTO TB_R_STOCK_TAKE_D_PER_SV
           ([STOCK_TAKE_KEY] ,[YEAR] ,[ROUND]   ,[ASSET_LOCATION]  ,[EMP_CODE]  ,[EMP_TITLE]
           ,[EMP_NAME]  ,[EMP_LASTNAME]    ,[FA_DATE_FROM] ,[FA_DATE_TO]
           ,[DATE_FROM]  ,[DATE_TO]  ,[TIME_START]  ,[TIME_END]
           ,[TIME_MINUTE]   ,[USAGE_HANDHELD]   ,[IS_LOCK]  ,[TOTAL_ASSET]  
		    ,[CREATE_DATE]   ,[CREATE_BY]   ,[UPDATE_DATE]    ,[UPDATE_BY]
			,COMPANY
			)
SELECT  CONCAT(@StockYear,@StockRound,@StockAssetLocation) ,@StockYear ,@StockRound,@StockAssetLocation
            , SV_EMP_CODE--<EMP_CODE, varchar(8),>
            ,NULL -- <EMP_TITLE, varchar(8),>
            ,NULL -- <EMP_NAME, varchar(30),>
            ,NULL -- <EMP_LASTNAME, nvarchar(30),>
            ,NULL --<FA_DATE_FROM, date,>
            ,NULL --<FA_DATE_TO, date,>
            ,NULL --<DATE_FROM, date,>
            ,NULL --<DATE_TO, date,>
            ,NULL --<TIME_START, varchar(5),>
            ,NULL --<TIME_END, varchar(5),>
            ,0 --<TIME_MINUTE, int,>
            ,NULL --<USAGE_HANDHELD, int,>
            ,'N' -- <IS_LOCK, varchar(1),>
            ,COUNT(ASSET_NO ) --<TOTAL_ASSET, int,>
            ,GETDATE(), @UserID,  GETDATE(), @UserID ,COMPANY
			
FROM TB_R_STOCK_TAKE_D StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
GROUP BY SV_EMP_CODE,COMPANY
-- 3.1 Update SV Emp_Code 
UPDATE StockSV
 SET StockSV.EMP_TITLE=Emp.EMP_TITLE
 ,StockSV.EMP_NAME=Emp.EMP_NAME
 ,StockSV.EMP_LASTNAME=Emp.EMP_LASTNAME
FROM TB_R_STOCK_TAKE_D_PER_SV StockSV LEFT JOIN TB_M_EMPLOYEE Emp ON StockSV.EMP_CODE=Emp.EMP_CODE
WHERE StockSV.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)

 --4 Update  value in  TB_R_STOCK_TAKE_H
  DECLARE @DATA_AS_OF DATE 
  DECLARE @SUB_TYPE VARCHAR(150) 
  DECLARE @ALL_ASSET_TOTAL INT  = 0
  DECLARE @MC_TOTAL INT = 0
  DECLARE @EC_TOTAL INT = 0 
  DECLARE @PC_TOTAL INT = 0
  DECLARE @LC_TOTAL INT = 0

   -- get data as of
   SELECT @DATA_AS_OF= MAX(DATE_IN_SERVICE) 
 FROM TB_M_ASSETS_H AssetH
 WHERE EXISTS( SELECT 1 FROM TB_S_STOCK_TAKE S_StockTake WHERE AssetH.ASSET_NO=S_StockTake.ASSET_NO)	
   -- get sub type
   SELECT @SUB_TYPE= CONCAT(@SUB_TYPE+',','') + ltrim(rtrim(SUB_TYPE))
	FROM  ( SELECT SUB_TYPE 
		     FROM TB_R_STOCK_TAKE_D  StockD
			 WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
			 GROUP BY SUB_TYPE
			)s

SELECT @ALL_ASSET_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)

SELECT @MC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='M/C'

SELECT @EC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='E/C'

SELECT @PC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='P/C'

SELECT @LC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='L/C'

 
DECLARE	@cntParent	INT =0
DECLARE	@cntChield	INT= 0

SELECT @cntParent=	COUNT(1)																											
FROM TB_S_STOCK_TAKE  S
WHERE RIGHT(ASSET_NO,2) 	='00'

SELECT 	@cntChield = COUNT(1)																												
FROM TB_S_STOCK_TAKE  S
WHERE RIGHT(ASSET_NO,2) <> '00'



 UPDATE StockH
 SET  StockH.DATA_AS_OF = @DATA_AS_OF,
      StockH.SUB_TYPE = @SUB_TYPE,
	  StockH.ALL_ASSET_TOTAL = @ALL_ASSET_TOTAL , 
     StockH.MC_TOTAL = @MC_TOTAL , 
     StockH.EC_TOTAL = @EC_TOTAL , 
     StockH.PC_TOTAL = @PC_TOTAL , 
     StockH.LC_TOTAL = @LC_TOTAL ,
	 StockH.ASSET_STATUS = CASE 
							WHEN  (( @cntParent>0) AND (@cntChield> 0) ) THEN 'B'
							WHEN (@cntParent >0)  THEN 'P'
							WHEN (@cntChield > 0) THEN 'C'
							END 
 FROM TB_R_STOCK_TAKE_H StockH 
 WHERE StockH.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)





	RETURN @Error ;
END TRY
BEGIN CATCH
			       PRINT concat('ERROR Message:',ERROR_MESSAGE())
				   SET @ERROR_MESSAGE=ERROR_MESSAGE();
				RETURN 1;
END CATCH







GO
