DROP PROCEDURE [dbo].[sp_WFD02620_GetStockTakeDPerSVDatas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_GetStockTakeDPerSVDatas]
	-- Add the parameters for the stored procedure here
	 @COMPANY	T_COMPANY
	, @YEAR	varchar(4)	
	, @ROUND	varchar(2)	
	, @ASSET_LOCATION	varchar(1)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC [sp_WFD02620_UpdateLockStatusOnScreen] @COMPANY,@YEAR, @ROUND, @ASSET_LOCATION

	SELECT
		LTRIM(RTRIM(STOCK_TAKE_KEY)) as STOCK_TAKE_KEY,
		LTRIM(RTRIM(COMPANY)) as COMPANY,
		LTRIM(RTRIM(YEAR)) as YEAR,
		LTRIM(RTRIM(ROUND)) as ROUND,
		LTRIM(RTRIM(ASSET_LOCATION)) as ASSET_LOCATION,
		LTRIM(RTRIM(EMP_CODE)) as EMP_CODE,
		LTRIM(RTRIM(ISNULL(EMP_TITLE, ''))) as EMP_TITLE,
		LTRIM(RTRIM(EMP_NAME)) as EMP_NAME,
		LTRIM(RTRIM(ISNULL(EMP_LASTNAME, ''))) as EMP_LASTNAME,
		FA_DATE_FROM,
		FA_DATE_TO,
		DATE_FROM,
		DATE_TO,
		LTRIM(RTRIM(TIME_START)) as TIME_START,
		LTRIM(RTRIM(TIME_END)) as TIME_END,
		TIME_MINUTE,
		USAGE_HANDHELD,
		LTRIM(RTRIM(IS_LOCK)) as IS_LOCK,
		TOTAL_ASSET,
		CREATE_DATE,
		LTRIM(RTRIM(CREATE_BY)) as CREATE_BY,
		UPDATE_DATE,
		LTRIM(RTRIM(UPDATE_BY)) as UPDATE_BY
	FROM TB_R_STOCK_TAKE_D_PER_SV
	WHERE COMPANY = @COMPANY and YEAR=@YEAR and ROUND=@ROUND and ASSET_LOCATION=@ASSET_LOCATION
	ORDER BY EMP_NAME ASC

END





GO
