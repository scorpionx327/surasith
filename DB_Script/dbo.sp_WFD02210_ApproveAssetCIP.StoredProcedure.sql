DROP PROCEDURE [dbo].[sp_WFD02210_ApproveAssetCIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec WFD02210_ValidationBeforeSubmitAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f14',null,''
CREATE PROCEDURE [dbo].[sp_WFD02210_ApproveAssetCIP]
(	
	@DOC_NO						VARCHAR(10)			= NULL,		--DOC NO	
	@ASSET_NO					VARCHAR(15)			= NULL,
	@COST_CODE					VARCHAR(8)			= NULL,
	@COMPLETE_DATE				VARCHAR(10)			= NULL,
	@APPROVE_BY					VARCHAR(50)			= NULL
)
AS
BEGIN

	--=============== UPDATE TB_R_REQUEST_CIP ===============--
	UPDATE TB_R_REQUEST_CIP
	SET		COMPLETE_DATE	= @COMPLETE_DATE,
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @APPROVE_BY  
	WHERE	DOC_NO			= @DOC_NO 
			AND ASSET_NO	= @ASSET_NO 

	DECLARE @DocStatus VARCHAR(2)

	SELECT	@DocStatus = [STATUS] 
	FROM	TB_R_REQUEST_H
	WHERE	DOC_NO = @DOC_NO

	
	IF @DocStatus NOT IN ('60','70', '10') -- Add status =10 for support re-submit
	BEGIN
		RETURN
	END

	DECLARE @CIPDescription VARCHAR(255)
	DECLARE @NEW_STATUS VARCHAR(2) = 'C'
	
	IF @DocStatus IN ('60','70')
	BEGIN
		SELECT	@CIPDescription = CONCAT('Close In Project : Project : {' , CIP_NO , '}')
		FROM	TB_R_REQUEST_CIP 
		WHERE	ASSET_NO = @ASSET_NO AND DOC_NO = @DOC_NO AND COST_CODE = @COST_CODE
		
		SET @NEW_STATUS = 'CC'
	END

	EXEC sp_WFD01270_ApproveAsset				
				@DOC_NO,
				@ASSET_NO,
				@COST_CODE,
				@DocStatus,
				'C',
				@NEW_STATUS,
				@CIPDescription,
				@APPROVE_BY

END


GO
