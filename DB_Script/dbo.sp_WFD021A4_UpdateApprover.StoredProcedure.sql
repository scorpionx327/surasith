DROP PROCEDURE [dbo].[sp_WFD021A4_UpdateApprover]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD021A4_UpdateApprover]
@DOC_NO			T_DOC_NO,
@CurrentIndx	INT
AS
BEGIN
	
	UPDATE	TB_R_REQUEST_APPR
	SET		APPR_STATUS = 'W',
			APPR_DATE	= NULL,
			UPDATE_DATE	= GETDATE(),
			UPDATE_BY	= 'System'
	WHERE	DOC_NO		= @DOC_NO AND
			INDX		= @CurrentIndx

	-- Send Email to Next Approver
END
GO
