DROP PROCEDURE [dbo].[sp_WFD02710_UpdateTargetAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	PROCEDURE [dbo].[sp_WFD02710_UpdateTargetAsset]
@GUID		T_GUID,
@ASSET_NO	T_ASSET_NO,
@ASSET_SUB	T_ASSET_SUB,
@CAPDATE	VARCHAR(10)
AS
BEGIN
	UPDATE	TB_T_REQUEST_SETTLE_D
	SET		CAP_DATE	= dbo.fn_ConvertStringToDate(@CAPDATE)
	WHERE	[GUID]		= @GUID AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB
END
GO
