DROP PROCEDURE [dbo].[sp_WFD02510_UpdateTempFileNameDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD02510_UpdateTempFileNameDetail]
(
	@DOC_NO						VARCHAR(10)		= NULL,
	@ASSET_NO					VARCHAR(15)		= NULL,
	@PHOTO_ATTACH				VARCHAR(200)	= NULL,
	@BOI_ATTACH_DOC				VARCHAR(200)	= NULL,
	@USER_BY					VARCHAR(8)		= NULL
)
	
AS
BEGIN
	UPDATE TB_R_REQUEST_DISP_D
	SET PHOTO_ATTACH	= @PHOTO_ATTACH,
	BOI_ATTACH_DOC		= @BOI_ATTACH_DOC
	WHERE DOC_NO		= @DOC_NO
	AND ASSET_NO		= @ASSET_NO	
END





GO
