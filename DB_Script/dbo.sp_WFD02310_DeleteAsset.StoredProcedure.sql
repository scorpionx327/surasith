DROP PROCEDURE [dbo].[sp_WFD02310_DeleteAsset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 09/03/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec sp_WFD02410_DeleteAssetTransfer 'c2d9080a-2039-4b00-b0de-0dc7c686b49f',null,'AS0000000400',null,'0019',null
CREATE PROCEDURE [dbo].[sp_WFD02310_DeleteAsset]
(	
	@GUID		T_GUID,
	@DOC_NO		T_DOC_NO,
	@COMPANY	T_COMPANY,
	@ASSET_NO	T_ASSET_NO,
	@ASSET_SUB	T_ASSET_SUB
)
AS
BEGIN

	IF EXISTS(SELECT 1 FROM TB_R_REQUEST_H WHERE DOC_NO = @DOC_NO)
	BEGIN
		UPDATE	TB_R_REQUEST_REPRINT
		SET		DELETE_FLAG = 'Y'
		WHERE	DOC_NO		= @DOC_NO AND
				COMPANY		= @COMPANY AND
				ASSET_NO	= @ASSET_NO AND
				ASSET_SUB	= @ASSET_SUB
		
	END

	-- delete if exists in submit mode
	DELETE 
	FROM	TB_T_SELECTED_ASSETS 
	WHERE	[GUID]		= @GUID AND 
			COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB
	
	DELETE	TB_T_REQUEST_REPRINT 
	WHERE	[GUID]		= @GUID AND 
			COMPANY		= @COMPANY AND
			ASSET_NO	= @ASSET_NO AND
			ASSET_SUB	= @ASSET_SUB

END
GO
