DROP PROCEDURE [dbo].[sp_WFD01270_SendRejectAndWaitingApproveNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_SendRejectAndWaitingApproveNotification]
@DocNo	VARCHAR(10)
AS
BEGIN
	
	DECLARE @Requestor VARCHAR(8), @RequestType	VARCHAR(1)
	SELECT	@Requestor = EMP_CODE, 
			@RequestType = REQUEST_TYPE
	FROM	TB_R_REQUEST_H WHERE DOC_NO = @DocNo

	-- Get Delegate User on time
	DECLARE @DelegatePIC VARCHAR(8)
	SELECT	@DelegatePIC = DELEGATE_PIC
	FROM	TB_M_EMPLOYEE 
	WHERE	EMP_CODE = @Requestor AND GETDATE() BETWEEN DELEGATE_FROM AND DELEGATE_TO

	-- Send Email To Approver
	DECLARE @TO VARCHAR(MAX),		@CC VARCHAR(MAX),
			@TITLE VARCHAR(255),	@MSG VARCHAR(MAX), @FOOTER VARCHAR(250),
			@Name	VARCHAR(200)
	
	DECLARE @FACode VARCHAR(3)
	SET @FACode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','FA_CODE','FA')

	IF (@Requestor = @FACode) -- Never Occur
	BEGIN
		SET		@TO = dbo.fn_GetEmailListofAdministrator() -- Include Delegate
		SET		@Name = 'FA Team'	
	END
	ELSE
	BEGIN
		SELECT	@TO = EMAIL, @Name = CONCAT(EMP_TITLE,' ', EMP_NAME,' ', LEFT(EMP_LASTNAME,1), '.')
		FROM	TB_M_EMPLOYEE 
		WHERE	EMP_CODE = @Requestor
	END
	IF @DelegatePIC IS NOT NULL
	BEGIN
		SELECT	@CC = EMAIL
		FROM	TB_M_EMPLOYEE 
		WHERE	EMP_CODE = @DelegatePIC
	END

		
	DECLARE @Req VARCHAR(100)

	SET @Req = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','REQUEST_TYPE',@RequestType)
	
	SET @TITLE	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','WFD01270_RESUBMIT')
	SET @MSG	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','WFD01270_RESUBMIT')
	SET @FOOTER = dbo.fn_GetSystemMaster('NOTIFICATION','EMAIL','FOOTER')

	SET @TITLE = dbo.fn_StringFormat(@TITLE, @DocNo )
	SET @MSG = dbo.fn_StringFormat(@MSG, CONCAT(@Req,'|',@RequestType, '|', @DocNo,'|', @Name))


	EXEC sp_Common_InsertNotification 
			@TO, @CC, NULL, @TITLE, @MSG, @FOOTER, 'I', NULL, NULL,'N', 'N', 'WFD01270', @DocNo, @Requestor

END


GO
