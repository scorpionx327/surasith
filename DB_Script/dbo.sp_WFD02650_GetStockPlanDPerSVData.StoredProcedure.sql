DROP PROCEDURE [dbo].[sp_WFD02650_GetStockPlanDPerSVData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Get stock take plan d per sv datas
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_GetStockPlanDPerSVData]
	-- Add the parameters for the stored procedure here
	@SV_EMP_CODES VARCHAR(MAX)
	, @STOCK_TAKE_KEY VARCHAR(7)
	, @IS_FAADMIN varchar(1)
AS
BEGIN
	SET NOCOUNT ON;

	select
		*
	from TB_R_STOCK_TAKE_D_PER_SV sv
	where STOCK_TAKE_KEY=@STOCK_TAKE_KEY 
		and (sv.EMP_CODE in (SELECT value from STRING_SPLIT (@SV_EMP_CODES, ',') )
				or @IS_FAADMIN = 'Y')

END





GO
