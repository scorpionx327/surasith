DROP PROCEDURE [dbo].[sp_Common_InsertLog_With_Param]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*-- ============================================= Inser System Log ==============
-- Author:			FSBT/Uten Sopradid
-- Create date:		2014/07/09(yyyy/mm/dd) 
-- Description:	 This store for insert data to tb_l_logging table
-- Update date: 2014/07/28 -----> by user: Uten Sopradid
Remark:CRI ---->R  ,WRN ---->W,INF ---->I,ERR ---->E,CFM ---->C
-- =============================================*/
CREATE PROCEDURE [dbo].[sp_Common_InsertLog_With_Param]
@APP_ID				numeric(18,0),
@STATUS				VARCHAR(1),
@MESSAGE_CODE		VARCHAR(12),
@MESSAGE_PARAM		VARCHAR(1000),
--@LEVEL				varchar(1),
@strUserID			VARCHAR(8),
@FAVORITE_FLAG		varchar(1)='N'
AS
BEGIN TRY
-- Declare variable 
--declare @FAVORITE_FLAG		varchar(1);
declare @LEVEL				varchar(1);	
declare @MESSAGE			varchar(2000);
declare @MessageText       varchar(4000);
--set @FAVORITE_FLAG='N';
SELECT @LEVEL = CASE  MESSAGE_TYPE
				   WHEN 'CRI' THEN 'R'
				   WHEN 'WRN' THEN 'W'
				   WHEN 'INF' THEN 'I'
				   WHEN 'ERR' THEN 'E'
				   WHEN 'CFM' THEN 'C'
				   ELSE ' ' END ,@MessageText=MESSAGE_TEXT
						FROM TB_M_MESSAGE
						WHERE MESSAGE_CODE=@MESSAGE_CODE ;

set @LEVEL=ISNULL(@LEVEL,'EEE')--- Cannot find MESSAGE_CODE 

if (@LEVEL='EEE')
begin
	set @MESSAGE=concat(@MESSAGE_CODE,':',@MESSAGE_PARAM)
end
else
begin

	set @MESSAGE=dbo.fn_StringFormat(@MessageText,@MESSAGE_PARAM)
	set @MESSAGE=CONCAT(@MESSAGE_CODE,' : ',@MESSAGE)
end
-------- Insert data into tb_l_logging

						INSERT INTO TB_L_LOGGING
						  (APP_ID, [STATUS], FAVORITE_FLAG, [LEVEL], [MESSAGE],DISPLAY_FLAG, CREATE_BY, CREATE_DATE)
						VALUES
						  (@APP_ID, @STATUS, @FAVORITE_FLAG, @LEVEL, @MESSAGE,'Y', @strUserID, GETDATE())
END TRY
BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );

		END CATCH











GO
