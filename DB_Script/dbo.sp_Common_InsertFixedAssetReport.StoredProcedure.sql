DROP PROCEDURE [dbo].[sp_Common_InsertFixedAssetReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Sira
-- Create date: 2017-03-20
-- Description:	Insert Fixed Asset Report Temp table
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_InsertFixedAssetReport]
	-- Add the parameters for the stored procedure here
	 @APL_ID numeric(18,0)
	,@ASSET_NO varchar(255) 
	,@COST_CENTER varchar(255) 
	,@CREATE_BY varchar(8) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO TB_T_FIXED_ASSEST_REPORT(  
 		  APL_ID
		, ASSET_NO
		, COST_CENTER
 		, CREATE_DATE
 		, CREATE_BY
 	) VALUES (  
 		  @APL_ID
 		, @ASSET_NO
 		, @COST_CENTER
 		, GETDATE()
 		, @CREATE_BY
 	)   


END



GO
