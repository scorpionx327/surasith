DROP PROCEDURE [dbo].[sp_WFD01250_UpdateApproveH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 10-03-2017
-- Description:	Update approve h data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01250_UpdateApproveH]
	@APPRV_ID	int ,
	@REQUEST_FLOW_TYPE	varchar(2) ,
	@ASSET_CLASS	T_ASSET_CLASS,
	@UPDATE_DATE	datetime ,
	@UPDATE_BY		T_SYS_USER
AS
	DECLARE @MESSAGE NVARCHAR(250);
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	if( SELECT COUNT(1) FROM TB_M_APPROVE_H WHERE APPRV_ID=@APPRV_ID and UPDATE_DATE != @UPDATE_DATE) > 0
	BEGIN

			SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'

			SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
			
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;

	END
	ELSE
	BEGIN

		UPDATE [dbo].[TB_M_APPROVE_H]
		   SET REQUEST_FLOW_TYPE	= @REQUEST_FLOW_TYPE
			  ,ASSET_CLASS	= @ASSET_CLASS
			  ,UPDATE_DATE	= GETDATE()
			  ,UPDATE_BY	= @UPDATE_BY
		 WHERE APPRV_ID		= @APPRV_ID

		 DELETE FROM TB_M_APPROVE_D WHERE APPRV_ID=@APPRV_ID 

	END

    


END
GO
