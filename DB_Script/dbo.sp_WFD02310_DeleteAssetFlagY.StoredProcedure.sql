DROP PROCEDURE [dbo].[sp_WFD02310_DeleteAssetFlagY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suphachai Leetrakool
-- Create date: 16/02/2017
-- Description:	Search Fixed assets screen
-- =============================================
--exec WFD02210_ValidationBeforeSubmitAssetCIP '5cf4749b-d6e0-4ad9-a71c-a91fcba2f14',null,''
CREATE PROCEDURE [dbo].[sp_WFD02310_DeleteAssetFlagY]
(	
	@DOC_NO		T_DOC_NO,
	@USER		T_SYS_USER
)
AS
BEGIN

	--=============== UPDATE TB_R_REQUEST_REPRINT ===============--

	DELETE
	FROM	TB_T_DELETED_ASSETS
	WHERE	DOC_NO = @DOC_NO
	INSERT
	INTO	TB_T_DELETED_ASSETS
	(		COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	COST_CODE)
	select	COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	NULL
	FROM	TB_R_REQUEST_REPRINT
	WHERE	DOC_NO	= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	EXEC sp_WFD01170_GenerateCommentDeleted @DOC_NO, @USER, 'A'
	-------------------------------------------------------------
	-- Unlock process status when assets is deleted from request
	UPDATE	H
	SET		H.PROCESS_STATUS	= NULL,
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_REPRINT M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO		= @DOC_NO AND
			M.DELETE_FLAG	= 'Y'
	
	UPDATE	H
	SET		H.PROCESS_STATUS	= NULL,
			H.UPDATE_DATE		= GETDATE(),
			H.UPDATE_BY			= @USER 
	FROM	TB_M_ASSETS_H H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_REPRINT M WITH(NOLOCK)
	ON		M.COMPANY = H.COMPANY AND M.ASSET_NO = H.ASSET_NO AND M.ASSET_SUB = H.ASSET_SUB
	WHERE	M.DOC_NO		= @DOC_NO AND
			M.DELETE_FLAG	= 'Y'

	-- Delete assets from request
	DELETE	TB_R_REQUEST_REPRINT 
	WHERE	DOC_NO		= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	DELETE	H
	FROM	TB_R_ASSET_HIS H WITH(NOLOCK)
	INNER	JOIN
			TB_R_REQUEST_REPRINT C WITH(NOLOCK)
	ON		H.ASSET_NO		= C.ASSET_NO AND
			H.COST_CODE		= C.COST_CODE AND
			H.DOC_NO		= C.DOC_NO
	WHERE	C.DOC_NO		= @DOC_NO AND
			C.DELETE_FLAG	= 'Y'

END
GO
