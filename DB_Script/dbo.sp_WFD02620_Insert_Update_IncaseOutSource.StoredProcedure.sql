DROP PROCEDURE [dbo].[sp_WFD02620_Insert_Update_IncaseOutSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*-- ========== SFAS =====BFD0261A : Upload Asset No. for  Stock Taking========================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/02/22 (yyyy/mm/dd)
- ============================================= SFAS =============================================*/
CREATE PROCEDURE   [dbo].[sp_WFD02620_Insert_Update_IncaseOutSource]
(
	@UserID VARCHAR(8),
    @StockYear VARCHAR(4),
    @StockRound VARCHAR(2),
    @StockAssetLocation VARCHAR(1), --I is In house,O is out-source
    @DATA_AS_OF DATETIME,
	@ASSET_STATUS VARCHAR(1),
    @SUB_TYPE VARCHAR(150)
)
AS
BEGIN TRY
	DECLARE @Error BIT;
	SET @Error =0;

IF OBJECT_ID('tempdb..#TEMP_SUPTYPE') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_SUPTYPE
END
	SELECT SUB_TYPE
	 INTO #TEMP_SUPTYPE
	FROM TB_M_ASSETS_H
	WHERE 1<>1 

	    INSERT INTO #TEMP_SUPTYPE(SUB_TYPE)
		SELECT VALUE AS SUB_TYPE
		FROM STRING_SPLIT (@SUB_TYPE, ',') 


-- 2  Insert into   TB_R_STOCK_TAKE_D
   INSERT INTO TB_R_STOCK_TAKE_D([STOCK_TAKE_KEY] ,[YEAR] ,[ROUND]    ,[ASSET_LOCATION]    ,[SV_EMP_CODE]
           ,[ASSET_NO]     ,[ASSET_NAME]  ,[EMP_CODE]     ,[COST_CODE]       ,[COST_NAME]      ,[PLANT_CD]
           ,[PLANT_NAME]       ,[SUPPLIER_NAME]       ,[BARCODE]        ,[SUB_TYPE]        ,[CHECK_STATUS]
           ,[SCAN_DATE]           ,[START_COUNT_TIME]           ,[END_COUNT_TIME]           ,[COUNT_TIME]
           ,[LOCATION_NAME]           ,[CREATE_DATE]           ,[CREATE_BY]           ,[UPDATE_DATE]           ,[UPDATE_BY]
		   ,SERIAL_NO,	MODEL,	PRODUCTION_PART_NO,	TOOLING_COST) -- Add new column Support CR#1 2017-06-01
 SELECT CONCAT(@StockYear,@StockRound,@StockAssetLocation) ,@StockYear ,@StockRound,@StockAssetLocation
           , AssetH.SUPPLIER_NO  -- <SV_EMP_CODE, varchar(8),>
            ,AssetH.ASSET_NO  -- <ASSET_NO, varchar(15),>
            ,AssetH.ASSET_NAME --<ASSET_NAME, varchar(200),>
            ,NULL -- <EMP_CODE, varchar(8),>
          , (SELECT TOP (1) COST_CODE   
		 		FROM TB_M_ASSETS_D AssetD WHERE AssetH.ASSET_NO=AssetD.ASSET_NO ORDER BY UNIT DESC 
				) AS COST_CODE --<COST_CODE, varchar(8),>  from detail table 
           , NULL -- <COST_NAME, varchar(50),>
           , NULL -- <PLANT_CD, varchar(3),>
           ,NULL -- <PLANT_NAME, varchar(50),>
           , SUPPLIER_NAME ---<SUPPLIER_NAME, varchar(50),>
           ,BARCODE --<BARCODE, varchar(19),>
           ,SUB_TYPE --<SUB_TYPE, varchar(3),>
           ,'N' --- <CHECK_STATUS, varchar(19),>
           ,NULL -- <SCAN_DATE, datetime,>
           ,NULL --  <START_COUNT_TIME, datetime,>
           ,NULL -- <END_COUNT_TIME, datetime,>
           ,NULL -- <COUNT_TIME, int,>
           ,LOCATION_NAME --<LOCATION_NAME, varchar(200),>
           ,GETDATE(), @UserID,  GETDATE(), @UserID
		   ,AssetH.SERIAL_NO,	AssetH.MODEL,	AssetH.PRODUCTION_PART_NO,	AssetH.TOOLING_COST -- Add new column Support CR#1 2017-06-01
 FROM TB_M_ASSETS_H AssetH
 WHERE AssetH.DATE_IN_SERVICE <= @DATA_AS_OF
		AND AssetH.STATUS='Y'
		AND AssetH.INHOUSE_FLAG='N'
		AND ((RIGHT(AssetH.ASSET_NO, 2)  = '00' AND @ASSET_STATUS='P')
				OR (RIGHT(AssetH.ASSET_NO, 2)  <> '00' AND @ASSET_STATUS='C')
				OR @ASSET_STATUS='B' )
		AND AssetH.SUPPLIER_NO is not null
		AND AssetH.SUB_TYPE IN ( SELECT  SUB_TYPE
								 FROM #TEMP_SUPTYPE)
 
 			
 -- 2.1 Update Cost center detial 
  UPDATE StockD
   SET StockD.COST_NAME =M_Cost.COST_NAME
   , StockD.PLANT_CD =M_Cost.PLANT_CD
   , StockD.PLANT_NAME =M_Cost.PLANT_NAME   
  FROM TB_R_STOCK_TAKE_D StockD
  LEFT JOIN TB_M_COST_CENTER M_Cost ON StockD.COST_CODE=M_Cost.COST_CODE
  WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)


 --3  Insert into   TB_R_STOCK_TAKE_D_PER_SV
 INSERT INTO TB_R_STOCK_TAKE_D_PER_SV
           ([STOCK_TAKE_KEY] ,[YEAR] ,[ROUND]   ,[ASSET_LOCATION]  ,[EMP_CODE]  ,[EMP_TITLE]
           ,[EMP_NAME]  ,[EMP_LASTNAME]    ,[FA_DATE_FROM] ,[FA_DATE_TO]
           ,[DATE_FROM]  ,[DATE_TO]  ,[TIME_START]  ,[TIME_END]
           ,[TIME_MINUTE]   ,[USAGE_HANDHELD]   ,[IS_LOCK]  ,[TOTAL_ASSET]  
		    ,[CREATE_DATE]   ,[CREATE_BY]   ,[UPDATE_DATE]    ,[UPDATE_BY])
SELECT  CONCAT(@StockYear,@StockRound,@StockAssetLocation) ,@StockYear ,@StockRound,@StockAssetLocation
            , SV_EMP_CODE--<EMP_CODE, varchar(8),>
            ,NULL -- <EMP_TITLE, varchar(8),>
            ,SUPPLIER_NAME -- <EMP_NAME, varchar(30),>
            ,NULL -- <EMP_LASTNAME, nvarchar(30),>
            ,NULL --<FA_DATE_FROM, date,>
            ,NULL --<FA_DATE_TO, date,>
            ,NULL --<DATE_FROM, date,>
            ,NULL --<DATE_TO, date,>
            ,NULL --<TIME_START, varchar(5),>
            ,NULL --<TIME_END, varchar(5),>
            ,0 --<TIME_MINUTE, int,>
            ,NULL --<USAGE_HANDHELD, int,>
            ,'N' -- <IS_LOCK, varchar(1),>
            ,COUNT(ASSET_NO ) --<TOTAL_ASSET, int,>
            ,GETDATE(), @UserID,  GETDATE(), @UserID 
FROM TB_R_STOCK_TAKE_D StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
GROUP BY SV_EMP_CODE,SUPPLIER_NAME


 --4 Update  value in  TB_R_STOCK_TAKE_H
  DECLARE @ALL_ASSET_TOTAL INT  = 0
  DECLARE @MC_TOTAL INT = 0
  DECLARE @EC_TOTAL INT = 0 
  DECLARE @PC_TOTAL INT = 0
  DECLARE @LC_TOTAL INT = 0

SELECT @ALL_ASSET_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)

SELECT @MC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='M/C'

SELECT @EC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='E/C'

SELECT @PC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='P/C'

SELECT @LC_TOTAL =COUNT(*) 
FROM TB_R_STOCK_TAKE_D  StockD
WHERE StockD.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)
AND SUB_TYPE ='L/C'

 UPDATE StockH
 SET  
	  StockH.ALL_ASSET_TOTAL = @ALL_ASSET_TOTAL , 
     StockH.MC_TOTAL = @MC_TOTAL , 
     StockH.EC_TOTAL = @EC_TOTAL , 
     StockH.PC_TOTAL = @PC_TOTAL , 
     StockH.LC_TOTAL = @LC_TOTAL 
 FROM TB_R_STOCK_TAKE_H StockH 
 WHERE StockH.STOCK_TAKE_KEY =CONCAT(@StockYear,@StockRound,@StockAssetLocation)



IF OBJECT_ID('tempdb..#TEMP_SUPTYPE') IS NOT NULL
BEGIN
		DROP TABLE #TEMP_SUPTYPE
END

	RETURN @Error ;
END TRY
BEGIN CATCH
			       PRINT concat('ERROR Message:',ERROR_MESSAGE())
				
END CATCH







GO
