DROP PROCEDURE [dbo].[sp_WFD02650_GetEmployeeWorkData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 02-03-2017
-- Description:	Login from handheld
-- Update by : Parichart coding by  uten 
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02650_GetEmployeeWorkData]
	-- Add the parameters for the stored procedure here
	@EMP_CODE varchar(8)
AS
	DECLARE @STOCK_TALE_KEY nvarchar(7)
	DECLARE @ASSET_LOCATION nvarchar(1)
BEGIN
	SET NOCOUNT ON;
	DECLARE @SV_EMP_CODE VARCHAR(MAX)
	DECLARE @COST_CODE VARCHAR(100)

	SELECT TOP 1 @STOCK_TALE_KEY = STOCK_TAKE_KEY,
				 @ASSET_LOCATION =ASSET_LOCATION
	FROM TB_R_STOCK_TAKE_H
    WHERE PLAN_STATUS in ('C','S') 
	order by CREATE_DATE desc


IF (@ASSET_LOCATION='O')
BEGIN
	IF( SELECT TOP 1 FAADMIN FROM TB_M_EMPLOYEE WHERE EMP_CODE=@EMP_CODE ) = 'Y'
	BEGIN
		SELECT 
			top 1
			e.EMP_CODE as EMP_NO
			,e.EMP_TITLE
			,e.EMP_NAME
			,e.EMP_LASTNAME
			,e.FAADMIN as IS_FAADMIN
			,'N' as IS_SV
			,'' as SV_EMP_CODE
		INTO #tmpTable 
		FROM TB_M_EMPLOYEE e
		WHERE EMP_CODE=@EMP_CODE

		SELECT
			(SELECT TOP 1  EMP_NO FROM #tmpTable) as EMP_NO
			,(SELECT TOP 1  EMP_TITLE FROM #tmpTable) as EMP_TITLE
			,(SELECT TOP 1  EMP_NAME FROM #tmpTable) as EMP_NAME
			,(SELECT TOP 1  EMP_LASTNAME FROM #tmpTable) as EMP_LASTNAME
			,sv.EMP_CODE as COST_CODE
			,sv.EMP_NAME as COST_NAME
			,(SELECT TOP 1  IS_FAADMIN FROM #tmpTable) as IS_FAADMIN 
			,'N' as IS_SV
			,sv.EMP_CODE as SV_EMP_CODE
			,FORMAT(h.UPDATE_DATE,'ddMMyyyyHHmmssfff') as UPDATE_DATE
		FROM TB_R_STOCK_TAKE_D_PER_SV sv 
		LEFT JOIN (SELECT STOCK_TAKE_KEY,UPDATE_DATE 
		           FROM  TB_R_STOCK_TAKE_H 
				     WHERE STOCK_TAKE_KEY=@STOCK_TALE_KEY
						)h ON sv.STOCK_TAKE_KEY=h.STOCK_TAKE_KEY
		WHERE sv.STOCK_TAKE_KEY=@STOCK_TALE_KEY

		DROP TABLE #tmpTable

	END
END
IF (@ASSET_LOCATION='I')
BEGIN
  
					  IF EXISTS( SELECT 1 FROM TB_R_STOCK_TAKE_D_PER_SV 
								  WHERE STOCK_TAKE_KEY=@STOCK_TALE_KEY
										AND EMP_CODE=@EMP_CODE )
					  BEGIN
						  SELECT @SV_EMP_CODE = EMP_CODE 
						  FROM TB_R_STOCK_TAKE_D_PER_SV 
								  WHERE STOCK_TAKE_KEY=@STOCK_TALE_KEY
										AND EMP_CODE=@EMP_CODE
					  END

					DECLARE @GLCode VARCHAR(3)
					SET @GLCode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','GL_CODE','GL')

					DECLARE @Length INT
					SET @Length = CONVERT(INT, dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPROVE_CHECK_LENGTH',@GLCode))

					DECLARE @TB_T_COST AS TABLE (COST_CODE VARCHAR(8))

					 IF(@SV_EMP_CODE IS NULL)
					 BEGIN
						
						

					   --- Login User is GL ?
						IF NOT EXISTS (SELECT 1 FROM TB_M_EMPLOYEE WHERE EMP_CODE=@EMP_CODE AND ACTIVEFLAG='Y' 
									AND [ROLE]= @GLCode)
						BEGIN
							RETURN
						END

							DECLARE @OrgCode VARCHAR(21)	

							SELECT	TOP 1 @OrgCode = ORG_CODE
							FROM	TB_M_EMPLOYEE 
							WHERE	EMP_CODE = @EMP_CODE AND ACTIVEFLAG = 'Y' 
							-- Get Length of GL
							
							PRINT @OrgCode
							
							-- Get Cost Center
							INSERT
							INTO	@TB_T_COST
							SELECT	DISTINCT D.COST_CODE
							FROM	TB_M_EMPLOYEE E INNER	JOIN
									TB_R_STOCK_TAKE_D D
							ON		E.COST_CODE = D.COST_CODE
							WHERE	D.STOCK_TAKE_KEY = @STOCK_TALE_KEY AND
									LEFT(ORG_CODE, @Length) = LEFT(@OrgCode, @Length) AND
									ACTIVEFLAG = 'Y'
							
						
							
							IF NOT EXISTS(SELECT 1	FROM TB_R_STOCK_TAKE_D D
											INNER JOIN @TB_T_COST C ON D.COST_CODE = C.COST_CODE
											WHERE	STOCK_TAKE_KEY = @STOCK_TALE_KEY)
							BEGIN
								PRINT 'No GL'
								RETURN ;
							END

							--find sv user 
				 			SELECT	@SV_EMP_CODE =  COALESCE(@SV_EMP_CODE + '|', '') + SV_EMP_CODE
							FROM	TB_R_STOCK_TAKE_D D
							INNER	JOIN @TB_T_COST C 
							ON		D.COST_CODE		= C.COST_CODE
							WHERE	STOCK_TAKE_KEY	= @STOCK_TALE_KEY
							GROUP	BY SV_EMP_CODE
							ORDER	BY	SV_EMP_CODE
							
						
						
					 END

					IF(@SV_EMP_CODE IS NULL)
					 BEGIN
					  --- No data incase user no in plan
						RETURN ;
					 END

					 --======================= ---- Load data per SV --------- =========================================
					 -- get gl data
					 DECLARE @TB_T_EMP AS TABLE
					 (
						EMP_NO			VARCHAR(8),
						EMP_TITLE		VARCHAR(8),
						EMP_NAME		VARCHAR(30),
						EMP_LASTNAME	VARCHAR(30),
						COST_CODE		VARCHAR(8),
						COST_NAME		VARCHAR(50),
						IS_FAADMIN		VARCHAR(1),
						IS_SV			VARCHAR(1),
						SV_EMP_CODE		VARCHAR(8),
						UPDATE_DATE		VARCHAR(17)
					 )

					 -- Get Cost Code of relate with SV

					INSERT
					INTO	@TB_T_COST
					SELECT	DISTINCT COST_CODE
					FROM	TB_R_STOCK_TAKE_D 
					WHERE	STOCK_TAKE_KEY	= @STOCK_TALE_KEY AND 
							SV_EMP_CODE		= @SV_EMP_CODE

					-- Get Org Code of selected Cost Center
					INSERT
					INTO	@TB_T_EMP
					(		EMP_NO,		EMP_TITLE,		EMP_NAME,		EMP_LASTNAME,
							COST_CODE,	COST_NAME, 
							IS_FAADMIN,	IS_SV,			SV_EMP_CODE,	UPDATE_DATE )
					SELECT	E.EMP_CODE,	E.EMP_TITLE,	E.EMP_NAME,		E.EMP_LASTNAME,
							M.COST_CODE,NULL,
							 'N',		'N',			'',	''
					FROM	TB_M_EMPLOYEE E
					INNER	JOIN
					(
							SELECT	M.ORG_CODE, M.COST_CODE
							FROM	TB_M_EMPLOYEE M
							INNER	JOIN
									@TB_T_COST T
							ON		M.COST_CODE = T.COST_CODE
							WHERE	ACTIVEFLAG = 'Y'
							GROUP	BY
									M.ORG_CODE, M.COST_CODE
					) M
					ON		LEFT(E.ORG_CODE, @Length) = LEFT(M.ORG_CODE,@Length) AND
							E.ROLE	= @GLCode AND
							E.ACTIVEFLAG	= 'Y'
					GROUP	BY
							E.EMP_CODE,	E.EMP_TITLE,	E.EMP_NAME,		E.EMP_LASTNAME,
							M.COST_CODE
					-- Update SV
					UPDATE	T
					SET		T.SV_EMP_CODE = S.EMP_CODE
					FROM	@TB_T_EMP T
					INNER	JOIN
							TB_M_SV_COST_CENTER S
					ON		T.COST_CODE = S.COST_CODE

					
					--SV
					INSERT
					INTO	@TB_T_EMP
					(		EMP_NO,		EMP_TITLE,		EMP_NAME,		EMP_LASTNAME,
							COST_CODE,	COST_NAME, 
							IS_FAADMIN,	IS_SV,			SV_EMP_CODE,	UPDATE_DATE )
					SELECT	E.EMP_CODE,	E.EMP_TITLE,	E.EMP_NAME,		E.EMP_LASTNAME,
							D.COST_CODE,D.COST_NAME,
							'N',		'Y',			D.SV_EMP_CODE,	''
					FROM	TB_R_STOCK_TAKE_D D
					INNER	JOIN
							TB_M_EMPLOYEE E
					ON		D.SV_EMP_CODE		= E.EMP_CODE
					WHERE	D.STOCK_TAKE_KEY	= @STOCK_TALE_KEY AND
							D.SV_EMP_CODE		IN (SELECT value from STRING_SPLIT (@SV_EMP_CODE, '|') ) AND
							E.ACTIVEFLAG		= 'Y'
					GROUP	BY
							E.EMP_CODE,	E.EMP_TITLE,	E.EMP_NAME,		E.EMP_LASTNAME,
							D.COST_CODE,D.COST_NAME,
							D.SV_EMP_CODE
					
					-- Get Update Date
					DECLARE @UpdateDate VARCHAR(17)
					SELECT	@UpdateDate = FORMAT(H.UPDATE_DATE,'ddMMyyyyHHmmssfff')
					FROM	TB_R_STOCK_TAKE_H H
					WHERE	STOCK_TAKE_KEY = @STOCK_TALE_KEY

					-- Update Cost Code
					UPDATE	T
					SET		T.COST_NAME = C.COST_NAME,
							T.UPDATE_DATE	= @UpdateDate
					FROM	@TB_T_EMP T
					INNER	JOIN
							TB_M_COST_CENTER C
					ON		T.COST_CODE = C.COST_CODE
					
					SELECT	*
					FROM	@TB_T_EMP

					END

END




GO
