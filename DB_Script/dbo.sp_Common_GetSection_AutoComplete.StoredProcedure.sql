DROP PROCEDURE [dbo].[sp_Common_GetSection_AutoComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetSection_AutoComplete]
@keyword		T_COST_CODE,
@EmpCode		T_SYS_USER,
@DepKeyword		varchar(200)
AS
BEGIN
		/*SELECT	SECTION_CODE,
				SECTION_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(	
					SECTION_CODE LIKE CONCAT(@keyword,'%') 
					OR
					SECTION_NAME LIKE CONCAT(@keyword,'%') 
				) 
		AND		(	
					DEPT_CODE LIKE CONCAT(@DepKeyword,'%') 
					OR
					DEPT_NAME LIKE CONCAT(@DepKeyword,'%') 
				) 
		GROUP by SECTION_CODE, SECTION_NAME
		ORDER BY SECTION_CODE ASC*/

		SELECT	SECTION_NAME
		FROM	TB_M_ORGANIZATION 
		WHERE	(						
					SECTION_NAME LIKE CONCAT(@keyword,'%') 
				) 
		AND		(						
					DEPT_NAME LIKE CONCAT(@DepKeyword,'%') 
				) 
		GROUP by SECTION_NAME
		ORDER BY SECTION_NAME ASC
END
GO
