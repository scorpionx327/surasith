DROP PROCEDURE [dbo].[sp_WFD02110_UpdateAssetBarcode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD02110_UpdateAssetBarcode]
@COMPANY	T_COMPANY,
@AssetNo	T_ASSET_NO,
@AssetSub	T_ASSET_SUB
AS
BEGIN
	/*
	-- Modified for support BARCODE IS NULL should be Generate barcode. by Pawares M. 20181011

	--IF NOT EXISTS(SELECT 1 FROM TB_M_ASSETS_H WHERE ASSET_NO = BARCODE AND ASSET_NO = @AssetNo)
	--	BEGIN
	--		-- Bar Code is changed.
	--		RETURN
	--	END

	IF EXISTS(SELECT 1 FROM	TB_M_ASSETS_H WHERE COMPANY = @COMPANY AND ASSET_NO = @AssetNo AND ASSET_SUB = @AssetSub AND BARCODE IS NOT NULL)
	BEGIN
		IF EXISTS(SELECT 1 FROM TB_M_ASSETS_H WHERE COMPANY = @COMPANY AND ASSET_NO = @AssetNo AND ASSET_SUB = @AssetSub AND PLATE_TYPE = 'P' )
		BEGIN
			GOTO CONTINUE_PROCESS
		END

		IF NOT EXISTS(SELECT 1 FROM TB_M_ASSETS_H WHERE COMPANY = @COMPANY AND ASSET_NO = @AssetNo AND ASSET_SUB = @AssetSub)
		BEGIN
			-- Bar Code is changed.
			RETURN
		END
	END

CONTINUE_PROCESS:

		IF EXISTS(SELECT 1 FROM TB_M_ASSETS_H WHERE COMPANY = @COMPANY AND ASSET_NO = @AssetNo AND ASSET_SUB = @AssetSub AND PLATE_TYPE = 'P')
		BEGIN
			RETURN
		end
	-- #######################################################################################################
	*/
	DECLARE @B VARCHAR(17)
	SET @B = dbo.fn_GetBarCodeOfAsset(@COMPANY, @AssetNo, @AssetSub)
	--  [dbo].[fn_GetBarCodeOfAsset] (@ASSET_NO VARCHAR(15))
	UPDATE	TB_M_ASSETS_H
	SET		BARCODE = @B
	WHERE	ASSET_NO = @AssetNo

	-- Check STock Taking
UPDATE	D
	SET		BARCODE = @B
	FROM	TB_R_STOCK_TAKE_D D
	INNER	JOIN
			TB_R_STOCK_TAKE_H H
	ON		D.STOCK_TAKE_KEY = H.STOCK_TAKE_KEY
	WHERE	H.PLAN_STATUS IN ('S','C','D') AND --Add status 'D' by Phitak K.
			D.COMPANY	= @COMPANY AND
			D.ASSET_NO = @AssetNo AND -- CHECK_STATUS = 'N' AND comment by Phitak K.
			D.ASSET_SUB	= @AssetSub AND
			D.BARCODE = @AssetNo
	




END
GO
