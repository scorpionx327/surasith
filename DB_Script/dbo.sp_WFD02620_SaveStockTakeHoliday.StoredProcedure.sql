DROP PROCEDURE [dbo].[sp_WFD02620_SaveStockTakeHoliday]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 15-02-2017
-- Description:	Save stock take  holiday
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_SaveStockTakeHoliday]
	-- Add the parameters for the stored procedure here
	@HOLIDAY_DATES		nvarchar(max)	
	, @STOCK_TAKE_KEY		varchar(7)
AS
	DECLARE @tb_holiday table (
		H_DAY	date
	);
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- /////////////////////////////////////////////
	-- Update TB_R_STOCK_TAKE_HOLIDAY

	-- Split holiday string to data table for insert 
	insert into @tb_holiday
	select CONVERT(DATE, value) as H_DAY FROM STRING_SPLIT(@HOLIDAY_DATES, ',')

	-- Delete old data
	DELETE FROM TB_R_STOCK_TAKE_HOLIDAY WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY

	-- Add datas
	INSERT INTO TB_R_STOCK_TAKE_HOLIDAY
	SELECT @STOCK_TAKE_KEY as STOCK_TAKE_KEY, H_DAY  as HOLIDATE_DATE
	FROM @tb_holiday

END




GO
