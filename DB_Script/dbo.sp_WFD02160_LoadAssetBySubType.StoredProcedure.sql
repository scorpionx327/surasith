DROP PROCEDURE [dbo].[sp_WFD02160_LoadAssetBySubType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*-- ========== SFAS =====WFD02160 : Fixed Asset Map Location ==================================
-- Author:     FTH/Uten Sopradid 
-- Create date:  2017/03/11 (yyyy/mm/dd)
 exec sp_WFD02160_LoadAssetBySubType 'E03DM102',null
*/
CREATE PROCEDURE [dbo].[sp_WFD02160_LoadAssetBySubType]
( 
   @COST_CODE AS VARCHAR(8),
   @SUB_TYPE AS VARCHAR(3) ---> Text is All,M/C,E/C,L/C or P/C 
 )
AS
BEGIN TRY


DECLARE @CAPITALIZED VARCHAR(40) 
SET @CAPITALIZED = dbo.fn_GetSystemMaster('FAS_TYPE','ASSET_TYPE','CAPITALIZED')
			

 --SELECT ROW_NUMBER()  OVER(ORDER BY AssetH.SUB_TYPE_SORT ASC, AssetH.MODEL ASC, AssetH.ASSET_NO ASC) AS ITEMS
 SELECT D.ITEM_SEQ ITEMS
					   , AssetH.ASSET_CATEGORY
					   , ISNULL(AssetH.MODEL, AssetH.ASSET_NO) AS MACHINE_NUMBER
					   , ISNULL(AssetH.LOCATION_NAME,'') AS  LOCATION_NAME --- For tooltip
					   , AssetH.ASSET_NO
					   , AssetH.SUB_TYPE
					   , CASE WHEN (Loc.X IS NOT NULL) AND   (Loc.Y IS NOT NULL)  THEN 'Y'
					    ELSE 'N' END AS SELECTED 
					   ,CASE 
					    WHEN AssetH.BOI_FLAG = 'Y' and AssetH.LICENSE_FLAG='Y' THEN 'red'
						WHEN AssetH.BOI_FLAG = 'Y'  THEN 'yellow'
						WHEN AssetH.LICENSE_FLAG='Y' THEN 'orange'
						ELSE '' END AS BG_COLOR
						,Loc.X AS X_POINT ,Loc.Y  AS Y_POINT
						,Loc.COST_CODE
		FROM (SELECT  ASSET_CATEGORY, ASSET_NO,MODEL ,LOCATION_NAME,MAP_STATUS,SUB_TYPE , S.REMARKS AS SUB_TYPE_SORT
		                ,H.BOI_FLAG,H.LICENSE_FLAG
				   FROM  TB_M_ASSETS_H H 
				   LEFT JOIN 
						   ( SELECT CODE,REMARKS -- Remark is field for sorting  
							 FROM TB_M_SYSTEM 
							 WHERE CATEGORY='FAS_TYPE' AND SUB_CATEGORY='SUB_TYPE' AND ACTIVE_FLAG='Y' 
							 )S ON H.SUB_TYPE=S.CODE
					WHERE H.SUB_TYPE IN (  SELECT [VALUE] 
											FROM TB_M_SYSTEM
											WHERE CATEGORY='LOC_MAP' AND SUB_CATEGORY='SUB_TYPE' AND ACTIVE_FLAG='Y' 
										)
				            AND H.STATUS='Y' 
							AND H.ASSET_TYPE = @CAPITALIZED -- Add By Surasith T. 2017-06-08 Filter only CAP Change Spec
							AND EXISTS (SELECT 1 
										FROM TB_M_ASSETS_D D
										WHERE D.ASSET_NO=H.ASSET_NO
												AND D.COST_CODE= @COST_CODE
											)
						AND NOT EXISTS (SELECT	1
								FROM	dbo.fn_FD0GetAdminAsset() A
								WHERE	A.ASSET_CATEGORY = H.ASSET_CATEGORY)		
															
			  )AssetH 
	   INNER JOIN TB_M_ASSETS_LOC Loc ON Loc.ASSET_NO=AssetH.ASSET_NO
	   LEFT JOIN TB_M_ASSETS_D D ON AssetH.ASSET_NO	= D.ASSET_NO
												AND D.COST_CODE		= @COST_CODE
       WHERE  (( UPPER(AssetH.SUB_TYPE) = UPPER(@SUB_TYPE)) OR (@SUB_TYPE IS NULL OR UPPER(@SUB_TYPE) = 'ALL' ))
	   ORDER BY ITEMS


END TRY
BEGIN CATCH
	
	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH




GO
