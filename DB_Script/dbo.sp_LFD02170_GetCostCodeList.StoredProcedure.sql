DROP PROCEDURE [dbo].[sp_LFD02170_GetCostCodeList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_GetCostCodeList]
@EMP_CODE T_SYS_USER
AS
BEGIN	
	SET NOCOUNT ON;

	    SELECT	COMPANY, COST_CODE 
		FROM	dbo.FN_FD0COSTCENTERLIST (@EMP_CODE)
		ORDER	BY COST_CODE 

END
GO
