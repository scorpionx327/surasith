DROP PROCEDURE [dbo].[sp_WFD01250_GetDefaultFlow]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01250_GetDefaultFlow]
@REQUEST_FLOW_TYPE VARCHAR(2)
AS
BEGIN
	DECLARE @RequestType	VARCHAR(1)
	SELECT	@RequestType = REMARKS
	FROM	TB_M_SYSTEM
	WHERE	CATEGORY		= 'SYSTEM_CONFIG' AND 
			SUB_CATEGORY	= 'REQUEST_FLOW_TYPE' AND
			CODE			= @REQUEST_FLOW_TYPE AND
			ACTIVE_FLAG		= 'Y'
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT *
		FROM	TB_M_SYSTEM
		WHERE	1 = 2
		RETURN
	END

	SELECT	*
	FROM	TB_M_SYSTEM
	WHERE	CATEGORY		= 'APPROVE_FLOW' AND
			SUB_CATEGORY	= CONCAT('DEFAULT_FLOW_',@RequestType) AND
			ACTIVE_FLAG		= 'Y'
	ORDER	BY
			CONVERT(INT, CODE)
	
END
GO
