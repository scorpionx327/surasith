DROP PROCEDURE [dbo].[sp_WFD021A4_AddNewApprover]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD021A4_AddNewApprover]
@DOC_NO			T_DOC_NO,
@CurrentIndx	INT,
@NewIndx		INT,
@Flag			VARCHAR(1) -- Y : Success Email, N : Error Mail
AS
BEGIN
	INSERT
	INTO	TB_R_REQUEST_APPR
	(		DOC_NO,				INDX,			APPR_ROLE,			ACTUAL_ROLE,			EMP_CODE,
			EMP_TITLE,			EMP_NAME,		EMP_LASTNAME,		EMAIL,					APPRV_GROUP,
			DIVISION,
			NOTIFICATION_MODE,
			[OPERATION_MODE]	,
			REQUIRE_FLAG,		FINISH_FLOW,	GEN_FILE,			ALLOW_DEL_ITEM,			ALLOW_REJECT,
			ALLOW_SEL_APPRV,	HIGHER_APPR,	REJECT_TO,			APPR_STATUS,			APPR_DATE,
			POS_CODE,			POS_NAME,		DEPT_CODE,			DEPT_NAME,				DIV_NAME,
			[GUID],				ORG_CODE,
			CREATE_DATE,		CREATE_BY,		UPDATE_DATE,		UPDATE_BY
	)
	SELECT	DOC_NO,				@NewIndx,		APPR_ROLE,			ACTUAL_ROLE,			EMP_CODE,
			EMP_TITLE,			EMP_NAME,		EMP_LASTNAME,		EMAIL,					APPRV_GROUP,
			DIVISION,
			NOTIFICATION_MODE,
			[OPERATION_MODE]	,
			REQUIRE_FLAG,		FINISH_FLOW,	GEN_FILE,			ALLOW_DEL_ITEM,			ALLOW_REJECT,
			ALLOW_SEL_APPRV,	HIGHER_APPR,	REJECT_TO,			'W',					NULL,
			POS_CODE,			POS_NAME,		DEPT_CODE,			DEPT_NAME,				DIV_NAME,
			[GUID],				ORG_CODE,
			GETDATE(),			'System',		GETDATE(),		'System'
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO	= @DOC_NO AND
			INDX	= @CurrentIndx

	-- Send Email to Next Approver
END
GO
