/****** Object:  StoredProcedure [dbo].[sp_WFD01130_GetRequestHistory]    Script Date: 2019/10/14 8:49:54 PM ******/
DROP PROCEDURE [dbo].[sp_WFD01130_GetRequestHistory]
GO
/****** Object:  StoredProcedure [dbo].[sp_WFD01130_GetRequestHistory]    Script Date: 2019/10/14 8:49:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 02/02/2017r
-- Description:	Search Fixed assets history inquiry screen
/*
Nipon 2019-08-07 09:44:34.570 --> Add COMPANY , ASSET_SUB
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01130_GetRequestHistory] @COMPANY varchar(50) = null--for TDEM#TMT#STM not use varchar(10)
, @ASSET_SUB T_ASSET_SUB = null
, @EMP_CODE T_SYS_USER
, @IS_SYS_ADMIN VARCHAR(1) --- Y is sytem admin, N is normal user
, @ASSET_NO T_ASSET_NO
, @TRANSACTION_TYPE VARCHAR(1)
, @DATE_SERVICE_FROM VARCHAR(10)	
, @DATE_SERVICE_TO VARCHAR(19)	
, @DOC_NO T_DOC_NO -- Spare for % %
, @STATUS VARCHAR(2)
, @CHKDATE varchar(1)
, @pageNum INT
, @pageSize INT
, @sortColumnName VARCHAR(50)
, @orderType VARCHAR(5)
, @TOTAL_ITEM		int output
AS


SET @DOC_NO = dbo.fn_GetSearchCriteria(@DOC_NO) 


DECLARE @IS_FA_ADMIN VARCHAR(1) ='N'

SELECT @IS_FA_ADMIN=FAADMIN
FROM TB_M_EMPLOYEE  WHERE SYS_EMP_CODE=@EMP_CODE

--begin check date
DECLARE @StartDate DateTime, @EndDate DateTime
	IF @CHKDATE = '0'
	BEGIN
	    IF @StartDate IS NULL
		BEGIN
		SET @StartDate	= '1900-01-01'
		END
		IF @EndDate IS NULL
		BEGIN
		SET @EndDate	= '9999-12-31'
		END
	END
	ELSE
	BEGIN
	/*
		SET @StartDate	= CONVERT(DATE,@DATE_SERVICE_FROM);
		SET @EndDate	=  CONVERT(DATE,@DATE_SERVICE_TO);
		*/
		SET @StartDate	= dbo.fn_ConvertStringToDateTime(@DATE_SERVICE_FROM);
		SET @EndDate	=  dbo.fn_ConvertStringToDateTime(@DATE_SERVICE_TO); 
	END
-- end check date
BEGIN

--begin modify by thanapon: fixed paging and sorting
  IF(@sortColumnName IS NULL )
  BEGIN
   SET @sortColumnName ='0';
  END
  IF(@orderType IS NULL )
  BEGIN
   SET @orderType ='asc';
  END
  
  DECLARE @FROM	INT
			  , @TO	INT

	SET @FROM = (@pageSize * (@pageNum-1)) + 1;
	SET @TO = @pageSize * (@pageNum);
  --end modify by thanapon: fixed paging and sorting
SET NOCOUNT ON;

	SELECT
		@TOTAL_ITEM = count(1)
	FROM TB_R_ASSET_HIS AssetHis WITH(NOLOCK)
	where (ASSET_NO = @ASSET_NO OR @ASSET_NO IS NULL)
		  and (ASSET_SUB = @ASSET_SUB OR @ASSET_SUB IS NULL)
		  and (COMPANY in (select COMPANY_CODE FROM dbo.fn_GetMultipleCompanyList(@COMPANY)))
		  and (REQUEST_TYPE = @TRANSACTION_TYPE  OR  @TRANSACTION_TYPE IS NULL)
		  and ([STATUS] != '10' and (STATUS =  @STATUS OR @STATUS IS NULL))
		  and  ( DOC_NO  LIKE @DOC_NO or @DOC_NO is NULL)
		  and ( [TRANS_DATE]	BETWEEN  @StartDate  AND  @EndDate )
          and (  (AssetHis.COST_CODE IN (SELECT COST_CODE FROM dbo.FN_FD0COSTCENTERLIST ( @EMP_CODE))
						  )  OR  
						  (@IS_SYS_ADMIN = 'Y')						  
						);
			
  WITH Paging_Request_H AS
  (
    SELECT TRAN_TYPE,COMPANY,ASSET_SUB, DOC_NO, ASSET_NO, COST_CODE, REQUEST_TYPE
      ,replace(convert(varchar, [POSTING_DATE], 106),' ','-') AS POSTING_DATE 
      ,replace(convert(varchar, [TRANS_DATE], 106),' ','-') AS TRANS_DATE
      ,ORACLE_SENT_DATE
      ,ENTRY_PERIOD
      ,DETAIL
      ,LATEST_APPROVE_CODE
      ,LATEST_APPROVE_NAME
      ,STATUS
      ,CREATE_DATE
      ,CREATE_BY
      ,UPDATE_DATE
      ,UPDATE_BY
      ,(CASE WHEN @sortColumnName = '0' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY TRAN_TYPE asc)  
             WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TRAN_TYPE desc)
             WHEN @sortColumnName = '1' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY TRANS_DATE asc)  
             WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY TRANS_DATE desc)
             WHEN @sortColumnName = '2' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY DETAIL asc)  
             WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DETAIL desc)
             WHEN @sortColumnName = '3' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY LATEST_APPROVE_NAME asc)
             WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY LATEST_APPROVE_NAME desc) 
             WHEN @sortColumnName = '4' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY POSTING_DATE asc)
             WHEN @sortColumnName = '4' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY POSTING_DATE desc)  
             WHEN @sortColumnName = '5' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY DOC_NO asc)
             WHEN @sortColumnName = '5' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY DOC_NO desc) 
      END) AS ROWNUMBER
    FROM (
      SELECT 
  	    (select VALUE from TB_M_SYSTEM where CATEGORY ='SYSTEM_CONFIG' and
  	    SUB_CATEGORY ='REQUEST_TYPE' and AssetHis.REQUEST_TYPE = CODE) as
    	  [TRAN_TYPE]
		  ,COMPANY
		  ,ASSET_SUB
    	  ,[DOC_NO]
        ,[ASSET_NO]
        ,[COST_CODE]
        ,[REQUEST_TYPE]
		,[dbo].fn_dateFAS(POSTING_DATE) AS POSTING_DATE
		,[dbo].fn_dateFAS(TRANS_DATE) AS TRANS_DATE
        ,null ORACLE_SENT_DATE
        ,[ENTRY_PERIOD]
        ,[DETAIL]
        ,[LATEST_APPROVE_CODE]
        ,[LATEST_APPROVE_NAME]
        ,[STATUS]
        ,[CREATE_DATE]
        ,[CREATE_BY]
        ,[UPDATE_DATE]
        ,[UPDATE_BY] 
      FROM TB_R_ASSET_HIS AssetHis WITH(NOLOCK)
	    where   (ASSET_NO = @ASSET_NO OR @ASSET_NO IS NULL)
		   and (ASSET_SUB = @ASSET_SUB OR @ASSET_SUB IS NULL)
		   and (COMPANY in (select COMPANY_CODE FROM dbo.fn_GetMultipleCompanyList(@COMPANY)))
		   and (REQUEST_TYPE = @TRANSACTION_TYPE  OR  @TRANSACTION_TYPE IS NULL)
		   and ([STATUS] != '10' and (STATUS =  @STATUS or @STATUS IS NULL))
		   and  ( DOC_NO  LIKE @DOC_NO or @DOC_NO IS NULL)
		   and ( [TRANS_DATE]	between  @StartDate  AND @EndDate )
		   and (  ( AssetHis.COST_CODE IN (SELECT COST_CODE FROM dbo.FN_FD0COSTCENTERLIST ( @EMP_CODE)) ) 
		         OR   (  (@IS_SYS_ADMIN = 'Y')	)					  
						)
					) T
  )
  
  --begin modify by thanapon: fixed paging and sorting
  SELECT *
  FROM Paging_Request_H d
  WHERE  d.ROWNUMBER >= @FROM and d.ROWNUMBER <= @TO
  ORDER BY d.ROWNUMBER
  --end modify by thanapon: fixed paging and sorting

END




GO
