DROP PROCEDURE [dbo].[sp_WFD021A0_DeleteAssetFlagY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD021A0_DeleteAssetFlagY]
@DOC_NO		T_DOC_NO,
@USER		T_SYS_USER
AS
BEGIN
	-- This store is executed by Approve Operation
	-------------------------------------------------------------
	DELETE
	FROM	TB_T_DELETED_ASSETS
	WHERE	DOC_NO = @DOC_NO
	INSERT
	INTO	TB_T_DELETED_ASSETS
	(		COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	COST_CODE)
	select	COMPANY,	DOC_NO,	ASSET_NO,	ASSET_SUB,	NULL
	FROM	TB_R_REQUEST_ASSET_D
	WHERE	DOC_NO	= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	EXEC sp_WFD01170_GenerateCommentDeleted @DOC_NO, @USER, 'A'
	
	-- Delete assets from request
	DELETE	TB_R_REQUEST_ASSET_D 
	WHERE	DOC_NO		= @DOC_NO AND
			DELETE_FLAG	= 'Y'

	-------------------------------------------------------------
END
GO
