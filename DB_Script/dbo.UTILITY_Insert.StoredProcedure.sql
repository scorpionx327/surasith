DROP PROCEDURE [dbo].[UTILITY_Insert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[UTILITY_Insert]
	@table varchar(128) ,
	@where_cause varchar(8000),
	@outtype char(1) = 'I',--'I,D'
	@is_new_id char(1) = 'F'
AS

/*******************************************************************

*******************************************************************/

DECLARE @InsertStmt varchar(8000),
	@DeleteStmt varchar(8000),
	@FieldSysID varchar(8000),
	@Fields varchar(8000),
	@SelList varchar(8000),
	@Data varchar(8000),
	@ColName varchar(128),
	@IsChar tinyint,
	@FldCounter int,
	@TableData varchar(8000),
	@tempI integer,
	@temp varchar(200),
	@total_rows int

-- Initialize some of the variables
SET NOCOUNT ON


IF @is_new_id = 'T'
BEGIN 
	PRINT '--SELECT NEWID()'
	PRINT '--DECLARE @'+@table+'_ID CHAR(36)'
	PRINT '--SET @'+@table+'_ID = NEWID()'
END

SET @FieldSysID = ''

SELECT @DeleteStmt ='DELETE ' + @Table  + ' WHERE ' + @where_cause

SELECT @InsertStmt = 'INSERT INTO ' + @Table + ' (',
	@Fields = '',
	@Data = '',
	@SelList = 'SELECT ',
	@FldCounter = 0,
	@total_rows = 0

-- Create a cursor that loops through the fields in the table
-- and retrieves the column names and determines the delimiter type that the
-- field needs
DECLARE CR_Table CURSOR FAST_FORWARD FOR
SELECT COLUMN_NAME,
'IsChar' = CASE
  WHEN DATA_TYPE in ('int', 'money', 'decimal', 'tinyint', 'smallint') THEN 0
  WHEN DATA_TYPE in ('char', 'nchar', 'varchar', 'nvarchar' ) THEN 1
  WHEN DATA_TYPE in ('ntext' ) THEN 11
  WHEN DATA_TYPE in ('datetime', 'smalldatetime') THEN 2
  ELSE 9 END
FROM INFORMATION_SCHEMA.COLUMNS
WHERE table_name = @table
  AND DATA_TYPE <> 'timestamp'
ORDER BY ORDINAL_POSITION
FOR READ ONLY

OPEN CR_Table
FETCH NEXT FROM CR_Table INTO @ColName, @IsChar
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
	  IF @FldCounter = 0
	  BEGIN
		SELECT @Fields =  @Fields + @ColName + ', '
		
		IF @ColName = 'SYS_ID' AND @is_new_id = 'T'
		BEGIN
			SELECT @SelList =  @SelList + '''NEWID()'''  +'+ '
		END
		ELSE
		BEGIN
			SELECT @SelList =  CASE
				WHEN @IsChar = 1 THEN @SelList + ''''''''''+'+' + ' ISNULL( REPLACE(['+  @ColName
			+ '], '''', ''''''''),''0semiunique1data2to3say4that5the6data7is8null9'')' + '+'+ ''''''''''  +'+ '
				WHEN @IsChar = 11 THEN @SelList + ''''''''''+'+' + ' ISNULL( REPLACE('+  'CONVERT(nvarchar(2000),['+@ColName+ '])' +
			+ ', '''', ''''''''),''0semiunique1data2to3say4that5the6data7is8null9'')' + '+'+ ''''''''''  +'+ '
				WHEN @IsChar = 2 THEN @SelList + ' '''''''' +  ISNULL(CONVERT(varchar(50),['
			+ @ColName + ']),''0semiunique1data2to3say4that5the6data7is8null9'') + '''''''' +' + '  '
				ELSE @SelList + 'ISNULL(CONVERT(varchar(2000),['+@ColName + ']),''0semiunique1data2to3say4that5the6data7is8null9'')' +  '+
			'''' + ' END
		END
		
		SELECT @FldCounter = @FldCounter + 1
		
		FETCH NEXT FROM CR_Table INTO @ColName, @IsChar
	  END

	  SELECT @Fields =  @Fields + @ColName + ', '


		IF @ColName = 'CREATED_DATE' --AND @is_new_id = 'T'
		BEGIN
			SELECT @SelList =  @SelList + ''', GETDATE()'''  +'+ '
		END
		ELSE IF @ColName = 'LAST_UPDATED_DATE' --AND @is_new_id = 'T'
		BEGIN
			SELECT @SelList =  @SelList + ''', GETDATE()'''  +'+ '
		END
		ELSE IF @ColName = 'UPDATED_DATE' --AND @is_new_id = 'T'
		BEGIN
			SELECT @SelList =  @SelList + ''', GETDATE()'''  +'+ '
		END
		ELSE IF @ColName = 'CREATED_USER_ID' OR @ColName = 'UPDATED_USER_ID'
		BEGIN
			SELECT @SelList =  @SelList + ''',''''Mameaw'''''''  + '+ '
		END		
		ELSE
		BEGIN
		  SELECT @SelList =  CASE
		  WHEN @IsChar = 1 THEN @SelList + ''''+ ',' + '''' + '+''''''''+' +  ' ISNULL(REPLACE([' + @ColName
			+ '], '''', ''''''''), ''0semiunique1data2to3say4that5the6data7is8null9'')' + '+'+'''''''''+'
			  WHEN @IsChar = 11 THEN @SelList + ''''+ ',' + '''' + '+''''''''+' +  ' ISNULL(REPLACE(' +  'CONVERT(nvarchar(2000),[' + @ColName + '])' +
			+ ', '''', ''''''''), ''0semiunique1data2to3say4that5the6data7is8null9'')' + '+'+'''''''''+'
			  WHEN @IsChar = 2 THEN @SelList + ' '','''''' +  ISNULL(CONVERT(varchar(50),['
			+ @ColName + ']),''0semiunique1data2to3say4that5the6data7is8null9'') + '''''''' +' + '  '
			  ELSE @SelList  + ' '','' +  ISNULL(CONVERT(varchar(2000),['+@ColName +
			']),''0semiunique1data2to3say4that5the6data7is8null9'')' + '+' END
		END
	END
	FETCH NEXT FROM CR_Table INTO @ColName, @IsChar
END
CLOSE CR_Table
DEALLOCATE CR_Table


SELECT @Fields =  SUBSTRING(@Fields, 1,(len(@Fields)-1))

SELECT @SelList =  SUBSTRING(@SelList, 1,(len(@SelList)-1))
SELECT @SelList = @SelList + ' FROM ' + @table 
IF @where_cause != ''
	SET @SelList = @SelList + ' WHERE ' + @where_cause
--print @SelList
IF @outtype = 'D'
	print @DeleteStmt
	
SELECT @InsertStmt = @InsertStmt + @Fields + ')'

CREATE TABLE #TheData (TableData varchar(8000))
INSERT INTO #TheData (TableData) EXEC (@SelList)

--PRINT 'SET IDENTITY_INSERT ' + @Table + ' ON' + CHAR(10) + 'GO' + CHAR(10)
-- Cursor through the data to generate the INSERT statement / VALUES clause
DECLARE CR_Data CURSOR FAST_FORWARD FOR SELECT TableData FROM #TheData FOR
READ ONLY
OPEN CR_Data
FETCH NEXT FROM CR_Data INTO @TableData
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
		SELECT @TableData = REPLACE (@TableData, '''0semiunique1data2to3say4that5the6data7is8null9''', 'NULL')
		SELECT @TableData = REPLACE (@TableData, '0semiunique1data2to3say4that5the6data7is8null9', 'NULL')
		IF @outtype = 'I'
		BEGIN
			PRINT @InsertStmt
			PRINT ' SELECT ' +  @TableData + '' + char(13) --+ 'GO'
		END
		SET @total_rows = @total_rows + 1
	END
	FETCH NEXT FROM CR_Data INTO @TableData
END
PRINT ''
--PRINT 'SET IDENTITY_INSERT ' + @Table + ' OFF' + CHAR(10) + 'GO' + CHAR(10)
PRINT '-- Total data of table ' + @Table + ' is ' + CAST(@total_rows AS VARCHAR(5)) + ' row(s)' + CHAR(10)


CLOSE CR_Data
DEALLOCATE CR_Data

RETURN (0)



GO
