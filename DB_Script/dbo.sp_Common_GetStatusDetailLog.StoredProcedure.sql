DROP PROCEDURE [dbo].[sp_Common_GetStatusDetailLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_GetStatusDetailLog]--\\ 2
(
	@ReqID	INT
)
AS
BEGIN
	SELECT	ISNULL(SUM(CASE [LEVEL] WHEN 'E' THEN 1 ELSE 0 END),0) AS E,
			ISNULL(SUM(CASE [LEVEL] WHEN 'W' THEN 1 ELSE 0 END),0) AS W,
			ISNULL(SUM(CASE [LEVEL] WHEN 'I' THEN 1 ELSE 0 END),0) AS I
	FROM	TB_L_LOGGING
	WHERE	APP_ID = @ReqID
END



GO
