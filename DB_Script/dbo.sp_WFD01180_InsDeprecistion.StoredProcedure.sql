DROP PROCEDURE [dbo].[sp_WFD01180_InsDeprecistion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01180_InsDeprecistion]
(
	@COMPANY T_COMPANY,
	@ASSET_CLASS T_ASSET_CLASS,
	@MINOR_CATEGORY T_MINOR_CATEGORY,
	@STICKER_TYPE varchar(1),
	@STICKER_SIZE varchar(1),
	@INVENTORY_INDICATOR varchar(1),
	@DPRE_KEY_01 varchar(4),
	@USEFUL_LIFE_YEAR_01 int,
	@USEFUL_LIFE_PERD_01 int,
	@USEFUL_LIFE_TYPE_01 varchar(2),
	@SCRAP_VALUE_01 int,
	@DEACT_DEPRE_AREA_15  varchar(1),
	@DPRE_KEY_15 varchar(4),	
	@USEFUL_LIFE_YEAR_15 int,
	@USEFUL_LIFE_PERD_15 int,
	@USEFUL_LIFE_TYPE_15 varchar(2),
	@SCRAP_VALUE_15 int,
	@DPRE_KEY_31 varchar(4),	
	@USEFUL_LIFE_YEAR_31 int,
	@USEFUL_LIFE_PERD_31 int,
	@USEFUL_LIFE_TYPE_31 varchar(2),
	@SCRAP_VALUE_31 int,
	@DPRE_KEY_41 varchar(4),	
	@USEFUL_LIFE_YEAR_41 int,
	@USEFUL_LIFE_PERD_41 int,
	@USEFUL_LIFE_TYPE_41 varchar(2),
	@SCRAP_VALUE_41 int,
	@DPRE_KEY_81 varchar(4),	
	@USEFUL_LIFE_YEAR_81 int,
	@USEFUL_LIFE_PERD_81 int,
	@USEFUL_LIFE_TYPE_81 varchar(2),
	@SCRAP_VALUE_81 int,
	@DPRE_KEY_91 varchar(4),	
	@USEFUL_LIFE_YEAR_91 int,
	@USEFUL_LIFE_PERD_91 int,
	@USEFUL_LIFE_TYPE_91 varchar(2),
	@SCRAP_VALUE_91 int,
	@USER T_SYS_USER
)
AS
BEGIN TRY 
	INSERT INTO [dbo].[TB_M_DEPRECIATION]
           ([COMPANY]
           ,[ASSET_CLASS]
           ,[MINOR_CATEGORY]
           ,[STICKER_TYPE]
           ,[STICKER_SIZE]
           ,[INVENTORY_INDICATOR]
           ,[DPRE_KEY_01]
		   ,[USEFUL_LIFE_YEAR_01]
		   ,[USEFUL_LIFE_PERD_01]
           ,[USEFUL_LIFE_TYPE_01]
           ,[SCRAP_VALUE_01]
		   ,[DEACT_DEPRE_AREA_15]
           ,[DPRE_KEY_15]           
		   ,[USEFUL_LIFE_YEAR_15]
		   ,[USEFUL_LIFE_PERD_15]
           ,[USEFUL_LIFE_TYPE_15]
           ,[SCRAP_VALUE_15]
           ,[DPRE_KEY_31]           
		   ,[USEFUL_LIFE_YEAR_31]
		   ,[USEFUL_LIFE_PERD_31]
           ,[USEFUL_LIFE_TYPE_31]
           ,[SCRAP_VALUE_31]
           ,[DPRE_KEY_41]           
		   ,[USEFUL_LIFE_YEAR_41]
		   ,[USEFUL_LIFE_PERD_41]
           ,[USEFUL_LIFE_TYPE_41]
           ,[SCRAP_VALUE_41]
           ,[DPRE_KEY_81]           
		   ,[USEFUL_LIFE_YEAR_81]
		   ,[USEFUL_LIFE_PERD_81]
           ,[USEFUL_LIFE_TYPE_81]
           ,[SCRAP_VALUE_81]
           ,[DPRE_KEY_91]           
		   ,[USEFUL_LIFE_YEAR_91]
		   ,[USEFUL_LIFE_PERD_91]
           ,[USEFUL_LIFE_TYPE_91]
           ,[SCRAP_VALUE_91]
           ,[CREATE_DATE]
           ,[CREATE_BY]
           ,[UPDATE_DATE]
           ,[UPDATE_BY])
     VALUES
           (@COMPANY
           ,@ASSET_CLASS
           ,@MINOR_CATEGORY
           ,@STICKER_TYPE
           ,@STICKER_SIZE
           ,@INVENTORY_INDICATOR
           ,@DPRE_KEY_01          
		   ,@USEFUL_LIFE_YEAR_01
		   ,@USEFUL_LIFE_PERD_01
           ,@USEFUL_LIFE_TYPE_01
           ,@SCRAP_VALUE_01
		   ,@DEACT_DEPRE_AREA_15
           ,@DPRE_KEY_15           
		   ,@USEFUL_LIFE_YEAR_15
		   ,@USEFUL_LIFE_PERD_15
           ,@USEFUL_LIFE_TYPE_15
           ,@SCRAP_VALUE_15
           ,@DPRE_KEY_31          
		   ,@USEFUL_LIFE_YEAR_31
		   ,@USEFUL_LIFE_PERD_31
           ,@USEFUL_LIFE_TYPE_31
           ,@SCRAP_VALUE_31
           ,@DPRE_KEY_41          
		   ,@USEFUL_LIFE_YEAR_41
		   ,@USEFUL_LIFE_PERD_41
           ,@USEFUL_LIFE_TYPE_41
           ,@SCRAP_VALUE_41
           ,@DPRE_KEY_81          
		   ,@USEFUL_LIFE_YEAR_81
		   ,@USEFUL_LIFE_PERD_81
           ,@USEFUL_LIFE_TYPE_81
           ,@SCRAP_VALUE_81
           ,@DPRE_KEY_91          
		   ,@USEFUL_LIFE_YEAR_91
		   ,@USEFUL_LIFE_PERD_91
           ,@USEFUL_LIFE_TYPE_91
           ,@SCRAP_VALUE_91
           ,getdate()
           ,@USER
           ,getdate()
           ,@USER)
END TRY
BEGIN CATCH
	THROW
END CATCH
GO
