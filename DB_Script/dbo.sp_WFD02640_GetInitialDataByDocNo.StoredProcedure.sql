DROP PROCEDURE [dbo].[sp_WFD02640_GetInitialDataByDocNo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 02/03/2017
-- Description:	Get detail Stock Taking Monitoring Detail
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_GetInitialDataByDocNo]
(	@DocNo	T_DOC_NO = NULL
)
AS
BEGIN
  ----==============================  Declare status 
DECLARE 	@COMPANY	T_COMPANY = NULL,
			@YEAR VARCHAR(4)= NULL,
			@ROUND VARCHAR(2)= NULL,
			@ASSET_LOCATION VARCHAR(1)= NULL,
			@SV_CODE VARCHAR(50) = NULL

        SELECT  @COMPANY		= COMPANY,
				@YEAR			= [YEAR], 
				@ROUND			= [ROUND] , 
				@ASSET_LOCATION = ASSET_LOCATION
	   	FROM	TB_R_REQUEST_STOCK 
		WHERE	DOC_NO			= @DocNo;

			
		SELECT	@SV_CODE =SV_EMP_CODE
		FROM	(
				SELECT	stockD.SV_EMP_CODE 
				FROM	TB_R_STOCK_TAKE_D stockD
				INNER	JOIN
						TB_R_REQUEST_STOCK  r 
				ON      stockD.COMPANY	= r.COMPANY AND
						stockD.[YEAR] = r.[YEAR]  AND
					    stockD.[ROUND]   =  r.[ROUND]  AND
						stockD.ASSET_LOCATION = r.ASSET_LOCATION  AND
						stockD.SV_EMP_CODE=r.SV_EMP_CODE
				WHERE   stockD.COMPANY	= @COMPANY AND
						stockD.[YEAR]	= @YEAR  AND
					    stockD.ROUND	=  @ROUND  AND
					    stockD.ASSET_LOCATION = @ASSET_LOCATION AND
						r.DOC_NO		= @DocNo
                      
				GROUP BY stockD.SV_EMP_CODE
		) t
 
    SELECT @COMPANY AS COMPANY, @YEAR AS [YEAR],  @ROUND  AS [ROUND] , @ASSET_LOCATION AS ASSET_LOCATION, @SV_CODE AS SV_CODE

END
GO
