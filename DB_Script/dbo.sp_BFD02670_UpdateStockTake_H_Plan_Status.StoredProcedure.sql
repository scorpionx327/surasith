DROP PROCEDURE [dbo].[sp_BFD02670_UpdateStockTake_H_Plan_Status]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 07-03-2017
-- Description:	Update stock take header plan status
-- =============================================
CREATE PROCEDURE [dbo].[sp_BFD02670_UpdateStockTake_H_Plan_Status]
	@STOCK_TAKE_KEY varchar(7) 
	, @EMP_CODE	varchar(8)
	, @PLAN_STATUS varchar(1)

AS
BEGIN
		UPDATE TB_R_STOCK_TAKE_H
			SET PLAN_STATUS = @PLAN_STATUS
				, UPDATE_BY=@EMP_CODE
		WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY And PLAN_STATUS <> 'F'
END




GO
