DROP PROCEDURE [dbo].[sp_WFD01270_GenerateDocNo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01270_GenerateDocNo]
@Company		T_COMPANY,
@GUID			VARCHAR(50),
@RequestType	VARCHAR(1)
AS
BEGIN


	UPDATE #TB_T_DOC SET USED_FLAG = 'N'


	DECLARE @DocIndex INT, @Cate VARCHAR(40)
	WHILE(EXISTS(SELECT 1 FROM #TB_T_DOC WHERE USED_FLAG = 'N'))
	BEGIN
		-- Update Loop Index
		SELECT TOP 1 @Cate = ASSET_CLASS  FROM #TB_T_DOC WHERE USED_FLAG = 'N'
		UPDATE #TB_T_DOC SET USED_FLAG = 'Y' WHERE ASSET_CLASS = @Cate

		-- Create List of Role for comparing
		DECLARE @List VARCHAR(1000) 
		SET		@List = ''
		SELECT	@List = COALESCE(@List + '|', '') + CONCAT(R.INDX,'#', R.[APPR_ROLE]) 
		FROM	
		(
				SELECT	INDX, [APPR_ROLE] 
				FROM	TB_R_REQUEST_APPR
				WHERE	[GUID] = @GUID
		) R
		INNER	JOIN
		(
				SELECT	D.INDX, D.[ROLE]
				FROM	TB_M_APPROVE_H H
				INNER	JOIN
						TB_M_APPROVE_D D
				ON		H.APPRV_ID		= D.APPRV_ID
				WHERE	REQUEST_TYPE	= @RequestType and 
						ASSET_CLASS  = @Cate
		) M
		ON	R.INDX = M.INDX AND
			R.[APPR_ROLE] = M.[ROLE]

		PRINT @List

		IF EXISTS(SELECT 1 FROM #TB_T_DOC WHERE ROLE_LIST = @List)
		BEGIN
			SELECT TOP 1 @DocIndex = DOC_INDX FROM #TB_T_DOC WHERE ROLE_LIST = @List
		END
		ELSE
		BEGIN
			SELECT @DocIndex = MAX(DOC_INDX) FROM #TB_T_DOC
			SET @DocIndex = @DocIndex + 1
		END

		UPDATE	#TB_T_DOC 
		SET		ROLE_LIST		= @List, 
				DOC_INDX		= @DocIndex 
		WHERE	ASSET_CLASS		= @Cate
	END

	-- Get Maximum Doc No
	DECLARE @LatestDocNo INT
	--DECLARE @T_DOC			VARCHAR(4)
	SELECT	@LatestDocNo = MAX(CONVERT(INT, RIGHT(DOC_NO,4))) 
	FROM	TB_R_REQUEST_H
	WHERE	DOC_NO LIKE  CONCAT(UPPER(@Company),'-', UPPER(@RequestType), CONVERT(VARCHAR(6),GETDATE(),112),'%')

	SET	@LatestDocNo = ISNULL(@LatestDocNo,0)

	--SET @T_DOC = RIGHT('000' + CONVERT(VARCHAR(4),@LatestDocNo + DOC_INDX),4)
	--select CONCAT(UPPER(@Company),'-', @RequestType, CONVERT(VARCHAR(6),GETDATE(),112), '/', RIGHT('000' + CONVERT(VARCHAR(4),@LatestDocNo + 1),4))

	UPDATE	#TB_T_DOC
	SET		DOC_NO = CONCAT(UPPER(@Company),'-', @RequestType, CONVERT(VARCHAR(6),GETDATE(),112), '/', RIGHT('000' + CONVERT(VARCHAR(4),@LatestDocNo + DOC_INDX),4))

END



GO
