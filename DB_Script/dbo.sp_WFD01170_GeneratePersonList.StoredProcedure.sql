DROP PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_WFD01170_GeneratePersonList]
@GUID	T_GUID = NULL
AS
BEGIN
	
	--select * from TB_T_REQUEST_APPR where [guid] = '7d83531c-ca32-43f1-98c1-9b56425102ad'
	UPDATE	TB_T_REQUEST_COST_CENTER 
	SET		RESP_COST_CODE	= NULL -- Prepare REsp CC for 1 condition
	WHERE	[GUID] = @GUID AND 
			RESP_COST_CODE = ''

	DELETE
	FROM	TB_H_REQUEST_APPR
	WHERE	[GUID] = @GUID

	SELECT	INDX, APPR_ROLE, DIVISION, 'N' AS USED_FLAG
	INTO	#TB_T_REQUEST_APPR
	FROM	TB_T_REQUEST_APPR 
	WHERE	[GUID] = @GUID AND
			NOTIFICATION_MODE = 'P'
	ORDER	BY
			INDX

	DECLARE @Company T_COMPANY, @Requestor T_USER, @RequestRole VARCHAR(3)
	SELECT	TOP 1 @Company	= COMPANY, 
			@Requestor		= EMP_CODE,
			@RequestRole	= ACT_ROLE  
	FROM	TB_T_REQUEST_COST_CENTER WHERE [GUID] = @GUID

	-- Prepare Cost Center Mapping with Org. Code
	DELETE
	FROM	TB_T_COST_CENTER_ORG_MAPPING
	WHERE	[GUID] = @GUID OR CREATE_DATE <= DATEADD(DAY, -1, GETDATE())

	PRINT '#1'
	
	INSERT
	INTO	TB_T_COST_CENTER_ORG_MAPPING
	(
			[GUID],	
			COMPANY,		
			COST_CENTER	,
			ORG_CODE,	
			CMP_CODE,	
			CMP_ABBR,	
			DIV_CODE,	
			SUB_DIV_CODE,
			DEPT_CODE,	
			SECTION_CODE,
			LINE_CODE,
			CREATE_DATE	
	)
	SELECT	@GUID,
			D.CMP_ABBR,		
			E.COST_CODE,
			D.ORG_CODE,	
			D.CMP_CODE,	
			D.CMP_ABBR,	
			D.DIV_CODE,	
			D.SUB_DIV_CODE,
			D.DEPT_CODE,	
			D.SECTION_CODE,
			D.LINE_CODE,
			GETDATE()
	FROM	TB_M_EMPLOYEE E 
	INNER	JOIN
			TB_M_ORGANIZATION D
	ON		E.ORG_CODE	= D.ORG_CODE
	WHERE	EXISTS(	SELECT	1 
					FROM	TB_T_REQUEST_COST_CENTER RC 
					WHERE	E.COMPANY		= RC.COMPANY AND 
							E.COST_CODE		= RC.COST_CODE AND
							E.ACTIVEFLAG	= 'Y' AND
							RC.[GUID]		= @GUID
					)

	WHILE( EXISTS(SELECT 1 FROM #TB_T_REQUEST_APPR WHERE USED_FLAG = 'N'))
	BEGIN
		
		DECLARE @ApprRole VARCHAR(3), @Indx INT, @Division VARCHAR(6)
		SELECT	TOP 1	@Indx		= INDX, 
						@ApprRole	= APPR_ROLE,
						@Division	= DIVISION
		FROM	#TB_T_REQUEST_APPR
		WHERE	USED_FLAG = 'N'
		ORDER	BY
				[INDX]

		PRINT	CONCAT(@Indx, ' => ', @ApprRole)
		--PRINT '#3'
		UPDATE	#TB_T_REQUEST_APPR SET USED_FLAG = 'Y' WHERE INDX = @Indx

		IF @ApprRole = 'FW'
		BEGIN
			-- Get FW
			EXEC sp_WFD01170_GeneratePersonList_FW @Indx, @Division,  @GUID
			PRINT CONCAT('FW => ', @@ROWCOUNT)
			GOTO NEXTLOOP
		END
		IF @ApprRole = 'FS'
		BEGIN
			EXEC sp_WFD01170_GeneratePersonList_FS @Indx, @Division, @GUID
			-- Get FS
			PRINT CONCAT('FS => ', @@ROWCOUNT)
			GOTO NEXTLOOP
		END
		IF @ApprRole = 'ACR'
		BEGIN
			-- Get ACR 
			EXEC sp_WFD01170_GeneratePersonList_SP @Indx, @Company, @ApprRole, @GUID
			PRINT CONCAT('ACR ', @@ROWCOUNT)
			GOTO NEXTLOOP
		END
		IF @ApprRole = 'ACM'
		BEGIN
			-- Get ACM
			PRINT 'ACM ** NO NEED'
		END
		PRINT '@ApprRole'
		PRINT @ApprRole
		PRINT @Division
		IF @Division NOT IN ('CRNT', 'NEW')
		BEGIN
			PRINT 'MATCH'
			EXEC sp_WFD01170_GeneratePersonList_SP @Indx, @Company, @ApprRole, @GUID
			PRINT 'Fixed Position'
			GOTO NEXTLOOP
		END
		--PRINT '#4'
		PRINT @Indx
		PRINT @Company
		PRINT @ApprRole
		PRINT @GUID
		-- Approver Role Management
		DECLARE @MainCnt INT = 0, @IndirectCnt INT = 0
		EXEC sp_WFD01170_GeneratePersonList_MGR @Indx, @Company, @ApprRole, @Division, 'M', @GUID, @MainCnt OUT
		
		IF @MainCnt = 0
		BEGIN
			EXEC sp_WFD01170_GeneratePersonList_MGR_HigherDirect @Indx, @Company, @ApprRole, @Division, @GUID, @MainCnt OUT
			-- Higher of direct approver
			PRINT 'Load Higher of direct approver'
		END


		EXEC sp_WFD01170_GeneratePersonList_MGR @Indx, @Company, @ApprRole, @Division, 'R', @GUID, @IndirectCnt OUT
		
		-- Incase all not found
		IF @MainCnt + @IndirectCnt = 0
		BEGIN
			PRINT 'Load Higher of Indirect approver'
		END

		
NEXTLOOP:
		PRINT 'NEXT'
	END

	-- Set Default for requestor
	DECLARE @fstRole VARCHAR(3), @fstIndx INT
	SELECT	TOP 1 @fstRole = APPR_ROLE, @fstIndx = INDX
	FROM	TB_T_REQUEST_APPR 
	WHERE	[GUID] = @GUID
	ORDER	BY
			INDX

	IF (@fstRole = @RequestRole AND 
			EXISTS(	SELECT 1 FROM TB_H_REQUEST_APPR 
					WHERE [GUID] = @GUID AND INDX = @fstIndx AND EMP_CODE = @Requestor))
	BEGIN
		DELETE
		FROM	TB_H_REQUEST_APPR
		WHERE	[GUID]	= @GUID AND 
				INDX	= @fstIndx AND
				EMP_CODE <> @Requestor ;
	END
	
	-- Update Div Name
	UPDATE	T
	SET		T.DIV_NAME	= W.DIV_NAME
	FROM	TB_T_REQUEST_APPR T
	INNER	JOIN
	( 
		SELECT	ROW_NUMBER() OVER(PARTITION BY INDX ORDER BY MAIN, H.EMP_CODE) ROW_INDX,
				INDX,
				D.DIV_NAME
		FROM	TB_H_REQUEST_APPR H   WITH(NOLOCK) 
		INNER	JOIN
				TB_M_EMPLOYEE E  WITH(NOLOCK) 
		ON		H.EMP_CODE	= E.SYS_EMP_CODE 
		INNER	JOIN
				TB_M_ORGANIZATION D  WITH(NOLOCK) 
		ON		E.ORG_CODE	= D.ORG_CODE
		WHERE	[GUID]		= @GUID
	)	W
	ON	W.ROW_INDX	= 1 AND T.INDX = W.INDX
	WHERE	T.[GUID] = @GUID
END
GO
