DROP PROCEDURE [dbo].[sp_WFD01120_GetChangeRequestDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[sp_WFD01120_GetChangeRequestDetail]
	@COMPANY			T_COMPANY = null,
	@REQUEST_TYPE		VARCHAR(2),
 	@DOC_NO				VARCHAR(12), -- Include %%
	@PERIOD_FROM		DATE,
	@PERIOD_TO			DATE,	
	@EMP_CODE			VARCHAR(8),
	--Part Paging Parameter
	@pageNum			INT,
	@pageSize			INT,
	@sortColumnName		VARCHAR(50),
	@orderType			VARCHAR(5),
	--@CURRENCT_PAGE	INT,
	--@PAGE_SIZE		INT,
	--@ORDER_BY	    NVARCHAR(50),
	@TOTAL_ITEM			INT OUTPUT
AS
	
BEGIN

	DECLARE @IS_FA_ADMIN VARCHAR(1) ='N'

	SELECT @IS_FA_ADMIN = FAADMIN
	FROM TB_M_EMPLOYEE  
	WHERE EMP_CODE		= @EMP_CODE

	SET @DOC_NO = dbo.fn_GetSearchCriteria(@DOC_NO)

	--begin modify by thanapon: fixed paging and sorting
	IF(@sortColumnName IS NULL )
	BEGIN
		SET @sortColumnName ='0';
	END
	
	IF(@orderType IS NULL )
	BEGIN
		SET @orderType ='asc';
	END
  
	DECLARE @FROM	INT
			  , @TO	INT

	SET @FROM = (@pageSize * (@pageNum-1)) + 1;
	SET @TO = @pageSize * (@pageNum);
  --end modify by thanapon: fixed paging and sorting
  
	IF OBJECT_ID('tempdb..#ASSETS') IS NOT NULL DROP TABLE #ASSETS

	SELECT  H.COMPANY,
		H.DOC_NO,
		H.REQUEST_TYPE,
		MAX(H.TRANS_DATE)	AS COMPLETE_DATE,
		H.COST_CODE
	INTO #ASSETS
	FROM TB_R_ASSET_HIS H
	WHERE H.REQUEST_TYPE	IN ('T','D')
	AND	H.TRANS_DATE	IS NOT NULL AND
		(   (H.TRANS_DATE >= @PERIOD_FROM OR @PERIOD_FROM IS NULL) 
			AND (H.TRANS_DATE < DATEADD(day,1,@PERIOD_TO) OR @PERIOD_TO IS NULL)
		)
	--No field ASSET_CATEGORY.
	--AND EXISTS (SELECT 1 
	--			  FROM TB_M_ASSETS_H assetH
	--			  WHERE H.ASSET_NO	= assetH.ASSET_NO
	--			  AND (
				  
	--					ASSET_CATEGORY  NOT IN ( SELECT [VALUE] 
	--													FROM TB_M_SYSTEM
	--													WHERE CATEGORY='ADMIN_ASSET' 
	--															AND SUB_CATEGORY='ASSET_CATEGORY' AND ACTIVE_FLAG='Y')
	--				   OR ( (@IS_FA_ADMIN = 'Y') AND (ASSET_CATEGORY IN ( SELECT [VALUE] 
	--													FROM TB_M_SYSTEM
	--													WHERE CATEGORY='ADMIN_ASSET' 
	--															AND SUB_CATEGORY='ASSET_CATEGORY' AND ACTIVE_FLAG='Y')))		
	--															)	
	--											)
	GROUP BY  H.DOC_NO,		H.REQUEST_TYPE,		H.COST_CODE,H.COMPANY

	INSERT INTO #ASSETS
	SELECT H.COMPANY,
		H.DOC_NO,
		H.REQUEST_TYPE,
		MAX(H.TRANS_DATE)	AS COMPLETE_DATE,
		TH.COST_CODE
	FROM TB_R_ASSET_HIS H
	INNER JOIN TB_R_REQUEST_TRNF_H TH ON H.DOC_NO	= TH.DOC_NO 
	WHERE H.REQUEST_TYPE	= 'T'
		AND NOT EXISTS (
			SELECT 1 FROM #ASSETS A
			WHERE H.DOC_NO		= A.DOC_NO 
			AND H.REQUEST_TYPE	= A.REQUEST_TYPE
			AND TH.COST_CODE	= A.COST_CODE 
		) 
		AND	H.TRANS_DATE	IS NOT NULL AND
		(   (H.TRANS_DATE >= @PERIOD_FROM OR @PERIOD_FROM IS NULL) 
			AND (H.TRANS_DATE < DATEADD(day,1,@PERIOD_TO) OR @PERIOD_TO IS NULL)
		)
		--No field ASSET_CATEGORY.
		--AND EXISTS (SELECT 1 
		--		  FROM TB_M_ASSETS_H assetH
		--		  WHERE H.ASSET_NO	= assetH.ASSET_NO
		--		  AND (
		--				ASSET_CATEGORY  NOT IN ( SELECT [VALUE] 
		--												FROM TB_M_SYSTEM
		--												WHERE CATEGORY='ADMIN_ASSET' 
		--														AND SUB_CATEGORY='ASSET_CATEGORY' AND ACTIVE_FLAG='Y')
		--			   OR ( (@IS_FA_ADMIN = 'Y') AND (ASSET_CATEGORY IN ( SELECT [VALUE] 
		--												FROM TB_M_SYSTEM
		--												WHERE CATEGORY='ADMIN_ASSET' 
		--														AND SUB_CATEGORY='ASSET_CATEGORY' AND ACTIVE_FLAG='Y')))		
		--														)	
		--										)
	GROUP BY  H.DOC_NO,		H.REQUEST_TYPE,		TH.COST_CODE,H.COMPANY

	SELECT
		@TOTAL_ITEM = COUNT(1)
	FROM (
		SELECT	
				H.DOC_NO
		FROM	#ASSETS H
		LEFT	JOIN
				TB_M_SYSTEM M
		ON		M.CATEGORY		= 'RPT_REQ_TYPE' AND
				M.SUB_CATEGORY	= 'REQ_TYPE' AND
				M.CODE			= H.REQUEST_TYPE
		WHERE	H.REQUEST_TYPE	= ISNULL(@REQUEST_TYPE,H.REQUEST_TYPE) AND
				H.REQUEST_TYPE	IN ('T','D') AND -- Transfer and Dispose Only
				H.COST_CODE		IN (	SELECT	CC.COST_CODE 
									FROM	FN_FD0COSTCENTERLIST(@EMP_CODE) CC
									)  AND
				H.DOC_NO		LIKE ISNULL(@DOC_NO, H.DOC_NO)	
		GROUP	BY
				H.DOC_NO, H.REQUEST_TYPE, M.VALUE) data;

WITH Paging_Transfer AS
  (	
    --begin modify by thanapon: fixed paging and sorting
    SELECT COMPANY,REPORT_NO, REQUEST_CODE, REPORT_TYPE
      ,dbo .fn_dateFAS(COMPLETE_DATE) AS COMPLETE_DATE
      ,(CASE WHEN @sortColumnName = '0' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_CODE asc)  
             WHEN @sortColumnName = '0' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_CODE desc)
             WHEN @sortColumnName = '1' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_CODE asc)  
             WHEN @sortColumnName = '1' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REQUEST_CODE desc)
             WHEN @sortColumnName = '2' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY REPORT_NO asc)  
             WHEN @sortColumnName = '2' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY REPORT_NO desc)
             WHEN @sortColumnName = '3' AND @orderType = 'asc' THEN ROW_NUMBER() OVER(ORDER BY COMPLETE_DATE asc)
             WHEN @sortColumnName = '3' AND @orderType = 'desc' THEN ROW_NUMBER() OVER(ORDER BY COMPLETE_DATE desc)  
      END) AS ROWNUMBER
      --end modify by thanapon: fixed paging and sorting
		FROM (
			SELECT	H.COMPANY,
					H.DOC_NO				AS REPORT_NO,
					H.REQUEST_TYPE			AS REQUEST_CODE,
					M.[VALUE]				AS REPORT_TYPE,
					MAX(H.COMPLETE_DATE)	AS COMPLETE_DATE
			FROM	#ASSETS H
			LEFT	JOIN
					TB_M_SYSTEM M
			ON		M.CATEGORY		= 'RPT_REQ_TYPE' AND
					M.SUB_CATEGORY	= 'REQ_TYPE' AND
					M.CODE			= H.REQUEST_TYPE
			WHERE	H.REQUEST_TYPE	= ISNULL(@REQUEST_TYPE,H.REQUEST_TYPE) AND
					H.REQUEST_TYPE	IN ('T','D') AND -- Transfer and Dispose Only
					H.COST_CODE		IN (	SELECT	CC.COST_CODE 
										FROM	FN_FD0COSTCENTERLIST(@EMP_CODE) CC
									  ) AND
					H.DOC_NO		LIKE ISNULL(@DOC_NO, H.DOC_NO)					
			GROUP	BY
					H.DOC_NO, H.REQUEST_TYPE, M.VALUE ,H.COMPANY
		) T
	) 

  --begin modify by thanapon: fixed paging and sorting
  SELECT *
  FROM Paging_Transfer d
  WHERE  d.ROWNUMBER >= @FROM and d.ROWNUMBER <= @TO
  ORDER BY d.ROWNUMBER
  --end modify by thanapon: fixed paging and sorting	

END




GO
