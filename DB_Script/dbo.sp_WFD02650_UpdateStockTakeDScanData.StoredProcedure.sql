DROP PROCEDURE [dbo].[sp_WFD02650_UpdateStockTakeDScanData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 03-03-2017
-- Description:	Update stock take d scan data
-- =============================================
-- Author:		Nuttapon
-- Create date: 24-10-2017
-- Description:	Update COUNT_TIME_NO_BREAK
-- =============================================
/*
Nipon 2019-08-08 15:16:28.020 Add @COMPANY, @ASSET_SUB
*/
CREATE PROCEDURE [dbo].[sp_WFD02650_UpdateStockTakeDScanData]
	-- Add the parameters for the stored procedure here
	@COMPANY T_COMPANY ,
	@STOCK_TAKE_KEY varchar(7) ,
    --@ASSET_NO T_ASSET_NO , shoube use this but now tabel field type varchar(15)
	@ASSET_NO varchar(15),
    @ASSET_SUB T_ASSET_SUB ,
    @EMP_CODE T_USER ,
    @CHECK_STATUS varchar(19) ,
    @SCAN_DATE datetime ,
    @START_COUNT_TIME datetime ,
    @END_COUNT_TIME datetime ,
    @COUNT_TIME int 
AS
	DECLARE @BREAK_TIME INT
	DECLARE @COUNT_TIME_NO_BREAK INT
BEGIN
	SET NOCOUNT ON;

	if @CHECK_STATUS = 'W'
	BEGIN

		SET @CHECK_STATUS = 'Y'
	END

	SET @COUNT_TIME = datediff(second, @START_COUNT_TIME,@END_COUNT_TIME);
	SET @COUNT_TIME_NO_BREAK = datediff(second, @START_COUNT_TIME,@END_COUNT_TIME);

	SELECT TOP 1 @BREAK_TIME = BREAK_TIME_MINUTE FROM TB_R_STOCK_TAKE_H h WHERE h.STOCK_TAKE_KEY=@STOCK_TAKE_KEY
	SET @BREAK_TIME = @BREAK_TIME*60;

	IF @COUNT_TIME > @BREAK_TIME
	BEGIN

	IF @BREAK_TIME = 0
	BEGIN
		SET @COUNT_TIME = @COUNT_TIME_NO_BREAK;
	END 
	ELSE
	BEGIN
		SET @COUNT_TIME = @BREAK_TIME;
	END
		
	END

	UPDATE TB_R_STOCK_TAKE_D
		SET EMP_CODE = CASE CHECK_STATUS
							WHEN 'N' THEN @EMP_CODE
							ELSE EMP_CODE
						END
			, CHECK_STATUS = CASE CHECK_STATUS
								WHEN 'N' THEN @CHECK_STATUS
								ELSE CHECK_STATUS
							END
			, SCAN_DATE= @SCAN_DATE
			, START_COUNT_TIME= @START_COUNT_TIME
			, END_COUNT_TIME= @END_COUNT_TIME
			, COUNT_TIME=@COUNT_TIME
			, UPDATE_DATE = GETDATE()
			--, COUNT_TIME_NO_BREAK = @COUNT_TIME_NO_BREAK

	WHERE STOCK_TAKE_KEY=@STOCK_TAKE_KEY 
	and ASSET_NO=@ASSET_NO 
	and CHECK_STATUS = 'N'
	and COMPANY = @COMPANY
	and ASSET_SUB = @ASSET_SUB
	--and Status='Y' -- No field Status can not check 'Y'

	EXEC sp_WFD02650_UpdateStockTakeHPlanStatus @STOCK_TAKE_KEY, @EMP_CODE

END
GO
