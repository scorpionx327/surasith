DROP PROCEDURE [dbo].[sp_WFD01210_DeleteEmployee]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 06/03/2017
-- Description:	Delete
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_DeleteEmployee] 
	@EMP_CODE		T_SYS_USER,
	@USER_LOGIN     varchar(8)
AS
BEGIN
		Update TB_M_EMPLOYEE 
		set ACTIVEFLAG = 'N'
		,UPDATE_BY = @USER_LOGIN
		,UPDATE_DATE = GETDATE()
		where SYS_EMP_CODE = @EMP_CODE;

		Delete from TB_M_SV_COST_CENTER
		where EMP_CODE = @EMP_CODE;

		Delete from TB_M_FASUP_COST_CENTER
		where EMP_CODE = @EMP_CODE;


		----------------------------------------------------------------------------------------------------------
		-- Add New By Surasith
		DELETE
		FROM	TB_T_EMPLOYEE_CHANGE WHERE SYS_EMP_CODE = @EMP_CODE

		INSERT
		INTO	TB_T_EMPLOYEE_CHANGE
		(		SYS_EMP_CODE, COMPANY, EMP_CODE, EMP_TITLE, EMP_NAME, EMP_LASTNAME, ORG_CODE, ACTIVEFLAG, ORG_CHANGED, INFO_CHANGED )
		SELECT	SYS_EMP_CODE, COMPANY, EMP_CODE, EMP_TITLE, EMP_NAME, EMP_LASTNAME, ORG_CODE, ACTIVEFLAG, 'N', 'N'
		FROM	TB_M_EMPLOYEE
		WHERE	SYS_EMP_CODE = @EMP_CODE;

		DECLARE @OldOrgCode VARCHAR(21), @ORG_CODE VARCHAR(21), @Role VARCHAR(3)
		SELECT	@OldOrgCode = ORG_CODE, @ORG_CODE = ORG_CODE, @Role = [ROLE]
		FROM	TB_M_EMPLOYEE 
		WHERE	SYS_EMP_CODE = @EMP_CODE

		EXEC sp_WFD01210_UpdateRequestWhenEmployeeInfoChanged @USER_LOGIN, @OldOrgCode, @ORG_CODE, @ROLE, 'N', @EMP_CODE

		EXEC sp_WFD01170_UpdateRequestwhenEmployeeInfoChanged
		---------------------------------------------------------------------------------------------------------
END
GO
