DROP PROCEDURE [dbo].[sp_WFD02130_DeleteBOIDoc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_WFD02130_DeleteBOIDoc]
	(	@EMP_CODE				T_SYS_USER,
		@COMPANY				T_COMPANY,
		@ASSET_NO				T_ASSET_NO,
		@ASSET_SUB				T_ASSET_SUB,		
		@UPDATE_DATE VARCHAR(50)															
	 )
AS
BEGIN TRY
BEGIN TRANSACTION T1
        IF NOT EXISTS( 
			SELECT  1 FROM TB_M_ASSETS_H 
			WHERE	COMPANY	= @COMPANY AND
					ASSET_NO =@ASSET_NO  AND 
					ASSET_SUB	= @ASSET_SUB AND
					FORMAT(UPDATE_DATE,'ddMMyyyyHHmmssfff') =@UPDATE_DATE)
             BEGIN
					 --SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MCOM0008AERR'
						--SET @MESSAGE = '[CUSTOM]MCOM0008AERR|' + @MESSAGE;
						DECLARE @Msg VARCHAR(500)

                      SET		@Msg = CONCAT('[CUSTOM]MCOM0008AERR|' ,dbo.fn_GetMessage('MCOM0008AERR'));

						RAISERROR (@Msg, -- Message text.
							   16, -- Severity.
							   1 -- State.
							   );
						RETURN;
			   END
				
				DECLARE @Seq INT
				SELECT	@Seq = SEQ
				FROM	TB_R_REQUEST_ATTACH_DOC
				WHERE	DOC_NO		= @COMPANY AND
						DOC_UPLOAD	= 'BOI' AND
						ASSET_NO	= @ASSET_NO AND
						ASSET_SUB	= @ASSET_SUB
				
				SET @Seq = ISNULL(@Seq,0) 

				DELETE
				FROM	TB_R_REQUEST_ATTACH_DOC
				WHERE	DOC_NO			= @COMPANY AND
						DOC_UPLOAD		= 'BOI' AND
						ASSET_NO		= @ASSET_NO AND
						ASSET_SUB		= @ASSET_SUB
			
			 
		COMMIT TRANSACTION T1
	 RETURN ;

END TRY
BEGIN CATCH
	 if @@TRANCOUNT <>0
	 BEGIN
       ROLLBACK TRANSACTION T1
	 END

	--  PRINT CONCAT('ERROR_MESSAGE:',ERROR_MESSAGE())
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();
        RAISERROR (@ErrorMessage, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		SELECT -1;
		RETURN;
END CATCH
GO
