DROP PROCEDURE [dbo].[sp_BFD021A3_GetAssetList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD021A3_GetAssetList]
AS
BEGIN
	
	
	
	SELECT		TOP 100
				DOC_NO			requestNo,				-- TFAST Request number
				LINE_NO			requestItem,			-- TFAST Request Item
				COMPANY				companyCode,			-- Company Code
				PARENT_ASSET_NO		refMainAssetNo,	-- Reference Main Asset No
				ASSET_MAINSUB		mainOrSubInd,		-- Indicator: Create Main or Sub number
				INDICATOR_VALIDATE	WBSCostCenterValInd, -- Indicator: Validate WBS(Budget)/Cost Center
				ASSET_CLASS			,		-- Asset Class
				ASSET_DESC			assetDesc,			-- Asset Description
				ADTL_ASSET_DESC		assetDescAdditional,	-- Additional Asset Description
				SERIAL_NO			serialNo,			-- Serial number
				INVEN_NO			inventoryNo,			-- Inventory Number
				BASE_UOM			baseUoM,			-- Base Unit of Measure
				LAST_INVEN_DATE		lastInvDate,	-- Last Inventory Date
				INVEN_INDICATOR		inventoryInd,	-- Inventory Indicator
				INVEN_NOTE			inventoryNote,			-- Inventory Note	
				COST_CODE			costCenter,			-- Cost Center
				RESP_COST_CODE		costCenterRespAsset,		-- Cost Center Responsible for Asset
				COMPANY				plant,
				[LOCATION]			assetLocation,			-- Asset location
				ROOM				room,				-- Room
				LICENSE_PLATE		licensePlate,		-- License Plate No. of Vehichle
				WBS_PROJECT			WBSDepre,		-- WBS (Depre cost)
	
				INVEST_REASON		investmentReason,		-- Investment Reason
				MINOR_CATEGORY		evalGroup1,		-- Evaluation group 1 (Minor Category)
				ASSET_STATUS		evalGroup2,		-- Evaluation group 2 (Asset Status)
				MACHINE_LICENSE		evalGroup3,	-- Evaluation group 3 (Asset Type)
				BOI_NO				evalGroup4,				-- Evaluation group 4 (Tax Privilege)
				FINAL_ASSET_CLASS	evalGroup5,	-- Evaluation group 5
				ASSET_SUPPER_NO		assetSuperNo,	-- Asset Super Number
				MANUFACT_ASSET		assetManu,		-- Manufacturer of Asset
				WBS_BUDGET			WBSBudget,			-- WBS (Budget)

				LEASE_AGREE_DATE,	-- Leasing: Agreement Date
				LEASE_START_DATE,	-- Lease start date
				LEASE_LENGTH_YEAR	leaseYear,	-- Lease Length (year)
				LEASE_LENGTH_PERD	leasePeriod,	-- Lease Length (period)
				LEASE_TYPE,			-- Leasing Type
				LEASE_SUPPLEMENT	leaseSupplText,	-- Leasing Supplementary text
				LEASE_NUM_PAYMENT	leasePaymentNo,	-- No. lease payments
				LEASE_PAY_CYCLE		leasePaymentCycle,	-- Leasing: Payment Cycle
				LEASE_ADV_PAYMENT	leaseAdvPayment,	-- Leasing:Advance Payments
				LEASE_PAYMENT		leasePayment,		-- Lease payment
				LEASE_ANU_INT_RATE	leaseAnnualIntRate,	-- Leasing: Annual Interest Rate
				DPRE_AREA_01		depreArea01,		-- Depreciation Area_01
				DPRE_KEY_01			depreKey01,		-- Depreciation key_01
				PLAN_LIFE_YEAR_01	planUseLifeYear01,	-- Planned Useful Life in Years_01
				PLAN_LIFE_PERD_01	planUseLifePeriod01,	-- Planned useful life in periods_01
				SCRAP_VALUE_01		scrapVal01,		-- Scrap value_01


				DEACT_DEPRE_AREA_15	deactiveDepreAreaInd15, -- Flag: Deactivate Depre.Area 15
				DPRE_AREA_15		depreArea15,		-- Depreciation Area_15
				DPRE_KEY_15			depreKey15,		-- Depreciation key_15	
				PLAN_LIFE_YEAR_15	planUseLifeYear15,	-- Planned Useful Life in Years_15
				PLAN_LIFE_PERD_15	planUseLifePeriod15,	-- Planned useful life in periods_15
				SCRAP_VALUE_15		scrapVal15,		-- Scrap value_15	
							
				DPRE_AREA_31		depreArea31,		-- Depreciation Area_31
				DPRE_KEY_31			depreKey31,		-- Depreciation key_31
				PLAN_LIFE_YEAR_31	planUseLifeYear31,	-- Planned Useful Life in Years_31
				PLAN_LIFE_PERD_31	planUseLifePeriod31,	-- Planned useful life in periods_31
				SCRAP_VALUE_31		scrapVal31,		-- Scrap value_31
				 
				DPRE_AREA_41		depreArea41,		-- 
				DPRE_KEY_41			depreKey41,		-- Depreciation key_41
				PLAN_LIFE_YEAR_41	planUseLifeYear41,	-- Planned Useful Life in Years_41
				PLAN_LIFE_PERD_41	planUseLifePeriod41,	-- Planned useful life in periods_41
				SCRAP_VALUE_41		scrapVal41,		-- Scrap value_41
				
				DPRE_AREA_81		depreArea81,		-- Depreciation Area_81
				DPRE_KEY_81			depreKey81,		-- Depreciation key_81
				PLAN_LIFE_YEAR_81	planUseLifeYear81,	-- Planned Useful Life in Years_81
				PLAN_LIFE_PERD_81	planUseLifePeriod81,	-- Planned useful life in periods_81
				SCRAP_VALUE_81		scrapVal81,		-- Scrap value_81

				DPRE_AREA_91		depreArea91,		-- Depreciation Area_91
				DPRE_KEY_91			depreKey91,		-- Depreciation key_91
				PLAN_LIFE_YEAR_91	planUseLifeYear91,	-- Planned Useful Life in Years_91
				PLAN_LIFE_PERD_91	planUseLifePeriod91,	-- Planned useful life in periods_91
				SCRAP_VALUE_91		scrapVal91		--Scrap value_91
	FROM		TB_R_REQUEST_ASSET_D D
	WHERE		D.[STATUS]		= 'GEN' AND
				D.ASSET_NO		IS NULL
	ORDER		BY
				D.DOC_NO, D.LINE_NO
	

END
GO
