DROP PROCEDURE [dbo].[sp_BFD01160_UpdateNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_UpdateNotification]
@ID			INT,
@SendFlag	VARCHAR(1),
@ResultFlag	VARCHAR(1),
@UpdateUser	VARCHAR(8)
AS
BEGIN
	UPDATE	TB_R_NOTIFICATION
	SET		SEND_FLAG	= @SendFlag,
			RESULT_FLAG = @ResultFlag,
			--SEND_FAIL_NUMBER = @ErrorCnt,
			UPDATE_DATE = GETDATE(),
			UPDATE_BY	= @UpdateUser
	WHERE	ID			= @ID
	
END



GO
