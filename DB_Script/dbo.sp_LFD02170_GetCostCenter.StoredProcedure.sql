DROP PROCEDURE [dbo].[sp_LFD02170_GetCostCenter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- ===========================================
CREATE PROCEDURE [dbo].[sp_LFD02170_GetCostCenter]
@COMPANY	T_COMPANY,
@COST_CODE	T_COST_CODE
AS
BEGIN	
	SET NOCOUNT ON;

	   DECLARE @UPDATE_DATE DATE 
	    
       SELECT @UPDATE_DATE=MAX(UPDATE_DATE)
	   FROM TB_M_ASSETS_LOC 
	   WHERE COST_CODE = @COST_CODE 


		SELECT	COMPANY,
				COST_CODE,
				COST_NAME,
				FORMAT(@UPDATE_DATE,'dd-MMM-yy') AS AS_OF
		FROM	TB_M_COST_CENTER
		WHERE	COMPANY	  = @COMPANY AND
				COST_CODE = @COST_CODE AND  
				[STATUS]	= 'Y'
		

END
GO
