DROP PROCEDURE [dbo].[sp_BFD01140_CheckWaitingBatchIsExists]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_BFD01140_CheckWaitingBatchIsExists]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	COUNT(*)
	FROM	TB_BATCH_Q
	--WHERE	[STATUS] = 'Q'
		WHERE 1=1
			AND [STATUS] = 'Q' 
			--OR ([STATUS] = 'P' AND DATEDIFF(minute, UPDATE_DATE, GETDATE()) < 30)

END




GO
