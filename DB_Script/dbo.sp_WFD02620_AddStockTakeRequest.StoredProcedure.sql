DROP PROCEDURE [dbo].[sp_WFD02620_AddStockTakeRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 15-02-2017
-- Description:	Add stock take request process data
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02620_AddStockTakeRequest]
	-- Add the parameters for the stored procedure here
	@CREATE_BY				T_USER
	, @COMPANY				T_COMPANY
	, @YEAR					varchar(4)
	, @ROUND				varchar(2)
	, @ASSET_LOCATION		varchar(1)
	, @DATA_AS_OF			date
	, @TARGET_DATE_FROM		date
	, @TARGET_DATE_TO		date
	, @QTY_OF_HANDHELD		int
	, @BREAK_TIME_MINUTE	int

	, @ASSET_STATUS			varchar(2)	
	, @ASSET_CLASS			T_ASSET_CLASS	
	, @MINOR_CATEGORY		T_MINOR_CATEGORY	
	, @COST_CENTER			varchar(200)	
	, @RESPONSIBLE_COST_CENTER		varchar(200)	
	, @BOI					varchar(2)	
	, @INVENTORY_CHECK		varchar(2)	
AS
	DECLARE	@TransactionName	nvarchar(25) = 'addstocktaketransaction'
			, @STOCK_TAKE_KEY	varchar(7);



	DECLARE @tb_Holidays TABLE (
		DT	DATE
	);

	DECLARE @MESSAGE NVARCHAR(250);

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN @TransactionName
	BEGIN TRY


		-- ///////////////////////////////////////////////////////////////////////		
		-- Validate add data

		--In case check Duplicate, System will check data in TB_R_STOCK_TAKE_H by follow key:								
		--	- Stock Taking Round(Year)							
		--	- Stock Taking Round(Round)							
		--	- Asset Location							
		IF(SELECT COUNT(1) FROM TB_R_STOCK_TAKE_H WHERE ASSET_LOCATION=@ASSET_LOCATION and YEAR=@YEAR and ROUND=@ROUND) > 0
		BEGIN


			SELECT TOP 1 @MESSAGE = MESSAGE_TEXT FROM TB_M_MESSAGE WHERE MESSAGE_CODE='MSTD0010AERR'

			SET @MESSAGE = '[CUSTOM]' + 'MSTD0010AERR|' + REPLACE(
								REPLACE(
									REPLACE(@MESSAGE, '{0}','COMPANY = '+@COMPANY + 'Year/Round')
									, '{1}', @YEAR + '/' + @ROUND + ' and Asset Location = ' 
										+ (	SELECT TOP 1 VALUE FROM TB_M_SYSTEM 
											WHERE CATEGORY='COMMON' and SUB_CATEGORY = 'ASSET_LOCATION' and CODE=@ASSET_LOCATION ))
								, '{2}', 'TB_R_STOCK_TAKE_H');
			
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;

			

		END


		--System not allow create new plan,
		--Incase old plan not finish
		---> check  by Asset Location (Inhouse /out source)

		

		ELSE IF(SELECT COUNT(1) FROM TB_R_STOCK_TAKE_H WHERE ASSET_LOCATION=@ASSET_LOCATION and PLAN_STATUS <> 'F') > 0
		BEGIN
		
			SET @MESSAGE = '[CUSTOM]MSTD0000AERR|System not allow create new plan, Incase old plan not finish
'
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;

		END

		


		-- Generate holiday list
		DECLARE @DT DATE;
		SET @DT=@TARGET_DATE_FROM;
		WHILE @DT<=@TARGET_DATE_TO
		BEGIN

			IF(DATEPART(WEEKDAY, @DT) in (1,7))
			BEGIN
				INSERT INTO @tb_Holidays select @DT as DT
			END

			SET @DT=DATEADD(day,1,@DT)

		END


		-- set a2+a3+a4 : Stock take ket
		--SET @STOCK_TAKE_KEY = CONCAT(@CREATE_BY , @YEAR , @ROUND , @ASSET_LOCATION);
		SET @STOCK_TAKE_KEY = CONCAT( @YEAR , @ROUND , @ASSET_LOCATION);

		-- ///////////////////////////////////////////////////////////////////////////
		-- Insert Header Data

		INSERT INTO [dbo].[TB_R_STOCK_TAKE_H]
           ([STOCK_TAKE_KEY]
           ,[COMPANY]
           ,[YEAR]
           ,[ROUND]
           ,[ASSET_LOCATION]
           ,[DATA_AS_OF]
           ,[TARGET_DATE_FROM]
           ,[TARGET_DATE_TO]
           ,[QTY_OF_HANDHELD]
           ,[BREAK_TIME_MINUTE]
           ,[ASSET_STATUS]
           ,[PLAN_STATUS]
           ,[ASSET_CLASS]
           ,[COST_CENTER]
           ,[RESPONSE_CC]
           ,[MINOR_CATEGORY]
           ,[INVEST_REASON]
           ,[INVEN_INDICATOR]
           ,[REMARK]
           ,[TOTAL]
           ,[CREATE_DATE]
           ,[CREATE_BY]
           ,[UPDATE_DATE]
           ,[UPDATE_BY])
     VALUES
           ( @STOCK_TAKE_KEY
			,@COMPANY
		    ,@YEAR
			,@ROUND
			,@ASSET_LOCATION
			,@DATA_AS_OF
			,@TARGET_DATE_FROM
			,@TARGET_DATE_TO
			,@QTY_OF_HANDHELD
			,@BREAK_TIME_MINUTE
			,@ASSET_STATUS
			,'D' --Set "D"(DRAFT)
			,@ASSET_CLASS 
			,@COST_CENTER 
			,@RESPONSIBLE_COST_CENTER 
			,@MINOR_CATEGORY 
			,@BOI 
			,@INVENTORY_CHECK 
			,NULL 
			,0 
			,GETDATE()
			,@CREATE_BY
			,GETDATE()
			,@CREATE_BY)
		   


			--- Insert Detail table 
			DECLARE @V_STOCK_TAKE_KEY	varchar(17);
			DECLARE @V_COMPANY	varchar(10);
			DECLARE @V_YEAR		varchar(4);
			DECLARE @V_ROUND	varchar(2);
			DECLARE @V_ASSET_LOCATION		varchar(1);
			DECLARE @V_ASSET_NO			varchar(12);
			DECLARE @V_ASSET_NAME			varchar(50);
			DECLARE @V_ASSET_CLASS			varchar(8);
			DECLARE @V_MINOR_CATEGORY		varchar(4);	
			DECLARE @V_COST_CODE			varchar(10);	
			DECLARE @V_COST_NAME			varchar(20);		
			DECLARE @V_EMP_CODE					varchar(50);	
			DECLARE @V_LOCATION		varchar(10);
			DECLARE @V_CUMU_ACQU_PRDCOST_01		varchar(14);	
			DECLARE @V_SERIAL_NO		varchar(18);
			DECLARE @V_MODEL		varchar(40);
		


		 /*
			DECLARE _CURSOR CURSOR FOR
     
			SELECT @STOCK_TAKE_KEY,AH.COMPANY,@YEAR,@ROUND,@ASSET_LOCATION,AH.ASSET_NO,AH.ASSET_NAME,AH.COST_CODE,CC.COST_NAME,EM.EMP_CODE,AH.[LOCATION],AH.SERIAL_NO,'' AS MODEL,AH.CUMU_ACQU_PRDCOST_01
			FROM TB_M_ASSETS_H AH
			INNER JOIN TB_M_COST_CENTER CC
			ON AH.COST_CODE = CC.COST_CODE AND Ah.COMPANY = cc.COMPANY 
			INNER JOIN TB_M_EMPLOYEE EM
			ON CC.COST_CODE = EM.COST_CODE AND CC.COMPANY = EM.COMPANY
			WHERE AH.COMPANY = @COMPANY 
			AND AH.MINOR_CATEGORY IN ( SELECT [VALUE] AS MINOR_CATEGORY FROM [dbo].[FN_SPLIT](@MINOR_CATEGORY,'#') ) 
			AND AH.ASSET_CLASS IN (SELECT [VALUE] AS ASSET_CLASS FROM [dbo].[FN_SPLIT](@ASSET_CLASS,'#') )
		


			OPEN _CURSOR
			FETCH NEXT FROM _CURSOR
				  INTO @V_STOCK_TAKE_KEY,@V_COMPANY,@V_YEAR,@V_ROUND,@V_ASSET_LOCATION,@V_ASSET_NO,@V_ASSET_NAME,@V_COST_CODE,@V_COST_NAME,@V_EMP_CODE,@V_LOCATION,@V_SERIAL_NO,@V_MODEL,@V_CUMU_ACQU_PRDCOST_01
			 WHILE @@FETCH_STATUS = 0
				BEGIN 
							IF (@ASSET_LOCATION='I') -- Insert Case
							BEGIN

							PRINT '1'; 
							  --exec sp_WFD02620_Insert_Update_IncaseInHouse @STOCK_TAKE_KEY,@COMPANY,@YEAR,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_NO,@ASSET_NAME,@COST_CODE,@COST_NAME,@EMP_CODE,@LOCATION,@SERIAL_NO,@MODEL,@CUMU_ACQU_PRDCOST_01
							END
							 IF (@ASSET_LOCATION='O') -- Insert Case
							BEGIN
							PRINT '2'; 
							 -- exec sp_WFD02620_Insert_Update_IncaseOutSource  @STOCK_TAKE_KEY,@COMPANY,@YEAR,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_NO,@ASSET_NAME,@COST_CODE,@COST_NAME,@EMP_CODE,@LOCATION,@SERIAL_NO,@MODEL,@CUMU_ACQU_PRDCOST_01
							END		
				
			   END 
			 CLOSE _CURSOR


		*/
		

		-- IF (@ASSET_LOCATION='I') -- Insert Case
		--BEGIN
		--  exec sp_WFD02620_Insert_Update_IncaseInHouse @CREATE_BY,@COMPANY, @YEAR ,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_STATUS,@SUB_TYPE
		--END
		-- IF (@ASSET_LOCATION='O') -- Insert Case
		--BEGIN
		--  exec sp_WFD02620_Insert_Update_IncaseOutSource @CREATE_BY,@COMPANY, @YEAR ,@ROUND,@ASSET_LOCATION,@DATA_AS_OF,@ASSET_STATUS,@SUB_TYPE
		--END		
		
		--Insert TB_R_STOCK_TAKE_D

		INSERT INTO TB_R_STOCK_TAKE_D 
		(STOCK_TAKE_KEY,
		YEAR,
		ROUND,
		ASSET_LOCATION,
		COMPANY,
		ASSET_NO,
		ASSET_NAME,
		EMP_CODE,
		COST_CODE,
		COST_NAME,
		BARCODE,
		LOCATION_NAME,
		CREATE_BY,
		SERIAL_NO,
		ASSET_CLASS)

		SELECT top 1
		@STOCK_TAKE_KEY,
		@YEAR,
		@ROUND,
		@ASSET_LOCATION,
		@COMPANY,
		AH.ASSET_NO,
		AH.ASSET_NAME,
		E.EMP_CODE,
		cc.COST_CODE,
		cc.COST_NAME,
		AH.BARCODE,
		AH.LOCATION_REMARK,
		@CREATE_BY,
		AH.SERIAL_NO,
		@ASSET_CLASS
		from TB_M_ASSETS_H AH (nolock)
		inner join TB_M_COST_CENTER cc (nolock) on cc.COMPANY=@COMPANY
		and cc.COST_CODE=AH.COST_CODE
		inner join TB_M_EMPLOYEE E (nolock) on E.COST_CODE=AH.COST_CODE
		and E.COMPANY=@COMPANY
		where AH.COMPANY=@COMPANY
		and AH.ASSET_CLASS=@ASSET_CLASS
		and AH.COST_CODE=@COST_CENTER
		and AH.MINOR_CATEGORY=@MINOR_CATEGORY

		 --Insert TB_R_STOCK_TAKE_D_PER_SV
		 INSERT INTO TB_R_STOCK_TAKE_D_PER_SV
				   ([STOCK_TAKE_KEY] ,[COMPANY],[YEAR] ,[ROUND] ,[ASSET_LOCATION] ,[EMP_CODE] ,[EMP_TITLE]
				   ,[EMP_NAME]  ,[EMP_LASTNAME]    ,[FA_DATE_FROM] ,[FA_DATE_TO]
				   ,[DATE_FROM]  ,[DATE_TO]  ,[TIME_START]  ,[TIME_END]
				   ,[TIME_MINUTE]   ,[USAGE_HANDHELD]   ,[IS_LOCK]  ,[TOTAL_ASSET]  
					,[CREATE_DATE]   ,[CREATE_BY]   ,[UPDATE_DATE]    ,[UPDATE_BY])
		 SELECT  @STOCK_TAKE_KEY,@COMPANY ,@YEAR ,@ROUND,@ASSET_LOCATION
					, SV_EMP_CODE--<EMP_CODE, varchar(8),>
					, EM.EMP_TITLE -- <EMP_TITLE, varchar(8),>
					, EM.EMP_NAME -- <EMP_NAME, varchar(30),>
					, EM.EMP_LASTNAME -- <EMP_LASTNAME, nvarchar(30),>
					,NULL --<FA_DATE_FROM, date,>
					,NULL --<FA_DATE_TO, date,>
					,NULL --<DATE_FROM, date,>
					,NULL --<DATE_TO, date,>
					,NULL --<TIME_START, varchar(5),>
					,NULL --<TIME_END, varchar(5),>
					,0 --<TIME_MINUTE, int,>
					,NULL --<USAGE_HANDHELD, int,>
					,'N' -- <IS_LOCK, varchar(1),>
					,COUNT(ASSET_NO) --<TOTAL_ASSET, int,>
					,GETDATE(), @CREATE_BY,  GETDATE(), @CREATE_BY 
		FROM TB_R_STOCK_TAKE_D SD
		INNER JOIN TB_M_EMPLOYEE EM 
		ON SD.SV_EMP_CODE = EM.EMP_CODE AND SD.COMPANY = EM.COMPANY 
		WHERE SD.STOCK_TAKE_KEY = @STOCK_TAKE_KEY
		GROUP BY SV_EMP_CODE,EMP_TITLE,EMP_NAME,EMP_LASTNAME



		-- TB_R_STOCK_TAKE_HOLIDAY
		INSERT INTO TB_R_STOCK_TAKE_HOLIDAY (STOCK_TAKE_KEY,HOLIDATE_DATE,COMPANY)
		SELECT @STOCK_TAKE_KEY as STOCK_TAKE_KEY, DT as HOLIDATE_DATE ,@COMPANY as COMPANY FROM @tb_Holidays
		


		DECLARE @ALL_ASSET_TOTAL INT  = 0

		SELECT @ALL_ASSET_TOTAL =COUNT(*) 
		FROM TB_R_STOCK_TAKE_D  StockD
		WHERE StockD.STOCK_TAKE_KEY = @STOCK_TAKE_KEY

		UPDATE StockH
		SET  
			StockH.TOTAL = @ALL_ASSET_TOTAL 

		FROM TB_R_STOCK_TAKE_H StockH 
		WHERE StockH.STOCK_TAKE_KEY = @STOCK_TAKE_KEY




		IF( SELECT COUNT(1) FROM TB_R_STOCK_TAKE_D WHERE STOCK_TAKE_KEY = @STOCK_TAKE_KEY ) <= 0
		BEGIN		
		
			SET @MESSAGE = '[CUSTOM]MSTD0000AERR|There is no asset data for stock taking'
			RAISERROR (@MESSAGE, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			return;			
		END

		
		COMMIT;

		select @STOCK_TAKE_KEY as STOCK_TAKE_KEY
			return;		
	END TRY
	BEGIN CATCH

		ROLLBACK ;

		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			   @ErrorSeverity = ERROR_SEVERITY(),
			   @ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );

	END CATCH




END





GO
