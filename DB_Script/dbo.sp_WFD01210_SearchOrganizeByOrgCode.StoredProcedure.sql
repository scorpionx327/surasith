DROP PROCEDURE [dbo].[sp_WFD01210_SearchOrganizeByOrgCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 09/03/2017
-- Description:	get organize by emp.org_code
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_SearchOrganizeByOrgCode]
	  @FIND varchar(60)
	, @ORG_CODE varchar(21)
	, @pageNum INT
    , @pageSize INT
    , @sortColumnName VARCHAR(50)
	, @orderType VARCHAR(5)
	, @TOTAL_ITEM		int output

AS
BEGIN
SELECT 	@TOTAL_ITEM = COUNT(1)
			FROM [TB_M_ORGANIZATION]
		  where (ORG_CODE = @ORG_CODE OR @ORG_CODE IS NULL)
		  AND ((ORG_CODE like			'%'+ @FIND	+'%'	 OR @FIND IS NULL)
				 OR (CMP_CODE like		'%'+ @FIND	+'%'	 OR @FIND IS NULL)
				 OR (DIV_NAME like		'%'+@FIND	+'%'	 OR @FIND IS NULL)
				 OR (DEPT_NAME like		'%'+@FIND	+'%'	 OR @FIND IS NULL)
				 OR (SECTION_NAME like		'%'+ @FIND	+'%'	 OR @FIND IS NULL)
				 OR (LINE_NAME like		'%'+ @FIND	+'%'	 OR @FIND IS NULL));

WITH Paging AS
(
SELECT  *	
, CAST(ROW_NUMBER() OVER 
			 (ORDER BY 
				CASE WHEN @sortColumnName = '0' and @orderType='asc' THEN ORG_CODE
					END ASC,											 
				CASE WHEN @sortColumnName = '0' and @orderType='desc' THEN ORG_CODE
					END DESC,
				CASE WHEN @sortColumnName = '1' and @orderType='asc' THEN CMP_CODE
					END ASC,
				CASE WHEN @sortColumnName = '1' and @orderType='desc' THEN CMP_CODE
					END DESC,
				CASE WHEN @sortColumnName = '3' and @orderType='asc' THEN DIV_NAME
					END ASC,			  
				CASE WHEN @sortColumnName = '3' and @orderType='desc' THEN DIV_NAME
					END DESC,
				CASE WHEN @sortColumnName = '4' and @orderType='asc' THEN SUB_DIV_NAME
					END ASC,			  
				CASE WHEN @sortColumnName = '4' and @orderType='desc' THEN SUB_DIV_NAME
					END DESC,
				CASE WHEN @sortColumnName = '5' and @orderType='asc' THEN DEPT_NAME
					END ASC,			  
				CASE WHEN @sortColumnName = '5' and @orderType='desc' THEN DEPT_NAME
					END DESC,
				CASE WHEN @sortColumnName = '6' and @orderType='asc' THEN SECTION_NAME
					END ASC,			  
				CASE WHEN @sortColumnName = '6' and @orderType='desc' THEN SECTION_NAME
					END DESC,
				CASE WHEN @sortColumnName = '7' and @orderType='asc' THEN LINE_NAME
					END ASC,			  
				CASE WHEN @sortColumnName = '7' and @orderType='desc' THEN LINE_NAME
					END DESC,
				CASE WHEN @sortColumnName = '7' and @orderType='asc' THEN CMP_ABBR
					END ASC,			  
				CASE WHEN @sortColumnName = '7' and @orderType='desc' THEN CMP_ABBR
					END DESC
				 ) AS INT) AS RowNumber
  FROM [TB_M_ORGANIZATION]
  where  (ORG_CODE = @ORG_CODE OR @ORG_CODE IS NULL)
		AND (
				 (ORG_CODE like			'%'+ @FIND	+'%' OR @FIND IS NULL)
				OR (CMP_CODE like		'%'+ @FIND	+'%' OR @FIND IS NULL)
				--OR (GRP_NAME like		'%'+ @FIND	+'%' OR @FIND IS NULL)
				OR (DIV_NAME like		'%'+@FIND	+'%' OR @FIND IS NULL)
				OR (DEPT_NAME like		'%'+@FIND	+'%' OR @FIND IS NULL)
				OR (SECTION_NAME like		'%'+ @FIND	+'%' OR @FIND IS NULL)
				OR (LINE_NAME like		'%'+ @FIND	+'%' OR @FIND IS NULL)
				--OR (SUBUNIT_NAME like	'%'+ @FIND  +'%'OR @FIND IS NULL)
				)
)
  SELECT *
  FROM Paging
  WHERE RowNumber BETWEEN (@pageNum - 1) * @pageSize + 1 
   AND @pageNum * @pageSize




END



GO
