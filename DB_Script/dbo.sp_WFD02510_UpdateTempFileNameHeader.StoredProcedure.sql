DROP PROCEDURE [dbo].[sp_WFD02510_UpdateTempFileNameHeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD02510_UpdateTempFileNameHeader]
(
	@DOC_NO						VARCHAR(10)		= NULL,
	@LOSS_COUNTER_MEASURE		VARCHAR(200)	= NULL,
	@DISP_MASS_ATTACH_DOC		VARCHAR(200)	= NULL,
	@USER_BY					VARCHAR(8)		= NULL
)
	
AS
BEGIN
	UPDATE TB_R_REQUEST_DISP_H
	SET LOSS_COUNTER_MEASURE	= @LOSS_COUNTER_MEASURE,
	 DISP_MASS_ATTACH_DOC		= @DISP_MASS_ATTACH_DOC
	WHERE DOC_NO				= @DOC_NO

END





GO
