DROP PROCEDURE [dbo].[sp_BFD01160_InsertMarkLocationNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BFD01160_InsertMarkLocationNotification]
@User	VARCHAR(8)
AS
BEGIN
	
	DECLARE @Category VARCHAR(40)		= 'NOTIFICATION'
	DECLARE @SubCategory VARCHAR(40)	= 'ALLW_MAP'
	DECLARE @NextTimeCode VARCHAR(40)	= 'NEXTTIME'

	--------------------------------------------------------------------------------------------------------
	-- Weekly
	DECLARE @dNextTime DATETIME
	SET @dNextTime = dbo.fn_BFD02160_GetNextDate(@Category, @SubCategory, @NextTimeCode)
	IF(@dNextTime > GETDATE())
		RETURN

	DECLARE @s VARCHAR(400)
	SET @s = dbo.fn_GetSystemMaster(@Category,@SubCategory,'DUEDATE')

	DECLARE @iDueDate INT
	SET @iDueDate = CONVERT(INT, @s)
	
	DECLARE @ParentFlag VARCHAR(400)
	SET @ParentFlag = dbo.fn_GetSystemMaster(@Category, @SubCategory, 'PARENT_FLAG')
	
	-- Sending to SV & GL
	-- NOTIFICATION	ASSET_TYPE_ALLW_PRINT
	SELECT	D.COST_CODE, 'N' AS FLAG
	INTO	#TB_T_COST_CODE
	FROM	TB_M_ASSETS_H H
	INNER	JOIN
			TB_M_ASSETS_D D
	ON		H.ASSET_NO = D.ASSET_NO	
	WHERE	H.[STATUS] = 'Y' AND
			H.MAP_STATUS = 'N' AND
			DATEADD(DAY, @iDueDate, H.DATE_IN_SERVICE) < GETDATE() AND
			(
				(@ParentFlag = 'P' AND H.ASSET_NO LIKE '%00') OR
				(@ParentFlag = 'C' AND H.ASSET_NO NOT LIKE '%00') OR
				(@ParentFlag = 'B')
			) AND
			H.ASSET_CATEGORY NOT IN 
							(
								SELECT A.ASSET_CATEGORY FROM  dbo.fn_FD0GetAdminAsset() A
							) AND
			H.ASSET_TYPE IN (	SELECT	[VALUE] 
								FROM	TB_M_SYSTEM 
								WHERE	CATEGORY = @Category AND 
										SUB_CATEGORY = CONCAT(@SubCategory, '_ASSET_TYPE') AND ACTIVE_FLAG = 'Y' 
							) AND
			H.SUB_TYPE IN	(	SELECT	[VALUE] 
								FROM	TB_M_SYSTEM 
								WHERE	CATEGORY = @Category AND 
										SUB_CATEGORY = CONCAT(@SubCategory, '_SUB_TYPE') AND ACTIVE_FLAG = 'Y'
							) 
							------########### S FOR TEST DUPLICATE EMAIL 
							--	AND	D.COST_CODE = 'C01DB002'
							------########### E FOR TEST DUPLICATE EMAIL 

	GROUP	BY
			D.COST_CODE
	IF @@ROWCOUNT = 0
		GOTO SETNEXT
	
	-- Sending a Email
	DECLARE @CC VARCHAR(8), @BodyTemplate VARCHAR(400), @SubjectTemplate VARCHAR(400)
	--[S]FIX CASE MAIL DUPLICATE BY NUTTAPON 2017/12/15
	DECLARE @TB_T_EMP_DIS TABLE(EMP_CODE VARCHAR(8), SHORT_NAME VARCHAR(68), EMAIL VARCHAR(50))
	--[E]FIX CASE MAIL DUPLICATE BY NUTTAPON 2017/12/15

	SET @SubjectTemplate	= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','SUBJECT','BFD02160_MARKLOCATION')
	SET @BodyTemplate		= dbo.fn_GetSystemMaster('SYSTEM_EMAIL','BODY','BFD02160_MARKLOCATION')

	DECLARE @TB_T_EMP TABLE(EMP_CODE VARCHAR(8), SHORT_NAME VARCHAR(68), EMAIL VARCHAR(50))

	WHILE(EXISTS(SELECT 1 FROM #TB_T_COST_CODE WHERE FLAG = 'N'))
	BEGIN
		SELECT	TOP 1 @CC = COST_CODE FROM	#TB_T_COST_CODE WHERE FLAG = 'N'
		UPDATE #TB_T_COST_CODE SET FLAG = 'Y' WHERE COST_CODE = @CC

		DELETE
		FROM	@TB_T_EMP

		-- Get SV
		INSERT
		INTO	@TB_T_EMP(EMP_CODE, SHORT_NAME, EMAIL)
		SELECT	E.EMP_CODE, dbo.fn_GetShortENName(E.EMP_CODE), E.EMAIL
		FROM	TB_M_SV_COST_CENTER SV
		INNER	JOIN
				TB_M_EMPLOYEE E
		ON		SV.EMP_CODE		= E.EMP_CODE
		WHERE	SV.COST_CODE	= @CC

		-- Get GL
		DECLARE @GLCode VARCHAR(3) 
		SET @GLCode = 'GL'
		INSERT
		INTO	@TB_T_EMP(EMP_CODE, SHORT_NAME, EMAIL)
		SELECT	E.EMP_CODE, dbo.fn_GetShortENName(E.EMP_CODE), E.EMAIL
		FROM	dbo.fn_FD0GetEmpCode(@CC, @GLCode) F
		INNER	JOIN
				TB_M_EMPLOYEE E
		ON		F.EMP_CODE = E.EMP_CODE

		--[S]FIX CASE MAIL DUPLICATE BY NUTTAPON 2017/12/15
		INSERT INTO @TB_T_EMP_DIS
		SELECT DISTINCT * FROM @TB_T_EMP
		--[E]FIX CASE MAIL DUPLICATE BY NUTTAPON 2017/12/15

		INSERT
		INTO	TB_R_NOTIFICATION
		(		ID,
				[TO],
				CC,
				BCC,
				TITLE,
				[MESSAGE],
				FOOTER,
				[TYPE],
				START_PERIOD,
				END_PERIOD,
				SEND_FLAG,
				RESULT_FLAG,
				REF_FUNC,
				REF_DOC_NO,
				CREATE_DATE,
				CREATE_BY
		)

		SELECT	NEXT VALUE FOR NOTIFICATION_ID,
				T.EMAIL		AS [TO],
				NULL		AS CC,
				NULL		AS BCC,
				dbo.fn_StringFormat(@SubjectTemplate,@CC) AS TITLE,
				dbo.fn_StringFormat(@BodyTemplate,CONCAT(T.SHORT_NAME,'|', @CC)) AS [MESSAGE],
				NULL	AS FOOTER, -- Get From Default
				'I'		AS [TYPE],
				NULL	AS [START_PERIOD],
				NULL	AS [END_PERIOD],
				'N'		AS [SEND_FLAG],
				NULL	AS RESULT_FLAG,
				'BFD02160'	AS REF_FUNC,
				NULL	AS REF_DOC_NO,
				GETDATE(),
				@User
			
		--[S]FIX CASE MAIL DUPLICATE BY NUTTAPON 2017/12/15
		--FROM	@TB_T_EMP T	
		FROM	@TB_T_EMP_DIS T	
		--[E]FIX CASE MAIL DUPLICATE BY NUTTAPON 2017/12/15

	END -- WHILE(EXISTS(SELECT 1 FROM #TB_T_COST_CODE WHERE FLAG = 'N'))
	

	--------------------------------------------------------------------------------------------------------
	-- Update Next Time
SETNEXT:	
	SET @s = dbo.fn_GetSystemMaster(@Category,@SubCategory,'EVERY_DATE')
	SET @dNextTime = DATEADD(DAY, CONVERT(INT, @s), @dNextTime)
	
	UPDATE	TB_M_SYSTEM
	SET		[VALUE]			= FORMAT( @dNextTime, 'yyyy-MM-dd HH:m:ss' ),
			UPDATE_DATE		= GETDATE(),
			UPDATE_BY		= @User
	WHERE	CATEGORY		= @Category AND
			SUB_CATEGORY	= @SubCategory AND
			CODE			= @NextTimeCode
	IF OBJECT_ID('tempdb..#TB_T_COST_CODE') IS NOT NULL	
	DROP TABLE #TB_T_COST_CODE

END



GO
