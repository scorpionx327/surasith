DROP PROCEDURE [dbo].[sp_Common_InsertNotification]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jirawat Jannet
-- Create date: 2017-02-23
-- Description:	Insert notification email data
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_InsertNotification]
	-- Add the parameters for the stored procedure here
	@TO varchar(255) 
	,@CC varchar(255) 
	,@BCC varchar(255) 
	,@TITLE varchar(255) 
	,@MESSAGE nvarchar(max)
	,@FOOTER varchar(250) 
	,@TYPE varchar(1) 
	,@START_PERIOD datetime 
	,@END_PERIOD datetime 
	,@SEND_FLAG varchar(1) 
	,@RESULT_FLAG varchar(1) 
	,@REF_FUNC varchar(8) 
	,@REF_DOC_NO T_DOC_NO
	,@CREATE_BY varchar(8) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO TB_R_NOTIFICATION (  
 		ID
 		, [TO]
 		, CC
 		, BCC
 		, TITLE
 		, MESSAGE
 		, FOOTER
 		, TYPE
 		, START_PERIOD
 		, END_PERIOD
 		, SEND_FLAG
 		, RESULT_FLAG
 		, REF_FUNC
 		, REF_DOC_NO
 		, CREATE_DATE
 		, CREATE_BY
 		, UPDATE_DATE
 		, UPDATE_BY
 	) VALUES (  
 		NEXT VALUE FOR NOTIFICATION_ID
 		, @TO
 		, @CC
 		, @BCC
 		, @TITLE
 		, @MESSAGE
 		, @FOOTER
 		, @TYPE
 		, @START_PERIOD
 		, @END_PERIOD
 		, @SEND_FLAG
 		, @RESULT_FLAG
 		, @REF_FUNC
 		, @REF_DOC_NO
 		, GETDATE()
 		, @CREATE_BY
 		, GETDATE()
 		, @CREATE_BY
 	)   


END



GO
