DROP PROCEDURE [dbo].[sp_WFD01170_GetPermission]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetPermission 'C2017/4064','1432','C' 
CREATE PROCEDURE [dbo].[sp_WFD01170_GetPermission]
(
	@DOC_NO				T_DOC_NO,
	@EMP_CODE			T_SYS_USER,
	@REQUEST_TYPE		VARCHAR(1)
)
	
AS
BEGIN
	
	-- Remark No Allow delegate of Higher

	DECLARE @MODE				VARCHAR(1) = 'N', -- N : No authorize, V : View Only, A:Approve, R : VERIFY
			@DELEGATE			VARCHAR(1) = 'N',
			@MAIN				VARCHAR(1) = 'N',
			@HIGHER				VARCHAR(1) = 'N',
			@AEC				VARCHAR(1) = 'N',
			@DELETE_ASSET		VARCHAR(1) = 'N', -- Delete Item	
			@ALLOW_APPROVE		VARCHAR(1) = 'N',
			@ALLOW_REJECT		VARCHAR(1) = 'N',
			@ALLOW_SEL_APPV		VARCHAR(1) = 'N',
			@ALLOW_CHANGE_APPV	VARCHAR(1) = 'N',	
			@ACRCode			VARCHAR(3), 
			@FSCode				VARCHAR(3)
	SET @ACRCode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','ACR_CODE','ACR')
	SET @FSCode = dbo.fn_GetSystemMaster('SYSTEM_CONFIG','APPRV_ROLE_CODE','FS')
	

	DECLARE @DocStatus		VARCHAR(2), @Company	T_COMPANY
	SELECT	@DocStatus		= [STATUS],
			@Company		= COMPANY 
	FROM	TB_R_REQUEST_H WITH (NOLOCK) 
	WHERE	DOC_NO = @DOC_NO
	SET		@DocStatus		= ISNULL(@DocStatus,'00')

	PRINT CONCAT('DOC STATUS :',@DocStatus)

	DECLARE @FinishFlow VARCHAR(2)


	SET @FinishFlow = IIF(@DocStatus IN ('90','70','75') OR ( @DocStatus IN ('60') AND @REQUEST_TYPE = 'A'), 'Y','N') ;


	IF (@FinishFlow = 'Y')
	BEGIN
		SET @MODE = 'V'
		SET @ALLOW_APPROVE = 'N'
		SET @ALLOW_REJECT = 'N'
		GOTO RET
	END

	------------------------------------------------------------------------------------------------
	-- Document No exists

	DECLARE @CurrentApprover	T_SYS_USER,
			@CurrentApproveGrp	VARCHAR(3), 
			@CurrentIndx		INT, 
			@AllowReject		VARCHAR(1), 
			@AllowDelete		VARCHAR(1),
			@CurrentSelAppr		VARCHAR(1),
			@OrgCode			T_ORG_CODE,
			@OpMode				VARCHAR(1),	@Division VARCHAR(4),
			@IsBOIUser			VARCHAR(1),
			@IsFSUser			VARCHAR(1),
			@IsBOIState			VARCHAR(1) -- N, A, Y : N = No, A = Active, Y = Passed
	DECLARE @CurrentRole VARCHAR(3)
	SELECT	@CurrentApprover	= EMP_CODE, 
			@CurrentApproveGrp	= APPRV_GROUP,
			@CurrentIndx		= INDX,
			@AllowReject		= ALLOW_REJECT,
			@AllowDelete		= ALLOW_DEL_ITEM,
			@CurrentSelAppr		= ALLOW_SEL_APPRV,
			@OrgCode			= ORG_CODE,
			@OpMode				= [OPERATION_MODE],
			@Division			= DIVISION,
			@CurrentRole		= [APPR_ROLE] 
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO = @DOC_NO AND 
			APPR_STATUS = 'W'

	
	------------------------------------------------------------------------------------------------

	-- Is Admin ?
	DECLARE @Cnt INT, @Cond INT
	SELECT	@Cnt = COUNT(*),
			@Cond = SUM(CASE WHEN SYS_EMP_CODE = @EMP_CODE THEN 1 ELSE 0 END)
	FROM	TB_M_EMPLOYEE WHERE FAADMIN = 'Y'  AND (SYS_EMP_CODE = @EMP_CODE OR (DELEGATE_PIC = @EMP_CODE AND  GETDATE() BETWEEN DELEGATE_FROM AND DELEGATE_TO))
	IF @Cnt > 0
	BEGIN
		PRINT 'AEC'
		SET @AEC			= 'Y'
		SET @DELEGATE		= IIF(@Cond = 0, 'Y', 'N')
		SET @ALLOW_APPROVE	= 'Y'
		
		------------------------------------------------------------------------------------------- 
		
		
		-------------------------------------------------------------------------------------------
		-- Incase FA Admin Login to other status
		SET @ALLOW_CHANGE_APPV	= IIF(@FinishFlow = 'N','Y', 'N')
		SET @DELETE_ASSET		= IIF(@FinishFlow = 'N','Y', @DELETE_ASSET)
		SET @ALLOW_REJECT		= IIF(@FinishFlow = 'N','Y', @ALLOW_REJECT) -- Allow FA to Reject when Document not complete
		SET @ALLOW_SEL_APPV		= IIF(@FinishFlow = 'N','Y', @ALLOW_SEL_APPV) 
		GOTO RET

	END	

	-- BOI
	----------------------------------------------------------------------------------------------------
	DECLARE @iBOIIndex INT
	SELECT	@iBOIIndex = MAX(INDX) 
	FROM	TB_R_REQUEST_APPR WHERE DOC_NO = @DOC_NO AND APPR_ROLE = 'BOI' 
	
	
	SET @iBOIIndex = ISNULL(@iBOIIndex,9999)

	SET @IsBOIState = 'N' -- No BOI in flow, no passed
	IF @CurrentIndx = @iBOIIndex
	BEGIN
		SET @IsBOIState = 'A' -- Wait BOI
	END
	IF @CurrentIndx > @iBOIIndex
	BEGIN
		SET @IsBOIState = 'Y' -- Passed
	END

	SET @IsBOIUser = 'N'
	IF EXISTS(SELECT 1 FROM TB_M_SP_ROLE WHERE EMP_CODE = @EMP_CODE AND SP_ROLE = 'BOI')
	BEGIN
		SET @IsBOIUser = 'Y'
	END
	IF EXISTS(SELECT 1 FROM TB_M_SP_ROLE WHERE EMP_CODE = @EMP_CODE AND SP_ROLE = @FSCode)
	BEGIN
		SET @IsFSUser = 'Y'
	END
	----------------------------------------------------------------------------------------------------


	IF @DOC_NO IS NULL 
	BEGIN
		SET @DocStatus = '00'

		-- Check SV (SV Create a document)
		--IF EXISTS(SELECT 1 FROM TB_M_SV_COST_CENTER WHERE EMP_CODE = @EMP_CODE)
		--BEGIN
		SET @MAIN = 'Y'
		SET @DELETE_ASSET = 'Y'
		SET @ALLOW_SEL_APPV = 'Y'
		GOTO RET
	END
	-- If Login = Requestor or Login is delegate from Requestor
	IF	EXISTS(SELECT 1 FROM TB_R_REQUEST_H WHERE EMP_CODE = @EMP_CODE OR 
		EXISTS(	SELECT 1 FROM TB_M_EMPLOYEE E INNER JOIN TB_R_REQUEST_H R ON E.SYS_EMP_CODE = R.EMP_CODE 
						WHERE R.DOC_NO = @DOC_NO AND E.DELEGATE_TO = @EMP_CODE AND  GETDATE() BETWEEN DELEGATE_FROM AND DELEGATE_TO
	))
	BEGIN
		SET @ALLOW_CHANGE_APPV = 'Y'
	END
	
	IF @EMP_CODE = @CurrentApprover
	BEGIN
		SET @MAIN	= 'Y'
		
		SET @ALLOW_REJECT = @AllowReject
		SET @DELETE_ASSET = @AllowDelete
		SET @ALLOW_SEL_APPV = @CurrentSelAppr
		GOTO RET
	END
	ELSE IF dbo.fn_IsDelegateUser(@CurrentApprover, @EMP_CODE) = 'Y'
	BEGIN
		SET @DELEGATE	= 'Y'
		SET @ALLOW_REJECT = @AllowReject
		SET @DELETE_ASSET = @AllowDelete
		SET @ALLOW_SEL_APPV = @CurrentSelAppr
		GOTO RET
	END
	ELSE IF @OpMode = 'G' -- It's mean Login user not match with approver name
	BEGIN
		DECLARE @Result VARCHAR(1)
		EXEC sp_WFD01170_CheckGroupOperation @Company, @DOC_NO, @EMP_CODE, @CurrentIndx, @Division, @CurrentRole, @Result OUT
		IF @Result = 'Y'
		BEGIN
			SET @MAIN	= 'Y'		
			SET @ALLOW_REJECT = @AllowReject
			SET @DELETE_ASSET = @AllowDelete
			SET @ALLOW_SEL_APPV = @CurrentSelAppr
			GOTO RET
		END

	END

	-- No support delegate of higher
	-- Check Higher
	SELECT	TOP 1
			@AllowReject	= ALLOW_REJECT,
			@AllowDelete	= ALLOW_DEL_ITEM, 
			@CurrentSelAppr = ALLOW_SEL_APPRV,
			@OrgCode		= ORG_CODE
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND 
			APPR_STATUS		= 'P' AND
			APPRV_GROUP		= ISNULL(@CurrentApproveGrp, '-') AND
			INDX			> @CurrentIndx AND
			EMP_CODE		= @EMP_CODE AND
			[OPERATION_MODE]= 'P' -- Check Only Managerment (TBC with IS surasith 2019-09-08)
	
	IF @@ROWCOUNT > 0
	BEGIN
		SET @HIGHER = 'Y'
		SET @ALLOW_REJECT = @AllowReject
		SET @DELETE_ASSET = @AllowDelete
		SET @ALLOW_SEL_APPV = @CurrentSelAppr
		GOTO RET
	END
	-- Otherwise, Exists in Flow but can not approve
	SELECT	TOP 1
			@AllowReject	= ALLOW_REJECT 
	FROM	TB_R_REQUEST_APPR
	WHERE	DOC_NO			= @DOC_NO AND
			EMP_CODE		= @EMP_CODE
	IF @@ROWCOUNT > 0
	BEGIN
		SET @ALLOW_REJECT = 'N'
		SET @DELETE_ASSET = 'N'
		SET @MODE = 'V'
		GOTO RET 
	END

RET:
	
	-- SET MODE
	IF (@AEC = 'Y' OR @MAIN = 'Y' OR @DELEGATE = 'Y' OR @HIGHER = 'Y')
	BEGIN
		SET @MODE = 'A'
		SET @ALLOW_APPROVE	= 'Y' 
	END

	-- Check View Mode or No Authorize
	SELECT	@Cnt = COUNT(1) 
	FROM	TB_R_REQUEST_APPR A WITH (NOLOCK)
	INNER	JOIN
			TB_M_EMPLOYEE E
	ON		A.EMP_CODE	= E.SYS_EMP_CODE
	WHERE	DOC_NO		= @DOC_NO AND 
		(	A.EMP_CODE	= @EMP_CODE OR 
			dbo.fn_IsDelegateUser(A.EMP_CODE, @EMP_CODE) = 'Y' -- Delegate User
		)
	IF @MODE = 'N' AND @Cnt > 0 --OR  @REQUEST_TYPE = 'S')
	BEGIN
		SET @MODE = 'V'
	END
	
	DECLARE @OrgChanged VARCHAR(1) = 'N'

	IF(@AEC != 'Y' AND @DELEGATE != 'Y' ) --delegate not in same Organize with PIC (not check)
	BEGIN
		SELECT	@OrgChanged = dbo.fn_IsOrganizeChanged([ROLE], @OrgCode, ORG_CODE)
		FROM	TB_M_EMPLOYEE 
		WHERE	SYS_EMP_CODE = @EMP_CODE
	END	
	
	SET @OrgChanged = ISNULL(@OrgChanged, 'N') -- FA, Not found

	IF @OrgChanged = 'Y' 
	BEGIN
		SET @ALLOW_APPROVE = 'N'
		SET @DELETE_ASSET = 'N'
		SET @ALLOW_SEL_APPV = 'N'
		PRINT 'Org is changed'
	END
	
	DECLARE @EditAll VARCHAR(1) = 'N'
	

	SELECT	@MODE				AS Mode, 
			@DELEGATE			AS IsDelegate,
			@MAIN				AS IsMainApprover,
			@HIGHER				AS IsHigher,
			
			IIF(@MODE = 'A' AND @MAIN <> 'Y','Y','N') -- Can approve but not main
								AS IsInstead,
			@ALLOW_APPROVE		AS AllowApprove,	-- BUTTON
			@ALLOW_REJECT		AS AllowReject,	-- BUTTON
			@DELETE_ASSET 		AS AllowDelete,
			@ALLOW_SEL_APPV 	AS AllowSelectApprover,
			@ALLOW_CHANGE_APPV 	AS AllowChangeApprover,
			@IsBOIUser			AS IsBOIUser,
			@IsBOIState			AS IsBOIState,

			@IsFSUser			AS IsFSUser,
			IIF(@CurrentApprover = @FSCode,'Y', 'N') AS IsFSState,

			@AEC				AS IsAECUser,
			IIF(@CurrentApprover = @ACRCode,'Y', 'N') AS IsAECState,

			@EditAll			AS AllowEditAll,
			@DocStatus			AS DOC_STATUS,			
			@OrgChanged			AS IsOrganizeChange
	
END
GO
