DROP PROCEDURE [dbo].[sp_WFD02640_NoticeEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- exec [sp_WFD02640_NoticeEmail] '2017','01','I','1423','0004'
-- Description:	Send mail notify ==> Modify by Uten : 2017-03-08
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02640_NoticeEmail]
(	@COMPANY	T_COMPANY = NULL,
	@YEAR		VARCHAR(4)= NULL,
	@ROUND		VARCHAR(2)= NULL,
	@ASSET_LOCATION VARCHAR(1)= NULL,
	@SV_CODE	T_SYS_USER = NULL,
	@EMP_CODE	T_SYS_USER
	)	
AS
BEGIN TRY
DECLARE @ASSET_LOCATION_IN_HOUSE VARCHAR(1)
DECLARE @ASSET_LOCATION_OUT_SOURCE VARCHAR(1)

--- set asset location  in-house ,out source 
SET @ASSET_LOCATION_IN_HOUSE='I';
SET @ASSET_LOCATION_OUT_SOURCE='O';
--- convert param to upper case
SET @ASSET_LOCATION=UPPER(RTRIM(LTRIM(@ASSET_LOCATION)));

/*--------------------------------- Email notice -------------------------------------------*/

	/*--DECLARE @URL_Like VARCHAR(500);
	--SET @URL_Like=N'http://192.168.104.101/WFD02630';
	*/

    DECLARE @to VARCHAR(MAX) 
	DECLARE @cc VARCHAR(MAX) 
	DECLARE @bcc VARCHAR(MAX) 
	DECLARE @subject VARCHAR(MAX)
    DECLARE @body NVARCHAR(MAX)
	DECLARE @textcontentBody NVARCHAR(MAX)
    SET @cc = dbo.fn_GetEmail_cc_ListofManager(@SV_CODE);
	SET @bcc = NULL;

		--- Set mail subject
	SELECT	@subject	=	VALUE
	FROM	TB_M_SYSTEM 
	WHERE	CATEGORY='SYSTEM_EMAIL' AND SUB_CATEGORY='SUBJECT' AND CODE='WFD02640' AND ACTIVE_FLAG='Y'

	SET @subject=dbo.fn_StringFormat(ISNULL(@subject,''),CONCAT(@YEAR,'/',@ROUND)) --- assign param value

		--- Set mail body
	SELECT	@body=VALUE
	FROM	TB_M_SYSTEM 
	WHERE	CATEGORY='SYSTEM_EMAIL' AND SUB_CATEGORY='BODY' AND CODE='WFD02640' AND ACTIVE_FLAG='Y'



IF(@ASSET_LOCATION=@ASSET_LOCATION_IN_HOUSE)
BEGIN --- send mail to sv
		 DECLARE	@MsgReturn VARCHAR(500)
		 SELECT		@to =EMAIL
		 FROM		TB_M_EMPLOYEE 
		 WHERE		SYS_EMP_CODE	=	@SV_CODE 

		IF(ISNULL(@to,'')='')-- Check Ex. Cannot find Email of employee code = for sending notification
		BEGIN
			SET		@MsgReturn = dbo.fn_GetMessage('MCOM0006BWRN')
			SET		@MsgReturn=dbo.fn_StringFormat(@MsgReturn , CONCAT('Email of employee code =',@SV_CODE, ' for sending notification' ))
			SET		@MsgReturn = CONCAT('[CUSTOM]MCOM0006BWRN|',@MsgReturn);
			RAISERROR (@MsgReturn, -- Message text.
						16, -- Severity.
						1 -- State.
					);
			RETURN;
		END

      set @textcontentBody = dbo.fn_StringFormat(@body,CONCAT(dbo.fn_GetShortENName(@SV_CODE),'|',
															  CONCAT(@YEAR,'/',@ROUND)))
END



IF(@ASSET_LOCATION=@ASSET_LOCATION_OUT_SOURCE) 
BEGIN
 -- Send mail to all FA user -->Dear sir,  
 	SELECT		@to = CONCAT(@to+';','') + ltrim(rtrim(EMAIL))
	FROM		TB_M_EMPLOYEE 
	WHERE		FAADMIN='Y' and (ISNULL(EMAIL,'')<>'')
	ORDER		BY EMP_CODE ASC;
	
	set @textcontentBody = dbo.fn_StringFormat(@body,CONCAT('sir','|', CONCAT(@YEAR,'/',@ROUND,' Company ',@COMPANY)))
END

IF((@ASSET_LOCATION=@ASSET_LOCATION_OUT_SOURCE) OR (@ASSET_LOCATION=@ASSET_LOCATION_IN_HOUSE))
BEGIN
    -- insert notify email table 
	   	  INSERT INTO [TB_R_NOTIFICATION]
           ([ID]   ,[TO]    ,[CC]    ,[BCC]  ,[TITLE] ,[MESSAGE] ,[FOOTER]    ,[TYPE]
           ,[START_PERIOD]  ,[END_PERIOD]    ,[SEND_FLAG],[RESULT_FLAG]
           ,[REF_FUNC]   ,[REF_DOC_NO] ,[CREATE_DATE]
           ,[CREATE_BY] ,[UPDATE_DATE] ,[UPDATE_BY])
         VALUES
           (NEXT VALUE FOR NOTIFICATION_ID ,@to ,@cc    ,@bcc ,@subject    , @textcontentBody ,NULL   ,'I' -- (Immediately) 
			 ,NULL,NULL  ,'N'  ,NULL ,'WFD02640'  ,NULL    ,GETDATE()  ,@EMP_CODE ,GETDATE()  ,@EMP_CODE)
END
  RETURN;
END TRY
BEGIN CATCH
		
        DECLARE @Msg VARCHAR(MAX)
		DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE();
        SELECT @ErrorSeverity = ERROR_SEVERITY();
        SELECT @ErrorState = ERROR_STATE();

            SET	@Msg = CONCAT('[CUSTOM]MSTD1008AERR|' ,dbo.fn_StringFormat(dbo.fn_GetMessage('MSTD1008AERR'),@ErrorMessage));

        RAISERROR (@Msg, -- Message text.
                   @ErrorSeverity, -- Severity.
                   @ErrorState -- State.
                   );
		RETURN;
END CATCH
GO
