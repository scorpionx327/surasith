DROP PROCEDURE [dbo].[sp_WFD01270_UpdateFileCommentHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec sp_WFD01270_GetCommentHistory 'C2017/0033'
CREATE PROCEDURE [dbo].[sp_WFD01270_UpdateFileCommentHistory]
(
	@COMMENT_HISTORY_ID			NUMERIC(18,0),
	@DOC_NO						VARCHAR(10),
	@INDX						INT,
	@ATTACH_FILE				VARCHAR(200)
)
	
AS
BEGIN
	UPDATE TB_R_REQUEST_COMMENT
	SET ATTACH_FILE = @ATTACH_FILE 
	WHERE COMMENT_HISTORY_ID	= @COMMENT_HISTORY_ID 
	AND DOC_NO					= @DOC_NO
	AND INDX					= @INDX
END





GO
