DROP PROCEDURE [dbo].[sp_WFD01210_UpdateEmployeeFAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun yuanyong
-- Create date: 08/03/2017
-- Description:	Update Employee source Fas
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD01210_UpdateEmployeeFAS]
	@SYS_EMP_CODE		T_SYS_USER
	,@EMP_CODE			T_USER
	,@EMP_TITLE			varchar(8)
	,@EMP_NAME			varchar(30)
	,@EMP_LASTNAME		varchar(30)
	,@ORG_CODE			T_ORG_CODE
	,@COST_CODE			T_COST_CODE
	,@POST_CODE			varchar(4)
	,@POST_NAME			varchar(50)
	,@EMAIL				varchar(50)
	,@TEL_NO			varchar(20)
	,@SIGNATURE_PATH	varchar(100)
	,@ROLE				varchar(3)
	,@SPEC_DIV			varchar(100)
	,@FAADMIN			varchar(1)
	,@DELEGATE_PIC		varchar(8)
	,@DELEGATE_FROM		varchar(30)
	,@DELEGATE_TO		varchar(30)
	,@USER_LOGIN        varchar(8)
	,@JOB varchar(100) = null
	
	,@COMPANY varchar(100)
	,@SP_ROLE_COMPANY	T_COMPANY
	,@AEC_USER			varchar(1) = null
	,@FA_Window			char(1) = null
	,@AEC_MANAGER 		varchar(1) = null
	
	,@AEC_GENERAL_MANAGER 		varchar(1) = null
	,@AEC_USER_LIST 		varchar(MAX) = null
	,@FASupervisor				varchar(1) = null
	,@FAWindow					varchar(1) = null
AS
BEGIN

	----------------------------------------------------------------------------------------------------------
	-- Add New By Surasith
	DELETE
	FROM	TB_T_EMPLOYEE_CHANGE WHERE SYS_EMP_CODE = @SYS_EMP_CODE

	INSERT
	INTO	TB_T_EMPLOYEE_CHANGE
	(		SYS_EMP_CODE,EMP_CODE, EMP_TITLE, EMP_NAME, EMP_LASTNAME, ORG_CODE, ACTIVEFLAG, 
			ORG_CHANGED, INFO_CHANGED )
	SELECT	@SYS_EMP_CODE,@EMP_CODE, @EMP_TITLE, @EMP_NAME, @EMP_LASTNAME, @ORG_CODE, ACTIVEFLAG, 
			CASE WHEN @ORG_CODE != ORG_CODE THEN 'Y' ELSE 'N' END,
			CASE WHEN @EMP_TITLE != EMP_TITLE OR @EMP_NAME != EMP_NAME OR @EMP_LASTNAME != EMP_LASTNAME THEN 'Y' ELSE 'N' END 
	FROM	TB_M_EMPLOYEE
	WHERE	SYS_EMP_CODE = @SYS_EMP_CODE AND
			( @ORG_CODE != ORG_CODE OR @EMP_TITLE != EMP_TITLE OR @EMP_NAME != EMP_NAME OR @EMP_LASTNAME != EMP_LASTNAME )

	---------------------------------------------------------------------------------------------------------

	DECLARE @ActiveFlag VARCHAR(1), @OldOrgCode VARCHAR(21)
	SELECT	@OldOrgCode = ORG_CODE, 
			@ActiveFlag = ACTIVEFLAG
	FROM	TB_M_EMPLOYEE 
	WHERE	SYS_EMP_CODE	= @SYS_EMP_CODE

	DECLARE @JOB_CODE VARCHAR(4) , @JOB_NAME VARCHAR(50)
	SELECT @JOB_CODE=M.CODE, @JOB_NAME=VALUE FROM TB_M_SYSTEM M (NOLOCK)
	WHERE CATEGORY = 'USER_PROFILE' 
	AND SUB_CATEGORY = 'JOB' 
	AND CODE=@JOB
	AND ACTIVE_FLAG = 'Y'

UPDATE [dbo].[TB_M_EMPLOYEE]
   SET
 
       [EMP_TITLE] = @EMP_TITLE
      ,[EMP_NAME] = @EMP_NAME
      ,[EMP_LASTNAME] = @EMP_LASTNAME
      ,[ORG_CODE] = @ORG_CODE
      ,[COST_CODE] = @COST_CODE
      ,[COST_NAME] = (select top 1 COST_NAME from TB_M_COST_CENTER where  COST_CODE = @COST_CODE)
      ,[POST_CODE] = @POST_CODE
      ,[POST_NAME] = @POST_NAME
      ,[EMAIL] = @EMAIL
      ,[TEL_NO] = @TEL_NO
      ,[SIGNATURE_PATH] = (CASE WHEN (@SIGNATURE_PATH IS NULL OR @SIGNATURE_PATH = '') THEN NULL WHEN @SIGNATURE_PATH != '#' THEN @SIGNATURE_PATH ELSE (SELECT TOP 1 E.SIGNATURE_PATH FROM TB_M_EMPLOYEE E WHERE E.EMP_CODE = @EMP_CODE) END) -- '':Clear; '#':Old; '@Signature_Path:New
      ,[ROLE] = @ROLE
      ,[SPEC_DIV] = @SPEC_DIV
      ,[FAADMIN] = @FAADMIN
      ,[DELEGATE_PIC] = @DELEGATE_PIC
      ,[DELEGATE_FROM] = dbo.fn_ConvertStringToDate(@DELEGATE_FROM)
      ,[DELEGATE_TO] =dbo.fn_ConvertStringToDate(@DELEGATE_TO)
      ,[UPDATE_DATE] = GETDATE()
      ,[UPDATE_BY] = @USER_LOGIN
	  ,COMPANY=@COMPANY
	  ,JOB_CODE=@JOB_CODE
	  ,JOB_NAME=@JOB_NAME
 WHERE 
	SYS_EMP_CODE = @SYS_EMP_CODE


	-- Update Manager
	UPDATE	TB_M_EMPLOYEE -- clear current
	SET		AEC_MGR_EMP_ID = NULL
	WHERE	AEC_MGR_EMP_ID = @SYS_EMP_CODE

	UPDATE	M -- asssign new 
	SET		M.AEC_MGR_EMP_ID = @SYS_EMP_CODE
	FROM	TB_M_EMPLOYEE M
	INNER	JOIN
			dbo.fn_GetMultipleList(@AEC_USER_LIST) T
	ON		M.SYS_EMP_CODE	= T.value
	


	EXEC sp_WFD01210_UpdateRequestWhenEmployeeInfoChanged @USER_LOGIN, @OldOrgCode, @ORG_CODE, @ROLE, @ActiveFlag, @EMP_CODE
	EXEC sp_WFD01270_UpdateRequestwhenEmployeeInfoChanged
	

	create table #Temp
	(
		 SP_ROLE_CODE varchar(3), 
		 SP_ROLE varchar(3)
	)

	--AEC User (List by company) If AEC User - Checked	

	
	if(@FAWindow='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='01',SP_ROLE='FW' 
	end

	if(@FASupervisor='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='02',SP_ROLE='FS' 
	end


	if(@AEC_USER='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='03',SP_ROLE='ACR'
	end

	if(@AEC_MANAGER='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='04',SP_ROLE='ACM' 
	end

	if(@AEC_GENERAL_MANAGER='Y')
	begin
	Insert Into #Temp
	select SP_ROLE_CODE='05',SP_ROLE='AGM' 
	end

	

	-- Insert SPEC_ROLE
	Insert Into #Temp
	SELECT V.[VALUE] ,M.[VALUE]
	FROM FN_SPLIT(@SPEC_DIV,',') V
	INNER JOIN 
	(SELECT CODE,[VALUE] FROM TB_M_SYSTEM WHERE  CATEGORY = 'RESPONSIBILITY' AND SUB_CATEGORY = 'SPEC_DIV') M
	ON V.[VALUE] = CODE
	
	
	PRINT	'XXXX'
	DELETE	TB_M_SP_ROLE WHERE EMP_CODE= @SYS_EMP_CODE

	INSERT	INTO TB_M_SP_ROLE (EMP_CODE, SP_ROLE_COMPANY,SP_ROLE_CODE,SP_ROLE, CREATE_DATE, CREATE_BY,UPDATE_BY)
	SELECT	@SYS_EMP_CODE,@COMPANY,
			t.SP_ROLE_CODE,t.SP_ROLE,GETDATE(),@USER_LOGIN,@USER_LOGIN 
	FROM	#TEMP T

	SELECT	@SYS_EMP_CODE,@COMPANY,
			t.SP_ROLE_CODE,t.SP_ROLE,GETDATE(),@USER_LOGIN,@USER_LOGIN 
	FROM	#TEMP T
	
	DROP	TABLE #TEMP
	
	--Update SV Cost Center
	IF EXISTS (SELECT * FROM TB_T_SV_COST_CENTER WHERE EMP_CODE = @SYS_EMP_CODE) 
	BEGIN
		DELETE	TB_M_SV_COST_CENTER
		WHERE	(COST_CODE	IN
					(SELECT	COST_CODE 
					 FROM	TB_T_SV_COST_CENTER 
					 WHERE	EMP_CODE	= @SYS_EMP_CODE))	OR EMP_CODE	= @SYS_EMP_CODE

		INSERT INTO TB_M_SV_COST_CENTER
			([COMPANY],			[COST_CODE],			[EMP_CODE],			[CREATE_DATE],
			[CREATE_BY],		[UPDATE_DATE],			[UPDATE_BY])
		SELECT
			@COMPANY,			tCos.COST_CODE,			@SYS_EMP_CODE,			CREATE_DATE,
			@USER_LOGIN,		CREATE_DATE,			@USER_LOGIN
		FROM	TB_T_SV_COST_CENTER tCos
		WHERE	tCos.EMP_CODE	= @SYS_EMP_CODE

		DELETE	TB_T_SV_COST_CENTER
		WHERE	EMP_CODE	= @SYS_EMP_CODE
	END

	--Update Responsible Cost Center
	IF EXISTS (SELECT * FROM TB_T_FASUP_COST_CENTER WHERE EMP_CODE = @SYS_EMP_CODE) 
	BEGIN
		DELETE	TB_M_FASUP_COST_CENTER
		WHERE	EMP_CODE	= @SYS_EMP_CODE

		INSERT INTO TB_M_FASUP_COST_CENTER
			([COMPANY],			[COST_CODE],			[EMP_CODE],			[CREATE_DATE],
			[CREATE_BY],		[UPDATE_DATE],			[UPDATE_BY])
		SELECT
			@COMPANY,			tSupCos.RES_COST_CODE,	@SYS_EMP_CODE,			CREATE_DATE,
			@USER_LOGIN,		CREATE_DATE,				@USER_LOGIN
		FROM	TB_T_FASUP_COST_CENTER tSupCos
		WHERE	tSupCos.EMP_CODE	= @SYS_EMP_CODE

		DELETE	TB_T_FASUP_COST_CENTER
		WHERE	EMP_CODE	= @SYS_EMP_CODE
	END


	EXEC sp_Common_GenerateCostCenterMapping @EMP_CODE
	EXEC sp_Common_GenerateCostCenterRole @EMP_CODE

END
GO
