DROP PROCEDURE [dbo].[sp_LFD02812_Export_Asset_Info_Change_Request]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LFD02812_Export_Asset_Info_Change_Request]
(
	@DOC_NO T_DOC_NO
)
AS
BEGIN
	select RD.ASSET_NO
		,RD.ASSET_SUB
		,RD.ASSET_DESC							
		,RD.ADTL_ASSET_DESC							
		,RD.SERIAL_NO						
		,RD.INVEN_INDICATOR							
		,RD.INVEN_NOTE						
		,RD.INVEST_REASON				
		,RD.MINOR_CATEGORY													
		,RD.ASSET_STATUS													
		,RD.ASSET_TYPE_NAME														
		,RD.BOI_NO																								
	from TB_R_REQUEST_ASSET_D RD
		WHERE (RD.DOC_NO = @DOC_NO OR @DOC_NO IS NULL OR LEN(@DOC_NO) = 0)
		ORDER BY RD.LINE_NO
END
GO
