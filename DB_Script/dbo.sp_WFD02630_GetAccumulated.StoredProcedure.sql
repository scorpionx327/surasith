DROP PROCEDURE [dbo].[sp_WFD02630_GetAccumulated]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sarun Yuanyong
-- Create date: 22/02/2017
-- Description:	GetAccumulated
-- =============================================
CREATE PROCEDURE [dbo].[sp_WFD02630_GetAccumulated]
@year varchar(4),
@round varchar(2),
@asset_location varchar(1),
@emp_code T_USER,
@date datetime
AS
BEGIN
	declare @FAADMIN NVARCHAR(1);
SELECT @FAADMIN = FAADMIN 
FROM TB_M_EMPLOYEE WHERE EMP_CODE = @emp_code AND ACTIVEFLAG = 'Y'


declare @PERTOTAL       INT;
declare @TOTALS			INT;
declare @PERSENT		INT;
declare @CLASS			varchar(150);
select @PERTOTAL = COUNT(d.ASSET_NO) from TB_R_STOCK_TAKE_D	 d	
left join TB_R_STOCK_TAKE_D_PER_SV	 sv	on sv.STOCK_TAKE_KEY = d.STOCK_TAKE_KEY													
where d.YEAR = @year and d.[ROUND] = @round
and d.ASSET_LOCATION=@asset_location and d.[CHECK_STATUS]='Y'
and sv.DATE_TO <= @date												
and 
( (@FAADMIN = 'Y') OR 
( D.SV_EMP_CODE = @emp_code)
);	

select @TOTALS =COUNT(d.ASSET_NO)  from TB_R_STOCK_TAKE_D_PER_SV sv											
left join TB_R_STOCK_TAKE_D	 d	on d.STOCK_TAKE_KEY = sv.STOCK_TAKE_KEY														
where d.YEAR = @year and d.[ROUND] = @round
and d.ASSET_LOCATION=@asset_location 
and sv.DATE_TO <= @date												
 and 
  ( (@FAADMIN = 'Y') OR 
    ( D.SV_EMP_CODE = @emp_code)
  );														

IF 	@TOTALS > 0
BEGIN
	set	@PERSENT =(@PERTOTAL/@TOTALS)*100
END
											
SELECT	PERTOTAL
		,TOTALS
		,PERSENT
	    ,CLASS
	  FROM (SELECT  ISNULL(@PERTOTAL,0)    AS   PERTOTAL   , 
	         ISNULL(@TOTALS,0)  AS TOTALS,
			 ISNULL(@PERSENT,0)  AS PERSENT,
			 ISNULL(@CLASS,'') as CLASS
			 )ACC 														
														
														

END



GO
