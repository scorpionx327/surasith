﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Microsoft.Win32;
using System.Net;


namespace th.co.toyota.stm.fas.common
{
    public class MailSending
    {
        public MailSending()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string AppID = string.Empty;

        //private void ValidateEmailAddress(string txt)
        //{
        //    if (txt.Length == 0)
        //        throw new Exception("Email address is a required field");
        //    else
        //    {
        //        if (txt.IndexOf(".") == -1 || txt.IndexOf("@") == -1)
        //        {
        //            throw new Exception("E-mail address must be valid e-mail");
        //        }
        //    }
        //}

        private bool ValidateEmailAddress(string txt)
        {
            if (txt.Length == 0)
                return false;
            else
            {
                if (txt.IndexOf(".") == -1 || txt.IndexOf("@") == -1)
                {
                    return false;
                }
            }
            return true;
        }
        public void Send()
        {
            Log4NetFunction _log4net = new Log4NetFunction();
            //ValidateEmailAddress(this.mai);
            if (!ValidateEmailAddress(this.MailTo)){
                throw new Exception(string.Format("Cannot find {0}", "Email for sending notification"));
            }
            MailMessage mailMsg = new MailMessage();
            SmtpClient SmtpMail = new SmtpClient(this.MailSmtpServer);

            if ((MailSmtpPort != 0))
            {
                SmtpMail.Port = this.MailSmtpPort;
            }


            if (!string.IsNullOrEmpty(MailSmtpUser) && !string.IsNullOrEmpty(MailSmtpPassword))
            {

                SmtpMail.Credentials = new System.Net.NetworkCredential(MailSmtpUser, MailSmtpPassword);
                SmtpMail.EnableSsl = false;
            }

            if (string.IsNullOrEmpty(this.DisplayName))
            {
                mailMsg.From = new MailAddress(this.MailFrom);
            }else
            {
                mailMsg.From = new MailAddress(this.MailFrom, this.DisplayName);
            }

            string[] _ss = this.MailTo.Split(';');
            foreach (string _s in _ss)
            {
                if (ValidateEmailAddress(_s))
                {
                    mailMsg.To.Add(_s);
                }
            }
            if (mailMsg.To.Count == 0)
            {
                throw (new Exception(string.Format("Invalid Email To {0}", this.MailTo)));
            }
                

            //mailMsg.To.Add(this.MailTo.Replace(";", ","));

            if (!string.IsNullOrEmpty(this.Mailcc))
            {

                mailMsg.CC.Add(this.Mailcc.Replace(";", ","));
            }



            if (!string.IsNullOrEmpty(this.MailBcc))
            {
                mailMsg.Bcc.Add(this.MailBcc.Replace(";", ","));
            }


            mailMsg.Subject = this.MailSubject;
            
            mailMsg.Body = WebUtility.HtmlDecode(this.MailBody);
            mailMsg.Priority = this.MailPriority ;
            mailMsg.IsBodyHtml = true;
            //_log4net.WriteInfoLogFile(WebUtility.HtmlDecode(this.MailBody));

            try
            {
                SmtpMail.Send(mailMsg);
                SmtpMail.Dispose();
            }
            catch (Exception e)
            {
                _log4net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + e.Message);
                throw new Exception(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + e.Message);
            }
        }

        
        public void SendEmailToAdministratorInSystemConfig(string _BatchName,params string[] _htmlBodyFormat)
        {

            //Read all from configuration
            Log4NetFunction _log4net = new Log4NetFunction();
            if (_ErrorMessageList.Count == 0 && string.IsNullOrEmpty(this.AppID))
                return;

            var _db = new DAO();


            this.MailSmtpUser = _db.GetEmailConfiguration("SMTP_USER");
            this.MailSmtpPassword = _db.GetEmailConfiguration("SMTP_PASSWORD");

            this.MailFrom = _db.GetEmailConfiguration("MAIL_FROM");
            this.DisplayName = _db.GetEmailConfiguration("MAIL_FROM_DISPLY_ERROR");

            this.MailTo = _db.GetEmailConfiguration("MAIL_TO");
            this.Mailcc = _db.GetEmailConfiguration("MAIL_CC");
            
            this.MailSmtpServer = _db.GetEmailConfiguration("SMTP_SERVER");
            this.MailSmtpPort = Convert.ToInt32(_db.GetEmailConfiguration("SMTP_PORT"));
            
            string _bodyFormat = _db.GetEmailConfiguration("BODY_FORMAT");
            if(_htmlBodyFormat.Length > 0)
            {
                _bodyFormat = _htmlBodyFormat[0];
            }


            string _titleFormat = _db.GetEmailConfiguration("TITLE_FORMAT");

            var _sendFlag = _db.GetEmailConfiguration("EMAIL_IS_SEND");
            

            this.MailSubject = string.Format(_titleFormat, _BatchName) ;
            
            foreach(string _s in _ErrorMessageList)
            {
                this.MailBody = this.MailBody + _s + "<BR>";
            }

            this.MailBody = string.Format(_bodyFormat, this.MailBody,this.AppID);

            _log4net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : MailFrom = " + this.MailFrom);
            _log4net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : MailTo = " + this.MailTo);
            _log4net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : MailCC = " + this.Mailcc);
            _log4net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : _sendFlag = " + _sendFlag.ToString());
            _log4net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : MailBody = " + this.MailBody);

            if (_sendFlag != "Y")
                return;

            this.Send();

        }

        public void AddErrorMessage(string _ErrorMessage)
        {
            _ErrorMessageList.Add(_ErrorMessage);
        }
        List<string> _ErrorMessageList = new List<string>();
        public string MailFrom {get;set;}
       

        public string MailTo {get;set;}
        

        public string Mailcc {get;set;}
       

        public string MailBcc {get;set;}
       

        public string MailSubject {get;set;}
       

        public string MailBody {get;set;}
       

        public MailPriority MailPriority {get;set;}
        

        public string MailSmtpServer {get;set;}
        
        public string MailSmtpUser {get;set;}
        
        public string MailSmtpPassword {get;set;}
      
        public int MailSmtpPort {get;set;}
       
        public string DisplayName { get; set; }
    }
}
