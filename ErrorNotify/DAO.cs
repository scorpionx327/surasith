﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace th.co.toyota.stm.fas.common
{
    class DAO
    {
        private string ConnectionString = string.Empty;
        private DataConnection _db;
        public DAO()
        {
            var _cfg = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"];
            if (_cfg == null || string.IsNullOrEmpty(_cfg.ConnectionString))
            {
                ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            }else
            {
                ConnectionString = _cfg.ConnectionString;
            }
            _db = new DataConnection(ConnectionString);

            if (!_db.TestConnection())
            {
                throw (new Exception("System cannot send email to administrator because system cannot connect database"));

            }

        }
        public string GetEmailConfiguration(string _code)
        {
          
            var _cmd = new System.Data.SqlClient.SqlCommand("sp_Common_GetSystemValues");
            _cmd.CommandType = System.Data.CommandType.StoredProcedure;
            _cmd.Parameters.AddWithValue("@CATEGORY", "SYSTEM_EMAIL");
            _cmd.Parameters.AddWithValue("@SUB_CATEGORY", "EMAILTO_PIC");
            _cmd.Parameters.AddWithValue("@CODE", _code);

            var _dt = _db.GetData(_cmd);
            if (_dt.Rows.Count == 0)
                return string.Empty;
            return string.Format("{0}",_dt.Rows[0]["VALUE"]);


        }

    }
}
