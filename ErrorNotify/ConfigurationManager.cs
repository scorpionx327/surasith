﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Hino.ErrorNotify
{
    public class ConfigurationManagerSimple
    {
        private XmlDocument _doc = new XmlDocument();
        private string ConfigurationFileName = string.Empty;

        public ConfigurationManagerSimple(string _configFileName)
        {
            this.ConfigurationFileName = _configFileName;
            _doc.Load(this.ConfigurationFileName);

        }
        private Hashtable GetChildNodes(string _nodeName)
        {
            XmlNode _node = _doc.SelectSingleNode(_nodeName);
            XmlNodeList _childs = _node.ChildNodes;

            Hashtable _ht = new Hashtable();
            foreach (XmlNode _nd in _childs)
            {
                if (!_ht.ContainsKey(_nd.Name))
                    _ht.Add(_nd.Name, _nd.InnerText);
            }

            return _ht;

        }
        public string GetValueFromHashtable(string _nodeName, string _key)
        {
            Hashtable _ht = this.GetChildNodes(_nodeName);
            if (!_ht.ContainsKey(_key))
            {
                return string.Empty;
            }
            return string.Format("{0}", _ht[_key]);
        }

    }
}
