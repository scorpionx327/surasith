﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;


namespace Hino.ErrorNotify
{
    public class MailFunction
    {
        private SendMail mail = new SendMail();
        public MailFunction()
        {
            //
            // TODO: Add constructor logic here
            //
            //
            // TODO: Add constructor logic here
            //

        }
        public string AppID = string.Empty;
        public void Send(string _configFileName)
        {
            // Get Mail Config 
            if (string.IsNullOrEmpty(this.AppID))
            {
                return;
            }

            string _node = @"configuration/Email";
            ConfigurationManagerSimple _cfg = new ConfigurationManagerSimple(_configFileName);

            if (string.IsNullOrEmpty(_cfg.GetValueFromHashtable(_node, "EmailIsSend")))
            {
                return;
            }
            if (!_cfg.GetValueFromHashtable(_node, "EmailIsSend").Equals("Y"))
            {
                return;
            }
            try
            {
                //System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"
                string stmpPort = string.Empty;
                double num;

                mail.MailSmtpServer = _cfg.GetValueFromHashtable(_node, "SMTPServer");
                mail.MailSmtpUser = _cfg.GetValueFromHashtable(_node, "SMTPUser");
                mail.MailSmtpPassword = _cfg.GetValueFromHashtable(_node, "SMTPPassword");
                stmpPort = _cfg.GetValueFromHashtable(_node, "SMTPPort");

                if (stmpPort == string.Empty)
                {
                    mail.MailSmtpPort = 0;
                }
                else
                {
                    if (!Double.TryParse(stmpPort, out num))
                    {
                        mail.MailSmtpPort = 0;
                    }
                    else
                    {
                        mail.MailSmtpPort = Convert.ToInt16(stmpPort);
                    }

                }

                mail.MailFrom = _cfg.GetValueFromHashtable(_node, "MailFrom");
                mail.MailTo = _cfg.GetValueFromHashtable(_node, "MailTo");

                mail.Mailcc = _cfg.GetValueFromHashtable(_node, "MailCc");

                if (this.Subject.Equals(string.Empty))
                {
                    mail.MailSubject = _cfg.GetValueFromHashtable(_node, "MailSubject");
                }
                else
                {
                    mail.MailSubject = this.Subject;
                }


                mail.MailBody = _cfg.GetValueFromHashtable(_node, "MailMessage");

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            if (mail.MailFrom == string.Empty)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "MailFrom")));
            }

            if (mail.MailTo == string.Empty)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "MailTo")));
            }

            if (mail.MailSmtpServer == string.Empty)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "SMTPServer")));
            }


            //if (mail.MailSmtpUser == string.Empty)
            //{
            //    throw (new Exception(string.Format("Not found {0} in configuration", "SMTPUser")));
            //}

            //if (mail.MailSmtpPassword == string.Empty)
            //{
            //    throw (new Exception(string.Format("Not found {0} in configuration", "SMTPPassword")));
            //}

            if (mail.MailSmtpPort == 0)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "SMTPPort")));
            }

            if (mail.MailSubject == string.Empty)
            {
                mail.MailSubject = string.Format("Batch Result of {0:dd.MM.yyyy}", DateTime.Now);
            }

            try
            {
                mail.MailBody = string.Format(mail.MailBody, this.AppID);
                mail.Send();
            }
            catch (Exception ex)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(string.Empty.PadRight(50, '-'));
                sb.Append(Environment.NewLine);
                sb.Append("Send E-mail fail =>");
                sb.Append(Environment.NewLine);
                sb.Append(mail.MailBody);
                sb.Append(Environment.NewLine);
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append(string.Empty.PadRight(50, '-'));
                //write log
                Log4NetFunction _log = new Log4NetFunction();
                _log.WriteErrorLogFile(sb.ToString());
            }
        }
        public static bool IsNetWorkConnected(string _ip)
        {
            try
            {
                Ping _ping = new Ping();
                PingReply rs = _ping.Send(_ip, 1000);
                return (rs.Status == IPStatus.Success);
            }
            catch
            {
                return false;
            }
        }

        public void Send(string SMTPServer, string SMTPUser,
                        string SMTPPassword, string SMTPPort,
                         string MailFrom, string MailTo,
                        string MailCc, string MailSubject,
                        string MailMessage)
        {

            try
            {
                //System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"
                string stmpPort = string.Empty;
                double num;

                mail.MailSmtpServer = SMTPServer;
                mail.MailSmtpUser = SMTPUser;
                mail.MailSmtpPassword = SMTPPassword;
                stmpPort = SMTPPort;

                if (stmpPort == string.Empty)
                {
                    mail.MailSmtpPort = 0;
                }
                else
                {
                    if (!Double.TryParse(stmpPort, out num))
                    {
                        mail.MailSmtpPort = 0;
                    }
                    else
                    {
                        mail.MailSmtpPort = Convert.ToInt16(stmpPort);
                    }

                }

                mail.MailFrom = MailFrom;
                mail.MailTo = MailTo;
                mail.Mailcc = MailCc;

                if (this.Subject.Equals(string.Empty))
                {
                    mail.MailSubject = MailSubject;
                }
                else
                {
                    mail.MailSubject = this.Subject;
                }


                mail.MailBody = MailMessage;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            if (mail.MailFrom == string.Empty)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "MailFrom")));
            }

            if (mail.MailTo == string.Empty)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "MailTo")));
            }

            if (mail.MailSmtpServer == string.Empty)
            {
                throw (new Exception(string.Format("Not found {0} in configuration", "SMTPServer")));
            }


            //if (mail.MailSmtpUser == string.Empty)
            //{
            //    throw (new Exception(string.Format("Not found {0} in configuration", "SMTPUser")));
            //}

            //if (mail.MailSmtpPassword == string.Empty)
            //{
            //    throw (new Exception(string.Format("Not found {0} in configuration", "SMTPPassword")));
            //}

            //if (mail.MailSmtpPort == 0)
            //{
            //    throw (new Exception(string.Format("Not found {0} in configuration", "SMTPPort")));
            //}

            if (mail.MailSubject == string.Empty)
            {
                mail.MailSubject = string.Format("Batch Result of {0:dd.MM.yyyy}", DateTime.Now);
            }

            try
            {
                mail.MailBody = string.Format(mail.MailBody, this.AppID);

                if (IsNetWorkConnected(SMTPServer))
                {
                    mail.Send();
                }

            }
            catch (Exception ex)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(string.Empty.PadRight(50, '-'));
                sb.Append(Environment.NewLine);
                sb.Append("Send E-mail fail =>");
                sb.Append(Environment.NewLine);
                sb.Append(mail.MailBody);
                sb.Append(Environment.NewLine);
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append(string.Empty.PadRight(50, '-'));
                //write log
                Log4NetFunction _log = new Log4NetFunction();
                _log.WriteErrorLogFile(sb.ToString());
            }
        }

        //public void Send(System.Collections.ArrayList _arErrorList)
        //{
        //    Send(GetErrorList(_arErrorList));
        //}
        private string GetErrorList(System.Collections.ArrayList _arErrorList)
        {
            System.Text.StringBuilder _sb = new System.Text.StringBuilder();
            foreach (string _err in _arErrorList)
            {
                _sb.Append(_err);
                _sb.Append(Environment.NewLine);
            }
            return _sb.ToString();
        }

        public string Subject
        {
            set
            {
                mail.MailSubject = value;
            }
            get
            {
                return mail.MailSubject;
            }
        }

    }
}
