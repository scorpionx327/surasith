﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02440_SummaryFATransferList
{
    class LFD02440DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02440DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }

        public DataSet GetTransferRequestSummaryReport(DateTime START_PERIOD, DateTime END_PERIOD, string USER_ID, string COMPANY_CODE)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                SqlCommand cmd = new SqlCommand("sp_LFD02440_GetTransferRequestSummaryReport");
                cmd.Parameters.AddWithValue("@START_PERIOD", START_PERIOD);
                cmd.Parameters.AddWithValue("@END_PERIOD", END_PERIOD);
                cmd.Parameters.AddWithValue("@USER_ID", USER_ID);
                cmd.Parameters.AddWithValue("@COMPANY_CODE", COMPANY_CODE);

                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);
                //ds = TempGetTransferRequestSummaryReport(START_PERIOD, END_PERIOD, USER_ID);     

                ds.DataSetName = "ReportDataSource";
                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0)
                    {
                        table.TableName = "Content";
                    }
                    if (ii == 1)
                    {
                        table.TableName = "Summary";
                    }
                    ii++;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }

        public string GetLogoURL(string CATEGORY, string SUB_CATEGORY, string CODE)
        {
            string result = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return result;
                }

                SqlCommand cmd = new SqlCommand("sp_Common_GetSystemValues");
                cmd.Parameters.AddWithValue("@CATEGORY", CATEGORY);
                cmd.Parameters.AddWithValue("@SUB_CATEGORY", SUB_CATEGORY);
                cmd.Parameters.AddWithValue("@CODE", CODE);

                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = _db.GetData(cmd);

                result = (dt.Rows.Count > 0 ? dt.Rows[0]["VALUE"].ToString() : null);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return result;
        }
         
    }
}
