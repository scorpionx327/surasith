﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Threading;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02440_SummaryFATransferList
{
    class LFD02440BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02440DAO _dbconn = null;
        private DetailLogData _log = null;

        IFormatProvider _culture = new CultureInfo("en-US", true);
        #endregion

        public LFD02440BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
           
            try
            {
                //Thanyarat Edit 02122019
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Summary FA transfer list by period report";
                }

                string[] _p = this.BLoggingData.Arguments.Split('|');
                if (_p.Length != 4)
                {
                    throw (new Exception("Invalid Parameter (Should be 4)"));
                }

                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02440DAO();
                BLoggingData.BatchName = _batchName;
                BLoggingData.ProcessStatus = eLogLevel.Error;
                BLogging.StartBatchQ(BLoggingData);

                BLoggingData.ProcessStatus = this.BusinessLogic();
                
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(_batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private eLogLevel BusinessLogic()
        {
            try
            {

                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');
                _dbconn = new LFD02440DAO();
                DataSet ds = new DataSet();
                string COMPANY_CODE = _p[0];
                DateTime START_PERIOD = DateTime.ParseExact(_p[1], "yyyyMMdd", _culture);
                DateTime END_PERIOD = DateTime.ParseExact(_p[2], "yyyyMMdd", _culture);
                string USER_ID = _p[3];

             //XXXXXXX
                string[] _companyList = COMPANY_CODE.Split('#');
                int _cnt = 0;
                List<string> _fileNameList = new List<string>();
                foreach (string _com in _companyList)
                {
                    ds = _dbconn.GetTransferRequestSummaryReport(START_PERIOD, END_PERIOD, USER_ID, _com);
                    _cnt += CountDataAllTable(ds);
                    if (CountDataAllTable(ds) > 0)
                    {
                        _log4Net.WriteDebugLogFile("Generate Report");
                        _log4Net.WriteDebugLogFile(_com);
                        var _rs = GenerateReport(_com, ds, START_PERIOD, END_PERIOD);  //String _company, DataSet ds, DateTime Start_Period, DateTime End_Period
                        _fileNameList.Add(_rs.Replace(".pdf",".xlsx"));
                        _fileNameList.Add(_rs);
                    }
                }               
                if (_cnt == 0)
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
                   if(_fileNameList.Count == 1)
                {
                    //Add download
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID,
                       _fileNameList[0]); 
                }
                //Create Zip
                _log4Net.WriteDebugLogFile("Create Zip File"); 
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  
                string _zip = string.Format(System.Configuration.ConfigurationManager.AppSettings["ZipFile"], DateTime.Now);
                
                _zip = System.IO.Path.Combine(oDirectoryName, _zip);
                                this.CreateZipFile(_fileNameList, _zip);
                //Thread.Sleep(1800);
                //this.DeleteJunkFiles(_fileNameList, _zip);
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID,_zip);
                return eLogLevel.Information;
            }
            //XXXXXXX
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if ((ds != null) && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                { CountRow += dt.Rows.Count; }
            }
            return CountRow;
        } 

        private string GenerateReport(string _company, DataSet ds, DateTime Start_Period, DateTime End_Period)
        {
            try
            {
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(cfgLayoutSection))
                {
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                }

                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                //Get PDF file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, _company, DateTime.Now);
                oFileName = oFileName.Replace(".pdf", string.Empty);

                Console.WriteLine("Genereate File");
               

                string _fName = System.IO.Path.Combine(oDirectoryName, oFileName + ".pdf");

                //Gernerate PDF File
                GenerateFile(ds, Start_Period, End_Period,
                    oDirectoryName,
                    oFileName,
                    cfgLayoutSection);

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".pdf");
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".pdf");
                return _fName;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }

        }

        XtraReport GetXtraReportObject(string LayoutSection, DataSet dataSource, DateTime startPeriod, DateTime endPeriod)
        {
            var xtraReport = XtraReport.FromFile(LayoutSection, true);

            string Param1 = startPeriod.ToString("dd-MMM-yyyy");
            string Param2 = endPeriod.ToString("dd-MMM-yyyy");

            xtraReport.DataSource = dataSource;

            xtraReport.Parameters["Start_Period"].Value = Param1;
            xtraReport.Parameters["Start_Period"].Visible = true;

            xtraReport.Parameters["End_Period"].Value = Param2;
            xtraReport.Parameters["End_Period"].Visible = true;

            LoadImageLogoCompany(xtraReport);

            var subDataSource = dataSource.Copy();
            SetXtraSubReportObject(xtraReport, subDataSource, Param1, Param2);

            return xtraReport;
        }

        void SetXtraSubReportObject(XtraReport MainReport, DataSet dataSource, string startPeriod, string endPeriod)
        {
            //Get sub report lay out section name
            string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["SubReportLayoutSection"]; //follow your config

            var subReport = (XRSubreport)MainReport.FindControl("SUMMARY_ASSET_TRANSFER", true);

            var ReferenceReport = XtraReport.FromFile(cfgLayoutSection, true);

            ReferenceReport.DataSource = dataSource;
            ReferenceReport.Parameters["Start_Period"].Value = startPeriod;
            ReferenceReport.Parameters["Start_Period"].Visible = true;

            ReferenceReport.Parameters["End_Period"].Value = endPeriod;
            ReferenceReport.Parameters["End_Period"].Visible = true;

            LoadImageLogoCompany(ReferenceReport);

            subReport.ReportSource = ReferenceReport;
        }

        public void LoadImageLogoCompany(XtraReport MainReport)
        {
            string url = _dbconn.GetLogoURL("PRINT_TAG", "LOGO", "LFD02440");

            var ImageLogo = (XRPictureBox)MainReport.FindControl("Logo", true); 
            if (ImageLogo != null)
            {
                // Set its image.
                ImageLogo.ImageUrl = url;
            }
        }

        byte[] Export2XLSx(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".xlsx";

            // Get its XLSx export options.
            XlsxExportOptions xlsxOptions = xtraReport.ExportOptions.Xlsx;
            var tttt = xtraReport.Pages.Count;
            // Set XLSx-specific export options.
            xlsxOptions.ShowGridLines = true;
            xlsxOptions.TextExportMode = TextExportMode.Value;

            //// Add a script to a report's collection of scripts.
            //xtraReport.ScriptsSource += "private void xrLabel1_BeforePrint(object sender, " +
            //                        "System.Drawing.Printing.PrintEventArgs e) {\r\n  " +
            //                        "((XRLabel)sender).BackColor = Color.Aqua;\r\n}";

            //// Assign this script to a label's BeforePrint script.
            //xtraReport..xrLabel1.Scripts.OnBeforePrint = "xrLabel1_BeforePrint";

            // Export the report to XLSx.
            xtraReport.ExportToXlsx(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }

        byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            var tttt = xtraReport.Pages.Count;

            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }

        void CreateZipFile(string DirectoryName, string FileName, byte[] PdfBytes, byte[] XlsxBytes)
        {
            var zipPath = DirectoryName + @"\" + FileName + ".zip";

            using (var fileStream = new FileStream(zipPath, FileMode.CreateNew))
            {
                using (var archive = new ZipArchive(fileStream, ZipArchiveMode.Create, true))
                {
                    var zipArchiveEntryPdf = archive.CreateEntry(FileName + ".pdf", CompressionLevel.Fastest);
                    using (var zipStream = zipArchiveEntryPdf.Open())
                    {
                        zipStream.Write(PdfBytes, 0, PdfBytes.Length);
                    }

                    var zipArchiveEntryXlsx = archive.CreateEntry(FileName + ".xlsx", CompressionLevel.Fastest);
                    using (var zipStream = zipArchiveEntryXlsx.Open())
                    {
                        zipStream.Write(XlsxBytes, 0, XlsxBytes.Length);
                    }
                }
            }
        }

        void GenerateFile(DataSet ds, DateTime Start_Period, DateTime End_Period, string DirectoryName, string FileName, string LayoutDirectory)
        {
            try
            {
                var xTraReport = GetXtraReportObject(LayoutDirectory, ds, Start_Period, End_Period);

                var xlsxBytes = Export2XLSx(xTraReport, DirectoryName, FileName);
                var pdfBytes = Export2PDF(xTraReport, DirectoryName, FileName);

                //CreateZipFile(DirectoryName, FileName, pdfBytes, xlsxBytes);
                //DeleteJunkFiles(DirectoryName, FileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteJunkFiles(List<string> _fileNameList, string _targetFileName)
         {
            using (FileStream zipFileToOpen = new FileStream(_targetFileName, FileMode.OpenOrCreate))
            {
                using (ZipArchive archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Create))
                {
                    foreach (string _fName in _fileNameList)
                    {
                    var item = new FileInfo(_fName);
                    item.Delete();
                    }
                }
            }
        }
           
        private void CreateZipFile(List<string> _fileNameList, string _targetFileName)
        {
            using (FileStream zipFileToOpen = new FileStream(_targetFileName, FileMode.OpenOrCreate))
            {
                using (ZipArchive archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Create))
                {
                    foreach (string _fName in _fileNameList)
                    {
                        archive.CreateEntryFromFile(_fName, new System.IO.FileInfo(_fName).Name);
                    }
                }
            }
        }
    }
}
