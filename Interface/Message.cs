﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BatchCommon.Database;
using BatchCommon.Config;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Hino.Common
{
    public class Message
    {
        private string _strConn = "";
        private DataConnection _db;

        public Message()
        {
            _strConn = ConfigurationSettings.AppSettings["ConnectionString"];
        }

        public string GetMessage(string messageCode, params object[] param)
        {
            string message = string.Empty;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return message = "Cannot connect the database.";
                }

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT dbo.fn_ReplaceMessage(@ERRORCODE)";
                cmd.Parameters.Add(new SqlParameter("@ERRORCODE", SqlDbType.Text)).Value = messageCode;
                cmd.CommandType = CommandType.Text;

                object obj = _db.Execute(cmd);
                message = obj.ToString();
                message = string.Format(message, param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return message;
        }
    }

    public class MSG_Common
    {
        public const string NotFound = "MSTD0083AINF"; //MSTD0083AINF : {0} is not found
        public const string BatchBegin = "MSTD7000BINF"; //MSTD7000BINF : {0} Begin
        public const string BatchEndError = "MSTD7002BINF";  //MSTD7002BINF :  {0} End with error {1}
        public const string BatchEndSuccessfully = "MSTD7001BINF"; //MSTD7001BINF : {0} End successfully
        public const string CannotReadFile = "MSTD7012BERR"; //MSTD7012BERR : Cannot read file : {0} in {1}
        public const string DataNotFoundProcessing = "MSTD7026BINF";//MSTD7026BINF : Data not found for processing from {0}
    }
    public class MSG_PL2B413
    {
        public const string MoreThan1Line = "MPL24501BERR"; //MPL24501BERR : There are more than 1 line in upload file. Only 1 line can be upload in one file.
        public const string ContainShift = "MPL24502BERR"; //MPL24502BERR : Upload file must contain both Shift A and B
        public const string NotExists = "MPL24503BERR"; //MPL24503BERR : {0} does not exist  in {1}
        public const string ProductionNotActive = "MPL24504BERR"; //MPL24504BERR : Production month is not active production month.

    }
}