﻿using System;
using System.Data ;
using System.IO ;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Collections.Generic;

namespace th.co.toyota.stm.fas.common.Interface
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class GenerateFileClass
    {
        //private Log4NetFunction _log = new Log4NetFunction() ;
        public GenerateFileClass(string _configFileName, string _fileLayoutSection)
        {
            //
            // TODO: Add constructor logic here
            //
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            this.configFileName = _configFileName;
            this.fileLayoutSection = string.Format("configuration/SendingFileCommonBatch/{0}", _fileLayoutSection);

            if (!System.IO.File.Exists(this.configFileName))
            {
                throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, _configFileName));
            }

            clsFileLayout = GetSendingFileLayout(_configFileName, _fileLayoutSection);

        }
        #region -- private attribute --
        private System.Collections.ArrayList arCustomExcel = new System.Collections.ArrayList();
        // เพิ่มเติม ///////////////////////////////////////////
        private string directoryName = null;
        private string fileName = null;
        DataTable dtFileContent = null;
        private System.Collections.ArrayList arFileContent = new System.Collections.ArrayList();
        private string footerText = null;
        private System.Collections.ArrayList arCustomExcelMerge = new System.Collections.ArrayList();
        private int _excelStartRow = 2;
        private string _filePrefix = null;
        private string _excelTemplateFileName = string.Empty;
        private SendingFileLayoutClass clsFileLayout;
        private string configFileName = null;
        private string fileLayoutSection = null;
        private string headerText = null;

        private System.Collections.ArrayList arCustomExcelFormat = new System.Collections.ArrayList();
        private struct DynamicColumn
        {
            public bool IsDyNamic;
            public int StartColumn;
            public int EndColumn;
        }

        private DynamicColumn dExcelDynamicColumn = new DynamicColumn();
        #endregion

        // -- private method --
        private void SetDataForFixLength(DataTable _dtsrc)
        {
            foreach (DataRow _dr in _dtsrc.Rows)
            {

                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                foreach (SendingFileColumn _col in clsFileLayout.ColumnList) //don't move column index
                {
                    if (_col.FieldName == "*")
                        continue; //Not support Dynamic Columns

                    object _obj = _dr[_col.FieldName];
                    SendingFileColumn _field = clsFileLayout.ColumnList.Find(x => x.FieldName == _col.FieldName);

                    string _s = string.Empty;
                    if (_dtsrc.Columns[_col.FieldName].DataType.Name.StartsWith("Int"))
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj).PadLeft(_field.Length, '0');
                        _s = _s.Substring(_s.Length - _field.Length);
                    }
                    else if (_dtsrc.Columns[_col.FieldName].DataType.Name == "DateTime")
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj).PadRight(_field.Length, ' ');
                        _s = _s.Substring(0, _field.Length);
                    }
                    else
                    {
                        _s = string.Format("{0}", _obj).PadRight(_field.Length, ' ');
                        _s = _s.Substring(0, _field.Length);
                    }
                    _sb.Append(_s);
                }
                arFileContent.Add(_sb.ToString());

            }

        }
        private void SetDataForSplit(DataTable _dtsrc)
        {
            foreach (DataRow _dr in _dtsrc.Rows)
            {
                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                foreach (SendingFileColumn _col in clsFileLayout.ColumnList)
                {
                    if (_col.FieldName == "*")
                        continue; //Not support Dynamic Columns

                    object _obj = _dr[_col.FieldName];
                    SendingFileColumn _field = clsFileLayout.ColumnList.Find(x => x.FieldName == _col.FieldName); //clsFileLayout.GetColumn(_col.FieldName);

                    string _s = string.Empty;
                    if (_dtsrc.Columns[_col.FieldName].DataType.Name.StartsWith("Int"))
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj);
                    }
                    else if (_dtsrc.Columns[_col.FieldName].DataType.Name == "DateTime")
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj);
                    }
                    else
                    {
                        _s = string.Format("{0}", _obj);
                    }
                    _sb.Append(string.Concat(_s, clsFileLayout.SplitChar));

                } // end for loop column
                if (_sb.Length > 1)
                   // _sb.Remove(_sb.Length - 1, 1); // remove last | comment with because status is use
                arFileContent.Add(_sb.ToString());
            } //end for loop row
        }
        private void SetDataForMulti(DataTable _dtsrc)
        {
            foreach (DataRow _dr in _dtsrc.Rows)
            {

                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                foreach (SendingFileColumn _col in clsFileLayout.ColumnList) //don't move column index
                {
                    if (_col.FieldName == "*")
                        continue; //Not support Dynamic Columns
                    object _obj = _dr[_col.FieldName];
                    SendingFileColumn _field = clsFileLayout.ColumnList.Find( x => x.FieldName == _col.FieldName);

                    string _s = string.Empty;
                    if (_dtsrc.Columns[_col.FieldName].DataType.Name.StartsWith("Int"))
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj).PadLeft(_field.Length, '0');
                        _s = _s.Substring(_s.Length - _field.Length);
                    }
                    else if (_dtsrc.Columns[_col.FieldName].DataType.Name.StartsWith("Decimal"))
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj).PadLeft(_field.Length, '0');
                        _s = _s.Substring(_s.Length - _field.Length);
                    }
                    else if (_dtsrc.Columns[_col.FieldName].DataType.Name == "DateTime")
                    {
                        _s = string.Format("{0:" + _field.Format + "}", _obj).PadRight(_field.Length, ' ');
                        _s = _s.Substring(0, _field.Length);
                    }
                    else
                    {
                        _s = string.Format("{0}", _obj).PadRight(_field.Length, ' ');
                        _s = _s.Substring(0, _field.Length);
                    }
                    _sb.Append(string.Concat(_s, "|"));
                }
                if (_sb.Length > 1)
                   _sb.Remove(_sb.Length - 1, 1); // remove last |
                arFileContent.Add(_sb.ToString());

            }

        }
        private string GetNextColumnOfDynamic()
        {
            for (int i = 0; i < clsFileLayout.ColumnList.Count - 1; i++) //not include latst
            {
                //FileColumn _col = clsFileLayout.ColumnList[i] as FileColumn;
                SendingFileColumn _col = clsFileLayout.ColumnList[i] ;
                if (_col.FieldName == "*")
                {
                    return (clsFileLayout.ColumnList[i + 1] as SendingFileColumn).FieldName;
                }
            }
            return string.Empty;
        }
        public ExcelPackage GetExcelPackage()
        {


            if (Util.IsNullorEmpty(this.FileName))
            {
                if (Util.IsNullorEmpty(this.FilePrefix))
                {
                    throw (Util.CustomException(SendingBatch.E_REQ_FILENAME));
                }
                this.FileName = string.Format(this.FilePrefix, DateTime.Now);
            }

            //Need to modify Incase template is using
            ExcelPackage _pck = new ExcelPackage();
            using (var source = System.IO.File.OpenRead(_excelTemplateFileName))
                if (_excelTemplateFileName != null)
                {
                    _pck = new ExcelPackage(source);
                }

            return _pck;
        }
        private void SetDataForExcelFile(DataTable _dtsrc)
        {
            dtFileContent = new DataTable();
            string _fieldName = string.Empty;
            foreach (SendingFileColumn _col in clsFileLayout.ColumnList) //don't move column index
            {
                if (_col.FieldName == "*")
                {

                    int _1stIndex = _dtsrc.Columns.IndexOf(_fieldName);
                    int _2ndIndex = _dtsrc.Columns.Count - 1;
                    string _lst = GetNextColumnOfDynamic();
                    if (_lst != string.Empty)
                    {
                        _2ndIndex = _dtsrc.Columns.IndexOf(_lst);
                    }
                    this.dExcelDynamicColumn.IsDyNamic = true;
                    this.dExcelDynamicColumn.StartColumn = _1stIndex + 1;
                    this.dExcelDynamicColumn.EndColumn = _2ndIndex - 1;

                    for (int i = this.dExcelDynamicColumn.StartColumn; i <= this.dExcelDynamicColumn.EndColumn; i++)
                    {
                        DataColumn _colFDynamic = _dtsrc.Columns[i];
                        dtFileContent.Columns.Add(_colFDynamic.ColumnName, _colFDynamic.DataType);
                    }
                    continue;
                }
                DataColumn _colF = _dtsrc.Columns[_col.FieldName];
                dtFileContent.Columns.Add(_colF.ColumnName, _colF.DataType);
                _fieldName = _col.FieldName;
            }
            //For Support Dynamic Coloumns
            if (dtFileContent.Columns.Count == 0)
            {
                foreach (DataColumn _col in _dtsrc.Columns)
                {
                    dtFileContent.Columns.Add(_col.ColumnName, _col.DataType);
                }
            }
            //End

            foreach (DataRow _dr in _dtsrc.Rows)
            {
                DataRow _drF = dtFileContent.NewRow();
                foreach (DataColumn _colF in dtFileContent.Columns)
                {
                    if (_dtsrc.Columns.IndexOf(_colF.ColumnName) >= 0)
                        _drF[_colF.ColumnName] = _dr[_colF.ColumnName];
                }
                dtFileContent.Rows.Add(_drF);
            }
            dtFileContent.AcceptChanges();
        }

        private void GenerateTextFile(string strPath , string runningFileName)
        {

            if (Util.IsNullorEmpty(this.FileName))
            {
                if (Util.IsNullorEmpty(this.FilePrefix))
                {
                    //_log.WriteErrorLogFile(SendingBatch.E_REQ_FILENAME) ;
                    throw (Util.CustomException(SendingBatch.E_REQ_FILENAME));
                }
                this.FileName = string.Format(this.FilePrefix, DateTime.Now);
                this.FileName = runningFileName + "_" + this.FileName;

            }

            if (!Util.IsNullorEmpty(this.HeaderText))
            {
                arFileContent.Insert(0, this.HeaderText);
            }
            if (!Util.IsNullorEmpty(this.FooterText))
            {
                arFileContent.Add(this.FooterText);
            }

            System.IO.StreamWriter sw = null;
            string _tmpF = Path.GetTempFileName();

            var utf8WithoutBom = new System.Text.UTF8Encoding(false);

            sw = new StreamWriter(_tmpF, false, utf8WithoutBom);
            for (int i = 0; i < arFileContent.Count; i++)
            {
                if (i > 0)
                    sw.WriteLine();

                sw.Write(arFileContent[i]);

            }
            //Check Flag here
            if (clsFileLayout.EnterOnLatestLine)
                sw.WriteLine();
            sw.Close();
            sw = null;

            //copy file
            string _destFileName = Path.Combine(strPath, this.FileName);
            if (File.Exists(_destFileName))
                File.Delete(_destFileName);
            System.IO.File.Move(_tmpF, _destFileName);

            Util.ConsoleWriteLine(string.Format(SendingBatch.I_GEN_END, DateTime.Now));
            Util.ConsoleWriteLine(string.Format(SendingBatch.I_GENFILE_END, _destFileName, this.FileName));
            Util.ConsoleWriteLine(string.Empty);
        }

        private void GenerateTextFile()
        {

            if (Util.IsNullorEmpty(this.FileName))
            {
                if (Util.IsNullorEmpty(this.FilePrefix))
                {
                    //_log.WriteErrorLogFile(SendingBatch.E_REQ_FILENAME) ;
                    throw (Util.CustomException(SendingBatch.E_REQ_FILENAME));
                }
                this.FileName = string.Format(this.FilePrefix, DateTime.Now);
            }

            if (!Util.IsNullorEmpty(this.HeaderText))
            {
                arFileContent.Insert(0, this.HeaderText);
            }
            if (!Util.IsNullorEmpty(this.FooterText))
            {
                arFileContent.Add(this.FooterText);
            }

            System.IO.StreamWriter sw = null;
            string _tmpF = Path.GetTempFileName();
            //sw = new StreamWriter(_tmpF, false, System.Text.Encoding.ASCII);
            //sw = new StreamWriter(_tmpF, false, System.Text.Encoding.GetEncoding(874));      
            var utf8WithoutBom = new System.Text.UTF8Encoding(false);
            sw = new StreamWriter(_tmpF, false, utf8WithoutBom);
            for (int i = 0; i < arFileContent.Count; i++)
            {
                if (i > 0)
                    sw.WriteLine();

                sw.Write(arFileContent[i]);

            }
            //Check Flag here
            if (clsFileLayout.EnterOnLatestLine)
                sw.WriteLine();
            sw.Close();
            sw = null;

            //copy file
            string _destFileName = Path.Combine(this.DirectoryName, this.FileName);
            if (File.Exists(_destFileName))
                File.Delete(_destFileName);
            System.IO.File.Move(_tmpF, _destFileName);

            Util.ConsoleWriteLine(string.Format(SendingBatch.I_GEN_END, DateTime.Now));
            Util.ConsoleWriteLine(string.Format(SendingBatch.I_GENFILE_END, _destFileName, this.FileName));
            Util.ConsoleWriteLine(string.Empty);
        }

        private void GenerateExcelFile(string strPath, string runningFileName )
        {
            try
            {
                if (Util.IsNullorEmpty(this.FileName))
                {
                    if (Util.IsNullorEmpty(this.FilePrefix))
                    {
                        throw (Util.CustomException(SendingBatch.E_REQ_FILENAME));
                    }
                    this.FileName = string.Format(this.FilePrefix, DateTime.Now);
                    this.FileName = runningFileName + "_" + this.FileName;
                }

                //Need to modify Incase template is using
                if (!System.IO.File.Exists(_excelTemplateFileName))
                {
                    throw new Exception("Template file is not found");
                }
                using (var source = System.IO.File.OpenRead(_excelTemplateFileName))

                using (var _pck = (_excelTemplateFileName != null ? new ExcelPackage(source) : new ExcelPackage()))
                {
                    var _ws = _excelTemplateFileName != null ? _pck.Workbook.Worksheets[1] : _pck.Workbook.Worksheets.Add("Result");

                    if (dtFileContent.Rows.Count > 0)
                    {
                        if (this.dtFileContent.Rows.Count > 1)
                            _ws.InsertRow(_excelStartRow + 1, this.dtFileContent.Rows.Count - 1, _excelStartRow);
                        ExcelRange _range = _ws.Cells[_excelStartRow, 1, _excelStartRow + this.dtFileContent.Rows.Count, this.dtFileContent.Columns.Count];
                        _range.LoadFromDataTable(dtFileContent, false);
                        if (SetAllBorderByDataSource)
                        {
                            SetBorders(_ws.Cells[_excelStartRow, 1, _excelStartRow + this.dtFileContent.Rows.Count - 1, this.dtFileContent.Columns.Count], true, true, true, true, true);
                        }
                    }
                    if (_excelTemplateFileName == null)
                    {
                        for (int i = 0; i < dtFileContent.Columns.Count - 1; i++)
                        {
                            _ws.Cells[1, i + 1].Value = dtFileContent.Columns[i].ColumnName;
                        }
                        _ws.Cells[1, 1, 1, dtFileContent.Columns.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _ws.Cells[1, 1, 1, dtFileContent.Columns.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Brown); //(System.Drawing.FromHtml("#f0f3f5"));
                    }
                    for (int i = 0; i < arCustomExcel.Count; i++)
                    {
                        object[] _obj = arCustomExcel[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Value = _obj[2];
                        if (_obj[3].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[3].ToString()));
                        }
                    }
                    //Add By Surasith T.
                    //Date : 2014-09-18
                    for (int i = 0; i < arCustomExcelFormat.Count; i++)
                    {
                        object[] _obj = arCustomExcelFormat[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Style.Numberformat.Format = string.Format("{0}", _obj[2]);
                    }
                    //End of Add New
                    /* Add new By Supachai
                       Add Date : 2014-08-15
                     */
                    for (int i = 0; i < arCustomExcelMerge.Count; i++)
                    {
                        object[] _obj = arCustomExcelMerge[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Value = _obj[4];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Merge = Convert.ToBoolean(_obj[5]);
                        if (_obj[6].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[6].ToString()));
                        }
                        if (_obj[7].ToString() != "")
                        {
                            if (_obj[7].ToString().ToUpper() == "CENTER")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            if (_obj[7].ToString().ToUpper() == "LEFT")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            }
                            if (_obj[7].ToString().ToUpper() == "RIGHT")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                        }
                        if (_obj[8].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[8].ToString()));
                            //_ws.Cells["A1:A3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#f0f3f5"));
                        }
                        SetBorders(_ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])], true, true, true, true, true);
                    }
                    //End Of Add New by Supachai
                    foreach (int _row in _arDeletedRow)
                    {
                        _ws.DeleteRow(_row, 1);
                    }
                    if (_excelTemplateFileName == null)
                    {
                        SetBorders(_ws.Cells[1, 1, _ws.Dimension.End.Row, _ws.Dimension.End.Column], true, true, true, true, true);
                    }

                    // int delRow = dtFileContent.Rows.Count;
                    //_ws.DeleteRow(delRow, delRow);
                    _pck.SaveAs(new System.IO.FileInfo(Path.Combine(strPath, this.FileName)));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GenerateExcelFile()
        {
            try
            {
                if (Util.IsNullorEmpty(this.FileName))
                {
                    if (Util.IsNullorEmpty(this.FilePrefix))
                    {
                        throw (Util.CustomException(SendingBatch.E_REQ_FILENAME));
                    }
                    this.FileName = string.Format(this.FilePrefix, DateTime.Now);
                }

                //Need to modify Incase template is using
                if (!System.IO.File.Exists(_excelTemplateFileName))
                {
                    throw new Exception("Template file is not found");
                }
                using (var source = System.IO.File.OpenRead(_excelTemplateFileName))

                using (var _pck = (_excelTemplateFileName != null ? new ExcelPackage(source) : new ExcelPackage()))
                {
                    var _ws = _excelTemplateFileName != null ? _pck.Workbook.Worksheets[1] : _pck.Workbook.Worksheets.Add("Result");

                    if (dtFileContent.Rows.Count > 0)
                    {
                        if (this.dtFileContent.Rows.Count > 1)
                            _ws.InsertRow(_excelStartRow + 1, this.dtFileContent.Rows.Count - 1, _excelStartRow);
                        ExcelRange _range = _ws.Cells[_excelStartRow, 1, _excelStartRow + this.dtFileContent.Rows.Count, this.dtFileContent.Columns.Count];
                        _range.LoadFromDataTable(dtFileContent, false);
                        if (SetAllBorderByDataSource)
                        {
                            SetBorders(_ws.Cells[_excelStartRow, 1, _excelStartRow + this.dtFileContent.Rows.Count-1, this.dtFileContent.Columns.Count], true, true, true, true, true);
                        }
                    }
                    if (_excelTemplateFileName == null)
                    {
                        for (int i = 0; i < dtFileContent.Columns.Count - 1; i++)
                        {
                            _ws.Cells[1, i + 1].Value = dtFileContent.Columns[i].ColumnName;
                        }
                        _ws.Cells[1, 1, 1, dtFileContent.Columns.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _ws.Cells[1, 1, 1, dtFileContent.Columns.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Brown); //(System.Drawing.FromHtml("#f0f3f5"));
                    }
                    for (int i = 0; i < arCustomExcel.Count; i++)
                    {
                        object[] _obj = arCustomExcel[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Value = _obj[2];
                        if (_obj[3].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[3].ToString()));
                        }
                    }
                    //Add By Surasith T.
                    //Date : 2014-09-18
                    for (int i = 0; i < arCustomExcelFormat.Count; i++)
                    {
                        object[] _obj = arCustomExcelFormat[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Style.Numberformat.Format = string.Format("{0}", _obj[2]);
                    }
                    //End of Add New
                    /* Add new By Supachai
                       Add Date : 2014-08-15
                     */
                    for (int i = 0; i < arCustomExcelMerge.Count; i++)
                    {
                        object[] _obj = arCustomExcelMerge[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Value = _obj[4];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Merge = Convert.ToBoolean(_obj[5]);
                        if (_obj[6].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[6].ToString()));
                        }
                        if (_obj[7].ToString() != "")
                        {
                            if (_obj[7].ToString().ToUpper() == "CENTER")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            if (_obj[7].ToString().ToUpper() == "LEFT")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            }
                            if (_obj[7].ToString().ToUpper() == "RIGHT")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                        }
                        if (_obj[8].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[8].ToString()));
                            //_ws.Cells["A1:A3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#f0f3f5"));
                        }
                        SetBorders(_ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])], true, true, true, true, true);
                    }
                    //End Of Add New by Supachai
                    foreach (int _row in _arDeletedRow)
                    {
                        _ws.DeleteRow(_row, 1);
                    }
                    if (_excelTemplateFileName == null)
                    {
                        SetBorders(_ws.Cells[1, 1, _ws.Dimension.End.Row, _ws.Dimension.End.Column], true, true, true, true, true);
                    }
                    
                    // int delRow = dtFileContent.Rows.Count;
                    //_ws.DeleteRow(delRow, delRow);
                    _pck.SaveAs(new System.IO.FileInfo(Path.Combine(this.DirectoryName, this.FileName)));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private SendingFileLayoutClass GetSendingFileLayout(string _configFileName, string _configSection)
        {
            SendingFileLayoutClass _tmp = new SendingFileLayoutClass();

            ConfigurationManager _cfg = new ConfigurationManager(_configFileName);
            System.Collections.Hashtable _ht = _cfg.GetAttributeList(_configSection);

            _tmp.SetFileLayoutType(Util.GetStringFromHash(_ht, "FileType"));

            if (_tmp.FileLayoutType == FileLayoutType.Split)
            {

                string _s = Util.GetStringFromHash(_ht, "SplitText");
                if (_s.Length == 0)
                {
                    throw (new Exception(SendingBatch.E_INVALID_SPLIT));
                }
                _tmp.SetSplitChar(_s.ToCharArray(0, 1)[0]);
            }
            else if (_tmp.FileLayoutType != FileLayoutType.Excel)
            {
                _tmp.EnterOnLatestLine = false;
                if (!Util.IsNullorEmpty(_ht, "EnterOnLatestLine"))
                {
                    _tmp.EnterOnLatestLine = (Util.GetStringFromHash(_ht, "EnterOnLatestLine") == "Y");
                }
            }
            else if (_tmp.FileLayoutType == FileLayoutType.Excel)
            {
                if (_ht.ContainsKey("ExcelTemplateFileName") && !Util.IsNullorEmpty(_ht, "ExcelTemplateFileName"))
                    this.ExcelTemplateFileName = Util.GetStringFromHash(_ht, "ExcelTemplateFileName");
                if (_ht.ContainsKey("ExcelStartRow") && !Util.IsNullorEmpty(_ht, "ExcelStartRow"))
                {
                    this._excelStartRow = Convert.ToInt32(Util.GetStringFromHash(_ht, "ExcelStartRow"));
                    excelStartRow = Convert.ToInt32(Util.GetStringFromHash(_ht, "ExcelStartRow"));
                }
                else
                    this._excelStartRow = 2;
            }
            System.Collections.ArrayList _ar = _cfg.GetNodesListAndAttribute(_configSection + "/Columns/Column");
            //if (_ar.Count == 0) Remove for support dynamics report
            //    throw(new Exception(SendingBatch.E_NO_FIELD_IN_CONFIG)) ;

            int _seq = 0;

            foreach (System.Collections.Hashtable _htColumn in _ar)
            {
                SendingFileColumn _col = new SendingFileColumn(_seq, Util.GetStringFromHash(_htColumn, "FieldName"));

                if (!Util.IsNullorEmpty(_htColumn["Format"]))
                    _col.Format = Util.GetStringFromHash(_htColumn, "Format");

                if (_tmp.FileLayoutType == FileLayoutType.FixLength || _tmp.FileLayoutType == FileLayoutType.Multi)
                {
                    _col.Length = Convert.ToInt32(Util.GetStringFromHash(_htColumn, "Length"));
                }
                _tmp.AddColumn(_col);
                _seq++;
            }

            //set default
            _ht = _cfg.GetChildNodes(_configSection);

            if (!Util.IsNullorEmpty(_ht, "DirectoryName"))
                this.DirectoryName = Util.GetStringFromHash(_ht, "DirectoryName");
            if (!Util.IsNullorEmpty(_ht, "Success"))
                this.SuccessPath = Util.GetStringFromHash(_ht, "Success");
            if (!Util.IsNullorEmpty(_ht, "Error"))
                this.ErrorPath = Util.GetStringFromHash(_ht, "Error");

            if (!Util.IsNullorEmpty(_ht, "FilePrefix"))
                this.FilePrefix = Util.GetStringFromHash(_ht, "FilePrefix");

            if (!Util.IsNullorEmpty(_ht, "HeaderText"))
                this.HeaderText = Util.GetStringFromHash(_ht, "HeaderText");

            if (!Util.IsNullorEmpty(_ht, "FooterText"))
                this.FooterText = Util.GetStringFromHash(_ht, "FooterText");

            //Comment by Patcharin - Cannot generate folder to multiple
            try
            {
                _ht = _cfg.GetChildNodes(_configSection + "/MultipleSend");
                if (_ht != null && _ht.Count > 0)
                {
                    foreach (string _f in _ht.Values)
                    {
                        _fMultipleFile.Add(_f);
                    }
                }
            }
            catch (Exception e)
            {
            }
            return _tmp;
        }
        public System.Collections.ArrayList _fMultipleFile = new System.Collections.ArrayList();
        #region -- public Property --
        public string HeaderText
        {
            get { return headerText; }
            set { headerText = value; }
        }
        public string FooterText
        {
            get { return footerText; }
            set { footerText = value; }
        }
        public bool SetAllBorderByDataSource
        {
            get;
            set; 
        }
        public DataTable DataSource
        {
            set
            {
                arFileContent = new System.Collections.ArrayList();
                DataTable _tmp = value;
                //Compare datatable with Datasource
                foreach (SendingFileColumn _col in clsFileLayout.ColumnList)
                {
                    if (_tmp.Columns.IndexOf(_col.FieldName) < 0 && _col.FieldName != "*")
                    {
                        throw (Util.CustomException(SendingBatch.E_INVALID_FIELDNAME, _col.FieldName));
                    }
                    //Modify support *
                }

                if (clsFileLayout.FileLayoutType == FileLayoutType.FixLength)
                {
                    SetDataForFixLength(_tmp);
                }
                else if (clsFileLayout.FileLayoutType == FileLayoutType.Split)
                {
                    SetDataForSplit(_tmp);
                }
                else if (clsFileLayout.FileLayoutType == FileLayoutType.Multi)
                {
                    SetDataForMulti(_tmp);
                }
                else if (clsFileLayout.FileLayoutType == FileLayoutType.Excel)
                {
                    SetDataForExcelFile(_tmp);
                }

            }
        }
        /// <summary>
        /// Directory Name for Output file
        /// Autocreated, incase does not exists.
        /// </summary>
        public string DirectoryName
        {
            set
            {
                directoryName = value;
                if (Directory.Exists(directoryName))
                {
                    return;
                }
                try
                {
                    Directory.CreateDirectory(directoryName);
                }
                catch (Exception ex)
                {
                    //_log.WriteErrorLogFile(ex.Message,ex) ;
                    throw (ex);
                }
            }
            get { return this.directoryName; }
        }

        private string _errorPath = string.Empty;
        public string ErrorPath
        {
            set
            {
                _errorPath = value;
                if (Directory.Exists(_errorPath))
                {
                    return;
                }
                try
                {
                    Directory.CreateDirectory(_errorPath);
                }
                catch (Exception ex)
                {
                    //_log.WriteErrorLogFile(ex.Message,ex) ;
                    throw (ex);
                }
            }
            get { return this._errorPath; }
        }
        private string _successPath = string.Empty;
        public string SuccessPath
        {
            set
            {
                _successPath = value;
                if (Directory.Exists(_successPath))
                {
                    return;
                }
                try
                {
                    Directory.CreateDirectory(_successPath);
                }
                catch (Exception ex)
                {
                    //_log.WriteErrorLogFile(ex.Message,ex) ;
                    throw (ex);
                }
            }
            get { return this._successPath; }
        }

       
        public int excelStartRow { get; set; }
        public string FileName
        {
            set
            {
                fileName = value;
            }
            get { return fileName; }
        }
        public string FullFileName
        {
            get
            {
                return System.IO.Path.Combine(this.DirectoryName, this.FileName);
            }
        }

        public bool MultipleSend(System.Collections.ArrayList arPathList)
        {
           
            bool bError = false;
            foreach (string _fPath in arPathList)
            {
               
                try
                {
                    if (!Directory.Exists(_fPath))
                    {
                        Directory.CreateDirectory(_fPath);
                    }
                    File.Copy(this.FullFileName, System.IO.Path.Combine(_fPath, this.FileName));
                }
                catch (Exception ex)
                {
                    bError = true;
                    continue;
                }

            }
            return bError;
        }

        public string ExcelTemplateFileName
        {
            set
            {
                if (!System.IO.File.Exists(value))
                {
                    _excelTemplateFileName = null;
                    return;
                    // throw (new Exception(string.Format(CommonMessage.E_FILE_NOTFOUND, value)));
                }
                _excelTemplateFileName = value;
            }
            get
            {
                return _excelTemplateFileName;
            }
        }
        public string FilePrefix
        {
            get { return _filePrefix; }
            set { _filePrefix = value; }
        }
        #endregion

        #region -- public method --
        // เพิ่มเติม วันที่ 15-08-2014 ///////////////////////////////////////////
        public void WriteCustomExcelDataMerge(int _FromRow, int _FromCol, int _ToRow, int _ToCol, object _data, bool _mergeCell = false, string _color = "", string _HorizontalAlignment = "", string _BackgroundColor = "")
        {
            if (clsFileLayout.FileLayoutType != FileLayoutType.Excel)
            {
                throw (new Exception("This property for excel only"));
            }
            arCustomExcelMerge.Add(new object[] { _FromRow, _FromCol, _ToRow, _ToCol, _data, _mergeCell, _color, _HorizontalAlignment, _BackgroundColor });
        }

        /// <summary>
        /// This Function for write excel only
        /// </summary>
        /// <param name="_row"></param>
        /// <param name="_col"></param>
        /// <param name="_data"></param>
        public void WriteCustomExcelData(int _row, int _col, object _data, string _color = "")
        {
            if (clsFileLayout.FileLayoutType != FileLayoutType.Excel)
            {
                throw (new Exception("This property for excel only"));
            }
            arCustomExcel.Add(new object[] { _row, _col, _data, _color });
        }
        public void SetCustomExcelFormat(int _row, int _col, string _format)
        {
            if (clsFileLayout.FileLayoutType != FileLayoutType.Excel)
            {
                throw (new Exception("This property for excel only"));
            }
            arCustomExcelFormat.Add(new object[] { _row, _col, _format });
        }
        private System.Collections.ArrayList _arDeletedRow = new System.Collections.ArrayList();
        public void DeleteRow(System.Collections.ArrayList _arDeletedRow)
        {
            this._arDeletedRow = _arDeletedRow;
        }
        public void Execute()
        {
            if (clsFileLayout.FileLayoutType == FileLayoutType.Excel)
            {
                GenerateExcelFile();
            }
            else
            {
                GenerateTextFile();
            }
            if (_fMultipleFile.Count > 0)
                MultipleSend(_fMultipleFile);
        }

        public void ExecuteByFixPath(string path, string runningFileName)
        {
            if (clsFileLayout.FileLayoutType == FileLayoutType.Excel)
            {
                GenerateExcelFile(path, runningFileName);
            }
            else
            {
                // Only text file
                GenerateTextFile(path, runningFileName);
            }
            if (_fMultipleFile.Count > 0)
                MultipleSend(_fMultipleFile);

        }

        public void MoveFileToArchive(bool _success)
        {

            if (_success)
                Util.MoveFile(this.FullFileName, this.SuccessPath); //Need move after finish process
            else
                Util.MoveFile(this.FullFileName, this.ErrorPath);
        }


        public void Reset()
        {
            this.headerText = null;
            this.footerText = null;
            this.arFileContent = null;
            this.directoryName = null;
            this.fileName = null;
        }
        public void SetBorders(ExcelRange _wrs, bool _bTop, bool _bLeft, bool _bRight, bool _bBottom, bool _bInner)
        {
            if (_bTop)
                _wrs.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            if (_bLeft)
                _wrs.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            if (_bRight)
                _wrs.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            if (_bBottom)
                _wrs.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            if (_bInner)
                _wrs.Style.Border.Diagonal.Style = ExcelBorderStyle.Thin;

        }
        #endregion

        public void ExecuteBorder(List<SetBorder> bdList, string headerText)//Add new by Patcharin K.
        {
            if (clsFileLayout.FileLayoutType == FileLayoutType.Excel)
            {
                GenerateExcelFileBorder(bdList, headerText);
            }
        }
        private void GenerateExcelFileBorder(List<SetBorder> bdList, string headerText)//Add new by Patcharin K.
        {
            try
            {
                if (Util.IsNullorEmpty(this.FileName))
                {
                    if (Util.IsNullorEmpty(this.FilePrefix))
                    {
                        throw (Util.CustomException(SendingBatch.E_REQ_FILENAME));
                    }
                    this.FileName = string.Format(this.FilePrefix, DateTime.Now);
                }

                if (!System.IO.File.Exists(_excelTemplateFileName))
                {
                    throw new Exception("Template file is not found");
                }
                using (var source = System.IO.File.OpenRead(_excelTemplateFileName))

                using (var _pck = (_excelTemplateFileName != null ? new ExcelPackage(source) : new ExcelPackage()))
                {
                    var _ws = _excelTemplateFileName != null ? _pck.Workbook.Worksheets[1] : _pck.Workbook.Worksheets.Add("Result");
                    if (!string.IsNullOrEmpty(headerText))
                    {
                        _ws.Cells[1, 1].Value = headerText;
                    }
                    if (dtFileContent.Rows.Count > 0)
                    {
                        if (this.dtFileContent.Rows.Count > 1)
                            _ws.InsertRow(_excelStartRow + 1, this.dtFileContent.Rows.Count - 1, _excelStartRow);
                        ExcelRange _range = _ws.Cells[_excelStartRow, 1, _excelStartRow + this.dtFileContent.Rows.Count, this.dtFileContent.Columns.Count];
                        //_ws.InsertRow(_excelStartRow + 1, this.dtFileContent.Rows.Count - 1, _excelStartRow);
                        //ExcelRange _range = _ws.Cells[_excelStartRow, 1, _excelStartRow + this.dtFileContent.Rows.Count - 1, this.dtFileContent.Columns.Count];
                        //Row, ColumnFrom, ColumnTo
                        if (bdList != null && bdList.Count > 0)
                        {
                            for (int i = 0; i < bdList.Count; i++)
                            {
                                _ws.Cells[bdList[i].Row, bdList[i].ColumnFrom, bdList[i].Row, bdList[i].ColumnTo].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                        }
                        _range.LoadFromDataTable(dtFileContent, false);
                    }
                    if (_excelTemplateFileName == null)
                    {
                        for (int i = 0; i < dtFileContent.Columns.Count - 1; i++)
                        {
                            _ws.Cells[1, i + 1].Value = dtFileContent.Columns[i].ColumnName;
                        }
                        _ws.Cells[1, 1, 1, dtFileContent.Columns.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _ws.Cells[1, 1, 1, dtFileContent.Columns.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Brown); //(System.Drawing.FromHtml("#f0f3f5"));
                    }
                    for (int i = 0; i < arCustomExcel.Count; i++)
                    {
                        object[] _obj = arCustomExcel[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Value = _obj[2];
                        if (_obj[3].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1])].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[3].ToString()));
                        }
                    }

                    for (int i = 0; i < arCustomExcelMerge.Count; i++)
                    {
                        object[] _obj = arCustomExcelMerge[i] as object[];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Value = _obj[4];
                        _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Merge = Convert.ToBoolean(_obj[5]);
                        if (_obj[6].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[6].ToString()));
                        }
                        if (_obj[7].ToString() != "")
                        {
                            if (_obj[7].ToString().ToUpper() == "CENTER")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }
                            if (_obj[7].ToString().ToUpper() == "LEFT")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            }
                            if (_obj[7].ToString().ToUpper() == "RIGHT")
                            {
                                _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                        }
                        if (_obj[8].ToString() != "")
                        {
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(_obj[8].ToString()));
                        }
                        SetBorders(_ws.Cells[Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3])], true, true, true, true, true);
                    }
                    if (_excelTemplateFileName == null)
                    {
                        SetBorders(_ws.Cells[1, 1, _ws.Dimension.End.Row, _ws.Dimension.End.Column], true, true, true, true, true);
                    }
                    _pck.SaveAs(new System.IO.FileInfo(Path.Combine(this.DirectoryName, this.FileName)));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

    }

    public class SetBorder//Add new by Patcharin K.
    {
        int row = 0;
        int columnFrom = 0;
        int columnTo = 0;
        public int Row
        {
            get
            {
                return row;
            }
            set
            {
                row = value;
            }
        }
        public int ColumnFrom
        {
            get
            {
                return columnFrom;
            }
            set
            {
                columnFrom = value;
            }
        }
        public int ColumnTo
        {
            get
            {
                return columnTo;
            }
            set
            {
                columnTo = value;
            }
        }
    }
}