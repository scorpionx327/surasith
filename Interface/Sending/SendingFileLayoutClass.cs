using System;
using System.Collections.Generic;
namespace th.co.toyota.stm.fas.common.Interface
{
	/// <summary>
	/// Summary description for FileLayoutClass.
	/// </summary>
	public class SendingFileLayoutClass
	{
		public SendingFileLayoutClass()
		{
			//
			// TODO: Add constructor logic here
			//
            ColumnList = new List<SendingFileColumn>();
		}
        public List<SendingFileColumn> ColumnList { get; set; }
		//private System.Collections.Hashtable htColumnList= new System.Collections.Hashtable() ;

		private FileLayoutType fileLayoutType = FileLayoutType.None  ;
		public char SplitChar ;
        public bool EnterOnLatestLine = false;
        //public bool OnlineMode = false;
        //public int ExcelStartRow = 2;
		public FileLayoutType FileLayoutType
		{
			get{return fileLayoutType ;}
		}
		public void SetFileLayoutType(string _fileType)
		{
			//already check
			if(_fileType.ToLower() == "FixLength".ToLower())
			{
				this.fileLayoutType = FileLayoutType.FixLength ;
				return ;
			}
			if(_fileType.ToLower() == "Split".ToLower())
			{
				this.fileLayoutType  = FileLayoutType.Split ;
				return ;
			}
			if(_fileType.ToLower() == "Multi".ToLower())
			{
				this.fileLayoutType  = FileLayoutType.Multi ;
				return ;
			}
            if (_fileType.ToLower() == "Excel".ToLower())
            {
                this.fileLayoutType = FileLayoutType.Excel;
                return;
            }
			throw(new Exception(SendingBatch.E_INVALID_FILEOUTPUT_TYPE)) ; //missing fileoutputtype
		}
		
		
		public void SetSplitChar(char _splitText)
		{
			this.SplitChar = _splitText ;
		}
		
		
		public void AddColumn(SendingFileColumn _col)
		{
			if(this.fileLayoutType == FileLayoutType.None)
			{
				throw(new Exception(SendingBatch.E_INVALID_FILELAYOUT)) ;
			}

			//if(htColumnList.ContainsKey(_col.FieldName))
            if (this.ColumnList.FindAll(x => x.FieldName == _col.FieldName).Count > 0)
			{
				throw(Util.CustomException(SendingBatch.E_DUP_FIELDNAME,_col.FieldName)) ; //duplicate
			}
			if(this.ColumnList.Count == 0)
			{

                this.ColumnList.Add(_col);
				//htColumnList.Add(_col.FieldName,_col) ;

				return ;
			}
			

			//check 
            SendingFileColumn _f = this.ColumnList[this.ColumnList.Count - 1]; //this.arColumnList[this.arColumnList.Count - 1]  ;
			if(_f.Sequence + 1 != _col.Sequence )
			{
				throw(Util.CustomException(SendingBatch.E_INVALID_SEQUENCE,_f.FieldName, _col.FieldName)) ;
			}



            this.ColumnList.Add(_col);
			//htColumnList.Add(_col.FieldName,_col) ;
		}
        //public SendingFileColumn GetColumn(string _colName)
        //{
        //    //already check exists
        //    return htColumnList[_colName] as SendingFileColumn;
        //}

        //public System.Collections.ArrayList ColumnList
        //{
        //    get { return arColumnList; }
        //}
	
	}
    //public enum FileLayoutType
    //{
    //    None,
    //    FixLength,
    //    Split,
    //    Multi,
    //    Excel
    //}
	public class SendingFileColumn
	{
        public SendingFileColumn(int _seq, string _fieldName)
		{
			this.Sequence = _seq ;
			this.FieldName = _fieldName ;
//			this.StartPosition = -1 ;
			this.Length = -1 ;
		}

		public int Sequence = -1 ;
		public string FieldName = string.Empty ;
//		public int StartPosition = -1 ;
		public int Length = -1 ;
		public string Format = string.Empty ;
	}
}
