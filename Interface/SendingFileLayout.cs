﻿
using System;
using System.Collections.Generic;
using System.IO;

using System.Text;
using System.Threading.Tasks;

namespace Hino.Interface
{
    public class SendingFileLayout
    {
        public SendingFileLayout()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private System.Collections.ArrayList arColumnList = new System.Collections.ArrayList();
        private System.Collections.Hashtable htColumnList = new System.Collections.Hashtable();
        private FileLayoutType fileLayoutType = FileLayoutType.None;
        //private string parkingTable = null;
        private int _StartRow = 0;
        private string _DirectoryName = null;
        //private string _FilePrefix = null;
        private string _TemplateName = null;
        private string _TemplateSheet = null;
        private string _DirectoryFileTemplate = null;
        //private string _CheckTypeConfigColumn = null;
        public int StartRow
        {
            get { return _StartRow; }
            set { _StartRow = value; }
        }
        //public string ParkingTable
        //{
        //    get { return parkingTable; }
        //    set { parkingTable = value; }
        //}

        public string DirectoryName
        {
            set
            {
                _DirectoryName = value;
                if (Directory.Exists(_DirectoryName))
                {
                    return;
                }
                try
                {
                    Directory.CreateDirectory(_DirectoryName);
                }
                catch (Exception ex)
                {
                    //_log.WriteErrorLogFile(ex.Message, ex);
                    throw (ex);
                }
            }
            get { return _DirectoryName; }
        }

        //public string FilePrefix
        //{
        //    get { return _FilePrefix; }
        //    set { _FilePrefix = value; }
        //}

        public string TemplateName
        {
            get { return _TemplateName; }
            set { _TemplateName = value; }
        }
        public string TemplateSheet
        {
            get { return _TemplateSheet; }
            set { _TemplateSheet = value; }
        }
        public string DirectoryFileTemplate
        {
            set
            {
                _DirectoryFileTemplate = value;
                if (Directory.Exists(_DirectoryFileTemplate))
                {
                    return;
                }
                try
                {
                    Directory.CreateDirectory(_DirectoryFileTemplate);
                }
                catch (Exception ex)
                {
                    //_log.WriteErrorLogFile(ex.Message, ex);
                    throw (ex);
                }
            }
            get { return _DirectoryFileTemplate; }
        }

        //public string CheckTypeConfigColumn
        //{
        //    get { return _CheckTypeConfigColumn; }
        //    set { _CheckTypeConfigColumn = value; }
        //}

        public FileLayoutType FileLayoutType
        {
            get { return fileLayoutType; }
        }
        public void SetFileLayoutType(string _fileType)
        {
            if (_fileType.ToLower() == "Excel".ToLower())
            {
                this.fileLayoutType = FileLayoutType.Excel;
                return;
            }
            throw (new Exception(SendingBatch.E_INVALID_FILEOUTPUT_TYPE)); //missing fileoutputtype
        }


        //public void AddColumn(FileColumn _col)
        //{
        //    if (this.fileLayoutType == FileLayoutType.None)
        //    {
        //        throw (new Exception(STMError.SendingBatch.E_INVALID_FILELAYOUT));
        //    }
        //    if (htColumnList.ContainsKey(_col.FieldName))
        //    {
        //        throw (Util.CustomException(STMError.SendingBatch.E_DUP_FIELDNAME, _col.FieldName)); //duplicate
        //    }
        //    if (this.arColumnList.Count == 0)
        //    {

        //        this.arColumnList.Add(_col);
        //        htColumnList.Add(_col.FieldName, _col);

        //        return;
        //    }


        //    //check 
        //    FileColumn _f = this.arColumnList[this.arColumnList.Count - 1] as FileColumn;
        //    if (_f.Sequence + 1 != _col.Sequence)
        //    {
        //        throw (Util.CustomException(STMError.SendingBatch.E_INVALID_SEQUENCE, _f.FieldName, _col.FieldName));
        //    }



        //    this.arColumnList.Add(_col);
        //    htColumnList.Add(_col.FieldName, _col);
        //}
        //public FileColumn GetColumn(string _colName)
        //{
        //    //already check exists
        //    return htColumnList[_colName] as FileColumn;
        //}

        public System.Collections.ArrayList ColumnList
        {
            get { return arColumnList; }
        }

    }
    
}
public enum FileLayoutType
{
    None,
    Excel
}
//public class FileColumn
//{
//    public FileColumn(int _seq, string _fieldName)
//    {
//        this.Sequence = _seq;
//        this.FieldName = _fieldName;
//        this.Length = -1;
//    }

//    public int Sequence = -1;
//    public string FieldName = string.Empty;
//    public int Length = -1;
//    public string Format = string.Empty;
//    public string DataType = string.Empty;
//}