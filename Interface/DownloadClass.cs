﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web;

namespace th.co.toyota.stm.fas.common.Interface
{
    public class DownloadClass
    {
        public DownloadClass() { 
        
        }
        public string ConfigurationFileName { get; set; }
        public DataTable DataSource{get;set;}
        private string ConfigurationSectionName { get; set; }
        private System.Collections.ArrayList arCustomItem = new System.Collections.ArrayList();
        private System.Collections.ArrayList arDeletedRow = new System.Collections.ArrayList();
        public void SetCustomData(int _row, int _col, object _data, string _color = "")
        {
            arCustomItem.Add(new object[] { _row, _col, _data, _color });
        }
        // เพิ่มเติมวันที่ 15-08-2014 //////////////////////
        private System.Collections.ArrayList arCustomItemMerge = new System.Collections.ArrayList();
        public void SetCustomDataMerge(int _FromRow, int _FromCol, int _ToRow, int _ToCol, object _data, bool _mergeCell = false, string _color = "", string _HorizontalAlignment = "", string _BackgroundColor = "")
        {
            arCustomItemMerge.Add(new object[] { _FromRow, _FromCol, _ToRow, _ToCol, _data, _mergeCell, _color, _HorizontalAlignment, _BackgroundColor });
        }
        ////////////////////////////////////////////

        public bool SetAllBorderByDataSource { get; set; }

        public string FileName { get; set; }
        public void DeletedRow(int _row)
        {
            arDeletedRow.Add(_row);
        }
        /// <summary>
        /// Return Full File Name
        /// </summary>
        /// <returns></returns>
        public string DownloadFile()
        {
            try
            {
                if (this.DataSource == null)
                    throw (new Exception("Not found Data source for download"));


                if (!System.IO.File.Exists(this.ConfigurationFileName))
                    throw (new Exception("Configuration file not found"));

                Hino.Interface.Sending.GenerateFileClass _d = new Interface.Sending.GenerateFileClass(this.ConfigurationFileName, "configuration/SendingFileCommonBatch/ReportConfiguration");
                _d.DataSource = this.DataSource ;
                foreach (object[] _obj in arCustomItem) {
                    _d.WriteCustomExcelData(Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), _obj[2], _obj[3].ToString());
                }
                // เพิ่มเติมวันที่ 15-08-2014 //////////////////////
                foreach (object[] _obj in arCustomItemMerge)
                {
                    _d.WriteCustomExcelDataMerge(Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), Convert.ToInt32(_obj[2]), Convert.ToInt32(_obj[3]), _obj[4], Convert.ToBoolean(_obj[5]), _obj[6].ToString(), _obj[7].ToString(), _obj[8].ToString());
                }
                ////////////////////////////////////////////


                // add this function on 20141125 by Uten //////////////////////
                foreach (object[] _obj in arCustomExcelFormat)
                {
                    _d.SetCustomExcelFormat(Convert.ToInt32(_obj[0]), Convert.ToInt32(_obj[1]), _obj[2].ToString());
                }
                ////////////////////////////////////////////

                if (!string.IsNullOrEmpty(this.FileName))
                {
                    _d.FileName = this.FileName;
                }
                _d.SetAllBorderByDataSource = this.SetAllBorderByDataSource;
                _d.DeleteRow(arDeletedRow);
                _d.Execute();
              //  this.DeleteOutofDateFile(_d.DirectoryName);                
                return _d.FullFileName;
              
            }
            catch (Exception ex)
            {
               // return null;
                throw (ex);
            }    
        }

        private void DeleteOutofDateFile(string _directoryName)
        {
            System.IO.DirectoryInfo _dInfo = new System.IO.DirectoryInfo(_directoryName);
            System.IO.FileInfo[] _fInfo = _dInfo.GetFiles();

            foreach (System.IO.FileInfo _fs in _fInfo)
            {
                if ((DateTime.Now - _fs.CreationTime).TotalDays <= 1)
                {
                    continue;
                }
                // deleted
                try
                {
                    _fs.Delete();
                }
                catch
                {
                    //No Action
                }
            }
            
        }


        private System.Collections.ArrayList arCustomExcelFormat = new System.Collections.ArrayList();
 
        public void SetCustomExcelFormat(int _row, int _col, string _format)
        {
            arCustomExcelFormat.Add(new object[] { _row, _col, _format });
        }
    }
}