﻿using System;

namespace th.co.toyota.stm.fas.common.Interface
{
    public struct ErrorInfo
    {
        public string ErrorCode, ErrorTitle, ErrorDescription;
    }
    public class CommonMessage
    {
        public const string E_FILE_CONFIG = "Configuration file not found";
        public const string E_CONFIG = "Configuration {0} not found";
        public const string E_SYSCONFIG = "Not found configuration {0} in TB_M_SYSTEM ";
        public const string E_INVALID_PARAMS = "Invalid Batch Parameter";
        public const string E_FILE_NOTFOUND = "File {0} not found";
    }
    public class SendingBatch //No connect DB
    {

        public const string E_DUP_FIELDNAME = "Duplicate FieldName {0} in FileLayout";
        public const string E_INVALID_FILELAYOUT = "Please assign FileLayout Type before call this process";
        public const string E_INVALID_FILEOUTPUT_TYPE = "FileOutputType should be FixLength or SplitColumn only";

        public const string E_INVALID_SEQUENCE = "Error Sequence between {0} and {1}";
        public const string E_INVALID_LENGTH = "Error Length between {0} and {1}";

        public const string E_INVALID_FIELDNAME = "Field Name {0} not found in Datasource";
        public const string E_NO_FIELD_IN_CONFIG = "No column in Configuration file";
        public const string E_INVALID_SPLIT = "Split Text not found for Split Filelayout";
        public const string E_REQ_FILENAME = "Filename cannot be blank";
        public const string I_BATCH_RESULT = "Batch Result of {0:dd.MM.yyyy}";
        public const string I_GEN_START = "{0:HH:mm:ss} START GENERATE FILE";
        public const string I_GEN_END = "{0:HH:mm:ss} END GENERATE FILE";
        public const string I_GENFILE_END = "File {1} is generated to {0}";
    }
    public class ReceivingBatch
    {
        public const string E_DUP_FIELDNAME = "Duplicate FieldName {0} in FileLayout";
        public const string E_INVALID_FILELAYOUT = "Please assign FileLayout Type before call this process";
        public const string E_INVALID_FILEOUTPUT_TYPE = "FileOutputType should be FixLength or SplitColumn only";

        public const string E_INVALID_SEQUENCE = "Error Sequence between {0} and {1}";
        public const string E_INVALID_LENGTH = "Error Length between {0} and {1}";

        public const string E_INVALID_FIELDNAME = "Field Name {0} not found in Datasource";
        public const string E_REQ_FILENAME = "Filename {0} not found";
        public const string E_NO_FIELD_IN_CONFIG = "No column in Configuration file";
        public const string E_INVALID_SPLIT = "Split Text not found for Split Filelayout";
        public const string E_INVALID_FILENAME = "Filename cannot be blank";
        public const string E_NO_DATA = "No data in file {0}";
        public const string E_LINE_MISSMATCH = "line@{0} => Total Length in configuration not equal to Total Length of all columns in Database";
        public const string E_DATA_MISSMATCH = "Filename {0} => Data is missmatch";

        public const string E_DATA_DUP_FILE = "Filename {0} => Data is duplicate";
        public const string E_LINE_DUP = "Duplicate data for line ==> {0}";
        public const string E_INVALID_CONFIG = "Invalid {0} in Configuration file";
        public const string E_MISSMATCH_LENGTH = "line@{0} => Missmatch total length in configuration and line in file";

        public const string E_REQ_TARGET = "Target table {0} not found";
        public const string E_REQ_COLUMNS_FILE = "Target Column {0} not found in Table";
        public const string E_COL_MISSMATCH = "Target Column {0} is missmatch with file layout";
        public const string E_COL_INVALID_TYPE = "line@{0} => column ({1}) is missmatch type";
        public const string E_REQ_FORMAT = "Format in filelayout cannot be blank for DateTime DataType";
        public const string E_REQ_COLUMNS_DB = "line@{0} => column {1} cannot be blank"; //25
        public const string E_REQ_COLUMNS_LENGTH = "line@{0} => column {1} invalid Length";

        public const string E_COL_REQEXPRESS = "line@{0} => column {1} is invalid regular expression : {2}";

        public const string E_COL_COUNT_MISS = "line@{0} => Column Count missmatch between File layout and Database";
        public const string E_DIR_FAIL = "Create directory {0} fail => {1}";
        public const string E_DIR_NO_FOUND = "Path {0} not found";

        public const string I_BATCH_RESULT = "Batch Result of {0:dd.MM.yyyy}";
        public const string I_BATCH_START = "BATCH START";
        public const string I_BATCH_END = "BATCH END";
        public const string I_INSERT_STAGGING_SUCCESS = "MSTD0000BINF : Copy data from file to stagging table success";
        public const string E_ACCESS_DENIED = "Cannot access path => {0}";
        public const string E_EXTRACT_FAIL = "Cannot extract file {0} => {1}";
        public const string E_HEADER_PREFIX_MISSMATCH = "Header data is missmatch with Configuration {0} ";
        public const string E_FOOTER_PREFIX_MISSMATCH = "Footer data is missmatch with Configuration {0} ";
        public const string E_FOOTER_TOTALCOUNT_MISSMATCH = "File {0} is invalid ASCII File, Trailer - Total Records do not match";


        public const string E_HEADER_ERROR = "Cannot check header column {0} => {1}";
        public const string E_EXCEL_VERSION_INVALID = "Excel File version is not xlsx => {0}";

        public const string E_NOT_IN_LIST = "line@{0} : column ({1}) must exists in ({2}) only. Your data is ({3})"; //split | to ,
        public const string E_NOT_IN_LIST_1_VAL = "line@{0} : column ({1}) must be ({2}) only. Your data is ({3})"; //split | to ,
        public const string E_LEAVE_BANK = "line@{0} : column ({1}) must be blank only. Your data is ({2})"; //split | to ,
    }
    public class CommonMessageBatch
    {
        /// <summary> MSTD7000BINF : {0} Begin , {0} is Batch Name
        /// </summary> 
        public const string MSTD7000BINF = "MSTD7000BINF : {0} Begin";
        /// <summary> MSTD7001BINF : {0} End successfully , {0} is Batch Name
        /// </summary> 
        public const string MSTD7001BINF = "MSTD7001BINF : {0} End successfully";
        /// <summary> MSTD7002BINF : {0} End with error {1}, {0} is Batch Name ,{1} is error description
        /// </summary> 
        public const string MSTD7002BINF = "MSTD7002BINF : {0} End with error {1}";
        /// <summary> MSTD7003BINF : {0} End with warning {1} , {0} is Batch Name ,{1} is warning description
        /// </summary> 
        public const string MSTD7003BINF = "MSTD7003BINF : {0} End with warning {1}";

        /// <summary> MSTD0067AERR : Undefine Error : {0} , {0} is exception 
        /// </summary> 
        public const string MSTD0067AERR = "MSTD0067AERR : Undefine Error : {0}";

        /// <summary> MSTD7066BINF : {0}  End successfully {1}, {0} is Batch Name,{1} : Date and time, No. of records
        /// </summary> 
        public const string MSTD7066BINF = "MSTD7066BINF : {0}  End successfully {1}";

        public const string MSTD7054BERR = "MSTD7054BERR : {0} Data not found from {1}";
        public const string DATA_NOT_FOUND = "MSTD0059AERR : No data found";

        public const string MSTD0013AERR = "MSTD0013AERR : {0} file does not exist";
        
    }
    public class MessageResult
    {
        public eLogLevel MessageResultType = eLogLevel.Information;
        public string Message = string.Empty;
        public MessageResult(eLogLevel _type, string _msg)
        {
            this.MessageResultType = _type;
            this.Message = _msg;
        }
        public MessageResult(eLogLevel _type, string _msg, params object[] _args)
        {
            this.MessageResultType = _type;
            this.Message = string.Format(_msg, _args);
        }
    }
}
