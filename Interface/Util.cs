﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.common.Interface
{
    public class Util
    {
        public Util()
        {
            //
            // TODO: Add constructor logic here
            //

        }
        public static bool IsCorrectDateTimeFormat(string _t, string _format)
        {
            if (_t == string.Empty)
                return true;
            try
            {
                DateTime _c = DateTime.ParseExact(_t, _format, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool IsNullorEmpty(object _obj)
        {
            if (_obj == null) return true;
            return string.Format("{0}", _obj).Trim() == string.Empty;
        }
        public static bool IsNullorEmpty(Hashtable _ht, string _key)
        {
           
            if (!_ht.ContainsKey(_key))
            {
                return true;
            }
            return IsNullorEmpty(_ht[_key]);
        }
        public static string GetStringFromHash(Hashtable _ht, string _key)
        {
           
            if (!_ht.ContainsKey(_key) || IsNullorEmpty(_ht[_key]))
            {
                throw (Util.CustomException(CommonMessage.E_CONFIG, _key)); // config key not found
            }
            return string.Format("{0}", _ht[_key]);
        }
        //public static int GetIntFromHash(Hashtable _ht, string _key)
        //{
        //    if (!_ht.ContainsKey(_key) || IsNullorEmpty(_ht[_key]))
        //    {
        //        throw (Util.CustomException(CommonMessage.E_CONFIG, _key)); // config key not found
        //    }
        //    if (!Util.IsInt(_ht[_key].ToString()))
        //    {
        //        throw (Util.CustomException(CommonMessage.E_CONFIG, _key)); // config key not found
        //    }
        //    return int.Parse(_ht[_key].ToString());
        //}
        public static Exception CustomException(string _format, params object[] _args)
        {
            return new Exception(string.Format(_format, _args));
        }
        public static void ConsoleWriteLine(string _s)
        {
            int _max = 64;
            int _len = _s.Length;

            if (_s == string.Empty)
            {
                Console.WriteLine(_s.PadRight(_max, '#'));
            }
            else
            {
                Console.WriteLine("##{1:HH:mm:ss}  {0}##", _s.PadRight(58, ' '), DateTime.Now);
            }
        }
        public static void ConsoleWriteLine(string _format, params object[] _args)
        {
            string _f = string.Format(_format, _args);
            Util.ConsoleWriteLine(_f);
        }
        public static bool IsInt(string _str)
        {
            try
            {
                int.Parse(_str);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static void DeleteFile(string _fileName, bool _bThrowException)
        {
            if (Util.IsNullorEmpty(_fileName))
                return;
            try
            {
                System.IO.File.Delete(_fileName);
            }
            catch (Exception ex)
            {
                if (_bThrowException)
                    throw (ex);
            }
        }
        public static string MoveFile(string _srcName, string _destPath)
        {
            if (!System.IO.File.Exists(_srcName))
            {
                return null ;
            }

            if (string.IsNullOrEmpty(_destPath))
                return null;

            Util.ConsoleWriteLine("Move File {0} to {1}", _srcName, _destPath);

            if (!System.IO.Directory.Exists(_destPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(_destPath);
                }
                catch (Exception ex)
                {
                    throw (ex);
                    //throw (new BatchCommon.Log.BatchLoggingException(ReceivingBatch.E_DIR_FAIL, _destPath, ex.Message));
                }
            }

            try
            {

                
                string _name = (new System.IO.FileInfo(_srcName)).Name;
                string _destName = System.IO.Path.Combine(_destPath, _name);
                if (System.IO.File.Exists(_destName))
                {
                    System.IO.FileInfo _f = new System.IO.FileInfo(_destName);
                    string _newFileName = _f.Name;
                    if (_f.Extension != string.Empty)
                        _newFileName = _f.Name.Replace(_f.Extension, string.Empty);

                    _newFileName = _newFileName + string.Format("_{0:yyyyMMdd_HHmmss}", DateTime.Now) + _f.Extension;

                    _destName = System.IO.Path.Combine(_destPath, _newFileName);
                }

                System.IO.File.Move(_srcName, _destName);

                return (new System.IO.FileInfo(_destName).Name);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public static void DeleteDirectory(string _path)
        {
            if (_path != string.Empty && System.IO.Directory.Exists(_path))
            {
                try
                {
                    System.IO.Directory.Delete(_path, true);
                }
                catch (Exception ex)
                {
                    //no exp
                }
            }

        }
        
        public static DateTime StringToDate(string convertDatetime)
        {
            DateTime datetimeResult = new DateTime();
            //datetimeResult = DateTime.ParseExact(convertDatetime, "dd MMM yyyy HH:mm:ss FFFF", null);
            datetimeResult = DateTime.ParseExact(convertDatetime, "dd/MM/yyyy HH:mm:ss FFFF", null);
            return datetimeResult;
        }
        public static DateTime StringToDate (string convertDatetime,string datetimeFormat)
        {
            DateTime datetimeResult = new DateTime();
            datetimeResult = DateTime.ParseExact(convertDatetime, datetimeFormat, null);
            return datetimeResult;
        }

        public static bool IsNullDatetime(DateTime? datetimeValue)
        {
            if (datetimeValue.HasValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}
