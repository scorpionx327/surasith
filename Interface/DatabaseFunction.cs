﻿
using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common.Interface ;
namespace th.co.toyota.stm.fas.common.Interface
{
    public class DatabaseFunction
    {
        private th.co.toyota.stm.fas.common.DataConnection dbconn = null;
        public DatabaseFunction()
        {
            dbconn = new th.co.toyota.stm.fas.common.DataConnection(ConfigurationManager.GetAppSetting("ConnectionString"));
            if (!dbconn.TestConnection())
            {

                throw (new Exception("Cannot connect Database"));
            }
        }

        public void Bulk(string _localfilename, string _targetTable)
        {
            System.Text.StringBuilder _sb = new System.Text.StringBuilder();
            _sb.AppendFormat("BULK INSERT {0} ", _targetTable);
            _sb.AppendFormat("FROM '{0}' ", _localfilename);
            _sb.Append("WITH ( ");
            _sb.Append(" CODEPAGE = '65001', DATAFILETYPE = 'char',FIELDTERMINATOR = '|' ");
            _sb.Append(" )");

            Util.ConsoleWriteLine("Execute Bulk Script");
            Util.ConsoleWriteLine(_sb.ToString());

            SqlCommand _cmd = new SqlCommand(_sb.ToString());
            dbconn.ExecuteNonQuery(_cmd);

        }
        //public void InsertFileImportHis(string _batchID, string _fName, string _result, string _user)
        //{

        //    SqlCommand _cmd = new SqlCommand("sp_IMV3InsertFileImportHis");
        //    _cmd.CommandType = CommandType.StoredProcedure;
        //    _cmd.Parameters.Add("@FileType", _batchID);
        //    _cmd.Parameters.Add("@FileName", _fName);
        //    _cmd.Parameters.Add("@ImportResult", _result);
        //    _cmd.Parameters.Add("@CreateUser", _user);
        //    dbconn.ExecuteNonQuery(_cmd);

        //}
        private DataTable GetStructureTargetTable(string _targetTable)
        {
            SqlCommand _cmd = new SqlCommand();
            _cmd.CommandText = "SELECT COLUMN_NAME,CHARACTER_MAXIMUM_LENGTH AS MAX_LENGTH,CASE UPPER(IS_NULLABLE) WHEN 'YES' THEN 'Y' ELSE 'N' END AS REQ ";
            _cmd.CommandText += "FROM INFORMATION_SCHEMA.COLUMNS ";
            //Add 2019.12.03 AND COLUMN_NAME != 'ROW_INDX'
            _cmd.CommandText += "WHERE TABLE_NAME = @TableName AND COLUMN_NAME != 'ROW_INDX' ";
            _cmd.CommandText += "ORDER BY ORDINAL_POSITION "; // don't change
            _cmd.Parameters.AddWithValue("@TableName", _targetTable);
            DataTable _dt = dbconn.GetData(_cmd);
            return _dt;
        }

        public List<ReceivingFileColumn> StructureTable2Array(string _targetTable)
        {
            DataTable _dt = GetStructureTargetTable(_targetTable);

            List<ReceivingFileColumn> _ar = new List<ReceivingFileColumn>();
            for (int i = 0; i < _dt.Rows.Count; i++)
            {
                DataRow _dr = _dt.Rows[i];
                ReceivingFileColumn _fCol = new ReceivingFileColumn(i, string.Format("{0}", _dr["COLUMN_NAME"]));
                _fCol.Length = Convert.ToInt32(_dr["MAX_LENGTH"]);
                _fCol.AllowNull = string.Format("{0}", _dr["REQ"]) == "Y";
                _ar.Add(_fCol);
            }

            return _ar;

        }


        public void Delete(string _targetTable)
        {
            SqlCommand _cmd = new SqlCommand(string.Format("DELETE FROM [{0}]", _targetTable));
            dbconn.ExecuteNonQuery(_cmd);
        }
        
        public void Close()
        {
            dbconn.Close();
        }

        public string GetSystemConfig(string _category, string _sub_category, string _code)
        {
            string _sql = "SELECT VALUE FROM TB_M_SYSTEM WHERE CATEGORY = @Category and SUB_CATEGORY = @SubCategory and CODE = @Code";
            SqlCommand _cmd = new SqlCommand(_sql);
            _cmd.Parameters.AddWithValue("@Category", _category);
            _cmd.Parameters.AddWithValue("@SubCategory", _sub_category);
            _cmd.Parameters.AddWithValue("@Code", _code);
            object _obj = dbconn.Execute(_cmd);
            return string.Format("{0}", _obj);
        }
    }
}
