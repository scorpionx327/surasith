﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web;

namespace Hino.Common
{
    public class Download
    {
        public bool DownloadFile(DataTable dt, string keyLayout, string keyCopyTo, string configFileName, string fileLayoutSection,string fileNameDownload)
        {
            try
            {
                //Create file on server
                GenerateFile commonGenerateFile = new GenerateFile(configFileName, fileLayoutSection);
                commonGenerateFile.FileName = fileNameDownload;
                commonGenerateFile.SetSheetName = "Sheet1";
                commonGenerateFile.LoadFile();
                commonGenerateFile.DataSource = dt;
                commonGenerateFile.ExecuteExcel();
                commonGenerateFile.SaveExcel();
                commonGenerateFile.Close();

                #region #Copy file to target path#
                if (keyCopyTo != null && !keyCopyTo.Equals(string.Empty))
                {
                    string fileName = commonGenerateFile.FileName + ".xlsx"; //commonGenerateFile.FileType;
                    // Use Path class to manipulate file and directory paths.
                    string sourceFile = System.IO.Path.Combine(commonGenerateFile.DirectoryName, fileName);
                    string copyToPath = System.Configuration.ConfigurationSettings.AppSettings[keyCopyTo];
                    string destinationFile = System.IO.Path.Combine(copyToPath, fileName);

                    if (!copyToPath.Equals(string.Empty))
                    {
                        // To copy a folder's contents to a new location: Create a new target folder, if necessary.
                        if (!System.IO.Directory.Exists(copyToPath))
                        {
                            System.IO.Directory.CreateDirectory(copyToPath);
                        }
                        // To copy a file to another location and overwrite the destination file if it already exists.
                        System.IO.File.Copy(sourceFile, destinationFile, true);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }
    }
}