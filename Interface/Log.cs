﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BatchCommon.Database;
using BatchCommon.Config;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Hino.Common
{
    public class Log
    {
        private string _strConn = "";
        private DataConnection _db;

        public Log()
        {
            _strConn = ConfigurationSettings.AppSettings["ConnectionString"];
            //bConfig = new BatchConfig();
        }
        public bool InsertLog(LogDetail logDetail)
        {
            bool result = false;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return false;
                }
                object obj = new object();
                SqlCommand cmd = new SqlCommand("sp_Common_InsertLog");
                cmd.Parameters.Add(new SqlParameter("@APP_ID", SqlDbType.Int)).Value = logDetail.AppID;
                cmd.Parameters.Add(new SqlParameter("@STATUS", SqlDbType.Text)).Value = logDetail.Status;
                cmd.Parameters.Add(new SqlParameter("@MESSAGE_CODE", SqlDbType.Text)).Value = logDetail.MessageCode;
                cmd.Parameters.Add(new SqlParameter("@MESSAGE", SqlDbType.Text)).Value = logDetail.Message;
                cmd.Parameters.Add(new SqlParameter("@USER", SqlDbType.Text)).Value = logDetail.User;
                cmd.CommandType = CommandType.StoredProcedure;
                obj = _db.Execute(cmd);
                result = Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return result;
        }
    }

    public class LogDetail
    {
        private int _appID = 0;
        private string _status = null;
        private string _messageCode = null;
        private string _message = null;
        private string _user = null;

        public int AppID
        {
            set
            {
                _appID = value;
            }
            get { return _appID; }
        }
        public string Status
        {
            set
            {
                _status = value;
            }
            get { return _status; }
        }
        public string MessageCode
        {
            set
            {
                _messageCode = value;
            }
            get { return _messageCode; }
        }
        public string Message
        {
            set
            {
                _message = value;
            }
            get { return _message; }
        }
        public string User
        {
            set
            {
                _user = value;
            }
            get { return _user; }
        }
    }
}