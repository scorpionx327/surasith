﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using System.IO;
using th.co.toyota.stm.fas.common;
using System.Text.RegularExpressions;
using log4net;

namespace th.co.toyota.stm.fas.common.Interface
{
    public class ReceiveFileClass
    {
        protected static readonly ILog fLog = LogManager.GetLogger("Log4NetConfiguration");
        public ReceiveFileClass(string _configFileName, string _fileLayoutSection)
        {
            this.configFileName = _configFileName;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //
            // TODO: Add constructor logic here
            //
            log4net.Config.XmlConfigurator.Configure();

            string _DBServerSharePath = dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerSharePath");
            string _DBServerLocalPath = dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerLocalPath");

            if (Util.IsNullorEmpty(_DBServerSharePath))
                throw (new BatchLoggingException(CommonMessage.E_SYSCONFIG, "DBServerSharePath"));
            if (Util.IsNullorEmpty(_DBServerLocalPath))
                throw (new BatchLoggingException(CommonMessage.E_SYSCONFIG, "DBServerLocalPath"));
            if (Util.IsNullorEmpty(ConfigurationManager.GetAppSetting("BatchID")))
                throw (new BatchLoggingException(CommonMessage.E_CONFIG, "BatchID"));

      
            this.fileLayoutSection = string.Format("configuration/ReceivingFileCommonBatch/{0}", _fileLayoutSection);


            SetRecevingFileLayout( _fileLayoutSection);


            //check can access sharepath
            if (!clsFileLayout.BatchIsRunningOnDB && !System.IO.Directory.Exists(_DBServerSharePath))
            {
                throw (new BatchLoggingException(ReceivingBatch.E_ACCESS_DENIED, _DBServerSharePath));
            }

            this.Delete();

        }

        private ReceivingExcelFileLayout clsFileLayout = null;
        private string configFileName = null;
        private string fileLayoutSection = null;
        public string FullFileName = null;
        public string FileName
        {

            get
            {
                return new System.IO.FileInfo(this.FullFileName).Name;
            }
        }

		private DatabaseFunction dbconn = new DatabaseFunction();
		private ArrayList arErrorItem; // store error item
		private ArrayList arItemPerFile;
		private ArrayList arResult;

        public ArrayList ProcessResultList
        {
            get { return arResult; }
        }
        private string SuccessDirectory = string.Empty;
        public string SuccessPath
        {
            get
            {
                return SuccessDirectory;
            }
            set
            {
                this.SuccessDirectory = value;
            }
        }
        private string FailDirectory = string.Empty;
        public string FailPath
        {
            get
            {
                return FailDirectory;
            }
            set
            {
                this.FailDirectory = value;
            }
        }
        private string fileLocation = string.Empty;
        private string filePreFix = string.Empty;

        public string FileLocation
        {
            get { return fileLocation; }
            set
            {
                fileLocation = value;
                if (!System.IO.Directory.Exists(fileLocation))
                    throw (new BatchLoggingException(ReceivingBatch.E_DIR_NO_FOUND, fileLocation));
            }
        }
        public string FilePreFix
        {
            get { return filePreFix; }
            set { filePreFix = value; }
        }
        private void SetRecevingFileLayout( string _configSection)
        {
            clsFileLayout = new ReceivingExcelFileLayout();

            ConfigurationManager _cfg = new ConfigurationManager(this.configFileName);
            Hashtable _ht = _cfg.GetAttributeList(_configSection);

            clsFileLayout.SetFileLayoutType(Util.GetStringFromHash(_ht, "FileType"));
            

            if (!Util.GetStringFromHash(_ht, "FileType").Equals("Excel"))
            {
                clsFileLayout.SetHeader(Util.GetStringFromHash(_ht, "withHeader"));
                clsFileLayout.SetFooter(Util.GetStringFromHash(_ht, "withFooter"));

                if (clsFileLayout.HeaderFlag)
                    clsFileLayout.HeaderPrefix = Util.GetStringFromHash(_ht, "HeaderPrefix");
                if (clsFileLayout.FooterFlag)
                    clsFileLayout.FooterPrefix = Util.GetStringFromHash(_ht, "FooterPrefix");

                if (clsFileLayout.FileLayoutType == FileLayoutType.Split)
                {

                    string _s = Util.GetStringFromHash(_ht, "SplitText");
                    if (_s.Length == 0)
                    {
                        throw (new BatchLoggingException(ReceivingBatch.E_INVALID_SPLIT));
                    }
                    clsFileLayout.Splitchar = _s.ToCharArray(0, 1)[0];
                }
                else if (clsFileLayout.FileLayoutType == FileLayoutType.FixLength)
                {
                    clsFileLayout.SetDetailLength(Util.GetStringFromHash(_ht, "DetailLength"));
                }

            }

            if (_ht.ContainsKey("withCheckDuplicate"))
                clsFileLayout.SetCheckDuplicate(Util.GetStringFromHash(_ht, "withCheckDuplicate"));

            clsFileLayout.TargetTable = Util.GetStringFromHash(_ht, "TargetTable");

            if (_ht.ContainsKey("ParkingTable"))
                clsFileLayout.ParkingTable = Util.GetStringFromHash(_ht, "ParkingTable");

            clsFileLayout.StartRow = Convert.ToInt32(Util.GetStringFromHash(_ht, "StartRow"));

            if (!Util.IsNullorEmpty(_ht, "HeaderStart"))
            {
                clsFileLayout.SetHeaderStart = Convert.ToInt32(Util.GetStringFromHash(_ht, "HeaderStart"));
            }
            if (!Util.IsNullorEmpty(_ht, "BatchIsRunningOnDB"))
            {
                clsFileLayout.BatchIsRunningOnDB = Util.GetStringFromHash(_ht, "BatchIsRunningOnDB") == "True";
            }
            if (!Util.IsNullorEmpty(_ht, "FastTransferMode"))
            {
                clsFileLayout.FastTransferMode = Util.GetStringFromHash(_ht, "FastTransferMode") == "True";
            }
            
            ArrayList _ar = _cfg.GetNodesListAndAttribute(_configSection + "/ColumnValidation/Column");

           

            List<ReceivingFileColumn> _arDBCol = dbconn.StructureTable2Array(clsFileLayout.TargetTable);
            
            if (_arDBCol.Count == 0)
            {
                throw (new BatchLoggingException(ReceivingBatch.E_REQ_TARGET, clsFileLayout.TargetTable));
            }

          
            foreach (ReceivingFileColumn _fCol in _arDBCol)
            {
                foreach (Hashtable _htColumn in _ar)
                {
                    string _fName = Util.GetStringFromHash(_htColumn, "FieldName");
                    if (_fName == _fCol.FieldName) // Column Match
                    {
                        if (!Util.IsNullorEmpty(_htColumn, "Format"))
                            _fCol.Format = Util.GetStringFromHash(_htColumn, "Format");

                        if (!Util.IsNullorEmpty(_htColumn, "RegularExpression"))
                            _fCol.RegularExpression = Util.GetStringFromHash(_htColumn, "RegularExpression");

                        if (_htColumn.ContainsKey("ValueList"))
                        {
                            _fCol.ValueRawList = string.Format("{0}", _htColumn["ValueList"]);
                            _fCol.ValueList = new List<string>();
                            string[] str = _fCol.ValueRawList.Split('|');
                            foreach (var s in str)
                            {
                                _fCol.ValueList.Add(s);
                            }
                            //Support leave field is blank (not allow to input anything
                            if (str.Length == 0)
                            {
                                _fCol.ValueList.Add(string.Empty);
                            }
                        }



                        if (!Util.IsNullorEmpty(_htColumn, "DataType"))
                        {
                            string _dtype = Util.GetStringFromHash(_htColumn, "DataType");
                            if (_dtype.ToLower() == "integer")
                                _fCol.DataType = DataType.Integer;
                            else if (_dtype.ToLower() == "datetime")
                                _fCol.DataType = DataType.DateTime;
                        }

                        if (clsFileLayout.FileLayoutType == FileLayoutType.FixLength)
                        {
                            if (!Util.IsNullorEmpty(_htColumn, "StartPosition"))
                            {
                                _fCol.StartPosition = int.Parse(Util.GetStringFromHash(_htColumn, "StartPosition")); 
                            }
                        }
                            break;
                    }

                }
                if (_fCol.DataType == DataType.DateTime && Util.IsNullorEmpty(_fCol.Format))
                    throw (new BatchLoggingException(ReceivingBatch.E_REQ_FORMAT));

         

                clsFileLayout.AddColumn(_fCol);

            } // End of column

            ArrayList _arHeader = _cfg.GetNodesListAndAttribute(_configSection + "/HeaderSection/Header");
            foreach (Hashtable _htColumn in _arHeader)
            {
                string _row = Util.GetStringFromHash(_htColumn, "Row");
                string _col = Util.GetStringFromHash(_htColumn, "Column");
                string _txt = Util.GetStringFromHash(_htColumn, "Text");

                int[] _pos = new int[2] { Convert.ToInt32(_row), Convert.ToInt32(_col) };
                clsFileLayout.AddHeaderXCheckList(_pos, _txt);
            }

            //set default
            _ht = _cfg.GetChildNodes(_configSection);

            if (!Util.IsNullorEmpty(_ht, "DIR"))
                this.FileLocation = Util.GetStringFromHash(_ht, "DIR");
            else
                throw (new BatchLoggingException(CommonMessage.E_CONFIG, "DIR"));

            if (clsFileLayout.FileLayoutType == FileLayoutType.Split)
            {
                if (!Util.IsNullorEmpty(_ht, "FILEPREFIX"))
                    this.FilePreFix = Util.GetStringFromHash(_ht, "FILEPREFIX");
                else
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, "FILEPREFIX"));
            }

            if (clsFileLayout.FileLayoutType == FileLayoutType.FixLength)
            {
                if (!Util.IsNullorEmpty(_ht, "FILEPREFIX"))
                    this.FilePreFix = Util.GetStringFromHash(_ht, "FILEPREFIX");
                else
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, "FILEPREFIX"));
            }


            if (!Util.IsNullorEmpty(_ht, "Success"))
                this.SuccessDirectory = Util.GetStringFromHash(_ht, "Success");

            if (!Util.IsNullorEmpty(_ht, "Error"))
                this.FailDirectory = Util.GetStringFromHash(_ht, "Error");


        }

        /// <summary>
        /// Read All line into ArrayList (ListOf(String))
        /// </summary>
        /// <returns></returns>
        private ArrayList ReadFile()
        {
            //Check FileName not found
            
            fLog.InfoFormat("READ FILE => {0}", this.FileName);
            ArrayList _ar = new ArrayList();

            var pck = new OfficeOpenXml.ExcelPackage();
            using (System.IO.FileStream _fileStream = System.IO.File.OpenRead(this.FullFileName))
                pck = new OfficeOpenXml.ExcelPackage(_fileStream);
          
            var ws = pck.Workbook.Worksheets[1] ;
            
            //Check Header
            foreach (System.Collections.DictionaryEntry _item in clsFileLayout.HeaderXCheckList)
            {
                try
                {
                    int[] _pos = _item.Key as int[] ;

                    if (ws.Cells[_pos[0], _pos[1], _pos[0], _pos[1]].Text.Replace("\n"," ") != string.Format("{0}", _item.Value))
                    {
                        arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, _item.Value));
                        //return _ar ;
                    }
                }
                catch (Exception ex)
                {
                        arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_HEADER_ERROR, _item.Value,ex.Message));
                        return _ar ;
                }
            }
            if(arErrorItem.Count > 0)
                return _ar ;
           
            for (int i = clsFileLayout.StartRow; i <= ws.Dimension.End.Row; i++)
            {
                bool bBlankRow = true ;
                Hashtable _ht = new Hashtable();
                if (ws.Dimension.End.Column < clsFileLayout.ColumnList.Count)
                {
                    //Add Error
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LINE_MISSMATCH, i + clsFileLayout.StartRow));
                }
                var wsRow = ws.Cells[i, 1, i, clsFileLayout.ColumnList.Count];

                int _ttl = clsFileLayout.ColumnList.Count ;
                for(int _colIndx = 1;_colIndx <= _ttl;_colIndx++)
                {
                    try
                    {

                        ReceivingFileColumn _col = clsFileLayout.ColumnList[_colIndx - 1] ;
                        var cell = wsRow[i, _colIndx];
                        string _v = cell.Text;
                        if (_v == string.Empty || _v.StartsWith("."))
                        {
                            _v = string.Format("{0}", cell.Value);                          

                        }
                        if (!string.IsNullOrWhiteSpace(_v))
                            bBlankRow = false;

                        _ht.Add(_col.FieldName, _v);
                    }
                    catch (Exception ex)
                    {
                        //Add Error
                        arErrorItem.Add(new MessageResult(eLogLevel.Error, "Line@{0} is error => {1}", i + clsFileLayout.StartRow, ex.Message));
                        break; //go to new line
                    }
                }
                if (!bBlankRow)
                    _ar.Add(_ht);
                else
                    break;
            }
            fLog.InfoFormat("TOTAL {0} RECORD(S)", _ar.Count);
            pck.Dispose();
            
            return _ar;
        }

        private ArrayList ReadFileASCII()
        {
            fLog.InfoFormat("READFILEASCII FILE => {0}", this.FileName);
            ArrayList _ar = new ArrayList();
            const int iBufferSize = 128;
            using (System.IO.FileStream _fileStream = System.IO.File.OpenRead(this.FullFileName))
            // using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.ASCII, true, iBufferSize))
            // using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.GetEncoding(874), true, iBufferSize)) // for support thai language
            using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream,System.Text.Encoding.UTF8, true, iBufferSize))
            {
                string _line = string.Empty;
                while ((_line = _rd.ReadLine()) != null)
                {
                    if (_line == "\u0a0d") //Add By Surasith T. 2017-05-08 : BCT IS IS confirm to remove becuase this data is generated from HRMS
                        continue;

                    _ar.Add(_line);
                }
                _rd.Close();
            }
            fLog.InfoFormat("TOTAL {0} RECORD(S)", _ar.Count);
            return _ar;
        }


        private bool CheckFileFormat(ArrayList _ar)
        {

            bool bError = false;
            int _totalCol = clsFileLayout.ColumnList.Count;

           

            for (int i = 0; i < _ar.Count; i++)
            {
                Console.WriteLine("{0}  of {1}", i, _ar.Count);
                Hashtable _htItem = _ar[i] as Hashtable;

                int indxLine = i +clsFileLayout.StartRow;
                
                if (!this._CheckItemDataType(indxLine, _htItem))
                {
                    bError = true;
                    continue;
                }

                arItemPerFile.Add(_htItem);

            }

            return !bError;

        }

        //public void Reprocess()
        //{
        //    //Move Data from Parking to Stagging
        //    try
        //    {
        //        dbconn.Reprocess(clsFileLayout.TargetTable, clsFileLayout.ParkingTable);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex); //no move file
        //    }
        //}
        //private void ConvertOldExcelToNewVersion()
        //{
        //    //string file = @"\\192.168.104.201\SharePath\PLANI_BATCH\PL1B205\Error\PL1B205_20140813_180317.xls";
        //    var app = new Microsoft.Office.Interop.Excel.Application();
        //    var wb = app.Workbooks.Open(this.FullFileName);
        //    wb.SaveAs(Filename: this.FullFileName + "x", FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
        //    wb.Close();
        //    app.Quit();
            
        //    this.FullFileName = this.FullFileName + "x";
        //}
        private void Delete()
        {
            try
            {
                dbconn.Delete(clsFileLayout.TargetTable);
                //				Delete After Import Data 2013-09-30
                //				dbconn.Reprocess(clsFileLayout.TargetTable, clsFileLayout.ParkingTable) ;
            }
            catch (Exception ex)
            {
                throw (new BatchLoggingException(ex.Message));
            }
        }

        public void Execute()
        {
            arResult = new ArrayList();
            arErrorItem = new ArrayList();

            string[] _fs1 = System.IO.Directory.GetFiles(this.FileLocation, this.FilePreFix);

            // Sorting by update date---> 
            Hashtable _htf = new Hashtable();

            if (_fs1.Length.Equals(0))
            {
                this.Execute(this.FilePreFix);
            }
            else
            {
                foreach (string _s in _fs1)//user For  1 - - _fs1.count 
                {
                    try
                    {
                        //Check if zip File (Zipfiles / *.txt)
                        System.IO.FileInfo _f = new System.IO.FileInfo(_s);

                        if (!_htf.ContainsKey(_f.FullName))
                        {
                            fLog.DebugFormat("START EXECUTE FILE {0}", _f.FullName);
                            this.Execute(_f.FullName);
                            fLog.DebugFormat("END EXECUTE FILE {0}", _f.FullName);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }


            }
           

        
        }

        public void Execute(string _FileName)
        {
            arResult = new ArrayList();
			arErrorItem = new ArrayList();
            arItemPerFile = new ArrayList();
            try
            {
                this.FullFileName = System.IO.Path.Combine(this.FileLocation, _FileName) ;
                if (!System.IO.File.Exists(this.FullFileName))
                {
                    throw (new BatchLoggingException("File {0} not found", this.FullFileName));
                }
                /*
                 * Convert Xls to Xlsx -> On Upload screen
                 */
                
                //End of Add New

                if (string.IsNullOrEmpty(this.SuccessPath))
                {
                    DirectoryInfo _f = new DirectoryInfo(this.FileLocation);
                    this.SuccessPath = System.IO.Path.Combine(_f.Parent.FullName, "Success");
                }
                if (string.IsNullOrEmpty(this.FailPath))
                {
                    DirectoryInfo _f = new DirectoryInfo(this.FileLocation);
                    this.FailPath = System.IO.Path.Combine(_f.Parent.FullName, "Error");
                }
                //Add New By Surasith T. 08.09.2014 ---> Comment by Uten not support TMAP I/F File
                //if (!_FileName.ToLower().EndsWith(".xlsx"))
                //{
                //    throw (new BatchLoggingException(ReceivingBatch.E_EXCEL_VERSION_INVALID, _FileName));
                //} 

                this._Execute();
                //Add complete message
                arResult.Add(new MessageResult(eLogLevel.Information, ReceivingBatch.I_INSERT_STAGGING_SUCCESS, this.FileName));

                // 2019.12.03 No move file to success this step
                // Util.MoveFile(this.FullFileName, this.SuccessDirectory); //Need move after finish process

            }
            catch (BatchLoggingException ex)
            {
                arResult.Add(new MessageResult(eLogLevel.Error, ex.Message));
                Console.WriteLine(ex.Message);
                if (ex.arErrorList != null)
                {
                    arResult.AddRange(ex.arErrorList);
                }
                 Util.MoveFile(this.FullFileName, this.FailDirectory) ;
                throw (ex);
            }
            catch(Exception ex){
                fLog.Error(ex);
                Util.MoveFile(this.FullFileName, this.FailDirectory);
                throw (ex);
            }
            
        }

        public void ExecuteExcelFile(string _FileName)
        {
            arResult = new ArrayList();
            arErrorItem = new ArrayList();
            arItemPerFile = new ArrayList();
            try
            {
                this.FullFileName = System.IO.Path.Combine(this.FileLocation, _FileName);
                if (!System.IO.File.Exists(this.FullFileName))
                {
                    throw (new BatchLoggingException("File {0} not found", this.FullFileName));
                }
                /*
                 * Convert Xls to Xlsx -> On Upload screen
                 */

                //End of Add New

                if (string.IsNullOrEmpty(this.SuccessPath))
                {
                    DirectoryInfo _f = new DirectoryInfo(this.FileLocation);
                    this.SuccessPath = System.IO.Path.Combine(_f.Parent.FullName, "Success");
                }
                if (string.IsNullOrEmpty(this.FailPath))
                {
                    DirectoryInfo _f = new DirectoryInfo(this.FileLocation);
                    this.FailPath = System.IO.Path.Combine(_f.Parent.FullName, "Error");
                }
                //Add New By Surasith T. 08.09.2014 ---> Comment by Uten not support TMAP I/F File
                //if (!_FileName.ToLower().EndsWith(".xlsx"))
                //{
                //    throw (new BatchLoggingException(ReceivingBatch.E_EXCEL_VERSION_INVALID, _FileName));
                //} 

                this._ExecuteExcel();
                //Add complete message
                arResult.Add(new MessageResult(eLogLevel.Information, ReceivingBatch.I_INSERT_STAGGING_SUCCESS, this.FileName));
              //  Util.MoveFile(this.FullFileName, this.SuccessDirectory); //Need move after finish process

            }
            catch (BatchLoggingException ex)
            {
                arResult.Add(new MessageResult(eLogLevel.Error, ex.Message));
                Console.WriteLine(ex.Message);
                if (ex.arErrorList != null)
                {
                    arResult.AddRange(ex.arErrorList);
                }
                Util.MoveFile(this.FullFileName, this.FailDirectory);
                throw (ex);
            }
            catch (Exception ex)
            {
                Util.MoveFile(this.FullFileName, this.FailDirectory);
                throw (ex);
            }

        }
        private void _ExecuteExcel()
        {

            ArrayList _ar = this.ReadFile ();

            int TotalRowsOfAsscii = _ar.Count;

            if (clsFileLayout.HeaderFlag && _ar.Count > 0)
            {
                if (!string.Format("{0}", _ar[0]).StartsWith(clsFileLayout.HeaderPrefix)) //first line
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, clsFileLayout.HeaderPrefix)));

                _ar.RemoveAt(0);
            }
            if (clsFileLayout.FooterFlag && _ar.Count > 0)
            {
                if (!string.Format("{0}", _ar[_ar.Count - 1]).StartsWith(clsFileLayout.FooterPrefix)) //last line
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_PREFIX_MISSMATCH, clsFileLayout.FooterPrefix)));
                }
                //Check row count
                string strTotalRowsOfAsscii = TotalRowsOfAsscii.ToString("0000000");
                string strTotalRowsOnTailer = _ar[_ar.Count - 1].ToString().Substring(3, 7);
                if (!strTotalRowsOfAsscii.Equals(strTotalRowsOnTailer))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_TOTALCOUNT_MISSMATCH, this.FileName)));
                }
                _ar.RemoveAt(_ar.Count - 1);
            }

            //Add New 08.09.2014 By Surasith T.
            if (arErrorItem.Count > 0)
            {
                throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
            }
            //End of Add New


            if (_ar.Count == 0)
            {
                //log no data found
                throw (new BatchLoggingException(ReceivingBatch.E_NO_DATA, this.FileName));
            }

            //Check Line Duplicate 2013-10-18
            if (clsFileLayout.CheckDuplicate && CheckDuplicateInFile(_ar))
            {
                throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_DUP_FILE, this.FileName), arErrorItem));
            }
            if (clsFileLayout.FileLayoutType == FileLayoutType.Excel && !CheckFileFormat(_ar))
            {
                throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
            }


            fLog.DebugFormat("EXEC START");

            //update db		
            string _bulkFileName = string.Empty;

            string _batchID = ConfigurationManager.GetAppSetting("BatchID"); //already check on run

            string _networkPath = System.IO.Path.Combine(dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerSharePath"), _batchID);
            string _dbLocalPath = System.IO.Path.Combine(dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerLocalPath"), _batchID);

            var _p = clsFileLayout.BatchIsRunningOnDB ? _dbLocalPath : _networkPath;

            try
            {
                
                _bulkFileName = GenerateBulkFile(_p);
                dbconn.Bulk(System.IO.Path.Combine(_dbLocalPath, _bulkFileName), clsFileLayout.TargetTable);
            }
            catch (Exception ex)
            {
                fLog.Error(ex);
                throw (ex);
            }
            finally
            {
                //delete bulk file
                Util.DeleteFile(System.IO.Path.Combine(_p, _bulkFileName), false);
                //clean old file
                foreach (string _fName in System.IO.Directory.GetFiles(_p))
                {
                    System.IO.FileInfo _f = new System.IO.FileInfo(_fName);
                    TimeSpan _ts = System.DateTime.Now.Subtract(_f.LastWriteTime);
                    if (_ts.TotalHours > 10)
                    {
                        Util.DeleteFile(_fName, false);
                    }
                }
            }
            fLog.DebugFormat("EXEC END");

        }

        public void MoveFile(eLogLevel _success)
        {
            if (System.IO.File.Exists(this.FullFileName))
            {
                System.IO.File.SetLastWriteTime(this.FullFileName, DateTime.Now); //2019.12.06
            }
            switch (_success)
            {
                case eLogLevel.Information:
                    Util.MoveFile(this.FullFileName, this.SuccessDirectory);
                    break;
                default:
                    Util.MoveFile(this.FullFileName, this.FailDirectory);
                    break;
            }

        }

        private void _Execute()
        {

            ArrayList _ar = this.ReadFileASCII();


            if(clsFileLayout.FastTransferMode) //all validation execute by store procedure (except length)
                _ExecuteLargeFile(_ar);
            else //all validate by common
                _ExecuteSmallFile(_ar);
            
        }

        private void _ExecuteLargeFile(ArrayList _ar)
        {
            fLog.DebugFormat("_ExecuteLargeFile");
            int TotalRowsOfAsscii = _ar.Count;

            if (clsFileLayout.HeaderFlag && _ar.Count > 0)
            {
                if (clsFileLayout.SetHeaderStart > 0)
                {
                    string strHeader = string.Format("{0}", _ar[0]).Substring(clsFileLayout.SetHeaderStart, _ar[0].ToString().Length - clsFileLayout.SetHeaderStart);

                    if (!string.Format("{0}", strHeader).StartsWith(clsFileLayout.HeaderPrefix)) //first line
                        throw (new BatchLoggingException(string.Format(ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, clsFileLayout.HeaderPrefix)));
                }
                else
                {
                    if (!string.Format("{0}", _ar[0]).StartsWith(clsFileLayout.HeaderPrefix)) //first line
                        throw (new BatchLoggingException(string.Format(ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, clsFileLayout.HeaderPrefix)));
                }

                _ar.RemoveAt(0);
            }
            if (clsFileLayout.FooterFlag && _ar.Count > 0)
            {
                if (!string.Format("{0}", _ar[_ar.Count - 1]).StartsWith(clsFileLayout.FooterPrefix)) //last line
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_PREFIX_MISSMATCH, clsFileLayout.FooterPrefix)));
                }
                //Check row count
                string strTotalRowsOfAsscii = TotalRowsOfAsscii.ToString("0000000");
                string strTotalRowsOnTailer = _ar[_ar.Count - 1].ToString().Substring(3, 7);
                if (!strTotalRowsOfAsscii.Equals(strTotalRowsOnTailer))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_TOTALCOUNT_MISSMATCH, this.FileName)));
                }
                _ar.RemoveAt(_ar.Count - 1);
            }

            //Add New 08.09.2014 By Surasith T.
            if (arErrorItem.Count > 0)
            {
                throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
            }
            //End of Add New


            if (_ar.Count == 0)
            {
                //log no data found
                throw (new BatchLoggingException(ReceivingBatch.E_NO_DATA, this.FileName));
            }
            //Check Line Duplicate 2013-10-18
            if (clsFileLayout.CheckDuplicate)
            {
                fLog.DebugFormat("No check CheckDuplicate");
            }

            if (clsFileLayout.FileLayoutType == FileLayoutType.Excel)
            {
                fLog.DebugFormat("No Support CheckFileFormat Excel");
            }

            if (clsFileLayout.FileLayoutType == FileLayoutType.FixLength)
            {
                fLog.DebugFormat("CheckFormatFixLength");
                //check format
                if (!this._CheckFormatFixLengthBigFile(_ar))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
                }
            }
            else
            {
                fLog.DebugFormat("Support Only FixLength Format");
                throw (new Exception("Support Only FixLength Format"));
            }

            fLog.DebugFormat("EXEC START");

            //update db		
            string _bulkFileName = string.Empty;

            string _batchID = ConfigurationManager.GetAppSetting("BatchID"); //already check on run

            string _networkPath = System.IO.Path.Combine(dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerSharePath"), _batchID);
            string _dbLocalPath = System.IO.Path.Combine(dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerLocalPath"), _batchID);

            var _p = clsFileLayout.BatchIsRunningOnDB ? _dbLocalPath : _networkPath;

            try
            {
                

                fLog.DebugFormat("Start Generate BulkFile");
                _bulkFileName = GenerateBulkFile(_ar, _p);
                dbconn.Bulk(System.IO.Path.Combine(_dbLocalPath, _bulkFileName), clsFileLayout.TargetTable);
                fLog.DebugFormat("End Generate BulkFile");
            }
            catch (Exception ex)
            {
                fLog.Error(ex);
                throw (ex);
            }
            finally
            {
                //delete bulk file
                Util.DeleteFile(System.IO.Path.Combine(_p, _bulkFileName), false);
                //clean old file
                foreach (string _fName in System.IO.Directory.GetFiles(_p))
                {
                    System.IO.FileInfo _f = new System.IO.FileInfo(_fName);
                    TimeSpan _ts = System.DateTime.Now.Subtract(_f.LastWriteTime);
                    if (_ts.TotalHours > 10)
                    {
                        Util.DeleteFile(_fName, false);
                    }
                }
            }
            fLog.DebugFormat("EXEC END");

        }
        private void _ExecuteSmallFile(ArrayList _ar)
        {

            fLog.DebugFormat("_ExecuteSmallFile");
            int TotalRowsOfAsscii = _ar.Count;

            if (clsFileLayout.HeaderFlag && _ar.Count > 0)
            {
                if (clsFileLayout.SetHeaderStart > 0)
                {
                    string strHeader = string.Format("{0}", _ar[0]).Substring(clsFileLayout.SetHeaderStart, _ar[0].ToString().Length - clsFileLayout.SetHeaderStart);

                    if (!string.Format("{0}", strHeader).StartsWith(clsFileLayout.HeaderPrefix)) //first line
                        throw (new BatchLoggingException(string.Format(ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, clsFileLayout.HeaderPrefix)));
                }
                else
                {
                    if (!string.Format("{0}", _ar[0]).StartsWith(clsFileLayout.HeaderPrefix)) //first line
                        throw (new BatchLoggingException(string.Format(ReceivingBatch.E_HEADER_PREFIX_MISSMATCH, clsFileLayout.HeaderPrefix)));
                }

                _ar.RemoveAt(0);
            }
            if (clsFileLayout.FooterFlag && _ar.Count > 0)
            {
                if (!string.Format("{0}", _ar[_ar.Count - 1]).StartsWith(clsFileLayout.FooterPrefix)) //last line
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_PREFIX_MISSMATCH, clsFileLayout.FooterPrefix)));
                }
                //Check row count
                string strTotalRowsOfAsscii = TotalRowsOfAsscii.ToString("0000000");
                string strTotalRowsOnTailer = _ar[_ar.Count - 1].ToString().Substring(3, 7);
                if (!strTotalRowsOfAsscii.Equals(strTotalRowsOnTailer))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_FOOTER_TOTALCOUNT_MISSMATCH, this.FileName)));
                }
                _ar.RemoveAt(_ar.Count - 1);
            }

            //Add New 08.09.2014 By Surasith T.
            if (arErrorItem.Count > 0)
            {
                throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
            }
            //End of Add New


            if (_ar.Count == 0)
            {
                //log no data found
                throw (new BatchLoggingException(ReceivingBatch.E_NO_DATA, this.FileName));
            }
            //Check Line Duplicate 2013-10-18
            if (clsFileLayout.CheckDuplicate)
            {
                fLog.DebugFormat("CheckDuplicate");
                if (CheckDuplicateInFileASCII(_ar)) //CheckDuplicateInFile(_ar))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_DUP_FILE, this.FileName), arErrorItem));
                }
            }

            if (clsFileLayout.FileLayoutType == FileLayoutType.Excel)
            {
                fLog.DebugFormat("CheckFileFormat Excel");
                if (!CheckFileFormat(_ar))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
                }
            }

            if (clsFileLayout.FileLayoutType == FileLayoutType.FixLength)
            {
                fLog.DebugFormat("CheckFormatFixLength");
                //check format
                if (!this._CheckFormatFixLength(_ar))
                {
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
                }
            }
            else if (clsFileLayout.FileLayoutType == FileLayoutType.Split)
            {
                fLog.DebugFormat("CheckFormatSplit");
                if (!this._CheckFormatSplit(_ar))
                    throw (new BatchLoggingException(string.Format(ReceivingBatch.E_DATA_MISSMATCH, this.FileName), arErrorItem));
            }

            fLog.DebugFormat("EXEC START");

            //update db		
            string _bulkFileName = string.Empty;

            string _batchID = ConfigurationManager.GetAppSetting("BatchID"); //already check on run

            string _networkPath = System.IO.Path.Combine(dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerSharePath"), _batchID);
            string _dbLocalPath = System.IO.Path.Combine(dbconn.GetSystemConfig("COMMON", "UPLOAD", "DBServerLocalPath"), _batchID);

            var _p = clsFileLayout.BatchIsRunningOnDB ? _dbLocalPath : _networkPath;

            try
            {
                fLog.InfoFormat("Start Generate BulkFile");
                _bulkFileName = GenerateBulkFile(_p);
                dbconn.Bulk(System.IO.Path.Combine(_dbLocalPath, _bulkFileName), clsFileLayout.TargetTable);
                fLog.InfoFormat("End Generate BulkFile");
            }
            catch (Exception ex)
            {
                fLog.Error(ex);
                throw (ex);
            }
            finally
            {
                //delete bulk file
                Util.DeleteFile(System.IO.Path.Combine(_p, _bulkFileName), false);
                //clean old file
                foreach (string _fName in System.IO.Directory.GetFiles(_p))
                {
                    System.IO.FileInfo _f = new System.IO.FileInfo(_fName);
                    TimeSpan _ts = System.DateTime.Now.Subtract(_f.LastWriteTime);
                    if (_ts.TotalHours > 10)
                    {
                        Util.DeleteFile(_fName, false);
                    }
                }
            }
            fLog.DebugFormat("EXEC END");

        }
        private bool _CheckFormatFixLength(ArrayList _ar)
        {

            bool bError = false;
            int _totalLength = clsFileLayout.DetailLength;

            int _limit = 10000;

            for (int i = 0; i < _ar.Count; i++)
            {
                _limit++;
                if (_limit >= 10000)
                {
                    fLog.DebugFormat("{0}/{1}", i, _ar.Count);
                    _limit = 0;
                }

                int indxLine = i + (clsFileLayout.HeaderFlag ? 1 : 0) + 1;
                string _s = string.Format("{0}", _ar[i]);
                if (_s.Length != _totalLength)
                {
                    bError = true;
                    //add log
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_MISSMATCH_LENGTH, indxLine));
                    continue;
                }

                var _colLength = _s.Remove(_s.Length - 1, 1).Split('|');
                if(_colLength.Length != clsFileLayout.ColumnList.Count)
                {
                    bError = true;
                    //add log
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LINE_MISSMATCH, indxLine));
                    continue;
                }

                //convert
                Hashtable _htItem = this.ConvertFixLength(_s);
                if (!this._CheckItemDataType(indxLine, _htItem))
                {
                    bError = true;
                    continue;
                }
                arItemPerFile.Add(_htItem);
            }

            return !bError;

        }

        private bool _CheckFormatFixLengthBigFile(ArrayList _ar)
        {

            bool bError = false;
            int _totalLength = clsFileLayout.DetailLength;
            int _limit = 0;
            for (int i = 0; i < _ar.Count; i++)
            {
                _limit++;
                if (_limit >= 10000)
                {
                    fLog.DebugFormat("{0}/{1}", i+1, _ar.Count);
                    _limit = 0;
                }

                int indxLine = i + (clsFileLayout.HeaderFlag ? 1 : 0) + 1;
                string _s = string.Format("{0}", _ar[i]);
                if (_s.Length != _totalLength)
                {
                    bError = true;
                    //add log
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_MISSMATCH_LENGTH, indxLine));
                    continue;
                }

                var _colData = _s.Remove(_s.Length - 1, 1).Split('|');
                if (_colData.Length != clsFileLayout.ColumnList.Count)
                {
                    bError = true;
                    //add log
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LINE_MISSMATCH, indxLine));
                    continue;
                }

                //Check RequiredFields
                
                for(int _colIndx = 0; _colIndx < clsFileLayout.ColumnList.Count; _colIndx ++)
                {
                    ReceivingFileColumn _col = clsFileLayout.ColumnList[_colIndx];
                    if (_col.AllowNull)
                        continue;

                    string _tmp = string.Format("{0}", _colData[_colIndx]).Trim() ;
                    if (_tmp == string.Empty)
                    {
                        fLog.ErrorFormat(ReceivingBatch.E_REQ_COLUMNS_DB, i + 1, _col.FieldName);
                        arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_REQ_COLUMNS_DB, i+1, _col.FieldName));
                        bError = true ;
                        continue;
                    }
                }
            }

            return !bError;

        }
        
        private Hashtable _ConvertSplit(string[] _txt)
        {
            Hashtable _ht = new Hashtable();

            foreach (ReceivingFileColumn _col in clsFileLayout.ColumnList)
            {
                _ht.Add(_col.FieldName, _txt[_col.Sequence].Trim());
            }
            return _ht;
        }

        private Hashtable ConvertFixLength(string _txt)
        {
            Hashtable _ht = new Hashtable();
            foreach (ReceivingFileColumn _col in clsFileLayout.ColumnList)
            {
                string _tmp = _txt.Substring(_col.StartPosition-1, _col.Length);//Test
                _ht.Add(_col.FieldName, _tmp); //.Trim() not trim
            }

            return _ht;
        }
        private bool _CheckFormatSplit(ArrayList _ar)
        {

            bool bError = false;
            int _totalCol = clsFileLayout.ColumnList.Count;
            for (int i = 0; i < _ar.Count; i++)
            {
                Console.WriteLine("{0}  of {1}", i, _ar.Count);
                int indxLine = i + (clsFileLayout.HeaderFlag ? 1 : 0) + 1;

                string[] _s = null;

                if(string.Format("{0}", _ar[i]).EndsWith("|"))
                {
                    _s = string.Format("{0}", _ar[i].ToString().Substring(0, _ar[i].ToString().Length - 1)).Split(clsFileLayout.Splitchar);
                }
                else
                {
                    _s = string.Format("{0}", _ar[i]).Split(clsFileLayout.Splitchar);
                }

                if (_s.Length != _totalCol)
                {
                    bError = true;
                    //add log
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_COL_COUNT_MISS, i + (clsFileLayout.HeaderFlag ? 1 : 0)));
                    continue;
                }
                //convert
                Hashtable _htItem = this._ConvertSplit(_s);
                if (!this._CheckItemDataType(indxLine, _htItem))
                {
                    bError = true;
                    continue;
                }

                if (bError) //no need to convert item
                    continue;
                arItemPerFile.Add(_htItem);

            }

            return !bError;

        }


        
        private bool _CheckItemDataType(int indx, Hashtable _ht)
        {
            bool _bResult = true;
            foreach (ReceivingFileColumn _col in clsFileLayout.ColumnList)
            {
                string _tmp = string.Format("{0}", _ht[_col.FieldName]);
                if (_tmp == string.Empty && !_col.AllowNull)
                {
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_REQ_COLUMNS_DB, indx, _col.FieldName));
                    _bResult = false;
                    continue;
                }
                if (_tmp.Length > _col.Length)
                {
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_REQ_COLUMNS_LENGTH, indx, _col.FieldName));
                    _bResult = false;
                    continue;
                }

                if (_tmp != string.Empty && _col.RegularExpression != string.Empty)
                {
                    if (!Regex.Match(_tmp, _col.RegularExpression).Success)
                    {
                        arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_COL_REQEXPRESS, indx, _col.FieldName, _col.RegularExpression));
                        _bResult = false;
                        continue;
                    }
                }

                //Add by Surasith T. 2018-02-11
                if (_tmp != string.Empty &&  _col.ValueList.Count > 0)
                {
                    if (_col.ValueList.FindAll(x => x == _tmp).Count == 0)
                    {
                        if (_col.ValueList.Count == 1 && string.IsNullOrEmpty(_col.ValueList[0]))
                            arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LEAVE_BANK, indx, _col.FieldName, _tmp));
                        else if (_col.ValueList.Count == 1)
                            arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_NOT_IN_LIST_1_VAL, indx, _col.FieldName, _col.ValueRawList, _tmp));
                        else
                            arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_NOT_IN_LIST, indx, _col.FieldName, _col.ValueRawList.Replace("|", ", "), _tmp));

                        _bResult = false;
                        continue;
                    }
                }

                if (_col.DataType == DataType.Varchar)
                    continue;

                if (_tmp != string.Empty && _col.DataType == DataType.Integer && !Util.IsInt(_tmp))
                {
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_COL_INVALID_TYPE, indx, _col.FieldName));
                    _bResult = false;
                    continue;
                }
                if (_tmp != string.Empty && _col.DataType == DataType.DateTime && !Util.IsCorrectDateTimeFormat(_tmp, _col.Format))
                {
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_COL_INVALID_TYPE, indx, _col.FieldName));
                    _bResult = false;
                    continue;
                }

            }

            return _bResult;
        }

        private string GenerateBulkFile(ArrayList _LargeData, string _targetPath)
        {
            List<ReceivingFileColumn> _arDBColumn = dbconn.StructureTable2Array(clsFileLayout.TargetTable);
            System.IO.StreamWriter sw = null;
            string _tmpF = System.IO.Path.GetTempFileName();
            // sw = new System.IO.StreamWriter(_tmpF, false, System.Text.Encoding.ASCII);
            //sw = new System.IO.StreamWriter(_tmpF, false, System.Text.Encoding.GetEncoding(874)); // for support thai language
            //int _cnt = 0, _accum = 0;
            int _recCnt = clsFileLayout.StartRow;
            sw = new System.IO.StreamWriter(_tmpF, false, System.Text.Encoding.UTF8);
            foreach(string _s in _LargeData)
            {
                //_cnt++;
                //_accum++;
                //if (_cnt == 10000)
                //{
                //    _cnt = 0;
                //    fLog.DebugFormat("Write {0}", _accum);
                //}

                string[] _ss = _s.Remove(_s.Length - 1, 1).Split('|');
                StringBuilder _sb = new StringBuilder();
                _sb.AppendFormat("{0}", _recCnt);
                for (int i = 0; i < _ss.Length; i++)
                {
                    _sb.AppendFormat("|{0}", _ss[i].Trim());
                }
                sw.WriteLine(_sb);
                _recCnt++;
                //sw.WriteLine(_s.Remove(_s.Length - 1, 1));
            }
            fLog.DebugFormat("Generate Temp File Finish");
            //foreach (Hashtable _ht in arItemPerFile) //line
            //{
            //    System.Text.StringBuilder _sb = new System.Text.StringBuilder();

            //    foreach (ReceivingFileColumn _fCol in _arDBColumn)
            //    {
            //        _sb.AppendFormat("{0}|", _ht[_fCol.FieldName]);
            //    }
            //    _sb.Remove(_sb.Length - 1, 1);
            //    sw.WriteLine(_sb.ToString());
            //}
            sw.Close();
            sw = null;

            return Util.MoveFile(_tmpF, _targetPath);

        }


        private string GenerateBulkFile(string _targetPath)
        {
            List<ReceivingFileColumn> _arDBColumn = dbconn.StructureTable2Array(clsFileLayout.TargetTable);
            System.IO.StreamWriter sw = null;
            string _tmpF = System.IO.Path.GetTempFileName();
            // sw = new System.IO.StreamWriter(_tmpF, false, System.Text.Encoding.ASCII);
            //sw = new System.IO.StreamWriter(_tmpF, false, System.Text.Encoding.GetEncoding(874)); // for support thai language
            sw = new System.IO.StreamWriter(_tmpF, false, System.Text.Encoding.UTF8);
            int _recCnt = clsFileLayout.StartRow ;
            foreach (Hashtable _ht in arItemPerFile) //line
            {
                
                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                _sb.AppendFormat("{0}", _recCnt); //Add Row Count
                foreach (ReceivingFileColumn _fCol in _arDBColumn)
                {
                    _sb.AppendFormat("|{0}", _ht[_fCol.FieldName]);
                }
                //_sb.Remove(_sb.Length - 1, 1);
                sw.WriteLine(_sb.ToString());
                _recCnt++;
            }
            sw.Close();
            sw = null;
            fLog.DebugFormat("Generate Temp File Finish");
            return Util.MoveFile(_tmpF, _targetPath);

        }

        private string HashTableToString(Hashtable _ht,string[] sListOfKey)
        {
            string _rs = string.Empty;
            foreach (string s in sListOfKey)
            {
                if (_ht.ContainsKey(s))
                {
                    _rs = string.Format("{0}|{1}", _rs, _ht[s]);
                }

            }
            return _rs;
        }

        public bool CheckDuplicateInFileASCII(ArrayList _ar)
        {
            Hashtable _htCompare = new Hashtable();
            ArrayList _arDup = new ArrayList();
            bool _b = false;
            for (int i = 0; i < _ar.Count; i++)
            {
                string _txt = string.Format("{0}", _ar[i]).Trim();
                string _s = (_txt.Length >= 10 ? _txt.Substring(0, 10) : _txt);

                if (_htCompare.ContainsKey(_s))
                {
                    string _iLst = string.Format("{0}", _htCompare[_s]);
                    _htCompare[_s] = _iLst + "|" + i.ToString();

                    int _cnt = _iLst.Split('|').Length;
                    if (_cnt == 1)
                    {
                        int _index = Convert.ToInt32(_iLst.Split('|')[0]);
                        _arDup.Add(new DuplicateLineDataClass(_index, string.Format("{0}", _ar[_index])));
                    }
                    _arDup.Add(new DuplicateLineDataClass(i, _txt));
                    _b = true; // first check is duplicate
                    continue;
                }
                _htCompare.Add(_s, i.ToString());

            }
            if (!_b) return false; // first check not found => No need to check again

            _htCompare = new Hashtable();
            _b = false;
            //Hashtable _ht = new Hashtable() ;
            ArrayList _arListOfLineDup = new ArrayList();

            foreach (DuplicateLineDataClass _d in _arDup) //full content check
            {
                if (_htCompare.ContainsKey(_d.Text))
                {
                    int _minSeq = Convert.ToInt32(_htCompare[_d.Text]);
                    _d.minSeqGroup = _minSeq;
                    _b = true;
                    continue;
                }
                _d.minSeqGroup = _d.Seq;
                _htCompare.Add(_d.Text, _d.Seq);
            }
            if (!_b) return false; // full check not found => No need to check again

            //add error
            _htCompare = new Hashtable();
            foreach (DuplicateLineDataClass _d in _arDup) //full content check
            {
                if (!_htCompare.ContainsKey(_d.minSeqGroup))
                {
                    _htCompare.Add(_d.minSeqGroup, string.Empty);
                }
                string _ss = string.Format("{0}", _htCompare[_d.minSeqGroup]);
                _htCompare[_d.minSeqGroup] = _ss + (_ss == string.Empty ? "" : ",") + _d.Seq.ToString();
            }

            SortedList _st = new SortedList(_htCompare);
            foreach (string _sDup in _st.Values)
            {
                if (_sDup.IndexOf(",") >= 0)
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LINE_DUP, _sDup));
            }


            return true;
        }
         
        public bool CheckDuplicateInFile(ArrayList _ar) /// แก้ใหม่ โดย Suphachai L. วันที่ 10-10-2014
        {
            bool _valueReturn = false;
            string[] _sList = null;
            Hashtable _htCompare = new Hashtable();
            ArrayList _arDup = new ArrayList();
            for (int i = 0; i < _ar.Count; i++)
            {
                if (i == 0)
                {
                    _sList = new string[(_ar[i] as Hashtable).Count];
                    int j = 0;
                    foreach (string _ss in (_ar[i] as Hashtable).Keys)
                    {
                        _sList[j] = _ss;
                        j++;
                    }
                }
                string _txt = HashTableToString(_ar[i] as Hashtable, _sList);
                string _s = _txt; 

                if (_htCompare.ContainsKey(_s))
                {
                    string _iLst = string.Format("{0}", _htCompare[_s]);
                    _htCompare[_s] = _iLst + "|" + (i+1).ToString();

                    _arDup.Add(new DuplicateLineDataClass(i + (clsFileLayout.HeaderFlag ? 1 : 0) + 1, _txt));
                    _valueReturn = true; 
                    continue;
                }
                _htCompare.Add(_s, (i + 1).ToString());

            }
            if (!_valueReturn) return false; 

            SortedList _st = new SortedList(_htCompare);
            foreach (string _sDup in _st.Values)
            {
                if (_sDup.IndexOf("|") > 0)
                 arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LINE_DUP, _sDup.Replace("|"," and ")));
            }
            return _valueReturn;
        }

        public bool CheckDuplicateInFile_backup(ArrayList _ar) /// backup ของเก่า
        {
            string[] _sList = null ;
            Hashtable _htCompare = new Hashtable();
            ArrayList _arDup = new ArrayList();
            bool _b = false;
            for (int i = 0; i < _ar.Count; i++)
            {
                if(i==0){
                    _sList = new string[(_ar[i] as Hashtable).Count]  ;
                    int j = 0;
                    foreach (string _ss in (_ar[i] as Hashtable).Keys)
                    {
                        _sList[j] = _ss;
                        j++;
                    }
                }
                string _txt = HashTableToString(_ar[i] as Hashtable,_sList);
                string _s = _txt; // (_txt.Length >= 40 ? _txt.Substring(0, 40) : _txt);

                _htCompare.Add(_s, i.ToString());
                if (_htCompare.ContainsKey(_s))
                {
                    string _iLst = string.Format("{0}", _htCompare[_s]);
                    _htCompare[_s] = _iLst + "|" + i.ToString();

                    //int _cnt = _iLst.Split('|').Length;
                    //if (_cnt == 1)
                    //{
                    //    int _index = Convert.ToInt32(_iLst.Split('|')[0]);
                    //    _arDup.Add(new DuplicateLineDataClass(_index + (clsFileLayout.HeaderFlag ? 1 : 0) + 1, string.Format("{0}", _ar[_index])));
                    //}
                    _arDup.Add(new DuplicateLineDataClass(i + (clsFileLayout.HeaderFlag ? 1 : 0) + 1, _txt));
                    _b = true; // first check is duplicate
                    continue;
                }
                

            }
            if (!_b) return false; // first check not found => No need to check again

            _htCompare = new Hashtable();
            _b = false;
            //Hashtable _ht = new Hashtable() ;
            ArrayList _arListOfLineDup = new ArrayList();

            foreach (DuplicateLineDataClass _d in _arDup) //full content check
            {
                if (_htCompare.ContainsKey(_d.Text))
                {
                    int _minSeq = Convert.ToInt32(_htCompare[_d.Text]);
                    _d.minSeqGroup = _minSeq;
                    _b = true;
                    continue;
                }
                _d.minSeqGroup = _d.Seq;
                _htCompare.Add(_d.Text, _d.Seq);
            }
            if (!_b) return false; // full check not found => No need to check again

            //add error
            _htCompare = new Hashtable();
            foreach (DuplicateLineDataClass _d in _arDup) //full content check
            {
                if (!_htCompare.ContainsKey(_d.minSeqGroup))
                {
                    _htCompare.Add(_d.minSeqGroup, string.Empty);
                }
                string _ss = string.Format("{0}", _htCompare[_d.minSeqGroup]);
                _htCompare[_d.minSeqGroup] = _ss + (_ss == string.Empty ? "" : "|") + _d.Seq.ToString();
            }

            SortedList _st = new SortedList(_htCompare);
            foreach (string _sDup in _st.Values)
            {
                if (_sDup.IndexOf("|") >= 0)
                    arErrorItem.Add(new MessageResult(eLogLevel.Error, ReceivingBatch.E_LINE_DUP, _sDup));
            }


            return true;
        }
 
    }

    public class DuplicateLineDataClass
    {
        public DuplicateLineDataClass(int _i, string _data)
        {
            Seq = _i;
            Text = _data;
        }
        public DuplicateLineDataClass()
        {
        }
        public int minSeqGroup = -1;
        public int Seq = -1;
        public string Text = string.Empty;
    }
}
