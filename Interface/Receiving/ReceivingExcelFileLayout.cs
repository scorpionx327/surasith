﻿using th.co.toyota.stm.fas.common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.common.Interface
{
    public class ReceivingExcelFileLayout
    {
        public ReceivingExcelFileLayout()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private List<ReceivingFileColumn> arColumnList = new List<ReceivingFileColumn>();
        private System.Collections.Hashtable htColumnList = new System.Collections.Hashtable();
        private FileLayoutType fileLayoutType = FileLayoutType.None;
        private char splitChar;
        private string targetTable = null;
        private string parkingTable = null;
        private bool bHeader = false;
        private bool bFooter = false;
        private int iHeaderStart = 0;


        private string _headerPrefix = null;
        private string _footerPrefix = null;

        private int _StartRow = 0;
       
        public string TargetTable
        {
            get { return targetTable; }
            set { targetTable = value; }
        }

        public int StartRow
        {
            get { return _StartRow; }
            set { _StartRow = value; }
        }

        public string ParkingTable
        {
            get { return parkingTable; }
            set { parkingTable = value; }
        }
        public void SetHeader(string _header)
        {
            bHeader = (string.Format("{0}", _header).ToLower() == "true");
        }
        public void SetFooter(string _footer)
        {
            bFooter = (string.Format("{0}", _footer).ToLower() == "true");
        }

        public int SetHeaderStart
        {
            get { return iHeaderStart; }
            set { iHeaderStart = value; }
        }

        public void SetCheckDuplicate(string _keyword)
        {
            _CheckDuplicate = (string.Format("{0}", _keyword).ToLower() == "true");
        }
        public string PrimaryKeyList { get; set; }
        public bool HeaderFlag
        {
            get { return bHeader; }
        }
        public bool FooterFlag
        {
            get { return bFooter; }
        }
        public string HeaderPrefix
        {
            get { return _headerPrefix; }
            set { _headerPrefix = value; }
        }
        public string FooterPrefix
        {
            get { return _footerPrefix; }
            set { _footerPrefix = value; }
        }


        private bool _CheckDuplicate = true;
        public bool CheckDuplicate
        {
            get
            {
                return _CheckDuplicate;
            }

        }
        public FileLayoutType FileLayoutType
        {
            get { return fileLayoutType; }
        }
        public void SetFileLayoutType(string _fileType)
        {
            //already check
            if (_fileType.ToLower() == "FixLength".ToLower())
            {
                this.fileLayoutType = FileLayoutType.FixLength;
                return;
            }
            if (_fileType.ToLower() == "Split".ToLower())
            {
                this.fileLayoutType = FileLayoutType.Split;
                return;
            }

            if (_fileType.ToLower() == "Excel".ToLower())
            {
                this.fileLayoutType = FileLayoutType.Excel;
                return;
            }
            throw (new BatchLoggingException(ReceivingBatch.E_INVALID_FILEOUTPUT_TYPE)); //missing fileoutputtype
        }


        public char Splitchar
        {
            set { this.splitChar = value; }
            get { return this.splitChar; }
        }


        private int _detailLength = 0;
        public int DetailLength
        {
            get { return _detailLength; }
            set { _detailLength = value; }
        }
        public void SetDetailLength(string _detail)
        {
            //already check
            //			if(!Util.IsInt(_detail))
            //				throw(new BatchLoggingException(Message.ERR018,"DetailLength")) ; 

            this.DetailLength = Convert.ToInt32(_detail);
        }

        public void AddColumn(ReceivingFileColumn _col)
        {
            if (this.fileLayoutType == FileLayoutType.None)
            {
                throw (new BatchLoggingException(ReceivingBatch.E_INVALID_FILELAYOUT));
            }
            if (htColumnList.ContainsKey(_col.FieldName))
            {
                throw (new BatchLoggingException(ReceivingBatch.E_DUP_FIELDNAME, _col.FieldName)); //duplicate
            }
            if (this.arColumnList.Count == 0)
            {

                this.arColumnList.Add(_col);
                htColumnList.Add(_col.FieldName, _col);

                return;
            }


            //check 
            ReceivingFileColumn _f = this.arColumnList[this.arColumnList.Count - 1] as ReceivingFileColumn;
            if (_f.Sequence + 1 != _col.Sequence)
            {
                throw (new BatchLoggingException(ReceivingBatch.E_INVALID_SEQUENCE, _f.FieldName, _col.FieldName));
            }
            //if (fileLayoutType == FileLayoutType.FixLength && _f.StartPosition + _f.Length < _col.StartPosition)
            //{
            //    throw (new BatchLoggingException(ReceivingBatch.E_INVALID_LENGTH, _f.FieldName, _col.FieldName));
            //}
            this.arColumnList.Add(_col);
            htColumnList.Add(_col.FieldName, _col);
        }
        public ReceivingFileColumn GetColumn(string _colName)
        {
            //already check exists
            return htColumnList[_colName] as ReceivingFileColumn;
        }

        public List<ReceivingFileColumn> ColumnList
        {
            get { return arColumnList; }
        }

        private System.Collections.Hashtable htHeaderXCheck = new System.Collections.Hashtable();
        public void AddHeaderXCheckList(int[] _pos, string _txt)
        {
            if (!htHeaderXCheck.ContainsKey(_pos))
            {
                htHeaderXCheck.Add(_pos, _txt);
            }
        }
        public System.Collections.Hashtable HeaderXCheckList
        {
            get
            {
                return htHeaderXCheck;
            }
        }


        public bool BatchIsRunningOnDB = false;
        public bool FastTransferMode = false;
    }
    public enum FileLayoutType
    {
        None,
        FixLength,
        Split,
        Multi,
        Excel
    }
    public enum DataType
    {
        Varchar,
        Integer,
        DateTime
    }
    public class ReceivingFileColumn
    {
        public ReceivingFileColumn(int _seq, string _fieldName)
        {
            this.Sequence = _seq;
            this.FieldName = _fieldName;
            this.StartPosition = -1;
            this.Length = -1;
        }

        public int Sequence = -1;
        public string FieldName = string.Empty;
        public int StartPosition = -1;
        public int Length = -1;
        public DataType DataType = DataType.Varchar;
        public string Format = string.Empty;
        public bool AllowNull = true;
        public string RegularExpression = string.Empty;

        public List<string> ValueList = new List<string>();
        public string ValueRawList = string.Empty;
    }
}
