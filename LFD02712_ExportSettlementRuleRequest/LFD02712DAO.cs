﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace LFD02712_ExportSettlementRuleRequest
{
    class LFD02712DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02712DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }
        //public DataTable GetReprintRequestData(string GUID, string Company)
        public DataTable GetReprintRequestData(string GUID)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_LFD02712_GetSettlementRuleRequest");
                //cmd.Parameters.AddWithValue("@COMPANY", Company);
                cmd.Parameters.AddWithValue("@GUID", GUID);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }
    }
}
