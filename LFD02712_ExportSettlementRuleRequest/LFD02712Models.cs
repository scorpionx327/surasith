﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFD02712_ExportSettlementRuleRequest
{
    public class LFD02712ResourceModel
    {
        public string Company { get; set; }
        public string AUCNo { get; set; }
        public string AUCSubNo { get; set; }
        public string AssetName { get; set; }
        public string PONo { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceLine { get; set; }
        public string InvoiceDescription { get; set; }
        public string AUCAmount { get; set; }
        public string NewAssetNo { get; set; }
        public string AssetSubNo { get; set; }
        public string CapitalizedDate { get; set; }
        public string AmountPosted { get; set; }
    }
    public class LFD02712SearchModel
    {
        public string Company { get; set; }
        public string GUID { get; set; }
    }
}
