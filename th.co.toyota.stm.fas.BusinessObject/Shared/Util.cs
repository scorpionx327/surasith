﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.IO;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using th.co.toyota.stm.fas.BusinessObject.Shared;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class Util
    {
        public Util()
        {
            //
            // TODO: Add constructor logic here
            //

        }
        public static bool IsCorrectDateTimeFormat(string _t, string _format)
        {
            if (_t == string.Empty)
                return true;
            try
            {
                DateTime _c = DateTime.ParseExact(_t, _format, null);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool IsNullorEmpty(object _obj)
        {
            if (_obj == null) return true;
            return string.Format("{0}", _obj).Trim() == string.Empty;
        }
        public static bool IsNullorEmpty(Hashtable _ht, string _key)
        {
            if (!_ht.ContainsKey(_key))
            {
                return true;
            }
            return IsNullorEmpty(_ht[_key]);
        }
        
        public static Exception CustomException(string _format, params object[] _args)
        {
            return new Exception(string.Format(_format, _args));
        }
        public static void ConsoleWriteLine(string _s)
        {
            int _max = 64;
            int _len = _s.Length;

            if (_s == string.Empty)
            {
                Console.WriteLine(_s.PadRight(_max, '#'));
            }
            else
            {
                Console.WriteLine("##  {0}##", _s.PadRight(58, ' '));
            }
        }
        public static bool IsInt(string _str)
        {
            try
            {
                int.Parse(_str);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static void DeleteFile(string _fileName, bool _bThrowException)
        {
            if (Util.IsNullorEmpty(_fileName))
                return;
            try
            {
                System.IO.File.Delete(_fileName);
            }
            catch (Exception ex)
            {
                if (_bThrowException)
                    throw (ex);
            }
        }
        public static string MoveFile(string _srcName, string _destPath)
        {
            if (!System.IO.File.Exists(_srcName))
            {
                return null;
            }

            if (!System.IO.Directory.Exists(_destPath))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(_destPath);
                }
                catch (Exception ex)
                {
                    throw (ex);
                    //throw (new BatchCommon.Log.BatchLoggingException(ReceivingBatch.E_DIR_FAIL, _destPath, ex.Message));
                }
            }

            try
            {
                string _name = (new System.IO.FileInfo(_srcName)).Name;
                string _destName = System.IO.Path.Combine(_destPath, _name);
                if (System.IO.File.Exists(_destName))
                {
                    System.IO.FileInfo _f = new System.IO.FileInfo(_destName);
                    string _newFileName = _f.Name;
                    if (_f.Extension != string.Empty)
                        _newFileName = _f.Name.Replace(_f.Extension, string.Empty);

                    _newFileName = _newFileName + string.Format("_{0:yyyyMMdd_HHmmss}", DateTime.Now) + _f.Extension;

                    _destName = System.IO.Path.Combine(_destPath, _newFileName);
                }

                System.IO.File.Move(_srcName, _destName);

                return (new System.IO.FileInfo(_destName).Name);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public static void CopyRefFile(string _srcName, string _destName, string _PathSource,string _PathDest = null)
        {
            try
            {
                if (_srcName == null || _srcName == "") {
                    return;
                }
                if (_PathDest == null || _PathDest == "") {
                    _PathDest = _PathSource;
                }
                string _s = System.IO.Path.Combine(_PathSource, _srcName);
                if (!System.IO.File.Exists(_s))
                    return;

                string _t = System.IO.Path.Combine(_PathDest, _destName);
                if (System.IO.File.Exists(_t))
                {
                    DeleteFile(_t, true);
                }
                System.IO.File.Copy(_s, _t, true);
            }catch(Exception ex)
            {
                throw (new Exception(string.Format("Copy {0} To {1} in path {2} => {3}",_srcName, _destName, _PathDest, ex.Message)));
            }
        }

        public static void DeleteDirectory(string _path)
        {
            if (_path != string.Empty && System.IO.Directory.Exists(_path))
            {
                try
                {
                    System.IO.Directory.Delete(_path, true);
                }
                catch (Exception ex)
                {
                    //no exp
                }
            }

        }

        public static DateTime StringToDate(string convertDatetime)
        {
            DateTime datetimeResult = new DateTime();
            //datetimeResult = DateTime.ParseExact(convertDatetime, "dd MMM yyyy HH:mm:ss FFFF", null);
            datetimeResult = DateTime.ParseExact(convertDatetime, "dd/MM/yyyy HH:mm:ss FFFF", null);
            return datetimeResult;
        }
        public static DateTime StringToDate(string convertDatetime, string datetimeFormat)
        {
            DateTime datetimeResult = new DateTime();
            datetimeResult = DateTime.ParseExact(convertDatetime, datetimeFormat, null);
            return datetimeResult;
        }

        public static bool IsNullDatetime(DateTime? datetimeValue)
        {
            if (datetimeValue.HasValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static string ConvertImagePathToByte(string ImagePath)
        {

            string base64Image = string.Empty;
            if (File.Exists(ImagePath))
            {
                string extension = string.Empty;
                extension = Path.GetExtension(ImagePath).Replace(".", "");

                byte[] imageArray = System.IO.File.ReadAllBytes(ImagePath);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);
            }
            return base64Image;
        }

        public static bool CheckFileExtension(string _fName, string _AllowFileExtension)
        {
            
            if (string.IsNullOrEmpty(_AllowFileExtension)) //Allow All
                return true;

            if(_fName.LastIndexOf(".") < 0)
            {
                return false;
            }

            string _ext = _fName.Substring(_fName.LastIndexOf("."));
            
            var _allow = _AllowFileExtension.Replace("*",string.Empty).Split('|').AsEnumerable();

            return _allow.Where(x => x.ToUpper() == _ext.ToUpper()).Count() > 0;
            //MSTD7013BERR	ERR	 File {0} is not expected file type {1}, {2}, {3}
        }

  
        public static string GetExtension(string _relative)
        {
            if(_relative.LastIndexOf(".") < 0)
            {
                return _relative;
            }

            return _relative.Substring(_relative.LastIndexOf("."));
        }

        public static Byte[] ReadLocalFile(string fileName, string filepath)
        {
            string file = System.IO.Path.Combine(filepath, fileName);
            
            MemoryStream memoryStream = new MemoryStream();
            using (Stream input = File.OpenRead(file))
            {
                byte[] buffer = new byte[32 * 1024]; // 32K buffer for example
                int bytesRead;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, bytesRead);
                }
            }
            memoryStream.Position = 0;
            Byte[] byteArray = memoryStream.ToArray();
            memoryStream.Flush();
            memoryStream.Close();

            return byteArray;
        }

    
        public static Byte[] ReadLocalFile(string _fullFileName)
        {
            if (System.IO.File.Exists(_fullFileName))
            {
                return null;
            }

            var _f = new FileInfo(_fullFileName);

            return Util.ReadLocalFile(_f.Name, _f.DirectoryName);
          
        }



        public static bool FTPFile(string fullFileName, string cfgFileName, string fileName)
        {
            // return eLogLevel.Information; //Test

            bool _rs = false;

            string ftpFile = "Tranfer(FTP) file : " + fileName;
            try
            {

                string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["LayoutSectionFTPUploadExcel"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
                {
                    throw (new Exception(string.Format("Configuration {0} not found", "LayoutSectionFTPUploadExcel")));
                }

                ConfigurationManager cfg = new ConfigurationManager(cfgFileName);
                System.Collections.Hashtable ht = cfg.GetChildNodes(cfgLayoutSectionFTP);
                foreach (string cfgFTP in ht.Keys)
                {
                    try
                    {
                        var ftp = new common.FTP.FTP(cfgFileName, cfgLayoutSectionFTP + @"/" + cfgFTP);
                        ftp.UploadFileName = fullFileName;
                        _rs = ftp.Execute();
                        
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc);

                        if (exc.HResult.Equals(-2146231997))
                        {
                            //  this.InsertLog(MSG.DataNotFound, "FTP", "configuration"); //MSTD7054BERR : {0} Data not found from {1}	
                            throw new Exception("No configuration found");
                        }
                        throw exc;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _rs;
        }

    }



    /// <summary>
    /// Class contaning method to resize an image and save in JPEG format
    /// </summary>
    public class ImageHandler
    {
        /// <summary>
        /// Method to resize, convert and save the image.
        /// </summary>
        /// <param name="image">Bitmap image.</param>
        /// <param name="maxWidth">resize width.</param>
        /// <param name="maxHeight">resize height.</param>
        /// <param name="quality">quality setting value.</param>
        /// <param name="filePath">file path.</param>      
        public byte[] ResizeStream(string _FileName, int quality, float ratioXY)
        {

            if (string.IsNullOrEmpty(_FileName))
            {
                return new byte[0];
            }

            if (!System.IO.File.Exists(_FileName))
            {
                return new byte[0];
            }


            Image _img = Image.FromFile(_FileName);
            // Get the image's original width and height
            int originalWidth = _img.Width;
            int originalHeight = _img.Height;

            // To preserve the aspect ratio
            float ratioX = ratioXY; // (float)maxWidth / (float)originalWidth;
            float ratioY = ratioXY; // (float)maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(_img, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the JPEG codec.
            ImageCodecInfo imageCodecInfo = this.GetEncoderInfo(ImageFormat.Jpeg);

            // Create an Encoder object for the Quality parameter.
            Encoder encoder = Encoder.Quality;

            // Create an EncoderParameters object. 
            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;

            System.IO.MemoryStream myResult = new System.IO.MemoryStream();

            newImage.Save(myResult, imageCodecInfo, encoderParameters);

            //newImage.Save(_FileName+"X", imageCodecInfo, encoderParameters);


            var _bb = myResult.ToArray();

            myResult.Dispose();

            return _bb;
        }

        /// <summary>
        /// Method to get encoder infor for given image format.
        /// </summary>
        /// <param name="format">Image format</param>
        /// <returns>image codec info.</returns>
        private ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }
    }
    
}