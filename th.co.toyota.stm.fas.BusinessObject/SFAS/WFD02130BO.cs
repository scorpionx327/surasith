﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD02130;
using th.co.toyota.stm.fas.BusinessObject.Common;
using System.IO;
using System.Web;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02130BO : BO
    {
        private SystemConfigurationDAO system_dao;
        private WFD02130DAO dao;
        private MessageDAO mdao;
        public WFD02130BO()
        {
            dao = new WFD02130DAO();
            system_dao = new SystemConfigurationDAO();
            mdao = new MessageDAO();
        }
        public BaseJsonResult GetFixedAssetList(WFD02130UpdateDataModel ds, string defaultImagePath)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(ds)) return Result(null, null);
                WFD02130_TB_M_ASSETS_H _AssetH = new WFD02130_TB_M_ASSETS_H();
                _AssetH = dao.GetFixedAssetList(ds);

                //_AssetH.TAG_PHOTO = ConvertImagePathToByte(Path.Combine(GetPathUpload(), _AssetH.TAG_PHOTO));
                _AssetH.TAG_PHOTO = ConvertImagePathToByte(_AssetH.TAG_PHOTO);


                var datas = _AssetH;
                if (datas == null)
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found

                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        //public SearchJsonResult GetFixedAssetDetail(WFD02130UpdateDataModel ds, PaginationModel page)
        //{
        //    Begin();
        //    try
        //    {
        //        var datas = dao.GetFixedAssetDetailList(ds, ref page);
        //        return Result(datas, page);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null, page);
        //    }
        //}

        //public SearchJsonResult GetFixedAssetFinance(WFD02130UpdateDataModel ds, PaginationModel page)
        //{
        //    Begin();
        //    try
        //    {
        //        var datas = dao.GetFixedAssetFinance(ds, ref page);
        //        return Result(datas, page);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null, page);
        //    }
        //}

        public string ConvertImagePathToByte(string _FileName)
        {

            if (string.IsNullOrEmpty(_FileName) || !File.Exists(_FileName))
            {
                // Show Default Incase no file
                return GetImageDefalut();
            }


            double _MaxLength = (1.5 * 1024 * 1014);
            SystemBO _bo = new SystemBO();
            var _sysList = _bo.SelectSystemDatas("SYSTEM_CONFIG", "FILE_SIZE").Find(x => x.CODE == "JSON_DOWNLOAD_LENGTH");
            if (_sysList != null)
            {
                _MaxLength = (Convert.ToDouble(_sysList.VALUE) * 1024 * 1024);
            }


            //File size
            var _fileInfo = new System.IO.FileInfo(_FileName);

            long _fs = _fileInfo.Length;

            // File size > Limit => Convert
            if (_fs > _MaxLength) //1.5 MB
            {
                ImageHandler _img = new ImageHandler();

                //Converting
                var _arry = _img.ResizeStream(_FileName, 50, 1);

                //Result still greater then Limit
                if (_arry.Length > _MaxLength)
                {
                    // Show Default
                    return GetImageMaxLength();
                }

                // Convert Result
                string extension = Path.GetExtension(_FileName).Replace(".", "");
                string base64ImageRepresentation = Convert.ToBase64String(_arry);
                string base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);

                return base64Image;

            }

            //Convert Actual file to JSON
            return Util.ConvertImagePathToByte(_FileName);







            //string base64Image = string.Empty;
            //if (File.Exists(ImagePath))
            //{
            //    string extension = string.Empty;
            //    extension = Path.GetExtension(ImagePath).Replace(".", "");

            //    byte[] imageArray = System.IO.File.ReadAllBytes(ImagePath);
            //    string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            //    base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);
            //}
            //else return null;
            //return base64Image;
        }
        public BaseJsonResult UpdateAssetInfo(WFD02130_TB_M_ASSETS_H _data, string _User)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                dao.UpdateAssetInfo(_data, _User);
                var datas = dao.GetFixedAssetList(new WFD02130UpdateDataModel()
                {
                    COMPANY = _data.COMPANY,
                    ASSET_NO = _data.ASSET_NO,
                    ASSET_SUB = _data.ASSET_SUB

                });
                AddMessage(Constants.MESSAGE.MSTD0101AINF);
                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }

        public BaseJsonResult UpdateTAG_Photo(WFD02130UpdateDataModel _data, HttpRequestBase Request = null)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                if (Request.Files.Count > 0)
                {
                    string fname;
                    string file_name;
                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = _data.FILE.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = _data.FILE.FileName;
                    }

                    string extension = string.Empty;
                    extension = Path.GetExtension(_data.FILE.FileName);
                    string AssetNoFileName = _data.ASSET_NO + extension;

                    // Get the complete folder path and store the file inside it
                    file_name = fname;
                    fname = Path.Combine(GetPathUpload(), file_name);
                    _data.FILE.SaveAs(fname); // upload file 

                    _data.TAG_PHOTO = file_name;
                }
                else
                {
                    _data.TAG_PHOTO = "";
                }

                dao.UpdateTAG_Photo(_data);
                AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }

        public BaseJsonResult UpdateTAG_PhotoBase64(WFD02130UpdateDataModel _data, HttpRequestBase Request = null)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    /*string fname;

                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = _data.FILE.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = _data.FILE.FileName;
                    }

                    string extension = string.Empty;
                    extension = Path.GetExtension(_data.FILE.FileName);
                    string AssetNoFileName = _data.ASSET_NO + extension;

                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(GetPathUpload(), AssetNoFileName);
                    _data.FILE.SaveAs(fname); // upload file 

                    _data.TAG_PHOTO = AssetNoFileName;
                    */
                    string AssetNoFileName = string.Format("{0}_{1}_{2}.{3}", _data.COMPANY, _data.ASSET_NO, _data.ASSET_SUB, _data.extension);
                    string fullOutputPath = Path.Combine(GetPathUpload(), AssetNoFileName);

                    if (!Directory.Exists(Path.GetDirectoryName(fullOutputPath)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(fullOutputPath));
                    }

                    //File.WriteAllBytes(fullOutputPath, Convert.FromBase64String(_data.base64Image.Replace("data:image/png;base64,", "")));
                    //file.SaveAs(fullOutputPath);


                    var imageSizeSettings = new SystemDAO().SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG, SYSTEM_SUB_CATEGORY.ASSET_IMAGE);
                    var Width = String.Empty;
                    var Height = String.Empty;

                    if (imageSizeSettings.Where(m => m.CODE == "WIDTH").Count() > 0)
                        Width = imageSizeSettings.Where(m => m.CODE == "WIDTH").Select(m => m.VALUE).First();
                    else Width = "400";
                    if (imageSizeSettings.Where(m => m.CODE == "HEIGHT").Count() > 0)
                        Height = imageSizeSettings.Where(m => m.CODE == "HEIGHT").Select(m => m.VALUE).First();
                    else Height = "400";

                    ImageUpload imageUpload = new ImageUpload { Width = int.Parse(Width), Height = int.Parse(Height) };
                    ImageResult imageResult = imageUpload.UploadFile(file, fullOutputPath);


                    _data.TAG_PHOTO = AssetNoFileName;
                }
                else
                {
                    _data.TAG_PHOTO = "";
                }

                dao.UpdateTAG_Photo(_data);
                AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }

        public bool AddPrintTagAndUpdateBarCodeSize(WFD02130UpdateDataModel _data, bool IsFAAdmin, out string message)
        {
            message = null;

            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                if (!IsFAAdmin && _data.PRINT_CONDITION.Equals(this.GetPrintStatusYBO()))
                {
                    //AddMessage(Constants.MESSAGE.MCOM0003AERR);
                    message = mBo.GetMessage(Constants.MESSAGE.MCOM0003AERR, null).VALUE;
                    return false;
                }
               
                //AddMessage(Constants.MESSAGE.MSTD0101AINF);
                //return Constants.MESSAGE.MSTD0101AINF;

                if (_data.PLATE_TYPE == "S")
                {
                    message = mBo.GetMessage(Constants.MESSAGE.MFAS1102BINF,null).VALUE;
                }
                else
                {
                    message = mBo.GetMessage(Constants.MESSAGE.MFAS1102CINF, null).VALUE;
                    _data.PRINT_LOCATION = null;
                }

                dao.AddPrintTagAndUpdateBarCodeSize(_data);

                return true;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return false;
        }
        public MessageModel GetMessage(string MESSAGE_CODE)
        {
            return mdao.GetMessage(MESSAGE_CODE);
        }
        public string GetPermissionUploadBOIDoc(string USER_LOGON)
        {
            try
            {
                string _Permission;
                _Permission = dao.GetPermissionUploadBOIDoc(USER_LOGON);
                return _Permission;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetImageDefalut()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_DEFAULT_UPLOAD";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }
        public string GetImageMaxLength()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_MAX_LENGTH";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }
        private string GetPathUpload()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "UPLOAD_PATH";
            m.CODE = "ASSET_PATH";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }
        public string GetExtension()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "ASSET_UPLOAD";
            m.CODE = "EXTENSION_FILE";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }
            //path = path.Replace(",", "|");
            if (string.IsNullOrEmpty(path))
            {
                path = "image/*";
            }
            return path;
        }

        public string GetPrintStatusYBO()
        {
            Begin();
            try
            {
                SystemConfigurationModels type = new SystemConfigurationModels();
                type.CATEGORY = "PRINT_STATUS";
                type.SUB_CATEGORY = "Y";
                var data = system_dao.GetSystemData(type).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();

                return data.VALUE;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return "";
            }
        }

        public BaseJsonResult GetPrintLocation(WFD02130UpdateDataModel _data)
        {
            Begin();
            try
            {
                var datas = dao.GetPrintLocation(_data);
                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }
        //public BaseJsonResult UploadFileAttchDoc(WFD02130BOIModel data, HttpRequestBase Request = null)
        //{
        //    Begin();
        //    SystemBO bo = new SystemBO();
        //    string _DocNo = "";
        //    string _fileName = "";

        //    WFD02130BOIModel _BOIDetail = new WFD02130BOIModel();
        //    try
        //    {
        //        if (Request.Files.Count == 0)
        //        {
        //            return Result(new { FileName = string.Empty });
        //        }

        //        string _cfgUpload = string.Format("WFD02410_{0}", "BOI");

        //        _BOIDetail.BOIFile = Request.Files[0];

        //        if (Request.Form["ASSET_NO"] == null || Request.Form["ASSET_NO"] == "")
        //        {
        //            _DocNo = "TEMP";
        //        }
        //        else
        //        {
        //            _DocNo = "T" + Request.Form["ASSET_NO"].Replace("/", "");
        //        }
        //        _fileName = _DocNo + "_BOI";

        //        string _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmss}{2}", _fileName, DateTime.Now,
        //            Util.GetExtension(_BOIDetail.BOIFile.FileName));

        //        var _b = CheckUploadDocument(_BOIDetail.BOIFile, _cfgUpload);
        //        if (!_b)
        //            return Result(new { FileName = string.Empty });

        //        var _bo = new Common.SystemBO();
        //        string _path = _bo.GetPathDocUpload(_cfgUpload);



        //        UploadDocument(_BOIDetail.BOIFile, _cfgUpload, _newFileName);
        //        _BOIDetail.ASSET_NO = Request.Form["ASSET_NO"];
        //        _BOIDetail.COMPANY = Request.Form["COMPANY"];
        //        _BOIDetail.ASSET_SUB = Request.Form["ASSET_SUB"];
        //        _BOIDetail.FullFileName = System.IO.Path.Combine(_path, _newFileName);
        //        _BOIDetail.FileName = _newFileName;
        //        _BOIDetail.FileType = Util.GetExtension(_BOIDetail.BOIFile.FileName) ;
        //        _BOIDetail.EMP_CODE = data.EMP_CODE;
        //        _BOIDetail.UPDATE_DATE = Request.Form["UPDATE_DATE"];
        //        dao.UpdateBOIDoc(_BOIDetail);
        //        return Result(new { FileName = _newFileName });
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(new { FileName = string.Empty });
        //    }
        //}
        public BaseJsonResult UploadFileAttchDoc(FileDownloadModel data, string user)
        {
            Begin();
            try
            {
                var updateDate = dao.UpdateBOIDoc(data, user);
                return Result(new { success = true, updateDate });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new { success = false });
            }
        }

        public BaseJsonResult DeleteFileAttchDoc(WFD02130BOIModel data)
        {
            Begin();
            try
            {
                dao.DeleteBOIDoc(data);

                return Result(new { FileName = string.Empty });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new { FileName = string.Empty });
            }
        }
        public BaseJsonResult CheckValidateUploadFile(CheckFileUploadModel data)
        {
            Begin();
            try
            {
                var _bo = CheckUploadFile(data);

                return Result(_bo);


            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }
    }
}
