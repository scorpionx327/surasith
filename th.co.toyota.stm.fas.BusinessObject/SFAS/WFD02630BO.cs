﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02630;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02630BO : BO
    {
        private WFD02630DAO dao;
        public WFD02630BO()
        {
            dao = new WFD02630DAO();

        }
        public BaseJsonResult GetYearBO(string company)
        {
            Begin();

            try
            {
                return Result(new { data = dao.GetYearDAO(company) });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GetRoundBO(string company, string year)
        {
            Begin();

            try
            {
                return Result(new { data = dao.GetRoundDAO(company,year) });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetAssetLocationBO(string company,string year, string round)
        {
            Begin();

            try
            {
                return Result(new { data = dao.GetAssetLocationDAO(company,year, round) });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        #region Search Function
        public BaseJsonResult GetHolidayBO(WFD02630SearchModel data)
        {
            Begin();

            try
            {
                return Result(new { data = dao.GetHolidayDAO(data) });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GetStockDetail(WFD02630SearchModel con)
        {
            Begin();
            try
            {
                if (!Validation(con)) return Result(null, null);
                var datas = dao.GetStockList(con);
                if (datas.DateAsOf == null || datas.OverAll == null || datas.Calendar == null || datas.TargetDate == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                else
                {
                    //gen class dateasof
                    datas.DateAsOf.CLASS_NAME = GetClass(datas.DateAsOf.CLASS_NAME);
                    //gen class / icon 
                    datas.OverAll.CLASS_NAME = GetBGClass(datas.OverAll.CLASS_NAME);

                    if(dao.CheckCreateRequestStock(con.COMPANY ,con.YEAR,con.ROUND,con.ASSET_LOCATION,con.EMP_CODE))
                    datas.OverAll.IMGSRC = GetImg(datas.OverAll.IMAGE_TYPE);


                    datas.OverAll.IMAGE_TYPE = GeIcon(datas.OverAll.IMAGE_TYPE);
                    //gen calendar

                    List<WFD02630CalendarModel> iList = new List<WFD02630CalendarModel>();

                    var requestContext = HttpContext.Current.Request.RequestContext;


                    foreach (var item in datas.Calendar)
                    {
                        if (!string.IsNullOrEmpty(item.start)){
                            WFD02630CalendarModel i = new WFD02630CalendarModel();
                            i.title = item.title;
                            i.description = item.description;
                            i.start = !string.IsNullOrEmpty(item.start) ? item.start : null;
                            i.end = !string.IsNullOrEmpty(item.end) ? DateTime.Parse(item.end).AddDays(1).ToString("yyyy/MM/dd") : null;
                            i.borderColor = "#000000";
                            i.className = GetClassCalendar(item.className);
                            i.url = new UrlHelper(requestContext).Action("Index", "WFD01270") + item.url;
                            i.IS_SEND_REQUEST = item.IS_SEND_REQUEST;
                            iList.Add(i);
                        }
                    }
                    datas.Calendar = iList;
                }


                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        #endregion

        public bool Validation(WFD02630SearchModel data)
        {
            bool isBool = true;
            if (string.IsNullOrEmpty(data.YEAR))
            {
                AddMessage("MSTD0031AERR", "Year");
                isBool = false;
            }
            if (string.IsNullOrEmpty(data.ROUND))
            {
                AddMessage("MSTD0031AERR", "Round");
                isBool = false;
            }
            if (string.IsNullOrEmpty(data.ASSET_LOCATION))
            {
                AddMessage("MSTD0031AERR", "Asset Location");
                isBool = false;
            }
            return isBool;
        }
        private string GetBGClass(string param)
        {
            string txt = "";
            switch (param)
            {
                case "Complete":
                    txt = "bg-green";
                    break;
                case "Complete with Delay":
                    txt = "bg-yellow";
                    break;
                case "Delay":
                    txt = "bg-yellow";
                    break;
                case "On Progress":
                    txt = "bg-aqua";
                    break;
                case "Complete with Loss":
                    txt = "bg-red";
                    break;
                case "not_yet_scan":
                    txt = "";
                    break;
            }
            return txt;
        }
        private string GetClass(string param)
        {
            string txt = "";
            switch (param)
            {
                case "Complete":
                    txt = "progress-bar-green";
                    break;
                case "Complete with Delay":
                    txt = "progress-bar-yellow";
                    break;
                case "Delay":
                    txt = "progress-bar-yellow";
                    break;
                case "On Progress":
                    txt = "progress-bar-aqua";
                    break;
                case "Complete with Loss":
                    txt = "progress-bar-red";
                    break;
                case "not_yet_scan":
                    txt = "";
                    break;
            }
            return txt;
        }
        private string GetClassCalendar(string param)
        {
            string txt = "";
            switch (param)
            {
                case "Complete":
                    txt = "text-des-calendar-complete";
                    break;
                case "Complete with Delay":
                    txt = "text-des-calendar-deley";
                    break;
                case "Delay":
                    txt = "text-des-calendar-deley";
                    break;
                case "On Progress":
                    txt = "text-des-calendar-onprocess";
                    break;
                case "Complete with Loss":
                    txt = "text-des-calendar-loss";
                    break;
                case "not_yet_scan":
                    txt = "text-des-calendar-notscan";
                    break;
            }
            return txt;
        }
        private string GeIcon(string param)
        {
            string txt = "";
            switch (param)
            {
                case "CIRCLE":
                    txt = "fa-circle-thin";
                    break;
                case "TRIANGLE":
                    txt = "fa-caret-up";
                    break;
                case "CROSS":
                    txt = "fa-close";
                    break;
            }
            return txt;
        }
        private string GetImg(string param)
        {
            string txt = "";
            switch (param)
            {
                case "CIRCLE":
                    txt = @"/Images/circle.gif";
                    break;
                case "TRIANGLE":
                    txt = @"/Images/caret.gif";
                    break;
                case "CROSS":
                    txt = @"/Images/cross.gif";
                    break;
            }
            return txt;
        }
        public  BaseJsonResult GetDefaultScreenBO(WFD02630SearchModel data)
        {
            Begin();

            try
            {
                return Result(new { data = dao.GetDefaultScreenDAO(data) });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
    }
}
