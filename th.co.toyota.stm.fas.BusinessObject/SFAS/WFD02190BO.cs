﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02190;
using th.co.toyota.stm.fas.BusinessObject.Common;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02190BO : BO
    {
        private WFD02190DAO dao;
        public WFD02190BO()
        {
            dao = new WFD02190DAO();
        }

        public SearchJsonResult Search(WFD02190SearchModel data, PaginationModel page)
        {
            Begin();
            try
            {
                if (ValidateSearch(data))
                {
                    var datas = dao.Search(data, ref page);
                    if (datas.Count > 0)
                    {
                        return Result(datas, page);
                    }
                    else
                    {
                        AddMessage(Constants.MESSAGE.MSTD0059AERR);
                        return Result(null, page, "#AlertMessageWFD02190");
                    }
                }
                else
                {
                    return Result(null, page);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page, "#AlertMessageWFD02190");
            }
        }
        public BaseJsonResult SaveAssetsData(WFD02190SaveModel data)
        {
            bool _returnSave = false;
            string _returnValue = "";

            Begin();
            try
            {
                if (!ValidateGetAssetsData(data)) return Result("-99");

                if (data.SING_FLAG == "Y")
                {
                    _returnValue = data.DETIAL_ASSET[0].ASSET_NO + "|" + data.DETIAL_ASSET[0].ASSET_NAME;
                    return Result(_returnValue);
                }
                else
                {
                    if (data.REQUEST_TYPE != null && data.REQUEST_TYPE == "D")
                    {
                        _returnSave = dao.SaveTempDispAssetsData(data);
                        if (_returnSave)
                        {
                            var _listReturn = dao.CheckParentAndChildDisposal(data);
                            if (_listReturn != null && _listReturn.Count > 0)
                            {
                                AddListMessage(_listReturn);
                                return Result("-99");
                            }
                            else
                            {
                                _returnSave = dao.SaveAssetsDataByGUID(data);
                                if (_returnSave)
                                {
                                    return Result("0");
                                }
                                else
                                {
                                    AddMessage(Constants.MESSAGE.MSTD0059AERR);
                                    return Result("-99");
                                }
                            }
                        }
                        else
                        {
                            return Result("-99");
                        }
                    }
                    else
                    {
                        _returnSave = dao.SaveAssetsData(data);
                        if (_returnSave)
                        {
                            return Result("0");
                        }
                        else
                        {
                            AddMessage(Constants.MESSAGE.MSTD0059AERR);
                            return Result("-99");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result("-99");
            }

        }
        private void AddListMessage(List<MessageModel> datas)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                switch (datas[i].MESSAGE_TYPE)
                {
                    case "WRN":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.WARNING, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "ERR":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "INF":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.INFO, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    default:
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                }
            }
        }

        private bool ValidateSearch(WFD02190SearchModel model)
        {
            bool correct = true;

            if (!string.IsNullOrEmpty(model.DATE_IN_SERVICE_START) && !string.IsNullOrEmpty(model.DATE_IN_SERVICE_TO))
            {
                if (!ValidateDate((DateTime)Date(model.DATE_IN_SERVICE_START), (DateTime)Date(model.DATE_IN_SERVICE_TO)))
                {
                    AddMessage(Constants.MESSAGE.MSTD0022AERR, "Capitialized Date (To)", "Capitialized Date (From)");
                    correct = false;
                }
            }

            return correct;
        }

        private bool ValidateGetAssetsData(WFD02190SaveModel ds)
        {
            string _checkCostCenter = "";
            if (ds == null || ds.DETIAL_ASSET == null)
            {
                AddMessage(Constants.MESSAGE.MCOM0001AERR, "adding asset no");
                return false;
            }
            if (ds.DETIAL_ASSET == null)
            {
                AddMessage(Constants.MESSAGE.MSTD0059AERR);
                return false;
            }
            if (ds.SING_FLAG == "Y")
            {
                if (ds.DETIAL_ASSET.Count > 1)
                {
                    AddMessage(Constants.MESSAGE.MSTD0103AINF, "Asset No");
                    return false;
                }
            }
            if (ds.DETIAL_ASSET.Count > 0)
            {
                for (int i = 0; i < ds.DETIAL_ASSET.Count; i++)
                {
                    if (i == 0)
                    {
                        _checkCostCenter = ds.DETIAL_ASSET[i].COST_CODE;
                        continue;
                    }
                    if (_checkCostCenter != ds.DETIAL_ASSET[i].COST_CODE)
                    {
                        AddMessage(Constants.MESSAGE.MFAS0706AERR);//Can't select asset with difference cost center
                        return false;
                    }
                }

            }
            return true;
        }
        //private string ValidateFormatDate(string _date)
        //{
        //    var returnDate = "";
        //    int day;
        //    int year;
        //    int Month = 0;
        //    var arrDate = _date.Split('-');
        //    if (arrDate.Length != 3)
        //    {
        //        return "";
        //    }
        //    else
        //    {
        //        try
        //        {
        //            day = int.Parse(arrDate[0]);
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }
        //        try
        //        {
        //            year = int.Parse(arrDate[2]);
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }
        //        try
        //        {
        //            switch (arrDate[1])
        //            {
        //                case "Jan":
        //                    Month = 1;
        //                    break;
        //                case "Feb":
        //                    Month = 2;
        //                    break;
        //                case "Mar":
        //                    Month = 3;
        //                    break;
        //                case "Apr":
        //                    Month = 4;
        //                    break;
        //                case "May":
        //                    Month = 5;
        //                    break;
        //                case "Jun":
        //                    Month = 6;
        //                    break;
        //                case "Jul":
        //                    Month = 7;
        //                    break;
        //                case "Aug":
        //                    Month = 8;
        //                    break;
        //                case "Sep":
        //                    Month = 9;
        //                    break;
        //                case "Oct":
        //                    Month = 10;
        //                    break;
        //                case "Nov":
        //                    Month = 11;
        //                    break;
        //                case "Dec":
        //                    Month = 12;
        //                    break;
        //                default:
        //                    Month = 0;
        //                    break;
        //            }
        //            if (Month != 0)
        //            {
        //                returnDate = year.ToString() + "-" + Month.ToString() + "-" + day.ToString();
        //                return returnDate;
        //            }
        //            else
        //            {
        //                return "";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }
        //    }
        //}
    }
}
