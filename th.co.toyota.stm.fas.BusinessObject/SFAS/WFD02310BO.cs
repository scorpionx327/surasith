﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02310;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02310BO : TFASTRequestBO
    {

        private WFD02310DAO dao;
        public WFD02310BO()
        {
            dao = new WFD02310DAO();
        }
        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.InitialAssetList(data);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }

        public List<WFD02310Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                //Get Document Status
                string _DocStatus = "00";

                var _header = (new WFD01170BO()).GetRequestHeader(data);
                if (_header != null)
                {
                    _DocStatus = _header.STATUS;
                }

                var _permission = (new WFD01170BO()).GetPermission(data);

                var datas = dao.GetAssetList(data, page);

                datas.ForEach(delegate (WFD02310Model _item)
                {
                    _item.AllowCheck = _permission.CanCheck(_item.ALLOW_CHECK, _item.STATUS);
                    //_item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y");
                    // _item.AllowDelete = (_item.ALLOW_DELETE == "Y" && (_permission.AllowDelete == "Y") ) || _item.COMMON_STATUS != "Y";
                    _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y");
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);

                });
                
                if (datas.Count == 0)
                {
                    return new List<WFD02310Model>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD02310Model>();
            }
        }

        public BaseJsonResult ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }

        public BaseJsonResult InsertAsset(List<WFD0BaseRequestDetailModel> _list)
        {
            Begin();
            try
            {
                if (_list == null || _list.Count == 0)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = "No Selected Data" });

                foreach (var _data in _list)
                    dao.InsertAsset(_data);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }
        public BaseJsonResult PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (!this.IsValidation(data))
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message =  "Invalid Asset List"});
                }
                dao.PrepareCostCenterToGenerateFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }

        }
        public BaseJsonResult UpdateAssetList(List<WFD02310Model> list, string _user)
        {
            Begin();
            try
            {

                if (list != null && list.Count > 0)
                {
                    dao.UpdateAsset(list,_user);
                }
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

		private bool IsValidation(WFD0BaseRequestDocModel _data)
        {
            
            var _s = dao.CheckValidation(_data);

            this.AddMessageList(_s);
            if (_s.Count > 0)
                return false;

            return true;
        }
        //Abstract Function
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                return this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID }) ;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.ApproveValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;


            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {

            try
            {
                dao.Submit(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //public override bool Resend(WFD01170SelectedAssetNoModel _data)
        //{
        //    return true; // No send file process
        //}

        //public override WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data)
        //{
        //    return new WFD01170UserPermissionModel()
        //    {
        //        AllowRegen = "N",
        //        AllowGenAssetNo = "N"
        //    };
        //}

       

        public override bool Copy(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }
    }
}
