﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using th.co.toyota.stm.fas.DAO.SFAS;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.SFAS.WFD01020;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01020BO : BO
    {
        WFD01020DAO dao;

        public WFD01020BO()
        {
            dao = new WFD01020DAO();          
        }

        public WFD01020ComboBoxModel GetCategory()
        {
            try
            {
                var values = dao.GetCategory();

                WFD01020ComboBoxModel Model = new WFD01020ComboBoxModel();

                Model.SelectItemOptions.Add(new SelectListItem { Text = "Select", Value = string.Empty, Selected = true });
                Model.SelectItemOptions.AddRange(values.Select(x => new SelectListItem { Text = x.Value, Value = x.Value }));

                return Model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public WFD01020ComboBoxModel GetSubCategory(string category)
        {
            try
            {
                var values = dao.GetSubCategory(category);

                WFD01020ComboBoxModel Model = new WFD01020ComboBoxModel();

                Model.SelectItemOptions.Add(new SelectListItem { Text = "Select", Value = string.Empty, Selected = true });
                Model.SelectItemOptions.AddRange(values.Select(x => new SelectListItem { Text = x.Value, Value = x.Value }));

                return Model;
            }
            catch (Exception)
            {
                throw;
            }
        }
     
        public SearchJsonResult SearchSystemMaster(WFD01020DataModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var result = new List<WFD01020DataModel>();
                if (Validate(data))
                {
                    result = dao.SearchSystemMaster(data.CATEGORY, data.SUB_CATEGORY, ref page);
                    if (result.Count > 0)
                    {
                        return Result(result, page);
                    }
                    else
                    {
                        AddMessage("MSTD0059AERR");
                        return Result(null, page);
                    }
                }
                else
                {
                    return Result(null, page);
                }

                return Result(result, page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }

        public BaseJsonResult GetSystemMaster(WFD01020DataModel data)
        {
            Begin();
            try
            {               
                var result = dao.GetSystemMaster(data);

                if (result == null) {
                    result = new WFD01020DataModel() {
                        CATEGORY = data.CATEGORY,
                        SUB_CATEGORY = data.SUB_CATEGORY,
                        ACTIVE_FLAG = "Y"
                    };
                }

                return Result(result);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult InsertSystemMaster(WFD01020DataModel data)
        {
            Begin();
            try
            {
                if (!ValidateItem(data))
                    return Result(false);

                dao.InsertSystemMaster(data);
              
                AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }

        public BaseJsonResult UpdateSystemMaster(WFD01020DataModel data)
        {
            Begin();
            try
            {
                if (!ValidateItem(data))
                    return Result(false);

                dao.UpdateSystemMaster(data);

                AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }

        public BaseJsonResult ValidateSystemMaster(WFD01020DataModel data)
        {
            Begin();
            try
            {
                return Result(Validate(data));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }            
        }

        private bool Validate(WFD01020DataModel data)
        {
            bool IsValid = true;
          
            if (string.IsNullOrWhiteSpace(data.CATEGORY))
            {
                AddMessage("MSTD0031AERR", "Category");
                IsValid = false;
            }

            if (string.IsNullOrWhiteSpace(data.SUB_CATEGORY))
            {
                AddMessage("MSTD0031AERR", "Sub category");
                IsValid = false;
            }
           
            return IsValid;
        }

        private bool ValidateItem(WFD01020DataModel data)
        {
            bool IsValid = true;

            if (string.IsNullOrWhiteSpace(data.CODE))
            {
                AddMessage("MSTD0031AERR", "Code");
                IsValid = false;
            }

            if (string.IsNullOrWhiteSpace(data.VALUE))
            {
                AddMessage("MSTD0031AERR", "Value");
                IsValid = false;
            }

            if (data.CATEGORY.Equals("SYSTEM_CONFIG") && data.SUB_CATEGORY.Equals("APPROVE_HIGHER_MATCH"))
            {
                System.Data.DataTable dtPriority = new System.Data.DataTable();
                dtPriority =  dao.GetSystemMasterList("ROLE_MAPPING", "ROLE_PRIORITY");

                //var result = dao.GetSystemMaster(data);

                int roleMain = GetPriority(data.CODE, dtPriority);

         
               string [] _priority  =  data.VALUE.Split('|');

                for (int i = 0; i < _priority.Length; i++)
                {
                    int rolePrio = GetPriority(_priority[i], dtPriority);
                    if (rolePrio <= roleMain)
                    {
                        AddMessage("MSTD0111AERR", _priority[i], data.CODE);
                        IsValid = false;
                        break;
                    }

                }

                

            }

            return IsValid;
        }


        private int GetPriority(string _code, System.Data.DataTable dtPriority)
        {
            int i = 0;

            System.Data.DataRow [] dr = dtPriority.Select(string.Format("CODE = '{0}'", _code));
            if (dr.Length > 0)
            {
                i =  int.Parse(string.Format("{0}",dr[0]["VALUE"]));

            }

            return i;
        }

    }
}
