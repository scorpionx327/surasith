﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD01210;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01210BO : BO
    {
        private WFD01210DAO dao;
        private SystemConfigurationDAO system_dao;
        private string _format = "dd.MM.yyyy";
        CultureInfo cultureInfo = new CultureInfo("en");
        public WFD01210BO()
        {
            dao = new WFD01210DAO();
            system_dao = new SystemConfigurationDAO();
        }
        public bool IsAECUser(string EMP_CODE)
        {
            var _user = dao.GetEmployeeByEmpCodeDAO(new WFD01210SearchModel() { EMP_CODE = EMP_CODE });
            if (_user == null)
                return false; ;
            return string.Format("{0}", _user.FAADMIN) == "Y";
        }
        public List<string> GetCompanyList(string EMP_CODE)
        {
            var _s = new List<string>();

            var _rs = dao.GetCompayList(new WFD01210SearchModel() { EMP_CODE = EMP_CODE });
            foreach (var _data in _rs)
            {
                _s.Add(_data.COMPANY);
            }

            return _s;
        }
        public BaseJsonResult GetDivisionBO()
        {
            Begin();
            try
            {
                var datas = dao.GetDivisionNameDAO();
                //var datas = dao.GetOrganizationDAO().Where(x => !string.IsNullOrEmpty(x.DIV_NAME)).GroupBy(g => new { g.DIV_CODE, g.DIV_NAME }).Select(x =>
                //          new
                //          {
                //              DIV_CODE = x.First().DIV_CODE,
                //              DIV_NAME = x.First().DIV_NAME
                //          }).OrderBy(x => x.DIV_CODE).ThenBy(x => x.DIV_NAME);

                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }
        public BaseJsonResult GetDepartmentBO(string DivisionName)
        {
            Begin();
            try
            {
                var datas = dao.GetDepartmentNameDAO(DivisionName);
                //var datas = dao.GetOrganizationDAO().Where(x => !string.IsNullOrEmpty(x.DEPT_NAME)).GroupBy(g => new { g.DEPT_CODE, g.DEPT_NAME }).Select(x =>
                //      new
                //      {
                //          DEPT_CODE = x.First().DEPT_CODE,
                //          DEPT_NAME = x.First().DEPT_NAME
                //      }).OrderBy(x => x.DEPT_CODE).ThenBy(x => x.DEPT_NAME);

                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }
        public BaseJsonResult GetSectionBO(string DivisionName, string DeptName)
        {
            Begin();
            try
            {
                var datas = dao.GetSectionNameDAO(DivisionName, DeptName);
                //var datas = dao.GetOrganizationDAO().Where(x => !string.IsNullOrEmpty(x.SEC_NAME)).GroupBy(g => new { g.SEC_CODE, g.SEC_NAME }).Select(x =>
                //      new
                //      {
                //          SEC_CODE = x.First().SEC_CODE,
                //          SEC_NAME = x.First().SEC_NAME
                //      }).OrderBy(x => x.SEC_CODE).ThenBy(x => x.SEC_NAME);

                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }
        public BaseJsonResult GetPositionBO()
        {
            Begin();
            try
            {
                var datas = dao.GetPositionDAO().Where(x => !string.IsNullOrEmpty(x.POST_NAME));

                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GetResponsibilityBO()
        {
            Begin();
            try
            {
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = "RESPONSIBILITY";
                m.SUB_CATEGORY = "USERROLE_CODE";
                m.CODE = null;
                var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y" && !string.IsNullOrEmpty(x.CODE)).OrderBy(x => x.REMARKS);

                return Result(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }



        }
        public SearchJsonResult SearchUserProfile(string _User, WFD01210SearchModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetSearchUserProfileDAO(data, ref page);
                datas.ForEach(delegate (WFD01210EmployeeModel item)
                {
                    item.EncodeMapping = Base64Encode(string.Format("{0}|{1}", _User, item.SYS_EMP_CODE));
                });
                if (datas.Count == 0)
                {
                    AddMessage("MSTD0059AERR");
                }

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public bool IsAllowEditByManager(string base64EncodedData, string _user)
        {
            if (string.IsNullOrEmpty(base64EncodedData))
                return false;

            try
            {
                var _s = Base64Decode(base64EncodedData);
                return _s.Split('|')[0] == _user;

            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public string GetSignaturePath()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "SIGNATURE_PATH";
            m.CODE = "LFD02420"; //Change From WFD01210 to LFD02420 By Surasith T. 2017-05-11 : common path with report
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }
            //2017-05-12 Surasith T : Incase remain \ js have an error
            if (path.EndsWith("\\"))
            {
                path = path.Remove(path.LastIndexOf("\\"));
            }
            return path;
        }
        public string GetImageDefalut()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_DEFAULT_UPLOAD";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }
        public SearchJsonResult GetCCBaseOnOrganizeBO(WFD01210SearchModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetCCBaseOnOrganizeDAO(data, ref page);
                //if (datas.Count == 0)
                //{
                //    AddMessage("MSTD0059AERR");
                //}

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }
        public BaseJsonResult DeleteEmployeeBO(WFD01210SearchModel data, string UserLogin)
        {
            Begin();
            try
            {
                if (!CheckSourceFasDelete(data)) return Result(false);

                dao.DeleteEmployeeDAO(data, UserLogin);
                AddMessage("MSTD0090AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public string GetSPECDIV()
        {
            //string value = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "RESPONSIBILITY";
            m.SUB_CATEGORY = "SPEC_DIV";
            m.CODE = null;
            //var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            //if (data != null)
            //{
            //    value = data.VALUE;
            //}
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y" && x.CODE.StartsWith("1")).OrderBy(x => Convert.ToInt32(x.REMARKS)).ToList();

            m.SUB_CATEGORY = "DISPLAY_SPECIAL_ROLE";
            var dataSpecailRoleDisplay = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").OrderBy(x => Convert.ToInt32(x.REMARKS)).ToList();

            string _fSeq = string.Empty;
            foreach (var _item in data)
            {
                var display = _item.VALUE;
                if (dataSpecailRoleDisplay != null)
                {
                    var found = dataSpecailRoleDisplay.Where(o => o.CODE == _item.VALUE).FirstOrDefault();
                    if (found != null)
                    {
                        display = found.VALUE;
                    }
                }

                _fSeq += string.Format("{0}|{1},", _item.CODE, display);
            }
            return _fSeq;
        }
        public BaseJsonResult GetCostCenterBO(WFD01210SearchModel data)
        {
            Begin();
            try
            {
                return Result(dao.GetCostCenterDAO(data).Where(x => x.STATUS == "Y"));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public List<TB_M_COST_CENTER> GetCostCenterBO()
        {
            Begin();
            try
            {
                return dao.GetCostCenterDAO(null).Where(x => x.STATUS == "Y").ToList();
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }
        public BaseJsonResult GetEmployeeByCodeBO(WFD01210SearchModel data, bool _includesignature)
        {
            Begin();
            try
            {
                WFD01210EmployeeDetailModel emp = new WFD01210EmployeeDetailModel();
                emp = dao.GetEmployeeByCodeDAO(data);
                if (_includesignature)
                {
                    emp.SIGNATURE_PATH = ConvertImagePathToByte(GetSignaturePath() + @"\" + emp.SIGNATURE_PATH);
                }
                return Result(emp);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }



        }
        //public BaseJsonResult GetSignature(WFD01210SearchModel data)
        //{
        //    Begin();
        //    try
        //    {
        //        WFD01210EmployeeDetailModel emp = new WFD01210EmployeeDetailModel();
        //        emp = dao.GetEmployeeByCodeDAO(data);
        //        return Result(ConvertImagePathToByte(GetSignaturePath() + @"\" + emp.SIGNATURE_PATH));
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null); 
        //    }

        //}
        public string ConvertImagePathToByte(string ImagePath)
        {

            string base64Image = string.Empty;
            if (File.Exists(ImagePath))
            {
                string extension = string.Empty;
                extension = Path.GetExtension(ImagePath).Replace(".", "");

                byte[] imageArray = System.IO.File.ReadAllBytes(ImagePath);


                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);
            }
            return base64Image;
        }

        public bool CheckSourceFasDelete(WFD01210SearchModel data)
        {
            bool isBool = true;
            //check source
            if (!string.IsNullOrEmpty(data.EMP_CODE))
            {
                var datas = dao.GetEmployeeByCodeDAO(data);
                if (datas != null)
                {
                    if (datas.SOURCE != "FAS")
                    {
                        isBool = false;
                        AddMessage("MCOM0009AERR", " to delete data from HRMS.");
                    }

                    if (datas.UPDATE_DATE.ToString() != data.UPDATE_DATE)
                    {
                        AddMessage("MCOM0008AERR");
                        isBool = false;

                    }
                }

            }
            else
            {
                isBool = false;
                AddMessage("MCOM0004AERR", "Delete");
            }
            return isBool;
        }
        public BaseJsonResult GetDelegateUserBO(WFD01210SearchModel data)
        {
            Begin();
            try
            {
                return Result(dao.GetDelegateUserDAO(data));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }



        }

        //public BaseJsonResult GetAECUserBO(WFD01210SearchModel data)
        //{
        //    Begin();
        //    try
        //    {
        //        return Result(dao.GetAECUserDAO(data));
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null);
        //    }
        //}

        public List<WFD01210DelegateUserModel> GetDelegateUserCBB(WFD01210SearchModel data)
        {
            try
            {
                return dao.GetDelegateUserDAO(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public List<WFD01210AECUserModel> GetAECUserCBB(WFD01210SearchModel data)
        {
            try
            {
                return dao.GetAECUserDAO(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public BaseJsonResult ConvertPathImg(string PATH)
        {
            Begin();
            try
            {
                PATH = GetSignaturePath() + @"\" + PATH;
                string base64Image = string.Empty;
                if (File.Exists(PATH))
                {
                    string extension = string.Empty;
                    extension = Path.GetExtension(PATH).Replace(".", "");

                    byte[] imageArray = System.IO.File.ReadAllBytes(PATH);
                    string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                    base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);
                }

                //string data = "";
                //if (!string.IsNullOrEmpty(PATH))
                //{
                //    byte[] imageArray = System.IO.File.ReadAllBytes(PATH);
                //    string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                //    data = string.Format("data:image/*;base64,{0}", base64ImageRepresentation);
                //}
                return Result(base64Image);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }
        public BaseJsonResult InsertEmployeeBO(WFD01210SaveEmployeeModel data, string UserLogin, HttpRequestBase Request = null)
        {
            Begin();
            try
            {
                if (!Validation(data, "ADD", Request)) return Result(false);
                if (Request.Files.Count > 0)
                {
                    string fname;
                    string file_name;
                    string extension;
                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = data.FILE.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = data.FILE.FileName;
                    }
                    extension = Path.GetExtension(fname);
                    file_name = data.EMP_CODE + extension;
                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(GetSignaturePath(), file_name);
                    data.FILE.SaveAs(fname);

                    data.SIGNATURE_PATH = file_name;
                }
                else
                {
                    data.SIGNATURE_PATH = null;
                }

                dao.InsertEmployee(data, UserLogin);

                AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }

        public BaseJsonResult InsertInChargeCostCodeBO(WFD01210SaveEmployeeModel data, string UserLogin, HttpRequestBase Request = null)
        {
            try
            {
                dao.InsertInChargeCostCenterDAO(data, UserLogin);
                //AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }

        public BaseJsonResult InsertInChargeResCostCodeBO(WFD01210SaveEmployeeModel data, string UserLogin, HttpRequestBase Request = null)
        {
            try
            {
                dao.InsertInChargeResCostCenterDAO(data, UserLogin);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }

        //public BaseJsonResult UpdateInChargeCostCodeBO(WFD01210SaveEmployeeModel data, string UserLogin, HttpRequestBase Request = null)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(data.IN_CHARGE_COST_CODE))
        //        {
        //            WFD01210SearchModel datas = new WFD01210SearchModel();
        //            datas.EMP_CODE = data.EMP_CODE;
        //            dao.DeleteEmployeeSVCostCenterDAO(datas);

        //            string[] arr_cc = data.IN_CHARGE_COST_CODE.Split(',');
        //            foreach (var item in arr_cc)
        //            {
        //                //dao.InsertInChargeCostCenterDAO(item, data.EMP_CODE, UserLogin);
        //            }
        //        }

        //        return Result(true);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null);
        //    }
        //}

        //public BaseJsonResult UpdateInChargeResCostCodeBO(WFD01210SaveEmployeeModel data, string UserLogin, HttpRequestBase Request = null)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(data.IN_RESPONSE_CHARGE_COST_CODE))
        //        {
        //           // dao.InsertInChargeResCostCenterDAO(string.Empty, data.EMP_CODE, UserLogin, "Y");

        //            string[] arr = data.IN_RESPONSE_CHARGE_COST_CODE.Split(',');
        //            foreach (var item in arr)
        //            {
        //               // dao.InsertInChargeResCostCenterDAO(item, data.EMP_CODE, UserLogin, "N");
        //            }
        //        }
        //        return Result(true);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null);
        //    }
        //}

        public BaseJsonResult UpdateEmployeeBO(WFD01210SaveEmployeeModel data, string UserLogin, HttpRequestBase Request = null)
        {
            Begin();
            try
            {
                if (!Validation(data, "EDIT", Request)) return Result(false);
                if (Request.Files.Count > 0)
                {
                    string fname;
                    string file_name;
                    string extension;
                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = data.FILE.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];

                    }
                    else
                    {
                        fname = data.FILE.FileName;

                    }
                    extension = Path.GetExtension(fname);
                    file_name = data.EMP_CODE + extension;
                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(GetSignaturePath(), file_name);
                    if (!Directory.Exists(Path.GetDirectoryName(fname)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(fname));
                    }
                    data.FILE.SaveAs(fname);

                    data.SIGNATURE_PATH = file_name;
                }
                else if (data.UPDATE_SIGNATURE_PATH_FLAG == "N")
                {
                    data.SIGNATURE_PATH = "#";
                }
                else
                {
                    data.SIGNATURE_PATH = null;
                }


                dao.UpdateEmployee(data, UserLogin);

                //AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }

        private bool Validation(WFD01210SaveEmployeeModel data, string MODE, HttpRequestBase Request = null)
        {
            bool isBool = true;
            bool check_date = true;
            if (string.IsNullOrWhiteSpace(data.EMP_CODE))
            {
                AddMessage("MSTD0031AERR", "Employee ID");
                isBool = false;
            }

            if (string.IsNullOrEmpty(data.COMPANY))
            {
                AddMessage("MSTD0031AERR", "Company");
                isBool = false;
            }

            if (MODE == "ADD")
            {
                if (!string.IsNullOrWhiteSpace(data.EMP_CODE) && !string.IsNullOrWhiteSpace(data.COMPANY))
                {
                    WFD01210SearchModel search = new WFD01210SearchModel();
                    search.EMP_CODE = string.Format("{0}.{1}", data.COMPANY.Trim(), data.EMP_CODE.Trim());
                    var result = dao.GetEmployeeByCodeDAO(search);
                    if (result != null)
                    {
                        AddMessage("MSTD0010AERR", "Employee ID", data.EMP_CODE, "Employee Master");
                        isBool = false;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(data.POST_CODE))
            {
                Regex pattern = new Regex(@"^[a-zA-Z0-9]{1,4}$");
                if (!pattern.IsMatch(data.POST_CODE.Trim()))
                {
                    AddMessage("MSTD0043AERR", "Position Code");
                    isBool = false;
                }
            }
            //if (!string.IsNullOrWhiteSpace(data.POST_NAME))
            //{
            //    Regex pattern = new Regex(@"^[a-zA-Z0-9_ ]{1,50}$");
            //    if (!pattern.IsMatch(data.POST_NAME.Trim()))
            //    {
            //        AddMessage("MSTD0043AERR", "Position Name");
            //        isBool = false;
            //    }
            //}
            if (!string.IsNullOrWhiteSpace(data.EMAIL))
            {
                Regex pattern = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                if (!pattern.IsMatch(data.EMAIL.Trim()))
                {
                    AddMessage("MSTD0043AERR", "Email");
                    isBool = false;
                }
            }
            if (!string.IsNullOrEmpty(data.DELEGATE_FROM) || !string.IsNullOrEmpty(data.DELEGATE_TO) || !string.IsNullOrEmpty(data.DELEGATE_PIC))
            {
                if (string.IsNullOrEmpty(data.DELEGATE_PIC))
                {
                    AddMessage("MSTD0031AERR", "Delegation");
                    isBool = false;
                }

                if (!string.IsNullOrEmpty(data.DELEGATE_FROM))
                {
                    if (!ValidFormat(data.DELEGATE_FROM))
                    {
                        AddMessage("MSTD0047AERR", "Delegate From", "dd.MM.yyyy");
                        isBool = false;
                        check_date = false;
                    }

                }
                else
                {
                    AddMessage("MSTD0031AERR", "Delegation From");
                    isBool = false;
                }

                if (!string.IsNullOrEmpty(data.DELEGATE_TO))
                {
                    if (!ValidFormat(data.DELEGATE_TO))
                    {
                        AddMessage("MSTD0047AERR", "Delegate To", "dd.MM.yyyy");
                        isBool = false;
                        check_date = false;
                    }

                }
                else
                {
                    AddMessage("MSTD0031AERR", "Delegation To");
                    isBool = false;
                }

                if (!string.IsNullOrEmpty(data.DELEGATE_FROM) && !string.IsNullOrEmpty(data.DELEGATE_TO))
                {
                    if (check_date)
                    {
                        if (!ValidateDate((DateTime)Date(data.DELEGATE_FROM), (DateTime)Date(data.DELEGATE_TO)))
                        {
                            AddMessage("MSTD0022AERR", "Delegate From", "Delegate To");
                            isBool = false;
                        }

                    }

                }
            }

            if (string.IsNullOrEmpty(data.EMP_TITLE))
            {
                AddMessage("MSTD0031AERR", "Title");
                isBool = false;
            }
            else
            {
                Regex pattern = new Regex(@"^[a-zA-Z0-9\.]{1,30}$");
                if (!pattern.IsMatch(data.EMP_TITLE.Trim()))
                {
                    AddMessage("MSTD0043AERR", "Title");
                    isBool = false;
                }
            }
            if (string.IsNullOrEmpty(data.EMP_NAME))
            {
                AddMessage("MSTD0031AERR", "First Name");
                isBool = false;
            }
            //else  K.Sukanya request 2019.10.17
            //{
            //    Regex pattern = new Regex(@"^[a-zA-Z0-9\-]{1,30}$");
            //    if (!pattern.IsMatch(data.EMP_NAME.Trim()))
            //    {
            //        AddMessage("MSTD0043AERR", "First Name");
            //        isBool = false;
            //    }
            //}

            if (string.IsNullOrEmpty(data.EMP_LASTNAME))
            {
                AddMessage("MSTD0031AERR", "Last Name");
                isBool = false;
            }
            //else Remove follow K.Sukanya request 2019.10.17
            //{
            //    Regex pattern = new Regex(@"^[a-zA-Z0-9\-]{1,30}$");
            //    if (!pattern.IsMatch(data.EMP_LASTNAME.Trim()))
            //    {
            //        AddMessage("MSTD0043AERR", "Last Name");
            //        isBool = false;
            //    }
            //}
            if (string.IsNullOrEmpty(data.COST_CODE))
            {
                AddMessage("MSTD0031AERR", "Cost Center");
                isBool = false;
            }
            if (string.IsNullOrEmpty(data.ORG_CODE))
            {
                AddMessage("MSTD0031AERR", "Organize Code");
                isBool = false;
            }
            else
            {
                var _filter = new WFD01210SearchModel() { ORG_CODE = data.ORG_CODE.Trim(), COMPANY = data.COMPANY };
                var _ref = new PaginationModel() { CurrentPage = 1, ItemPerPage = 10 };

                var datas = dao.SearchtOrganizationDAO(_filter, ref _ref);
                if (datas == null || datas.Count == 0)
                {
                    AddMessage("MSTD0012AERR", data.ORG_CODE, "Organize Master");
                    isBool = false;
                }

            }

            if (Request.Files.Count > 0)
            {
                int size = GetFileSize();
                if ((data.FILE.ContentLength / 1024) > size)
                {
                    AddMessage("MSTD7013BERR", data.FILE.FileName, size + "KB maximum", "", "");
                    isBool = false;
                }
            }

            //Check is FA special team -> FAAdmin must cheked
            //if (!string.IsNullOrEmpty(data.SPEC_DIV))
            //{
            //    SystemDAO sysDAO = new SystemDAO();
            //    var SPEC_DIV_SYS = sysDAO.SelectSystemDatas(SYSTEM_CATEGORY.RESPONSIBILITY, SYSTEM_SUB_CATEGORY.SPEC_DIV).ToList();
            //    if (!string.IsNullOrEmpty(data.FAADMIN) && data.FAADMIN != "Y")
            //    {
            //        foreach (string sd in data.SPEC_DIV.Split(','))
            //        {
            //            if (SPEC_DIV_SYS.Where(x => x.CODE.StartsWith("FA")).Select(x => x.CODE).Contains(sd))
            //            {
            //                AddMessage("MFAS2001AERR");
            //                isBool = false;
            //                break;
            //            }
            //        }
            //    }
            //}

            //if (!string.IsNullOrEmpty(data.IN_CHARGE_COST_CODE))
            //{
            //    var dt = dao.CheckSVCostCenterDAO(data.IN_CHARGE_COST_CODE, data.EMP_CODE);

            //    if ((dt != null) && (dt.Count > 0))
            //    {
            //        AddMessage("MFAS0401AERR", data.IN_CHARGE_COST_CODE);
            //        isBool = false;
            //    }

            //}
            if (MODE == "EDIT")
            {
                WFD01210SearchModel search = new WFD01210SearchModel();
                search.EMP_CODE = data.EMP_CODE.Trim();
                var UPDATE = dao.GetEmployeeByCodeDAO(search);
                if (UPDATE != null)
                {
                    if (UPDATE.UPDATE_DATE.ToString() != data.UPDATE_DATE)
                    {
                        AddMessage("MCOM0008AERR");
                        isBool = false;

                    }
                }
            }
            return isBool;
        }
        private int GetFileSize()
        {
            int size = 0;
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "FILE_SIZE";
            m.CODE = "WFD01210";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                size = Int32.Parse(data.VALUE);
            }

            return size;
        }

        public bool ValidFormat(string _text)
        {

            DateTime dateValue;
            return DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue);

        }
        public DateTime? Date(string _text)
        {

            DateTime dateValue;
            if (DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue))
            {
                return DateTime.ParseExact(_text, _format, cultureInfo);
            }
            else
            {
                return null;
            }

        }
        public SearchJsonResult SearchtOrganizationBO(WFD01210SearchModel data, PaginationModel page)
        {
            Begin();
            try
            {
                data.ORG_CODE = null; //Add By Surasith T. 2017-05-17 no filter by Org Code

                var datas = dao.SearchtOrganizationDAO(data, ref page);
                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }

        public WFD01210EmployeeDetailModel GetEmpDetail(string SYS_EMP_CODE)
        {
            string Org = "";
            WFD01210SearchModel data = new WFD01210SearchModel();
            data.EMP_CODE = SYS_EMP_CODE;
            var result = dao.GetEmployeeByCodeDAO(data);
            if (result == null)
            {
                //Org = result.ORG_CODE;
                result = new WFD01210EmployeeDetailModel();
            }
            return result;
        }

        public BaseJsonResult GetInChargeBO(WFD01210SearchModel data, string _Login)
        {
            Begin();
            try
            {
                //Left side of screen
                if (data.IS_RESPONSE_COST_CENTER == "Y")
                {
                    return Result(dao.GetResponsibleInChargeDAO(data, _Login));
                }
                return Result(dao.GetInChargeDAO(data, _Login));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetInChargeTableBO(WFD01210SearchModel data, string _Login)
        {
            Begin();
            try
            {
                //Selected (right side)
                return Result(dao.GetInChargeTableDAO(data, _Login));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetCheckSetUpInChargeBO(WFD01210SearchModel data)
        {
            Begin();
            string check = "N";
            try
            {
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = "RESPONSIBILITY";
                m.SUB_CATEGORY = "IN_CHARGE_CC";
                m.CODE = null;
                var result = system_dao.GetSystemData(m).Where(x =>
                                                            x.ACTIVE_FLAG == "Y"
                                                            && x.VALUE == "Y"
                                                            && x.CODE == data.RESPONSIBILITY
                                                            ).OrderBy(x => x.REMARKS);
                if (result.Count() > 0)
                {
                    check = "Y";
                }
                return Result(check);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }



        }

        public List<Models.CostCenterAutoCompleteModels> GetCostCenterAutoComplete(string _keyword, string _EmpCode, string _IsFASup)
        {
            Begin();
            try
            {
                if (string.IsNullOrWhiteSpace((_keyword)))
                {
                    return new List<Models.CostCenterAutoCompleteModels>();
                }

                var _rs = dao.GetCostCenterAutoComplete(_keyword, _EmpCode, _IsFASup);
                if (_rs == null)
                {
                    return new List<Models.CostCenterAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.CostCenterAutoCompleteModels>();
            }
        }


        public string GetAllowSearchBO(string _Company, string _SysEmpCode)
        {
            Begin();

            try
            {

                return dao.GetAllowSearchDAO(_Company, _SysEmpCode); ;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }


        }

        public void InitializeCostcenterPopup(string _Company, string _SysEmpCode, string _LoginUser)
        {
            Begin();
            try
            {
                dao.InitializeCostCenterPopup(_SysEmpCode, _Company, _LoginUser);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }
        }

    }
}
