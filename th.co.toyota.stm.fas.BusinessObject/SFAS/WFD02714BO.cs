﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02310;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.DAO.SFAS;
using th.co.toyota.stm.fas.Models.SFAS.WFD02714;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02714BO
    {

        private WFD02714DAO dao;
        public WFD02714BO()
        {
            dao = new WFD02714DAO();
        }

        public bool ValidateUser(Log4NetFunction _log, string _UserName, string _Password)
        {
            var _sys = (new SystemBO().SelectSystemDatas("SYSTEM_CONFIG", "SAP_RESPONSE_AUC_INFO"));
            if (_sys.Find(x => x.CODE.ToUpper() == "USERNAME") == null)
                return false;

            if (_sys.Find(x => x.CODE.ToUpper() == "PASSWORD") == null)
                return false;

            var _user = _sys.Find(x => x.CODE.ToUpper() == "USERNAME").VALUE;
            var _pwd = _sys.Find(x => x.CODE.ToUpper() == "PASSWORD").VALUE;

            _log.WriteInfoLogFile("--------------------");
            _log.WriteInfoLogFile(_user);
            _log.WriteInfoLogFile(_pwd);

            if (_user == _UserName && _pwd == _Password)
            {
                return true ;
            }
            return false;


        }

        public void UpdateAUCInfo(List<WFD02714ResponseAUCFromSAPModel> _list)
        {
            Log4NetFunction _f = new Log4NetFunction();
            try
            {
                _f.WriteInfoLogFile("Get AUC Info");
                var _json = Newtonsoft.Json.JsonConvert.SerializeObject(_list);
                _f.WriteInfoLogFile(_json);
                dao.UpdateAUCInfo(_list);
                //Update Log
                _f.WriteInfoLogFile("Update successfully");
            }
            catch (Exception ex)
            {
                _f.WriteErrorLogFile(ex.Message, ex);
            }
        }
        
    }
}
