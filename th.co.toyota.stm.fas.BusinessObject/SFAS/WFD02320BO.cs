﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD02320;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02320BO : BO
    {
        private WFD02320DAO dao;
        private SystemConfigurationDAO system_dao;
        public WFD02320BO()
        {
            dao = new WFD02320DAO();
            system_dao = new SystemConfigurationDAO();
        }


        public SearchJsonResult Search(WFD02320SearchModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.Search(data, ref page);
                if (datas.Count > 0)
                {
                    return Result(datas, page);
                }
                else
                {
                    AddMessage("MSTD0059AERR");
                    return Result(null, page);
                }

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }

        //public List<WFD02320PrintQDialogModel> GetPrintDetail(string costcode, string createby, string find, string printall)
        public WFD02320PrintQDCModel GetPrintDetail(string company, string costcode, string resp_cost_code, string createby, string find, string printall)
        {
            Begin();
            try
            {
                WFD02320PrintQDCModel result = dao.GetPrintDetail(company, costcode, resp_cost_code, createby, find, printall);
                return result;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return null;
            }
        }
        //public List<SystemConfigurationModels> GetPrintChange(string BARCODE_SIZE)
        //{
        //    Begin();
        //    try
        //    {
        //        return dao.GetPrintChange(BARCODE_SIZE);

        //    }
        //    catch (Exception ex)
        //    {
        //        AddMessage("Error", ex.Message, MessageStatus.ERROR);
        //        return null;
        //    }

        //}
    }
}
