﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using System.IO;
using Ionic.Zip;
namespace th.co.toyota.stm.fas.BusinessObject
{
    public class ComUploadBO : BO
    {
        private ComUploadDAO dao;
        public ComUploadBO()
        {
            dao = new ComUploadDAO();
        }
        public BaseJsonResult ValidateUploadFile(HttpRequestBase Request = null)
        {
            Begin();
            try
            {
                var _bo = CheckMultiUploadFile(Request);

                return Result(_bo);

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }

        public BaseJsonResult UploadFileAttchDoc(HttpRequestBase Request = null)
        {
            Begin();
            SystemBO bo = new SystemBO();
            string _DocNo = "";
            string _fileName = "";
            HttpPostedFileBase _file;
            string _type = "";
            string _newFileName = "";
            List<FileDownloadModel> _arrFile = new List<FileDownloadModel>();
            try
            {
                if (Request.Files.Count == 0)
                {
                    return Result(new { data = string.Empty });
                }

                string _cfgUpload = string.Format("{0}_{1}", Request.Form["FUNCTION_ID"], Request.Form["TYPE"]);

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    _file = Request.Files[i];
                    _type = Request.Form["TYPE"];
                    _DocNo = Request.Form["DOC_NO"];
                    _fileName = _type + i.ToString();
                    _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmssfff}{2}", _fileName, DateTime.Now, Util.GetExtension(_file.FileName));
                    UploadDocument(_file, _cfgUpload, _newFileName);
                    FileDownloadModel _FileResult = new FileDownloadModel();
                    _FileResult.COMPANY = Request.Form["COMPANY"];
                    _FileResult.DOC_NO = Request.Form["DOC_NO"];
                    _FileResult.ASSET_NO = Request.Form["ASSET_NO"];
                    _FileResult.ASSET_SUB = Request.Form["ASSET_SUB"];
                    _FileResult.TYPE = Request.Form["TYPE"];
                    _FileResult.GUID = Request.Form["GUID"];
                    _FileResult.FUNCTION_ID = Request.Form["FUNCTION_ID"];
                    _FileResult.FILE_NAME = _newFileName;
                    _FileResult.FILE_TYPE = Util.GetExtension(_file.FileName).Replace(".","");
                    _FileResult.FILE_NAME_ORIGINAL = _file.FileName;
                    _FileResult.FILE_ID = _newFileName.Replace(Util.GetExtension(_file.FileName),""); 
                    _FileResult.PATH_FILE_TYPE = bo.GetPathFileType(_FileResult.FILE_TYPE);
                    _FileResult.FILE_PATH_DOWNLOAD = string.Format("/{0}/DownloadFileTemp?FILENAME={1}&FunctionID={0}&Type={2}", Request.Form["FUNCTION_ID"], _newFileName, Request.Form["TYPE"]);
                    _FileResult.DOC_UPLOAD = _cfgUpload;
                    _FileResult.LINE_NO = Request.Form["LINE_NO"];
                    _arrFile.Add(_FileResult);                    
                }

                return Result(new { data = _arrFile });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new { data = string.Empty });
            }
        }
        public BaseJsonResult GetFileAttchDoc(FileDownloadModel data)
        {
            Begin();         

            try
            {
                 var _return = dao.GetFileAttchDoc(data);
                return Result(new { data = _return });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                var _return = new List<FileDownloadModel>();
                return Result(new { data = _return });
            }
        }
        public BaseJsonResult SaveToZipFileAttchDoc(FileDownloadModel data)
        {
            Begin();

            try
            {
                var _return = dao.GetFileAttchDoc(data);
                string _targetFileNameZip = ""; 
                string zipFullPath = ""; 

                if (_return != null && _return.Count > 0)
                {
                    _targetFileNameZip = string.Format("DOC_NO_{0}{1:yyyyMMdd_HHmmssFFF}{2}", _return[0].DOC_NO != null ? _return[0].DOC_NO.Replace("/","") : "", DateTime.Now, ".zip");
                    zipFullPath = Path.Combine(_return[0].FOLDER_DIRECTORY, _targetFileNameZip);
                    using (ZipFile zip = new ZipFile(zipFullPath))
                    {
                        for (int i = 0; i < _return.Count; i++)
                        {
                            zip.AddFile(string.Format("{0}\\{1}", _return[i].FOLDER_DIRECTORY, _return[i].FILE_NAME), @".\");
                        }
                        zip.Save();
                    }
                    return Result(new { data = string.Format("{0}\\{1}", _return[0].FOLDER_DIRECTORY, _targetFileNameZip) }); 
                }
                return Result(new { data = _return });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                var _return = new List<FileDownloadModel>();
                return Result(new { data = _return });
            }
        }


        public BaseJsonResult AssetDeleteFileAttchDoc(HttpRequestBase Request = null)
        {
            Begin();
            SystemBO bo = new SystemBO();

            try
            {
                if (Request.Form["TEMP_FILE_DOC_NAME"] == null)
                {
                    return Result(true);
                }

                string _cfgUpload = string.Format("{0}_{1}", Request.Form["FUNCTION_ID"], Request.Form["TYPE"]);

                string _tempFile = Request.Form["TEMP_FILE_DOC_NAME"];

                //No delete for boi
                if(Request.Form["TYPE"] != "BOI")
                    DeleteDocument(_cfgUpload, _tempFile);

                FileDownloadModel _FileResult = new FileDownloadModel();
                _FileResult.COMPANY = Request.Form["COMPANY"];
                _FileResult.DOC_NO = Request.Form["DOC_NO"];
                _FileResult.ASSET_NO = Request.Form["ASSET_NO"];
                _FileResult.ASSET_SUB = Request.Form["ASSET_SUB"];
                _FileResult.TYPE = Request.Form["TYPE"];
                _FileResult.GUID = Request.Form["GUID"];
                _FileResult.FUNCTION_ID = Request.Form["FUNCTION_ID"];
                _FileResult.FILE_NAME = Request.Form["FILE_NAME"];
                _FileResult.FILE_TYPE = Request.Form["FILE_TYPE"];
                //_FileResult.FILE_NAME_ORIGINAL = _file.FileName;
                _FileResult.FILE_ID = Request.Form["TEMP_FILE_DOC_NAME"];
                //_FileResult.PATH_FILE_TYPE = bo.GetPathFileType(_FileResult.FILE_TYPE);
                //_FileResult.FILE_PATH_DOWNLOAD = string.Format("/{0}/DownloadFileTemp?FILENAME={1}&FunctionID={0}&Type={2}", Request.Form["FUNCTION_ID"], _newFileName, Request.Form["TYPE"]);
                _FileResult.DOC_UPLOAD = _cfgUpload;
                _FileResult.LINE_NO = Request.Form["LINE_NO"];

                DeleteFileAttchDoc(_FileResult);

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public BaseJsonResult InsertFileAttchDoc(List<FileDownloadModel> data)
        {
            Begin();
            try
            {
                if (data != null)
                {
                    for (int i = 0; i < data.Count; i++)
                    {
                        dao.InsertFileAttchDoc(data[i]);
                    }                   
                }                
                return Result(true);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(false);
            }
        }
        public BaseJsonResult DeleteFileAttchDoc(FileDownloadModel data)
        {
            Begin();
            try
            {
                dao.DeleteFileAttchDoc(data);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(false);
            }
        }
    }
}
