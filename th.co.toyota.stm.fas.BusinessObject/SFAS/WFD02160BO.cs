﻿using System;
using System.Linq;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02160;
using System.Web;
using th.co.toyota.stm.fas.BusinessObject.Common;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D ;
namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02160BO : BO
    {
        private SystemConfigurationDAO system_dao;
        private WFD02160DAO dao;
        private MessageDAO mdao;
        public WFD02160BO()
        {
            dao = new WFD02160DAO();
            system_dao = new SystemConfigurationDAO();
            mdao = new MessageDAO();
        }

        public string GetImageDefalut()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_DEFAULT_UPLOAD";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }
        public string GetImageMaxLength()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_MAX_LENGTH";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }

        public BaseJsonResult UpdateMapLayout(WFD02160MapLocConditionModel _data, HttpRequestBase Request = null)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                if (Request.Files.Count > 0)
                {
                    string fname;

                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = _data.FILE.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = _data.FILE.FileName;
                    }

                     string extension = string.Empty;
                    extension = Path.GetExtension(_data.FILE.FileName);
                    string CostCodeFileName = _data.COST_CODE + extension;

                    // Get the complete folder path and store the file inside it.  
                    fname = Path.Combine(GetPathUpload(), CostCodeFileName);

                    _data.FILE.SaveAs(fname); // upload file 
                    _data.MAP_PATH = CostCodeFileName;
                    dao.UpdateMapLayout(_data);
                }    

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }
        //Check File Size Before Upload
        public BaseJsonResult CheckValidateUploadFile(CheckFileUploadModel data)
        {
            Begin();
            try
            {
                var _bo = CheckUploadFile(data);

                return Result(_bo);


            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }
        public BaseJsonResult DeleteMapLayout(WFD02160MapLocConditionModel _data)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                  dao.DeleteMapLayout(_data);
                // Delete file process
                if (File.Exists(_data.MAP_PATH))
                {
                    File.Delete(_data.MAP_PATH);
                }
                    return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }
            return Result(null);
        }


        private string GetPathUpload()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "MAP_PATH";
            m.CODE = "COST_CENTER";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }

        public BaseJsonResult WFD02610SaveMapLayout(List<WFD02160AssetModel> _data)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                   var valReturn = dao.SaveMapLayout(_data);
                  // AddMessage(Constants.MESSAGE.MSTD0101AINF);
                return Result(valReturn);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }



        public BaseJsonResult ViewMapLocation(WFD02160MapLocConditionModel ds)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(ds, new RV("COST_CODE", "Cost Center"))) return Result(null, null);
                WFD02160ResultModel _m = new WFD02160ResultModel();


                var _CostCodemapOut = dao.ViewMapLocation(ds);
                if (_CostCodemapOut == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                    return Result(_CostCodemapOut);
                }
            
                _m.MapLayOut = _CostCodemapOut;


                if (!string.IsNullOrEmpty(_m.MapLayOut.MAP_PATH) && File.Exists(_m.MapLayOut.MAP_PATH))
                {
                    double _MaxLength = (1.5 * 1024 * 1014);
                    SystemBO _bo = new SystemBO();
                    var _sysList = _bo.SelectSystemDatas("SYSTEM_CONFIG", "FILE_SIZE").Find(x => x.CODE == "JSON_DOWNLOAD_LENGTH") ;
                    if(_sysList != null)
                    {
                        _MaxLength = (Convert.ToDouble(_sysList.VALUE) * 1024 * 1024);
                    }


                    //File size
                    var _fileInfo = new System.IO.FileInfo(_m.MapLayOut.MAP_PATH);

                    long _fs = _fileInfo.Length;

                    // File size > Limit => Convert
                    if (_fs > _MaxLength) //1.5 MB
                    {
                        ImageHandler _img = new ImageHandler();

                        //Converting
                        var _arry = _img.ResizeStream(_m.MapLayOut.MAP_PATH, 50, 1);

                        //Result still greater then Limit
                        if (_arry.Length > _MaxLength)
                        {
                            // Show Default
                            _m.MapLayOut.MAP_PATH = GetImageMaxLength();
                        }
                        else
                        {
                            // Convert Result
                            string extension = Path.GetExtension(_m.MapLayOut.MAP_PATH).Replace(".", "");
                            string base64ImageRepresentation = Convert.ToBase64String(_arry);
                            string base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);

                            _m.MapLayOut.MAP_PATH = base64Image;
                        }
                    }
                    else
                    {
                        //Convert Actual file to JSON
                        _m.MapLayOut.MAP_PATH = Util.ConvertImagePathToByte(_m.MapLayOut.MAP_PATH);
                    }
                }
                else
                {
                    // Show Default Incase no file
                    _m.MapLayOut.MAP_PATH = GetImageDefalut();
                }
                
                _m.AssetLoc  = dao.GetAssetLoc(ds);

                return Result(_m);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult LoadMinorCategory(WFD02160MapLocConditionModel data)
        {
            Begin();
            try
            {
                var datas = dao.GetMinorCategory(data);
                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }


        public BaseJsonResult ViewMapPoint(WFD02160MapLocConditionModel data, bool _fa)
        {
            Begin();
            try
            {
    
                var datas = dao.ViewMapPoint(data, _fa);
         

                return Result(datas);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
       

    }


}
