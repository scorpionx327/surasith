﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02210;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD021A0BO : TFASTRequestBO
    {
        private WFD021A0DAO dao;
        public WFD021A0BO()
        {
            dao = new WFD021A0DAO();
        }

        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (data.COPY_MODE != "Y")
                {
                    dao.InitialAssetList(data);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }
        public List<WFD021A0Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var _permission = (new WFD01170BO()).GetPermission(data);

                var datas = dao.GetAssetList(data, page);

                datas.ForEach(delegate (WFD021A0Model _item)
                {
                    _item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.AllowResend == "Y");
                    //_item.AllowDelete = _item.ALLOW_DELETE == "Y" && (_permission.AllowDelete == "Y");
                    _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y");
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);


                });

                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD021A0Model>();
            }
        }

        public JsonResultBaseModel ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
        }

        public JsonResultBaseModel DeleteAsset(WFD021A0Model data)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
            

        }

    
        public JsonResultBaseModel PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.PrepareCostCenterToGenerateFlow(data); //Waiting to change
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
            
        }
        public JsonResultBaseModel UpdateAssetList(List<WFD021A0Model> list, string _user)
        {
            // Add Quotaion
            return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            //Begin();
            //try
            //{
            //    foreach (var _data in list)
            //        _data.USER_BY = _user;
            //    if(list != null && list.Count > 0)
            //    {
            //        dao.UpdateAsset(list);
            //    }
            //    return new JsonResultBaseModel() { Status = MessageStatus.INFO };

            //}
            //catch (Exception ex)
            //{
            //    AddMessage("Error", ex.Message, MessageStatus.ERROR);
            //    return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            //}
        }

        //Abstract Function
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {

                //Not allow to continue when 
                var _errList = dao.AssetValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _errList = dao.AssetValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {
            
            try
            {
                dao.Submit(_data);
                return true;
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);


                //Get Document Status Incase Current is ready
                
                if (!(new WFD01170DAO()).IsReadyGenFile(_data.RequestHeaderData.DOC_NO))
                {
                    return true;
                }

                //Call Batch & ODBC

                var _batch = new BatchProcessBO();

                var batch_id = _batch.addBatchQ(new StartBatchModel()
                {
                    BatchID = Constants.BatchID.BFD021A3,
                    UserId = _data.RequestHeaderData.USER_BY,
                    Description = string.Format("Generate Asset No Doc No = {0}", _data.RequestHeaderData.DOC_NO)
                });

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

       

       

        public override bool Copy(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.CopyToNewAsset(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _doc = (new WFD01170BO().GetRequestHeader(new WFD0BaseRequestDocModel() { DOC_NO = _data.RequestHeaderData.DOC_NO }));

                dao.Acknowledge(_data, _doc.STATUS == Constants.DocumentStatus.AECClosewithError ? 
                    Constants.DocumentStatus.UserCloseError : Constants.DocumentStatus.Finish);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
