﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02710;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02710BO : TFASTRequestBO
    {
        private WFD02710DAO dao;
        public WFD02710BO()
        {
            dao = new WFD02710DAO();
        }
        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (data.COPY_MODE != "Y")
                {
                    if (string.IsNullOrEmpty(data.DOC_NO))
                    {
                        var _draft = dao.GetDraft(data);
                        if (_draft != null)
                        {
                            data.DOC_NO = _draft.DOC_NO;
                        }
                    }

                    dao.InitialAssetList(data);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }

        public WFD02710FinalModel GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                //Prepare Grouping Data

                var _rowList = new List<WFD02710AUCModel>();
                var _colList = dao.GetTargetAssetList(data);

                var _permission = (new WFD01170BO()).GetPermission(data);


                var datas = dao.GetAssetList(data, page);

                datas.ForEach(delegate (WFD02710AUCModel _item)
                {
                    _item.Expand = true;
                    _item.AllowCheck = _permission.CanCheck(_item.ALLOW_CHECK, _item.STATUS);
                    //_item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y");
                    //_item.AllowDelete = (_item.ALLOW_DELETE == "Y" && (_permission.AllowDelete == "Y")) || _item.COMMON_STATUS != "Y";
                    _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y");
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);
                });

                var _mappList = new List<WFD02710SettlementMappingModel>();

                List<string> _InvoiceKeyGroup = new List<string>();
                //Summary
                foreach (var _assetRow in datas)
                {
                    if (_assetRow.GRP_ROW_INDX > 0)
                        continue;

                    foreach (var _col in _colList)
                    {
                        double total = datas.FindAll(x => x.AUC_GROUP_KEY == _assetRow.AUC_GROUP_KEY &&
                                           x.TARGET_ASSET_NO == _col.ASSET_NO &&
                                           x.TARGET_ASSET_SUB == _col.ASSET_SUB).Sum(x => x.SETTLE_AMOUNT).Value;
                        if (total == 0)
                            continue;

                        _mappList.Add(new WFD02710SettlementMappingModel()
                        {
                            RowKey = _assetRow.INVOICE_GROUP_KEY,
                            ColumnKey = _col.COL_INDX,
                            SETTLE_AMOUNT = total,
                            ADJ_TYPE = _assetRow.ADJ_TYPE,
                        });
                    }
                }


                //Detail
                foreach (var _assetRow in datas)
                {
                    if (!_InvoiceKeyGroup.Contains(_assetRow.INVOICE_GROUP_KEY))
                    {
                        _rowList.Add(_assetRow);
                        _InvoiceKeyGroup.Add(_assetRow.INVOICE_GROUP_KEY);
                    }

                    if (_colList.Exists(x => x.ASSET_NO == _assetRow.TARGET_ASSET_NO &&
                                             x.ASSET_SUB == _assetRow.TARGET_ASSET_SUB))
                    {
                        int _colIndex = _colList.Find(x => x.ASSET_NO == _assetRow.TARGET_ASSET_NO &&
                                                           x.ASSET_SUB == _assetRow.TARGET_ASSET_SUB).COL_INDX;

                        _mappList.Add(new WFD02710SettlementMappingModel()
                        {
                            RowKey = _assetRow.INVOICE_GROUP_KEY,
                            ColumnKey = _colIndex,
                            SETTLE_AMOUNT = _assetRow.SETTLE_AMOUNT,
                            ADJ_TYPE = _assetRow.ADJ_TYPE,
                        });
                    }
                }

                var rowList = new List<WFD02710AUCModel>();

                int aucCount = 0;
                int rowInx = 1;

                //POP REMOVE ROW(GROUP) ONLY ONE IN GROUP
                foreach (var g in _rowList.GroupBy(x => x.AUC_GROUP_KEY))
                {
                    aucCount++;
                    if (g.Count() > 2)
                    {
                        int i = 1;
                        foreach (var item in g)
                        {
                            //NOT COUNT GROUP ROW
                            if (i > 1)
                            {
                                item.ROW_INDX = rowInx;
                                rowInx++;
                            }

                            item.HAS_GROUP = true; //FLAGS HAS GROUP
                            rowList.Add(item);

                            i++;
                        }
                    }
                    else
                    {
                        var item = g.ElementAt(1);
                        item.ROW_INDX = rowInx;
                        rowInx++;

                        rowList.Add(item); //ONLY ADD FIRST IN GROUP
                    }
                }

                var _data = new WFD02710FinalModel();
                //_data.RowList = _rowList;
                _data.RowList = rowList;
                _data.ColumnList = _colList;
                _data.MappingList = _mappList;
                _data.AssetAUCCount = aucCount;

                return _data;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new WFD02710FinalModel();
            }
        }

        public BaseJsonResult ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult DeleteAsset(WFD0BaseRequestDetailModel data, string invNo, string invLine, string userBy)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data, invNo, invLine);
                SaveDraft(new WFD0BaseRequestDocModel() { GUID = data.GUID, USER_BY = userBy });

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }
        public BaseJsonResult DeleteTargetAsset(WFD0BaseRequestDetailModel data)
        {
            Begin();
            try
            {
                dao.DeleteTargetAssets(data);

                this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = data.GUID, USER_BY = data.USER_BY });
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }

        public BaseJsonResult InsertAsset(List<WFD0BaseRequestDetailModel> _list)
        {
            Begin();
            try
            {
                foreach (var _data in _list)
                    dao.InsertAsset(_data);

                if (_list.Count > 0)
                    this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = _list[0].GUID, USER_BY = _list[0].USER_BY });
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }
        public BaseJsonResult InsertExistsToTargetAssets(List<WFD0BaseRequestDetailModel> _list)
        {
            Begin();
            try
            {
                foreach (var _data in _list)
                    dao.InsertExistsToTargetAssets(_data);

                if (_list.Count > 0)
                    this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = _list[0].GUID });

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }
        public BaseJsonResult InsertNewToTargetAssets(WFD02710SaveModel _data)
        {
            Begin();
            try
            {
                dao.InsertNewToTargetAssets(_data);

                this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = _data.GUID });
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }

        public BaseJsonResult PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                //Not allow to continue when 
                var _errList = dao.GenerateFlowValidation(data);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });


                dao.PrepareCostCenterToGenerateFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }

        }
        public BaseJsonResult UpdateTargetAssetList(List<WFD02710TargetModel> list)
        {
            Begin();
            try
            {

                dao.UpdateTargetAsset(list);

                if (list.Count > 0)
                    this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = list[0].GUID });


                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult UpdateSettlements(List<WFD02710AUCModel> datas, string costStr, string _User)
        {
            Begin();
            try
            {
                if (!ValidateSettlement(costStr)) { return Result(null, null, "#AlertMessageWFD02710Cost"); }

                var cost = Double.Parse(costStr);

                double? all = datas.Sum(x => x.AUC_REMAIN) + datas.Sum(x => x.SETTLE_AMOUNT);

                foreach (var data in datas)
                {
                    var perAmount = (data.AUC_REMAIN + data.SETTLE_AMOUNT) / all;
                    var amount = Math.Round(cost * perAmount.Value, 2, MidpointRounding.ToEven);

                    data.SETTLE_AMOUNT = amount;
                }

                var useAmount = datas.Sum(x => x.SETTLE_AMOUNT);
                if (cost - useAmount.Value != 0)
                {
                    datas[0].SETTLE_AMOUNT += (cost - useAmount.Value);
                    datas[0].SETTLE_AMOUNT = Math.Round(datas[0].SETTLE_AMOUNT.Value, 2, MidpointRounding.ToEven);
                }

                foreach (var data in datas)
                {
                    dao.UpdateSettlement(data, _User, false);
                }

                this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = datas[0].GUID });

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message }, null, "#AlertMessageWFD02710Cost");
            }
        }

        //public BaseJsonResult UpdateSettlement(WFD02710AUCModel data, string _User)
        //{
        //    Begin();
        //    try
        //    {
        //        if (!ValidateSettlement(data)) { return Result(null, null, "#AlertMessageWFD02710Cost"); }

        //        if (data.GRP_ROW_INDX > 0)
        //        {
        //            dao.UpdateSettlement(data, _User, false);
        //        }
        //        else
        //        {
        //            dao.UpdateSettlementSummary(data, _User);
        //        }

        //        this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = data.GUID });

        //        return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
        //    }
        //    catch (Exception ex)
        //    {
        //        AddMessage("Error", ex.Message, MessageStatus.ERROR);
        //        return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message }, null, "#AlertMessageWFD02710Cost");
        //    }
        //}

        public BaseJsonResult AverageCosts(List<WFD02710AUCModel> datas, List<WFD02710TargetModel> selectedList, string _User)
        {
            Begin();
            try
            {
                foreach (var data in datas)
                {
                    //Surasith T. 2019-10-26 Incase user average cost (summary), get auc remain before cal.
                    dao.PrepareAverage(data);
                    //End of Modify Surasith T

                    double _avg = Convert.ToDouble((data.AUC_REMAIN / selectedList.Count));  // To be confirm logic
                    var _avgRound = Math.Round(_avg, 2, MidpointRounding.ToEven);
                    double? _rem = data.AUC_REMAIN - (_avgRound * selectedList.Count);
                    var _firstREM = Math.Round(_avgRound + _rem.Value, 2, MidpointRounding.ToEven);
                    foreach (var _selected in selectedList)
                    {
                        data.TARGET_ASSET_NO = _selected.ASSET_NO;
                        //POP : ASSET_SUB TEMP WAS EMPTY STRING 
                        //BUT MVC MODEL BINDING WAS CONVERT EMPTY STRING TO NULL
                        data.TARGET_ASSET_SUB = _selected.ASSET_SUB;

                        if (_rem == 0 && selectedList.Count() > 1)
                        {
                            data.SETTLE_AMOUNT = (data.AUC_REMAIN - _firstREM) / (selectedList.Count - 1);
                        }
                        else
                        {
                            data.SETTLE_AMOUNT = (_firstREM);
                        }

                        data.ADJ_TYPE = "P";
                        if (data.GRP_ROW_INDX > 0)
                            dao.UpdateSettlement(data, _User, true);
                        //else
                        //{
                        //    dao.UpdateSettlementSummary(data, _User);
                        //}
                        _rem = 0;
                    }

                    this.SaveDraft(new WFD0BaseRequestDocModel() { GUID = data.GUID });
                }

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message }, null, "#AlertMessageWFD02710Average");
            }
        }
        private bool IsValidation(WFD01170MainModel _data)
        {
            var _s = dao.AssetValidation(_data);
            this.AddMessageList(_s);
            if (_s.Count > 0)
                return false;

            return true;
        }
        
        //Abstract Function
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(_data);

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.SubmitValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(_data);

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.ApproveValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {

            try
            {
                dao.Submit(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void SaveDraft(WFD0BaseRequestDocModel _data)
        {
            //sp_WFD02710_SaveDraft
            Begin();
            try
            {
                dao.SaveDraft(_data);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        private bool ValidateSettlement(string costStr)
        {
            bool isValid = true;


            if (string.IsNullOrEmpty(costStr))
            {
                AddMessage("MSTD0031AERR", "Amount");
                isValid = false;
            }

            //else if (data.SETTLE_AMOUNT.Value <= 0)
            //{
            //    AddMessage("MSTD0031AERR", "Amount");
            //    isValid = false;
            //}

            return isValid;
        }

        //private bool ValidateSettlement(WFD02710AUCModel data)
        //{
        //    bool isValid = true;
        //    if (!data.SETTLE_AMOUNT.HasValue)
        //    {
        //        AddMessage("MSTD0031AERR", "Amount");
        //        isValid = false;
        //    }
        //    //else if (data.SETTLE_AMOUNT.Value <= 0)
        //    //{
        //    //    AddMessage("MSTD0031AERR", "Amount");
        //    //    isValid = false;
        //    //}

        //    return isValid;
        //}

        //public override bool Resend(WFD01170SelectedAssetNoModel _data)
        //{
        //    Begin();
        //    try
        //    {
        //        dao.Resend(_data);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //public override WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data)
        //{
        //    return new WFD01170UserPermissionModel()
        //    {
        //        AllowRegen = "N",
        //        AllowGenAssetNo = "N"
        //    };
        //}

        public BaseJsonResult Resend(WFD01170MainModel _header, List<WFD0BaseRequestDetailModel> _list, string _user)
        {
            Begin();
            try
            {
                //System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                //if (_list != null && _list.Count > 0)
                //{
                //    foreach (var _item in _list)
                //        _sb.AppendFormat("{0}|", _item.ROW_INDX);


                //    dao.Resend(_header, new WFD01170SelectedAssetNoModel()
                //    {
                //        DOC_NO = _header.RequestHeaderData.DOC_NO,
                //        GUID = _header.RequestHeaderData.GUID,

                //        UPDATE_DATE = _header.RequestHeaderData.UPDATE_DATE,
                //        SelectedList = _sb.ToString(),
                //        USER = _user
                //    });
                //}

                dao.Resend(_header, _list, _user);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult UpdateCapDate(string guid, string docNo, string tempAssetNo, string tempSubNo, string capDateStr)
        {
            Begin();
            try
            {
                var capDate = Date(capDateStr);
                dao.UpdateCapDate(guid, docNo, tempAssetNo, tempSubNo, capDate);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.NONE });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public override bool Copy(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                List<WFD02710TargetModel> activeSettle = dao.getCurrentActiveSettlement(_data);
                if (activeSettle != null)
                {
                    dao.CopyToNewSettlement(_data);
                    return true;
                }
                else
                {
                    throw new Exception("Cannot copy to new request because you have inprogress document.");
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Acknowledge(_data, Constants.DocumentStatus.UserCloseError);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
