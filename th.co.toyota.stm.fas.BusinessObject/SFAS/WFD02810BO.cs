﻿using System;
using System.Collections.Generic;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02210;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02810AOBO : TFASTRequestBO
    {
        private WFD02810DAO dao;
        public WFD02810AOBO()
        {
            dao = new WFD02810DAO();
        }
        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (data.COPY_MODE != "Y")
                {
                    dao.InitialAssetList(data);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }
        public List<WFD02810Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var _permission = (new WFD01170BO()).GetPermission(data);

                var datas = dao.GetAssetList(data, page);

                datas.ForEach(delegate (WFD02810Model _item)
                {
                    _item.AllowCheck = _permission.CanCheck(_item.ALLOW_CHECK, _item.STATUS);
                    //_item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y");

                    //_item.AllowDelete = (_item.ALLOW_DELETE == "Y" && (_permission.AllowDelete == "Y")) || _item.COMMON_STATUS != "Y";
                    _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y");
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);
                });

                if (datas.Count == 0)
                {
                    return new List<WFD02810Model>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD02810Model>();
            }
        }

        public JsonResultBaseModel ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
        }

        public JsonResultBaseModel DeleteAsset(WFD02810Model data)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
            

        }
        private List<WFD02810FieldChangeModel> CompareAsssets(WFD0BaseRequestDocModel data, bool IsAECUser)
        {
            try
            {
                var _fieldList = dao.GetFieldMapping(data);

                var _mappingResultList = new List<WFD02810FieldChangeModel>();

                var _dtOriginal = dao.GetOriginalAsset(new WFD02910Model() { GUID = data.GUID });
                var _dtTarget = dao.GetTargetAsset(new WFD02910Model() { GUID = data.GUID });

                foreach (System.Data.DataRow _dr in _dtOriginal.Rows)
                {
                    //find 
                    string _assetNo = string.Format("{0}", _dr["ASSET_NO"]);
                    string _assetSub = string.Format("{0}", _dr["ASSET_SUB"]);

                    System.Data.DataRow[] _drs = _dtTarget.Select(string.Format("ASSET_NO = '{0}' AND ASSET_SUB = '{1}'", _assetNo, _assetSub));
                    if (_drs.Length == 0)
                        continue;

                    string _lineNo = string.Format("{0}", _drs[0]["LINE_NO"]); ;
                    string assetClassName = string.Format("{0}", _drs[0]["ASSET_CLASS_NAME"]);
                    
                    foreach (System.Data.DataColumn _col in _dtOriginal.Columns)
                    {
                        if (!IsAECUser
                        && (_col.ColumnName.Contains("_15")
                         || _col.ColumnName.Contains("_31")
                         || _col.ColumnName.Contains("_41")
                         || _col.ColumnName.Contains("_81")
                         || _col.ColumnName.Contains("_91")) //If User isn't AEC User, program skip compare new-old
                        )
                        {
                            continue;
                        }

                        if (!assetClassName.Contains("Right-of-use"))
                        {
                            if (_col.ColumnName.Contains("_81") || _col.ColumnName.Contains("_91"))
                            {
                                continue;
                            }
                        }

                        if (!_dtTarget.Columns.Contains(_col.ColumnName))
                        continue;

                        string _original = string.Format("{0}", _dr[_col.ColumnName]);
                        string _new = string.Format("{0}", _drs[0][_col.ColumnName]);

                        if (_original == _new)
                        {
                            continue;

                        }

                        
                        var _f = _fieldList.Find(x => x.COLUMN_NAME == _col.ColumnName);
                        if(_f == null)
                        {
                            _f = new WFD02810FieldChangeModel()
                            {
                                COLUMN_NAME = _col.ColumnName,
                                DISPLAY_NAME = _col.ColumnName,
                                EDIT_TYPE = Constants.AssetInfoChange.NonAccountEditType
                            };
                        }
                        

                        //Changed
                        _mappingResultList.Add(new WFD02810FieldChangeModel()
                        {
                            LINE_NO = _lineNo,
                            ASSET_NO = _assetNo,
                            ASSET_SUB = _assetSub,
                            COLUMN_NAME = _col.ColumnName,
                            DISPLAY_NAME = _f.DISPLAY_NAME,
                            EDIT_TYPE = _f.EDIT_TYPE,
                            OLD = _original,
                            NEW = _new
                        });
                    }

                }
                return _mappingResultList;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public JsonResultBaseModel PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data, bool isAecUser)
        {
            Begin();
            try
            {
                var _mappingList = this.CompareAsssets(data, isAecUser);

                data.FLOW_TYPE = Constants.AssetInfoChange.NonAccountFlow;
                if(_mappingList.FindAll(x => x.EDIT_TYPE == Constants.AssetInfoChange.MixEditType).Count > 0
                    && _mappingList.FindAll(x => x.EDIT_TYPE == Constants.AssetInfoChange.NonAccountEditType).Count == 0)
                {
                    data.FLOW_TYPE = Constants.AssetInfoChange.AccountFlow;
                }

                dao.PrepareCostCenterToGenerateFlow(data); //Waiting to change
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
            
        }
        public JsonResultBaseModel UpdateAssetList(List<WFD02810Model> list, string _user)
        {
            // Add Quotaion
            return new JsonResultBaseModel() { Status = MessageStatus.INFO };
           
        }
        private bool IsValidation(WFD0BaseRequestDocModel _data)
        {
            Begin();
            var _s = dao.CheckValidation(_data);

            this.AddMessageList(_s);
            if (_s.Count > 0)
                return false;

            return true;
        }
        //Abstract Function
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                return this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                return this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }


        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {
            
            try
            {
                var _fieldList = this.CompareAsssets(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID }, _data.IsAECUser);
                dao.Submit(_data, _fieldList );
                return true;
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {

            try
            {
                var _fieldList = this.CompareAsssets(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID }, _data.IsAECUser);
                dao.InsertChangeCommentResubmit(_data, _fieldList);

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //public override bool Resend(WFD01170SelectedAssetNoModel _data)
        //{
        //    Begin();
        //    try
        //    {
        //        dao.Resend(_data);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //public override WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data)
        //{
        //    return new WFD01170UserPermissionModel()
        //    {
        //        AllowRegen = "N",
        //        AllowGenAssetNo = "N"
        //    };
        //}

        public BaseJsonResult Resend(WFD01170MainModel _header, List<WFD02810Model> _data, string _user)
        {
            Begin();
            try
            {
                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                if (_data != null && _data.Count > 0)
                {
                    foreach (var _item in _data)
                        _sb.AppendFormat("{0}|", _item.LINE_NO);

                  
                    dao.Resend(_header,new WFD01170SelectedAssetNoModel()
                    {
                        DOC_NO = _header.RequestHeaderData.DOC_NO,
                        GUID = _header.RequestHeaderData.GUID,
                       
                        UPDATE_DATE = _header.RequestHeaderData.UPDATE_DATE,
                        SelectedList = _sb.ToString(),
                        USER = _user
                    });
                }


                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public override bool Copy(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.CopyToNewInfoChange(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public override bool Acknowledge(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Acknowledge(_data, Constants.DocumentStatus.UserCloseError);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
