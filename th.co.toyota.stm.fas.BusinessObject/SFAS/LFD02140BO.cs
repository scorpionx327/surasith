﻿using DevExpress.XtraReports.UI;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.LFD02140;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using System.Management;
using System.IO;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.WFD02320;
using System.Globalization;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class LFD02140BO : BO
    {
        private LFD02140DAO dao;
        private SystemConfigurationDAO sys_dao;
        private MessageDAO mdao;
        private BatchProcessDAO bdao;
        private string path_report, path_report_cover, logo_path, company_name, header, footer = "";

        IFormatProvider _culture = new CultureInfo("en-US", true);
        public LFD02140BO()
        {
            dao = new LFD02140DAO();
            sys_dao = new SystemConfigurationDAO();
            mdao = new MessageDAO();
            bdao = new BatchProcessDAO();
        }
        public string GetSizeValue(string Value)
        {
            Begin();
            try
            {
                string text = "";
                SystemConfigurationModels size = new SystemConfigurationModels();
                size.CATEGORY = "FAS_TYPE";
                size.SUB_CATEGORY = "BARCODE_SIZE";
                size.CODE = null;
                var data = sys_dao.GetSystemData(size).FirstOrDefault(x => x.VALUE == Value);
                if (data != null)
                {
                    text = data.CODE;
                }
                return text;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return null;

            }

        }

        

        private DateTime ConvertstrToDateTime(string strFormat_yyyyMMdd)
        {
            return DateTime.ParseExact(strFormat_yyyyMMdd, "yyyyMMdd HH:mm:ss.ffffff", _culture); ;
        }

        private List<LFD02140AssetMasterModel> ConvertDataForPrint(string barcodeSize,string PRINT_LOCATION, List<WFD02320PrintQDetailsModel> AssetList)
        {
            List<LFD02140AssetMasterModel> _list = new List<LFD02140AssetMasterModel>();
            LFD02140AssetMasterModel _data = new LFD02140AssetMasterModel();

            for (int i = 0; i < AssetList.Count; i++)
            {
                if(AssetList[i].BARCODE_SIZE == barcodeSize && AssetList[i].PRINT_LOCATION == PRINT_LOCATION)
                { 
                    _data = new LFD02140AssetMasterModel() {
                        COMPANY = AssetList[i].COMPANY,
                        ASSET_NO = AssetList[i].ASSET_NO,
                        ASSET_SUB = AssetList[i].ASSET_SUB,
                        ASSET_NAME = AssetList[i].ASSET_NAME,
                        BARCODE = AssetList[i].BARCODE,
                        BARCODE_SIZE = AssetList[i].BARCODE_SIZE,
                        COST_CODE = AssetList[i].COST_CODE,
                        RESP_COST_CODE = AssetList[i].RESP_COST_CODE,
                        PRINT_COUNT = AssetList[i].PRINT_COUNT,


                        //  ASSET_NO | company_name | DATE_IN_SERVICE | logo_path | ASSET_NAME | PRINT_COUNT | CONTACT | COSTCENTER | INVEST_REASON | SERIAL_NO | TEL | TOTALPRINT | PRINTLOCATION
                        CONTACT = AssetList[i].CONTACT,
                        TEL = AssetList[i].TEL,
                        COSTCENTER = AssetList[i].COSTCENTER,
                        INVEST_REASON = AssetList[i].INVEST_REASON,
                        SERIAL_NO = AssetList[i].SERIAL_NO,
                        TOTALPRINT = AssetList[i].TOTALPRINT,
                        PRINTLOCATION = AssetList[i].PRINTLOCATION,

                    };

                    if (!string.IsNullOrEmpty(AssetList[i].DATE_IN_SERVICE))
                    {
                        _data.DATE_IN_SERVICE = ConvertstrToDateTime(AssetList[i].DATE_IN_SERVICE);
                    }         
                  
                    _list.Add(_data);
                }
            }
            return _list;
        }

        public BaseJsonResult GenerateReport(List<LFD02140RetriveTagPrintOut> SelectedList, List<WFD02320PrintQDetailsModel> AssetList, string emp_code)
        {
            bool isBool = false;
            string msg = "";
            decimal batch_id;
            Begin();

            Log4NetFunction _log = new Log4NetFunction();
  

            StartBatchModel _batch = new StartBatchModel();
            _batch.BatchID = "LFD02140";
            _batch.BatchName = "Print tag Queue";
            _batch.Description = "Print tag Queue process";
            _batch.UserId = emp_code;
            bdao.beginTransaction();
            dao.beginTransaction();
            try
            {
                _log.WriteInfoLogFile("Begin insertBatchNoTransaction");

                batch_id = bdao.insertBatchNoTransaction(_batch);
                _batch.AppID = batch_id;
                _log.WriteInfoLogFile("after insertBatchNoTransaction app_id =" + batch_id);

                if (SelectedList == null)
                {
                    AddMessage("MSTD0059AERR");
                    return Result(null);
                }

               
                bdao.UpdateBatchStatus(batch_id, "P");
                // Start batch process 
                dao.PrintTagProcess(_batch.AppID, "P", "MSTD7000BINF", "Print tag Queue", emp_code, "Y");//insert log

                _log.WriteInfoLogFile("after UpdateBatchStatus is P");
                #region For Selected
                foreach (var p in SelectedList)
                {
                    var barcode = GetSizeValue(p.BARCODE_SIZE);
                    if (!ValidateParameter(p)) return Result(null);
                    path_report = dao.GetPrintTagReport(barcode);
                    path_report_cover = dao.GetPrintTagReportCover(barcode);

                    if (!FileExist(path_report)) return Result(null);
                    if (!FileExist(path_report_cover)) return Result(null);
                    if (!checkPrinter(p.PRINTER_NAME)) return Result(null);

                    string _company = AssetList.Count > 0 ? AssetList[0].COMPANY : string.Empty;

                    if (string.IsNullOrEmpty(logo_path))
                        logo_path = GetLogoPath(_company);
                    if (string.IsNullOrEmpty(company_name))
                        company_name = GetCompanyName(_company);
                    if (string.IsNullOrEmpty(header))
                        header = GetHeader(_company);
                    if (string.IsNullOrEmpty(footer))
                        footer = GetFooter(_company);

                    List<LFD02140AssetMasterModel> asset = new List<LFD02140AssetMasterModel>();
                  //  var data = AssetList.Where(m => m.BARCODE_SIZE == barcode.ToUpper() && m.LOCATION_NAME == p.PRINT_LOCATION).ToList();
                    asset = dao.GetAsset(new LFD02140AssetMasterModel() {
                         COMPANY = _company ,
                         BARCODE_SIZE = p.BARCODE_SIZE,
                         COST_CODE = p.COST_CENTER,
                         PRINTLOCATION = p.PRINT_LOCATION
                    });

                //    asset = ConvertDataForPrint(barcode.ToUpper(), p.PRINT_LOCATION, AssetList);

                    if (asset == null || asset.Count == 0)
                    {
                    }
                    #region Asset matched
                    for (int i=0; i< asset.Count;i++) 
                    {
                       
                        var item = asset[i];
                        
                      // character_qr = GetCharacterQr(item.SUB_TYPE);
                       XtraReport reportx = XtraReport.FromFile(path_report, true);
                       XtraReport reportCover = XtraReport.FromFile(path_report_cover, true);

                        switch (barcode.ToUpper())
                       {
                           case "S":
                                if (i == 0)
                                {
                                    ReportSmallCover(reportCover, item);
                                    using (ReportPrintTool printToolCover = new ReportPrintTool(reportCover))
                                    {
                                        string _f = System.IO.Path.Combine(@"D:\TFAST\ReportPath", string.Format("C{1}_{0:yyMMdd_HHmmss}.pdf", DateTime.Now, item.ASSET_NO));
                                        reportCover.ExportToPdf(_f);

                                        printToolCover.Print(p.PRINTER_NAME);
                                    }
                                }
                                ReportSmall(reportx, item);

                               break;
                           case "M":
                                if (i == 0)
                                {
                                    ReportMiddleCover(reportCover, item);
                                    using (ReportPrintTool printToolCover = new ReportPrintTool(reportCover))
                                    {
                                        string _f = System.IO.Path.Combine(@"D:\TFAST\ReportPath", string.Format("C{1}_{0:yyMMdd_HHmmss}.pdf", DateTime.Now, item.ASSET_NO));
                                        reportCover.ExportToPdf(_f);
                                        printToolCover.Print(p.PRINTER_NAME);
                                    }
                                }

                                ReportMiddle(reportx, item);
                               break;
                           case "L":
                                if (i == 0)
                                {
                                    ReportLargeCover(reportCover, item);
                                    using (ReportPrintTool printToolCover = new ReportPrintTool(reportCover))
                                    {
                                        string _f = System.IO.Path.Combine(@"D:\TFAST\ReportPath", string.Format("C{1}_{0:yyMMdd_HHmmss}.pdf", DateTime.Now, item.ASSET_NO));
                                        reportCover.ExportToPdf(_f);
                                        printToolCover.Print(p.PRINTER_NAME);
                                    }
                                }

                                ReportLarge(reportx, item);
                               break;
                       }
                       

                        // Update Barcode before print by Pawares M. 20180619
                       
                        // Show the report's Print Preview.
                        dao.PrintTagSuccess(item, emp_code); //deleate 
                        dao.PrintTagProcess(_batch.AppID, "P", "MSTD0000AINF", "Print tag of Asset No: " + item.ASSET_NO + "  Cost Center Code : " + p.COST_CENTER, emp_code, "Y");//insert log

                        _log.WriteInfoLogFile("before printTool.Print"+ p.PRINTER_NAME );


                       
                        try
                        {
                            //reportx.CreateDocument();--
                            //reportx.DefaultPrinterSettingsUsing.UsePaperKind = true;--
                            //reportx.ShowPrintMarginsWarning = false;--
                            //reportx.ReportUnit = ReportUnit.HundredthsOfAnInch;
                            //reportx.PageSize = new System.Drawing.Size(240, 100);
                            //reportx.Margins = new System.Drawing.Printing.Margins(1, 1, 1, 1);
                            //reportx.Landscape = false;--



                            using (ReportPrintTool printTool = new ReportPrintTool(reportx))
                            {
                                //printTool.PrinterSettings.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("340x200", 340, 200);
                                //printTool.PrinterSettings.DefaultPageSettings.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
                                //printTool.PrinterSettings.DefaultPageSettings.Landscape = false;
                                // Send the report to the default printer.

                                //Try to export
                                string _f = System.IO.Path.Combine(@"D:\TFAST\ReportPath", string.Format("{1}_{0:yyMMdd_HHmmss}.pdf", DateTime.Now, item.ASSET_NO));
                                reportx.ExportToPdf(_f);


                                printTool.Print(p.PRINTER_NAME);     
                            }

                            isBool = true;
                        }
                        catch(Exception ex)
                        {
                            isBool = false;
                            _log.WriteInfoLogFile("  printTool.Print have exception error" + ex.Message);
                            throw (ex);
                        }
                               
                            _log.WriteInfoLogFile("after printTool.Print" + p.PRINTER_NAME);

                            //Test only
                            /*try
                            {
                                reportx.ExportToPdf(string.Format(@"D:\SFAS\BarCodeReport\{0}_{1:yyyyMMdd_HHmmss}.pdf", item.ASSET_NO, DateTime.Now));
                            }
                            catch (Exception ex)
                            {
                                AddMessage("MSTD0033AERR");
                            }*/

                    }
                    #endregion

                } //End Loop
                #endregion


                if (isBool)
                {
                    dao.PrintTagProcess(_batch.AppID, "S", "MSTD7001BINF", "Print tag Queue", emp_code, "Y");//insert log
                    dao.commitTransaction();

                    bdao.UpdateBatchStatus(batch_id, "S");
                    bdao.commitTransaction();
                    var m = mdao.GetMessage("MSTD7001BINF");
                    if (m != null)
                        msg = string.Format(m.MESSAGE_TEXT, "Tag Barcode Report");
                }
                else
                {
                    dao.rollbackTransaction();
                    bdao.rollbackTransaction();
                  
                }

                dao.closeConnection();
                bdao.closeConnection();
                return Result(new { status = isBool, msg = msg });

            }
            catch (Exception ex)
            {
                    isBool = false;
                _log.WriteInfoLogFile(" catch (Exception ex) : " + ex.Message);
                dao.rollbackTransaction();
                dao.closeConnection();

                bdao.rollbackTransaction();
                bdao.closeConnection();
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null);
            }
            finally
            {
                if (!isBool)
                {
                    _log.WriteInfoLogFile(" finally rollbackTransaction and closeConnection  ");
                    dao.rollbackTransaction();
                    dao.closeConnection();

                    bdao.rollbackTransaction();
                    bdao.closeConnection();
                }
                   
            }
        }
        private XtraReport ReportLarge(XtraReport reportx, LFD02140AssetMasterModel data)
        {
            reportx.Parameters["asset_no"].Value = data.ASSET_NO;
            reportx.Parameters["asset_no"].Visible = false;
            reportx.Parameters["company"].Value = company_name;
            reportx.Parameters["company"].Visible = false;
            reportx.Parameters["date_in_service"].Value = string.Format("{0}", data.DATE_IN_SERVICE.ToString("dd-MM-yy"));
            reportx.Parameters["date_in_service"].Visible = false;
            //reportx.Parameters["footer"].Value = footer;
            //reportx.Parameters["footer"].Visible = false;
            reportx.Parameters["header"].Value = header;
            reportx.Parameters["header"].Visible = false;
            reportx.Parameters["logo"].Value = logo_path;
            reportx.Parameters["logo"].Visible = false;
            reportx.Parameters["name"].Value = data.ASSET_NAME;
            reportx.Parameters["name"].Visible = false;
            reportx.Parameters["print_count"].Value = "[" + data.PRINT_COUNT + "]";
            reportx.Parameters["print_count"].Visible = false;

            reportx.Parameters["Contact"].Value = data.CONTACT;
            reportx.Parameters["Contact"].Visible = false;
            reportx.Parameters["CostCenter"].Value = data.COSTCENTER;
            reportx.Parameters["CostCenter"].Visible = false;
            reportx.Parameters["Invest_Reason"].Value = data.INVEST_REASON;
            reportx.Parameters["Invest_Reason"].Visible = false;
            reportx.Parameters["Serial_No"].Value = data.SERIAL_NO;
            reportx.Parameters["Serial_No"].Visible = false;
            reportx.Parameters["Tel"].Value = data.TEL;
            reportx.Parameters["Tel"].Visible = false;
            reportx.Parameters["TotalPrint"].Value = data.TOTALPRINT;
            reportx.Parameters["TotalPrint"].Visible = false;
            reportx.Parameters["PrintLocation"].Value = data.PRINTLOCATION;
            reportx.Parameters["PrintLocation"].Visible = false;

            if (data.INVEST_REASON == "B")
                reportx.FindControl("lblBOI", true).Visible = true;
            else
                reportx.FindControl("lblBOI", true).Visible = false;

            //if (!string.IsNullOrEmpty(character_qr))
            //{
            //    reportx.FindControl("qr_text", true).Visible = true;
            //    reportx.FindControl("barCode1", true).Visible = false;
            //    reportx.Parameters["qrcode"].Value = character_qr;
            //    reportx.Parameters["qrcode"].Visible = false;
            //}
            //else
            //{
                reportx.Parameters["qrcode"].Value = data.BARCODE;
                reportx.Parameters["qrcode"].Visible = false;
                reportx.FindControl("qr_text", true).Visible = false;
                reportx.FindControl("barCode1", true).Visible = true;
            //}


            return reportx;
        }

        private XtraReport ReportLargeCover(XtraReport reportx, LFD02140AssetMasterModel data)
        {
            reportx.Parameters["asset_no"].Value = data.ASSET_NO;
            reportx.Parameters["asset_no"].Visible = false;
            reportx.Parameters["company"].Value = company_name;
            reportx.Parameters["company"].Visible = false;
            reportx.Parameters["date_in_service"].Value = string.Format("{0}", data.DATE_IN_SERVICE.ToString("dd-MM-yy"));
            reportx.Parameters["date_in_service"].Visible = false;
            //reportx.Parameters["footer"].Value = footer;
            //reportx.Parameters["footer"].Visible = false;
            reportx.Parameters["header"].Value = header;
            reportx.Parameters["header"].Visible = false;
            reportx.Parameters["logo"].Value = logo_path;
            reportx.Parameters["logo"].Visible = false;
            reportx.Parameters["name"].Value = data.ASSET_NAME;
            reportx.Parameters["name"].Visible = false;
            reportx.Parameters["print_count"].Value = "[" + data.PRINT_COUNT + "]";
            reportx.Parameters["print_count"].Visible = false;

            reportx.Parameters["Contact"].Value = data.CONTACT;
            reportx.Parameters["Contact"].Visible = false;
            reportx.Parameters["CostCenter"].Value = data.COSTCENTER;
            reportx.Parameters["CostCenter"].Visible = false;
            reportx.Parameters["Invest_Reason"].Value = data.INVEST_REASON;
            reportx.Parameters["Invest_Reason"].Visible = false;
            reportx.Parameters["Serial_No"].Value = data.SERIAL_NO;
            reportx.Parameters["Serial_No"].Visible = false;
            reportx.Parameters["Tel"].Value = data.TEL;
            reportx.Parameters["Tel"].Visible = false;
            reportx.Parameters["TotalPrint"].Value = data.TOTALPRINT;
            reportx.Parameters["TotalPrint"].Visible = false;
            reportx.Parameters["PrintLocation"].Value = data.PRINTLOCATION;
            reportx.Parameters["PrintLocation"].Visible = false;


            return reportx;
        }

        private XtraReport ReportMiddle(XtraReport reportx, LFD02140AssetMasterModel data)
        {
            reportx.Parameters["asset_no"].Value = data.ASSET_NO;
            reportx.Parameters["asset_no"].Visible = false;
            reportx.Parameters["company"].Value = company_name;
            reportx.Parameters["company"].Visible = false;
            reportx.Parameters["date_in_service"].Value = string.Format("{0}", data.DATE_IN_SERVICE.ToString("dd-MM-yy"));
            reportx.Parameters["date_in_service"].Visible = false;
            reportx.Parameters["header"].Value = header;
            reportx.Parameters["header"].Visible = false;
            //reportx.Parameters["footer"].Value = footer;
            //reportx.Parameters["footer"].Visible = false;
            reportx.Parameters["logo"].Value = logo_path;
            reportx.Parameters["logo"].Visible = false;
            reportx.Parameters["name"].Value = data.ASSET_NAME;
            reportx.Parameters["name"].Visible = false;
            reportx.Parameters["print_count"].Value = "[" + data.PRINT_COUNT + "]";
            reportx.Parameters["print_count"].Visible = false;


            reportx.Parameters["Contact"].Value = data.CONTACT;
            reportx.Parameters["Contact"].Visible = false;
            reportx.Parameters["CostCenter"].Value = data.COSTCENTER;
            reportx.Parameters["CostCenter"].Visible = false;
            reportx.Parameters["Invest_Reason"].Value = data.INVEST_REASON;
            reportx.Parameters["Invest_Reason"].Visible = false;
            reportx.Parameters["Serial_No"].Value = data.SERIAL_NO;
            reportx.Parameters["Serial_No"].Visible = false;
            reportx.Parameters["Tel"].Value = data.TEL;
            reportx.Parameters["Tel"].Visible = false;
            reportx.Parameters["TotalPrint"].Value = data.TOTALPRINT;
            reportx.Parameters["TotalPrint"].Visible = false;
            reportx.Parameters["PrintLocation"].Value = data.PRINTLOCATION;
            reportx.Parameters["PrintLocation"].Visible = false;

            if (data.INVEST_REASON == "B")
                reportx.FindControl("lblBOI", true).Visible = true;
            else
                reportx.FindControl("lblBOI", true).Visible = false;

            //if (!string.IsNullOrEmpty(character_qr))
            //{
            //    reportx.FindControl("qr_text", true).Visible = true;
            //    reportx.FindControl("barCode1", true).Visible = false;
            //    reportx.Parameters["qrcode"].Value = character_qr;
            //    reportx.Parameters["qrcode"].Visible = false;
            //}
            //else
            //{
                reportx.Parameters["qrcode"].Value = data.BARCODE;
                reportx.Parameters["qrcode"].Visible = false;
                reportx.FindControl("qr_text", true).Visible = false;
                reportx.FindControl("barCode1", true).Visible = true;
           // }

            return reportx;
        }

        private XtraReport ReportMiddleCover(XtraReport reportx, LFD02140AssetMasterModel data)
        {
            reportx.Parameters["asset_no"].Value = data.ASSET_NO;
            reportx.Parameters["asset_no"].Visible = false;
            reportx.Parameters["company"].Value = company_name;
            reportx.Parameters["company"].Visible = false;
            reportx.Parameters["date_in_service"].Value = string.Format("{0}", data.DATE_IN_SERVICE.ToString("dd-MM-yy"));
            reportx.Parameters["date_in_service"].Visible = false;
            reportx.Parameters["header"].Value = header;
            reportx.Parameters["header"].Visible = false;
            //reportx.Parameters["footer"].Value = footer;
            //reportx.Parameters["footer"].Visible = false;
            reportx.Parameters["logo"].Value = logo_path;
            reportx.Parameters["logo"].Visible = false;
            reportx.Parameters["name"].Value = data.ASSET_NAME;
            reportx.Parameters["name"].Visible = false;
            reportx.Parameters["print_count"].Value = "[" + data.PRINT_COUNT + "]";
            reportx.Parameters["print_count"].Visible = false;


            reportx.Parameters["Contact"].Value = data.CONTACT;
            reportx.Parameters["Contact"].Visible = false;
            reportx.Parameters["CostCenter"].Value = data.COSTCENTER;
            reportx.Parameters["CostCenter"].Visible = false;
            reportx.Parameters["Invest_Reason"].Value = data.INVEST_REASON;
            reportx.Parameters["Invest_Reason"].Visible = false;
            reportx.Parameters["Serial_No"].Value = data.SERIAL_NO;
            reportx.Parameters["Serial_No"].Visible = false;
            reportx.Parameters["Tel"].Value = data.TEL;
            reportx.Parameters["Tel"].Visible = false;
            reportx.Parameters["TotalPrint"].Value = data.TOTALPRINT;
            reportx.Parameters["TotalPrint"].Visible = false;
            reportx.Parameters["PrintLocation"].Value = data.PRINTLOCATION;
            reportx.Parameters["PrintLocation"].Visible = false;



            return reportx;
        }

        private XtraReport ReportSmall(XtraReport reportx, LFD02140AssetMasterModel data)
        {
            reportx.Parameters["asset_no"].Value = data.ASSET_NO;
            reportx.Parameters["asset_no"].Visible = false;
            reportx.Parameters["Contact"].Value = data.CONTACT;
            reportx.Parameters["Contact"].Visible = false;
            reportx.Parameters["CostCenter"].Value = data.COSTCENTER;
            reportx.Parameters["CostCenter"].Visible = false;
            reportx.Parameters["Tel"].Value = data.TEL;
            reportx.Parameters["Tel"].Visible = false;

            //if (!string.IsNullOrEmpty(character_qr))
            //{
            //    reportx.FindControl("qr_text", true).Visible = true;
            //    reportx.FindControl("barCode1", true).Visible = false;
            //    reportx.Parameters["qrcode"].Value = character_qr;
            //    reportx.Parameters["qrcode"].Visible = false;
            //}
            //else
            //{
                reportx.Parameters["qrcode"].Value = data.BARCODE;
                reportx.Parameters["qrcode"].Visible = false;
                reportx.FindControl("qr_text", true).Visible = false;
                reportx.FindControl("barCode1", true).Visible = true;
            //}


            return reportx;
        }

        private XtraReport ReportSmallCover(XtraReport reportx, LFD02140AssetMasterModel data)
        {
            reportx.Parameters["asset_no"].Value = data.ASSET_NO;
            reportx.Parameters["asset_no"].Visible = false;
            reportx.Parameters["Contact"].Value = data.CONTACT;
            reportx.Parameters["Contact"].Visible = false;
            reportx.Parameters["CostCenter"].Value = data.COSTCENTER;
            reportx.Parameters["CostCenter"].Visible = false;
            reportx.Parameters["Tel"].Value = data.TEL;
            reportx.Parameters["Tel"].Visible = false;

        

            return reportx;
        }

        private string GetLogoPath(string _company)
        {
            string path = "";
            SystemConfigurationModels param = new SystemConfigurationModels();
            param.CATEGORY = "PRINT_TAG";
            param.SUB_CATEGORY = string.Format("LOGO_{0}",_company);
            param.CODE = "LFD02140";
            var result = sys_dao.GetSystemData(param);
            if (result.Count > 0 && result != null)
            {
                path = result.Select(x => x.VALUE).FirstOrDefault();
            }
            return path;
        }
        private string GetCompanyName(string _company)
        {
            string com_name = "";
            SystemConfigurationModels param = new SystemConfigurationModels();
            param.CATEGORY = "PRINT_TAG";
            param.SUB_CATEGORY = "COMPANY";
            param.CODE = _company;
            var result = sys_dao.GetSystemData(param);
            if (result.Count > 0 && result != null)
            {
                com_name = result.Select(x => x.VALUE).FirstOrDefault();
            }
            return com_name;
        }
        private string GetHeader(string _company)
        {
            string data = "";
            SystemConfigurationModels param = new SystemConfigurationModels();
            param.CATEGORY = "PRINT_TAG";
            param.SUB_CATEGORY = "HEADER";
            param.CODE = _company;
            var result = sys_dao.GetSystemData(param);
            if (result.Count > 0 && result != null)
            {
                data = result.Select(x => x.VALUE).FirstOrDefault();
            }
            return data;
        }
        //private string GetCharacterQr(string Subtype)
        //{
        //    string data = "";
        //    SystemConfigurationModels param = new SystemConfigurationModels();
        //    param.CATEGORY = "PRINT_TAG";
        //    param.SUB_CATEGORY = "BARCODE";
        //    param.CODE = Subtype;
        //    var result = sys_dao.GetSystemData(param);
        //    if (result.Count > 0 && result != null)
        //    {
        //        data = result.Select(x => x.CODE).FirstOrDefault();
        //    }
        //    return data;
        //}
        private string GetFooter(string _company)
        {
            string data = "";
            SystemConfigurationModels param = new SystemConfigurationModels();
            param.CATEGORY = "PRINT_TAG";
            param.SUB_CATEGORY = "FOOTER";
            param.CODE = _company;
            var result = sys_dao.GetSystemData(param);
            if (result.Count > 0 && result != null)
            {
                data = result.Select(x => x.VALUE).FirstOrDefault();
            }
            return data;
        }

        private bool checkPrinter(string printer_para)
        {
            string printerName = printer_para;
            string query = string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", printerName);

            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
            using (ManagementObjectCollection coll = searcher.Get())
            {
                bool isBool = false;
                try
                {
                    foreach (ManagementObject printer in coll)
                    {
                        foreach (PropertyData property in printer.Properties)
                        {
                            Console.WriteLine(string.Format("{0}: {1}", property.Name, property.Value));
                            isBool = true;
                        }
                    }

                    if (!isBool)
                    {
                        AddMessage("MCOM0019AERR");
                    }
                    return isBool;
                }
                catch (ManagementException ex)
                {
                    Console.WriteLine(ex.Message);
                    return isBool;
                }
            }

        }

        private bool ValidateParameter(LFD02140RetriveTagPrintOut p)
        {
            bool isBool = true;
            //if (string.IsNullOrEmpty(p.COST_CENTER))
            //{
            //    AddMessage("MSTD0043AERR", "COST CENTER");
            //    isBool = false;
            //}
            if (string.IsNullOrEmpty(p.BARCODE_SIZE))
            {
                AddMessage("MSTD0043AERR", "BARCODE SIZE");
                isBool = false;
            }
            if (string.IsNullOrEmpty(p.PRINTER_NAME))
            {
                AddMessage("MSTD0043AERR", "PRINTER NAME");
                isBool = false;
            }


            return isBool;
        }

        private bool FileExist(string path)
        {
            if (!File.Exists(path))
            {
                AddMessage("MSTD0013AERR", path);
                return false;
            }
            return true;
        }
    }


}
