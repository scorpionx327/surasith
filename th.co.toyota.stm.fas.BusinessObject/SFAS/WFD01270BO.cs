﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using th.co.toyota.stm.fas.DAO;
using System.Web.Mvc;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.Common;
using System.Web;
using System.IO;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.DAO.Shared;
using System.Data;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01170;
using th.co.toyota.stm.fas.DAO.Common;

namespace th.co.toyota.stm.fas.BusinessObject
{
    //public class WFD01270BO : BO
    //{
    //    private WFD01270DAO dao;
    //    private SystemConfigurationDAO system_dao;
    //    public WFD01270BO()
    //    {
    //        dao = new WFD01270DAO();
    //        system_dao = new SystemConfigurationDAO();
    //    }
        
    //    //public string CheckPhonNoSVNew(WFD01270Model data)
    //    //{
    //    //    try
    //    //    {
    //    //        string _return = "N"; 
    //    //        _return = dao.CheckPhonNoSVNew(data);
    //    //        return _return;
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        return "N";
    //    //    }            
    //    //}        
    //    //public string GetActiveStatusPerSV(string YEAR, string ROUND, string ASSET_LOCATION, string SV_EMP_CODE, string USER_LOGON)
    //    //{
    //    //    try
    //    //    {
    //    //        string _Permission;
    //    //        _Permission = dao.GetActiveStatusPerSV(YEAR, ROUND, ASSET_LOCATION, SV_EMP_CODE, USER_LOGON);
    //    //        return _Permission;
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        return null;
    //    //    }
    //    //}
    //    //public BaseJsonResult UploadFileAttchDoc(WFD01270CommentModel data, HttpRequestBase Request = null)
    //    //{
    //    //    Begin();
    //    //    SystemBO bo = new SystemBO();
    //    //    string _DocNo = "";
    //    //    string _fileName = "";

    //    //    WFD01270CommentModel _CommentDetail = new WFD01270CommentModel();
    //    //    try
    //    //    {
    //    //        if (Request.Files.Count == 0)
    //    //        {
    //    //            return Result(new { FileName = string.Empty });
    //    //        }

    //    //        string _cfgUpload = string.Format("WFD01270_{0}", "COMMENT");

    //    //        _CommentDetail.CommentFile = Request.Files[0];

    //    //        if (Request.Form["DOC_NO"] == null || Request.Form["DOC_NO"] == "")
    //    //        {
    //    //            _DocNo = "TEMP";
    //    //        }
    //    //        else
    //    //        {
    //    //            _DocNo = Request.Form["DOC_NO"].Replace("/", "");
    //    //        }
    //    //        _fileName = _DocNo + "_COMMENT";

    //    //        string _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmss}{2}", _fileName, DateTime.Now,
    //    //            Util.GetExtension(_CommentDetail.CommentFile.FileName));

    //    //        var _b = CheckUploadDocument(_CommentDetail.CommentFile, _cfgUpload);
    //    //        if (!_b)
    //    //            return Result(new { FileName = string.Empty });

    //    //        UploadDocument(_CommentDetail.CommentFile, _cfgUpload, _newFileName);

    //    //        return Result(new { FileName = _newFileName });
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddExceptionMessage(ex);
    //    //        return Result(new { FileName = string.Empty });
    //    //    }
    //    //}
    //    //public BaseJsonResult DeleteFileAttchDoc(WFD01270CommentModel data)
    //    //{
    //    //    Begin();
    //    //    SystemBO bo = new SystemBO();

    //    //    try
    //    //    {
    //    //        if (data.AttachFileName == null || data.AttachFileName == "")
    //    //        {
    //    //            return Result(new { FileName = string.Empty });
    //    //        }

    //    //        string _cfgUpload = string.Format("WFD01270_{0}", "COMMENT");

    //    //        DeleteDocument(_cfgUpload, data.AttachFileName);

    //    //        data.AttachFileName = string.Empty;

    //    //        //dao.UploadAttchDoc(data, Request.Form["TYPE"]);

    //    //        return Result(new { FileName = string.Empty });
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddExceptionMessage(ex);
    //    //        return Result(new { FileName = string.Empty });
    //    //    }
    //    //}
    //    //public BaseJsonResult SubmitRequest(WFD01270MainModel data)
    //    //{

    //    //    Begin();
    //    //    try
    //    //    {
    //    //        //dao.beginTransaction();
    //    //        if (!Validation(data))
    //    //            return Result("-99");

    //    //        string _TempDocNo = string.Format("{0}/{1}", data.RequestHeaderData.EMP_CODE, data.RequestHeaderData.REQUEST_TYPE);
    //    //        data.RequestHeaderData.DOC_NO = _TempDocNo;
    //    //        data.DetailData.TempDocNo = _TempDocNo;
    //    //        dao.SaveRequestData(data);


    //    //        //Detail
    //    //        var _bo = this.DetailBusiness(data.RequestHeaderData.REQUEST_TYPE);
    //    //        var _b = _bo.SaveTempDocument(data.DetailData);
    //    //        //AddMessageList(_bo.GetMessageList());

    //    //        if (!_b)
    //    //            return Result("-99");

    //    //        string _ApproveType = data.RequestHeaderData.REQUEST_TYPE;
    //    //        //if (data.DetailData != null && data.DetailData.DetailTransfer != null &&
    //    //        //        data.DetailData.DetailTransfer.NEW_ASSET_OWNER != null && 
    //    //        //        string.Format("{0}", data.DetailData.DetailTransfer.NEW_ASSET_OWNER.TRNF_TYPE) == Constants.RequestType.MASS_TRANSFER)
    //    //        //{
    //    //        //    _ApproveType = Constants.RequestType.MASS_TRANSFER;
    //    //        //}

    //    //        //dao.beginTransaction();
    //    //        var _DocumentList = dao.UpdateRequestData(data, _ApproveType);

    //    //        string _sDocList = string.Empty;
    //    //        var _temp = new List<WFD01270DocumentMappingModel>();
    //    //        foreach (var _item in _DocumentList)
    //    //        {
    //    //            _sDocList += string.Format("{0}*{1}#{2}|", _item.DOC_NO, _item.ASSET_CATEGORY, _item.REQUEST_TYPE);

    //    //            if (_temp.Where(x => x.DOC_NO == _item.DOC_NO).Count() > 0)
    //    //                continue;

    //    //            _temp.Add(new WFD01270DocumentMappingModel() { DOC_NO = _item.DOC_NO, REQUEST_TYPE = _item.REQUEST_TYPE });
    //    //        }

    //    //        string _sDocumentList = string.Empty;
    //    //        string _RequestType = "";
    //    //        foreach (var _item in _temp)
    //    //        {
    //    //            data.RequestHeaderData.DOC_NO = _item.DOC_NO;

    //    //           // dao.SaveCommentInfo(data);

    //    //            if (_item.REQUEST_TYPE == "Y" || _item.REQUEST_TYPE == "N")
    //    //            {
    //    //                _RequestType = "S";// case Stock
    //    //            }
    //    //            else
    //    //            {
    //    //                _RequestType = _item.REQUEST_TYPE;
    //    //            }
    //    //            _sDocumentList += _item.DOC_NO + ":" + _RequestType + "|";
    //    //        }

    //    //        _bo.Save(data.DetailData, _sDocList);
    //    //        //AddMessageList(_bo.GetMessageList());
    //    //        if (!_b)
    //    //            return Result("-99");

    //    //        //Continue to Approve
    //    //        foreach (var _doc in _temp)
    //    //        {

    //    //            if(_doc.REQUEST_TYPE == Constants.RequestType.DISPOSE)
    //    //            {
    //    //                dao.AdjustDisposeApproveFlow(_doc.DOC_NO);
    //    //            }

    //    //            data.RequestHeaderData.DOC_NO = _doc.DOC_NO;
    //    //            dao.ApproveRequest(data, "A", true);

    //    //            var _item = dao.GetDocumentInfo(_doc.DOC_NO);
    //    //            if (_item.DOC_STATUS == "10")
    //    //                continue;

    //    //            var _list = dao.GetAssetListHistory(_doc.DOC_NO);

    //    //            _bo.ApproveAfterSubmit(_list, data.DetailData); //Support Case 2 Documents
    //    //        }

    //    //        UpdateTempFileNameComment(_temp);       // Update Temp File Name Comment To DocNo

    //    //        _bo.UpdateTempFileName(_temp);          // Update Temp File Name Detial (PHOTO,BOI,LC,MC,MM) To DocNo

    //    //        _bo.UpdateDataAfterGenDocNo(_temp);     // Update DataTypeLossCase (COMPENSATION,USER_MISTAKE_FLAG,LOSS_COUNTER_MEASURE)

    //    //        //dao.commitTransaction();

    //    //        return Result(_sDocumentList);


    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        //dao.rollbackTransaction();
    //    //        AddExceptionMessage(ex);
    //    //        return Result("-99");
    //    //    }

    //    //}
    //    //public BaseJsonResult ApproveRequest(WFD01270MainModel data)
    //    //{
    //    //    List<MessageModel> _listError = new List<MessageModel>();
    //    //    MessageModel _messageMode = new MessageModel();

    //    //    Begin();
    //    //    try
    //    //    {
    //    //        if (!ValidationApprove(data))
    //    //            return Result("-99");

    //    //        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
    //    //        dao.UploadApproverName(data);

    //    //        var _bo = this.DetailBusiness(data.RequestHeaderData.REQUEST_TYPE);
    //    //        _bo.Approve(data, data.DetailData);

    //    //        //dao.ApproveRequest(data, "A", false);

    //    //        //var _bo = this.DetailBusiness(data.RequestHeaderData.REQUEST_TYPE);
    //    //        //_bo.Approve(data.DetailData);

    //    //        return Result(data.RequestHeaderData.DOC_NO);


    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddExceptionMessage(ex);
    //    //        return Result("-99");
    //    //    }


    //    //}
    //    //public BaseJsonResult RejectRequest(WFD01270MainModel data)
    //    //{
    //    //    List<MessageModel> _listError = new List<MessageModel>();
    //    //    MessageModel _messageMode = new MessageModel();

    //    //    Begin();
    //    //    try
    //    //    {

    //    //        if (!ValidationReject(data))
    //    //            return Result("-99");

    //    //        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
    //    //        var _bo = this.DetailBusiness(data.RequestHeaderData.REQUEST_TYPE);
    //    //        _bo.Reject(data, data.DetailData);
  
    //    //        //dao.ApproveRequest(data, "R", false);

    //    //        //var _bo = this.DetailBusiness(data.RequestHeaderData.REQUEST_TYPE);
    //    //        //_bo.Reject(data.DetailData);

    //    //        return Result(null);


    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddExceptionMessage(ex);
    //    //        return Result("-99");
    //    //    }

    //    //}
    //    //public BaseJsonResult CheckValidateUploadFile(CheckFileUploadModel data)
    //    //{
    //    //    Begin();
    //    //    try
    //    //    {
    //    //        var _bo = CheckUploadFile(data);

    //    //        return Result(_bo);


    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddExceptionMessage(ex);
    //    //        return Result(false);
    //    //    }

    //    //}

    //    #region Generate and Get Data
    //    //public BaseJsonResult GetRequestor(WFD01270Model data, ref WFD01270Requestor model)
    //    //{
    //    //    Begin();
    //    //    try
    //    //    {
    //    //        var datas = dao.GetRequestor(data);
    //    //        model = datas;
    //    //        return Result(datas);
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddExceptionMessage(ex);
    //    //        return null;
    //    //    }
    //    //}

    //    //public BaseJsonResult GenerateApprover(WFD01270MainModel main, string _Action)
    //    //{
    //    //    Begin();
    //    //    try
    //    //    {
    //    //        var data = main.RequestHeaderData;

    //    //        if (_Action == "Y")
    //    //        {
    //    //            var _bo = this.DetailBusiness(data.REQUEST_TYPE);
    //    //            var _b = _bo.GenApproveValidation(main.DetailData);
    //    //            if (!_b)
    //    //            {
    //    //                AddMessageList(_bo.GetMessageList());

    //    //                return Result(new { STR_HTML = "" });
    //    //            }

    //    //            //Detail
    //    //            var _boSave = this.DetailBusiness(main.RequestHeaderData.REQUEST_TYPE);
    //    //            var _bSave = _boSave.SaveTempDocument(main.DetailData);
    //    //            AddMessageList(_boSave.GetMessageList());
    //    //            if (!_bSave)
    //    //                return Result(new { STR_HTML = "" });


    //    //            if (string.IsNullOrEmpty(data.GUID))
    //    //                data.GUID = data.DOC_NO;

    //    //            //2017-04-24 : Approver generate approver flow of Transfer Request
    //    //            //data.REQUEST_TYPE == Constants.RequestType.MASS_TRANSFER || 
    //    //            //if (data.REQUEST_TYPE == Constants.RequestType.TRANSFER)
    //    //            //{
    //    //            //    data.COST_CENTER_TO = main.DetailData.DetailTransfer.NEW_ASSET_OWNER.COST_CODE;
    //    //            //}

    //    //            _bo.PrepareTempSelectedforApprove(main.DetailData);


    //    //            bool _GenerateRouteFlag = (string.IsNullOrEmpty(main.RequestHeaderData.DOC_NO) ||
    //    //                    main.RequestHeaderData.DOC_STATUS == "90" ||
    //    //                    main.RequestHeaderData.DOC_STATUS == "99");


    //    //            //Overrid Mass Transfer
    //    //            //if (main.DetailData != null && main.DetailData.DetailTransfer != null &&
    //    //            //    main.DetailData.DetailTransfer.NEW_ASSET_OWNER != null && string.Format("{0}", main.DetailData.DetailTransfer.NEW_ASSET_OWNER.TRNF_TYPE) == Constants.RequestType.MASS_TRANSFER)
    //    //            //{
    //    //            //    dao.GenerateApprover(data, Constants.RequestType.MASS_TRANSFER, _GenerateRouteFlag);
    //    //            //}
    //    //            //else
    //    //            //{
    //    //            //    dao.GenerateApprover(data, data.REQUEST_TYPE, _GenerateRouteFlag);
    //    //            //}
    //    //            dao.GenerateApprover(data, data.REQUEST_TYPE, _GenerateRouteFlag);
    //    //        }

    //    //        var _permission = this.GetPermission(data).FirstOrDefault();

    //    //        var _CheckSelect = false;
    //    //        var html = new StringBuilder();
    //    //        string require = "";
    //    //        string disabled = "";
    //    //        int _countMain = 0;
    //    //        if (!ValidateGenerateApprover(data)) return Result(new { STR_HTML = "" });
    //    //        Double _DivFirst = 0;
    //    //        if (string.IsNullOrEmpty(data.USER_BY))
    //    //        {
    //    //            data.USER_BY = data.EMP_CODE;
    //    //        }
    //    //        List<WFD01270ApproverModel> _Approvor = new List<WFD01270ApproverModel>();
    //    //        List<WFD01270ApproverModel> _ListApprovor = new List<WFD01270ApproverModel>();
    //    //        _Approvor = dao.GetApprover(data);
    //    //        _ListApprovor = dao.GetListApprover(data);


    //    //        _DivFirst = Math.Ceiling((_Approvor.Count / 2.0));
    //    //        if (_Approvor.Count > 0)
    //    //        {
    //    //            for (int i = 0; i < _Approvor.Count; i++)
    //    //            {

    //    //                if (_Approvor[i].DISABLE_EMP_CODE == "N") //Enable
    //    //                {
    //    //                    _Approvor[i].DISABLE_EMP_CODE = _permission.ALLOW_SEL_APPV == "Y" ? "N" : "Y"; //Override
    //    //                }


    //    //                if (i < _DivFirst)// div first
    //    //                {
    //    //                    if (i == 0)
    //    //                    {
    //    //                        html.AppendFormat(@"<div class=""col-md-6"">");
    //    //                        html.AppendFormat(@"<table id=""WFD02190_ApproverTable1"" style=""font-size:12px; "" class=""table table-striped table-bordered table-hover"">");
    //    //                        html.AppendFormat(@"<thead style=""height: 40px;"">");
    //    //                        html.AppendFormat(@"<tr>");
    //    //                        html.AppendFormat(@"<th style=""width: 25px;"" class=""text-center"">#</th>");
    //    //                        html.AppendFormat(@"<th style=""width: 40px"" class=""text-center"">Role</th>");
    //    //                        html.AppendFormat(@"<th style=""width: 150px"" class=""text-center"">Division</th>");
    //    //                        html.AppendFormat(@"<th class=""text-center"">Approver Name</th>");
    //    //                        html.AppendFormat(@"</tr>");
    //    //                        html.AppendFormat(@"</thead>");
    //    //                    }
    //    //                }
    //    //                else
    //    //                {
    //    //                    if (i == _DivFirst)
    //    //                    {
    //    //                        html.AppendFormat(@"</table>");
    //    //                        html.AppendFormat(@"</div>");
    //    //                        html.AppendFormat(@"<div class=""col-md-6"">");
    //    //                        html.AppendFormat(@"<table id=""WFD02190_ApproverTable2"" style=""font-size:12px; "" class=""table table-striped table-bordered table-hover"">");
    //    //                        html.AppendFormat(@"<thead style=""height: 40px;"">");
    //    //                        html.AppendFormat(@"<tr>");
    //    //                        html.AppendFormat(@"<th style=""width: 25px"" class=""text-center"">#</th>");
    //    //                        html.AppendFormat(@"<th style=""width: 40px"" class=""text-center"">Role</th>");
    //    //                        html.AppendFormat(@"<th style=""width: 150px"" class=""text-center"">Division</th>");
    //    //                        html.AppendFormat(@"<th class=""text-center"">Approver Name</th>");
    //    //                        html.AppendFormat(@"</tr>");
    //    //                        html.AppendFormat(@"</thead>");
    //    //                    }

    //    //                }
    //    //                html.AppendFormat(@"<tr>");
    //    //                html.AppendFormat(@"<td class=""text-center"">" + _Approvor[i].NO + "</td>");
    //    //                html.AppendFormat(@"<td class=""text-center"">" + _Approvor[i].ROLE_NAME + "</td>");
    //    //                html.AppendFormat(@"<td>" + _Approvor[i].DIVISION_NAME + "</td>");
    //    //                //------------hide column---------------//<td style="display:none;">
    //    //                html.AppendFormat(@"<td style=""display: none; "">" + _Approvor[i].REQUIRE_FLAG + "</td>");
    //    //                html.AppendFormat(@"<td style=""display: none; "">" + _Approvor[i].INDX + "</td>");
    //    //                html.AppendFormat(@"<td style=""display: none; "">" + _Approvor[i].ROLE + "</td>");
    //    //                html.AppendFormat(@"<td style=""display: none; "">" + _Approvor[i].DIVISION + "</td>");

    //    //                List<WFD01270ApproverModel> _ArrListApprovor = new List<WFD01270ApproverModel>();
    //    //                List<WFD01270ApproverModel> _ListCountMain = new List<WFD01270ApproverModel>();
    //    //                _ArrListApprovor = _ListApprovor.Where(m => m.INDX == _Approvor[i].INDX).ToList();

    //    //                _ListCountMain = _ArrListApprovor.Where(m => m.MAIN == "M").ToList();
    //    //                _countMain = _ListCountMain.Count();
    //    //                _CheckSelect = false;
    //    //                disabled = "";
    //    //                require = "";
    //    //                if (_Approvor[i].REQUIRE_FLAG == "Y")
    //    //                {
    //    //                    require = "require";
    //    //                }
    //    //                else
    //    //                {
    //    //                    require = "";
    //    //                }
    //    //                if (_Approvor[i].DISABLE_EMP_CODE == "Y")
    //    //                {
    //    //                    disabled = "disabled='disabled'";
    //    //                    require = "";
    //    //                }
    //    //                else
    //    //                {
    //    //                    disabled = "";
    //    //                }
    //    //                /* TFAST Test By Surasith
    //    //                 * if (_ArrListApprovor.Count == 1 && disabled == "")
    //    //                {
    //    //                    disabled = "disabled='disabled'";
    //    //                    require = "";
    //    //                }
    //    //                */

    //    //                html.AppendFormat(@"<td><select id='ddlApprover_" + _Approvor[i].INDX.ToString() + "' class='form-control select2 " + require + "' " + disabled + " style='width:100%;'>");

    //    //                if (_ArrListApprovor.Count > 1)
    //    //                {
    //    //                    if (_Approvor[i].REQUIRE_FLAG != "Y")
    //    //                    {
    //    //                        html.AppendFormat(@"<option>         </option>");
    //    //                    }
    //    //                }
    //    //                for (int j = 0; j < _ArrListApprovor.Count; j++)
    //    //                {
    //    //                    if (_ArrListApprovor[j].EMP_CODE == _Approvor[i].EMP_CODE)
    //    //                    {
    //    //                        _CheckSelect = true;
    //    //                        html.AppendFormat(@"<option selected='selected' value='" + _ArrListApprovor[j].EMP_CODE + "'>" + _ArrListApprovor[j].EMP_NAME + "</option>");
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        if ((_Approvor[i].EMP_CODE == null || _Approvor[i].EMP_CODE == "") && _CheckSelect == false)
    //    //                        {// ยังไม่เลือก
    //    //                            if (_ArrListApprovor[j].MAIN == "M")
    //    //                            {
    //    //                                if (_countMain == 1 && _Action == "Y")
    //    //                                {
    //    //                                    _CheckSelect = true;
    //    //                                    html.AppendFormat(@"<option style=""font-weight:600;"" selected='selected' value='" + _ArrListApprovor[j].EMP_CODE + "'>" + _ArrListApprovor[j].EMP_NAME + "</option>");
    //    //                                }
    //    //                                else
    //    //                                {
    //    //                                    html.AppendFormat(@"<option style=""font-weight:600;"" value='" + _ArrListApprovor[j].EMP_CODE + "'>" + _ArrListApprovor[j].EMP_NAME + "</option>");
    //    //                                }
    //    //                            }
    //    //                            else
    //    //                            {
    //    //                                html.AppendFormat(@"<option value='" + _ArrListApprovor[j].EMP_CODE + "'>" + _ArrListApprovor[j].EMP_NAME + "</option>");
    //    //                            }
    //    //                        }
    //    //                        else
    //    //                        {
    //    //                            html.AppendFormat(@"<option value='" + _ArrListApprovor[j].EMP_CODE + "'>" + _ArrListApprovor[j].EMP_NAME + "</option>");
    //    //                        }
    //    //                    }
    //    //                }

    //    //                html.AppendFormat(@"</td>");
    //    //                html.AppendFormat(@"</tr>");

    //    //            }
    //    //            html.AppendFormat(@"</table>");
    //    //            html.AppendFormat(@"</div>");



    //    //            return Result(new { STR_HTML = html.ToString() });
    //    //        }
    //    //        else
    //    //        {
    //    //            AddMessage("MSTD0059AERR");
    //    //            return Result(new { STR_HTML = "" });
    //    //        }
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        AddMessage("Error", ex.Message, MessageStatus.ERROR);
    //    //        return Result(new { STR_HTML = "" });
    //    //    }

    //    //    //font-weight
    //    //}

        

       

       
    //    #endregion

    //    #region Private Function

    //    //private bool Validation(WFD01270MainModel _data)
    //    //{

    //    //    if (_data == null)
    //    //    {
    //    //        AddMessage(Constants.MESSAGE.MSTD0059AERR);
    //    //        return false;
    //    //    }

    //    //    //if (string.IsNullOrEmpty(_data.Requestor.PHONE_NO))
    //    //    //{
    //    //    //    AddRequireMessage("เบอร์ติดต่อ/Tel No.");
    //    //    //    return false;
    //    //    //}
    //    //    if (_data.ApproverList == null || _data.ApproverList.Count == 0)
    //    //    {
    //    //        AddMessage(Constants.MESSAGE.MSTD0059AERR);
    //    //        return false;
    //    //    }

    //    //    bool _f = true;
    //    //    int _SelectedApprv = 0;
    //    //    for (int i = 0; i < _data.ApproverList.Count; i++)
    //    //    {
    //    //        if (_data.ApproverList[i].REQUIRE_FLAG == "Y" && string.IsNullOrEmpty(_data.ApproverList[i].EMP_CODE))
    //    //        {
    //    //            AddRequireMessage("Approver Name Row " + (_data.ApproverList[i].INDX).ToString());
    //    //            _f = false;
    //    //        }
    //    //        if (!string.IsNullOrEmpty(_data.ApproverList[i].EMP_CODE))
    //    //        {
    //    //            _SelectedApprv++;
    //    //        }
    //    //    }

    //    //    if (_SelectedApprv == 0)
    //    //    {
    //    //        AddMessage("MFAS0704AERR");
    //    //        return false;

    //    //    }

    //    //    if (!_f)
    //    //        return false;
    //    //    //Validation of Detail

    //    //    if (_data.DetailData == null)
    //    //    {
    //    //        AddExceptionMessage(new Exception("No Asset List for approving"));
    //    //        return false;
    //    //    }

    //    //    var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
    //    //    _f = _bo.Validation(_data.DetailData);
    //    // //   AddMessageList(_bo.GetMessageList());
    //    //    return _f;
    //    //}
    //    //private bool ValidationApprove(WFD01270MainModel _data)
    //    //{
    //    //    bool _f = true;

    //    //    if (_data.DetailData == null)
    //    //    {
    //    //        AddExceptionMessage(new Exception("No Asset List for approving"));
    //    //        return false;
    //    //    }

    //    //    if (_data.ApproverList == null)
    //    //    {
    //    //        AddExceptionMessage(new Exception("No Approver List for approving"));
    //    //        return false;
    //    //    }

    //    //    var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
    //    //    _f = _bo.ValidationApprove(_data.DetailData);
    //    // //   AddMessageList(_bo.GetMessageList());

    //    //    return _f;
    //    //}
    //    //private bool ValidationReject(WFD01270MainModel _data)
    //    //{
    //    //    bool _f = true;

    //    //    //if (_data.CommentInfo == null)
    //    //    //{
    //    //    //    AddExceptionMessage(new Exception("No Comment Info"));
    //    //    //    return false;
    //    //    //}

    //    //    //if (_data.CommentInfo.CommentText == null || _data.CommentInfo.CommentText == "")
    //    //    //{
    //    //    //    //AddExceptionMessage(new Exception("No Approver List for approving"));
    //    //    //    AddRequireMessage("Comment Info");
    //    //    //    return false;
    //    //    //}

    //    //    var _s = dao.ValidationConcurrency(_data);
    //    //    AddListMessage(_s);
    //    //    if (_s.Count > 0)
    //    //        return false;
    //    //    return _f;
    //    //}
    //    private string UploadCommentFile(HttpPostedFileBase _f, string _FileName)
    //    {
    //        if (_f == null)
    //        {
    //            return string.Empty;
    //        }

    //        string _cfgUpload = "WFD01270";

    //        string _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmss}.{2}", _FileName, DateTime.Now,
    //        Util.GetExtension(_f.FileName));

    //        UploadDocument(_f, _cfgUpload, _newFileName);


    //        return _newFileName;

    //    }
    //    private void UpdateTempFileNameComment(List<WFD01270DocumentMappingModel> _listDoc)
    //    {

    //        //string _RequestType = "";
    //        //List<WFD01270CommentHistoryModel> _listComment;
    //        //string _fullPathOld = "";
    //        //string _cfgUpload = string.Format("WFD01270_{0}", "COMMENT");
    //        //var _bo = new Common.SystemBO();
    //        //string _path = _bo.GetPathDocUpload(_cfgUpload);
    //        //string _newFileName = "";
    //        //foreach (var _doc in _listDoc)
    //        //{
    //        //    if (_doc.REQUEST_TYPE == "Y" || _doc.REQUEST_TYPE == "N")
    //        //    {
    //        //        _RequestType = "S";// case Stock
    //        //    }
    //        //    else
    //        //    {
    //        //        _RequestType = _doc.REQUEST_TYPE;
    //        //    }
    //        //    //Change temp file name Comment
    //        //    _listComment = dao.GetTempDocCommentHistory(_doc.DOC_NO);

    //        //    if (_listComment.Count > 0)
    //        //    {
    //        //        for (int i = 0; i < _listComment.Count; i++)
    //        //        {
    //        //            _fullPathOld = System.IO.Path.Combine(_path, _listComment[i].ATTACH_FILE);
    //        //            _newFileName = "";
    //        //            WFD01270CommentHistoryModel _data = new WFD01270CommentHistoryModel();

    //        //            if (System.IO.File.Exists(_fullPathOld))
    //        //            {
    //        //                _newFileName = _listComment[i].ATTACH_FILE.ToString().Replace("TEMP", _doc.DOC_NO.Replace("/", ""));
    //        //                string _fullPathNew = System.IO.Path.Combine(_path, _newFileName);

    //        //                File.Copy(_fullPathOld, _fullPathNew, true);

    //        //                _data.COMMENT_HISTORY_ID = _listComment[i].COMMENT_HISTORY_ID;
    //        //                _data.DOC_NO = _listComment[i].DOC_NO;
    //        //                _data.INDX = _listComment[i].INDX;
    //        //                _data.ATTACH_FILE = _newFileName;
    //        //                dao.UpdateFileCommentHistory(_data);
    //        //            }
    //        //        }
    //        //    }
    //        //}
    //        //if (System.IO.File.Exists(_fullPathOld))
    //        //{
    //        //    File.Delete(_fullPathOld);
    //        //}
    //    }
    //    #endregion

    //    //public bool ValidateGenerateApprover(WFD01270Model ds)
    //    //{
    //    //    try
    //    //    {
    //    //        var datas = dao.ValidateGenerateApprover(ds);
    //    //        if (datas.Count > 0)
    //    //        {
    //    //            AddListMessage(datas);
    //    //            return false;
    //    //        }
    //    //        else
    //    //        {
    //    //            return true;
    //    //        }
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        return false;
    //    //    }
    //    //}
    //    //private void AddListMessage(List<MessageModel> datas)
    //    //{
    //    //    for (int i = 0; i < datas.Count; i++)
    //    //    {
    //    //        switch (datas[i].MESSAGE_TYPE)
    //    //        {
    //    //            case "WRN":
    //    //                AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.WARNING, VALUE = datas[i].MESSAGE_TEXT });
    //    //                break;
    //    //            case "ERR":
    //    //                AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
    //    //                break;
    //    //            case "INF":
    //    //                AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.INFO, VALUE = datas[i].MESSAGE_TEXT });
    //    //                break;
    //    //            default:
    //    //                AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
    //    //                break;
    //    //        }
    //    //    }
    //    //}
        
        
    //}

    public abstract class TFASTRequestBO : BO
    {
        public abstract bool SubmitValidation(WFD01170MainModel _data);
        public abstract bool ApproveValidation(WFD01170MainModel _data);
        public abstract bool RejectValidation(WFD01170MainModel _data);

        public abstract bool Submit(WFD01170MainModel _data);
        public abstract bool InsertChangeCommentResubmit(WFD01170MainModel _data);
        public abstract bool Approve(WFD01170MainModel _data);
        public abstract bool Reject(WFD01170MainModel _data);
        public abstract bool Acknowledge(WFD01170MainModel _data);
        


        public List<SystemMessageModel> GetErrorMessage()
        {
            return this._systemMessages;
        }

       
        public abstract bool Copy(WFD01170MainModel _data);

        public bool AllowDelete(string _assetDelete, WFD01170UserPermissionModel _user, string _commonStatus)
        {
            if (string.Format("{0}",_user.LockDelete) == "Y")
            {
                return false;
            }

            if (_commonStatus == "C")
                return true;
            if (_commonStatus == "N")
                return true;

            return _assetDelete == "Y" && _user.AllowDelete == "Y";
        }

        //public abstract WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data);
        
    }
    public class WFD01170BO : BO
    {
        public BaseJsonResult IsSessionExpired(string _User)
        {
            Begin();
            
            if (string.IsNullOrEmpty(_User))
            {
                AddMessage("MFAS1910AERR", null);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });
            }
             return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
          
        }

        private TFASTRequestBO DetailBusiness(string _RequestType)
        {
            switch (_RequestType)
            {
                case Constants.RequestType.IMPAIRMENT:
                    return new WFD02A00BO();
                case Constants.RequestType.NEW_ASSET:
                    return new WFD021A0BO();
                case Constants.RequestType.STOCK:
                    return new WFD02640BO();
                case Constants.RequestType.REPRINT:
                    return new WFD02310BO();
                case Constants.RequestType.TRANSFER:
                    return new WFD02410BO();
                case Constants.RequestType.DISPOSE:
                    return new WFD02510BO();
                case Constants.RequestType.SATTLEMENT:
                    return new WFD02710BO();
                case Constants.RequestType.INFO_CHANGE:
                    return new WFD02810AOBO();
                case Constants.RequestType.RECLASS:
                    return new WFD02910BO();
            }

            throw (new Exception("Undefine Business object for this request"));
        }
      
        private WFD01170DAO dao = null;
        public WFD01170BO()
        {
            dao = new WFD01170DAO();
        }
        public WFD01170Requestor GetRequestor(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                return dao.GetRequestor(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                throw (ex);
            }
        }
        public List<SystemModel> GetRoleOfRequestor(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                var rs = dao.GetRoleOfRequestor(data);
                if(rs.Count > 1)
                {
                    rs.Insert(0,new SystemModel() { CODE = string.Empty, VALUE = string.Empty });
                }
                return rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                throw (ex);
            }
        }

        public bool IsAllowCreateDocument(WFD0BaseRequestDocModel _data)
        {
            var _rs = dao.IsAllowCreateDocument(_data);
            return _rs;
        }
        public Models.BaseModel.TB_R_REQUEST_H GetRequestHeader(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                return dao.GetRequestHeader(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                throw (ex);
            }
        }

        public WFD01170UserPermissionModel GetPermission(WFD0BaseRequestDocModel data)
        {
            try
            {
                var _rs = dao.GetPermission(data);
                return _rs ?? new WFD01170UserPermissionModel() { Mode = "N" } ;
            }
            catch (Exception ex)
            {
                return new WFD01170UserPermissionModel() { Mode = "N" };
            }
        }

        public WFD01170UserPermissionModel GetPermissionRetirement(WFD0BaseRequestDocModel data)
        {
            try
            {
                var _rs = dao.GetPermissionRetirement(data);
                return _rs ?? new WFD01170UserPermissionModel() { Mode = "N" };
            }
            catch (Exception ex)
            {
                return new WFD01170UserPermissionModel() { Mode = "N" };
            }
        }

        public void InitializeApproverList(WFD0BaseRequestDocModel data)
        {
            try
            {
                dao.InitializeApproverList(data);
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            
        }
        public BaseJsonResult GenerateApproverFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.GenerateApproverFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
        public BaseJsonResult ResetApproverFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ResetApproverFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult RenderApproverFlow(WFD0BaseRequestDocModel data, bool _genFlow)
        {
            Begin();
            try
            {
                var _rsApproveFlow = dao.GetApproverFlow(data);
                var _rsApprovePerson = dao.GetApproverPerson(data);
                //Prepare Route
                
                //Assign left right for role that not in config 

                List<int> _rowGroup = new List<int>();

                var _preModel = new WFD01170ApproverModel();

                

                var _permission = this.GetPermission(data);


                //int _pregroup = 0;
                //Assign group for all role
                foreach (var _item in _rsApproveFlow)
                {
                    if (_genFlow)
                        continue;
                    if (_item.DISABLE_EMP_CODE == "N" && _permission.DOC_STATUS != "00")
                    {
                        //add AllowChangeApprover == 'N' 2019.12.17
                        if (_permission.AllowChangeApprover == "N" && _permission.AllowSelectApprover == "Y" && _item.APPR_STATUS == "W")
                            _item.DISABLE_EMP_CODE = "Y";
                        else if (_permission.AllowChangeApprover != "Y" && _permission.AllowSelectApprover != "Y")
                            _item.DISABLE_EMP_CODE = "Y";
                    }

                }


                StringBuilder _html = new StringBuilder();

                int _iCounter = 1;

                while (_rsApproveFlow.Exists(x => x.DISPLAY_ROW == _iCounter))
                {
                    
                    _html.AppendFormat("<div class=\"row\">{0}", Environment.NewLine);
                    //left
                    _html.AppendFormat("    <div class=\"col-md-6 col-sm-6\">{0}", Environment.NewLine);
                    if (_rsApproveFlow.Exists(x => x.DISPLAY_SIDE == "L" && x.DISPLAY_ROW == _iCounter))
                    {
                        var _blockL = _rsApproveFlow.FindAll(x => x.DISPLAY_SIDE == "L" && x.DISPLAY_ROW == _iCounter); //_rsGroup.Find(x => x.LAYOUT_COL_INDX && x.LAYOUT_ROW_INDX == _rowIndx);      
                        _html.Append(this.RenderPerRole("L", _iCounter, _rsApproveFlow, _rsApprovePerson));
                    }
                  
                    _html.AppendFormat("    </div>{0}", Environment.NewLine);
                    _html.AppendFormat("    <div class=\"col-md-6 col-sm-6\">{0}", Environment.NewLine);
                    if (_rsApproveFlow.Exists(x => x.DISPLAY_SIDE == "R" && x.DISPLAY_ROW == _iCounter))
                    {
                        var _blockR = _rsApproveFlow.FindAll(x => x.DISPLAY_SIDE == "R" && x.DISPLAY_ROW == _iCounter);
                        _html.Append(this.RenderPerRole("R", _iCounter, _rsApproveFlow, _rsApprovePerson));
                    }
                    _html.AppendFormat("    </div>{0}", Environment.NewLine);
                    
                    _html.AppendFormat("</div>{0}", Environment.NewLine);
                    _iCounter++;
                }

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _html.ToString() });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult GetTotalDocument(WFD0BaseRequestDocModel data)
        {
            var _rs = dao.GetDocumentList(data);
            if (_rs != null && _rs.Count > 1)
            {
                //Get Message
                string _msg = GetSystemMessage("MFAS0902AINF", _rs.Count).VALUE;
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _msg });
            }

            return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR,Message = string.Empty });
        }
        private StringBuilder RenderPerRole(string _side, int _rowIndx, List<WFD01170ApproverModel> _roleList, List<WFD01170ApproverModel> _personList)
        { 

            StringBuilder html = new StringBuilder();
          
            var _blockList = _roleList.FindAll(x => x.DISPLAY_SIDE == _side && x.DISPLAY_ROW == _rowIndx).OrderBy(x => x.INDX).ToList();
            if (_blockList.Count == 0)
                return new StringBuilder();



            html.AppendFormat(@"<h4>{0}</h4>", _blockList.FirstOrDefault().DISPLAY_TITLE);
            html.AppendFormat(@"    <table id=""WFD02190_ApproverTable1"" style=""font-size:12px; "" class=""table table-striped table-bordered table-hover"">");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"        <thead style=""height: 40px;"">");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"            <tr>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"                <th style=""width: 25px;"" class=""text-center"">#</th>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"                <th style=""width: 40px"" class=""text-center"">Role</th>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"                <th style=""width: 150px"" class=""text-center"">Division</th>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"                <th class=""text-center"">Approver Name</th>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"            </tr>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"        </thead>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"        <tbody>");
            html.AppendFormat(Environment.NewLine);
            
            
            foreach (var _item in _blockList) { // per row

                var _person = _personList.FindAll(x => x.APPR_ROLE == _item.APPR_ROLE && x.INDX == _item.INDX);

                html.AppendFormat(@"            <tr>");
                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"                <td class=""text-center"">{0}</td>",_item.ROW_INDX);
                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"                <td class=""text-center"" data-container=""body"" data-toggle=""tooltip"" title=""{1}"" >{0}</td>", _item.APPR_ROLE, _item.ROLE_NAME);
                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"                <td class=""divname"">{0}</td>",_item.DIV_NAME);
                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"                <td>");
                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"                <input type=""hidden"" id=""hdRoleIndx_{0}"" name=""hdRoleIndx"" value=""{0}"">", _item.INDX);
                html.AppendFormat(@"                <input type=""hidden"" id=""hdRecordIndx_{0}"" name=""hdRecordIndx"" value=""{0}"">", _item.ROW_INDX);
                html.AppendFormat(@"                <input type=""hidden"" id=""hdApproveRole_{0}"" name=""hdApproveRole"" value=""{1}"">", _item.INDX, _item.APPR_ROLE);
                html.AppendFormat(Environment.NewLine);

                
                var _select = new StringBuilder();
                _select.Append(@"               <select id='ddlApprover_{0}' name='ddlApprover' class='ddlApprover form-control select2 {1}' {2} style='width:100%;'>");
                          

                _select.AppendFormat(Environment.NewLine);

                bool _disabled = false;

                if (_item.DISABLE_EMP_CODE == "Y")
                    _disabled = true;

                if (_item.NOTIFICATION_MODE == "P")
                {
                    _select.Append(this.RenderPersonInCombobox(_item, _person));

                    //Cout _person
                    if(_person.Count <= 1)
                        _disabled = true;
                }
                else // group
                {
                    _disabled = true;
                    if (!string.IsNullOrEmpty(_item.EMP_CODE))
                        _select.AppendFormat(@"                <option selected='selected' data-divname='{2}' value='{0}'>{1}</option>", _item.EMP_CODE, _item.EMP_NAME, _item.DIV_NAME);
                    else
                    {
                        _select.AppendFormat(@"                <option selected='selected' data-divname='{2}' value='{0}'>{1}</option>", _item.APPR_ROLE, _item.EMP_NAME, _item.DIV_NAME);
                        // 2020.01.25 Surasith t. change
                        // _select.AppendFormat(@"                <option selected='selected' data-divname='{2}' value='{0}'>{1}</option>", _item.APPR_ROLE, _item.APPR_ROLE, _item.DIV_NAME);
                    }
                }
                //RenderPersonInCombobox
                _select.AppendFormat(@"                    </select>");

                html.AppendFormat(_select.ToString(), _item.INDX, 
                    (_item.REQUIRE_FLAG == "Y" && !_disabled) ? "require" : string.Empty, _disabled ? "disabled='disabled" : "");
               // _disabled ? "disabled='disabled'" : "");

                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"                </td>");
                html.AppendFormat(Environment.NewLine);
                html.AppendFormat(@"            </tr>");
            }
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"        </tbody>");
            html.AppendFormat(Environment.NewLine);
            html.AppendFormat(@"    </table>");
            return html;
        }

        private StringBuilder RenderPersonInCombobox(WFD01170ApproverModel _item, List<WFD01170ApproverModel> _person)
        {
            var _SelectedItemFlag = false;

            var html = new StringBuilder();
            if (_person.Count == 0)
            {
                html.AppendFormat(@"                <option selected='selected' daa-divname='{2}' value='{0}'>{1}</option>", _item.EMP_CODE, _item.EMP_NAME, _item.DIV_NAME);
                return html;
            }

            if (_person.Count > 1)
            {
                html.AppendFormat(@"                <option></option>");
                html.AppendFormat(Environment.NewLine);
            }

            int _cntMain = _person.FindAll(x => x.MAIN == "M").Count;

            foreach (var _p in _person)
            {
                string _ff = "style=\"font-weight:600;\"";
                html.AppendFormat(Environment.NewLine);
                if (_item.EMP_CODE == _p.EMP_CODE)
                {
                    _SelectedItemFlag = true;
                    html.AppendFormat(@"                <option selected='selected' {3} data-divname='{2}' value='{0}'>{1}</option>", _p.EMP_CODE, _p.EMP_NAME, _p.DIV_NAME, (_p.MAIN == "M" ? _ff : string.Empty));
                    continue;
                }
                if (!string.IsNullOrEmpty(_item.EMP_CODE) || _SelectedItemFlag)
                {
                    
                    html.AppendFormat(@"                <option value='{0}' {3} data-divname='{2}'>{1}</option>", _p.EMP_CODE, _p.EMP_NAME, _p.DIV_NAME, (_p.MAIN == "M" ? _ff : string.Empty) );
                    continue;
                }
                if (_p.MAIN != "M")
                {
                    html.AppendFormat(@"                <option value='{0}' data-divname='{2}'>{1}</option>", _p.EMP_CODE, _p.EMP_NAME, _p.DIV_NAME);
                    continue;
                }

                if (_cntMain == 1)
                {
                    _SelectedItemFlag = true;
                    html.AppendFormat(@"                <option style=""font-weight:600;"" data-divname='{2}' selected='selected' value='{0}'>{1}</option>", _p.EMP_CODE, _p.EMP_NAME, _p.DIV_NAME);
                    continue;
                }

                html.AppendFormat(@"                <option style=""font-weight:600;"" data-divname='{2}' value='{0}'>{1}</option>", _p.EMP_CODE, _p.EMP_NAME, _p.DIV_NAME);

            }

            return html;
        }
        public List<WFD01170CommentHistoryModel> GetCommentHistoryList(WFD0BaseRequestDocModel data)
        {
            return dao.GetCommentHistory(data);
        }

        public List<WFD01170WorkflowTrackingProcessModel> GetWorkflowTrackingProcess(WFD0BaseRequestDocModel data)
        {
            return dao.GetWorkflowTrackingProcess(data);
        }

        public BaseJsonResult UploadFileAttchDoc(WFD01170CommentModel data, HttpRequestBase Request = null)
        {
            Begin();
            SystemBO bo = new SystemBO();
            string _DocNo = "";
            string _fileName = "";

            var _CommentDetail = new WFD01170CommentModel();
            try
            {
                if (Request.Files.Count == 0)
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });
                }

                string _cfgUpload = string.Format("WFD01170_{0}", "COMMENT");

                _CommentDetail.CommentFile = Request.Files[0];

                if (string.IsNullOrEmpty(Request.Form["DOC_NO"]))
                {
                    _DocNo = "TEMP";
                }
                else
                {
                    _DocNo = Request.Form["DOC_NO"].Replace("/", "");
                }
                _fileName = _DocNo + "_COMMENT";

                string _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmss}{2}", _fileName, DateTime.Now,
                    Util.GetExtension(_CommentDetail.CommentFile.FileName));

                var _b = base.CheckUploadDocument(_CommentDetail.CommentFile, _cfgUpload);
                if (!_b)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });

                base.UploadDocument(_CommentDetail.CommentFile, _cfgUpload, _newFileName);

                
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _newFileName });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
        public BaseJsonResult DeleteFileAttchDoc(WFD01170CommentModel data)
        {
            Begin();
            SystemBO bo = new SystemBO();

            try
            {
                if (string.IsNullOrEmpty(data.AttachFileName))
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });
         
                }

                string _cfgUpload = string.Format("WFD01170_{0}", "COMMENT");

                base.DeleteDocument(_cfgUpload, data.AttachFileName);

                data.AttachFileName = string.Empty;

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
       
        private bool CheckRequiredApprover(WFD01170MainModel _data)
        {
            //Load Approver
            var _masterRole = dao.GetApproverFlow(new WFD0BaseRequestDocModel() {  DOC_NO = _data.RequestHeaderData.DOC_NO, GUID = _data.RequestHeaderData.GUID});

            //1#FW##|5#FS##|6#ACR##
            var _user = _data.ApproverList.Split('|');
            var _userList = new List<WFD01170ApproverModel>();
            for (int i = 0; i < _user.Length; i++)
            {
                var _info = _user[i].Split('#');
                if (_info.Length != 3)
                    continue;

                _userList.Add(new WFD01170ApproverModel()
                {
                    INDX = Convert.ToInt32(_info[0]),
                    APPR_ROLE = _info[1],
                    EMP_CODE = _info[2]
                });
            }
            bool _isRequired = true;
            _masterRole.FindAll(s =>s.REQUIRE_FLAG == "Y").ForEach(delegate (WFD01170ApproverModel _role)
            {
                if(_userList.Find(x => x.INDX == _role.INDX && string.IsNullOrEmpty(x.EMP_CODE)) != null) // Found
                {
                    _isRequired = false;
                    //Error
                    AddRequireMessage(string.Format("Approver Name Row {0}", (_role.ROW_INDX)));
                }
            });


            return _isRequired ;
        }
        private bool SubmitValidation(WFD01170MainModel _data)
        {
            if (string.IsNullOrEmpty(_data.RequestHeaderData.PHONE_NO))
            {
                AddRequireMessage("Tel No.");
                return false;
            }

            if (!this.CheckRequiredApprover(_data))
            {
                return false;
            }
           

            return true;
        }
        public BaseJsonResult SubmitRequest(WFD01170MainModel _data)
        {
            
          
            Begin();
            try
            {
                //Validation => Header, Approver Flow
                if (!SubmitValidation(_data))
                    return Result(new JsonResultBaseModel() { Message = "Header Validation Error", ResetFlow = false, Status =  MessageStatus.ERROR });

                // Detail
                var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
                //Validation => Asset List and Request Detail
                if (!_bo.SubmitValidation(_data))
                {
                    this.AddMessageList(_bo.GetErrorMessage());
                    return Result(new JsonResultBaseModel() { Message = "Detail Validation Error", ResetFlow = true, Status = MessageStatus.ERROR });
                }


                _bo.Submit(_data);
                
                //Get Document
                var _rsDocList = dao.GetDocumentList(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });



                string _DocNoList = string.Empty;
                foreach(var _rs in _rsDocList)
                {   
                    _DocNoList += string.Concat(_rs.DOC_NO,":",_data.RequestHeaderData.REQUEST_TYPE, "|");
                }

                return Result(new JsonResultBaseModel() { ResetFlow = true, Status = MessageStatus.INFO,Message = _DocNoList }); // List of document


            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });
            }

        }

        private bool ApproveValidation(WFD01170MainModel _data)
        {
            if (!this.CheckRequiredApprover(_data))
            {
                return false;
            }

            return true;
        }
        public BaseJsonResult ApproveRequest(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _permission = dao.GetPermission(new WFD0BaseRequestDocModel() {
                     DOC_NO = _data.RequestHeaderData.DOC_NO,
                    USER_BY = _data.RequestHeaderData.USER_BY,
                    REQUEST_TYPE = _data.RequestHeaderData.REQUEST_TYPE
                });


                if (!ApproveValidation(_data))
                    return Result(new JsonResultBaseModel() { Message = "Header Error", Status = MessageStatus.ERROR });

                //Validation => Asset List and Request Detail
                var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
                if (!_bo.ApproveValidation(_data))
                {
                    this.AddMessageList(_bo.GetErrorMessage());
                    return Result(new JsonResultBaseModel() { Message = "Detail Error", Status = MessageStatus.ERROR });
                }


                _bo.Approve(_data);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.RequestHeaderData.DOC_NO }); // List of document
                
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });
            }


        }

        private bool RejectValidation(WFD01170MainModel _data)
        {
            bool _f = true;

            if (_data.CommentInfo == null)
            {
                AddExceptionMessage(new Exception("No Comment Info"));
                return false;
            }

            string s = System.Text.RegularExpressions.Regex.Replace(string.Format("{0}", _data.CommentInfo.CommentText), @"\t|\n|\r", "");
            if (string.IsNullOrEmpty(s))
            {
                //AddExceptionMessage(new Exception("No Approver List for approving"));
                AddRequireMessage("Comment Info");
                return false;
            }
            
            return _f;
        }
        public BaseJsonResult RejectRequest(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                
                if (!RejectValidation(_data))
                    return Result(new JsonResultBaseModel() { Message = "Reject Is invalid", Status = MessageStatus.ERROR });

                var _doc = this.GetRequestHeader(_data.RequestHeaderData);
                if ("10|20|85|88|".Contains(_doc.STATUS)) //reject back
                {
                    var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
                    _bo.Reject(_data);
                }
                else //Reject forward to AEC
                {
                    dao.AECUpdateRequestAfterSendSAP(_data, "R");
                }
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.RequestHeaderData.DOC_NO }); // List of document

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });
            }

        }

        public BaseJsonResult SaveApprover(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.SaveApprover(_data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.RequestHeaderData.DOC_NO }); // List of document
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });

            }
        }

        public BaseJsonResult ResubmitRequest(WFD01170MainModel _data)
        {

            //Resubmit is approve mode
            if (Constants.RequestType.INFO_CHANGE == _data.RequestHeaderData.REQUEST_TYPE)
            {
                WFD02810AOBO _bo = new WFD02810AOBO();
                _bo.InsertChangeCommentResubmit(_data);
            }

            return this.ApproveRequest(_data);

        }

        public BaseJsonResult AcknowledgeRequest(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                

                var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
                _bo.Acknowledge(_data);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.RequestHeaderData.DOC_NO }); // List of document

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });
            }
            
        }

        public BaseJsonResult CloseRequest(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.AECUpdateRequestAfterSendSAP(_data, "C");

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.RequestHeaderData.DOC_NO }); // List of document

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });
            }
        }

        

        public BaseJsonResult CopyNew(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                _data.NewGuid = Guid.NewGuid().ToString();
                var _bo = this.DetailBusiness(_data.RequestHeaderData.REQUEST_TYPE);
                _bo.Copy(_data);

                
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.NewGuid }); // List of document

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });
            }

        }

        public string GetDownloadTemplate(string _FunctionID)
        {
            
            var _templateFileName = string.Empty;
            var _rs = (new SystemBO()).SelectSystemDatas("SYSTEM_CONFIG", "UPLOAD_TEMPLATE_FILE");
            if (_rs.Count == 0)
            {
                _templateFileName = string.Empty;
            }
            else if(_rs.Find( x => x.CODE == _FunctionID) != null)
            {
                _templateFileName = _rs.Find(x => x.CODE == _FunctionID).VALUE;
            }

            if (string.IsNullOrEmpty(_templateFileName))
                throw (new Exception(string.Format("No found Value in Configuration CATEGORY = {0}, SUB_CATEGORY = {1}, CODE = {2}", "SYSTEM_CONFIG", "UPLOAD_TEMPLATE_FILE", _FunctionID)));

            if (!File.Exists(_templateFileName))
            {
                throw (new Exception(string.Format("File [{0}] not exists", _templateFileName)));
            }

            return _templateFileName;
        }


        public BaseJsonResult GenerateAssetNo(WFD01170SelectedAssetNoModel _data, string _User)
        {
            Begin();
            try
            {
                dao.UpdateGenStatusForNewAssetRequest(_data, _User);

                //Surasith T. 2020.01.15 change flag is false because only 1 function call to IsReadyGenFile
                if (!dao.IsReadyGenFile(_data.DOC_NO))
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = "No data for generate asset no" });
                }

                var _batch = new BatchProcessBO();

                var batch_id = _batch.addBatchQ(new StartBatchModel()
                {
                    BatchID = Constants.BatchID.BFD021A3,
                    UserId = _User,
                    Description = string.Format("Generate Asset No Doc No = {0}", _data.DOC_NO)
                });
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _data.DOC_NO }); // List of document


            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Message = ex.Message, Status = MessageStatus.ERROR });

            }
        }

        public BaseJsonResult UploadExcel(WFD01170UploadRequestModel data, HttpRequestBase Request = null)
        {
            Begin();
           
            try
            {
                if (Request.Files.Count == 0)
                {
                    return Result(new WFD01170UploadResultModel() { IsError = true,  Message = "No upload file" });
                }


                var _Path = System.Configuration.ConfigurationManager.AppSettings["ExcelUploadPath"];
                if (!Directory.Exists(_Path))
                {
                    Directory.CreateDirectory(_Path);
                }
                string _fName = Request.Files[0].FileName ;

                if (_fName.LastIndexOf("\\") > 0)
                {
                    _fName = _fName.Substring(_fName.LastIndexOf("\\") + 1);
                }


                string _fExt = Util.GetExtension(_fName);
                _fName = _fName.Replace(_fExt, string.Empty);

                string _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmss}{2}", _fName, DateTime.Now, _fExt);

                var _fullName = System.IO.Path.Combine(_Path, _newFileName);
                Request.Files[0].SaveAs(_fullName);


                
                //FTP
                /*
                 Nipon 2019/11/15 -> Not use FTP for batch upload. 
                 */
                //var _rs = Util.FTPFile(_fullName, data.StartUpPath, _newFileName);
                //if (!_rs)
                //{
                //    return Result(new WFD01170UploadResultModel() { IsError = true, Message = "Cannot transfer data to server" });
                //}

                return Result(new WFD01170UploadResultModel() { IsError = false, FileName = _newFileName });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new WFD01170UploadResultModel() { IsError = true, Message = ex.Message });
            }
        }

       
    }

}
