﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.Common;
using System.Web;
using System.Data.SqlClient;

namespace th.co.toyota.stm.fas.BusinessObject
{
    // Create by Jirawat Jannet
    // Create date : 2017-02-03
    public abstract class BO
    {
        protected List<SystemMessageModel> _systemMessages;

        public string _format = "dd.MM.yyyy";
        public CultureInfo cultureInfo = new CultureInfo("en");

        /// <summary>
        /// Begin business method ( Always call at firse line of method )
        /// </summary>
        protected virtual void Begin()
        {
            if (_systemMessages == null)
            {
                _systemMessages = new List<SystemMessageModel>();
            }
        }


        #region Validate

        protected bool IsValid
        {
            get
            {
                if (_systemMessages != null && _systemMessages.Count > 0) return false;
                else return true;
            }
        }

        /// <summary>
        /// Validate required field wirh attribute Model
        /// </summary>
        /// <param name="obj">Object</param>
        /// <returns>true/false</returns>
        protected bool ValidateRequireFields(object obj)
        {
            bool IsValid = true;
            Type myType = obj.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            foreach (PropertyInfo prop in props)
            {
                object propValue;

                try { propValue = prop.GetValue(obj, null); }
                catch (Exception) { continue; }
                object[] attrs = prop.GetCustomAttributes(true);


                foreach (object attr in attrs)
                {
                    ModelAttribute authAttr = attr as ModelAttribute;
                    if (authAttr != null)
                    {
                        if (authAttr.Required && propValue == null)
                        {
                            IsValid = false;
                            AddRequireMessage(authAttr.FieldName);
                            break;
                        }
                        if (!string.IsNullOrEmpty(authAttr.DateFormat))
                        {
                            if (propValue != null)
                            {
                                DateTime dt;
                                if (!DateTime.TryParseExact(propValue.ToString(),
                                       authAttr.DateFormat,
                                       new CultureInfo("en-Us"),
                                       DateTimeStyles.None,
                                       out dt))
                                {
                                    IsValid = false;
                                    AddDateInValdFormat(prop.Name, authAttr.DateFormat);
                                }
                            }
                        }
                        if (authAttr.MaxLength != -1 && propValue.ToString().Trim().Length != authAttr.MaxLength)
                        {
                            IsValid = false;
                            AddMaxLenMessage(authAttr.FieldName, authAttr.MaxLength);
                        }
                        if (authAttr.IsTime)
                        {
                            int n;
                            if (propValue.ToString().Split(':').Length != 2)
                            {
                                IsValid = false;
                                AddTimeInValidFormat(authAttr.FieldName);
                            }
                            else if (!int.TryParse(propValue.ToString().Split(':')[0], out n) || !int.TryParse(propValue.ToString().Split(':')[1], out n))
                            {
                                IsValid = false;
                                AddTimeInValidFormat(authAttr.FieldName);
                            }
                            else if (int.Parse(propValue.ToString().Split(':')[0]) > 23 || int.Parse(propValue.ToString().Split(':')[1]) > 59)
                            {
                                IsValid = false;
                                AddTimeInValidFormat(authAttr.FieldName);
                            }
                            else if (int.Parse(propValue.ToString().Split(':')[0]) < 0 || int.Parse(propValue.ToString().Split(':')[1]) < 0)
                            {
                                IsValid = false;
                                AddTimeInValidFormat(authAttr.FieldName);
                            }
                        }
                        break;
                    }
                }
            }
            return IsValid;
        }
        /// <summary>
        /// Validate required field with property name
        /// </summary>
        /// <param name="obj">Object</param>
        /// <param name="FieldsName">Fields name</param>
        /// <returns>true/false</returns>
        protected bool ValidateRequireFields(object obj, params RV[] Fields)
        {
            List<string> fs = new List<string>();
            bool IsValid = true;
            Type myType = obj.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
            List<string> validPropNames = new List<string>();

            foreach (PropertyInfo prop in props)
            {
                object propValue;
                try { propValue = prop.GetValue(obj, null); }
                catch (Exception) { continue; }
                object[] attrs = prop.GetCustomAttributes(true);


                if (attrs != null && attrs.Length > 0)
                {
                    foreach (object attr in attrs)
                    {
                        ModelAttribute authAttr = attr as ModelAttribute;
                        if (authAttr != null)
                        {

                            if (authAttr.Required && propValue == null)
                            {
                                IsValid = false;
                                AddRequireMessage(authAttr.FieldName);
                                validPropNames.Add(authAttr.FieldName);
                                break;
                            }
                            if (!string.IsNullOrEmpty(authAttr.DateFormat))
                            {
                                if (propValue != null)
                                {
                                    DateTime dt;
                                    if (!DateTime.TryParseExact(propValue.ToString(),
                                           authAttr.DateFormat,
                                           new CultureInfo("en-Us"),
                                           DateTimeStyles.None,
                                           out dt))
                                    {
                                        IsValid = false;
                                        AddDateInValdFormat(authAttr.FieldName, authAttr.DateFormat);
                                        validPropNames.Add(authAttr.FieldName);
                                    }
                                }
                            }
                            if (authAttr.MaxLength != -1 && propValue.ToString().Trim().Length != authAttr.MaxLength)
                            {
                                IsValid = false;
                                AddMaxLenMessage(authAttr.FieldName, authAttr.MaxLength);
                                validPropNames.Add(authAttr.FieldName);
                            }
                            if (authAttr.IsTime)
                            {
                                int n;
                                if (propValue.ToString().Split(':').Length != 2)
                                {
                                    IsValid = false;
                                    AddTimeInValidFormat(authAttr.FieldName);
                                    validPropNames.Add(authAttr.FieldName);
                                }
                                else if (!int.TryParse(propValue.ToString().Split(':')[0], out n) || !int.TryParse(propValue.ToString().Split(':')[1], out n))
                                {
                                    IsValid = false;
                                    AddTimeInValidFormat(authAttr.FieldName);
                                    validPropNames.Add(authAttr.FieldName);
                                }
                                else if (int.Parse(propValue.ToString().Split(':')[0]) > 23 || int.Parse(propValue.ToString().Split(':')[1]) > 59)
                                {
                                    IsValid = false;
                                    AddTimeInValidFormat(authAttr.FieldName);
                                    validPropNames.Add(authAttr.FieldName);
                                }
                                else if (int.Parse(propValue.ToString().Split(':')[0]) < 0 || int.Parse(propValue.ToString().Split(':')[1]) < 0)
                                {
                                    IsValid = false;
                                    AddTimeInValidFormat(authAttr.FieldName);
                                    validPropNames.Add(authAttr.FieldName);
                                }
                            }
                            break;
                        }
                    }
                }
            }

            if (Fields != null && Fields.Length > 0)
            {
                foreach (var f in Fields)
                {
                    if (validPropNames.Where(m => m == f.Title).Count() > 0) continue;
                    foreach (PropertyInfo prop in props)
                    {
                        object propValue = prop.GetValue(obj, null);
                        if (f.Name == prop.Name && propValue == null)
                        {
                            IsValid = false;
                            AddRequireMessage(f.Title);
                            validPropNames.Add(f.Title);
                            break;
                        }
                    }
                }
            }

            return IsValid;
        }
        protected class RV
        {
            public string Name { get; set; }
            public string Title { get; set; }

            /// <summary>
            /// Name : Name of property, Title : Name show in message
            /// </summary>
            /// <param name="Name">Name of property</param>
            /// <param name="Title">Name show in message</param>
            public RV(string Name, string Title)
            {
                this.Name = Name;
                this.Title = Title;
            }
        }

        #endregion

        #region Return function

        /// <summary>
        ///  Return BaseJsonResult for normal process
        /// </summary>
        /// <param name="ObjectResult">Result model data</param>
        protected BaseJsonResult Result(object ObjectResult)
        {
            BaseJsonResult model = new BaseJsonResult();
            model.ObjectResult = ObjectResult;
            model.SetMessage(_systemMessages != null && _systemMessages.Count > 0 ?
                                _systemMessages.ToArray() : null);
            return model;
        }
        /// <summary>
        /// Return SearchJsonResult for search process
        /// </summary>
        /// <param name="ObjectResult"></param>
        /// <param name="PaginData"></param>
        /// <returns></returns>
        protected SearchJsonResult Result(object ObjectResult, PaginationModel PaginData, string DivMessage = null)
        {
            SearchJsonResult model = new SearchJsonResult();
            model.ObjectResult = ObjectResult;
            model.Pagin = PaginData;
            model.SetMessage(_systemMessages != null && _systemMessages.Count > 0 ?
                                _systemMessages.ToArray() : null);
            model.DivMessages = DivMessage;
            return model;
        }

        #endregion

        #region Add message
        protected void AddMessage(string Id, string Message, MessageStatus Type, bool isOnlyMessage = false)
        {
            _systemMessages.Add(new SystemMessageModel()
            {
                ID = Id,
                VALUE = isOnlyMessage ? Message : Id + " : " + Message,
                STATUS = Type
            });
        }
        /// <summary>
        /// Add result message
        /// </summary>
        /// <param name="SystemMessage">SystemMessageModel</param>
        protected void AddMessage(SystemMessageModel SystemMessage)
        {
            _systemMessages.Add(SystemMessage);
        }
        protected void AddMessageList(List<MessageModel> datas)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                switch (datas[i].MESSAGE_TYPE)
                {
                    case "WRN":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.WARNING, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "ERR":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "INF":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.INFO, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    default:
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                }
            }
        }
        protected void AddMessageList(List<SystemMessageModel> SystemMessage)
        {
            _systemMessages.AddRange(SystemMessage);
        }
        /// <summary>
        /// Add result message
        /// </summary>
        /// <param name="MessageCode">Message code (Get from DR Document)</param>
        /// <param name="Parameters">Parameters</param>
        protected void AddMessage(string MessageCode, params object[] Parameters)
        {
            _systemMessages.Add(GetSystemMessage(MessageCode, Parameters));
        }
        /// <summary>
        /// Add require field error message
        /// </summary>
        /// <param name="FieldName">Field name</param>
        protected void AddRequireMessage(string FieldName)
        {
            _systemMessages.Add(GetRequireMessage(FieldName));
        }
        protected void AddMaxLenMessage(string FieldName, int maxLength)
        {
            AddMessage(Constants.MESSAGE.MSTD0051AERR, FieldName, maxLength.ToString("N0"));
        }
        protected void AddTimeInValidFormat(string FieldName)
        {
            AddMessage(Constants.MESSAGE.MSTD0057AERR, FieldName, "HH:mm");
        }
        protected void AddDateInValdFormat(string FieldName, string DateFormat)
        {
            AddMessage(Constants.MESSAGE.MSTD0047AERR, FieldName, DateFormat);
        }
        /// <summary>
        /// Add requirement feld error mesage
        /// </summary>
        /// <param name="FieldNames">Some fields name</param>
        protected void AddRequireMessage(List<string> FieldNames)
        {
            foreach (var f in FieldNames)
            {
                _systemMessages.Add(GetRequireMessage(f));
            }
        }

        protected void AddExceptionMessage(string exMesssage)
        {
            AddMessage("MSTD0000AERR", exMesssage, MessageStatus.ERROR);
        }
        protected void AddExceptionMessage(Exception ex)
        {
            try
            {
                string id = "MSTD0000AERR";

                string msg = ex.Message;
                if (ex is SqlException)
                {
                    var sqlEx = ex as SqlException;
                    if (sqlEx.Errors.Count > 0)
                    {
                        for (int i = 0; i < sqlEx.Errors.Count; i++)
                        {
                            var err = sqlEx.Errors[i];
                            if(err.Class == 16)
                            {
                                msg = err.Message;
                                break;
                            }
                        }
                    }
                }

                if (msg.Substring(0, 8) == "[CUSTOM]")
                {
                    string[] m = msg.Substring(8, msg.Length - 8).Split('|');
                    id = m[0];
                    msg = m[1];
                }

                AddMessage(id, msg, MessageStatus.ERROR, true);

                //    if (msg.Substring(0, 8) == "[CUSTOM]")
                //    {
                //        string[] m = msg.Substring(8, msg.Length - 8).Split('|');
                //        AddMessage(m[0], m[1], MessageStatus.ERROR);
                //    }
                //    else
                //    {
                //        AddMessage("MSTD0000AERR", ex.Message, MessageStatus.ERROR);
                //    }
                //}
            }
            catch
            {
                AddMessage("MSTD0000AERR", ex.Message, MessageStatus.ERROR, true);
            }
        }
        protected void AddCustomExceptionMessage(string message)
        {
            AddMessage("MSTD0000AERR", message, MessageStatus.ERROR);
        }

        protected void _genObjectResult(ref BaseJsonResult model)
        {
            model.SetMessage(_systemMessages != null && _systemMessages.Count > 0 ? _systemMessages.ToArray() : null);
        }
        protected void _genObjectResult(ref SearchJsonResult model)
        {
            model.SetMessage(_systemMessages != null && _systemMessages.Count > 0 ? _systemMessages.ToArray() : null);
        }

        #endregion

        #region Get system message

        /// <summary>
        /// Get require field error message 
        /// </summary>
        /// <param name="FieldName">Field name</param>
        /// <returns></returns>
        protected SystemMessageModel GetRequireMessage(string FieldName)
        {
            MessageBO bo = new MessageBO();
            return bo.GetRequireErrorMessage(FieldName);
        }

        /// <summary>
        /// Get system error message data
        /// </summary>
        /// <param name="MessageCode">Message code (Get from DR Document)</param>
        /// <param name="Parameters">Message parameters</param>
        /// <returns></returns>
        public SystemMessageModel GetSystemMessage(string MessageCode, params object[] Parameters)
        {
            MessageBO bo = new MessageBO();
            return bo.GetMessage(MessageCode, Parameters);
        }


        #endregion

        private static string GetPropertyName<TModel, TProperty>(Expression<Func<TModel, TProperty>> property)
        {
            MemberExpression memberExpression = (MemberExpression)property.Body;

            return memberExpression.Member.Name;
        }
        protected bool ValidateDate(DateTime datefrom, DateTime dateto)
        {
            bool isBool = true;
            if (datefrom.Date > dateto.Date)
            {
                isBool = false;
            }
            return isBool;
        }
        protected bool ValidateNotAllow(string text, string codition)
        {
            bool isBool = true;

            if (text.IndexOf(codition) >= 0)
            {
                isBool = false;
            }
            return isBool;
        }
        protected bool CheckUploadDocument(System.Web.HttpPostedFileBase _f, string _cfgUpload)
        {
            string[] _ff = _f.FileName.Split(new char[] { '\\' });

            string _fileName = _f.FileName;
            if (_f.FileName.Split(new char[] { '\\' }).Length > 1)
            {
                _fileName = _ff[_ff.Length - 1];
            }

            var _bo = new Common.SystemBO();
            var _MaxFileSize = _bo.GetMaximumFileKB(_cfgUpload);
            var _AllowFileExt = _bo.GetAllowFileExtension(_cfgUpload);

            if ((_f.ContentLength / 1024) > _MaxFileSize)
            {
                //Add Error Message
                //MSTD1033AERR	ERR	 Selected file size exceeds limit file size. Selected File Size : [{0}KB]. Limit File Size : [{1}KB].
                AddMessage("MSTD1033AERR", (_f.ContentLength / 1024), _MaxFileSize);
                return false;
            }

            if (!Util.CheckFileExtension(_fileName, _AllowFileExt))
            {
                //Add Error Message
                //MSTD7013BERR	ERR	 File {0} is not expected file type {1}, {2}, {3}
                AddMessage("MSTD7013BERR", _fileName, _AllowFileExt, null, null);
                return false;
            }

            return true;
        }
        protected void UploadDocument(System.Web.HttpPostedFileBase _f, string _cfgUpload, string _newFileName)
        {
            var _bo = new Common.SystemBO();

            string _path = _bo.GetPathDocUpload(_cfgUpload);
            _f.SaveAs(System.IO.Path.Combine(_path, _newFileName));
        }
        protected void DeleteDocument(string _cfgUpload, string _FileName)
        {
            var _bo = new Common.SystemBO();
            string _path = _bo.GetPathDocUpload(_cfgUpload);

            string _fullPath = System.IO.Path.Combine(_path, _FileName);

            if (!System.IO.File.Exists(_fullPath))
            {
                return;
            }

            System.IO.File.Delete(_fullPath);
        }
        protected bool ValidFormat(string _text)
        {

            DateTime dateValue;
            return DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue);

        }
        protected DateTime? Date(string _text)
        {

            DateTime dateValue;
            if (DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue))
            {
                return DateTime.ParseExact(_text, _format, cultureInfo);
            }
            else
            {
                return null;
            }

        }
        protected bool CheckUploadFile(CheckFileUploadModel Model)
        {
            if (Model == null)
            {
                return true;
            }
            bool _return = true;

            string _cfgUpload = string.Format("{0}_{1}", Model.FunctionID, Model.Folder);

            var _bo = new Common.SystemBO();
            var _MaxFileSize = _bo.GetMaximumFileKB(_cfgUpload);
            var _AllowFileExt = _bo.GetAllowFileExtension(_cfgUpload);

            if ((Model.FileSize / 1024) > _MaxFileSize)
            {
                //Add Error Message
                //MSTD1033AERR	ERR	 Selected file size exceeds limit file size. Selected File Size : [{0}KB]. Limit File Size : [{1}KB].
                AddMessage("MSTD1033AERR", (Model.FileSize / 1024), _MaxFileSize);
                return false;
            }

            if (!Util.CheckFileExtension(Model.FileName, _AllowFileExt))
            {
                //Add Error Message
                //MSTD7013BERR	ERR	 File {0} is not expected file type {1}, {2}, {3}
                AddMessage("MSTD7013BERR", Model.FileName, _AllowFileExt, null, null);
                return false;
            }


            return _return;
        }

        public JsonResultBaseModel CheckValidateUploadFile(CheckFileUploadModel data)
        {
            Begin();
            try
            {
                var _b = CheckUploadFile(data);

                if (_b)
                    return new JsonResultBaseModel() { Status = MessageStatus.INFO };

                return new JsonResultBaseModel() { Status = MessageStatus.ERROR };

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }

        }

        protected bool CheckMultiUploadFile(HttpRequestBase Request = null)
        {
            if (Request == null)
            {
                return true;
            }
            bool _return = true;
            int FileSize;
            string FileName;

            var aaa = Request.Form["TEMP_FILE_DOC"];

            string _cfgUpload = string.Format("{0}_{1}", Request.Form["FUNCTION_ID"], Request.Form["TYPE"]);

            var _bo = new Common.SystemBO();
            var _MaxFileSize = _bo.GetMaximumFileKB(_cfgUpload);
            var _AllowFileExt = _bo.GetAllowFileExtension(_cfgUpload);

            for (int i = 0; i < Request.Files.Count; i++)
            {
                FileSize = Request.Files[i].ContentLength;
                if ((FileSize / 1024) > _MaxFileSize)
                {
                    //Add Error Message
                    //MSTD1033AERR	ERR	 Selected file size exceeds limit file size. Selected File Size : [{0}KB]. Limit File Size : [{1}KB].
                    AddMessage("MSTD1033AERR", (FileSize / 1024), _MaxFileSize);
                    _return = false;
                }
                FileName = Request.Files[i].FileName;
                if (!Util.CheckFileExtension(FileName, _AllowFileExt))
                {
                    //Add Error Message
                    //MSTD7013BERR	ERR	 File {0} is not expected file type {1}, {2}, {3}
                    AddMessage("MSTD7013BERR", FileName, _AllowFileExt, null, null);
                    _return = false;
                }
            }
            return _return;
        }
    }
}
