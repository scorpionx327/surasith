﻿using System;
using System.Web;
using System.IO;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01180;
using th.co.toyota.stm.fas.BusinessObject.Common;
using System.Collections.Generic;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;
namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01180BO: BO
    {
        private WFD01180DAO dao;
        public WFD01180BO()
        {
            dao = new WFD01180DAO();
        }

        public List<WFD01180MSystemModel> GetAssetClass(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.GetAssetClass(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180MSystemModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180MSystemModel>();
            }
        }

        public List<WFD01180MSystemModel> GetMinorCategory(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.GetMinorCategory(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180MSystemModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180MSystemModel>();
            }
        }

        public List<WFD01180MSystemModel> GetCompany(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.GetCompany(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180MSystemModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180MSystemModel>();
            }
        }


        public List<WFD01180MSystemModel> GetPlateType(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.GetPlateType(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180MSystemModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180MSystemModel>();
            }
        }

        public List<WFD01180MSystemModel> CHKAssetClass(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.CHKAssetClass(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180MSystemModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180MSystemModel>();
            }
        }

        public List<WFD01180MSystemModel> GetPlateSize(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.GetPlateSize(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180MSystemModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180MSystemModel>();
            }
        }

        public List<WFD01180DepreciationModel> GetDepreciation(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.GetDepreciation(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180DepreciationModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180DepreciationModel>();
            }
        }

        public List<WFD01180DepreciationModel> ChkDepreciation(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            try
            {
                var datas = dao.ChkDepreciation(data, page);
                if (datas.Count == 0)
                {
                    return new List<WFD01180DepreciationModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD01180DepreciationModel>();
            }
        }

        public BaseJsonResult InsDeprecistion(WFD01180DepreciationModel data)
        {
            Begin();
            try
            {
                if (!ValidateData(data)) { return Result(null, null, "#AlertMessageWFD01180"); }

                dao.InsDeprecistion(data);

                AddMessage("MSTD0101AINF");
                return Result(true);
                //return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult UpdDeprecistion(WFD01180DepreciationModel data)
        {
            Begin();
            try
            {
                if (!ValidateData(data)) { return Result(null, null, "#AlertMessageEDITWFD01180"); }
                dao.UpdDeprecistion(data);
                return Result(true);
                //return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public JsonResultBaseModel DelDeprecistion(WFD01180DepreciationModel data)
        {
            Begin();
            try
            {
                dao.DelDeprecistion(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
        }

        private bool ValidateData(WFD01180DepreciationModel data)
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(data.COMPANY))
            {
                AddMessage("MSTD0031AERR", "Company");
                isValid = false;
            }
            if (string.IsNullOrEmpty(data.ASSET_CLASS))
            {
                AddMessage("MSTD0031AERR", "Asset Class");
                isValid = false;
            }

            if (string.IsNullOrEmpty(data.MINOR_CATEGORY))
            {
                AddMessage("MSTD0031AERR", "Minor Category");
                isValid = false;
            }


            return isValid;
        }
    }
}
