﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02210;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02210BO : RequestBO
    {
        private WFD02210DAO dao;
        public WFD02210BO()
        {
            dao = new WFD02210DAO();
        }
        public List<WFD02210SearchModel> GetAssetCIP(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetAssetCIP(data, page);
                bool _AllowDelete = base.GetPermission(data.DOC_NO, data.USER_BY, Constants.RequestType.RECLASS).AllowDelete == "Y" ;
                datas.ForEach(delegate (WFD02210SearchModel x)
                {
                    x.ALLOW_DELETE = _AllowDelete ? "Y" : x.ALLOW_DELETE;
                });
                if (datas.Count > 0)
                {
                    return datas;
                }
                else
                {
                    // AddMessage(Constants.MESSAGE.MSTD0059AERR);
                    return new List<WFD02210SearchModel>();
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD02210SearchModel>();
            }
        }

        public void ClearAssetCIP(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetCIP(data);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);

            }
        }

        public BaseJsonResult DeleteAssetCIP(WFD02210Model data)
        {
            /*
            Begin();
            try
            {
                PaginationModel page = new PaginationModel();
                page.ItemPerPage = 10;
                var _result = dao.GetAssetCIP(data, page);
                if (data.DOC_NO != null && data.DOC_NO != "")
                {
                    if (_result.Count == 1)
                    {
                        AddMessage(Constants.MESSAGE.MFAS0903AERR, "Asset");
                        return Result("-99");
                    }
                    else {
                        dao.DeleteAssetCIP(data);
                        return Result("0");
                    }
                }
                else
                {
                    dao.DeleteAssetCIP(data);
                    return Result("0");
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result("-99");
            }
            */
            return Result("0");

        }

        public void UpdateAssetList(WFD02210Model data)
        {
            Begin();
            try
            {
                if (data.ASSET_LIST != null)
                {
                    foreach (var _model in data.ASSET_LIST)
                    {
                        if (string.IsNullOrEmpty(_model.COMPLETE_DATE))
                        {
                            continue;
                        }
                        _model.COMPLETE_DATE = ValidateFormatDate(_model.COMPLETE_DATE);
                    }
                    dao.InsertAssetCIP(data);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void AddListMessage(List<MessageModel> datas)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                switch (datas[i].MESSAGE_TYPE)
                {
                    case "WRN":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.WARNING, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "ERR":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "INF":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.INFO, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    default:
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                }
            }
        }

        private string ValidateFormatDate(string _date)
        {
            var returnDate = "";
            //DateTime _CheckDate;
            int day;
            int year;
            int Month = 0;
            var arrDate = _date.Split('-');
            if (arrDate.Length != 3)
            {
                return "";
            }
            else
            {
                try
                {
                    day = int.Parse(arrDate[0]);
                    if (day > 2000)
                    {
                        return _date;
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }
                try
                {
                    year = int.Parse(arrDate[2]);
                }
                catch (Exception ex)
                {
                    return "";
                }
                try
                {
                    switch (arrDate[1])
                    {
                        case "Jan":
                            Month = 1;
                            break;
                        case "Feb":
                            Month = 2;
                            break;
                        case "Mar":
                            Month = 3;
                            break;
                        case "Apr":
                            Month = 4;
                            break;
                        case "May":
                            Month = 5;
                            break;
                        case "Jun":
                            Month = 6;
                            break;
                        case "Jul":
                            Month = 7;
                            break;
                        case "Aug":
                            Month = 8;
                            break;
                        case "Sep":
                            Month = 9;
                            break;
                        case "Oct":
                            Month = 10;
                            break;
                        case "Nov":
                            Month = 11;
                            break;
                        case "Dec":
                            Month = 12;
                            break;
                        default:
                            Month = 0;
                            break;
                    }
                    if (Month != 0)
                    {
                        returnDate = string.Format("{0:0000}-{1:00}-{2:00}", year, Month, day);
                        return returnDate;
                    }
                    else
                    {
                        return "";
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        private void SetCompleteDate(ref WFD02210Model ds)
        {
            if (ds.ASSET_LIST != null)
            {
                for (int i = 0; i < ds.ASSET_LIST.Count; i++)
                {
                    if (ds.ASSET_LIST[i].COMPLETE_DATE != "" && ds.ASSET_LIST[i].COMPLETE_DATE != null)
                    {
                        if (ValidateFormatDate(ds.ASSET_LIST[i].COMPLETE_DATE) != "")
                        {
                            ds.ASSET_LIST[i].COMPLETE_DATE = ValidateFormatDate(ds.ASSET_LIST[i].COMPLETE_DATE);
                        }
                    }
                }
            }
        }

        public override bool ValidationApprove(WFD01270DetailModel _data)
        {
            Begin();
            bool _f = true;
            foreach (var _model in _data.DetailCIP.ASSET_LIST)
            {
                if (string.IsNullOrEmpty(_model.COMPLETE_DATE))
                {
                    //Not Allow to remove complete date
                    AddRequireMessage("Complete Date");
                    _f = false;
                    continue;
                }

                if (ValidateFormatDate(_model.COMPLETE_DATE) == "")
                {
                    AddMessage(Constants.MESSAGE.MSTD0047AERR, _model.COMPLETE_DATE, "dd.MM.yyyy");
                    _f = false;
                    continue;
                }

                if (!ValidateDate((DateTime)Date(_model.COMPLETE_DATE), DateTime.Now))
                {
                    AddMessage("MSTD0025AERR", "Complete Date", "Current Date");
                    _f = false;
                    continue;
                }
                _model.COMPLETE_DATE = ValidateFormatDate(_model.COMPLETE_DATE);

            }
            //
            if (_data.DetailCIP.ASSET_LIST.FindAll(x => !string.IsNullOrEmpty(x.COMPLETE_DATE)).Count == 0)
            {
                //No selected item
                AddMessage("MFAS1901ERR");///MSTD0025AERR
                return false;
            }
            var _s = dao.ValidationBeforeApprove(_data.DetailCIP);
            AddListMessage(_s);
            if (_s.Count > 0)
                return false;

            return _f;
        }
        public override bool GenApproveValidation(WFD01270DetailModel _data)
        {
            try
            {
                Begin();

                var _reqdao = new DAO.WFD01270DAO();
                if (!Validation(_data))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return false;
            }
        }
        public override bool Validation(WFD01270DetailModel _data)
        {
            Begin();

            if (_data.DetailCIP == null)
            {
                AddMessage(Constants.MESSAGE.MSTD0059AERR);
                return false;
            }

            if (_data.DetailCIP.ASSET_LIST == null)
            {
                AddMessage(Constants.MESSAGE.MSTD0059AERR);
                return false;
            }

            bool _f = true;
            foreach (var _model in _data.DetailCIP.ASSET_LIST)
            {
                if (string.IsNullOrEmpty(_model.COMPLETE_DATE))
                {
                    continue;
                }

                if (ValidateFormatDate(_model.COMPLETE_DATE) == "")
                {
                    AddMessage(Constants.MESSAGE.MSTD0047AERR, _model.COMPLETE_DATE, "dd.MM.yyyy");
                    _f = false;
                    continue;
                }

                if (!ValidateDate((DateTime)Date(_model.COMPLETE_DATE), DateTime.Now))
                {
                    AddMessage("MSTD0025AERR", "Complete Date", "Current Date");
                    _f = false;
                    continue;
                }
                _model.COMPLETE_DATE = ValidateFormatDate(_model.COMPLETE_DATE);
            }

            if (!_f)
                return false;

            if (_data.DetailCIP.ASSET_LIST.FindAll(x => !string.IsNullOrEmpty(x.COMPLETE_DATE)).Count == 0)
            {
                //No selected item
                AddMessage("MFAS1901ERR");
                return false;
            }

            var _s = dao.CommonAssetValidation(new WFD01270Model
            {
                DOC_NO = _data.DetailCIP.DOC_NO,
                USER_BY = _data.DetailCIP.USER_BY,
                REQUEST_TYPE = Constants.RequestType.CIP,
                GUID = _data.DetailCIP.GUID,
                UPDATE_DATE = _data.DetailCIP.UPDATE_DATE

            });
            AddListMessage(_s);
            if (_s.Count > 0)
                return false;

            return _f;


        }

        public override bool SaveTempDocument(WFD01270DetailModel _data)
        {
            try
            {
                UpdateAssetList(_data.DetailCIP);
                return true;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return false;
            }
        }

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        //public override bool Approve(WFD01270DetailModel _data)
        //{
        //    try
        //    {
        //        Begin();

        //        dao.ApproveAssetCIP(_data.DetailCIP);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return false;
        //    }
        //}

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        public override bool Approve(WFD01270MainModel _data, WFD01270DetailModel _dataDetail)
        {
            /*
            try
            {
                Begin();

                dao.ApproveCIP(_data, _dataDetail.DetailCIP, "A", false);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            */
            return true;
        }

        public override bool ApproveAfterSubmit(List<BaseSearchModel> _list, WFD01270DetailModel _temp)
        {
            try
            {
                Begin();

                WFD02210Model _data = new WFD02210Model();
                _data.ASSET_LIST = new List<WFD02210SearchModel>();

                foreach (var _item in _list)
                {
                    _data.DOC_NO = _item.DOC_NO;
                    _data.EMP_CODE = _temp.DetailCIP.EMP_CODE;

                    var _rs = _temp.DetailCIP.ASSET_LIST.Find(x => x.ASSET_NO == _item.ASSET_NO);
                    if (_rs == null)
                        continue;

                    _data.ASSET_LIST.Add(_rs);
                }

                dao.ApproveAssetCIP(_data);
                return true;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return false;
            }
        }

        public override bool Save(WFD01270DetailModel _data, string _DocNoAssetCategoryMapping)
        {
            try
            {
                Begin();


                dao.SubmitAssetCIP(_data.DetailCIP, _DocNoAssetCategoryMapping);

                return true;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return false;
            }
        }

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        //public override bool Reject(WFD01270DetailModel _data)
        //{
        //    try
        //    {
        //        Begin();

        //        dao.RejectAssetCIP(_data.DetailCIP);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return false;
        //    }
        //}

        //FIX INCIDENT IN19-1227 (PB19-151) By Pawares M. 20190215
        public override bool Reject(WFD01270MainModel _data, WFD01270DetailModel _dataDetail)
        {
            try
            {
                Begin();

                dao.RejectCIP(_data, _dataDetail.DetailCIP, "R", false);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override void PrepareTempSelectedforApprove(WFD01270DetailModel _data)
        {
            try
            {
                Begin();

                dao.InsertAssetToTempSelected(_data.DetailCIP);

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                throw (ex);
            }
        }

        public override bool UpdateTempFileName(List<WFD01270DocumentMappingModel> _ListDoc)
        {
            return true;
        }

        public override bool UpdateDataAfterGenDocNo(List<WFD01270DocumentMappingModel> _ListDoc)
        {
            return true;
        }
    }
}
