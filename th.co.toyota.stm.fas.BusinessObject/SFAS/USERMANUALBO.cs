﻿using System;
using System.Linq;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class USERMANUALBO : BO
    {
        /// <summary>
        /// Get default search condition data
        /// </summary>
        /// <returns>UserManual Search</returns>
        public SearchJsonResult Search(PaginationModel page)
        {
            Begin();
            try
            {
                SystemDAO sysDAO = new SystemDAO();
                var userManual = sysDAO.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG, SYSTEM_SUB_CATEGORY.USER_MANUAL);

                return Result(userManual.Select(x => new { x.REMARKS, x.CODE, x.VALUE }).OrderBy(x => x.REMARKS).ToList(), page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }

        public SearchJsonResult CheckFileExist(string fullpath)
        {
            Begin();
            if (!System.IO.File.Exists(fullpath))
            {
                AddMessage("MSTD0013AERR", fullpath);
                return Result(false, null, "#AlertMessageUM");
            }
            return Result(null, null);
        }
    }
}
