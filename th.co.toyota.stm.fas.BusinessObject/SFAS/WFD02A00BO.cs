﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02210;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02A00BO : TFASTRequestBO
    {
        private WFD02A00DAO dao;
        public WFD02A00BO()
        {
            dao = new WFD02A00DAO();
        }
        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (data.COPY_MODE != "Y")
                {
                    dao.InitialAssetList(data);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }

        public List<WFD02A00Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                
                var _permission = (new WFD01170BO()).GetPermission(data);

                var datas = dao.GetAssetList(data, page);

                datas.ForEach(delegate (WFD02A00Model _item)
                {
                    _item.AllowCheck = _permission.CanCheck(_item.ALLOW_CHECK, _item.STATUS);
                    //_item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y");
                    //_item.AllowDelete = (_item.ALLOW_DELETE == "Y" && (_permission.AllowDelete == "Y")) || _item.COMMON_STATUS != "Y";
                    _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y");
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);
                });
                if (datas.Count == 0)
                {
                    return new List<WFD02A00Model>();
                }
                return datas;
            }

            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD02A00Model>();
            }
        }
        public BaseJsonResult ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        //public List<MessageModel> AddAssetListValidation(string assetNos)
        //{
        //    Begin();
        //    try
        //    {
        //        var errList = dao.AddAssetListValidation(assetNos);
        //        return errList;
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return new List<MessageModel>();
        //    }
        //}

        public BaseJsonResult InsertAsset(List<WFD02A00Model> _list)
        {
            Begin();
            try
            {
                if (_list == null || _list.Count == 0)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = "No Selected Data" });

                foreach (var _data in _list)
                    dao.InsertAsset(_data);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }
        public BaseJsonResult PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (!this.IsValidation(data))
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = "Error" });
                }
                dao.PrepareCostCenterToGenerateFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
            
        }
        public BaseJsonResult UpdateAssetList(List<WFD02A00Model> list, string _user)
        {
            Begin();
            try
            {
               
                if(list != null && list.Count > 0)
                {
                    dao.UpdateAsset(list,_user);
                }
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        private bool IsValidation(WFD0BaseRequestDocModel _data)
        {
            
            var _s = dao.CheckValidation(_data);

            this.AddMessageList(_s);
            if (_s.Count > 0)
                return false;

            return true;
        }
        //Abstract Function
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                return this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID }) ;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.ApproveValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;


            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {
            
            try
            {
                dao.Submit(_data);
                return true;
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //public override bool Resend(WFD01170SelectedAssetNoModel _data)
        //{
        //    Begin();
        //    try
        //    {
        //        dao.Resend(_data);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //public override WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data)
        //{
        //    return new WFD01170UserPermissionModel()
        //    {
        //        AllowRegen = "N",
        //        AllowGenAssetNo = "N"
        //    };
        //}

        public BaseJsonResult Resend(WFD01170MainModel _header, List<WFD02A00Model> _data, string _user)
        {
            Begin();
            try
            {
                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                if (_data != null && _data.Count > 0)
                {
                    foreach (var _item in _data)
                        _sb.AppendFormat("{0}#{1}|", _item.ASSET_NO, _item.ASSET_SUB);

                    dao.UpdateAsset(_data, _user);
                    dao.Resend(_header, new WFD01170SelectedAssetNoModel()
                    {
                        DOC_NO = _header.RequestHeaderData.DOC_NO,
                        GUID = _header.RequestHeaderData.GUID,
                     
                        UPDATE_DATE = _header.RequestHeaderData.UPDATE_DATE,
                        SelectedList = _sb.ToString(),
                        USER = _user
                    });
                }


                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public override bool Copy(WFD01170MainModel _data)
        {
            //throw new NotImplementedException();
            Begin();
            try
            {
                dao.CopyToNewImpairment(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public BaseJsonResult UpdateAmount(WFD02A00Model data, string _user)
        {
            Begin();
            try
            {

                //if (list != null && list.Count > 0)
                //{
                    dao.UpdateAmount(data, _user);
                //}
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Acknowledge(_data, Constants.DocumentStatus.UserCloseError);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
