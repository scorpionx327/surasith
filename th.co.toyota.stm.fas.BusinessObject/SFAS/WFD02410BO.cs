﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Linq;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02410;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.COMSCC;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02410BO : TFASTRequestBO
    {
        private WFD02410DAO dao;
        public WFD02410BO()
        {
            dao = new WFD02410DAO();
        }
        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (data.COPY_MODE != "Y")
                {
                    dao.InitialAssetList(data);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }
        public BaseJsonResult GetNewAssetOwnerTransfer(WFD02410HeaderModel data)
        {
            Begin();
            try
            {
                var datas = dao.GetNewAssetOwnerTransfer(data);
                if (datas.Count > 0)
                {
                    return Result(datas);
                }
                else
                {
                    // AddMessage(Constants.MESSAGE.MFAS0705AERR);// Selected Cost Center don't have SV PIC.
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }
        public List<WFD02410Model> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var _permission = (new WFD01170BO()).GetPermission(data);

                var datas = dao.GetAssetList(data, page);

                datas.ForEach(delegate (WFD02410Model _item)
                {
                    _item.AllowCheck = _permission.CanCheck(_item.ALLOW_CHECK, _item.STATUS);
                    //_item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y");
                    _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y");
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);
                });
                
                if (datas.Count == 0)
                {
                    return new List<WFD02410Model>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD02410Model>();
            }
        }

        public List<COMSCCModel> SearchCostCenter(COMSCCDefaultScreenModel data, PaginationModel page)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(data))
                    return new List<COMSCCModel>();

                var datas = new List<COMSCCModel>();
                if (!data.IsSearchRespCostCode)
                {
                    datas = dao.SearchCostCode(data, ref page);
                }
                else
                {
                    datas = dao.SearchResponseCostCode(data, ref page);
                }
                if (datas.Count == 0)
                {
                   return datas;
                }

                //return Result(datas, page);

                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
              //  return Result(null, page);
                return new List<COMSCCModel>();
            }
        }

        public BaseJsonResult ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }

        public BaseJsonResult InsertAsset(List<WFD0BaseRequestDetailModel> _list)
        {
            Begin();
            try
            {
                if (_list == null || _list.Count == 0)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = "No Selected Data" });

                foreach (var _data in _list)
                    dao.InsertAsset(_data);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }
        public BaseJsonResult PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                //Not allow to continue when 
                var _errList = dao.GenerateFlowValidation(data);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });


                dao.PrepareCostCenterToGenerateFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }

        }
        public BaseJsonResult UpdateAssetList(WFD02410HeaderModel _data, string _user)
        {
            Begin();
            try
            {

                dao.UpdateAsset(_data, _user);
                
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });

            }
            catch (Exception ex)
            {	
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
        public BaseJsonResult GetTransferReason(WFD02410Model data)
        {
            Begin();
            try
            {
                //Constants.SUB_CATEGORY.TRANSFER_REASON
                
                switch (data.FLOW_TYPE)
                {
                    case Constants.WFD02410.MassTransferCostCenter:
                        data.FLOW_TYPE = Constants.WFD02410.NormalTransferCostCenter;
                        break;
                    case Constants.WFD02410.MassTransferResponsible:
                        data.FLOW_TYPE = Constants.WFD02410.NormalTransferResponsible;
                        break;
                    case Constants.WFD02410.MassTransferEmployee:
                        data.FLOW_TYPE = Constants.WFD02410.NormalTransferEmployee;
                        break;
                       
                }

                string _subCategory = string.Format("{0}_{1}",Constants.SUB_CATEGORY.TRANSFER_REASON, data.FLOW_TYPE);

                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatas(Constants.CATEGORY.APPROVE_FLOW, _subCategory);
                if (datas.Count > 0)
                {
                    //ORDER VALUE ASC
                    datas = datas.OrderBy(x => x.VALUE).ToList();
                    return Result(datas);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MSTD1007AERR, "Transfer Reason");// NO DATA FOUND
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }
        public List<WBSMasterAutoCompleteModels> GetDropdownWBSCosProject(WFD02410Model data)
        {
            try
            {
                return dao.GetDropdownWBSCosProject(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SystemModel> GetDropdownToEmployee(WFD02410NewAssetOwnerModel data)
        {
            try
            {
                return dao.GetDropdownToEmployee(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SystemModel> GetDropdownLocation(WFD02410Model data)
        {
            try
            {
                return dao.GetDropdownLocation(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidation(WFD0BaseRequestDocModel _data)
        {            
            var _s = dao.AssetValidation(_data);
            this.AddMessageList(_s);
            if (_s.Count > 0)
                return false;

            return true;
        }
        //Abstract Function
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID }) ;

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.SubmitValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.ApproveValidation(_data);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;


            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {

            try
            {
                dao.Submit(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Acknowledge(_data, Constants.DocumentStatus.UserCloseError);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public BaseJsonResult Resend(WFD01170MainModel _header,  WFD02410HeaderModel _data, string _user)
        {
            Begin();
            try
            {
                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                if (_data.DetailList != null && _data.DetailList.Count > 0)
                {
                    foreach (var _item in _data.DetailList)
                        _sb.AppendFormat("{0}#{1}|", _item.ASSET_NO, _item.ASSET_SUB);

                    dao.UpdateAsset(_data, _user);
                    dao.Resend(_header, new WFD01170SelectedAssetNoModel() {
                         DOC_NO = _data.DOC_NO,
                         GUID = _data.GUID,
                         
                         UPDATE_DATE = _data.UPDATE_DATE,
                         SelectedList = _sb.ToString(),
                         USER = _user
                    }, _user);
                }


                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
       

      

        public override bool Copy(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.CopyToNewTransfer(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }
    }
}
