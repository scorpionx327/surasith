﻿using System;
using System.Web;
using System.IO;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02510;
using th.co.toyota.stm.fas.BusinessObject.Common;
using System.Collections.Generic;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02510BO : TFASTRequestBO
    {
        private WFD02510DAO dao;
        public WFD02510BO()
        {
            dao = new WFD02510DAO();
        }

        public void init(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                if (data.COPY_MODE != "Y")
                {
                    dao.InitialAssetList(data);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
            }
        }

        #region GetDataFromSystem
        public BaseJsonResult GetDisposalReason(WFD02510Model data)
        {
            Begin();
            try
            {
                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatas(Constants.CATEGORY.APPROVE_FLOW, Constants.SUB_CATEGORY.DISPOSAL_REASON);
                if (datas.Count > 0)
                {
                    return Result(datas);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MSTD1007AERR, "Disposal Reason");// NO DATA FOUND
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public BaseJsonResult GetBOIStatus(WFD02510Model data)
        {
            Begin();
            try
            {
                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatas(Constants.CATEGORY.APPROVE_FLOW, Constants.SUB_CATEGORY.BOI_STATUS);
                if (datas.Count > 0)
                {
                    return Result(datas);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MSTD1007AERR, "BOI Status");// NO DATA FOUND
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public BaseJsonResult GetStoreStatus(WFD02510Model data)
        {
            Begin();
            try
            {
                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatasOrderByRemark(Constants.CATEGORY.APPROVE_FLOW, Constants.SUB_CATEGORY.STORE_STATUS);
                if (datas.Count > 0)
                {
                    return Result(datas);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MSTD1007AERR, "Store Status");// NO DATA FOUND
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }
        public BaseJsonResult GetKeepByStatus(WFD02510Model data)
        {
            Begin();
            try
            {
                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatas(Constants.CATEGORY.APPROVE_FLOW, Constants.SUB_CATEGORY.KEEP_BY);
                if (datas.Count > 0)
                {
                    return Result(datas);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MSTD1007AERR, "Keep by Status");// NO DATA FOUND
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }
        public BaseJsonResult GetSaleProcessStatus(WFD02510Model data)
        {
            Begin();
            try
            {
                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatas(Constants.CATEGORY.APPROVE_FLOW, Constants.SUB_CATEGORY.SALE_PROCESS);
                if (datas.Count > 0)
                {
                    return Result(datas);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MSTD1007AERR, "Sale Process Status");// NO DATA FOUND
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }
        #endregion

        #region OperationDisposal
        public BaseJsonResult GetDataHeader(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                var datas = dao.GetDataHeader(data);
                if (datas.Count > 0)
                {
                    return Result(datas[0]);
                }
                else
                {
                    return Result(null);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public BaseJsonResult CheckMassPermission(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                var _s = new SystemBO().SelectSystemDatas("SYSTEM_CONFIG", "ALLOW_ROLE_MASS");
                if (_s.FindAll(x => x.VALUE == data.ACT_ROLE).Count > 0)
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = string.Empty });
                }
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = string.Empty });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

     
        #endregion

      
        
        public List<WFD02510AssetModel> GetAssetList(WFD0BaseRequestDocModel data, PaginationModel page, bool _isAECUser)
        {
            Begin();
            try
            {
                //Get Document Status
                string _DocStatus = "00";

                var _header = (new WFD01170BO()).GetRequestHeader(data);
                if (_header != null)
                {
                    _DocStatus = _header.STATUS;
                }



                var _permission = (new WFD01170BO()).GetPermission(data);

                var _permissionRetirement = new WFD01170UserPermissionModel();
               
                _permissionRetirement = (new WFD01170BO()).GetPermissionRetirement(data);
                if(_permissionRetirement != null)
                {
                    _permission.ActionRoleIndex = _permissionRetirement.ActionRoleIndex;

                    _permission.AllowApprove = (_permissionRetirement.AllowApprove == null) ? _permission.AllowApprove : _permissionRetirement.AllowApprove;
                    _permission.AllowReject = (_permissionRetirement.AllowReject == null) ? _permission.AllowReject : _permissionRetirement.AllowReject;
                    _permission.AllowEditRetirement = (_permissionRetirement.AllowEdit == null) ? _permission.AllowEdit : _permissionRetirement.AllowEdit;
                    _permission.ActionMark = (_permissionRetirement.ActionMark == null) ? _permission.ActionMark : _permissionRetirement.ActionMark;
                    
                    _permission.AllowResend = (_permissionRetirement.AllowResend == null) ? _permission.AllowResend : _permissionRetirement.AllowResend;
                    
                }
                if (_permission.DOC_STATUS != "00")
                {
                    _permission.AllowDelete = (_permission.AllowApprove == "N" ? "N" : _permission.AllowDelete);
                }
                var datas = dao.GetAssetList(data, page);

                var _MachingLicenseCnt = datas.FindAll(x => x.IS_MACHINE_LICENSE == "Y").Count;

                var _isDetailEnable = (_permission.IsAECState == "Y" && _permission.IsMainApprover == "Y") || //Support ACM
                                       (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y") ||
                                          ((_permission.IsAECUser == "Y" || _permission.IsMainApprover == "Y") && //supprt aec operate instead main
                                                Constants.WFD2510.AllowEnableDetail.Contains(string.Format("|{0}|", _permission.ActionMark))) ||
                                                (_permissionRetirement.IsActionMarker == "Y" && Constants.WFD2510.AllowEnableDetail.Contains(string.Format("|{0}|", _permission.ActionMark)));

                //2020.01.25 Surasith T. support already input detail info, all user can see detail
                _isDetailEnable = _isDetailEnable || datas.FindAll(x => !string.IsNullOrEmpty(x.DETAIL_DATE)).Count > 0;

                datas.ForEach(delegate (WFD02510AssetModel _item)
                {
                    _item.ENABLE_TMC_DOC = _MachingLicenseCnt > 0;

                    //Enable view mode support ACM
                   
                     _item.IsDetailView = (_permission.ActRole == Constants.Role.AECManager) && (_item.STATUS == "NEW" || _item.STATUS == "SENT");
                    
                    _item.ENABLE_DETAIL = _isDetailEnable ;

                    //_item.AllowCheck = _permission.CanCheck(_item.ALLOW_CHECK, _item.STATUS);
                    _item.AllowCheck = _permission.IsAEC && _item.ALLOW_CHECK == "Y";
                    //_item.AllowCheck = _item.ALLOW_CHECK == "Y" && (_permission.IsAECState == "Y" && _permission.IsAECUser == "Y");
                    _item.AllowEdit = false;
                    if (_permission.AllowResend == "Y" || _permission.AllowResubmit == "Y" || _permission.AllowApprove == "Y" || _permission.DOC_STATUS == "00")
                    {
                        _item.AllowEdit = _item.ALLOW_EDIT == "Y" && (_permission.AllowEdit == "Y") &&
                                            (_item.STATUS == "NEW" || (_permission.IsAECUser == "Y" && _item.STATUS == "SENT"));
                    }

                    _item.AllowEditRetirement = false;
                    if (_permission.AllowResend == "Y" || _permission.AllowApprove == "Y")
                    {
                        _item.AllowEditRetirement = (_permissionRetirement.ActionRoleIndex == _item.ROW_STATE &&
                                                            _permissionRetirement.AllowEdit == "Y" && _item.STATUS == "NEW") ||
                                                    (_permission.IsAECUser == "Y" && _permissionRetirement.AllowEdit == "Y" && _item.STATUS == "NEW") || //Suport AEC operate instead 2019.12.22
                                                        (_permission.AllowResend == "Y" && _permission.IsAECUser == "Y" && _item.STATUS == "SENT"); //Support Resent
                    }
                    _item.AllowDelete = base.AllowDelete(_item.ALLOW_DELETE, _permission, _item.COMMON_STATUS);

                    _item.IsBOIUpload = _permission.IsBOIState == "A" && (_permission.IsBOIUser == "Y" || _permission.IsAECUser == "Y");
                    _item.IsBOIView = _permission.IsBOIState == "Y" ;

                });
                if (datas.Count == 0)
                {
                    return new List<WFD02510AssetModel>();
                }
                return datas;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return new List<WFD02510AssetModel>();
            }

           
        }

        

        public BaseJsonResult ClearAssetList(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

        public BaseJsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            Begin();
            try
            {
                dao.DeleteAsset(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }


        }

        public BaseJsonResult InsertAsset(List<WFD0BaseRequestDetailModel> _list)
        {
            Begin();
            try
            {
                foreach (var _data in _list)
                    dao.InsertAsset(_data);

                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }

      
        public BaseJsonResult PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
                //Not allow to continue when 
                var _errList = dao.GenerateFlowValidation(data);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR });


                dao.PrepareCostCenterToGenerateFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }

        }
        public BaseJsonResult UpdateAssetList(WFD02510HeaderModel _data, string _user)
        {
            Begin();
            try
            {
                if (_data == null)
                {
                    return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
                }

                var _flowTypeList = new SystemBO().SelectSystemDatas("APPROVE_FLOW", "DISPOSE_FLOW_TYPE");
                if(_flowTypeList != null && _flowTypeList.FindAll(x => x.CODE.ToUpper() == string.Format("{0}_{1}", _data.DISP_TYPE, _data.ROUTE_TYPE).ToUpper()).Count > 0)
                {
                    _data.FLOW_TYPE = _flowTypeList.Find(x => x.CODE.ToUpper() == string.Format("{0}_{1}", _data.DISP_TYPE, _data.ROUTE_TYPE).ToUpper()).VALUE;
                }

                dao.UpdateAsset(_data, _user);

                
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
        private bool IsValidation(WFD0BaseRequestDocModel _data)
        {
            var _s = dao.AssetValidation(_data);
            this.AddMessageList(_s);
            if (_s.Count > 0)
                return false;

            return true;
        }
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                var _s = this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID });

                if (!_s)
                    return false;

                //Not allow to continue when 
                var _errList = dao.SubmitValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            try
            {
               

                //Not allow to continue when 
                var _errList = dao.ApproveValidation(_data.RequestHeaderData);
                this.AddMessageList(_errList);
                if (_errList.Count > 0)
                    return false;

                return true;


            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return false;
            }
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            return true;
        }

        public override bool Submit(WFD01170MainModel _data)
        {

            try
            {
                dao.Submit(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //public override bool Resend(WFD01170SelectedAssetNoModel _data)
        //{
        //    Begin();
        //    try
        //    {
        //        dao.Resend(_data);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        //public override WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data)
        //{
        //    return new WFD01170UserPermissionModel()
        //    {
        //        AllowRegen = "N",
        //        AllowGenAssetNo = "N"
        //    };
        //}

        public BaseJsonResult Resend(WFD01170MainModel _header, WFD02510HeaderModel _data, string _user)
        {
            Begin();
            try
            {
                System.Text.StringBuilder _sb = new System.Text.StringBuilder();
                if (_data.DetailList != null && _data.DetailList.Count > 0)
                {
                    foreach (var _item in _data.DetailList)
                        _sb.AppendFormat("{0}#{1}|", _item.ASSET_NO, _item.ASSET_SUB);

                    dao.UpdateAsset(_data, _user);
                    dao.Resend(_header, new WFD01170SelectedAssetNoModel()
                    {
                        DOC_NO = _data.DOC_NO,
                        GUID = _data.GUID,
                       
                        UPDATE_DATE = _data.UPDATE_DATE,
                        SelectedList = _sb.ToString(),
                        USER = _user
                    });
                }


                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }
        }
        public override bool Copy(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.CopyToNewRetirement(_data); //Dispose old name
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            Begin();
            try
            {
                dao.Acknowledge(_data, Constants.DocumentStatus.UserCloseError);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
