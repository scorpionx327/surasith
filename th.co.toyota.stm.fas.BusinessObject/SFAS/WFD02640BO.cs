﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.WFD02640;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02640BO : TFASTRequestBO
    {
        private WFD02640DAO dao;
        private SystemConfigurationDAO system_dao;

        public WFD02640BO()
        {
            dao = new WFD02640DAO();
            system_dao = new SystemConfigurationDAO();
        }
        public string GetEmpTitle(string company, string year, string round, string assetLocation, string empCode)
        {
            
            return dao.GetEmpTitle(company, year, round, assetLocation, empCode);
        }
        public string GetActiveStatusPerSV(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION, string SV_EMP_CODE, string USER_LOGON)
        {
            try
            {
                string _Permission;
                _Permission = dao.GetActiveStatusPerSV(COMPANY, YEAR, ROUND, ASSET_LOCATION, SV_EMP_CODE, USER_LOGON);
                return _Permission;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BaseJsonResult SetInitialDataIncaseReSubmit(string DocNo)
        {

              
            Begin();
            try
            {
                dao.SetInitialDataIncaseReSubmit(DocNo);
                return Result("0");
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result("-99");
            }
        }

        public WFD02640SearchModel GetInitialDataByDocNo(string DocNo)
            {
                 return dao.GetInitialDataByDocNo(DocNo);
            }

        public BaseJsonResult GetOnSrcreenLoad(WFD02640SearchModel data)
        {
            Begin();
            try
            {
                if (data == null || data.YEAR == null) //Add by Surasith : Support Incase document is rejected
                    return Result(null);

                WFD02640ScreenModel result = dao.GetScreenOnload(data);
                result.EMP_NAME = dao.GetEmpTitle(data.COMPANY, data.YEAR, data.ROUND, data.ASSET_LOCATION, data.SV_CODE); // getNameEMp(data.SV_CODE);

                result.SummaryTime.CLASS = GetClass(result.SummaryTime.CLASS);

                result.SummaryAsset.CLASS = GetBGClass(result.SummaryAsset.CLASS);
                

                if (dao.CheckCreateRequestStock(data.COMPANY, data.YEAR, data.ROUND, data.ASSET_LOCATION, data.SV_CODE))
                {
                    if (result.SummaryAsset.ICON == "")
                    {
                        if (result.SummaryAsset.CLASS == "bg-yellow")
                        {
                            result.SummaryAsset.ICON = "fa-caret-up";
                            result.SummaryAsset.IMGSRC = @"/Images/caret.gif";
                        }
                        else
                        {
                            result.SummaryAsset.ICON = "fa-circle-thin";
                            if (result.SummaryAsset.CLASS == "bg-aqua")
                            {
                                result.SummaryAsset.IMGSRC = @"/Images/circle_aqua.gif";
                            }
                            else
                            {
                                result.SummaryAsset.IMGSRC = @"/Images/circle.gif";
                            }
                        }
                    }
                    else
                    {
                        result.SummaryAsset.IMGSRC = GetImg(result.SummaryAsset.ICON);
                        result.SummaryAsset.ICON = GeIcon(result.SummaryAsset.ICON);
                    }

                }

                List<WFD02640SummaryDetailModel> List = new List<WFD02640SummaryDetailModel>();
                foreach (var item in result.SummaryDetail)
                {
                    WFD02640SummaryDetailModel detail = new WFD02640SummaryDetailModel();
                    detail.TOTAL = item.TOTAL;
                    detail.STATUS = GetBGClass(item.STATUS);
                    detail.PERSEN = item.PERSEN;
                    detail.DESCRIPTION = item.DESCRIPTION;
                    detail.COST_CENTER_NAME = item.COST_CENTER_NAME;
                    detail.COST_CENTER_CODE = item.COST_CENTER_CODE;
                    detail.COMMENT = item.COMMENT;
                    detail.ATTACHMENT = item.ATTACHMENT;
                    detail.CHECKED = item.CHECKED;
                    List.Add(detail);
                }
                result.SummaryDetail = List;
                return Result(result);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }

        }

        public BaseJsonResult SaveNoticeEmail(WFD02640SearchModel data)
        {
            Begin();
            try
            {
                dao.SaveNoticaEmail(data);
                AddMessage("MFA1503AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }

        public BaseJsonResult SaveTempRequestStockBO(WFD02640SearchModel data, HttpRequestBase Request = null)
        {
            Begin();
            try
            {
                if (Request.Files.Count > 0)
                {
                    //if (!ValidationFile(data, Request)) return Result(false);
                    string fname;
                    string file_name;
                    string extension;
                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = data.FILE.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = data.FILE.FileName;
                    }
                    extension = Path.GetExtension(fname);
                    file_name = data.COST_CENTER + data.YEAR + data.ROUND + data.ASSET_LOCATION + extension;
                    // Get the complete folder path and store the file inside it.  
                    string _cfgUpload = string.Format("WFD02640_{0}", "STOCK");


                    UploadDocument(Request.Files[0], _cfgUpload, file_name);

                    //fname = Path.Combine(GetPathUploadTemp(), file_name);
                    //data.FILE.SaveAs(fname);

                    data.ATTACHMENT = file_name;
                  
                }
                else
                {
                    data.ATTACHMENT = null;
                }
                dao.SaveTempRequestStockDAO(data);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }

        public BaseJsonResult UpdateAssetList(WFD02640SearchModel data)
        {
            Begin();
            try
            {
                
                dao.UpdateAssetList(data);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }

        public BaseJsonResult CheckValidateUploadFile(CheckFileUploadModel data)
        {
            Begin();
            try
            {
                var _bo = CheckUploadFile(data);

                return Result(_bo);


            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }

        }

        public BaseJsonResult InsertAssetList(WFD02640SearchModel data)
        {
            Begin();
            try
            {
                dao.InsertAssetList(data);
                return Result("0");
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result("-99");
            }

        }

        public BaseJsonResult ClearAssetList(WFD02640SearchModel data)
        {
            Begin();
            try
            {
                dao.ClearAssetList(data);
                return Result("0");
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result("-99");
            }

        }

        public BaseJsonResult UpdateFileToTempStock(WFD02640SearchModel data)
        {
            Begin();
            try
            {
                dao.UpdateFileToTempStock(data);
                return Result("0");
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result("-99");
            }

        }

        private bool ValidationFile(WFD02640SearchModel data, HttpRequestBase Request = null)
        {
            bool isBool = true;
            if (Request.Files.Count > 0)
            {
                int size = GetFileSize();
                if (data.FILE.ContentLength > size)
                {
                    AddMessage("MSTD7013BERR", data.FILE.FileName, size + "KB maximum", "", "");
                    isBool = false;
                }
            }
            return isBool;
        }

        private string GetBGClass(string param)
        {
            string txt = "";
            switch (param)
            {
                case "Complete":
                    txt = "bg-green";
                    break;
                case "Complete with Delay":
                    txt = "bg-yellow";
                    break;
                case "Delay":
                    txt = "bg-yellow";
                    break;
                case "On Progress":
                    txt = "bg-aqua";
                    break;
                case "Complete with Loss":
                    txt = "bg-red";
                    break;
                case "not_yet_scan":
                    txt = "";
                    break;
            }
            return txt;
        }

        private string GetClass(string param)
        {
            string txt = "";
            switch (param)
            {
                case "Complete":
                    txt = "progress-bar-green";
                    break;
                case "Complete with Delay":
                    txt = "progress-bar-yellow";
                    break;
                case "Delay":
                    txt = "progress-bar-yellow";
                    break;
                case "On Progress":
                    txt = "progress-bar-aqua";
                    break;
                case "Complete with Loss":
                    txt = "progress-bar-red";
                    break;
                case "not_yet_scan":
                    txt = "";
                    break;
            }
            return txt;
        }

        private string GeIcon(string param)
        {
            string txt = "";
            switch (param)
            {
                case "CIRCLE":
                    txt = "fa-circle-thin";
                    break;
                case "TRIANGLE":
                    txt = "fa-caret-up";
                    break;
                case "CROSS":
                    txt = "fa-close";
                    break;
            }
            return txt;
        }

        private string GetImg(string param)
        {
            string txt = "";
            switch (param)
            {
                case "CIRCLE":
                    txt = @"/Images/circle.gif";
                    break;
                case "TRIANGLE":
                    txt = @"/Images/caret.gif";
                    break;
                case "CROSS":
                    txt = @"/Images/cross.gif";
                    break;
            }
            return txt;
        }

        

        private int GetFileSize()
        {
            int size = 0;
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "FILE_SIZE";
            m.CODE = "WFD02640";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                size = Int32.Parse(data.VALUE);
            }

            return size;
        }

        public Byte[] ReadLocalFile( string fileName, string filepath)
        {
            string file = filepath+ fileName;

            fileName = new FileInfo(file).Name;

            MemoryStream memoryStream = new MemoryStream();
            using (Stream input = File.OpenRead(file))
            {
                byte[] buffer = new byte[32 * 1024]; // 32K buffer for example
                int bytesRead;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, bytesRead);
                }
            }
            memoryStream.Position = 0;
            Byte[] byteArray = memoryStream.ToArray();
            memoryStream.Flush();
            memoryStream.Close();

            return byteArray;
        }
        

        //public  bool IsValidation(WFD01270DetailModel _data)
        //{
        //    Begin();

        //    if (_data.DetailStockTaking == null || _data.DetailStockTaking.COST_CENTER_LIST == null)
        //    {
        //        AddMessage(Constants.MESSAGE.MSTD0059AERR);
        //        return false;
        //    }

        //    if (_data.DetailStockTaking.COST_CENTER_LIST.Count() == 0)
        //    {
        //        AddMessage(Constants.MESSAGE.MSTD0059AERR);
        //        return false;
        //    }

            
        //    string _cc = dao.ValidateSubmitIsRequireComment(_data.DetailStockTaking);
        //    if (!string.IsNullOrEmpty(_cc))
        //    {
        //        AddMessage(Constants.MESSAGE.MFAS1605AERR, _cc);//no input comment and atth
        //        return false;
        //    }

        //    string _ccd =  dao.ValidateSubmitIsRequireCommentWithDelay(_data.DetailStockTaking);
        //    if (!string.IsNullOrEmpty(_ccd))
        //    {
        //        AddMessage(Constants.MESSAGE.MFAS1606AERR, _ccd);//no input comment 
        //        return false;
        //    }



        //    bool _f = true;

        //    if (!_f)
        //        return false;

        //    //var _s = dao.CommonAssetValidation(new WFD01270Model
        //    //{
        //    //    DOC_NO = _data.DetailStockTaking.DOC_NO,
        //    //    USER_BY = _data.DetailStockTaking.USER_BY,
        //    //    REQUEST_TYPE = Constants.RequestType.STOCK,
        //    //    GUID = _data.DetailStockTaking.GUID,
        //    //    UPDATE_DATE = _data.DetailStockTaking.UPDATE_DATE

        //    //});
           

        //    return _f;
        //}

       

        //private string ValidateSubmitIsRequireComment(WFD02640Model _data)
        //{
        //    return dao.ValidateSubmitIsRequireComment(_data);
        //}

      
        

        public Models.WFD01170UserPermissionModel GetPermission(WFD02640SearchModel data)
        {
            try
            {
                 var _Permission = (new DAO.WFD01170DAO()).GetPermission(new Models.WFD0BaseRequestDocModel() { DOC_NO = data.DOCUMENT,
                        USER_BY = data.EMP_CODE, REQUEST_TYPE = Constants.RequestType.STOCK });
                
                if(string.IsNullOrEmpty(_Permission.DOC_NO))
                {
                    //Check 
                    var _complete = dao.GetSpecialPermission(data);
                    _Permission.Mode = (_complete == "Y" ? "V" : _Permission.Mode);
                    _Permission.AllowApprove = (_complete == "Y" ? "N" : _Permission.AllowApprove);
                }

                return _Permission;
            }
            catch (Exception ex)
            {
                return new Models.WFD01170UserPermissionModel();
            }
        }

        public BaseJsonResult DeleteFileAttchDoc(CheckFileUploadModel data)
        {
            Begin();
            SystemBO bo = new SystemBO();

            try
            {
                if (data == null)
                {
                    return Result(new { FileName = string.Empty });
                }
                if (string.IsNullOrEmpty(data.FileName))
                {
                    return Result(new { FileName = string.Empty });
                }

                string _cfgUpload = string.Format("WFD02640_{0}", "STOCK");

                DeleteDocument(_cfgUpload, data.FileName);

                data.FileName = string.Empty;

                return Result(new { FileName = string.Empty });

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new { FileName = string.Empty });
            }
        }

        public BaseJsonResult PrepareCostCenterToGenerateFlow(WFD0BaseRequestDocModel data)
        {
            Begin();
            try
            {
               
                dao.PrepareCostCenterToGenerateFlow(data);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.INFO });
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });
            }

        }
        public override bool SubmitValidation(WFD01170MainModel _data)
        {
            return true;
            //Begin();
            //try
            //{
            //    return this.IsValidation(new WFD0BaseRequestDocModel() { GUID = _data.RequestHeaderData.GUID }) ;
            //}
            //catch (Exception ex)
            //{
            //    AddMessage("Error", ex.Message, MessageStatus.ERROR);
            //    return false;
            //}
        }

        public override bool ApproveValidation(WFD01170MainModel _data)
        {
            Begin();
            bool _f = true;

            // string _cc = dao.ValidateSubmitIsRequireComment(_data.DetailStockTaking);
            //string _cc = string.Empty;
            //if (!string.IsNullOrEmpty(_cc))
            //{
            //    AddMessage(Constants.MESSAGE.MFAS1605AERR, _cc);//no input comment and atth
            //    return false;
            //}

          //  var _s = dao.ValidationBeforeApprove(_data.DetailStockTaking);

           // if (_s.Count > 0)
           // {
           //     _f = false;
           ////     AddListMessage(_s);
           // }

            return _f;
        }

        public override bool RejectValidation(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Submit(WFD01170MainModel _data)
        {
            try
            {
                Begin();

                dao.Submit(_data);
                return true;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                throw (ex);
            }
        }

        public override bool Approve(WFD01170MainModel _data)
        {
            try
            {
                Begin();

                dao.Approve(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public override bool Reject(WFD01170MainModel _data)
        {
            try
            {
                Begin();

                dao.Reject(_data);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        
        //public override WFD01170UserPermissionModel GetDetailPermission(WFD0BaseRequestDocModel _data)
        //{
        //    return new WFD01170UserPermissionModel()
        //    {
        //        AllowRegen = "N",
        //        AllowGenAssetNo = "N"
        //    };
        //}

       

        public override bool Copy(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool InsertChangeCommentResubmit(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }

        public override bool Acknowledge(WFD01170MainModel _data)
        {
            throw new NotImplementedException();
        }
    }
}
