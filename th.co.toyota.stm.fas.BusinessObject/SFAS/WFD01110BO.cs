﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD01110;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01110BO : BO
    {
        private WFD01110DAO dao;
        private SystemConfigurationDAO system_dao;
        private MessageDAO mdao;
        private string startFileName = "AnnouncementPhoto";
        public WFD01110BO()
        {
            dao = new WFD01110DAO();
            system_dao = new SystemConfigurationDAO();
            mdao = new MessageDAO();
        }
        public SearchJsonResult GetTransferBO(WFD01110TransferModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetTransferDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        public SearchJsonResult GetAssetNewBO(WFD01110AssetNewModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetAssetNewDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        public SearchJsonResult GetSettlementwBO(WFD01110SettlementModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetSettlementDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        //Reclassification
        public SearchJsonResult GetReclassificationBO(WFD01110ReclassificationModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetReclassificationDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        //Asset Change
        public SearchJsonResult GetAssetChangeBO(WFD01110AssetChangeModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetAssetChangeDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        //Asset Change
        public SearchJsonResult GetReprintBO(WFD01110AssetChangeModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetReprintDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }


        //Retirement
        public SearchJsonResult GetRetirementBO(WFD01110RetirementModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetRetirementDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        //Imparment
        public SearchJsonResult GetImparmentBO(WFD01110ImpairmentModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetImpairmentDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        //Physical Count
        public SearchJsonResult GetPhysicalCountBO(WFD01110PhysicalCountModel model, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetPhysicalCountDAO(model, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        public SearchJsonResult GetOnProcessBO(WFD01110SearchRequestModel data, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetOnProcessDAO(data, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }
        public SearchJsonResult GetOnHandAECBO(WFD01110SearchRequestModel data, string EMP_CODE, PaginationModel page)
        {
            Begin();
            try
            {
                var datas = dao.GetOnHandAEC(data, EMP_CODE, ref page);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        public BaseJsonResult GetRequestTypeBO()
        {
            Begin();
            try
            {


                SystemConfigurationModels type = new SystemConfigurationModels();
                type.CATEGORY = "SYSTEM_CONFIG";
                type.SUB_CATEGORY = "REQUEST_TYPE_HOME";
                var datas = system_dao.GetSystemData(type).Where(x => x.ACTIVE_FLAG == "Y").OrderBy(x => x.REMARKS);

                return Result(new { data = datas });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public WFD01110ToDoListModel GetToDoListBO(string emp_code)
        {
            var datas = dao.GetToDoListDAO(emp_code);
            return datas;
        }
        public WFD01110ToDoListFASupModel GetToDoListFASupBO(string emp_code)
        {
            var datas = dao.GetToDoListFASubDAO(emp_code);
            return datas;
        }
        public BaseJsonResult GetAnnouncementBO(WFD01110SearchRequestModel com, List<string> companyAccesses)
        {
            Begin();
            try
            {
                List<WFD01110AnnouncementModel> ch = new List<WFD01110AnnouncementModel>();
                ch = dao.GetAnnouncementDAO(com);
                #region Test
                //if (ch != null)
                //{
                //    string carouselHeader = "<div class=\"bs-example\" data-example-id=\"simple-carousel\"><div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">";
                //    string carouselIndicators = "<ol class=\"carousel-indicators\">";
                //    string carouselInner = "<div class=\"carousel-inner\" role=\"listbox\">";
                //    string carouselControl = "<a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\"><span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span><div class=\"fixed-top\">FIXED-TOP</div><span class=\"sr-only\">Previous</span></a><a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\"><span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span><span class=\"sr-only\">Next</span></a>";
                //    string setActive = string.Empty;
                //    string item = string.Empty;

                //    var builder = new StringBuilder();

                //    for (int j = 0; j < ch.Count; j++)
                //    {

                //        for (int i = 0; i < ch.Count; i++)
                //        {
                //            setActive = string.Empty;

                //            if (i == 0)
                //            {
                //                setActive = " class=\"active\"";
                //                item = "item next left";
                //            }
                //            if (i == 1)
                //                item = "item next";
                //            if (i == 2)
                //                item = "item active left";

                //            var ANNOUNCE_URL = string.Format("{0}{1}", GetPathUploadURL(), ch[i].ANNOUNCE_URL);

                //            carouselIndicators += string.Format("<li data-target=\"#carousel-example-generic\" data-slide-to=\"{0}\" {1}></li>", i, setActive);
                //            carouselInner += string.Format("<div class=\"{1}\"> <img data-src = \"holder.js/900x500/auto/#777:#555/text:First slide\" alt = \"First slide [900x500]\" src = \"{0}\" data-holder-rendered = \"true\" > </div >", ANNOUNCE_URL, item);

                //        }

                //        builder.Append(carouselHeader);
                //        builder.Append(carouselIndicators + "</ol>");
                //        builder.Append(carouselInner + "</div>");
                //        builder.Append(carouselControl);
                //        builder.Append("</div></div>");

                //        ch[j].ANNOUNCE_URL = builder.ToString();

                //    }

                //}
                #endregion

                for (int i = 0; i < ch.Count; i++)
                {
                    if (ch != null)
                    {
                        if (!string.IsNullOrEmpty(ch[i].ANNOUNCE_URL) && ch[i].ANNOUNCE_TYPE == "P")
                        {
                            ch[i].ANNOUNCE_URL = string.Format("{0}{1}", GetPathUploadURL(), ch[i].ANNOUNCE_URL);

                        }
                        if (!string.IsNullOrEmpty(ch[i].ANNOUNCE_URL) && ch[i].ANNOUNCE_TYPE == "T")
                        {
                            ch[i].ANNOUNCE_URL = string.Format("{0}{1}", GetPathUploadURL(), "bg-wrigth.png");

                        }
                    }
                }

                //Where only company has access
                ch = ch.Where(c => companyAccesses.Any(x => x == c.COMPANY)).ToList();

                return Result(new { data = ch });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public WFD01110TotalPerTabModel GetTotalPerTab(string emp_code)
        {
            var datas = dao.GetTotalPerTab(emp_code);
            return datas;
        }

        public SearchJsonResult GetRequestSummary(string emp_code)
        {
            Begin();
            var page = new PaginationModel();
            try
            {
                var datas = dao.GetRequestSummary(emp_code);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }


        }


        public BaseJsonResult SaveTextAnnouncement(WFD01110SaveAnnouncementModel data, HttpRequestBase Request = null)
        {
            Begin();
            bool isBool = false;

            try
            {
                if (!Validation(data)) return Result(new { status = isBool });

                if (data.ANNOUNCE_TYPE == "P")
                {
                    if (Request.Files.Count > 0)
                    {
                        string fname = "";
                        //string file_name = "";
                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = data.FILE.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = data.FILE.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        string _newFileName = string.Format("{0}_{1:yyyyMMdd_HHmmss}{2}", startFileName, DateTime.Now, Util.GetExtension(fname));

                        string[] fileList = System.IO.Directory.GetFiles(GetPathUpload(), startFileName + "*");
                        //foreach (string file in fileList)
                        //{
                        //    System.IO.File.Delete(file);
                        //}

                        fname = Path.Combine(GetPathUpload(), _newFileName);
                        data.FILE.SaveAs(fname);

                        data.ANNOUNCE_URL = _newFileName;
                    }
                    else
                    {
                        data.ANNOUNCE_URL = "";
                    }
                }

                var imgUrl = string.Format("{0}{1}", GetPathUploadURL(), data.ANNOUNCE_URL);

                dao.SaveAnnouncementDAO(data);
                AddMessage("MSTD0101AINF");
                isBool = true;
                return Result(new { status = isBool, imgUrl });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(new { status = isBool });
            }
        }





        public bool Validation(WFD01110SaveAnnouncementModel data)
        {
            bool isBool = true;
            //if (string.IsNullOrEmpty(data.ANNOUNCE_DESC) && data.ANNOUNCE_TYPE == "T")
            //{
            //    AddMessage("MSTD0043AERR", "Announcement");
            //    isBool = false;
            //}

            if (data.ANNOUNCE_TYPE == "P")
            {
                int size = GetFileSize(); //KB
                if (data.FILE.ContentLength / 1024 > size) //KB
                {
                    AddMessage("MSTD1033AERR", (data.FILE.ContentLength / 1024), (size));
                    isBool = false;
                }
            }

            return isBool;
        }
        private int GetFileSize()
        {
            int size = 0;
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "FILE_SIZE";
            m.CODE = "WFD01110";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                size = Int32.Parse(data.VALUE);
            }

            return size;
        }
        private string GetPathUpload()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_UPLOAD";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }

            return path;
        }
        public string GetImageDefalut()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_DEFAULT_UPLOAD";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }

        public MessageModel GetMessage(string MESSAGE_CODE)
        {
            return mdao.GetMessage(MESSAGE_CODE);
        }

        public string CheckStockTake(string _User)
        {
            return dao.CheckStockTakeDAO(_User);
        }

        public BaseJsonResult GetStockTakeBO(string EMP_CODE, PaginationModel page)
        {
            Begin();

            try
            {
                var datas = dao.GetStockTakeDAO(EMP_CODE, ref page);
                return Result(datas, page);

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }


        public string ConvertImagePathToByte(string ImagePath)
        {

            string base64Image = string.Empty;
            if (File.Exists(ImagePath))
            {
                string extension = string.Empty;
                extension = Path.GetExtension(ImagePath).Replace(".", "");

                byte[] imageArray = System.IO.File.ReadAllBytes(ImagePath);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                base64Image = string.Format("data:image/{0};base64,{1}", extension, base64ImageRepresentation);
            }
            return base64Image;
        }
        private string GetPathUploadURL()
        {
            string path = "";
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "IMAGE_PATH";
            m.CODE = "IMAGE_UPLOAD_URL";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                path = data.VALUE;
            }

            return path;
        }


        public bool IsActiveUser(string _empCode)
        {
            var datas = (new DAO.WFD01210DAO()).GetEmployeeByEmpCodeDAO(new Models.WFD01210.WFD01210SearchModel() { EMP_CODE = _empCode });
            if (datas == null)
                return false;

            return (datas.ACTIVEFLAG == "Y");
        }

        // Select tab depend on priority from system master by Pawares M. 20180718
        public BaseJsonResult GetTabPriority()
        {
            try
            {
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = "SYSTEM_CONFIG";
                m.SUB_CATEGORY = "TAB_PRIORITY";
                m.CODE = null;
                var result = system_dao.GetSystemData(m)
                                        .Where(x => x.ACTIVE_FLAG == "Y")
                                            .OrderBy(x => x.VALUE);

                return Result(result);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }


        public List<WFD01110BaseModel> GetCompanyList(string EMP_CODE)
        {
            Begin();

            try
            {
                return dao.GetCompanyList(EMP_CODE);

            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<WFD01110BaseModel>();
            }
        }
    }
}