﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.WFD01010;
using System.Web.Mvc;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01010BO : BO
    {
        WDF01010DAO dao;
        SystemConfigurationDAO daoSystem;

        public WFD01010BO()
        {
            dao = new WDF01010DAO();
            daoSystem = new SystemConfigurationDAO();
        }

        private bool Validate(WFD01010DefaultScreenModel data)
        {
            bool correct = true;
            bool checkDate = true;

            int APP_ID = 0;

            if (!string.IsNullOrEmpty(data.APP_ID)) {
                if (!int.TryParse(data.APP_ID, out APP_ID))
                {
                    AddMessage("MSTD1002AERR", "App Id");
                    correct = false;
                }
                //else {
                //    AddMessage("MSTD0031AERR", "App Id");
                //}
              
            }          
            if (!string.IsNullOrEmpty(data.DATE_START) && !ValidFormat(data.DATE_START))
            {
                AddMessage("MSTD0047AERR", "Date From", "dd.MM.yyyy");
                correct = false;
                checkDate = false;
            }
            if (!string.IsNullOrEmpty(data.DATE_TO) && !ValidFormat(data.DATE_TO))
            {
                AddMessage("MSTD0047AERR", "Date To", "dd.MM.yyyy");
                correct = false;
                checkDate = false;
            }
            if (!string.IsNullOrEmpty(data.DATE_START) && !string.IsNullOrEmpty(data.DATE_TO) && checkDate)
            {
                if (!ValidateDate((DateTime)Date(data.DATE_START), (DateTime)Date(data.DATE_TO)))
                {
                    AddMessage("MSTD0022AERR", "Date To", "Date From");
                    correct = false;
                }
            }

            return correct;
        }

        public BaseJsonResult GetFunctionBO()
        {
            Begin();
            try
            {
                var datas = dao.GetFunctionDAO();
                return Result(datas);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return null;
            }
        }
        public BaseJsonResult GetModuleBO()
        {
            Begin();
            try
            {
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = "COMMON";
                m.SUB_CATEGORY = "MODULE";
                m.CODE = "FD0";
                var result = daoSystem.GetSystemData(m)
                                        .Where(x => x.ACTIVE_FLAG == "Y")
                                            .OrderBy(x => x.VALUE);

                return Result(result);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GetStatusLoggingBO()
        {
            Begin();
            try
            {
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = "COMMON";
                m.SUB_CATEGORY = "STATUS_LOGGING";
                m.CODE = null;
                var result = daoSystem.GetSystemData(m)
                                        .Where(x =>x.ACTIVE_FLAG == "Y")
                                            .OrderBy(x => x.VALUE);
               
                return Result(result);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GetLevelLoggingBO()
        {
            Begin();
            try
            {
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = "COMMON";
                m.SUB_CATEGORY = "LEVEL_LOGGING";
                m.CODE = null;
                var result = daoSystem.GetSystemData(m)
                                        .Where(x => x.ACTIVE_FLAG == "Y")
                                            .OrderBy(x => x.VALUE);

                return Result(result);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public SearchJsonResult GetSearchLoggin(WFD01010DefaultScreenModel data, PaginationModel page)
        {
            Begin();
            try
            {
                var result = new List<WFD01010ResultSearchModel>();
                if (Validate(data))
                {
                    result = dao.GetSearchLogginDAO(data, ref page);
                    if (result.Count > 0)
                    {
                        return Result(result, page);
                    }
                    else
                    {
                        AddMessage("MSTD0059AERR");
                        return Result(null, page);
                    }
                }
                else
                {
                    return Result(null, page);
                }

                return Result(result, page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }
        public SearchJsonResult GetDetailLoggin(WFD01010DefaultScreenModel data, PaginationModel page)
        {
            Begin();
            try
            {             
                var result = dao.GetDetailLogginDAO(data, ref page);
                if (result.Count > 0)
                {
                    return Result(result, page);
                }
                else
                {
                    AddMessage("MSTD0059AERR");
                    return Result(null, page);
                }
         
                return Result(result, page);
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }
    }
}