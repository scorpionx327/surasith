﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02120;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02120BO : BO
    {
        private WFD02120DAO dao;
        private string _format = "dd.MM.yyyy";
        CultureInfo cultureInfo = new CultureInfo("en");

        public WFD02120BO()
        {
            dao = new WFD02120DAO();
        }

        public SearchJsonResult GetFixedAssetAuto(WFD02120SearhConditionModel ds, PaginationModel page)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(ds) || !isValidate(ds)) return Result(null, page);
                var datas = dao.GetFixedAssetAutoList(ds, ref page);
                if (datas == null || datas.Count <= 0)
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }
        public SearchJsonResult GetFixedAsset(WFD02120SearhConditionModel ds, PaginationModel page)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(ds) || !isValidate(ds)) return Result(null, page);
                var datas = dao.GetFixedAssetList(ds, ref page);
                if (datas == null || datas.Count <= 0)
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        private bool isValidate(WFD02120SearhConditionModel Model)
        {
            bool isVali = true;
            bool check_date = true;

            if (!string.IsNullOrEmpty(Model.CapitalizeDateFrom))
            {
                if (!ValidDateFormat(Model.CapitalizeDateFrom))
                {
                    AddMessage("MSTD0047AERR", "Date in service from", "dd.MM.yyyy");
                    isVali = false;
                    check_date = false;
                }

            }
            if (!string.IsNullOrEmpty(Model.CapitalizeDateTo))
            {
                if (!ValidDateFormat(Model.CapitalizeDateTo))
                {
                    AddMessage("MSTD0047AERR", "Date in service to", "dd.MM.yyyy");
                    isVali = false;
                    check_date = false;
                }

            }

            if (!string.IsNullOrEmpty(Model.CapitalizeDateFrom) && !string.IsNullOrEmpty(Model.CapitalizeDateTo))
            {
                if (check_date)
                {
                    if (!ValidateDate((DateTime)ValidDateValue(Model.CapitalizeDateFrom), (DateTime)ValidDateValue(Model.CapitalizeDateTo)))
                    {
                        AddMessage("MSTD0022AERR", "Date in service to", "Date in service from");
                        isVali = false;
                    }

                }

            }

            return isVali;
        }
        public bool ValidDateFormat(string _text)
        {

            DateTime dateValue;
            return DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue);

        }
        public DateTime? ValidDateValue(string _text)
        {

            DateTime dateValue;
            if (DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue))
            {
                return DateTime.ParseExact(_text, _format, cultureInfo);
            }
            else
            {
                return null;
            }

        }

        public BaseJsonResult ValidationAddPrintTag(List<WFD02120FixedAssetResultModel> data, bool IsFAAdmin)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            if (!IsFAAdmin)
            {
                foreach (WFD02120FixedAssetResultModel _tdata in data)  // Loop add print tag
                {
                    if (_tdata.PRINT_STATUS.Equals("Y"))
                    {
                        AddMessage(Constants.MESSAGE.MFAS1104AERR);
                        return Result(false);
                    }
                }
            }

            bool isValid = true;

            foreach (WFD02120FixedAssetResultModel _tdata in data)  // Loop add print tag
            {
                if (string.IsNullOrEmpty(_tdata.PLATE_TYPE) || (_tdata.PLATE_TYPE == "S" && string.IsNullOrEmpty(_tdata.BARCODE_SIZE)))
                {
                    AddMessage("MFAS3603AERR", _tdata.ASSET_NO, _tdata.ASSET_SUB);
                    isValid = false;
                }
            }

            return Result(isValid);
        }

        public BaseJsonResult AddPrintTag(List<WFD02120FixedAssetResultModel> data, bool IsFAAdmin)
        {
            Begin();
            MessageBO mBo = new MessageBO();

            //if (!IsFAAdmin)
            //{
            //    foreach (WFD02120FixedAssetResultModel _tdata in data)  // Loop add print tag
            //    {
            //        if (_tdata.PRINT_STATUS.Equals("Y"))
            //        {
            //            AddMessage(Constants.MESSAGE.MFAS1104AERR);
            //            return Result(true);
            //        }
            //    }
            //}

            //bool isValid = true;

            //foreach (WFD02120FixedAssetResultModel _tdata in data)  // Loop add print tag
            //{
            //    if (string.IsNullOrEmpty(_tdata.PLATE_TYPE) || (_tdata.PLATE_TYPE == "S" && string.IsNullOrEmpty(_tdata.BARCODE_SIZE)))
            //    {
            //        AddMessage("MFAS3603AERR", _tdata.ASSET_NO, _tdata.ASSET_SUB);
            //        isValid = false;
            //    }
            //}

            //if (!isValid)
            //{
            //    return Result(true);
            //}

            try
            {
                bool hasSticker = false;

                foreach (WFD02120FixedAssetResultModel _tdata in data)
                {
                    if (_tdata.PLATE_TYPE == "S")
                    {
                        hasSticker = true;
                    }
                    else
                    {
                        _tdata.PRINT_LOCATION = null;
                    }
                }

                dao.ExecAddPrintTag(data);

                if (hasSticker)
                {
                    AddMessage(Constants.MESSAGE.MFAS1102BINF);
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MFAS1102CINF);
                }

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }

        public BaseJsonResult RegisterSelectedRowToTemp(List<WFD02120FixedAssetResultModel> data, string userId)
        {
            Begin();
            try
            {
                int resultAplId = dao.ExecAddPrintReportRow(data, userId);
                AddMessage(Constants.MESSAGE.MSTD0101AINF);
                return Result(resultAplId);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }


    }

}
