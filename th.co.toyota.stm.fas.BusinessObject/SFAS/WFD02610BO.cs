﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02610;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02610BO : BO
    {
        private WFD02610DAO dao;
        public WFD02610BO()
        {
            dao = new WFD02610DAO();
        }

        /// <summary>
        /// Get default search condition data
        /// </summary>
        /// <returns>WFD02610DefaultScreenModel</returns>
        public BaseJsonResult GetDefaultScreenData(WFD02610DefaultScreenModel con)
        {
            Begin();
            try
            {
                var data = dao.GetDefaultScreenData(con);
                return Result(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        /// <summary>
        /// Get finish plan modal data
        /// </summary>
        /// <param name="data">WFD02610DefaultScreenModel</param>
        /// <returns>WFD02610FinishPlanModel</returns>
        public BaseJsonResult GetFinishPlanData(List<WFD02610SearchModel> ds)
        {
            Begin();
            try
            {
                if (!ValidateGetFinishPlanDataSelectItem(ds)) return Result(null);

                var data = ds[0];
                if (!ValidateRequireFields(data)) return Result(null);

                var resData = dao.GetFinishPlanData(data);

                if (!ValidateFinishPlanResultStatus(resData, data)) return Result(null);

                return Result(new { model = resData, data = data });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        private bool ValidateGetFinishPlanDataSelectItem(List<WFD02610SearchModel> ds)
        {
            if (ds == null || ds.Count <= 0)
            {
                AddMessage(Constants.MESSAGE.MCOM0004AERR, "Finish Plan");
                return false;
            }
            if (ds.Count > 1)
            {
                AddMessage(Constants.MESSAGE.MCOM0004AERR, "Finish Plan");
                return false;
            }
            return true;
        }
        private bool ValidateFinishPlanResultStatus(WFD02610FinishPlanModel data, WFD02610SearchModel sData)
        {

            if (data.PLAN_STATUS == WFD02610PlanStatus.D.ToString())
            {
                AddMessage(Constants.MESSAGE.MFAS1204AERR, sData.COMPANY, sData.YEAR, sData.ROUND, sData.ASSET_LOCATION_NAME);
                return false;
            }
            if (data.PLAN_STATUS == WFD02610PlanStatus.F.ToString())
            {
                AddMessage(Constants.MESSAGE.MFAS1201AERR, sData.COMPANY, sData.YEAR, sData.ROUND, sData.ASSET_LOCATION_NAME);
                return false;
            }

            if (!dao.IsApprovedAllCostCenter(sData.COMPANY, sData.YEAR, sData.ROUND, sData.ASSET_LOCATION))
            {
                if (sData.ASSET_LOCATION.Equals("O"))
                {
                    AddMessage(Constants.MESSAGE.MFAS1206AERR, "supplier");
                }
                else if (sData.ASSET_LOCATION.Equals("I"))
                {
                    AddMessage(Constants.MESSAGE.MFAS1206AERR, "cost center");
                }
                return false;
            }

            return true;
        }

        /// <summary>
        /// Search plan data
        /// </summary>
        /// <param name="data">WFD02610DefaultScreenModel</param>
        /// <param name="page">PaginationModel : Ref parameter</param>
        /// <returns>List<WFD02610SearchModel></returns>
        public SearchJsonResult Search(string UserLogin, string IsFAAdmin, WFD02610DefaultScreenModel data, PaginationModel page)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(data)) return Result(null, page);
             
                var datas = dao.Search(UserLogin, IsFAAdmin,data, ref page);

                if (datas == null || datas.Count <= 0)
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);

                return Result(datas, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        /// <summary>
        /// Finish plan process 
        /// </summary>
        /// <param name="data">WFD02610FinishPlanConditionModel</param>
        /// <returns>BaseJsonResult</returns>
        public BaseJsonResult FinishPlan(WFD02610FinishPlanConditionModel data)
        {
            Begin();
            MessageBO mBo = new MessageBO();
            try
            {
                if (!validateFinishPlan(data)) return Result(null);

                var resData = dao.GetFinishPlanData(new WFD02610SearchModel() { COMPANY= data.COMPANY, YEAR = data.YEAR, ROUND = data.ROUND, ASSET_LOCATION = data.ASSET_LOCATION });
                if (!ValidateFinishPlanResultStatus(resData, new WFD02610SearchModel() { COMPANY = data.COMPANY, YEAR = data.YEAR, ROUND = data.ROUND, ASSET_LOCATION = data.ASSET_LOCATION })) return Result(null);

                dao.FinishPlan(data);

                AddMessage(Constants.MESSAGE.MFAS1203AINF);

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }

        /// <summary>
        /// Validate finish plan process
        /// </summary>
        /// <param name="data">WFD02610FinishPlanConditionModel</param>
        /// <param name="res">BaseJsonResult</param>
        /// <returns>BaseJsonResult</returns>
        private bool validateFinishPlan(WFD02610FinishPlanConditionModel data)
        {
            if (data.TOTAL_NOT_CHECK_ASSETS > 0)
                return ValidateRequireFields(data, new RV("REMARK", "Remark"));
            return true;
        }

        public BaseJsonResult ValidateExportToOracle(List<WFD02610SearchModel> ds, string processName, string batchId)
        {
            Begin();
            try
            {
                if (!(ds != null && ds.Count > 0))
                {
                    AddMessage(Constants.MESSAGE.MCOM0004AERR, processName);
                    return Result(null);
                }
                else
                {
                    //string keyCodes = string.Join(",", ds.Select(m => m.STOCK_TAKE_KEY).ToArray());
                    //return Result(keyCodes);

                    dao.beginTransaction();

                    decimal aplId = dao.GetNewAPL_IDForExportToTemp();
                 
                    foreach (var d in ds)
                    {
                        dao.AddStockExportData(new Models.BaseModel.TB_T_STOCK_EXPORT()
                        {
                            ASSET_LOCATION = d.ASSET_LOCATION, BATCH_ID = batchId, ROUND = d.ROUND, YEAR = d.YEAR, COMPANY = d.COMPANY
                        },aplId);
                    }

                    dao.commitTransaction();
                    return Result(aplId);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }



    }
}