﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.COMSCC;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class COMSCCBO : BO
    {
        private COMSCCDAO dao;
        public COMSCCBO()
        {
            dao = new COMSCCDAO();
        }

        public COMSCCModel getDefaultCostCode(string EMP_CODE)
        {
            return dao.getDefaultCostCode(EMP_CODE);
        }
        public COMSCCModel getDetailCostCode(string COST_CODE)
        {
            return dao.getDetailCostCode(COST_CODE);
        }
        /// <summary>
        /// Get default search condition data
        /// </summary>
        /// <returns>WFD02610DefaultScreenModel</returns>
        public SearchJsonResult Search(COMSCCDefaultScreenModel data, PaginationModel page)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(data)) return Result(null, page);
                var datas = new List<COMSCCModel>();
                if (!data.IsSearchRespCostCode)
                {
                    datas = dao.SearchFW(data, ref page);
                }
                else
                {
                    datas = dao.SearchFS(data, ref page);
                }
                if (datas.Count > 0)
                {
                    datas.ForEach(delegate (COMSCCModel item)
                    {
                        item.FS_CODE = (null == item.FS_CODE) ? "" : item.FS_CODE;
                        item.FW_CODE = (null == item.FW_CODE) ? "" : item.FW_CODE;
                        item.FS_NAME = (null == item.FS_NAME) ? "" : item.FS_NAME;
                        item.FW_NAME = (null == item.FW_NAME) ? "" : item.FW_NAME;
                        item.EMP_CODE = (data.IsSearchRespCostCode ? item.FS_CODE : item.FW_CODE).Replace(",","<BR>");
                        item.EMP_NAME = (data.IsSearchRespCostCode ? item.FS_NAME : item.FW_NAME).Replace(",", "<BR>");
                    });
                    
                    return Result(datas, page);
                }
                else
                {
                    AddMessage("MSTD0059AERR");
                      return Result(null, page, "#AlertMessageCOMSCC");
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
        }
    }
    
}
