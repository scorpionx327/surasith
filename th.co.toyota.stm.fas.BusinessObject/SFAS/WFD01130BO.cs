﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.WFD01130;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.Common;
using System.Text.RegularExpressions;
using System.Globalization;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01130BO : BO
    {
        private WFD01130DAO dao;
        private SystemConfigurationDAO system_dao;
        private string _format = "dd.MM.yyyy";
        CultureInfo cultureInfo = new CultureInfo("en");
        public WFD01130BO()
        {
            dao = new WFD01130DAO();
            system_dao = new SystemConfigurationDAO();
        }
        public WFD01130SearchModel GetSystemMaster(WFD01130SearchModel Model)
        {
            Begin();
            try
            {
                SystemConfigurationModels status = new SystemConfigurationModels();
                status.CATEGORY = "SYSTEM_CONFIG";
                status.SUB_CATEGORY = "DOC_STATUS";
                status.CODE = null;
                Model.ListStatus = new List<SelectListItem>();
                Model.ListStatus.Add(new SelectListItem { Text = " ", Value = "" });
                Model.ListStatus.AddRange(system_dao.GetSystemData(status).Where(x => x.ACTIVE_FLAG == "Y" && Int16.Parse(x.CODE) > 10).OrderBy(x => x.CODE).Select(x => new SelectListItem
                {
                    Text = x.VALUE,
                    Value = x.CODE
                }));

                SystemConfigurationModels TransactionType = new SystemConfigurationModels();
                status.CATEGORY = "SYSTEM_CONFIG";
                status.SUB_CATEGORY = "REQUEST_TYPE";
                status.CODE = null;
                Model.ListTransactionType = new List<SelectListItem>();
                Model.ListTransactionType.Add(new SelectListItem { Text = " ", Value = "" });
                Model.ListTransactionType.AddRange(system_dao.GetSystemData(status).Where(x => x.ACTIVE_FLAG == "Y").Select(x => new SelectListItem
                {
                    Text = x.VALUE,
                    Value = x.CODE
                }));

                return Model;
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return null;
            }

        }
        public BaseJsonResult GetAssetName(string ASSET_NO, string COMPANY, string ASSET_SUB)
        {
            Begin();
            try
            {
                var datas = dao.GetAssetName(ASSET_NO,  COMPANY,  ASSET_SUB);
                return Result(datas);

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return null;
            }

        }
        public SearchJsonResult SearchResult(WFD01130SearchModel Model, PaginationModel page)
        {
            Begin();
            try
            {

                if (isValidate(Model))
                {
                    var datas = dao.GetRequestHistory(Model, ref page);
                    if (datas.Count > 0)
                    {
                        return Result(datas, page);
                    }
                    else
                    {
                        AddMessage("MSTD0059AERR");
                        return Result(null, page);
                    }
                }
                else
                {
                    return Result(null, page);

                }

            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, page);
            }
            //return dao.GetRequestHistory(Model, ref page);
        }
        private bool isValidate(WFD01130SearchModel Model)
        {
            bool isVali = true;
            bool check_date = true;
            if (string.IsNullOrWhiteSpace(Model.ASSET_NO))
            {
                AddMessage("MSTD0031AERR", "Asset No");
                isVali = false;
            }
            if (!string.IsNullOrWhiteSpace(Model.ASSET_NO))
            {
                Regex pattern = new Regex(@"^[A-Za-z0-9\-]{0,12}$");
                if (!pattern.IsMatch(Model.ASSET_NO))
                {
                    AddMessage("MSTD0043AERR", "Asset No");
                    isVali = false;
                }
            }
            if (string.IsNullOrWhiteSpace(Model.ASSET_SUB))
            {
                AddMessage("MSTD0031AERR", "Asset Sub No");
                isVali = false;
            }
            if (!string.IsNullOrWhiteSpace(Model.ASSET_SUB))
            {
                Regex pattern = new Regex(@"^[A-Za-z0-9\-]{0,4}$");
                if (!pattern.IsMatch(Model.ASSET_SUB))
                {
                    AddMessage("MSTD0043AERR", "Asset Sub No");
                    isVali = false;
                }
            }
            if (!string.IsNullOrWhiteSpace(Model.DOCUMENT_NO))
            {
                Regex pattern = new Regex(@"^[A-Za-z0-9\/]{0,10}$");
                if (!pattern.IsMatch(Model.DOCUMENT_NO))
                {
                    AddMessage("MSTD0043AERR", "DOCUMENT NO");
                    isVali = false;
                }
            }
            if (!string.IsNullOrEmpty(Model.DATE_SERVICE_FROM))
            {
                if (!ValidFormat(Model.DATE_SERVICE_FROM))
                {
                    AddMessage("MSTD0047AERR", "Transfer Date From", "dd.MM.yyyy");
                    isVali = false;
                    check_date = false;
                }

            }
            if (!string.IsNullOrEmpty(Model.DATE_SERVICE_TO))
            {
                if (!ValidFormat(Model.DATE_SERVICE_TO))
                {
                    AddMessage("MSTD0047AERR", "Transfer Date To", "dd.MM.yyyy");
                    isVali = false;
                    check_date = false;
                }

            }

            if (!string.IsNullOrEmpty(Model.DATE_SERVICE_FROM) && !string.IsNullOrEmpty(Model.DATE_SERVICE_TO))
            {
                if (check_date)
                {
                    if (!ValidateDate((DateTime)Value(Model.DATE_SERVICE_FROM), (DateTime)Value(Model.DATE_SERVICE_TO)))
                    {
                        AddMessage("MSTD0022AERR", "Transfer Date To", "Transfer Date From");
                        isVali = false;
                    }

                }

            }

            return isVali;
        }
        public bool ValidFormat(string _text)
        {

            DateTime dateValue;
            return DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue);

        }
        public DateTime? Value(string _text)
        {

            DateTime dateValue;
            if (DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue))
            {
                return DateTime.ParseExact(_text, _format, cultureInfo);
            }
            else
            {
                return null;
            }

        }
    }
}
