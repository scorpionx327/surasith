﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models.WFD01120;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD01120BO : BO
    {
        WFD01120DAO dao;
        SystemConfigurationDAO system_dao;
        //private string _format = "dd.MM.yyyy";
        //CultureInfo cultureInfo = new CultureInfo("en");
        public WFD01120BO()
        {
            dao = new WFD01120DAO();
            system_dao = new SystemConfigurationDAO();
        }

        //public WFD01120ReportTypeModel GetReportType()
        //{
        //    try
        //    {
        //        var reportType = dao.GetReportType();

        //        WFD01120ReportTypeModel Model = new WFD01120ReportTypeModel();

        //      //  Model.ListReportType.Add(new SelectListItem { Text = " ", Value = "", Selected = true });
        //        Model.ListReportType.AddRange(reportType.Select(x => new SelectListItem { Text = x.VALUE,  Value = x.CODE }));          

        //        return Model;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<SystemModel> GetReportType()
        {
            try
            {
                return dao.GetReportType();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SystemModel> GetReportNameForCombo()
        {
            try
            {
                return dao.GetReportNameForCombo();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public WFD01120ReportTypeModel GetReportName(string ReportType)
        {
            try
            {
                ReportType = "RPT_TYPE" + ReportType;
                var reportType = dao.GetReportName(ReportType);

                WFD01120ReportTypeModel Model = new WFD01120ReportTypeModel();

                // Model.ListReportType.Add(new SelectListItem { Text = "-- SELECT ITEM --", Value = "", Selected = true });
                Model.ListReportType.AddRange(reportType.Select(x => new SelectListItem { Text = x.VALUE, Value = x.CODE }));

                return Model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public WFD01120ReportTypeModel GetCostCenter(string data)
        {
            try
            {
                var CostCenterModels = dao.LoadCostCenterByEmployeeCode(data);

                WFD01120ReportTypeModel Model = new WFD01120ReportTypeModel();

                //Model.ListCostCenter.Add(new SelectListItem { Text = "-- SELECT ITEM --", Value = "", Selected = true });
                Model.ListCostCenter.AddRange(CostCenterModels.Select(x => new SelectListItem { Text = x.COST_NAME, Value = x.COST_CODE }));

                return Model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool IsValidate(WFD01120ChangeRequestHeader Model)
        {
            return ValidateSearchChangeRequestDetail(Model);
        }

        private bool PrintValidate(WFD01120ChangeRequestHeader Model)
        {
            bool isCorrect = true;

            if (Model.REPORT_TYPE.Equals(1))
            {
                return ValidateSummaryReport(Model);
            }
            if (Model.REPORT_TYPE.Equals(2))
            {
                return ValidateChangeRequestDetailReport(Model);
            }
            if (Model.REPORT_TYPE.Equals(3))
            {
                return ValidateFixedAssetDetailReport(Model);
            }

            return isCorrect;
        }

        private bool ValidateSummaryReport(WFD01120ChangeRequestHeader Model)
        {
            bool correct = true;
            bool checkDate = true;
            if (string.IsNullOrWhiteSpace(Model.REPORT_NAME))
            {
                AddMessage("MSTD0031AERR", "Report Name");
                correct = false;
            }
            if (string.IsNullOrEmpty(Model.PERIOD_FROM))
            {
                AddMessage("MSTD0031AERR", "Period Date From");
                correct = false;
            }
            else  if (!ValidFormat(Model.PERIOD_FROM))
            {
                AddMessage("MSTD0047AERR", "Period Date From", "dd.MM.yyyy");
                correct = false;
                checkDate = false;
            }

            if (string.IsNullOrEmpty(Model.PERIOD_TO))
            {
                AddMessage("MSTD0031AERR", "Period Date To");
                correct = false;
            }
            else if (!ValidFormat(Model.PERIOD_TO))
            {
                AddMessage("MSTD0047AERR", "Period Date To", "dd.MM.yyyy");
                correct = false;
                checkDate = false;
            }

            if (!string.IsNullOrEmpty(Model.PERIOD_FROM) && !string.IsNullOrEmpty(Model.PERIOD_TO) && checkDate)
            {
                if (!ValidateDate((DateTime)Date(Model.PERIOD_FROM), (DateTime)Date(Model.PERIOD_TO)))
                {
                    AddMessage("MSTD0022AERR", "Period Date To", "Period Date From");
                    correct = false;
                }
            }

            return correct;
        }

        private bool ValidateChangeRequestDetailReport(WFD01120ChangeRequestHeader Model)
        {
            bool correct = true;

            if (Model.SELECTED == null || Model.SELECTED.Count == 0)
            {

                AddMessage("MCOM0001AERR", "Generating Report");
                correct = false;
            }

            return correct;
        }

        private bool ValidateFixedAssetDetailReport(WFD01120ChangeRequestHeader Model)
        {
            bool correct = true;

            if (string.IsNullOrWhiteSpace(Model.REPORT_NAME))
            {
                AddMessage("MSTD0031AERR", "Report Name");
                correct = false;
            }

            return correct;
        }

        private bool ValidateSearchChangeRequestDetail(WFD01120ChangeRequestHeader Model)
        {
            bool correct = true;
            bool checkDate = true;
            // Remove By Surasith : Incident No 0025
            //if (string.IsNullOrWhiteSpace(Model.REPORT_NAME))
            //{
            //    AddMessage("MSTD0031AERR", "Report Name");
            //    correct = false;
            //}

            //Remove % support user fileter by %
            //if (!string.IsNullOrWhiteSpace(Model.DOC_NO))
            //{
            //    Regex pattern = new Regex(@"^[A-Za-z0-9]{0,1}[A-Za-z0-9\/]{0,9}$"); 
            //    if (!pattern.IsMatch(Model.DOC_NO))
            //    {
            //        AddMessage("MSTD0043AERR", "Report NO");
            //        correct = false;
            //    }
            //}
            if (string.IsNullOrEmpty(Model.REPORT_NAME))
            {
                AddMessage("MSTD0031AERR", "Report Name");
                correct = false;
            }
            if (!ValidFormat(Model.PERIOD_FROM) && !string.IsNullOrEmpty(Model.PERIOD_FROM))
            {
                AddMessage("MSTD0047AERR", "Period Date From", "dd.MM.yyyy");
                correct = false;
                checkDate = false;
            }

            if (!ValidFormat(Model.PERIOD_TO) && !string.IsNullOrEmpty(Model.PERIOD_TO))
            {
                AddMessage("MSTD0047AERR", "Period Date To", "dd.MM.yyyy");
                correct = false;
                checkDate = false;
            }

            if (!string.IsNullOrEmpty(Model.PERIOD_FROM) && !string.IsNullOrEmpty(Model.PERIOD_TO) && checkDate)
            {
                if (!ValidateDate((DateTime)Date(Model.PERIOD_FROM), (DateTime)Date(Model.PERIOD_TO)))
                {
                    AddMessage("MSTD0022AERR", "Period Date To", "Period Date From");
                    correct = false;
                }
            }


            return correct;
        }

        public SearchJsonResult GetChangeRequestDetail(WFD01120ChangeRequestHeader data, PaginationModel Page)
        {
            Begin();
            try
            {
                if (IsValidate(data))
                {
                    if (data.REPORT_NAME == "1")
                        data.REQUEST_TYPE = "T";

                    if (data.REPORT_NAME == "2")
                        data.REQUEST_TYPE = "D";

                    var datas = dao.GetChangeRequestDetail( data.COMPANY,
                                                            data.REQUEST_TYPE,
                                                            data.DOC_NO,
                                                            Date(data.PERIOD_FROM),
                                                             Date(data.PERIOD_TO),
                                                            data.EMP_CODE,
                                                            ref Page);

                    if (datas.Count > 0)
                    {
                        return Result(datas, Page);
                    }
                    else
                    {
                        AddMessage("MSTD0059AERR");
                        return Result(null, Page);
                    }
                }
                else
                {
                    return Result(null, Page);
                }
            }
            catch (Exception ex)
            {
                AddMessage("Error", ex.Message, MessageStatus.ERROR);
                return Result(null, Page);
            }
        }

        public BaseJsonResult CheckPrintValidation(WFD01120ChangeRequestHeader data)
        {
            Begin();
            try
            {
                var _b = PrintValidate(data);
                if (_b)
                {
                    return Result(true);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }

            return Result(null);
        }
        public bool ValidFormat(string _text)
        {

            DateTime dateValue;
            return DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue);

        }
        public DateTime? Date(string _text)
        {

            DateTime dateValue;
            if (DateTime.TryParseExact(_text, _format, cultureInfo, DateTimeStyles.None, out dateValue))
            {
                return DateTime.ParseExact(_text, _format, cultureInfo);
            }
            else
            {
                return null;
            }

        }
        public BaseJsonResult GetStockTakingReportBO(string EMP_CODE,string IS_FAADMIN)
        {
            Begin();

            try
            {
                return Result(new { data = dao.GetStockTakingReportDAO(EMP_CODE, IS_FAADMIN) });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
    }
}
