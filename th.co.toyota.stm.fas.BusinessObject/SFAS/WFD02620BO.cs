﻿using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02620;
using System.Linq;
using th.co.toyota.stm.fas.Models;
using System.Web;
using System.IO;
using th.co.toyota.stm.fas.Models.SFAS.WFD02620;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02620BO : BO
    {
        private WFD02620DAO dao;

        public WFD02620BO()
        {
            dao = new WFD02620DAO();
        }

        
        public BaseJsonResult GetParamWithoutAssetLocation(string COMPANY ,string YEAR, string ROUND)
        {
            Begin();
            try
            {
                var data = dao.GetParamWithoutAssetLocation(COMPANY,YEAR, ROUND);
                return Result(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetStockTakePerSV(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            Begin(); //Should remove becuase this function is called from update (update include success message)
            try
            {
                var sv = dao.GetStockTakeDPerSVDatas(COMPANY,YEAR, ROUND, ASSET_LOCATION);
                AddMessage("MSTD0101AINF");
                return Result(sv);
            }
            catch(Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetStockTakeModel(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            Begin();
            try
            {

                List<WFD02620SummaryModel> summaryMinorCate = new List<WFD02620SummaryModel>();

                var stockTakeH = dao.GetSettingSectionData(COMPANY,YEAR, ROUND, ASSET_LOCATION);

                var stockTakeD = dao.GetStockTakeDDatas(COMPANY,YEAR, ROUND, ASSET_LOCATION);

                var stockTakeDPerSV = dao.GetStockTakeDPerSVDatas(COMPANY,YEAR, ROUND, ASSET_LOCATION);

                var stockTakeHolidays = dao.GetStockTakeHolidayDatas(COMPANY,stockTakeH.STOCK_TAKE_KEY);

                var masterSetting = dao.GetMasterSettingData();


                if (stockTakeD != null && stockTakeD.Count > 0)
                {
                    foreach (var d in stockTakeD.OrderBy(m => m.MINOR_CATEGORY).GroupBy(m => new { m.MINOR_CATEGORY }).ToList())
                    {
                        summaryMinorCate.Add(new WFD02620SummaryModel()
                        {
                            MINOR_CATEGORY = d.Key.MINOR_CATEGORY,
                            TOTAL_CNT = stockTakeD.Where(m => m.MINOR_CATEGORY == d.Key.MINOR_CATEGORY).Count(),
                        });
                    }
                }


                return Result(new
                {
                    stockTakeH = stockTakeH,
                    //stockTakeD = stockTakeD,
                    stockTakeDPerSV = stockTakeDPerSV.OrderBy(m=>m.EMP_NAME).ToList(),
                    stockTakeHolidays = stockTakeHolidays,
                    masterSetting = masterSetting,
                    summaryMinorCate = summaryMinorCate.OrderBy(m=>m.MINOR_CATEGORY).ToList()
                });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetDefaultSreenData()
        {
            Begin();
            try
            {
                var data = dao.GetDefaultSreenData();
                return Result(data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetSettingSectionData(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            Begin();
            try
            {
                var settingSectionData = dao.GetSettingSectionData(COMPANY,YEAR, ROUND, ASSET_LOCATION);
                var defaultData = dao.GetDefaultSreenData();
                return Result(new { settingSectionData = settingSectionData, defaultData = defaultData });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);

            }
        }

        public BaseJsonResult GetMasterSettingData()
        {
            Begin();
            try
            {
                return Result(dao.GetMasterSettingData());
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult UploadAssetListFile(HttpPostedFileBase file)
        {
            Begin();

            try
            {


                var batchInfo = new BatchInfoDAO().GetBatchInfoData(Constants.BatchID.BFD0261A);
                if (!Directory.Exists(batchInfo.UPLOAD_PATH)) { Directory.CreateDirectory(batchInfo.UPLOAD_PATH); }

                string saveFilePath = string.Format("{0}\\{1}{2}", batchInfo.UPLOAD_PATH
                    , Constants.BatchID.BFD0261A + DateTime.Now.ToString("ddMMyyyyHHmmss"), Path.GetExtension(file.FileName));

                file.SaveAs(saveFilePath);

                return Result(saveFilePath);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GenBatchParam(WFD02620SettingModel data, string uploadFileName, string UserId, string isUpdate)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(data)) return Result(null);
                if (data.TARGET_DATE_FROM > data.TARGET_DATE_TO) AddMessage("MSTD0022AERR", "Target date from", "Target date to");
                if (isUpdate != "Y")
                    if (!dao.ValidateCreateStockTakePlan(data.COMPANY, data.YEAR, data.ROUND, data.ASSET_LOCATION)) return Result(null);

                if (Path.GetExtension(uploadFileName) != ".xlsx")
                {
                    AddMessage("");
                }

                WFD02620BatchParamModel d = new WFD02620BatchParamModel()
                {
                    ASSET_LOCATION = data.ASSET_LOCATION,
                    BREAK_TIME_MINUTE = data.BREAK_TIME_MINUTE?.ToString(),
                    IS_UPDATE = "N",
                    QTY_OF_HANDHELD = data.QTY_OF_HANDHELD?.ToString(),
                    COMPANY = data.COMPANY,
                    ROUND = data.ROUND,
                    TARGET_DATE_FROM = data.TARGET_DATE_FROM?.ToString("yyyyMMdd", new CultureInfo("en-Us")),
                    TARGET_DATE_TO = data.TARGET_DATE_TO?.ToString("yyyyMMdd", new CultureInfo("en-Us")),
                    UPDATE_BY = UserId,
                    UPLOAD_FILE_NAME = uploadFileName,
                    YEAR = data.YEAR?.ToString()
                };
                return Result(d);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult AddStockTakeRequest(WFD02620SettingModel data)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(data, new RV("ASSET_LOCATION", "Asset Location")//, new RV("SUB_TYPE", "Sub Type")
                    , new RV("DATA_AS_OF", "Data as of"), new RV("ASSET_STATUS", "Asset Status"))) return Result(null);
                if (data.TARGET_DATE_FROM > data.TARGET_DATE_TO) AddMessage("MSTD0022AERR", "Target date from", "Target date to");
                //S Fix : Can Ajust BREAK TIME = 0 by Nuttapon
                //if (data.BREAK_TIME_MINUTE.Value <= 0) AddMessage("MSTD0021AERR", "Break Time", "0");
                //E Fix : Can Ajust BREAK TIME = 0 by Nuttapon


                if (!IsValid) return Result(null);

                var res = dao.AddStockTakeRequest(data);

               
                string company = string.Empty, year = string.Empty, round = string.Empty, assetLocation = string.Empty;

                if (res.Length == 7)
                {
                    //company = res.Substring(0, 10);
                    year = res.Substring(0, 4);
                    round = res.Substring(4, 2);
                    assetLocation = res.Substring(6, 1);
                }
                else
                {
                    return Result(null);
                }

                return Result(new { Company = company, Year = year, Round = round, AssetLocation = assetLocation });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult EditStockTakeRequest(WFD02620SettingModel data)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(data, new RV("SUB_TYPE", "Sub Type"), new RV("DATA_AS_OF", "Data as of"), new RV("ASSET_STATUS", "Asset Status"))) return Result(null);
                if (data.TARGET_DATE_FROM > data.TARGET_DATE_TO) AddMessage("MSTD0022AERR", "Target date from", "Target date to");
                if (data.BREAK_TIME_MINUTE.Value <= 0) AddMessage("MSTD0021AERR", "Break Time", "0");
                if (!IsValid) return Result(null);

                dao.EditStockTakeRequest(data);
                string year = data.YEAR, round = data.ROUND, assetLocation = data.ASSET_LOCATION;

                AddMessage("MSTD0101AINF");

                return Result(new { Year = year, Round = round, AssetLocation = assetLocation });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult ValidateSetWorkingTimeDialog(WFD02620SetWorkingTimeDialogModel data, List<WFD02620HolidayModel>  holidays)
        {
            Begin();
            try
            {
                CultureInfo en = new CultureInfo("en-Us");
                string dtFormat = "dd.MM.yyyy";
                if (!ValidateRequireFields(data)) return Result(null);

                DateTime from = DateTime.ParseExact(data.DateFrom, dtFormat, en) + new TimeSpan(int.Parse(data.TimeStart.Split(':')[0]), int.Parse(data.TimeStart.Split(':')[1]), 0);
                DateTime to = DateTime.ParseExact(data.DateTo, dtFormat, en) + new TimeSpan(int.Parse(data.TimeEnd.Split(':')[0]), int.Parse(data.TimeEnd.Split(':')[1]), 0);
                data.TargetFrom = data.TargetFrom.Value.Date + new TimeSpan(int.Parse(data.TimeStart.Split(':')[0]), int.Parse(data.TimeStart.Split(':')[1]), 0);
                data.TargetTo = data.TargetTo.Value.Date + new TimeSpan(int.Parse(data.TimeEnd.Split(':')[0]), int.Parse(data.TimeEnd.Split(':')[1]), 0);

                if (from >= to) AddMessage("MSTD0021AERR", "Date to", "Date from"); //Update > to >= By Surasith

                if (from < data.TargetFrom || to > data.TargetTo)
                    AddMessage("MSTD0016AERR", "Working date period",
                        data.TargetFrom.Value.ToString("dd MMM yyyy"),
                        data.TargetTo.Value.ToString("dd MMM yyyy"));

                if (from.TimeOfDay < data.TimeStartDay.Value.TimeOfDay || to.TimeOfDay > data.TimeEndDay.Value.TimeOfDay
                        || from.TimeOfDay >= data.TimeEndDay.Value.TimeOfDay || to.TimeOfDay <= data.TimeStartDay.Value.TimeOfDay)
                    AddMessage("MSTD0016AERR", "Working time period",
                        data.TimeStartDay.Value.ToString("HH:mm"),
                        data.TimeEndDay.Value.ToString("HH:mm"));

                if (holidays != null 
                    && (holidays.Where(m => m.HOLIDATE_DATE?.ToString(dtFormat, en) == data.DateFrom).Count() > 0
                        || holidays.Where(m => m.HOLIDATE_DATE?.ToString(dtFormat, en) == data.DateTo).Count() > 0))
                {
                    AddExceptionMessage("Cannot set day in holiday !");
                }

                return Result(IsValid);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }


        #region Calculate intersect usage HHT

        public BaseJsonResult CalculateIntersectUsageHHTMoreThanMaxHHT(List<WFD02620StockTakeDPerSVModel> datas, int MaxHHT)
        {
            Begin();
            try
            {
                if (datas == null) return Result(null);
                return Result(_CalculateIntersectUsageHHTMoreThanMaxHHT(datas, MaxHHT));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }
            return Result(null);
        }
        private List<WFD02620StartEndDateModel> _CalculateIntersectUsageHHTMoreThanMaxHHT(List<WFD02620StockTakeDPerSVModel> datas, int MaxHHT)
        {
            List<WFD02620StockTakeDPerSVModel> ds = new List<WFD02620StockTakeDPerSVModel>();
            foreach (var d in datas)
            {
                if (d.DATE_FROM == null || string.IsNullOrEmpty(d.TIME_START) || d.DATE_TO == null || string.IsNullOrEmpty(d.TIME_END)) continue;
                d.DATE_FROM = d.DATE_FROM.Value.Date + new TimeSpan(int.Parse(d.TIME_START.Split(':')[0]), int.Parse(d.TIME_START.Split(':')[1]), 0);
                d.DATE_TO = d.DATE_TO.Value.Date + new TimeSpan(int.Parse(d.TIME_END.Split(':')[0]), int.Parse(d.TIME_END.Split(':')[1]), 0);
                ds.Add(d);
            }


            var intersectPeriods = _CalInterDateTimePeriod(ds, new List<WFD02620StartEndDateModel>(), MaxHHT);
            var morThanMaxHH = _getIntersectDatePeriodUsageHHTMoreThanMax(datas, intersectPeriods, MaxHHT);

            //foreach (var d in datas)
            //{
            //    if (d.USAGE_HANDHELD > MaxHHT)
            //    {
            //        foreach (var t in morThanMaxHH.OrderBy(m=>m.start))
            //        {
            //            if(t.start )
            //        }

            //        DateTime start, end;
            //        start = d.DATE_FROM.Value;
            //        end = d.DATE_TO.Value;

            //        morThanMaxHH.Add(new WFD02620StartEndDateModel(start, end, d.USAGE_HANDHELD ?? 0, d.USAGE_HANDHELD ?? 0));
            //    }
            //}

            return morThanMaxHH;
        }
        private List<WFD02620StartEndDateModel> _CalInterDateTimePeriod(List<WFD02620StockTakeDPerSVModel> datas, List<WFD02620StartEndDateModel> res, int MaxHHT)
        {
            if (datas.Count <= 0) return res;
            else
            {
                var data = datas.First();
                var nDatas = datas.Where(m => m.EMP_CODE != data.EMP_CODE).OrderBy(m => m.DATE_FROM).ToList();

                if (data.USAGE_HANDHELD > MaxHHT)
                    res.Add(new WFD02620StartEndDateModel(data.DATE_FROM.Value, data.DATE_TO.Value, data.USAGE_HANDHELD ?? 0, 0));

                foreach (var d in nDatas)
                {
                    if (data.DATE_FROM <= d.DATE_FROM && data.DATE_TO >= d.DATE_FROM && data.DATE_TO != d.DATE_FROM)
                    {
                        DateTime start, end;
                        start = d.DATE_FROM.Value;
                        if (data.DATE_TO >= d.DATE_TO) end = d.DATE_TO.Value;
                        else end = data.DATE_TO.Value;
                        res.Add(new WFD02620StartEndDateModel(start, end, data.USAGE_HANDHELD ?? 0, d.USAGE_HANDHELD ?? 0));
                    }
                }

                return _CalInterDateTimePeriod(nDatas, res, MaxHHT);
            }
        }
        private List<WFD02620StartEndDateModel> _getIntersectDatePeriodUsageHHTMoreThanMax(List<WFD02620StockTakeDPerSVModel> datas, List<WFD02620StartEndDateModel> perods, int MaxHHT)
        {
            List<WFD02620StartEndDateModel> res = new List<WFD02620StartEndDateModel>();

            foreach (var p in perods)
            {
                int cnt = 0;
                foreach (var d in datas.Where(m => m.DATE_FROM <= p.start && m.DATE_TO >= p.end))
                {
                    cnt += d.USAGE_HANDHELD ?? 0;
                }
                if (cnt > MaxHHT)
                    res.Add(new WFD02620StartEndDateModel(p.start, p.end, cnt, 0));
            }

            return res;
        }

        private class WFD02620StartEndDateModel
        {
            public int HHT_CNT { get; set; }
            public DateTime start { get; set; }
            public DateTime end { get; set; }
            public WFD02620StartEndDateModel(DateTime s, DateTime e, int c1, int c2)
            {
                start = s;
                end = e;
                HHT_CNT = c1 + c2;
            }
        }

        #endregion

        public BaseJsonResult SaveAssigmentDatas(WFD02620SettingModel HData, List<WFD02620StockTakeDPerSVModel> sv
            , List<WFD02620HolidayModel> holidays, UserPrinciple User)
        {
            Begin();
            try
            {
                if (_CalculateIntersectUsageHHTMoreThanMaxHHT(sv, HData.QTY_OF_HANDHELD.Value).Count > 0)
                {
                    AddMessage(Constants.MESSAGE.MFAS1310AERR);
                    return Result(null);
                }

                if (User.IsAECUser)
                {
                    bool _error = false;
                    //Check All SV
                    foreach (var d in sv)
                    {
                        if(d.USAGE_HANDHELD.HasValue && d.USAGE_HANDHELD == 0 && d.DATE_FROM.HasValue)
                        {
                            _error = true;
                            AddMessage(Constants.MESSAGE.MFAS1311AERR, string.Format("{0} {1} {2}", d.EMP_TITLE, d.EMP_NAME, d.EMP_LASTNAME) );
                        }
                    }
                    if (_error)
                        return Result(null);


                    var adSV = dao.GetStockTakeDPerSVDatas(HData.COMPANY,HData.YEAR, HData.ROUND, HData.ASSET_LOCATION);
                    List<WFD02620StockTakeDPerSVModel> iDatas = new List<WFD02620StockTakeDPerSVModel>();
                    foreach (var d in sv)
                    {
                        if (adSV.Where(m => (_GDFDTN(m.DATE_FROM) != _GDFDTN(d.DATE_FROM) || _GDFDTN(m.DATE_TO) != _GDFDTN(d.DATE_TO)
                                                || ST(m.TIME_START) != ST(d.TIME_START) || ST(m.TIME_END) != ST(d.TIME_END) || m.IS_LOCK != d.IS_LOCK)
                                             && m.EMP_CODE == d.EMP_CODE).Count() > 0)
                        {
                            d.FA_DATE_FROM = d.DATE_FROM;
                            d.FA_DATE_TO = d.DATE_TO;
                            d.UPDATE_BY = User.User.UserId;

                            iDatas.Add(d);
                        }
                    }

                    dao.UpdateStockTakePerSVDatas(iDatas);

                    string holidayStr = string.Empty;
                    if (holidays != null && holidays.Count > 0)
                    {
                        holidayStr = string.Join(",", holidays.Select(m => m.HOLIDATE_DATE.Value.ToString("yyyy-MM-dd", new CultureInfo("en-Us"))));
                    }

                    dao.SaveStockTakeHoliday(HData.COMPANY, HData.STOCK_TAKE_KEY, holidayStr);
                }
                else
                {
                   

                    var data = sv.Where(m => m.EMP_CODE == User.User.UserId).First();

                    if (data.USAGE_HANDHELD.HasValue && data.USAGE_HANDHELD == 0 && data.DATE_FROM.HasValue)
                    {
                        AddMessage(Constants.MESSAGE.MFAS1311AERR, string.Format("{0} {1} {2}", data.EMP_TITLE, data.EMP_NAME, data.EMP_LASTNAME));
                    }

                    dao.UpdateStockTakePerSVData(data);
                }

                AddMessage("MSTD0101AINF");
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
            }
            return Result(false);
        }
        private string ST(string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            else return str.Trim();
        }
        private DateTime? _GDFDTN(DateTime? date)
        {
            if (date == null) return null;
            else return date.Value.Date;
        }

        public BaseJsonResult ResetAssignment(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION, string UPDATE_BY)
        {
            Begin();
            try
            {
                dao.ResetAssignment(UPDATE_BY, COMPANY, YEAR, ROUND, ASSET_LOCATION);

                var sv = dao.GetStockTakeDPerSVDatas(COMPANY,YEAR, ROUND, ASSET_LOCATION);

                AddMessage(Constants.MESSAGE.MFAS1302AINF);
                return Result(sv);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetDiffMinute(WFD02620MasterSettingModel setting, DateTime from, DateTime to)
        {
            Begin();
            try
            {
                JJWorkingTime jtime = new JJWorkingTime(setting.START_HRS, setting.END_HRS, setting.BREAK_TIME.Split('-')[0], setting.BREAK_TIME.Split('-')[1]);

                return Result(jtime.GetDifMinute(from, to));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(0);
            }
        }
        public BaseJsonResult GetResultDateTimeAfterAddMinute(WFD02620MasterSettingModel setting, DateTime date, int minutes)
        {
            Begin();
            try
            {
                JJWorkingTime jtime = new JJWorkingTime(setting.START_HRS, setting.END_HRS, setting.BREAK_TIME.Split('-')[0], setting.BREAK_TIME.Split('-')[1]);

                return Result(jtime.GetResultDateTimeAfterAddMinute(date, minutes));
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(0);
            }
        }

        #region Announce

        public BaseJsonResult Announce(string STOCK_TAKE_KEY, string UserId, string url, string COMPANY)
        {
            Begin();
            try
            {
                var valdiateRes = dao.ValidateAnnounce(STOCK_TAKE_KEY, COMPANY);
                if (valdiateRes != null && valdiateRes.Count > 0)
                {
                    foreach (var v in valdiateRes) AddMessage(Constants.MESSAGE.MFAS1311AERR, v);

                    return Result(false);
                }

                var annEmails = _GenNotificationData(STOCK_TAKE_KEY, "WFD02620_Announce", UserId, url, "Announce", COMPANY);
                new NotificationDAO().InsertDatas(annEmails);

                AddMessage(Constants.MESSAGE.MFAS1304AINF);

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult Completed(string STOCK_TAKE_KEY, string UserId, string url, string COMPANY)
        {
            Begin();
            try
            {
                var valdiateRes = dao.ValidateAnnounce(STOCK_TAKE_KEY, COMPANY);
                if (valdiateRes != null && valdiateRes.Count > 0)
                {
                    foreach (var v in valdiateRes) AddMessage(Constants.MESSAGE.MFAS1311AERR, v);
                    return Result(false);
                }

                valdiateRes = dao.IsOtherPlanInProcess(STOCK_TAKE_KEY, COMPANY);
                if (valdiateRes != null && valdiateRes.Count > 0)
                {
                    foreach (var v in valdiateRes) AddMessage(Constants.MESSAGE.MFAS1307AERR, v);
                    return Result(false);
                }

                dao.CompletedStockTakePlan(STOCK_TAKE_KEY, COMPANY);

                var annEmails = _GenNotificationData(STOCK_TAKE_KEY, "WFD02620_Completed", UserId, url, "Completed", COMPANY);
                new NotificationDAO().InsertDatas(annEmails);

                AddMessage(Constants.MESSAGE.MFAS1306AINF);

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        private List<TB_R_NOTIFICATION> _GenNotificationData(string STOCK_TAKE_KEY, string EmailCode, string UserId, string url, string TypeOfEmail, string COMPANY)
        {
            List<TB_R_NOTIFICATION> datas = new List<TB_R_NOTIFICATION>();
            SystemDAO sysDAO = new SystemDAO();
            string subject = sysDAO.SelectSystemValue(SYSTEM_CATEGORY.SYSTEM_EMAIL, SYSTEM_SUB_CATEGORY.SUBJECT, EmailCode).VALUE;
            //string body = EmailUtil.GenerateBodyEmailTemplate(EmailCode);
            var emailDatas = dao.GetEmailDatas(STOCK_TAKE_KEY, COMPANY);
            List<string> wrnList = new List<string>();

            subject = string.Format(subject, COMPANY, emailDatas[0].YEAR + "/" + emailDatas[0].ROUND, emailDatas[0].ASSET_LOCATION_NAME);

            string mailCCManage = string.Empty;

            foreach (var e in emailDatas)
            {
                if (!string.IsNullOrEmpty(e.EMAIL))
                {

                    ////Fix Offline Notify Email for Created, Completed, Remind
                    //if (TypeOfEmail == "Announce" && e.ASSET_LOCATION == "I")
                    //{
                    //    mailCCManage = dao.GetManagerEmailOfSV(e.EMP_CODE);
                    //}

                    mailCCManage = dao.GetManagerEmailOfSV(e.EMP_CODE, COMPANY);

                    datas.Add(new TB_R_NOTIFICATION()
                    {
                        CREATE_BY = UserId,
                        //MESSAGE = EmailUtil.GenerateBodyEmailTemplate(EmailCode, e.EMP_NAME + " " + e.EMP_LASTNAME, e.YEAR + "/" + e.ROUND, e.ASSET_LOCATION_NAME, url, "Stock take request system"),
                        MESSAGE = EmailUtil.GenerateBodyEmailTemplate(EmailCode, e.EMP_NAME + " " + e.EMP_LASTNAME, e.YEAR + "/" + e.ROUND, e.ASSET_LOCATION_NAME, url, url),
                        SEND_FLAG = "N",
                        TITLE = subject,
                        REF_FUNC = "WFD02620",
                        TO = e.EMAIL,
                        CC= mailCCManage,
                        TYPE = "I",
                        REF_DOC_NO = STOCK_TAKE_KEY
                    });
                }
                else
                {
                    wrnList.Add(string.Format("{0} {1} {2}", e.EMP_TITLE, e.EMP_NAME, e.EMP_LASTNAME));
                }
            }
            if (wrnList.Count > 0)
            {
                AddMessage(Constants.MESSAGE.MCOM0022AWRN, string.Join(",", wrnList));
            }

            return datas;
        }

        public List<WFD02620CostCenterModel> GetCostCenterForCombo()
        {
            try
            {
                return dao.GetCostCenterForCombo();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<WFD02620CostCenterModel> GetResponsibleCostCenterForCombo()
        {
            try
            {
                return dao.GetResponsibleCostCenterForCombo();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}