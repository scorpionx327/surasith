﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02115;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.WFD01180;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Web;

namespace th.co.toyota.stm.fas.BusinessObject
{
    public class WFD02115BO : BO
    {
        private WFD02115DAO dao;
        public WFD02115BO()
        {
            dao = new WFD02115DAO();
        }

        public string GetDocumentStatus(WFD02115SaveModel _data)
        {
            try
            {
                return dao.GetDocumentStatus(_data);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public List<Models.WBSMasterAutoCompleteModels> GetWBSBudgetAutoComplete(string _company, string _flowType, string _EmpCode, string _IsFilterByFiscalYear)
        {
            Begin();
            try
            {
                if(_flowType == "AU") //AUC
                {
                    return dao.GetWBSBudgetAUCAutoComplete(_company, _EmpCode, _IsFilterByFiscalYear);
                }
                
                return dao.GetWBSBudgetRMAAutoComplete(_company, _EmpCode, _IsFilterByFiscalYear) ;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.WBSMasterAutoCompleteModels>();
            }
        }
        public List<Models.WBSMasterAutoCompleteModels> GetWBSProjectAutoComplete(string _company , string _EmpCode, string _FlowType, string _wbsBudget)
        {
            Begin();
            try
            {

                return dao.GetWBSProjectAutoComplete(_company, _EmpCode, _FlowType, _wbsBudget);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.WBSMasterAutoCompleteModels>();
            }
        }

        public JsonResultBaseModel SaveAssetsData(WFD02115SaveModel data)
        {
            Begin();
            try
            {
                dao.SaveAssetsData(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }   
            catch(Exception ex)
            {
                AddExceptionMessage(ex);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
        }
        public JsonResultBaseModel SaveAssetsReclassData(WFD02115SaveModel data)
        {
            Begin();
            try
            {
                dao.SaveAssetsReclassData(data);
                return new JsonResultBaseModel() { Status = MessageStatus.INFO };
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message };
            }
        }
        private void AddListMessage(List<MessageModel> datas)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                switch (datas[i].MESSAGE_TYPE)
                {
                    case "WRN":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.WARNING, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "ERR":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    case "INF":
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.INFO, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                    default:
                        AddMessage(new SystemMessageModel() { ID = datas[i].MESSAGE_CODE, STATUS = MessageStatus.ERROR, VALUE = datas[i].MESSAGE_TEXT });
                        break;
                }
            }
        }
        //private bool ValidateRequireSearch(ref WFD02115Model ds)
        //{
        //    bool correct = true;
        //    if (ds == null)
        //    {
        //        AddMessage(Constants.MESSAGE.MSTD0059AERR);
        //        correct = false;
        //    }
        //    // Remove By Surasith T. : Change Spec user request to remove for friendly 
        //    //if (string.IsNullOrWhiteSpace(ds.COST_CODE))
        //    //{
        //    //    AddMessage(Constants.MESSAGE.MSTD0031AERR,"Cost Center");
        //    //    correct = false;
        //    //}
        //    if (ds.DATE_IN_SERVICE_START != null && ds.DATE_IN_SERVICE_START != "")
        //    {
        //        if (ValidateFormatDate(ds.DATE_IN_SERVICE_START) == "")
        //        {
        //            AddMessage(Constants.MESSAGE.MSTD0047AERR, ds.DATE_IN_SERVICE_START, "dd.MM.yyyy");
        //            correct = false;
        //        }
        //        else
        //        {
        //            ds.DATE_IN_SERVICE_START = ValidateFormatDate(ds.DATE_IN_SERVICE_START);
        //        }

        //    }
        //    if (ds.DATE_IN_SERVICE_TO != null && ds.DATE_IN_SERVICE_TO != "")
        //    {
        //        if (ValidateFormatDate(ds.DATE_IN_SERVICE_TO) == "")
        //        {
        //            AddMessage(Constants.MESSAGE.MSTD0047AERR, ds.DATE_IN_SERVICE_TO, "dd.MM.yyyy");
        //            correct = false;
        //        }
        //        else
        //        {
        //            ds.DATE_IN_SERVICE_TO = ValidateFormatDate(ds.DATE_IN_SERVICE_TO);
        //        }
        //    }

        //    if (correct && ds.DATE_IN_SERVICE_START != null && ds.DATE_IN_SERVICE_TO != null && ds.DATE_IN_SERVICE_START != "" && ds.DATE_IN_SERVICE_TO != "")
        //    {
        //        if (DateTime.Parse(ds.DATE_IN_SERVICE_START) > DateTime.Parse(ds.DATE_IN_SERVICE_TO))
        //        {
        //            AddMessage("MSTD0022AERR", "Date In Service To", "Date In Service From");
        //            correct = false;
        //        }
        //    }

        //    //if(ds.DATE_IN_SERVICE_START.HasValue && ds.DATE_IN_SERVICE_TO.HasValue)
        //    //{
        //    //    if (!ValidateDate(ds.DATE_IN_SERVICE_START.Value, ds.DATE_IN_SERVICE_TO.Value))
        //    //    {
        //    //        AddMessage("MSTD0022AERR", "Date In Service To", "Date In Service From");
        //    //        correct = false;
        //    //    }
        //    //}

        //    return correct;


        //}
        public BaseJsonResult GetFixedAssetList(WFD02115SaveModel _data)
        {
            Begin();
            try
            {
                if (!ValidateRequireFields(_data)) return Result(null, null);
                var _rs = dao.GetAssetsData(_data);
                if (_rs == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        //GetOriginalAssetsData
        public BaseJsonResult GetFixedAssetMaster(WFD02115SaveModel _data)
        {
            Begin();
            try
            {
                var _rs = dao.GetFixedAssetMaster(_data);
                if (_rs == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetWBSData(WFD02115WBSModel _data)
        {
            Begin();
            try
            {
                var _rs = dao.GetWBSData(_data);
                if (_rs == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetFixedAssetMasterForReclass(WFD02115SaveModel _data)
        {
            Begin();
            try
            {
                _data.ASSET_NO = _data.ORIGINAL_ASSET_NO;
                _data.ASSET_SUB = _data.ORIGINAL_ASSET_SUB;

                var _rs = dao.GetFixedAssetMaster(_data);
                if (_rs == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetOriginalAssetsData(WFD02115SaveModel _data)
        {
            Begin();
            try
            {
                var _rs = dao.GetOriginalAssetsData(_data);
                if (_rs == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public string GetLastLineNo(WFD02115SaveModel _data)
        {
            Begin();
            try
            {
                var _rs = dao.GetLastLineNo(_data);
                if (_rs == null)
                {
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found
                }
                return (_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return "0";
            }
        }

        public BaseJsonResult GetDeriveWBSBudget(WFD02115WBSModel _data)
        {
            try
            {
                var _rs = dao.GetDeriveWBSBudget(_data);               
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetDepreciationAutocomplete(string _company)
        {
            try
            {
                var _rs = dao.GetDepreciationAutocomplete(_company);
                if (_rs == null)
                {
                    return Result(new WFD02115DepreciationModel());
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult GetDefaultDepreciation(TB_M_DEPRECIATION _data)
        {
            try
            {
                var _rs = dao.GetDefaultDepreciation(_data);
                if(_rs == null)
                {
                    return Result(new TB_M_DEPRECIATION());
                }
                return Result(_rs);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        //private bool ValidateGetAssetsData(WFD02115SaveModel ds)
        //{
        //    string _checkCostCenter = "";
        //    //if (ds == null || ds.DETIAL_ASSET == null)
        //    //{
        //    //    AddMessage(Constants.MESSAGE.MCOM0001AERR, "adding asset no");
        //    //    return false;
        //    //}
        //    //if (ds.DETIAL_ASSET == null)
        //    //{
        //    //    AddMessage(Constants.MESSAGE.MSTD0059AERR);
        //    //    return false;
        //    //}
        //    //if (ds.SING_FLAG == "Y")
        //    //{
        //    //    if (ds.DETIAL_ASSET.Count > 1)
        //    //    {
        //    //        AddMessage(Constants.MESSAGE.MSTD0103AINF, "Asset No");
        //    //        return false;
        //    //    }
        //    //}
        //    //if (ds.DETIAL_ASSET.Count > 0)
        //    //{
        //    //    for (int i = 0; i < ds.DETIAL_ASSET.Count; i++)
        //    //    {
        //    //        if (i == 0)
        //    //        {
        //    //            _checkCostCenter = ds.DETIAL_ASSET[i].COST_CODE;
        //    //            continue;
        //    //        }
        //    //        if (_checkCostCenter != ds.DETIAL_ASSET[i].COST_CODE)
        //    //        {
        //    //            AddMessage(Constants.MESSAGE.MFAS0706AERR);//Can't select asset with difference cost center
        //    //            return false;
        //    //        }
        //    //    }

        //    //}
        //    return true;
        //}
        //private string ValidateFormatDate(string _date)
        //{
        //    var returnDate = "";
        //    int day;
        //    int year;
        //    int Month = 0;
        //    var arrDate = _date.Split('-');
        //    if (arrDate.Length != 3)
        //    {
        //        return "";
        //    }
        //    else
        //    {
        //        try
        //        {
        //            day = int.Parse(arrDate[0]);
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }
        //        try
        //        {
        //            year = int.Parse(arrDate[2]);
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }
        //        try
        //        {
        //            switch (arrDate[1])
        //            {
        //                case "Jan":
        //                    Month = 1;
        //                    break;
        //                case "Feb":
        //                    Month = 2;
        //                    break;
        //                case "Mar":
        //                    Month = 3;
        //                    break;
        //                case "Apr":
        //                    Month = 4;
        //                    break;
        //                case "May":
        //                    Month = 5;
        //                    break;
        //                case "Jun":
        //                    Month = 6;
        //                    break;
        //                case "Jul":
        //                    Month = 7;
        //                    break;
        //                case "Aug":
        //                    Month = 8;
        //                    break;
        //                case "Sep":
        //                    Month = 9;
        //                    break;
        //                case "Oct":
        //                    Month = 10;
        //                    break;
        //                case "Nov":
        //                    Month = 11;
        //                    break;
        //                case "Dec":
        //                    Month = 12;
        //                    break;
        //                default:
        //                    Month = 0;
        //                    break;
        //            }
        //            if (Month != 0)
        //            {
        //                returnDate = year.ToString() + "-" + Month.ToString() + "-" + day.ToString();
        //                return returnDate;
        //            }
        //            else
        //            {
        //                return "";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }
        //    }
        //}

        //public BaseJsonResult GetSystemData(WFD02115SystemModel ds)
        //{
        //    Begin();
        //    try
        //    {
        //        if (!ValidateRequireFields(ds)) return Result(null, null);
        //        WFD02115SystemModel _AssetH = new WFD02115SystemModel();
        //        _AssetH = dao.GetAssetsSystemData(ds);

        //        var datas = _AssetH;
        //        if (datas == null)
        //            AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found

        //        return Result(datas);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null);
        //    }
        //}

        //public BaseJsonResult GetWBSData(WFD02115WBSModel ds)
        //{
        //    Begin();
        //    try
        //    {
        //        if (!ValidateRequireFields(ds)) return Result(null, null);
        //        WFD02115WBSModel _AssetH = new WFD02115WBSModel();
        //        _AssetH = dao.GetWBSData(ds);

        //        var datas = _AssetH;
        //        if (datas == null)
        //            AddMessage(Constants.MESSAGE.MSTD0059AERR);//No data found

        //        return Result(datas);
        //    }
        //    catch (Exception ex)
        //    {
        //        AddExceptionMessage(ex);
        //        return Result(null);
        //    }
        //}


        private bool Validation(WFD02115SaveModel data, string MODE, HttpRequestBase Request = null)
        {
            bool isBool = true;

            //if (string.IsNullOrEmpty(data.ASSET_DESC))
            //{
            //    AddMessage("MSTD0031AERR", "Asset Description");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.ADTL_ASSET_DESC))
            //{
            //    AddMessage("MSTD0031AERR", "Additional Asset Desc.");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.FINAL_ASSET_CLASS))
            //{
            //    AddMessage("MSTD0031AERR", "Final Asset Class");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.SERIAL_NO))
            //{
            //    AddMessage("MSTD0031AERR", "Serial Number");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.INVEN_NOTE))
            //{
            //    AddMessage("MSTD0031AERR", "Inventory Note");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LICENSE_PLATE))
            //{
            //    AddMessage("MSTD0031AERR", "License Plate No.");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LOCATION))
            //{
            //    AddMessage("MSTD0031AERR", "Location Detail");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LEASE_LENGTH_YEAR.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Lease Length (year)");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LEASE_LENGTH_PERD.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Lease Length (period)");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LEASE_SUPPLEMENT.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Lease Supplementary");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LEASE_PAYMENT.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Lease payment");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.LEASE_ANU_INT_RATE.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Lease Annual Interest Rate");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.DPRE_KEY_01.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Depreciation key");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_YEAR_01.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Years");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_PERD_01.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Periods");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.SCRAP_VALUE_01.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Scrap value");
            //    isBool = false;
            //}

            /////--------------------------------------------------------------15

            //if (string.IsNullOrEmpty(data.DPRE_KEY_15.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Depreciation key 15");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_YEAR_15.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Years 15");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_PERD_15.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Periods 15");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.SCRAP_VALUE_15.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Scrap value 15");
            //    isBool = false;
            //}

            /////--------------------------------------------------------------31

            //if (string.IsNullOrEmpty(data.DPRE_KEY_31.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Depreciation key 31");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_YEAR_31.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Years 31");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_PERD_31.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Periods 31");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.SCRAP_VALUE_31.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Scrap value 31");
            //    isBool = false;
            //}

            /////--------------------------------------------------------------41

            //if (string.IsNullOrEmpty(data.DPRE_KEY_41.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Depreciation key 41");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_YEAR_41.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Years 41");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_PERD_41.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Periods 41");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.SCRAP_VALUE_41.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Scrap value 41");
            //    isBool = false;
            //}

            /////--------------------------------------------------------------81

            //if (string.IsNullOrEmpty(data.DPRE_KEY_81.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Depreciation key 81");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_YEAR_81.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Years 81");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.PLAN_LIFE_PERD_81.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Useful Life in Periods 81");
            //    isBool = false;
            //}

            //if (string.IsNullOrEmpty(data.SCRAP_VALUE_81.ToString()))
            //{
            //    AddMessage("MSTD0031AERR", "Scrap value 81");
            //    isBool = false;
            //}

            return isBool;
        }

    }
}
