﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.DAO.SFAS;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.SFAS.WFD01150;

namespace th.co.toyota.stm.fas.BusinessObject.SFAS
{
    public class WFD01150BO : BO
    {
        private WFD01150DAO dao;
        public WFD01150BO() : base()
        {
            dao = new WFD01150DAO();
        }

        public SearchJsonResult SearchApproveByRequestType(string COMPANY, string REQUEST_FLOW_TYPE, PaginationModel page)
        {
            Begin();
            try
            {
                if (string.IsNullOrEmpty(COMPANY))
                {
                    AddRequireMessage("Company");
                    return Result(null, page);
                }
                if (string.IsNullOrEmpty(REQUEST_FLOW_TYPE))
                {
                    AddRequireMessage("Operation");
                    return Result(null, page);
                }

                var data = dao.SearchApproveByRequestType(COMPANY, REQUEST_FLOW_TYPE, ref page);

                if (data == null || (data != null && data.Rows.Count <= 0))
                    AddMessage(Constants.MESSAGE.MSTD0059AERR);

                return Result(data, page);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null, page);
            }
        }

        public BaseJsonResult GetApproveDDatas(int? APPROVE_ID, string REQUEST_FLOW_TYPE)
        {
            Begin();
            try
            {
                var datas = APPROVE_ID == null ? new List<WFD01150ApproveDModel>() : dao.GetApproveDDatas(APPROVE_ID.Value);
                var hData = APPROVE_ID == null ? new TB_M_APPROVE_H() : dao.GetApproveH(APPROVE_ID.Value);
                var approveFlows = dao.GetDefaultFlow(REQUEST_FLOW_TYPE);
                //var headerEmail = dao.GetHeaderEmail(REQUEST_FLOW_TYPE);
                //var detailEmail = dao.GetDetailEmail(REQUEST_FLOW_TYPE, APPROVE_ID.Value);
                //SystemModel
                if (datas != null)
                {
                    foreach (var d in datas)
                    {
                        if (datas.Where(m => m.INDX.ToString() == d.REJECT_TO).Count() > 0)
                        {
                            var data = datas.Where(m => m.INDX.ToString() == d.REJECT_TO).First();
                            d.REJECT_TO_NAME = string.Format("{0}: {1}", data.INDX, data.ROLE_NAME);
                        }
                    }
                }
                Guid _Guid = Guid.NewGuid();
                hData.GUID = _Guid.ToString();
                return Result(new { hData = hData, datas = datas, approveFlows = approveFlows, GUID = _Guid });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        public BaseJsonResult GetApproveEmailDatas(WFD01150EditScreenModel data)
        {
            Begin();
            try
            {
                var headerEmail = dao.GetHeaderEmail(data);
                var detailEmail = dao.GetDetailEmail(data);
                var roleData = dao.GetApproveRoleList();

                return Result(new { hData = headerEmail, datas = detailEmail, rData = roleData });
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult UpdateApprove(TB_M_APPROVE_H hData, List<WFD01150ApproveDModel> datas)
        {
            Begin();
            WFD01150DAO dao = new WFD01150DAO();
            dao.beginTransaction();
            try
            {
                #region Validate group

                string targetGroup = string.Empty;
                List<string> groups = new List<string>();
                if (datas != null && datas.Count > 0)
                {
                    foreach (var d in datas.OrderBy(m => m.INDX))
                    {
                        if (string.IsNullOrEmpty(d.APPRV_GROUP)) continue;
                        if (targetGroup != d.APPRV_GROUP)
                        {
                            targetGroup = d.APPRV_GROUP;

                            if (groups.Where(m => m == d.APPRV_GROUP).Count() <= 0) groups.Add(d.APPRV_GROUP);
                            else
                            {
                                AddMessage(Constants.MESSAGE.MFAS0701AERR);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MFAS0704AERR);
                    return Result(-1);
                }

                #endregion

                // Validate Finish 
                if (datas.Where(m => m.FINISH_FLOW == "Y").Count() > 1) AddMessage(Constants.MESSAGE.MFAS0702AERR);
                if (datas.Where(m => m.FINISH_FLOW == "Y").Count() <= 0) AddMessage(Constants.MESSAGE.MFAS0703AERR);

                if (!IsValid) return Result(false);

                dao.UpdateApproveH(hData);
                foreach (var d in datas.Where(m => !string.IsNullOrEmpty(m.ROLE)))
                {
                    d.APPRV_ID = hData.APPRV_ID;
                    dao.AddApproveD(d);
                }
                dao.UpdateApproveEmailDetail(hData);

                dao.commitTransaction();
                AddMessage(Constants.MESSAGE.MSTD0101AINF);

                return Result(true);
            }
            catch (Exception ex)
            {
                dao.rollbackTransaction();
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public BaseJsonResult AddApprove(string Company, List<WFD01150ApproveDModel> datas, string AssetClass, string ApplyToAll, string requestFlowType, string UserId, string GUID)
        {
            Begin();
            int APPRV_ID = -1;
            var assetClass = new SystemDAO().SelectSystemDatas(SYSTEM_CATEGORY.ASSET_CLASS.ToString(), SYSTEM_SUB_CATEGORY.ASSET_CLASS.ToString());
            WFD01150DAO dao = new WFD01150DAO();
            dao.beginTransaction();
            try
            {
                #region Validate group

                string targetGroup = string.Empty;
                List<string> groups = new List<string>();
                if (datas != null && datas.Count > 0)
                {
                    foreach (var d in datas.OrderBy(m => m.INDX))
                    {
                        if (string.IsNullOrEmpty(d.APPRV_GROUP)) continue;
                        if (targetGroup != d.APPRV_GROUP)
                        {
                            targetGroup = d.APPRV_GROUP;

                            if (groups.Where(m => m == d.APPRV_GROUP).Count() <= 0) groups.Add(d.APPRV_GROUP);
                            else
                            {
                                AddMessage(Constants.MESSAGE.MFAS0701AERR);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    AddMessage(Constants.MESSAGE.MFAS0704AERR);
                    return Result(-1);
                }

                #endregion

                // Validate Finish Flow
                if (datas.Where(m => m.FINISH_FLOW == "Y").Count() > 1) AddMessage(Constants.MESSAGE.MFAS0702AERR);
                if (datas.Where(m => m.FINISH_FLOW == "Y").Count() <= 0) AddMessage(Constants.MESSAGE.MFAS0703AERR);

                if (!string.IsNullOrEmpty(AssetClass) && ApplyToAll == "Y")
                    AddMessage(Constants.MESSAGE.MCOM0014AERR, "Fix Asset Category", "Apply to all asset");

                if (string.IsNullOrEmpty(AssetClass) && ApplyToAll == "N")
                    AddMessage(Constants.MESSAGE.MSTD0031AERR, "Fix Asset Category or Apply to all asset");

                if (!IsValid) return Result(-1);


                if (ApplyToAll == "Y")
                {
                    var acDup = dao.CheckDupplicateApproveH(Company, requestFlowType, string.Join(",", assetClass.Select(m => m.CODE)));
                    if (acDup.Count > 0)
                    {
                        string invalidAssetCate = string.Join(",", acDup);
                        AddMessage(Constants.MESSAGE.MSTD0010AERR, "Fix Asset Category", invalidAssetCate, "TB_M_APPROVE_H");
                    }
                }
                else
                {
                    var acDup = dao.CheckDupplicateApproveH(Company, requestFlowType, string.Join(",", AssetClass));
                    if (acDup.Count > 0)
                    {
                        string invalidAssetCate = string.Join(",", acDup);
                        AddMessage(Constants.MESSAGE.MSTD0010AERR, "Fix Asset Category", invalidAssetCate, "Approve flow request type = " + requestFlowType);
                    }
                }

                if (!IsValid) return Result(-1);

                if (ApplyToAll == "Y")
                {
                    foreach (var a in assetClass.Select(m => m.CODE))
                    {

                        APPRV_ID = dao.AddApproveH(new TB_M_APPROVE_H()
                        {
                            COMPANY = Company,
                            REQUEST_FLOW_TYPE = requestFlowType,
                            ASSET_CLASS = a,
                            CREATE_BY = UserId
                        });
                        foreach (var d in datas)
                        {
                            d.APPRV_ID = APPRV_ID;
                            dao.AddApproveD(d);
                        }
                        TB_M_APPROVE_H _dHA = new TB_M_APPROVE_H();
                        _dHA.APPRV_ID = APPRV_ID;
                        _dHA.GUID = GUID;
                        _dHA.UPDATE_BY = UserId;
                        dao.InsertApproveEmailDetail(_dHA);
                    }
                    APPRV_ID = -99;
                }
                else
                {
                    APPRV_ID = dao.AddApproveH(new TB_M_APPROVE_H()
                    {
                        COMPANY = Company,
                        REQUEST_FLOW_TYPE = requestFlowType,
                        ASSET_CLASS = AssetClass,
                        CREATE_BY = UserId
                    });
                    foreach (var d in datas)
                    {
                        d.APPRV_ID = APPRV_ID;
                        dao.AddApproveD(d);
                    }
                    TB_M_APPROVE_H _dH = new TB_M_APPROVE_H();
                    _dH.APPRV_ID = APPRV_ID;
                    _dH.GUID = GUID;
                    _dH.UPDATE_BY = UserId;
                    dao.InsertApproveEmailDetail(_dH);
                }



                dao.commitTransaction();
                AddMessage(Constants.MESSAGE.MSTD0101AINF);

                return Result(APPRV_ID);
            }
            catch (Exception ex)
            {
                dao.rollbackTransaction();
                AddExceptionMessage(ex);
                return Result(-1);
            }
        }

        public BaseJsonResult ValidateSaveApproveDetail(TB_M_APPROVE_D data)
        {
            Begin();
            try
            {
                List<RV> requiredFields = new List<RV>();
                requiredFields.Add(new RV("ROLE", "Role"));
                requiredFields.Add(new RV("DIVISION", "DIVISION"));

                if (data.CONDITION_CODE == "D") requiredFields.Add(new RV("OPERATOR", "Operator"));
                if (data.OPERATOR == "1" || data.OPERATOR == "2" || data.OPERATOR == "3") requiredFields.Add(new RV("VALUE1", "Value 1"));
                if (data.OPERATOR == "3")
                {
                    requiredFields.Add(new RV("VALUE2", "Value 2"));
                    if (double.Parse(data.VALUE2) <= double.Parse(data.VALUE1))
                    {
                        AddMessage(Constants.MESSAGE.MSTD0021AERR, "Value 2", "Value 1");
                    }
                }

                if (data.ALLOW_REJECT == "Y") requiredFields.Add(new RV("REJECT_TO", "Reject To"));

                if (!ValidateRequireFields(data, requiredFields.ToArray())) return Result(false);


                if (IsValid)
                {
                    dao.SaveTempApproveD(data);
                }

                return Result(IsValid);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public BaseJsonResult DeleteApprove(WFD01150SearchApproveRowModel data)
        {
            Begin();
            try
            {
                dao.DeleteApprove(data);
                AddMessage(Constants.MESSAGE.MSTD0090AINF);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public BaseJsonResult SaveTempApproveEmailDetail(List<WFD01150ApproveEmailDModel> data, string UserId)
        {
            Begin();
            try
            {
                dao.SaveTempApproveEmailDetail(data, UserId);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public BaseJsonResult DeleteTempApproveEmailDetail(WFD01150ApproveEmailDModel data)
        {
            Begin();
            try
            {
                dao.DeleteTempApproveEmailDetail(data);
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
        public BaseJsonResult ValidateAddApprove(string REQUEST_TYPE)
        {
            Begin();
            try
            {
                if (string.IsNullOrEmpty(REQUEST_TYPE))
                {
                    AddRequireMessage("Operation");
                    return Result(false);
                }

                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(false);
            }
        }
    }
}
