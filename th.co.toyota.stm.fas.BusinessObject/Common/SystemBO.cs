﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.DAO.Shared;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class SystemBO : BO
    {
        private SystemDAO dao;
        private SystemConfigurationDAO system_dao;
        public SystemBO()
        {
            dao = new SystemDAO();
            system_dao = new SystemConfigurationDAO();
        }

        public SystemModel SelectSystemDatas(string CATEGORY, string SUB_CATEGORY, string CODE)
        {
            try
            {
                return dao.SelectSystemValue(CATEGORY, SUB_CATEGORY, CODE);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SystemModel> SelectSystemDatas(string CATEGORY, string SUB_CATEGORY)
        {
            try
            {
                return dao.SelectSystemDatas(CATEGORY, SUB_CATEGORY);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SystemModel> SelectSystemDatasOrderByCode(string CATEGORY, string SUB_CATEGORY)
        {
            try
            {
                return dao.SelectSystemDatasOrderByCode(CATEGORY, SUB_CATEGORY);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SystemModel> SelectSystemDatasOrderByRemark(string CATEGORY, string SUB_CATEGORY)
        {
            try
            {
                return dao.SelectSystemDatasOrderByRemark(CATEGORY, SUB_CATEGORY);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetPathDocUpload(string _Code)
        {
            try
            {
                string path = "";
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = Constants.CATEGORY.SYSTEM_CONFIG;
                m.SUB_CATEGORY = Constants.SUB_CATEGORY.DOC_UPLOAD;
                m.CODE = _Code;
                var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();

                if(data == null)
                {
                    throw (new Exception(string.Format("Not Found Path Config Category = [{0}] Sub Cate = [{1}]", m.CATEGORY, m.SUB_CATEGORY))) ;

                }

                if (data != null)
                {
                    path = data.VALUE;
                }

                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                return path;
            }
            catch (Exception ex)
            {
                throw(ex) ;
            }

        }

        public string GetPathFileType(string _Code)
        {
            try
            {
                string path = "";
                SystemConfigurationModels m = new SystemConfigurationModels();
                m.CATEGORY = Constants.CATEGORY.SYSTEM_CONFIG;
                m.SUB_CATEGORY = Constants.SUB_CATEGORY.PATH_FILE_TYPE;
                m.CODE = _Code.Replace(".","");
                var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
                if (data != null)
                {
                    path = data.VALUE;
                }
                else {
                    path = "/Images/ini-icon.png";
                }                
                return path;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public string GetAllowFileExtension(string _Code)
        {
            var m = new SystemConfigurationModels()
            {
                CATEGORY = "SYSTEM_CONFIG",
                SUB_CATEGORY = "EXTENSION_FILE",
                CODE = _Code
            };
           
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                return string.Format("{0}", data.VALUE) ;
            }

            m = new SystemConfigurationModels()
            {
                CATEGORY = "SYSTEM_CONFIG",
                SUB_CATEGORY = "EXTENSION_FILE",
                CODE = "DEFAULT"
            };

            data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                return string.Format("{0}", data.VALUE);
            }


            return string.Empty;
            
        }
        public double GetMaximumFileKB(string _Code)
        {
            var m = new SystemConfigurationModels()
            {
                CATEGORY = "SYSTEM_CONFIG",
                SUB_CATEGORY = "FILE_SIZE",
                CODE = _Code
            };

            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                return Convert.ToDouble(data.VALUE) * (1024);
            }
            m = new SystemConfigurationModels()
            {
                CATEGORY = "SYSTEM_CONFIG",
                SUB_CATEGORY = "FILE_SIZE",
                CODE = "DEFAULT"
            };
            if (data != null)
            {
                return Convert.ToDouble(data.VALUE) * (1024);
            }
            return 3 * (1024 ) ;
        }

        //
        public List<SystemModel> GetSystemMasterAutoComplete(string CATEGORY, string SUB_CATEGORY, bool CodeFlag, string Keyword)
        {
            try
            {
                var _rs = dao.GetSystemMasterAutoComplete(CATEGORY, SUB_CATEGORY, CodeFlag ? "Y" : "N", Keyword);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                _rs.Insert(0, new SystemModel() { CATEGORY = CATEGORY, SUB_CATEGORY = SUB_CATEGORY, CODE = string.Empty, VALUE = string.Empty });
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }
        public List<SystemModel> GetSystemMasterAutoCompleteWhereRemark(string CATEGORY, string SUB_CATEGORY, string CODE, bool CodeFlag, string Remark)
        {
            Begin();
            try
            {
                var _rs = dao.GetSystemMasterAutoCompleteWhereRemark(CATEGORY, SUB_CATEGORY, CODE, CodeFlag ? "Y" : "N", Remark);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }

        public List<SystemModel> GetAUCAssetClassAutoComplete(bool CodeFlag, string Company, string Keyword)
        {
            try
            {
                var _rs = dao.GetAUCAssetClass(CodeFlag ? "Y" : "N", Company, Keyword);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                _rs.Insert(0, new SystemModel() { CATEGORY = string.Empty, SUB_CATEGORY = string.Empty, CODE = string.Empty, VALUE = string.Empty });
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }
        public List<SystemModel> GetFinalAssetClassAutoComplete(bool CodeFlag, string Company, string Keyword)
        {
            try
            {
                var _rs = dao.GetFinalAssetClass(CodeFlag ? "Y" : "N", Company, Keyword);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                _rs.Insert(0, new SystemModel() { CATEGORY = string.Empty, SUB_CATEGORY = string.Empty, CODE = string.Empty, VALUE = string.Empty });
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }
        //
        public List<SystemModel> GetAssetClassAutoComplete(bool CodeFlag, string Company, string Keyword)
        {
            try
            {
                var _rs = dao.GetAssetClass(CodeFlag ? "Y" : "N", Company, Keyword);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                _rs.Insert(0, new SystemModel() { CATEGORY = string.Empty, SUB_CATEGORY = string.Empty, CODE = string.Empty, VALUE = string.Empty });
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }
        public List<SystemModel> GetMinorCategoryByAssetClass(bool CodeFlag, string Company, string AssetClass)
        {
            try
            {
                var _rs = dao.GetMinorCategoryByAssetClass(CodeFlag ? "Y" : "N", Company, AssetClass);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                _rs.Insert(0, new SystemModel() { CATEGORY = string.Empty, SUB_CATEGORY = string.Empty, CODE = string.Empty, VALUE = string.Empty });
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }
        //sp_Common_GetAUCAssetClass_AutoComplete
        public List<DepreciationModel> SelectDepreciationDatas(string COMPANY, string ASSET_CLASS, string MINOR_CATEGORY)
        {
            try
            {
                return dao.SelectDepreciationDatas( COMPANY,  ASSET_CLASS,  MINOR_CATEGORY);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SystemModel> SelectSpecialRoleDatas(string CATEGORY, string SUB_CATEGORY)
        {
            try
            {
                return dao.SelectSpecialRoleDatas(CATEGORY, SUB_CATEGORY);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SystemModel> GetCommonMasterAutoComplete(string CATEGORY, string SUB_CATEGORY, bool CodeFlag, string Keyword)
        {
            try
            {
                var _rs = dao.GetSystemMasterAutoComplete(CATEGORY, SUB_CATEGORY, CodeFlag ? "Y" : "N", Keyword);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                //_rs.Insert(0, new SystemModel() { CATEGORY = CATEGORY, SUB_CATEGORY = SUB_CATEGORY, CODE = string.Empty, VALUE = string.Empty });
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }

    }
    
}
