﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class BatchProcessBO : BO
    {
        private BatchProcessDAO dao;
        public BatchProcessBO()
        {
            dao = new BatchProcessDAO();
        }

        public BaseJsonResult GetUserBatchAppId(string BATCH_ID, string USER_ID)
        {
            Begin();
            try
            {
                var data = dao.GetUserBatchAppId(BATCH_ID, USER_ID);
                var msg = new MessageBO().GetMessage(Constants.MESSAGE.MCOM0024AERR, "[APP_ID]");


                return Result(new { data = data, message = msg.VALUE });
            }
            catch (Exception ex)
            {
                return Result(null);
            }
        }

        public BaseJsonResult addBatchQ(StartBatchModel data)
        {
            Begin();
            try
            {
                decimal appId = dao.insertBatchQ(data);
                RunCMBatch(appId, data.UserId); // ODB Batch calling
                return Result(appId);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        private void RunCMBatch(decimal appId, string UserId)
        {

            //  string exePath = BatchProcess.TestBatch;
            string exePath = dao.GetODBBatchCallingPath();
            if (File.Exists(exePath))
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = exePath,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                proc.Start();
            }
            else
            {
                dao.InsertLog(appId, BATCH_STATUS.C.ToString(), "I", string.Format("Cannot find exe file path {0}", exePath), UserId);
                dao.UpdateBatchStatus(appId, BATCH_STATUS.E.ToString());
            }
        }

        public BaseJsonResult GetBatchStatus(int APP_ID)
        {
            Begin();
            try
            {
                var status = dao.GetBatchStatus(APP_ID);

                return Result(status);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }

        public BaseJsonResult CancelBatch(decimal APP_ID, string UserId)
        {
            Begin();
            try
            {
                int processId = dao.GetBatchProcessId(APP_ID);

                if (processId != -1)
                {
                    if (ProcessExists(processId))
                    {
                        KillBatch(processId);
                    }
                    dao.InsertLog(APP_ID, BATCH_STATUS.C.ToString(), "I", "Cancel batch by user", UserId);
                }
                else
                {
                    dao.InsertLog(APP_ID, BATCH_STATUS.C.ToString(), "I", "Cancel batch by user but cannot find process id to kill batch application.", UserId);
                }

                dao.UpdateBatchStatus(APP_ID, BATCH_STATUS.C.ToString());
                return Result(true);
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return Result(null);
            }
        }
        private void KillBatch(int processId)
        {
            Process p = Process.GetProcessById(processId);
            p.Kill();
        }
        private bool ProcessExists(int id)
        {
            return Process.GetProcesses().Any(x => x.Id == id);
        }

        public byte[] GetFileDownload(decimal APP_ID, ref string fileName, ref string filepath)
        {
            try
            {
                var fileData = new FileDownloadDAO().GetFileDownloadFromAppId(APP_ID);
                byte[] fileBytes = System.IO.File.ReadAllBytes(fileData.FILE_PATH);
                filepath = fileData.FILE_PATH;
                fileName = Path.GetFileName(fileData.FILE_PATH);
                return fileBytes;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return null;
            }
        }

        public Byte[] ReadLocalFile(decimal APP_ID, bool isDeleteAfterDownload, ref string fileName, ref string filepath)
        {

            var fileData = new FileDownloadDAO().GetFileDownloadFromAppId(APP_ID);
            string file = fileData.FILE_PATH;

            fileName = new FileInfo(fileData.FILE_PATH).Name;

            MemoryStream memoryStream = new MemoryStream();
            using (Stream input = File.OpenRead(file))
            {
                byte[] buffer = new byte[32 * 1024]; // 32K buffer for example
                int bytesRead;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, bytesRead);
                }
            }
            memoryStream.Position = 0;
            Byte[] byteArray = memoryStream.ToArray();
            memoryStream.Flush();
            memoryStream.Close();

            //if (isDeleteAfterDownload) // No need remove file on screen 
            //    File.Delete(file);

            return byteArray;
        }


        //public FileResult Download()
        //{
        //    byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\folder\myfile.ext");
        //    string fileName = "myfile.ext";
        //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        //}
    }
    public enum BatchTypes
    {
        TEST = -1
    }
}