﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Configuration;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class CommonBO : BO
    {
        private SystemDAO dao;
    
        public CommonBO()
        {
            dao = new SystemDAO();
        }

        public bool IsSc2OnlineMode()
        {
            //#CheckDeploy
            string deployProfile = ConfigurationManager.AppSettings["deployProfile"];
            bool isSc2OnlineMode = Constants.Deployment.SC2_OFFLINE;
            if (string.IsNullOrEmpty(deployProfile))
            {
                deployProfile = Constants.Deployment.MODE_OFFLINE;
            }

            
            if (Constants.Deployment.MODE_OFFLINE.Equals(deployProfile) || Constants.Deployment.MODE_PU.Equals(deployProfile))
            {
                isSc2OnlineMode = Constants.Deployment.SC2_OFFLINE;
            }
            else if (Constants.Deployment.MODE_IFT.Equals(deployProfile))
            {
                isSc2OnlineMode = Constants.Deployment.SC2_OFFLINE;
            }
            else if (Constants.Deployment.MODE_BCT.Equals(deployProfile)
                || Constants.Deployment.MODE_BCTIS.Equals(deployProfile)
                || Constants.Deployment.MODE_UT.Equals(deployProfile)
                || Constants.Deployment.MODE_PRODUCTION.Equals(deployProfile))
            {
                isSc2OnlineMode = Constants.Deployment.SC2_ONLINE;
            }
            else
            {
                isSc2OnlineMode = Constants.Deployment.SC2_OFFLINE;
            }

            return isSc2OnlineMode;
        }
        
        //
        public List<SystemModel> GetSystemMasterAutoComplete(string CATEGORY, string SUB_CATEGORY, bool CodeFlag, string Keyword)
        {
            Begin();
            try
            {
                var _rs = dao.GetSystemMasterAutoComplete(CATEGORY, SUB_CATEGORY, CodeFlag ? "Y" : "N", Keyword);
                if (_rs == null)
                {
                    return new List<SystemModel>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<SystemModel>();
            }
        }
        
        public List<Models.CostCenterAutoCompleteModels> GetCostCenterAutoComplete(string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                var _rs = dao.GetCostCenterAutoComplete(_keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.CostCenterAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.CostCenterAutoCompleteModels>();
            }
        }
        public List<Models.CostCenterAutoCompleteModels> GetResponsibleCenterAutoComplete(string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                
                var _rs = dao.GetResponseCostCenterAutoComplete(_keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.CostCenterAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.CostCenterAutoCompleteModels>();
            }
        }
        public List<Models.WBSMasterAutoCompleteModels> GetWBSBudgetAutoComplete(string _company, string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                var _rs = dao.GetWBSBudgetAutoComplete(_company, _keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.WBSMasterAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.WBSMasterAutoCompleteModels>();
            }
        }
        

        public List<Models.WBSMasterAutoCompleteModels> GetWBSProjectAutoComplete(string _company, string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                var _rs = dao.GetWBSProjectAutoComplete(_company, _keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.WBSMasterAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.WBSMasterAutoCompleteModels>();
            }
        }
        public List<Models.FixAssetsAutoCompleteModels> GetFixedAssetAutoComplete(string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                var _rs = dao.GetFixedAssetAutoComplete(_keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.FixAssetsAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.FixAssetsAutoCompleteModels>();
            }
        }

        public List<Models.OrganizationAutoCompleteModels> GetDivisionAutoComplete(string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                if (string.IsNullOrWhiteSpace(_keyword))
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }

                var _rs = dao.GetDivisionAutoComplete(_keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.OrganizationAutoCompleteModels>();
            }
        }

        public List<Models.OrganizationAutoCompleteModels> GetSubDivistionAutoComplete(string _keyword, string _EmpCode, string _divKeyword)
        {
            Begin();
            try
            {
                if (string.IsNullOrWhiteSpace(_keyword))
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }

                var _rs = dao.GetSubDivistionAutoComplete(_keyword, _EmpCode, _divKeyword);
                if (_rs == null)
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.OrganizationAutoCompleteModels>();
            }
        }

        public List<Models.OrganizationAutoCompleteModels> GetDepartmentAutoComplete(string _keyword, string _EmpCode, string _subDivKeyword)
        {
            Begin();
            try
            {
                if (string.IsNullOrWhiteSpace(_keyword))
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }

                var _rs = dao.GetDepartmentAutoComplete(_keyword, _EmpCode, _subDivKeyword);
                if (_rs == null)
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.OrganizationAutoCompleteModels>();
            }
        }

        public List<Models.OrganizationAutoCompleteModels> GetGetSectionAutoComplete(string _keyword, string _EmpCode, string _depKeyword)
        {
            Begin();
            try
            {
                if (string.IsNullOrWhiteSpace(_keyword))
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }

                var _rs = dao.GetGetSectionAutoComplete(_keyword, _EmpCode, _depKeyword);
                if (_rs == null)
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.OrganizationAutoCompleteModels>();
            }
        }

        public List<Models.OrganizationAutoCompleteModels> GetGetLineAutoComplete(string _keyword, string _EmpCode, string _secKeyword)
        {
            Begin();
            try
            {
                var _rs = dao.GetGetLineAutoComplete(_keyword, _EmpCode, _secKeyword);
                if (_rs == null)
                {
                    return new List<Models.OrganizationAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.OrganizationAutoCompleteModels>();
            }
        }

        public List<Models.EmployeeAutoCompleteModels> GetGetPositionAutoComplete(string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                var _rs = dao.GetGetPositionAutoComplete(_keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.EmployeeAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.EmployeeAutoCompleteModels>();
            }
        }

        public List<Models.EmployeeAutoCompleteModels> GetJobAutoComplete(string _keyword, string _EmpCode)
        {
            Begin();
            try
            {
                var _rs = dao.GetJobAutoComplete(_keyword, _EmpCode);
                if (_rs == null)
                {
                    return new List<Models.EmployeeAutoCompleteModels>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<Models.EmployeeAutoCompleteModels>();
            }
        }

        public List<TB_M_EMPLOYEE> GetEmployeeAutoComplete(string _keyword, string _EmpCode, string company)
        {
            Begin();
            try
            {
                var _rs = dao.GetEmployeeAutoComplete(_keyword, _EmpCode, company);
                if (_rs == null)
                {
                    return new List<TB_M_EMPLOYEE>();
                }
                return _rs;
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex);
                return new List<TB_M_EMPLOYEE>();
            }
        }

    }
    
}
