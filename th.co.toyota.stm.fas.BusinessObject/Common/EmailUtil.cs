﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class EmailUtil
    {
        private static string emailTemplateFolder = HostingEnvironment.ApplicationPhysicalPath + "Config/EmailTemplate/";
        public static string getEmailTemplateFileVal(string EmailCode, bool isThrow = true)
        {
            string filePath = emailTemplateFolder + EmailCode + ".txt";
            if (!File.Exists(filePath))
            {
                if (isThrow) throw new Exception("There is no file template at path: " + filePath);
                else return string.Empty;
            }

            using (StreamReader sr = new StreamReader(filePath))
            {
                return sr.ReadToEnd();
            }
        }

        public static string GenerateBodyEmailTemplate(string EmailCode, params string[] param)
        {
            string text = getEmailTemplateFileVal(EmailCode, false);
            string sysEmailBody = new SystemDAO().SelectSystemValue(SYSTEM_CATEGORY.SYSTEM_EMAIL, SYSTEM_SUB_CATEGORY.BODY, EmailCode).VALUE;
            text += string.Format(sysEmailBody, param);
            return text;
        }
    }
}
