﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class JJWorkingTime
    {
        private string _workingTimeStartStr;
        private string _workingTimeEndStr;
        private string _breakTimeStartStr;
        private string _breakTimeEndStr;

        private TimeSpan workingTimeStart;
        private TimeSpan workingTimeEnd;
        private TimeSpan breakTimeStart;
        private TimeSpan breakTimeEnd;

        public JJWorkingTime(string workingTimeStartStr, string workingTimeEndStr, string breakTimeStartStr, string breakTimeEndStr)
        {
            _workingTimeStartStr = workingTimeStartStr;
            _workingTimeEndStr = workingTimeEndStr;
            _breakTimeStartStr = breakTimeStartStr;
            _breakTimeEndStr = breakTimeEndStr;

            workingTimeStart = _convertStringToTimeSpan(workingTimeStartStr);
            workingTimeEnd = _convertStringToTimeSpan(workingTimeEndStr);
            breakTimeStart = _convertStringToTimeSpan(breakTimeStartStr);
            breakTimeEnd = _convertStringToTimeSpan(breakTimeEndStr);

            if (workingTimeStart > breakTimeStart || breakTimeEnd > workingTimeEnd)
            {
                throw new Exception("Break time can be only in period of working time.");
            }
        }

        #region Get minute
        public int GetDifMinute(DateTime from, DateTime To)
        {
            int minute = 0;
            DateTime date = from;

            while (date.Date <= To.Date)
            {
                DateTime f = date;
                DateTime t;

                if (date.Date == To.Date)
                    t = date.Date + To.TimeOfDay;
                else if (date.Date < To.Date) t = date.Date + workingTimeEnd;
                else return 0;

                minute += getMinuteZone1(f, t);
                minute += getMinuteZone2(f, t);

                date = date.AddDays(1).Date + new TimeSpan(0, 0, 0);
            }

            return minute;
        }
        private int GetDifMinute(TimeSpan t1, TimeSpan t2)
        {
            if (t2 <= t1) return 0;
            return (int)(t2 - t1).TotalMinutes;
        }
        private int getMinuteZone1(DateTime from, DateTime To)
        {
            if (from >= To) return 0;
            TimeSpan t1, t2;

            if (from.TimeOfDay >= workingTimeStart) t1 = from.TimeOfDay;
            else t1 = workingTimeStart;

            if (To.TimeOfDay <= breakTimeStart) t2 = To.TimeOfDay;
            else t2 = breakTimeStart;

            return GetDifMinute(t1, t2);
        }
        private int getMinuteZone2(DateTime from, DateTime To)
        {
            if (from >= To) return 0;
            TimeSpan t1, t2;

            if (from.TimeOfDay >= breakTimeEnd) t1 = from.TimeOfDay;
            else t1 = breakTimeEnd;

            if (To.TimeOfDay <= workingTimeEnd) t2 = To.TimeOfDay;
            else t2 = workingTimeEnd;

            return GetDifMinute(t1, t2);
        }
        #endregion


        public DateTime GetResultDateTimeAfterAddMinute(DateTime from, int minute)
        {
            Console.WriteLine("Start calculate : ");
            while (minute > 0)
            {
                Console.WriteLine("Date: {0} Minute: {1}", from.ToString("dd/MM/yyyy HH:mm:ss"), minute);
                if (from.TimeOfDay < workingTimeStart) from = from.Date + workingTimeStart;
                else if (from.TimeOfDay >= breakTimeStart && from.TimeOfDay < breakTimeEnd) from = from.Date + breakTimeEnd;
                else if (from.TimeOfDay >= workingTimeEnd) from = from.Date.AddDays(1) + workingTimeStart;
                else
                {
                    from = from.AddMinutes(1);
                    minute -= 1;
                }
            }

            return from;
        }
        private int getTimeDiffZone1(TimeSpan t)
        {
            if (t < workingTimeStart) t = workingTimeStart;
            if (t >= breakTimeStart) return 0;

            return GetDifMinute(t, breakTimeStart);
        }
        private int getTimeDiffZone2(TimeSpan t)
        {
            if (t < breakTimeEnd) t = breakTimeEnd;
            if (t >= workingTimeEnd) return 0;

            return GetDifMinute(t, workingTimeEnd);
        }

        private TimeSpan _convertStringToTimeSpan(string timeStr)
        {
            return new TimeSpan(timeStr.Split(':')[0].ToInt(), timeStr.Split(':')[1].ToInt(), 0);
        }
    }

    public static class JStringUtil
    {
        public static int ToInt(this string val)
        {
            return int.Parse(val);
        }
    }
}
