﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class MessageBO
    {
        private MessageDAO dao;
        public MessageBO()
        {
            dao = new MessageDAO();
        }

        private MessageModel GetMessagefromDB(string MESSAGE_CODE)
        {
            try
            {
                return dao.GetMessage(MESSAGE_CODE);
            }
            catch (Exception ex)
            {
                return new MessageModel()
                {
                    MESSAGE_CODE = "MSTD0000AERR" , MESSAGE_TEXT = ex.Message, MESSAGE_TYPE = "ERR"
                };
            }
        }

        private MessageStatus GetMessageStatus(string status)
        {
            MessageStatus messageStatus = MessageStatus.NONE;
            switch (status)
            {
                case "INF":
                    messageStatus = MessageStatus.INFO;
                    break;
                case "WRN":
                    messageStatus = MessageStatus.WARNING;
                    break;
                case "CFM":
                    messageStatus = MessageStatus.CONFIRM;
                    break;
                case "ERR":
                    messageStatus = MessageStatus.ERROR;
                    break;
            }
            return messageStatus;
        }

        public SystemMessageModel GetMessage(string messageID, params object[] parameter)
        {

                MessageModel m_Model = new MessageModel();
                SystemMessageModel returnModel = new SystemMessageModel();
                m_Model = this.GetMessagefromDB(messageID);
                if (m_Model != null)
                {
                    returnModel.ID = m_Model.MESSAGE_CODE;
                    returnModel.STATUS = this.GetMessageStatus(m_Model.MESSAGE_TYPE);
                    returnModel.VALUE = m_Model.MESSAGE_TEXT;

                    if (parameter != null)
                    {
                        returnModel.VALUE = m_Model.MESSAGE_CODE + " : " +String.Format(m_Model.MESSAGE_TEXT, parameter);
                    }
                }
                return returnModel;
           
        }

        public SystemMessageModel GetRequireErrorMessage(string FieldName)
        {
            return GetMessage("MSTD0031AERR", FieldName);
        }
    }
}
