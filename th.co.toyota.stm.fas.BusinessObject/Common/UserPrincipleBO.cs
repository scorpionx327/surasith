﻿using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class UserPrincipleBO
    {

        public UserPrinciple SetUser(UserInfo User)
        {
            try
            {
                if (User != null)
                {
                    return new UserPrinciple()
                    {
                        User = User,
                        IsAECUser = new WFD01210BO().IsAECUser(User.UserId)
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public HttpCookie SetUserCk(UserInfo User)
        {
            try
            {

                if (User != null)
                {

                    var usrData = new UserPrinciple()
                    {
                        User = User,
                        IsAECUser = new WFD01210BO().IsAECUser(User.UserId)
                    };

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(usrData);

                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                1,
                                usrData.DisplayName,
                                DateTime.Now,
                                DateTime.Now.AddMinutes(30),
                                false,
                                userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    return faCookie;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool isSystemAdmin(List<string> roleList)
        {
            bool _valReturn = false;
            if (roleList != null)
            {
                SystemBO bo = new SystemBO();
                var datas = bo.SelectSystemDatas("SYSTEM_CONFIG", "ADMIN_ROLE");
                if (datas != null)
                {
                    foreach (var d in datas.OrderBy(m => m.CODE))
                    {
                        if (d.CODE == "ADMIN")
                        {
                            foreach (var r in roleList)
                            {
                                if (d.VALUE == r)
                                {
                                    _valReturn = true;
                                    return _valReturn;
                                }
                            }
                        }
                    }
                }
            }

            return _valReturn;
        }

        public static bool IsAECUser(string UserID)
        {
            bool _valReturn = false;
            _valReturn = new WFD01210BO().IsAECUser(UserID);
            return _valReturn;
        }

        public static bool IsAECManager(string SysEmpCode)
        {
            bool _valReturn = false;

            var _usr = (new DAO.WFD01210DAO()).GetEmployeeByEmpCodeDAO(
                                    new Models.WFD01210.WFD01210SearchModel { EMP_CODE = SysEmpCode }); //SysEmpCode = {Company Code}.{Emp Code} , STM.1112
            if (_usr != null)
            {
                _valReturn = _usr.AEC_MANAGER == "Y";
            }
            else
            {
                _valReturn = false;
            }

            return _valReturn;
        }
        public static List<string> GetCompanyList(string UserID)
        {
            return new WFD01210BO().GetCompanyList(UserID);
        }

        public static string GetCompanyFromSC2(string location)
        {
            if ("TMAP-EM".Equals(location.ToUpper()))
            {
                return "TDEM";
            }
            else
            {
                return location.ToUpper();
            }
            
        }

        public static bool IsAccessButton(string button_id, AccessControlList Acl)
        {
            bool _valReturn = false;
            if(Acl != null)
            { 
                foreach (var d in Acl.MapButtonAcl)
                {
                    if(d.Key.ButtonId== button_id)
                    {
                        _valReturn = true;
                    }
                }
            }else
            {
                _valReturn = true; // incase cannot access control list
            }
            return _valReturn;
        }
    }
}
