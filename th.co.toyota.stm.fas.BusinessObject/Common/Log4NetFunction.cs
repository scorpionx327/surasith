﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Repository;


namespace th.co.toyota.stm.fas.BusinessObject.Common
{
    public class Log4NetFunction
    {
        protected static readonly ILog fLog = LogManager.GetLogger("Log4NetConfiguration");

        public static object Config { get; set; }

        public Log4NetFunction()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //
            // TODO: Add constructor logic here
            //
            log4net.Config.XmlConfigurator.Configure();
        }
        public void WriteDebugLogFile(string _error)
        {
            try
            {
                fLog.Debug(_error);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void WriteErrorLogFile(string _error, Exception innerex)
        {
            try
            {
                fLog.Error(_error, innerex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void WriteELogFile(string _error)
        {
            try
            {
                fLog.Error(_error);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void WriteInfoLogFile(string msg)
        {
            try
            {
                
                fLog.Info(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
