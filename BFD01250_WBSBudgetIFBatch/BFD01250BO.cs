﻿using System;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;
using th.co.toyota.stm.fas.common.FTP;

namespace th.co.toyota.stm.fas
{
    class BFD01250BO
    {
        private string strBatchName = "Receiving WBS Budget Interface Batch";
        private string strBatchID = "BFD01250";
        private string strMailBodyPath = string.Empty;

        private Log4NetFunction log4Net = new Log4NetFunction();
        private MailSending mail = new MailSending();

        public BatchLoggingData BatchQModel = null;

        public BatchLogging bLogging = null;
        private BFD01250DAO _dao = null;

        private DetailLogData LogModel = new DetailLogData();
        ReceiveFileClass _cls = null;
        public BFD01250BO()
        {
             bLogging = new BatchLogging();

            // Set Batch Q information
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchName"])))
            {
                strBatchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
            }
            BatchQModel = new BatchLoggingData();
            BatchQModel.BatchID = strBatchID;
            BatchQModel.Status = eLogStatus.Queue;
            BatchQModel.Description = "Run by Schedule";
            BatchQModel.ReqBy = "System";
            BatchQModel.BatchName = strBatchName;
            if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["BatchID"])))
            {
                BatchQModel.BatchID  = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            }            
        }

        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        public void Processing()
        {
            try
            {
                //open connection
                bool _success = false;

                _dao = new BFD01250DAO();
                LogModel.AppID = BatchQModel.AppID; 

                StartBatchQ();
                
                //_success = this.ReceivingFileFromFTP(ConfigFileName); // receiving file from ftp
                //if (!_success)
                //{
                //    // Cannot receive file from FTP 
                //    //add to email list
                //    mail.AppID = string.Format("{0}", BatchQModel.AppID );
                //    return;
                //}

                _success = this.ReceivingProcess(); // Common Receiving file on local path
                if (!_success)
                {
                    //add to email list
                    mail.AppID = string.Format("{0}", BatchQModel.AppID); //assign when send email
                    return;
                }


                BatchQModel.ProcessStatus =  this.BusinessProcess();
                if (BatchQModel.ProcessStatus == eLogLevel.Error)
                {
                    mail.AppID = string.Format("{0}", BatchQModel.AppID); //assign when send email
                }


                //2019.12.03
                //2019.12.03
                if (_cls != null)
                    _cls.MoveFile(BatchQModel.ProcessStatus);

            }
            catch (Exception ex) //cannot connect
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                if ((ex is SqlException))
                {
                    return;
                }

                try
                {
                    LogModel.Status = eLogStatus.Processing;
                    LogModel.Level = eLogLevel.Error;
                    LogModel.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    bLogging.InsertDetailLog(LogModel);
                }
                catch (Exception ex1)
                {
                    //log4net
                    log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex1.Message, ex1);
                }

                
            }
            finally
            {
                try
                {
                    // Move file
                    SetBatchQEnd();
                    //if mail list > 0 send email
                    //Incomplete

                    _dao.Close();//Close connection

                    mail.SendEmailToAdministratorInSystemConfig(this.strBatchName);
                }
                catch (Exception ex)
                {
                    //log4net
                    log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                }
            }

        }

        #region "BatchQ Method"

        public int InsertBatchQ(BatchLoggingData _batchModel)
        {
            int _AppID;
            _AppID = bLogging.InsertBatchQ(_batchModel);

            log4Net.WriteInfoLogFile(string.Format("App ID = {0}", _AppID));

            return _AppID;
        }
        private void StartBatchQ()
        {
            try
            {
                bLogging.StartBatchQ(BatchQModel);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private void SetBatchQEnd()
        {
            try
            {
                var _message = "";
                if (BatchQModel.ProcessStatus == eLogLevel.Information)
                    _message = "Successfully";
                else if (BatchQModel.ProcessStatus == eLogLevel.Error)
                    _message = "Error";

                log4Net.WriteInfoLogFile(string.Format("Batch Result of App ID = {0} is {1} ", 
                        BatchQModel.AppID, _message));
                bLogging.SetBatchQEnd(BatchQModel);
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        #endregion

        #region "Receiving Method"
         private bool ReceivingProcess()
        {
           

            try
            {
                string _cfgLayoutSection = ConfigurationManager.GetAppSetting("LayoutSection"); //follow your config
                if (_cfgLayoutSection == null)
                {
                    // message
                    throw (new BatchLoggingException(CommonMessage.E_CONFIG, "LayoutSection"));
                }

                _cls = new ReceiveFileClass(ConfigFileName, _cfgLayoutSection);
                _cls.Execute();
              
                foreach (MessageResult _rs in _cls.ProcessResultList)
                {
                    LogModel.Status = eLogStatus.Processing;
                    LogModel.Level = _rs.MessageResultType;
                    LogModel.Description = _rs.Message;
                    bLogging.InsertDetailLog(LogModel);
                }

            }
            catch (BatchLoggingException bLoggingex)
            {
                LogModel.Status = eLogStatus.Processing;
                if (bLoggingex.arErrorList == null)
                {
                    LogModel.Description = bLoggingex.Message;
                    LogModel.Level = eLogLevel.Error;
                    bLogging.InsertDetailLog(LogModel);
                }
                else
                {
                    foreach (MessageResult msg in bLoggingex.arErrorList)
                    {
                        LogModel.Description = msg.Message;
                        LogModel.Level = msg.MessageResultType;
                        bLogging.InsertDetailLog(LogModel);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
                throw (ex);
            }
            return true;
        }
        #endregion

        #region "FTP Receiving Method"
        // private bool ReceivingFileFromFTP(string cfgFileName)
        //{
        //    try
        //    {
        //        string cfgLayoutSectionFTP = System.Configuration.ConfigurationManager.AppSettings["LayoutSectionFTP"]; //follow your config

        //        if (string.IsNullOrEmpty(cfgLayoutSectionFTP))
        //        {
        //            throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutOracalFTP")));
        //        }
        //        FTP ftp = new FTP(cfgFileName, cfgLayoutSectionFTP, "LayoutOracalFTP");
        //        ftp.DownloadFileFromFTP();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name.ToString() + " : " + ex.Message, ex);
        //        throw (ex);
               
        //    }
        //    return true;
        //}
        #endregion
        #region "BusinessProcess Method"

        /** Allow to Edit */
        private eLogLevel BusinessProcess()
        {
            
            var _rs = _dao.Execute(BatchQModel);
            return _rs;
            //if (_rs == eLogLevel.Error)
            //{
            //    // Read file
            //    if (!(string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["MailBodyPath"])))
            //    {
            //        strMailBodyPath = System.Configuration.ConfigurationManager.AppSettings["MailBodyPath"];
            //         string _MailBody = string.Empty;
            //         _MailBody = ReadConfigEmail(strMailBodyPath);
            //         _valuReturn = _dataAccess.SendNotifyEmail(BatchQModel.AppID, _MailBody, BatchQModel.ReqBy);
            //    }
               
            //}
            //return _valuReturn;
        }

        //private string ReadConfigEmail(string fileConfigPath)
        //{
        //    string _strReturn = string.Empty;
        //    const int iBufferSize = 128;
        //    using (System.IO.FileStream _fileStream = System.IO.File.OpenRead(fileConfigPath))
        //    using (System.IO.StreamReader _rd = new System.IO.StreamReader(_fileStream, System.Text.Encoding.ASCII, true, iBufferSize))
        //    {
        //        string _line = string.Empty;
        //        while ((_line = _rd.ReadLine()) != null)
        //        {
        //            if(! _line.Equals (string.Empty))
        //            {
        //                _strReturn = _strReturn + _line + " ";
        //            }                   
        //        }
        //        _rd.Close();
        //    }

        //    return _strReturn;
        //}

        /** End Allow */
        #endregion
    }
}
