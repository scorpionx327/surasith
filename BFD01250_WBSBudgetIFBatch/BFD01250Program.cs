﻿using System;
using System.Runtime.InteropServices;
using th.co.toyota.stm.fas.common;


namespace th.co.toyota.stm.fas
{
    class BFD01250Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleTitle(string _title);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main(string[] args)
        {
            string _modName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            string _procName = System.IO.Path.GetFileNameWithoutExtension(_modName);
            System.Diagnostics.Process[] _appProc = System.Diagnostics.Process.GetProcessesByName(_procName);

        

            if (_appProc.Length > 1)
            {
                Console.WriteLine("There is another instances running. This instance will be closed automatically.");
               return;
            }
          
            /************************************   Check Multiple Running    ****************************************/
            string _cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
            if (!System.IO.File.Exists(_cfgFileName))
            {
                // message
                Console.WriteLine("Not found Configuration File : {0}", _cfgFileName);
             return;
            }

   
            Log4NetFunction _log4net = new Log4NetFunction();
            if (!BatchLogging.IsConnected())
            {
                _log4net.WriteErrorLogFile(MessageCommonBatch.E_CANNOT_CONNECT_DB);
                return;
            }

            try
            {
                var _cls = new BFD01250BO();
                _cls.BatchQModel.AppID = _cls.InsertBatchQ(_cls.BatchQModel);// Insert Batch queue to Batch Queue table
                SetConsoleTitle(_cls.BatchQModel.BatchID);
                _cls.Processing();
            }
            catch (Exception ex)
            {
                _log4net.WriteErrorLogFile(ex.Message);
                return;
            }
        }
    }
}
