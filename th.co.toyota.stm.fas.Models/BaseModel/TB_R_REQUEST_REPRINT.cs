//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace th.co.toyota.stm.fas.Models.BaseModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_R_REQUEST_REPRINT
    {
        public string DOC_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string COST_CODE { get; set; }
        public string DELETE_FLAG { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public System.DateTime UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public Nullable<int> PRINT_COUNT { get; set; }
    }
}
