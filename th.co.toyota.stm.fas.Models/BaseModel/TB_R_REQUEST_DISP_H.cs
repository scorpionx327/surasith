//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace th.co.toyota.stm.fas.Models.BaseModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_R_REQUEST_DISP_H
    {
        public string DOC_NO { get; set; }
        public string DISP_TYPE { get; set; }
        public string LOSS_COUNTER_MEASURE { get; set; }
        public string USER_MISTAKE_FLAG { get; set; }
        public decimal COMPENSATION { get; set; }
        public string DISP_MASS_REASON { get; set; }
        public string DISP_MASS_ATTACH_DOC { get; set; }
        public string MINUTE_OF_BIDDING { get; set; }
        public string BIDDING_ATTACH_DOC { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public Nullable<System.DateTime> UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
    }
}
