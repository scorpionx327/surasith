﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.BaseModel
{
    public class TB_T_STOCK_EXPORT
    {
        public string COMPANY { get; set; }
        public decimal APL_ID { get; set; }
        public string BATCH_ID { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
    }
}
