﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.Shared
{
    public class SystemConfigurationModels
    {
        public string CATEGORY { get; set; }
        public string SUB_CATEGORY { get; set; }
        public string CODE { get; set; }
        public string VALUE { get; set; }
        public string REMARKS { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }

        public IEnumerable<SelectListItem> Select(Func<object, SelectListItem> p)
        {
            throw new NotImplementedException();
        }
    }
}