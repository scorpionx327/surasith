﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models
{
    public class EmployeeAutoCompleteModels
    {
        public string POST_CODE { get; set; }
        public string POST_NAME { get; set; }
        public string JOB_CODE { get; set; }
        public string JOB_NAME { get; set; }
    }
}
