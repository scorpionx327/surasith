﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models
{
    public class CDateTime
    {
        private string _text { get; set; }
        private string _format { get; set; } = "dd-MMM-yyyy";
        private string _cultureInfo { get; set; } = "en-En";

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        public bool ValidFormat
        {
            get
            {
                DateTime dateValue;
                return DateTime.TryParseExact(_text, _format, new CultureInfo(_cultureInfo), DateTimeStyles.None, out dateValue);
            }
        }
        public string CultureStr
        {
            get { return _cultureInfo; }
            set { _cultureInfo = value;  }
        }
        public bool IsNull
        {
            get { return string.IsNullOrEmpty(_text); }
        }
        public string DateFormat
        {
            get
            {
                return _format;
            }
        }
        //    public DateTime? Value
        //    {
        //        get
        //        {
        //            DateTime dateValue;
        //            if (DateTime.TryParseExact(_text, _format, new CultureInfo(_cultureInfo), DateTimeStyles.None, out dateValue))
        //            {
        //                return DateTime.ParseExact(_text, _format, new CultureInfo(_cultureInfo));
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //    }
    }
}
