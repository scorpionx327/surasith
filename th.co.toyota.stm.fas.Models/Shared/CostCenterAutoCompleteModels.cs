﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models
{
    public class CostCenterAutoCompleteModels
    {
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        
    }
}
