﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Shared
{
    public class RequestHeaderModels
    {
        public string DOC_NO { get; set; }
        public DateTime REQUEST_DATE { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }        
        public string EMP_LASTNAME { get; set; }
        public string COST_CODE { get; set; }
        public string POS_CODE { get; set; }
        public string POS_NAME { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }    
        public string SECT_CODE { get; set; }
        public string SECT_NAME { get; set; }
        public string PHONE_NO { get; set; }
        public string STATUS { get; set; }    
        public DateTime CREATE_DATE { get; set; }        
        public string CREATE_BY { get; set; }
        public Nullable<DateTime> UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
    }

    public class RequestPageInfoModels
    {
        public string FunctionID { get; set; }
        public string FunctionTitle { get; set; }
        
    }
}
