﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models
{
    public class OrganizationAutoCompleteModels
    {
        public string ORG_CODE { get; set; }
        public string CMP_CODE { get; set; }
        public string CMP_ABBR { get; set; }
        public string DIV_CODE { get; set; }
        public string SUB_DIV_CODE { get; set; }
        public string DEPT_CODE { get; set; }
        public string SECTION_CODE { get; set; }
        public string LINE_CODE { get; set; }
        public string EFF_FROM_DT { get; set; }
        public string EFF_TO_DT { get; set; }
        public string CMP_NAME { get; set; }
        public string DIV_NAME { get; set; }
        public string SUB_DIV_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string SECTION_NAME { get; set; }
        public string LINE_NAME { get; set; }
        public string ORG_LEVEL { get; set; }
        public string COST_CODE { get; set; }
        public string RA_COST_CODE { get; set; }
        public string RA_FLAG { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
    }
}
