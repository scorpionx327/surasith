﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace th.co.toyota.stm.fas.Models
{
    public class FixAssetsAutoCompleteModels
    {
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_NAME { get; set; }
        public string INVEN_NO { get; set; }

    }
}