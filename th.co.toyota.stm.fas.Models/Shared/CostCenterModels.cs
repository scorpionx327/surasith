﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models
{
    public class CostCenterModels
    {
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        public string PLANT_CD { get; set; }
        public string PLANT_NAME { get; set; }
        public string MAP_PATH { get; set; }
        public string ENABLE_FLAG { get; set; }
        public DateTime EFFECTIVE_FROM { get; set; }
        public DateTime EFFECTIVE_TO { get; set; }
        public string STATUS { get; set; }
        public string PARENT_FLAG { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
    }
}
