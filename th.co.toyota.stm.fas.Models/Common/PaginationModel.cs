﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class PaginationModel
    {
        public string OrderColIndex { get; set; }
        public string OrderType { get; set; }
        public int CurrentPage { get; set; }
        public int ItemPerPage { get; set; }
        public int Totalitem { get; set; }
        public int TotalPage
        {
            get
            {
                if (ItemPerPage > 0 && Totalitem > 0)
                {
                    return (int)Math.Ceiling((double)Totalitem / (double)ItemPerPage);
                }
                else
                {
                    return 1;
                }
            }
        }
    }
}
