﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class BaseJsonResult
    {
        public object ObjectResult { get; set; }
        public List<MessageBlockData> Messages { get; set; }
        public string DivMessages { get; set; }
        public bool IsThereError
        {
            get
            {
                if (this.Messages != null && this.Messages.Where(m => m.Type == MessageStatus.ERROR).Count() > 0)
                    return true;
                else return false;
            }
        }
        public void SetMessage(params SystemMessageModel[] SystemMessages)
        {
            if (SystemMessages == null) return;
            this.Messages = new List<MessageBlockData>();
            foreach (var m in SystemMessages.GroupBy(m=>m.STATUS).Select(m=>m.Key))
            {
                MessageBlockData msg = new MessageBlockData();
                msg.Type = m;
                msg.Messages = new List<string>();

                foreach (var me in SystemMessages.Where(a => a.STATUS == m))
                {
                    //msg.Messages.Add(string.Format("{0}: {1}", me.ID, me.VALUE));
                    msg.Messages.Add(me.VALUE);
                }
                this.Messages.Add(msg);
            }
        }
        public void AddMessage(SystemMessageModel msg)
        {
            if (this.Messages == null) this.Messages = new List<MessageBlockData>();

            if (this.Messages.Where(m => m.Type == msg.STATUS).Count() > 0)
            {
                this.Messages.Where(m => m.Type == msg.STATUS).First()
                    .Messages.Add(string.Format("{0}: {1}", msg.ID, msg.VALUE));
            }
            else
            {
                MessageBlockData m = new MessageBlockData();
                m.Type = msg.STATUS;
                m.Messages.Add(string.Format("{0}: {1}", msg.ID, msg.VALUE));
                this.Messages.Add(m);
            }
        }
    }

    public class MessageBlockData
    {
        public MessageStatus Type { get; set; }
        public List<string> Messages { get; set; }
    }

    public class ControlMessageData
    {
        public string ControlId { get; set; }
        public string Message { get; set; }
        public JsonResultTypes Type { get; set; }
    }

    public enum JsonResultTypes
    {
        None = 0, Info = 1, Success = 2, Waring = 3, Error = 4
    }
}
