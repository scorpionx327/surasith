﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class DepreciationModel
    {
        public string COMPANY { get; set; }
        public string ASSET_CLASS { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public string STICKER_TYPE { get; set; }
        public string STICKER_SIZE { get; set; }
        public string INVENTORY_INDICATOR { get; set; }
        public string DPRE_KEY_01 { get; set; }
        public int USEFUL_LIFE_01 { get; set; }
        public string USEFUL_LIFE_TYPE_01 { get; set; }
        public int SCRAP_VALUE_01 { get; set; }
        public string DPRE_KEY_15 { get; set; }
        public int USEFUL_LIFE_15 { get; set; }
        public string USEFUL_LIFE_TYPE_15 { get; set; }
        public int SCRAP_VALUE_15 { get; set; }
        public string DPRE_KEY_31 { get; set; }
        public int USEFUL_LIFE_31 { get; set; }
        public string USEFUL_LIFE_TYPE_31 { get; set; }
        public int SCRAP_VALUE_31 { get; set; }
        public string DPRE_KEY_41 { get; set; }
        public int USEFUL_LIFE_41 { get; set; }
        public string USEFUL_LIFE_TYPE_41 { get; set; }
        public int SCRAP_VALUE_41 { get; set; }
        public string DPRE_KEY_81 { get; set; }
        public int USEFUL_LIFE_81 { get; set; }
        public string USEFUL_LIFE_TYPE_81 { get; set; }
        public int SCRAP_VALUE_81 { get; set; }
        public string DPRE_KEY_91 { get; set; }
        public int USEFUL_LIFE_91 { get; set; }
        public string USEFUL_LIFE_TYPE_91 { get; set; }
        public int SCRAP_VALUE_91 { get; set; }
    }
}
