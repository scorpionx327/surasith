﻿using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public interface IUserData : IPrincipal
    {
        bool IsSystemAdmin { get; set; }
        bool IsAECUser { get; set; }
        string DisplayName { get; }
        UserInfo User { get; set; }
    }
    public class UserPrinciple : IUserData
    {
        public UserPrinciple() {
            this.CompanyList = new List<string>();
        }
       public UserInfo User { get; set; }
        public AccessControlList Acl { get; set; }

        public bool IsInRole(string role)
        {
            return true; //Wait to Implement
        }

        public string DisplayName
        {
            get
            {
                if (User != null) return string.Format("{0} {1} {2}", User.Title, User.FirstName, User.LastName);
                else return string.Empty;
            }
        }

        public IIdentity Identity { get; private set; }

        public bool IsAECUser { get; set; }

        public bool IsAECManager { get; set; }


        public bool IsSystemAdmin { get; set; }

        public string DefaultCompany {
            get {
                if (this.CompanyList == null ||  this.CompanyList.Count == 0 || this.CompanyList.Count > 1)
                    return string.Empty;

                return this.CompanyList[0];
            }
        }

        public string TFASTEmployeeNo { get; set; }
        public List<string> CompanyList { get; set; }

        public string Company { get; set; }
    }
  }
