﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{

    public enum SYSTEM_CATEGORY
    {
        COMMON = 0,
        FAS_TYPE,
        FAS_STATUS,
        SYSTEM_EMAIL,
        SYSTEM_CONFIG,
        RESPONSIBILITY,
        APPROVE_FLOW,
        SYTEM_CONFIG,
        MINOR_CATEGORY,
        MAINSUB,
        ASSET_CLASS,
        MACHINE_LICENSE,
        UOM,
        BASE_UOM,
        LOCATION,
        BOI_NO,
        FINAL_ASSET_CLASS,
        WBS_BUDGET,
        WBS_PROJECT,
        COMPANY,
        ROLE_MAPING,
        USER_PROFILE,
        SYS_CONFIRM,
        REQUEST,
        STOCK_TAKE,
        APPRV_FLOW_MASTER
    }
    public enum SYSTEM_SUB_CATEGORY
    {
        ASSET_LOCATION,
        SUB_TYPE,
        ASSET_STATUS,
        SUBJECT,
        BODY,
        REQUEST_TYPE,
        USERROLE_CODE,
        ROLE_CODE,
        APP_DIVISION,
        CONDITION,
        OPERATOR,
        DISPOSAL_PURPOSE,
        DISPOSAL_REASON,
        ASSET_CATEGORY,
        ASSET_IMAGE,
        SPEC_DIV,
        USER_MANUAL,
        EXCEPT_CHECK_PHOTO,
        COMPANY,
        MINOR_CATEGORY,
        ASSET_CLASS,
        WBS_BUDGET,
        WBS_PROJECT,
        MAINSUB,
        BASE_UOM,
        ROLE_PRIORITY,
        JOB, POSITION,
        PLATE_TYPE,
        BARCODE_SIZE,
        YN,
        REQUEST_FLOW_TYPE,
        TRANS_MAIN_TYPE,
        RETIREMENT_TYPE,
        INVENT_CHECK

    }

    public class SystemModel
    {
        public string CATEGORY { get; set; }
        public string SUB_CATEGORY { get; set; }
        public string CODE { get; set; }
        public string VALUE { get; set; }
        public string REMARKS { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DATE { get; set; }
        public string ROLE_TYPE { get; set; }
    }
}
