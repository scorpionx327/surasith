﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class WBSModel
    {
        public string COMPANY_CODE { get; set; }
        public string WBS_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string PROJECT_DEFINITION { get; set; }
        public string BUDGET_TYPE { get; set; }
    }
}
