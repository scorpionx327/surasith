﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public enum MessageStatus
    {
        NONE = 0,
        INFO = 1,
        WARNING = 2,
        ERROR = 3,
        CONFIRM = 4,
        NOTFOUND = 5
    }

    public class MessageModel
    {
        public string MESSAGE_CODE { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string MESSAGE_TEXT { get; set; }
    }

    public class SystemMessageModel
    {
        public string ID { get; set; }
        public string VALUE { get; set; }
        public MessageStatus STATUS { get; set; }
    }
}
