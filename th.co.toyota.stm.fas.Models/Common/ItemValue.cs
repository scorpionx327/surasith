﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    /// <summary>
    /// DO for item data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class ItemValue<T>
    {
        private T objValue;
        public object Value { get { return this.objValue; } set { this.objValue = (T)value; } }
        private string strDisplay;
        public string Display { get { return this.strDisplay; } set { this.strDisplay = value; } }
        private string hiddendata;
        public string Description { get { return this.hiddendata; } set { this.hiddendata = value; } }
        public bool Disabled { get; set; }
        public bool Selected { get; set; }
    }
}
