﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class BatchModel
    {

    }

    public class BatchProcess
    {
        public static string TestBatch = @"E:\_Fujitsu Test program\CommonBatch\CommonBatch2.exe";
        public static string BatchProcessName = "STMAssetCMBatch";
    }

    public class BatchProcessList
    {
        public static int getUserAppId(string UserId)
        {
            var datas = batches.Where(m => m.UserId == UserId).First();
            return datas.AppId;
        }
        public static bool isThereUserBatchProcessing(string UserID)
        {
            if (batches.Where(m => m.UserId == UserID).Count() > 0) return true;
            else return false;
        }
        public static List<BatchUserModel> batches { get; set; }
    }

    public class BatchUserModel
    {
        public int AppId { get; set; }
        public string UserId { get; set; }
    }

    public class StartBatchModel
    {
        public string ModuleID { get; set; }
        public decimal? AppID { get; set; }
        public string BatchID { get; set; }
        public string BatchName { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }

        public BatchParameterModel[] Parameters { get; set; }

    }
    public class BatchParameterModel
    {
        public decimal? AppID { get; set; }
        public int? ParamSeq { get; set; }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
    }

    public class BatchMessageStatusData
    {
        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
    }

    public enum BATCH_STATUS
    {
        /// <summary>
        /// Waiting for working
        /// </summary>
        Q = 0,
        /// <summary>
        /// Processing
        /// </summary>
        P = 1,
        /// <summary>
        /// End with success
        /// </summary>
        S = 2,
        /// <summary>
        /// End with error
        /// </summary>
        E = 3,
        /// <summary>
        /// Cancel batch by user
        /// </summary>
        C = 4
    }
}