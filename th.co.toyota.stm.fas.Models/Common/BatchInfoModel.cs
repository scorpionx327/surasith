﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class BatchInfoModel
    {
        public string BATCH_ID { get; set; }
        public string BATCH_NAME { get; set; }
        public string EXEC_PATH { get; set; }
        public Nullable<int> PARAM_SEQ { get; set; }
        public string PARAM_NAME { get; set; }
        public string PARAM_TYPE { get; set; }
        public string UPLOAD_PATH { get; set; }
    }

}
