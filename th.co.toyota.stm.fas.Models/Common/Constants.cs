﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class Constants
    {
        #region Session
        public const string SESSION_USER_PROFILE = "UserInfo";
        #endregion

        #region Message_Code
        public struct MESSAGE
        {
            public const string MCOM0001AERR = "MCOM0001AERR";
            public const string MCOM0002AERR = "MCOM0002AERR";
            public const string MCOM0003AERR = "MCOM0003AERR";
            public const string MCOM0004AERR = "MCOM0004AERR";
            public const string MCOM0005BERR = "MCOM0005BERR";
            public const string MCOM0006BWRN = "MCOM0006BWRN";
            public const string MCOM0007ACFM = "MCOM0007ACFM";
            public const string MCOM0008AERR = "MCOM0008AERR";
            public const string MCOM0009AERR = "MCOM0009AERR";
            public const string MCOM0009BWRN = "MCOM0009BWRN";
            public const string MCOM0010AWRN = "MCOM0010AWRN";
            public const string MCOM0011AERR = "MCOM0011AERR";
            public const string MCOM0012AERR = "MCOM0012AERR";
            public const string MCOM0013AERR = "MCOM0013AERR";
            public const string MCOM0014AERR = "MCOM0014AERR";
            public const string MCOM0015AERR = "MCOM0015AERR";
            public const string MCOM0016AINF = "MCOM0016AINF";
            public const string MCOM0017AERR = "MCOM0017AERR";
            public const string MCOM0018CFM = "MCOM0018CFM";
            public const string MCOM0019AERR = "MCOM0019AERR";
            public const string MCOM0020AWRN = "MCOM0020AWRN";
            public const string MFAS0401AERR = "MFAS0401AERR";
            public const string MFAS0701AERR = "MFAS0701AERR";
            public const string MFAS0702AERR = "MFAS0702AERR";
            public const string MFAS1101CFM = "MFAS1101CFM";
            public const string MFAS1102BINF = "MFAS1102BINF";
            public const string MFAS1102CINF = "MFAS1102CINF";
            public const string MFAS1103AERR = "MFAS1103AERR";
            public const string MFAS1104AERR = "MFAS1104AERR";
            public const string MFAS1105BWRN = "MFAS1105BWRN";
            public const string MFAS1201AERR = "MFAS1201AERR";
            public const string MFAS1202AERR = "MFAS1202AERR";
            public const string MFAS1203AINF = "MFAS1203AINF";
            public const string MFAS1204AERR = "MFAS1204AERR";
            public const string MFAS1601AERR = "MFAS1601AERR";
            public const string MFAS1602AERR = "MFAS1602AERR";
            public const string MFAS1603AERR = "MFAS1603AERR";
            public const string MFAS1604ACFM = "MFAS1604ACFM";
            public const string MFAS1901AERR = "MFAS1901AERR";
            public const string MFAS1902AERR = "MFAS1902AERR";
            public const string MFAS1903AERR = "MFAS1903AERR";
            public const string MSTD0000ACFM = "MSTD0000ACFM";
            public const string MSTD0000AERR = "MSTD0000AERR";
            public const string MSTD0000AINF = "MSTD0000AINF";
            public const string MSTD0000AWRN = "MSTD0000AWRN";
            public const string MSTD0001ACFM = "MSTD0001ACFM";
            public const string MSTD0002ACFM = "MSTD0002ACFM";
            public const string MSTD0003ACFM = "MSTD0003ACFM";
            public const string MSTD0004ACFM = "MSTD0004ACFM";
            public const string MSTD0005ACFM = "MSTD0005ACFM";
            public const string MSTD0006ACFM = "MSTD0006ACFM";
            public const string MSTD0007ACFM = "MSTD0007ACFM";         
            public const string MSTD0008AERR = "MSTD0008AERR";
            public const string MSTD0009AERR = "MSTD0009AERR";
            public const string MSTD0010AERR = "MSTD0010AERR";
            public const string MSTD0011AERR = "MSTD0011AERR";
            public const string MSTD0012AERR = "MSTD0012AERR";
            public const string MSTD0013AERR = "MSTD0013AERR";
            public const string MSTD0014AERR = "MSTD0014AERR";
            public const string MSTD0015AERR = "MSTD0015AERR";
            public const string MSTD0016AERR = "MSTD0016AERR";
            public const string MSTD0017AERR = "MSTD0017AERR";
            public const string MSTD0018AERR = "MSTD0018AERR";
            public const string MSTD0019AERR = "MSTD0019AERR";
            public const string MSTD0020AERR = "MSTD0020AERR";
            public const string MSTD0021AERR = "MSTD0021AERR";
            public const string MSTD0022AERR = "MSTD0022AERR";
            public const string MSTD0023AERR = "MSTD0023AERR";
            public const string MSTD0024AERR = "MSTD0024AERR";
            public const string MSTD0025AERR = "MSTD0025AERR";
            public const string MSTD0026AERR = "MSTD0026AERR";
            public const string MSTD0027AERR = "MSTD0027AERR";
            public const string MSTD0028AERR = "MSTD0028AERR";
            public const string MSTD0029AERR = "MSTD0029AERR";
            public const string MSTD0030AERR = "MSTD0030AERR";
            public const string MSTD0031AERR = "MSTD0031AERR";
            public const string MSTD0032AERR = "MSTD0032AERR";
            public const string MSTD0033AERR = "MSTD0033AERR";
            public const string MSTD0034AERR = "MSTD0034AERR";
            public const string MSTD0035AERR = "MSTD0035AERR";
            public const string MSTD0036AERR = "MSTD0036AERR";
            public const string MSTD0037AERR = "MSTD0037AERR";
            public const string MSTD0038AERR = "MSTD0038AERR";
            public const string MSTD0039AERR = "MSTD0039AERR";
            public const string MSTD0040AERR = "MSTD0040AERR";
            public const string MSTD0041AERR = "MSTD0041AERR";
            public const string MSTD0042AERR = "MSTD0042AERR";
            public const string MSTD0043AERR = "MSTD0043AERR";
            public const string MSTD0044AERR = "MSTD0044AERR";
            public const string MSTD0045AERR = "MSTD0045AERR";
            public const string MSTD0046AERR = "MSTD0046AERR";
            public const string MSTD0047AERR = "MSTD0047AERR";
            public const string MSTD0048AERR = "MSTD0048AERR";
            public const string MSTD0049AERR = "MSTD0049AERR";
            public const string MSTD0050AERR = "MSTD0050AERR";
            public const string MSTD0051AERR = "MSTD0051AERR";
            public const string MSTD0052AERR = "MSTD0052AERR";
            public const string MSTD0053AERR = "MSTD0053AERR";
            public const string MSTD0054AERR = "MSTD0054AERR";
            public const string MSTD0055AERR = "MSTD0055AERR";
            public const string MSTD0056AERR = "MSTD0056AERR";
            public const string MSTD0057AERR = "MSTD0057AERR";
            public const string MSTD0058AERR = "MSTD0058AERR";
            public const string MSTD0059AERR = "MSTD0059AERR";
            public const string MSTD0060AERR = "MSTD0060AERR";
            public const string MSTD0061AERR = "MSTD0061AERR";
            public const string MSTD0062AERR = "MSTD0062AERR";
            public const string MSTD0063AERR = "MSTD0063AERR";
            public const string MSTD0064AERR = "MSTD0064AERR";
            public const string MSTD0065AERR = "MSTD0065AERR";
            public const string MSTD0066AERR = "MSTD0066AERR";
            public const string MSTD0067AERR = "MSTD0067AERR";
            public const string MSTD0068AERR = "MSTD0068AERR";
            public const string MSTD0069AERR = "MSTD0069AERR";
            public const string MSTD0070AERR = "MSTD0070AERR";
            public const string MSTD0071AERR = "MSTD0071AERR";
            public const string MSTD0072AERR = "MSTD0072AERR";
            public const string MSTD0073AERR = "MSTD0073AERR";
            public const string MSTD0074AERR = "MSTD0074AERR";
            public const string MSTD0075AERR = "MSTD0075AERR";
            public const string MSTD0076AINF = "MSTD0076AINF";
            public const string MSTD0077AINF = "MSTD0077AINF";
            public const string MSTD0078AINF = "MSTD0078AINF";
            public const string MSTD0079AINF = "MSTD0079AINF";
            public const string MSTD0080AINF = "MSTD0080AINF";
            public const string MSTD0081AINF = "MSTD0081AINF";
            public const string MSTD0082AINF = "MSTD0082AINF";
            public const string MSTD0083AINF = "MSTD0083AINF";
            public const string MSTD0084AINF = "MSTD0084AINF";
            public const string MSTD0085AINF = "MSTD0085AINF";
            public const string MSTD0086AINF = "MSTD0086AINF";
            public const string MSTD0087AINF = "MSTD0087AINF";
            public const string MSTD0088AINF = "MSTD0088AINF";
            public const string MSTD0089AINF = "MSTD0089AINF";
            public const string MSTD0090AINF = "MSTD0090AINF";
            public const string MSTD0091AINF = "MSTD0091AINF";
            public const string MSTD0092AINF = "MSTD0092AINF";
            public const string MSTD0093AINF = "MSTD0093AINF";
            public const string MSTD0094AINF = "MSTD0094AINF";
            public const string MSTD0095AINF = "MSTD0095AINF";
            public const string MSTD0096AINF = "MSTD0096AINF";
            public const string MSTD0097AINF = "MSTD0097AINF";
            public const string MSTD0098AINF = "MSTD0098AINF";
            public const string MSTD0099AINF = "MSTD0099AINF";
            public const string MSTD0100AINF = "MSTD0100AINF";
            public const string MSTD0101AINF = "MSTD0101AINF";
            public const string MSTD0102AINF = "MSTD0102AINF";
            public const string MSTD0103AINF = "MSTD0103AINF";
            public const string MSTD0104AINF = "MSTD0104AINF";
            public const string MSTD0105AINF = "MSTD0105AINF";
            public const string MSTD0106AINF = "MSTD0106AINF";
            public const string MSTD0107AINF = "MSTD0107AINF";
            public const string MSTD0108AINF = "MSTD0108AINF";
            public const string MSTD0109AINF = "MSTD0109AINF";
            public const string MSTD0110AINF = "MSTD0110AINF";
            public const string MSTD0111AINF = "MSTD0111AINF";
            public const string MSTD0112AINF = "MSTD0112AINF";
            public const string MSTD0113AINF = "MSTD0113AINF";
            public const string MSTD0114ACFM = "MSTD0114ACFM";
            public const string MSTD0114AERR = "MSTD0114AERR";
            public const string MSTD0115AERR = "MSTD0115AERR";
            public const string MSTD0115AINF = "MSTD0115AINF";
            public const string MSTD0116AERR = "MSTD0116AERR";
            public const string MSTD0117AERR = "MSTD0117AERR";
            public const string MSTD0118AERR = "MSTD0118AERR";
            public const string MSTD0119AERR = "MSTD0119AERR";
            public const string MSTD0120AERR = "MSTD0120AERR";
            public const string MSTD0121AERR = "MSTD0121AERR";
            public const string MSTD0122AERR = "MSTD0122AERR";
            public const string MSTD0123SERR = "MSTD0123SERR";
            public const string MSTD0124ACFM = "MSTD0124ACFM";
            public const string MSTD1000AERR = "MSTD1000AERR";
            public const string MSTD1001AERR = "MSTD1001AERR";
            public const string MSTD1002AERR = "MSTD1002AERR";
            public const string MSTD1003AERR = "MSTD1003AERR";
            public const string MSTD1004AERR = "MSTD1004AERR";
            public const string MSTD1005AERR = "MSTD1005AERR";
            public const string MSTD1006AERR = "MSTD1006AERR";
            public const string MSTD1007AERR = "MSTD1007AERR";
            public const string MSTD1008AERR = "MSTD1008AERR";
            public const string MSTD1009AERR = "MSTD1009AERR";
            public const string MSTD1010AERR = "MSTD1010AERR";
            public const string MSTD1011AERR = "MSTD1011AERR";
            public const string MSTD1012AERR = "MSTD1012AERR";
            public const string MSTD1013AERR = "MSTD1013AERR";
            public const string MSTD1014AERR = "MSTD1014AERR";
            public const string MSTD1015AERR = "MSTD1015AERR";
            public const string MSTD1016AERR = "MSTD1016AERR";
            public const string MSTD1017AERR = "MSTD1017AERR";
            public const string MSTD1018AERR = "MSTD1018AERR";
            public const string MSTD1019AERR = "MSTD1019AERR";
            public const string MSTD1020AERR = "MSTD1020AERR";
            public const string MSTD1021AERR = "MSTD1021AERR";
            public const string MSTD1022AERR = "MSTD1022AERR";
            public const string MSTD1023AERR = "MSTD1023AERR";
            public const string MSTD1024AERR = "MSTD1024AERR";
            public const string MSTD1025AERR = "MSTD1025AERR";
            public const string MSTD1026AERR = "MSTD1026AERR";
            public const string MSTD1027AERR = "MSTD1027AERR";
            public const string MSTD1028AERR = "MSTD1028AERR";
            public const string MSTD1029AERR = "MSTD1029AERR";
            public const string MSTD1030AERR = "MSTD1030AERR";
            public const string MSTD1031AERR = "MSTD1031AERR";
            public const string MSTD1032AERR = "MSTD1032AERR";
            public const string MSTD1033AERR = "MSTD1033AERR";
            public const string MSTD1034AERR = "MSTD1034AERR";
            public const string MSTD1035AERR = "MSTD1035AERR";
            public const string MSTD1036AERR = "MSTD1036AERR";
            public const string MSTD1037AERR = "MSTD1037AERR";
            public const string MSTD1038AERR = "MSTD1038AERR";
            public const string MSTD1039AERR = "MSTD1039AERR";
            public const string MSTD1040AERR = "MSTD1040AERR";
            public const string MSTD1041AERR = "MSTD1041AERR";
            public const string MSTD1042AERR = "MSTD1042AERR";
            public const string MSTD1043AERR = "MSTD1043AERR";
            public const string MSTD1044AERR = "MSTD1044AERR";
            public const string MSTD1045AERR = "MSTD1045AERR";
            public const string MSTD1046AERR = "MSTD1046AERR";
            public const string MSTD1047AERR = "MSTD1047AERR";
            public const string MSTD1048AERR = "MSTD1048AERR";
            public const string MSTD1049AERR = "MSTD1049AERR";
            public const string MSTD1050AERR = "MSTD1050AERR";
            public const string MSTD1051AERR = "MSTD1051AERR";
            public const string MSTD1052AERR = "MSTD1052AERR";
            public const string MSTD1053AERR = "MSTD1053AERR";
            public const string MSTD1054AERR = "MSTD1054AERR";
            public const string MSTD1055AERR = "MSTD1055AERR";
            public const string MSTD1056AERR = "MSTD1056AERR";
            public const string MSTD1057AERR = "MSTD1057AERR";
            public const string MSTD1058AERR = "MSTD1058AERR";
            public const string MSTD1059AERR = "MSTD1059AERR";
            public const string MSTD1060AERR = "MSTD1060AERR";
            public const string MSTD1061AERR = "MSTD1061AERR";
            public const string MSTD1062AERR = "MSTD1062AERR";
            public const string MSTD1063AERR = "MSTD1063AERR";
            public const string MSTD1244SERR = "MSTD1244SERR";
            public const string MSTD2000ACFM = "MSTD2000ACFM";
            public const string MSTD2001ACFM = "MSTD2001ACFM";
            public const string MSTD4000AINF = "MSTD4000AINF";
            public const string MSTD4001AINF = "MSTD4001AINF";
            public const string MSTD4002AINF = "MSTD4002AINF";
            public const string MSTD4003AINF = "MSTD4003AINF";
            public const string MSTD4004AINF = "MSTD4004AINF";
            public const string MSTD4005AINF = "MSTD4005AINF";
            public const string MSTD4006AINF = "MSTD4006AINF";
            public const string MSTD7000BINF = "MSTD7000BINF";
            public const string MSTD7001BINF = "MSTD7001BINF";
            public const string MSTD7002BINF = "MSTD7002BINF";
            public const string MSTD7003BINF = "MSTD7003BINF";
            public const string MSTD7004BERR = "MSTD7004BERR";
            public const string MSTD7005BERR = "MSTD7005BERR";
            public const string MSTD7006BERR = "MSTD7006BERR";
            public const string MSTD7007BERR = "MSTD7007BERR";
            public const string MSTD7008BERR = "MSTD7008BERR";
            public const string MSTD7009BERR = "MSTD7009BERR";
            public const string MSTD7010BERR = "MSTD7010BERR";
            public const string MSTD7011BERR = "MSTD7011BERR";
            public const string MSTD7012BERR = "MSTD7012BERR";
            public const string MSTD7013BERR = "MSTD7013BERR";
            public const string MSTD7014BERR = "MSTD7014BERR";
            public const string MSTD7015BERR = "MSTD7015BERR";
            public const string MSTD7016BERR = "MSTD7016BERR";
            public const string MSTD7017BERR = "MSTD7017BERR";
            public const string MSTD7018BERR = "MSTD7018BERR";
            public const string MSTD7019BERR = "MSTD7019BERR";
            public const string MSTD7020BERR = "MSTD7020BERR";
            public const string MSTD7021BERR = "MSTD7021BERR";
            public const string MSTD7022BERR = "MSTD7022BERR";
            public const string MSTD7023BERR = "MSTD7023BERR";
            public const string MSTD7024BERR = "MSTD7024BERR";
            public const string MSTD7025BINF = "MSTD7025BINF";
            public const string MSTD7026BINF = "MSTD7026BINF";
            public const string MSTD7027BERR = "MSTD7027BERR";
            public const string MSTD7028BERR = "MSTD7028BERR";
            public const string MSTD7029BERR = "MSTD7029BERR";
            public const string MSTD7030BCRI = "MSTD7030BCRI";
            public const string MSTD7031BINF = "MSTD7031BINF";
            public const string MSTD7032BINF = "MSTD7032BINF";
            public const string MSTD7033BINF = "MSTD7033BINF";
            public const string MSTD7034BINF = "MSTD7034BINF";
            public const string MSTD7035BINF = "MSTD7035BINF";
            public const string MSTD7036BINF = "MSTD7036BINF";
            public const string MSTD7037BWRN = "MSTD7037BWRN";
            public const string MSTD7038BWRN = "MSTD7038BWRN";
            public const string MSTD7039BERR = "MSTD7039BERR";
            public const string MSTD7039BINF = "MSTD7039BINF";
            public const string MSTD7039BWRN = "MSTD7039BWRN";
            public const string MSTD7040BERR = "MSTD7040BERR";
            public const string MSTD7041BERR = "MSTD7041BERR";
            public const string MSTD7042BERR = "MSTD7042BERR";
            public const string MSTD7043BERR = "MSTD7043BERR";
            public const string MSTD7044BERR = "MSTD7044BERR";
            public const string MSTD7045BERR = "MSTD7045BERR";
            public const string MSTD7046BERR = "MSTD7046BERR";
            public const string MSTD7047BERR = "MSTD7047BERR";
            public const string MSTD7048BERR = "MSTD7048BERR";
            public const string MSTD7049BERR = "MSTD7049BERR";
            public const string MSTD7050BERR = "MSTD7050BERR";
            public const string MSTD7051BERR = "MSTD7051BERR";
            public const string MSTD7051BWRN = "MSTD7051BWRN";
            public const string MSTD7052BERR = "MSTD7052BERR";
            public const string MSTD7053BINF = "MSTD7053BINF";
            public const string MSTD7054BERR = "MSTD7054BERR";
            public const string MSTD7054BINF = "MSTD7054BINF";
            public const string MSTD7055BERR = "MSTD7055BERR";
            public const string MSTD7056BERR = "MSTD7056BERR";
            public const string MSTD7057BINF = "MSTD7057BINF";
            public const string MSTD7058BINF = "MSTD7058BINF";
            public const string MSTD7059BINF = "MSTD7059BINF";
            public const string MSTD7060BINF = "MSTD7060BINF";
            public const string MSTD7061BINF = "MSTD7061BINF";
            public const string MSTD7062BINF = "MSTD7062BINF";
            public const string MSTD7063BERR = "MSTD7063BERR";
            public const string MSTD7064BERR = "MSTD7064BERR";
            public const string MSTD7065BERR = "MSTD7065BERR";
            public const string MSTD7066BINF = "MSTD7066BINF";
            public const string MSTD7067BERR = "MSTD7067BERR";
            public const string MSTD7068BINF = "MSTD7068BINF";
            public const string MSTD7069BERR = "MSTD7069BERR";
            public const string MSTD7070BERR = "MSTD7070BERR";
            public const string MSTD7071BERR = "MSTD7071BERR";
            public const string MSTD7072BERR = "MSTD7072BERR";
            public const string MSTD7073BERR = "MSTD7073BERR";
            public const string MSTD8000SERR = "MSTD8000SERR";
            public const string MSTD8001SERR = "MSTD8001SERR";
            public const string MSTD8002SERR = "MSTD8002SERR";
            public const string MSTD8003SERR = "MSTD8003SERR";
            public const string MSTD8004SERR = "MSTD8004SERR";
            public const string MSTD9003SINF = "MSTD9003SINF";
            public const string MSTD9005SCFM = "MSTD9005SCFM";
            public const string MSTD9006SCFM = "MSTD9006SCFM";
            public const string MSTD9007SCFM = "MSTD9007SCFM";
            public const string MSTD9008SCFM = "MSTD9008SCFM";
            public const string MSTD9009SERR = "MSTD9009SERR";
            public const string MSTD9010SERR = "MSTD9010SERR";
            public const string MSTD9011SERR = "MSTD9011SERR";
            public const string MFAS1310AERR = "MFAS1310AERR";
            public const string MFAS1302AINF = "MFAS1302AINF";
            public const string MFAS1311AERR = "MFAS1311AERR";
            public const string MFAS1304AINF = "MFAS1304AINF";
            public const string MFAS1307AERR = "MFAS1307AERR";
            public const string MFAS1306AINF = "MFAS1306AINF";
            public const string MCOM0022AWRN = "MCOM0022AWRN";
            public const string MFAS0703AERR = "MFAS0703AERR";
            public const string MFAS0704AERR = "MFAS0704AERR";
            public const string MFAS1206AERR = "MFAS1206AERR";
            public const string MFAS0704AWRN = "MFAS0704AWRN";
            public const string MFAS0705AERR = "MFAS0705AERR";
            public const string MCOM0024AERR = "MCOM0024AERR";
            public const string MFAS0706AERR = "MFAS0706AERR";
            public const string MFAS0707AWRN = "MFAS0707AWRN";

            public const string MFAS1605AERR = "MFAS1605AERR";
            public const string MFAS0903AERR = "MFAS0903AERR";
            public const string MFAS1606AERR = "MFAS1606AERR";
        }
        #endregion

        #region TB_M_SYSTEM
        public struct CATEGORY
        {
            public const string APPROVE_FLOW = "APPROVE_FLOW";
            public const string SYSTEM_CONFIG = "SYSTEM_CONFIG";
        }
        public struct SUB_CATEGORY
        {
            public const string TRANSFER_REASON = "TRANSFER_REASON";
            public const string DISPOSAL_REASON = "DISPOSAL_REASON";
            public const string BOI_STATUS = "BOI_STATUS";
            public const string STORE_STATUS = "STORE_STATUS";
            public const string IMAGE_PATH = "IMAGE_PATH";
            public const string DOC_UPLOAD = "DOC_UPLOAD";
            public const string PATH_FILE_TYPE = "PATH_FILE_TYPE";
            public const string KEEP_BY = "KEEP_BY";
            public const string SALE_PROCESS = "SALE_PROCESS";
        }
        public struct CODE
        {
            public const string IMAGE_UPLOAD = "IMAGE_UPLOAD";
            public const string DOC_PATH = "DOC_PATH";
        }
        #endregion

        public struct Role
        {
            public const string AECManager = "ACM";
            public const string AECUser = "ACU";

        }

        public struct WFD2510
        {
            public const string AllowEnableDetail = "|DNTE|SALE|ISCR|SCP|BID|";
        }

        public struct WFD02410
        {
            public const string MassTransferResponsible = "TR";
            public const string MassTransferCostCenter = "TC";
            public const string MassTransferEmployee = "TE";
            public const string NormalTransferResponsible = "RC";
            public const string NormalTransferCostCenter = "CC";
            public const string NormalTransferEmployee = "EC";

        }

        public struct RequestType
        {
            public const string CIP = "X";
            public const string STOCK = "S";
            public const string DISPOSE = "D";
            public const string REPRINT = "R";
            public const string RECLASS = "C";
            public const string SATTLEMENT = "K";
            public const string INFO_CHANGE = "G";
            public const string TRANSFER = "T";
            public const string NEW_ASSET = "A";
           // public const string NEW_ASSET_AUC = "U";
            public const string IMPAIRMENT = "M";

        }
        
        public struct AssetInfoChange
        {
            public const string NonAccountFlow = "GN";
            public const string AccountFlow = "GA";

            public const string NonAccountEditType = "N";
            public const string AccountEditType = "A";
            public const string MixEditType = "M";


        }
        public struct DocumentStatus
        {
            public const string New = "00";
            public const string Approve = "10";
            
            public const string Resend = "45";
            public const string AECRejectwithError = "85";
            public const string AECClosewithError = "88";
            public const string UserCloseError = "90";
            public const string Finish = "99";
        }

        #region Batch Id

        public struct BatchID
        {
            public const string BFD01140 = "BFD01140";
            public const string BFD01220 = "BFD01220";
            public const string BFD01240 = "BFD01240";
            public const string BFD01260 = "BFD01260";
            public const string BFD02110 = "BFD02110";
            public const string BFD02430 = "BFD02430";
            public const string BFD02530 = "BFD02530";
            public const string BFD0261A = "BFD0261A";
            public const string BFD02670 = "BFD02670";
            public const string BFD02680 = "BFD02680";
            public const string BFD02690 = "BFD02690";
            public const string LFD02140 = "LFD02140";
            public const string LFD02150 = "LFD02150";
            public const string LFD02170 = "LFD02170";
            public const string LFD02180 = "LFD02180";
            public const string LFD02230 = "LFD02230";
            public const string LFD02240 = "LFD02240";
            public const string LFD02330 = "LFD02330";
            public const string LFD02420 = "LFD02420";
            public const string LFD02440 = "LFD02440";
            public const string LFD02450 = "LFD02450";
            public const string LFD02520 = "LFD02520";
            public const string LFD02540 = "LFD02540";
            public const string LFD02550 = "LFD02550";
            public const string LFD02660 = "LFD02660";
            public const string BFD021A3 = "BFD021A3";

            public const string WFD02310 = "WFD02310";
            public const string WFD02410 = "WFD02410";
            public const string WFD02510 = "WFD02510";
            public const string WFD02710 = "WFD02710";
            public const string WFD02810 = "WFD02810";
            public const string WFD02910 = "WFD02910";
            public const string WFD02A00 = "WFD02A00";
            public const string WFD021A0 = "WFD021A0";
        }

        #endregion

        #region Deployment
        public struct Deployment
        {
            public const string MODE_OFFLINE = "Offline";
            public const string MODE_PU = "PU";
            public const string MODE_IFT = "IFT";
            public const string MODE_BCT = "BCT";
            public const string MODE_BCTIS = "BCT-IS";
            public const string MODE_SIT = "SIT";
            public const string MODE_UT = "UT";
            public const string MODE_PRODUCTION = "PRODUCTION";
            public const bool SC2_ONLINE = true;
            public const bool SC2_OFFLINE = false;
        }
        #endregion
    }
}
