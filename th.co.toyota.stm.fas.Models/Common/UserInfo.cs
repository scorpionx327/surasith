﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
   public class UserInfo
    {
        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public string CompanyType { get; set; }

        public string Department { get; set; }

        public string Division { get; set; }

        public string Email { get; set; }

        public string EmployeeNo { get; set; }

        public string Location { get; set; }

        public string Section { get; set; }

        public string TelephoneNumber { get; set; }

        public string Title { get; set; }

        public string Region { get; set; }

        public string Country { get; set; }

        public string Language { get; set; }

        public string UserAlias { get; set; }

        public bool IsFAAdmin { get; set; } //FA Admin

        public bool IsSysAdmin { get; set; } //System Admin
    }

    public class AccessControlList
    {
        public string SystemDn { get; set; }

        public string SystemUrl { get; set; }

        public List<KeyValuePair<AccessControlButton, int>> MapButtonAcl { get; set; }

        public List<KeyValuePair<AccessControlScreen, string>> MapScreenAcl { get; set; }

        public List<KeyValuePair<string, string>> MapGroupId { get; set; }

        public List<KeyValuePair<string, string>> MapSubGroupId { get; set; }

        public List<KeyValuePair<string, string>> MapFunctionId { get; set; }

        public List<KeyValuePair<string, string>> MapScreenId { get; set; }

        public List<string> RoleList { get; set; }
    }
    public class AccessControlScreen
    {
        public string FunctionId { get; set; }

        public string ScreenId { get; set; }

        public string ScreenResource { get; set; }

        public string Group { get; set; }

        public string SubGroup { get; set; }
    }
    public class AccessControlButton
    {
        public string FunctionId { get; set; }

        public string ScreenId { get; set; }

        public string ButtonId { get; set; }

        public int AccessRight { get; set; }
    }

}
