﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;



namespace th.co.toyota.stm.fas
{
    public static class Extensions
    {
        public static List<T> ToList<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        /*  Init from off  ------------------------
        //private static T GetItem<T>(DataRow dr)
        //{
        //    Type temp = typeof(T);
        //    T obj = Activator.CreateInstance<T>();

        //    foreach (DataColumn column in dr.Table.Columns)
        //    {
        //        foreach (PropertyInfo pro in temp.GetProperties())
        //        {
        //            if (pro.Name == column.ColumnName)
        //                pro.SetValue(obj, dr[column.ColumnName], null);
        //            else
        //                continue;
        //        }
        //    }
        //    return obj;
        //}
        */

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                PropertyInfo pro = temp.GetProperty(column.ColumnName);

                if (!object.Equals(dr[column.ColumnName], DBNull.Value)) // check prod exist
                {
                    if(pro!=null)
                    { 
                    if (pro.PropertyType == typeof(String))
                    {
                        pro.SetValue(obj, GetString(dr, column.ColumnName, false), null);
                        continue;
                    }

                    //Decimal
                    if (pro.PropertyType == typeof(Decimal) || (pro.PropertyType == typeof(Decimal?)))
                    {
                        pro.SetValue(obj, GetDecimal(dr, column.ColumnName), null);
                        continue;
                    }

                    //DateTime
                    if (pro.PropertyType == typeof(DateTime) || pro.PropertyType == typeof(DateTime?))
                    {
                        pro.SetValue(obj, GetDate(dr, column.ColumnName), null);
                        continue;
                    }

                    //Int
                    if (pro.PropertyType == typeof(int) || pro.PropertyType == typeof(int?))
                    {
                        pro.SetValue(obj, GetInt(dr, column.ColumnName), null);
                        continue;
                    }
                    }//check have property of model
                }// check prod exist
            }
            return obj;
        }


        #region Covert value 


        //private bool ColumnExists(DataColumn col, string columnName)
        //{
        //    for (int i = 0; i < reader. ;++)
        //    {
        //        if (reader.(i) == columnName)
        //        {
        //            return true;
        //        }
        //    }

        //    return false;
        //}

        private static System.DateTime? GetDate(DataRow objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDateTime(objRow[fieldName]);
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        private static decimal GetDecimal(DataRow objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDecimal(objRow[fieldName]);
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static int GetInt(DataRow objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return int.Parse(objRow[fieldName].ToString());
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static string GetString(DataRow objRow, string fieldName,bool isWithNullable)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToString(objRow[fieldName]);
                }
            }
            catch
            {
                if (isWithNullable)
                { return null; }
                return string.Empty;
            }
            if (isWithNullable)
            { return null; }
            return string.Empty;
        }
        #endregion
    }
}
