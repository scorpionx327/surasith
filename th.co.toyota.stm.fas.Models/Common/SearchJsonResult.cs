﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class SearchJsonResult : BaseJsonResult
    {
        public PaginationModel Pagin { get; set; }
    }
}
