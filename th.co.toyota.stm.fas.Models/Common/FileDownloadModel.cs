﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Common
{
    public class FileDownloadModel : WFD0BaseRequestDetailModel
    {
        public decimal APP_ID { get; set; }
        public string FILE_STATUS { get; set; }
        public string FILE_PATH { get; set; }
        public string FILE_TYPE { get; set; }
        public string FILE_PATH_DOWNLOAD { get; set; }
        public string PATH_FILE_TYPE { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_NAME_ORIGINAL { get; set; }
        public string FILE_ID { get; set; }
        public string TYPE { get; set; }//BOI
        public string FUNCTION_ID { get; set; }
        public string CREATE_BY { get; set; }
        public string LINE_NO { get; set; }
        public string DOC_UPLOAD { get; set; }
        public string FIRST_FLAG { get; set; }
        public string FOLDER_DIRECTORY { get; set; }
        public DateTime CREATE_DATE { get; set; }
    }
    public class CheckFileUploadModel
    {
        public string FunctionID { get; set; }
        public string Folder { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }

    }
}
