﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150SearchApproveRowModel
    {
        public Nullable<long> NO { get; set; }
        public int APPRV_ID { get; set; }
        public string ASSET_CLASS { get; set; }
        public string REQUEST_FLOW_TYPE { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string FIXED_ASSET_CATEGORY { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string COMPANY { get; set; }
        public List<WFD01150SearchApproveFlowModel> Flows { get; set; }
    }
}
