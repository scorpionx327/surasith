﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150RoleSeqModel
    {
        public int SEQ { get; set; }
        public string ROLE { get; set; }
        public string ROLE_NAME { get; set; }
    }
}
