﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150SearchModel
    {
        public string COMPANY { get; set; }
        public string REQUEST_FLOW_TYPE { get; set; }
    }
    public class WFD01150ApproveDModel
    {
        public int APPRV_ID { get; set; }
        public int INDX { get; set; }
        public string ROLE { get; set; }
        public string ROLE_NAME { get; set; }
        public string APPRV_GROUP { get; set; }
        public string DIVISION { get; set; }
        public string DIVISION_NAME { get; set; }
        public string REQUIRE_FLAG { get; set; }
        public string CONDITION_CODE { get; set; }
        public string OPERATOR { get; set; }
        public string OPERATOR_NAME { get; set; }
        public string VALUE1 { get; set; }
        public string VALUE2 { get; set; }
        public string FINISH_FLOW { get; set; }
        public string ALLOW_DEL_ITEM { get; set; }
        public string ALLOW_REJECT { get; set; }
        public string ALLOW_SEL_APPRV { get; set; }
        public string REJECT_TO { get; set; }
        public string REJECT_TO_NAME { get; set; }
        public string HIGHER_APPR { get; set; }
        public string REMARK { get; set; }


        public string LEAD_TIME { get; set; }
        public string GENERATE_FILE { get; set; }
        public string NOTI_BY_EMAIL { get; set; }
        public string OPERATION { get; set; }

    }
    public class WFD01150ApproveEmailDModel
    {
        public int APPRV_ID { get; set; }
        public int INDX { get; set; }
        public string ROLE { get; set; }
        public string ROLE_NAME { get; set; }
        public string CATEGORY { get; set; }
        public string SUB_CATEGORY { get; set; }
        public int E_INDX { get; set; }
        public string E_ROLE { get; set; }
        public string EMAIL_APR_TO { get; set; }
        public string EMAIL_APR_CC { get; set; }
        public string EMAIL_REJ_TO { get; set; }
        public string EMAIL_REJ_CC { get; set; }
        public string DISPLAY_CHECKBOX { get; set; }
        public string GUID { get; set; }
        public string USER { get; set; }
    }
}
