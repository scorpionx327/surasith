﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150SearchApproveFlowModel
    {
        public Nullable<long> NO { get; set; }
        public int APPRV_ID { get; set; }
        public int INDX { get; set; }
        public string ROLE_CODE { get; set; }
        public string ROLE_NAME { get; set; }
    }
}
