﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150FlowMasterModel
    {
        public string REQUEST_TYPE { get; set; }
        public Nullable<int> CODE { get; set; }
    }
}
