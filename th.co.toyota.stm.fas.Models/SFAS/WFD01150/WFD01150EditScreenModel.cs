﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150EditScreenModel
    {
        public int APPROVE_ID { get; set; }
        public string REQUEST_FLOW_TYPE { get; set; }
        public int CurrentSeq { get; set; }
        public string OperationCode { get; set; }
        public string GUID { get; set; }
        public string USER { get; set; }
        public List<WFD01150RoleSeqModel> UpperRoles { get; set; }
    }
}
