﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01150
{
    public class WFD01150SearchApproveModel
    {
        public List<WFD01150SearchApproveRowModel> Rows { get; set; }
        public List<WFD01150FlowMasterModel> FlowMasterDatas { get; set; }

    }
}
