﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.WFD01270
{
    public class WFD01270Model
    {
        public string COMPANY { get; set; }
        public string DOC_NO { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string APPROVE_FLOW_TYPE { get; set; }
        public string EMP_CODE { get; set; }
        public string GUID { get; set; }
        public string USER_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string COST_CENTER_TO { get; set; }
        public Nullable<int> INDX { get; set; }

        public string DOC_STATUS { get; set; }
        public string MODE { get; set; }            //-- N : No authorize, V : View Only, A:Approve, R : VERIFY
        public string DELEGATE { get; set; }
        public string MAIN { get; set; }            // Y = EmpCode Current
        public string HIGHER { get; set; }
        public string FA { get; set; }

        public string IS_REQ { get; set; }
        public string IS_BOI_STATE { get; set; }
        public string IS_STORE_STATE { get; set; }
        public string IS_FA_STATE { get; set; }

        public string IS_ORG_CHANGED { get; set; }
        public string VIEW_BOI_STATE { get; set; }
        public string VIEW_STORE_STATE { get; set; }
        public string VIEW_FA_STATE { get; set; }


        public string DELETE_ASSET { get; set; }

        public string ALLOW_APPROVE { get; set; }   // Y = Show BUTTON APPROVE  and MODE = R Change BUTTON APPROVE To VERIFY
        public string ALLOW_REJECT { get; set; }    // Y = Show BUTTON REJECT
        public string ALLOW_SEL_APPV { get; set; }
        public int LINE_NO { get; set; }
        public string STATUS { get; set; }
        public string STATUS_HINT { get; set; }
        public string PARENT_ASSET_NO { get; set; }
        public string ASSET_DESC { get; set; }
        public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_HINT { get; set; }
        public string SERIAL_NO { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string COST_NAME_HINT { get; set; }
        public string WBS_BUDGET { get; set; }
        public string WBS_NAME { get; set; }
        public string LOCATION { get; set; }
        public string LOCATION_HINT { get; set; }
        public string INVEST_REASON { get; set; }
        public string INVEST_REASON_HINT { get; set; }
        public string EVA_GRP_1 { get; set; }
        public string EVA_GRP_1_HINT { get; set; }
        public string EVA_GRP_4 { get; set; }
        public string EVA_GRP_4_HINT { get; set; }
        public string ASSET_SUB { get; set; }
        public string EMP_NAME { get; set; }
        public string POSITION { get; set; }
        public string ROLE { get; set; }
        public string DEPT_NAME { get; set; }
        public string TEL_NO { get; set; }
        public string SECTION_NAME { get; set; }

    }




    public class BaseSearchModel
    {
        public Nullable<int> NO { get; set; }

        public string DOC_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_NAME { get; set; }
        public string ASSET_NAME_SHOW { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }

        public string DELETE_FLAG { get; set; }
        public string RETIRED_FLAG { get; set; }
        public string ALLOW_DELETE { get; set; }
        public string ASSET_NO_LINK
        {
            get
            {
                return string.Format("<a href=\"../WFD02130?ASSET_NO={0}&COST_CODE={1}\" target=\"_DetailAssetNo\">{0}</a>", this.ASSET_NO, this.COST_CODE);
            }
        }
        //'' AS ASSET_NO_LINK,

    }
}
namespace th.co.toyota.stm.fas.Models
{

    public class WFD0BaseRequestDocModel
    {
        public string GUID { get; set; }
        public string COPY_MODE { get; set; }
        public string DOC_NO { get; set; }
        public string USER_BY { get; set; }
        public string UPDATE_DATE { get; set; }

        public string PHONE_NO { get; set; }

        public string REQUEST_TYPE { get; set; }
        public string FLOW_TYPE { get; set; }
        public string ACT_ROLE { get; set; }

        public string SUBMIT_DOC_STATUS { get; set; }

        public string TEMP_DOC_NO { get; set; }

        public string COMPANY_SELECTED{ get; set; }

        public int CURRENT_APPR_INDX { get; set; }
        public WFD01270.WFD01170CommentModel CommentAfterComplete { get; set; }
    }
    public class WFD0BaseRequestDetailModel : WFD0BaseRequestDocModel
    {
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_CLASS { get; set; }
        public string MINOR_CATEGORY { get; set; }

        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }

        //Grid display 
        public int ROW_INDX { get; set; }
        public string DATE_IN_SERVICE { get; set; }
        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
        public string ASSET_NAME { get; set; }

        public bool SEL { get; set; }


        // From DB
        public string ALLOW_CHECK { get; set; }
        public string ALLOW_DELETE { get; set; }
        public string ALLOW_EDIT { get; set; }

        //Overide on BO
        public bool AllowCheck { get; set; }
        public bool AllowDelete { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowEditRetirement { get; set; }
        public string COMMON_STATUS { get; set; }
        public string COMMON_MESSAGE
        {
            get
            {
                switch (this.COMMON_STATUS)
                {
                    case "Y":
                        return string.Empty;
                    case "C":
                        return "Cost Center was changed";
                    case "P":
                        return "Asset is in request";
                    case "N":
                        return "Asset is inactive";
                    default:
                        return string.Empty;
                }
            }
        }
    }


    public class JsonResultBaseModel
    {
        public JsonResultBaseModel() { }

        public Common.MessageStatus Status { get; set; }
        public bool IsError
        {
            get
            {
                return this.Status == Common.MessageStatus.ERROR;
            }
        }
        public bool IsSuccess
        {
            get
            {
                return this.Status == Common.MessageStatus.INFO;
            }
        }
        public string Message { get; set; }
        public bool ResetFlow { get; set; }
    }
    public enum ProcessStatus
    {
        Error,
        Warning,
        Success
    }

    public static class SAPSentStatus
    {
        public const string New = "NEW";
        public const string Generate = "GEN";
        public const string Success = "SUCCESS";
        public const string Error = "ERROR";

        public static bool IsSuccess(string _state)
        {
            return string.Format("{0}", _state) == SAPSentStatus.Success;
        }

    }
    public class WFD01170UserPermissionModel : WFD0BaseRequestDocModel
    {

        public string Company { get; set; }
        public string Mode { get; set; }            //-- N : No authorize, V : View Only, A:Approve, R : VERIFY
        public string IsDelegate { get; set; }
        public string IsMainApprover { get; set; }            // Y = EmpCode Current
        public string IsHigher { get; set; }
        public string IsAECUser { get; set; }
        public string IsInstead { get; set; }
        public string IsOrganizeChange { get; set; }
        public string IsBOIUser { get; set; }
        public string IsFSUser { get; set; }
        public string AllowDelete { get; set; }

        public string AllowEdit { get; set; } //Submit, AEC, Reject to Requestor
        public string AllowApprove { get; set; }   // Y = Show BUTTON APPROVE  and MODE = R Change BUTTON APPROVE To VERIFY
        public string AllowReject { get; set; }    // Y = Show BUTTON REJECT
        public string AllowSelectApprover { get; set; }
        public string AllowChangeApprover { get; set; }

        public string AllowGenAssetNo { get; set; }

        public string AllowShowTMCDoc { get; set; } // Y = Allow, N = Not Allow

        public string AllowResend { get; set; }
        public string AllowCopy { get; set; }
        public string AllowClose { get; set; }
        public string AllowAcknowledge { get; set; }
        public string AllowResubmit { get; set; }

        public string IsFSState { get; set; }
        public string IsBOIState { get; set; } //A : Active, Y : Passed, N : No Passed
        public string IsStoreState { get; set; }
        public string IsAECState { get; set; } //A : Active, Y : Passed, N : No Passed
        public string IsActionMarker { get; set; }
        public string DOC_STATUS { get; set; }
        //      public string AllowEditAll { get; set; }

        public string LockDelete { get; set; }
        public string ActionMark { get; set; } //TMC DONATE ba ba ba

        public bool IsAEC => IsAECState == "Y" && IsAECUser == "Y";

        public bool CanReSend => AllowResend == "Y";

        public bool CanCheck(string allowCheck, string status)
        {
            return IsAEC && 
                (allowCheck == "Y" || (status == "NEW" && CanReSend));
        }

        public string ActRole { get; set; }

        public int ActionRoleIndex { get; set; }
        public string AllowEditRetirement { get; set; }
    }



}