﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD01170
{
    public class WFD01170CommentHistoryModel
    {
        public string COMMENT_HISTORY_ID { get; set; }
        public int INDX { get; set; }
        public string DOC_NO { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string STATUS { get; set; }
        public string COMMENT { get; set; }

        public HtmlString COMMENT_HTML { 
            get {
                return new HtmlString(this.COMMENT);
            }  
        }
        public string ATTACH_FILE { get; set; }
        public string CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
       public string ICON_DISPLAY
        {
            get
            {
                switch (this.STATUS)
                {
                    case "S" :
                        return "glyphicon glyphicon-floppy-disk bg-aqua";
                    case "A":
                        return "glyphicon glyphicon-ok bg-aqua";
                    case "R":
                        return "glyphicon glyphicon-remove bg-maroon";
                    case "C":
                        return "glyphicon glyphicon-off bg-maroon";
                    case "K":
                        return "glyphicon glyphicon-ok-sign bg-aqua";
                    default:
                        return "";
                }
            }
        }
        //public string APPROVER_NAME { get; set; }
    }
}
