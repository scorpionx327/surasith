﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.WFD01270
{
    public class WFD01270Requestor 
    {
        
        public string REQUEST_DATE { get; set; }
        public string REQUEST_TYPE { get; set; }

        public string EMP_TITLE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }

        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }

        public string POS_CODE { get; set; }
        public string POS_NAME { get; set; }

        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }

        public string SECT_CODE { get; set; }
        public string SECT_NAME { get; set; }

        public string PHONE_NO { get; set; }
        public string STATUS { get; set; }
        public string UPDATE_DATE { get; set; }
    }
}
