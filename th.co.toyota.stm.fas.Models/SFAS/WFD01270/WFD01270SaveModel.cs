﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using th.co.toyota.stm.fas.Models.WFD02210;
using th.co.toyota.stm.fas.Models.WFD02310;
using th.co.toyota.stm.fas.Models.WFD02410;
using th.co.toyota.stm.fas.Models.WFD02510;

namespace th.co.toyota.stm.fas.Models.WFD01270
{
    //public class WFD01270MainModel
    //{
    //    public WFD01270Model RequestHeaderData { get; set; }
    //    public WFD01270Requestor Requestor { get; set; }
    //    public List<WFD01270ApproverModel> ApproverList { get; set; }
    //    public WFD01170CommentModel CommentInfo { get; set; }
       
    //    public WFD01270DetailModel DetailData { get; set; }
    //}
    

    //public class WFD01270DetailModel
    //{
    //    public string TempDocNo { get; set; }
    //    //public WFD02210Model DetailCIP { get; set; }
    //    public WFD02510Model DetailDisposal { get; set; }
    //    public WFD02410Model DetailTransfer { get; set; }
    //    public WFD02310Model DetailReprint { get; set; }
    //    public WFD02640.WFD02640Model DetailStockTaking { get; set; }

    //}

    //public class WFD01270DocumentMappingModel
    //{
    //    public string DOC_NO { get; set; }
    //    public string ASSET_CATEGORY { get; set; }

    //    public string REQUEST_TYPE { get; set; }
    //}

    public class WFD01170MainModel
    {
        public WFD0BaseRequestDocModel RequestHeaderData { get; set; }
        
        public WFD01170UserPermissionModel UserPermissionData { get; set; }
        public WFD01170CommentModel CommentInfo { get; set; }
        
        public string ApproverList { get; set; }

        public bool IsAECUser { get; set; }

        public string NewGuid { get; set; } //For Copy New Operation will create new guid for new request
    }

    public class WFD01170SelectedAssetNoModel
    {
      //  public string REQUEST_TYPE { get; set; }
        public string DOC_NO { get; set; }
        public string UPDATE_DATE { get; set; }
        public string GUID { get; set; }
        public string SelectedList { get; set; }
        public string USER { get; set; }
    }
    public class WFD01170CommentModel
    {
        public int Index { get; set; }
        public HttpPostedFileBase CommentFile { get; set; }
        public string AttachFileName { get; set; }
        public string CommentText { get; set; }

        public string CommentStatus { get; set; }
    }

    public class WFD01170UploadRequestModel
    {
        public string StartUpPath { get; set; }
        public HttpPostedFileBase ExcelFile { get; set; }
        public string GUID { get; set; }
        public string RequestType { get; set; }
        public string User { get; set; }

    }
    public class WFD01170UploadResultModel
    {
       
        public string FileName { get; set; }
        public bool IsError { get; set; }
        public string Message { get; set; }
    }
}
