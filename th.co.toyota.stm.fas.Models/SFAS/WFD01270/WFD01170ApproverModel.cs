﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.WFD01170
{

    //public class WFD01170ApproveRenderGroupModel
    //{
    //    public int LAYOUT_GROUP_INDX { get; set; }
    //    public string LAYOUT_GROUP_NAME { get; set; }

    //    public bool LAYOUT_COL_INDX { get; set; }
    //    public int LAYOUT_ROW_INDX { get; set; }
    //}
    public class WFD01170ApproverModel
    {
        public int ROW_INDX { get; set; }
        public int INDX { get; set; }

        public string NOTIFICATION_MODE { get; set; }
        public string OPERATION_MODE { get; set; }

        public string APPR_ROLE { get; set; }
        public string ROLE_NAME { get; set; }
        public string ACTUAL_ROLE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }

        public string APPRV_GROUP { get; set; }
        public string DIVISION { get; set; }
        public string DIV_NAME { get; set; }
        public string REQUIRE_FLAG { get; set; }

        public string APPR_STATUS { get; set; }
        public string APPR_DATE { get; set; }

        public string GUID { get; set; }
        public string DISABLE_EMP_CODE { get; set; }

        public string MAIN { get; set; }

       
        public string DISPLAY_SIDE { get; set; }
        public int DISPLAY_ROW { get; set; }
        public string DISPLAY_TITLE { get; set; }
        public string DISPLAY_FLAG { get; set; }

    }
}
