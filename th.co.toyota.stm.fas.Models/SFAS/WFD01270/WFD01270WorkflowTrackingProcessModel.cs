﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01170
{
    public class WFD01170WorkflowTrackingProcessModel 
    {
        public int INDX { get; set; }//ROLE
        public string DOC_NO { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string ROLE { get; set; }
        public string APPR_STATUS { get; set; }
        public string APPR_STATUS_NAME { get; set; }
        public string APPR_STATUS_CSS { get; set; }
    }
}
