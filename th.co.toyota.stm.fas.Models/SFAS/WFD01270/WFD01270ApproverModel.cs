﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.WFD01270
{
    //public class WFD01270GenerateApproverModel : WFD01270BaseModel
    //{
    //    public Nullable<int> NO { get; set; }
    //    public Nullable<int> INDX { get; set; }
    //    public string ROLE { get; set; }
    //    public string ROLE_NAME { get; set; }
    //    public string DIVISION { get; set; }
    //    public string DIVISION_NAME { get; set; }
    //    public string EMP_CODE { get; set; }
    //    public string EMP_NAME { get; set; }
    //    public string DOC_NO { get; set; }
    //    public string REQUEST_TYPE { get; set; }
    //    public string USER_BY { get; set; }
    //    public string REQUIRE_FLAG { get; set; }
    //    public string DISABLE_EMP_CODE { get; set; }
    //}

    public class WFD01270ApproverModel
    {
        public Nullable<int> NO { get; set; }
        public string DOC_NO { get; set; }
        public Nullable<int> INDX { get; set; }
        public string ROLE { get; set; }
        public string ROLE_NAME { get; set; }
        public string ACTUAL_ROLE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string EMP_NAME_DISPLAY { get; set; }
        public string EMAIL { get; set; }
        public string APPRV_GROUP { get; set; }
        public string DIVISION { get; set; }
        public string DIVISION_NAME { get; set; }
        public string REQUIRE_FLAG { get; set; }
        public string FINISH_FLOW { get; set; }
        public string ALLOW_DEL_ITEM { get; set; }
        public string ALLOW_REJECT { get; set; }
        public string ALLOW_SEL_APPRV { get; set; }
        public Nullable<int> REJECT_TO { get; set; }
        public string APPR_STATUS { get; set; }
        public string APPR_DATE { get; set; }
        public string POS_CODE { get; set; }
        public string POS_NAME { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string GUID { get; set; }
        public string DISABLE_EMP_CODE { get; set; }

        public string MAIN { get; set; }

    
    }
}
