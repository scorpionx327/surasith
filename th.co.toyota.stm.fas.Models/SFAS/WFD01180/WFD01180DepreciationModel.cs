﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01180
{
    public class WFD01180DepreciationModel : WFD0BaseRequestDetailModel
    {
        //public string ASSET_CLASS { get; set; }
        //public string MINOR_CATEGORY { get; set; }

        public string ASSET_DESCRIPTION { get; set; }
        public string MINOR_CATEGORY_DESCRIPTION { get; set; }
        public string STICKER_TYPE { get; set; }
        public string STICKER_SIZE { get; set; }
        public string INVENTORY_INDICATOR { get; set; }
        public string DPRE_KEY_01 { get; set; }
        public int USEFUL_LIFE_YEAR_01 { get; set; }
        public int USEFUL_LIFE_PERD_01 { get; set; }
        public string USEFUL_LIFE_TYPE_01 { get; set; }
        public decimal SCRAP_VALUE_01 { get; set; }
        public string DEACT_DEPRE_AREA_15 { get; set; }
        public string DPRE_KEY_15 { get; set; }
        public int USEFUL_LIFE_YEAR_15 { get; set; }
        public int USEFUL_LIFE_PERD_15 { get; set; }
        public string USEFUL_LIFE_TYPE_15 { get; set; }
        public decimal SCRAP_VALUE_15 { get; set; }
        public string DPRE_KEY_31 { get; set; }
        public int USEFUL_LIFE_YEAR_31 { get; set; }
        public int USEFUL_LIFE_PERD_31 { get; set; }
        public string USEFUL_LIFE_TYPE_31 { get; set; }
        public decimal SCRAP_VALUE_31 { get; set; }
        public string DPRE_KEY_41 { get; set; }
        public int USEFUL_LIFE_YEAR_41 { get; set; }
        public int USEFUL_LIFE_PERD_41 { get; set; }
        public string USEFUL_LIFE_TYPE_41 { get; set; }
        public decimal SCRAP_VALUE_41 { get; set; }
        public string DPRE_KEY_81 { get; set; }
        public int USEFUL_LIFE_YEAR_81 { get; set; }
        public int USEFUL_LIFE_PERD_81 { get; set; }
        public string USEFUL_LIFE_TYPE_81 { get; set; }
        public decimal SCRAP_VALUE_81 { get; set; }
        public string DPRE_KEY_91 { get; set; }
        public int USEFUL_LIFE_YEAR_91 { get; set; }
        public int USEFUL_LIFE_PERD_91 { get; set; }
        public string USEFUL_LIFE_TYPE_91 { get; set; }
        public decimal SCRAP_VALUE_91 { get; set; }
    }
    public class WFD01180MSystemModel : WFD0BaseRequestDetailModel
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }

    public class WFD01180MinorCategory
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
}
