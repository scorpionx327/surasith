﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02610
{
    public class WFD02610SearchModel
    {
        public Nullable<bool> SELECTED { get; set; }
        public string STOCK_TAKE_KEY { get; set; }
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string ASSET_LOCATION_NAME { get; set; }
        public Nullable<int> TOTAL_COST_CENTER { get; set; }
        public Nullable<int> TOTAL_ASSETS { get; set; }
        //public string TOTAL_ASSETS_STR
        //{
        //    get
        //    {
        //        if (TOTAL_ASSETS != null)
        //        {
        //            return TOTAL_ASSETS.Value.ToString("N2");
        //        }
        //        else
        //        {
        //            return "";
        //        }
        //    }
        //}
        public Nullable<int> SCAN_RESULT { get; set; }
        //public string SCAN_RESULT_STR
        //{
        //    get
        //    {
        //        if (SCAN_RESULT != null) return SCAN_RESULT.Value.ToString("N2");
        //        else return "";
        //    }
        //}
        public Nullable<int> PLAN_DAY { get; set; }
        public Nullable<int> ACTUAL_DAY { get; set; }
        public string REMARK { get; set; }
        public string PLAN_STATUS { get; set; }
        public string PLAN_STATUS_TEXT { get; set; }
    }
}
