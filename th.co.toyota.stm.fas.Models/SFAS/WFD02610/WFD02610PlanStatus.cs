﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02610
{
    public enum WFD02610PlanStatus
    {
        /// <summary>
        /// DRAFT
        /// </summary>
        D = 0,
        /// <summary>
        /// COMPLETE PLAN
        /// </summary>
        C = 1,
        /// <summary>
        /// Begin scan cannot edit plan
        /// </summary>
        S = 2,
        /// <summary>
        /// Finish plan
        /// </summary>
        F = 3
    }
}
