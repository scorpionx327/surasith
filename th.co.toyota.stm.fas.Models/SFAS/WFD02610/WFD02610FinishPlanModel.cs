﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02610
{
    public class WFD02610FinishPlanModel
    {
        public string PLAN_STATUS { get; set; }
        public Nullable<int> UNCHECK_CNT { get; set; }
    }
}
