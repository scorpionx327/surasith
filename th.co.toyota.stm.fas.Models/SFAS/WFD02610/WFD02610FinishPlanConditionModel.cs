﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02610
{
    public class WFD02610FinishPlanConditionModel
    {
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string REMARK { get; set; }
        public int TOTAL_NOT_CHECK_ASSETS { get; set; }
        public string USER { get; set; }
    }
}
