﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Models.WFD02610
{
    public class WFD02610DefaultScreenModel
    {
        public string COMPANY { get; set; }
        //[Model(true, "Year")]
        public string YEAR { get; set; }
        //[Model(true, "Round")]
        public string ROUND { get; set; }
        //[Model(true, "Asset Location")]
        public string ASSET_LOCATION { get; set; }
    }
}
