﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using th.co.toyota.stm.fas.Models.Shared;

namespace th.co.toyota.stm.fas.Models.WFD01120
{
    public class WFD01120ChangeRequestHeader
    {
        public string COMPANY { get; set; }
        public int REPORT_TYPE { get; set; }
        public string DOC_NO { get; set; }
        public string REPORT_NAME { get; set; }         
        public string REQUEST_TYPE { get; set; }      
	    public string PERIOD_FROM { get; set; }
        public string PERIOD_TO  { get; set; }
        public string EMP_CODE { get; set; }
        public string COST_CENTER_CODE { get; set; }

        public List<WFD01120ChangeRequestDetail> SELECTED { get; set; }
    }

    public class WFD01120ChangeRequestDetail 
    {
        public Int64 ROW_NUMBER { get; set; }
        public string COMPANY { get; set; }
        public string REQUEST_CODE { get; set; }
        public string REPORT_TYPE { get; set; }
        public string REPORT_NO { get; set; }
        public string COMPLETE_DATE { get; set; }            
    }

    public class WFD01120ChangeRequestDetailModel
    {
        public List<WFD01120ChangeRequestDetail> ListChangeRequestDetail { get; set; }      
    }    
}
 