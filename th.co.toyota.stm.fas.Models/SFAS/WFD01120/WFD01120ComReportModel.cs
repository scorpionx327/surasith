﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using th.co.toyota.stm.fas.Models.Shared;

namespace th.co.toyota.stm.fas.Models.WFD01120
{
    public class WFD01120ReportTypeModel
    {
        public WFD01120ReportTypeModel()
        {
            ListReportType = new List<SelectListItem>();
            ListCostCenter = new List<SelectListItem>();
        }

        public List<SelectListItem> ListReportType { get; set; }
        public List<SelectListItem> ListCostCenter { get; set; }
    }
}
