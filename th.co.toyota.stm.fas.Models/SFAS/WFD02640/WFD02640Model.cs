﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02640
{
    public class WFD02640SummaryTimeModel
    {
        public string PERSEN { get; set; }
        public string STATUS { get; set; }
        public string CLASS { get; set; }
    }

    public class WFD02640SummaryAssetModel
    {
        public string PERSEN { get; set; }
        public string CLASS { get; set; }
        public string ICON { get; set; }
        public string DESCRIPTION { get; set; }
        public string IMGSRC { get; set;}
    }
    public class WFD02640SummaryDetailModel
    {
        public string COST_CENTER_CODE { get; set; }
        public string COST_CENTER_NAME { get; set; }
        public string PERSEN { get; set; }
        public string STATUS { get; set; }
        public string DESCRIPTION { get; set; }
        public string COMMENT { get; set; }
        public string ATTACHMENT { get; set; }
        public string TOTAL { get; set; }
        public string CHECKED { get; set; }
    }
    public class WFD02640ScreenModel
    {
        public WFD02640SummaryTimeModel SummaryTime;
        public WFD02640SummaryAssetModel SummaryAsset;
        public List<WFD02640SummaryDetailModel> SummaryDetail;
        public string EMP_NAME { get; set; }
    }
    public class WFD02640Model
    {
        public List<WFD02640SummaryDetailModel> COST_CENTER_LIST { get; set; }

        public string DOC_NO { get; set; }
        public string GUID { get; set; }
        
        public string USER_BY { get; set; }
        public string UPDATE_DATE { get; set; }

        //public string IS_BOI_ROLE_APPROVE { get; set; }
        //public string IS_STORE_ROLE_APPROVE { get; set; }
        //public string IS_FA_ROLE_APPROVE { get; set; }
        //public string IS_SV_SUBMIT { get; set; }
    }
}
