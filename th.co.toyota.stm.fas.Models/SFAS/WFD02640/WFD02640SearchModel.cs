﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02640
{
    public class WFD02640SearchModel
    {
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string EMP_CODE { get; set; }
        public string ISFAADMIN { get; set; }
        public string SV_CODE { get; set; }
        public string COST_CENTER { get; set; }
        public string DOCUMENT { get; set; }
        public string COMMENT { get; set; }
        public string ATTACHMENT { get; set; }
        public string GUID { get; set; }
        public HttpPostedFileBase FILE { get; set; }
        public string SELECTED { get; set; }

        public string COMPLETE_FLAG { get; set; }

      
    }
}
