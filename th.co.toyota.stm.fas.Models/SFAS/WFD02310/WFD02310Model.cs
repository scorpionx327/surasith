﻿using System;
using System.Collections.Generic;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02310
{
    public class WFD02310Model : WFD0BaseRequestDetailModel
    {
        //public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_HINT { get; set; }
        public string DAMAGE_FLAG { get; set; }
        public string LOSS_FLAG { get; set; }
        public int? PRINT_COUNT { get; set; }
       

    }

}
