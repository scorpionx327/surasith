﻿using System;
using System.Web;


namespace th.co.toyota.stm.fas.Models.WFD02310
{
    public class WFD02310DetailModel : WFD01270.BaseSearchModel
    {
        public string SUB_TYPE { get; set; }
        public string DAMAGE_FLAG { get; set; }
        public string LOSS_FLAG { get; set; }
        public string DAMAGE_FLAG_CHECKBOX { get; set; }
        public string LOSS_FLAG_CHECKBOX { get; set; }
        public string PRINT_COUNT { get; set; }
        public string CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }

        public string REQUIRE_DAMAGE_FLAG { get; set; }
        public string DISABLE_DAMAGE_FLAG { get; set; }

        public string REQUIRE_LOSS_FLAG { get; set; }
        public string DISABLE_LOSS_FLAG { get; set; }

    }
}
