﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02210
{
    public class WFD02210SearchModel : WFD01270.BaseSearchModel
    {
        public string GUID { get; set; }
        public string CIP_NO { get; set; }   
        public string PLAN_COST { get; set; }
        public string ACTUAL_COST { get; set; }        
        public string COMPLETE_DATE { get; set; }   
        public string UPDATE_DATE { get; set; }
    
    }
}
