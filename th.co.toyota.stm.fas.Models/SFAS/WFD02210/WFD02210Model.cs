﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02210
{
    public class WFD02210Model
    {
        public string GUID { get; set; }
        public string DOC_NO { get; set; }
        public string EMP_CODE { get; set; }
        public string USER_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public List<WFD02210SearchModel> ASSET_LIST { get; set; }
    }
}
