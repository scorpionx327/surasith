﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02210
{
    public class WFD02910Model : WFD0BaseRequestDetailModel
    {

        public int LINE_NO { get; set; }
        public int ROW_SEQ { get; set; }
        public string ASSET_CLASS_HINT { get; set; }
        public string COST_VALUE { get; set; }
        public string NEW_ASSET_NO { get; set; }
        public string NEW_ASSET_SUB { get; set; }
        public string NEW_ASSET_NAME { get; set; }
        public string NEW_ASSET_CLASS { get; set; }
        public string NEW_ASSET_CLASS_HINT { get; set; }
        public string NEW_COST_CODE { get; set; }
        public string NEW_RESP_COST_CODE { get; set; }

        public string DOCUMENT_DATE { get; set; }
        public string POSTING_DATE { get; set; }
        public double? PERCENTAGE { get; set; }

        public double? QTY { get; set; }
        public double? ORIGIN_QTY { get; set; }
        public double? AMOUNT_POSTED { get; set; }
        public string PARTIAL_FLAG { get; set; }
        public string ASSET_VALUE_DATE { get; set; }
        public string REASON { get; set; }
        public string BOI_NO { get; set; }
        public string INVEST_REASON { get; set; }
        public string INVEST_REASON_HINT { get; set; }

        public string REQ_GEN { get; set; }

        public string MACHINE_LICENSE { get; set; }

        public string IS_MACHINE_LICENSE { get; set; }

        public string ASSET_STATUS { get; set; }
        public string ASSET_MESSAGE { get; set; }

        public string HAS_ASSET_MASTER { get; set; }
    }


}
