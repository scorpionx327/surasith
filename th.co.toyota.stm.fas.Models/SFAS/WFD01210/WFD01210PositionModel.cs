﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public class WFD01210PositionModel
    {
        public string POST_CODE { get; set; }
        public string POST_NAME { get; set; }
    }
}
