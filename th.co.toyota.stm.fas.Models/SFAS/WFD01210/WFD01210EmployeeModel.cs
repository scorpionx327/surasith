﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public class WFD01210EmployeeModel
    {
        public int RowNumber { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string POST_CODE { get; set; }
        public string POST_NAME { get; set; }
        public string RESPONSIBILITY { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        public string DIV_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMAIL { get; set; }
        public string SIGNATURE_PATH { get; set; }
        public string UPDATE_DATE { get; set; }
        public string COMPANY { get; set; }
        public string JOB { get; set; }
        public string ROLE { get; set; }
        public string SUB_DIV_NAME { get; set; }
        public string SECTION_NAME { get; set; }
        public string LINE_NAME { get; set; }
        public string SYS_EMP_CODE { get; set; }

        public string EncodeMapping { get; set; }
    }

    public class WFD01210AECUserModel
    {
        public string CODE { get; set; }
        public string NAME { get; set; }

        public string SELECTED { get; set;  }

    }
}
