﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public  class WFD01210SaveEmployeeModel
    {
        public string SYS_EMP_CODE { get; set; }
        public string EMP_CODE { get; set; }
        //public string EMP_TITLET { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        //public string EMP_NAME_T { get; set; }
        //public string EMP_LASTNAME_T { get; set; }
        public string ORG_CODE { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        public string POST_CODE { get; set; }
        public string POST_NAME { get; set; }
        public string EMAIL { get; set; }
        public Nullable<System.DateTime> EFF_START_DATE { get; set; }
        public Nullable<System.DateTime> RESIGNED_DATE { get; set; }
        public string EMP_TYPE { get; set; }
        public string SOURCE { get; set; }
        public string TEL_NO { get; set; }
        public string SIGNATURE_PATH { get; set; }
        public string ROLE { get; set; }
        public string SPEC_DIV { get; set; }
        public string ACTIVEFLAG { get; set; }
        public string PRINT_LOCATION { get; set; }
        public string FAADMIN { get; set; }
        public string DELEGATE_PIC { get; set; }
        public string DELEGATE_FROM { get; set; }
        public string DELEGATE_TO { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string RESPONSIBILITY { get; set; }
        public string GRP_NAME { get; set; }
        public string DIV_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string SEC_NAME { get; set; }
        public string IN_CHARGE_COST_CODE { get; set; }
        public string IN_RESPONSE_CHARGE_COST_CODE { get; set; }
        public HttpPostedFileBase FILE { get; set; }
        public string USER_LOGIN { get; set; }
        public string UPDATE_SIGNATURE_PATH_FLAG { get; set; }
        public string POSITION { get; set; }
        public string JOB { get; set; }
        public string JOB_NAME { get; set; }
       // public string AEC_MGR_EMP_ID { get; set; }
        public string COMPANY { get; set; }
        public string SP_ROLE_COMPANY { get; set; }
        public string AEC_User { get; set; }
        public string AEC_Manager { get; set; }
        public string AEC_GENERAL_MANAGER { get; set; }
        public string AEC_USER_LIST { get; set; } //Incase selected record is manager
        public string FASupervisor { get; set; }
        public string FAWindow { get; set; }

    }
}
