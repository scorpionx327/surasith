﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public class WFD01210CCBaseOnOrganizeModel
    {
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        public string PLANT_NAME { get; set; }
    }
}
