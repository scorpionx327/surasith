﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public class WFD01210InChargeModel
    {
        public string PLANT_CD { get; set; }
        public string PLANT_NAME { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }

        public string STYLE { get; set; }

        public string EMP_CODE { get; set; }
        public string STATUS { get; set; }
        public string FLAG_SV_COST_CENTER { get; set; }
        public string TOOLTIP_SV_COST_CENTER { get; set; }
    }
}
