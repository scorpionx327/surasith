﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public class WFD01210SVCostCenterModel
    {
        public string COST_CENTER { get; set; }
        public string EMP_CODE { get; set; }
    }
}
