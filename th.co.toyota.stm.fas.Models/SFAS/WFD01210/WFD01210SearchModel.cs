﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01210
{
    public class WFD01210SearchModel
    {
        public string DIV_CODE { get; set; }
        public string DEPT_CODE { get; set; }
        public string SEC_CODE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string POST_CODE { get; set; }
        public string ROLE { get; set; }
        public string COST_CODE { get; set; }
        public string IN_CHARGE_COST_CENTER { get; set; }
        public string FIND { get; set; }
        public string ORG_CODE { get; set; }
        public string RESPONSIBILITY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string NO_ASSET_FLAG { get; set; }

        public string COMPANY { get; set; }
        public string JOB { get; set; }
        public string SUB_DIV_NAME { get; set; }
        public string SECTION_NAME { get; set; }
        public string LINE { get; set; }
        public string SPECIAL_ROLE { get; set; }
        public string IS_RESPONSE_COST_CENTER { get; set; }

        public string SYS_EMP_CODE { get; set; }

        public string M_SYS_EMP_CODE { get; set; }

        public string LoginUser { get; set; }
    }
}
