﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.LFD02140
{
    public class LFD02140GetBatchIdModel
    {
        public int IDENT_APP_ID { get; set; }
    }
}
