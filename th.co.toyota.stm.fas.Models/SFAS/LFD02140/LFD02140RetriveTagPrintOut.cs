﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.LFD02140
{
    public class LFD02140RetriveTagPrintOut
    {
        public string COST_CENTER { get; set; }
        public string COMPANY { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string BARCODE_SIZE { get; set; }
        public string PRINTER_NAME { get; set; }
        public string PRINT_LOCATION { get; set; }
    }
}
