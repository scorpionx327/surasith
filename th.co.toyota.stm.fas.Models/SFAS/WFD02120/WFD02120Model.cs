﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02120
{
    public class WFD02120Model
    {

    }
    public class WFD02120PrintTagModel
    {
        public string EMP_CODE { get; set; }
        public string ASSET_NO { get; set; }
        public string COST_CODE { get; set; }
        public string PRINT_LOCATION { get; set; }
    }


    public class WFD02120SearhConditionModel
    {


        /* Search Criteria */
        public string UserId { get; set; }
        public string Company { get; set; }
        public string AssetNo { get; set; }
        public string AssetSub { get; set; }
        public string AssetName { get; set; }
        public string AssetClass { get; set; }
        public string AssetGroup { get; set; }
        public string CostCenterCode { get; set; }
        public string ResponsibleCostCenter { get; set; }
        public string Location { get; set; }
        public string WBSProject { get; set; }
        public string WBSBudget { get; set; }
        public string BOI { get; set; }
        public string CapitalizeDateFrom { get; set; }
        public string CapitalizeDateTo { get; set; }
        public string AssetPlateNo { get; set; }
        public string AssetStatus { get; set; }

        public string PrintStatus { get; set; }
        public string PhotoStatus { get; set; }
        public string MapStatus { get; set; }
        public int? CostPending { get; set; }
        public string SearchMode { get; set; }
       
    }
    public class WFD02120FixedAssetResultModel
    {
        /* Search Result */
   
        public int ROW_NO { get; set; }
        public string COMPANY { get; set; }
        public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_HINT { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public string MINOR_CATEGORY_HINT { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_NAME { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME_HINT { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string RESP_COST_NAME_HINT { get; set; }

        public string INVEST_REASON { get; set; }
        public string INVEST_REASON_HINT { get; set; }
        public string BOI_NO { get; set; }
        public string BOI_NO_HINT { get; set; }

        public string WBS_PROJECT { get; set; }
        public string DATE_IN_SERVICE { get; set; }
        public string WBS_PROJECT_HINT { get; set; }

        public string PRINT_STATUS { get; set; }
        public string PHOTO_STATUS { get; set; }
        public string MAP_STATUS { get; set; }
        
        // For Print Location change
        public string EMP_CODE { get; set; }
        public string PRINT_LOCATION { get; set; }

        public string PLATE_TYPE { get; set; }
        public string BARCODE_SIZE { get; set; }
    }

}
