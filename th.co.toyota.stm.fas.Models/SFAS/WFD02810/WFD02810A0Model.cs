﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models
{
    public class WFD02810Model : WFD0BaseRequestDetailModel
    {
        
        public int LINE_NO { get; set; }
        public string MESSAGE { get; set; }

        
        public string ASSET_DESC { get; set; }

        //public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_HINT { get; set; }

        public string SERIAL_NO { get; set; }

        public string ADTL_ASSET_DESC { get; set; }
        
        public string INVEN_NO { get; set; }
        public string INVEN_INDICATOR { get; set; }
        public string INVEN_NOTE { get; set; }
        //public string MINOR_CATEGORY { get; set; }  //EVA_1
        public string MINOR_CATEGORY_HINT { get; set; }
        public string ASSET_STATUS { get; set; }    //EVA_2
        public string ASSET_STATUS_HINT { get; set; }    //EVA_2
        public string MACHINE_LICENSE { get; set; } //EVA_3
        public string MACHINE_LICENSE_HINT { get; set; } //EVA_4
        public string BOI_NO { get; set; } //EVA_4
        public string BOI_NO_HINT { get; set; } //EVA_4
        public string INVEST_REASON { get; set; }
        public string INVEST_REASON_HINT { get; set; }


    }

    public class WFD02810FieldChangeModel
    {
        public string LINE_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string COLUMN_NAME { get; set; }
        public string DISPLAY_NAME { get; set; }
        public string EDIT_TYPE { get; set; }
        public string OLD { get; set; }
        public string NEW { get; set; }
    }
}
