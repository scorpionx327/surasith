﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02710
{
    public class WFD02710AUCModel : WFD0BaseRequestDetailModel
    { 
        public int GRP_ROW_INDX { get; set; }
        public bool Expand { get; set; }
        public string AUC_GROUP_KEY { get; set; }
        public string INVOICE_GROUP_KEY { get; set; }
        public string HEADER_KEY { get; set; }
        public string SHOW_DETAIL { get; set; }
        public string HEADER_DETAIL { get; set; }
        public int COUNT_DETAIL { get; set; }

        public string AUC_NO { get; set; }
        public string AUC_SUB { get; set; }
        public string AUC_NAME { get; set; }

        public string WBS_BUDGET { get; set; }
        public string FISCAL_YEAR { get; set; }
        public string SAP_DOC { get; set; }
        public string PO_NO { get; set; }
        public string INV_NO { get; set; }
        public string INV_LINE { get; set; }
        public string INV_DESC { get; set; }
        public double? AUC_AMOUNT { get; set; }
        public double? AUC_REMAIN { get; set; }
        public double? SETTLE_AMOUNT { get; set; }
        public string CAP_DATE { get; set; }
        public string IS_ROW_FOOTER { get; set; }

        public string TARGET_ASSET_NO { get; set; }
        public string TARGET_ASSET_SUB { get; set; }
        public string ADJ_TYPE { get; set; }

        public bool HAS_GROUP { get; set; }
        public string FINAL_ASSET_CLASS { get; set; }
    }

    public class WFD02710TargetModel
    {
        public string GUID { get; set; }
        public int COL_INDX { get; set; }
        public string DOC_NO { get; set; }
        public string COMPANY { get; set; }

        public string ASSET_NO { get; set; }

        public string ASSET_SUB { get; set; }

        public string CAP_DATE { get; set; }
        public string ASSET_COST_CODE { get; set; }

        public string ASSET_RESP_COST_CODE { get; set; }

        public string ColumnText { get; set; }
        public string TEMP_ASSETS { get; set; }
        public int REF_LINE_NO { get; set; }

        public string TARGET_ASSET_NAME { get; set; }
        public string TARGET_ASSET_CLASS { get; set; }

        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
    }
    public class WFD02710SettlementMappingModel
    {
        public string  RowKey { get; set; }
        public int ColumnKey { get; set; }

        public double? SETTLE_AMOUNT { get; set; }
        public string ADJ_TYPE { get; set; }
    }

    public class WFD02710FinalModel
    {
        public List<WFD02710SettlementMappingModel> MappingList;
        public List<WFD02710AUCModel> RowList ;
        public List<WFD02710TargetModel> ColumnList ;
        public int AssetAUCCount { get; set; }
    }

    public class WFD02710SaveModel
    {
        //Initial data from main screen
        public string FLOW_TYPE { get; set; }
        public string MODE { get; set; }
        public string ORIGINAL_ASSET_NO { get; set; }
        public string ORIGINAL_ASSET_SUB { get; set; }
        public string ROLEMODE { get; set; }

        public string DOC_NO { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string COMPANY { get; set; }
        public int LINE_NO { get; set; }
        public string NEW_LINE_NO { get; set; }
        public string PARENT_ASSET_NO { get; set; }
        public string ASSET_MAINSUB { get; set; }
        public string INDICATOR_VALIDATE { get; set; }
        public string ASSET_CLASS { get; set; }

        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_DESC { get; set; }
        public string ADTL_ASSET_DESC { get; set; }
        public string SERIAL_NO { get; set; }
        public string INVEN_NO { get; set; }
        public string BASE_UOM { get; set; }
        public string LAST_INVEN_DATE { get; set; }
        public string INVEN_INDICATOR { get; set; }
        public string INVEN_NOTE { get; set; }
        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string LOCATION { get; set; }
        public string ROOM { get; set; }
        public string LICENSE_PLATE { get; set; }
        public string WBS_PROJECT { get; set; }
        public string INVEST_REASON { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public string ASSET_STATUS { get; set; }
        public string MACHINE_LICENSE { get; set; }
        public string BOI_NO { get; set; }
        public string FINAL_ASSET_CLASS { get; set; }
        public string ASSET_SUPPER_NO { get; set; }
        public string MANUFACT_ASSET { get; set; }
        public string LOCATION_REMARK { get; set; }
        public string ASSET_TYPE_NAME { get; set; }
        public string WBS_BUDGET { get; set; }
        public decimal BUDGET { get; set; }
        public string PLATE_TYPE { get; set; }
        public string PLATE_TYPE_DESC { get; set; }
        public string BARCODE_SIZE { get; set; }
        public string BARCODE_SIZE_DESC { get; set; }
        public string LEASE_AGREE_DATE { get; set; }
        public string LEASE_START_DATE { get; set; }
        public int LEASE_LENGTH_YEAR { get; set; }
        public int LEASE_LENGTH_PERD { get; set; }
        public string LEASE_TYPE { get; set; }
        public string LEASE_SUPPLEMENT { get; set; }
        public string LEASE_NUM_PAYMENT { get; set; }
        public string LEASE_PAY_CYCLE { get; set; }
        public string LEASE_ADV_PAYMENT { get; set; }
        public int LEASE_PAYMENT { get; set; }
        public int LEASE_ANU_INT_RATE { get; set; }
        public string DPRE_AREA_01 { get; set; }
        public string DPRE_KEY_01 { get; set; }
        public string USEFUL_LIFE_TYPE_01 { get; set; }
        public int PLAN_LIFE_YEAR_01 { get; set; }
        public int PLAN_LIFE_PERD_01 { get; set; }
        public int USEFUL_LIFE_YEAR_01 { get; set; }
        public int USEFUL_LIFE_PERD_01 { get; set; }
        public decimal SCRAP_VALUE_01 { get; set; }

        public decimal CUMU_ACQU_PRDCOST_01 { get; set; }

        public string DEACT_DEPRE_AREA_15 { get; set; }
        public string DPRE_AREA_15 { get; set; }
        public string DPRE_KEY_15 { get; set; }
        public string USEFUL_LIFE_TYPE_15 { get; set; }
        public int PLAN_LIFE_YEAR_15 { get; set; }
        public int PLAN_LIFE_PERD_15 { get; set; }
        public int USEFUL_LIFE_YEAR_15 { get; set; }
        public int USEFUL_LIFE_PERD_15 { get; set; }
        public decimal SCRAP_VALUE_15 { get; set; }
        public string DPRE_AREA_31 { get; set; }
        public string DPRE_KEY_31 { get; set; }
        public string USEFUL_LIFE_TYPE_31 { get; set; }
        public int PLAN_LIFE_YEAR_31 { get; set; }
        public int PLAN_LIFE_PERD_31 { get; set; }
        public int USEFUL_LIFE_YEAR_31 { get; set; }
        public int USEFUL_LIFE_PERD_31 { get; set; }
        public decimal SCRAP_VALUE_31 { get; set; }
        public string DPRE_AREA_41 { get; set; }
        public string DPRE_KEY_41 { get; set; }
        public string USEFUL_LIFE_TYPE_41 { get; set; }
        public int PLAN_LIFE_YEAR_41 { get; set; }
        public int PLAN_LIFE_PERD_41 { get; set; }
        public int USEFUL_LIFE_YEAR_41 { get; set; }
        public int USEFUL_LIFE_PERD_41 { get; set; }
        public decimal SCRAP_VALUE_41 { get; set; }
        public string DPRE_AREA_81 { get; set; }
        public string DPRE_KEY_81 { get; set; }
        public string USEFUL_LIFE_TYPE_81 { get; set; }
        public int PLAN_LIFE_YEAR_81 { get; set; }
        public int PLAN_LIFE_PERD_81 { get; set; }
        public int USEFUL_LIFE_YEAR_81 { get; set; }
        public int USEFUL_LIFE_PERD_81 { get; set; }
        public decimal SCRAP_VALUE_81 { get; set; }
        public string DPRE_AREA_91 { get; set; }
        public string DPRE_KEY_91 { get; set; }
        public string USEFUL_LIFE_TYPE_91 { get; set; }
        public int PLAN_LIFE_YEAR_91 { get; set; }
        public int PLAN_LIFE_PERD_91 { get; set; }
        public int USEFUL_LIFE_YEAR_91 { get; set; }
        public int USEFUL_LIFE_PERD_91 { get; set; }
        public decimal SCRAP_VALUE_91 { get; set; }
        public string GUID { get; set; }
        public string STATUS { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string MESSAGE { get; set; }
        public string DELETE_FLAG { get; set; }
        public string CREATE_BY { get; set; }

        public int Cnt { get; set; }

        public string COST_NAME { get; set; }
        public string RESP_COST_NAME { get; set; }
        public string ASSET_GROUP { get; set; }

        public string OPTION { get; set; }
    }
}
