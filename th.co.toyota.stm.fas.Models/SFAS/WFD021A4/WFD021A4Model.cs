﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD021A4
{
    public class WFD021A4ResponseRequestNo
    {
        public WFD021A4ResponseRequestNo() {
            this.ASSETS = new List<WFD021A4ResponseAsset>();
        }
        public string requestNo { get; set; } // : "TMT-S201908/0001",  
        public List<WFD021A4ResponseAsset> ASSETS { get;set; }
    }
    public class WFD021A4ResponseAsset
    {
        public WFD021A4ResponseAsset()
        {
            this.message = new List<WFD021A4ResponseMessage>();
        }
        public string requestItem { get; set; }
        public string companyCode { get; set; }
        public string assetNo { get; set; }
        public string assetSubNo { get; set; }
        public string status { get; set; }
        public string messagelist {
            get
            {
                string _f = string.Empty;
                foreach(var _d in this.message)
                {
                    _f += string.Format(" {0}", _d.Text);
                }
                return _f;
            }
        }
        public List<WFD021A4ResponseMessage> message { get; set; }
    }
    public class WFD021A4ResponseMessage   
    {
        public string Type { get; set; }
        public string ID { get; set; }
        public string No { get; set; }
        public string Text { get; set; }

    }
}
