﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD02714
{
    public class WFD02714ResponseAUCFromSAPModel
    {
        public string requestNo { get; set; } // : "TMT-S201908/0001",  
        public string requestItem { get; set; } // : "1",
        public string companyCode { get; set; } // : "TMT",
        public string aucNo { get; set; } // : "190002300001",
        public string aucSubNo { get; set; } // : "0000",
        public string returnMessage { get; set; } // : "",
        public string poNo { get; set; } // : "PO201902000",
        public string SAPDocNo { get; set; } // : "docno1",
        public string fiscalYear { get; set; } // : "2019",
        public string invoiceNo { get; set; } // : "invoice1",
        public string invoiceItemNo { get; set; } // : "1",
        public string invoiceDesc { get; set; } // : "xxxxxxxxxxx",
        public string invoiceAmt { get; set; } // : "14000608.00"		

        public override string ToString()
        {
            
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

    }
}
