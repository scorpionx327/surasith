﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620StockTakeDPerSVModel
    {
        public string STOCK_TAKE_KEY { get; set; }
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public Nullable<DateTime> FA_DATE_FROM { get; set; }
        public Nullable<DateTime> FA_DATE_TO { get; set; }
        [Model(true, "Date From")]
        public Nullable<DateTime> DATE_FROM { get; set; }
        [Model(true, "Date To")]
        public Nullable<DateTime> DATE_TO { get; set; }
        [Model(true, "Time start", true)]
        public string TIME_START { get; set; }
        [Model(true, "Time end", true)]
        public string TIME_END { get; set; }
        public Nullable<int> TIME_MINUTE { get; set; }
        public Nullable<int> USAGE_HANDHELD { get; set; }
        public string IS_LOCK { get; set; }
        public Nullable<int> TOTAL_ASSET { get; set; }
        public Nullable<DateTime> CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public Nullable<DateTime> UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }

    }
}
