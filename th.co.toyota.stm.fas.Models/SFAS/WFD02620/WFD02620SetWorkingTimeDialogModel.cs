﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620SetWorkingTimeDialogValidateModel
    {
        [Model(true, "Time start")]
        public string TimeStart { get; set; }
        [Model(true, "Time End")]
        public string TimeEnd { get; set; }
        [Model(true, "Date from")]
        public string DateFrom { get; set; }
        [Model(true, "Date to")]
        public string DateTo { get; set; }
        public DateTime? TargetFrom { get; set; }
        public DateTime? TargetTo { get; set; }
        public DateTime? TimeStartDay { get; set; }
        public DateTime? TimeEndDay { get; set; }
    }
    public class WFD02620SetWorkingTimeDialogModel
    {
        [Model(true, "Time start", true)]
        public string TimeStart { get; set; }
        [Model(true, "Time End", true)]
        public string TimeEnd { get; set; }
        [Model(true, "Date from", DateFormat = "dd-MMM-yyyy")]
        public string DateFrom { get; set; }
        [Model(true, "Date to", DateFormat ="dd-MMM-yyyy")]
        public string DateTo { get; set; }
        public DateTime? TargetFrom { get; set; }
        public DateTime? TargetTo { get; set; }
        public DateTime? TimeStartDay { get; set; }
        public DateTime? TimeEndDay { get; set; }
    }
}