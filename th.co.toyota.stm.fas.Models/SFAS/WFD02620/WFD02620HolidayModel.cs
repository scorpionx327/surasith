﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620HolidayModel
    {
        public string STOCK_TAKE_KEY { get; set; }
        public Nullable<DateTime> HOLIDATE_DATE { get; set; }
    }
}
