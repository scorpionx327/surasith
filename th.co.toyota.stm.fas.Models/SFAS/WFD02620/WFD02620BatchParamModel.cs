﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD02620
{
    public class WFD02620BatchParamModel
    {
        public string UPLOAD_FILE_NAME { get; set; }
        public string IS_UPDATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string QTY_OF_HANDHELD { get; set; }
        public string TARGET_DATE_FROM { get; set; }
        public string TARGET_DATE_TO { get; set; }
        public string BREAK_TIME_MINUTE { get; set; }
    }
}
