﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620SummaryModel
    {
        //public string ORDER_SUB_TYPE { get; set; }
        //public string SUB_TYPE { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public int TOTAL_CNT { get; set; }
    }
}
