﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620SettingModel
    {
        [Model(true, "Create By")]
        public string CREATE_BY { get; set; }
        public string EMP_NAME { get; set; }
        public string STOCK_TAKE_KEY { get; set; }
        public string EMP_LASTNAME { get; set; }    
        [Model(true, "Company")]
        public string COMPANY { get; set; }
        [Model(true, "Year")]
        public string YEAR { get; set; }
        [Model(true, "Round")]
        public string ROUND { get; set; }
        //[Model(true, "Asset Location")]
        public string ASSET_LOCATION { get; set; }
        [Model(true, "Total Handheld")]
        public Nullable<int> QTY_OF_HANDHELD { get; set; }
        public string SUB_TYPE { get; set; }
        [Model(true, "Target date from")]
        public Nullable<DateTime> TARGET_DATE_FROM { get; set; }
        //[Model("dd-MMM-yyyy", FieldName = "Target date from")]
        public string TARGET_DATE_FROM_STR { get; set; }
        [Model(true, "Target date to")]
        public Nullable<DateTime> TARGET_DATE_TO { get; set; }
        //[Model("dd-MMM-yyyy", FieldName = "Target date to")]
        public string TARGET_DATE_TO_STR { get; set; }
        public Nullable<DateTime> DATA_AS_OF { get; set; }
        //[Model("dd-MMM-yyyy", FieldName = "Data as of")]
        public string DATA_AS_OF_STR { get; set; }
        [Model(true, "Break Time ( m )")]
        public Nullable<int> BREAK_TIME_MINUTE { get; set; }
        public string ASSET_STATUS { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        public string PLAN_STATUS { get; set; }

        [Model(true, "ASSET CLASS")]
        public string ASSET_CLASS { get; set; }
        [Model(true, "MINOR CATEGORY")]
        public string MINOR_CATEGORY { get; set; }
        [Model(true, "COST CENTER")]
        public string COST_CENTER { get; set; }
        [Model(true, "RESPONSIBLE COST CENTER")]
        public string RESPONSIBLE_COST_CENTER { get; set; }
        [Model(true, "BOI")]
        public string BOI { get; set; }
        //[Model(true, "INVEST_REASON")]
        //public string INVEST_REASON { get; set; }
        [Model(true, "INVENTORY CHECK")]
        public string INVENTORY_CHECK { get; set; }
        //[Model(true, "INVEN_INDICATOR")]
        //public string INVEN_INDICATOR { get; set; }


    }
    public class WFD02620YearRoundAssetLocModel
    {
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
    }
}
