﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620StockTakeDModel
    {
        public string STOCK_TAKE_KEY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string SV_EMP_CODE { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        //public string PLANT_CD { get; set; }
        //public string PLANT_NAME { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string BARCODE { get; set; }
        //public string SUB_TYPE { get; set; }
        public string CHECK_STATUS { get; set; }
        public Nullable<DateTime> SCAN_DATE { get; set; }
        public Nullable<DateTime> START_COUNT_TIME { get; set; }
        public Nullable<DateTime> END_COUNT_TIME { get; set; }
        public Nullable<int> COUNT_TIME { get; set; }
        public string LOCATION_NAME { get; set; }
        public Nullable<DateTime> CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public Nullable<DateTime> UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        //public string ORDER_SUB_TYPE { get; set; }
        public string SERIAL_NO { get; set; }
        public string ASSET_CLASS { get; set; }
        public float CUMU_ACQU_PRDCOST_01 { get; set; }
        public string MINOR_CATEGORY { get; set; }
    }
}
