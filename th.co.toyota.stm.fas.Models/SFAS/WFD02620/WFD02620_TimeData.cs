﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD02620
{
    public static class WFD02620_TimeData
    {
        public static string workingTimeStartStr { get; set; }
        public static string workingTimeEndStr { get; set; }
        public static string breakTimeStartStr { get; set; }
        public static string breakTimeEndStr { get; set; }
    }
}
