﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class AnnounceEmailModel
    {
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string ASSET_LOCATION_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string EMAIL { get; set; }
    }
}
