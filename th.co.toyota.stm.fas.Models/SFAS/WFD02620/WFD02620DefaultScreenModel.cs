﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620DefaultScreenModel
    {
        public TimeSpan START_HRS { get;  }
        public TimeSpan END_HRS { get;  }
        public TimeSpan BREAK_TIME_START { get; }
        public TimeSpan BREAK_TIME_END { get;  }

        public string BREAK_TIME_STR { get; set; }
        public string START_HRS_STR { get; set; }
        public string END_HRS_STR { get; set; }
    }
}
