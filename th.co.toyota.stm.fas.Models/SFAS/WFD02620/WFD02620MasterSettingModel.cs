﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02620
{
    public class WFD02620MasterSettingModel
    {
        public string BREAK_TIME { get; set; }
        public string DISP_PERIOD { get; set; }
        public string END_HRS { get; set; }
        public string START_HRS { get; set; }
    }

    public class WFD02620CostCenterModel
    {
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }

    }
}
