﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.SFAS.BFD02670
{
    public class BFD02670_CostEmpModel
    {
        public string EMP_CODE { get; set; }
        public string PLANT_CD { get; set; }
        public string PLANT_NAME { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }
        public string PLAN_STATUS { get; set; }
        public string IS_LOCK { get; set; }
        public string STOCK_TAKE_KEY { get; set; }
        public BFD02670_ServerDateTime DATETIME_FROM { get; set; }
        public BFD02670_ServerDateTime DATETIME_TO { get; set; }
        public BFD02670_ServerDateTime PLAN_UPDATE_DATE { get; set; }
    }
}