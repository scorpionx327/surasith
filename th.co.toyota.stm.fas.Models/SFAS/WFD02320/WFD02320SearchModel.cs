﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02320
{
    public class WFD02320SearchModel
    {
        public string COMPANY { get; set; }
        public string FIND { get; set; }
        public string EMP_CODE { get; set; }
    }
}
