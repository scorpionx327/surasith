﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02320
{
    public class WFD02320PrintQDialogModel
    {
        //public string ASSET_NO { get; set; }
        public string PRINT_COMPANY_LOCATION { get; set; }
        public string BARCODE_SIZE { get; set; }
        public string BARCODE_SIZE_DESC { get; set; }
        public string PLATE_TYPE { get; set; }
        public string PLATE_TYPE_DESC { get; set; }
        public string PRINT_LOCATION { get; set; }
        public string LOCATION { get; set; }
        public string PRINTER_NAME { get; set; }
        public int ITEM_QTY { get; set; }
    }

    public class WFD02320PrintAllCountModel
    {
        public string COST_COUNT { get; set; }
        public string RESP_COST_COUNT { get; set; }
        public string CREATEDBY_COUNT { get; set; }
        public int ITEM_COUNT { get; set; }
    }

    public class WFD02320PrintQDCModel
    {
        public List<WFD02320PrintQDialogModel> printQDialog;
        public List<WFD02320PrintQDetailsModel> AssetList;
        public WFD02320PrintAllCountModel printAllCount;
    }

    public class WFD02320PrintQDetailsModel
    {
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }

        public string COST_NAME { get; set; }
        public string BARCODE_SIZE { get; set; }
        public string BARCODE_SIZE_DESC { get; set; }
        public string PRINT_LOCATION { get; set; }
        public string CREATE_BY { get; set; }

        public string PLATE_TYPE { get; set; }
        public string ASSET_NAME { get; set; }
       
        public string DATE_IN_SERVICE { get; set; }
        public string BARCODE { get; set; }
        public int PRINT_COUNT { get; set; }
        public string LOCATION_NAME { get; set; }



        public string CONTACT { get; set; }
        public string COSTCENTER { get; set; }
        public string INVEST_REASON { get; set; }
        public string SERIAL_NO { get; set; }
        public string TEL { get; set; }
        public string TOTALPRINT { get; set; }
        public string PRINTLOCATION { get; set; }

    }


}
