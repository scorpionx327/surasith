﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.Shared
{
    public class LFD02140AssetMasterModel
    {
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string COST_CODE { get; set; }

        public string ASSET_NAME { get; set; }
    //    public string SUB_TYPE { get; set; }
        //public string MODEL { get; set; }
        //public string SERIAL_NO { get; set; }
        //public string TAG_NO { get; set; }
        //public string CLASS_SACCESS { get; set; }
        //public string  BOI_NO { get; set; }
        //public string PROJECT_NAME { get; set; }
        //public float PLAN_COST { get; set; }
        //public string PRODUCTION_PART_NO { get; set; }
        //public string FA_BOOK { get; set; }
        //public string MACHINE_LICENSE { get; set; }
        //public string ASSET_CATEGORY { get; set; }
        //public string MINOR_CATEGORY { get; set; }
        //public string ASSET_TYPE { get; set; }
        public DateTime DATE_IN_SERVICE { get; set; }
        //public float COST { get; set; }
        //public float NBV { get; set; }
        //public float TOOLING_COST { get; set; }
        public string BARCODE_SIZE { get; set; }
        //public float UOP_CAPACITY { get; set; }
        //public float UOP_LTD_PRODUCTION { get; set; }
        //public float STL_LIFE_YEAR { get; set; }
        //public float STL_REMAIN_LIFE_YEAR { get; set; }
        //public float SALVAGE_VALUE { get; set; }
        //public float YTD_DEPRECIATION { get; set; }
        //public float ACCUMULATE_DEPRECIATION { get; set; }
        //public DateTime RETIREMENT_DATE { get; set; }
        //public DateTime TRANSFER_DATE { get; set; }
        //public DateTime ENTRY_DATE { get; set; }
        //public string SUPPLIER_NO { get; set; }
        //public string SUPPLIER_NAME { get; set; }
        //public string LOCATION_NAME { get; set; }
        public string BARCODE { get; set; }
        //public string BOI_FLAG { get; set; }
        //public string LICENSE_FLAG { get; set; }
        //public string FULLY_DP_FLAG { get; set; }
        //public string MAP_STATUS { get; set; }
        //public string PHOTO_STATUS { get; set; }
        //public string PRINT_STATUS { get; set; }
        //public string STATUS { get; set; }
        //public string PROCESS_STATUS { get; set; }
        public int PRINT_COUNT { get; set; }
        //public string TAG_PHOTO { get; set; }
        //public string INHOUSE_FLAG { get; set; }
        //public DateTime LAST_SCAN_DATE { get; set; }
        //public string TRNF_BOI_DECISION { get; set; }
        //public string TRNF_BOI_ATTACH { get; set; }
        //public string DISP_BOI_DECISION { get; set; }
        //public string DISP_BOI_ATTACH { get; set; }
        //public DateTime CREATE_DATE { get; set; }
        //public string CREATE_BY { get; set; }
        //public DateTime UPDATE_DATE { get; set; }
        //public string UPDATE_BY { get; set; }

        public string CONTACT { get; set; }
        public string COSTCENTER { get; set; }
        public string INVEST_REASON { get; set; }
        public string SERIAL_NO { get; set; }
        public string TEL { get; set; }
        public string TOTALPRINT { get; set; }
        public string PRINTLOCATION { get; set; }




    }
}
