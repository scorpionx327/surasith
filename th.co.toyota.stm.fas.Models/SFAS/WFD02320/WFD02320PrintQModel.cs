﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02320
{
    public class WFD02320PrintQModel
    {

        public string COMPANY { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string RESP_COST_NAME { get; set; }
        public int ROWNUMBER { get; set; }
        public string REQUEST_BY_CODE { get; set; }
        public string REQUEST_BY_NAME { get; set; }
        public string COST_CENTER { get; set; }
        public string COST_CENTER_NAME { get; set; }
        public int TOTAL_PRINT_ITEM { get; set; }
    }
}
