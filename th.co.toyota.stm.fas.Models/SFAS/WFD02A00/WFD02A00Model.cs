﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02210
{
    public class WFD02A00Model : WFD0BaseRequestDetailModel
    {      
        public string ACC_PRINCIPLE { get; set; }
        public string DPRE_AREA { get; set; }
        public string DOCUMENT_DATE { get; set; }
        public string POSTING_DATE { get; set; }
        public string REASON { get; set; }
        public double? AMOUNT_POSTED { get; set; }
        public double? PERCENTAGE { get; set; }
        public string IMPAIRMENT_FLAG { get; set; }
        public string ASSET_GROUP { get; set; }
        public double? BF_CUMU_ACQU_PRDCOST_01 { get; set; }
        public string ADJ_TYPE { get; set; }
        public double? BF_NBV { get; set; }
        public double? BF_SCRAP { get; set; }
        public string TOTALCOST { get; set; }
        public string TOTALNETBOOK { get; set; }
        public string TOTALAMOUNT { get; set; }

    }
}
