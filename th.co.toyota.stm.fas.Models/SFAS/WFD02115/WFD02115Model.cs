﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02115
{
    //public class WFD02115Model
    //{
    //    public string SUB_TYPE { get; set; }
    //    public string ONLY_PARENT_ASSETS { get; set; }
    //    public string ASSET_NO { get; set; }
    //    public string ASSET_NAME { get; set; }
    //    public string PO_NO { get; set; }
    //    public string SUPPLIER { get; set; }
    //    public string COST_CODE { get; set; }
    //    public string DATE_IN_SERVICE_START { get; set; }
    //    public string DATE_IN_SERVICE_TO { get; set; }
    //    public string SINGLE_FLAG { get; set; }
    //    public string GUID { get; set; }
    //    public string USER_BY { get; set; }
    //    public string DOC_NO { get; set; }
    //    public string REQUEST_TYPE { get; set; }
    //}

   

    public class WFD02115WBSModel
    {
        
        public string COMPANY_CODE { get; set; }
        public string WBS { get; set; }
        public int FISCAL_YEAR { get; set; }
        public double BUDGET_AMOUNT { get; set; }
        public double ACTUAL { get; set; }
        public double COMMITMENT { get; set; }
        public double AVAILABLE { get; set; }

        public string COM_IND_BUDGET { get; set; }

        //Derived Value
        public string ASSET_CLASS { get; set; }
        public string RESP_COST_CENTER { get; set; }
        public string RESP_COST_NAME { get; set; }
        public string COST_CENTER { get; set; }
        public string COST_NAME { get; set; }
        public string ASSET_GROUP { get; set; }

        
    }
}
