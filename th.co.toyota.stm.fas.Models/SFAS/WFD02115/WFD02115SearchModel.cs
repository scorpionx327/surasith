﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Models.WFD02115
{

    public class WFD02115SaveModel
    {
        //Initial data from main screen
        public string FLOW_TYPE { get; set; }
        public string MODE { get; set; }
        public string ORIGINAL_ASSET_NO { get; set; }
        public string ORIGINAL_ASSET_SUB { get; set; }
        public string ROLEMODE { get; set; }
        public bool IS_FIRST_ASSET { get; set; }

        public string DOC_NO { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string COMPANY { get; set; }
        public int LINE_NO { get; set; }
        public string NEW_LINE_NO { get; set; }
        public string PARENT_ASSET_NO { get; set; }
        public string PARENT_ASSET_NAME { get; set; }
        public string ASSET_MAINSUB { get; set; }
        public string INDICATOR_VALIDATE { get; set; }
        public string ASSET_CLASS { get; set; }

        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_DESC { get; set; }
        public string ADTL_ASSET_DESC { get; set; }
        public string SERIAL_NO { get; set; }
        public string INVEN_NO { get; set; }
        public string BASE_UOM { get; set; }
        public string LAST_INVEN_DATE { get; set; }
        public string INVEN_INDICATOR { get; set; }
        public string INVEN_NOTE { get; set; }
        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string LOCATION { get; set; }
        public string ROOM { get; set; }
        public string LICENSE_PLATE { get; set; }
        public string WBS_PROJECT { get; set; }
        public string INVEST_REASON { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public string ASSET_STATUS { get; set; }
        public string MACHINE_LICENSE { get; set; }
        public string BOI_NO { get; set; }
        public string FINAL_ASSET_CLASS { get; set; }
        public string ASSET_SUPPER_NO { get; set; }
        public string MANUFACT_ASSET { get; set; }
        public string LOCATION_REMARK { get; set; }
        public string ASSET_TYPE_NAME { get; set; }
        public string WBS_BUDGET { get; set; }
        public decimal BUDGET { get; set; }
        public string PLATE_TYPE { get; set; }
        public string PLATE_TYPE_DESC { get; set; }
        public string BARCODE_SIZE { get; set; }
        public string BARCODE_SIZE_DESC { get; set; }
        public string LEASE_AGREE_DATE { get; set; }
        public string LEASE_START_DATE { get; set; }
        public int? LEASE_LENGTH_YEAR { get; set; }
        public int? LEASE_LENGTH_PERD { get; set; }
        public string LEASE_TYPE { get; set; }
        public string LEASE_SUPPLEMENT { get; set; }
        public string LEASE_NUM_PAYMENT { get; set; }
        public string LEASE_PAY_CYCLE { get; set; }
        public string LEASE_ADV_PAYMENT { get; set; }
        public decimal? LEASE_PAYMENT { get; set; }
        public decimal? LEASE_ANU_INT_RATE { get; set; }
        public string DPRE_AREA_01 { get; set; }
        public string DPRE_KEY_01 { get; set; }
        public string USEFUL_LIFE_TYPE_01 { get; set; }
        public int? PLAN_LIFE_YEAR_01 { get; set; }
        public int? PLAN_LIFE_PERD_01 { get; set; }
        public int? USEFUL_LIFE_YEAR_01 { get; set; }
        public int? USEFUL_LIFE_PERD_01 { get; set; }
        public decimal? SCRAP_VALUE_01 { get; set; }

        public decimal? CUMU_ACQU_PRDCOST_01 { get; set; }

        public string DEACT_DEPRE_AREA_15 { get; set; }
        public string DPRE_AREA_15 { get; set; }
        public string DPRE_KEY_15 { get; set; }
        public string USEFUL_LIFE_TYPE_15 { get; set; }
        public int? PLAN_LIFE_YEAR_15 { get; set; }
        public int? PLAN_LIFE_PERD_15 { get; set; }
        public int? USEFUL_LIFE_YEAR_15 { get; set; }
        public int? USEFUL_LIFE_PERD_15 { get; set; }
        public decimal? SCRAP_VALUE_15 { get; set; }
        public string DPRE_AREA_31 { get; set; }
        public string DPRE_KEY_31 { get; set; }
        public string USEFUL_LIFE_TYPE_31 { get; set; }
        public int? PLAN_LIFE_YEAR_31 { get; set; }
        public int? PLAN_LIFE_PERD_31 { get; set; }
        public int? USEFUL_LIFE_YEAR_31 { get; set; }
        public int? USEFUL_LIFE_PERD_31 { get; set; }
        public decimal? SCRAP_VALUE_31 { get; set; }
        public string DPRE_AREA_41 { get; set; }
        public string DPRE_KEY_41 { get; set; }
        public string USEFUL_LIFE_TYPE_41 { get; set; }
        public int? PLAN_LIFE_YEAR_41 { get; set; }
        public int? PLAN_LIFE_PERD_41 { get; set; }
        public int? USEFUL_LIFE_YEAR_41 { get; set; }
        public int? USEFUL_LIFE_PERD_41 { get; set; }
        public decimal? SCRAP_VALUE_41 { get; set; }
        public string DPRE_AREA_81 { get; set; }
        public string DPRE_KEY_81 { get; set; }
        public string USEFUL_LIFE_TYPE_81 { get; set; }
        public int? PLAN_LIFE_YEAR_81 { get; set; }
        public int? PLAN_LIFE_PERD_81 { get; set; }
        public int? USEFUL_LIFE_YEAR_81 { get; set; }
        public int? USEFUL_LIFE_PERD_81 { get; set; }
        public decimal? SCRAP_VALUE_81 { get; set; }
        public string DPRE_AREA_91 { get; set; }
        public string DPRE_KEY_91 { get; set; }
        public string USEFUL_LIFE_TYPE_91 { get; set; }
        public int? PLAN_LIFE_YEAR_91 { get; set; }
        public int? PLAN_LIFE_PERD_91 { get; set; }
        public int? USEFUL_LIFE_YEAR_91 { get; set; }
        public int? USEFUL_LIFE_PERD_91 { get; set; }
        public decimal? SCRAP_VALUE_91 { get; set; }
        public string GUID { get; set; }
        public string STATUS { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string MESSAGE { get; set; }
        public string DELETE_FLAG { get; set; }
        public string CREATE_BY { get; set; }

        public int Cnt { get; set; }

        public string COST_NAME { get; set; }
        public string RESP_COST_NAME { get; set; }
        public string ASSET_GROUP { get; set; }

        public string OPTION { get; set; }

        public string WBS_DESCRIPTION { get; set; }

        public string VIEW { get; set; }

        public string DPRE_TYPE_DESC_BC { get; set; }
        public string DPRE_TYPE_DESC_VL { get; set; }
        public string DPRE_TYPE_DESC_NA { get; set; }

        public List<SystemModel> assetClassOutsource { get; set; }
    }


}
