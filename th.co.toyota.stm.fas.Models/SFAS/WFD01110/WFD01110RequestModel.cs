﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110OnProcessModel : WFD01110BaseModel
    {
       public string REQUEST_TYPE_DESC { get; set; }
        public int DIFF_DATE { get; set; }

        public string LAST_APPRV_DATE { get; set; }
        
    }

    public class WFD01110OnHandAECModel : WFD01110BaseModel
    {
        public string REQUEST_TYPE_DESC { get; set; }
        public int DIFF_DATE { get; set; }

        public string LAST_APPRV_DATE { get; set; }

    }
}
