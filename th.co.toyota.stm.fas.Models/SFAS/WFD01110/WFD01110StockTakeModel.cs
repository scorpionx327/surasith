﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110StockTakeModel
    {
        public string ASSET_NO { get; set; }
        public string ASSET_NAME { get; set; }
        public string COST_CENTER_OWNER { get; set; }
        public string COST_CENTER_FOUND { get; set; }
        public string FOUND_BY { get; set; }
    }
}
