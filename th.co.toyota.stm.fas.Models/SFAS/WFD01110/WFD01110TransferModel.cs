﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110TransferModel : WFD01110BaseModel
    {
       
        public string TRNF_MAIN_TYPE { get; set; }
        public string TRANSFER_FROM { get; set; }
        public string TRANSFER_TO { get; set; }
      

        public string TRANSFER_FROM_HINT { get; set; }
        public string TRANSFER_TO_HINT { get; set; }
    }




}