﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110TotalRequestSummary
    {
        public string REQUEST_TYPE { get; set; }
        public string REQUEST_TYPE_NAME { get; set; }
        public int? STM_CNT { get; set; }
        public int? TDEM_CNT { get; set; }
        public int? TMT_CNT { get; set; }
        public int? TOTAL { get; set; }
    }
}
