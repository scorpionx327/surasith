﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110SearchRequestModel
    {
        public string EMP_CODE { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string COMPANY { get; set; }
        public string DOC_TYPE { get; set; }

    }
}
