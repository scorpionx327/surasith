﻿namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110BaseModel
    {
        public int ROWNUMBER { get; set; }
        public string COMPANY { get; set; }
        public string DOC_NO { get; set; }

        public string COST_CODE { get; set; }
        public string COST_NAME_HINT { get; set; }
        public string CURRENT_STATUS { get; set; }
        public string REQUEST_DATE { get; set; }

        public string REQUESTOR { get; set; }
        public string REQUEST_TYPE { get; set; }

    }
    public class WFD01110SettlementModel : WFD01110BaseModel
    {
        
        public int FINAL_CNT { get; set; }
        public decimal AMOUNT { get; set; }
    }

    //Reclassification
    public class WFD01110ReclassificationModel : WFD01110BaseModel
    {
        public decimal AMOUNT_POSTED { get; set; }
       
        public int ASSET_CNT { get; set; }
       
       
    }

    //Asset Change
    public class WFD01110AssetChangeModel : WFD01110BaseModel
    {
        public int ASSET_CNT { get; set; }

    }

    //Retirement
    public class WFD01110RetirementModel : WFD01110BaseModel
    {
        public string DISP_TYPE { get; set; }
     
      
        public int ASSET_CNT { get; set; }
     
      
        public decimal TOTAL_NET_BOOK { get; set; }

    }

    //Imparment
    public class WFD01110ImpairmentModel : WFD01110BaseModel
    {
        public decimal AMOUNT { get; set; }
       
       
        public int ASSET_CNT { get; set; }
      
     
        public decimal NEW_AMOUNT { get; set; }
       
    }

    //Physical Count
    public class WFD01110PhysicalCountModel : WFD01110BaseModel
    {
        
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }

        public int ASSET_CNT { get; set; }
     
 
    }
}