﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110TotalPerTabModel
    {
        public int? ONHAND_AEC_CNT { get; set; }
        public int? NEW_ASSET_CNT { get; set; }
        public int? RECLASS_CNT { get; set; }
        public int? RETIREMENT_CNT { get; set; }
        public int? INFO_CHANGE_CNT { get; set; }
        public int? SETTLEMENT_CNT { get; set; }
        public int? IMPAIRMENT_CNT { get; set; }
        public int? REPRINT_CNT { get; set; }
        public int? STOCK_TAKE_CNT { get; set; }
        public int? TRANSFER_CNT { get; set; }
      
        public int? ONPROC_CNT { get; set; }

    }
}
