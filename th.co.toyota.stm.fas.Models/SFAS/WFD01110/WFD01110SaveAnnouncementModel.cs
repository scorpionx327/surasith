﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110SaveAnnouncementModel
    {
        public string COMPANY { get; set; }
        public string ANNOUNCE_DESC { get; set; }
        public string ANNOUNCE_URL { get; set; }
        public string DISPLAY_FLAG { get; set; }
        public string ANNOUNCE_TYPE { get; set; }
        public string CREATE_BY { get; set; }
        public HttpPostedFileBase FILE { get; set; }
    }
}
