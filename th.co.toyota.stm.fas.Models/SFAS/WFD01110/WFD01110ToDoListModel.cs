﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01110
{
    public class WFD01110ToDoListModel
    {
        public int COUNT_OF_UPLOAD { get; set; }
        public int COUNT_OF_PRINT_TAG { get; set; }
        public int COUNT_OF_MARK_LOCATION { get; set; }

        public string PRINT_CONDITION { get; set; }
        public string UPLOAD_CONDITION { get; set; }
    }

    public class WFD01110ToDoListFASupModel
    {
        public int COUNT_OF_UPLOAD { get; set; }
        public int COUNT_OF_PRINT_TAG { get; set; }
        public int COUNT_OF_MARK_LOCATION { get; set; }

        public string PRINT_CONDITION { get; set; }
        public string UPLOAD_CONDITION { get; set; }
    }
}
