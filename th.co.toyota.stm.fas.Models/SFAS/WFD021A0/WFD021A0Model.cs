﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models
{
    public class WFD021A0Model : WFD0BaseRequestDetailModel
    {
        
        public int LINE_NO { get; set; }
        public string MESSAGE { get; set; }

        public string PARENT_ASSET_NO { get; set; }

        public string ASSET_DESC { get; set; }
        //public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_HINT { get; set; }

        public string SERIAL_NO { get; set; }

        //public string COST_CODE { get; set; }
        public string COST_NAME_HINT { get; set; }

        //public string RESP_COST_CODE { get; set; }
        public string RESP_COST_NAME_HINT { get; set; }

        public string WBS_BUDGET { get; set; }
        public string WBS_BUDGET_NAME { get; set; }
        public string LOCATION { get; set; }
        public string LOCATION_HINT { get; set; }
        public string INVEST_REASON { get; set; }
        public string INVEST_REASON_HINT { get; set; }
        //public string MINOR_CATEGORY { get; set; } //EVA_1
        public string MINOR_CATEGORY_HINT { get; set; }
        public string BOI_NO { get; set; } //EVA_4
        public string BOI_NO_HINT { get; set; }
       
        public string QUOTATION { get; set; }
    }
}
