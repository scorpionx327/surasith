﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02160
{
    public class WFD02160MapLocConditionModel
    {
        public string COMPANY { get; set; }
        public string EMP_CODE { get; set; }
        public string COST_CODE { get; set; }
        public string MAP_PATH { get; set; }
        public string UPDATE_DATE { get; set; } // format ddMMyyyyHHmmssfff
      
        public HttpPostedFileBase FILE { get; set; }
    }
    public class WFD02160ResultModel
    {
        public WFD02160MapLocModel MapLayOut;
        public List<WFD02160AssetLoc> AssetLoc;
    }
    public class WFD02160MapLocModel
    {
        public string COST_CODE { get; set; }
        public string MAP_PATH { get; set; }   // Map location 
        public string MAP_LOCATION_FULL_PATH { get; set; }   // Map location 
        public string UPDATE_DATE { get; set; } // format ddMMyyyyHHmmssfff
    }

    public class WFD02160AssetLoc
    {
        public string COST_CODE { get; set; }
        public string ASSET_NO { get; set; }
        public string X_POINT { get; set; }
        public string Y_POINT { get; set; }
        public int SEQ { get; set; }

    }
    public class WFD02160AssetModel
    {
        public string EMP_CODE { get; set; }
        public string COMPANY { get; set; }
        public string COST_CODE { get; set; }
      
        public string ITEMS { get; set; } //should be string because
        public string Display { get; set; }
        public string MINOR_CATEGORY { get; set; }
      
        public string ASSET_NO { get; set; }
        public string ASSET_NAME { get; set; }  //Pitiphat CR-B-011 201709
        public string ASSET_HINT { get; set; }
        public string ASSET_MAP_HINT { get; set; }
        public string MAP_STATUS { get; set; }
        public string SELECTED { get; set; }
        
        public string BG_COLOR { get; set; }

        public string X_POINT { get; set; }
        public string Y_POINT { get; set; }
        public string DEGREE { get; set; }  //Pitiphat CR-B-011 2017009
        public int SEQ { get; set; }
    }
}
