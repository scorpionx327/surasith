﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02630
{
    public class WFD02630SearchModel
    {
        public string COMPANY { get; set; }
        public string YEAR { get; set; }
        public string ROUND { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string EMP_CODE { get; set; }
        public string ISFAADMIN { get; set; }
    }
}
