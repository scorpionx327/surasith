﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02630
{
    public class WFD02630CalendarModel
    {
        public string title { get; set; }
        public string description { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string borderColor { get; set; }
        public string url { get; set; }
        public string className { get; set; }

        public string IS_SEND_REQUEST { get; set; }
    }
    public class WFD2630DataAsOfModel
    {
        public string  DATE_AS_OF { get; set; }
        public string ACTUAL { get; set; }
        public string TOTAL { get; set; }
        public string PROGRESS_BAR_PERCEN { get; set; }
        public string CLASS_NAME { get; set; }
    }
    public class WFD2630OverAllModel
    {
        public string ACTUAL { get; set; }
        public string TOTAL { get; set; }
        public string PROGRESS_BAR_PERCEN { get; set; }
        public string CLASS_NAME { get; set; }
        public string IMAGE_TYPE { get; set; }
        public string IMGSRC { get; set; }
    }
    public class WFD02630FixedAssetModel
    {
        public List<WFD02630CalendarModel> Calendar;
        public WFD2630DataAsOfModel DateAsOf;
        public WFD2630OverAllModel OverAll;
        public WFD2630TargetDateModel TargetDate;
    }
    public class WFD2630TargetDateModel
    {
        public string TARGET_DATE_FROM { get; set; }
        public string TARGET_DATE_TO { get; set; }
    }
}
