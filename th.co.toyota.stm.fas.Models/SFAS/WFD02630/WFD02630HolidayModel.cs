﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02630
{
    public class WFD02630HolidayModel
    {
        public string STOCK_TAKE_KEY { get; set; }
        public DateTime HOLIDATE_DATE { get; set; }
    }
}