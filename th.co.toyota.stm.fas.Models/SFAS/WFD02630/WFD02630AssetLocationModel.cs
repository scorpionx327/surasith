﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02630
{
   public class WFD02630AssetLocationModel
    {
        public string CODE { get; set; }
        public string ASSET_LOCATION { get; set; }
    }
}
