﻿using System;
using System.Collections.Generic;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02510
{
    public class WFD02510Model
    {
        public List<WFD02510DetailModel> ASSET_LIST { get; set; }
        public WFD02510HeaderModel HEADER_LIST { get; set; }
        
        public string DOC_NO { get; set; }
        public string GUID { get; set; }
        public string USER_BY { get; set; }
        public string UPDATE_DATE { get; set; }

        public HttpPostedFileBase TEMP_FILE_DOC { get; set; }
        public string TEMP_FILE_DOC_NAME { get; set; }

        public string IS_BOI_ROLE_APPROVE { get; set; }
        public string IS_STORE_ROLE_APPROVE { get; set; }
        public string IS_FA_ROLE_APPROVE { get; set; }
        public string IS_SV_SUBMIT { get; set; }
    }
}
