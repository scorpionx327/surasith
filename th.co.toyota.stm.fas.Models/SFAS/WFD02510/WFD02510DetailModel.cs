﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02510
{
    public class WFD02510DetailModel : WFD0BaseRequestDetailModel
    {  
			
        

        //public Nullable<int> ROW_INDX { get; set; }
        //public string DATE_IN_SERVICE { get; set; }
        //public Nullable<decimal> RETIRE { get; set; }
        //public Nullable<decimal> QTY { get; set; }
        //public Nullable<decimal> PERCEN_RETIRE_OLD { get; set; }
        //public Nullable<decimal> QTY_OLD { get; set; }
        //public Nullable<decimal> COST_OLD { get; set; }
        //public Nullable<decimal> BOOK_VALUE_OLD { get; set; }
        //public Nullable<decimal> COST_DISP { get; set; }
        //public string BOOK_VALUE_SHOW { get; set; }
        //public string COST_DISP_SHOW { get; set; }
        //public string REQUIRE_QTY { get; set; }
        //public string QTY_TEXT_BOX { get; set; }
        //public string COST { get; set; }
        //public Nullable<decimal> BOOK_VALUE { get; set; }
        //public string ACCUM_DEPREC { get; set; }
        //public string USEFUL_LIFE { get; set; }
        //public string PHOTO_ATTACH { get; set; }
        //public string DISP_REASON { get; set; }
        //public string BOI_NO { get; set; }
        //public string BOI_CHECK_FLAG { get; set; }
        //public string BOI_ATTACH_DOC { get; set; }
        //public string STORE_CHECK_FLAG { get; set; }
        //public string STORE_ATTACH_DOC { get; set; }
        //public string PURPOSE { get; set; }
        //public string SOLD_TO { get; set; }
        //public Nullable<decimal> PRICE { get; set; }
        //public string INVOICE_NO { get; set; }
        //public string SALE_DATE { get; set; }
        //public Nullable<decimal> REMOVAL_COST { get; set; }
        //public string DISP_REASON_OTHER { get; set; }
        //public string DISP_REASON_SALE { get; set; }
        //public string STATUS { get; set; }
        //public string ORACLE_SENT_FLAG { get; set; }
        //public string ASSET_CATEGORY { get; set; }
        //public string REQUIRE_FLAG_DISP_REASON { get; set; }
        //public string DISABLE_DISP_REASON { get; set; }
        //public string REQUIRE_FLAG_BOI { get; set; }
        //public string DISABLE_BOI { get; set; }
        //public string REQUIRE_FLAG_STORE { get; set; }
        //public string DISABLE_STORE { get; set; }
        //public string FULLY_DP_FLAG { get; set; }
        //public HttpPostedFileBase PHOTO_FILE_DOC { get; set; }
        //public HttpPostedFileBase BOI_FILE_DOC { get; set; }
        //public HttpPostedFileBase STORE_FILE_DOC { get; set; }
        //public HttpPostedFileBase TEMP_FILE_DOC { get; set; }
        //public string TEMP_FILE_DOC_NAME { get; set; }

        //public string BOI_SECTION_VISIBLE { get; set; }
        //public string STORE_SECTION_VISIBLE { get; set; }
        //public string FA_SECTION_VISIBLE { get; set; }
        //public string GUID { get; set; }
        //public string SALE_DETAIL { get; set; }

        ////2017-06-15 Suphachai L.
        //public string PARENT_FLAG { get; set; }                             // Check parent flag Y=PARENT, N=CHILD
        //public Nullable<decimal> PARENT_AND_CHILD_BOOK_VALUE { get; set; }  // Sum book value parent and child
        //public Nullable<decimal> PARENT_AND_CHILD_COST { get; set; }        // Sum cost parent and child
        //public Nullable<decimal> PARENT_AND_CHILD_ACCUMULATE { get; set; }
        //public Nullable<decimal> COST_BY_ASSET { get; set; }
        //public Nullable<decimal> BOOK_VALUE_BY_ASSET { get; set; }
        //public Nullable<decimal> ACCUM_DEPREC_BY_ASSET { get; set; }
        //public string KEEP_BY { get; set; }    // CR4 Suphachai L. 2017-06-23 Env / Fam
        //public string SALE_PROCESS { get; set; }    // CR4 Suphachai L. 2017-06-23 Y , N
        //public string RECEIVE_FLAG { get; set; }    // CR4 Suphachai L. 2017-06-23 Y , N
        //public string REQUIRE_KEEP_BY { get; set; }
    }
}
