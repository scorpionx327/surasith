﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02510
{
    public class WFD02510AssetModel : WFD0BaseRequestDetailModel
    {
        //public string ACC_PRINCIPLE { get; set; }
        //public string DPRE_AREA { get; set; }
        //public string DOCUMENT_DATE { get; set; }
        //public string POSTING_DATE { get; set; }
        //public string REASON { get; set; }
        //public double? AMOUNT_POSTED { get; set; }
        //public int? PERCENTAGE { get; set; }
        //public string IMPAIRMENT_FLAG { get; set; }
        //public string ASSET_GROUP { get; set; }
        //public double? BF_CUMU_ACQU_PRDCOST_01 { get; set; }

        //public string DOC_NO { get; set; }
        //public string COMPANY { get; set; }
        //public string ASSET_NO { get; set; }
        //public string ASSET_SUB { get; set; }

        //public string ASSET_NAME { get; set; }
        //public string DATE_IN_SERVICE { get; set; }
        public Nullable<decimal> RETIRE { get; set; }
        public Nullable<decimal> QTY { get; set; }

        public Nullable<decimal> COST { get; set; }

        public Nullable<decimal> BOOK_VALUE { get; set; }

        public string ADJ_TYPE { get; set; }
        public Nullable<decimal> RETIRE_QTY { get; set; }
        public Nullable<decimal> RETIRE_COST { get; set; }

        public Nullable<decimal> RETIRE_BOOK_VALUE { get; set; }

        public Nullable<decimal> ACCUM_DEPREC { get; set; }

        public Nullable<decimal> USEFUL_LIFE { get; set; }

        public string PHOTO_ATTACH { get; set; }

        public string DISP_REASON { get; set; }

        public string BOI_NO { get; set; }

        public string INVEST_REASON { get; set; }

        public string BOI_ATTACH_DOC { get; set; }

        public string STORE_CHECK_FLAG { get; set; }

        public string RETIREMENT_TYPE { get; set; }

        public string SOLD_TO { get; set; }
        public Nullable<decimal> PRICE { get; set; }

        public string INVOICE_NO { get; set; }

        public string DETAIL_DATE { get; set; }
        public Nullable<decimal> REMOVAL_COST { get; set; }

        public string DETAIL_ATTACH { get; set; }

        public string DETAIL_REMARK { get; set; }

        public string DISP_REASON_OTHER { get; set; }
        public string LOSS_COUNTER_MEASURE { get; set; }

      

        public string COMPENSATION { get; set; }

        public string DELETE_FLAG { get; set; }


        public string ASSET_GROUP { get; set; }



        public string IS_MACHINE_LICENSE { get; set; }
        public string PHOTO_REQ { get; set; }

        public string DISP_TYPE { get; set; }
        public string DISPOSAL_BY { get; set; }        
        public string DISPOAL_TYPE { get; set; }
        public string PROCESS_STATUS { get; set; }
        public string USER_MISTAKE { get; set; }
        public string RECEIVE_FLAG { get; set; }
        public string DISP_MASS_REASON { get; set; }
        public string ROUTE_TYPE { get; set; }

        public bool ENABLE_TMC_DOC { get; set; } // Control Screen
        public bool ENABLE_DETAIL { get; set; }
        
        public bool IsBOIUpload { get; set; }

        public bool IsBOIView { get; set; }

        public bool IsDetailView { get; set; }

        public string EDIT_FLAG { get; set; }
        public string ONHAND_ROLE { get; set; }
        public int ROW_STATE { get; set; }
    }

    //public class WFD02510ReasonModel : WFD0BaseRequestDetailModel
    //{
    //    public string CODE { get; set; }
    //    public string VALUE { get; set; }
    //}

    //public class WFD02510StoreModel : WFD0BaseRequestDetailModel
    //{
    //    public string CODE { get; set; }
    //    public string VALUE { get; set; }
    //}

    public class WFD02510AttachDocModel : WFD0BaseRequestDetailModel
    {
        public int LINE_NO { get; set; }
        public string ATTACH_DOC { get; set; }
        public string ATTACH_TYPE { get; set; }
        public HttpPostedFileBase Photos { get; set; }
    }
}
