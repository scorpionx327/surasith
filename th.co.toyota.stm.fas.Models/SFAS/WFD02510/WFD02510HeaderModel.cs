﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02510
{
    public class WFD02510HeaderModel : WFD0BaseRequestDocModel
    {
        public WFD02510HeaderModel() {
            this.DetailList = new List<WFD02510AssetModel>();
        }
        public string COMPANY { get; set; }
        public string DISP_TYPE { get; set; } //Sale Scrap Donate
        //public string LOSS_COUNTER_MEASURE { get; set; }
        //public string USER_MISTAKE_FLAG { get; set; }
        //public string COMPENSATION { get; set; }
        public string DISP_MASS_REASON { get; set; }
        public string DISP_MASS_ATTACH_DOC { get; set; }
        public string MINUTE_OF_BIDDING { get; set; }
        public string HIGHER_APP_ATTACH { get; set; }
        
        public string ROUTE_TYPE { get; set; } // Normal / Mass
        
        public HttpPostedFileBase TEMP_FILE_DOC { get; set; }
        public string TEMP_FILE_DOC_NAME { get; set; }


        public string TOP_MANAGEMENT_FLAG { get; set; }
        public int TMC_ATTACH_CNT { get; set; }

        public List<WFD02510AssetModel> DetailList { get; set; }
    }
}
