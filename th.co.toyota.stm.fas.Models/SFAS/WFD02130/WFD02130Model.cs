﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.Models.BaseModel;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02130
{
    public class WFD02130UpdateDataModel
    {
        //For print Location
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string ASSET_NO { get; set; }
        public string COST_CODE { get; set; }
        public string PRINT_LOCATION { get; set; }
        public string BARCODE_SIZE { get; set; }
        // Tag Photo
        public string TAG_PHOTO { get; set; }

        //for Update location
        public string LOCATION_NAME { get; set; }
        public string UPDATE_DATE { get; set; } // format ddMMyyyyHHmmssfff

        public HttpPostedFileBase FILE { get; set; }
        public string base64Image { get; set; }
        public string PRINT_CONDITION { get; set; }
        public string extension { get; set; }
        public string STOCKTAKE_FREQUENCY { get; set; }
        public string PROCESS_STATUS { get; set; }
        public string EMP_REMARK { get; set; }
        public string COMPANY { get; set; }
        public string ASSET_SUB { get; set; }
        public string PLATE_SIZE { get; set; }
        public string PRINT_SIZE { get; set; }
        public string PLATE_TYPE { get; set; }
        public string LOCATION_REMARK { get; set; }

    }


   
    public class WFD02130_TB_M_ASSETS_H
    {
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_NAME { get; set; }
        public string ADTL_ASSET_NAME { get; set; }
        public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_NAME { get; set; }
        
        public string ASSET_GROUP { get; set; }
        public string SERIAL_NO { get; set; }
        public string TAG_NO { get; set; }
        public string BOI_NO { get; set; }
        public string BOI_NO_NAME { get; set; }
        public string MACHINE_LICENSE { get; set; }
        public string MACHINE_LICENSE_NAME { get; set; }
        public string MINOR_CATEGORY { get; set; }
        public string MINOR_CATEGORY_NAME { get; set; }
        public string DATE_IN_SERVICE { get; set; }
        public string PLATE_TYPE { get; set; }
        public string BARCODE_SIZE { get; set; }
        public int? STOCKTAKE_FREQUENCY { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_REMARK { get; set; }
        public string LOCATION_REMARK { get; set; }
      
        public string BARCODE { get; set; }
        public string BOI_FLAG { get; set; }
        public string MAP_STATUS { get; set; }
        public string PHOTO_STATUS { get; set; }
        public string PRINT_STATUS { get; set; }
        public string STATUS { get; set; }
        public string STATUS_DESC { get; set; }
        public string PROCESS_STATUS { get; set; }
        public int? PRINT_COUNT { get; set; }
        public string TAG_PHOTO { get; set; }
        public string INHOUSE_FLAG { get; set; }
        public int ITEM_SEQ { get; set; }
        public string LAST_SCAN_DATE { get; set; }
        public string AC_DETERMINE { get; set; }
        public string INVEN_NO { get; set; }
        public int QUANTITY { get; set; }
        public string BASE_UOM { get; set; }
        public string LAST_INVEN_DATE { get; set; }
        public string INVEN_INDICATOR { get; set; }
        public string INVEN_NOTE { get; set; }
        public string WBS_PROJECT { get; set; }
        public string WBS_BUDGET { get; set; }
        public string WBS_PROJECT_NAME { get; set; }
        public string WBS_BUDGET_NAME { get; set; }

        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string COST_CODE_NAME { get; set; }
        public string RESP_COST_CODE_NAME { get; set; }

        public string PLANT { get; set; }
        public string LOCATION { get; set; }
        public string LOCATION_NAME { get; set; }
        public string ROOM { get; set; }
        public string LICENSE_PLATE { get; set; }
        public string ASSET_STATUS { get; set; }
        public string FINAL_ASSET_CLASS { get; set; }
        public string FINAL_ASSET_CLASS_NAME { get; set; }
        public string ASSET_SUPER_NO { get; set; }
        public string INVEST_REASON { get; set; }
        public string VENDOR_CODE { get; set; }
        public string VENDOR_NAME { get; set; }
        public string MANUFACT_ASSET { get; set; }
        public string ORG_ASSET { get; set; }
        public string ORG_ASSET_SUB { get; set; }
        public string LEASE_AGREE_DATE { get; set; }
        public string LEASE_START_DATE { get; set; }
        public int? LEASE_LENGTH_YEAR { get; set; }
        public int? LEASE_LENGTH_PERD { get; set; }
        public string LEASE_TYPE { get; set; }
        public string LEASE_SUPPLEMENT { get; set; }
        public string LEASE_NUM_PAYMENT { get; set; }
        public int? LEASE_PAY_CYCLE { get; set; }
        public string LEASE_ADV_PAYMENT { get; set; }
        public int? LEASE_PAYMENT { get; set; }
        public int? LEASE_ANU_INT_RATE { get; set; }
        public string DPRE_AREA_01 { get; set; }
        public string DPRE_KEY_01 { get; set; }
        public int? USEFUL_LIFE_YEAR_01 { get; set; }
        public int? USEFUL_LIFE_PERD_01 { get; set; }
        public string DPRE_START_DATE_01 { get; set; }
        public decimal? SCRAP_VALUE_01 { get; set; }
        public decimal? SCRAP_VALUE_PERCENT_APC_01 { get; set; }
        public decimal? CUMU_ACQU_PRDCOST_01 { get; set; }
        public decimal? ACCU_DPRE_01 { get; set; }
        public decimal? DPRE_CUR_YEAR_01 { get; set; }
        public decimal? NET_VALUE_01 { get; set; }
        public decimal? CAPACITY_UOP_01 { get; set; }
        public decimal? LTD_PROD_UOP_01 { get; set; }
        public string DEACTIVATE_15 { get; set; }
        public string DPRE_AREA_15 { get; set; }
        public string DPRE_KEY_15 { get; set; }
        public int? USEFUL_LIFE_YEAR_15 { get; set; }
        public int? USEFUL_LIFE_PERD_15 { get; set; }
        public string DPRE_START_DATE_15 { get; set; }
        public decimal? SCRAP_VALUE_15 { get; set; }
        public decimal? SCRAP_VALUE_PERCENT_APC_15 { get; set; }
        public decimal? CUMU_ACQU_PRDCOST_15 { get; set; }
        public decimal? ACCU_DPRE_15 { get; set; }
        public decimal? DPRE_CUR_YEAR_15 { get; set; }
        public decimal? NET_VALUE_15 { get; set; }
        public decimal? CAPACITY_UOP_15 { get; set; }
        public decimal? LTD_PROD_UOP_15 { get; set; }
        public string DPRE_AREA_31 { get; set; }
        public string DPRE_KEY_31 { get; set; }
        public string USEFUL_LIFE_YEAR_31 { get; set; }
        public string USEFUL_LIFE_PERD_31 { get; set; }
        public string DPRE_START_DATE_31 { get; set; }
        public decimal? SCRAP_VALUE_31 { get; set; }
        public decimal? SCRAP_VALUE_PERCENT_APC_31 { get; set; }
        public decimal? CUMU_ACQU_PRDCOST_31 { get; set; }
        public decimal? ACCU_DPRE_31 { get; set; }
        public decimal? DPRE_CUR_YEAR_31 { get; set; }
        public decimal? NET_VALUE_31 { get; set; }
        public decimal? CAPACITY_UOP_31 { get; set; }
        public decimal? LTD_PROD_UOP_31 { get; set; }
        public string DPRE_AREA_41 { get; set; }
        public string DPRE_KEY_41 { get; set; }
        public string USEFUL_LIFE_YEAR_41 { get; set; }
        public string USEFUL_LIFE_PERD_41 { get; set; }
        public string DPRE_START_DATE_41 { get; set; }
        public decimal? SCRAP_VALUE_41 { get; set; }
        public decimal? SCRAP_VALUE_PERCENT_APC_41 { get; set; }
        public decimal? CUMU_ACQU_PRDCOST_41 { get; set; }
        public decimal? ACCU_DPRE_41 { get; set; }
        public decimal? DPRE_CUR_YEAR_41 { get; set; }
        public decimal? NET_VALUE_41 { get; set; }
        public decimal? CAPACITY_UOP_41 { get; set; }
        public decimal? LTD_PROD_UOP_41 { get; set; }
        public string DPRE_AREA_81 { get; set; }
        public string DPRE_KEY_81 { get; set; }
        public string USEFUL_LIFE_YEAR_81 { get; set; }
        public string USEFUL_LIFE_PERD_81 { get; set; }
        public string DPRE_START_DATE_81 { get; set; }
        public decimal? SCRAP_VALUE_81 { get; set; }
        public decimal? SCRAP_VALUE_PERCENT_APC_81 { get; set; }
        public decimal? CUMU_ACQU_PRDCOST_81 { get; set; }
        public decimal? ACCU_DPRE_81 { get; set; }
        public decimal? DPRE_CUR_YEAR_81 { get; set; }
        public decimal? NET_VALUE_81 { get; set; }
        public decimal? CAPACITY_UOP_81 { get; set; }
        public decimal? LTD_PROD_UOP_81 { get; set; }
        public string DPRE_AREA_91 { get; set; }
        public string DPRE_KEY_91 { get; set; }
        public string USEFUL_LIFE_YEAR_91 { get; set; }
        public string USEFUL_LIFE_PERD_91 { get; set; }
        public string DPRE_START_DATE_91 { get; set; }
        public decimal? SCRAP_VALUE_91 { get; set; }
        public decimal? SCRAP_VALUE_PERCENT_APC_91 { get; set; }
        public decimal? CUMU_ACQU_PRDCOST_91 { get; set; }
        public decimal? ACCU_DPRE_91 { get; set; }
        public decimal? DPRE_CUR_YEAR_91 { get; set; }
        public decimal? NET_VALUE_91 { get; set; }
        public decimal? CAPACITY_UOP_91 { get; set; }
        public decimal? LTD_PROD_UOP_91 { get; set; }
        public string IMP_INVOICE_1 { get; set; }
        public string IMP_INVOICE_2 { get; set; }
        public string IMP_DATE { get; set; }
        public string AORGOR_1 { get; set; }
        public string AORGOR_2 { get; set; }
        public string Free_Text { get; set; }
        public string CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }

        public string PROCESS_STATUS_DESC { get; set; }

        public string COST_PENDING { get; set; }

        public string DOC_NO { get; set; }

        public string PRINT_CONDITION { get; set; }

        public string PRINT_LOCATION { get; set; }

        public string BOI_UPDATE_DATE { get; set; }
    }

    public class WFD02130_TB_M_EMPLOYEE
    {
        public string PRINT_LOCATION { get; set; }
    }

    public class WFD02130BOIModel
    {
        public int Index { get; set; }
        public HttpPostedFileBase BOIFile { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string FullFileName { get; set; }
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string EMP_CODE { get; set; }
        public string UPDATE_DATE { get; set; }
    }

}
