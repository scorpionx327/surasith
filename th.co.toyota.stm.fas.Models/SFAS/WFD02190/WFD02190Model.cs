﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02190
{
    public class WFD02190Model
    {
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_NAME { get; set; }
        public string ASSET_CLASS { get; set; }
        public string ASSET_CLASS_HINT { get; set; }
        public string COST_CODE { get; set; }
        public string COST_NAME_HINT { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string RESP_COST_NAME_HINT { get; set; }

        public string WBS_PROJECT { get; set; }
        public string WBS_PROJECT_HINT { get; set; }

        public string WBS_BUDGET { get; set; }

        public string BOI_NO { get; set; }
        public string BOI_NO_HINT { get; set; }

        public string MINOR_CATEGORY { get; set; }
        public string INVEST_REASON { get; set; }
        public string LOCATION { get; set; }

        public string DATE_IN_SERVICE { get; set; }

        public string STATUS { get; set; }
        public string INVEST_REASON_HINT { get; set; }
        public string MINOR_CATEGORY_HINT { get; set; }

        public string REF_DOC_NO { get; set; }
        public string ALLOW_CHECK { get; set; }
        public string MACHINE_LICENSE { get; set; }

    }
}
