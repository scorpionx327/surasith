﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02190
{
    public class WFD02190SaveModel
    {
        public string COST_CODE { get; set; }
        public string SING_FLAG { get; set; }
        public string GUID { get; set; }
        public string USER_BY { get; set; }
        public string DOC_NO { get; set; }
        public string REQUEST_TYPE { get; set; }
        public List<WFD02190Model> DETIAL_ASSET { get; set; }        
    }

    public class WFD02190SearchModel
    {
        public string EMP_CODE { get; set; }

        public string ACT_ROLE { get; set; }
        
        public string REQUEST_TYPE { get; set; }
        public string COMPANY { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_SUB { get; set; }
        public string ASSET_NAME { get; set; }
        public string ASSET_CLASS { get; set; }
        public string ASSET_GROUP { get; set; }

        public string WBS_PROJECT { get; set; }
        public string WBS_BUDGET { get; set; }
        public string ASSET_PLATE_NO { get; set; }
        public string LOCATION { get; set; }
        public string ONLY_PARENT_ASSETS { get; set; }

        public string COST_CODE { get; set; }
        public string RESP_COST_CODE { get; set; }
        public string DATE_IN_SERVICE_START { get; set; }
        public string DATE_IN_SERVICE_TO { get; set; }

        public string INVEST_REASON { get; set; }
        public string SINGLE_FLAG { get; set; }
        public string GUID { get; set; }
        public string INVEST_REASON_HINT { get; set; }
        public string MINOR_CATEGORY_HINT { get; set; }

        public string searchOption { get; set; }
        public string MACHINE_LICENSE { get; set; }

        public bool? HasCostValue { get; set; }
    }
}
