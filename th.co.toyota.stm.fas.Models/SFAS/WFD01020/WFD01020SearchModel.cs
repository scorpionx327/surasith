﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.SFAS.WFD01020
{
    public class WFD01020ComboBoxModel
    {
        public WFD01020ComboBoxModel()
        {
            this.SelectItemOptions = new List<SelectListItem>();        
        }

        public List<SelectListItem> SelectItemOptions { get; set; }     
    }

    public class WFD01020ComboBoxValueModel {
        public string Value { get; set; }
    }
}
