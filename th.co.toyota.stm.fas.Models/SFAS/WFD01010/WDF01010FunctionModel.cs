﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01010
{
    public class WDF01010FunctionModel
    {
        public string FUNCTION_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
    }
}
