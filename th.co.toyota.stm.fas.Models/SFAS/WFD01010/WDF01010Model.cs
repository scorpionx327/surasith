﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Models.WFD01010
{
    public class WDF01010Model
    {
        public string SYSTEM_ID { get; set; }
        public string FUNCTION_ID { get; set; }
        public string STATUS { get; set; }
        public string APP_ID { get; set; }
        public string USER_ID { get; set; }
        public string FAVORITE_FLAG { get; set; }
        public string FROM { get; set; }
        public string TO { get; set; }

        public string ddlSystem { get; set; }
        public string ddlFunction { get; set; }
        public string ddlStatus { get; set; }
        public string txtAppID { get; set; }
        public string txtUserID { get; set; }
        public string txtStartTime { get; set; }
        public string txtEndTime { get; set; }
        public bool chkFavoriteFlag { get; set; }



        public string moduleID { get; set; }
        public string functionID { get; set; }


        public SelectList systemList { get; set; }
        
        public SelectList functionList { get; set; }
        
        public SelectList statusList { get; set; }
        
        
        public string favoriteFlag { get; set; }
        public SelectList levelList { get; set; }
        public string ddlLevel { get; set; }
        public string baseUrl { get; set; }

        public List<SelectListItem> ListSystem { get; set; }
        public List<SelectListItem> ListFunction { get; set; }
        public List<SelectListItem> ListStatus { get; set; }
        public List<SelectListItem> ListLevel { get; set; }

        //public List<ADMS102LOGMONITORINGVM_DDL> system { get; set; }
        //public List<ADMS102LOGMONITORINGVM_DDL> function { get; set; }
        //public List<ADMS102LOGMONITORINGVM_DDL> status { get; set; }
        //public List<ADMS102LOGMONITORINGVM_DDL> level { get; set; }
        public List<WFD01010ResultSearchModel> resultList { get; set; }
        public List<WDF01010DetailModel> detailList { get; set; }
        public string userCurrent { get; set; }
        
        public string hidAppID { get; set; }
        public string hidModuleName { get; set; }
        public string hidfunctionID { get; set; }
        public string hidBatchName { get; set; }
        //public string urlPath { get; set; }
        public string s_ddlSystem { get; set; }
        public string s_ddlFunction { get; set; }
        public string s_ddlStatus { get; set; }
        public string s_txtAppID { get; set; }
        public string s_txtUserID { get; set; }
        public string s_txtStartTime { get; set; }
        public string s_txtEndTime { get; set; }
        public bool s_chkFavoriteFlag { get; set; }
    }
    public class StructureDDL
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
}
