﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01010
{
    public class WDF01010DetailModel
    {
        //LOGGING_TIME DATETIME,
        //STATUS_DESC VARCHAR(500),
        //LEVEL_DESC VARCHAR(500), 		
        //[MESSAGE] VARCHAR(4000)

        //public Nullable<int> APP_ID { get; set; }
        //public string FAVORITE_FLAG { get; set; }
        //public string MODULE_NAME { get; set; }
        //public string BATCH_NAME { get; set; }
        //public string START_TIME { get; set; }
        //public string END_TIME { get; set; }
        //public string CREATE_BY { get; set; }
        //public string STATUS { get; set; }
        //public string DOC_NO { get; set; }
        //public string FILE_PATH { get; set; }
        //public string MESSAGE { get; set; }
        //public Boolean IS_SELECT { get; set; }

        public string LOGGING_TIME { get; set; }
        public string STATUS_DESC { get; set; }
        public string LEVEL_DESC { get; set; }
        public string MESSAGE { get; set; }
    }

}
