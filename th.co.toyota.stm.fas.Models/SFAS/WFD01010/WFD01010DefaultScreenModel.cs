﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01010
{
    public class WFD01010DefaultScreenModel
    {
        [Model(true, "System")]

        public string SYSTEM_ID { get; set; }
        public string FUNCTION_ID { get; set; }
        public string STATUS { get; set; }
        public string APP_ID { get; set; }
        public string USER_ID { get; set; }
        public string LEVEL { get; set; }
        public string DATE_START { get; set; }
        public string DATE_TO { get; set; }
        public string FAVORITE_FLAG { get; set; }
    }
}
