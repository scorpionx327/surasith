﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01130
{
    public class WFD01130AssetModel
    {
        public string ASSET_NO { get; set; }
        public string ASSET_NAME { get; set; }
    }
}
