﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace th.co.toyota.stm.fas.Models.WFD01130
{
    public class WFD01130SearchModel
    {
        public string EMP_CODE { get; set; }
        public string IS_SYS_ADMIN { get; set; }
        public string ASSET_NO { get; set; }
        public string TRANSACTION_TYPE { get; set; }
        public string DATE_SERVICE_FROM { get; set; }
        public string DATE_SERVICE_TO { get; set; }
        public string DOCUMENT_NO { get; set; }
        public string STATUS { get; set; }
        public List<SelectListItem> ListTransactionType { get; set; }
        public List<SelectListItem> ListStatus { get; set; }
        public string COMPANY { get; set; }
        public string ASSET_SUB { get; set; }
    }
}
