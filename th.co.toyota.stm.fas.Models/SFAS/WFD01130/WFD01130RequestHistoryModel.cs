﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD01130
{
    public class WFD01130RequestHistoryModel
    {
        public string TRAN_TYPE { get; set; }
        public string DOC_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string COST_CODE { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string POSTING_DATE { get; set; }
        public string TRANS_DATE { get; set; }
        public string DETAIL { get; set; }
        public string LATEST_APPROVE_NAME { get; set; }
        public string COMPANY { get; set; }
        public string ASSET_SUB { get; set; }

        //public string TRANS_DATE_FORMAT
        //{
        //    get
        //    {
        //        return String.Format("{0:dd-M-yyyy}", TRANS_DATE); ;
        //    }
        //}
        //public string POSTING_DATE_FORMAT
        //{
        //    get
        //    {
        //        return String.Format("{0:dd-M-yyyy}", POSTING_DATE); ;
        //    }
        //}
    }
}
