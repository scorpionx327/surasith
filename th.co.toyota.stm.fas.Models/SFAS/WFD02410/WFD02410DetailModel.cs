﻿using System;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02410
{
    public class WFD02410DetailModel : WFD01270.BaseSearchModel
    {

        public Nullable<int> ROW_INDX { get; set; }
        public string DATE_IN_SERVICE { get; set; }
        public string COST { get; set; }     
        public string BOOK_VALUE { get; set; }
        public string ACCUM_DEPREC { get; set; }
        public string TRNF_REASON { get; set; }
        public string BOI_NO { get; set; }
        public string BOI_CHECK_FLAG { get; set; }
        public string BOI_ATTACH_DOC { get; set; }     

        // Submit & Re-Submit + Login is Approve 
        public string REQUIRE_FLAG_TRNF_REASON { get; set; }
        public string DISABLE_TRNF_REASON { get; set; }

        // BOI State + (Login is BOI or FA)
        public string REQUIRE_FLAG_BOI { get; set; }
        public string DISABLE_BOI { get; set; }

       
        public string BOI_SECTION_VISIBLE { get; set; }
      
        public HttpPostedFileBase BOI_FILE_DOC { get; set; }
    }
}
