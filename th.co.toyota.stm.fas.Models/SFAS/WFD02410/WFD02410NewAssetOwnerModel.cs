﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.WFD02410
{
    public  class WFD02410NewAssetOwnerModel : WFD0BaseRequestDetailModel
    {
        public string SYS_EMP_CODE { get; set; }
        public string TRNF_TYPE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string EMP_DISPLAY { get; set; }

        public string COST_NAME { get; set; }
        public string RESP_COST_NAME { get; set; }
        //public string POST_CODE { get; set; }
        public string POST_NAME { get; set; }
        public string RESPONSIBILITY { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public string SECT_CODE { get; set; }
        public string SECT_NAME { get; set; }
        public string NEW_PHONE_NO { get; set; }
        public string CREATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        //public string UPDATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }

        public int TMC_ATTACH_CNT { get; set; }

        public string REQUIRE_PHONE { get; set; }
    }
}
