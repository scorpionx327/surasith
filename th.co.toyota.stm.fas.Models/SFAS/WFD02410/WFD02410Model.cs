﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace th.co.toyota.stm.fas.Models.WFD02410
{
    public class WFD02410HeaderModel : WFD0BaseRequestDocModel
    {
        public string COMPANY { get; set; }
        public string REQUEST_EMP_CODE { get; set; }
        public string NEW_COST_CODE { get; set; }
        public string NEW_RESP_COST_CODE { get; set; }
        public string NEW_EMP_CODE { get; set; }
        public string TRNF_TYPE { get; set; }
        public string TEL_NO { get; set; }
        public string RECEIVER { get; set; }

     
        public List<WFD02410Model> DetailList { get; set; }
    }
    public class WFD02410Model : WFD0BaseRequestDetailModel
    {   
        public string COST { get; set; }
        public string BOOK_VALUE { get; set; }
        public string TRNF_REASON { get; set; }
        public string TRNF_REASON_OTHER { get; set; }
        public string BOI { get; set; }
        public string INVEST_REASON_HINT { get; set; }
        public string BOI_NO { get; set; }
        public string BOI_CHECK_FLAG { get; set; }
        public string BOI_ATTACH_DOC { get; set; }
        public string PLANT { get; set; }
        public string LOCATION_CD { get; set; }
        public string LOCATION_NAME { get; set; }
        public string FUTURE_USE { get; set; }
        public string WBS_PROJECT { get; set; }
        public string WBS_PROJECT_DESC { get; set; }
        public string TO_EMPLOYEE { get; set; }
        public string LOCATION_DETAIL { get; set; }
        public string TRNF_EFFECTIVE_DT { get; set; }

        public string IS_MACHINE_LICENSE { get; set; }

        // public HttpPostedFileBase BOI_FILE_DOC { get; set; }
        public string TRANS_START_DATE { get; set; }

        public int BOI_ATTACH_CNT { get; set; }
        public string BOI_NO_HINT { get; set; }
    }
    public class WFD02410WBS
    {
        public string COMPANY_CODE { get; set; }
        public string WBS_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string PROJECT_DEFINITION { get; set; }        
    }
}
