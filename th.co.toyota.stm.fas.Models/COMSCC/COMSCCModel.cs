﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.COMSCC
{
    public class COMSCCModel
    {
        public string COST_CODE { get; set; }
        public string COST_NAME { get; set; }

        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string FW_CODE {get;set;}
        public string FW_NAME { get; set; }

        public string FS_CODE { get; set; }

        public string FS_NAME { get; set; }
        
        public string STATUS { get; set; }
    }
}
