﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models.COMSCC
{
    public class COMSCCDefaultScreenModel
    {
        //[Model(true, "Find")]
        public string COMPANY { get; set; }
        public string FIND { get; set; }
        public string EMP_CODE { get; set; }
        public string COST_CODE { get; set; }
        public string ALL_COST_CENTER { get; set; }
        public string AssetOwnerFlag { get; set; }
        public string PAGE_REQUEST { get; set; }
        
        public bool IsSearchRespCostCode { get; set; }
    }
}
