﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th.co.toyota.stm.fas.Models
{
    public class ModelAttribute : Attribute
    {
        private bool isTime = false;
        private bool required = false;
        private int maxLen = -1;
        private string fieldName = "";
        public string DateFormat = "";

        public virtual bool Required
        {
            get { return required; }
            set
            {
                required = value;
            }
        }
        public virtual int MaxLength
        {
            get { return maxLen; }
            set
            {
                maxLen = value;
            }
        }
        public virtual string FieldName
        {
            get { return fieldName; }
            set
            {
                fieldName = value;
            }
        }
        public bool IsTime
        {
            get
            {
                return isTime;
            }
            set
            {
                isTime = value;
            }
        }

        public ModelAttribute(bool Required)
        {
            this.required = Required;
        }
        public ModelAttribute(bool Required, string FieldName) : this(Required)
        {
            this.fieldName = FieldName;
        }
        public ModelAttribute(bool Required, string FieldName, int MaxLength) : this(Required, FieldName)
        {
            this.maxLen = MaxLength;
        }
        public ModelAttribute(bool Required, string FieldName, bool IsTime) : this(Required, FieldName)
        {
            this.isTime = IsTime;
        }

        public ModelAttribute(string DateFormat)
        {
            this.DateFormat = DateFormat;
        }
    }
}
