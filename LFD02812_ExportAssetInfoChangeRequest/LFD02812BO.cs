﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02812_FixedAssetsList
{
    class LFD02812BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02812DAO _dbconn = null;
        private DetailLogData _log = null;
        #endregion

        public LFD02812BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
            //BLoggingData.IProcessStatus = _iProcessStatus;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Export reprint request screen to excel";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02812DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);

                BusinessLogic();
                //BLoggingData.IProcessStatus = _iProcessStatus;

                BLoggingData.Status = eLogStatus.Successfully; //Test
            }
            catch (Exception ex) //cannot connect
            {
                BLoggingData.Status = eLogStatus.Error;
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void BusinessLogic()
        {
            try
            {
                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');
                if(_p.Length < 2)
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, "Parameter is incorrect");
                    BLogging.InsertDetailLog(_log);
                    return;
                }
                _dbconn = new LFD02812DAO();

                var _data = new WFD02120SearhConditionModel();
                _data.GUID = _p[0];
                _data.IsAECUser = !(string.IsNullOrEmpty(_p[1]) || _p[1] == "N");
                
                var _dt = _dbconn.GetFixedAssetResult(_data);

                if(_dt == null || _dt.Rows.Count == 0)
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                    return;
                }

                BLoggingData.ProcessStatus =  GenerateReport(_dt, _data.IsAECUser);

            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }
        private eLogLevel GenerateReport(DataTable _dt, bool _isAECUser)
        {
            try
            {
                string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }

                if (_isAECUser)
                {
                    cfgLayoutSection = ConfigurationManager.GetAppSetting("AECLayoutSection");
                    if (string.IsNullOrEmpty(cfgLayoutSection))
                    {
                        throw (new Exception(string.Format(CommonMessage.E_CONFIG, "AECLayoutSection")));
                    }
                }
                
                GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);

                
                _cls.DataSource = _dt;
                _cls.Execute();

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, _cls.DirectoryName, _cls.FileName);
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, _cls.DirectoryName + @"\" + _cls.FileName);
                return eLogLevel.Information;
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            
        }        

        //private void MergeExcelFile(List<string> lstPathFilename)
        //{
        //    try
        //    {
        //        FileInfo outputFile = new FileInfo(lstPathFilename[0]);
        //        ExcelPackage pckMerge = new ExcelPackage(outputFile);
        //        for (int inx = 1; inx < lstPathFilename.Count; inx++)
        //        {

        //            if (!string.IsNullOrEmpty(lstPathFilename[inx]))
        //            {
        //                FileInfo fileCopy = new FileInfo(lstPathFilename[inx]);
        //                ExcelPackage pckSheet = new ExcelPackage(fileCopy);

        //                string sheetName =  pckSheet.Workbook.Worksheets[1].Name;

        //                ExcelWorksheet sheetCopy = pckSheet.Workbook.Worksheets.Copy(sheetName, string.Format("Sheet{0}", inx.ToString()));
                      
        //        //Add sheet
        //            pckMerge.Workbook.Worksheets.Add(sheetName, sheetCopy);
        //            }
        //        }
               
        //        pckMerge.Save();

        //        // Delete File
        //        for (int inx = 1; inx < lstPathFilename.Count; inx++)
        //        {
        //            if (!string.IsNullOrEmpty(lstPathFilename[inx]))
        //            {
        //                FileInfo file = new FileInfo(lstPathFilename[inx]);
        //                file.Delete();
        //            }
        //        }

        //        string outputDirectoryName = lstPathFilename[0].Substring(0, lstPathFilename[0].LastIndexOf(@"\"));
        //        string outputFilename = lstPathFilename[0].Substring(lstPathFilename[0].LastIndexOf(@"\") + 1, lstPathFilename[0].Length - outputDirectoryName.Length - 1);

        //        // Insert log : File {1} is generated to {0}
        //        DetailLogData _detail = new DetailLogData();
        //        _detail.AppID = BLoggingData.AppID;
        //        _detail.Description = string.Format(SendingBatch.I_GENFILE_END, outputDirectoryName, outputFilename);
        //        _detail.Level = eLogLevel.Information;
        //        BLogging.InsertDetailLog(_detail);

        //        // Insert File Download
        //        BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, outputDirectoryName + @"\" + outputFilename);              
        //    }
        //    catch (Exception ex)
        //    {              
        //        _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
        //        throw ex;
        //    }
        //}
    }
}
