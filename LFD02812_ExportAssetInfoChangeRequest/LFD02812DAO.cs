﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;
using System.Configuration;

namespace LFD02812_FixedAssetsList
{
    public class LFD02812DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02812DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }     
        public DataTable GetFixedAssetResult(WFD02120SearhConditionModel data)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }

                var param = new List<SqlParameter>(); 
				SqlCommand cmd = new SqlCommand("sp_LFD02812_Export_Asset_Info_Change_Request");
                cmd.Parameters.AddWithValue("@GUID", _db.IfNull(data.GUID));
                
                cmd.CommandType = CommandType.StoredProcedure;
          
                var _dt = _db.GetData(cmd);
                return _dt;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            
        }
    }

    public class WFD02120SearhConditionModel
    {
        public string GUID { get; set; }
        public bool IsAECUser { get; set; }

    }
}
