﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using th.co.toyota.stm.fas.common;

namespace LFD02150_AssetNumberListWithPhoto
{
    class LFD02150DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02150DAO()
        {
            _strConn = ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataSet GetAssetDatawithPhotoReport(string EMP_CODE, string COST_CODE, string ASSET_NO,
                                                    string ASSET_NAME, string ASSET_STATUS, string PO_NO,
                                                    string SUPPLIER_NAME, string ASSET_CATEGORY, string FULLY,
                                                    string ONLY_PARENT, string DATE_IN_SERVICE_FROM, string DATE_IN_SERVICE_TO,
                                                    string ASSET_TYPE, string PRINT_STATUS,
                                                    string PHOTO_STATUS, string MAP_STATUS, string APL_ID, string COMPANY_CODE)
        {
            DataSet ds = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return ds;
                }

                EMP_CODE = nullToEmpty(EMP_CODE);
                COST_CODE = nullToEmpty(COST_CODE);
                ASSET_NO = nullToEmpty(ASSET_NO);
                ASSET_NAME = nullToEmpty(ASSET_NAME);
                ASSET_STATUS = nullToEmpty(ASSET_STATUS);
                PO_NO = nullToEmpty(PO_NO);
                SUPPLIER_NAME = nullToEmpty(SUPPLIER_NAME);
                ASSET_CATEGORY = nullToEmpty(ASSET_CATEGORY);
                FULLY = nullToEmpty(FULLY);
                ONLY_PARENT = nullToEmpty(ONLY_PARENT);
                DATE_IN_SERVICE_FROM = nullToEmpty(DATE_IN_SERVICE_FROM);
                DATE_IN_SERVICE_TO = nullToEmpty(DATE_IN_SERVICE_TO);
                ASSET_TYPE = nullToEmpty(ASSET_TYPE);
                PRINT_STATUS = nullToEmpty(PRINT_STATUS);
                PHOTO_STATUS = nullToEmpty(PHOTO_STATUS);
                MAP_STATUS = nullToEmpty(MAP_STATUS);
                APL_ID = nullToEmpty(APL_ID);
                COMPANY_CODE = nullToEmpty(COMPANY_CODE);

                SqlCommand cmd = new SqlCommand("sp_LFD02150_GetAssetDatawithPhotoReport");

                cmd.Parameters.AddWithValue("@EMP_CODE", EMP_CODE);
                if (!COST_CODE.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@COST_CODE", COST_CODE);
                }
                if (!ASSET_NO.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@ASSET_NO", ASSET_NO);
                }
                if (!ASSET_NAME.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@ASSET_NAME", ASSET_NAME);
                }
                if (!ASSET_STATUS.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@ASSET_STATUS", ASSET_STATUS);
                }
                if (!PO_NO.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@PO_NO", PO_NO);
                }
                if (!SUPPLIER_NAME.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@SUPPLIER_NAME", SUPPLIER_NAME);
                }
                //if (!SUB_TYPE.Equals(string.Empty))
                //{
                //    cmd.Parameters.AddWithValue("@SUB_TYPE", SUB_TYPE);
                //}
                if (!ASSET_CATEGORY.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@ASSET_CATEGORY", ASSET_CATEGORY);
                }
                if (!FULLY.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@FULLY", FULLY);
                }
                if (!ONLY_PARENT.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@ONLY_PARENT", ONLY_PARENT);
                }
                if (!DATE_IN_SERVICE_FROM.Equals(string.Empty))
                {
                    DATE_IN_SERVICE_FROM = "02-10-2017";//DD-MMM-YYYY
                    cmd.Parameters.AddWithValue("@DATE_IN_SERVICE_FROM", DATE_IN_SERVICE_FROM);
                }

                if (!DATE_IN_SERVICE_TO.Equals(string.Empty))
                {
                    DATE_IN_SERVICE_TO = "28-10-2017";//DD-MMM-YYYY
                    cmd.Parameters.AddWithValue("@DATE_IN_SERVICE_TO", DATE_IN_SERVICE_TO);
                }
                if (!ASSET_TYPE.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@ASSET_TYPE", ASSET_TYPE);
                }
                if (!PRINT_STATUS.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@PRINT_STATUS", PRINT_STATUS);
                }
                if (!PHOTO_STATUS.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@PHOTO_STATUS", PHOTO_STATUS);
                }
                if (!MAP_STATUS.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@MAP_STATUS", MAP_STATUS);
                }
                if (!APL_ID.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@APL_ID", APL_ID);
                }
                if (!COMPANY_CODE.Equals(string.Empty))
                {
                    cmd.Parameters.AddWithValue("@COMPANY", COMPANY_CODE);
                }

                cmd.CommandType = CommandType.StoredProcedure;
                ds = _db.GetDataSet(cmd);

                ds.DataSetName = "ReportDataSource";
                int ii = 0;
                foreach (DataTable table in ds.Tables)
                {
                    if (ii == 0) {
                        table.TableName = "ASSET";
                    }                  
                    ii++;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ds;
        }   
        
        private String nullToEmpty(String val)
        {
            return ("NULL".Equals(val)) ? "" : val;
        }   
    }
}
