﻿using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02150_AssetNumberListWithPhoto
{
    class LFD02150BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
        private int _iProcessStatus = -1;
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02150DAO _dbconn = null;
        private DetailLogData _log = null;

        IFormatProvider _culture = new CultureInfo("en-US", true);
        #endregion

        public LFD02150BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
           
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Asset number list with photo report";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02150DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                BLoggingData.ProcessStatus =  GetAssetDatawithPhotoReport();
              
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(this._batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private eLogLevel GetAssetDatawithPhotoReport()
        {
            try
            {
                Console.WriteLine("Get Data");

                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');

                _log4Net.WriteDebugLogFile(BatchParam);

                _dbconn = new LFD02150DAO();
                DataSet ds = new DataSet();

                int i = 0;
                string COMPANY_CODE = _p[i++];
                string EMP_CODE = _p[i++];
                string COST_CODE = _p[i++];
                string ASSET_NO = _p[i++];
                string ASSET_NAME = _p[i++];
                string ASSET_STATUS = _p[i++];
                string PO_NO = _p[i++];
                string SUPPLIER_NAME = _p[i++];
                string SUP_TYPE = _p[i++];
                string ASSET_CATEGORY = _p[i++];
                string FULLY = _p[i++];
                string ONLY_PARENT = _p[i++];
                string DATE_IN_SERVICE_FROM = _p[i++];
                string DATE_IN_SERVICE_TO = _p[i++];
                string ASSET_TYPE = _p[i++];
                string PRINT_STATUS = _p[i++];
                string PHOTO_STATUS = _p[i++];
                string MAP_STATUS = _p[i++];
                string IS_FA_ADMIN = _p[i++];
                string IS_SYS_ADMIN = _p[i++];
                string APL_ID = _p[i++];


                if (string.IsNullOrEmpty(COMPANY_CODE))
                    return eLogLevel.Error   ;

                string[] _companyList = COMPANY_CODE.Split('#');

                int _cnt = 0;
                List<string> _fileNameList = new List<string>();
                foreach (string _com in _companyList)
                {

                    ds = _dbconn.GetAssetDatawithPhotoReport(EMP_CODE,
                                                            COST_CODE,
                                                            ASSET_NO,
                                                            ASSET_NAME,
                                                            ASSET_STATUS,
                                                            PO_NO,
                                                            SUPPLIER_NAME,
                                                            ASSET_CATEGORY,
                                                            FULLY,
                                                            ONLY_PARENT,
                                                            DATE_IN_SERVICE_FROM,
                                                            DATE_IN_SERVICE_TO,
                                                            ASSET_TYPE,
                                                            PRINT_STATUS,
                                                            PHOTO_STATUS,
                                                            MAP_STATUS,
                                                            APL_ID,
                                                            _com);

                    _cnt += CountDataAllTable(ds) ;

                    if (CountDataAllTable(ds) > 0)
                    {
                        _log4Net.WriteDebugLogFile("Generate Report");
                        _log4Net.WriteDebugLogFile(_com);
                        var _rs = GenerateReport(_com, ds, _p.ToList<string>());
                        _fileNameList.Add(_rs);
                    }

                }

                if(_cnt == 0)
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }

                if (_fileNameList.Count == 0)
                {
                    return eLogLevel.Error;
                }
                if(_fileNameList.Count == 1)
                {
                    //Add download
                    BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID,
                       _fileNameList[0]);

                    return eLogLevel.Information;
                  
                }


                //Zip
                _log4Net.WriteDebugLogFile("Create Zip File");
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                string _zip = string.Format(System.Configuration.ConfigurationManager.AppSettings["ZipFile"],DateTime.Now);
                _zip = System.IO.Path.Combine(oDirectoryName, _zip);
                this.CreateZipFile(_fileNameList, _zip);
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID,
                       _zip);
                return eLogLevel.Information;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        } 
        XtraReport GetXtraReportObject(string LayoutSection, DataSet dataSource, List<string> Parameters)
        {
            var xtraReport = XtraReport.FromFile(LayoutSection, true);        
            xtraReport.DataSource = dataSource;

          
            return xtraReport;
        }    
     
        void GenerateFile(DataSet ds, List<string> Parameters, string DirectoryName, string FileName, string LayoutDirectory)
        {
            try
            {
                var xTraReport = GetXtraReportObject(LayoutDirectory, ds, Parameters);         
                var pdfBytes = Export2PDF(xTraReport, DirectoryName, FileName);             
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string GenerateReport(string _company, DataSet ds, List<string> Parameters)
        {
            try
            {
                //Get lay out section name
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config

                if (string.IsNullOrEmpty(cfgLayoutSection)){
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                if (!System.IO.File.Exists(cfgLayoutSection)) {
                    throw (new System.IO.FileNotFoundException(CommonMessage.E_FILE_CONFIG, cfgLayoutSection));
                }

                //Get output directory name
                string oDirectoryName = System.Configuration.ConfigurationManager.AppSettings["DirectoryName"]; //follow your config;  

                //Get PDF file name
                string oFileName = System.Configuration.ConfigurationManager.AppSettings["FilePrefix"]; //follow your config;
                oFileName = string.Format(oFileName, _company, DateTime.Now);
                oFileName = oFileName.Replace(".pdf", string.Empty);

                Console.WriteLine("Genereate File");
                //Gernerate PDF File
                GenerateFile(ds,
                    Parameters,
                    oDirectoryName,
                    oFileName,
                    cfgLayoutSection);

                string _fName = System.IO.Path.Combine(oDirectoryName, oFileName + ".pdf");

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, oDirectoryName, oFileName + ".pdf");
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, oDirectoryName + @"\" + oFileName + ".pdf");
                return _fName;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
            
        }
        private int CountDataAllTable(DataSet ds)
        {
            int CountRow = 0;
            if ((ds != null) && ds.Tables.Count > 0)
            {
                foreach (DataTable dt in ds.Tables)
                { CountRow += dt.Rows.Count; }
            }
            return CountRow;
        }
        private byte[] Export2PDF(XtraReport xtraReport, string DirectoryName, string FileName)
        {
            Console.WriteLine("Convert To PDF");
            string filePath = DirectoryName + @"\" + FileName + ".pdf";

            // Get its PDF export options.
            PdfExportOptions pdfOptions = xtraReport.ExportOptions.Pdf;

            // Export the report to PDF.
            xtraReport.ExportToPdf(filePath);

            return System.IO.File.ReadAllBytes(filePath);
        }

        private void CreateZipFile(List<string> _fileNameList, string _targetFileName)
        {
            using (FileStream zipFileToOpen = new FileStream(_targetFileName, FileMode.OpenOrCreate))
            {
                using (ZipArchive archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Create))
                {
                    foreach (string _fName in _fileNameList)
                    {
                        archive.CreateEntryFromFile(_fName, new System.IO.FileInfo(_fName).Name);
                    }
                }
            }
           
        }
    }
}
