﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using th.co.toyota.stm.fas.common;

namespace LFD02150_AssetNumberListWithPhoto
{
    class LFD02150Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleTitle(string _title);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static void Main(string[] args)
        {
            Log4NetFunction _log4net = new Log4NetFunction();
            string _modName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            string _procName = System.IO.Path.GetFileNameWithoutExtension(_modName);
            System.Diagnostics.Process[] _appProc = System.Diagnostics.Process.GetProcessesByName(_procName);
            
            /************************************   Check Multiple Running    ****************************************/
            string _cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
            if (!System.IO.File.Exists(_cfgFileName))
            {
                // message
                Console.WriteLine("Not found Configuration File : {0}", _cfgFileName);
                return;
            }
            Console.WriteLine("Start Batch");

            LFD02150BO cls = new LFD02150BO();
            if (args.Length >= 3) // manual
            {
                cls.BLoggingData.AppID = Convert.ToInt32(args[0]);
                cls.BLoggingData.BatchID = args[1];
                cls.BLoggingData.Arguments = args[2];
            }
            else //Not Allow Auto
            {
                //Log4net
                _log4net.WriteErrorLogFile("Invalid Batch Parameter");
                return;
            }

            string _batchID = System.Configuration.ConfigurationManager.AppSettings["BatchID"];
            if (_batchID != cls.BLoggingData.BatchID)
            {
                //Log4Net
                _log4net.WriteErrorLogFile("Invalid Batch ID");
                return;
            }
            SetConsoleTitle(_batchID);
            cls.Processing();
        }
    }
}
