﻿using System;
using System.Data.SqlClient;
using System.Data;
using th.co.toyota.stm.fas.common;

namespace LFD02450_ExportTransferRequest
{
    class LFD02450DAO
    {
        private string _strConn = string.Empty;
        private DataConnection _db;
        public LFD02450DAO()
        {
            _strConn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }
        public DataTable GetReprintRequestData(string _guid)
        {
            DataTable dt = null;
            try
            {
                _db = new DataConnection(_strConn);
                if (!_db.TestConnection())
                {
                    return dt;
                }
                SqlCommand cmd = new SqlCommand("sp_LFD02450_GetTransferRequestData");
                //cmd.Parameters.AddWithValue("@DOC_NO", _DocNO);
                cmd.Parameters.AddWithValue("@GUID", _guid);
                cmd.CommandType = CommandType.StoredProcedure;
                dt = _db.GetData(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dt;
        }
    }
}
