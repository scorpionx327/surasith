﻿using System;
using System.Data;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.common.Interface;

namespace LFD02450_ExportTransferRequest
{
    class LFD02450BO
    {
        #region "Variable Declare"
        public BatchLoggingData BLoggingData = null;
        public BatchLogging BLogging = null;
        public string UploadFileName = string.Empty;
        private string ConfigFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config";
        private string _batchName = string.Empty;
     
        private Log4NetFunction _log4Net = new Log4NetFunction();
        private LFD02450DAO _dbconn = null;
        private DetailLogData _log = null;
        #endregion

        public LFD02450BO()
        {
            BLoggingData = new BatchLoggingData();
        }

        public void Processing()
        {
            MailSending mail = null;
            BLoggingData.ProcessStatus =  eLogLevel.Error ;
            try
            {
                mail = new MailSending();
                _batchName = System.Configuration.ConfigurationManager.AppSettings["BatchName"];
                if (string.IsNullOrEmpty(_batchName))
                {
                    _batchName = "Export transfer request screen to excel";
                }
                BLogging = new BatchLogging();//Test connect data base
                _dbconn = new LFD02450DAO();
                BLoggingData.BatchName = _batchName;
                BLogging.StartBatchQ(BLoggingData);
                GetReprintRequestData();
                   
            }
            catch (Exception ex) //cannot connect
            {
                _log4Net.WriteErrorLogFile(ex.Message, ex);
                try
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.MSTD0067AERR, ex.Message);
                    BLogging.InsertDetailLog(_log);
                }
                catch (Exception exc)
                {
                    _log4Net.WriteErrorLogFile(exc.Message, exc);
                }
                mail.AppID = string.Format("{0}", BLoggingData.AppID);
            }
            finally
            {
                try
                {
                    BLogging.SetBatchQEnd(BLoggingData);
                    mail.SendEmailToAdministratorInSystemConfig(_batchName);
                }
                catch (Exception ex)
                {
                    _log4Net.WriteErrorLogFile(ex.Message, ex);
                }
            }
        }

        private void GetReprintRequestData()
        {
            try
            {
                string BatchParam = BLoggingData.Arguments.Replace("'", "");
                string[] _p = BatchParam.Split('|');
                _dbconn = new LFD02450DAO();
                DataTable dt = new DataTable();
                dt = _dbconn.GetReprintRequestData(_p[0]);
                if (dt != null && dt.Rows.Count > 0)
                {
                    BLoggingData.ProcessStatus  = GenerateReport(dt);
                }
                else
                {
                    _log = new DetailLogData();
                    _log.AppID = BLoggingData.AppID;
                    _log.Status = eLogStatus.Processing;
                    _log.Level = eLogLevel.Error;
                    _log.Favorite = false;
                    _log.Description = string.Format(CommonMessageBatch.DATA_NOT_FOUND);
                    BLogging.InsertDetailLog(_log);
                }
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
        }

        private eLogLevel GenerateReport(DataTable _dt)
        {
            try
            {
                string cfgFileName = System.Reflection.Assembly.GetExecutingAssembly().Location + ".config"; //don't edit
                string cfgLayoutSection = System.Configuration.ConfigurationManager.AppSettings["LayoutSection"]; //follow your config
                if (string.IsNullOrEmpty(cfgLayoutSection))
                {
                    throw (new Exception(string.Format(CommonMessage.E_CONFIG, "LayoutSection")));
                }
                GenerateFileClass _cls = new GenerateFileClass(cfgFileName, cfgLayoutSection);
                _cls.DataSource = _dt;
                _cls.Execute();

                // Insert log : File {1} is generated to {0}
                DetailLogData _detail = new DetailLogData();
                _detail.AppID = BLoggingData.AppID;
                _detail.Description = string.Format(SendingBatch.I_GENFILE_END, _cls.DirectoryName, _cls.FileName);
                _detail.Level = eLogLevel.Information;
                BLogging.InsertDetailLog(_detail);

                // Insert File Download
                BLogging.AddDownloadFile(BLoggingData.AppID, BLoggingData.ReqBy, BLoggingData.BatchID, _cls.DirectoryName + @"\" + _cls.FileName);
                return eLogLevel.Information;
            }
            catch (Exception ex)
            {
                _log4Net.WriteErrorLogFile(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message, ex);
                throw ex;
            }
           
        }

    }
}
