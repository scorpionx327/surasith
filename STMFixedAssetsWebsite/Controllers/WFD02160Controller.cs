﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.COMSCC;
using th.co.toyota.stm.fas.Models.WFD02160;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02160Controller :  BaseController
    {
        private WFD02160BO bo;
        protected WFD02160BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02160BO();
                return bo;
            }
        }

        // GET: WFD02160
        public ActionResult Index(string CostCode, string searchMode)
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            ViewBag.COMSCC_ITEM_PER_PAGE = GetItemPerPage("COMSCC");
            ViewBag.DafaultImage = BusinessLogic.GetImageDefalut();
            ViewBag.UserLogon = GlobalUser.UserSc2.TFASTEmployeeNo;

            if (null != searchMode && !"".Equals(searchMode))
            {
                ViewBag.SearchMode = searchMode;
            }

            //Message 
            ViewBag.NoSelectFile = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MCOM0017AERR, "file").VALUE;
            ViewBag.Err_NoInput = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Cost Center").VALUE;
            ViewBag.ConfirmDelete = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MCOM0018CFM, "Delete layout").VALUE;
            ViewBag.SaveSuccess = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0101AINF).VALUE;
            ViewBag.ConfirmSave = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0006ACFM).VALUE; //MSTD0006ACFM MSTD0085AINF
            ViewBag.UploadPhotoSuccess = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0085AINF, "Upload Photo").VALUE;
            ViewBag.DeleteSuccess = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0090AINF).VALUE;//
            ViewBag.ConfirmUploadPhoto = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0124ACFM).VALUE;
            ViewBag.ConfirmCancel = BusinessLogic.GetSystemMessage(Constants.MESSAGE.MCOM0007ACFM, "edit location map").VALUE; 

            COMSCCBO _ccBO = new COMSCCBO();

            COMSCCModel _m = new COMSCCModel();
            _m = _ccBO.getDefaultCostCode(ViewBag.UserLogon);
            ViewBag.DefaultCostCode = string.Empty;
            ViewBag.DefaultCostName = string.Empty;
            if (_m != null)
            {
                ViewBag.DefaultCostCode = _m.COST_CODE;
                ViewBag.DefaultCostName = _m.COST_NAME;
            }
            if (CostCode != null && CostCode != "") {
                COMSCCModel _default = new COMSCCModel();
                _default = _ccBO.getDetailCostCode(CostCode);
                if (_default != null)
                {
                    ViewBag.DefaultCostCode = _default.COST_CODE;
                    ViewBag.DefaultCostName = _default.COST_NAME;
                }
            }


            return View();
        }

        [HttpPost]
        public JsonResult UpdateMapLayout(WFD02160MapLocConditionModel con)
        {

            con.FILE = Request.Files[0];
            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BusinessLogic.UpdateMapLayout(con, Request));
        }

        [HttpPost]
        public JsonResult DeleteMapLayout(WFD02160MapLocConditionModel con)
        {
            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BusinessLogic.DeleteMapLayout(con));
        }

        [HttpPost]
        public JsonResult WFD02610SaveMapLayout(List<WFD02160AssetModel> con)
        {
             return BaseJson(BusinessLogic.WFD02610SaveMapLayout(con));
        }
        

        [HttpPost]
        public JsonResult ViewMapLocation(WFD02160MapLocConditionModel con)
        {

            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BusinessLogic.ViewMapLocation(con));
        }


        [HttpPost]
        public JsonResult ViewMapPoint(WFD02160MapLocConditionModel con)
        {
            
            return BaseJson(BusinessLogic.ViewMapPoint(con, GlobalUser.UserSc2.IsAECUser));
        }
        [HttpPost]
        public JsonResult CheckFileUpload(CheckFileUploadModel data)
        {
            var d = BusinessLogic.CheckValidateUploadFile(data);
            return BaseJson(d);
        }

        public ActionResult Test()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetMinorCategory(string COMPANY)
        {

            var _rs = this.BusinessLogic.LoadMinorCategory(new WFD02160MapLocConditionModel() { COMPANY = COMPANY });
            return BaseJson(_rs);
        }
    }
}