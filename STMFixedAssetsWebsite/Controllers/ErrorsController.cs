﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace th.co.toyota.stm.fas.Controllers
{
    public class ErrorsController : Controller
    {
        // GET: /Error/
        public ActionResult Http403(string exception)
        {
            ViewBag.error = exception;
            return View();
        }
        public ActionResult Http404(string exception)
        {
            ViewBag.error = exception;
            return View();
        }
        public ActionResult Http500(string exception)
        {
            ViewBag.error = exception;
            return View();
        }
        public ActionResult ErrorPage(string MessageCode)
        {

            if (!string.IsNullOrEmpty(MessageCode))
                ViewBag.error = (new BusinessObject.WFD01110BO()).GetMessage(MessageCode).MESSAGE_TEXT;
            else
                ViewBag.error = "No Permission to access";

            return View();
        }
        public ActionResult ErrorDirect(string exception)
        {
            return View();
        }
    }
}