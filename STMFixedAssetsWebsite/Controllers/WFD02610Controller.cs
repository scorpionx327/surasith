﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02610;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02610Controller : BaseController
    {
        private WFD02610BO bo;
        protected WFD02610BO BO
        {
            get
            {
                if (bo == null) bo = new WFD02610BO();
                return bo;
            }
        }

        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD02610");
            return View();
        }

        [HttpPost]
        public JsonResult WFD02610_getDefaultScreenData(WFD02610DefaultScreenModel con)
        {
            return BaseJson(BO.GetDefaultScreenData(con));
        }

        [HttpPost]
        public JsonResult WFD02610_Search(WFD02610DefaultScreenModel con, PaginationModel page)
        {
            string UserLogon = GlobalUser.UserSc2.TFASTEmployeeNo;
            string IsFAAdmin = "N";
            if (GlobalUser.UserSc2.IsAECUser)
            {
                IsFAAdmin = "Y";
            }
            else
            {
                IsFAAdmin = "N";
            }
            return SearchJson(BO.Search(UserLogon, IsFAAdmin,con, page));
        }

        [HttpPost]
        public JsonResult WFD02610_GetFinishPlanData(List<WFD02610SearchModel> data)
        {
            var d = BO.GetFinishPlanData(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult WFD02610_FinishPlan(WFD02610FinishPlanConditionModel data)
        {
            data.USER = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.FinishPlan(data));
        }

        //[HttpPost]
        //public BaseJsonResult WFD02610_ValidateLoadFinishPlan(WFD02610SearchModel searchData, WFD02610FinishPlanModel chkData)
        //{

        //}

        [HttpPost]
        public JsonResult WFD02610_ValidateExportToOracle(List<WFD02610SearchModel> data, string processName, string batchId)
        {
            return BaseJson(BO.ValidateExportToOracle(data, processName, batchId));
        }
    }
}