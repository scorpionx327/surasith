﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.Common;
using System.IO;
using th.co.toyota.stm.fas.Models.WFD02640;
using th.co.toyota.stm.fas.Models.COMSCC;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Shared;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01170Controller : BaseController
    {
        // GET: WFD01270
        private WFD01170BO _bo;
        protected WFD01170BO BusinessLogic
        {
            get
            {
                if (_bo == null) _bo = new WFD01170BO();
                return _bo;
            }
        }
        private BusinessObject.Common.SystemBO systembo = new BusinessObject.Common.SystemBO();

        public RequestPageInfoModels GetFunctionID(string _RequestType, string _flowType)
        {

            var _rs = systembo.SelectSystemDatas("SYSTEM_CONFIG", "SCREEN_REQUEST_INFO");

            if (_rs.Find(x => x.CODE == _flowType) != null)
            {
                var _data = _rs.Find(x => x.CODE == _flowType);
                return new RequestPageInfoModels() { FunctionID = _data.VALUE, FunctionTitle = _data.REMARKS };
            }

            if (_rs.Find(x => x.CODE == _RequestType) != null)
            {
                var _data = _rs.Find(x => x.CODE == _RequestType);
                return new RequestPageInfoModels() { FunctionID = _data.VALUE, FunctionTitle = _data.REMARKS };
            }
            return new RequestPageInfoModels() { };

        }
        public ActionResult Index(string DocNo, string RequestType, string FlowType, string guid, string SourceDocNo, string SourceRole)
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            bool isCopyToNewMode = false;

            this._InitialValue();
            if (!string.IsNullOrEmpty(guid))
            {
                ViewBag.GUID = guid;
                ViewBag.SOURCE_DOC_NO = SourceDocNo;
                isCopyToNewMode = true;
                ViewBag.COPY_MODE = "Y";
                ViewBag.SOURCE_ROLE = SourceRole;
            }
            else
            {
                ViewBag.GUID = Guid.NewGuid();
            }

            var _doctemp = new Models.BaseModel.TB_R_REQUEST_H();

            var _reqInfo = new RequestPageInfoModels();
            if (string.IsNullOrEmpty(DocNo))
            {
                if (!this.BusinessLogic.IsAllowCreateDocument(new WFD0BaseRequestDocModel() { USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo }))
                {
                    return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
                }

                //Check User authenication

                _reqInfo = this.GetFunctionID(RequestType, FlowType);
                ViewBag.FUNCTION_ID = _reqInfo.FunctionID;
                ViewBag.ITEM_PER_PAGE = GetItemPerPage(_reqInfo.FunctionID);
                ViewBag.ScreenTitle = _reqInfo.FunctionTitle;
                ViewBag.RequestType = RequestType;
                ViewBag.STATUS = "00";
                ViewBag.FlowType = FlowType;

                string defaultCompany = GlobalUser.UserSc2.DefaultCompany;
                if (isCopyToNewMode && !string.IsNullOrEmpty(SourceDocNo))
                {
                    //GET COMPANY ON SOURCEDOC
                    defaultCompany = SourceDocNo.Split('-')[0];
                }

                ViewBag.COMPANY = defaultCompany;
            }
            else
            {
                var _docInfo = this.BusinessLogic.GetRequestHeader(
                   new WFD0BaseRequestDocModel() { DOC_NO = DocNo });
                if (_docInfo == null)
                {
                    return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
                }

                this.BusinessLogic.InitializeApproverList(new WFD0BaseRequestDocModel()
                {
                    DOC_NO = DocNo,
                    GUID = string.Format("{0}", ViewBag.GUID)
                });



                _doctemp = _docInfo;
                ViewBag.DOC_NO = _docInfo.DOC_NO;
                ViewBag.COMPANY = _docInfo.COMPANY;
                ViewBag.STATUS = _docInfo.STATUS;

                _reqInfo = this.GetFunctionID(_docInfo.REQUEST_TYPE, _docInfo.APPROVE_FLOW_TYPE);
                ViewBag.FUNCTION_ID = _reqInfo.FunctionID;
                ViewBag.ITEM_PER_PAGE = GetItemPerPage(_reqInfo.FunctionID);
                ViewBag.ScreenTitle = _reqInfo.FunctionTitle;
                RequestType = _docInfo.REQUEST_TYPE;
                ViewBag.RequestType = _docInfo.REQUEST_TYPE;
                ViewBag.FlowType = _docInfo.APPROVE_FLOW_TYPE;

                ViewBag.UpdateDate = _docInfo.UPDATE_DATE;
            }

            if (string.IsNullOrEmpty(ViewBag.FlowType))
            {
                ViewBag.FlowType = string.Format("{0}N", ViewBag.RequestType);
            }


            ViewBag.UrlAddAsset = Url.Action("AddAsset", _reqInfo.FunctionID);
            ViewBag.UrlClearAssetsList = Url.Action("ClearAssetsList", _reqInfo.FunctionID);

            ViewBag.UrlGetAssetList = Url.Action("GetAssetList", _reqInfo.FunctionID);
            ViewBag.UrlDeleteAsset = Url.Action("DeleteAsset", _reqInfo.FunctionID);
            ViewBag.UrlPrepare = Url.Action("Prepare", _reqInfo.FunctionID);
            ViewBag.UrlUpdateAssetList = Url.Action("UpdateAssetList", _reqInfo.FunctionID);
            ViewBag.UpdateAmount = Url.Action("UpdateAmount", _reqInfo.FunctionID);

            ViewBag.UrlResend = Url.Action("Resend", _reqInfo.FunctionID);
            ViewBag.GetDocCount = Url.Action("GetDocCount", _reqInfo.FunctionID);
            ViewBag.UpdateCapDate = Url.Action("UpdateCapDate", _reqInfo.FunctionID);

            ViewBag.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;


            //Check User Permission
            var _permission = this.BusinessLogic.GetPermission(new WFD0BaseRequestDocModel()
            {
                DOC_NO = DocNo,
                USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo,
                REQUEST_TYPE = RequestType
            });

            //Orveride Approve/Reject permission for Retirement (for support approve after Status = Completed)
            if (RequestType == "D")
            {
                var _permissionRetirement = this.BusinessLogic.GetPermissionRetirement(new WFD0BaseRequestDocModel()
                {
                    DOC_NO = DocNo,
                    USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo,
                    REQUEST_TYPE = RequestType
                });
                if (_permissionRetirement != null)
                {
                    _permission.AllowEdit = (_permissionRetirement.AllowEdit == null) ? _permission.AllowEdit : _permissionRetirement.AllowEdit;
                    _permission.ActionMark = (_permissionRetirement.ActionMark == null) ? _permission.ActionMark : _permissionRetirement.ActionMark;
                    _permission.ActionRoleIndex = _permissionRetirement.ActionRoleIndex;

                    _permission.AllowApprove = (_permissionRetirement.AllowApprove == null) ? _permission.AllowApprove : _permissionRetirement.AllowApprove;
                    _permission.AllowReject = (_permissionRetirement.AllowReject == null) ? _permission.AllowReject : _permissionRetirement.AllowReject;

                    _permission.AllowResend = (_permissionRetirement.AllowResend == null) ? _permission.AllowResend : _permissionRetirement.AllowResend;
                    _permission.AllowCopy = (_permissionRetirement.AllowCopy == null) ? _permission.AllowCopy : _permissionRetirement.AllowCopy;

                    _permission.AllowClose = (_permissionRetirement.AllowClose == null) ? _permission.AllowClose : _permissionRetirement.AllowClose;

                    _permission.AllowAcknowledge = (_permissionRetirement.AllowAcknowledge == null) ? _permission.AllowAcknowledge : _permissionRetirement.AllowAcknowledge;

                }
                //2019.12.29
                if (_permission.AllowApprove == "Y")
                    _permission.Mode = "A";

            }

            _permission.Company = _doctemp.COMPANY;
            _permission.UPDATE_DATE = _doctemp.UPDATE_DATE;

            if (_permission == null ||  (_permission.Mode == "N")) //No Permission
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            if (_permission.IsHigher == "Y")
            {
                ViewBag.WarningMessage = "";
            }
            // _permission.AllowGenAssetNo = (_doctemp.FINISH_FLOW == "Y" && _permission.IsAECUser == "Y") ? "Y" : "N";

            //_permission.AllowRegen = "N";

            //var _rePermission = this.BusinessLogic.GetDetailPermission(new WFD0BaseRequestDocModel() {
            //      DOC_NO = _doctemp.DOC_NO,
            //      REQUEST_TYPE = _doctemp.REQUEST_TYPE
            //}, _permission);

            return View(_permission);
        }

        private void _InitialValue()
        {
            COMSCCBO _ccBO = new COMSCCBO();
            COMSCCModel _m = new COMSCCModel();
            _m = _ccBO.getDefaultCostCode(GlobalUser.UserSc2.TFASTEmployeeNo);
            ViewBag.DefaultCostCode = string.Empty;
            if (_m != null)
            {
                ViewBag.DefaultCostCode = _m.COST_CODE;
            }
            //Row Per page
            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01170");

            ViewBag.ACRCode = Constants.Role.AECUser;
            //Message
            var _confMessage = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, null).VALUE;
            ViewBag.ConfirmSubmit = string.Format(_confMessage, "Submit ");
            ViewBag.ConfirmReSubmit = string.Format(_confMessage, "Re-submit ");
            ViewBag.ConfirmApprove = string.Format(_confMessage, "Approve ");
            ViewBag.ConfirmReject = string.Format(_confMessage, "Reject ");

            ViewBag.ConfirmClose = string.Format(_confMessage, "Close ");
            ViewBag.ConfirmCopy = string.Format(_confMessage, "Copy ");
            ViewBag.ConfirmAckhowledge = string.Format(_confMessage, "acknowledge ");

            ViewBag.ConfirmGenerateApprover = string.Format(_confMessage, "Generate Approver List ");
            ViewBag.ConfirmResetApprover = string.Format(_confMessage, "Clear Approver List ");
            ViewBag.ConfirmClear = string.Format(_confMessage, "Clear ");
            ViewBag.ConfirmDelete = string.Format(_confMessage, " Delete {1}");
            ViewBag.ChangeApprover = string.Format(_confMessage, "change approver ");

            ViewBag.ConfirmExportToExcel = string.Format(_confMessage, "Export all record to excel ");
            ViewBag.ConfirmPrint = string.Format(_confMessage, "print a report ");

            ViewBag.NoAuthorize = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0069AERR).VALUE;
            ViewBag.HigherMessage = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MFAS0704AWRN).VALUE;

            ViewBag.ShouldNotBeEmpty = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, " {1} ").VALUE;
            ViewBag.MustBeNumber = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD1002AERR, " {1} ").VALUE;
            ViewBag.OrgChanged = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MFAS0707AWRN).VALUE;
            ViewBag.FormatDateInvalid = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0047AERR, " {1} ", "dd.MM.yyyy").VALUE;
            ViewBag.LessOrEqual = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0025AERR, "Sale Date", "Current Date").VALUE;

            ViewBag.Noassetselected = this.BusinessLogic.GetSystemMessage("MFAS3601AERR").VALUE;

            ViewBag.NoAssetApprove = this.BusinessLogic.GetSystemMessage("MFAS1901ERR").VALUE;
            //ConfirmGenerateAssetNo
            ViewBag.ConfirmGenerateAssetNo = this.BusinessLogic.GetSystemMessage("MFAS1911ACFM", null).VALUE;

            ViewBag.ConfirmResend = this.BusinessLogic.GetSystemMessage("MFAS1912ACFM", null).VALUE;


            ViewBag.ConfirmNotice = this.BusinessLogic.GetSystemMessage("MFAS1502ACFM").VALUE;
            ViewBag.ConfirmReport = this.BusinessLogic.GetSystemMessage("MFAS1501ACFM").VALUE;
            ViewBag.ErrorGenApprover = this.BusinessLogic.GetSystemMessage("MFAS1901AERR").VALUE;
        }


        public PartialViewResult Detail(string _x)
        {
            return PartialView();
        }
        [HttpPost]
        public JsonResult GetRequestor(WFD0BaseRequestDocModel data)
        {
            var _result = this.BusinessLogic.GetRequestor(data);
            return BaseJson(_result);
        }
        [HttpPost]
        public JsonResult GetRoleListOfRequestor(WFD0BaseRequestDocModel data)
        {
            var _result = this.BusinessLogic.GetRoleOfRequestor(data);
            return BaseJson(_result);
        }
        [HttpPost]
        public ActionResult CommentHistory(WFD0BaseRequestDocModel model)
        {

            var _result = this.BusinessLogic.GetCommentHistoryList(model);
            return PartialView(_result);
        }

        [HttpPost]
        public ActionResult WorkflowTrackingProcess(WFD0BaseRequestDocModel model)
        {
            var _result = this.BusinessLogic.GetWorkflowTrackingProcess(model);
            return PartialView(_result);
        }

        [HttpPost]
        public JsonResult UploadCommentInfoData(WFD01170CommentModel data)
        {
            var d = this.BusinessLogic.UploadFileAttchDoc(data, Request);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult DeleteAttchDoc(WFD01170CommentModel data)
        {
            var d = this.BusinessLogic.DeleteFileAttchDoc(data);
            return BaseJson(d);
        }
        [HttpPost]
        public JsonResult CheckFileUpload(CheckFileUploadModel data)
        {
            var d = this.BusinessLogic.CheckValidateUploadFile(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult GenerateFlow(WFD0BaseRequestDocModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }

            var rs = this.BusinessLogic.GenerateApproverFlow(data);
            if ((rs.ObjectResult as JsonResultBaseModel).IsError)
                return BaseJson(rs);

            //Render Flow
            var _s = this.BusinessLogic.RenderApproverFlow(data, true);
            return BaseJson(_s);
        }

        [HttpPost]
        public JsonResult RenderFlow(WFD0BaseRequestDocModel data)
        {
            var rs = this.BusinessLogic.RenderApproverFlow(data, false);
            return BaseJson(rs);
        }

        [HttpPost]
        public JsonResult ResetApproverFlow(WFD0BaseRequestDocModel data)
        {
            var rs = this.BusinessLogic.ResetApproverFlow(data);
            if ((rs.ObjectResult as JsonResultBaseModel).IsError)
                return BaseJson(rs);

            //Render Flow
            var _s = this.BusinessLogic.RenderApproverFlow(data, false);
            return BaseJson(_s);
        }

        [HttpPost]
        public JsonResult GetTotalDocument(WFD0BaseRequestDocModel data)
        {
            var d = this.BusinessLogic.GetTotalDocument(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult Submit(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }

            data.IsAECUser = GlobalUser.UserSc2.IsAECUser;

            var rs = this.BusinessLogic.SubmitRequest(data);
            return BaseJson(rs);
        }

        [HttpPost]
        public JsonResult Approve(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }

            data.RequestHeaderData.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            var rs = this.BusinessLogic.ApproveRequest(data);
            return BaseJson(rs);
        }

        [HttpPost]
        public JsonResult Reject(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }

            var rs = this.BusinessLogic.RejectRequest(data);
            return BaseJson(rs);
        }

        [HttpPost]
        public JsonResult ReSubmit(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }
            data.IsAECUser = GlobalUser.UserSc2.IsAECUser;

            var rs = this.BusinessLogic.ResubmitRequest(data);
            return BaseJson(rs);
        }

        [HttpPost]
        public JsonResult Acknowledge(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }
            var rs = this.BusinessLogic.AcknowledgeRequest(data);
            return BaseJson(rs);
        }

        [HttpPost]
        public JsonResult CloseRequest(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }

            var rs = this.BusinessLogic.CloseRequest(data);
            return BaseJson(rs);
        }
        //
        [HttpPost]
        public JsonResult CopyNew(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }
            var rs = this.BusinessLogic.CopyNew(data);
            return BaseJson(rs);
        }


        [HttpPost]
        public JsonResult SaveApprover(WFD01170MainModel data)
        {
            var _session = this.BusinessLogic.IsSessionExpired(GlobalUser.UserSc2.TFASTEmployeeNo);
            if (_session.IsThereError)
            {
                return BaseJson(_session);
            }
            data.RequestHeaderData.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            var d = this.BusinessLogic.SaveApprover(data);
            return BaseJson(d);
        }

        [HttpPost]
        public virtual ActionResult GetDownloadTemplate(string FUNCTION_ID)
        {
            try
            {
                var _templateFileName = this.BusinessLogic.GetDownloadTemplate(FUNCTION_ID);
                return BaseJson(new JsonResultBaseModel() { Status = MessageStatus.INFO, Message = _templateFileName });
            }
            catch (Exception ex)
            {
                return BaseJson(new JsonResultBaseModel() { Status = MessageStatus.ERROR, Message = ex.Message });

            }

        }
        [HttpGet]
        public virtual ActionResult DownloadTemplate(string FUNCTION_ID)
        {
            try
            {
                var _templateFileName = this.BusinessLogic.GetDownloadTemplate(FUNCTION_ID);
                var _fs = new FileInfo(_templateFileName);
                return File(_templateFileName, "application/vnd.ms-excel", _fs.Name);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error");
            }

        }


        [HttpPost]
        public JsonResult GenerateAssetNo(WFD01170MainModel header, WFD01170SelectedAssetNoModel data)
        {

            //Manual
            var d = this.BusinessLogic.GenerateAssetNo(data, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(d);
        }
        //WFD01170fileUploadRequestExcel
        [HttpPost]
        public JsonResult UploadRequest(WFD01170UploadRequestModel data)
        {
            data.StartUpPath = ControllerContext.HttpContext.Server.MapPath("~/web.config");

            var d = this.BusinessLogic.UploadExcel(data, Request);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult InsertComment(WFD01170MainModel data)
        {
            var rs = this.BusinessLogic.AcknowledgeRequest(data);
            return BaseJson(rs);
        }
    }

}