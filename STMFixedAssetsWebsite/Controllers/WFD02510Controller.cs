﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02510;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Controllers;
using System.Drawing;
using System.Text.RegularExpressions;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02510Controller : BaseController
    {
        // GET: WFD02510
        public ActionResult Index(WFD0BaseRequestDocModel data)
        {
            ViewBag.UrlGetDisposalReason = Url.Action("GetDisposalReason", "WFD02510");
            ViewBag.UrlGetDisposalStore = Url.Action("GetDisposalStore", "WFD02510");
            ViewBag.UrlUpdateRetire = Url.Action("UpdateRetire", "WFD02510");
            ViewBag.UrlInsertAttachDoc = Url.Action("InsertAttachDoc", "WFD02510");
            ViewBag.UrlUpdateReasonOther = Url.Action("UpdateReasonOther", "WFD02510");

            //Allow Mass Permission Please set for 
            ViewBag.Allowmass = "FAS|ACU";


            data.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            this.BusinessLogic.init(data);
            return PartialView();
            
        }

        private WFD02510BO bo;
        protected WFD02510BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02510BO();
                return bo;
            }
        }


       
       

        #region OperationDisposal
        //[HttpPost]
        //public JsonResult WFD02510_GetAssetDisposal(WFD02510Model con, PaginationModel page)
        //{
        //    return SearchJson(BO.GetAssetDisposal(con, page));
        //}

        //[HttpPost]
        //public JsonResult WFD02510_ClearAssetDisposal(WFD02510Model data)
        //{
        //    var d = BO.ClearAssetDisposal(data);
        //    return BaseJson(d);
        //}

        //[HttpPost]
        //public JsonResult WFD02510_DeleteAssetDisposal(WFD02510Model data)
        //{
        //    var d = BO.DeleteAssetDisposal(data);
        //    return BaseJson(d);
        //}
        
        [HttpPost]
        public JsonResult WFD02510_IsMassPermission(WFD0BaseRequestDocModel con)
        {
            // CheckMassPermission
            return BaseJson(this.BusinessLogic.CheckMassPermission(con));
        }
        #endregion

        #region FunctionUpload

        //[HttpPost]
        //public JsonResult WFD02510_AssetUploadFileAttchDoc()
        //{
        //    WFD02510Model data = new WFD02510Model();
        //    var d = this.BusinessLogic.AssetUploadFileAttchDoc(data, Request);
        //    return BaseJson(d);
        //}

        //[HttpPost]
        //public JsonResult WFD02510_UploadAttchDoc()
        //{
        //    WFD02510Model data = new WFD02510Model();
        //    var d = this.BusinessLogic.UploadFileAttchDoc(data, Request);
        //    return BaseJson(d);
        //}

        //[HttpPost]
        //public JsonResult WFD02510_DeleteAttchDoc()
        //{
        //    WFD02510Model data = new WFD02510Model();
        //    var d = this.BusinessLogic.DeleteFileAttchDoc(data, Request);
        //    return BaseJson(d);
        //}

        //[HttpPost]
        //public JsonResult WFD02510_AssetDeleteAttchDoc(string type)
        //{
        //    WFD02510Model data = new WFD02510Model();
        //    var d = this.BusinessLogic.AssetDeleteFileAttchDoc(data, Request);
        //    return BaseJson(d);
        //}
        

        //[HttpPost]
        //public JsonResult CheckFileUpload(CheckFileUploadModel data)
        //{
        //    var d = this.BusinessLogic.CheckValidateUploadFile(data);
        //    return BaseJson(d);
        //}
        #endregion

        [HttpPost]
        public JsonResult GetAssetList(WFD0BaseRequestDocModel con, PaginationModel page)
        {

            var _result = this.BusinessLogic.GetAssetList(con, page, GlobalUser.UserSc2.IsAECUser );
            return SearchJson(_result, page);
        }
        public JsonResult GetRetirementHeader(WFD0BaseRequestDocModel con)
        {
            var _rs = this.BusinessLogic.GetDataHeader(con);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult AddAsset(List<WFD0BaseRequestDetailModel> data)
        {
            var _rs = this.BusinessLogic.InsertAsset(data);
            return BaseJson(_rs);
        }
        

        [HttpPost]
        public JsonResult ClearAssetsList(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.ClearAssetList(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            var _rs = this.BusinessLogic.DeleteAsset(data);
            return BaseJson(_rs);
        }

        // Operation when user click button on request master screen
        // 1270.js call request.JS -> collect data -> this function and return data to main screen
        [HttpPost]
        public JsonResult Prepare(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.PrepareCostCenterToGenerateFlow(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult UpdateAssetList(WFD02510HeaderModel _data)
        {
            var _rs = this.BusinessLogic.UpdateAssetList(_data, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(_rs);
        }

        //[HttpPost]
        //public JsonResult InsertAttachDoc(WFD02510AttachDocModel model)
        //{
        //    string _FileName = Path.GetFileName(model.Photos.FileName);
        //    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
        //    model.Photos.SaveAs(_path);
        //    //var _rs = this.BusinessLogic.InsertAttachDoc(data);
        //    //return BaseJson(_rs);
        //    return null;
        //}

        #region GetDataFromSystem
        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD02510_GetDisposalReason(WFD02510Model data)
        {
            var d = this.BusinessLogic.GetDisposalReason(data);
            return BaseJson(d);
        }

        [HttpPost] //data in dropdown boi status
        public JsonResult WFD02510_GetBOIStatus(WFD02510Model data)
        {
            var d = this.BusinessLogic.GetBOIStatus(data);
            return BaseJson(d);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD02510_GetStoreStatus(WFD02510Model data)
        {
            var d = this.BusinessLogic.GetStoreStatus(data);
            return BaseJson(d);
        }
        [HttpPost] //data in dropdown KeepBy
        public JsonResult WFD02510_GetKeepByStatus(WFD02510Model data)
        {
            var d = this.BusinessLogic.GetKeepByStatus(data);
            return BaseJson(d);
        }
        [HttpPost] //data in dropdown SaleProcess
        public JsonResult WFD02510_GetSaleProcessStatus(WFD02510Model data)
        {
            var d = this.BusinessLogic.GetSaleProcessStatus(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult Resend(Models.WFD01270.WFD01170MainModel header, WFD02510HeaderModel data)
        {
            var d = this.BusinessLogic.Resend(header, data, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(d);
        }
        #endregion
    }
}