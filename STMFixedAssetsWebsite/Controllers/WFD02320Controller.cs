﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.LFD02140;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02320;
using th.co.toyota.stm.fas.BusinessObject.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02320Controller : BaseController
    {
        private WFD02320BO bo;
        protected WFD02320BO BO
        {
            get
            {
                if (bo == null) bo = new WFD02320BO();
                return bo;
            }
        }
        // GET: WFD02320
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }
            ViewBag.LoginUser = GlobalUser.UserSc2.TFASTEmployeeNo;

            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD02320");
            ViewBag.RECORDISNOTSELECTED = BO.GetSystemMessage("MCOM0001AERR", " print tag").VALUE;
            ViewBag.ConfirmExportToExcel = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "export {0} record to excel ").VALUE;
            ViewBag.ConfirmPrint = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "print {0} asset(s)").VALUE;
            ViewBag.ConfirmChangePrinter = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "change printer from {0} to {1}").VALUE;
            return View();
        }

        [HttpPost]
        public JsonResult Search(WFD02320SearchModel con, PaginationModel page)
        {
            return SearchJson(BO.Search(con, page));
        }
        [HttpPost]
        public JsonResult DataDialog(string company, string costcode, string resp_cost_code, string createby, string find, string printall)
        {
            //WFD02320PrintQDCModel data = BO.GetPrintDetail(costcode, createby, find, printall);
            //int recordsTotal = 0;
            //recordsTotal = data.printQDialog != null ? data.printQDialog.Count : 0;
            //return Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

            return BaseJson(BO.GetPrintDetail(company, costcode, resp_cost_code, createby, find, printall));
        }

        //[HttpPost]
        //public JsonResult GetSystemMaster(string BARCODE_SIZE)
        //{
        //    return Json(new { data = BO.GetPrintChange(BARCODE_SIZE).Where(x => x.ACTIVE_FLAG == "Y") });
        //}
        [HttpPost]
        public JsonResult GetPrinterLocation(string COMPANY)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("PRINT_CONFIG", "PRINTER_LOCATION", true, string.Empty).FindAll(x => x.REMARKS == COMPANY) ;
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult GetPrinterName(string PRINTER_LOCATION, string BARCODE_SIZE)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("PRINT_CONFIG", string.Format("PRINTER_{0}",PRINTER_LOCATION), true,string.Empty).FindAll(x=>x.CODE == BARCODE_SIZE) ;
            return BaseJson(_rs);
        }


        [HttpPost]
        //public JsonResult PrintTag(LFD02140RetriveData data)
        //{
        //    LFD02140BO bo_print = new LFD02140BO();
        //    string emp_code = GlobalUser.UserSc2.TFASTEmployeeNo;
        //    return BaseJson(bo_print.GenerateReport(data, emp_code));
        //}

        public JsonResult PrintTag(List<LFD02140RetriveTagPrintOut> SelectedList, List<WFD02320PrintQDetailsModel> AssetList)
        {
           LFD02140BO bo_print = new LFD02140BO();
            string emp_code = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(bo_print.GenerateReport(SelectedList, AssetList, emp_code));
        }
    }
}