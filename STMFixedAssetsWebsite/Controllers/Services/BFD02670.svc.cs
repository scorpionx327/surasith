﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using th.co.toyota.stm.fas.BusinessObject.SFAS;
using th.co.toyota.stm.fas.Models.SFAS.BFD02670;

namespace th.co.toyota.stm.fas.Controllers.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BFD02670" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BFD02670.svc or BFD02670.svc.cs at the Solution Explorer and start debugging.
    public class BFD02670 : IBFD02670
    {
        public bool Upload(List<BFD02670_ScanModel> scans, List<BFD02670_InvalidModel> invalids, string employeeNo)
        {
            #region Test
            /*
            // Test 
            // Begin
            string Value = DateTime.Now.ToString();
            string Format = "dd/MM/yyyy";
            string CultureStr = "en-US";
            // ------------------------ 1 Assign value to BFD02670_ScanModel Group
            var date = new BFD02670_ServerDateTime();
            date.Value = Value;
            date.Format = Format;
            date.CultureStr = CultureStr;

            var start_count_time = new BFD02670_ServerDateTime();
            start_count_time.Value = Value;
            start_count_time.Format = Format;
            start_count_time.CultureStr = CultureStr;

            var end_count_time = new BFD02670_ServerDateTime();
            end_count_time.Value = Value;
            end_count_time.Format = Format;
            end_count_time.CultureStr = CultureStr;

            List<BFD02670_ScanModel> scansForTest = new List<BFD02670_ScanModel>
            {
                new BFD02670_ScanModel {
                    COMPANY = "stm",
                    STOCK_TAKE_KEY = "201321I",
                    ASSET_NO = "GAT-P01N0026900",
                    ASSET_SUB = "0000",
                    EMP_CODE = "stm.1423",
                    CHECK_STATUS = "N",
                    DATE = date,
                    START_COUNT_TIME = start_count_time,
                    END_COUNT_TIME = end_count_time,
                    COUNT_TIME = 10
                }
            };


            // ------------------------ 2 Assign value to BFD02670_InvalidModel Group
            var scan_date = new BFD02670_ServerDateTime();
            scan_date.Value = Value;
            scan_date.Format = Format;
            scan_date.CultureStr = CultureStr;

            List<BFD02670_InvalidModel> invalidsForTest = new List<BFD02670_InvalidModel>
            {
                new BFD02670_InvalidModel {
                    COST_CODE = "E01DK002",
                    EMP_CODE = "stm.1423",
                    BARCODE = "GAT-P01N002680072",
                    SCAN_DATE=scan_date
                }

            };

            string employeeNoForTest = "stm.1423";

            // End
            */

            #endregion

            BFD02670BO bo = new BFD02670BO();
            return bo.SaveUploadPlan(scans, invalids, employeeNo); //--> For use
            //return bo.SaveUploadPlan(scansForTest, invalidsForTest, employeeNoForTest); //--> For test
        }
    }
}
