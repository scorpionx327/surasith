﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using th.co.toyota.stm.fas.BusinessObject.SFAS;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.SFAS.WFD02650;

namespace th.co.toyota.stm.fas.Controllers.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WFD02650" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WFD02650.svc or WFD02650.svc.cs at the Solution Explorer and start debugging.
    public class WFD02650 : IWFD02650
    {
        public bool UserLogin(string EMP_CODE)
        {
            return new WFD02650BO().Login(EMP_CODE);
        }

        public WFD02650_PlanModel DownloadPlanData(string EMP_CODE)
        {
            return new WFD02650BO().DownloadPlanData(EMP_CODE);
        }


        public bool AddStockTakeInvalidScanData(WFD02650_InvalidScanModel data)
        {
            return new WFD02650BO().AddStockTakeInvalidScanData(data);
        }

        public bool UpdatePlan(string STOCK_TAKE_KEY, string EMP_CODE, List<TB_R_STOCK_TAKE_D> StockTakeDDatas)
        {
            return new WFD02650BO().UpdatePlan(STOCK_TAKE_KEY, EMP_CODE, StockTakeDDatas);
        }

        public bool UpdateStockTakeDScanData(WFD02650_ScanItemModel data)
        {
            return new WFD02650BO().UpdateStockTakeDScanData(data);
        }

        public bool TestConnection()
        {
            return true;
        }

        #region Download plan ย่อยทีละอัน

        public List<WFD02650_EmployeeModel> DownloadPlan_Employee(string EMP_CODE)
        {
            return new WFD02650BO().DownloadPlan_Employee(EMP_CODE);
        }

        public TB_R_STOCK_TAKE_H DownloadPlan_Header(string IS_FAADMIN)
        {
            return new WFD02650BO().DownloadPlan_Header(IS_FAADMIN);
        }

        public List<TB_R_STOCK_TAKE_D> DownloadPlan_Details(string STOCK_TAKE_KEY, string SV_EMP_CODE, string IS_FAADMIN)
        {
            return new WFD02650BO().DownloadPlan_Details(STOCK_TAKE_KEY, SV_EMP_CODE, IS_FAADMIN);
        }

        public List<TB_R_STOCK_TAKE_D_PER_SV> DownloadPlan_DetailsPerSV(string SV_EMP_CODE, string STOCK_TAKE_KEY, string IS_FAADMIN)
        {
            return new WFD02650BO().DownloadPlan_DetailsPerSV(STOCK_TAKE_KEY, SV_EMP_CODE, IS_FAADMIN);
        }

        public WFD02650_ServerDateTime GetServerDateTime()
        {
            WFD02650_ServerDateTime res = new WFD02650_ServerDateTime();
            var dt = DateTime.Now;
            res.Format = "dd-MM-yyyy HH:mm:ss";
            res.CultureStr = "en-Us";
            res.Value = dt.ToString(res.Format, new CultureInfo(res.CultureStr));
            return res;
        }

        #endregion

    }
}
