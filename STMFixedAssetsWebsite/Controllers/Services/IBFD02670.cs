﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using th.co.toyota.stm.fas.Models.SFAS.BFD02670;

namespace th.co.toyota.stm.fas.Controllers.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBFD02670" in both code and config file together.
    [ServiceContract]
    public interface IBFD02670
    {
        [OperationContract]
        bool Upload(List<BFD02670_ScanModel> scans, List<BFD02670_InvalidModel> invalids, string employeeNo);
    }
}
