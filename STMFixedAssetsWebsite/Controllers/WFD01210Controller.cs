﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01210;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01210Controller : BaseController
    {
        private WFD01210BO bo;
        //public static string _empCodeProfile = "";
     
        protected WFD01210BO BO
        {
            get
            {
                if (bo == null) bo = new WFD01210BO();
                return bo;
            }
        }
        // GET: WFD01210
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01210");
            ViewBag.SIGNATUREPATH = BO.GetSignaturePath();
            ViewBag.DeleteConfirm = BO.GetSystemMessage("MSTD0001ACFM").VALUE;
            ViewBag.DeleteSuccess = BO.GetSystemMessage("MSTD0090AINF").VALUE;
            ViewBag.NotSelectEdit = BO.GetSystemMessage("MCOM0004AERR", "Edit").VALUE;
            ViewBag.NotSelectView = BO.GetSystemMessage("MCOM0004AERR", "View").VALUE;
            ViewBag.DafaultImage = BO.GetImageDefalut();
            ViewBag.NoSeleteDelete = BO.GetSystemMessage("MSTD1016AERR").VALUE;
            ViewBag.IsManager = "N";
            ViewBag.TFASTEmployeeNo = GlobalUser.UserSc2.TFASTEmployeeNo;
            string strAllow = BO.GetAllowSearchBO(GlobalUser.UserSc2.DefaultCompany,GlobalUser.UserSc2.TFASTEmployeeNo);

            if (!GlobalUser.UserSc2.IsAECUser)
            {
                if (strAllow == "N")
                {
                    return Redirect(Url.Action("ManageUser", "WFD01210", new { EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo, MODE = "EDIT" }));
                }
                // Manager Can search 
                ViewBag.IsManager = "Y";

            }
           
            return View();
        }
        public ActionResult ManageUser(string EMP_CODE, string MODE, string Encode)
        {
            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01210");
            ViewBag.CancelEdit = BO.GetSystemMessage("MCOM0007ACFM", "Edit operation").VALUE;
            ViewBag.CancelAdd = BO.GetSystemMessage("MCOM0007ACFM", "Add operation").VALUE;
            ViewBag.CancelView = BO.GetSystemMessage("MCOM0007ACFM", "View operation").VALUE;
            ViewBag.ConfirmSave = BO.GetSystemMessage("MSTD0006ACFM").VALUE;
            ViewBag.SaveSuccess = BO.GetSystemMessage("MSTD0101AINF").VALUE;
            ViewBag.SIGNATUREPATH = BO.GetSignaturePath();
            ViewBag.DafaultImage = BO.GetImageDefalut();
            ViewBag.SPEC_DIV = BO.GetSPECDIV();
            //_empCodeProfile = EMP_CODE;
            ViewBag.MODE = MODE;
            //ViewBag.ORG_CODE = BO.GetEmpOrg(EMP_CODE);
            ViewBag.COMPANY_NOT_EMPTY = BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Company").VALUE;
            ViewBag.TFASTEmployeeNo = GlobalUser.UserSc2.TFASTEmployeeNo;
            var empDetail = BO.GetEmpDetail(EMP_CODE);
            if (empDetail != null && empDetail.SYS_EMP_CODE != null)
            {
                ViewBag.EMP_CODE = empDetail.EMP_CODE;
                ViewBag.SYS_EMP_CODE = empDetail.SYS_EMP_CODE;
                ViewBag.ORG_CODE = empDetail.ORG_CODE;
            }

            BO.InitializeCostcenterPopup(empDetail.COMPANY, empDetail.SYS_EMP_CODE, GlobalUser.UserSc2.TFASTEmployeeNo);

            ViewBag.FAADMIN = GlobalUser.UserSc2.IsAECUser ? 'Y' : 'N';

            ViewBag.ALLOW_EDIT = 'N';
            ViewBag.IsManager = "N";
            if (BO.IsAllowEditByManager(Encode, GlobalUser.UserSc2.TFASTEmployeeNo))
            {
                ViewBag.ALLOW_EDIT = 'Y';
                string strAllow = BO.GetAllowSearchBO(GlobalUser.UserSc2.DefaultCompany, GlobalUser.UserSc2.TFASTEmployeeNo);
                if (!GlobalUser.UserSc2.IsAECUser)
                {                    
                    if (strAllow != "N")
                    {
                        //ViewBag.ALLOW_EDIT = 'N';
                        ViewBag.IsManager = "Y";
                    }
                }
            }
            
            ViewBag.FileExtension = GetExtension();
            return View();
        }
        [HttpPost]
        public JsonResult GetDivision()
        {
            return BaseJson(BO.GetDivisionBO());
        }
        [HttpPost]
        public JsonResult GetDepartment(WFD01210SearchModel con)
        {
            //Store Name in Code
            return BaseJson(BO.GetDepartmentBO(con.DIV_CODE));
        }
        [HttpPost]
        public JsonResult GetSection(WFD01210SearchModel con)
        {
            //Store Name in Code
            return BaseJson(BO.GetSectionBO(con.DIV_CODE, con.DEPT_CODE));
        }
        [HttpPost]
        public JsonResult GetPosition()
        {
            return BaseJson(BO.GetPositionBO());
        }
        [HttpPost]
        public JsonResult GetResponsibility()
        {
            return BaseJson(BO.GetResponsibilityBO());
        }
        [HttpPost]
        public JsonResult Search(WFD01210SearchModel con, PaginationModel page)
        {
            string strAllow = BO.GetAllowSearchBO(GlobalUser.UserSc2.DefaultCompany, GlobalUser.UserSc2.TFASTEmployeeNo);
            con.M_SYS_EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            return SearchJson(BO.SearchUserProfile(GlobalUser.UserSc2.TFASTEmployeeNo, con, page));
        }
        [HttpPost]
        public JsonResult CCBaseonOrganize(WFD01210SearchModel con, PaginationModel page)
        {
            return SearchJson(BO.GetCCBaseOnOrganizeBO(con, page));
        }
        [HttpPost]
        public JsonResult GetCostCenter(WFD01210SearchModel con)
        {
            return BaseJson(BO.GetCostCenterBO(con));
        }
        [HttpPost]
        public JsonResult Delete(WFD01210SearchModel con)
        {
            return BaseJson(BO.DeleteEmployeeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo));
        }
        [HttpPost]
        public JsonResult GetEmployeeByCode(WFD01210SearchModel con)
        {
            return BaseJson(BO.GetEmployeeByCodeBO(con,true));
        }

        //[HttpPost]
        //public JsonResult GetSignature(WFD01210SearchModel con)
        //{
        //    return BaseJson(BO.GetSignature(con));
        //}


        [HttpPost]
        public JsonResult GetDelegateUser(WFD01210SearchModel con)
        {
            return BaseJson(BO.GetDelegateUserBO(con));
        }

        [HttpPost]
        public JsonResult ConvertPathImg(string PATH)
        {
            return BaseJson(BO.ConvertPathImg(PATH));
        }

        [HttpPost]
        public JsonResult InsertEmployee(WFD01210SaveEmployeeModel con)
        {
            con.USER_LOGIN = GlobalUser.UserSc2.TFASTEmployeeNo;
            //con.FAADMIN = GlobalUser.UserSc2.IsFAAdmin ? "Y" : "N";
            return BaseJson(BO.InsertEmployeeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, Request));
        }

        [HttpPost]
        public JsonResult InsertInChargeCostCode(WFD01210SaveEmployeeModel con)
        {
            con.USER_LOGIN = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.InsertInChargeCostCodeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, Request));
        }

        [HttpPost]
        public JsonResult InsertInChargeResCostCode(WFD01210SaveEmployeeModel con)
        {
            con.USER_LOGIN = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.InsertInChargeResCostCodeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, Request));
        }

        //[HttpPost]
        //public JsonResult UpdateInChargeCostCode(WFD01210SaveEmployeeModel con)
        //{
        //    con.USER_LOGIN = GlobalUser.UserSc2.TFASTEmployeeNo;
        //    return BaseJson(BO.UpdateInChargeCostCodeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, Request));
        //}

        [HttpPost]
        public JsonResult UpdateEmployee(WFD01210SaveEmployeeModel con)
        {
            con.USER_LOGIN = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.UpdateEmployeeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, Request));
        }
        [HttpPost]
        public JsonResult SearchtOrganization(WFD01210SearchModel con, PaginationModel page)
        {
            return SearchJson(BO.SearchtOrganizationBO(con, page));
        }
        [HttpPost]
        public JsonResult GetInCharge(WFD01210SearchModel con)
        {
            //Left side of screen
            string _Login = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.GetInChargeBO(con, _Login));
        }

        [HttpPost]
        public JsonResult GetInChargeTable(WFD01210SearchModel con)
        {
            //Selected (right side)
            string _Login = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.GetInChargeTableBO(con, _Login));
        }
        [HttpPost]
        public JsonResult GetCheckSetUpInCharge(WFD01210SearchModel con)
        {
            return BaseJson(BO.GetCheckSetUpInChargeBO(con));
        }

        [HttpPost]
        public JsonResult CostCenter_AutoComplete(string _keyword, string _EmpCode)
        {
            var _rs = BO.GetCostCenterAutoComplete(_keyword, _EmpCode, "N");
            return BaseJson(_rs);
        }
    }
}

public static partial class DropdownListHelper
{
    public static MvcHtmlString WFD01210CostcenterCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
    {
        WFD01210BO bo = new WFD01210BO();
        List<ItemValue<string>> ls = new List<ItemValue<string>>();
        var datas = bo.GetCostCenterBO();

        ItemValue<string> item;

        if (datas != null)
        {
            foreach (var d in datas)
            {
                item = new ItemValue<string>();
                item.Value = d.COST_CODE;
                item.Display = string.Format("{0} : {1}", d.COST_CODE, d.COST_NAME);

                ls.Add(item);
            }
        }

        return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
    }

    public static MvcHtmlString WFD01210DelegationUserProfileCombobox(this HtmlHelper helper, string id, string _empCodeProfile, object attribute = null, bool includeIdx0 = true, string firstElement = null)
    {
        WFD01210BO bo = new WFD01210BO();
        List<ItemValue<string>> ls = new List<ItemValue<string>>();
        var datas = bo.GetDelegateUserCBB(new WFD01210SearchModel() { EMP_CODE = _empCodeProfile });

        ItemValue<string> item;

        if (datas != null)
        {
            foreach (var d in datas)
            {
                item = new ItemValue<string>();
                item.Value = d.EMP_CODE;
                item.Display = d.FULL_NAME;

                ls.Add(item);
            }
        }

        return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
    }

    public static MvcHtmlString WFD01210JobCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
    {
        SystemBO bo = new SystemBO();
        List<ItemValue<string>> ls = new List<ItemValue<string>>();
        var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.USER_PROFILE.ToString(), SYSTEM_SUB_CATEGORY.JOB.ToString());

        ItemValue<string> item;

        if (datas != null)
        {
            foreach (var d in datas.OrderBy(m => m.REMARKS))
            {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = string.Format("{0} : {1}", d.CODE, d.VALUE);

                ls.Add(item);
            }
        }

        return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
    }

    public static MvcHtmlString WFD01210PositionCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
    {
        SystemBO bo = new SystemBO();
        List<ItemValue<string>> ls = new List<ItemValue<string>>();
        var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.USER_PROFILE.ToString(), SYSTEM_SUB_CATEGORY.POSITION.ToString());

        ItemValue<string> item;

        if (datas != null)
        {
            foreach (var d in datas.OrderBy(m => m.REMARKS))
            {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = string.Format("{0} : {1}", d.CODE, d.VALUE);

                ls.Add(item);
            }
        }

        return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
    }

    public static MvcHtmlString AECUserCombobox(this HtmlHelper helper, string id, string _empCodeProfile, object attribute = null)
    {
        WFD01210BO bo = new WFD01210BO();
        List<ItemValue<string>> ls = new List<ItemValue<string>>();
        var datas = bo.GetAECUserCBB(new WFD01210SearchModel() { EMP_CODE = _empCodeProfile });

        ItemValue<string> item;

        if (datas != null)
        {
            foreach (var d in datas)
            {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = d.NAME;
                item.Selected = d.SELECTED == "Y";
                ls.Add(item);
            }
        }

        return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
        //return ControlCreator.CommonComboBox<ItemValue<string>>(id, ls, "Display", "Value", attribute, includeIdx0);
    }
}