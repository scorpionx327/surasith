﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.WFD02620;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02620Controller : BaseController
    {
        private WFD02620BO _bo;
        protected WFD02620BO BO
        {
            get
            {
                
                if (_bo == null) _bo = new WFD02620BO();
                return _bo;
            }
        }
        // GET: WFD02620
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            ViewBag.SCREEN_MODE = "ADD";
            ViewBag.COMPANY = null;
            ViewBag.YEAR = null;
            ViewBag.ROUND = null;
            ViewBag.ASSET_LOCATION = null;
            return View();
        }

        [HttpGet]
        public ActionResult Index(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            //YEAR = "2017";
            //ROUND = "01";
            //ASSET_LOCATION = "I";
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }


            ViewBag.SCREEN_MODE = "EDIT";
            if (string.IsNullOrEmpty(YEAR)) ViewBag.SCREEN_MODE = "ADD";
            ViewBag.COMPANY = COMPANY;
            ViewBag.YEAR = YEAR;
            ViewBag.ROUND = ROUND;
            ViewBag.ASSET_LOCATION = ASSET_LOCATION;
            ViewBag.UserId = GlobalUser.UserSc2.TFASTEmployeeNo;
            ViewBag.UserFullName = string.Format("{0} {1}.", GlobalUser.UserSc2.User.FirstName, GlobalUser.UserSc2.User.LastName.Substring(0, 1));
            
            ViewBag.ConfirmSaveAssignment = BO.GetSystemMessage("MSTD0006ACFM").VALUE; 
            ViewBag.ConfirmResetAssignment = BO.GetSystemMessage("MFAS1301ACFM").VALUE;
            ViewBag.ConfirmSaveHeader = BO.GetSystemMessage("MSTD0006ACFM").VALUE;
            ViewBag.ConfirmComplete = BO.GetSystemMessage("MFAS1305ACFM").VALUE;
            ViewBag.ConfirmComplete = BO.GetSystemMessage("MFAS1305ACFM").VALUE;
            ViewBag.ConfirmAnnounce = BO.GetSystemMessage("MFAS1303ACFM").VALUE;

            ViewBag.NoWorkingDay = BO.GetSystemMessage("MFAS1506AERR").VALUE;
            ViewBag.InvalidFormat = BO.GetSystemMessage("MSTD0050AERR", "{0}", "{1}").VALUE;

            return View();
        }

        [HttpPost]
        public JsonResult GetDefaultSreenData()
        {

            var data = BO.GetDefaultSreenData();
            return BaseJson(data);
        }

        [HttpPost]
        public JsonResult GetSettingSectionData(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            return BaseJson(BO.GetSettingSectionData(COMPANY,YEAR, ROUND, ASSET_LOCATION));
        }


        [HttpPost]
        public JsonResult AddStockTakeRequest(WFD02620SettingModel data)
        {
            data.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.AddStockTakeRequest(data));
        }


        [HttpPost]
        public JsonResult UploadAssetListFile()
        {
            HttpPostedFileBase file = Request.Files[0];
            return BaseJson(BO.UploadAssetListFile(file));
        }
        [HttpPost]
        public JsonResult GenBatchParam(WFD02620SettingModel data, string uploadFileName, string isUpdate)
        {
            return BaseJson(BO.GenBatchParam(data, uploadFileName, GlobalUser.UserSc2.TFASTEmployeeNo, isUpdate));
        }


        [HttpPost]
        public JsonResult EditStockTakeRequest(WFD02620SettingModel data)
        {
            data.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.EditStockTakeRequest(data));
        }

        [HttpPost]
        public JsonResult GetStockTakeData(string Company, string Year, string Round, string AssetLocation)
        {
            try
            {
                return BaseJson(BO.GetStockTakeModel(Company,Year, Round, AssetLocation));
            }
            catch (Exception ex)
            {
              // JLog.write(LOG_TYPE.ERROR, LOG_POSITION.COMMON, ex, "", JLog.GetCurrentMethod());
                return BaseJson(null);
            }
        }

        [HttpPost]
        public JsonResult ValidateSetWorkingTimeDialog(WFD02620SetWorkingTimeDialogModel data, List<WFD02620HolidayModel> holidays)
        {
            return BaseJson(BO.ValidateSetWorkingTimeDialog(data, holidays));
        }
        [HttpPost]
        public JsonResult CalculateIntersectUsageHHTMoreThanMaxHHT(List<WFD02620StockTakeDPerSVModel> datas, int MaxHHT)
        {
            return BaseJson(BO.CalculateIntersectUsageHHTMoreThanMaxHHT(datas, MaxHHT));
        }
        [HttpPost]
        public JsonResult SaveAssigmentDatas(WFD02620SettingModel HData, List<WFD02620StockTakeDPerSVModel> sv
            , List<WFD02620HolidayModel> holidays)
        {
            return BaseJson(BO.SaveAssigmentDatas(HData, sv, holidays, GlobalUser.UserSc2));
        }
        [HttpPost]
        public JsonResult ResetAssignment(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            return BaseJson(BO.ResetAssignment(COMPANY,YEAR, ROUND, ASSET_LOCATION, GlobalUser.UserSc2.TFASTEmployeeNo));
        }
        [HttpPost]
        public JsonResult GetSVAssignment(string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            return BaseJson(BO.GetStockTakePerSV(COMPANY,YEAR, ROUND, ASSET_LOCATION));
        }

        [HttpPost]
        public JsonResult Announce(string STOCK_TAKE_KEY, string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority 
                + Request.ApplicationPath.TrimEnd('/') + "/" 
                + "WFD02620?COMPANY=" + COMPANY + "&YEAR =" + YEAR  + "&ROUND=" + ROUND  
                + "&ASSET_LOCATION=" + ASSET_LOCATION;

            return BaseJson(BO.Announce(STOCK_TAKE_KEY, GlobalUser.UserSc2.TFASTEmployeeNo, baseUrl, COMPANY));
        }

        [HttpPost]
        public JsonResult Completed(string STOCK_TAKE_KEY, string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority
                + Request.ApplicationPath.TrimEnd('/') + "/"
                + "WFD02620?COMPANY=" + COMPANY + "&YEAR=" + YEAR + "&ROUND=" + ROUND
                + "&ASSET_LOCATION=" + ASSET_LOCATION;
            return BaseJson(BO.Completed(STOCK_TAKE_KEY, GlobalUser.UserSc2.TFASTEmployeeNo, baseUrl, COMPANY));
        }

        [HttpPost]
        public JsonResult GetDiffMinute(WFD02620MasterSettingModel setting, DateTime from, DateTime to)
        {
            return BaseJson(BO.GetDiffMinute(setting, from, to));
        }
        [HttpPost]
        public JsonResult GetResultDateTimeAfterAddMinute(WFD02620MasterSettingModel setting, DateTime date, int minutes)
        {
            return BaseJson(BO.GetResultDateTimeAfterAddMinute(setting, date, minutes));
        }

        [HttpPost]
        public JsonResult GetParamWithoutAssetLocation(string Company, string Year, string Round, string AssetLocation)
        {
          
            return BaseJson(BO.GetParamWithoutAssetLocation(Company,Year, Round));
        }

        [HttpPost]
        public JsonResult WFD02620GetMinorCategory(string company)
        {
            SystemBO boSystem = new SystemBO();
            var datas = boSystem.SelectSystemDatas("EVA_GRP_1", company);
            return BaseJson(datas);


        }

    }

    
}