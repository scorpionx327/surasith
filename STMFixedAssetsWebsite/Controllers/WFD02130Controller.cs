﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.DAO.Common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02130;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02130Controller : BaseController
    {

        private WFD02130BO bo;
        protected WFD02130BO BO
        {
            get
            {
                if (bo == null) bo = new WFD02130BO();
                return bo;
            }
        }

        // GET: WFD02130
        public ActionResult Index(string COMPANY, string ASSET_NO, string ASSET_SUB, string Cost_Code, string MODE = "VIEW")
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            //Check User permission
            ViewBag.IsAECUser = (GlobalUser.UserSc2.IsAECUser) ? "Y" : "N";
            //|| GlobalUser.UserSc2.IsInRole(Constants.Role.AECManager)



            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD02130");
            ViewBag.ConfirmSaveLocation = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "update location").VALUE;
            ViewBag.SaveLocation_Err_NoInput = BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Location").VALUE;
            ViewBag.PrintTag_Err_NoInput = BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Location of Printer").VALUE;
            ViewBag.ConfirmPrintTag = BO.GetSystemMessage(Constants.MESSAGE.MCOM0001AERR, "print").VALUE;
            ViewBag.SaveSuccess = BO.GetSystemMessage(Constants.MESSAGE.MSTD0101AINF).VALUE;
            ViewBag.PrintTag_Locked_Err = BO.GetSystemMessage(Constants.MESSAGE.MCOM0003AERR).VALUE;
            ViewBag.DafaultImage = BO.GetImageDefalut();
            ViewBag.MessageConfirmSave = BO.GetMessage("MSTD0006ACFM").MESSAGE_TEXT;
            ViewBag.MessageSuccess = BO.GetMessage("MSTD0101AINF").MESSAGE_TEXT;
            ViewBag.DeleteSuccess = BO.GetSystemMessage(Constants.MESSAGE.MSTD0080AINF, "BOI Privilege Termination").VALUE;

            ViewBag.PlateType_Err_NoInput = BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Plate Type").VALUE;
            ViewBag.PlateSize_Err_NoInput = BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Plate Size").VALUE;

            ViewBag.MessageConfirmCancelT = string.Format(BO.GetMessage("MCOM0007ACFM").MESSAGE_TEXT, "Edit Text");
            ViewBag.MessageConfirmCancelP = string.Format(BO.GetMessage("MCOM0007ACFM").MESSAGE_TEXT, "Upload Photo");
            ViewBag.FileExtension = BO.GetExtension();

            ViewBag.COMPANY = COMPANY;
            ViewBag.ASSET_NO = ASSET_NO;
            ViewBag.ASSET_SUB = ASSET_SUB;

            ViewBag.COST_CODE = Cost_Code;
            ViewBag.MODE = MODE != "EDIT" ? "VIEW" : "EDIT";
            ViewBag.CodeSuccess = Constants.MESSAGE.MSTD0101AINF;
            ViewBag.PerUploadBOIDoc = BO.GetPermissionUploadBOIDoc(GlobalUser.UserSc2.TFASTEmployeeNo);
            ViewBag.ConfirmDelete = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, " Delete {1} ").VALUE;
            ViewBag.CodeContactAdmin = Constants.MESSAGE.MFAS1102BINF;
            ViewBag.ContactAdmin = BO.GetSystemMessage(Constants.MESSAGE.MFAS1102BINF).VALUE;
            ViewBag.GoToPrintQueue = BO.GetSystemMessage(Constants.MESSAGE.MFAS1102CINF).VALUE;
            ViewBag.MessageContactAdmin = BO.GetMessage("MFAS1102BINF").MESSAGE_TEXT;

            var imageSizeSettings = new SystemDAO().SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG, SYSTEM_SUB_CATEGORY.ASSET_IMAGE);
            if (imageSizeSettings.Where(m => m.CODE == "WIDTH").Count() > 0)
                ViewBag.ImageWidth = imageSizeSettings.Where(m => m.CODE == "WIDTH").Select(m => m.VALUE).First();
            else ViewBag.ImageWidth = "400";

            if (imageSizeSettings.Where(m => m.CODE == "HEIGHT").Count() > 0)
                ViewBag.ImageHeight = imageSizeSettings.Where(m => m.CODE == "HEIGHT").Select(m => m.VALUE).First();
            else ViewBag.ImageHeight = "400";

            return View();
        }

        [HttpPost]
        public JsonResult GetAssetList(WFD02130UpdateDataModel datas)
        {
            return BaseJson(BO.GetFixedAssetList(datas, BO.GetImageDefalut()));
        }



        [HttpPost]
        public JsonResult UpdateAssetInfo(WFD02130_TB_M_ASSETS_H _data)
        {

            return BaseJson(BO.UpdateAssetInfo(_data, GlobalUser.UserSc2.TFASTEmployeeNo));
        }

        [HttpPost]
        public JsonResult AddPrintTagAndUpdateBarCodeSize(WFD02130UpdateDataModel _datas)
        {

            _datas.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;

            string message;

            var success = BO.AddPrintTagAndUpdateBarCodeSize(_datas, GlobalUser.UserSc2.IsAECUser, out message);
            return BaseJson(new { success, message });
        }

        [HttpPost]
        public JsonResult UpdateTAG_Photo(WFD02130UpdateDataModel _datas)
        {

            _datas.FILE = Request.Files[0];
            _datas.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;

            return BaseJson(BO.UpdateTAG_PhotoBase64(_datas, Request));
        }

        [HttpPost]
        public JsonResult GetPrintLocation(WFD02130UpdateDataModel _datas)
        {

            _datas.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;

            return BaseJson(BO.GetPrintLocation(_datas));
        }

        [HttpPost]
        public JsonResult WFD02130_UploadBOIData(FileDownloadModel data)
        {
            var user = GlobalUser.UserSc2.TFASTEmployeeNo;
            var d = BO.UploadFileAttchDoc(data, user);
            return BaseJson(d);
        }

        //[HttpPost]
        //public JsonResult WFD02130_UploadBOIData(WFD02130BOIModel data)
        //{
        //    data.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
        //    var d = BO.UploadFileAttchDoc(data, Request);
        //    return BaseJson(d);
        //}

        [HttpPost]
        public JsonResult WFD02130_DeleteAttchDoc(WFD02130BOIModel data)
        {
            data.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            var d = BO.DeleteFileAttchDoc(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult CheckFileUpload(CheckFileUploadModel data)
        {
            var d = BO.CheckValidateUploadFile(data);
            return BaseJson(d);
        }
    }
}