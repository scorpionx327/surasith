﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class UserManualController : BaseController
    {
        private USERMANUALBO bo;
        protected USERMANUALBO BO
        {
            get
            {
                if (bo == null) bo = new USERMANUALBO();
                return bo;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UserManual_Search(PaginationModel page)
        {
            return SearchJson(BO.Search(page));
        }

        [HttpGet]
        public FileResult Download(string FULLPATH)
        {
            var _f = new FileInfo(FULLPATH);
            byte[] fb = null;

            fb = Util.ReadLocalFile(_f.Name, _f.DirectoryName);
            return File(fb, GetMimeType(_f.Extension), _f.Name);
        }

        [HttpPost]
        public JsonResult Check_File_Existed(string FULLPATH)
        {
            var d = BO.CheckFileExist(FULLPATH);
            return SearchJson(d);

        }
    }
}