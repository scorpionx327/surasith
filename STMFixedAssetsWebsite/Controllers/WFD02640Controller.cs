﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;

using th.co.toyota.stm.fas.Models.WFD02640;
//using th.co.toyota.stm.fas.Models.WFD02640;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02640Controller : BaseController
    {
        private WFD02640BO bo;
        protected WFD02640BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02640BO();
                return bo;
            }

        }
    

        public ActionResult Index(string DOC_NO, string COMPANY, string YEAR, string ROUND, string ASSET_LOCATION, string EMP_CODE)
        {
            //if (!base.CheckResignUser())
            //{
            //    return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            //}

            ViewBag.WFD02640FAADMIN = GlobalUser.UserSc2.IsAECUser;// User.IsFAAdmin;
           
            ViewBag.ConfirmNotice = this.BusinessLogic.GetSystemMessage("MFAS1303ACFM").VALUE;
            ViewBag.ConfirmReport = this.BusinessLogic.GetSystemMessage("MFAS1501ACFM").VALUE;
            ViewBag.NoticeSuccess = this.BusinessLogic.GetSystemMessage("MFA1503AINF").VALUE;
            ViewBag.ReportSuccess = this.BusinessLogic.GetSystemMessage("MSTD7001BINF", "Stock taking result report").VALUE;
            ViewBag.ErrorGenApprover = this.BusinessLogic.GetSystemMessage("MFAS1901AERR").VALUE;
            ViewBag.AllCostCenterIsSelected = this.BusinessLogic.GetSystemMessage(ASSET_LOCATION == "I" ? "MFAS1504AWRN" : "MFAS1505AWRN", YEAR, ROUND).VALUE;

            //Parameter
            ViewBag.Year = YEAR;
            ViewBag.Round = ROUND;
            ViewBag.Asset_Location = ASSET_LOCATION;
            ViewBag.Sv_Code = EMP_CODE;

            ViewBag.WFD02640SVEMPNAME = this.BusinessLogic.GetEmpTitle(COMPANY, YEAR, ROUND, ASSET_LOCATION, EMP_CODE);
            
            
            WFD02640BO _2640bo = new WFD02640BO();

            if (!string.IsNullOrEmpty(DOC_NO))
            {
                

                WFD02640SearchModel _initData = _2640bo.GetInitialDataByDocNo(DOC_NO);
                ViewBag.WFD02640FAADMIN = GlobalUser.UserSc2.IsAECUser;// User.IsFAAdmin;
                ViewBag.ConfirmNotice = this.BusinessLogic.GetSystemMessage("MFAS1303ACFM").VALUE;
                ViewBag.ConfirmReport = this.BusinessLogic.GetSystemMessage("MFAS1501ACFM").VALUE;
                ViewBag.NoticeSuccess = this.BusinessLogic.GetSystemMessage("MFA1503AINF").VALUE;
                
                ViewBag.ReportSuccess = this.BusinessLogic.GetSystemMessage("MSTD7001BINF", "Stock taking result report").VALUE;

                if (_initData != null)
                {
                    ViewBag.WFD02640SVEMPNAME = this.BusinessLogic.GetEmpTitle(_initData.COMPANY, 
                        _initData.YEAR, _initData.ROUND, _initData.ASSET_LOCATION, _initData.SV_CODE);

                    if (ViewBag.WFD02640SVEMPNAME == string.Empty)
                    {
                        //Show Error Message
                        throw (new Exception(this.BusinessLogic.GetSystemMessage("MFAS1909AERR", DOC_NO).VALUE));

                    }
                    //Parameter
                    ViewBag.Year = _initData.YEAR;
                    ViewBag.Round = _initData.ROUND;
                    ViewBag.Asset_Location = _initData.ASSET_LOCATION;
                    ViewBag.Sv_Code = _initData.SV_CODE;

                }
            }

            string _PermissionStock;
            if (YEAR != null)
            {
                _PermissionStock = _2640bo.GetActiveStatusPerSV(COMPANY,YEAR, ROUND, ASSET_LOCATION, EMP_CODE, GlobalUser.UserSc2.TFASTEmployeeNo);
                if (_PermissionStock != null && _PermissionStock == "Y")
                {//Set to view mode
                    ViewBag.MODE = "V";
                }
            }

            return PartialView();
        }
        // GET: WFD02640

        [HttpPost]
        public JsonResult onLoadScreen(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.GetOnSrcreenLoad(con));
        }
        [HttpPost]
        public JsonResult NoticeEmail(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.SaveNoticeEmail(con));
        }
        [HttpPost]
        public JsonResult SaveTempRequestStock(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.SaveTempRequestStockBO(con, Request));
        }

        public JsonResult UpdateAssetList(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.UpdateAssetList(con));
        }


        
        [HttpPost]
        public JsonResult InsertAssetList(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.InsertAssetList(con));
        }
        

        [HttpPost]
        public JsonResult SetInitialDataIncaseReSubmit(string DocNo)
        {
            return BaseJson(this.BusinessLogic.SetInitialDataIncaseReSubmit(DocNo));
        }

        [HttpPost]
        public JsonResult ClearAssetList(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.ClearAssetList(con));
        }

        
        [HttpPost]
        public JsonResult CheckFileUpload(CheckFileUploadModel data)
        {
            var d = this.BusinessLogic.CheckValidateUploadFile(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult DeleteAttchDoc(CheckFileUploadModel data)
        {
            var d = this.BusinessLogic.DeleteFileAttchDoc(data);
            return BaseJson(d);
        }
        [HttpPost]
        public JsonResult UpdateFileToTempStock(WFD02640SearchModel con)
        {
            return BaseJson(this.BusinessLogic.UpdateFileToTempStock(con));
        }

        [HttpPost]
        public JsonResult DeleteCostCodeAttchDoc(CheckFileUploadModel data)
        {
            var d = this.BusinessLogic.DeleteFileAttchDoc(data);
            return BaseJson(d);
        }
        [HttpPost]
        public JsonResult Prepare(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.PrepareCostCenterToGenerateFlow(data);
            return BaseJson(_rs);
        }
    }
}