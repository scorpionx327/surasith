﻿using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02115;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.Common;
using System.Collections.Generic;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02115Controller : BaseController
    {
        // GET: WFD02115
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Popup(WFD02115SaveModel _data)
        {
            if (string.IsNullOrEmpty(_data.GUID) && string.IsNullOrEmpty(_data.DOC_NO))
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            var _isAEC = GlobalUser.UserSc2.IsAECUser;

            ViewBag.USER_LOGIN = GlobalUser.UserSc2.User.UserId;

            if (string.IsNullOrEmpty(_data.DOC_NO))
            {
                _data.MODE = "N"; //New Mode
                ViewBag.IsAEC = GlobalUser.UserSc2.IsAECUser ? "Y" : "N";
                ViewBag.IsAECManager = GlobalUser.UserSc2.IsAECManager ? "Y" : "N";
                ViewBag.AllowEdit = "Y";
                //Force view mode
                if (!string.IsNullOrEmpty(_data.VIEW) && _data.VIEW == "Y")
                {
                    ViewBag.AllowEdit = "N";
                }
                else
                {
                    ViewBag.AllowEdit = "Y";
                }
            }
            else
            {
                //CHECK IS TEMP ASSET
                var isTempAsset = false; //_data.ASSET_NO.ToLower().Contains("temp");
                if(string.IsNullOrEmpty(_data.ASSET_NO) || _data.ASSET_NO.ToLower().Contains("temp"))
                {
                    isTempAsset = true;
                }

                var _permission = (new WFD01170BO()).GetPermission(new WFD0BaseRequestDocModel()
                {
                    DOC_NO = _data.DOC_NO,
                    USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo,
                    REQUEST_TYPE = _data.REQUEST_TYPE
                });

                var allowEdit = _permission.AllowEdit;

                if (_data.FLOW_TYPE == "KN")
                {
                    ViewBag.IsAEC = (_permission.Mode == "A" && GlobalUser.UserSc2.IsAECUser) ? "Y" : "N";
                    ViewBag.IsAECManager = GlobalUser.UserSc2.IsAECManager ? "Y" : "N";
                 
                    _data.MODE = "E"; //Edit Mode
                }
                else
                {
                    if (_permission == null || _permission.Mode == "N") //No Permission
                    {
                        return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
                    }

                    ViewBag.IsAEC = (_permission.Mode == "A" && GlobalUser.UserSc2.IsAECUser) ? "Y" : "N";
                    ViewBag.IsAECManager = GlobalUser.UserSc2.IsAECManager ? "Y" : "N";
                    //ViewBag.AllowEdit = _permission.AllowEdit;

                    var _docStatus = this.BusinessLogic.GetDocumentStatus(_data);

                    if ((!"GEN".Equals(_docStatus) && !"SUCCESS".Equals(_docStatus))
                        && ((_permission.Mode == "A" && GlobalUser.UserSc2.IsAECUser)
                            || "Y".Equals(_permission.AllowResubmit)))
                    {
                        _data.MODE = "E"; //Edit Mode
                    }
                    else
                    {
                        _data.MODE = "V"; //View Mode
                    }
                }

                if (_data.OPTION == "E")
                {
                    if (!isTempAsset)
                    {
                        allowEdit = "N";
                    }
                }

                ViewBag.AllowEdit = allowEdit;
            }

            if (string.IsNullOrEmpty(_data.DOC_NO) && string.IsNullOrEmpty(_data.ROLEMODE)) //No need Role mode when view data.
            {
                return RedirectToAction("ErrorPage", "Error", new { MessageCode = "NOROLEMODE" });
            }

            _data.ORIGINAL_ASSET_NO = _data.ASSET_NO;
            _data.ORIGINAL_ASSET_SUB = _data.ASSET_SUB;

            if (_data.FLOW_TYPE == "KN" && _data.LINE_NO == 0)
            {
                var _tempLineNo = this.BusinessLogic.GetLastLineNo(_data);
                _data.NEW_LINE_NO = _tempLineNo;
            }

            //Get Depreciation area type description from system master
            List<SystemModel> dpreTypeList = sysBo.SelectSystemDatas("FAS_TYPE", "DPRE_TYPE");

            _data.DPRE_TYPE_DESC_BC = dpreTypeList.Find(x => x.CODE == "BC")?.VALUE;
            _data.DPRE_TYPE_DESC_VL = dpreTypeList.Find(x => x.CODE == "VL")?.VALUE;
            _data.DPRE_TYPE_DESC_NA = dpreTypeList.Find(x => x.CODE == "NA")?.VALUE;

            //Get Asset Class Outsource Tools from system master
            _data.assetClassOutsource = sysBo.SelectSystemDatas("DPRE_KEY_OUTSOURCE_CONFIG", _data.COMPANY);

            //Load message
            ViewBag.MSTD0031AERR = this.BusinessLogic.GetSystemMessage("MSTD0031AERR", null).VALUE;

            return View(_data);
        }
        private WFD02115BO bo;
        private SystemBO sysBo = new SystemBO();
        protected WFD02115BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02115BO();
                return bo;
            }
        }

        private BusinessObject.Common.SystemBO systembo = new BusinessObject.Common.SystemBO();

        [HttpPost]
        public JsonResult WBSBudget_AutoComplete(string COMPANY, string FLOW_TYPE, string SCREEN_MODE, string IS_FILTER_BY_FISCAL_YEAR)
        {
            string empCode = GlobalUser.UserSc2.TFASTEmployeeNo;
            if ("V".Equals(SCREEN_MODE))
            {
                empCode = null;
            }
            var _rs = this.BusinessLogic.GetWBSBudgetAutoComplete(COMPANY, FLOW_TYPE, empCode, IS_FILTER_BY_FISCAL_YEAR);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult WBSProject_AutoComplete(string COMPANY, string FLOW_TYPE, string WBS_BUDGET)
        {
            var _rs = this.BusinessLogic.GetWBSProjectAutoComplete(COMPANY, GlobalUser.UserSc2.TFASTEmployeeNo, FLOW_TYPE, WBS_BUDGET);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult SaveAssetsData(WFD02115SaveModel _data)
        {
            _data.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            var d = this.BusinessLogic.SaveAssetsData(_data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult SaveAssetsReclassData(WFD02115SaveModel _data)
        {
            _data.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            var d = this.BusinessLogic.SaveAssetsReclassData(_data);
            return BaseJson(d);
        }


        [HttpPost]
        public JsonResult GetAssetsData(WFD02115SaveModel _data)
        {

            return BaseJson(this.BusinessLogic.GetFixedAssetList(_data));
        }
        [HttpPost]
        public JsonResult GetFixedAssetMaster(WFD02115SaveModel _data)
        {

            return BaseJson(this.BusinessLogic.GetFixedAssetMaster(_data));
        }

        [HttpPost]
        public JsonResult GetFixedAssetMasterForSettlement(WFD02115SaveModel _data)
        {

            return BaseJson(this.BusinessLogic.GetFixedAssetMasterForReclass(_data));
        }

        [HttpPost]
        public JsonResult GetFixedAssetMasterForReclass(WFD02115SaveModel _data)
        {

            return BaseJson(this.BusinessLogic.GetFixedAssetMasterForReclass(_data));
        }

        [HttpPost]
        public JsonResult GetWBSData(WFD02115WBSModel _data)
        {

            return BaseJson(this.BusinessLogic.GetWBSData(_data));
        }

        [HttpPost]
        public JsonResult GetOriginalAssetsData(WFD02115SaveModel _data)
        {

            return BaseJson(this.BusinessLogic.GetOriginalAssetsData(_data));
        }
        [HttpPost]
        public JsonResult GetDeriveWBSBudget(WFD02115WBSModel _data)
        {

            return BaseJson(this.BusinessLogic.GetDeriveWBSBudget(_data));
        }

        [HttpPost]
        public JsonResult GetDepreciationData(TB_M_DEPRECIATION _data)
        {
            return BaseJson(this.BusinessLogic.GetDefaultDepreciation(_data));
        }


        [HttpPost]
        public JsonResult GetDepreciationAutocomplete(string _company)
        {
            return BaseJson(this.BusinessLogic.GetDepreciationAutocomplete(_company));
        }
    }
}