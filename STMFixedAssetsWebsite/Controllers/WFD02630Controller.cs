﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02630;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02630Controller : BaseController
    {

        private WFD02630BO bo;
        protected WFD02630BO BO
        {
            get
            {
                if (bo == null) bo = new WFD02630BO();
                return bo;
            }
        }
        // GET: WFD02630
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            return View();
        }

        [HttpPost]
        public JsonResult GetYear(string company)
        {
            return BaseJson(BO.GetYearBO(company));
        }
        [HttpPost]
        public JsonResult GetRound(string company,string year)
        {
            return BaseJson(BO.GetRoundBO(company,year));
        }
        [HttpPost]
        public JsonResult GetAssetLocation(string company, string year, string round)
        {
            return BaseJson(BO.GetAssetLocationBO(company, year, round));
        }
        [HttpPost]
        public JsonResult GetHoliday(WFD02630SearchModel con)
        {
            return BaseJson(BO.GetHolidayBO(con));
        }
        [HttpPost]
        public JsonResult GetStockData(WFD02630SearchModel con)
        {
         
            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            con.ISFAADMIN = GlobalUser.UserSc2.IsAECUser ? "Y" : "N";
            return BaseJson(BO.GetStockDetail(con));
        }
        [HttpPost]
        public JsonResult GetDefaultScreen(WFD02630SearchModel con)
        {
            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            con.ISFAADMIN = GlobalUser.UserSc2.IsAECUser ? "Y" : "N";
            return BaseJson(BO.GetDefaultScreenBO(con));
        }
    }
}