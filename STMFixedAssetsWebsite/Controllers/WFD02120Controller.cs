﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02120;
using th.co.toyota.stm.fas.Models.COMSCC;
using th.co.toyota.stm.fas.Models.WFD02130;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02120Controller : BaseController
    {

        private WFD02120BO bo;
        protected WFD02120BO _wfd02120BO
        {
            get
            {
                if (bo == null) bo = new WFD02120BO();
                return bo;
            }
        }

        // GET: WFD02120
        public ActionResult Index(string PRINT_STATUS, string PHOTO_STATUS, String searchMode)
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            ViewBag.PRINT_STATUS = PRINT_STATUS;
            ViewBag.PHOTO_STATUS = PHOTO_STATUS;
            if (null != searchMode && !"".Equals(searchMode))
            {
                ViewBag.SearchMode = searchMode;
            }



            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD02120");
            ViewBag.COMSCC_ITEM_PER_PAGE = GetItemPerPage("COMSCC");

            ViewBag.ConfirmExportToExcel = _wfd02120BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Export Data").VALUE;
            ViewBag.ConfirmPrintToReport = _wfd02120BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "print all record").VALUE;
            ViewBag.ConfirmSelectedPrintToReport = _wfd02120BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "print the selected record").VALUE;
            ViewBag.ConfirmPrintTag = _wfd02120BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "print tag").VALUE;
            ViewBag.PrintTag_Error_NoSelect = _wfd02120BO.GetSystemMessage(Constants.MESSAGE.MCOM0001AERR, "print").VALUE;
            ViewBag.PrintTag_Err_NoInput = _wfd02120BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, "Location of Printer").VALUE;
            ViewBag.PrintTag_Validate = _wfd02120BO.GetSystemMessage("MFAS3603AERR", null).VALUE;

            ViewBag.UserLoginID = GlobalUser.UserSc2.TFASTEmployeeNo;
            ViewBag.IsCompany = GlobalUser.UserSc2.User.Company;
            ViewBag.IsFAAdmin = GlobalUser.UserSc2.IsAECUser ? "Y" : "N";
            ViewBag.IsSystemAdmin = GlobalUser.UserSc2.IsSystemAdmin ? "Y" : "N";

            COMSCCBO _ccBO = new COMSCCBO();
            COMSCCModel _m = new COMSCCModel();
            _m = _ccBO.getDefaultCostCode(ViewBag.UserLoginID);
            ViewBag.DefaultCostCode = string.Empty;
            if (_m != null)
            {
                ViewBag.DefaultCostCode = _m.COST_CODE;
            }

            return View();
        }

        [HttpPost]
        public JsonResult SearchFixedAssetAuto(WFD02120SearhConditionModel con, PaginationModel page)
        {
            con.UserId = GlobalUser.UserSc2.TFASTEmployeeNo;
            //con.IsFAAdmin = GlobalUser.UserSc2.IsFAAdmin ? "Y" : "N";
            //con.IsSystemAdmin = GlobalUser.UserSc2.IsSystemAdmin ? "Y" : "N";
            return SearchJson(_wfd02120BO.GetFixedAssetAuto(con, page));
        }

        [HttpPost]
        public JsonResult SearchFixedAsset(WFD02120SearhConditionModel con, PaginationModel page)
        {
            con.UserId = GlobalUser.UserSc2.TFASTEmployeeNo;
            //con.IsFAAdmin = GlobalUser.UserSc2.IsFAAdmin ? "Y" : "N";
            //con.IsSystemAdmin = GlobalUser.UserSc2.IsSystemAdmin ? "Y" : "N";
            return SearchJson(_wfd02120BO.GetFixedAsset(con, page));
        }
        public JsonResult PrintTag(List<WFD02120FixedAssetResultModel> datas)
        {
            return BaseJson(_wfd02120BO.AddPrintTag(datas, GlobalUser.UserSc2.IsAECUser));
        }

        public JsonResult ValidationPrintTag(List<WFD02120FixedAssetResultModel> datas)
        {
            return BaseJson(_wfd02120BO.ValidationAddPrintTag(datas, GlobalUser.UserSc2.IsAECUser));
        }

        public JsonResult RegiesterSelectedRow(List<WFD02120FixedAssetResultModel> datas)
        {
            string userId = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(_wfd02120BO.RegisterSelectedRowToTemp(datas, userId));
        }

        [HttpPost]
        public JsonResult GetPrintLocation()
        {
            WFD02130BO BO213 = new WFD02130BO();
            WFD02130UpdateDataModel _datas = new WFD02130UpdateDataModel
            {
                EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo
            };

            return BaseJson(BO213.GetPrintLocation(_datas));
        }
    }
}