﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.COMSCC;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class COMSCCController : BaseController
    {
        // GET: COMSCC
        private COMSCCBO bo;
        protected COMSCCBO BO
        {
            get
            {
                if (bo == null) bo = new COMSCCBO();
                return bo;
            }
        }

        [HttpPost]
        public JsonResult Index(string EmpCode,string CostCode,string AllCostCode, string AssetOwnerFlag,string PageRequest)
        {
            COMSCCDefaultScreenModel _Para = new COMSCCDefaultScreenModel();
            _Para.ALL_COST_CENTER = AllCostCode;
            _Para.EMP_CODE = EmpCode;
            _Para.COST_CODE = CostCode;
            _Para.AssetOwnerFlag = AssetOwnerFlag;
            _Para.PAGE_REQUEST = PageRequest;
            return BaseJson(_Para);
        }

        public ActionResult Index()
        {            
            return View();
        }

        [HttpPost]
        public JsonResult COMSCC_Search(COMSCCDefaultScreenModel con, PaginationModel page)
        {
            if (con.EMP_CODE == null || con.EMP_CODE == "")
            {
                con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            }
            return SearchJson(BO.Search(con, page));
        }
    }
}