﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD01270;
using th.co.toyota.stm.fas.Models.Common;
using System.IO;
using th.co.toyota.stm.fas.Models.WFD02640;
using th.co.toyota.stm.fas.Models.COMSCC;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Shared;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01270Controller : BaseController
    {
        // GET: WFD01270
        //private WFD01270BO bo;
        //protected WFD01270BO BO
        //{
        //    get
        //    {
        //        if (bo == null) bo = new WFD01270BO();
        //        return bo;
        //    }
        //}
        private BusinessObject.Common.SystemBO systembo = new BusinessObject.Common.SystemBO();


        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            ViewBag.SCREEN_MODE = "ADD";
            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01270");
            ViewBag.GUID = Guid.NewGuid();

            ViewBag.ScreenTitle = "Request Master Screen";
            return View();
        }

        [HttpGet]
        public ActionResult Index(string RequestType, string DocNo, string YEAR, string ROUND, string ASSET_LOCATION, string EMP_CODE)
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }
            WFD01270Model model = new WFD01270Model();

            List<WFD01270Model> _Permission = new List<Models.WFD01270.WFD01270Model>();

            ViewBag.ScreenTitle = "Request Master Screen";

            var _rs = systembo.SelectSystemDatas("SYSTEM_CONFIG", "SCREEN_REQUEEST_TITLE");
            if (_rs.Find(x => x.CODE == RequestType) != null)
            {
                ViewBag.ScreenTitle = _rs.Find(x => x.CODE == RequestType).VALUE;
            }

            if (RequestType == Constants.RequestType.TRANSFER)
            {
                model.DOC_NO = DocNo;
                model.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
                //    ViewBag.EnablePhone = BO.CheckPhonNoSVNew(model);
            }

            try
            {
                //  InitialValue(RequestType, DocNo, YEAR, ROUND, ASSET_LOCATION, EMP_CODE);
            }
            catch (Exception ex)
            {
                ViewBag.MODE = "S"; //
                ViewBag.NoAuthorize = ex.Message;
                return View();
            }

            if (string.IsNullOrEmpty(DocNo))
            {
                ViewBag.SCREEN_MODE = "ADD";
                ViewBag.GUID = Guid.NewGuid();
            }
            else
            {
                ViewBag.SCREEN_MODE = "EDIT";
            }



            model.DOC_NO = DocNo;
            model.REQUEST_TYPE = RequestType;
            model.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            // _Permission = BO.GetPermission(model);

            if (_Permission != null && _Permission.Count > 0)
            {
                ViewBag.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
                ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
                ViewBag.MODE = _Permission[0].MODE;
                ViewBag.DELEGATE = _Permission[0].DELEGATE;
                ViewBag.MAIN = _Permission[0].MAIN;
                ViewBag.HIGHER = _Permission[0].HIGHER;

                ViewBag.FA = _Permission[0].FA;

                ViewBag.ALLOW_APPROVE = _Permission[0].ALLOW_APPROVE;
                ViewBag.ALLOW_REJECT = _Permission[0].ALLOW_REJECT;
                ViewBag.IS_FA_ADMIN = _Permission[0].FA;
                ViewBag.DOC_STATUS = _Permission[0].DOC_STATUS;
                ViewBag.IS_BOI_STATE = _Permission[0].IS_BOI_STATE;
                ViewBag.IS_STORE_STATE = _Permission[0].IS_STORE_STATE;
                ViewBag.VIEW_BOI_STATE = _Permission[0].VIEW_BOI_STATE;
                ViewBag.VIEW_STORE_STATE = _Permission[0].VIEW_STORE_STATE;
                ViewBag.ALLOW_SEL_APPV = _Permission[0].ALLOW_SEL_APPV;
                ViewBag.IS_REQ = _Permission[0].IS_REQ;
                ViewBag.IS_ORG_CHANGED = _Permission[0].IS_ORG_CHANGED;

                ViewBag.VIEW_FA_STATE = _Permission[0].VIEW_FA_STATE;   //CR4 Suphachai L. 2017-06-23
                ViewBag.IS_FA_STATE = _Permission[0].IS_FA_STATE;       //CR4 Suphachai L. 2017-06-23

            }
            string _PermissionStock;
            //if (RequestType == "S" && YEAR != null)
            //{
            //    _PermissionStock = bo.GetActiveStatusPerSV(YEAR, ROUND, ASSET_LOCATION, EMP_CODE, model.EMP_CODE);
            //    if (_PermissionStock != null && _PermissionStock == "Y")
            //    {//Set to view mode
            //        ViewBag.MODE = "V";
            //    }
            //}

            SystemConfigurationDAO system_dao = new SystemConfigurationDAO();
            SystemConfigurationModels m = new SystemConfigurationModels();
            m.CATEGORY = "SYSTEM_CONFIG";
            m.SUB_CATEGORY = "CONSTANTS";
            m.CODE = "REDIR_COUNT";
            var data = system_dao.GetSystemData(m).Where(x => x.ACTIVE_FLAG == "Y").FirstOrDefault();
            if (data != null)
            {
                ViewBag.COUNTDOWN = data.VALUE;
            }

            return View();
        }

        //[HttpPost]
        //public JsonResult GetRequestor(WFD01270Model data)
        //{
        //    WFD01270Requestor model = new WFD01270Requestor();
        //    var _return = BO.GetRequestor(data, ref model);
        //    if (model != null)
        //    {

        //    }

        //    return BaseJson(model);
        //}
        //[HttpPost]
        //public JsonResult GenerateApprover(WFD01270MainModel data)
        //{
        //    var d = BO.GenerateApprover(data, "Y");
        //    return BaseJson(d);
        //}

        //[HttpPost]
        //public JsonResult GetApprover(WFD01270MainModel data)
        //{
        //    var d = BO.GenerateApprover(data, "N");
        //    return BaseJson(d);
        //}

        //[HttpPost]
        //public JsonResult SaveApprover(string DOC_NO, string APPROVER_LIST)
        //{
        //    var d = BO.SaveApprover(DOC_NO, APPROVER_LIST, GlobalUser.UserSc2.TFASTEmployeeNo);
        //    return BaseJson(d);
        //}

        [HttpPost]
        public JsonResult GetEmployeeInfo(string Approver)
        {
            WFD01210BO _emp = new WFD01210BO();
            var _d = _emp.GetEmployeeByCodeBO(new Models.WFD01210.WFD01210SearchModel() { EMP_CODE = Approver }, false);
            if (_d.ObjectResult == null)
            {
                return BaseJson(new BaseJsonResult() { ObjectResult = "-99" });
            }

            return BaseJson(_d);

        }
        //[HttpPost]
        //public ActionResult CommentHistory(WFD01270Model model)
        //{

        //    var _result = BO.GetCommentHistoryList(model);
        //    return PartialView(_result);
        //}

        //[HttpPost]
        //public ActionResult WorkflowTrackingProcess(WFD01270Model model)
        //{
        //    var _result = BO.GetWorkflowTrackingProcess(model);
        //    return PartialView(_result);
        //}

        //[HttpPost]
        //public JsonResult WFD01270_SubmitData(WFD01270MainModel data)
        //{

        //    var d = BO.SubmitRequest(data);
        //    return BaseJson(d);
        //}
        //[HttpPost]
        //public JsonResult WFD01270_ApproveData(WFD01270MainModel data)
        //{

        //    var d = BO.ApproveRequest(data);
        //    return BaseJson(d);
        //}
        //[HttpPost]
        //public JsonResult WFD01270_RejectRequest(WFD01270MainModel data)
        //{

        //    var d = BO.RejectRequest(data);
        //    return BaseJson(d);
        //}

        //public JsonResult GetTotalDocument(WFD01270Model data)
        //{
        //    var d = BO.GetTotalDocument(data);
        //    return BaseJson(d);
        //}
        //private void InitialValue(string RequestType, string DocNo, string YEAR, string ROUND, string ASSET_LOCATION, string EMP_CODE)
        //{
        //    COMSCCBO _ccBO = new COMSCCBO();
        //    COMSCCModel _m = new COMSCCModel();
        //    _m = _ccBO.getDefaultCostCode(GlobalUser.UserSc2.TFASTEmployeeNo);
        //    ViewBag.DefaultCostCode = string.Empty;
        //    if (_m != null)
        //    {
        //        ViewBag.DefaultCostCode = _m.COST_CODE;
        //    }

        //    ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01270");
        //    ViewBag.COMSCC_ITEM_PER_PAGE = GetItemPerPage("COMSCC");


        //    ViewBag.WFD02190_ITEM_PER_PAGE = GetItemPerPage("WFD02190");
        //    ViewBag.WFD02210_ITEM_PER_PAGE = GetItemPerPage("WFD02210");
        //    ViewBag.WFD02310_ITEM_PER_PAGE = GetItemPerPage("WFD02310");
        //    ViewBag.WFD02410_ITEM_PER_PAGE = GetItemPerPage("WFD02410");
        //    ViewBag.WFD02510_ITEM_PER_PAGE = GetItemPerPage("WFD02510");
        //    //Add New By Surasith t.
        //    ViewBag.WFD02710_ITEM_PER_PAGE = GetItemPerPage("WFD02710");
        //    ViewBag.WFD02810_ITEM_PER_PAGE = GetItemPerPage("WFD02810");
        //    ViewBag.WFD02910_ITEM_PER_PAGE = GetItemPerPage("WFD02910");
        //    ViewBag.WFD02A10_ITEM_PER_PAGE = GetItemPerPage("WFD02A10");




        //    ViewBag.ConfirmSubmit = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Submit ").VALUE;
        //    ViewBag.ConfirmReSubmit = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Re-submit ").VALUE;
        //    ViewBag.ConfirmApprove = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Approve ").VALUE;
        //    ViewBag.ConfirmReject = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Reject ").VALUE;
        //    ViewBag.ConfirmGenerateApprover = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Generate Approver List ").VALUE;
        //    ViewBag.ConfirmResetApprover = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Clear Approver List ").VALUE;
        //    ViewBag.ConfirmClear = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Clear ").VALUE;
        //    ViewBag.ConfirmDelete = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, " Delete {1} ").VALUE;
        //    ViewBag.NoAuthorize = BO.GetSystemMessage(Constants.MESSAGE.MSTD0069AERR).VALUE;
        //    ViewBag.HigherMessage = BO.GetSystemMessage(Constants.MESSAGE.MFAS0704AWRN).VALUE;
        //    ViewBag.ConfirmExportToExcel = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "Export all record to excel ").VALUE;
        //    ViewBag.ConfirmPrint = BO.GetSystemMessage(Constants.MESSAGE.MFAS1101CFM, "print a report ").VALUE;
        //    ViewBag.ShouldNotBeEmpty = BO.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, " {1} ").VALUE;
        //    ViewBag.MustBeNumber = BO.GetSystemMessage(Constants.MESSAGE.MSTD1002AERR, " {1} ").VALUE;
        //    ViewBag.OrgChanged = BO.GetSystemMessage(Constants.MESSAGE.MFAS0707AWRN).VALUE;
        //    ViewBag.FormatDateInvalid = BO.GetSystemMessage(Constants.MESSAGE.MSTD0047AERR, " {1} ", "dd-MMM-yyyy").VALUE;
        //    ViewBag.LessOrEqual = BO.GetSystemMessage(Constants.MESSAGE.MSTD0025AERR, "Sale Date", "Current Date").VALUE;

        //    ViewBag.REQUEST_TYPE = RequestType;
        //    ViewBag.DOC_NO = DocNo;
        //    ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
        //    ViewBag.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;

        //    ViewBag.WFD02640FAADMIN = GlobalUser.UserSc2.IsAECUser;// User.IsFAAdmin;
        //    ViewBag.WFD02640SVEMPNAME = "";//getNameEMp(EMP_CODE);
        //    ViewBag.ConfirmNotice = BO.GetSystemMessage("MFAS1502ACFM").VALUE;
        //    ViewBag.ConfirmReport = BO.GetSystemMessage("MFAS1501ACFM").VALUE;
        //    ViewBag.ErrorGenApprover = BO.GetSystemMessage("MFAS1901AERR").VALUE;

        //    //Parameter
        //    ViewBag.Year = YEAR;
        //    ViewBag.Round = ROUND;
        //    ViewBag.Asset_Location = ASSET_LOCATION;
        //    ViewBag.Sv_Code = EMP_CODE;


        //    if (RequestType == "S")
        //    {
        //        ViewBag.AllCostCenterIsSelected = BO.GetSystemMessage(ASSET_LOCATION == "I" ? "MFAS1504AWRN" : "MFAS1505AWRN",YEAR, ROUND).VALUE;

        //        if (!string.IsNullOrEmpty(DocNo))
        //        { 
        //        WFD02640BO _2640bo = new WFD02640BO();

        //        WFD02640SearchModel _initData = _2640bo.GetInitialDataByDocNo(DocNo);
        //            ViewBag.WFD02640FAADMIN = GlobalUser.UserSc2.IsAECUser;// User.IsFAAdmin;
        //            ViewBag.ConfirmNotice = BO.GetSystemMessage("MFAS1303ACFM").VALUE;
        //            ViewBag.ConfirmReport = BO.GetSystemMessage("MFAS1501ACFM").VALUE;
        //            ViewBag.NoticeSuccess = BO.GetSystemMessage("MFA1503AINF").VALUE;




        //            ViewBag.ReportSuccess = BO.GetSystemMessage("MSTD7001BINF", "Stock taking result report").VALUE;

        //            if (_initData != null)
        //            {
        //              //  ViewBag.WFD02640SVEMPNAME = _2640bo.GetEmpTitle(_initData.YEAR, _initData.ROUND, _initData.ASSET_LOCATION, _initData.SV_CODE);

        //                if (ViewBag.WFD02640SVEMPNAME == string.Empty)
        //                {
        //                    //Show Error Message
        //                    throw (new Exception(BO.GetSystemMessage("MFAS1909AERR", DocNo).VALUE));

        //                }
        //                //Parameter
        //                ViewBag.Year = _initData.YEAR;
        //                ViewBag.Round = _initData.ROUND;
        //                ViewBag.Asset_Location = _initData.ASSET_LOCATION;
        //                ViewBag.Sv_Code = _initData.SV_CODE;

        //            }
        //        }
        //    }

        //}

        //[HttpPost]
        //public JsonResult CheckFileUpload(CheckFileUploadModel data)
        //{
        //    var d = BO.CheckValidateUploadFile(data);
        //    return BaseJson(d);
        //}

    }
}
