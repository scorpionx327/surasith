﻿using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01110;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01110Controller : BaseController
    {
    
        protected UserPrinciple Sc2User = new UserPrinciple();
        private Log4NetFunction log4net = new Log4NetFunction();
        private CommonBO commonBO = new CommonBO();

        private WFD01110BO bo;

        protected WFD01110BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD01110BO();
                return bo;
            }
        }
        // GET: WFD01110
        public ActionResult Index()
        {
            try
            {
                // ----------------Set Session SC2
                GlobalUser.SetSC2Session();
                Sc2User = GlobalUser.UserSc2;
                // ------------------------------------
                
                if (Sc2User != null) {

                    if (commonBO.IsSc2OnlineMode()) {
                        if (!base.CheckResignUser()) {
                            //return RedirectToAction("Login", "Home");
                            return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
                        }
                    } else {
                        if (GlobalUser.UserSc2.TFASTEmployeeNo == null) {
                            return RedirectToAction("Login", "Home");
                        } else {
                            if (!base.CheckResignUser()) {
                                return RedirectToAction("Login", "Home");
                            }
                        }
                    }


                    ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
                    ViewBag.FAADMIN = Sc2User.IsAECUser;
                    ViewBag.AECUser = Sc2User.IsAECUser;// User.IsFAAdmin;
                    ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01110");

                    ViewBag.TODOLIST = this.BusinessLogic.GetToDoListBO(ViewBag.EMP_CODE);
                    ViewBag.TODOLISTFASup = this.BusinessLogic.GetToDoListFASupBO(ViewBag.EMP_CODE);

                    ViewBag.TAB = this.BusinessLogic.GetTotalPerTab(ViewBag.EMP_CODE); // OK
                
                    ViewBag.DafaultImage = this.BusinessLogic.GetImageDefalut();
                    ViewBag.MessageConfirmSave = this.BusinessLogic.GetMessage("MSTD0006ACFM").MESSAGE_TEXT;
                    ViewBag.MessageSuccess = this.BusinessLogic.GetMessage("MSTD0101AINF").MESSAGE_TEXT;
                    ViewBag.MessageConfirmCancelT = string.Format(this.BusinessLogic.GetMessage("MCOM0007ACFM").MESSAGE_TEXT, "Edit Text");
                    ViewBag.MessageConfirmCancelP = string.Format(this.BusinessLogic.GetMessage("MCOM0007ACFM").MESSAGE_TEXT, "Upload Photo");
                    ViewBag.CheckStocktake = this.BusinessLogic.CheckStockTake(GlobalUser.UserSc2.TFASTEmployeeNo);
                    ViewBag.FileExtension = GetExtension();
                } else {
                    ViewBag.TAB = new WFD01110TotalPerTabModel();
                    ViewBag.TODOLIST = new WFD01110ToDoListModel();
                    ViewBag.REQUESTSUMMARY = new WFD01110TotalRequestSummary();
                    ViewBag.TODOLISTFASup = new WFD01110ToDoListFASupModel();

                    UserPrinciple usersc2 = new UserPrinciple();
                    usersc2.User = new UserInfo();
                    usersc2.Acl = new AccessControlList();
                    usersc2.Acl.MapButtonAcl = new List<KeyValuePair<AccessControlButton, int>>();
                    usersc2.Acl.MapFunctionId = new List<KeyValuePair<string, string>>();
                    usersc2.Acl.MapGroupId = new List<KeyValuePair<string, string>>();
                    usersc2.Acl.MapScreenAcl = new List<KeyValuePair<AccessControlScreen, string>>();
                    usersc2.Acl.MapScreenId = new List<KeyValuePair<string, string>>();
                    usersc2.Acl.MapSubGroupId = new List<KeyValuePair<string, string>>();


                    HttpContext.Session[Constants.SESSION_USER_PROFILE]  = usersc2;
                    HttpContext.User = null;
                }


                //Set Condition
            
            



                if (UserPrincipleBO.IsAccessButton("WFD01110StockTakeCalendar", GlobalUser.UserSc2.Acl))
                {
                    ViewBag.IsEnableButtonWFD01110StockTakeCalendar = "Y";
                }
                else
                {
                    ViewBag.IsEnableButtonWFD01110StockTakeCalendar = "N";
                }

            }
            catch (Exception e)
            {
                log4net.WriteELogFile(e.Message);
            }

            return View();
        }
        //this.BusinessLogic.GetRequestSummary(GlobalUser.UserSc2.TFASTEmployeeNo); //OK

        [HttpPost]
        public JsonResult WFD01110GetAECSummary()
        {
            return SearchJson(this.BusinessLogic.GetRequestSummary(GlobalUser.UserSc2.TFASTEmployeeNo));
        }

        [HttpPost]
        public JsonResult WFD01110GetTransfer(WFD01110TransferModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetTransferBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        [HttpPost]
        public JsonResult WFD01110GetAssetNew(WFD01110AssetNewModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetAssetNewBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        [HttpPost]
        public JsonResult WFD01110GetSettlement(WFD01110SettlementModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetSettlementwBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        //Reclassification
        [HttpPost]
        public JsonResult WFD01110GetReclassification(WFD01110ReclassificationModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetReclassificationBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        //Asset Change
        [HttpPost]
        public JsonResult WFD01110GetAssetChange(WFD01110AssetChangeModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetAssetChangeBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        //Retirement
        [HttpPost]
        public JsonResult WFD01110GetRetirement(WFD01110RetirementModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetRetirementBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        //Imparment
        [HttpPost]
        public JsonResult WFD01110GetImparment(WFD01110ImpairmentModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetImparmentBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        //Asset Change
        [HttpPost]
        public JsonResult WFD01110GetReprint(WFD01110AssetChangeModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetReprintBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }


        //Physical Count
        [HttpPost]
        public JsonResult WFD01110GetPhysicalCount(WFD01110PhysicalCountModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetPhysicalCountBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        //[HttpPost]
        //public JsonResult WFD01110GetRequestByType(WFD01110SearchRequestModel con, PaginationModel page)
        //{
        //    return SearchJson(this.BusinessLogic.GetRequestByTypeBO(con, page));
        //}

        [HttpPost]
        public JsonResult GetSystemMaster()
        {
            return BaseJson(this.BusinessLogic.GetRequestTypeBO());
        }

        [HttpPost]
        public JsonResult TextAnnouncement(WFD01110SaveAnnouncementModel con)
        {
            con.ANNOUNCE_URL = "";
            con.ANNOUNCE_TYPE = "T";
            con.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            con.DISPLAY_FLAG = "Y";
            return BaseJson(this.BusinessLogic.SaveTextAnnouncement(con));
        }
        [HttpPost]
        public JsonResult UploadAnnouncement(WFD01110SaveAnnouncementModel con)
        {

            //if (Request.Files.Count > 0)
            //{

            //  Get all files from Request object  
            //HttpFileCollectionBase files = Request.Files;
            //for (int i = 0; i < files.Count; i++)
            //{
            //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
            //string filename = Path.GetFileName(Request.Files[i].FileName);  

            //HttpPostedFileBase file = files[0];
            //}

            //WFD01110SaveAnnouncementModel con = new WFD01110SaveAnnouncementModel();
            con.FILE = Request.Files[0];
            con.ANNOUNCE_DESC = "";
            //con.ANNOUNCE_URL = path;
            con.ANNOUNCE_TYPE = "P";
            con.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            con.DISPLAY_FLAG = "Y";
            return BaseJson(this.BusinessLogic.SaveTextAnnouncement(con, Request));
            //}
            //else
            //{
            //    return BaseJson(null);
            //}


        }
        [HttpPost]
        public JsonResult GetAnnouncement(WFD01110SearchRequestModel con)
        {
            var Resource = BaseJson(this.BusinessLogic.GetAnnouncementBO(con, GlobalUser.UserSc2.CompanyList));
            return Resource;
        }

        [HttpPost]
        public JsonResult GetTableStockTake(PaginationModel page)
        {
            //var data = this.BusinessLogic.GetStockTakeBO(GlobalUser.UserSc2.TFASTEmployeeNo);
            //int recordsTotal = 0;
            //recordsTotal = data != null ? data.Count : 0;
            //Json(new { recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
            return BaseJson(this.BusinessLogic.GetStockTakeBO(GlobalUser.UserSc2.TFASTEmployeeNo,page));
        }
        [HttpPost]
        public JsonResult WFD01110GetOnProcess(WFD01110SearchRequestModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetOnProcessBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }
        [HttpPost]
        public JsonResult WFD01110GetOnhandAEC(WFD01110SearchRequestModel con, PaginationModel page)
        {
            return SearchJson(this.BusinessLogic.GetOnHandAECBO(con, GlobalUser.UserSc2.TFASTEmployeeNo, page));
        }

        // Select tab depend on priority from system master by Pawares M. 20180718
        [HttpPost]
        public JsonResult GetTabPriority()
        {
            return BaseJson(this.BusinessLogic.GetTabPriority());
        }
    }
}