﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02810Controller : BaseController
    {
        public ActionResult Index(WFD0BaseRequestDocModel data)
        {
            //Initial
            data.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            this.BusinessLogic.init(data);
            
            return PartialView();
        }
        private WFD02810AOBO bo;
        protected WFD02810AOBO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02810AOBO();
                return bo;
            }
        }

        [HttpPost]
        public JsonResult GetAssetList(WFD0BaseRequestDocModel con, PaginationModel page)
        {
            var _result = this.BusinessLogic.GetAssetList(con, page);
            return SearchJson(_result, page);
        }
        [HttpPost]
        public JsonResult ClearAssetsList(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.ClearAssetList(data);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult Prepare(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.PrepareCostCenterToGenerateFlow(data, GlobalUser.UserSc2.IsAECUser);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult DeleteAsset(WFD02810Model data)
        {
            var _rs = this.BusinessLogic.DeleteAsset(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Resend(Models.WFD01270.WFD01170MainModel header, List<WFD02810Model> list)
        {
            var d = this.BusinessLogic.Resend(header, list, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(d);
        }
    }
}