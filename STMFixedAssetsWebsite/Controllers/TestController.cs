﻿using Net.Client.SC2;
using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.BusinessObject.Test;
using th.co.toyota.stm.fas.common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.Test;

namespace th.co.toyota.stm.fas.Controllers
{
    public class TestController : BaseController
    {
        // GET: Test
        public ActionResult DefaultLayout()
        {
              return View();
        }

        public ActionResult CollapseLayout()
        {
            return View();
        }

        public ActionResult DTExam()
        {
            return View();
        }

        public ActionResult UserView()
        {
            if (HttpContext.Session != null)
            {
                //For User Information (Authentication information)
                UserInfo userInfo = (UserInfo)HttpContext.Session[SC2Defaults.SessionUserInfo];
                if (userInfo != null)
                {
                    ViewBag.UserInfo = userInfo;
                } // if (userInfo != null) { 

                //For Access Control List (If authorization is enabled)
                AccessControlList acl = (AccessControlList)HttpContext.Session[SC2Defaults.SessionSystemAcl];
                if (acl != null)
                {
                    ViewData["SC2.Acl.SystemDn"] = acl.SystemDn;
                    ViewData["SC2.Acl.SystemUrl"] = acl.SystemUrl;

                    List<KeyValuePair<AccessControlScreen, string>> mapScreenAcl = acl.MapScreenAcl;
                    if (mapScreenAcl == null)
                    {
                        mapScreenAcl = new List<KeyValuePair<AccessControlScreen, string>>();
                    }
                    ViewBag.MapScreenAcl = mapScreenAcl;

                    List<KeyValuePair<AccessControlButton, int>> mapButtonAcl = acl.MapButtonAcl;
                    if (mapButtonAcl == null)
                    {
                        mapButtonAcl = new List<KeyValuePair<AccessControlButton, int>>();
                    }
                    ViewBag.MapButtonAcl = mapButtonAcl;

                    List<KeyValuePair<string, string>> mapFunctionId = acl.MapFunctionId;
                    if (mapFunctionId == null)
                    {
                        mapFunctionId = new List<KeyValuePair<string, string>>();
                    }
                    ViewBag.MapFunctionId = mapFunctionId;

                    List<KeyValuePair<string, string>> mapScreenId = acl.MapScreenId;
                    if (mapScreenId == null)
                    {
                        mapScreenId = new List<KeyValuePair<string, string>>();
                    }
                    ViewBag.MapScreenId = mapScreenId;

                    List<KeyValuePair<string, string>> mapGroupId = acl.MapGroupId;
                    if (mapGroupId == null)
                    {
                        mapGroupId = new List<KeyValuePair<string, string>>();
                    }
                    ViewBag.MapGroupId = mapGroupId;

                    List<KeyValuePair<string, string>> mapSubGroupId = acl.MapSubGroupId;
                    if (mapSubGroupId == null)
                    {
                        mapSubGroupId = new List<KeyValuePair<string, string>>();
                    }
                    ViewBag.MapSubGroupId = mapSubGroupId;

                    List<string> roleList = acl.RoleList;
                    if (roleList == null)
                    {
                        roleList = new List<string>();
                    }
                    ViewBag.RoleList = roleList;

                } //  if (acl != null)


            } // if (HttpContext.Session != null)
              //SC2: End sample of reading SC2 info from session object



            return View();
        }

        [HttpPost]
        public JsonResult SetUserData(string UserId)
        {
            UserPrinciple _Sc2User = new UserPrinciple();
            _Sc2User = GlobalUser.UserSc2;
            if (_Sc2User != null)
            {
                _Sc2User.User.UserId = UserId;
              
                _Sc2User.IsFAAdmin = new WFD01210BO().IsFAAdmin(UserId);

                _Sc2User.IsSystemAdmin = UserPrincipleBO.isSystemAdmin(_Sc2User.Acl.RoleList);

                HttpContext.Session[Constants.SESSION_USER_PROFILE] = _Sc2User;
                HttpContext.Session[SC2Defaults.SessionUserInfo] = _Sc2User.User;
    
            }

            return Json(new BaseJsonResult() { ObjectResult = new { userId = _Sc2User.User.UserId, IsFaAdmin = _Sc2User.IsFAAdmin }, DivMessages = null, Messages = null });
        }

        public ActionResult Common()
        {
            return View();
        }

        #region Date and time picker

        public ActionResult DateAndTimePicker()
        {
            return View();
        }
        [HttpPost]
        public JsonResult TestValidateDateAndTime(DateAndTimePickerModel data)
        {
            return BaseJson(new DateAndTimePickerBO().Validate(data));
        }

        #endregion


        #region Combobox

        public ActionResult BaseCombobox()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSystemSettingCombobox(string category, string subcategory)
        {
            var datas = new BaseComboboxBO().GetSystemSetting(category, subcategory);
            return BaseJson(new BaseJsonResult() { ObjectResult = datas });
        }


        #endregion


    }
}