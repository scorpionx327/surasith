﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01130;
using th.co.toyota.stm.fas.Models.COMSCC;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01130Controller : BaseController
    {
        private WFD01130BO bo;
        protected WFD01130BO BO
        {
            get
            {
                if (bo == null) bo = new WFD01130BO();
                return bo;
            }
        }
        //
        // GET: /WFD01130/
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }
            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01130");
            ViewBag.COMSCC_ITEM_PER_PAGE = GetItemPerPage("COMSCC");
            ViewBag.WFD02190_ITEM_PER_PAGE = GetItemPerPage("WFD02190");
            WFD01130SearchModel model = new WFD01130SearchModel();
            BO.GetSystemMaster(model);

            COMSCCBO _ccBO = new COMSCCBO();
            COMSCCModel _m = new COMSCCModel();
            _m = _ccBO.getDefaultCostCode(GlobalUser.UserSc2.TFASTEmployeeNo);
            ViewBag.DefaultCostCode = string.Empty;
            if (_m != null)
            {
                ViewBag.DefaultCostCode = _m.COST_CODE;
            }

            ViewBag.ShouldNotBeEmpty = this.bo.GetSystemMessage(Constants.MESSAGE.MSTD0031AERR, " {1} ").VALUE;

            return View(model);
        }

        [HttpPost]
        public JsonResult Search(WFD01130SearchModel con, PaginationModel page)
        {
            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            con.IS_SYS_ADMIN = GlobalUser.UserSc2.IsSystemAdmin?"Y":"N";
            return SearchJson(BO.SearchResult(con, page));
        }
        [HttpPost]
        public JsonResult GetAsset(string ASSET_NO, string COMPANY, string ASSET_SUB)
        {
            return BaseJson(BO.GetAssetName(ASSET_NO,  COMPANY,  ASSET_SUB));
        }
    }
}