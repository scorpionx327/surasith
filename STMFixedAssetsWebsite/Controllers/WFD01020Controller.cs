﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.SFAS.WFD01020;


namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01020Controller : BaseController
    {
        private WFD01020BO bo;

        protected WFD01020BO BO
        {
            get
            {
                if (bo == null) bo = new WFD01020BO();
                return bo;
            }
        }
       
        /*
        public ActionResult Index()
        {
            ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            return View();
        }
        */
        public ActionResult Index (string CATEGORY, string SUB_CATEGORY, string MODE)
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            ViewBag.CATEGORY = CATEGORY;
            ViewBag.SUB_CATEGORY = SUB_CATEGORY;
            ViewBag.MODE = MODE;


   
            if (UserPrincipleBO.IsAccessButton("WFD01020Edit", GlobalUser.UserSc2.Acl))
            {
                ViewBag.IsEnableButtonWFD01020Edit = "Y";
            }
            else
            {
                ViewBag.IsEnableButtonWFD01020Edit = "N";
            }
      
            return View();
        }      

        public ActionResult ManageSystem(string CATEGORY, string SUB_CATEGORY, string CODE)
        {
            ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            ViewBag.CATEGORY = CATEGORY;
            ViewBag.SUB_CATEGORY = SUB_CATEGORY;
            ViewBag.CODE = CODE;
            ViewBag.MODE = (string.IsNullOrWhiteSpace(CODE)? "ADD": "UPDATE");

            return View();
        }

        [HttpPost]
        public JsonResult LoadCategorySys()
        {
            var result = new WFD01020BO().GetCategory();
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult LoadSubCategorySys(string data)
        {
            var result = new WFD01020BO().GetSubCategory(data);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult SearchSystemMaster(WFD01020DataModel con, PaginationModel page)
        {
            return SearchJson(new WFD01020BO().SearchSystemMaster(con, page));
        }

        [HttpPost]
        public JsonResult GetSystemMaster(WFD01020DataModel data)
        {
            return BaseJson(new WFD01020BO().GetSystemMaster(data));                    
        }

        [HttpPost]
        public JsonResult InsertSystemMaster(WFD01020DataModel data)
        {
            return BaseJson(BO.InsertSystemMaster(data));
        }

        [HttpPost]
        public JsonResult UpdateSystemMaster(WFD01020DataModel data)
        {
            return BaseJson(BO.UpdateSystemMaster(data));
        }

        [HttpPost]
        public JsonResult ValidateSystemMaster(WFD01020DataModel data)
        {
            return BaseJson(new WFD01020BO().ValidateSystemMaster(data));
        }
    }
}