﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD01120;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01120Controller : BaseController
    {
        // GET: WFD01120
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01120");
            return View();
        }

        //public JsonResult Load()
        //{                    
        //    var result = new WFD01120BO().GetReportType();
        //    return Json(new { data = result });
        //}
        public JsonResult GetReportType()
        {
            var result = new WFD01120BO().GetReportType();
            return BaseJson(result);
        }
        public JsonResult GetReportName(string data)
        {
            var result = new WFD01120BO().GetReportName(data);
            return Json(new { data = result });
        }

        public JsonResult GetCostCenter(string data)
        {
            data = GlobalUser.UserSc2.TFASTEmployeeNo;
            var result = new WFD01120BO().GetCostCenter(data);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult GetChangeRequestDetail(WFD01120ChangeRequestHeader con, PaginationModel page)
        {
            return SearchJson(new WFD01120BO().GetChangeRequestDetail(con, page));
        }

        [HttpPost]
        public JsonResult CheckPrintValidation(WFD01120ChangeRequestHeader data)
        {
            //This function not executed
            return BaseJson(new WFD01120BO().CheckPrintValidation(data));
        }
        [HttpPost]
        public JsonResult GetStockTakingReport()
        {
            return BaseJson(new WFD01120BO().GetStockTakingReportBO(GlobalUser.UserSc2.TFASTEmployeeNo, GlobalUser.UserSc2.IsAECUser ? "Y" : "N"));
        }
    }
}