﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02210;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02210Controller : BaseController
    {
        // GET: WFD02210
        public ActionResult Index()
        {
            return View();
        }

        private WFD02210BO bo;
        protected WFD02210BO BO
        {
            get
            {
                if (bo == null) bo = new WFD02210BO();
                return bo;
            }
        }
        [HttpPost]
        public JsonResult WFD02210_GetAssetCIP(WFD02210Model con, PaginationModel page, string singleFlag, string Guid)
        {
            con.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            con.GUID = Guid;

            var _result = BO.GetAssetCIP(null, page);

            return SearchJson(_result, page);
        }

        [HttpPost]
        public JsonResult WFD02210_ClearAssetCIP(WFD02210Model con)
        {
            BO.ClearAssetCIP(null);
            return BaseJson("0");
        }

        [HttpPost]
        public JsonResult WFD02210_DeleteAssetCIP(WFD02210Model data)
        {
            var d = BO.DeleteAssetCIP(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult WFD02210_UpdateAssetList(WFD02210Model data)
        {
            try
            {
                BO.UpdateAssetList(data);
                return BaseJson("0");
            }
            catch (Exception ex)
            {
                return BaseJson("-99");
            }
        }
    }
}