﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class CommonController : BaseController
    {
        [HttpPost]
        public JsonResult AssetMap_AutoComplete(string _keyword)
        {
            var bo = new SystemBO();
            var _rs = bo.GetCommonMasterAutoComplete("COMMON", "ASSET_MAP", true, _keyword);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult AssetClass_AutoComplete(string _keyword)
        {
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("ASSET_CLASS", "ASSET_CLASS", true, _keyword);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult AssetClass_DropDown(string COMPANY, string _keyword)
        {
            var bo = new SystemBO();
            var _rs = bo.GetAssetClassAutoComplete(true, COMPANY, _keyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult AUCAssetClass_AutoComplete(string COMPANY, string _keyword)
        {
            var bo = new SystemBO();
            var _rs = bo.GetAUCAssetClassAutoComplete(true, COMPANY, _keyword);
            
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult FinalAssetClass_AutoComplete(string COMPANY, string _keyword)
        {
            var bo = new SystemBO();
            var _rs = bo.GetFinalAssetClassAutoComplete(true, COMPANY, _keyword);

            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult MinorCategory_AutoComplete(string COMPANY, string _keyword)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("EVA_GRP_1", COMPANY, true, _keyword);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult MinorCategory_DropDown(string COMPANY, string ASSET_CLASS)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetMinorCategoryByAssetClass(true, COMPANY, ASSET_CLASS);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult BOI_AutoComplete(string COMPANY, string _keyword)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("EVA_GRP_4", COMPANY, true, _keyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult TaxPrivilegeAutoComplete(string COMPANY, string CODE, string _keyword)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoCompleteWhereRemark("EVA_GRP_4", COMPANY, CODE, true, _keyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult MachineLicense_AutoComplete(string COMPANY, string _keyword)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("EVA_GRP_3", COMPANY, true, _keyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Location_AutoComplete(string COMPANY, string _keyword)
        {
            var _bo = new CommonBO();
            var bo = new SystemBO();
            var _rs = bo.GetSystemMasterAutoComplete("LOCATION", COMPANY, true, _keyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult CostCenter_AutoComplete(string _keyword)
        {
            var _bo = new CommonBO();
            var emp = GlobalUser.UserSc2.TFASTEmployeeNo;
            var _rs = _bo.GetCostCenterAutoComplete(_keyword, emp);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult ResponsibleCostCenter_AutoComplete(string _keyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetResponsibleCenterAutoComplete(_keyword, string.Empty);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult WBSBudget_AutoComplete(string COMPANY, string _keyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetWBSBudgetAutoComplete(COMPANY, _keyword, string.Empty);
            return BaseJson(_rs);
        }
        [HttpPost]
        public JsonResult WBSProject_AutoComplete(string COMPANY,string _keyword)
        {
            var _bo = new CommonBO();
           var _rs = _bo.GetWBSProjectAutoComplete(COMPANY, _keyword, string.Empty);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult AssetPlate_AutoComplete(string _keyword)
        {
          var _bo = new CommonBO();
            var _rs = _bo.GetFixedAssetAutoComplete(_keyword, string.Empty);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Division_AutoComplete(string _keyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetDivisionAutoComplete(_keyword, string.Empty);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult SubDivistion_AutoComplete(string _keyword, string _divKeyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetSubDivistionAutoComplete(_keyword, string.Empty, _divKeyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Department_AutoComplete(string _keyword, string _subDivKeyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetDepartmentAutoComplete(_keyword, string.Empty, _subDivKeyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Section_AutoComplete(string _keyword, string _depKeyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetGetSectionAutoComplete(_keyword, string.Empty, _depKeyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Line_AutoComplete(string _keyword, string _secKeyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetGetLineAutoComplete(_keyword, string.Empty, _secKeyword);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Position_AutoComplete(string _keyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetGetPositionAutoComplete(_keyword, string.Empty);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Job_AutoComplete(string _keyword)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetJobAutoComplete(_keyword, string.Empty);
            return BaseJson(_rs);
        }

        
        [HttpPost]
        public JsonResult Employee_AutoComplete(string _keyword, string company)
        {
            var _bo = new CommonBO();
            var _rs = _bo.GetEmployeeAutoComplete(_keyword, string.Empty, company);
            return BaseJson(_rs);
        }
    }
}