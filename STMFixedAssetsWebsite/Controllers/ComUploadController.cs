﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class ComUploadController : BaseController
    {
        // GET: ComUpload
        public ActionResult Index()
        {
            return View();
        }
        private ComUploadBO bo;
        protected ComUploadBO BO
        {
            get
            {
                if (bo == null) bo = new ComUploadBO();
                return bo;
            }
        }
        [HttpPost]
        public JsonResult CheckFileUpload()
        {
            var d = BO.ValidateUploadFile(Request);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult UploadAttchDoc()
        {
            var d = BO.UploadFileAttchDoc(Request);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult AssetDeleteAttchDoc()
        {
            var d = BO.AssetDeleteFileAttchDoc(Request);
            return BaseJson(d);
        }
        [HttpPost]
        public JsonResult GetAttchDoc(FileDownloadModel data)
        {
            var d = BO.GetFileAttchDoc(data);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult SaveAttchDoc(List<FileDownloadModel> data)
        {
            var d = BO.InsertFileAttchDoc(data);
            return BaseJson(d);
        }
        [HttpPost]
        public JsonResult DownloadDoc(FileDownloadModel data)
        {
            var d = BO.GetFileAttchDoc(data);
            return BaseJson(d);
        }
        public JsonResult DownloadFileZip(FileDownloadModel data)
        {
            var d = BO.SaveToZipFileAttchDoc(data);
            
            return BaseJson(d);
        }

        public ActionResult DownloadZip(string _filePath)
        {
            if (System.IO.File.Exists(_filePath))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(_filePath);
                string fileName = System.IO.Path.GetFileName(_filePath);
                return File(fileBytes, System.Web.MimeMapping.GetMimeMapping(fileName), fileName);
            }
            return null;
        }
    }
}