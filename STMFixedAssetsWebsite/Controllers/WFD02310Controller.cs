﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02310;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02310Controller : BaseController
    {
        // GET: WFD02210
        public ActionResult Index(WFD0BaseRequestDocModel data)
        {
            //Initial
            data.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo ;
            this.BusinessLogic.init(data);
            return PartialView();
        }

        private WFD02310BO bo;
        protected WFD02310BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02310BO();
                return bo;
            }
        }

        [HttpPost]
        public JsonResult GetAssetList(WFD0BaseRequestDocModel con, PaginationModel page)
        {
            var _result = this.BusinessLogic.GetAssetList(con, page);
            return SearchJson(_result, page);
        }

        [HttpPost]
        public JsonResult AddAsset(List<WFD0BaseRequestDetailModel> data)
        {
            var _rs = this.BusinessLogic.InsertAsset(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult ClearAssetsList(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.ClearAssetList(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            var _rs = this.BusinessLogic.DeleteAsset(data);
            return BaseJson(_rs);
        }

        // Operation when user click button on request master screen
        // 1270.js call request.JS -> collect data -> this function and return data to main screen
        [HttpPost]
        public JsonResult Prepare(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.PrepareCostCenterToGenerateFlow(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult UpdateAssetList(List<WFD02310Model> list)
        {
            var _rs = this.BusinessLogic.UpdateAssetList(list, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(_rs);
        }
        
    }
}