﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02190;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02190Controller : BaseController
    {
        // GET: WFD02190
        public ActionResult Index()
        {

            ViewBag.MCOM0004AERR = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MCOM0004AERR, "add").VALUE;
            ViewBag.MCOM0010AWRN = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MCOM0010AWRN).VALUE;
            ViewBag.MFAS1902AERR = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MFAS1902AERR,null).VALUE;
            ViewBag.MFAS1913AERR = this.BusinessLogic.GetSystemMessage("MFAS1913AERR", null).VALUE;
            ViewBag.MFAS1914AERR = this.BusinessLogic.GetSystemMessage("MFAS1914AERR", null).VALUE;
            ViewBag.MFAS1917AERR = this.BusinessLogic.GetSystemMessage("MFAS1917AERR", null).VALUE;

            return PartialView();
        }

        private WFD02190BO bo;
        protected WFD02190BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02190BO();
                return bo;
            }
        }

        [HttpPost]
        public JsonResult Search(WFD02190SearchModel con, PaginationModel page)
        {
            con.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;
            return SearchJson(this.BusinessLogic.Search(con, page));
        }

        [HttpPost]
        public JsonResult Save(WFD02190SaveModel data)
        {
            if (string.IsNullOrEmpty(data.USER_BY ))
            {
                data.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            }
            var d = this.BusinessLogic.SaveAssetsData(data);
            return BaseJson(d);
        }
    }
}