﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    [Route("BatchProcess")]
    public class BatchProcessController : BaseController
    {
        private BatchProcessBO bo;
        protected BatchProcessBO BO
        {
            get
            {
                if (bo == null) bo = new BatchProcessBO();
                return bo;
            }
        }
        [HttpPost]
        public JsonResult GetBatchStatus(int appId)
        {
            return BaseJson(BO.GetBatchStatus(appId));
        }
        [HttpPost]
        public JsonResult GetBatchUserAppId(string batchId, string userId)
        {
            return BaseJson(BO.GetUserBatchAppId(batchId, userId));
        }
        [HttpPost]
        public JsonResult StartBatch(StartBatchModel data)
        {

            data.UserId =  GlobalUser.UserSc2.TFASTEmployeeNo;
            data.Description = "Calling by screen";

            return BaseJson(BO.addBatchQ(data));
        }
        [HttpPost]
        public JsonResult CancelBatch(int appId)
        {
            string UserId = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.CancelBatch(appId, UserId));
        }

        [HttpPost]         
        public FileResult DownloadFile(decimal AppId)
        {
            
            string fileName = string.Empty;
            string filePath = string.Empty;
            //byte[] fb = BO.GetFileDownload(AppId, ref fileName,ref filePath);
            byte[] fb = BO.ReadLocalFile(AppId, true, ref fileName, ref filePath);
            return File(fb, GetMimeType(Path.GetExtension(fileName)), fileName);
        }

        [HttpGet]
        public FileResult DownloadFileName(decimal AppId)
        {

            string fileName = string.Empty;
            string filePath = string.Empty;
            //byte[] fb = BO.GetFileDownload(AppId, ref fileName,ref filePath);
            byte[] fb = BO.ReadLocalFile(AppId, true, ref fileName, ref filePath);
            return File(fb, GetMimeType(Path.GetExtension(fileName)), fileName);
        }

        //[Authorize] [HttpPost]
        //public FileResult DownloadFile(decimal AppId)
        //{
        //    string fileName = string.Empty;
        //    string filePath = string.Empty;
        //    byte[] fb = BO.ReadLocalFile(AppId, true, ref fileName, ref filePath);
        //    return File(fileData.FileStream, fileData.ContentType, fileName);
        //}


    }
}