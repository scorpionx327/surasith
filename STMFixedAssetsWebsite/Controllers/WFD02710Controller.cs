﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02710;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02710Controller : BaseController
    {
        // GET: WFD02710
        
        public ActionResult Index(WFD0BaseRequestDocModel data)
        {
            //Initial
            data.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            this.BusinessLogic.init(data);

            


            return PartialView();
        }

        private WFD02710BO bo;
        protected WFD02710BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02710BO();
                return bo;
            }
        }

        [HttpPost]
        public JsonResult GetAssetList(WFD0BaseRequestDocModel con, PaginationModel page)
        {
            var _result = this.BusinessLogic.GetAssetList(con, page);
            return SearchJson(_result, page);
        }

        [HttpPost]
        public JsonResult AddAsset(List<WFD0BaseRequestDetailModel> data)
        {
            var _rs = this.BusinessLogic.InsertAsset(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult ClearAssetsList(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.ClearAssetList(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult DeleteAsset(WFD0BaseRequestDetailModel data, string invNo, string invLine)
        {
            var _rs = this.BusinessLogic.DeleteAsset(data, invNo, invLine, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(_rs);
        }

        // Operation when user click button on request master screen
        // 1270.js call request.JS -> collect data -> this function and return data to main screen
        [HttpPost]
        public JsonResult Prepare(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.PrepareCostCenterToGenerateFlow(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult UpdateAssetList(List<WFD02710TargetModel> list)
        {
            var _rs = this.BusinessLogic.UpdateTargetAssetList(list);
            return BaseJson(_rs);
        }

        //[HttpPost]
        //public JsonResult UpdateSettlement(WFD02710AUCModel data)
        //{
        //    var _rs = this.BusinessLogic.UpdateSettlement(data, GlobalUser.UserSc2.TFASTEmployeeNo);
        //    return BaseJson(_rs);
        //}

        //[HttpPost]
        //public JsonResult AverageCost(WFD02710AUCModel data, List<WFD02710TargetModel> selectedList)
        //{
        //    var _rs = this.BusinessLogic.AverageCost(data, selectedList, GlobalUser.UserSc2.TFASTEmployeeNo);
        //    return BaseJson(_rs);
        //}

        [HttpPost]
        public JsonResult UpdateSettlement(List<WFD02710AUCModel> datas, string costStr)
        {
            var _rs = this.BusinessLogic.UpdateSettlements(datas, costStr, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult AverageCost(List<WFD02710AUCModel> datas, List<WFD02710TargetModel> selectedList)
        {
            var _rs = this.BusinessLogic.AverageCosts(datas, selectedList, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult InsertNewToTargetAssets(WFD02710SaveModel data)
        {
            var _rs = this.BusinessLogic.InsertNewToTargetAssets(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult InsertExistsToTargetAssets(List<WFD0BaseRequestDetailModel> data)
        {
            var _rs = this.BusinessLogic.InsertExistsToTargetAssets(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult DeleteTargetAsset(WFD0BaseRequestDetailModel data)
        {
            var _rs = this.BusinessLogic.DeleteTargetAsset(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult Resend(Models.WFD01270.WFD01170MainModel header, List<WFD0BaseRequestDetailModel> list)
        {
            var d = this.BusinessLogic.Resend(header, list, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(d);
        }

        [HttpPost]
        public JsonResult UpdateCapDate(string guid, string docNo, string tempAssetNo, string tempSubNo, string capDateStr)
        {
            var d = BusinessLogic.UpdateCapDate(guid, docNo, tempAssetNo, tempSubNo, capDateStr);
            return BaseJson(d);
        }
    }
}