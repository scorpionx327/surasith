﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD02410;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.COMSCC;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD02410Controller : BaseController
    {
        // GET: WFD02410
        public ActionResult Index(WFD0BaseRequestDocModel data)
        {
            //Initial
            data.USER_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            this.BusinessLogic.init(data);

            //Check Is require TMC
            ViewBag.MSTD0059AERR = this.BusinessLogic.GetSystemMessage(Constants.MESSAGE.MSTD0059AERR, null).VALUE;
            return PartialView();
        }

        private WFD02410BO bo;
        protected WFD02410BO BusinessLogic
        {
            get
            {
                if (bo == null) bo = new WFD02410BO();
                return bo;
            }
        }
        [HttpPost]
        public JsonResult SearchCostCenter(COMSCCDefaultScreenModel con, PaginationModel page)
        {
            var _result = this.BusinessLogic.SearchCostCenter(con, page);
            return SearchJson(_result, page);
            
        }

        [HttpPost]
        public JsonResult GetNewAssetOwner(WFD02410HeaderModel con)
        {
            var _rs = this.BusinessLogic.GetNewAssetOwnerTransfer(con);
            return BaseJson(_rs);            
        }
        [HttpPost]
        public JsonResult WFD02410_IsMassPermission(WFD0BaseRequestDocModel con)
        {
            // CheckMassPermission
            return BaseJson((new WFD02510BO()).CheckMassPermission(con));
        }
        [HttpPost]
        public JsonResult GetAssetList(WFD0BaseRequestDocModel con, PaginationModel page)
        {
            var _result = this.BusinessLogic.GetAssetList(con, page);
            return SearchJson(_result, page);
        }

        [HttpPost]
        public JsonResult AddAsset(List<WFD0BaseRequestDetailModel> data)
        {
            var _rs = this.BusinessLogic.InsertAsset(data);
            return BaseJson(_rs);
        }


        [HttpPost]
        public JsonResult ClearAssetsList(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.ClearAssetList(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult DeleteAsset(WFD0BaseRequestDetailModel data)
        {
            var _rs = this.BusinessLogic.DeleteAsset(data);
            return BaseJson(_rs);
        }

        // Operation when user click button on request master screen
        // 1270.js call request.JS -> collect data -> this function and return data to main screen
        [HttpPost]
        public JsonResult Prepare(WFD0BaseRequestDocModel data)
        {
            var _rs = this.BusinessLogic.PrepareCostCenterToGenerateFlow(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult UpdateAssetList(WFD02410HeaderModel _data)
        {
            var _rs = this.BusinessLogic.UpdateAssetList(_data, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(_rs);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult GetTransferReason(WFD02410Model data)
        {
            var d = this.BusinessLogic.GetTransferReason(data);
            return BaseJson(d);
        }
        [HttpPost] //data in dropdown Location
        public JsonResult GetDropdownLocation(WFD02410Model data)
        {
            var d = this.BusinessLogic.GetDropdownLocation(data);
            return BaseJson(d);
        }
        [HttpPost] //data in dropdown WBSCosProject CostCenter
        public JsonResult GetDropdownWBSCosProject(WFD02410Model data)
        {
            var d = this.BusinessLogic.GetDropdownWBSCosProject(data);
            return BaseJson(d);
        }
        [HttpPost] //data in dropdown ToEmployee
        public JsonResult GetDropdownToEmployee(WFD02410NewAssetOwnerModel data)
        {
            var d = this.BusinessLogic.GetDropdownToEmployee(data);
            return BaseJson(d);
        }

        [HttpPost] 
        public JsonResult Resend(Models.WFD01270.WFD01170MainModel header, WFD02410HeaderModel data)
        {
            var d = this.BusinessLogic.Resend(header, data, GlobalUser.UserSc2.TFASTEmployeeNo);
            return BaseJson(d);
        }


    }
}