﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.WFD01180;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Controllers;
using System.Drawing;
using System.Text.RegularExpressions;
using th.co.toyota.stm.fas.Models.COMSCC;
using th.co.toyota.stm.fas.DAO.Shared;
using th.co.toyota.stm.fas.Models.Shared;


namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01180Controller : BaseController
    {
        // GET: WFD01180
        public ActionResult Index()
        {
            if (!base.CheckResignUser()){
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });
            }

            var _reqInfo = new RequestPageInfoModels();
            _reqInfo = this.GetFunctionID("M");
            ViewBag.FUNCTION_ID = _reqInfo.FunctionID;
            ViewBag.ITEM_PER_PAGE = GetItemPerPage(_reqInfo.FunctionID);
            ViewBag.ScreenTitle = _reqInfo.FunctionTitle;
            ViewBag.RequestType = "M";
            ViewBag.STATUS = "00";
            ViewBag.FlowType = "";

            ViewBag.MSTD0010AERR = this.BusinessLogic.GetSystemMessage("MSTD0010AERR", null).VALUE;
            ViewBag.MSTD0031AERR = this.BusinessLogic.GetSystemMessage("MSTD0031AERR", null).VALUE;
            ViewBag.MSTD0010AERR = this.BusinessLogic.GetSystemMessage("MSTD0010AERR", null).VALUE;
            ViewBag.MSTD1008AERR = this.BusinessLogic.GetSystemMessage("MSTD1008AERR", null).VALUE;
            ViewBag.MSTD0006ACFM = this.BusinessLogic.GetSystemMessage("MSTD0006ACFM", null).VALUE;
            ViewBag.MSTD0101AINF = this.BusinessLogic.GetSystemMessage("MSTD0101AINF", null).VALUE;
            ViewBag.MCOM0007ACFM = this.BusinessLogic.GetSystemMessage("MCOM0007ACFM", null).VALUE;
            ViewBag.MCOM0004AERR = this.BusinessLogic.GetSystemMessage("MCOM0004AERR", null).VALUE;
            ViewBag.MSTD0001ACFM = this.BusinessLogic.GetSystemMessage("MSTD0001ACFM", null).VALUE;
            ViewBag.MCOM0008AERR = this.BusinessLogic.GetSystemMessage("MCOM0008AERR", null).VALUE;
            ViewBag.MSTD0090AINF = this.BusinessLogic.GetSystemMessage("MSTD0090AINF", null).VALUE;
            ViewBag.MSTD0059AERR = this.BusinessLogic.GetSystemMessage("MSTD0059AERR", null).VALUE;
            ViewBag.MSTD0085AINF = this.BusinessLogic.GetSystemMessage("MSTD0085AINF", null).VALUE;


            return PartialView();
        }

        private BusinessObject.Common.SystemBO systembo = new BusinessObject.Common.SystemBO();
        public RequestPageInfoModels GetFunctionID(string _RequestType)
        {
            var _rs = systembo.SelectSystemDatas("SYSTEM_CONFIG", "SCREEN_REQUEST_INFO");
            if (_rs.Find(x => x.CODE == _RequestType) != null)
            {
                var _data = _rs.Find(x => x.CODE == _RequestType);
                return new RequestPageInfoModels() { FunctionID = _data.VALUE, FunctionTitle = _data.REMARKS };
            }
            return new RequestPageInfoModels() { };

        }

        private WFD01180BO bo;
        protected WFD01180BO BO
        {
            get
            {
                if (bo == null) bo = new WFD01180BO();
                return bo;
            }
        }


        private WFD01180BO boAsset;
        protected WFD01180BO BusinessLogic
        {
            get
            {
                if (boAsset == null) boAsset = new WFD01180BO();
                return boAsset;
            }
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_GetAssetClass(WFD01180MSystemModel data, PaginationModel page)
        {
            var d = this.BusinessLogic.GetAssetClass(data, page);
            return SearchJson(d, page);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_GetMinorCategory(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            var d = BO.GetMinorCategory(data, page);
            return BaseJson(d);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_GetDepreciation(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            var d = BO.GetDepreciation(data, page);
            return BaseJson(d);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_ChkDepreciation(WFD0BaseRequestDetailModel data, PaginationModel page)
        {
            var d = BO.ChkDepreciation(data, page);
            return BaseJson(d);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_GetCompany(WFD01180MSystemModel data, PaginationModel page)
        {
            var d = BO.GetCompany(data, page);
            return SearchJson(d, page);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_GetPlateType(WFD01180MSystemModel data, PaginationModel page)
        {
            var d = BO.GetPlateType(data, page);
            return SearchJson(d, page);
        }

        [HttpPost]
        public JsonResult WFD01180_CHKAssetClass(WFD01180MSystemModel data, PaginationModel page)
        {
            var d = BO.CHKAssetClass(data, page);
            return SearchJson(d, page);
        }

        [HttpPost] //data in dropdown transfer reason 
        public JsonResult WFD01180_GetPlateSize(WFD01180MSystemModel data, PaginationModel page)
        {
            var d = BO.GetPlateSize(data, page);
            return SearchJson(d, page);
        }

        [HttpPost]
        public JsonResult WFD01180_InsDeprecistion(WFD01180DepreciationModel data)
        {
            var _rs = this.BusinessLogic.InsDeprecistion(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult WFD01180_UpdDeprecistion(WFD01180DepreciationModel data)
        {
            var _rs = this.BusinessLogic.UpdDeprecistion(data);
            return BaseJson(_rs);
        }

        [HttpPost]
        public JsonResult WFD01180_DelDeprecistion(WFD01180DepreciationModel data)
        {
            var _rs = this.BusinessLogic.DelDeprecistion(data);
            return BaseJson(_rs);
        }
    }
}