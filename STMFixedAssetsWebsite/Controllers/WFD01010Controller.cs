﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.Models.WFD01010;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01010Controller : BaseController
    {
        // GET: WFD01010
        WFD01010BO bo;

        protected WFD01010BO BO
        {
            get
            {
                if (bo == null) bo = new WFD01010BO();
                return bo;
            }
        }
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }
            ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;                                 
            return View();
        }
        public ActionResult Detail(string Det,string Module, string App,string Func, string Stat,
                string User,string From,string To,string Fav,string ByApp)
        {
            ViewBag.EMP_CODE = GlobalUser.UserSc2.TFASTEmployeeNo;         
            return View();
        }
        [HttpPost]
        public JsonResult GetFunction()
        {
            return BaseJson(BO.GetFunctionBO());
        }
        [HttpPost]
        public JsonResult GetModuleLoggin()
        {
            return BaseJson(BO.GetModuleBO());
        }
        [HttpPost]
        public JsonResult GetStatusLoggin()
        {
            return BaseJson(BO.GetStatusLoggingBO());
        }
        [HttpPost]
        public JsonResult GetLevelLoggin()
        {
            return BaseJson(BO.GetLevelLoggingBO());
        }      
        [HttpPost]
        public JsonResult GetSearchLoggin(WFD01010DefaultScreenModel con, PaginationModel page)
        {
            return SearchJson(BO.GetSearchLoggin(con, page));
        }
        [HttpPost]
        public JsonResult GetDetailLoggin(WFD01010DefaultScreenModel con, PaginationModel page)
        {
            return SearchJson(BO.GetDetailLoggin(con, page));
        }     
        [HttpGet]
        public virtual ActionResult Download(string file)
        {
            string  filename = System.IO.Path.GetFileName(file);
            string contentType = GetMimeType(file);
            return File(file, contentType, filename);
        }       
    }
}