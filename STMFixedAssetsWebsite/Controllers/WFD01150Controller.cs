﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.BusinessObject.SFAS;
using th.co.toyota.stm.fas.Models.BaseModel;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.SFAS.WFD01150;

namespace th.co.toyota.stm.fas.Controllers
{
    public class WFD01150Controller : BaseController
    {
        private WFD01150BO _bo;
        protected WFD01150BO BO
        {
            get
            {
                if (_bo == null) _bo = new WFD01150BO();
                return _bo;
            }
        }
        // GET: WFD01150
        public ActionResult Index()
        {
            if (!base.CheckResignUser())
            {
                return RedirectToAction("ErrorPage", "Errors", new { MessageCode = "MFAS1910AERR" });

            }

            ViewBag.ITEM_PER_PAGE = GetItemPerPage("WFD01150");

            ViewBag.MSTD0006ACFM = BO.GetSystemMessage(Constants.MESSAGE.MSTD0006ACFM).VALUE;
            ViewBag.MCOM0007ACFM = BO.GetSystemMessage(Constants.MESSAGE.MCOM0007ACFM, "[PROCESS]").VALUE;
            ViewBag.MSTD0001ACFM = BO.GetSystemMessage(Constants.MESSAGE.MSTD0001ACFM).VALUE;


            return View();
        }
        [HttpPost]
        public JsonResult SearchApproveByRequestType(WFD01150SearchModel con, PaginationModel page)
        {
            return SearchJson(BO.SearchApproveByRequestType(con.COMPANY, con.REQUEST_FLOW_TYPE, page));
        }

        [HttpPost]
        public JsonResult SearchPage()
        {
            return BaseJson(new BaseJsonResult()
            {
                //ObjectResult = RenderRazorViewToString("_Search", null)
                //ObjectResult = PartialView("_Search")
                ObjectResult = RenderViewToString(this.ControllerContext, "_Search", null)
            });
        }

        [HttpPost]
        public JsonResult EditPage()
        {
            return BaseJson(new BaseJsonResult()
            {
                //ObjectResult = RenderRazorViewToString("_EditPage", null)
                ObjectResult = RenderViewToString(this.ControllerContext, "_EditPage", null)
            });
        }

        [HttpPost]
        public JsonResult EditDetailPage(WFD01150EditScreenModel data)
        {
            return BaseJson(new BaseJsonResult()
            {
                //ObjectResult = RenderRazorViewToString("_EditDetail", data)
                //ObjectResult = PartialView("_EditDetail", data)
                ObjectResult = RenderViewToString(this.ControllerContext, "_EditDetail", data)
            });
        }

        [HttpPost]
        public JsonResult GetApproveDDatas(int? APPROVE_ID, string REQUEST_FLOW_TYPE)
        {
            return BaseJson(BO.GetApproveDDatas(APPROVE_ID, REQUEST_FLOW_TYPE));
        }

        [HttpPost]
        public JsonResult GetApproveEmailDatas(WFD01150EditScreenModel data)
        {
            data.USER = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.GetApproveEmailDatas(data));
        }

        [HttpPost]
        public JsonResult SubmitSaveApproveDDatas(TB_M_APPROVE_H hData, List<WFD01150ApproveDModel> datas)
        {
            hData.CREATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            hData.UPDATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.UpdateApprove(hData, datas));
        }

        [HttpPost]
        public JsonResult SubmitAddApproveData(string Company, List<WFD01150ApproveDModel> datas, string AssetCategory, string ApplyToAll, string requestType, string GUID)
        {
            return BaseJson(BO.AddApprove(Company, datas, AssetCategory, ApplyToAll, requestType, GlobalUser.UserSc2.TFASTEmployeeNo, GUID));
        }

        [HttpPost]
        public JsonResult ValidateSaveApproveDetail(TB_M_APPROVE_D data)
        {
            data.USER = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.ValidateSaveApproveDetail(data));
        }
        [HttpPost]
        public JsonResult SaveTempApproveEmailDetail(List<WFD01150ApproveEmailDModel> data)
        {
            return BaseJson(BO.SaveTempApproveEmailDetail(data, GlobalUser.UserSc2.TFASTEmployeeNo));
        }
        [HttpPost]
        public JsonResult DeleteTempApproveEmailDetail(WFD01150ApproveEmailDModel data)
        {
            return BaseJson(BO.DeleteTempApproveEmailDetail(data));
        }
        [HttpPost]
        public JsonResult ValidateAddApprove(string REQUEST_TYPE)
        {
            return BaseJson(BO.ValidateAddApprove(REQUEST_TYPE));
        }

        [HttpPost]
        public JsonResult DeleteApprove(WFD01150SearchApproveRowModel data)
        {
            data.UPDATE_BY = GlobalUser.UserSc2.TFASTEmployeeNo;
            return BaseJson(BO.DeleteApprove(data));
        }


    }



}

namespace th.co.toyota.stm.fas
{
    public static partial class DropdownListHelper
    {
        public static MvcHtmlString WFD01150_ApproveRejectTo(this HtmlHelper helper, string id, List<WFD01150RoleSeqModel> roles, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            ItemValue<string> item;

            if (roles != null)
            {
                foreach (var d in roles.OrderBy(m => m.SEQ))
                {
                    item = new ItemValue<string>();
                    item.Value = d.SEQ.ToString("N0");
                    item.Display = string.Format("{0}: {1}", d.SEQ,  d.ROLE_NAME);//d.ROLE, Suphachai 2019-10-28

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

    //    public static MvcHtmlString WFD01250_ApproveRejectTo(this HtmlHelper helper, string id, List<WFD01150RoleSeqModel> roles, object attribute = null
    //, bool includeIdx0 = true, string firstElement = null)
    //    {
    //        List<ItemValue<string>> ls = new List<ItemValue<string>>();

    //        ItemValue<string> item;

    //        if (roles != null)
    //        {
    //            foreach (var d in roles.OrderBy(m => m.SEQ))
    //            {
    //                item = new ItemValue<string>();
    //                item.Value = d.SEQ.ToString("N0");
    //                item.Display = string.Format("{0}: {1}", d.SEQ, d.ROLE);

    //                ls.Add(item);
    //            }
    //        }

    //        return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
    //    }
    }
}