﻿using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Controllers
{
    public class HomeController : BaseController
    {
        private Log4NetFunction log4net = new Log4NetFunction();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
           
            return View();
        }
        [HttpPost]
        public JsonResult TestLogin(string COMPANY, string USER_ID)
        {
            try
            {
                var _usr = (new DAO.WFD01210DAO()).GetEmployeeByEmpCodeDAO(
            new Models.WFD01210.WFD01210SearchModel { EMP_CODE = string.Format("{0}.{1}",COMPANY, USER_ID) }); //3747 0181 0339 4311 0427 3747 0641 0338 0181
                if(_usr == null)
                {
                    throw (new Exception("Invalid User ID"));
                }

                UserPrinciple usersc2 = new UserPrinciple();
                usersc2.User = new UserInfo()
                {
                    UserId = _usr.SYS_EMP_CODE,
                    FirstName = _usr.EMP_NAME,
                    LastName = string.Format("{0}({1})", _usr.EMP_LASTNAME, _usr.ROLE),
                    Company = _usr.COMPANY,
                    CompanyType = "supplier",
                    Department = "AMBG",
                    Division = "BC",
                    Email = "scorpionx327@gmail.com",
                    EmployeeNo = "0728",
                    Location = "SF",
                    Section = "",
                    TelephoneNumber = "9111111",
                    Title = "Mr.",
                    Region = "America",
                    Country = "US",
                    Language = "en_us",
                    UserAlias = "Surasith T."
                };
                usersc2.IsAECManager = _usr.AEC_MANAGER == "Y";
                usersc2.IsAECUser = _usr.FAADMIN == "Y"; // UserPrincipleBO.IsAECUser(usersc2.User.UserId);
               // usersc2.IsSystemAdmin = UserPrincipleBO.isSystemAdmin(usersc2.Acl.RoleList);
                usersc2.CompanyList = UserPrincipleBO.GetCompanyList(usersc2.User.UserId);
                usersc2.TFASTEmployeeNo = _usr.SYS_EMP_CODE;

                
                usersc2.Acl = new AccessControlList();
                usersc2.Acl.MapButtonAcl = new List<KeyValuePair<AccessControlButton, int>>();
                usersc2.Acl.MapFunctionId = new List<KeyValuePair<string, string>>();
                usersc2.Acl.MapGroupId = new List<KeyValuePair<string, string>>();
                usersc2.Acl.MapScreenAcl = new List<KeyValuePair<AccessControlScreen, string>>();
                usersc2.Acl.MapScreenId = new List<KeyValuePair<string, string>>();
                usersc2.Acl.MapSubGroupId = new List<KeyValuePair<string, string>>();


                HttpContext.Session[Constants.SESSION_USER_PROFILE] = usersc2;

                //GlobalUser.SetOfflineSession_offline("STM.0033");
                return BaseJson(new JsonResultBaseModel() { Status = Models.Common.MessageStatus.INFO, Message = "Success" });
            }
            catch(Exception ex)
            {
                log4net.WriteELogFile(ex.Message);
                return BaseJson(new JsonResultBaseModel() { Status = Models.Common.MessageStatus.ERROR, Message = ex.Message });
            }
            

        }


    }
}