﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Net.Client.SC2;
using Net.Client.SC2.Filter;
using Net.Client.SC2.Model;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using th.co.toyota.stm.fas.BusinessObject;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Controllers;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.Models.WFD02130;

namespace th.co.toyota.stm.fas
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {

            string deployProfile = ConfigurationManager.AppSettings["deployProfile"];

            string Sc2Mainserverurlbase = "http://sc2-dev.tmap-em.toyota-asia.com/main";
            string sc2homeserverurlbase = "http://sc2-dev.tmap-em.toyota-asia.com";
            string appsystemdn = "fd1.tmap-em.toyota.co.th.asia";
            bool usesc2authorization = false;
            bool usesc2offline = true;
          

            if (string.IsNullOrEmpty(deployProfile))
            {
                deployProfile = Constants.Deployment.MODE_OFFLINE;
            }

            //#CheckDeploy
            if (Constants.Deployment.MODE_PRODUCTION.Equals(deployProfile))
            {
                appsystemdn = "fd0.tmap-em.toyota.co.th.asia";
                usesc2authorization = true;
                usesc2offline = false;
            }
            else if (Constants.Deployment.MODE_OFFLINE.Equals(deployProfile) || Constants.Deployment.MODE_PU.Equals(deployProfile))
            {
                //Use default value
                appsystemdn = "fda.tmap-em.toyota.co.th.asia";
                usesc2authorization = false;
                usesc2offline = true;
            }
            else if (Constants.Deployment.MODE_IFT.Equals(deployProfile))
            {
                appsystemdn = "fda.tmap-em.toyota.co.th.asia";
                usesc2authorization = true;
                usesc2offline = false;
            }
            else if (Constants.Deployment.MODE_BCT.Equals(deployProfile))
            {
                appsystemdn = "fdb.tmap-em.toyota.co.th.asia";
                usesc2authorization = true;
                usesc2offline = false;
            }
            else if (Constants.Deployment.MODE_BCTIS.Equals(deployProfile))
            {
                appsystemdn = "fdc.tmap-em.toyota.co.th.asia";
                usesc2authorization = true;
                usesc2offline = false;
            }
            else if (Constants.Deployment.MODE_SIT.Equals(deployProfile))
            {
                appsystemdn = "fdd.tmap-em.toyota.co.th.asia";
                usesc2authorization = true;
                usesc2offline = false;
            }
            else if (Constants.Deployment.MODE_UT.Equals(deployProfile))
            {
                appsystemdn = "fde.tmap-em.toyota.co.th.asia";
                usesc2authorization = true;
                usesc2offline = false;
            }
            Sc2Options sc2VerificationOptions = new Sc2Options()
            {

                Sc2MainServerUrlBase = Sc2Mainserverurlbase,
                Sc2HomeServerUrlBase = sc2homeserverurlbase,
                AppSystemDn = appsystemdn,
                UseSc2Authorization = usesc2authorization,
                UseSc2Offline = usesc2offline,

                offlineUser = new UserInfo()
                {
                    UserId = "1423",
                    FirstName = "Somchai",
                    LastName = "First Class",
                    Company = "pennypop",
                    CompanyType = "supplier",
                    Department = "Gaming Services",
                    Division = "BC",
                    Email = "yulan@support.pennypop.com",
                    EmployeeNo = "0728",
                    Location = "SF",
                    Section = "",
                    TelephoneNumber = "9111111",
                    Title = "Mr.",
                    Region = "America",
                    Country = "US",
                    Language = "en_us",
                    UserAlias = "uten S."
                },

                offlineAcl = new AccessControlList()
                {
                    SystemDn = "fd1.tmap-em.toyota.co.th.asia",
                    SystemUrl = "http://localhost",
                    MapScreenAcl = new List<KeyValuePair<AccessControlScreen, string>>() {
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01110",
                                                ScreenId = "WFD01110",
                                                Group = "WFD011",
                                                SubGroup = "FD011",
                                                ScreenResource = ""

                                            }, "screen.aspx"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01210",
                                                ScreenId = "WFD01210",
                                                Group = "WFD0121",
                                                SubGroup = "FD012",
                                                ScreenResource = "WFD01210"

                                            }, "WFD01210"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01120",
                                                ScreenId = "WFD01120",
                                                Group = "WFD0112",
                                                SubGroup = "FD011",
                                                ScreenResource = "WFD01120"

                                            }, "WFD01120"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01130",
                                                ScreenId = "WFD01130",
                                                Group = "WFD0113",
                                                SubGroup = "FD011",
                                                ScreenResource = "WFD01130"

                                            }, "WFD01130"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01010",
                                                ScreenId = "WFD01010",
                                                Group = "WFD0101",
                                                SubGroup = "FD010",
                                                ScreenResource = "WFD01010"

                                            }, "WFD01010"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01010",
                                                ScreenId = "WFD01010",
                                                Group = "WFD0101",
                                                SubGroup = "FD010",
                                                ScreenResource = "WFD01010"

                                            }, "WFD01010"),
                                    },
                    MapButtonAcl = new List<KeyValuePair<AccessControlButton, int>>()
                                    {
                                        new KeyValuePair<AccessControlButton, int>(
                                            new AccessControlButton()
                                            {
                                                FunctionId = "WFD02120",
                                                ScreenId = "WFD02120",
                                                ButtonId = "WFD02120Clear",
                                                AccessRight = 3
                                            }, 1),
                                        new KeyValuePair<AccessControlButton, int>(
                                            new  AccessControlButton()
                                            {
                                                FunctionId = "WFD02120",
                                                ScreenId = "WFD02120",
                                                ButtonId = "WFD02120Search",
                                                AccessRight = 1
                                            }, 1)

                                    },
                    MapFunctionId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("XXX123", "XXX123")
                                    },

                    MapScreenId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("WFD01110", "WFD01110"),
                                        new KeyValuePair<string, string>("WFD01210", "WFD01210"),
                                        new KeyValuePair<string, string>("WFD01120", "WFD01120"),
                                        new KeyValuePair<string, string>("WFD01130", "WFD01130"),
                                        new KeyValuePair<string, string>("WFD01010", "WFD01010"),
                                        new KeyValuePair<string, string>("WFD02120", "WFD02120")
                                    },
                    MapGroupId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("XXX123Group", "XXX123Group")
                                    },
                    MapSubGroupId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("XXX123SubGroup", "XXX123SubGroup")
                                    },
                    RoleList = new List<string>()
                                    {
                                        "Admin.tmap-em.toyota.co.th.asia",
                                        "Operator.tmap-em.toyota.co.th.asia"
                                    }
                }
            };


            AreaRegistration.RegisterAllAreas();

            //Modified to pass SC2options
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters, sc2VerificationOptions);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //  JLog.settingLogVariables("|", @"D:\SFASLog\", "STMFixedAssetLog-");

        }

        protected void Session_Start(Object sender, EventArgs e)
        {

            // GlobalUser.SetSC2Session();

        }
        protected void Session_End(Object sender, EventArgs e)
        {
            ClearSession();
        }
        protected void ClearSession()
        {
            if (Session[Constants.SESSION_USER_PROFILE] != null)
            {
                Session.Remove(Constants.SESSION_USER_PROFILE);
            }

            if (Session[SC2Defaults.SessionSystemAcl] != null)
            {
                Session.Remove(SC2Defaults.SessionSystemAcl);
            }

            if (Session[SC2Defaults.SessionUserInfo] != null)
            {
                Session.Remove(SC2Defaults.SessionUserInfo);
            }
        }

        protected void Application_End()
        {
            ClearSession();

        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var app = (MvcApplication)sender;
            var context = app.Context;
            var ex = app.Server.GetLastError();
            var urlError = Request.RawUrl;
            context.Response.Clear();
            context.ClearError();
            var httpException = ex as HttpException;

            var routeData = new RouteData();
            routeData.Values["controller"] = "errors";
            routeData.Values["exception"] = urlError + ":" + ex;
            routeData.Values["action"] = "http500";

            //JLog.write(LOG_TYPE.ERROR, LOG_POSITION.COMMON, ex, "", JLog.GetCurrentMethod());

            if (httpException != null)
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        routeData.Values["action"] = "http404";
                        break;
                    case 403:
                        routeData.Values["action"] = "http403";
                        break;
                    case 500:
                        routeData.Values["action"] = "http500";
                        break;
                    default:
                        routeData.Values["action"] = "ErrorPage";
                        break;
                }
            }
            IController controller = new ErrorsController();
            controller.Execute(new RequestContext(new HttpContextWrapper(context), routeData));
        }
    }
}