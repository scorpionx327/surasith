﻿$(function () {
    //Initialize
    $('#divSearchRequestResult').hide();
});
var control = {
    DataTable: null,
    Company: $('#WFD01170ddpCompany'), // Temp , still request best solution.
    ColumnIndex: {
        Sel: 0
    }
}
var fAction = {
    SelectedCostCenter: "",
    SelectedRespCostCenter: "",
    //Should define all request screen.
    Initialize: function (callback) {
        if (callback != null)
            callback();
    },
    LoadData: function (callback) {
        console.log("LoadData is callled");
        //Do here
        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        console.log(GLOBAL);
        console.log(Url.GetAsset);
        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
            console.log(datas);
            fAction.SelectedCostCenter = ""; //Clear Selected
            fAction.SelectedRespCostCenter = "";

            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                table.clear().draw().destroy();
            }

            //Set Screen Mode
            if (GLOBAL.DOC_NO == '' && datas.length == 0) {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            }
            if (GLOBAL.DOC_NO == '' && datas.length > 0) {
                fScreenMode.Submit_AddAssetsMode(function () {
                    fMainScreenMode.Submit_AddAssetsMode();
                });
            }

            if (datas == null || datas.length == 0) {
                $('#divSearchResult').hide();

                if (callback != null)
                    callback();

                return;
            }
            fAction.SelectedCostCenter = datas[0].COST_CODE;
            fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;
            $('#divSearchResult').show();

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            control.DataTable = $('#tbSearchResult').DataTable({
                data: datas,
                "columns": [
                    { "data": null, 'orderable': false },               // 0 
                    { "data": "LINE_NO", 'orderable': false },          // 1
                    { "data": null, 'orderable': false },               // 2
                    { "data": "ASSET_SUB", 'orderable': false },        // 3    
                    {
                        "data": "ASSET_DESC", 'orderable': false, 'class': 'text-center',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    },       // 4
                    { "data": "ADTL_ASSET_DESC", 'orderable': false },               // 5
                    { "data": "SERIAL_NO", 'orderable': false },               // 6
                    { "data": "INVEN_INDICATOR", 'orderable': false },               // 7
                    { "data": "INVEN_NOTE", 'orderable': false },               // 8               
                    //{ "data": "INVEST_REASON", 'orderable': false },               // 9
                    { "data": null, 'orderable': false },               // 9
                    { "data": null, 'orderable': false },               // 10 Minor Cate (eva1)
                    //{ "data": null, 'orderable': false },               // 11 Asset Status (eva2)
                    { "data": null, 'orderable': false },               // 12 Asset Type (eva3)
                    { "data": null, 'orderable': false },         // 13 (eva4)
                    { "data": "STATUS", 'orderable': false },        // 14
                    { "data": null, 'orderable': false },                // 15
                    { "data": null, 'orderable': false }                // 16
                ],
                'columnDefs': [
                    {
                        //'targets': [ 1, 2, 14],
                        //className: 'text-center'
                    },
                    {
                        'targets': [1],
                        className: 'text-right'
                    },
                    {
                        'targets': 0, //Check Box
                        'orderable': true,
                        "visible": (PERMISSION.AllowResend == "Y"),
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return fRender.CheckBoxRender(data);
                        }
                    },
                    {
                        'targets': 2, //Asset No
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return fRender.HyperLinkRender(data);
                        }
                    },
                    {
                        'targets': 9, //Invert Reason
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.INVEST_REASON == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.INVEST_REASON_HINT + '">' + data.INVEST_REASON + '</span>';
                        }
                    },
                    {
                        'targets': 10, //10 Minor Cate
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            if (data.MINOR_CATEGORY == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.MINOR_CATEGORY_HINT + '">' + data.MINOR_CATEGORY + '</span>';
                        }
                    },

                    //{
                    //    'targets': 11, //Asset Status
                    //    'orderable': true,
                    //    'className': 'text-center',
                    //    "render": function (data, type, full, meta) {
                    //        if (data.ASSET_STATUS == null)
                    //            return '';

                    //        return '<span data-toggle="tooltip" title="' + data.ASSET_STATUS_HINT + '">' + data.ASSET_STATUS + '</span>';
                    //    }
                    //},
                    {
                        'targets': 11, //Asset Type
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {

                            if (data.MACHINE_LICENSE == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.MACHINE_LICENSE_HINT + '">' + data.MACHINE_LICENSE + '</span>';
                        }
                    },
                    {
                        'targets': 12, //Asset Type
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {

                            if (data.BOI_NO == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.BOI_NO_HINT + '">' + data.BOI_NO + '</span>';
                        }
                    },
                    {
                        'targets': 14,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DeleteButtonRender(data);
                        }
                    },
                    {
                        'targets': 15,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DetailButtonRender(data);
                        }
                    }

                ],
                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false
            }).columns.adjust().draw();

            //control.DataTable.column(0).visible(false);  // CheckBox
            //control.DataTable.column(8).visible(false);  // Account Principle
            //control.DataTable.column(10).visible(false); // Status

            if (callback == null)
                return;

            callback();

        }, null);


    },
    ShowSearchPopup: function (callback) {
        //Nothing
    },
    Clear: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }

            fAction.LoadData(function () {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            });

            if (callback == null)
                return;

            callback();
        });
    },
    Export: function (callback) {
        var batch = BatchProcess({
            BatchId: "LFD02812",
            Description: "Export Change Info Request",
            UserId: GLOBAL.EMP_CODE

        });
        //batch.Addparam(1, 'GUID', GLOBAL.GUID);
        //batch.Addparam(2, 'DOC_NO', GLOBAL.DOC_NO);
        //batch.StartBatch();
        batch.Addparam(1, 'GUID', GLOBAL.GUID);
        batch.Addparam(2, 'IsAECUser', fMainAction.getIsAECRole());
        batch.StartBatch();
        if (callback == null)
            return;

        callback();
    },
    AddAsset: function (_data, callback) { //It's call from dialog

        fAction.LoadData();

        if (callback == null)
            return;

        callback();


    },
    DeleteAsset: function (_assetno, _assetsub) {
        //console.log(_AssetNo);
        //console.log(Url.DeleteAsset);
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: control.Company.val(),
            ASSET_NO: _assetno,
            ASSET_SUB: _assetsub
        }
        console.log(_data);
        ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
            if (result.IsError) {
                return;
            }
            fAction.LoadData();
            // SuccessNotification(CommonMessage.ClearSuccess);

        });
    },
    DownloadTemplate: function (callback) {
        //Under implement
    },
    UploadExcel: function (_fileName) {
        //call Batch with filename
        var batch = BatchProcess({
            BatchId: "BFD02811",
            Description: "Upload Asset Info Change Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function (_appID, _Status) {

            if (_Status != 'S') {
                return;
            }
            fAction.LoadData();
        }, true);
    },
    PrepareGenerateFlow: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.PrepareFlow, GLOBAL, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();
        });
    },

    PrepareSubmit: function (callback) {

        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();

    },
    PrepareApprove: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },

    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");
        if (control.DataTable == null) {
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List

        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }

            var _bSel = $(this).find("input[name='chkGEN']").is(":checked");

            if (_requireCheck && !_bSel) // Required check but no selected 
                return;

            _row = control.DataTable.row(this).data(); // Get source data
            _assetList.push({
                GUID: GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                COMPANY: GLOBAL.COMPANY,
                ASSET_NO: _row.ASSET_NO,
                ASSET_SUB: _row.ASSET_SUB,
                LINE_NO: _row.LINE_NO
            });
        });

        return _assetList;
    },
    EnableMassMode: function () {

    },
    UpdateRow: function (_data) {
        console.log("UpdateRow");

        var _bFoundFlag = false;
        //_Company, _AssetNo, _AssetSub, _AmountPosted, _Percent
        $("#tbSearchResult tr").each(function () {

            if (_bFoundFlag)
                return;

            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data
            if (_row.COMPANY != control.CostPopup.COMPANY ||
                _row.ASSET_NO != control.CostPopup.ASSET_NO ||
                _row.ASSET_SUB != control.CostPopup.ASSET_SUB) {
                return;
            }

            if ($('#chkAll').is(':checked')) {
                _row.PERCENTAGE = null;
                _row.AMOUNT_POSTED = $('#txtCostAll').val();
            }
            if ($('#chkAmount').is(':checked')) {
                _row.PERCENTAGE = null;
                _row.AMOUNT_POSTED = $('#txtCostAmount').val();
            }
            if ($('#chkPercentage').is(':checked')) {
                _row.PERCENTAGE = $('#txtCostPercentage').val();
                _row.AMOUNT_POSTED = $('#txtCostPercentageAmount').val();
            }
            console.log("Update Row is success")
            _bFoundFlag = true;
        });

    },
    Open2115Popup: function (button) {
        var _popupWidth = $(window).width() - 20;
        var left = ($(window).width() / 2) - (_popupWidth / 2);
        top = ($(window).height() / 2) - (600 / 2);

        var _selectedList = fAction.GetClientAssetList();
        var row = {};

        var _selectedRCC = "", _mode = "";
        if (_selectedList != null && _selectedList.length > 0) {
            _selectedRCC = _selectedList[0].RESP_COST_CODE;
        }
        else {

        }
        //Edit
        if (button != null) {
            row = control.DataTable.row($(button).parents('tr')).data();
        }
        else {
            //Add New
            row.LINE_NO = 0;
            row.ASSET_NO = "";
            row.ASSET_SUB = "";
        }
        var flowType = (GLOBAL.FLOW_TYPE == "GA") ? "GN" : GLOBAL.FLOW_TYPE;
        var viewMode = (PERMISSION.AllowEditBeforeGenFlow == false) ? "Y" : "N";

        popup = window.open(MainUrl.NewAssetsPopup +
            "?GUID=" + GLOBAL.GUID +
            "&COMPANY=" + control.Company.val() +
            "&DOC_NO=" + GLOBAL.DOC_NO +
            "&LINE_NO=" + row.LINE_NO +
            "&RESP_COST_CODE=" + _selectedRCC +
            "&FLOW_TYPE=" + flowType +
            "&REQUEST_TYPE=" + GLOBAL.REQUEST_TYPE +
            "&ROLEMODE=" + $('#WFD01170ddpRequestorRole').val() +
            "&ASSET_NO=" + row.ASSET_NO +
            "&ASSET_SUB=" + row.ASSET_SUB +
            "&VIEW=" + viewMode,
            "popup", "width=" + _popupWidth + ", height=600, top=" + top + ", left=" + left + ", scrollbars=yes");

    },
    Resend: function (callback) {
        var _header = fMainAction.GetParameter();

        var _list = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, { header: _header, list: _list }, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList -> Send


    },
}

//Call this function when PG set screen mode
var fScreenMode = {


    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        $('#WFD02810_SelectAll').prop("disabled", true);
        $('#WFD02810_SelectAll').hide();
        $('#DivTMCDocument').hide();

    },

    Submit_InitialMode: function (callback) {

        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset').prop("disabled", false);



        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').removeClass('disabled').prop("disabled", false);


        $('button.delete').prop("disabled", false);
        $('button.delete').show();

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').removeClass('disabled').prop("disabled", true);

        $('button.delete').prop("disabled", true);
        $('button.delete').show();


        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) {

        console.log("#_ClearMode");
        fScreenMode._ClearMode();

        $('#btnRequestExport').show();
        $('#btnRequestExport').prop("disabled", false);


        if (callback == null)
            return;
        callback();
        return;
    }
}

var fRender = {
    CheckBoxRender: function (data) {

        if (!data.AllowCheck)
            return '';

        _strHtml = '<input style="width:20px; height:28px;" name="chkGEN" class="chkGEN" value="' + data.LINE_NO + '" onclick="fAction.DetailCheck(this);" type="checkbox" >';
        return _strHtml;
    },
    HyperLinkRender: function (data) {
        return '<a href="../WFD02130?COMPANY=' + data.COMPANY + '&ASSET_NO=' + data.ASSET_NO + '&ASSET_SUB=' + data.ASSET_SUB + '" target="_blank">' + data.ASSET_NO + '</a>';
    },
    DeleteButtonRender: function (data) {

        if (!data.AllowDelete)
            return '';

        var _strHtml = '';
        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB + "_" + data.LINE_NO;
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="btnDelete_"' + data.LINE_NO + ' class="btn btn-danger btn-xs delete" data-title="Delete" type="button" ' +
            'onclick=fAction.DeleteAsset("' +
            data.ASSET_NO + '","' + data.ASSET_SUB + '")>' +
            '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },
    DetailButtonRender: function (data) {
        var _strHtml = '';

        _strHtml += '<p title="" data-original-title="Detail" data-toggle="tooltip" data-placement="top"> <button id="btn_Detail"' + data.LINE_NO + ' class="btn btn-success btn-xs" data-title="View Detail" type="button" ' +
            'onclick=fAction.Open2115Popup(this)>' +
            '<span class="glyphicon glyphicon-edit"></span></button></p>';
        _strHtml += '</div>';
        return _strHtml;
    },
}
// Control Event
$('#btnRequestAddAsset').click(function (e) {

    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }
    fAction.Open2115Popup(null);
});