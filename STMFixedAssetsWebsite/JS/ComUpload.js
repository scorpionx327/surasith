﻿var controlUpload = {
    GUID: "",
    DOC_NO: "",
    ASSET_NO: "",
    ASSET_SUB: "",
    COMPANY: "",
    TYPE: "",
    FUNCTION_ID: "",
    DOC_UPLOAD: "",
    FIRST_FLAG: "N",
    LINE_NO: "",
    DEL_FLAG: "Y",
    FILE_LIMIT: 5,
    SAVE_CALLBACK : null
}

var TempFileInsert = [];

var fUpload = {
    SaveFileToTempServer: function () {

        

        var model = new FormData();
        var fileInput = document.getElementById('btnAddMultipleFile');

        var _existsCnt = $('#DivFileList div.attach').length;

        if (_existsCnt + fileInput.files.length > controlUpload.FILE_LIMIT) {
            AlertTextErrorMessagePopup("Not allow upload greater than {0} files".format(controlUpload.FILE_LIMIT), "#AlertMessageCommonUpload");
            return;
        }


        model.append('DOC_NO', controlUpload.DOC_NO);
        model.append('GUID', controlUpload.GUID);
        model.append('ASSET_NO', controlUpload.ASSET_NO);
        model.append('ASSET_SUB', controlUpload.ASSET_SUB);
        model.append('COMPANY', controlUpload.COMPANY);
        model.append('TYPE', controlUpload.TYPE);
        model.append('FUNCTION_ID', controlUpload.FUNCTION_ID);
        model.append('LINE_NO', controlUpload.LINE_NO);
        if (fileInput.files.length > 0) {
            for (var x = 0; x < fileInput.files.length; x++) {
                model.append("files[]", document.getElementById('btnAddMultipleFile').files[x]);
            }
            //model.append('TEMP_FILE_DOC', fileInput.files[0]);            
            ajax_method.PostMultiFile(URLComUpload.CheckUploadFile, model, function (result) {
                if (result.ObjectResult) {// true 
                    //debugger;
                    ajax_method.PostMultiFile(URLComUpload.UploadAssetFile, model, function (result) {
                        //debugger;
                        fUpload.RenderFileUpload(result.ObjectResult.data, controlUpload.DEL_FLAG);
                        //debugger;
                        if (TempFileInsert != null && TempFileInsert.length > 0) {
                            TempFileInsert.push.apply(TempFileInsert, result.ObjectResult.data);
                            //TempFileInsert.push(result.ObjectResult.data);
                        } else {
                            TempFileInsert = result.ObjectResult.data;
                        }
                    }, null);
                }
            }, null);
        }
    },

    GetFileFromDB: function () {
        //debugger;
        var _DivFileList = document.getElementById("DivFileList");
        _DivFileList.innerHTML = "";

        if (controlUpload.DEL_FLAG == 'Y') {
            $('#CommonUploadBtn').show();
            $('#btnComUploadSave').show();
        } else {
            $('#CommonUploadBtn').hide();
            $('#btnComUploadSave').hide();
        }

        ajax_method.Post(URLComUpload.GetFileAttchDoc, controlUpload, false, function (result) {
            //debugger;
            if (result.data != null)
            {
                fUpload.RenderFileUpload(result.data, controlUpload.DEL_FLAG);
            }            
        }, null);
    },

    GetZipFileFromDB: function () {
        //debugger;
        ajax_method.Post(URLComUpload.GetZipFileAttchDoc, controlUpload, false, function (result) {
            //debugger;
            if (result.data != null && result.data != '') {
                window.location.href = '/ComUpload/DownloadZip?_filePath=' + result.data;
            }
        }, null);
    },

    RenderFileUpload: function (result, _flagDel) {
        //console.log(result.ObjectResult.data);
        var data = result;
        var _DivFileList = document.getElementById("DivFileList");
        //_DivFileList.innerHTML = "";
        //var _row = document.createElement('div');
        //_row.className = 'row';

        var _divThumbnail = document.createElement('div');
        _divThumbnail.className = 'thumbnail';

        var _divFile = document.createElement('div');
        _divFile.className = 'col-md-2';

        var _divImgHtml = '';
        var _del = '';

        if (data != null) {
            for (i = 0; i < data.length; i++) {
                //if (i % 6 == 0) {
                //    _DivFileList.appendChild(_row);
                //    _row = document.createElement('div');
                //    _row.className = 'row';
                //}

                _divFile = document.createElement('div');
                _divFile.className = 'col-md-2 attach';

                _divThumbnail = document.createElement('div');
                _divThumbnail.className = 'thumbnail';
                _divFile.id = data[i].FILE_ID;

                _divImgHtml = '<a href="' + data[i].FILE_PATH_DOWNLOAD + '" target="_blank">' +
                    '<img title="' + data[i].FILE_NAME_ORIGINAL + '" src="' + data[i].PATH_FILE_TYPE + '" alt="Lights" style="width:100%"/>' +
                    '</a>';                   
                if (_flagDel == 'Y') {                            
                    _del = 
                        '<div class="caption" style="text-align:right">' +
                        '<i class="fa fa-remove icon-del" data-title="Delete File" onclick="fUpload.DeleteFileTemp(\'' + data[i].FILE_ID + '\',\'' + data[i].FILE_NAME + '\',\'' + data[i].FILE_TYPE + '\');" ></i></button>' +
                        '</div>';
                } else {
                    _del = '';
                }

                _divImgHtml = _divImgHtml + _del;

                _divThumbnail.innerHTML = _divImgHtml;

                _divFile.appendChild(_divThumbnail);

                _DivFileList.appendChild(_divFile);
            }
            $('#btnAddMultipleFile').val('');
            // _DivFileList.appendChild(_row);

            $('#CommonDownloadBtn').prop("disabled", data.length <= 0);
            return;
        }
        $('#CommonDownloadBtn').prop("disabled", true);
    },

    DeleteFileTemp: function (_FileId,_FileName,_FileType) {       
        var model = new FormData();
        model.append('DOC_NO', controlUpload.DOC_NO);
        model.append('GUID', controlUpload.GUID);
        model.append('ASSET_NO', controlUpload.ASSET_NO);
        model.append('ASSET_SUB', controlUpload.ASSET_SUB);
        model.append('COMPANY', controlUpload.COMPANY);
        model.append('TYPE', controlUpload.TYPE);
        model.append('FUNCTION_ID', controlUpload.FUNCTION_ID);
        model.append('DOC_UPLOAD', controlUpload.FUNCTION_ID + '_' + controlUpload.TYPE);
        model.append('TEMP_FILE_DOC_NAME', _FileId);
        model.append('FILE_NAME', _FileName);
        model.append('FILE_TYPE', _FileType);
        model.append('LINE_NO', controlUpload.LINE_NO);
        ajax_method.PostFile(URLComUpload.DeleteFileTemp, model, function (result) {
            if (result.ObjectResult) {// true 
                var id = '#' + _FileId;
                //$(id).removeClass('col-md-2');
                $(id).remove();
                //debugger;
                for (var i = 0; i < TempFileInsert.length; i++) {
                    if (TempFileInsert[i].FILE_ID == _FileId) {
                        TempFileInsert.splice(i, 1);
                    }
                }

                $('#CommonDownloadBtn').prop("disabled", $('#DivFileList div.attach').length <= 0);
            }
        }, null);
    },

    InsertFile: function (arrFileInsert) {
        //alert(1111);
        ajax_method.Post(URLComUpload.InsertFile, arrFileInsert, false, function (result) {
            //debugger;
            console.log(result);
        }, null);        
    },
    InsertFileCallBack: function (arrFileInsert,callback) {
        //alert(1111);
        ajax_method.Post(URLComUpload.InsertFile, arrFileInsert, false, function (result) {
            //debugger;
            console.log(result);
            if (callback != null) {
                callback();
            }

        }, null);
    },
}

$('#CommonUploadBtn').click(function (e) {
    ClearMessageC();
    
    if (controlUpload.FILE_LIMIT == null)
        controlUpload.FILE_LIMIT = 5;

    if ($('#DivFileList div.attach').length >= controlUpload.FILE_LIMIT) {
        AlertTextErrorMessagePopup("Not allow upload greater than {0} files".format(controlUpload.FILE_LIMIT), "#AlertMessageCommonUpload");
        return;
    }


    $('#btnAddMultipleFile').click();
})

$('#btnAddMultipleFile').change(function (e) {
    fUpload.SaveFileToTempServer();
});

$('#CommonUpload').on('show.bs.modal', function () {
    controlUpload.FIRST_FLAG = 'Y';
    fUpload.GetFileFromDB();
    TempFileInsert = [];
})

$('#CommonUpload').on('hide.bs.modal', function () {
    controlUpload.SAVE_CALLBACK = null;
})

$('#btnComUploadSave').click(function (e) {
    fUpload.InsertFileCallBack(TempFileInsert, controlUpload.SAVE_CALLBACK);
    $('#CommonUpload').modal('hide');
})

$('#CommonDownloadBtn').click(function (e) {
    fUpload.GetZipFileFromDB();
})