﻿
var WFD01150_Body = $('#WFD01150_Body');
var WFD01150_MODE = {
    ADD_MODE: 'WFD01150_AddMode',
    Edit_MODE: 'WFD01150_EditMode'
};

var WFD01150_SearchControls = {
    Box: {
        SearchBox: $('#WFD01150_SearchBox')
    },
    Company: {
        Code: '',
        Name: ''
    },
    Operation: {
        Code: '',
        Name: ''
    },
    Datas: [],
    Button: {
        Add: $('#WFD01150Add'),
        Search: $('#WFD01150Search'),
        Clear: $('#WFD01150Clear')
    },
    Table: {

        SearchTable: $('#WFD01150_SearchTable'),
        SearchTable_ApproveFlowHeadCol: $('#WFD01150_ApproveFlowHeadCol'),
        SearchTable_FlowApproverRow: $('#WFD01150_FlowApproverRow'),
        SearchTablePagin: $('#WFD01150_SearchTable').Pagin(WFD01150_Config.ITEM_PER_PAGE),
        SearchTable_Body: $('#WFD01150_SearchTable tbody'),

        SearchResultRow: $('#WFD01150_SearchResultRow'),
        SearchResultPaginArea: $('#WFD01150_SearchTable-pagin-area'),

        Width: {
            col1: 50,
            col2: 50,
            col3: 150,
            flowCol: 80
        }
    },
    Input: {
        Operation: $('#WFD01150_Operation'),
        Company: $('#WFD01150_Company')
    },
    RequiredInput1: ['#WFD01150_Operation'],
    RequiredInput2: ['#WFD01150_Company'],
};

var WFD01150_EditControls = {
    Mode: '',
    ProcessType: '',
    Data: {
        GUID: '',
        ApproveRole: null,
        ApproveH: null,
        ApproveDDatas: [],
        ApproveFlows: [],
        PageCondition: {
            ApproveId: 0,
            AssetClass: {
                Code: '',
                Name: ''
            },
            RequestType: {
                Code: '',
                Name: ''
            },
            RequestFlowType: {
                Code: '',
                Name: ''
            }
        },
        EditDetailItem: null,
        ApproveDBlankData: {
            ALLOW_DEL_ITEM: 'N',
            ALLOW_REJECT: 'N',
            ALLOW_SEL_APPRV: 'N',
            APPRV_GROUP: null,
            APPRV_ID: null,
            CONDITION_CODE: '',
            DIVISION: '',
            DIVISION_NAME: '',
            FINISH_FLOW: 'N',
            HIGHER_APPR: 'N',
            INDX: null,
            OPERATOR: '',
            REJECT_TO: null,
            REJECT_TO_NAME: null,
            REMARK: null,
            REQUIRE_FLAG: '',
            ROLE: '',
            ROLE_NAME: '',
            VALUE1: '',
            VALUE2: null,
            GENERATE_FILE: '',
            LEAD_TIME: '',
            NOTI_BY_EMAIL: '',
            OPERATION: '',
            GUID: ''
        },
        EditDetaiEmaillItem: null
    },
    Box: {
        EditDetailBox_ID: '#WFD01150_EditDetailBox'
    },
    ListControl: {
        SubmitEdit_ID: '#WFD01150SubmitEdit',
        CancelEdit_ID: '#WFD01150CancelEdit',
        AssetCategoryLabel_ID: '#WFD01150_AssetCategory',
        OperationLabel_ID: '#WFD01150_OperationEditPage',
        ApproveDTable_ID: {
            Table: '#WFD01150_ApproveDTable',
            Body: '#WFD01150_ApproveDTable tbody'
        },
        ApproveEmailDTable_ID: {
            Table: '#WFD01150_ApproveEmailDTable',
            Thead: '#WFD01150_ApproveEmailDTable thead',
            Body: '#WFD01150_ApproveEmailDTable tbody'
        }
    },
    EditControl: {
        Input_ID: {
            INDX: '#WFD01150_INDX',
            ROLE: '#WFD01150_ROLE',
            DIVISION: '#WFD01150_DIVISION',
            CONDITION_CODE: '#WFD01150_CONDITION_CODE',
            OPERATOR: '#WFD01150_OPERATOR',
            VALUE1: '#WFD01150_VALUE1',
            ALLOW_REJECT: '#WFD01150_ALLOW_REJECT',
            REJECT_TO: '#WFD01150_REJECT_TO',
            ALLOW_DEL_ITEM: '#WFD01150_ALLOW_DEL_ITEM',
            ALLOW_SEL_APPRV: '#WFD01150_ALLOW_SEL_APPRV',
            HIGHER_APPR: '#WFD01150_HIGHER_APPR',
            FINISH_FLOW: '#WFD01150_FINISH_FLOW',
            REQUIRE_FLAG: '#WFD01150_REQUIRE_FLAG',
            APPRV_GROUP: '#WFD01150_APPRV_GROUP',
            VALUE2: '#WFD01150_VALUE2',
            AssetCategoryCBB: '#WFD01150_AssetCategoryCBB',
            ApplyToAllAsset: '#WFD01150_ApplyToAllAsset',
            NOTIFICATION_PERSON: '#WFD01150_NOTIFICATION_PERSON',
            NOTIFICATION_GROUP: '#WFD01150_NOTIFICATION_GROUP',
            OPERATION_PERSON: '#WFD01150_OPERATION_PERSON',
            OPERATION_GROUP: '#WFD01150_OPERATION_GROUP',
            GENERATE_FILE: '#WFD01150_GENERATE_FILE',
            KPI_LEADTIME: '#WFD01150_KPI_LEADTIME'
        },
        Button: {
            SaveItem: '#WFD01150SaveItem',
            CancelItem: '#WFD01150CancelItem',
        },

        Required: ['#WFD01150_ROLE', '#WFD01150_DIVISION']
    }
};

var defaultFilter = {
    company: ""
};

// Ready functionc
$(function () {
    //Get current company Selected
    defaultFilter.company = WFD01150_SearchControls.Input.Company.val();

    pagin = $('#tbSearchResult').Pagin(WFD01150_Config.ITEM_PER_PAGE).GetPaginData();
    //$('#WFD01150Search').width(100);
    //$('#WFD01150Clear').width(100);
    //$('#WFD01150Add').width(100);

    $('#WFD01150SubmitAdd').width(100);
    $('#WFD01150CancelAdd').width(100);
    //$('#WFD01150SaveAdd').width(100);
    //$('#WFD01150CancelAdd').width(100);

    WFD01150_SearchControls.Button.Search
        .width(100)
        .click(WFD01150_OnSearch);
    WFD01150_SearchControls.Button.Clear
        .width(100)
        .click(WFD01150_OnClear);
    WFD01150_SearchControls.Button.Add
        .width(100)
        .click(WFD01150_OnAdd);

    WFD01150_SearchControls.Table.SearchResultRow.hide();

    WFD01150_setControlsRequiredField(WFD01150_SearchControls.RequiredInput1, WFD01150_SearchControls.RequiredInput2, true);

    WFD01150_OnClear();
});


//$(document).ready(function () {
    //$('#WFD01150_Button_Add').hide();
    //$('#WFD01150_Button_Load').show();
    //$('#myFormlAddInfo').hide();

    //$('#WFD01150Search').click(function (event) {
    //    $('#WFD01150_Button_Add').hide();
    //    $('#WFD01150_Button_Load').show();
    //    $('#myFormlAddInfo').hide();
    //    $('#WFD01150_SearchResult').show();
    //    //fAction.LoadData();
    //});

    //$('#WFD01150Add').click(function () {
    //    $('#WFD01150_Button_Add').show();
    //    $('#WFD01150_Button_Load').hide();
    //    $('#WFD01150_SearchResult').hide();
    //    $('#myFormlAddInfo').show();
    //    //LoadDataListApprove();
    //});

    //$('#WFD01150Clear').click(function () {
    //    location.reload();
    //});

    //$('#WFD01150CancelAdd').click(function () {
    //    loadConfirmAlert("Are you sure you want to Cancel Add Operation?", function (result) {
    //        if (!result) {
    //            return;
    //        }
    //        location.reload();
    //    })
    //});
    //$('#WFD01150_ROLE').trigger('change.select2');
    //$('#WFD01150_ROLE').on("change", function (e) { alert(11); });
    //$('#WFD01150_ROLE').select2().on('change', function () {
    //    DefauleNotificationAndOperation();
    //});
    //$('#WFD01150_ROLE').trigger("change");
//});

//#region   Search page

//var initSearchPage = function () {

//    ajax_method.Post(WFD01150_Url.SearchPage, null, true, function (res) {

//        WFD01150_Body.html(res);
//        $(".select2").select2({ width: '100%' });
//        WFD01150_SearchControls.Input.Operation.select2("val", WFD01150_SearchControls.Operation.Code);
//        WFD01150_setControlsRequiredField(WFD01150_SearchControls.RequiredInput1, WFD01150_SearchControls.RequiredInput2, true);
//        WFD01150_OnClear();
//    }, null);

//}

var WFD01150_OnSearch = function () {
    WFD01150_SearchControls.Table.SearchTablePagin.ClearPaginData();
    WFD01150_SearchControls.Operation.Code = WFD01150_SearchControls.Input.Operation.val();
    WFD01150_SearchControls.Operation.Name = WFD01150_SearchControls.Input.Operation.select2('data')[0].text;

    WFD01150_SearchControls.Company.Code = WFD01150_SearchControls.Input.Company.val();
    WFD01150_SearchControls.Company.Name = WFD01150_SearchControls.Input.Company.select2('data')[0].text;

    WFD01150_Search(true);
}
var WFD01150_OnAdd = function () {
    
    var data = {
        REQUEST_TYPE: WFD01150_SearchControls.Input.Operation.val()
    };

    ajax_method.Post(WFD01150_Url.ValidateAddApprove, data, true, function (res) {
        if (res == true) {

            if (WFD01150_SearchControls.Input.Operation.val() != '' && WFD01150_SearchControls.Input.Operation.val() != null) {
                WFD01150_SearchControls.Operation.Code = WFD01150_SearchControls.Input.Operation.val();
                WFD01150_SearchControls.Operation.Name = WFD01150_SearchControls.Input.Operation.select2('data')[0].text;
                WFD01150_EditPage.InitAddPage();
            }

        } else {
            $('#WFD01150_Button_Add').hide();
            $('#WFD01150_Button_Load').show();
            $('#myFormlAddInfo').hide();
            //$('#WFD01150_SearchResult').show();
        }
    }, null);
    WFD01150_SearchControls.Company.Code = WFD01150_SearchControls.Input.Company.val();
}
var WFD01150_OnClear = function () {
    $('#WFD01150_Button_Add').hide();
    $('#WFD01150_Button_Load').show();

    WFD01150_SearchControls.Input.Company.select2('val', defaultFilter.company);
    WFD01150_SearchControls.Input.Operation.select2('val', null);
    WFD01150_SearchControls.Table.SearchResultRow.hide();
    $('#WFD01150_SearchTable-pagin-area').hide();
    WFD01150_Body.html('');
    ClearMessageC();
}

var WFD01150_Search = function (isClearMessage) {
    if (isClearMessage) {
        ClearMessageC();
    }
    var page = WFD01150_SearchControls.Table.SearchTablePagin.GetPaginData();

    var data = {
        REQUEST_FLOW_TYPE: WFD01150_SearchControls.Operation.Code,
        COMPANY: WFD01150_SearchControls.Company.Code
    };

    $('#WFD01150_Button_Add').hide();
    $('#WFD01150_Button_Load').show();
    $('#myFormlAddInfo').hide();
    WFD01150_SearchControls.Box.SearchBox.show();
    WFD01150_Body.html('');

    ajax_method.SearchPost(WFD01150_Url.SearchApproveByRequestType, data, page, true, function (data, pagin) {

        if (data != null && data.Rows.length > 0 && data.FlowMasterDatas.length) {
            //debugger;
            WFD01150_SearchControls.Datas = data;
            WFD01150_FNSearchTable.GenTable(data);
            WFD01150_SearchControls.Table.SearchTablePagin.Init(pagin, null, changePage, '', 'WFD01150');
            WFD01150_SearchControls.Table.SearchResultRow.show();

        } else {
            $('#WFD01150_SearchTable-pagin-area').hide();
            WFD01150_SearchControls.Table.SearchResultRow.hide();
        }

        WFD01150_SearchControls.Box.SearchBox.show();
        WFD01150_setControlsRequiredField(WFD01150_SearchControls.RequiredInput1, WFD01150_SearchControls.RequiredInput2, true);
        WFD01150_Body.html('');

    }, null, isClearMessage);
}


// Callback function after change pagination on table
var changePage = function (PaginData) {
    WFD01150_Search(true);
}
// Initial search table UI
var WFD01150_FNSearchTable = {
    GenTable: function (model) {
        this.GenFlowApproveColHeader(model);
        var html = '';
        var width = 0;
        width += WFD01150_SearchControls.Table.Width.col1 + WFD01150_SearchControls.Table.Width.col2 + WFD01150_SearchControls.Table.Width.col3;
        width += (WFD01150_SearchControls.Table.Width.flowCol * model.FlowMasterDatas.length);

        for (var i = 0; i < model.Rows.length; i++) {
            //debugger;
            html += '<tr ondblclick="WFD01150_EditPage.initEditPage(' + i + ',\'' + model.Rows[i].COMPANY + '\')">';
            html += '   <td><button type="button" class="WFD01150-btn-del" onclick="WFD01150_FNSearchTable.WFD01150_DeleteApproveData(' + i + ')" >X</button></td>';
            html += '   <td>' + model.Rows[i].NO + '</td>';
            html += '   <td>' + model.Rows[i].FIXED_ASSET_CATEGORY + '</td>';

            html += this.GetFlowApproveRow(model.Rows[i].Flows, model.FlowMasterDatas);

            html += '</tr>';

        }

        WFD01150_SearchControls.Table.SearchTable.removeAttr('style');

        WFD01150_SearchControls.Table.SearchTable_Body.html(html);
        var oldWidth = WFD01150_SearchControls.Table.SearchTable.width();
        //debugger;
        if (oldWidth < width) {
            WFD01150_SearchControls.Table.SearchTable.width(width);
            WFD01150_SearchControls.Table.SearchTable.find('th.dynamic-width').css('width', WFD01150_SearchControls.Table.Width.col3 + 'px');
            ///$('#WFD01150_SearchTable-pagin-area').width(width + 15);

        } else {
            WFD01150_SearchControls.Table.SearchTable.width(width);
            //$('#WFD01150_SearchTable-pagin-area').width(width + 15);
        }

    },
    WFD01150_DeleteApproveData: function (i) {

        loadConfirmAlert(WFD01150_Message.ConfirmDelete, function (res) {
            if (res == true) {

                var data = WFD01150_SearchControls.Datas.Rows[i];
                //debugger;
                ajax_method.Post(WFD01150_Url.DeleteApprove, data, true, function (res) {

                    if (res == true) {

                        WFD01150_Search(false);

                    }

                }, null);

            }
        });

    },
    GenFlowApproveColHeader: function (model) {

        WFD01150_SearchControls.Table.SearchTable_ApproveFlowHeadCol.attr('colspan', model.FlowMasterDatas.length);
        WFD01150_SearchControls.Table.SearchTable_ApproveFlowHeadCol.html('Approve Flow');

        var html = '';

        for (var i = 0; i < model.FlowMasterDatas.length; i++) {
            html += '<th class="WFD01150-flow-col">' + model.FlowMasterDatas[i].CODE + '</th>';
        }

        WFD01150_SearchControls.Table.SearchTable_FlowApproverRow.html(html);
    }, GetFlowApproveRow: function (Flows, FlowApproves) {

        var html = '';

        for (var i = 0; i < FlowApproves.length; i++) {
            var role = this.GetRoleInThisFlow(FlowApproves[i], Flows);
            html += '<td class="WFD01150-flow-col"  title="' + role + '">' + role + '</td>';
        }

        return html;

    }, GetRoleInThisFlow: function (flowApprove, Flows) {

        for (var i = 0; i < Flows.length; i++) {
            if (Flows[i].INDX == flowApprove.CODE) {
                return Flows[i].ROLE_NAME;
            }
        }

        return '';
    }

};


//#endregion

//#region   Edit Page

var WFD01150_EditPage = {
    initEditPage: function (i, _Company) {
        WFD01150_SearchControls.Company.Code = _Company;
        WFD01150_EditControls.Mode = WFD01150_MODE.Edit_MODE;
        WFD01150_EditControls.ProcessType = 'Edit Operation';
        WFD01150_EditControls.Data.PageCondition.ApproveId = WFD01150_SearchControls.Datas.Rows[i].APPRV_ID;
        WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code = WFD01150_SearchControls.Operation.Code;
        WFD01150_EditControls.Data.PageCondition.RequestFlowType.Name = WFD01150_SearchControls.Operation.Name;

        WFD01150_EditControls.Data.PageCondition.AssetClass.Code = WFD01150_SearchControls.Datas.Rows[i].ASSET_CLASS;
        WFD01150_EditControls.Data.PageCondition.AssetClass.Name = WFD01150_SearchControls.Datas.Rows[i].FIXED_ASSET_CATEGORY;

        ajax_method.Post(WFD01150_Url.EditPage, null, true, function (res) {

            WFD01150_Body.html(res);
            WFD01150_SearchControls.Box.SearchBox.hide();
            $(WFD01150_EditControls.Box.EditDetailBox_ID).show();
            WFD01150_EditPage.initEditPageData();

            $('.WFD01150_EditMode').show();
            $('.WFD01150_AddMode').hide();
        }, null);

    },
    InitAddPage: function () {
        WFD01150_EditControls.Mode = WFD01150_MODE.ADD_MODE;
        WFD01150_EditControls.ProcessType = 'Add Operation';
        WFD01150_EditControls.Data.PageCondition.ApproveId = null;
        WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code = WFD01150_SearchControls.Operation.Code;
        WFD01150_EditControls.Data.PageCondition.RequestFlowType.Name = WFD01150_SearchControls.Operation.Name;
        WFD01150_EditControls.Data.PageCondition.AssetClass.Code = null;
        WFD01150_EditControls.Data.PageCondition.AssetClass.Name = null;

        ajax_method.Post(WFD01150_Url.EditPage, null, true, function (res) {
            WFD01150_Body.html(res);
            WFD01150_SearchControls.Box.SearchBox.hide();
            $(WFD01150_EditControls.Box.EditDetailBox_ID).show();
            WFD01150_EditPage.initEditPageData();

            $(".select2").select2({ width: '100%' });
            $('.WFD01150_AddMode').show();
            $('.WFD01150_EditMode').hide();
            $(WFD01150_EditControls.EditControl.Input_ID.AssetCategoryCBB).Select2Require(true);

        }, null);

    },
    initEditPageData: function () {
        var data = {
            APPROVE_ID: WFD01150_EditControls.Data.PageCondition.ApproveId,
            REQUEST_FLOW_TYPE: WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code
        };

        ajax_method.Post(WFD01150_Url.GetApproveDDatas, data, true, function (res) {

            WFD01150_EditPage.BindingEditPageData(res);

        }, null, false);

    },
    BindingEditPageData: function (model) {
        WFD01150_EditControls.Data.ApproveDDatas = model.datas;
        WFD01150_EditControls.Data.ApproveFlows = model.approveFlows;
        WFD01150_EditControls.Data.ApproveH = model.hData;
        WFD01150_EditControls.Data.ApproveH.UPDATE_DATE = convertCSDate(WFD01150_EditControls.Data.ApproveH.UPDATE_DATE);
        WFD01150_EditPage.ApproveDTable.InitApproveDTable(WFD01150_EditControls.Data.ApproveDDatas, WFD01150_EditControls.Data.ApproveFlows);

        $(WFD01150_EditControls.ListControl.AssetCategoryLabel_ID).html(WFD01150_EditControls.Data.PageCondition.AssetClass.Name);
        $(WFD01150_EditControls.ListControl.OperationLabel_ID).html(WFD01150_EditControls.Data.PageCondition.RequestFlowType.Name);
        $(WFD01150_EditControls.Box.EditDetailBox_ID).hide();

        $(WFD01150_EditControls.ListControl.SubmitEdit_ID).off('click').click(WFD01150_EditPage.OnSubmit);
        $(WFD01150_EditControls.ListControl.CancelEdit_ID).off('click').click(WFD01150_EditPage.CancelEdit);

        WFD01150_EditControls.Data.GUID = model.GUID;
    },

    OnSubmit: function () {
        if (WFD01150_EditControls.Mode == WFD01150_MODE.Edit_MODE) {
            WFD01150_EditPage.SubmitEdit();
        } else {
            WFD01150_EditPage.SubmitAdd();
        }

    },

    SubmitEdit: function () {


        loadConfirmAlert(WFD01150_Message.ConfirmSave, function (res) {
            //debugger;
            if (res) {
                var data = {
                    hData: ajax_method.ConvertDateBeforePostBack(WFD01150_EditControls.Data.ApproveH),
                    datas: WFD01150_EditControls.Data.ApproveDDatas
                };
                ajax_method.Post(WFD01150_Url.SubmitSaveApproveDDatas, data, true, function (res) {
                    //debugger;
                    if (res) {
                        //WFD01150_EditPage.initEditPageData();

                        $('#WFD01150_Button_Add').hide();
                        $('#WFD01150_Button_Load').show();
                        $('#myFormlAddInfo').hide();
                        WFD01150_SearchControls.Box.SearchBox.show();
                        WFD01150_Search(false);
                        WFD01150_Body.html('');
                        WFD01150_setControlsRequiredField(WFD01150_SearchControls.RequiredInput1, WFD01150_SearchControls.RequiredInput2, true);
                    }

                }, null);
            }

        });
    },

    SubmitAdd: function () {

        loadConfirmAlert(WFD01150_Message.ConfirmSave, function (res) {
            //debugger;
            if (res) {
                //alert(WFD01150_SearchControls.Company.Code);
                var data = {
                    Company: WFD01150_SearchControls.Company.Code,
                    datas: WFD01150_EditControls.Data.ApproveDDatas,
                    AssetCategory: $(WFD01150_EditControls.EditControl.Input_ID.AssetCategoryCBB).val(),
                    ApplyToAll: BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.ApplyToAllAsset).prop('checked')),
                    requestType: WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code,
                    GUID: WFD01150_EditControls.Data.GUID
                };

                ajax_method.Post(WFD01150_Url.SubmitAddApproveData, data, true, function (res) {
                    if (res != -1) {

                        if (res != -99) {
                            WFD01150_EditControls.Mode = WFD01150_MODE.Edit_MODE;
                            WFD01150_EditControls.ProcessType = 'Edit Operation';
                            WFD01150_EditControls.Data.PageCondition.ApproveId = res;
                            WFD01150_EditControls.Data.PageCondition.AssetClass.Code = $(WFD01150_EditControls.EditControl.Input_ID.AssetCategoryCBB).val();
                            WFD01150_EditControls.Data.PageCondition.AssetClass.Name = $(WFD01150_EditControls.EditControl.Input_ID.AssetCategoryCBB).select2('data')[0].text;

                           // $('.WFD01150_EditMode').show();
                            //$('.WFD01150_AddMode').hide();

                            //WFD01150_EditPage.initEditPageData();

                            $('#WFD01150_Button_Add').hide();
                            $('#WFD01150_Button_Load').show();
                            $('#myFormlAddInfo').hide();
                            WFD01150_SearchControls.Box.SearchBox.show();
                            WFD01150_Search(false);
                            WFD01150_Body.html('');
                            WFD01150_setControlsRequiredField(WFD01150_SearchControls.RequiredInput1, WFD01150_SearchControls.RequiredInput2, true);
                        } else {

                            WFD01150_SearchControls.Input.Operation.select2('val', WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code);
                            WFD01150_Search(false);
                        }
                    }

                }, null);
            }

        });

    },
    CancelEdit: function () {

        if (loadConfirmAlert(WFD01150_Message.ConfirmCancel.replace('[PROCESS]', WFD01150_EditControls.ProcessType), function (res) {
            if (res == true) {

                $('#WFD01150_Button_Add').hide();
                $('#WFD01150_Button_Load').show();
                $('#myFormlAddInfo').hide();
                WFD01150_SearchControls.Box.SearchBox.show();
            //WFD01150_Search(true);
                //initSearchPage();
                WFD01150_Search(true);
                //WFD01150_SearchControls.Button.Search.click();

                //WFD01150_Body.html('');
                ClearMessageC();
            WFD01150_setControlsRequiredField(WFD01150_SearchControls.RequiredInput1, WFD01150_SearchControls.RequiredInput2, true);
        }
        }));
    },

    ApproveDTable: {

        InitApproveDTable: function (datas, approveFlows) {
            var html = '';
            for (var i = 0; i < approveFlows.length; i++) {
                var data = WFD01150_EditPage.EditApproveDetail._GetApproveData(approveFlows[i], datas);
                var dataIndex = WFD01150_EditPage.EditApproveDetail._GetApproveDataIndex(approveFlows[i], datas);

                html += WFD01150_EditPage.ApproveDTable._InitApproveDTableRow(data, approveFlows[i], i, dataIndex);

            }


            $(WFD01150_EditControls.ListControl.ApproveDTable_ID.Body).html(html);
            $(WFD01150_EditControls.Box.EditDetailBox_ID).hide();
            //$('#WFD01150_ApproveDTable tbody').html(html);
        },
        _InitApproveDTableRow: function (data, approveFlow, index, dataIndex) {
            //debugger;
            var html = '';
            html += '<tr ondblclick="WFD01150_EditPage.EditApproveDetail.EditApproveDDetail(' + index + ')">';
            if (data == null) {

                html += '   <td class="text-center"></td>';
                html += '   <td class="text-center">' + approveFlow.CODE + '</td>';
                html += '   <td class="text-center"></td>';
                html += '   <td class="text-center"></td>';
                html += '   <td class="text-center"></td>';
                html += '   <td class="text-center"></td>';
                html += '   <td class="text-center"></td>';
                html += '   <td class="text-center"></td>';
                html += '   <td></td>';

            } else {
                html += '   <td class="text-center"><button type="button" class="WFD01150-btn-del" onclick="WFD01150_EditPage.EditApproveDetail.DeleteApproveDDetail(' + dataIndex + ',\'' + data.INDX + '\')">X</button></td>';
                html += '   <td class="text-center">' + approveFlow.CODE + '</td>';
                html += '   <td class="text-center">' + IsNull(data.ROLE_NAME) + '</td>';
                html += '   <td class="text-center">' + IsNull(data.DIVISION_NAME) + '</td>';
                html += '   <td class="text-center">' + IsNull(data.APPRV_GROUP) + '</td>';
                html += '   <td class="text-center">' + WFD01150_EditPage.ApproveDTable._ShowOperator(data) + '</td>';
                html += '   <td class="text-center">' + FlagStr(data.REQUIRE_FLAG) + '</td>';
                html += '   <td class="text-center">' + FlagStr(data.FINISH_FLOW) + '</td>';
                html += '   <td class="text-center">' + IsNull(data.REJECT_TO_NAME) + '</td>';
            }
            html += '</tr>';

            return html;
        },
        _ShowOperator: function (data) {
            var txt = '';

            if (data.OPERATOR == null) {
                return '';
            }

            if (data.OPERATOR_NAME != null)
                txt = data.OPERATOR_NAME;
            //switch (data.OPERATOR) {
            //    case '1':
            //        txt = '>=';
            //        break;
            //    case '2':
            //        txt = '<';
            //        break;
            //    case '3':
            //        txt = 'Between';
            //        break;
            //}

            if (data.OPERATOR != '3') {
                txt += ShowStringMoneyFormat(IsNull(data.VALUE1));
            } else {
                txt += ShowStringMoneyFormat(IsNull(data.VALUE1)) + ' and ' + ShowStringMoneyFormat(IsNull(data.VALUE2));
            }

            return txt;
        },

    },
    ApproveEmailTable: {

        InitApproveEmailTable: function (headerData, datas) {
            var html = '';
            var htmlApprove = '';
            var htmlSeq = '';
            var htmlRole = '';
            var _column = 0
            if (headerData != null && headerData.length > 0) {
                _column = headerData.length + 1;
                htmlApprove += '<tr style="border-bottom-width: 1px; border-bottom-style: solid;">';
                htmlApprove += '   <th colspan="' + _column + '">Approve Flow</th>';
                htmlApprove += '</tr>';

                htmlSeq += '<tr style="border-bottom-width: 1px; border-bottom-style: solid;"><th style="width: 100px;" class="text-center">Sequence</th>';
                htmlRole += '<tr><th style="width: 100px;" class="text-center">Role</th>';

                for (var i = 0; i < headerData.length; i++) {
                    htmlSeq += '   <th style="width: 70px;" class="text-center">' + headerData[i].INDX + '<input type="hidden" id="WFD01150_KEYEMAIL_' + headerData[i].INDX + '" value="' + headerData[i].INDX + '" /></th>';
                    htmlRole += '   <th style="width: 70px;" class="text-center"><label class="control-label" id="WFD01150_KEYEMAIL_ROLE_LABEL_' + headerData[i].INDX + '">' + headerData[i].ROLE + '</label><input type="hidden" id="WFD01150_KEYEMAIL_ROLE_' + headerData[i].INDX + '" value="' + headerData[i].ROLE + '" /></th>';
                }

                htmlSeq += '</tr>';
                htmlRole += '</tr>';
            }
            html = htmlApprove + htmlSeq + htmlRole;
            $(WFD01150_EditControls.ListControl.ApproveEmailDTable_ID.Thead).append(html);
            WFD01150_EditPage.ApproveEmailTable._InitEmailBodyTableRow(headerData, datas);
            //$(WFD01150_EditControls.ListControl.ApproveDTable_ID.Body).html(html);
            //$(WFD01150_EditControls.Box.EditDetailBox_ID).hide();
            //$('#WFD01150_ApproveDTable tbody').html(html);
        },
        _InitEmailBodyTableRow: function (headerData, datas) {
            var html = '';
            var htmlAPR_TO = '';
            var htmlAPR_CC = '';
            var htmlREJ_TO = '';
            var htmlREJ_CC = '';

            htmlAPR_TO += '<tr><td class="text-center">Approve : To</td>';
            htmlAPR_CC += '<tr><td class="text-center">            : CC</td>';
            htmlREJ_TO += '<tr><td class="text-center">Reject  : To</td>';
            htmlREJ_CC += '<tr><td class="text-center">            : CC</td>';

            for (var i = 0; i < datas.length; i++) {
                if (datas[i].DISPLAY_CHECKBOX == 'Y') {
                    if (datas[i].EMAIL_APR_TO == 'Y') {
                        htmlAPR_TO += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_APR_TO_' + datas[i].E_INDX + '" value="Y" checked /></td>';
                    } else {
                        htmlAPR_TO += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_APR_TO_' + datas[i].E_INDX + '" value="Y" /></td>';
                    }
                    if (datas[i].EMAIL_APR_CC == 'Y') {
                        htmlAPR_CC += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_APR_CC_' + datas[i].E_INDX + '" value="Y" checked /></td>';
                    } else {
                        htmlAPR_CC += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_APR_CC_' + datas[i].E_INDX + '" value="Y" /></td>';
                    }
                    if (datas[i].EMAIL_REJ_TO == 'Y') {
                        htmlREJ_TO += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_REJ_TO_' + datas[i].E_INDX + '" value="Y" checked /></td>';
                    } else {
                        htmlREJ_TO += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_REJ_TO_' + datas[i].E_INDX + '" value="Y" /></td>';
                    }
                    if (datas[i].EMAIL_REJ_CC == 'Y') {
                        htmlREJ_CC += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_REJ_CC_' + datas[i].E_INDX + '" value="Y" checked /></td>';
                    } else {
                        htmlREJ_CC += '   <td class="text-center"><input type="checkbox" id="WFD01150_EMAIL_REJ_CC_' + datas[i].E_INDX + '" value="Y" /></td>';
                    }
                } else {
                    htmlAPR_TO += '   <td class="text-center"></td>';
                    htmlAPR_CC += '   <td class="text-center"></td>';
                    htmlREJ_TO += '   <td class="text-center"></td>';
                    htmlREJ_CC += '   <td class="text-center"></td>';
                }
            }
            htmlAPR_TO += '</tr>';
            htmlAPR_CC += '</tr>';
            htmlREJ_TO += '</tr>';
            htmlREJ_CC += '</tr>';
            html = htmlAPR_TO + htmlAPR_CC + htmlREJ_TO + htmlREJ_CC;
            $(WFD01150_EditControls.ListControl.ApproveEmailDTable_ID.Body).html(html);
        },
        _ShowOperator: function (data) {
            var txt = '';

            if (data.OPERATOR == null) {
                return '';
            }

            if (data.OPERATOR_NAME != null)
                txt = data.OPERATOR_NAME;
            //switch (data.OPERATOR) {
            //    case '1':
            //        txt = '>=';
            //        break;
            //    case '2':
            //        txt = '<';
            //        break;
            //    case '3':
            //        txt = 'Between';
            //        break;
            //}

            if (data.OPERATOR != '3') {
                txt += ShowStringMoneyFormat(IsNull(data.VALUE1));
            } else {
                txt += ShowStringMoneyFormat(IsNull(data.VALUE1)) + ' and ' + ShowStringMoneyFormat(IsNull(data.VALUE2));
            }

            return txt;
        },

    },
    EditApproveDetail: {

        EditApproveDDetail: function (i) {
            WFD01150_EditControls.Data.EditDetailItem = WFD01150_EditPage.EditApproveDetail._GetApproveData(WFD01150_EditControls.Data.ApproveFlows[i], WFD01150_EditControls.Data.ApproveDDatas);
            if (WFD01150_EditControls.Data.EditDetailItem == null) {
                WFD01150_EditControls.Data.EditDetailItem = jQuery.extend(true, {}, WFD01150_EditControls.Data.ApproveDBlankData);
                WFD01150_EditControls.Data.EditDetailItem.INDX = WFD01150_EditControls.Data.ApproveFlows[i].CODE;
            }


            var datas = [];
            for (var j = 0; j < WFD01150_EditControls.Data.ApproveDDatas.length; j++) {
                //debugger;
                console.log(integer(WFD01150_EditControls.Data.ApproveDDatas[j].INDX) + ' ' + integer(WFD01150_EditControls.Data.EditDetailItem.INDX) + ' : '
                    + (integer(WFD01150_EditControls.Data.ApproveDDatas[j].INDX) < integer(WFD01150_EditControls.Data.EditDetailItem.INDX)));

                if (integer(WFD01150_EditControls.Data.ApproveDDatas[j].INDX) < integer(WFD01150_EditControls.Data.EditDetailItem.INDX)) {
                    datas.push({ SEQ: WFD01150_EditControls.Data.ApproveDDatas[j].INDX, ROLE: WFD01150_EditControls.Data.ApproveDDatas[j].ROLE, ROLE_NAME: WFD01150_EditControls.Data.ApproveDDatas[j].ROLE_NAME });
                }
            }


            var model = {
                CurrentSeq: WFD01150_EditControls.Data.EditDetailItem.INDX,
                OperationCode: WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code,
                UpperRoles: datas
            };

            ajax_method.Post(WFD01150_Url.EditDetailPage, model, true, function (res) {

                $(WFD01150_EditControls.Box.EditDetailBox_ID).html(res);
                $(WFD01150_EditControls.Box.EditDetailBox_ID).show();

                WFD01150_EditPage.EditApproveDetail.BindingEditDetailData(WFD01150_EditControls.Data.EditDetailItem, datas);

            }, null);



        },
        BindingEditDetailData: function (data, rejectList) {

            $(".select2").select2({ width: '100%' });
            $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).IntegerInput();
            $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).IntegerInput();
            $(WFD01150_EditControls.EditControl.Input_ID.APPRV_GROUP).NormalTextInput();
            $(WFD01150_EditControls.EditControl.Input_ID.AssetCategoryCBB).Select2Require(true);

            WFD01150_setControlsRequiredField(WFD01150_EditControls.EditControl.Required, null, true);


            $(WFD01150_EditControls.EditControl.Input_ID.ALLOW_DEL_ITEM).prop('checked', FlagBool(data.ALLOW_DEL_ITEM));
            $(WFD01150_EditControls.EditControl.Input_ID.ALLOW_REJECT).prop('checked', FlagBool(data.ALLOW_REJECT));
            $(WFD01150_EditControls.EditControl.Input_ID.ALLOW_SEL_APPRV).prop('checked', FlagBool(data.ALLOW_SEL_APPRV));
            $(WFD01150_EditControls.EditControl.Input_ID.HIGHER_APPR).prop('checked', FlagBool(data.HIGHER_APPR));
            $(WFD01150_EditControls.EditControl.Input_ID.FINISH_FLOW).prop('checked', FlagBool(data.FINISH_FLOW));
            $(WFD01150_EditControls.EditControl.Input_ID.REQUIRE_FLAG).prop('checked', FlagBool(data.REQUIRE_FLAG));

            if (rejectList != null && rejectList.length > 0) {
                $(WFD01150_EditControls.EditControl.Input_ID.ALLOW_REJECT).prop('disabled', false);
            } else {
                $(WFD01150_EditControls.EditControl.Input_ID.ALLOW_REJECT).prop('disabled', true);
            }

            $(WFD01150_EditControls.EditControl.Input_ID.INDX).html(data.INDX);
            $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).val(data.VALUE1);
            $(WFD01150_EditControls.EditControl.Input_ID.APPRV_GROUP).val(data.APPRV_GROUP);
            $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).val(data.VALUE2);

            $(WFD01150_EditControls.EditControl.Input_ID.ROLE).select2('val', data.ROLE);
            $(WFD01150_EditControls.EditControl.Input_ID.DIVISION).select2('val', data.DIVISION);
            $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).select2('val', data.OPERATOR);
            $(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).select2('val', data.REJECT_TO);
            $(WFD01150_EditControls.EditControl.Input_ID.CONDITION_CODE).select2('val', data.CONDITION_CODE);

            $(WFD01150_EditControls.EditControl.Input_ID.KPI_LEADTIME).val(data.LEAD_TIME);
            $(WFD01150_EditControls.EditControl.Input_ID.GENERATE_FILE).prop('checked', data.GENERATE_FILE == 'Y');

            $(WFD01150_EditControls.EditControl.Input_ID.NOTIFICATION_PERSON).prop('checked', data.NOTI_BY_EMAIL == 'P');
            $(WFD01150_EditControls.EditControl.Input_ID.NOTIFICATION_GROUP).prop('checked', data.NOTI_BY_EMAIL == 'G');
            $(WFD01150_EditControls.EditControl.Input_ID.OPERATION_PERSON).prop('checked', data.OPERATION == 'P');
            $(WFD01150_EditControls.EditControl.Input_ID.OPERATION_GROUP).prop('checked', data.OPERATION == 'G');


            $('html, body').animate({ scrollTop: $(WFD01150_EditControls.Box.EditDetailBox_ID).offset().top }, 'fast');

            $(WFD01150_EditControls.EditControl.Button.SaveItem).click(WFD01150_EditPage.EditApproveDetail.SaveEditDetail);
            $(WFD01150_EditControls.EditControl.Button.CancelItem).click(WFD01150_EditPage.EditApproveDetail.CancelEditDetail);


            WFD01150_EditPage.EditApproveDetail.BidingInputEditDetailEvent(data);

            //------- 2019-10-14 Suphachai -------//
            var modelEmail = {
                APPROVE_ID: WFD01150_EditControls.Data.PageCondition.ApproveId,
                REQUEST_FLOW_TYPE: WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code,
                CurrentSeq: WFD01150_EditControls.Data.EditDetailItem.INDX,
                OperationCode: WFD01150_EditControls.Data.PageCondition.RequestFlowType.Code,
                GUID: WFD01150_EditControls.Data.GUID,
                UpperRoles: data
            };

            ajax_method.Post(WFD01150_Url.GetApproveEmailDatas, modelEmail, true, function (data) {
                //debugger;
                WFD01150_EditPage.ApproveEmailTable.InitApproveEmailTable(data.hData, data.datas);
                WFD01150_EditControls.Data.ApproveRole = data.rData;

                //debugger;
                //$(WFD01150_EditControls.Box.EditDetailBox_ID).html(data);
                //$(WFD01150_EditControls.Box.EditDetailBox_ID).show();

                //WFD01150_EditPage.EditApproveDetail.BindingEditDetailData(WFD01150_EditControls.Data.EditDetailItem, datas);

            }, null);

        },

        BidingInputEditDetailEvent: function (data) {

            $(WFD01150_EditControls.EditControl.Input_ID.ALLOW_REJECT).on('change', function (e) {
                WFD01150_EditPage.EditApproveDetail.SetEnableControls.ALLOW_REJECT($(this).prop('checked'));
                WFD01150_EditPage.EditApproveDetail.SetInputRequired.ALLOW_REJECT($(this).prop('checked'));
            });

            $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).on("change", function (e) {
                WFD01150_EditPage.EditApproveDetail.SetEnableControls.OPERATOR($(this).val());
                WFD01150_EditPage.EditApproveDetail.SetInputRequired.OPERATOR($(this).val());
            });

            $(WFD01150_EditControls.EditControl.Input_ID.CONDITION_CODE).on("change", function (e) {
                WFD01150_EditPage.EditApproveDetail.SetEnableControls.CONDITION_CODE($(this).val());
                WFD01150_EditPage.EditApproveDetail.SetInputRequired.CONDITION_CODE($(this).val());
            });


            WFD01150_EditPage.EditApproveDetail.SetEnableControls.OPERATOR(data.OPERATOR);
            WFD01150_EditPage.EditApproveDetail.SetEnableControls.ALLOW_REJECT(FlagBool(data.ALLOW_REJECT));
            WFD01150_EditPage.EditApproveDetail.SetEnableControls.CONDITION_CODE(data.CONDITION_CODE);


            WFD01150_EditPage.EditApproveDetail.SetInputRequired.CONDITION_CODE(data.CONDITION_CODE);
            WFD01150_EditPage.EditApproveDetail.SetInputRequired.ALLOW_REJECT($(this).prop('checked'));


            $(WFD01150_EditControls.EditControl.Input_ID.ROLE).on("change", function (e) {
                WFD01150_EditPage.EditApproveDetail.DefauleNotificationAndOperation();
            });
        },
        DefauleNotificationAndOperation: function () {
            //debugger;
            if (WFD01150_EditControls.Data.ApproveRole != null) {
                var resultAarray = jQuery.grep(WFD01150_EditControls.Data.ApproveRole, function (n, i) {
                    return (n.CODE == $('#WFD01150_ROLE').select2('data')[0].id);
                }, false);

                if (resultAarray != null) {
                    if (resultAarray[0].ROLE_TYPE == 'U') {
                        $('#WFD01150_NOTIFICATION_PERSON').prop('checked', true);
                        $('#WFD01150_NOTIFICATION_GROUP').prop('checked', false);
                        $('#WFD01150_OPERATION_PERSON').prop('checked', true);
                        $('#WFD01150_OPERATION_GROUP').prop('checked', false);
                        $('#WFD01150_NOTIFICATION_GROUP').prop('disabled', true);
                        $('#WFD01150_OPERATION_GROUP').prop('disabled', true);
                    } else {
                        $('#WFD01150_NOTIFICATION_GROUP').prop('disabled', false);
                        $('#WFD01150_OPERATION_GROUP').prop('disabled', false);
                    }
                }
                var _index = $(WFD01150_EditControls.EditControl.Input_ID.INDX).html();
                _index = '#WFD01150_KEYEMAIL_ROLE_LABEL_' + _index;
                $(_index).html($('#WFD01150_ROLE').select2('data')[0].text);
                //WFD01150_KEYEMAIL_ROLE_
            }
        },
        SetEnableControls: {
            ALLOW_REJECT: function (c) {

                $(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).select2().prop('disabled', !c);
                if (c == false) {
                    $(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).select2('val', null);
                }
            },
            OPERATOR: function (val) {
                //if (val == '3') {
                //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).prop('disabled', false);
                //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).prop('disabled', false);
                //} else if (val == '1' || val == '2') {
                //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).prop('disabled', false);
                //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).prop('disabled', true);
                //} else {
                //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).prop('disabled', true);
                //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).prop('disabled', true);
                //}

                if (val == '3') {
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).prop('disabled', false);
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).prop('disabled', false);
                } else if (val == '' || val == null) {
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).prop('disabled', true);
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).prop('disabled', true);
                } else {
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).prop('disabled', false);
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).prop('disabled', true);
                }
            },
            CONDITION_CODE: function (val) {
                if (val != null && val != '') {
                    $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).select2().prop('disabled', false);

                } else {
                    $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).select2().prop('disabled', true);
                    $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).select2('val', null);
                    $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).removeClass('require');
                }
                WFD01150_EditPage.EditApproveDetail.SetInputRequired.OPERATOR($(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).val());
            },

        },
        SetInputRequired: {
            ALLOW_REJECT: function (c) {

                $(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).Select2Require(c);

            },
            CONDITION_CODE: function (val) {
                if (val != null || val != '') {
                    $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).Select2Require(true);
                } else {
                    $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).Select2Require(false);
                }
                WFD01150_EditPage.EditApproveDetail.SetInputRequired.OPERATOR($(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).val());
            },
            OPERATOR: function (val) {
                if (val == '3') {
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).addClass('require');
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).addClass('require');
                }
                    //else if (val == '1' || val == '2') {
                    //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).addClass('require');
                    //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).removeClass('require');
                    //    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).val('');
                    //}
                else if (val == null || val == '') {
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).removeClass('require');
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).removeClass('require');
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).val('');
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).val('');
                } else {
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).addClass('require');
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).removeClass('require');
                    $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).val('');
                }
            }

        },

        SaveEditDetail: function () {

            var data = WFD01150_EditPage.EditApproveDetail._GetEditDetailScreenData();
            ajax_method.Post(WFD01150_Url.ValidateSaveApproveDetail, data, true, function (res) {
                if (res == true) {
                    WFD01150_EditPage.EditApproveDetail._SaveApproveDData(data);
                    WFD01150_EditPage.EditApproveDetail._GetEditDetailEmailScreenData();
                }
            }, null);
        },
        DeleteApproveDDetail: function (i, _indx) {

            loadConfirmAlert(WFD01150_Message.ConfirmDelete, function (res) {

                if (res == true) {
                    console.log(WFD01150_EditControls.Data.ApproveDDatas);
                    console.log(i);
                    WFD01150_EditControls.Data.ApproveDDatas.splice(i, 1);
                    console.log(WFD01150_EditControls.Data.ApproveDDatas);
                    WFD01150_EditPage.ApproveDTable.InitApproveDTable(WFD01150_EditControls.Data.ApproveDDatas, WFD01150_EditControls.Data.ApproveFlows);

                    var _data =
                    {
                        APPRV_ID: WFD01150_EditControls.Data.PageCondition.ApproveId,
                        INDX: _indx,
                        GUID: WFD01150_EditControls.Data.GUID
                    }

                    ajax_method.Post(WFD01150_Url.DeleteTempApproveEmailDetail, _data, true, function (res) {
                        //if (res == true) {
                        //    WFD01150_EditPage.EditApproveDetail._SaveApproveDData(data);
                        //}
                    }, null);
                }

            });

            //delete WFD01150_EditControls.Data.ApproveDDatas[i];
        },
        CancelEditDetail: function () {

            WFD01150_EditPage.ApproveDTable.InitApproveDTable(WFD01150_EditControls.Data.ApproveDDatas, WFD01150_EditControls.Data.ApproveFlows);

        },

        _SaveApproveDData: function (approveDData) {
            if (WFD01150_EditPage.EditApproveDetail._IsExistApproveDData(approveDData) == false) {
                WFD01150_EditPage.EditApproveDetail._AddApproveDData(approveDData);
            } else {
                WFD01150_EditPage.EditApproveDetail._UpdateApproveDDatas(approveDData);
            }

            console.log(WFD01150_EditControls.Data.ApproveDDatas);

            WFD01150_EditPage.ApproveDTable.InitApproveDTable(WFD01150_EditControls.Data.ApproveDDatas, WFD01150_EditControls.Data.ApproveFlows);
        },
        _AddApproveDData: function (approveDData) {
            if (WFD01150_EditControls.Data.ApproveDDatas == null) {
                WFD01150_EditControls.Data.ApproveDDatas = [];
            }
            WFD01150_EditControls.Data.ApproveDDatas.push(approveDData);
            WFD01150_EditPage.ApproveDTable.InitApproveDTable(WFD01150_EditControls.Data.ApproveDDatas, WFD01150_EditControls.Data.ApproveFlows);
        },
        _UpdateApproveDDatas: function (approveDData) {
            for (var i = 0; i < WFD01150_EditControls.Data.ApproveDDatas.length; i++) {

                if (WFD01150_EditControls.Data.ApproveDDatas[i].INDX == approveDData.INDX) {
                    WFD01150_EditControls.Data.ApproveDDatas[i] = jQuery.extend(true, {}, approveDData);
                    break;
                }
            }
        },
        _IsExistApproveDData: function (approveDData) {
            if (WFD01150_EditControls.Data.ApproveDDatas == null) return false;
            var exist = false;
            for (var i = 0; i < WFD01150_EditControls.Data.ApproveDDatas.length; i++) {

                if (WFD01150_EditControls.Data.ApproveDDatas[i].INDX == approveDData.INDX) {
                    exist = true;
                    break;
                }
            }

            return exist;
        },
        _GetApproveData: function (approveFlow, approveDDatas) {
            var res = null;
            if (approveDDatas == null) return res;
            for (var i = 0; i < approveDDatas.length; i++) {

                if (approveDDatas[i].INDX == approveFlow.CODE) {
                    res = jQuery.extend(true, {}, approveDDatas[i]);
                    break;
                }
            }
            return res;
        },
        _GetApproveDataIndex: function (approveFlow, approveDDatas) {
            var res = null;
            if (approveDDatas == null) return res;
            for (var i = 0; i < approveDDatas.length; i++) {

                if (approveDDatas[i].INDX == approveFlow.CODE) {
                    res = i;
                    break;
                }
            }
            return res;
        },
        _GetEditDetailScreenData: function () {
            WFD01150_EditControls.Data.EditDetailItem.ALLOW_DEL_ITEM = BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.ALLOW_DEL_ITEM).prop('checked'));
            WFD01150_EditControls.Data.EditDetailItem.ALLOW_REJECT = BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.ALLOW_REJECT).prop('checked'));
            WFD01150_EditControls.Data.EditDetailItem.ALLOW_SEL_APPRV = BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.ALLOW_SEL_APPRV).prop('checked'));
            WFD01150_EditControls.Data.EditDetailItem.HIGHER_APPR = BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.HIGHER_APPR).prop('checked'));
            WFD01150_EditControls.Data.EditDetailItem.FINISH_FLOW = BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.FINISH_FLOW).prop('checked'));
            WFD01150_EditControls.Data.EditDetailItem.REQUIRE_FLAG = BoolFlag($(WFD01150_EditControls.EditControl.Input_ID.REQUIRE_FLAG).prop('checked'));

            WFD01150_EditControls.Data.EditDetailItem.DIVISION = $(WFD01150_EditControls.EditControl.Input_ID.DIVISION).val();
            WFD01150_EditControls.Data.EditDetailItem.CONDITION_CODE = $(WFD01150_EditControls.EditControl.Input_ID.CONDITION_CODE).val();
            WFD01150_EditControls.Data.EditDetailItem.OPERATOR = $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).val();
            WFD01150_EditControls.Data.EditDetailItem.OPERATOR_NAME = $(WFD01150_EditControls.EditControl.Input_ID.OPERATOR).select2('data')[0].text;
            WFD01150_EditControls.Data.EditDetailItem.VALUE1 = $(WFD01150_EditControls.EditControl.Input_ID.VALUE1).val();
            WFD01150_EditControls.Data.EditDetailItem.APPRV_GROUP = $(WFD01150_EditControls.EditControl.Input_ID.APPRV_GROUP).val();
            WFD01150_EditControls.Data.EditDetailItem.VALUE2 = $(WFD01150_EditControls.EditControl.Input_ID.VALUE2).val();
            WFD01150_EditControls.Data.EditDetailItem.REMARK = $(WFD01150_EditControls.EditControl.Input_ID.REMARK).val();

            WFD01150_EditControls.Data.EditDetailItem.NOTI_BY_EMAIL = $(WFD01150_EditControls.EditControl.Input_ID.NOTIFICATION_GROUP).is(":checked") ? "G" : "P";
            WFD01150_EditControls.Data.EditDetailItem.OPERATION = $(WFD01150_EditControls.EditControl.Input_ID.OPERATION_GROUP).is(":checked") ? "G" : "P";

            WFD01150_EditControls.Data.EditDetailItem.ROLE = $(WFD01150_EditControls.EditControl.Input_ID.ROLE).select2('val');
            WFD01150_EditControls.Data.EditDetailItem.REJECT_TO = $(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).select2('val');

            if ($(WFD01150_EditControls.EditControl.Input_ID.ROLE).select2('data').length > 0) {
                WFD01150_EditControls.Data.EditDetailItem.ROLE_NAME = $(WFD01150_EditControls.EditControl.Input_ID.ROLE).select2('data')[0].text;
            } else {
                WFD01150_EditControls.Data.EditDetailItem.ROLE_NAME = "";
            }
            if ($(WFD01150_EditControls.EditControl.Input_ID.DIVISION).select2('data').length > 0) {
                WFD01150_EditControls.Data.EditDetailItem.DIVISION_NAME = $(WFD01150_EditControls.EditControl.Input_ID.DIVISION).select2('data')[0].text;
            }
            else {
                WFD01150_EditControls.Data.EditDetailItem.DIVISION_NAME = "";
            }
            if ($(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).select2('data').length > 0) {
                WFD01150_EditControls.Data.EditDetailItem.REJECT_TO_NAME = $(WFD01150_EditControls.EditControl.Input_ID.REJECT_TO).select2('data')[0].text;
            }
            else {
                WFD01150_EditControls.Data.EditDetailItem.REJECT_TO_NAME = "";
            }


            WFD01150_EditControls.Data.EditDetailItem.INDX = $(WFD01150_EditControls.EditControl.Input_ID.INDX).html();
            WFD01150_EditControls.Data.EditDetailItem.APPROVE_ID = WFD01150_EditControls.Data.PageCondition.ApproveId;


            WFD01150_EditControls.Data.EditDetailItem.GENERATE_FILE = $(WFD01150_EditControls.EditControl.Input_ID.GENERATE_FILE).is(":checked") ? "Y" : "N";
            WFD01150_EditControls.Data.EditDetailItem.LEAD_TIME = $(WFD01150_EditControls.EditControl.Input_ID.KPI_LEADTIME).val();

            WFD01150_EditControls.Data.EditDetailItem.APPRV_ID = WFD01150_EditControls.Data.PageCondition.ApproveId,
            WFD01150_EditControls.Data.EditDetailItem.GUID = WFD01150_EditControls.Data.GUID;
            return WFD01150_EditControls.Data.EditDetailItem;
        },
        _GetEditDetailEmailScreenData: function () {
            var _returnVal = [];
            $("#WFD01150_ApproveEmailDTable input[id^='WFD01150_EMAIL_APR_TO_']").each(function () {
                var id = $(this).attr('id');
                var arr = id.split("WFD01150_EMAIL_APR_TO_");
                //alert($("#WFD01150_KEYEMAIL_ROLE_" + arr[1]).val());
                //alert($("#WFD01150_EMAIL_APR_TO_" + arr[1]).is(":checked"));
                _returnVal.push({
                    APPRV_ID: WFD01150_EditControls.Data.PageCondition.ApproveId,
                    INDX: $(WFD01150_EditControls.EditControl.Input_ID.INDX).html(),
                    ROLE: $("#WFD01150_KEYEMAIL_ROLE_" + arr[1]).val(),
                    E_INDX: arr[1],
                    E_ROLE: '',
                    EMAIL_APR_TO: $("#WFD01150_EMAIL_APR_TO_" + arr[1]).is(":checked") ? "Y" : "N",
                    EMAIL_APR_CC: $("#WFD01150_EMAIL_APR_CC_" + arr[1]).is(":checked") ? "Y" : "N",
                    EMAIL_REJ_TO: $("#WFD01150_EMAIL_REJ_TO_" + arr[1]).is(":checked") ? "Y" : "N",
                    EMAIL_REJ_CC: $("#WFD01150_EMAIL_REJ_CC_" + arr[1]).is(":checked") ? "Y" : "N",
                    GUID: WFD01150_EditControls.Data.GUID
                });
            });
            ajax_method.Post(WFD01150_Url.SaveTempApproveEmailDetail, _returnVal, true, function (res) {
                //if (res == true) {
                //    WFD01150_EditPage.EditApproveDetail._SaveApproveDData(data);
                //}
            }, null);

            //return WFD01150_EditControls.Data.EditDetaiEmaillItem;
            //$(WFD01150_EditControls.EditControl.Input_ID.OPERATION_GROUP).is(":checked")

            //$("#WFD01150_ApproveEmailDTable input[id^='WFD01150_EMAIL_APR_TO_']").each(function () {
            //    var id = $(this).attr('id');
            //    var arr = id.split("WFD01150_EMAIL_APR_TO_");
            //    alert(arr[1]);
            //});
        }

    },







}


//#endregion

//#region  Common function

var FlagStr = function (f) {
    if (f == 'Y') return 'Yes';
    else return 'No';
}
// Convert flag 'Y, 'N' to true, false
var FlagBool = function (flag) {

    if (flag == 'Y') {
        return true;
    } else {
        return false;
    }
}
// Convert true, false to flag 'Y, 'N'
var BoolFlag = function (b) {
    if (b) return 'Y';
    else return 'N';
}

var IsNull = function (obj) {
    if (obj == null) return '';
    else return obj;
}

//  Initial required field
var WFD01150_setControlsRequiredField = function (controls1, controls2, isRequired) {
    for (var i = 0; i < controls1.length; i++) {
        console.log($(controls1[i]));
        if (isRequired == true) {
            if ($(controls1[i]).hasClass('select2')) {
                $(controls1[i]).Select2Require(true);
            } else {
                $(controls1[i]).addClass('require');
            }
        } else {
            if ($(controls1[i]).hasClass('select2')) {
                $(controls1[i]).Select2Require(false);
            } else {
                $(controls1[i]).removeClass("require");
            }
        }
    }
    if (controls2 == null)
        return;

    for (var i = 0; i < controls2.length; i++) {
        console.log($(controls2[i]));
        if (isRequired == true) {
            if ($(controls2[i]).hasClass('select2')) {
                $(controls2[i]).Select2Require(true);
            } else {
                $(controls2[i]).addClass('require');
            }
        } else {
            if ($(controls2[i]).hasClass('select2')) {
                $(controls2[i]).Select2Require(false);
            } else {
                $(controls2[i]).removeClass("require");
            }
        }
    }
}


//#endregion


var convertCSDate = function (d) {
    if (d != null && d != '')
        return new Date(parseInt(d.substr(6)));
    else
        return null
}

var WFD01150_NOTIFICATION_PERSON_Click = function (sender) {
    if ($(sender).is(':checked')) {
        $('#WFD01150_NOTIFICATION_GROUP').prop('checked', false);
    }
};
var WFD01150_NOTIFICATION_GROUP_Click = function (sender) {
    if ($(sender).is(':checked')) {
        $('#WFD01150_NOTIFICATION_PERSON').prop('checked', false);
    }
};
var WFD01150_OPERATION_PERSON_Click = function (sender) {
    if ($(sender).is(':checked')) {
        $('#WFD01150_OPERATION_GROUP').prop('checked', false);
    }
};
var WFD01150_OPERATION_GROUP_Click = function (sender) {
    if ($(sender).is(':checked')) {
        $('#WFD01150_OPERATION_PERSON').prop('checked', false);
    }
};
