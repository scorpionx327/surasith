﻿var typeaheadSearchHelper = function (url, keyName, valueName, isKeyAndValue) {
    var sources = [];

    return {
        getSource: function () {
            return sources;
        },
        loadData: function (searchParams, callBack) {
            $.post(url, searchParams, function (data) {
                sources = [];
                var options = [];
                for (i = 0; i < data.ObjectResult.length; i++) {
                    var row = data.ObjectResult[i];
                    row.displayValue = row[keyName] + ' - ' + row[valueName];
                    sources.push(row);
                    options.push(isKeyAndValue ? row.displayValue : row[valueName]);
                }

                if (callBack) {
                    callBack(options);
                }
            });
        },
        updateValue: function (value) {
            for (i = 0; i < sources.length; i++) {
                var val = isKeyAndValue ? sources[i].displayValue : sources[i][valueName];
                if (value === val) {
                    return sources[i][keyName];
                }
            }
            return value;
        }
    };
};

var typeaheadSearchHelperFocus = function () {
    var $this = $(this);
    var val = $this.val();
    if (!val) {
        $this.typeahead('lookup');
    }
};