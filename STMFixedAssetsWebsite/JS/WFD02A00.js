﻿var control = {
    DataTable: null,
    CostPopup : {
        ASSET_NO : "",
        ASSET_SUB : "",
        COMPANY : "",
        PERCENTAGE : null,
        AMOUNT_POSTED: null,
        BF_CUMU_ACQU_PRDCOST_01 : null
    },
    ColumnIndex : {
        Sel : 0,
        Depreciation : 9,
        Status : 11
    },
    SelectedRow : null
}
var constants = {
    Impairment : "IM",
    Reverse : "RI"
}
$(function () {
    $('#divSearchRequestResult').hide();
});
var fAction = {
    SelectedCostCenter : "",
    SelectedRespCostCenter: "",

    Initialize: function (callback) {
        if (callback != null)
            callback();
    },
    //Should define all request screen.
    LoadData: function (callback) {
        //console.log("LoadData is callled");
        //Do here
        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        //console.log(GLOBAL);
        //console.log(Url);
        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
         //   console.log(datas);
            fAction.SelectedCostCenter = ""; //Clear Selected
            fAction.SelectedRespCostCenter = ""; //Clear Selected
            
            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                table.clear().draw().destroy();
            }
            
            if (datas == null || datas.length == 0) {
                $('#divSearchRequestResult').hide();

                if (callback != null)
                    callback();

                return;
            }

            if (datas[0].IMPAIRMENT_FLAG == constants.Impairment) {
                $('#chkReverseImpairment').prop('checked', false);
                $('#chkImpairment').prop('checked', true);
            } 
            if (datas[0].IMPAIRMENT_FLAG == constants.Reverse) {
                $('#chkImpairment').prop('checked', false);
                $('#chkReverseImpairment').prop('checked', true);
            }

            fAction.SelectedCostCenter = datas[0].COST_CODE;
            fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;
            

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            var _bDP = (PERMISSION.IsAECUser == "Y" && PERMISSION.IsAECState == "A") || PERMISSION.IsAECState == "Y";
            // To be discussion

            control.DataTable = $('#tbSearchResult').DataTable({
                data: datas,
                "columns": [
                    { "data": null }, // 0
                    { "data": "ROW_INDX", 'orderable': false, "class": "text-right" }, // 1
                    { "data": null }, // Asset No 2
                    {
                        "data": "ASSET_SUB",
                        'orderable': false,
                        "class": "text-center",
                        "visible": (true),
                    }, // 3
                    {
                        "data": "ASSET_NAME", 'orderable': false,
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    }, // 4
                    { "data": "ASSET_GROUP", 'orderable': false }, // 5
                    { "data": "DATE_IN_SERVICE", 'orderable': false, "class": "text-center" }, // 6
                    { "data": "TOTALCOST", 'orderable': false, "class": "text-right" }, // 7
                    { "data": "TOTALNETBOOK", 'orderable': false }, // 8
                    { "data": "AMOUNT_POSTED", 'orderable': false }, // 9
                    { "data": null }, // 10
                    { "data": "POSTING_DATE", 'orderable': false}, // 11
                    { "data": "REASON", 'orderable': false }, // 12
                    { "data": "STATUS", 'orderable': false, "class": "text-center" }, // 13
                    { "data": null } // 14
                ],
                'columnDefs': [
                    {
                        'targets': 0,
                        'searchable': false,
                        "visible": (PERMISSION.AllowResend == "Y"),
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CheckBoxRender(data);
                        }
                    },
                    {
                        'targets': 2, //ASSET_NO Show hyperlink
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return fRender.HyperLinkRender(full);
                        }
                    },//
                    {
                        'targets': 7, //BF_CUMU_ACQU_PRDCOST_01 SUM -> TOTALCOST

                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            //   console.log("AMOUNT_POSTED");
                            //   console.log(full);
                            return currencyFormat(data);
                        }

                    },
                    {
                        'targets': 8, //BF_NBV SUM -> TOTALNETBOOK

                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }

                    },
                    {
                        'targets': 9, //AMOUNT_POSTED
                        
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.ButtonRender(full);
                        }

                    },
                    {
                        'targets': 10, //DP
                        'searchable': false,
                        "visible": (_bDP),
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DPCheckBoxRender(full);
                        }

                    },
                    {
                        'targets': 11, //POSTING_DATE
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CalendarRender(full);
                        }
                    },
                    {
                        'targets': 12, //REASON
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.TextBoxRender(full);
                        }

                    },
                    {
                        'targets': 14, //Delete
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DeleteButtonRender(full);
                        }

                    }],
                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                fixedHeader: {
                    header: true,
                    footer: false
                },
                "initComplete": function (settings, json) {
                    console.log('DataTables has finished its initialisation.');
                },
                //2020.01.27 Surasith T. CR spit child and parent
                //"createdRow": function (row, data, index) {
                //    console.log("createdRow");
                //    if (data.ASSET_SUB != '0000' && $('#chkImpairment').is(':checked')) {
                //        $(row).hide();
                //        console.log("Hide Row : " + row);
                //    }
                //},
            }).columns.adjust().draw();

            
            //Incase New Mode
            if (GLOBAL.DOC_NO == '') {
                if (datas.length == 0) {
                    fScreenMode.Submit_InitialMode(function () {
                        fMainScreenMode.Submit_InitialMode();
                    });
                }
                if (datas.length > 0) {
                    fScreenMode.Submit_AddAssetsMode(function () {
                        fMainScreenMode.Submit_AddAssetsMode();
                    });
                }
                //fScreenMode._GridVisible(false, false);
            }

            $('#divSearchRequestResult').show();

            $('.datepicker').datepicker({
                dateFormat: 'dd.mm.yy',
                //minDate: datas[0].TRANS_START_DATE,
                //comment the beforeShow handler if you want to see the ugly overlay 
                beforeShow: function () {
                    setTimeout(function () {
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                }
            });

            if (callback == null)
                return;

            callback();

        }, null);


    },
    
    Clear: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }
            
            fAction.LoadData(function () {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            });

            if (callback == null)
                return;

            callback();
        });
    },
    Export: function (callback) {

        fAction.UpdateAsset(function () {
            var batch = BatchProcess({
                BatchId: "LFD02A02",
                Description: "Export Impairment Request",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'GUID', GLOBAL.GUID);
            batch.Addparam(2, 'DOC_NO', GLOBAL.DOC_NO);
            batch.StartBatch();

            if (callback == null)
                return;

            callback();
        });
    },
    AddAsset: function (_list, callback) { //It's call from dialog
        if (_list == null || _list.length == 0)
            return;

        _list.forEach(function (_data) {
            // do something with `item`
            _data.GUID = GLOBAL.GUID;
            _data.USER_BY = GLOBAL.USER_BY;
            _data.IMPAIRMENT_FLAG = ($('#chkImpairment').is(':checked')) ? 'IM' :
                            ($('#chkReverseImpairment').is(':checked') ? "RI" : '')
        });
      

        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.AddAsset, _list, false, function (result) {
            console.log(result);
            if (result.IsError) {
            //    alert(result.Message); //Test only
                return;
            }
            
            fAction.LoadData();
           
            if (callback == null)
                return;

            callback();

        });
    },
    UpdateAsset:function(callback){
        var _data = fAction.GetClientAssetList(false); // Get All
        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            if (result.IsError) {
                //fnErrorDialog("UpdateAssetList", result.Message);
                return;
            }
            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList

    },
    DeleteAsset: function (_AssetNo, _AssetSub) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var _data = {
            DOC_NO : GLOBAL.DOC_NO,
            GUID    : GLOBAL.GUID,
            COMPANY: GLOBAL.COMPANY,
            ASSET_NO: _AssetNo,
            ASSET_SUB: _AssetSub
        }
        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(_AssetNo, _AssetSub), function (result) {
            if (!result) {
                return;
            }
            fAction.UpdateAsset(function () {
                ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
                    if (result.IsError) {
                        return;
                    }
                    console.log("Delete");
                    fAction.LoadData();
                    // SuccessNotification(CommonMessage.ClearSuccess);
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Deletion process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                });
            }); // UpdateAsset 
        }); // loadConfirmAlert
    },
    ValidateUploadExcel: function () {
        return fAction.ValidateAddAssetDetail();
    },
    UploadExcel: function (_fileName) {
        //call Batch with filename
        var batch = BatchProcess({
            BatchId: "BFD02A01",
            Description: "Upload Impairement Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'TransaType', ($('#chkImpairment').is(':checked')) ? 'IM' : ($('#chkReverseImpairment').is(':checked') ? "RI" : ''));
        batch.Addparam(5, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function (_appID, _Status) {

            if (_Status != 'S') {
                return;
            }
            fAction.LoadData();
        }, true);
        //Company = _p[0],
        //GUID = _p[1],
        //UploadFileName = _p[2],
        //User = _p[3]
    },
    PrepareGenerateFlow: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        
        fAction.UpdateAsset(function(){
            GLOBAL.FLOW_TYPE = 'MN';
            ajax_method.Post(Url.PrepareFlow, { data: GLOBAL}, false, function (result) {
                if (result.IsError) {
                    //fnErrorDialog("PrepareFlow", result.Message);
                    return;
                }

                if (callback == null)
                    return;

                callback();
            }); //ajax_method.PrepareFlow
        }); //ajax_method.UpdateAssetList

        
    },

    PrepareSubmit: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlow
    },
    PrepareApprove: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlows
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },
    ValidateAddAssetDetail: function () {
        ClearMessageC();

        if (!$('#chkReverseImpairment').is(":checked") && !$('#chkImpairment').is(":checked")) {
            AlertTextErrorMessage(CommonMessage.ShouldNotBeEmpty.format("", "Transaction Type"));
            return false;
        }

        return true;
    },
    ShowSearchPopup: function (callback) {
        if (!fAction.ValidateAddAssetDetail()) {
            return;
        }

        fAction.UpdateAsset(function() {

            //Set search parameter

            f2190SearchAction.ClearDefault();

            Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();

            var isIMMode = $('#chkImpairment').is(":checked");

            Search2190Config.searchOption = isIMMode ? "IM" : "";
            Search2190Config.AllowMultiple = true;

            Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
            Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
            Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedRespCostCenter, Enable: fAction.SelectedRespCostCenter == "" };
            Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: true };

            //2020.01.27 Support CR child and Parent
            Search2190Config.Default.Parent = { Value: "Y", Enable: true };
            // Search2190Config.Default.Parent = { Value: "Y", Enable: $('#chkReverseImpairment').is(":checked") };
            //Search2190Config.HasCostValue = isIMMode;

            //Assign Default and Initial screen
            f2190SearchAction.Initialize();

            $('#WFD02190_SearchAssets').modal();

        });
        
        
    },
    
    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");
        if (control.DataTable == null){ 
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List
      
        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "")
            {
                return ;
            }
            var _row = [];
            if (control.DataTable == null) {
                return ;
            }

            var $reasonInput = $(this).find("input[name='txtReason']");

            //var _reason = $(this).find("input[name='txtReason']").val();

            

            var _bSel = $(this).find("input[name='chkGEN']").is(":checked");

            if (_requireCheck && !_bSel) // Required check but no selected 
                return ;
            //var _acc = "2" // Test
          
            _row = control.DataTable.row(this).data(); // Get source data

            var _reason = _row.REASON;

            if ($reasonInput.length > 0) {
                _reason = $reasonInput.val();
            }

            var _acc = _row.ACC_PRINCIPLE;
            if (row.find('.custom-control.custom-checkbox').length > 0) {
                var chkDPAll = $(this).find("input[name='chkDPAll']").is(':checked');
                var chkLocal = $(this).find("input[name='chkLocal']").is(':checked');
                var chkGlobal = $(this).find("input[name='chkGlobal']").is(':checked');

                _acc = chkDPAll ? "3" : chkLocal ? "1" : chkGlobal ? "2" : "0";
            }

            var _calendar = $(this).find("input[name='Calendar2A00']").val();

            _assetList.push({
                SEL : _bSel,
                GUID: GLOBAL.GUID,
                DOC_NO : GLOBAL.DOC_NO,
                COMPANY : _row.COMPANY,
                ASSET_NO : _row.ASSET_NO,
                ASSET_SUB: _row.ASSET_SUB,
                REASON: _reason,
                //ACC_PRINCIPLE: _row.ACC_PRINCIPLE,
                ACC_PRINCIPLE: _acc,
                POSTING_DATE: _calendar,
                AMOUNT_POSTED: _row.AMOUNT_POSTED,
                PERCENTAGE: _row.PERCENTAGE,
                ADJ_TYPE: _row.ADJ_TYPE,
                IMPAIRMENT_FLAG: ($('#chkImpairment').is(':checked')) ? 'IM' :
                            ($('#chkReverseImpairment').is(':checked') ? "RI" : '')

            });
        });

        return _assetList;
    },
    GetSelectedRow: function (_LineNo) {
        var _bFoundFlag = false;
        var _selectedRow = null;
        //_Company, _AssetNo, _AssetSub, _AmountPosted, _Percent
        $("#tbSearchResult tr").each(function () {

            if (_bFoundFlag)
                return;

            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }

            var _row = [];

            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data

            if (_row.ROW_INDX != _LineNo) {
                return;
            }

            _selectedRow = this;
            _bFoundFlag = true;
        });
        return _selectedRow;
    },
    
    DetailCheck: function (sender) {
        var row = $(sender).closest("tr");
       
        $('#WFD02A00_SelectAll').prop('checked', _act = $('.chkGEN:checked').length == $('.chkGEN').length);
    },
    DPCheck: function (sender) {

        var row = $(sender).closest("tr");

        if ($(sender).attr("name") == "chkDPAll") {
            row.find('[name="chkLocal"]').prop("checked", $(sender).is(':checked'));
            row.find('[name="chkGlobal"]').prop("checked", $(sender).is(':checked'));
        }
        if ($(sender).attr("name") == "chkLocal") {
            row.find('[name="chkDPAll"]').prop("checked", false);

            if ($(sender).is(':checked'))
                row.find('[name="chkGlobal"]').prop("checked", false);
        }
        if ($(sender).attr("name") == "chkGlobal") {
            row.find('[name="chkDPAll"]').prop("checked", false);
            if($(sender).is(':checked'))
                row.find('[name="chkLocal"]').prop("checked", false);
        }

        //var _data = control.DataTable.row($(sender).parents('tr')).data();

        //if (row.find('[name="chkDPAll"]').is(":checked"))
        //    _data.ACC_PRINCIPLE = "3";
        //else if (row.find('[name="chkLocal"]').is(":checked"))
        //    _data.ACC_PRINCIPLE = "1";
        //else if (row.find('[name="chkGlobal"]').is(":checked"))
        //    _data.ACC_PRINCIPLE = "2";
        //else
        //    _data.ACC_PRINCIPLE = "0";

        //control.DataTable.row($(sender).parents('tr')).data(_data).draw(false);
    },
    EnableMassMode: function () {

    },
    Resend: function (callback) {

        var _header = fMainAction.GetParameter();

        var _list = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, { header : _header, list : _list}, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList -> Send

       
    },

    UpdateAssetListReason: function (input) {
        var $tr = $(input).closest('tr');
        var row = control.DataTable.row($tr).data();
        row.REASON = input.value;
    }
}

//Call this function when PG set screen mode
var fScreenMode = {

    
    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        $('#chkReverseImpairment, #chkImpairment').prop("disabled", true);

        $('#WFD02A00_SelectAll').prop("disabled", true);
        $('#WFD02A00_SelectAll').hide();

        if (GLOBAL.STATUS == '00') //Submit mode only
        {
            $('button.CostDialog, button.delete').prop("disabled", true);
            $('input.Remark, input.chkGEN').prop("disabled", true);
        }

    },   
    Submit_InitialMode: function (callback) {

        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd').prop("disabled", false);

        $('#chkReverseImpairment, #chkImpairment').prop("disabled", false);

        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').removeClass('disabled').prop("disabled", false);

       

        $('button.CostDialog').prop("disabled", false);
        $('input.Remark').prop("disabled", false);
        $('input.chkGEN').prop("disabled", false);
        $('button.delete').prop("disabled", false);
        $('button.delete').show();

        $('input[name="Calendar2A00"]').prop("disabled", false);

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').removeClass('disabled').prop("disabled", false);
     
        $('input[name="Calendar2A00"]').prop("disabled", true);

        $('button.CostDialog').prop("disabled", true);
        $('input.Remark').prop("disabled", true);
      
        $('button.delete').show();

        if (callback == null)
            return;
        callback();
    },
    Approve_Mode : function (callback) {

        console.log("#_ClearMode");
        fScreenMode._ClearMode();
        


        $('#btnRequestExport').show();
        $('#btnRequestExport').prop("disabled", false);

        

        if (callback == null)
            return;
        callback();
        return;

    }
}

var fRender = {
    CheckBoxRender : function (data) {
        var _strHtml = '';

        var _disabled = (data.AllowCheck ? '' : "disabled");


       
        _strHtml = '<input style="width:20px; height:28px;" name="chkGEN" class="chkGEN" value="' + data.ASSET_NO + '#' + data.ASSET_SUB + '" onclick="fAction.DetailCheck(this);" type="checkbox" ' + _disabled +  ' >';
        return _strHtml;
    },
    
    HyperLinkRender: function (data) {
      //  console.log("Load Detail");
        if (data.ASSET_NO == null)
            return '';
        return '<a href="../WFD02130/Index?COMPANY='+data.COMPANY+'&ASSET_NO='+data.ASSET_NO+'&ASSET_SUB='+data.ASSET_SUB+'" target="_blank">' + data.ASSET_NO + '</a>';
    },
    DPCheckBoxRender: function (data) {
        var _str = '';

        var _disabled = (data.AllowEdit ? '' : "disabled");

        var _local = "";
        var _global = "";
        var _all = "";
        if (data.ACC_PRINCIPLE == "1" || data.ACC_PRINCIPLE == "3")
            _local = " checked ";
        if (data.ACC_PRINCIPLE == "2" || data.ACC_PRINCIPLE == "3")
            _global = " checked ";
        if ( data.ACC_PRINCIPLE == "3")
            _all = " checked ";

       
            
        var _strChecked = '';
        _str = '<div class="custom-control custom-checkbox form-group"> ' +
                '   <input type="checkbox" class="custom-control-input" id="chkDPAll" onclick="fAction.DPCheck(this);" name="chkDPAll" ' + _disabled + _all + ' >' +
                '   <label class="custom-control-label" for="chkDPAll">All</label>' +
                '   <input type="checkbox" class="custom-control-input" id="chkLocal" onclick="fAction.DPCheck(this);" name="chkLocal"' + _disabled + _local  + ' >' +
                '   <label class="custom-control-label" for="chkLocal">Local</label>' +
                '   <input type="checkbox" class="custom-control-input" id="chkGlobal" onclick="fAction.DPCheck(this);" name="chkGlobal" ' + _disabled + _global  + ' >' +
                '   <label class="custom-control-label" for="chkGlobal">Global</label>' +
                '</div>';

        return _str;
    },

    CalendarRender: function (data) {
        var _disabled = (data.AllowEdit ? '' : "disabled");

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;

        return '<div class="input-group date">' +
            '<input style="width:85px;" type="text" id="Calendar2A00_' + _id + '" name="Calendar2A00" class="form-control datepicker" ' + _disabled + ' value="' + (data.POSTING_DATE == null ? '' : data.POSTING_DATE) + '" />' +
            '<span class="input-group-btn" style="cursor:pointer;">' +
            '<button class="btn" id="Calendar2A00_' + _id + '_BTN" onclick="ClickIconDate(\'Calendar2A00_' + _id + '\')" >' +
            '<i class="fa fa-calendar"></i>' +
            '               </button>' +
            '</span>' +
            '</div>';
    },
    
    TextBoxRender: function (data) {

        var reason = (data.REASON == null ? "" : data.REASON);
        var _disabled = "";
        var tooltip = "";
        var _class = "form-control Remark";

        if (!data.AllowEdit) {
            _disabled = "disabled";
            tooltip = 'data-toggle="tooltip" data-placement="top" title="" data-original-title="' + reason + '"';
        } else {
            _class += " require";
        }

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<input type="text" id="txtReason_' + _id + '" name="txtReason" onblur="fAction.UpdateAssetListReason(this)" class="' + _class +'" maxlength="50" value="' + reason + '" ' + _disabled + ' />'
    },
    ButtonRender : function (data) {
     
        var _text = ' ... ';
        if (data.TOTALAMOUNT != null) {
            _text = currencyFormat(data.TOTALAMOUNT);
        }
        
        var _disabled = (data.AllowEdit ? "" : "disabled");

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<input type="hidden" id="hdOriginal_"' + _id + '" name="hdOriginal" value="' + data.BF_CUMU_ACQU_PRDCOST_01 + '" />' +
                '<input type="hidden" id="hdPercent_"' + _id + '" name="hdPercent" value="' + data.PERCENTAGE + '" />' +
                '<input type="hidden" id="hdAmount_"' + _id + '" name="hdAmount" value="' + _text + '"/>' +
                '<button type="button" id="btnAmountPosted_' + _id + '" name="btnAmountPosted" class="CostDialog btn btn-info" ' + _disabled + 
                    ' onclick="fPopup.ShowCostDialog(this);return false;">' + _text + '</button>';
                
                //,\'' + data.COMPANY + '\',\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\'
    },
    DeleteButtonRender : function (data) {

      var _hide = (data.AllowDelete ? '' : 'hide');
      var _disbled = (PERMISSION.AllowEditBeforeGenFlow ? '' : "disabled");

        var _strHtml = '';
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="btnDelete_"' + data.ASSET_NO +
            ' class="btn btn-danger btn-xs delete ' + _hide + ' " data-title="Delete" type="button" ' + _disbled + ' ' +
                    'onclick=fAction.DeleteAsset(\'' +
                    data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\')>' +
                    '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },  
}
var fPopup = {
    ShowCostDialog: function (button) {
        control.SelectedRow = control.DataTable.row($(button).parents('tr')) ;
        var row = control.DataTable.row($(button).parents('tr')).data();

        var _format = "{0} Asset Sub : {1}".format(row.ASSET_NO, row.ASSET_SUB);
        $('#lblAssetNoOnCostdialog').text(_format);

        var imFlag = $('#chkImpairment').is(':checked');

        var totalAmount = row.TOTALAMOUNT;
        var totalNBV = row.TOTALNETBOOK;
        if (imFlag) {
            $('#lblCostValueOriginal').text("Value");
            $('#lblDescription').text("Value = NBV - Scrap Value");
           // totalAmount = totalAmount - 1;
                totalNBV = totalNBV - row.BF_SCRAP;
        } else {
            $('#lblCostValueOriginal').text("Net Value");
            $('#lblDescription').text("");
        }

        //Get Cost Info
        //console.log(row);
        control.CostPopup = {
            LINE_NO: row.ROW_INDX,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: row.COMPANY,
            PERCENTAGE: row.PERCENTAGE,
            AMOUNT_POSTED: totalAmount,
            TOTALNETBOOK: row.TOTALNETBOOK,
            //2020.01.27
            COST_VALUE: totalNBV,
            //COST_VALUE: row.TOTALNETBOOK,
            ADJ_TYPE: row.ADJ_TYPE,
            IM_FLAG: imFlag,
            //TOTAL_AMOUNT: currencyFormat(row.TOTALCOST),
            TOTAL_AMOUNT: currencyFormat(totalNBV),
            $lblCost : $('#lblCost'),
            $lblCostTxt : $('#lblCostTxt')
        };
        //2020.01.27
        //$("#totalAmount").val(currencyFormat(row.TOTALNETBOOK));
        $("#totalAmount").val(currencyFormat(totalNBV));

        if (control.CostPopup.IM_FLAG) {
            control.CostPopup.$lblCost.text('Remain Cost :');
        } else {
            control.CostPopup.$lblCost.text('New Cost :');
        }
        
        control.CostPopup.$lblCostTxt.text(currencyFormat(totalAmount));
        //control.CostPopup.$lblCostTxt.text(currencyFormat(row.TOTALNETBOOK));

        $("#AlertMessageWFD02A00").html('');
        // #Clear Data
        $('#txtCostValueOriginal, #txtCostAmount, #txtCostAll, #txtCostPercentage,#txtAmountByPercentage').val('');

        $('#chkAmount, #chkAll, #chkPercentage').prop('disabled', false)
                                                .prop('checked', false);

        $('#divCostDialog input').prop('disabled', true);
        $("#txtCostAmount,#txtCostAll").autoNumeric('init', { aSep: ',', vMin: '0.00', vMax: '9999999999.99' });
        //$("#txtCostPercentage").autoNumeric('init', { vMin: '0.01', vMax: '100.00' });

        //Lock checkbox for editing and view mode
        $('#divCostDialog input:checkbox').prop('disabled', !row.AllowEdit);

        // Binging
        //if (control.CostPopup.PERCENTAGE > 0) {
        if (control.CostPopup.ADJ_TYPE == 'P') {
            $('#chkPercentage').prop('checked', true);

            $('#txtCostPercentage').prop('disabled', false);
            $('#txtCostPercentage').val(control.CostPopup.PERCENTAGE.toFixed(2));

            $('#txtAmountByPercentage').val(currencyFormat(control.CostPopup.AMOUNT_POSTED));

            reCalcRemainCost($('#txtAmountByPercentage').val());
        }
        else if (control.CostPopup.ADJ_TYPE == 'A') { //if (control.CostPopup.AMOUNT_POSTED == control.CostPopup.COST_VALUE) {

            $('#chkAll').prop('checked', true);
            $('#txtCostAll').val(currencyFormat(control.CostPopup.AMOUNT_POSTED));

            reCalcRemainCost($('#txtCostAll').val());

        }
        else if (control.CostPopup.ADJ_TYPE == 'M') { //if (control.CostPopup.AMOUNT_POSTED > 0) {

            $('#chkAmount').prop('checked', true);

            $('#txtCostAmount').prop('disabled', false);
            $('#txtCostAmount').val(currencyFormat(control.CostPopup.AMOUNT_POSTED));

            reCalcRemainCost($('#txtCostAmount').val());
        } else {
            if (parseFloat(control.CostPopup.COST_VALUE) === 0) {
                $('#chkAll, #chkPercentage').prop('disabled', true);
            }
        }

        $('#txtCostValueOriginal').val(currencyFormat(control.CostPopup.COST_VALUE));

        $('#txtCostPercentage').off('keypress').on('keypress', function (e) {
            return isNumberKey(e);
        });

        $('#divCostDialog').modal();
    },
    UpdateAmountOnRow: function (callback) {

        var _selectedRow = control.SelectedRow;
        if (_selectedRow == null)
            return;

        var _obj = control.DataTable.row(_selectedRow).data();
        _obj.PERCENTAGE = null;
        _obj.AMOUNT_POSTED = null;
        _obj.ADJ_TYPE = null;
        if ($('#divCostDialog #chkAll').is(':checked')) {
            _obj.AMOUNT_POSTED = parseFloatWithComma($('#txtCostAll').val());
            _obj.ADJ_TYPE = 'A';
        }
        if ($('#divCostDialog #chkAmount').is(':checked')) {
            _obj.AMOUNT_POSTED = parseFloatWithComma($('#txtCostAmount').val());
            _obj.ADJ_TYPE = 'M';
        }        
        if ($('#divCostDialog #chkPercentage').is(':checked')) {
            _obj.PERCENTAGE = parseFloat($('#txtCostPercentage').val());
            _obj.AMOUNT_POSTED = parseFloatWithComma($('#txtAmountByPercentage').val());
            _obj.ADJ_TYPE = 'P';
        }
       
        //control.DataTable.row(_selectedRow).data(_obj).draw(false);

        var _amount = 0;

        var _data = {
            GUID: GLOBAL.GUID,
            ASSET_NO: _obj.ASSET_NO,
            ASSET_SUB: _obj.ASSET_SUB,
            COMPANY: GLOBAL.COMPANY,
            PERCENTAGE: _obj.PERCENTAGE,
            AMOUNT_POSTED: _obj.AMOUNT_POSTED,
            ADJ_TYPE: _obj.ADJ_TYPE,
            IMPAIRMENT_FLAG: ($('#chkImpairment').is(':checked')) ? 'IM' :
                            ($('#chkReverseImpairment').is(':checked') ? "RI" : ''),
            TOTALCOST: _obj.TOTALNETBOOK
        }
        console.log(_data);

        ajax_method.Post(Url.UpdateAmount, { data: _data }, false, function (result) {
            //debugger;
            if (result == null || result.IsError) {
                return;
            }

            fPopup.selectedRow = null;
            fPopup.selectedCol = null;

            fAction.UpdateAsset(function () {
                //Update Row
                fAction.LoadData();
            });

            if (callback == null)
                return;

            callback();
        }); //ajax_method.UpdateCost

        //if (callback != null)
        //    callback();
    },
    CalculateAmountByPercent : function(){
        try{
            // var _amount = parseFloatWithComma($('#txtCostValueOriginal').val()) * parseFloat($('#txtCostPercentage').val()) / 100;
          
              //  TOTALNETBOOK: row.TOTALNETBOOK,
            if (parseFloatWithComma($('#txtCostPercentage').val()) == 100) { //txtCostValueOriginal = TOTALNETBOOK - scrap
                var _amount = parseFloatWithComma($('#txtCostValueOriginal').val()) * parseFloat($('#txtCostPercentage').val()) / 100;
                $('#txtAmountByPercentage').val(currencyFormat(_amount));
            } else {
                var _amount = parseFloatWithComma(control.CostPopup.TOTALNETBOOK) * parseFloat($('#txtCostPercentage').val()) / 100;
                $('#txtAmountByPercentage').val(currencyFormat(_amount));
            }

           //control.CostPopup.TOTALNETBOOK
            reCalcRemainCost($('#txtAmountByPercentage').val());
        }
        catch(err) {
            $('#txtAmountByPercentage').val('');
            console.error(err);
        }

        checkEnabledOKButton();
    },
    ClearValue : function(){
        $('#txtCostAmount, #txtCostAll, #txtCostPercentage, #txtAmountByPercentage').val('');
    }
    
}
$('#WFD02A00_SelectAll').click(function(e){
    $('.chkGEN:enabled').prop('checked', $(this).is(':checked'));
})
// Popup Control Event
$('#divCostDialog #chkAll').click(function (e) {

    if ($(this).is(':disabled'))
        return;
    $('#chkAmount, #chkPercentage').prop('checked', false);
    $('#txtCostAmount, #txtCostPercentage').prop('disabled', true);

    fPopup.ClearValue();

    if ($('#chkAll').is(':checked')) {
        //$('#txtCostAll').prop('disabled', false);

        if ($('#txtCostAll').val() == "") {
            $('#txtCostAll').val($('#totalAmount').val());
        }
    }

    reCalcRemainCost($('#txtCostAll').val());

    checkEnabledOKButton();
})
$('#divCostDialog #chkAmount').click(function (e) {
  if ($(this).is(':disabled'))
        return;
    $('#chkAll, #chkPercentage').prop('checked', false);
    $('#txtCostAmount, #txtCostPercentage').prop('disabled', true);

    fPopup.ClearValue();

    if ($('#chkAmount').is(':checked')) {
        $('#txtCostAmount').prop('disabled', false);

        if ($('#txtCostAmount').val() == "") {
            $('#txtCostAmount').val($('#txtCostValueOriginal').val());
        }
    }
    reCalcRemainCost($('#txtCostAmount').val());

    checkEnabledOKButton();
})
$('#divCostDialog #chkPercentage').click(function (e) {
    if ($(this).is(':disabled'))
        return;

    $('#chkAll, #chkAmount').prop('checked', false);
    $('#txtCostAmount, #txtCostPercentage').prop('disabled', true);
    
    fPopup.ClearValue();
    
    if ($('#chkPercentage').is(':checked')) {
        $('#txtCostPercentage').prop('disabled', false);

        if ($('#txtCostAmount').val() == "") {
            $('#txtCostPercentage').val('100.00');
            fPopup.CalculateAmountByPercent();
        }
    }

    checkEnabledOKButton();
})

$('#txtCostAmount').blur(function () {
    reCalcRemainCost($('#txtCostAmount').val());
});
$("#divCostDialog #txtCostPercentage").blur(function () {
    fPopup.CalculateAmountByPercent();
});


$('#divCostDialog #btnCostDialogOK').click(function(e) {
    ClearMessageC();

    var postedAmount = 0;
    var TotalCost = $('#txtCostValueOriginal').val();
    // # Validation
    
    if ($('#divCostDialog input:checked').length == 0) {
        AlertTextErrorMessagePopup('MCOM0017AERR : Please select one of choice', "#AlertMessageWFD02A00");
        return;
    }
    // check All
    if ($('#divCostDialog #chkAll').is(":checked")) {
        if ($('#txtCostAll').val() == "") {
            var _msg = Message.ShouldNotEmpty.replace('{0}', 'Amount');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02A00");
            $('#txtCostAll').focus();
            return;
        }

        if (parseFloatWithComma($('#txtCostAll').val()) > parseFloatWithComma($('#txtCostValueOriginal').val())) {
            var _msg = Message.Lessthan.replace('{0}', 'Amount').replace('{1}', $('#txtCostValueOriginal').val());
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02A00");
            $('#txtCostAll').focus();
            return;
        }

        postedAmount = $('#txtCostAll').val();
    }

    if ($('#divCostDialog #chkAmount').is(":checked")) {
        if ($('#txtCostAmount').val() == "") {
            var _msg = Message.ShouldNotEmpty.replace('{0}', 'Amount');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02A00");
            $('#txtCostAmount').focus();
            return;
        }

        if (control.CostPopup.IM_FLAG) {
            if (parseFloatWithComma($('#txtCostAmount').val()) > parseFloatWithComma($('#txtCostValueOriginal').val())) {
                var _msg = Message.Lessthan.replace('{0}', 'Amount').replace('{1}', $('#txtCostValueOriginal').val());
                AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02A00");
                $('#txtCostAmount').focus();
                return;
            }
        } else {
            if (parseFloatWithComma(control.CostPopup.COST_VALUE) === 0 && parseFloatWithComma($('#txtCostAmount').val()) === 0) {
                AlertTextErrorMessagePopup('MCOM0017AERR : Amount should greater than 0', "#AlertMessageWFD02A00");
                $('#txtCostAmount').focus();
                return;
            }
        }

        postedAmount = $('#txtCostAmount').val();
    }

    if ($('#divCostDialog #chkPercentage').is(":checked")) {
        if ($('#txtCostPercentage').val() == "") {
            var _msg = Message.ShouldNotEmpty.replace('{0}', 'Percent');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02A00");
            $('#txtCostPercentage').focus();
            return;
        }
        if (parseFloat($('#txtCostPercentage').val()) < 1 ||
            parseFloat($('#txtCostPercentage').val()) > 100) {
            var _msg = Message.Between.replace('{0}', 'Percent').replace('{1}', '1').replace('{2}', '100');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02A00");
            $('#txtCostPercentage').focus();
            return;
        }
        postedAmount = $('#txtCostAmountByPercentage').val();
    }

    fPopup.UpdateAmountOnRow(function () {
        $('#divCostDialog').modal('hide');
    });
});

$('#chkReverseImpairment').click(function (e) {
    if ($(this).is(':checked')) {
        $('#chkImpairment').prop('checked', false);
    }
})
$('#chkImpairment').click(function (e) {

    if ($(this).is(':checked')) {
        $('#chkReverseImpairment').prop('checked', false);
    }
})


function _GetCost(){
       
    if ($('#chkAll').is(":checked")) {
        return $('#txtCostAll').val();
    }
    if ($('#chkAmount').is(":checked")) {
        return $('#txtCostAmount').val();
    }
    if ($('#chkPercentage').is(":checked")) {
        return $('#txtAmountByPercentage').val();
    }

    return 0;
}

function _GetAdjType() {
    if ($('#chkAll').is(":checked")) {
        return 'A';
    }
    if ($('#chkAmount').is(":checked")) {
        return 'M';
    }
    if ($('#chkPercentage').is(":checked")) {
        return 'P';
    }
};

function checkEnabledOKButton() {

    var adjType = _GetAdjType();
    var amontCost = _GetCost();
    var costValueOriginal = $('#txtCostValueOriginal').val();

    var numCostValueOriginal = parseFloatWithComma(costValueOriginal);
    var numAmountCost = parseFloatWithComma(amontCost);

    if (control.CostPopup.IM_FLAG) {
        if (numCostValueOriginal >= numAmountCost) {
            $("#AlertMessageWFD02A00").html('');
            $("#btnCostDialogOK").prop('disabled', false);
        } else {
            AlertTextErrorMessagePopup('MCOM0017AERR : Amount greater than Cost Value', "#AlertMessageWFD02A00");
            $("#btnCostDialogOK").prop('disabled', true);
        }
    }
}

function reCalcRemainCost(cost) {
    cost = cost === '' ? 0 : cost;
    var fCost = parseFloatWithComma(cost);
    var fAmount = parseFloatWithComma(control.CostPopup.TOTAL_AMOUNT);
    var costText = '';

    if (control.CostPopup.IM_FLAG) {
        costText = currencyFormat(fAmount - fCost);
    } else {
        costText = currencyFormat(fAmount + fCost);
    }

    control.CostPopup.$lblCostTxt.text(costText);
    //$('#lblCostTxt').text(currencyFormat(parseFloatWithComma($('#totalAmount').val()) - parseFloatWithComma((cost == '' ? 0: cost))));
}