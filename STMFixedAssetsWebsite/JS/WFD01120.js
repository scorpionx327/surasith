﻿//Declare variables
var Ctrl = {
    ReportType: $('#cbbTypeOfReport'),
    ReportName: $('#cbbReportName'),
    PeriodFrom: $('#datePeriodFrom'),
    PeriodTo: $('#datePeriodTo'),
    ReportNo: $('#txtReportNo'),
    Print: $('#WFD01120Print'),
    Search: $('#WFD01120Search'),
    CostCenter: $('#cbbCostCenter'),
    PanelPeriodDate: $('#divPeriod').find("[name=datePeriod]"),
    PanelReportName: $('#divReportName'),
    PanelPeriod: $('#divPeriod'),
    LabelReportNo: $('label[for="cbbReportNo"]'),
    PanelReportNo: $('#divForReportNo'),
    PanelCostCenter: $('#divPeriod').find("[name=CostCenter]"),
    PanelPrint: $('#divPrint'),
    PanelSearch: $('#divSearch'),
    Pagin: $('#tableGeneralReport').Pagin(URL_CONST.ITEM_PER_PAGE),
    DataTable: null,
    SearchTable: $('#tableGeneralReport'),
    SearchPanel: $('#divReportType'),
    ButtonPanel: $('#divButton')
};

//Event function-------------------------------------------------------
$(function () {
    setInitialBatch();
    Initial();
    Ctrl.ReportType.Select2Require(true);

    //Add
    $('#txtReportNo').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            Ctrl.Search.click();
            return false;
        }
    });

});

var batchProcessLFD02150 = BatchProcess({
    BatchId: 'LFD02150', // 'LFD02150' 
    Description: 'Asset number list with photo report'
});
var batchProcessLFD02420 = BatchProcess({
    BatchId: "LFD02420", // Transfer & Dispose Report
    Description: "Export Transfer Detail Report", //
    UserId: URL_CONST.EmployeeCode

});
var batchProcessLFD02520 = BatchProcess({
    BatchId: "LFD02520", // Transfer & Dispose Report select 2
    Description: "Export  Dispose Detail Report", //
    UserId: URL_CONST.EmployeeCode

});
var batchProcessLFD02660 = BatchProcess({
    BatchId: "LFD02660",
    Description: "Print out FA update scan status(countuncount) withwithout cover page report",
    UserId: URL_CONST.EmployeeCode

});
var batchProcessLFD02170 = BatchProcess({
    BatchId: "LFD02170",
    Description: "Print out visualize Asset location map report",
    UserId: URL_CONST.EmployeeCode

});
var batchProcessLFD02230 = BatchProcess({
    BatchId: "LFD02230",
    Description: "LFD02230_SummaryCloseProject",
    UserId: URL_CONST.EmployeeCode
});
var batchProcessLFD02440 = BatchProcess({
    BatchId: "LFD02440",
    Description: "LFD02440_SummaryFATransferList",
    UserId: URL_CONST.EmployeeCode
});
var batchProcessLFD02540 = BatchProcess({
    BatchId: "LFD02540",
    Description: "LFD02540_SummaryFAdisposeListByPeriod",
    UserId: URL_CONST.EmployeeCode
});
var setInitialBatch = function () {

    batchProcessLFD02150.Initial();
    batchProcessLFD02520.Initial();
    batchProcessLFD02420.Initial();
    batchProcessLFD02660.Initial();
    batchProcessLFD02170.Initial();

    batchProcessLFD02230.Initial();
    batchProcessLFD02440.Initial();
    batchProcessLFD02540.Initial();
}

var ClearInitialBatch = function () {

    batchProcessLFD02150.Clear();
    batchProcessLFD02520.Clear();
    batchProcessLFD02420.Clear();
    batchProcessLFD02660.Clear();
    batchProcessLFD02170.Clear();
    batchProcessLFD02230.Clear();
    batchProcessLFD02440.Clear();
    batchProcessLFD02540.Clear();
}


//Initial
var Initial = function () {
    WFD01120GetReportType();
    //Bind Combobox Report Type
    //$.ajax({
    //    type: "POST",
    //    contentType: "application/json; charset=utf-8",
    //    url: URL_CONST.ReportTypeURL,
    //    dataType: "json",
    //    data: null,
    //    success: function (data) {
    //        Ctrl.ReportType.empty();
    //        $.each(data.data.ListReportType, function (key, value) {               
    //            if (value.Selected) {
    //                Ctrl.ReportType.append($("<option selected ></option>").val(value.Value).html(value.Text));
    //            } else {
    //                Ctrl.ReportType.append($("<option></option>").val(value.Value).html(value.Text));
    //            }
    //        });           
    //    }
    //});

    Ctrl.ReportType.change(ReportTypeChanged);
    Ctrl.ReportName.change(ReportNameChanged);

    Ctrl.PanelReportName.hide();
    Ctrl.PanelPeriod.hide();
    Ctrl.PanelCostCenter.hide();
    Ctrl.SearchPanel.hide();
    Ctrl.ReportName.Select2Require(true);
    Ctrl.ButtonPanel.hide();


}



$('#WFD01120Print').click(function (e) {

    con = null;
    //ReportTypeCode = 1 ('Summary report')
    if (Ctrl.ReportType.val() == '1') {
        con = GetParamsSummaryReport();
    }

    //ReportTypeCode = 2 ('Change request detail report')
    if (Ctrl.ReportType.val() == '2') {
        con = GetParamsValidateChangeRequestDetailReport(); //GetParamsChangeRequestDetailReport();
    }

    //ReportTypeCode = 3 ('Fixed Asset detail report')
    if (Ctrl.ReportType.val() == '3') {
        con = GetParamsFixedAssetDetailReport();
    }

    ajax_method.Post(URL_CONST.PrintURL, con, true, function (result, pagin) {
        if (result != null) {
            Print();
        }
    }, null);

});

//Report Type Event
var ReportTypeChanged = function () {

    //ReportTypeCode = 1 ('Summary report')
    if (Ctrl.ReportType.val() == '1') {
        ClearSummaryReport();
        ViewCriteriaSummary();
        Ctrl.PeriodFrom.addClass('require');
        Ctrl.PeriodTo.addClass('require');
    }

    //ReportTypeCode = 2 ('Change request detail report')
    if (Ctrl.ReportType.val() == '2') {
        ClearChangeRequestDetailReport();
        ViewCriteriaChangeRequest();
        Ctrl.PeriodFrom.removeClass('require');
        Ctrl.PeriodTo.removeClass('require');
    }

    //ReportTypeCode = 3 ('Fixed Asset detail report')
    if (Ctrl.ReportType.val() == '3') {
        ClearFixedAssetDetailReport();
        ViewCriteriaFADetail();
        Ctrl.PeriodFrom.removeClass('require');
        Ctrl.PeriodTo.removeClass('require');

        //$("#cbbCostCenter").val($("#cbbCostCenter option:first").val());
    }

    //ReportTypeCode = '' ('Default Screen')
    if (Ctrl.ReportType.val() == '') {
        ClearSummaryReport();
        ClearChangeRequestDetailReport();
        ClearFixedAssetDetailReport();

        Ctrl.PanelReportName.hide();
        Ctrl.PanelPeriod.hide();
        Ctrl.ButtonPanel.hide();
        Ctrl.PeriodFrom.removeClass('require');
        Ctrl.PeriodTo.removeClass('require');
    }
    Ctrl.ReportName.val('').change();
}


//Report Name Event
var ReportNameChanged = function () {

    //ReportTypeCode = 2 ('Change request detail report')
    if (Ctrl.ReportType.val() == '2') {
        ClearChangeRequestDetailReport();
        Ctrl.PanelPrint.hide();
        Ctrl.PanelSearch.show();

        if ($.fn.DataTable.isDataTable(Ctrl.SearchTable)) {
            var table = Ctrl.SearchTable.DataTable();
            table.destroy();
        }

        Ctrl.SearchPanel.hide();
        Ctrl.Pagin.Clear();
    }

    if (Ctrl.ReportType.val() == '3') {
        if (Ctrl.ReportName.val() == '3') {
            Ctrl.PanelCostCenter.hide();
        } else {
            Ctrl.PanelCostCenter.show();
        }
    }
}

//Search button Event
var Search = function (followPageNo) {

    var con = ParamsSearchChangeRequestDetailReport();
    var pagin = Ctrl.Pagin.GetPaginData();

    if (typeof(followPageNo) !== 'boolean') {
        pagin.CurrentPage = 1;
    }

    ajax_method.SearchPost(URL_CONST.SearchURL, con, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = "asc";

            if ($.fn.DataTable.isDataTable(Ctrl.SearchTable)) {
                var table = Ctrl.SearchTable.DataTable();
                table.destroy();
            }

            Ctrl.DataTable = Ctrl.SearchTable.DataTable({
                data: datas,
                "columns": [
                  { "data": null },
                  { "data": "COMPANY" },
                  { "data": "REPORT_TYPE" },
                  { "data": "REPORT_NO" },
                  { "data": "COMPLETE_DATE", "className": "text-center" },
                  { "data": "REQUEST_CODE" }
                ],
                "columnDefs": [
                    {
                        "targets": 0,
                        "searchable": false,
                        "orderable": false,
                        "className": "text-center",
                        "render": function (data, type, full, meta) {
                            return '<input type="checkbox" style="margin-left: -8px;"  name="CheckBoxItem" onclick="detailItemCheck(this);"/>';
                        }
                    }, {
                        "targets": 1,
                        "className": "text-center"
                    },{
                        "targets": 3,
                        "className": "text-center"
                    },
                    //{
                    //    "targets": 3,
                    //    "className": "text-center",
                    //    "render": function (data, type, full, meta) {
                    //        return dtConvFromJSON(data);
                    //    }
                    //},
                    {
                        targets: 5,
                        "visible": false
                    }
                ],
                //"order": [pagin.OrderColIndex, pagin.OrderType],
                //"paging": false,
                'order': [],
                "searching": false,
                "paging": false,
                "retrieve": true,
                "bInfo": false,
                "autoWidth": false
            });
            //var page = Ctrl.SearchTable.Pagin();
            //page.Init(pagin, sorting, changePage, "itemPerPage");
            Ctrl.Pagin.Init(pagin, sorting, changePage, "itemPerPage");
            Ctrl.SearchPanel.show();
            Ctrl.PanelPrint.show();
        } else {
            Ctrl.SearchPanel.hide();
            Ctrl.Pagin.Clear();
            Ctrl.PanelPrint.hide();
        }
    }, null);
    $('#selectAll').attr("checked", false);
}
//Print button Event
var Print = function () {

    //Summary
    if (Ctrl.ReportType.val() == '1') {
        if (Ctrl.ReportName.val() == "03")
            WFD01120Print_SummaryReport("LFD02230", "LFD02230_SummaryCloseProject");
        else if (Ctrl.ReportName.val() == "01")
            WFD01120Print_SummaryReport("LFD02440", "LFD02440_SummaryFATransferList");
        else if (Ctrl.ReportName.val() == "02")
            WFD01120Print_SummaryReport("LFD02540", "LFD02540_SummaryFAdisposeListByPeriod");
    }
        // Transfer & Dispose Request
    else if (Ctrl.ReportType.val() == '2') {
        SelectedList = GetParamsChangeRequestDetailReport();

        if (Ctrl.ReportName.val() == "01")
        {
             WFD01120Print_DetailRequestReportTranfer(SelectedList); //--1 
        }
        if (Ctrl.ReportName.val() == "02") {
            WFD01120Print_DetailRequestReportDisPose(SelectedList); //--1 
        }
    }

    else if (Ctrl.ReportType.val() == '3') {
        if (Ctrl.ReportName.val() == "01")
            WFD01120Print_FixedAssetPhotoReport(Ctrl.CostCenter.val());
        else if (Ctrl.ReportName.val() == "02")
            WFD01120Print_FixedAssetMapReport(Ctrl.CostCenter.val());
        else if (Ctrl.ReportName.val() == "03")
            WFD01120Print_StockTakingReport(Ctrl.CostCenter.val());
    }
    

}
// Callback function after click soirting column in table
var sorting = function (PaginData) {
    console.log(PaginData)
    $('#selectAll').attr("checked", false);
    
    Search(true);
}
// Callback function after change pagination on table
var changePage = function (PaginData) {
    $('#selectAll').attr("checked", false);
    Search(true);
}
// Check all row checkbox
var selectAllItem = function (sender) {
    var chk = $(sender).is(':checked');
    Ctrl.SearchTable.find('[name="CheckBoxItem"]').each(function (e) {
        $(this).prop('checked', chk);
    });
}
var detailItemCheck = function (sender) {

    var numberOfChecked = $('[name="CheckBoxItem"]:checked').length;
    var totalCheckboxes = $('[name="CheckBoxItem"]').length;

    $('#selectAll').prop('checked', totalCheckboxes > 0 && totalCheckboxes == numberOfChecked);

}

//Other methods
var ViewCriteriaSummary = function () {

    Ctrl.PanelReportName.show();
    Ctrl.PanelPeriod.show();
    Ctrl.ButtonPanel.show();
    Ctrl.PanelPeriodDate.show();
    Ctrl.PanelCostCenter.hide();
    Ctrl.Search.hide();
    Ctrl.LabelReportNo.hide();
    Ctrl.ReportNo.hide();
    Ctrl.PanelPrint.show();
    Ctrl.PanelSearch.hide();
    Ctrl.SearchPanel.hide();

    var params = JSON.stringify({ data: Ctrl.ReportType.val() });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: URL_CONST.ReportNameURL,
        dataType: "json",
        data: params,
        success: function (data) {
            Ctrl.ReportName.empty();
            Ctrl.ReportName.append($('<option value="">' + "&nbsp;" + '</option>'));
            $.each(data.data.ListReportType, function (key, value) {
                if (value.Selected) {
                    Ctrl.ReportName.append($("<option selected ></option>").val(value.Value).html(value.Text));
                } else {
                    Ctrl.ReportName.append($("<option></option>").val(value.Value).html(value.Text));
                }
            });
        }
    });
}
var ViewCriteriaChangeRequest = function () {

    Ctrl.PanelReportName.show();
    Ctrl.PanelPeriod.show();
    Ctrl.ButtonPanel.show();
    Ctrl.PanelPeriodDate.show();
    Ctrl.PanelCostCenter.hide();
    Ctrl.Search.show();
    Ctrl.LabelReportNo.show();
    Ctrl.PanelReportNo.show();
    Ctrl.ReportNo.show();
    Ctrl.PanelPrint.hide();
    Ctrl.PanelSearch.show();
    //Ctrl.Search.click(Search);

    Ctrl.Search.click(function (e) {
        //$('#txtReportNo').val(GetSearchCriteria($('#txtReportNo')));
        Search();
    });


    var params = JSON.stringify({ data: Ctrl.ReportType.val() });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: URL_CONST.ReportNameURL,
        dataType: "json",
        data: params,
        success: function (data) {
            Ctrl.ReportName.empty();
            Ctrl.ReportName.append($('<option value="">' + "&nbsp;" + '</option>'));
            $.each(data.data.ListReportType, function (key, value) {
                if (value.Selected) {
                    Ctrl.ReportName.append($("<option selected ></option>").val(value.Value).html(value.Text));
                } else {
                    Ctrl.ReportName.append($("<option></option>").val(value.Value).html(value.Text));
                }
            });
        }
    });
}
var ViewCriteriaFADetail = function () {
    Ctrl.PanelReportName.show();
    Ctrl.PanelPeriod.show();
    Ctrl.ButtonPanel.show();
    Ctrl.PanelPeriodDate.hide();
    Ctrl.PanelCostCenter.show();
    Ctrl.PanelPrint.show();
    Ctrl.Search.hide();
    Ctrl.LabelReportNo.hide();
    Ctrl.PanelReportNo.hide();
    Ctrl.ReportNo.hide();
    Ctrl.PanelSearch.hide();

    var params = JSON.stringify({ data: Ctrl.ReportType.val() });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: URL_CONST.ReportNameURL,
        dataType: "json",
        data: params,
        success: function (data) {
            Ctrl.ReportName.empty();
            Ctrl.ReportName.append($('<option value="">' + "&nbsp;" + '</option>'));
            $.each(data.data.ListReportType, function (key, value) {
                if (value.Selected) {
                    Ctrl.ReportName.append($("<option selected ></option>").val(value.Value).html(value.Text));
                } else {
                    Ctrl.ReportName.append($("<option></option>").val(value.Value).html(value.Text));
                }
            });
        }
    });

    var params = JSON.stringify({ data: null });
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: URL_CONST.CostCenterURL,
        dataType: "json",
        data: params,
        success: function (data) {
            Ctrl.CostCenter.empty();
            if (data.data.ListCostCenter.length == 1) {
                $.each(data.data.ListCostCenter, function (key, value) {
                    Ctrl.CostCenter.append($("<option selected></option>").val(value.Value).html(value.Text).change());
                    Ctrl.CostCenter.val(value.Value).change();
               });
            } else {
                Ctrl.CostCenter.append($('<option value="">' + "&nbsp;" + '</option>'));
                $.each(data.data.ListCostCenter, function (key, value) {
                    if (value.Selected) {
                        Ctrl.CostCenter.append($("<option selected ></option>").val(value.Value).html(value.Text));
                    } else {
                        Ctrl.CostCenter.append($("<option></option>").val(value.Value).html(value.Text));
                    }
                });

            }
        }
    });
}

var GetParamsSummaryReport = function () {
    return {
        REPORT_TYPE: 1,
        REQUEST_TYPE: null,
        DOC_NO: Ctrl.ReportNo.val(),
        REPORT_NAME: Ctrl.ReportName.val(),
        PERIOD_FROM: Ctrl.PeriodFrom.val(),
        PERIOD_TO: Ctrl.PeriodTo.val(),
        EMP_CODE: URL_CONST.EmployeeCode,
        SELECTED: null,
        COMPANY: getCompanyValue()
    }
}

var GetParamsValidateChangeRequestDetailReport = function () {
    var datas = [];
    datas = GetParamsChangeRequestDetailReport();
    return {
        REPORT_TYPE: 2,
        REQUEST_TYPE: null,
        DOC_NO: Ctrl.ReportNo.val(),
        REPORT_NAME: Ctrl.ReportName.val(),
        PERIOD_FROM: Ctrl.PeriodFrom.val(),
        PERIOD_TO: Ctrl.PeriodTo.val(),
        EMP_CODE: URL_CONST.EmployeeCode,
        SELECTED: datas
    }
}

var GetParamsChangeRequestDetailReport = function () {
    var datas = [];
    Ctrl.SearchTable.find('tr').each(function (e) {
        var d = Ctrl.DataTable.row(this).data();
        var cb = $(this).find('[name="CheckBoxItem"]');
        if (cb.is(':checked')) {
            datas.push(d);
            datas[datas.length - 1].ROW_NUMBER = d.ROW_NUMBER;
            datas[datas.length - 1].COMPANY = d.COMPANY;
            datas[datas.length - 1].REQUEST_CODE = d.REQUEST_CODE;
            datas[datas.length - 1].REPORT_TYPE = d.REPORT_TYPE;
            datas[datas.length - 1].REPORT_NO = d.REPORT_NO;
            datas[datas.length - 1].COMPLETE_DATE = d.COMPLETE_DATE;
        }
    });

    return datas;
}

var GetParamsFixedAssetDetailReport = function () {
    return {
        REPORT_TYPE: 3,
        REQUEST_TYPE: null,
        REPORT_NAME: Ctrl.ReportName.val(),
        DOC_NO: Ctrl.null,
        PERIOD_FROM: null,
        PERIOD_TO: null,
        EMP_CODE: URL_CONST.EmployeeCode,
        SELECTED: null
    }
}
var ParamsSearchChangeRequestDetailReport = function () {
    return {
        REPORT_TYPE: 2,
        REQUEST_TYPE: null,
        REPORT_NAME: Ctrl.ReportName.val(),
        DOC_NO: Ctrl.ReportNo.val(),
        PERIOD_FROM: Ctrl.PeriodFrom.val(),
        PERIOD_TO: Ctrl.PeriodTo.val(),
        EMP_CODE: URL_CONST.EmployeeCode,
        SELECTED: null,
        COMPANY: getCompanyValue()

    }
}

var ClearSummaryReport = function () {
    ClearMessage();
    Ctrl.PeriodFrom.val(null);
    Ctrl.PeriodTo.val(null);
    WFD01120ClearTableResult();
}
var ClearChangeRequestDetailReport = function () {
    ClearMessage();
    Ctrl.ReportNo.val(null);
    Ctrl.PeriodFrom.val(null);
    Ctrl.PeriodTo.val(null);
    Ctrl.PanelSearch.hide();
    Ctrl.PanelPrint.hide();
}
var ClearFixedAssetDetailReport = function () {
    ClearMessage();
    Ctrl.CostCenter.val(null);
    WFD01120ClearTableResult();
}

var WFD01120ClearTableResult = function () {
    var table = Ctrl.SearchTable.DataTable();
    table.destroy();
    Ctrl.DataTable = null;
    Ctrl.SearchPanel.hide();
    Ctrl.Pagin.Clear();
    Ctrl.PanelPrint.hide();
}

var WFD01120Print_SummaryReport = function (_BatchID, _Desc) {
    // Seq ,Name, Value
    //LFD02230_SummaryCloseProject
    //var batch = BatchProcess({
    //    BatchId: _BatchID,
    //    Description: _Desc,
    //    UserId: URL_CONST.EmployeeCode

    //});
    ClearInitialBatch();
    switch (_BatchID)
    {
        case "LFD02230":
                    batchProcessLFD02230.Addparam(1, 'COMPANY', getCompanyValue())
                    batchProcessLFD02230.Addparam(2, 'START_PERIOD', ConvertCanlenDaTextToStrFormatddMMyyyy(Ctrl.PeriodFrom.val()))
                    batchProcessLFD02230.Addparam(3, 'END_PERIOD', ConvertCanlenDaTextToStrFormatddMMyyyy(Ctrl.PeriodTo.val()))
                    batchProcessLFD02230.Addparam(4, 'EMP_CODE', URL_CONST.EmployeeCode)
                    batchProcessLFD02230.StartBatch();
                    break;
        case "LFD02440":
                    batchProcessLFD02440.Addparam(1, 'COMPANY', getCompanyValue())
                    batchProcessLFD02440.Addparam(2, 'START_PERIOD', ConvertCanlenDaTextToStrFormatddMMyyyy(Ctrl.PeriodFrom.val()))
                    batchProcessLFD02440.Addparam(3, 'END_PERIOD', ConvertCanlenDaTextToStrFormatddMMyyyy(Ctrl.PeriodTo.val()))
                    batchProcessLFD02440.Addparam(4, 'EMP_CODE', URL_CONST.EmployeeCode)
                    batchProcessLFD02440.StartBatch();
            break;
        case "LFD02540":
                    batchProcessLFD02540.Addparam(1, 'COMPANY', getCompanyValue())
                    batchProcessLFD02540.Addparam(2, 'START_PERIOD', ConvertCanlenDaTextToStrFormatddMMyyyy(Ctrl.PeriodFrom.val()))
                    batchProcessLFD02540.Addparam(3, 'END_PERIOD', ConvertCanlenDaTextToStrFormatddMMyyyy(Ctrl.PeriodTo.val()))
                    batchProcessLFD02540.Addparam(4, 'EMP_CODE', URL_CONST.EmployeeCode)
                    batchProcessLFD02540.StartBatch();
            break;
    }

}

function ConvertCanlenDaTextToStrFormatddMMyyyy(strVal) {
    if (typeof (strVal) !== "undefined" && strVal !== null) {
        dDay = strVal.substr(0, 2) //dd
        tmpMonth = strVal.substr(3, 2);
        dYear = strVal.substr(6, 4);
        //01-jan-2017 
        tmpMonth = tmpMonth.toLowerCase();
        //switch (tmpMonth) {
        //    case "jan": dMonth = "01";
        //        break;

        //    case "feb": dMonth = "02";
        //        break;

        //    case "mar": dMonth = "03";
        //        break;

        //    case "apr": dMonth = "04";
        //        break;

        //    case "may": dMonth = "05";
        //        break;

        //    case "jun": dMonth = "06";
        //        break;

        //    case "jul": dMonth = "07";
        //        break;

        //    case "aug": dMonth = "08";
        //        break;

        //    case "sep": dMonth = "09";
        //        break;

        //    case "oct": dMonth = "10";
        //        break;

        //    case "nov": dMonth = "11";
        //        break;

        //    case "dec": dMonth = "12";
        //        break;

        //    default: break;

        //}

        //returnStringddMMyyyy = dYear + "" + dMonth + "" + dDay;
        returnStringddMMyyyy = dYear + "" + tmpMonth + "" + dDay;
        return returnStringddMMyyyy;
    }
    else
    {
        return null;

   }
}

var WFD01120Print_DetailRequestReportTranfer = function (_SelectedList) {
    ClearInitialBatch();
   
    for (i=0; i < _SelectedList.length; i++) {
        // batch.Addparam(i, 'DOC' + i, _SelectedList[i].REQUEST_CODE + "*" + _SelectedList[i].REPORT_NO);
        batchProcessLFD02420.Addparam(i, 'DOC' + i, _SelectedList[i].REPORT_NO);
    }

    batchProcessLFD02420.StartBatch();

}


var WFD01120Print_DetailRequestReportDisPose = function (_SelectedList) {
    ClearInitialBatch();
    for (i=0 ; i < _SelectedList.length; i++) {
        // batch.Addparam(i, 'DOC' + i, _SelectedList[i].REQUEST_CODE + "*" + _SelectedList[i].REPORT_NO);
        batchProcessLFD02520.Addparam(i, 'DOC' + i, _SelectedList[i].REPORT_NO);
    }

    batchProcessLFD02520.StartBatch();

}

var WFD01120Print_FixedAssetPhotoReport = function (_CostCenter) {
    //LFD02150_AssetNumberListWithPhoto
    ClearInitialBatch();
    batchProcessLFD02150.Addparam(1, 'COMPANY', getCompanyValue());
    batchProcessLFD02150.Addparam(2, 'EMP_CODE', URL_CONST.EmployeeCode)
    batchProcessLFD02150.Addparam(3, 'COST_CODE', _CostCenter)
    for (i = 4 ; i <= 20; i++) {
        batchProcessLFD02150.Addparam(i, 'T' + i, ""); //Set Remain is blank
    }
    batchProcessLFD02150.Addparam(21, 'APL_ID', '0')

    batchProcessLFD02150.StartBatch();
}


var WFD01120Print_FixedAssetMapReport = function (_CostCenter) {
    //LFD02170
    ClearInitialBatch();
    batchProcessLFD02170.Addparam(1, 'EMP_CODE', URL_CONST.EmployeeCode)
    batchProcessLFD02170.Addparam(2, 'COMPANY', getCompanyValue());
    batchProcessLFD02170.Addparam(3, 'COST_CODE', _CostCenter)

    batchProcessLFD02170.StartBatch();
}
var WFD01120Print_StockTakingReport = function (_CostCenter) {
    ClearInitialBatch();
    //Print out FA update scan status(countuncount) withwithout cover page report
    var con = {
        PERIOD_YEAR: '',
        PERIOD_ROUND: '',
        ASSET_LOCATION: ''
    }
    ajax_method.Post(URL_CONST.GET_STOCKTAKE, '', false, function (result) {
        if (result != null) {

            con.PERIOD_YEAR       = result.data[0].YEAR;
            con.PERIOD_ROUND    = result.data[0].ROUND;
            con.ASSET_LOCATION = result.data[0].ASSET_LOCATION;
        }

    }, null);
    //Parameter wait to confirm 
    batchProcessLFD02660.Addparam(1, 'COMPANY', getCompanyValue());
    batchProcessLFD02660.Addparam(2, 'PERIOD_YEAR', con.PERIOD_YEAR);
    batchProcessLFD02660.Addparam(3, 'PERIOD_ROUND', con.PERIOD_ROUND);
    batchProcessLFD02660.Addparam(4, 'ASSET_LOCATION', con.ASSET_LOCATION);
    batchProcessLFD02660.Addparam(5, 'COST_CODE', _CostCenter)
    batchProcessLFD02660.Addparam(6, 'DOC_NO', '')
    batchProcessLFD02660.StartBatch();
}
var WFD01120GetReportType = function () {

    ajax_method.Post(URL_CONST.ReportTypeURL, '', true, function (result) {
        if (result != null) {
            Ctrl.ReportType.html('');
            var optionhtml1 = '<option value="">' + "&nbsp;" + '</option>';
            Ctrl.ReportType.append(optionhtml1);

            $.each(result, function (i) {
                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].VALUE + '</option>';

                Ctrl.ReportType.append(optionhtml);
            });
        }

    }, null);

}

var getCompanyValue = function () {
    return GetMultipleCompany($('#cbb_company'));
    //var _com = '';
    //if ($('#cbb_company').val() != null) {
    //    _com = ($('#cbb_company').val() + '');
    //    _com = _com.replace(/,/g, '#');
    //}
    //return _com
}