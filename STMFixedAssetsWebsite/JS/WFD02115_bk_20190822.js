﻿$(function () {
    
    if (Params.MODE == "N") {
        fScreenMode._ClearMode();
        if (Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "RM") {
            fScreenMode.SetNewMode_NewAssetAUCRequest();
        }
        if (Params.FLOW_TYPE == "GN") {
            fScreenMode.SetNewMode_AssetInfoChange();
        }
        if (Params.FLOW_TYPE == "CN") {
            fScreenMode.SetNewMode_NewAssetReclassification();
        }
    }
   
    fAction.Initialize();
    
    
    //Load Data
    //setWFD02115ControlsRequiredField();


});

// Operation Action 
function format(item) { return item.text; };
var fAction = {
    SelectedCostCenter : '',
    _GetParameter: function () {
        return {
            GUID: Params.GUID,
            COMPANY: $('#WFD02115_COMPANY').val(),
            DOC_NO: Params.DOC_NO,
            LINE_NO: Params.LINE_NO,
            PARENT_ASSET_NO: $('#WFD02115_PARENT_ASSET_NO').val(),
            ASSET_MAINSUB: $('#WFD02115_ASSET_MAIN_SUB').val(),
            WBS_BUDGET: $('#WFD02115_WBS_BUDGET').val(),
            INDICATOR_VALIDATE: "Y", // Wait logic
            ASSET_CLASS: $('#WFD02115_ASSET_CLASS').val(),
            ASSET_NO: $('#WFD02115_ASSET_NO').val(),
            ASSET_SUB: $('#WFD02115_ASSET_SUB').val(),
            ASSET_DESC: $('#WFD02115_ASSET_DESC').val(),
            ADTL_ASSET_DESC: $('#WFD02115_ADTL_ASSET_DESC').val(),
            SERIAL_NO: $('#WFD02115_SERIAL_NO').val(),
            INVEN_NO: $('#WFD02115_INVEN_NO').val(),
            BASE_UOM: $('#WFD02115_BASE_UOM').val(),
            // LAST_INVEN_DATE: $('#WFD02115_LAST_INVEN_DATE').val(), -- No Used
            INVEN_INDICATOR: ($('#WFD02115_INVEN_INDICATOR').is(":checked") ? 'Y' : 'N'),
            INVEN_NOTE: $('#WFD02115_INVEN_NOTE').val(),
            COST_CODE: $('#WFD02115_COST_CODE').val(),
            RESP_COST_CODE: $('#WFD02115_RESP_COST_CODE').val(),
            LOCATION: $('#WFD02115_LOCATION').val(),
            ROOM: $('#WFD02115_ROOM').val(),
            LICENSE_PLATE: $('#WFD02115_LICENSE_PLATE').val(),
            WBS_PROJECT: $('#WFD02115_WBS_PROJECT').val(),
            INVEST_REASON: $('#WFD02115_BOI').val(),
            MINOR_CATEGORY: $('#WFD02115_MINOR_CATEGORY').val(),
            // ASSET_STATUS: $('#WFD02115_ASSET_STATUS').val(),
            MACHINE_LICENSE: $('#WFD02115_MACHINE_LICENSE').val(),
            BOI_NO: $('#WFD02115_BOI_NO').val(),
            FINAL_ASSET_CLASS: $('#WFD02115_FINAL_ASSET_CLASS').val(),
            // ASSET_SUPPER_NO: $('#WFD02115_ASSET_SUPPER_NO').val(),
            MANUFACT_ASSET: $('#WFD02115_MANUFACTURE_OF_ASSET').val(),
            ASSET_TYPE_NAME: $('#MACHINE_LICENSE_NAME').text(),
            PLATE_TYPE: $('#WFD02115_PLATE_TYPE').val(),
            BARCODE_SIZE: $('#WFD02115_BARCODE_SIZE').val(),
            LEASE_AGREE_DATE: $('#WFD02115_LEASE_AGREE_DATE').val(),
            LEASE_START_DATE: $('#WFD02115_LEASE_START_DATE').val(),
            LEASE_LENGTH_YEAR: $('#WFD02115_LEASE_LENGTH_YEAR').val(),
            LEASE_LENGTH_PERD: $('#WFD02115_LEASE_LENGTH_PERD').val(),
            LEASE_TYPE: $('#WFD02115_LEASE_TYPE').val(),
            LEASE_SUPPLEMENT: $('#WFD02115_LEASE_SUPPLEMENT').val(),
            LEASE_NUM_PAYMENT: $('#WFD02115_LEASE_NUM_PAYMENT').val(),
            LEASE_PAY_CYCLE: $('#WFD02115_LEASE_PAY_CYCLE').val(),
            LEASE_ADV_PAYMENT: $('#WFD02115_LEASE_ADV_PAYMENT').val(),
            LEASE_PAYMENT: $('#WFD02115_LEASE_PAYMENT').val(),
            LEASE_ANU_INT_RATE: $('#WFD02115_LEASE_ANU_INT_RATE').val(),
            // DPRE_AREA_01: $('#WFD02115_DPRE_AREA_01').val(),
            DPRE_KEY_01: $('#WFD02115_DPRE_KEY_01').val(),
            PLAN_LIFE_YEAR_01: $('#WFD02115_PLAN_LIFE_YEAR_01').val(),
            PLAN_LIFE_PERD_01: $('#WFD02115_PLAN_LIFE_PERD_01').val(),
            SCRAP_VALUE_01: $('#WFD02115_SCRAP_VALUE_01').val(),
            // DPRE_AREA_15: $('#WFD02115_DPRE_AREA_15').val(),
            DPRE_KEY_15: $('#WFD02115_DPRE_KEY_15').val(),
            PLAN_LIFE_YEAR_15: $('#WFD02115_PLAN_LIFE_YEAR_15').val(),
            PLAN_LIFE_PERD_15: $('#WFD02115_PLAN_LIFE_PERD_15').val(),
            SCRAP_VALUE_15: $('#WFD02115_SCRAP_VALUE_15').val(),
            // DPRE_AREA_31: $('#WFD02115_DPRE_AREA_31').val(),
            DPRE_KEY_31: $('#WFD02115_DPRE_KEY_31').val(),
            PLAN_LIFE_YEAR_31: $('#WFD02115_PLAN_LIFE_YEAR_31').val(),
            PLAN_LIFE_PERD_31: $('#WFD02115_PLAN_LIFE_PERD_31').val(),
            SCRAP_VALUE_31: $('#WFD02115_SCRAP_VALUE_31').val(),
            // DPRE_AREA_41: $('#WFD02115_DPRE_AREA_41').val(),
            DPRE_KEY_41: $('#WFD02115_DPRE_KEY_41').val(),
            PLAN_LIFE_YEAR_41: $('#WFD02115_PLAN_LIFE_YEAR_41').val(),
            PLAN_LIFE_PERD_41: $('#WFD02115_PLAN_LIFE_PERD_41').val(),
            SCRAP_VALUE_41: $('#WFD02115_SCRAP_VALUE_41').val(),
            // DPRE_AREA_81: $('#WFD02115_DPRE_AREA_81').val(),
            DPRE_KEY_81: $('#WFD02115_DPRE_KEY_81').val(),
            PLAN_LIFE_YEAR_81: $('#WFD02115_PLAN_LIFE_YEAR_81').val(),
            PLAN_LIFE_PERD_81: $('#WFD02115_PLAN_LIFE_PERD_81').val(),
            SCRAP_VALUE_81: $('#WFD02115_SCRAP_VALUE_81').val(),
            // DPRE_AREA_91: $('#WFD02115_DPRE_AREA_91').val(),
            DPRE_KEY_91: $('#WFD02115_DPRE_KEY_91').val(),
            PLAN_LIFE_YEAR_91: $('#WFD02115_PLAN_LIFE_YEAR_91').val(),
            PLAN_LIFE_PERD_91: $('#WFD02115_PLAN_LIFE_PERD_91').val(),
            SCRAP_VALUE_91: $('#WFD02115_SCRAP_VALUE_91').val(),
            Cnt: $('#INSERT_ASSET_ROWS').val()
        }
        
    },
    _Mapping: function (result) {
        $('#WFD02115_WBS_BALANCE').text(result.WBS_BALANCE);

        //$('#WFD02115_WBS_BUDGET_NAME').text(result.WBS_BUDGET_NAME);
        //$('#WFD02115_WBS_PROJECT_NAME').text(result.WBS_PROJECT_NAME);
        
        //$('#WFD02115_FINAL_ASSET_CLASS_NAME').text(result.FINAL_ASSET_CLASS_NAME);
        //$('#WFD02115_PARENT_ASSET_NAME').text(result.PARENT_ASSET_NAME);
        //$('#WFD02115_ASSET_CLASS_NAME').text(result.PARENT_ASSET_NAME);
        //$('#WFD02115_MINOR_CATEGORY_NAME').text(result.MINOR_CATEGORY_NAME);
        //$('#WFD02115_UOM_NAME').text(result.UOM_NAME);
        //$('#WFD02115_COST_NAME').text(result.COST_NAME);
        //$('#WFD02115_RESPONSIBLE_COST_CENTER_NAME').text(result.COST_NAME)
        //$('#WFD02115_LOCATION_NAME').text(result.LOCATION_NAME);
        //$('#WFD02115_BO_HINT').text(result.BO_HINT);
        //$('#WFD02115_TAX_PRIVILEGE_NAME').text(result.TAX_PRIVILEGE_NAME);
        //$('#WFD02115_MACHINE_LICENSE_NAME').text(result.MACHINE_LICENSE_NAME);
        $('#WFD02115_COMPANY').val(result.COMPANY);
        $('#WFD02115_LOCATION_REMARK').val(result.LOCATION_REMARK);
        $('#WFD02115_ASSET_NO').val(result.ASSET_NO);
        $('#WFD02115_COST_CODE').val(result.COST_CODE);
        $('#WFD02115_RESP_COST_CODE').val(result.RESP_COST_CODE);
        $('#WFD02115_ASSET_MAIN_SUB').select2("val", result.ASSET_MAINSUB);
        $('#WFD02115_PARENT_ASSET_NO').val(result.PARENT_ASSET_NO);
        $('#WFD02115_ASSET_GROUP').val(result.ASSET_GROUP);
        $('#WFD02115_WBS_BUDGET').select2("val", result.WBS_BUDGET);
        $('#WFD02115_ASSET_CLASS').select2("val", result.ASSET_CLASS);
        $('#WFD02115_ASSET_SUB').val(result.ASSET_SUB);
        $('#WFD02115_ASSET_DESC').val(result.ASSET_DESC);
        $('#WFD02115_ADTL_ASSET_DESC').val(result.ADTL_ASSET_DESC);
        $('#WFD02115_SERIAL_NO').val(result.SERIAL_NO);
        $('#WFD02115_INVEN_NO').val(result.INVEN_NO);
        $('#WFD02115_BASE_UOM').select2("val", result.BASE_UOM);
        $('#WFD02115_LAST_INVEN_DATE').datepicker('setDate', ConvertToJsonDate(result.LAST_INVEN_DATE));
        $('#WFD02115_INVEN_INDICATOR').prop("checked", (result.INVEN_INDICATOR == 'Y' ? true : false));
        $('#WFD02115_INVEN_NOTE').val(result.INVEN_NOTE);
        $('#WFD02115_LOCATION').select2("val", result.LOCATION);
        $('#WFD02115_ROOM').val(result.ROOM);
        $('#WFD02115_LICENSE_PLATE').val(result.LICENSE_PLATE);
        $('#WFD02115_WBS_PROJECT').select2("val", result.WBS_PROJECT);
        $('#WFD02115_BOI').select2("val", result.INVEST_REASON);
        $('#WFD02115_MINOR_CATEGORY').select2("val", result.MINOR_CATEGORY);
        $('#WFD02115_ASSET_STATUS').val(result.ASSET_STATUS);
        $('#WFD02115_MACHINE_LICENSE').select2("val", result.MACHINE_LICENSE);
        $('#WFD02115_BOI_NO').select2("val", result.BOI_NO);
        $('#WFD02115_FINAL_ASSET_CLASS').select2("val", result.FINAL_ASSET_CLASS);
        $('#WFD02115_MANUFACTURE_OF_ASSET').val(result.MANUFACT_ASSET);
        $('#WFD02115_PLATE_TYPE').select2("val", result.PLATE_TYPE);
        $('#WFD02115_BARCODE_SIZE').select2("val", result.BARCODE_SIZE);
        $('#WFD02115_LEASE_AGREE_DATE').datepicker('setDate', ConvertToJsonDate(result.LEASE_AGREE_DATE));
        $('#WFD02115_LEASE_START_DATE').datepicker('setDate', ConvertToJsonDate(result.LEASE_START_DATE));
        $('#WFD02115_LEASE_LENGTH_YEAR').val(result.LEASE_LENGTH_YEAR);
        $('#WFD02115_LEASE_LENGTH_PERD').val(result.LEASE_LENGTH_PERD);
        $('#WFD02115_LEASE_TYPE').select2("val", result.LEASE_TYPE);
        $('#WFD02115_LEASE_SUPPLEMENT').val(result.LEASE_SUPPLEMENT);
        $('#WFD02115_LEASE_NUM_PAYMENT').val(result.LEASE_NUM_PAYMENT);
        $('#WFD02115_LEASE_PAY_CYCLE').val(result.LEASE_PAY_CYCLE);
        $('#WFD02115_LEASE_ADV_PAYMENT').val(result.LEASE_ADV_PAYMENT);
        $('#WFD02115_LEASE_PAYMENT').val(result.LEASE_PAYMENT);
        $('#WFD02115_LEASE_ANU_INT_RATE').val(result.LEASE_ANU_INT_RATE);
        $('#WFD02115_DPRE_AREA_01').val(result.DPRE_AREA_01);
        $('#WFD02115_DPRE_KEY_01').val(result.DPRE_KEY_01);
        $('#WFD02115_PLAN_LIFE_YEAR_01').val(result.PLAN_LIFE_YEAR_01);
        $('#WFD02115_PLAN_LIFE_PERD_01').val(result.PLAN_LIFE_PERD_01);
        $('#WFD02115_SCRAP_VALUE_01').val(result.SCRAP_VALUE_01);
        $('#WFD02115_DPRE_AREA_15').val(result.DPRE_AREA_15);
        $('#WFD02115_DPRE_KEY_15').val(result.DPRE_KEY_15);
        $('#WFD02115_PLAN_LIFE_YEAR_15').val(result.PLAN_LIFE_YEAR_15);
        $('#WFD02115_PLAN_LIFE_PERD_15').val(result.PLAN_LIFE_PERD_15);
        $('#WFD02115_SCRAP_VALUE_15').val(result.SCRAP_VALUE_15);
        $('#WFD02115_DPRE_AREA_31').val(result.DPRE_AREA_31);
        $('#WFD02115_DPRE_KEY_31').val(result.DPRE_KEY_31);
        $('#WFD02115_PLAN_LIFE_YEAR_31').val(result.PLAN_LIFE_YEAR_31);
        $('#WFD02115_PLAN_LIFE_PERD_31').val(result.PLAN_LIFE_PERD_31);
        $('#WFD02115_SCRAP_VALUE_31').val(result.SCRAP_VALUE_31);
        $('#WFD02115_DPRE_AREA_41').val(result.DPRE_AREA_41);
        $('#WFD02115_DPRE_KEY_41').val(result.DPRE_KEY_41);
        $('#WFD02115_PLAN_LIFE_YEAR_41').val(result.PLAN_LIFE_YEAR_41);
        $('#WFD02115_PLAN_LIFE_PERD_41').val(result.PLAN_LIFE_PERD_41);
        $('#WFD02115_SCRAP_VALUE_41').val(result.SCRAP_VALUE_41);
        $('#WFD02115_DPRE_AREA_81').val(result.DPRE_AREA_81);
        $('#WFD02115_DPRE_KEY_81').val(result.DPRE_KEY_81);
        $('#WFD02115_PLAN_LIFE_YEAR_81').val(result.PLAN_LIFE_YEAR_81);
        $('#WFD02115_PLAN_LIFE_PERD_81').val(result.PLAN_LIFE_PERD_81);
        $('#WFD02115_SCRAP_VALUE_81').val(result.SCRAP_VALUE_81);
        $('#WFD02115_DPRE_AREA_91').val(result.DPRE_AREA_91);
        $('#WFD02115_DPRE_KEY_91').val(result.DPRE_KEY_91);
        $('#WFD02115_PLAN_LIFE_YEAR_91').val(result.PLAN_LIFE_YEAR_91);
        $('#WFD02115_PLAN_LIFE_PERD_91').val(result.PLAN_LIFE_PERD_91);
        $('#WFD02115_SCRAP_VALUE_91').val(result.SCRAP_VALUE_91);
        $('#WFD02115_GUID').val(result.GUID);
        $('#WFD02115_STATUS').val(result.STATUS);
        $('#WFD02115_MESSAGE_TYPE').val(result.MESSAGE_TYPE);
        $('#WFD02115_MESSAGE').val(result.MESSAGE);
        $('#WFD02115_DELETE_FLAG').val(result.DELETE_FLAG);
        $('#WFD02115_CREATE_DATE').val(result.CREATE_DATE);
        $('#WFD02115_CREATE_BY').val(result.CREATE_BY);
        $('#WFD02115_UPDATE_DATE').val(result.UPDATE_DATE);
        $('#WFD02115_UPDATE_BY').val(result.UPDATE_BY);
    },
    _LoadAssetClass: function () {
        return $.post('/WFD02115/AssetClass_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, true, function (result) {
            console.log("_LoadAssetClass");
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_ASSET_CLASS').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));

                $('#WFD02115_FINAL_ASSET_CLASS').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
        });
    },
    _LoadAUCAssetClass: function () {
        return $.post('/WFD02115/AUCAssetClass_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, true, function (result) {
            console.log("_LoadAUCAssetClass");
            if (result == null || result.length == 0) {
                return;
            }
            $('#WFD02115_ASSET_CLASS').html('');
            $.each(result, function () {
                $('#WFD02115_ASSET_CLASS').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
        });
    },
    _LoadWBSBudget: function () {
      //  var dfd = $.Deferred();
       return $.post('/WFD02115/WBSBudget_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, function (result) {
            console.log("_LoadWBSBudget");
           
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_WBS_BUDGET').append($("<option />")
                    .attr('data-description', this.DESCRIPTION)
                    .val(this.WBS_CODE)
                    .text(this.WBS_CODE));
            });
            //fAction._LoadWBSBudget();
            
        });
      //  return dfd.promise();
    },
    _LoadWBSProject: function () {
       // var dfd = $.Deferred();
        return $.post('/WFD02115/WBSProject_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, function (result) {
            console.log("_LoadWBSProject");
            console.log(result);
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
            //    console.log(this.WBS_CODE);
                $('#WFD02115_WBS_PROJECT').append($("<option />")
                    .attr('data-description', this.DESCRIPTION)
                    .val(this.WBS_CODE)
                    .text(this.WBS_CODE));
            });

            
        });
       // return dfd.promise();
    },
    LoadMinorCategory: function () {

        $('#WFD02115_MINOR_CATEGORY').html('');
        return $.post('/WFD02115/MinorCategory_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val(), ASSET_CLASS: $('#WFD02115_ASSET_CLASS').val() }, true, function (result) {
            console.log("LoadMinorCategory");
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_MINOR_CATEGORY').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
        });
    },

    _LoadLocation: function () {
        return $.post('/WFD02115/Location_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, true, function (result) {

            console.log("_LoadLocation");
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_LOCATION').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
        });
    },

    _LoadBOI: function () {
        return $.post('/WFD02115/BOI_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, true, function (result) {
            console.log("_LoadBOI");
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_BOI_NO').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
        });
    },

    _LoadMachineLicense: function () {
        return $.post('/WFD02115/MachineLicense_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, true, function (result) {
            console.log("_LoadMachineLicense");
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_MACHINE_LICENSE').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
        });
    },

    LoadWBSBudgetInfo: function () {

        // If WBS Budget is blank
        if ($('#WFD02115_WBS_BUDGET').val() == '') {
           
            $('#WFD02115_WBS_BALANCE').text('');
            $('#WFD02115_ASSET_CLASS').select2('val','');
            $('#WFD02115_MINOR_CATEGORY').select2('val','');

            $('#WFD02115_RESP_COST_CODE').val('');
            $('#WFD02115_COST_CODE').val('');
            $('#WFD02115_ASSET_GROUP').val('RMA');

            return ;
        }


        var _data = {
            COMPANY_CODE: $('#WFD02115_COMPANY').val(),
            WBS: $('#WFD02115_WBS_BUDGET').val(),
        };
        ajax_method.Post(URL_CONST.WFD02115_GetDeriveWBSBudget, _data, true, function (result) {
            if (result == null) {
                console.log("WFD02115_GetWBSBudgetInfo  : No data found");
                return;
            }
            $('#WFD02115_WBS_BALANCE').text(result.BUDGET_AMOUNT);

            $('#WFD02115_RESP_COST_CODE').val(result.RESP_COST_CENTER);
            $('#WFD02115_COST_CODE').val(result.COST_CENTER);
            $('#WFD02115_ASSET_GROUP').val(result.ASSET_GROUP);

            $('#WFD02115_COST_NAME').text(result.COST_NAME);
            $('#WFD02115_RESP_COST_NAME').text(result.RESP_COST_NAME);

            $('#WFD02115_ASSET_CLASS').removeClass("required");
            $('#WFD02115_ASSET_CLASS').prop('required', false);
            //If WBS for RMA Assets
            if (GLOBAL.REQUEST_TYPE == 'A') {
                if (result.ASSET_GROUP == "RMA") {
                    $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                    $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');

                    $('#WFD02115_ASSET_CLASS').prop('disabled', true);
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
                }
                if (result.ASSET_GROUP == "AUC") {
                    $('#WFD02115_FINAL_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);

                    $('#WFD02115_ASSET_CLASS').prop('disabled', false); //enable
                    $('#WFD02115_ASSET_CLASS').select2('val', '');
                    //Load Asset Class for AUC

                    $('#WFD02115_ASSET_CLASS').addClass("required");
                    $('#WFD02115_ASSET_CLASS').prop('required', true);
                }
                return;
            }
            if (GLOBAL.REQUEST_TYPE == 'C') { //Reclass

                $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');

                $('#WFD02115_ASSET_CLASS').addClass("required");
                $('#WFD02115_ASSET_CLASS').prop('required', true);

                $('#WFD02115_ASSET_CLASS').prop('disabled', false); //allow to user change
                $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true); // set blank and disables
                
            }
        })
    },

    LoadDepreciation: function () {

        //New Mode only
        if(Params.MODE != "N"){
            console.log("LoadDepreciatioin is executed on new mode only");
            return ;
        }


        //Clear Value
        $('#DPRE_MANI_1 input').val('');
        $('.aeconly input').val('');

        $('#WFD02115_PLATE_TYPE').select2('val','');
        $('#WFD02115_BARCODE_SIZE').select2('val','');
        $('#WFD02115_INVEN_INDICATOR').prop("checked", false);



        var _data = {
            COMPANY: $('#WFD02115_COMPANY').val(),
            ASSET_CLASS: $('#WFD02115_ASSET_CLASS').val(),
            MINOR_CATEGORY: $('#WFD02115_MINOR_CATEGORY').val(),
        };
        //User must select both of criteria
        if ((_data.ASSET_CLASS == null || _data.ASSET_CLASS == "") || (_data.MINOR_CATEGORY == null || _data.MINOR_CATEGORY == "")) {
            console.log("LoadDepreciatioin : some field is blank");
            return ;
        }


        ajax_method.Post(URL_CONST.WFD02115_GetDepreciationData, _data, true, function (result) {
            if (result == null) {
                console.log("WFD02115_GetDepreciationData  : No data found");
                return;
            }

            $('#WFD02115_PLATE_TYPE').select2('val',result.STICKER_TYPE);
            $('#WFD02115_BARCODE_SIZE').select2('val',result.STICKER_SIZE);
            // INVENTORY_INDICATOR
            if(result.INVENTORY_INDICATOR == "Y"){
                $('#WFD02115_INVEN_INDICATOR').prop("checked", true);
            }
            // DPRE_AREA_01
            
            $('#WFD02115_DPRE_KEY_01').val(result.DPRE_KEY_01);
            $('#WFD02115_PLAN_LIFE_YEAR_01').val(result.USEFUL_LIFE_YEAR_01);
            $('#WFD02115_PLAN_LIFE_PERD_01').val(result.USEFUL_LIFE_PERD_01);
            $('#WFD02115_SCRAP_VALUE_01').val(result.SCRAP_VALUE_01);
            
            //$('#WFD02115_DPRE_AREA_15').val(result.DPRE_KEY_15),
            $('#WFD02115_DPRE_KEY_15').val(result.DPRE_KEY_15);
            $('#WFD02115_PLAN_LIFE_YEAR_15').val(result.USEFUL_LIFE_YEAR_15);
            $('#WFD02115_PLAN_LIFE_PERD_15').val(result.USEFUL_LIFE_PERD_15);
            $('#WFD02115_SCRAP_VALUE_15').val(result.SCRAP_VALUE_15);

            //DPRE_KEY_31,		USEFUL_LIFE_YEAR_31,	USEFUL_LIFE_PERD_31,		USEFUL_LIFE_TYPE_31,			SCRAP_VALUE_31,
            //$('#WFD02115_DPRE_AREA_31').val(result.DPRE_KEY_01),
            $('#WFD02115_DPRE_KEY_31').val(result.DPRE_KEY_31);
            $('#WFD02115_PLAN_LIFE_YEAR_31').val(result.USEFUL_LIFE_YEAR_31);
            $('#WFD02115_PLAN_LIFE_PERD_31').val(result.USEFUL_LIFE_PERD_31);
            $('#WFD02115_SCRAP_VALUE_31').val(result.SCRAP_VALUE_31);

            //$('#WFD02115_DPRE_AREA_41').val(result.DPRE_KEY_01),
            $('#WFD02115_DPRE_KEY_41').val(result.DPRE_KEY_41);
            $('#WFD02115_PLAN_LIFE_YEAR_41').val(result.USEFUL_LIFE_YEAR_41);
            $('#WFD02115_PLAN_LIFE_PERD_41').val(result.USEFUL_LIFE_PERD_41);
            $('#WFD02115_SCRAP_VALUE_41').val(result.SCRAP_VALUE_41);

            //$('#WFD02115_DPRE_AREA_81').val(result.DPRE_KEY_01),
            $('#WFD02115_DPRE_KEY_81').val(result.DPRE_KEY_81);
            $('#WFD02115_PLAN_LIFE_YEAR_81').val(result.USEFUL_LIFE_YEAR_81);
            $('#WFD02115_PLAN_LIFE_PERD_81').val(result.USEFUL_LIFE_PERD_81);
            $('#WFD02115_SCRAP_VALUE_81').val(result.SCRAP_VALUE_81);
            
            //$('#WFD02115_DPRE_AREA_91').val(result.DPRE_KEY_01),
            $('#WFD02115_DPRE_KEY_91').val(result.DPRE_KEY_91);
            $('#WFD02115_PLAN_LIFE_YEAR_91').val(result.USEFUL_LIFE_YEAR_91);
            $('#WFD02115_PLAN_LIFE_PERD_91').val(result.USEFUL_LIFE_PERD_91);
            $('#WFD02115_SCRAP_VALUE_91').val(result.SCRAP_VALUE_91);

        })
    },

    Initialize: function () {
       
        $('#WFD02115_COMPANY').val(GLOBAL.COMPANY);
        $('#WFD02115_DOCNO').val(Params.DOC_NO);
        $('#WFD02115_ITEMNO').val(Params.LINE_NO);
        $('#WFD02115_ORIGIN_ASSET_NO').val(Params.ORIGINAL_ASSET_NO);
        $('#WFD02115_ORIGIN_ASSET_SUB_NO').val(Params.ORIGINAL_ASSET_SUB);
       // $.when(j1, j2).then(function (a1, a2) {
        $.when(fAction._LoadWBSBudget(),
            fAction._LoadWBSProject(),
            fAction._LoadLocation(),
            fAction._LoadBOI(),
            fAction._LoadMachineLicense()
            ).then(function () {
            console.log("OK");
        });

       

        return ;
        //Bind Combobox

        fAction._LoadAssetClass();
        
        //WFD02115_WBS_BUDGET
        //fAction._LoadWBSBudget();
        
        //WFD02115_WBS_PROJECT
        //fAction._LoadWBSProject();

        //WFD02115_LOCATION
        fAction._LoadLocation();

        //WFD02115_BOI_NO
        fAction._LoadBOI();

        //WFD02115_MACHINE_LICENSE
        fAction._LoadMachineLicense();

        //Trigger Event change
        $('#WFD02115_ASSET_MAIN_SUB').trigger('change');
        //Load Data

        //Edit Mode
        if ((Params.DOC_NO != null && Params.DOC_NO != '') && (Params.LINE_NO != null && Params.LINE_NO != '')) {
            fAction.LoadData(function () {
                fScreenMode._ClearMode();
                if ((Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "RM") && Params.MODE == "V") {
                    fScreenMode.SetEditMode_AssetInfoChange(); // Test
                }

                if ((Params.FLOW_TYPE == "GN") && Params.MODE == "V") {
                    //fScreenMode.SetNewMode_AssetInfoChange();
                }

            });
        }

    },
    LoadData : function(callback){
            ajax_method.Post(URL_CONST.WFD02115_GetAssetsData, Params, true, function (result) {
                if (result == null) {
                    console.log("No data found");
                    return ;
                }
                
                fAction._Mapping(result);
                
                if(callback != null)
                    callback();
            })
    },
    ShowSearchPopup: function (callback) {
        //Set search parameter
        
        f2190SearchAction.ClearDefault();

        Search2190Config.RoleMode = Params.ROLEMODE;

        Search2190Config.searchOption = Params.FLOW_TYPE;
        Search2190Config.AllowMultiple = false;

        Search2190Config.Default.Company = { Value: GLOBAL.COMPANY, Enable: false };

        // Get Information from Existing Assets (No need to check selected CC / RCC
        //Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedSourceCC == "" };
        //Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedSourceRCC, Enable: fAction.SelectedSourceRCC == "" };
       
        Search2190Config.customCallback = null;
        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();

    },
    ShowSearchParentPopup: function (callback) {

        //Set search parameter

        f2190SearchAction.ClearDefault();

        Search2190Config.RoleMode = Params.ROLEMODE;

        Search2190Config.searchOption = Params.FLOW_TYPE;
        Search2190Config.AllowMultiple = false;

        Search2190Config.Default.Company = { Value: GLOBAL.COMPANY, Enable: false };
        Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
        Search2190Config.Default.Parent = { Value: "Y", Enable: false };

        Search2190Config.customCallback = function (_list, callback) {
            if (_list == null || _list.length == 0) {
                fnErrorDialog('Error', "No selected data");
                return;
            }

            var _selected = _list[0];
            console.log(_selected);
            $('#WFD02115_PARENT_ASSET_NO').val(_selected.ASSET_NO);
            $('#WFD02115_ASSET_CLASS').select2("val", _selected.ASSET_CLASS);
            $('#WFD02115_MINOR_CATEGORY').select2("val", _selected.MINOR_CATEGORY);
            Search2190Config.customCallback = null;
            if (callback == null)
                return;

            callback();


        };

        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();

    },
    SaveData: function (callback) {
        console.log(fAction._GetParameter());
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(URL_CONST.WFD02115_SaveAssetsData, fAction._GetParameter(), false, function (result) {
            console.log(result);
            if (result.IsError) {
                fnErrorDialog('Error', result.Message);
                return;
            }
            fnCompleteDialog('Save successfully!!!!!!!!!!!!!!!');

            if (callback == null)
                return;

            callback();

        });
    },    
    AddAsset: function (_list, callback) { //It's call from dialog
        console.log("AddAsset");
        console.log(_list);
        console.log(URL_CONST.WFD02115_GetFixedAssetMaster);
        //should have 1 records.
        if (_list == null || _list.length == 0) {
            fnErrorDialog('Error', "No selected data");
            return;
        }
        
        var _data = _list[0];
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(URL_CONST.WFD02115_GetFixedAssetMaster, _data, true, function (result) {
            if (result == null) {
                console.log("AddAsset : No data found");
                return;
            }

            fAction._Mapping(result);

            //Incase Asset Info Change (Add Mode)
            if (Params.MODE == "N") {
                if (Params.FLOW_TYPE == "GN") {
                    fScreenMode.SetNewMode_AssetInfoChangeAfterGet();
                }
            }

            if (callback != null)
                callback();
        })
    },

}
//Set Status of Control
var fScreenMode = {
    _ClearMode : function(){
        return; 
        $('.aeconly').hide();
        $('#DPRE_MANI_2').hide();

        $('#divWFD02115.form-horizontal').find('input,select,button').prop('disabled', true);

        //
        $('.duplicaterow').hide();
       
    },
    SetNewMode_AssetInfoChange: function () {
        fScreenMode._ClearMode();
        $('#btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);

    },
    SetNewMode_AssetInfoChangeAfterGet: function () {
        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', true);
        $('.assetkey').prop('disabled', true);
        $('#WFD02115_ASSET_GROUP, #WFD02115_INVEN_NO, #WFD02115_ROOM').prop('disabled', true);
        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);
        $('#btnWFD02115OK, #btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);

    },
    SetEditMode_AssetInfoChange: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', true);

        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);

        $('#btnWFD02115OK,#btnWFD02115Close').prop('disabled', false);
    },
    SetAECMode_AssetInfoChange: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);

        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);

        $('#btnWFD02115OK,#btnWFD02115Close').prop('disabled', false);
    },
    SetNewMode_NewAssetAUCRequest: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', true);
        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);
        $('#btnWFD02115OK,#btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);
    },
    SetEditMode_NewAssetAUCRequest: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115.form-horizontal').find('input,select,button').prop('disabled', false);
        $('.aeconly').prop('disabled', true);
        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);
        $('#btnWFD02115OK,#btnWFD02115Close').prop('disabled', false);
    },

    SetNewMode_NewAssetRMARequest: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', true);
        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);
        $('#btnWFD02115OK,#btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);
    },

    SetNewMode_NewAssetReclassification: function () {
        fScreenMode._ClearMode();

        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', true);
        $('.assetkey').prop('disabled', true);
        $('#WFD02115_ASSET_GROUP, #WFD02115_INVEN_NO, #WFD02115_ROOM').prop('disabled', true);
        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);

        $('#btnWFD02115OK,#btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);
    },
    SetNewMode_NewAssetSettlement: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115.form-horizontal').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', true);

        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);

        $('#btnWFD02115OK,#btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);
    }

}

$('#btnWFD02115OK').click(function (e) {

    e.preventDefault();

    $('#btnWFD02115OK').prop('disabled', true);
    $('#btnWFD02115OK').prop('disabled', false);

    //$("#frmWFD02115Submit").validate({
    //    debug: true
    //});

    

    fAction.SaveData(function () {
        $('#btnWFD02115OK').prop('disabled', false);
        opener.LoadDataAbstract(function () {
            window.close();
        });
    });
})
//Close dialog
$('#btnWFD02115Close').click(function (e) {
    //Show confirm message to close
    e.preventDefault();
    window.close();
})

//Search existing assets
$('#btnGetExistsAssetInfo').click(function (e) {
    e.preventDefault();
    fAction.ShowSearchPopup(function () {
       
    });
})

$('#WFD02115BtnPARENT_ASSET_NO').click(function (e) {
    e.preventDefault();
    fAction.ShowSearchParentPopup(function () {
      
    });
});
$('#WFD02115BtnCOST_CODE').click(function () {
    
    $('#SearchCostCenter').modal('show');
    CostControls.IsSearchRespCostCode = false;
    COMSCC_InitialParameter();

    CostControls.callback = function (_data) {
        $('#WFD02115_COST_CODE').val(_data.COST_CODE);
        $('#WFD02115_COST_NAME').text(_data.COST_NAME);
    };

});
$('#WFD02115BtnRESPONSIBLE_COST_CENTER').click(function () {

    $('#SearchCostCenter').modal('show');
    CostControls.IsSearchRespCostCode = true;
    COMSCC_InitialParameter();

    CostControls.callback = function (_data) {
        $('#WFD02115_RESP_COST_CODE').val(_data.COST_CODE);
        $('#WFD02115_RESP_COST_NAME').text(_data.COST_NAME);
    };
});

$('#WFD02115_ASSET_MAIN_SUB').change(function () {

    $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', true);
    $('#WFD02115_ASSET_CLASS').prop('disabled', true);
    $('#WFD02115_MINOR_CATEGORY').prop('disabled', true);

    $('#WFD02115_PARENT_ASSET_NO').val(''); //parent
    $('#WFD02115_ASSET_CLASS').select2('val',''); //Asse class
    $('#WFD02115_MINOR_CATEGORY').select2('val', ''); //minor category

    if ($('#WFD02115_ASSET_MAIN_SUB').val() == "") {
        return;
    }


    if ($('#WFD02115_ASSET_MAIN_SUB').val() == "M" ) {        
        $('#WFD02115_ASSET_CLASS').prop('disabled', false); //enable asset class
        $('#WFD02115_MINOR_CATEGORY').prop('disabled', false); //enable minor category

        $('#WFD02115_PARENT_ASSET_NO').removeClass("required");
        $('#WFD02115_PARENT_ASSET_NO').prop('required', false);
    }
    else {
        $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', false); //enable parenet

        $('#WFD02115_PARENT_ASSET_NO').removeClass("required").addClass("required");
        $('#WFD02115_PARENT_ASSET_NO').prop('required', true);
    }
});

$('#WFD02115_ASSET_CLASS').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_ASSET_CLASS_NAME').text(desc);

    $('#WFD02115_MINOR_CATEGORY').val('');
    fAction.LoadMinorCategory();

});
$('#WFD02115_FINAL_ASSET_CLASS').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_FINAL_ASSET_CLASS_NAME').text(desc);
});
$('#WFD02115_WBS_BUDGET').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_WBS_BUDGET_NAME').text(desc);
    $('#WFD02115_WBS_BUDGET_NAME').text(desc);


    fAction.LoadWBSBudgetInfo();
});
$('#WFD02115_WBS_PROJECT').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_WBS_PROJECT_NAME').text(desc);
});
$('#WFD02115_MINOR_CATEGORY').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_MINOR_CATEGORY_NAME').text(desc);

    fAction.LoadDepreciation();

})
$('#WFD02115_LOCATION').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_LOCATION_NAME').text(desc);
})
$('#WFD02115_BOI_NO').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_TAX_PRIVILEGE_NAME').text(desc);
})
$('#WFD02115_MACHINE_LICENSE').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_MACHINE_LICENSE_NAME').text(desc);
})

$('#WFD02115_BASE_UOM').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_BOM_NAME').text(desc);
});
$('#WFD02115_BOI').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_BOI_HINT').text(desc);
    
    //Set Required
    $('#WFD02115_BOI_NO').removeClass("required");
    if ($('#WFD02115_BOI').val() == 'B') {
        $('#WFD02115_BOI_NO').addClass("required");
    }
})



