﻿var Controls = {
    SYSTEM: $('#hidSystem'),
    APP_DETAIL: $('#hidAppId'),
    FUNCTION: $('#ddlFunction'),
    STATUS: $('#ddlStatus'),
    LEVEL: $('#ddlLevel'),
    APP_ID: $('#txtAppID'),
    USER_ID: $('#txtUserID'),
    FROM: $('#txtStartTime'),
    TO: $('#txtEndTime'),
    FAVORITE: $('#chkFavorite'),
    SearchButton: $("#WFD01010Search"),
    ClearButton: $('#WFD01010Clear'),   
    SearchResultTable: $('#SearchResultTable'), 
    Pagin: $('#SearchResultTable').Pagin(),
    SearchDataRow: $('#SearchDataRow'),
    BackButton: $('#WFD01010Back')
};

$(function () {
    SetDefaultScreenValue();

    $('#txtAppID, #txtUserID').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            Controls.SearchButton.click();
            
            return false;
        }
    });
});



var SetDefaultScreenValue = function () {
    if (SCREEN.IsDetailScreen == false) Clear();
    else ClearDetail(); 
    
}

var getDefaultSceenValue = function () {  
    return {
        SYSTEM_ID: Controls.SYSTEM.val(),
        FUNCTION_ID: Controls.FUNCTION.val(),
        STATUS: Controls.STATUS.val(),
        APP_ID: Controls.APP_ID.val(),
        USER_ID: Controls.USER_ID.val(),
        FROM: Controls.FROM.val(),
        TO: Controls.TO.val(),
        LEVEL : Controls.LEVEL.val(),
        DATE_START: Controls.FROM.val(),
        DATE_TO : Controls.TO.val(),
        FAVORITE_FLAG: (Controls.FAVORITE.is(':checked')? 'Y': 'N')
    }
}

var Search = function () {

    ClearMessageC();
    var con = getDefaultSceenValue();
    var pagin = Controls.Pagin.GetPaginData();

    Controls.SearchDataRow.show();

    pagin.OrderType = 'asc';
    ajax_method.SearchPost(URL_CONST.GetSearchLoggin, con, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {

            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }
                                      
            Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { data: 'APP_ID' },
                    { data: 'MODULE_NAME' },
                    { data: "BATCH_NAME" },
                    { data: 'START_TIME' },
                    { data: 'END_TIME' },
                    { data: 'CREATE_BY' },
                    { data: 'STATUS'},
                    { data: 'DOC_NO'},
                    { data: 'MESSAGE' },
                    { data: 'FILE_PATH' , visible: false}
                ],
                'columnDefs': [
                    {
                        "targets": 0,
                        "orderable": false,
                        "className": "text-center",
                        "render": function (data, type, full, meta) {
                            return '<a onclick=\"OpenIndexByAppId('+data+')\">' + data + '</a>';
                        }
                    },
                    {
                        "targets": 1,
                        "orderable": false,
                        "className": "text-center"
                    }, 
                    {
                        "targets": 2,
                        "orderable": false                    
                    },                    
                    {
                        "targets": 3,
                        "className": "text-center",
                        "orderable": false
                    },
                    {
                        "targets": 4,
                        "className": "text-center",
                        "orderable": false
                    },
                    {
                        "targets": 5,
                        "orderable": false,
                        "className": "text-center"
                    },
                    {
                        "targets": 6,
                        "orderable": false
                    },
                    {
                        "targets": 7,
                        "orderable": false,
                        "className": "text-center",
                        "render": function (data, type, full, meta) {
                            return (data != null ?
                                '<a download  target="_blank" href="/WFD01010/Download?file=' + full.FILE_PATH + '">' + data + '</a>'
                                : '');
                        }
                    },
                    {
                        "targets": 8,
                        "orderable": false
                    }
                ],                       
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                fixedHeader: true
            });
                
            //var page = Controls.SearchResultTable.Pagin();
            //page.Init(pagin, sorting, changePage, 'itemPerPage');

            Controls.Pagin.Init(pagin, sorting, changePage, 'itemPerPage');
            Controls.BackButton.click(backToSearchLogmonitoring);
            Controls.SearchDataRow.show();
        } else {
            Controls.SearchDataRow.hide();
            Controls.BackButton.click(backToSearchLogmonitoring);
            Controls.Pagin.Clear();
        }

    }, null, true, 'SearchDataRow');
}
var sorting = function (PaginData) {
    Controls.Pagin.ClearPaginData();
    Search();
}
var changePage = function (PaginData) {
    Search();
}

var SearchDetail = function () {

    ClearMessageC();

    var con = {
        SYSTEM_ID: Controls.SYSTEM.val(),
        FUNCTION_ID: null,
        STATUS: null,
        APP_ID: Controls.APP_DETAIL.val(),
        USER_ID: null,
        FROM: null,
        TO: null,
        LEVEL: Controls.LEVEL.val(),
        DATE_START: null,
        DATE_TO: null,
        FAVORITE_FLAG: null
    }
    Controls.APP_ID.val(GetUrlParameter("ByApp"));
    var pagin = Controls.Pagin.GetPaginData();

    ajax_method.SearchPost(URL_CONST.GetSearchLoggin, con, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            pagin.OrderType = pagin.OrderType == 'asc' ? 'desc' : 'asc';

            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }            

            Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { data: 'LOGGING_TIME' },
                    { data: 'STATUS_DESC' },
                    { data: 'LEVEL_DESC' },                
                    { data: 'MESSAGE' }
                ],
                'columnDefs': [
                   {
                       "targets": 0,
                       "orderable": false
                   },
                    {
                        "targets": 1,
                        "orderable": false,
                        "className": "text-center"
                    },
                    {
                        "targets": 2,
                        "orderable": false,
                        "className": "text-center"
                    },
                    {
                        "targets": 3,
                        "orderable": false
                    }
                ],
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                fixedHeader: true
            });

            //var page = Controls.SearchResultTable.Pagin();
            //page.Init(pagin, sortingDetail, changePageDetail, 'itemPerPage');

            Controls.Pagin.Init(pagin, sortingDetail, changePageDetail, 'itemPerPage');
            Controls.SearchDataRow.show();
            Controls.BackButton.click(backToSearchLogmonitoring);
        } else {
            Controls.SearchDataRow.hide();
            Controls.Pagin.Clear();
            Controls.BackButton.click(backToSearchLogmonitoring);
        }

    }, null, true, 'SearchDataRow');
}
var sortingDetail = function (PaginData) {
    Controls.Pagin.ClearPaginData();
    SearchDetail();
}
var changePageDetail = function (PaginData) {  
    SearchDetail();
}

var OpenIndexByAppId = function (AppId) {    
    
    var con = getDefaultSceenValue();

    var url = URL_CONST.GetDetailPage;
    url += "?Det=sub";
    url += "&Module=" + con.SYSTEM_ID;
    url += "&App=" + con.APP_ID;
    url += "&Func=" +con.FUNCTION_ID;
    url += "&Stat=" +con.STATUS;
    url += "&User=" + con.USER_ID;
    url += "&From=" + con.DATE_START;
    url += "&To=" + con.DATE_TO;
    url += "&Fav=" + con.FAVORITE_FLAG;
    url += "&ByApp=" + AppId;

    window.open(url, "WFD_01010_Index_Loggin_STM_Fix_Asset");
}

var Clear = function () {
    Controls.SYSTEM.val(null);
    Controls.APP_ID.val(null);
    Controls.USER_ID.val(null);
    Controls.FROM.val(null);
    Controls.TO.val(null);
    Controls.FAVORITE.prop("checked", false);

    GetFunction(null);
    GetModuleLoggin(URL_CONST.GetModuleLoggin, Controls.SYSTEM, null);
    GetConfigurationLoggin(URL_CONST.GetStatusLoggin, Controls.STATUS, null);
    GetConfigurationLoggin(URL_CONST.GetLevelLoggin, Controls.LEVEL, null);

    Controls.SearchButton.click(function (e) {

        //$('#txtUserID').val(GetSearchCriteria($('#txtUserID')));
        Search();
    });
    Controls.ClearButton.click(Clear);
    Controls.BackButton.click(backToSearchLogmonitoring);
    Controls.SearchDataRow.hide();
}
var ClearDetail = function () {
    var FunctionId = GetUrlParameter("Func");
    var StatusId = GetUrlParameter("Stat");
    var AppId = GetUrlParameter("App");
    var UserId = GetUrlParameter("User");
    var From = GetUrlParameter("From");
    var To = GetUrlParameter("To");
    var Favorite = GetUrlParameter("Fav");
    var GetByAppId = GetUrlParameter("ByApp");

    Controls.SYSTEM.val(null);
    Controls.APP_ID.val(AppId);
    Controls.USER_ID.val(UserId);
    Controls.FROM.val(From);
    Controls.TO.val(To);
    Controls.FAVORITE.prop("checked", (Favorite == 'Y'));
    Controls.APP_DETAIL.val(GetByAppId);

    GetFunction(FunctionId);
    GetModuleLoggin(URL_CONST.GetModuleLoggin, Controls.SYSTEM);
    GetConfigurationLoggin(URL_CONST.GetStatusLoggin, Controls.STATUS, StatusId);
    GetConfigurationLoggin(URL_CONST.GetLevelLoggin, Controls.LEVEL, null);

    Controls.SearchButton.click(SearchDetail);
    Controls.ClearButton.click(ClearDetail);
    Controls.BackButton.click(backToSearchLogmonitoring);
    Controls.SearchDataRow.hide();

    SearchDetail();
}


//Shared script
var GetFunction = function (Selected_value) {
    ajax_method.Post(URL_CONST.GetFunction, '', true, function (result) {
        if (result != null) {
            Controls.FUNCTION.html('');
            var optionhtml1 = '<option value=""> All </option>';
            Controls.FUNCTION.append(optionhtml1);

            $.each(result, function (i) {
                var optionhtml = '<option value="' + result[i].FUNCTION_ID + '">' + result[i].FUNCTION_NAME + '</option>';
                Controls.FUNCTION.append(optionhtml);
            });

            if (Selected_value != null) {
                Controls.FUNCTION.val(Selected_value).change();
            } else {
                Controls.FUNCTION.val("").change();
            }
        }

    }, null);
}
var GetModuleLoggin = function (URL, hidden) {

    ajax_method.Post(URL_CONST.GetModuleLoggin, '', true, function (result) {
        if (result != null) {
            hidden.val(result[0].CODE);
        }
    }, null);

}
var GetConfigurationLoggin = function (URL, ComboBox, Selected_value) {
    ajax_method.Post(URL, '', true, function (result) {
        if (result != null) {

            ComboBox.html('');          
            ComboBox.append('<option value=""> All </option>');

            $.each(result, function (i) {
                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].VALUE + '</option>';
                ComboBox.append(optionhtml);
            });

            if (Selected_value != null) {
                ComboBox.val(Selected_value).change();
            } else {
                ComboBox.val("").change();
            }
        }
    }, null);

}
var GetUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var backToSearchLogmonitoring = function () {
    document.location.href = "/WFD01010";
};