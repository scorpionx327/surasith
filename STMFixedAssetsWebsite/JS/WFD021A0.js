﻿//SCREEN_PARAMS is parameter from main screen
$(function () {
    //Initialize
    $('#divSearchRequestResult').hide();
});
var control = {
    DataTable: null,
    Company: $('#WFD01170ddpCompany'), // Temp , still request best solution.
    ColumnIndex: {
        Sel: 0
    },
}

var fAction = {
    SelectedCostCenter: "",
    SelectedRespCostCenter: "",
    Initialize: function (callback) {
        if (callback != null)
            callback();
    },
    //Should define all request screen.
    LoadData: function (callback) {
        console.log("LoadData is callled");
        //Do here
        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        console.log(GLOBAL);
        console.log(Url.GetAsset);
        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
            console.log(datas);

            fAction.SelectedCostCenter = ""; //Clear Selected
            fAction.SelectedRespCostCenter = ""; //Clear Selected


            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                table.clear().draw().destroy();
            }


            //Set Screen Mode
            if (GLOBAL.DOC_NO == '' && datas.length == 0) {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            }
            if (GLOBAL.DOC_NO == '' && datas.length > 0) {
                fScreenMode.Submit_AddAssetsMode(function () {
                    fMainScreenMode.Submit_AddAssetsMode();
                });
            }

            if (datas == null || datas.length == 0) {
                $('#divSearchResult').hide();

                if (callback != null)
                    callback();

                return;
            }

            fAction.SelectedCostCenter = datas[0].COST_CODE;
            fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;

            _matchList = jQuery.grep(datas, function (a) {
                return a.AllowCheck == true;
            });


            $('#divSearchResult').show();

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            control.DataTable = $('#tbSearchResult').DataTable({
                "searching": false,
                data: datas,
                "rowCallback": function (row, data, index) {
                    if (data.STATUS == "ERROR") {
                        $('td', row).css('background-color', '#F9966B');
                    }
                },
                "columns": [
                    { "data": null, 'orderable': false },               // 0
                    { "data": "LINE_NO", 'orderable': false },          // 1
                    { "data": "PARENT_ASSET_NO", 'orderable': false },  // 2    
                    {
                        "data": "ASSET_DESC", 'orderable': false,
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    },       // 3
                    { "data": null, 'orderable': false },               // 4
                    { "data": null, 'orderable': false },               // 5
                    { "data": null, 'orderable': false },               // 6
                    { "data": null, 'orderable': false },               // 7                    
                    { "data": null, 'orderable': false },               // 8
                    { "data": null, 'orderable': false },               // 9
                    { "data": null, 'orderable': false },               // 10
                    { "data": null, 'orderable': false },               // 11
                    { "data": "ASSET_NO", 'orderable': false },         // 12
                    { "data": "ASSET_SUB", 'orderable': false, 'class': 'text-center' },        // 13
                    { "data": null, 'orderable': false },               // 14
                    { "data": null, 'orderable': false },               // 15
                    { "data": null, 'orderable': false },               // 16
                    { "data": null, 'orderable': false }                // 17
                ],
                'columnDefs': [
                    {
                        'targets': [1],
                        className: 'text-right'
                    },
                    {
                        'targets': 0,
                        "visible": (PERMISSION.AllowResend == "Y"),
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            console.log(data);
                            return fRender.CheckBoxRender(data);
                        }
                    },
                    {
                        'targets': 4,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.ASSET_CLASS == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.ASSET_CLASS_HINT + '">' + data.ASSET_CLASS + '</span>';
                        }
                    },
                    {
                        'targets': 5,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            if (data.MINOR_CATEGORY == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.MINOR_CATEGORY_HINT + '">' + data.MINOR_CATEGORY + '</span>';
                        }
                    },
                    {
                        'targets': 6,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.COST_CODE == null)
                                return '';
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    {
                        'targets': 7,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {

                            if (data.RESP_COST_CODE == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.RESP_COST_NAME_HINT + '">' + data.RESP_COST_CODE + '</span>';
                        }
                    },
                    {
                        'targets': 8,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            if (data.WBS_BUDGET == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.WBS_BUDGET_NAME + '">' + data.WBS_BUDGET + '</span>';
                        }
                    },
                    {
                        'targets': 9,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            if (data.LOCATION == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.LOCATION_HINT + '">' + data.LOCATION + '</span>';
                        }
                    },
                    {
                        'targets': 10,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.INVEST_REASON == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.INVEST_REASON_HINT + '">' + data.INVEST_REASON + '</span>';
                        }
                    },
                    {
                        'targets': 11,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.BOI_NO == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.BOI_NO_HINT + '">' + data.BOI_NO + '</span>';
                        }
                    },
                    {
                        'targets': 14,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.ButtonQuatationRender(data);
                            //return WFD02111_SetButtonQuotation(data);
                        }
                    },
                    {
                        'targets': 15,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.MESSAGE == null)
                                return data.STATUS;

                            return '<span data-toggle="tooltip" title="' + data.MESSAGE + '">' + data.STATUS + '</span>';
                        }
                    },
                    {
                        'targets': 16,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DeleteButtonRender(data);
                        }
                    },
                    {
                        'targets': 17,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DetailButtonRender(data);
                        }
                    }

                ],
                'order': [],
                paging: false,
                retrieve: true,
                "bInfo": false
            })


            console.log("Finish");

            if (callback == null)
                return;

            callback();

        }, null);


    },
    ShowSearchPopup: function (callback) {
    },
    Clear: function (callback) {

        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }
            fAction.LoadData(function () {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            });
            //  SuccessNotification(CommonMessage.ClearSuccess);

            if (callback == null)
                return;

            callback();
        });
    },
    Export: function (callback) {
        var batch = BatchProcess({
            BatchId: "LFD021A2",
            Description: "Export Fixed Asset Request",
            UserId: GLOBAL.EMP_CODE
        });
        batch.Addparam(1, 'GUID', GLOBAL.GUID);
        batch.Addparam(2, 'IsAECUser', fMainAction.getIsAECRole());
        batch.StartBatch();

        if (callback == null)
            return;

        callback();
    },
    AddAsset: function (_data, callback) { //It's call from dialog

        fAction.LoadData();

        if (callback == null)
            return;

        callback();

        /*
        console.log(Url.AddAsset);
        
        _data.GUID = GLOBAL.GUID;
        _data.USER_BY = GLOBAL.USER_BY;

        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.AddAsset, _data, false, function (result) {
            console.log(result);
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }
            
            fAction.LoadData();
           
            if (callback == null)
                return;

            callback();
        });
        */
    },
    DeleteAsset: function (_lineNo) {
        //console.log(_AssetNo);
        //console.log(Url.DeleteAsset);
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: control.Company.val(),
            LINE_NO: _lineNo
        }
        console.log(_data);
        ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
            if (result.IsError) {
                return;
            }
            fAction.LoadData();
            // SuccessNotification(CommonMessage.ClearSuccess);

        });
    },
    DownloadTemplate: function (callback) {
        //Under implement
    },
    UploadExcel: function (_fileName) {
        var batch = BatchProcess({
            BatchId: "BFD021A1",
            Description: "Upload New Asset Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'User', GLOBAL.USER_BY);
        batch.Addparam(5, 'FlowType', GLOBAL.FLOW_TYPE);
        batch.StartBatch(function (_appID, _Status) {

            if (_Status != 'S') {
                return;
            }
            fAction.LoadData();
        }, true);
    },
    PrepareGenerateFlow: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.PrepareFlow, GLOBAL, false, function (result) {
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }

            if (callback == null)
                return;

            callback();
        });
    },

    PrepareSubmit: function (callback) {
        //This request should update information from client to server before request approve
        var _data = fAction.GetClientAssetList(false);

        // Validate all

        //Data is independent YOU can design data with freee format 

        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            console.log(result);
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }

            if (callback == null)
                return;

            callback();
        });
    },
    PrepareApprove: function (callback) {
        //This request should update information from client to server before request approve

        //Data is independent YOU can design data with freee format
        var _data = fAction.GetClientAssetList(false);

        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            if (result.IsError) {
                fnErrorDialog("Prepare Approve Error", result.Message);
                return;
            }

            if (callback == null)
                return;

            callback();
        });
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },

    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");
        if (control.DataTable == null) {
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List

        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }

            var _bSel = $(this).find("input[name='chkGEN']").is(":checked");

            if (_requireCheck && !_bSel) // Required check but no selected 
                return;


            _row = control.DataTable.row(this).data(); // Get source data
            //  console.log(_row.COMPANY);
            //   console.log(_row);
            _assetList.push({
                GUID: GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                COMPANY: _row.COMPANY,
                LINE_NO: _row.LINE_NO
            });
        });

        return _assetList;
    },
    DetailCheck: function (sender) {
        var row = $(sender).closest("tr");

        $('#WFD021A0_SelectAll').prop('checked', _act = $('.chkGEN:checked').length == $('.chkGEN').length);
    },
    UpdateRow: function (_data) {
        console.log("UpdateRow");

        var _bFoundFlag = false;
        //_Company, _AssetNo, _AssetSub, _AmountPosted, _Percent
        $("#tbSearchResult tr").each(function () {

            if (_bFoundFlag)
                return;

            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data
            if (_row.COMPANY != control.CostPopup.COMPANY ||
                _row.ASSET_NO != control.CostPopup.ASSET_NO ||
                _row.ASSET_SUB != control.CostPopup.ASSET_SUB) {
                return;
            }

            if ($('#chkAll').is(':checked')) {
                _row.PERCENTAGE = null;
                _row.AMOUNT_POSTED = $('#txtCostAll').val();
            }
            if ($('#chkAmount').is(':checked')) {
                _row.PERCENTAGE = null;
                _row.AMOUNT_POSTED = $('#txtCostAmount').val();
            }
            if ($('#chkPercentage').is(':checked')) {
                _row.PERCENTAGE = $('#txtCostPercentage').val();
                _row.AMOUNT_POSTED = $('#txtCostPercentageAmount').val();
            }
            console.log("Update Row is success")
            _bFoundFlag = true;
        });

    },
    Open2115Popup: function (_lineno) {
        var _popupWidth = $(window).width() - 20;
        var left = ($(window).width() / 2) - (_popupWidth / 2);
        top = ($(window).height() / 2) - (600 / 2);

        var viewMode = (PERMISSION.AllowEditBeforeGenFlow == false) ? "Y" : "N";

        popup = window.open(MainUrl.NewAssetsPopup +
            "?GUID=" + GLOBAL.GUID +
            "&COMPANY=" + control.Company.val() +
            "&DOC_NO=" + GLOBAL.DOC_NO +
            "&LINE_NO=" + _lineno +
            "&FLOW_TYPE=" + GLOBAL.FLOW_TYPE +
            "&REQUEST_TYPE=" + GLOBAL.REQUEST_TYPE +
            "&ROLEMODE=" + $('#WFD01170ddpRequestorRole').val() +
            "&VIEW=" + viewMode,
            "popup", "width=" + _popupWidth + ", height=600, top=" + top + ", left=" + left + ", scrollbars=yes");

    },

    ShowDetailUploadDialog: function (button, _Type) {
        var row = control.DataTable.row($(button).parents('tr')).data();
        console.log('----------------');
        console.log(row.ASSET_NO);
        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: row.ASSET_NO == null ? "" : row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB == null ? "" : row.ASSET_SUB,
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: GLOBAL.FLOW_TYPE + '_' + _Type,
            FUNCTION_ID: "WFD021A0",
            DOC_UPLOAD: "WFD021A0" + "_" + GLOBAL.FLOW_TYPE + '_' + _Type,
            LINE_NO: row.LINE_NO,
            DEL_FLAG: GLOBAL.STATUS == '00' ? "Y" : "N"
        }

        $('#CommonUpload').appendTo("body").modal('show');

    },
    EnableMassMode: function (_Role) {
        //

    },
    Resend: function (callback) {
        //No this function
        var _data = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, _data, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList -> Send


    }
}

//Call this function when PG set screen mode
var fScreenMode = {


    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        $('#WFD021A0_SelectAll').prop('disabled', true).hide();

        $('#WFD01170btnResend').hide(); //Hide for Request new asset no, ACR use generate asset no button

    },

    Submit_InitialMode: function (callback) {

        //fScreenMode._GridVisible(false);

        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset').prop("disabled", false);

        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').removeClass('disabled').prop("disabled", false);


        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAddAsset, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').removeClass('disabled').prop("disabled", false);



        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) { //Call from main screen

        fScreenMode._ClearMode();

        console.log("#Approve_Mode");

        $('#btnRequestExport').show();
        $('#btnRequestExport').removeClass('disabled').prop("disabled", false);

        //Incase All is approve, system will show Sel column
        if (PERMISSION.AllowResend == "Y") {
            $('#btnGenerateAssetNo, #WFD021A0_SelectAll').show();
            $('#btnGenerateAssetNo, #WFD021A0_SelectAll').removeClass('disabled').prop("disabled", false);

        }
        else {
            $('#btnGenerateAssetNo, #WFD021A0_SelectAll').hide();
            $('#btnGenerateAssetNo, #WFD021A0_SelectAll').prop("disabled", true);

        }

        if (callback == null)
            return;
        callback();
    },
}

var fRender = {
    CheckBoxRender: function (data) {

        var _disabled = (data.AllowCheck ? '' : "disabled");

        var _strHtml = '';
        _strHtml = '<input style="width:20px;" class="chkGEN" value="' + data.LINE_NO + '" onclick="fAction.DetailCheck(this);" height:28px;" type="checkbox" ' + _disabled + ' >';
        return _strHtml;
    },
    HyperLinkRender: function (data) {
        return '<a href="../WFD02130?COMPANY=' + data.COMPANY + '&ASSET_NO=' + data.ASSET_NO + '&ASSET_SUB=' + data.ASSET_SUB + '" target="_blank">' + data.ASSET_NO + '</a>';
    },
    DeleteButtonRender: function (data) {

        if (!data.AllowDelete)
            return '';

        var _strHtml = '';
        _strHtml += '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"><button id="btnDelete_"' + data.LINE_NO + ' class="btn btn-danger btn-xs" data-title="Delete" type="button" ' +
            'onclick=fAction.DeleteAsset(\'' +
            data.LINE_NO + '\')>' +
            '<span class="glyphicon glyphicon-trash"></span></button></p>';
        _strHtml += '</div>';
        return _strHtml;
    },
    DetailButtonRender: function (data) {
        var _strHtml = '';

        _strHtml += '<p title="" data-original-title="Detail" data-toggle="tooltip" data-placement="top"> <button id="btn_Detail"' + data.LINE_NO + ' class="btn btn-success btn-xs" data-title="View Detail" type="button" ' +
            'onclick=fAction.Open2115Popup(\'' +
            data.LINE_NO + '\')>' +
            '<span class="glyphicon glyphicon-edit"></span></button></p>';
        _strHtml += '</div>';
        return _strHtml;
    },

    ButtonQuatationRender: function (data) {

        var _strHtml = '';
        _strHtml = '<div class="btn-group">' +
            '<button type="button" class="btn btn-info" style = "width: 100%;" ' + (data.QUOTATION == 'Y' || data.AllowEdit ? "" : "disabled") + ' onclick="fAction.ShowDetailUploadDialog(this,\'QUOTATION\')">' +
            '<span class="glyphicon glyphicon-search"></span></button>' +
            '</div>';
        return _strHtml;
    }
}
// Control Event

$('#btnRequestAddAsset').click(function () {

    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }

    fAction.Open2115Popup(0);
});

$('#btnGenerateAssetNo').click(function () {
    fMainAction.GenerateAssetNo();
})

$('#WFD021A0_SelectAll').click(function (e) {
    $('.chkGEN:enabled').prop('checked', $(this).is(':checked'));
})