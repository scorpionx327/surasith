﻿search2190Control = {
    Pagin: $('#WFD02190_SearchResultTable').Pagin(20),
    DataTable: null,
    //Search2190Config.ItemPerPage
    constant : {
        FAWindows: "FAW",
        FASup: "FAS"
    }
}

$(function () {
    $('#WFD02190Loading').hide();
    $('#WFD02190_SearchDataRow').hide();

    //setWFD02190ControlsRequiredField();
    if (Search2190Config.DefaultCostCode != null || Search2190Config.DefaultCostCode != '') {
        $('#WFD02190CostCenter').val(Search2190Config.DefaultCostCode);
    }
    $('#WFD02190_SearchAssets').on('hidden.bs.modal', function () {
        f2190SearchAction.ClearScreen();
    });
    $('#WFD02190_SearchAssets').on('shown.bs.modal', function () {
        //f2190SearchAction.ClearScreen();
        //$('#WFD02190CostCenter').focus();
        $('#WFD02190txtAssetNo').focus();
        $('#WFD02190chkOnlyParent').prop('checked', true);
    });

    $('.entersearch').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            $('#WFD02190Search').trigger('click');
            return false;
        }
    });   

    var TMPCostCenter = [];
    var TMPRespCostCenter = [];
    var TMPAssetClass = [];

    $('#WFD02190CostCenter').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
        source: function (query, process) {
            COST_CENTERS = [];
            $.post(UrlWFD02190.GetAutoCompleteCostCenter, { _keyword: query }, function (data) {
                console.log('WFD02190CostCenter');
                TMPCostCenter = data.ObjectResult;
                for (i = 0; i < data.ObjectResult.length; i++) {
                    var displayValue = data.ObjectResult[i].COST_CODE + ' - ' + data.ObjectResult[i].COST_NAME;
                    TMPCostCenter[i].displayValue = displayValue;
                    COST_CENTERS.push(displayValue);
                }

                process(COST_CENTERS);
            });
        },
        updater: function (item) {
            for (i = 0; i < TMPCostCenter.length; i++) {
                if (item === TMPCostCenter[i].displayValue) {
                    return TMPCostCenter[i].COST_CODE;
                }
            }
            return item;
        }
    });
    $('#WFD02190ResponsibleCostCenter').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
        source: function (query, process) {
            RESP_COST_CENTERS = [];
            $.post(UrlWFD02190.GetAutoCompleteRespCostCenter, { _keyword: query }, function (data) {
                console.log('WFD02190ResponsibleCostCenter');
                TMPRespCostCenter = data.ObjectResult;
                for (i = 0; i < data.ObjectResult.length; i++) {
                    var displayValue = data.ObjectResult[i].COST_CODE + ' - ' + data.ObjectResult[i].COST_NAME;
                    TMPRespCostCenter[i].displayValue = displayValue;
                    RESP_COST_CENTERS.push(displayValue);
                }

                process(RESP_COST_CENTERS);
            });
        },
        updater: function (item) {
            for (i = 0; i < TMPRespCostCenter.length; i++) {
                if (item === TMPRespCostCenter[i].displayValue) {
                    return TMPRespCostCenter[i].COST_CODE;
                }
            }
            return item;
        }
    });
    $('#WFD02190txtWBSProject').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
        source: function (query, process) {
            WBSCostProject = [];
            $.post(UrlWFD02190.GetAutoCompleteWBSProject, { COMPANY: GLOBAL.COMPANY, _keyword: query }, function (data) {
                console.log('WFD02190txtWBSProject');
                TMPCostProject = data.ObjectResult;
                for (i = 0; i < data.ObjectResult.length; i++) {
                    WBSCostProject.push(data.ObjectResult[i].WBS_CODE);
                }

                process(WBSCostProject);
            });
        }
    });
    $('#WFD02190txtWBSBudget').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
        source: function (query, process) {
            WBSBudgets = [];
            $.post(UrlWFD02190.GetAutoCompleteWBSBudget, { COMPANY: GLOBAL.COMPANY, _keyword: query }, function (data) {
                console.log('WFD02190txtWBSBudget');
                TMPBudgets = data.ObjectResult
                for (i = 0; i < data.ObjectResult.length; i++) {
                    WBSBudgets.push(data.ObjectResult[i].WBS_CODE);
                }

                process(WBSBudgets);
            });
        }
    });
    $('#WFD02190txtAssetPlateNo').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
        source: function (query, process) {
            ASSETPLATENO = [];
            map = {};
            $.post(UrlWFD02190.GetAutoCompleteAssetPlate, { _keyword: query }, function (data) {
                TMPAssetPlateNo = data.ObjectResult;
                for (i = 0; i < data.ObjectResult.length; i++) {
                    ASSETPLATENO.push(data.ObjectResult[i].INVEN_NO);
                }

                process(ASSETPLATENO);
            });
        }
    });
    $('#WFD02190txtAssetClass').typeahead({
        hint: true,
        highlight: true,
        minLength: 0,
        source: function (query, process) {
            ASSETCLASS = [];
            $.post(UrlWFD02190.GetAutoCompleteAssetClass, { _keyword: query }, function (data) {
                TMPAssetClass = [];
                for (i = 0; i < data.ObjectResult.length; i++) {
                    var row = data.ObjectResult[i];
                    if (row.CODE) {
                        row.displayValue = row.CODE + ' - ' + row.VALUE;
                        TMPAssetClass.push(row);
                        ASSETCLASS.push(row.displayValue);
                    }
                }
                process(ASSETCLASS);
            });
        },
        updater: function (item) {
            for (i = 0; i < TMPAssetClass.length; i++) {
                if (item === TMPAssetClass[i].displayValue) {
                    return TMPAssetClass[i].CODE;
                }
            }
            return item;
        }
    });
});


var f2190SearchAction = {
    ClearDefault:function(){

        Search2190Config.RoleMode = '';
        Search2190Config.searchOption = '';
        Search2190Config.AllowMultiple = true;

        Search2190Config.Default = {
            Company: { Value: "", Enable: true },
            CostCode: { Value: "", Enable: true },
            ResponsibleCostCode: { Value: "", Enable: true }, // Surasith change to FS in the future
            INVEST_REASON: { Value: "", Enable: true },
            Parent: { Value: "", Enable: true },
            AssetClass: { Value: "", Enable: true },
            AssetGroup: { Value: "", Enable: true },
            WBSProject: { Value: "", Enable: true },
            WBSBudget: { Value: "", Enable: true }
        }; 
            
    },
    _SetDefault: function () {
        
        if (Search2190Config.RoleMode ==''){
            alert("Please set Role Mode (WFD02190)");
            throw new Error('Please set Role Mode (WFD02190)');
        }
        // Control, Value, status
        $('#WFD02190MultipleBoxCompany').val(Search2190Config.Default.Company.Value);
        $('#WFD02190MultipleBoxCompany').prop("disabled", !Search2190Config.Default.Company.Enable);

        //Comment 
        //if (Search2190Config.RoleMode == search2190Control.constant.FAWindows) {
        $('#WFD02190CostCenter').val(Search2190Config.Default.CostCode.Value);
        $('#WFD02190CostCenter').prop("disabled", !Search2190Config.Default.CostCode.Enable);
        //}
        //if (Search2190Config.RoleMode == search2190Control.constant.FAWindows || 
        //    Search2190Config.RoleMode == search2190Control.constant.FASup) {
        
        $('#WFD02190ResponsibleCostCenter').val(Search2190Config.Default.ResponsibleCostCode.Value);
        $('#WFD02190ResponsibleCostCenter').prop("disabled", !Search2190Config.Default.ResponsibleCostCode.Enable);
        //}
        $('#WFD02190cmbBOI').select2("val", Search2190Config.Default.INVEST_REASON.Value);
        $('#WFD02190cmbBOI').prop("disabled", !Search2190Config.Default.INVEST_REASON.Enable);

        $('#WFD02190chkOnlyParent').prop('checked', Search2190Config.Default.Parent.Value == "Y");
        $("#WFD02190chkOnlyParent").prop('disabled', !Search2190Config.Default.Parent.Enable);

        $('#WFD02190txtAssetSub').val('0000').prop('readonly', true);

        $('#WFD02190txtAssetClass').val(Search2190Config.Default.AssetClass.Value);
        $('#WFD02190txtAssetClass').prop("disabled", !Search2190Config.Default.AssetClass.Enable);

        $('#WFD02190cmbAssetGroup').select2("val",Search2190Config.Default.AssetGroup.Value);
        $('#WFD02190cmbAssetGroup').prop("disabled", !Search2190Config.Default.AssetGroup.Enable);


        $('#WFD02190txtWBSProject').val(Search2190Config.Default.WBSProject.Value);
        $('#WFD02190txtWBSProject').prop("disabled", !Search2190Config.Default.WBSProject.Enable);

        $('#WFD02190txtWBSBudget').val(Search2190Config.Default.WBSBudget.Value);
        $('#WFD02190txtWBSBudget').prop("disabled", !Search2190Config.Default.WBSBudget.Enable);

    },
    Initialize: function () {
        
        f2190SearchAction.ClearScreen();

        f2190SearchAction._SetDefault();

        f2190SearchAction.LoadLocation();

        $('#lblWFD02190SearchTitle').html("Fixed Assets Search Dialog");
        if (Search2190Config.searchOption == "CNS") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search Source Assets For Reclassification");
        }
        if (Search2190Config.searchOption == "CND") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search Target Assets For Reclassification");
        }
        if (Search2190Config.searchOption == "CN") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search Assets For Reclassification");
        }

        if (Search2190Config.searchOption == "GN") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search Assets For Info. Change");
        }

        if (Search2190Config.searchOption == "KN") //New Asset No for Settlement
        {
            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
        }

    },
    _GetSearchParameter : function(){
        return {
            GUID : GLOBAL.GUID,
            REQUEST_TYPE: GLOBAL.REQUEST_TYPE,
            ACT_ROLE: Search2190Config.RoleMode,
            COMPANY: $("#WFD02190MultipleBoxCompany").val(),
            ASSET_NO: $("#WFD02190txtAssetNo").val(),
            ASSET_SUB: $("#WFD02190txtAssetSub").val(),
            ASSET_NAME: $("#WFD02190txtAssetName").val(),
            ASSET_CLASS: $("#WFD02190txtAssetClass").val(),
            ASSET_GROUP: $("#WFD02190cmbAssetGroup").val(),
            WBS_PROJECT: $("#WFD02190txtWBSProject").val(),
            WBS_BUDGET: $("#WFD02190txtWBSBudget").val(),
            ASSET_PLATE_NO: $("#WFD02190txtAssetPlateNo").val(),
            ONLY_PARENT_ASSETS: $("#WFD02190chkOnlyParent").is(':checked') ? "Y" : "N",
            COST_CODE: $("#WFD02190CostCenter").val(),
            LOCATION: $("#WFD02190ddpLocation").val(),
            RESP_COST_CODE: $("#WFD02190ResponsibleCostCenter").val(),
            INVEST_REASON: $("#WFD02190cmbBOI").val(),
            DATE_IN_SERVICE_START: $("#WFD02190CapitalizeDateFrom").val(),
            DATE_IN_SERVICE_TO: $("#WFD02190CapitalizeDateTo").val(),
            searchOption: Search2190Config.searchOption,
            //HasCostValue: Search2190Config.HasCostValue ? true : false 
        };
    },
    LoadLocation: function (_default) {
        ajax_method.Post('/Common/Location_AutoComplete', { COMPANY: $('#WFD02190MultipleBoxCompany').val() }, true, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02190ddpLocation').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
            //Set Default
            $('#WFD02190ddpLocation').select2('val', '');
        });
    },
    Search: function () {
        ClearMessageC();
        //search2190Control.Pagin.ClearPaginData();

        var _data = f2190SearchAction._GetSearchParameter();
        var pagin = search2190Control.Pagin.GetPaginData();
        console.log("Search");
        console.log(_data);
        $('#WFD02190Loading').show();
        console.log(Search2190Config.SearchUrl);
        ajax_method.SearchPost(Search2190Config.SearchUrl, _data, pagin, true, function (datas, pagin) {
            
            if (datas === null || datas.length === 0) {
                //control.SearchDataRow.hide();
                search2190Control.Pagin.Clear();
                $('#WFD02190Loading, #WFD02190_SearchDataRow').hide();
                return ;
            }
            //WFD02190SetParmeter(datas);

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#WFD02190_SearchResultTable'))) {
                var table = $('#WFD02190_SearchResultTable').DataTable();
                table.destroy();
            }
            search2190Control.DataTable = $('#WFD02190_SearchResultTable').DataTable({
                data: datas,
                "columns": [
                    { "data": null },
                    { "data": "ASSET_NO" },
                    { "data": "ASSET_SUB"},
                    { "data": "ASSET_NAME", 'orderable': false },
                    { "data": null }, //Asset class
                    { "data": null }, //Cost Code 
                    { "data": null }, //Response Cost Center 
                    { "data": null, 'orderable': false }, //WBS
                    { "data": null, 'orderable': false }, //BOI
                    { "data": null }, // MINOR CATE
                    { "data": null, 'orderable': false }, //TAX Priviledge
                    { "data": "LOCATION", 'orderable': false },
                    { "data": "DATE_IN_SERVICE", 'orderable': false },
                    { "data": null, 'orderable': false },
                    //{ "data": "MACHINE_LICENSE" } //Machine License
                ],
                'columnDefs': [{
                    'targets': 0,
                    'orderable': false,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.CheckBoxRender(data);
                    }
                },
                {
                    'targets': 1,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        var _text = full.ASSET_NO;
                        if (full.MACHINE_LICENSE) {
                            _text = full.ASSET_NO + "<span style=\"padding-left:3px;color:red;font-weight:bold\">*</span>";
                        }
                        return _text;
                    }
                },
                {
                    'targets': 2,
                    'className': 'text-center'
                },
                {
                    'targets': 3,
                    'className': 'text-left',
                    'render': function (data, type, full, meta) {
                        if (data === null) {
                            return data;
                        }
                        if (data.length <= 25) {
                            return data;
                        }

                        return fSearch2190Render.TooltipRender(data.substring(0, 25), data);
                    }
                },
                {
                    'targets': 4,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.ASSET_CLASS, data.ASSET_CLASS_HINT);
                    }
                },
                {
                    'targets': 5,
                    'className': 'text-left',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.COST_CODE, data.COST_NAME_HINT);
                    }
                },
                {
                    'targets': 6,
                    'className': 'text-left',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.RESP_COST_CODE, data.RESP_COST_NAME_HINT);
                    }
                },
                {
                    'targets': 7,
                    'className': 'text-left',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.WBS_PROJECT, data.WBS_PROJECT_HINT);
                    }
                },
                {
                    'targets': 8,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.INVEST_REASON, data.INVEST_REASON_HINT);
                    }
                },
                {
                    'targets': 9,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.MINOR_CATEGORY, data.MINOR_CATEGORY_HINT);
                    }
                },
                {
                    'targets': 10,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.BOI_NO, data.BOI_NO_HINT);
                    }
                },
                {
                    'targets': 11,
                    'className': 'text-center',
                },
                {
                    'targets': 12,
                    'className': 'text-center',
                },
                {
                    'targets': 13,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.HyperLinkRender(data);
                    }
                },
                //{
                //    'targets': 14,
                //    'className': 'text-center',
                //},

                //{'targets': [10, 11, 12, 13, 14], className: 'text-center' }
                ],
                //scrollY: "400px",
                //scrollX: true,
                "paging": false,
                searching: false,
                'order': [],
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            });

            //setTimeout(function () {
            //    search2190Control.DataTable.columns.adjust();
            //}, 1);

            search2190Control.Pagin.Init(pagin, WFD02190sorting, WFD02190changePage, 'itemPerPage');

            //control.SearchDataRow.show();
            $('#WFD02190_SearchDataRow').show();
            $('#WFD02190_selectAll').prop('checked', false);
            $('#WFD02190Loading').hide();
        }, null);

        
    },
    ClearScreen: function () {
        // Textbox
        $('#WFD02190_SearchAssets .entersearch:enabled').val('');
        $('#WFD02190_SearchAssets .select2:enabled').select2('val','');
        $('#WFD02190ddpLocation').select2('val', '');

        $('#AlertMessageWFD02190').html('');
        
        if ($.fn.DataTable.isDataTable($('#WFD02190_SearchResultTable'))) {
            var table = $('#WFD02190_SearchResultTable').DataTable();
            table.destroy();
        }
        $('#WFD02190_SearchDataRow').hide();
      
        
        $('#AlertMessageWFD02190').html('');

        if (!$('#WFD02190chkOnlyParent').is(':disabled'))
        {
            $('#WFD02190chkOnlyParent').prop('checked', Search2190Config.Default.Parent.Value == "Y");
            $("#WFD02190chkOnlyParent").prop('disabled', !Search2190Config.Default.Parent.Enable);
        }

        f2190SearchAction._SetDefault();

    },
    CloseScreen: function () {
        f2190SearchAction.ClearScreen();
        $('#WFD02190_SearchAssets').modal('hide');
    },
    SelectAll: function (_chk) {
        $('#WFD02190_SearchResultTable').find('[name="sel"]').each(function (e) {
            $(this).prop('checked', _chk);
        });
    },
    _GetClientAssetList: function () {
        console.log("GetClientAssetList");
        if (search2190Control.DataTable == null) {
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List

        $("#WFD02190_SearchResultTable tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            //Check checkbox is checked.
            var _chk = $(this).find("input[name='sel']").is(':checked');
            if (!_chk) //no select
                return;
            var _row = [];

            _row = search2190Control.DataTable.row(this).data(); // Get source data

            _assetList.push(_row);
        });

        return _assetList;
    },

    Save: function () {
        //Load Selected Parameter and Check and Send to Main screen
        var _asset = f2190SearchAction._GetClientAssetList();
        try
        {
            var _bValid = true;

            if (_asset.length == 0) {
                //$('#AlertMessageWFD02190').html(Search2190Message.NoSelected);
                AlertTextErrorMessagePopup(Search2190Message.NoSelected, "#AlertMessageWFD02190", "#WFD02190_SearchAssets");
                return;
            }
            if (!Search2190Config.AllowMultiple && _asset.length > 1) {
                //$('#AlertMessageWFD02190').html(Search2190Message.SelectOneRecord);
                AlertTextErrorMessagePopup(Search2190Message.SelectOneRecord, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');
                return;
            }
            if (Search2190Config.AllowMultiple) {
                var _cnt = 0;

               // if (Search2190Config.RoleMode == search2190Control.constant.FAWindows) {
                    //Not allow mix 1 CC / 1 RC
                    var _tCC = "", _tRC = "";
                    _cnt = 0;
                    _asset.forEach(function (item) {
                        if (item.COST_CODE != _tCC || item.RESP_COST_CODE != _tRC) {
                            _tCC = item.COST_CODE;
                            _tRC = item.RESP_COST_CODE;
                            _cnt++;
                        }
                        if(_cnt > 1)
                        {
                            _bValid = false;
                            AlertTextErrorMessagePopup(Search2190Message.MixCostCenterAndResponseCostCenter, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');
                            return;
                        }
                    });
                //}
                //if (Search2190Config.RoleMode == search2190Control.constant.FASup) {
                //    //Not allow mix 1 CC / 1 RC
                //    _cnt = 0;
                //    var  _tRC = "";
                //    _asset.forEach(function (item) {
                //        if (item.RESP_COST_CODE != _tRC) {
                //            _tRC = item.RESP_COST_CODE;
                //            _cnt++;
                //        }
                //    });
                //    if (_cnt > 1) {
                //        _bValid = false;
                //        AlertTextErrorMessagePopup(Search2190Message.MixResponseCostCenter, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');
                //        return;
                //    }
                //}

                if (Search2190Config.searchOption == "K") {
                    _cnt = 0;
                    var _wbs = "";
                    _asset.forEach(function (item) {
                        if (item.WBS_BUDGET != _wbs) {
                            _wbs = item.WBS_BUDGET;
                            _cnt++;
                        }

                    });
                    if (_cnt > 1) {
                        _bValid = false;
                        AlertTextErrorMessagePopup(Search2190Message.MixWBSBudget, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');
                        return;
                    }
                }
            }

            if (!_bValid)
                return;

            //Exists in other requesst (except reprint)
            var _msg = [{ Type: 3, Messages: [] }];
            var _found = false;
            $.each(_asset, function (key, obj) {
                if (obj.ALLOW_CHECK == "N") {
                    _found = true;
                    _msg[0].Messages.push(Search2190Message.UnderRequest.format("Asset No : " + obj.ASSET_NO + " Sub :" + obj.ASSET_SUB, obj.STATUS));
                }
            });

            if (_found){
                //$('#AlertMessageWFD02190').html(_msg);
                AlertMessageCPopup(_msg, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');
                return;
            }

            //CHECK Impairment NOT ALLOW ASSET CHILD PROCESS
            //if (Search2190Config.searchOption === "IM") {
            //    var assetNos = [];
            //    $.each(_asset, function (key, obj) {
            //        assetNos.push(obj.ASSET_NO);
            //    });

            //    var valid = true;

            //    ajax_method.Post(UrlWFD02190.AddAssetListValidation2710, { assetNos: assetNos.join('#') }, false, function (result) {
            //        if (result && result.length > 0) {

            //            var _m = [{ Type: 3, Messages: [] }];
            //            $.each(result, function (key, obj) {
            //                _m[0].Messages.push(obj.MESSAGE_TEXT);
            //            });
            //            AlertMessageCPopup(_m, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');

            //            valid = false;
            //        }
            //    });

            //    if (!valid) return;
            //}

            //Recheck ?
            if (Search2190Config.customCallback != null) {

                Search2190Config.customCallback(_asset, function () {
                    $('#WFD02190_SearchAssets').modal('hide');
                });
                return;
            }
            fAction.AddAsset(_asset, function () {
                $('#WFD02190_SearchAssets').modal('hide');
            });
        }
        catch(e)
        {
            console.error(e);
        }
    },
    DetailCheck: function () {
        var numberOfChecked = $('#WFD02190_SearchResultTable [name="sel"]:checked').length;
        var totalCheckboxes = $('#WFD02190_SearchResultTable [name="sel"]').length;
        $('#WFD02190_SearchResultTable #WFD02190_selectAll').prop('checked', totalCheckboxes > 0 && totalCheckboxes == numberOfChecked);

    }

}
var fSearch2190Render = {
    CheckBoxRender: function (data) {
        var _strHtml = '';
        var _strDisable = '';
        var _strChecked = '';
        _strHtml = '<input name="sel" style="" type="checkbox"' + _strDisable + ' ' + _strChecked + ' >';
        return _strHtml;
    },
    TooltipRender: function (display, toolTipText) {
        var _strHtml = '';
        if (toolTipText == null || toolTipText.length == 0) {
            _strHtml = display;
            return _strHtml;
        }

        if (display == null)
            display = '';

        _strHtml = '<p title="" data-original-title="' + replaceAll(toolTipText, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + display + '</p>'
        return _strHtml;
    },
    HyperLinkRender: function (data) {

        if (data.STATUS == null)
            return '';

        if (data.REF_DOC_NO == null || data.REF_DOC_NO == "")
            return data.STATUS ;

        return '<a href="../WFD01170?DocNo=' + data.REF_DOC_NO + '" target="_blank">' + data.STATUS + '</a>';
    },

}

//Event Control
$('#WFD02190Search').click(function (e) {

    ClearMessageC();
    search2190Control.Pagin.ClearPaginData();
    
    f2190SearchAction.Search();
}),
$('#WFD02190Clear').click(function (e) {
    f2190SearchAction.ClearScreen();
})
$('#WFD02190Close').click(function (e) {
    f2190SearchAction.CloseScreen();
})
$('#WFD02190OK').click(function (e) {
    f2190SearchAction.Save();
})
$('#WFD02190Cancel').click(function (e) {
    f2190SearchAction.CloseScreen();
})
$('#WFD02190_selectAll').change(function (e) {
   // alert("XXXX");
    var chk = $(this).is(':checked');
    f2190SearchAction.SelectAll(chk);
})



var WFD02190sorting = function (PaginData) {    
    f2190SearchAction.Search();
}

// Callback function after change pagination on table
var WFD02190changePage = function (PaginData) {
    f2190SearchAction.Search();
}


var getCompanyFromAnyPage = function (value) {
    $('#WFD02190MultipleBoxCompany').val(value);
}


$('#WFD02190chkOnlyParent').click(function () {
    if ($(this).prop('checked')) {
        $('#WFD02190txtAssetSub').val('0000').prop('readonly', true);
    } else {
        $('#WFD02190txtAssetSub').val('').prop('readonly', false);
    }
});


// Auto complete
var searchHelper = {
    assetClass: typeaheadSearchHelper(UrlWFD02190.GetAutoCompleteAssetClass, 'CODE', 'VALUE', true),
    plateNo: typeaheadSearchHelper(UrlWFD02190.GetAutoCompleteAssetPlate, 'INVEN_NO', 'INVEN_NO', false),
    wbsBudget: typeaheadSearchHelper(UrlWFD02190.GetAutoCompleteWBSBudget, 'WBS_CODE', 'DESCRIPTION', true),
    wbsCostProject: typeaheadSearchHelper(UrlWFD02190.GetAutoCompleteWBSProject, 'WBS_CODE', 'DESCRIPTION', true),
    responsibleCostCenter: typeaheadSearchHelper(UrlWFD02190.GetAutoCompleteRespCostCenter, 'COST_CODE', 'COST_NAME', true),
    costCenter: typeaheadSearchHelper(UrlWFD02190.GetAutoCompleteCostCenter, 'COST_CODE', 'COST_NAME', true)
};

$('#WFD02190txtAssetClass').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.assetClass.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.assetClass.updateValue
});

$('#WFD02190txtAssetPlateNo').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.plateNo.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    }
});

$('#WFD02190txtWBSBudget').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.wbsBudget.loadData({ COMPANY: $('#WFD02190MultipleBoxCompany').val(), _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.wbsBudget.updateValue
});

$('#WFD02190txtWBSProject').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.wbsCostProject.loadData({ COMPANY: $('#WFD02190MultipleBoxCompany').val(), _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.wbsCostProject.updateValue
});

$('#WFD02190ResponsibleCostCenter').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.responsibleCostCenter.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.responsibleCostCenter.updateValue
});

$('#WFD02190CostCenter').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.costCenter.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.costCenter.updateValue
});