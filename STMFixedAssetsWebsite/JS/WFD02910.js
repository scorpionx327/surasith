﻿var control = {
    DataTable: null,
    Company: $('#WFD01170ddpCompany'), // Temp , still request best solution.
    CostPopup: {
        ASSET_NO: "",
        ASSET_SUB: "",
        COMPANY: "",
        PERCENTAGE: null,
        AMOUNT_POSTED: null,
        BF_CUMU_ACQU_PRDCOST_01: null
    },
    ColumnIndex: {
        Sel: 0,
        ToAsset: 8,
        Cost: 13,
        BosDoc: 16
    },
    TMCAttachCount: 0,
    AllowUploadTMC: false,
    HasMachineLicense: false,
    AllowFSUser: PERMISSION.IsFSUser === 'Y' && PERMISSION.IsFSState === 'Y'
}
var constants = {
    TMC: "TMC"
}

$(function () {
    $('#divSearchRequestResult').hide();
});
var fAction = {
    SelectedSourceCC: "", //Find when load data
    SelectedSourceRCC: "", //Find when load data
    SelectedTargetCC: "", //Find when click existing / new
    SelectedTargetRCC: "",  //Find when click existing / new
    //Should define all request screen.

    Initialize: function (callback) {
        if (callback != null)
            callback();
    },

    LoadData: function (callback) {
        //console.log("LoadData is callled");
        //Do here
        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        //console.log(GLOBAL);
        //console.log(Url);

        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
            //   console.log(datas);
            fAction.SelectedCostCenter = ""; //Clear Selected

            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                table.clear().draw().destroy();
            }
            //debugger;
            //Incase New Mode

            if (datas == null || datas.length == 0) {
                $('#divSearchRequestResult').hide();
                fAction.SelectedSourceCC = "";
                fAction.SelectedSourceRCC = "";

                if (callback != null)
                    callback();

                return;
            } else {
                $('#divSearchRequestResult').show();
            }

            //For Search Dialog
            fAction.SelectedSourceCC = datas[0].COST_CODE;
            fAction.SelectedSourceRCC = datas[0].RESP_COST_CODE;


            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            // To be discussion
            _matchList = jQuery.grep(datas, function (a) {
                return a.AllowCheck == true && a.REQ_GEN == "Y";
            });

            var _bAllowGenerateAssetNo = (_matchList.length > 0);
            var _bBOI = (PERMISSION.IsBOIState == "A" && (PERMISSION.IsBOIUser == "Y" || PERMISSION.IsAECUser == "Y")) || PERMISSION.IsBOIState == "Y";


            if (_bAllowGenerateAssetNo && PERMISSION.AllowGenAssetNo == "Y") {
                $('#btnGenerateAssetNo').show();
                $('#btnGenerateAssetNo').prop("disabled", false);
            }
            else {
                $('#btnGenerateAssetNo').hide();
                $('#btnGenerateAssetNo').prop("disabled", true);
            }

            //CHECK HAS MACHINE_LICENSE AD LEAST ONE
            var machineLicenses = $.grep(datas, function (i) {
                return i.IS_MACHINE_LICENSE === "Y";
            });
            control.HasMachineLicense = machineLicenses.length > 0;

            control.DataTable = $('#tbSearchResult').DataTable({
                data: datas,
                "columns": [
                    { "data": null, 'orderable': false }, // 0
                    { "data": null, 'orderable': false, "class": "text-right" }, // 1LINE_NO
                    { "data": null, 'orderable': false }, // Asset No 2
                    { "data": "ASSET_SUB", 'orderable': false }, // 3
                    { "data": null, 'orderable': false }, // Asset Class 4
                    { "data": "DATE_IN_SERVICE", 'orderable': false, "class": "text-center" }, // 5
                    {
                        "data": "ASSET_NAME", 'orderable': false,
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    }, // 6
                    { "data": null, 'orderable': false }, // 7
                    { "data": null, 'orderable': false }, // Button 8
                    { "data": null, 'orderable': false }, // Asset No 9
                    { "data": "NEW_ASSET_SUB", 'orderable': false }, // 10
                    { "data": null, 'orderable': false }, // Asset Class New 11
                    {
                        "data": "NEW_ASSET_NAME", 'orderable': false,
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    }, // 12
                    { "data": null, 'orderable': false }, // Amount 13
                    { "data": "DOCUMENT_DATE", 'orderable': false, 'className': 'text-center' }, // 14
                    { "data": null, 'orderable': false }, // 15
                    { "data": null, 'orderable': false }, // BOI 16
                    { "data": null, 'orderable': false }, // 17
                    { "data": "STATUS", 'orderable': false, "class": "text-center" }, // 18
                    { "data": null, 'orderable': false } // 19
                ],
                'columnDefs': [
                    {
                        'targets': 0,
                        'searchable': false,
                        "visible": (PERMISSION.AllowResend == "Y" || _bAllowGenerateAssetNo),
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CheckBoxRender(data);
                        }
                    }, // CheckBox
                    {
                        'targets': 1, //ROW_SEQ
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return fRender.LableRowSeq(full);
                        }
                    },
                    {
                        'targets': 2, //ASSET_NO Show hyperlink
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return fRender.HyperLinkRender(full);
                        }
                    },//Asset No  
                    {
                        'targets': 4,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            if (data.ASSET_CLASS == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.ASSET_CLASS_HINT + '">' + data.ASSET_CLASS + '</span>';
                        }
                    }, //COST_VALUE
                    {
                        'targets': 7,
                        'orderable': false,
                        'className': 'text-right',
                        "render": function (data, type, full, meta) {
                            if (data.COST_VALUE == null)
                                return '';
                            return fRender.commaSeparateNumber(data.COST_VALUE);
                        }
                    },
                    {
                        'targets': 8, // Button
                        'searchable': false,
                        'orderable': false,
                        "visible": (PERMISSION.DOC_STATUS == "00"),
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.TargetAssetButtonRender(full);
                        }

                    }, // Button
                    {
                        'targets': 9, //AMOUNT_POSTED
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            // console.log("AMOUNT_POSTED");
                            //  console.log(full);
                            return fRender.NewAssetHyperLinkRender(full);
                        }

                    }, // New Asset No
                    {
                        'targets': 11,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            // console.log(data);
                            if (data.NEW_ASSET_CLASS == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.NEW_ASSET_CLASS_HINT + '">' + data.NEW_ASSET_CLASS + '</span>';
                        }
                    },
                    {
                        'targets': 13, //Amount
                        'searchable': false,
                        'orderable': false,

                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.ButtonRender(full);
                        }

                    }, //BOIDocument
                    {
                        'targets': 15,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.INVEST_REASON == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.INVEST_REASON_HINT + '">' + data.INVEST_REASON + '</span>';
                        }
                    },
                    {
                        'targets': 16,
                        'searchable': false,
                        'orderable': false,
                        'visible': (_bBOI),
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.ButtonBoiDocumentRender(full);
                        }

                    }, //Reason
                    {
                        'targets': 17,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.TextBoxRender(full);
                        }
                    },
                    {//STATUS
                        'targets': 18,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            if (full.ASSET_STATUS !== "ERROR")
                                return full.STATUS;

                            return '<span data-toggle="tooltip" data-html="true" title="<div style=&quot;min-width:80px;&quot;>' + full.ASSET_MESSAGE + '</div>">' + full.ASSET_STATUS + '</span>';
                        }

                    }, //Remark
                    {
                        'targets': 19, //Delete
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DeleteButtonRender(full);
                        } // DeleteButtonRender

                    }],
                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                fixedHeader: {
                    header: true,
                    footer: false
                },
                "initComplete": function (settings, json) {
                    console.log('DataTables has finished its initialisation.');
                },
                "createdRow": function (row, data, index) {
                    console.log("createdRow");
                },
                "rowCallback": function (row, data, index) {
                    if (data.ASSET_STATUS === "ERROR") {
                        $(row).css('background-color', '#F9966B');
                    }
                }
            }).columns.adjust().draw();

            $('#divSearchRequestResult').show();

            if (GLOBAL.DOC_NO == '') {
                if (datas.length == 0) {
                    fScreenMode.Submit_InitialMode(function () {
                        fMainScreenMode.Submit_InitialMode();
                    });
                }
                if (datas.length > 0) {
                    fScreenMode.Submit_AddAssetsMode(function () {
                        fMainScreenMode.Submit_AddAssetsMode();
                    });
                }
            }

            if (callback == null)
                return;

            callback();

        }, null);
    },
    Clear: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }
            fAction.LoadData(function () {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            });
            //  SuccessNotification(CommonMessage.ClearSuccess);

            if (callback == null)
                return;

            callback();
        });
    },

    Export: function (callback) {
        fAction.UpdateAsset(function () {
            var batch = BatchProcess({
                BatchId: "LFD02912",
                Description: "Export Reclassification Request",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'GUID', GLOBAL.GUID);
            batch.Addparam(2, 'DOC_NO', GLOBAL.DOC_NO);
            batch.StartBatch();

            if (callback == null)
                return;

            callback();
        });
    },



    AddAsset: function (_list, callback) { //It's call from dialog

        //console.log(Url.AddAsset);

        if (_list == null || _list.length == 0)
            return;

        _list.forEach(function (_data) {
            // do something with `item`
            _data.GUID = GLOBAL.GUID;
            _data.USER_BY = GLOBAL.USER_BY;
        });


        //Post: Validation function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.AddAsset, _list, false, function (result) {
            console.log(result);
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }

            fAction.LoadData();

            if (callback == null)
                return;

            callback();

        });

    },
    UpdateAsset: function (callback) {
        var _data = fAction.GetClientAssetList(false); // Get All
        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            if (result.IsError) {
                //fnErrorDialog("UpdateAssetList", result.Message);
                return;
            }
            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList

    },
    DeleteAsset: function (assetNo, AssetSub, _LineNo) {

        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: control.Company.val(),
            LINE_NO: _LineNo
        }
        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(assetNo, AssetSub), function (result) {
            if (!result) {
                return;
            }
            fAction.UpdateAsset(function () {
                ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
                    if (result.IsError) {
                        return;
                    }
                    console.log("Delete");
                    fAction.LoadData();
                    // SuccessNotification(CommonMessage.ClearSuccess);
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Deletion process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                });
            }); // UpdateAsset 
        }); // loadConfirmAlert

    },
    DeleteTargetAsset: function (sender, id) {
        //Delete On Server if New
        var row = control.DataTable.row($(sender).parents('tr')).data();

        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: control.Company.val(),
            LINE_NO: row.LINE_NO,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB
        }
        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(row.ASSET_NO, row.ASSET_SUB), function (result) {
            if (!result) {
                return;
            }

            ajax_method.Post("/WFD02910/DeleteTargetAsset", _data, false, function (result) {
                if (result.IsError) {
                    return;
                }
                console.log("Delete");

                var _selectedRow = fAction.GetSelectedRow(row.LINE_NO);
                if (_selectedRow == null)
                    return;

                var _obj = control.DataTable.row(_selectedRow).data();

                _obj.NEW_ASSET_NO = null;
                _obj.NEW_ASSET_SUB = null;
                _obj.NEW_ASSET_NAME = null;
                _obj.NEW_ASSET_CLASS = null;
                _obj.NEW_ASSET_CLASS_HINT = null;
                _obj.NEW_COST_CODE = null;
                _obj.NEW_RESP_COST_CODE = null;

                control.DataTable.row(_selectedRow).data(_obj).draw(false);

                $.notify({
                    icon: 'glyphicon glyphicon-ok',
                    message: "Deletion process is completed."
                }, {
                    type: 'success',
                    delay: 500,
                });

                $("#btnAmountPosted_" + id).html('...');
            });

        }); // loadConfirmAlert

    },
    UploadExcel: function (_fileName) {
        //call Batch with filename
        var batch = BatchProcess({
            BatchId: "BFD02911",
            Description: "Upload Reclassificationt Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function (_appID, _Status) {

            if (_Status != 'S') {
                return;
            }
            fAction.LoadData();
        }, true);


    },

    PrepareGenerateFlow: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)

        fAction.UpdateAsset(function () {
            GLOBAL.FLOW_TYPE = 'CN';
            ajax_method.Post(Url.PrepareFlow, { data: GLOBAL }, false, function (result) {
                if (result.IsError) {
                    //fnErrorDialog("PrepareFlow", result.Message);
                    return;
                }

                if (callback == null)
                    return;

                callback();
            }); //ajax_method.PrepareFlow
        }); //ajax_method.UpdateAssetList


    },

    PrepareSubmit: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlow
    },
    PrepareApprove: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlows
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },

    ShowSearchPopup: function (callback) {

        fAction.UpdateAsset(function () {

            //Set search parameter

            f2190SearchAction.ClearDefault();
            Search2190Config.customCallback = null;
            Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();

            Search2190Config.searchOption = "CNS"; //Normal Reclassification Source
            Search2190Config.AllowMultiple = true;

            Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
            Search2190Config.Default.CostCode = { Value: fAction.SelectedSourceCC, Enable: fAction.SelectedSourceCC == "" };
            Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedSourceRCC, Enable: fAction.SelectedSourceRCC == "" };
            Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: false };
            //Search2190Config.HasCostValue = true;

            //Assign Default and Initial screen
            f2190SearchAction.Initialize();

            $('#WFD02190_SearchAssets').modal();

        });

    },

    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");
        if (control.DataTable == null) {
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List

        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }

            var $reasonInput = $(this).find("input[name='txtReason']");

            //var _reason = $(this).find("input[name='txtReason']").val();
            var _bSel = $(this).find("input[name='chkGEN']").is(":checked");

            if (_requireCheck && !_bSel) // Required check but no selected 
                return;
            //var _acc = "2" // Test

            _row = control.DataTable.row(this).data(); // Get source data

            var _reason = _row.REASON;

            if ($reasonInput.length > 0) {
                _reason = $reasonInput.val();
            }

            _assetList.push({
                SEL: _bSel,
                GUID: GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                COMPANY: _row.COMPANY,
                ASSET_NO: _row.ASSET_NO,
                ASSET_SUB: _row.ASSET_SUB,
                LINE_NO: _row.LINE_NO,

                COST_CODE: _row.COST_CODE,
                INVEST_REASON: _row.INVEST_REASON,
                RESP_COST_CODE: _row.RESP_COST_CODE,
                COST_VALUE: _row.COST_VALUE,
                NEW_ASSET_NO: _row.NEW_ASSET_NO,
                NEW_ASSET_SUB: _row.NEW_ASSET_SUB,
                NEW_ASSET_NAME: _row.NEW_ASSET_NAME,
                NEW_COST_CODE: _row.NEW_COST_CODE,
                NEW_RESP_COST_CODE: _row.NEW_RESP_COST_CODE,
                NEW_ASSET_CLASS: _row.NEW_ASSET_CLASS,
                PERCENTAGE: _row.PERCENTAGE,
                QTY: _row.QTY,
                AMOUNT_POSTED: _row.AMOUNT_POSTED,
                REASON: _reason
            });
        });

        return _assetList;
    },
    GetSelectedRow: function (_iLineNo) {
        var _bFoundFlag = false;
        var _selectedRow = null;
        //_Company, _AssetNo, _AssetSub, _AmountPosted, _Percent
        $("#tbSearchResult tr").each(function () {

            if (_bFoundFlag)
                return;

            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }

            var _row = [];

            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data

            if (_row.LINE_NO != _iLineNo) {
                return;
            }

            _selectedRow = this;
            _bFoundFlag = true;
        });
        return _selectedRow;
    },
    EnableMassMode: function () {

    },
    DetailCheck: function (sender) {
        var row = $(sender).closest("tr");

        $('#WFD02910_SelectAll').prop('checked', _act = $('.chkGEN:checked').length == $('.chkGEN').length);
    },
    SetUploadButton: function (sender, _isUpload) {
        if (_isUpload)
            $(sender).html('<i class="fa fa-upload"></i> Upload');
        else
            $(sender).html('<i class="fa fa-download"></i> Download');
    },
    //GetDocUploadTypeName: function (_Type) {
    //    var _DocUpload = 'WFD02910_' + _Type ; //2020.01.25 Surasith change from empty 
       
    //    return _DocUpload;
    //},
    Resend: function (callback) {

        var _header = fMainAction.GetParameter();

        var _list = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, { header: _header, list: _list }, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        });
    },

    UpdateAssetListReason: function (input) {
        var $tr = $(input).closest('tr');
        var row = control.DataTable.row($tr).data();
        row.REASON = input.value;
    }
}

//Call this function when PG set screen mode
var fScreenMode = {

    _GridVisible: function (_sel, _to, _cost, _BoiDoc) {

        //if (control.DataTable == null)
        //    return;

        //control.DataTable.column(control.ColumnIndex.Sel).visible(_sel);
        //control.DataTable.column(control.ColumnIndex.ToAsset).visible(_to);
        //control.DataTable.column(control.ColumnIndex.Cost).visible(_cost);
        //control.DataTable.column(control.ColumnIndex.BosDoc).visible(_BoiDoc);

    },
    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        $('#WFD02910_SelectAll').prop("disabled", true);
        $('#WFD02910_SelectAll').hide();
        $('#DivTMCDocument').hide();
        $('span.DeleteTarget').hide();

        if (GLOBAL.STATUS !== '00') {
            //Submit mode only
            // Grid
            $('button.ExistsAsset, button.NewAsset').prop("disabled", true);
            $('span.DeleteTarget').hide();
        } else {
            // Grid
            $('button.CostDialog').prop("disabled", true);
            $('input.Remark').prop("disabled", true);
        }
    },
    Submit_InitialMode: function (callback) {

        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd').prop("disabled", false);

        //fScreenMode._GridVisible(false, true, true, false);

        $('#DivTMCDocument').hide();

        if (callback == null)
            return;
        callback();
    },
    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').removeClass('disabled').prop("disabled", false);

        //Control in Grid
        $('button.CostDialog').prop("disabled", false);
        $('input.Remark').prop("disabled", false);

        $('button.delete, button.ExistsAsset, button.NewAsset').prop("disabled", false);
        $('button.delete, button.ExistsAsset, button.NewAsset, span.DeleteTarget').show();

        //fScreenMode._GridVisible(false, true, true, false);

        $('#DivTMCDocument').hide();

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').removeClass('disabled').prop("disabled", false);

        //Control in Grid
        $('button.CostDialog').prop("disabled", true);
        $('input.Remark').prop("disabled", false); //allow to edit alway
        // $('input.chkGEN').prop("disabled", true);

        $('button.delete, button.ExistsAsset, button.NewAsset').prop("disabled", true);
        $('button.delete, button.ExistsAsset, button.NewAsset').show();

        $('#DivTMCDocument').hide();

        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) {

        console.log("#_ClearMode");
        fScreenMode._ClearMode();

        $('#btnRequestExport').show();
        $('#btnRequestExport').prop("disabled", false);

        //Load TMC DOC Count
        ajax_method.Post(Url.GetDocCount, { docNo: GLOBAL.DOC_NO, docType: 'WFD02910_TMC' }, true, function (result) {
            
            control.TMCAttachCount = result;

            control.AllowUploadTMC = (PERMISSION.ActionMark == constants.TMC) //(control.HasMachineLicense && control.AllowFSUser) || PERMISSION.IsAECUser === "Y";

            if (control.TMCAttachCount > 0 || control.AllowUploadTMC) {
                $('#DivTMCDocument').show();
                fAction.SetUploadButton('#WFD02910btnUploadTMCDocument', control.AllowUploadTMC);
            }
        });

        //if ((PERMISSION.IsFSUser == "Y" && (PERMISSION.IsFSState == "A") || PERMISSION.IsFSState == "Y")) {
        //    $('#DivTMCDocument').show();
        //}

        if (PERMISSION.AllowResend == "Y" || !$('#btnGenerateAssetNo').is(":disabled")) {
            $('#WFD02910_SelectAll').show();
            $('#WFD02910_SelectAll').prop("disabled", false);
        }

        if (callback == null)
            return;
        callback();
        return;

    },

}

var fRender = {
    CheckBoxRender: function (data) {
        var _strHtml = '';
        var _disabled = '';
        if (!data.AllowCheck)
            _disabled = ' disabled ';

        var _hide = '';
        if (GLOBAL.STATUS < '35' && PERMISSION.AllowGenAssetNo == "Y" && data.REQ_GEN == "N") {
            _hide = 'hide';
        }

        _strHtml = '<input style="width:20px; height:28px;" name="chkGEN" class="chkGEN ' + _hide + ' " value="' + data.LINE_NO + '" onclick="fAction.DetailCheck(this);" type="checkbox" ' + _disabled + '>';
        return _strHtml;
    },

    HyperLinkRender: function (data) {
        if (data.ASSET_NO == null)
            return '';
        var _text = data.ASSET_NO;
        if (data.IS_MACHINE_LICENSE == "Y") {
            _text = data.ASSET_NO + "<span style=\"color:red;font-weight:bold\">*</span>";
        }
        return '<a style="white-space: nowrap;" href="../WFD02130?COMPANY=' + data.COMPANY + '&ASSET_NO=' + data.ASSET_NO + '&ASSET_SUB=' + data.ASSET_SUB + '" target="_blank">' + _text + '</a>';
    },

    LableRowSeq: function (data) {
        return '<label id="lblRowSeq_' + data.LINE_NO + '">' + data.ROW_SEQ + '</label>';
    },

    NewAssetHyperLinkRender: function (data) {
        if (data.NEW_ASSET_NO == null)
            return '';

        var _strHtml = '';

        if (data.HAS_ASSET_MASTER === 'N') {
            _strHtml = '<button type="button" class="btn btn-link" onclick="fPopup.Open2115PopupEdit(this);return false;">' + data.NEW_ASSET_NO + '</button>';
        } else {
            _strHtml = '<a href="../WFD02130?COMPANY=' + data.COMPANY + '&ASSET_NO=' +
                data.NEW_ASSET_NO + '&ASSET_SUB=' + data.NEW_ASSET_SUB + '" target="_blank">' + data.NEW_ASSET_NO + '</a>';
        }

        var _strDelete = '';

        var _disabled = '';
        if (GLOBAL.DOC_NO != '') {
            return _strHtml;
        }

        _strDelete = '<span title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <span id="btnDeleteTarget_' + data.LINE_NO + '" class="DeleteTarget" data-title="Delete" type="button" ' +
            'onclick=fAction.DeleteTargetAsset(this,\'' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB + "_" + data.LINE_NO + '\') style:pading-right:5px;>' +
            '<span class="glyphicon glyphicon-trash" style="color: red;"></span></span></span>';
        return _strHtml + _strDelete;

    },

    TextBoxRender: function (data) {

        var reason = (data.REASON == null ? "" : data.REASON);
        var _disabled = "";
        var tooltip = "";
        var _class = "form-control Remark";

        if (!data.AllowEdit) {
            _disabled = "disabled";
            tooltip = 'data-toggle="tooltip" data-placement="top" title="" data-original-title="' + reason + '"';
        } else {
            _class += " require";
        }

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB + "_" + data.LINE_NO;
        return '<input type="text" ' + tooltip + ' id="txtReason_' + _id + '" name="txtReason" onblur="fAction.UpdateAssetListReason(this)" class="' + _class + '" maxlength="50" value="' + reason + '" ' + _disabled + ' />';
    },

    ButtonRender: function (data) {
        var _text = ' ... ';
        if (data.AMOUNT_POSTED != null) {
            var _AMOUNT_POSTED = parseFloat(data.AMOUNT_POSTED);

            _text = fRender.commaSeparateNumber(_AMOUNT_POSTED.toFixed(2));
        }

        var _disabled = (data.AllowEdit ? "" : "disabled");

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB + "_" + data.LINE_NO;
        return '<input type="hidden" id="hdOriginal_"' + _id + '" name="hdOriginal" value="' + data.COST_VALUE + '" />' +
            '<input type="hidden" id="hdPercent_"' + _id + '" name="hdPercent" value="' + data.PERCENTAGE + '" />' +
            '<input type="hidden" id="hdAmount_"' + _id + '" name="hdAmount" value="' + _text + '"/>' +
            '<button type="button" id="btnAmountPosted_' + _id + '" name="btnAmountPosted" class="CostDialog btn btn-info btn-xs" ' + _disabled +
            ' onclick="fPopup.ShowCostDialog(this);return false;">' + _text + '</button>';

    },

    TargetAssetButtonRender: function (data) {
        console.log(data);

        var _disabled = (PERMISSION.DOC_STATUS == '00' && PERMISSION.AllowEditBeforeGenFlow ? "" : "disabled");

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB + "_" + data.LINE_NO;
        return '<div class="btn-group" role="group" aria-label="Basic example"><button type="button" id="btnNewAsset_' + _id + '" name="btnNewAssets" class="NewAsset btn btn-info btn-xs" ' + _disabled +
            ' onclick="fPopup.Open2115Popup(this);return false;"> New </button>' +
            '<button type="button" id="btnExistsAsset_' + _id + '" name="btnExistsAsset" class="ExistsAsset btn btn-info btn-xs" ' + _disabled +
            ' onclick="fPopup.ShowSearchExistingPopup(this);return false;"> Exists </button></div>';

        //,\'' + data.COMPANY + '\',\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\'
    },

    DeleteButtonRender: function (data) {

        var _disbled = (PERMISSION.AllowEditBeforeGenFlow ? '' : "disabled");
        var _hide = (data.AllowDelete ? '' : 'hide');

        if (GLOBAL.DOC_NO != '' && data.AllowDelete) {
            _disbled = '';
        }

        var _strHtml = '';
        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB + "_" + data.LINE_NO;
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="btnDelete_' +
            data.LINE_NO + '" class="' + _hide + ' btn btn-danger btn-xs delete" data-title="Delete" type="button" ' + _disbled + ' ' +
            'onclick=fAction.DeleteAsset(\'' +
            //data.LINE_NO + ')>' +
            data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\',\'' + data.LINE_NO + '\')>' +
            '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },

    ButtonBoiDocumentRender: function (data) {
        // console.log(data);
        var _text = data.BOI_ATTACH_DOC;
        if (data.BOI_ATTACH_DOC != null) {
            _text = data.BOI_ATTACH_DOC;
        }
        var _disabled = '';
        if (((PERMISSION.IsAECUser == 'Y' || PERMISSION.IsBOIUser == 'Y') && PERMISSION.IsBOIState == 'A') || PERMISSION.IsBOIState == 'Y') { //Y=Past, A=Active, N=No
            _disabled = "";
        } else {
            _disabled = "disabled";
        }

        if (data.INVEST_REASON == 'B') {
            return '<div class="btn-group">' +
                '<button type="button" class="btn btn-info" style = "width: 100%;" ' + _disabled + ' onclick="fPopup.ShowBOIUploadDialog(this)">' +
                '<span class="glyphicon glyphicon-search"></span></button>' +
                '</div>';
        } else {
            return '';
        }
    },

    commaSeparateNumber: function (val) {
        if (val != null) {
            val = currencyFormat(val);
            //while (/(\d+)(\d{3})/.test(val.toString())) {
            //    val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            //}
        }
        return val;
    }
};

var fPopup = {
    ShowCostDialog: function (button) {
        var row = control.DataTable.row($(button).parents('tr')).data();

        //Get Summary without selected Line
        var _preSummary = 0;
        var _detailList = fAction.GetClientAssetList(false); //all
        _detailList.forEach(function (_item, _indx) {
            console.log(_item);
            if (_item.LINE_NO == row.LINE_NO)
                return; // next row

            if (_item.ASSET_NO == row.ASSET_NO && _item.ASSET_SUB == row.ASSET_SUB && _item.COMPANY == row.COMPANY) {
                _preSummary += $.isEmptyObject(_item.AMOUNT_POSTED) ? 0 : _item.AMOUNT_POSTED;//.split(",").join("")
            }
            console.log(_preSummary)
        })

        var _rowAmountPosted = parseFloat(row.AMOUNT_POSTED != null ? row.AMOUNT_POSTED : 0);//.split(",").join("")

        //Get Cost Info
        //console.log(row);
        control.CostPopup = {
            LINE_NO: row.LINE_NO,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: row.COMPANY,
            ORIGIN_QTY: row.ORIGIN_QTY,
            QTY: row.QTY,
            PERCENTAGE: row.PERCENTAGE,
            AMOUNT_POSTED: _rowAmountPosted,
            COST_VALUE: row.COST_VALUE,
            SUMMARY: _preSummary,
            REMAIN: (row.COST_VALUE - (_preSummary + _rowAmountPosted))
        };

        $("#AlertMessageWFD02910").html('');
        // #Clear Data
        $('#txtCostValueOriginal, #txtCostAmount, #txtQty, #txtAmountByQty,#txtCostPercentage,#txtAmountByPercentage').val('');
        $('#lblUnit, #lblRemainCost').text('');
        $('#chkAmount, #chkQty, #chkPercentage').prop('checked', false);

        $("#txtCostAmount").autoNumeric('init', { aSep: ',', vMin: '0.00', vMax: '9999999999.99' });
        //$("#txtCostPercentage").autoNumeric('init', { vMin: '0.01', vMax: '100.00' });

        var maxQty = control.CostPopup.ORIGIN_QTY.toFixed(3).toString();
        //$('#txtQty').autoNumeric('init', { vMin: '0.000', vMax: maxQty });

        $('#lblUnit').text(maxQty);
        $('#lblAssetNoOnCostdialog').text(row.ASSET_NO);

        //$('#lblRemainCost').text(control.CostPopup.REMAIN);
        $('#lblRemainCost').text(fRender.commaSeparateNumber(control.CostPopup.REMAIN.toFixed(2)));
        $('#divCostDialog input').prop('disabled', true);
        $('#divCostDialog input:checkbox').prop('disabled', false);

        // Binging
        $('#txtCostValueOriginal').val(fRender.commaSeparateNumber(control.CostPopup.COST_VALUE));
        if (control.CostPopup.PERCENTAGE > 0) {
            $('#chkPercentage').prop('checked', true);
            $('#txtCostPercentage').val(control.CostPopup.PERCENTAGE.toFixed(2));
            //$('#txtAmountByPercentage').val(control.CostPopup.AMOUNT_POSTED);
            $('#txtAmountByPercentage').val(fRender.commaSeparateNumber(_rowAmountPosted.toFixed(2)));
            //$('#chkPercentage').trigger('click');
            $('#txtCostPercentage').prop('disabled', false);

        }
        else if (control.CostPopup.QTY > 0) {
            $('#chkQty').prop('checked', true);
            $('#txtQty').val(control.CostPopup.QTY.toFixed(3));
            //alert(1);
            $('#txtAmountByQty').val(fRender.commaSeparateNumber(_rowAmountPosted.toFixed(2)));
            $('#txtQty').prop('disabled', false);
            //$('#chkQty').trigger('click');
        }
        else if (_rowAmountPosted > 0) {
            $('#chkAmount').prop('checked', true);
            $('#txtCostAmount').val(fRender.commaSeparateNumber(_rowAmountPosted.toFixed(2)));
            $('#txtCostAmount').prop('disabled', false);
            //$('#chkAmount').trigger('click');
            //fPopup.CalculateRemainCost();
        }
        //$('#txtCostValueOriginal').val(control.CostPopup.COST_VALUE);

        $('#divCostDialog').modal();

        $('#txtCostPercentage').off('keypress').on('keypress', function (e) {
            return isNumberKey(e);
        });

        $('#txtQty').off('keypress').on('keypress', function (e) {
            return isNumberKey(e);
        });
    },
    UpdateAmountOnRow: function (callback) {

        var _selectedRow = fAction.GetSelectedRow(control.CostPopup.LINE_NO);
        if (_selectedRow == null)
            return;

        var _obj = control.DataTable.row(_selectedRow).data()
        _obj.QTY = null;
        _obj.PERCENTAGE = null;

        var _id = _obj.COMPANY + "_" + _obj.ASSET_NO + "_" + _obj.ASSET_SUB + "_" + _obj.LINE_NO;

        if ($('#divCostDialog #chkAmount').is(':checked')) {
            var _CostAmount = parseFloatWithComma($('#txtCostAmount').val());
            var _returnVal = fRender.commaSeparateNumber(_CostAmount.toFixed(2));
            _obj.AMOUNT_POSTED = _CostAmount;
            $("#btnAmountPosted_" + _id).html(_returnVal);
        }
        if ($('#divCostDialog #chkQty').is(':checked')) {
            _obj.QTY = parseFloat($('#txtQty').val());
            var _CostAmountQty = parseFloat($('#txtAmountByQty').val().split(",").join(""));
            var _returnValQty = fRender.commaSeparateNumber(_CostAmountQty.toFixed(2));
            _obj.AMOUNT_POSTED = _CostAmountQty;
            $("#btnAmountPosted_" + _id).html(_returnValQty);
        }
        if ($('#divCostDialog #chkPercentage').is(':checked')) {
            _obj.PERCENTAGE = parseFloat($('#txtCostPercentage').val());
            var _CostAmountPer = parseFloat($('#txtAmountByPercentage').val().split(",").join(""));
            var _returnValPer = fRender.commaSeparateNumber(_CostAmountPer.toFixed(2));
            _obj.AMOUNT_POSTED = _CostAmountPer;
            $("#btnAmountPosted_" + _id).html(_returnValPer);
        }

        control.DataTable.row(_selectedRow).data(_obj).draw(false);

        if (callback != null)
            callback();
    },
    CalculateAmountByQty: function () {
        try {
            var oriQty = parseFloat(control.CostPopup.ORIGIN_QTY);
            var _amount = 0;
            if (oriQty > 0) {
                _amount = parseFloat($('#txtCostValueOriginal').val().split(",").join("")) * parseFloat($('#txtQty').val()) / oriQty;
            }

            $('#txtAmountByQty').val(fRender.commaSeparateNumber(_amount.toFixed(2)));
            fPopup.CalculateRemainCost();
        }
        catch (err) {
            $('#txtAmountByQty').val('')
            console.error(err);
        }
    },

    CalculateAmountByPercent: function () {
        try {
            var _amount = parseFloat($('#txtCostValueOriginal').val().split(",").join("")) * parseFloat($('#txtCostPercentage').val()) / 100;
            //$('#txtAmountByPercentage').val(_amount);
            $('#txtAmountByPercentage').val(fRender.commaSeparateNumber(_amount.toFixed(2)));
            fPopup.CalculateRemainCost();
        }
        catch (err) {
            $('#txtAmountByPercentage').val('')
            console.error(err);
        }
    },
    CalculateRemainCost: function () {

        //var _remain = control.CostPopup.COST_VALUE - (control.CostPopup.SUMMARY + _amountPost);
        //$('#lblRemainCost').text(_remain);
        var amontCost = _GetCost();
        var costValueOriginal = $('#txtCostValueOriginal').val().split(",").join("");

        var numCostValueOriginal = parseFloat(costValueOriginal);
        var numAmountCost = parseFloatWithComma(amontCost);

        var _remain = parseFloat(costValueOriginal) - parseFloat(numAmountCost);

        if (numCostValueOriginal >= numAmountCost) {
            $("#AlertMessageWFD02910").html('');
            $("#btnCostDialogOK").prop('disabled', false);
        } else {
            AlertTextErrorMessagePopup('MCOM0017AERR : Amount greater than Cost Value or Remain Cost', "#AlertMessageWFD02910");
            $("#btnCostDialogOK").prop('disabled', true);
        }

        $('#lblRemainCost').text(fRender.commaSeparateNumber(_remain.toFixed(2)));

    },
    ClearValue: function () {
        $('#txtCostAmount, #txtQty, #txtCostPercentage, #txtAmountByQty, #txtAmountByPercentage').val('');
    },

    Open2115PopupEdit: function (button) {

        fAction.UpdateAsset(function () {
            var _popupWidth = $(window).width() - 20;
            var left = ($(window).width() / 2) - (_popupWidth / 2);
            top = ($(window).height() / 2) - (600 / 2);

            var _selectedList = fAction.GetClientAssetList(false); //all

            var _selectedRCC = "", _selectedBOI = "";
            if (_selectedList.length > 0) {
                _selectedRCC = _selectedList[0].RESP_COST_CODE;
                _selectedBOI = _selectedList[0].INVEST_REASON;
            }

            var row = control.DataTable.row($(button).closest('tr')).data();
            //var isGenAsset = row.NEW_ASSET_NO.toLowerCase().indexOf("temp");

            //var allowEdit = "&OPTION=E";
            ////CREATE REQUEST OR AEC LOGIN
            //if ((!GLOBAL.DOC_NO || PERMISSION.IsAECUser === 'Y') && isGenAsset === 0) {
            //    allowEdit += "&AllowEdit=Y";//CAN EDIT
            //}

            popup = window.open(MainUrl.NewAssetsPopup +
                "?GUID=" + GLOBAL.GUID +
                "&COMPANY=" + control.Company.val() +
                "&DOC_NO=" + GLOBAL.DOC_NO +
                "&LINE_NO=" + row.LINE_NO +
                "&INVEST_REASON=" + _selectedBOI +
                "&RESP_COST_CODE=" + _selectedRCC +
                "&FLOW_TYPE=" + GLOBAL.FLOW_TYPE +
                //"&MODE=N" +
                "&OPTION=E" +
                //allowEdit +
                "&ROLEMODE=" + $('#WFD01170ddpRequestorRole').val() +
                "&ASSET_NO=" + row.ASSET_NO +
                "&ASSET_SUB=" + row.ASSET_SUB,
                "popup", "width=" + _popupWidth + ", height=600, top=" + top + ", left=" + left + ", scrollbars=yes");
        });
    },

    Open2115Popup: function (button) {

        fAction.UpdateAsset(function () {
            var _popupWidth = $(window).width() - 20;
            var left = ($(window).width() / 2) - (_popupWidth / 2);
            top = ($(window).height() / 2) - (600 / 2);

            var _selectedList = fAction.GetClientAssetList(false); //all

            var isFirstAsset = true;

            var _selectedRCC = "", _selectedBOI = "", _selectCC = "";
            if (_selectedList.length > 0) {
                _selectedRCC = _selectedList[0].RESP_COST_CODE;
                _selectedBOI = _selectedList[0].INVEST_REASON;
                _selectCC = _selectedList[0].COST_CODE;

                for (var i = 0; i < _selectedList.length; i++) {
                    var selected = _selectedList[i];
                    if (selected.NEW_COST_CODE) {
                        _selectedRCC = selected.NEW_RESP_COST_CODE;
                        _selectCC = selected.NEW_COST_CODE;
                        isFirstAsset = false;
                        break;
                    }
                }
            }

            var row = control.DataTable.row($(button).parents('tr')).data();

            popup = window.open(MainUrl.NewAssetsPopup +
                "?GUID=" + GLOBAL.GUID +
                "&COMPANY=" + control.Company.val() +
                "&DOC_NO=" + GLOBAL.DOC_NO +
                "&LINE_NO=" + row.LINE_NO +
                "&INVEST_REASON=" + _selectedBOI +
                "&COST_CODE=" + _selectCC +
                "&RESP_COST_CODE=" + _selectedRCC +
                "&FLOW_TYPE=" + GLOBAL.FLOW_TYPE +
                "&IS_FIRST_ASSET=" + isFirstAsset +
                "&MODE=N" +
                "&ROLEMODE=" + $('#WFD01170ddpRequestorRole').val() +
                "&ASSET_NO=" + row.ASSET_NO +
                "&ASSET_SUB=" + row.ASSET_SUB,
                "popup", "width=" + _popupWidth + ", height=600, top=" + top + ", left=" + left + ", scrollbars=yes");
        });

    },
    ShowSearchExistingPopup: function (sender) {
        //Set search parameter
        fAction.UpdateAsset(function () {
            var data = control.DataTable.row($(sender).parents('tr')).data();

            f2190SearchAction.ClearDefault();

            var _selectedList = fAction.GetClientAssetList(false); //false

            //BOI same as original
            if (_selectedList.length > 0) {
                for (var i = 0; i < _selectedList.length; i++) {
                    var selected = _selectedList[i];

                    if (selected.NEW_COST_CODE) {
                        Search2190Config.Default.CostCode = { Value: selected.NEW_COST_CODE, Enable: false };
                        Search2190Config.Default.ResponsibleCostCode = { Value: selected.NEW_RESP_COST_CODE, Enable: false };
                        break;
                    } else {
                        Search2190Config.Default.CostCode = { Value: '', Enable: true };
                        Search2190Config.Default.ResponsibleCostCode = { Value: '', Enable: true };
                    }
                }
            }

            // 2020.01.25 Surasith) User request to unlock BOI condition (BOI can reclass to NB)
            // Search2190Config.Default.INVEST_REASON = { Value: data.INVEST_REASON, Enable: false };
            Search2190Config.Default.INVEST_REASON = { Value: data.INVEST_REASON, Enable: true };

            Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();
            Search2190Config.searchOption = "CND";
            Search2190Config.AllowMultiple = false; //Not allow to select > 1

            Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
            Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: false };
            //Search2190Config.HasCostValue = false;


            Search2190Config.customCallback = function (_targetList, callback) {

                console.log(data);

                if (_targetList.length == 0)
                    return;

                var _target = _targetList[0];


                var _selectedRow = fAction.GetSelectedRow(data.LINE_NO);
                if (_selectedRow == null)
                    return;

                var _obj = control.DataTable.row(_selectedRow).data();
                _obj.NEW_ASSET_NO = _target.ASSET_NO;
                _obj.NEW_ASSET_SUB = _target.ASSET_SUB;
                _obj.NEW_ASSET_NAME = _target.ASSET_NAME;
                _obj.NEW_ASSET_CLASS = _target.ASSET_CLASS;
                _obj.NEW_ASSET_CLASS_HINT = _target.ASSET_CLASS_HINT;
                _obj.NEW_COST_CODE = _target.COST_CODE;
                _obj.NEW_RESP_COST_CODE = _target.RESP_COST_CODE;

                control.DataTable.row(_selectedRow).data(_obj).draw(false);

                fAction.UpdateAsset(function () {
                    fAction.LoadData();
                });

                if (callback != null)
                    callback();
            };

            //Assign Default and Initial screen
            f2190SearchAction.Initialize();

            $('#WFD02190_SearchAssets').modal();
        });
    },
    ShowBOIUploadDialog: function (button) {

        var _bIsUploadBOI = (PERMISSION.IsBOIState == "A" && (PERMISSION.IsBOIUser == "Y" || PERMISSION.IsAECUser == "Y"));

        var _Type = "BOI";
        var row = control.DataTable.row($(button).parents('tr')).data();
        console.log('----------------');
        console.log(row.ASSET_NO);
        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: row.ASSET_NO == null ? "" : row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB == null ? "" : row.ASSET_SUB,
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _Type,
            FUNCTION_ID: GLOBAL.FUNCTION_ID,
            DOC_UPLOAD: GLOBAL.FUNCTION_ID + "_" + _Type,
            LINE_NO: row.LINE_NO,
            DEL_FLAG: (_bIsUploadBOI) ? "Y" : "N"
        }

        $('#lblUploadFileText').html("Upload BOI Document Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB);
        $('#CommonUpload').modal('show');
        //$('#CommonUpload').appendTo("body").modal('show');

    },

    ShowTMCUploadDialog: function (button, _Type) {
      //  var _DocUpload = fAction.GetDocUploadTypeName(_Type);

        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: '',
            ASSET_SUB: '',
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _Type,
            FUNCTION_ID: GLOBAL.FUNCTION_ID,
            DOC_UPLOAD: GLOBAL.FUNCTION_ID + "_" + _Type,
            LINE_NO: '0',
            DEL_FLAG: control.AllowUploadTMC ? "Y" : "N"
        }
        $('#lblUploadFileText').html("Upload TMC Document");
        //$('#CommonUpload').modal('show');
        $('#CommonUpload').appendTo("body").modal('show');

    }
}
$('#WFD02910_SelectAll').click(function (e) {
    $('.chkGEN:enabled').prop('checked', $(this).is(':checked'));
})



// Popup Control Event
$('#divCostDialog #chkAmount').click(function (e) {

    if ($(this).is(':disabled'))
        return;

    $('#chkQty, #chkPercentage').prop('checked', false);
    $('#txtQty, #txtCostPercentage').prop('disabled', true);

    fPopup.ClearValue();

    $('#txtCostAmount').prop('disabled', !$(this).is(':checked'));
    if (!$('#txtCostAmount').is(':disabled') && $('#txtCostAmount').val() == "") {
        $('#txtCostAmount').val($('#txtCostValueOriginal').val());
    }
    fPopup.CalculateRemainCost();
})
$('#divCostDialog #chkQty').click(function (e) {

    if ($(this).is(':disabled'))
        return;

    $('#chkAmount, #chkPercentage').prop('checked', false);
    $('#txtCostAmount, #txtCostPercentage').prop('disabled', true);

    $('#txtQty').prop('disabled', !$(this).is(':checked'));

    fPopup.ClearValue();

    if (!$('#txtQty').is(':disabled') && $('#txtQty').val() == "") {
        $('#txtQty').val(control.CostPopup.ORIGIN_QTY.toFixed(3));
        fPopup.CalculateAmountByQty();
    }
    fPopup.CalculateRemainCost();
})
$('#divCostDialog #chkPercentage').click(function (e) {
    if ($(this).is(':disabled'))
        return;

    $('#chkAmount, #chkQty').prop('checked', false);
    $('#txtCostAmount, #txtQty').prop('disabled', true);

    $('#txtCostPercentage').prop('disabled', !$(this).is(':checked'));

    fPopup.ClearValue();

    if (!$('#txtCostPercentage').is(':disabled') && $('#txtCostPercentage').val() == "") {
        $('#txtCostPercentage').val('100.00');
        fPopup.CalculateAmountByPercent();
    }
    fPopup.CalculateRemainCost();
})
$("#divCostDialog #txtCostPercentage").blur(function () {
    fPopup.CalculateAmountByPercent();
});
$("#divCostDialog #txtQty").blur(function () {
    fPopup.CalculateAmountByQty();
});

$('#WFD02910btnUploadTMCDocument').click(function (e) {
    ClearMessageC();
    //$('#WFD01170fileUploadCommentInfo').click();
    fPopup.ShowTMCUploadDialog(this, 'TMC');
})


$('#divCostDialog #btnCostDialogOK').click(function (e) {

    //Validation
    if ($('#divCostDialog input:checked').length == 0) {
        AlertTextErrorMessagePopup('MCOM0017AERR : Please select one of choice', "#AlertMessageWFD02910");
        return;
    }

    fPopup.UpdateAmountOnRow(function () {
        $('#divCostDialog').modal('hide');
    });

})

function _GetCost() {
    //alert($('#txtAmountByQty').val().split(",").join(""));
    if ($('#chkAmount').is(":checked")) {
        return $('#txtCostAmount').val();
    }
    if ($('#chkQty').is(":checked")) {
        return $('#txtAmountByQty').val().split(",").join("");
    }
    if ($('#chkPercentage').is(":checked")) {
        return $('#txtAmountByPercentage').val().split(",").join("");
    }

    return 0;
}


//var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
//var str = $('#myTextBox').val();
//if(numberRegex.test(str)) {
//    alert('I am a number');
//    ...
//    } 

$('#btnGenerateAssetNo').click(function () {
    fMainAction.GenerateAssetNo();
})