﻿var EMPLOYEE_CODE = "";
var ENCODE_DATA = "";
var SOURCE = "";
var UPDATE_DATE = "";
var GotoPageOne = false;
var isResponseCostCenter = null;
var selectResponseCostCenter = null;
var selectCostCenter = null;
var in_charge = [];
var rescc_in_charge = [];

var Controls = {
    WFD01210DIVISTION: $('#DIVISTION'),
    WFD01210DEPARTMENT: $('#DEPARTMENT'),
    WFD01210SECTION: $('#SECTION'),
    WFD01210POSITION: $('#POSITION'),
    WFD01210RESPONSIBILITY: $('#RESPONSIBILITY'),
    WFD01210EMPLOYEE_CODE: $('#EMPLOYEE_CODE'),
    WFD01210FIRST_NAME: $('#FIRST_NAME'),
    WFD01210LAST_NAME: $('#LAST_NAME'),
    WFD01210COST_CENTER: $('#COST_CENTER'),
    WFD01210IN_CHARGE_COST_CENTER: $('#IN_CHARGE_COST_CENTER'),
    //WFD01210DATANOTFOUND: $('.DataNotFound'),

    ClearButton: $("#WFD01210Clear"),
    SearchButton: $("#WFD01210Search"),
    Pagin: $('#WFD1210Table').Pagin(PARAMETER.ITEM_PER_PAGE),
    SearchResultTable: $('#WFD1210Table'),
    SearchDataRow: $('#SearchDataRow'),
    WFD01210AddBTN: $('#WFD01210Add'),
    DeleteButton: $('#WFD01210Delete'),
    EditButton: $('#WFD01210Edit'),
    ViewButton: $('#WFD01210View'),


    CCPagin: $('#tableCCBaseOnOrganize').Pagin(PARAMETER.ITEM_PER_PAGE),
    CCSearchResultTable: $('#tableCCBaseOnOrganize'),
    CCSearchDataRow: $('#RowDataCCBaseOnOrganize'),

    WFD01210RESPONSIBILITY_USERPROFILE: $('#WFD01210RESPONSIBILITY_USERPROFILE'),
    //dialog
    SIGNATURE_DIALOG: $('#signature-dialog'),
    IMG_SIGNATURE_DIALOG: $('#IMG_SIGNATURE_DIALOG'),

    DataTable: null,

};

var Controls_UserProfile = {
    WFD01210UPLOAD: $('#WFD01210Upload'),
    WFD01210Cancel: $('#WFD01210Cancel'),
    SIGNATURE: $('#SIGNATURE'),
    PREVIEW: $('#Image_Priview'),
    CLEAR: $('#WFD01210ClearPreview'),
    COST_CENTER: $('#WFD01210COST_CENTER_USERPROFILE'),
    COMPANY: $('#COMPANY'),
    ORGANIZE: $('#WFD01210ORGANIZE_CODE_USERPROFILE'),
    WFD01210SYS_EMP_ID_USERPROFILE: $('#WFD01210SYS_EMP_ID_USERPROFILE'),
    WFD01210EMP_ID_USERPROFILE: $('#WFD01210EMP_ID_USERPROFILE'),
    WFD01210EMP_TITLE_USERPROFILE: $('#WFD01210EMP_TITLE_USERPROFILE'),
    WFD01210EMP_FIRST_NAME_USERPROFILE: $('#WFD01210EMP_FIRST_NAME_USERPROFILE'),
    WFD01210EMP_LAST_NAME_USERPROFILE: $('#WFD01210EMP_LAST_NAME_USERPROFILE'),
    WFD01210POSITION_CODE_USERPROFILE: $('#WFD01210POSITION_CODE_USERPROFILE'),
    WFD01210POSITION_NAME_USERPROFILE: $('#WFD01210POSITION_NAME_USERPROFILE'),
    WFD01210GROUP_USERPROFILE: $('#WFD01210GROUP_USERPROFILE'),
    WFD01210DEPARTMENT_USERPROFILE: $('#WFD01210DEPARTMENT_USERPROFILE'),
    WFD01210DEVISION_USERPROFILE: $('#WFD01210DEVISION_USERPROFILE'),
    WFD01210Section_USERPROFILE: $('#WFD01210Section_USERPROFILE'),
    WFD01210RESPONSIBILITY_USERPROFILE: $('#WFD01210RESPONSIBILITY_USERPROFILE'),
    WFD01210Setup: $('#WFD01210Setup'),
    WFD01210SetupText_USERPROFILE: $('#WFD01210SetupText_USERPROFILE'),
    WFD01210Email_USERPROFILE: $('#WFD01210Email_USERPROFILE'),
    WFD01210Mobile_USERPROFILE: $('#WFD01210Mobile_USERPROFILE'),
    WFD01210DELEGATION_USERPROFILE: $('#WFD01210DELEGATION_USERPROFILE'),
    DELEGATE_FROM_USERPROFILE: $('#DELEGATE_FROM_USERPROFILE'),
    DELEGATE_TO_USERPROFILE: $('#DELEGATE_TO_USERPROFILE'),
    WFD01210Save: $('#WFD01210Save'),
    WFD01210FAADMIN_USERPROFILE: $('#WFD01210FAADMIN_USERPROFILE'),

    DIV_TABLE_ORGANIZE: $('#DIV_TABLE_ORGANIZE'),
    TABLEORGANIZE: $('#tableOrganize'),
    OrgPagin: $('#tableOrganize').Pagin(PARAMETER.ITEM_PER_PAGE),
    HAVE_DATA: $('#HaveData'),
    //NO_DATA: $('#NoData'),
    IN_CHARGE_DIALOG: $('#incharge-dialog'),
    IN_CHARGERIGHTBTN: $('#In_ChargeRightBtn'),
    IN_CHARGELEFTBTN: $('#In_ChargeLeftBtn'),
    WFD01210SAVEIN_CHARGE: $('#WFD01210SaveIn_Charge'),
    WFD01210CancelIn_Charge: $('#WFD01210CancelIn_Charge'),
    SEARCH_IN_CHARGEBTN: $('#WFD01210SearchInCharge'),
    WFD01210CLOSE: $('#WFD01210Close'),
    WFD01210UPDATEDATE_USERPROFILE: $('#WFD01210UPDATEDATE_USERPROFILE')
}
var requiredMainControl_WFD01210 = [Controls_UserProfile.COST_CENTER, Controls_UserProfile.COMPANY];
var defaultValue = {};

$(function () {
    if (PARAMETER.MODE === 'SEARCH') {
        defaultValue.company = $('#COMPANY').val();
        $('#RESPONSIBILITY').multiselect('selectAll', false);
        $('#SPECIAL_ROLE').multiselect('selectAll', false);

        //When open page focus to input EMPLOYEE CODE
        $('#EMPLOYEE_CODE').focus();

    } else {
        $('#WFD01210AECUser_USERPROFILE').multiselect(multiSelectOptionWidth100);         

        $('#WFD01210AEC_Manager_USERPROFILE').on('click', function () {
            setDisplayNone('WFD01210AEC_Manager_USERPROFILE', 'aecManager');
        });

        $('#WFD01210FA_Supervisor_USERPROFILE').on('click', function () {
            setDisabled('WFD01210FA_Supervisor_USERPROFILE', 'WFD01210BtnResponse_Cost_Center');
        });

        $('#WFD01210FA_Window_USERPROFILE').on('click', function () {
            setDisabled('WFD01210FA_Window_USERPROFILE', 'WFD01210BtnCost_Center');
        });

        $("#Selected_Incharge > tbody >tr.detail").off('click').on('click', function () {
            //debugger;
            $(this).toggleClass("table-row-select");
        });

        $('#COMPANY').on('select2:select', function () {  
            console.log($('#COMPANY').val());
            getCostCenterData_ddl('#WFD01210COST_CENTER_USERPROFILE', $('#COMPANY').val());

            console.log('Company change and get new org');
            GotoPageOne = true;

            $('#WFD01210ORGANIZE_CODE_USERPROFILE').val('');

            getTbOrganize();
        });

        $("#Incharge_CostCode > tbody > tr.detail").off('click').on('click', function () {
            //debugger;
            $(this).toggleClass("table-row-select");
        });

        //if (PARAMETER.MODE === 'ADD') {
        //    $('#WFD01210POSITION_CODE_USERPROFILE_TXT_DIV').hide();
        //    $('#JOBSave_TXT_DIV').hide();
        //}

        setControlsRequiredField(requiredMainControl_WFD01210, true);

        //Controls_UserProfile.CLEAR.hide();
        WFD01210setDisableButton('#WFD01210ClearPreview');//Suphachai L.2017-06-19
    }

    $('.modal ').appendTo($('body'));

    Controls_UserProfile.SEARCH_IN_CHARGEBTN.click(SearchIn_Charge);
    Controls_UserProfile.IN_CHARGERIGHTBTN.click(rightToTable);
    Controls_UserProfile.IN_CHARGELEFTBTN.click(LeftToTable);
    Controls_UserProfile.WFD01210SAVEIN_CHARGE.click(SaveIn_Charge);
    Controls_UserProfile.WFD01210CancelIn_Charge.click(CancelIn_Charge);
    Controls_UserProfile.WFD01210CLOSE.click(Close);
    Controls_UserProfile.WFD01210CLOSE.hide();
    Controls_UserProfile.WFD01210Setup.attr('disabled', 'disabled');

    //$('#WFD01210BtnResponse_Cost_Center').attr('disabled', 'disabled');
    //$('#WFD01210BtnCost_Center').attr('disabled', 'disabled');

    // the selector will match all input controls of type :checkbox
    // and attach a click event handler 
    //$("input:checkbox").on('click', function () {
    //    // in the handler, 'this' refers to the box clicked on
    //    var $box = $(this);
    //    if ($box.is(":checked")) {
    //        // the name of the box is retrieved using the .attr() method
    //        // as it is assumed and expected to be immutable
    //        var group = "input:checkbox[name='" + $box.attr("name") + "']";
    //        // the checked state of the group/box on the other hand will change
    //        // and the current value is retrieved using .prop() method
    //        $(group).prop("checked", false);
    //        $box.prop("checked", true);

    //    } else {
    //        $box.prop("checked", false);
    //    }
    //});



    //get data in index
    //GetDivision();
    //GetDepartment();
    //GetSection();
    //GetPosition();



    //GetResponsibility();
    //GetDelegateUser();
    //Search();
    Controls.SearchDataRow.hide();


    Controls.SearchButton.click(OnSearchBtnClick);
    Controls.ClearButton.click(Clear);
    Controls.WFD01210AddBTN.click(WFD01210Add);
    Controls.DeleteButton.click(Delete);
    Controls.EditButton.click(Edit);
    Controls.ViewButton.click(View);
    //
    //get data in Manage user
    //GetCostCenter();
    Controls_UserProfile.CLEAR.click(ClearPreview);
    Controls_UserProfile.WFD01210UPLOAD.on('click', function () {
        Controls_UserProfile.SIGNATURE.click();
    });
    var clientUpload = false;
    Controls_UserProfile.SIGNATURE.change(function () {
        if (this.value == '') return;
        //var ext = this.value.match(/\.(.+)$/)[1]; not support folder include .
        var ext = this.value.substr((this.value.lastIndexOf('.') + 1));

        var file = Controls_UserProfile.SIGNATURE[0].files[0]
        var arr = PARAMETER.FILEEXTENSION.split(",");

        if (CheckExtension(ext, arr, file.name, PARAMETER.FILEEXTENSION)) {
            readURL(this);
            //Controls_UserProfile.CLEAR.show();
            WFD01210setEnableButton('#WFD01210ClearPreview');
            clientUpload = true;
        } else {
            this.value = '';

            if (clientUpload) {
                //Controls_UserProfile.CLEAR.show();
                WFD01210setEnableButton('#WFD01210ClearPreview');
            }
            else {
                //Controls_UserProfile.CLEAR.hide();
                WFD01210setDisableButton('#WFD01210ClearPreview');
            }
        }
        //switch (ext.toLowerCase()) {
        //    case 'jpg':
        //    case 'jpeg':
        //    case 'png':
        //    case 'gif':
        //    case 'bmp':
        //    case 'jfif':
        //        readURL(this);
        //        Controls_UserProfile.CLEAR.show();
        //        break;
        //    default:
        //        fnErrorDialog('Error', 'File ' + file.name + ' is not expected file type image');
        //        this.value = '';
        //        Controls_UserProfile.CLEAR.hide();
        //}

    });

    Controls_UserProfile.WFD01210Setup.click(Setup);
    $('#WFD01210BtnResponse_Cost_Center').click(ResCCSetup);
    $('#WFD01210BtnCost_Center').click(Setup);

    //get data tab user
    //getDataTabUserProfile();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        //if (target == "#tab_userprofile") {
        //    getDataTabUserProfile();

        //} else if (target == "#tab_ccbaseorganize") {
        //    getDataTabCCBaseOrganize();
        //}

        if (target == "#tab_ccbaseorganize") {
            getDataTabCCBaseOrganize();
        }

    });
    var _current = "-";
    //Controls_UserProfile.ORGANIZE.select2({
    Controls_UserProfile.ORGANIZE.keyup(function () {
        Controls_UserProfile.DIV_TABLE_ORGANIZE.show();

        if (_current == Controls_UserProfile.ORGANIZE.val()) {
            return;
        }
        console.log('keyup1');
        _current = Controls_UserProfile.ORGANIZE.val();
        GotoPageOne = true;
        getTbOrganize()

    }).focusin(function () {
        Controls_UserProfile.DIV_TABLE_ORGANIZE.show();

        console.log(Controls_UserProfile.ORGANIZE.val());
        if (_current == Controls_UserProfile.ORGANIZE.val()) {
            return;
        }
        console.log('keyup2');
        _current = Controls_UserProfile.ORGANIZE.val();
        getTbOrganize()

    }).focusout(function () {
        setTimeout(function () {
            if (Controls_UserProfile.ORGANIZE.val() == '') {

                Controls_UserProfile.WFD01210GROUP_USERPROFILE.html('');
                Controls_UserProfile.WFD01210DEPARTMENT_USERPROFILE.html('');
                Controls_UserProfile.WFD01210DEVISION_USERPROFILE.html('');
                Controls_UserProfile.WFD01210Section_USERPROFILE.html('');
            }

            //     Controls_UserProfile.DIV_TABLE_ORGANIZE.hide();
        }, 1000);

    });

    if (PARAMETER.SYS_EMP_CODE != '') {
        GetEmpByCode();
    }

    Controls_UserProfile.WFD01210Cancel.click(Cancel);

    Controls_UserProfile.WFD01210Save.click(Save);

    $(".DataNotFound").attr("disabled", true);
    Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.change(CheckSetUpInCharge);

    //Add Search 
    $('#WFD01210FIND_INCHARGE').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //alert("OK");
            //console.log(this.id);
            SearchIn_Charge();
            //      $('#WFD01210SaveIn_Charge').click();
            return false;
        }
    });

    $('#Condition input[type=text]').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            Controls.SearchButton.click();
            return false;
        }
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            Controls_UserProfile.PREVIEW.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
var ClearPreview = function () {
    Controls_UserProfile.SIGNATURE.val('');
    //Controls_UserProfile.CLEAR.hide();
    WFD01210setDisableButton('#WFD01210ClearPreview');
    Controls_UserProfile.PREVIEW.attr('src', PARAMETER.DafaultImage);

}
//var GetDivision = function () {

//    ajax_method.Post(URL_CONST.GET_DIVISTION_DDL, '', true, function (result) {
//        if (result != null) {
//            Controls.WFD01210DIVISTION.html('');
//            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
//            Controls.WFD01210DIVISTION.append(optionhtml1);

//            $.each(result, function (i) {
//                var optionhtml = '<option value="' + result[i].DIV_NAME + '">' + result[i].DIV_NAME + '</option>';

//                Controls.WFD01210DIVISTION.append(optionhtml);
//            });
//            //Controls.WFD01210DIVISTION.select2('open');
//        }

//    }, null);

//}

//$('#DIVISTION').on('change', function () {
//    //alert(this.value);

//    GetDepartment();
//})
//var GetDepartment = function () {

//    var data = WFD01210getDefaultSceenValue();

//    ajax_method.Post(URL_CONST.GET_DEPARTMENT_DDL, data, true, function (result) {
//        if (result != null) {
//            //Controls.WFD01210DEPARTMENT.select2("val", '');

//            Controls.WFD01210DEPARTMENT.html('');
//            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
//            Controls.WFD01210DEPARTMENT.append(optionhtml1);

//            $.each(result, function (i) {
//                var optionhtml = '<option value="' + result[i].DEPT_NAME + '">' + result[i].DEPT_NAME + '</option>';

//                Controls.WFD01210DEPARTMENT.append(optionhtml);
//            });

//        }

//    }, null);

//}
//$('#DEPARTMENT').on('change', function () {
//    //alert(this.value);

//    GetSection();

//})
//var GetSection = function () {

//    var data = WFD01210getDefaultSceenValue();

//    ajax_method.Post(URL_CONST.GET_SECTION_DDL, data, true, function (result) {
//        if (result != null) {

//            //Controls.WFD01210SECTION.select2("val", '');

//            Controls.WFD01210SECTION.html('');
//            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
//            Controls.WFD01210SECTION.append(optionhtml1);

//            $.each(result, function (i) {
//                var optionhtml = '<option value="' + result[i].SEC_NAME + '">' + result[i].SEC_NAME + '</option>';

//                Controls.WFD01210SECTION.append(optionhtml);
//            });

//        }

//    }, null);

//}
//var GetPosition = function () {

//    ajax_method.Post(URL_CONST.GET_POSITION_DDL, '', true, function (result) {
//        if (result != null) {
//            Controls.WFD01210POSITION.html('');
//            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
//            Controls.WFD01210POSITION.append(optionhtml1);

//            $.each(result, function (i) {
//                var optionhtml = '<option value="' + result[i].POST_CODE + '">' + result[i].POST_NAME + '</option>';

//                Controls.WFD01210POSITION.append(optionhtml);
//            });

//        }

//    }, null);

//}
//var GetResponsibility = function () {

//    ajax_method.Post(URL_CONST.GET_RESPONSIBILITY_DDL, '', true, function (result) {
//        if (result != null) {
//            Controls.WFD01210RESPONSIBILITY.html('');
//            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
//            Controls.WFD01210RESPONSIBILITY.append(optionhtml1);
//            $.each(result, function (i) {
//                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].VALUE + '</option>';

//                Controls.WFD01210RESPONSIBILITY.append(optionhtml);
//            });

//        }

//    }, null);

//}

var WFD01210getDefaultSceenValue = function () {
    return {
        DIV_CODE: Controls.WFD01210DIVISTION.val()
        , DEPT_CODE: Controls.WFD01210DEPARTMENT.val()
        , SEC_CODE: Controls.WFD01210SECTION.val()
        , EMP_CODE: Controls.WFD01210EMPLOYEE_CODE.val()
        , EMP_NAME: Controls.WFD01210FIRST_NAME.val()
        , EMP_LASTNAME: Controls.WFD01210LAST_NAME.val()
        , POST_CODE: Controls.WFD01210POSITION.val()
        , ROLE: Controls.WFD01210RESPONSIBILITY.val()
        , COST_CODE: Controls.WFD01210COST_CENTER.val()
        , IN_CHARGE_COST_CENTER: Controls.WFD01210IN_CHARGE_COST_CENTER.val()
        , COMPANY: getMultiControlValue($('#COMPANY'))
        , JOB: $('#JOB').val()
        , SUB_DIV_NAME: $('#SUB_DIVISTION').val()
        , LINE: $('#LINE').val()
        , SECTION_NAME: $('SECTION').val()
        , SPECIAL_ROLE: getMultiControlValue($('#SPECIAL_ROLE'))
        , RESPONSIBILITY: getMultiControlValue($('#RESPONSIBILITY'))
    }
}
var WFD01210Add = function () {
    window.location = URL_CONST.ManageUser + "?EMP_CODE=&MODE=ADD";
}
var ClearMultiple = function (el) {
    $('option', $(this)).each(function (element) {
        $(el).multiselect('deselect', $(this).val());
    });
}
var Clear = function () {

    $('.form-horizontal').find('input.form-control').val('');
    $('.form-horizontal').find('.label-text').text('');

    //$("#RESPONSIBILITY").multiselect('clearSelection');

    $("#COMPANY").multiselect('clearSelection');
    if (defaultValue.company) {
        $("#COMPANY").multiselect('select', defaultValue.company);
    }

    //$("#SPECIAL_ROLE").multiselect('clearSelection');
    $('#RESPONSIBILITY').multiselect('selectAll', false);
    $('#SPECIAL_ROLE').multiselect('selectAll', false);

    //EMPLOYEE_CODE = "";
    //Controls.WFD01210DIVISTION.select2("val", '');
    //Controls.WFD01210DEPARTMENT.select2("val", '');
    //Controls.WFD01210SECTION.select2("val", '');
    //Controls.WFD01210POSITION.select2("val", '');
    //Controls.WFD01210RESPONSIBILITY.select2("val", '');
    //Controls.WFD01210EMPLOYEE_CODE.val('');
    //Controls.WFD01210FIRST_NAME.val('');
    //Controls.WFD01210LAST_NAME.val('');
    //Controls.WFD01210COST_CENTER.val('');
    //Controls.WFD01210IN_CHARGE_COST_CENTER.val('');
    $(".DataNotFound").attr("disabled", true);
    //Controls.WFD01210DATANOTFOUND.hide();
    Controls.SearchDataRow.hide();
    Controls.Pagin.ClearPaginData();
    ClearMessageC();
}


var Search = function () {
    var pagin = Controls.Pagin.GetPaginData();
    var con = WFD01210getDefaultSceenValue();

    Controls.SearchDataRow.show();

    ajax_method.SearchPost(URL_CONST.SEARCH, con, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }
            Controls.DataTable = Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                    {
                        data: null, 'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return '<input type="checkbox" id="chk" name="chk[]" value="' + data.SYS_EMP_CODE + '" data-update_date="' + data.UPDATE_DATE + '">';
                        }, "orderable": false
                    },
                    { data: 'RowNumber', name: 'RowNumber', className: 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY' },
                    { data: 'EMP_CODE', name: 'EMP_CODE' },
                    {
                        data: 'EMP_NAME', name: 'EMP_NAME',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 35); }
                    },
                    {
                        data: 'POST_NAME', name: 'POST_NAME',
                        'render': function (data) {
                            if (data != null && data != '' && data != 'undefined') {
                                return SetTooltipInDataTableByColumn(data, 23);
                            } else {
                                return '';
                            }

                        }
                    },
                    {
                        data: 'JOB', name: 'JOB',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    },
                    {
                        data: 'RESPONSIBILITY', name: 'RESPONSIBILITY', "orderable": false, className: 'text-no-warp',
                        'render': function (data) {
                            if (data != null) {
                                return data.replace(/\,/g, '<br/>');
                            } else {
                                return '';
                            }

                        }
                    },
                    {
                        data: null, 'className': 'text-left',
                        'render': function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME + '">' + data.COST_CODE + '</span>';
                        },
                    },
                    {
                        data: 'DIV_NAME', name: 'DIV_NAME',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 30); }
                    },
                    { data: 'SUB_DIV_NAME', name: 'SUB_DIV_NAME' },
                    {
                        data: 'DEPT_NAME', name: 'DEPT_NAME',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 30); }
                    },
                    {
                        data: 'SECTION_NAME', name: 'SECTION_NAME',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 30); }
                    }
                    ,
                    {
                        data: 'LINE_NAME', name: 'LINE_NAME',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 30); }
                    },
                    {
                        data: 'EMAIL', name: 'EMAIL',
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 40); }
                    },
                    {
                        data: null, name: 'SIGNATURE_PATH', 'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            if (data.SIGNATURE_PATH != null && data.SIGNATURE_PATH != '')
                                return '<a href="#" class="signature_view" data-img="' + data.SIGNATURE_PATH + '">View</a>';
                            else
                                return '';
                        }, "orderable": false
                    },
                ],
                'columnDefs': [
                    {
                        //'targets': 10,
                        //'render': function (data, type, full, meta) {
                        //    return SetTooltipInDataTableByColumn(data.DIV_NAME, 30);
                        //}
                    }
                ],
                'order': [],
                "paging": false,
                searching: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                fixedHeader: true,
                //scrollX: "2000px",

            });
            //var page = Controls.SearchResultTable.Pagin();

            //page.Init(pagin, sorting, changePage, 'itemPerPage');
            Controls.Pagin.Init(pagin, sorting, changePage, 'itemPerPage');
            Controls.SearchDataRow.show();
            //Controls.WFD01210DATANOTFOUND.show();
            $(".DataNotFound").attr("disabled", false);
            // the selector will match all input controls of type :checkbox
            // and attach a click event handler 
            $("input:checkbox").on('click', function () {
                // in the handler, 'this' refers to the box clicked on
                var $box = $(this);
                if ($box.is(":checked")) {

                    var row = $(this).closest("tr");

                    _row = Controls.DataTable.row(row).data(); // Get source data

                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);

                    EMPLOYEE_CODE = $box.val();
                    ENCODE_DATA = _row.EncodeMapping;

                    UPDATE_DATE = $box.data('update_date');
                    console.log(UPDATE_DATE)
                } else {
                    $box.prop("checked", false);
                    EMPLOYEE_CODE = '';
                    ENCODE_DATA = '';
                    UPDATE_DATE = '';
                }
            });
            $('.signature_view').on('click', function () {
                Controls.SIGNATURE_DIALOG.modal('show');
                var con = {
                    PATH: $(this).data('img')
                }
                ajax_method.Post(URL_CONST.CONVERT_PATH_IMG, con, true, function (result) {
                    if (result != "")
                        Controls.IMG_SIGNATURE_DIALOG.attr('src', result);
                    else
                        Controls.IMG_SIGNATURE_DIALOG.attr('src', PARAMETER.DafaultImage);

                });
            });
        }
        else {
            //Controls.WFD01210DATANOTFOUND.hide();
            $(".DataNotFound").attr("disabled", true);
            Controls.SearchDataRow.hide();
            //Controls.Pagin.Clear();
            EMPLOYEE_CODE = "";
        }

    }, null, true, 'SearchDataRow');

}
var Edit = function () {
    if (EMPLOYEE_CODE == "") {
        AlertTextErrorMessage(PARAMETER.NotSelectEdit);
    } else {
        ClearMessageC();
        window.location = URL_CONST.ManageUser + "?EMP_CODE=" + EMPLOYEE_CODE + "&MODE=EDIT&Encode=" + ENCODE_DATA;
    }
}
var View = function () {
    if (EMPLOYEE_CODE == "") {
        AlertTextErrorMessage(PARAMETER.NotSelectView);
    } else {
        ClearMessageC();
        window.location = URL_CONST.ManageUser + "?EMP_CODE=" + EMPLOYEE_CODE + "&MODE=VIEW";
    }
}
var Delete = function () {
    Controls.Pagin.ClearPaginData();
    if (EMPLOYEE_CODE == '') {
        //alert("A single record must be selected to Delete operation.");
        AlertTextErrorMessage(PARAMETER.NoSeleteDelete);
        return;
    }

    loadConfirmAlert(PARAMETER.DeleteConfirm, function (result) {
        if (result) {
            var con = {
                EMP_CODE: EMPLOYEE_CODE,
                UPDATE_DATE: UPDATE_DATE
            };
            ajax_method.Post(URL_CONST.Delete, con, true, function (result) {
                //function
                if (result) {
                    //setTimeout(function () {
                    //    AletTextInfoMessage(PARAMETER.DeleteSuccess);
                    //}, 2000);
                    //WFD01210getDefaultSceenValue = function () {
                    //    return {
                    //        DIV_CODE: ''
                    //       , DEPT_CODE: ''
                    //       , SEC_CODE: ''
                    //       , EMP_CODE: ''
                    //       , EMP_NAME: ''
                    //       , EMP_LASTNAME: ''
                    //       , POST_CODE: ''
                    //       , ROLE: ''
                    //       , COST_CODE: ''
                    //       , IN_CHARGE_COST_CENTER: ''
                    //    }
                    //}
                    Search();
                }
            }, null);
        }
    });
}
var Cancel = function () {
    var msg = '';
    if (PARAMETER.MODE == 'VIEW')
        msg = PARAMETER.CancelView
    else if (PARAMETER.MODE == 'EDIT')
        msg = PARAMETER.CancelEdit
    else if (PARAMETER.MODE == 'ADD')
        msg = PARAMETER.CancelAdd
    loadConfirmAlert(msg, function (result) {
        if (result) {
            //if (PARAMETER.MODE == 'ADD')
            //window.close();
            window.location = URL_CONST.List_PAGE;
        }
    });
}
var Close = function () {
    window.location = URL_CONST.List_PAGE;
}

var CCOpenDialogValidate = function () {
    var valid = true;
    var company = $('#COMPANY').val();
    if (!company) {
        valid = false;
        AlertTextErrorMessagePopup(MESSAGE.COMPANY_NOT_EMPTY, "#AlertMessageArea");
    }

    return valid;
}

var CCDialogInit = function () {
    //CLEAR VALUE IN TABLE
    $('#Incharge_CostCode tbody').empty();
    $('#Selected_Incharge tbody').empty();

    var $element = $('#in-charge-btn, #WFD01210SaveIn_Charge');
    //if (PARAMETER.MODE == 'VIEW') {
    if ((PARAMETER.IS_FAADMIN === 'Y' || PARAMETER.ALLOW_EDIT === 'Y') && (PARAMETER.MODE === 'EDIT' || PARAMETER.MODE === 'ADD')) {
        $element.css({ "visibility": "visible" });
    } else {
        $element.css({ "visibility": "collapse" });
    }    
}

var Setup = function () {
    if (CCOpenDialogValidate()) {
        CCDialogInit();

        isResponseCostCenter = 'N'; //-- Assign value.
        selectResponseCostCenter = 'N';
        selectCostCenter = 'Y';
        $('#WFD01210FIND_INCHARGE').val('');
        $('#chkIncludeInActive').prop('checked', false);
        GetInCharge();
        GetInChargeTable();
        $('#lblCCTitle').html("Select In Charge Cost Center");
        Controls_UserProfile.IN_CHARGE_DIALOG.modal('show');
    }
}

var ResCCSetup = function () {
    if (CCOpenDialogValidate()) {
        CCDialogInit();

        isResponseCostCenter = 'Y'; //-- Assign value.
        selectResponseCostCenter = 'Y';
        selectCostCenter = 'N';
        $('#WFD01210FIND_INCHARGE').val('');
        $('#chkIncludeInActive').prop('checked', false);
        GetInCharge();
        GetInChargeTable();
        $('#lblCCTitle').html("Select Responsible Cost Center");
        Controls_UserProfile.IN_CHARGE_DIALOG.modal('show');

        var $element = $('#in-charge-btn, #WFD01210SaveIn_Charge');
        if (PARAMETER.IsManager == 'Y') {
            $element.css({ "visibility": "collapse" });
        }
        
    }
}

// Callbac function after click search button
var OnSearchBtnClick = function () {

    //$('#EMPLOYEE_CODE').val(GetSearchCriteria($('#EMPLOYEE_CODE')));
    //$('#FIRST_NAME').val(GetSearchCriteria($('#FIRST_NAME')));
    //$('#LAST_NAME').val(GetSearchCriteria($('#LAST_NAME')));
    //$('#COST_CENTER').val(GetSearchCriteria($('#COST_CENTER')));

    Controls.Pagin.ClearPaginData();
    Search();
}

var sorting = function (PaginData) {
    //PaginData.OrderType = PaginData.OrderType == 'asc' ? 'desc' : 'asc';
    //console.log(PaginData);
    Search();
}
var changePage = function (PaginData) {
    //console.log(PaginData);
    Search();
}
//var getDataTabUserProfile = function () {
//    //RESPONSIBILITY
//    ajax_method.Post(URL_CONST.GET_RESPONSIBILITY_DDL, '', true, function (result) {
//        if (result != null) {

//            Controls.WFD01210RESPONSIBILITY_USERPROFILE.html('');
//            var optionhtml1 = '<option value="">' + "" + '</option>';
//            Controls.WFD01210RESPONSIBILITY_USERPROFILE.append(optionhtml1);
//            $.each(result, function (i) {
//                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].VALUE + '</option>';

//                Controls.WFD01210RESPONSIBILITY_USERPROFILE.append(optionhtml);
//            });

//        }

//    }, null);

//}
var getDataTabCCBaseOrganize = function () {
    var pagin = Controls.CCPagin.GetPaginData();
    var con = {
        EMP_CODE: PARAMETER.SYS_EMP_CODE
    }
    ajax_method.SearchPost(URL_CONST.CC_BASE_ON_ORGANIZE, con, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null || typeof pagin.OrderColIndex == 'undefined') pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable(Controls.CCSearchResultTable)) {
                var table = Controls.CCSearchResultTable.DataTable();
                table.destroy();
            }

            Controls.CCSearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { "data": "COST_CODE" },
                    { "data": "COST_NAME" }

                ],
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                //fixedHeader: true
            })
            //var page = Controls.CCSearchResultTable.Pagin();
            //page.Init(pagin, CCBaseOrganizesorting, CCBaseOrganizechangePage, 'itemPerPage');
            Controls.CCPagin.Init(pagin, CCBaseOrganizesorting, CCBaseOrganizechangePage, 'itemPerPage');
            Controls.CCSearchDataRow.show();
        } else {
            Controls.CCPagin.Clear();
            Controls.CCSearchDataRow.show();
        }
    }, null);
}
var CCBaseOrganizesorting = function (PaginData) {
    getDataTabCCBaseOrganize();
}
var CCBaseOrganizechangePage = function (PaginData) {
    getDataTabCCBaseOrganize();
}

var ViewMode = function (result) {
    Controls_UserProfile.WFD01210Save.hide();
    Controls_UserProfile.WFD01210CLOSE.show();
    Controls_UserProfile.WFD01210Cancel.hide();
    Controls_UserProfile.DELEGATE_TO_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.DELEGATE_FROM_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210UPLOAD.hide();
    Controls_UserProfile.CLEAR.hide();
    WFD01210setDisableButton('#WFD01210ClearPreview');
    Controls_UserProfile.COST_CENTER.attr('disabled', 'disabled');
    Controls_UserProfile.ORGANIZE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.attr('disabled', 'disabled');
    $('#COMPANY').attr('disabled', 'disabled');

    //if (SOURCE == "SAP") {
    //    $('#WFD01210POSITION_CODE_USERPROFILE_DIV').hide();
    //    $('#JOBSave_DIV').hide();
    //} else {
    //    $('#WFD01210POSITION_CODE_USERPROFILE_TXT_DIV').hide();
    //    $('#JOBSave_TXT_DIV').hide();
    //}

    $('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');
    $('#JOBSave_TXT').attr('disabled', 'disabled');

    //$('#WFD01210POSITION_CODE_USERPROFILE').attr('disabled', 'disabled');
    //$('#JOB').attr('disabled', 'disabled');

    Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.removeClass('require');
    setControlsRequiredField(requiredMainControl_WFD01210, false);
    $('#COMPANY').removeClass('require');
    //$('#WFD01210POSITION_CODE_USERPROFILE').removeClass('require');
    //$('#JOB').removeClass('require');
    $('#WFD01210ORGANIZE_CODE_USERPROFILE').removeClass('require');

    //
    //Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.attr('disabled', 'disabled');
    //Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.attr('disabled', 'disabled');
    //Controls_UserProfile.WFD01210GROUP_USERPROFILE
    //Controls_UserProfile.WFD01210DEPARTMENT_USERPROFILE
    //Controls_UserProfile.WFD01210DEVISION_USERPROFILE
    //Controls_UserProfile.WFD01210Section_USERPROFILE
    Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210Setup.attr('disabled', 'disabled');
    $('#WFD01210BtnResponse_Cost_Center').attr('disabled', 'disabled');
    $('#WFD01210BtnCost_Center').attr('disabled', 'disabled');
    //$('#JOB').attr('disabled', 'disabled');

    Controls_UserProfile.WFD01210SetupText_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210Mobile_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210DELEGATION_USERPROFILE.attr('disabled', 'disabled');
    $("input:checkbox").attr('disabled', 'disabled');

    if ($('#WFD01210FA_Supervisor_USERPROFILE').is(':checked')) {
        $('#WFD01210BtnResponse_Cost_Center').prop('disabled', false);
    }

    if ($('#WFD01210FA_Window_USERPROFILE:checked').is(':checked')) {
        $('#WFD01210BtnCost_Center').prop('disabled', false);
    }
}
var EditManagerMode = function(result)
{
    $("input:checkbox").attr('disabled', 'disabled');
    $('#WFD01210FA_Window_USERPROFILE').prop('disabled', false);
    $('#WFD01210BtnCost_Center').prop('disabled', false);
    setControlsRequiredField(requiredMainControl_WFD01210, false);
    Controls_UserProfile.WFD01210UPLOAD.show();
    if (Controls_UserProfile.PREVIEW.attr('src') == PARAMETER.DafaultImage) {
        //Controls_UserProfile.CLEAR.hide();
        WFD01210setDisableButton('#WFD01210ClearPreview');
    } else {
        //Controls_UserProfile.CLEAR.show();
        WFD01210setEnableButton('#WFD01210ClearPreview');
    }
    $('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');
    $('#JOBSave_TXT').attr('disabled', 'disabled');
    Controls_UserProfile.COST_CENTER.attr('disabled', 'disabled');
    Controls_UserProfile.ORGANIZE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.attr('disabled', 'disabled');

    Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.attr('disabled', 'disabled');

    $('#COMPANY').attr('disabled', 'disabled');

    Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.removeClass('require');
    Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.removeClass('require');
    Controls_UserProfile.ORGANIZE.removeClass('require');
    $('#COMPANY').removeClass('require');
    $('#WFD01210POSITION_CODE_USERPROFILE').removeClass('require');
    //$('#JOBSave').removeClass('require');
    //
    //$('#JOBSave').attr('disabled', 'disabled');
    //Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.attr('disabled', 'disabled');
    //Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.attr('disabled', 'disabled');
    Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');
    if ($('#WFD01210FA_Supervisor_USERPROFILE').is(':checked')) {
        $('#WFD01210BtnResponse_Cost_Center').prop('disabled', false);
    }    
    CCDialogInit();   
    if (PARAMETER.TFASTEmployeeNo != result.SYS_EMP_CODE) {
        Controls_UserProfile.DELEGATE_TO_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.DELEGATE_FROM_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210UPLOAD.hide();
        Controls_UserProfile.CLEAR.hide();
        Controls_UserProfile.WFD01210Mobile_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210DELEGATION_USERPROFILE.attr('disabled', 'disabled');
    }
}
var EditMode = function (result) {
    setDisabled('WFD01210FA_Window_USERPROFILE', 'WFD01210BtnCost_Center');
    setDisabled('WFD01210FA_Supervisor_USERPROFILE', 'WFD01210BtnResponse_Cost_Center');

    //alert(PARAMETER.IS_FAADMIN);
    Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.focus();
    if (PARAMETER.IS_FAADMIN == 'Y') {
        if (SOURCE == "FAS") {
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
            if (Controls_UserProfile.PREVIEW.attr('src') == PARAMETER.DafaultImage) {
                //Controls_UserProfile.CLEAR.hide();
                WFD01210setDisableButton('#WFD01210ClearPreview');
            } else {
                //Controls_UserProfile.CLEAR.show();
                WFD01210setEnableButton('#WFD01210ClearPreview');
            }


            //$('#WFD01210POSITION_CODE_USERPROFILE_TXT_DIV').hide();
            //$('#JOBSave_TXT_DIV').hide();

        }
        else {
            setControlsRequiredField(requiredMainControl_WFD01210, false);
            Controls_UserProfile.WFD01210UPLOAD.show();
            if (Controls_UserProfile.PREVIEW.attr('src') == PARAMETER.DafaultImage) {
                //Controls_UserProfile.CLEAR.hide();
                WFD01210setDisableButton('#WFD01210ClearPreview');
            } else {
                //Controls_UserProfile.CLEAR.show();
                WFD01210setEnableButton('#WFD01210ClearPreview');
            }
            Controls_UserProfile.COST_CENTER.attr('disabled', 'disabled');
            Controls_UserProfile.ORGANIZE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.attr('disabled', 'disabled');
            $('#COMPANY').attr('disabled', 'disabled');
            //$('#WFD01210POSITION_CODE_USERPROFILE_DIV').hide();
            //$('#WFD01210POSITION_CODE_USERPROFILE').attr('disabled', 'disabled');

            $('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');
            $('#JOBSave_TXT').attr('disabled', 'disabled');

            //$('#JOBSave_DIV').hide();
            //$('#JOBSave').attr('disabled', 'disabled');
            //


            //
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.removeClass('require');
            Controls_UserProfile.ORGANIZE.removeClass('require');
            $('#COMPANY').removeClass('require');
            //$('#WFD01210POSITION_CODE_USERPROFILE').removeClass('require');
            //$('#JOBSave').removeClass('require');
            //
            //$('#JOBSave').attr('disabled', 'disabled');
            //Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.attr('disabled', 'disabled');
            //Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');
        }

    }
    else if (PARAMETER.ALLOW_EDIT == 'Y') {

        if (SOURCE == "FAS") {
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
            if (Controls_UserProfile.PREVIEW.attr('src') == PARAMETER.DafaultImage) {
                //Controls_UserProfile.CLEAR.hide();
                WFD01210setDisableButton('#WFD01210ClearPreview');
            } else {
                //Controls_UserProfile.CLEAR.show();
                WFD01210setEnableButton('#WFD01210ClearPreview');
            }


            //$('#WFD01210POSITION_CODE_USERPROFILE_TXT_DIV').hide();
            //$('#JOBSave_TXT_DIV').hide();

        }
        else {
            setControlsRequiredField(requiredMainControl_WFD01210, false);
            Controls_UserProfile.WFD01210UPLOAD.show();
            if (Controls_UserProfile.PREVIEW.attr('src') == PARAMETER.DafaultImage) {
                //Controls_UserProfile.CLEAR.hide();
                WFD01210setDisableButton('#WFD01210ClearPreview');
            } else {
                //Controls_UserProfile.CLEAR.show();
                WFD01210setEnableButton('#WFD01210ClearPreview');
            }

            Controls_UserProfile.COST_CENTER.attr('disabled', 'disabled');
            Controls_UserProfile.ORGANIZE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.attr('disabled', 'disabled');

            Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.attr('disabled', 'disabled');

            $('#COMPANY').attr('disabled', 'disabled');
            //$('#WFD01210POSITION_CODE_USERPROFILE_DIV').hide();
            //$('#WFD01210POSITION_CODE_USERPROFILE').attr('disabled', 'disabled');
            //$('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');

            //$('#JOBSave_DIV').hide();
            //$('#JOBSave').attr('disabled', 'disabled');
            //$('#JOBSave_TXT').attr('disabled', 'disabled');

            $('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');
            $('#JOBSave_TXT').attr('disabled', 'disabled');
            //
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.removeClass('require');
            Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.removeClass('require');
            Controls_UserProfile.ORGANIZE.removeClass('require');
            $('#COMPANY').removeClass('require');
            $('#WFD01210POSITION_CODE_USERPROFILE').removeClass('require');
            //$('#JOBSave').removeClass('require');
            //
            //$('#JOBSave').attr('disabled', 'disabled');
            //Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.attr('disabled', 'disabled');
            //Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.attr('disabled', 'disabled');
            Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');

            $("input:checkbox").attr('disabled', 'disabled');


            $('#WFD01210FA_Supervisor_USERPROFILE').prop('disabled', false);
            $('#WFD01210FA_Window_USERPROFILE').prop('disabled', false);

        }

        
    }

    else {


        Controls_UserProfile.WFD01210Cancel.hide();
        setControlsRequiredField(requiredMainControl_WFD01210, false);
        //Controls_UserProfile.WFD01210UPLOAD.hide(); //2017-05-09 Not FA no Upload Signature. Suphachai L.
        if (Controls_UserProfile.PREVIEW.attr('src') == PARAMETER.DafaultImage) {
            //Controls_UserProfile.CLEAR.hide();
            WFD01210setDisableButton('#WFD01210ClearPreview');
        } else {
            //Controls_UserProfile.CLEAR.show();
            WFD01210setEnableButton('#WFD01210ClearPreview');
        }
        Controls_UserProfile.COST_CENTER.attr('disabled', 'disabled');
        Controls_UserProfile.ORGANIZE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.attr('disabled', 'disabled');
        $('#COMPANY').attr('disabled', 'disabled');

        //$('#WFD01210POSITION_CODE_USERPROFILE').attr('disabled', 'disabled');
        //$('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');

        //$('#JOBSave').attr('disabled', 'disabled');
        //$('#JOBSave_TXT').attr('disabled', 'disabled');
        //if (SOURCE == "SAP") {
        //    $('#WFD01210POSITION_CODE_USERPROFILE_DIV').hide();
        //    $('#JOBSave_DIV').hide();
        //} else {
        //    $('#WFD01210POSITION_CODE_USERPROFILE_TXT_DIV').hide();
        //    $('#JOBSave_TXT_DIV').hide();
        //}
        $('#WFD01210POSITION_CODE_USERPROFILE_TXT').attr('disabled', 'disabled');
        $('#JOBSave_TXT').attr('disabled', 'disabled');
        //
        Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.removeClass('require');
        Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.removeClass('require');
        Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.removeClass('require');
        Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.removeClass('require');
        Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.removeClass('require');
        Controls_UserProfile.ORGANIZE.removeClass('require');
        $('#COMPANY').removeClass('require');
        //$('#WFD01210POSITION_CODE_USERPROFILE').removeClass('require');
        //$('#JOBSave').removeClass('require');

        //
        //$('#JOBSave').attr('disabled', 'disabled');
        //Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.attr('disabled', 'disabled');
        //Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.attr('disabled', 'disabled');
        //Controls_UserProfile.WFD01210Setup.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210SetupText_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210Setup.attr('disabled', 'disabled');

        if ($('#WFD01210FA_Supervisor_USERPROFILE').is(':checked')) {
            $('#WFD01210BtnResponse_Cost_Center').attr('disabled', false);
        }
        if ($('#WFD01210FA_Window_USERPROFILE').is(':checked')) {
            $('#WFD01210BtnCost_Center').attr('disabled', false);
        }
        
        //$('#WFD01210BtnCost_Center').attr('disabled', 'disabled');

        Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');
        $("input:checkbox").attr('disabled', 'disabled');
    }

    if (PARAMETER.TFASTEmployeeNo != result.SYS_EMP_CODE) {
        Controls_UserProfile.DELEGATE_TO_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.DELEGATE_FROM_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210UPLOAD.hide();
        Controls_UserProfile.CLEAR.hide();
        Controls_UserProfile.WFD01210Mobile_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210Email_USERPROFILE.attr('disabled', 'disabled');
        Controls_UserProfile.WFD01210DELEGATION_USERPROFILE.attr('disabled', 'disabled');
    } 
}

var TMPEmp = {};

var GetEmpByCode = function () {
    var con = {
        EMP_CODE: PARAMETER.SYS_EMP_CODE
    };
    ajax_method.Post(URL_CONST.GET_EMPLOYEE_BY_CODE, con, true, function (result) {
        TMPEmp = {};
        if (result != null) {
            console.log(result);
            TMPEmp = result;
            SOURCE = result.SOURCE;
            Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.val(result.SYS_EMP_CODE);
            Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.val(result.EMP_CODE);
            Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.val(result.EMP_TITLE);
            Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.val(result.EMP_NAME);
            Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.val(result.EMP_LASTNAME);
            //Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.select2('val',result.POST_CODE);
            Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.val(result.POST_NAME);
            Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.select2('val', result.ROLE);
            Controls_UserProfile.ORGANIZE.val(result.ORG_CODE);
            Controls_UserProfile.WFD01210GROUP_USERPROFILE.html(result.GRP_NAME);
            Controls_UserProfile.WFD01210DEPARTMENT_USERPROFILE.html(result.DEPT_NAME);
            Controls_UserProfile.WFD01210DEVISION_USERPROFILE.html(result.DIV_NAME);
            Controls_UserProfile.WFD01210Section_USERPROFILE.html(result.SEC_NAME);
            Controls_UserProfile.WFD01210UPDATEDATE_USERPROFILE.val(result.UPDATE_DATE);

            //$('#COMPANY').select2("val", result.COMPANY);
            $('#COMPANY').val(result.COMPANY).trigger('change');
            getCostCenterData_ddl('#WFD01210COST_CENTER_USERPROFILE', $('#COMPANY').val());
            $('#WFD01210COMPANY_USERPROFILE').text(nullToEmpty(result.CMP_ABBR));
            $('#WFD01210DEVISION_USERPROFILE').text(nullToEmpty(result.DIV_NAME));
            $('#WFD01210SUB_DEPARTMENT_USERPROFILE').text(nullToEmpty(result.SUB_DIV_NAME))
            $('#WFD01210Section_USERPROFILE').text(nullToEmpty(result.SECTION_NAME))
            $('#WFD01210LINE_USERPROFILE').text(nullToEmpty(result.LINE_NAME))
            //$('#JOBSave').select2('val', result.JOB);

            $('#WFD01210POSITION_CODE_USERPROFILE_TXT').val(nullToEmpty(result.POST_NAME));
            $('#JOBSave_TXT').val(nullToEmpty(result.JOB_NAME));

            //Controls_UserProfile.COST_CENTER.select2('val', result.COST_CODE);
            Controls_UserProfile.COST_CENTER.val(result.COST_CODE).trigger('change');

            //WHEN COST CENTER EXPIRE
            var _cost = $('#WFD01210COST_CENTER_USERPROFILE').select2('val');
            if (!_cost && result.COST_CODE) {
                $('#WFD01210COST_CENTER_USERPROFILE').append($('<option/>').attr("value", result.COST_CODE).text(result.COST_CODE));
                $('#WFD01210COST_CENTER_USERPROFILE').select2('val', result.COST_CODE);
            }

            //$('#WFD01210COST_CENTER_USERPROFILE').select2('val', result.COST_CODE);

            if (result.AEC_USER == 'Y') {
                $('#WFD01210AEC_User_USERPROFILE').prop('checked', true);
            }

            if (result.AEC_MANAGER == 'Y') {
                $('#WFD01210AEC_Manager_USERPROFILE').prop('checked', true);
                setDisplayNone('WFD01210AEC_Manager_USERPROFILE', 'aecManager');
            }

            if (result.FAADMIN == 'Y') {
                Controls_UserProfile.WFD01210FAADMIN_USERPROFILE.prop('checked', true);
            }

            if (result.AEC_GENERAL_MANAGER == 'Y') {
                $('#WFD01210AEC_General_Manager_USERPROFILE').prop('checked', true);
            }

            if (result.AEC_USER_FOR_RESP_CC == 'Y') {
                $('#WFD01210AEC_User_For_Response_CC_USERPROFILE').prop('checked', true);
            }

            if (result.FASupervisor == 'Y') {
                $('#WFD01210FA_Supervisor_USERPROFILE').prop('checked', true);
                setDisplayNone('WFD01210FA_Supervisor_USERPROFILE', 'faSupervisor');

            }

            if (result.FAWindow == 'Y') {
                $('#WFD01210FA_Window_USERPROFILE').prop('checked', true);
                setDisplayNone('WFD01210FA_Window_USERPROFILE', 'faWindow');
            }

            if (result.SPEC_DIV != null && result.SPEC_DIV != "") {
                var res = result.SPEC_DIV.split(",");
                $.each(res, function (index, value) {
                    $("input[name='SPECIAL_TEAM_USERPROFILE[]']").each(function () {
                        if ($(this).val() == value) {
                            $(this).prop('checked', true);
                        }
                    });
                });
            }

            Controls_UserProfile.WFD01210Email_USERPROFILE.val(result.EMAIL);
            Controls_UserProfile.WFD01210Mobile_USERPROFILE.val(result.TEL_NO);
            Controls_UserProfile.WFD01210DELEGATION_USERPROFILE.select2("val", result.DELEGATE_PIC);

            if (result.DELEGATE_FROM != null && result.DELEGATE_FROM != "") {
                var date = new Date(result.DELEGATE_FROM);
                var formattedDate = moment(date).format('DD.MM.YYYY');
                Controls_UserProfile.DELEGATE_FROM_USERPROFILE.val(formattedDate);
            }
            if (result.DELEGATE_TO != null && result.DELEGATE_TO != "") {
                var date = new Date(result.DELEGATE_TO);
                var formattedDate = moment(date).format('DD.MM.YYYY');
                Controls_UserProfile.DELEGATE_TO_USERPROFILE.val(formattedDate);
            }
            if (result.SIGNATURE_PATH != '') {
                Controls_UserProfile.PREVIEW.attr('src', result.SIGNATURE_PATH);
            }
            Controls_UserProfile.WFD01210SetupText_USERPROFILE.val(result.IN_CHARGE_COST_CODE);

            $('#WFD01210txtResponse_Cost_Center').text(result.IN_CHARGE_RESPONSE_COST_CODE)

            $('#WFD01210txtCost_Center').text(result.IN_CHARGE_COST_CODE)

            //Load Image
            clientUpload = false;
            //ajax_method.Post(URL_CONST.GET_SIGNATURE, con, true, function (rs) {
            //    console.log(rs);
            //    if (rs == "") {
            //        Controls_UserProfile.PREVIEW.attr("src", rs);
            //    } else {

            //    }
            //    console.log("AJAX");
            //});
            // console.log("VIEW");
            if (PARAMETER.MODE == 'VIEW') {
                ViewMode(result);
            } else if (PARAMETER.MODE == 'EDIT') {
                EditMode(result);
                if (PARAMETER.IsManager == 'Y') {
                    EditManagerMode(result);
                }
            }   
        }

    }, null, true, 'tab_userprofile');
}

function nullToEmpty(val) {
    return (val == undefined || val == null) ? "" : val;
}

function GetMultipleAECUserList(_selectedList) {
    if (_selectedList != null) {
        _com = (_selectedList + '');
        _com = _com.replace(/,/g, '#');
        return _com;
    }
    else {
        return null;
    }
}

var Save = function () {
    var spec = [];
    $("input[name='SPECIAL_TEAM_USERPROFILE[]']:checked").each(function () {
        spec.push($(this).val());
    });

    var fileUpload = $("#SIGNATURE").get(0);
    var files = fileUpload.files;

    // Create FormData object  
    var con = new FormData();
    con.append('SYS_EMP_CODE', Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.val());
    con.append('EMP_CODE', Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.val());
    con.append('EMP_TITLE', Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.val());
    con.append('EMP_NAME', Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.val());
    con.append('EMP_LASTNAME', Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.val());
    con.append('ORG_CODE', Controls_UserProfile.ORGANIZE.val());
    var costCode = Controls_UserProfile.COST_CENTER.val();
    con.append('COST_CODE', costCode ? costCode : '');
    //con.append('POST_CODE', Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.val());
    //con.append('POST_NAME', Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.find('option:selected').text());
    con.append('POST_NAME', $('#WFD01210POSITION_CODE_USERPROFILE_TXT').val());
    con.append('EMAIL', Controls_UserProfile.WFD01210Email_USERPROFILE.val());
    con.append('TEL_NO', Controls_UserProfile.WFD01210Mobile_USERPROFILE.val());
    var role = Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.val();
    con.append('ROLE', role ? role : '');
    con.append('SPEC_DIV', spec.join());
    con.append('FAADMIN', Controls_UserProfile.WFD01210FAADMIN_USERPROFILE.is(":checked") ? 'Y' : 'N');
    con.append('DELEGATE_PIC', Controls_UserProfile.WFD01210DELEGATION_USERPROFILE.val());
    con.append('DELEGATE_FROM', Controls_UserProfile.DELEGATE_FROM_USERPROFILE.val());
    con.append('DELEGATE_TO', Controls_UserProfile.DELEGATE_TO_USERPROFILE.val());
    con.append('SOURCE', SOURCE);

    con.append('IN_CHARGE_COST_CODE', $("#WFD01210txtCost_Center").val());
    con.append('IN_RESPONSE_CHARGE_COST_CODE', $("#WFD01210txtResponse_Cost_Center").val());

    con.append('UPDATE_DATE', Controls_UserProfile.WFD01210UPDATEDATE_USERPROFILE.val());

    //con.append('JOB', $('#JOBSave').val());
    //con.append('JOB_NAME', $('#JOBSave').find('option:selected').text());
    con.append('JOB_NAME', $('#JOBSave_TXT').val());

    var aec_list = GetMultipleAECUserList($('#WFD01210AECUser_USERPROFILE').val());
    con.append('AEC_USER_LIST', aec_list ? aec_list : '');
    var company = $('#COMPANY').val();
    con.append('COMPANY', company ? company : '');
    //con.append('SP_ROLE_COMPANY', $('#SP_ROLE_COMPANY').val());

    con.append('AEC_USER', $('#WFD01210AEC_User_USERPROFILE').is(":checked") ? 'Y' : 'N');
    con.append('AEC_MANAGER', $('#WFD01210AEC_Manager_USERPROFILE').is(":checked") ? 'Y' : 'N');
    con.append('AEC_GENERAL_MANAGER', $('#WFD01210AEC_General_Manager_USERPROFILE').is(":checked") ? 'Y' : 'N');


    con.append('FASupervisor', $('#WFD01210FA_Supervisor_USERPROFILE').is(":checked") ? 'Y' : 'N');
    con.append('FAWindow', $('#WFD01210FA_Window_USERPROFILE').is(":checked") ? 'Y' : 'N');

    //// Looping over all files and add it to FormData object  
    //for (var i = 0; i < files.length; i++) {
    //    con.append(files[i].name, files[i]);
    //}
    if (files.length > 0) {
        con.append('FILE', files[0]);
        con.append('UPDATE_SIGNATURE_PATH_FLAG', 'Y');
    } else if (Controls_UserProfile.PREVIEW.attr('src') != PARAMETER.DafaultImage) {
        con.append('UPDATE_SIGNATURE_PATH_FLAG', 'N');
    } else {
        con.append('UPDATE_SIGNATURE_PATH_FLAG', '');
    }
    //var con = {
    //    EMP_CODE                : Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.val(),
    //    EMP_TITLE               : Controls_UserProfile.WFD01210EMP_TITLE_USERPROFILE.val(),
    //    EMP_NAME                : Controls_UserProfile.WFD01210EMP_FIRST_NAME_USERPROFILE.val(),
    //    EMP_LASTNAME            : Controls_UserProfile.WFD01210EMP_LAST_NAME_USERPROFILE.val(),
    //    ORG_CODE                : Controls_UserProfile.ORGANIZE.val(),
    //    COST_CODE               : Controls_UserProfile.COST_CENTER.val(),
    //    POST_CODE               : Controls_UserProfile.WFD01210POSITION_CODE_USERPROFILE.val(),
    //    POST_NAME               : Controls_UserProfile.WFD01210POSITION_NAME_USERPROFILE.val(),
    //    EMAIL                   : Controls_UserProfile.WFD01210Email_USERPROFILE.val(),
    //    TEL_NO                  : Controls_UserProfile.WFD01210Mobile_USERPROFILE.val(),
    //    ROLE                    : Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.val(),
    //    SPEC_DIV                : spec.join(),
    //    FAADMIN                 : Controls_UserProfile.WFD01210FAADMIN_USERPROFILE.is(":checked") ? 'Y' : 'N',
    //    DELEGATE_PIC            : Controls_UserProfile.WFD01210DELEGATION_USERPROFILE.val(),
    //    DELEGATE_FROM           : Controls_UserProfile.DELEGATE_FROM_USERPROFILE.val(),
    //    DELEGATE_TO             : Controls_UserProfile.DELEGATE_TO_USERPROFILE.val(),
    //    SOURCE                  : SOURCE,
    //    FILE                    : fileData
    //}
    loadConfirmAlert(PARAMETER.ConfirmSave, function (result) {
        if (result) {
            Controls_UserProfile.DIV_TABLE_ORGANIZE.hide();

            if (PARAMETER.MODE == "ADD") {
                ajax_method.PostFile(URL_CONST.INSERT, con, function (result) {
                    console.log(result);
                    if (result.ObjectResult) {
                        window.location = URL_CONST.ManageUser + "?EMP_CODE=" + $('#COMPANY').val() + '.'.concat(Controls_UserProfile.WFD01210EMPLOYEE_CODE.val()) + "&MODE=EDIT";
                        setTimeout(function () {
                            AletTextInfoMessage(PARAMETER.SaveSuccess);
                        }, 1000);
                    }

                });
            } else if (PARAMETER.MODE == "EDIT") {
                ajax_method.PostFile(URL_CONST.UPDATE, con, function (result) {
                    console.log(result)
                    if (result.ObjectResult) {
                        GetEmpByCode();
                        setTimeout(function () {
                            AletTextInfoMessage(PARAMETER.SaveSuccess);
                        }, 1000);
                    }
                });
            }
        }
    });


}
var setControlsRequiredField = function (controls, isRequired) {
    for (var i = 0; i < controls.length; i++) {
        if (isRequired == true) {
            if (controls[i].hasClass('select2')) {
                controls[i].Select2Require(true);
            } else {
                controls[i].addClass('require');
            }
        } else {
            if (controls[i].hasClass('select2')) {
                controls[i].Select2Require(false);
            } else {
                controls[i].removeClass("require");
            }
        }
    }
}

//set table organize
var _find = "-";
var _OrgCode = "-";
var getTbOrganize = function () {
    var pagin = Controls_UserProfile.OrgPagin.GetPaginData();
    if (GotoPageOne) {
        pagin.CurrentPage = 1;
    }
    var con = {
        FIND: Controls_UserProfile.ORGANIZE.val(),
        ORG_CODE: PARAMETER.ORG_CODE,
        COMPANY: $('#COMPANY').val()
    }

    console.log('get org');

    //if (con.FIND != _find || con.ORG_CODE != _OrgCode) {
    //    _find = con.FIND;
    //    _OrgCode = con.ORG_CODE;
    //} else {
    //    console.log("getTbOrganize not change");
    //    return;
    //}
    // console.log("Search Process");
    // console.log(pagin);
    ajax_method.SearchPost(URL_CONST.GET_ORG_TABLE, con, pagin, true, function (datas, pagin) {
        //  console.log(pagin);
        if (datas == null || datas.length == 0) {
            Controls_UserProfile.OrgPagin.Clear();
        }
        // Controls_UserProfile.HAVE_DATA.show();
        // Controls_UserProfile.NO_DATA.hide();


        if (pagin.OrderColIndex == null || typeof pagin.OrderColIndex == 'undefined') pagin.OrderColIndex = 1;
        if (!pagin.OrderType) pagin.OrderType = 'asc';

        if ($.fn.DataTable.isDataTable(Controls_UserProfile.TABLEORGANIZE)) {
            var table = Controls_UserProfile.TABLEORGANIZE.DataTable();
            table.destroy();
        }
        console.log("Found");
        console.log(datas.length);
        console.log(datas);
        Controls_UserProfile.TABLEORGANIZE.DataTable({
            data: datas,
            "columns": [
                { "data": "CMP_ABBR", className: 'cursorPointer' },
                { "data": "ORG_CODE", className: 'cursorPointer' },
                { "data": "COMP_CODE", className: 'cursorPointer' },
                { "data": "GRP_NAME", className: 'cursorPointer' },
                { "data": "DIV_NAME", className: 'cursorPointer' },
                { "data": "SUB_DIV_NAME", className: 'cursorPointer' },
                { "data": "DEPT_NAME", className: 'cursorPointer' },
                { "data": "SECTION_NAME", className: 'cursorPointer' },
                { "data": "LINE_NAME", className: 'cursorPointer' },
            ],
            'order': [],
            "paging": false,
            searching: false,
            paging: false,
            retrieve: true,
            "bInfo": false,
            "autoWidth": false,
            "language": {
                "emptyTable": "No data found"
            }
        })
        var page = Controls_UserProfile.TABLEORGANIZE.Pagin();
        page.Init(pagin, TbOrganizesorting, getTbOrganize, 'itemPerPage');
        Controls_UserProfile.OrgPagin.Init(pagin, TbOrganizesorting, getTbOrganize, 'itemPerPage');


        onRowClick("tableOrganize", function (row) {
            var CMP_ABBR = row.getElementsByTagName("td")[0];
            var ORG_CODE = row.getElementsByTagName("td")[1];
            var COMP_CODE = row.getElementsByTagName("td")[2];
            var GRP_NAME = row.getElementsByTagName("td")[3];
            var DIV_NAME = row.getElementsByTagName("td")[4];
            var SUB_DIV_NAME = row.getElementsByTagName("td")[5];
            var DEPT_NAME = row.getElementsByTagName("td")[6];
            var SEC_NAME = row.getElementsByTagName("td")[7];
            var LINE_NAME = row.getElementsByTagName("td")[8];
            if (ORG_CODE != undefined) {
                Controls_UserProfile.ORGANIZE.val(ORG_CODE.innerHTML);
                Controls_UserProfile.WFD01210GROUP_USERPROFILE.html(' ' + GRP_NAME.innerHTML);
                $('#WFD01210DEPARTMENT_USERPROFILE').html(' ' + DEPT_NAME.innerHTML);
                Controls_UserProfile.WFD01210DEVISION_USERPROFILE.html(' ' + DIV_NAME.innerHTML);
                Controls_UserProfile.WFD01210Section_USERPROFILE.html(' ' + SEC_NAME.innerHTML);
                $('#WFD01210LINE_USERPROFILE').html(' ' + LINE_NAME.innerHTML);
                $('#WFD01210SUB_DEPARTMENT_USERPROFILE').html(' ' + SUB_DIV_NAME.innerHTML);
                $('#WFD01210COMPANY_USERPROFILE').html(' ' + CMP_ABBR.innerHTML);

                GotoPageOne = true;

                Controls_UserProfile.DIV_TABLE_ORGANIZE.hide();
            }
        });

        // Controls_UserProfile.TABLEORGANIZE.focusout(function () {
        //    Controls_UserProfile.DIV_TABLE_ORGANIZE.hide();
        // });
        //} else {
        //   Controls_UserProfile.OrgPagin.Clear();
        //Controls_UserProfile.NO_DATA.show();
        //Controls_UserProfile.HAVE_DATA.hide();
        //}
    }, null);
    GotoPageOne = false;
}
var TbOrganizesorting = function (PaginData) {
    getTbOrganize();
}
var TbOrganizechangePage = function (PaginData) {

    getTbOrganize();
}
$('#btnCloseOrgCode').click(function (e) {
    Controls_UserProfile.DIV_TABLE_ORGANIZE.hide();
});
function onRowClick(tableId, callback) {
    var table = document.getElementById(tableId),
        rows = table.getElementsByTagName("tr"),
        i;
    for (i = 0; i < rows.length; i++) {
        table.rows[i].onclick = function (row) {
            return function () {
                callback(row);
            };
        }(table.rows[i]);
    }
};

var rightToTable = function () {
    $('#Incharge_CostCode > tbody >tr.detail.table-row-select').each(function () {
        var content = '<tr class="detail"><td>' + $(this)[0].cells[1].innerHTML + '</td><td>' + $(this)[0].cells[2].innerHTML + '</td><td style="display: none;">' + $(this)[0].cells[3].innerHTML + ' </td><td style="display: none;">' + $(this)[0].cells[4].innerHTML + ' </td></tr>';
        $(this).closest('tr').remove();
        $('#Selected_Incharge > tbody').append(content);
    });
    $("#Selected_Incharge > tbody >tr.detail").off('click').on('click', function () {
        $(this).toggleClass("table-row-select");
    });

    $("#Incharge_CostCode > tbody >tr.detail").off('click').on('click', function () {
        $(this).toggleClass("table-row-select");
    });
}
var LeftToTable = function () {
    $('#Selected_Incharge > tbody >tr.detail.table-row-select').each(function () {
        var content = '<tr class="detail"><td style="display: none;"></td><td>' + $(this)[0].cells[0].innerHTML + '</td><td>' + $(this)[0].cells[1].innerHTML + '</td><td style="display: none;">' + $(this)[0].cells[2].innerHTML + '</td><td style="display: none;">' + $(this)[0].cells[3].innerHTML + ' </td></tr>';
        $(this).closest('tr').remove();
        //$("#Incharge_CostCode > tbody > tr.header." + $(this)[0].cells[2].innerHTML).after(content);
        $("#Incharge_CostCode > tbody").append(content);
    });
    $("#Selected_Incharge > tbody >tr.detail").off('click').on('click', function () {
        $(this).toggleClass("table-row-select");
    });

    $("#Incharge_CostCode > tbody >tr.detail").off('click').on('click', function () {
        $(this).toggleClass("table-row-select");
    });
}

var CancelIn_Charge = function () {
    Controls_UserProfile.IN_CHARGE_DIALOG.modal('hide');
    //if ($('#chkIncludeInActive').is(':checked')) {
    //    $('#chkIncludeInActive').prop('checked', false);
    //};
}

var SaveIn_Charge = function () {
    //Clear old value before
    rescc_in_charge = [];
    in_charge = [];
    //debugger;
    var _span = ''
    if (selectResponseCostCenter == 'Y') {
        $('#Selected_Incharge > tbody >tr').each(function () {           
            rescc_in_charge.push($(this)[0].cells[0].children[0].innerHTML);//$(this)[0].cells[0].innerHTML
        });

    }

    if (selectCostCenter == 'Y') {
        $('#Selected_Incharge > tbody >tr').each(function () {
            in_charge.push($(this)[0].cells[0].children[0].innerHTML);//$(this)[0].cells[0].innerHTML
        });

    }

    Controls_UserProfile.WFD01210SetupText_USERPROFILE.val(in_charge.join());

    $("#WFD01210txtResponse_Cost_Center").val(rescc_in_charge.join());
    $("#WFD01210txtCost_Center").val(in_charge.join());

    var con = new FormData();
    con.append('SYS_EMP_CODE', Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.val());
    con.append('EMP_CODE', Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.val());
    con.append('IN_RESPONSE_CHARGE_COST_CODE', rescc_in_charge.join());
    con.append('IN_CHARGE_COST_CODE', in_charge.join());
    con.append('COMPANY', $('#COMPANY').val());
    loadConfirmAlert(PARAMETER.ConfirmSave, function (result) {
        if (!result) {
            return;
        }

        if (selectResponseCostCenter == "Y") {
            ajax_method.PostFile(URL_CONST.INSINCHARGERESCOST, con, function (result) {
                AletTextInfoMessage(PARAMETER.SaveSuccess);
                selectResponseCostCenter = null;

                Controls_UserProfile.IN_CHARGE_DIALOG.modal('hide');
            }, 1000);
        } else if (selectCostCenter == "Y") {
            ajax_method.PostFile(URL_CONST.INSINCHARGECOST, con, function (result) {
                AletTextInfoMessage(PARAMETER.SaveSuccess);
                selectCostCenter = null;
                Controls_UserProfile.IN_CHARGE_DIALOG.modal('hide');
            }, 1000);
        }
        // Modified for save on main page only by Pawares M. 20190921
        Controls_UserProfile.IN_CHARGE_DIALOG.modal('hide');

    }, 1000);
    // Controls_UserProfile.IN_CHARGE_DIALOG.modal('hide');
}
var SearchIn_Charge = function () {
    GetInCharge();
}
var GetInCharge = function () {
    var con = {
        FIND: $('#WFD01210FIND_INCHARGE').val(),
        NO_ASSET_FLAG: $('#chkIncludeInActive').is(':checked') ? "Y" : "N",
        SYS_EMP_CODE: Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.val(),
        EMP_CODE: Controls_UserProfile.WFD01210EMP_ID_USERPROFILE.val(),
        IS_RESPONSE_COST_CENTER: isResponseCostCenter,
        COMPANY: $('#COMPANY').val()
    }
    console.log('select in charge');
    console.log(con);

    //To Check exists in right tables
    var _Selected = [];
    $('#Selected_Incharge > tbody >tr').each(function () {
        _Selected.push($(this)[0].cells[0].innerHTML);
    });
    var content = '';
    var plant_cd = [];
    ajax_method.Post(URL_CONST.GET_IN_CHARGE, con, true, function (result) {
        if (result != null) {

            $.each(result, function (index, item) {

                var style = "";

                //if (item.STYLE == "N") {
                //    style = "font-style: italic;"
                //}
                if (_Selected.indexOf(item.COST_CODE) >= 0) {
                    console.log("Exists => " + item.COST_CODE);
                    return; //next loop
                }
               
                var trClass = "detail";
                
                if (con.IS_RESPONSE_COST_CENTER !== "Y" && item.FLAG_SV_COST_CENTER === "Y") {
                    //HAS ASSGIN COST CENTER TO USER
                    trClass += " not-allow";
                }

                if (toggleArrayItem(plant_cd, item.PLANT_CD)) {
                    content += '<tr class="header expand hide ' + item.PLANT_CD + '"><td>' + item.PLANT_CD + '<span class="sign"></span></td><td colspan="2">' + item.PLANT_NAME + '</td></tr>'
                        // + '<tr class="detail" "><td style="display: none;"></td><td style="' + style + '">' + item.COST_CODE + '</td><td  style="' + style + '">' + item.COST_NAME + ' </td><td style="display: none;">' + item.PLANT_CD + ' </td></tr>'
                        + '<tr class="' + trClass + '"><td style="display: none;"></td><td style="' + style + '"><span data-toggle="tooltip" title="' + (item.TOOLTIP_SV_COST_CENTER ? item.TOOLTIP_SV_COST_CENTER : '') + '">' + item.COST_CODE + '</span></td><td style="' + style + '">' + item.COST_NAME + '</td><td style="display: none;">' + item.PLANT_CD + '</td><td style="display: none;">' + item.FLAG_SV_COST_CENTER + '</td></tr>';
                    
                } else {
                    //content += '<tr class="detail" "><td  style="display: none;"></td><td  style="' + style + '">' + item.COST_CODE + '</td><td  style="' + style + '">' + item.COST_NAME + ' </td>' + '</td><td style="display: none;">' + item.PLANT_CD + ' </td></tr>'
                    content += '<tr class="' + trClass + '"><td style="display: none;"></td><td  style="' + style + '"><span data-toggle="tooltip" title="' + (item.TOOLTIP_SV_COST_CENTER ? item.TOOLTIP_SV_COST_CENTER : '') + '">' + item.COST_CODE + '</span></td><td style="' + style + '">' + item.COST_NAME + '</td></td><td style="display: none;">' + item.PLANT_CD + '</td><td style="display: none;">' + item.FLAG_SV_COST_CENTER + '</td></tr>';
                }
            });
            $('#Incharge_CostCode > tbody').html(content);
            $('.header').off('click').on('click', function () {
                $(this).toggleClass('expand').nextUntil('tr.header').slideToggle(100);
            });
            $("#Incharge_CostCode > tbody >tr.detail").off('click').on('click', function () {
                //debugger;
                var $tr = $(this);
                if (!$tr.hasClass('not-allow')){
                    $(this).toggleClass("table-row-select");
                }
            });
        }
    }, null, false, 'section-cost-map');

}

var GetInChargeTable = function () {
    var content = '';
    var con = {
        IN_CHARGE_COST_CENTER: $('#WFD01210COST_CENTER_USERPROFILE').val(),
        EMP_CODE: Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.val(),
        SYS_EMP_CODE: Controls_UserProfile.WFD01210SYS_EMP_ID_USERPROFILE.val(),
        IS_RESPONSE_COST_CENTER: isResponseCostCenter,
        COMPANY: $('#COMPANY').val()
    }
    $('#Selected_Incharge > tbody').html('');
    ajax_method.Post(URL_CONST.GET_IN_CHARGE_TABLE, con, true, function (result) {
        if (result != null) {
            $.each(result, function (index, item) {
                if (item.STATUS == "N") {
                    content += '<tr class="detail" style="background-color:#BBB;"><td><span data-toggle="tooltip" title="' + (item.TOOLTIP_SV_COST_CENTER ? item.TOOLTIP_SV_COST_CENTER : '') + '">' + item.COST_CODE + '</span></td><td>' + item.COST_NAME + ' </td>' + '</td><td style="display: none;">' + item.PLANT_CD + ' </td><td style="display: none;">' + item.FLAG_SV_COST_CENTER + ' </td></tr>'
                } else {
                    content += '<tr class="detail"><td><span data-toggle="tooltip" title="' + (item.TOOLTIP_SV_COST_CENTER ? item.TOOLTIP_SV_COST_CENTER : '') + '">' + item.COST_CODE + '</span></td><td>' + item.COST_NAME + ' </td>' + '</td><td style="display: none;">' + item.PLANT_CD + ' </td><td style="display: none;">' + item.FLAG_SV_COST_CENTER + ' </td></tr>'
                }


            });
        }
        $('#Selected_Incharge > tbody').html(content);

        //Add New By Surasith T. 2017-05-08
        $("#Selected_Incharge > tbody >tr.detail").off('click').on('click', function () {
            //debugger;
            $(this).toggleClass("table-row-select");
        });

    }, null);

}
//end
function toggleArrayItem(a, v) {
    var i = a.indexOf(v);
    if (i === -1) {
        a.push(v);
        return true;
    }

    else
        return false;
    // a.splice(i, 1);
}

var CheckSetUpInCharge = function () {

    var con = {
        RESPONSIBILITY: Controls_UserProfile.WFD01210RESPONSIBILITY_USERPROFILE.val()
    }
    ajax_method.Post(URL_CONST.CHECK_IN_CHARGE, con, true, function (result) {
        if (result == 'Y' && PARAMETER.IS_FAADMIN == 'Y' && PARAMETER.MODE != 'VIEW') {
            Controls_UserProfile.WFD01210Setup.removeAttr('disabled');
            //$('#WFD01210BtnResponse_Cost_Center').removeAttr('disabled');
            //$('#WFD01210BtnCost_Center').removeAttr('disabled');

        } else {
            Controls_UserProfile.WFD01210Setup.attr('disabled', 'disabled');
            //$('#WFD01210BtnResponse_Cost_Center').attr('disabled', 'disabled');
            //$('#WFD01210BtnCost_Center').attr('disabled', 'disabled');

        }

    }, null);

}

var WFD01210setEnableButton = function (id) {
    $(id).removeClass('disabled');
    $(id).prop('disabled', false);
}

var WFD01210setDisableButton = function (id) {
    $(id).addClass('disabled');
    $(id).removeClass('active');
    $(id).prop('disabled', true);
}

var getCostCenterData_ddl = function (controll_id, company) {

    //$(controll_id).val('');
   // $(controll_id).text('');
    $(controll_id).empty();

    var Param = {
        COMPANY: company
    }

    ajax_method.Post(URL_CONST.GET_COST_CENTER, Param, false, function (result) {
        if (result != null) {
           
            $(controll_id).append($('<option/>').attr("value", "").text('Select'));

            $.each(result, function (i, option) {

                $(controll_id).append($('<option/>').attr("value", option.COST_CODE).text(option.COST_CODE + ' : ' + option.COST_NAME));
            });
            $(controll_id).val('').trigger('change');
        }
    })
}

var setDisplayNone = function (checkBox, visable) {
    var checkBox = document.getElementById(checkBox);
    var visable = document.getElementById(visable);
    if (checkBox.checked == true) {
        visable.style.display = "block";
    } else {
        visable.style.display = "none";
    }
}


var setDisabled = function (checkBox, visable) {
    var checkBox = document.getElementById(checkBox);
    var visable = document.getElementById(visable);
    if (checkBox.checked == true) {
        visable.disabled = false;
    } else {
        visable.disabled = true;
    }
}

var getMultiControlValue = function (id) {
    var _val = '';
    if (id.val() != null) {
        if ($('option', id).length !== id.val().length) {
            _val = (id.val() + '');
            _val = _val.replace(/,/g, '#');
        }
    }
    return _val;
}
// Begin Autocomplete Cost Center
var TMPCostCenter;
$('#COST_CENTER').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        COST_CENTERS = [];
        $.post(URL_CONST.GET_AUTOCOMPLETE_COST_CENTER, { _keyword: query }, function (data) {
            TMPCostCenter = data.ObjectResult;
            for (i = 0; i < data.ObjectResult.length; i++) {
                var displayValue = data.ObjectResult[i].COST_CODE + " - " + data.ObjectResult[i].COST_NAME;
                TMPCostCenter[i].displayValue = displayValue;
                COST_CENTERS.push(displayValue);
            }
            process(COST_CENTERS);
        });
    },
    updater: function (item) {
        for (i = 0; i < TMPCostCenter.length; i++) {
            if (item === TMPCostCenter[i].displayValue) {
                return TMPCostCenter[i].COST_CODE;
            }
        }
        return item;
    }
});

//$('#COST_CENTER').change(function () {
//    var anchor = document.getElementById('COST_CENTER');
//    for (i = 0; i < TMPCostCenter.length; i++) {
//        if ($('#COST_CENTER').val() == TMPCostCenter[i].COST_CODE) {
//            anchor.title = TMPCostCenter[i].COST_NAME;
//        }
//    }
//});
// End Autocomplete Cost Center


// Begin Autocomplete Divistion
var TMPDivision;
$('#DIVISTION').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    //displayKey: 'company_name',
    source: function (query, process) {
        var RESOURCE = [];
        $.post(URL_CONST.GET_AUTOCOMPLETE_DIVISION, { _keyword: query }, function (data) {
            //debugger;
            TMPDivision = data.ObjectResult;
            for (i = 0; i < data.ObjectResult.length; i++) {
                RESOURCE.push(data.ObjectResult[i].DIV_NAME);
            }
            process(RESOURCE);
        });
    }
});

//$('#DIVISTION').change(function () {
//    //debugger;
//    var anchor = document.getElementById('DIVISTION');
//    for (i = 0; i < TMPDivision.length; i++) {
//        if ($('#DIVISTION').val() == TMPDivision[i].DIV_CODE) {
//            anchor.title = TMPDivision[i].DIV_NAME;
//        }
//    }
//});
// End Autocomplete Divistion

// Begin Autocomplete Sub Divistion
var TMPSubDivistion;
$('#SUB_DIVISTION').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        var RESOURCE = [];

        $.post(URL_CONST.GET_AUTOCOMPLETE_SUB_DIVISTION, { _keyword: query, _divKeyword: $('#DIVISTION').val() }, function (data) {
            TMPSubDivistion = data.ObjectResult
            for (i = 0; i < data.ObjectResult.length; i++) {
                RESOURCE.push(data.ObjectResult[i].SUB_DIV_NAME);
            }
            process(RESOURCE);
        });
    }
});

//$('#SUB_DIVISTION').change(function () {
//    var anchor = document.getElementById('SUB_DIVISTION');
//    for (i = 0; i < TMPSubDivistion.length; i++) {
//        if ($('#SUB_DIVISTION').val() == TMPSubDivistion[i].SUB_DIV_CODE) {
//            anchor.title = TMPSubDivistion[i].SUB_DIV_NAME;
//        }
//    }
//});
// End Autocomplete Sub Divistion

// Begin Autocomplete Department
var TMPDepartment;
$('#DEPARTMENT').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        var RESOURCE = [];
        $.post(URL_CONST.GET_AUTOCOMPLETE_DEPARTMENT, { _keyword: query, _subDivKeyword: $('#SUB_DIVISTION').val() }, function (data) {
            TMPDepartment = data.ObjectResult
            for (i = 0; i < data.ObjectResult.length; i++) {
                RESOURCE.push(data.ObjectResult[i].DEPT_NAME);
            }
            process(RESOURCE);
        });
    }
});

//$('#DEPARTMENT').change(function () {
//    var anchor = document.getElementById('DEPARTMENT');
//    for (i = 0; i < TMPDepartment.length; i++) {
//        if ($('#DEPARTMENT').val() == TMPDepartment[i].SUB_DIV_CODE) {
//            anchor.title = TMPDepartment[i].SUB_DIV_NAME;
//        }
//    }
//});
// End Autocomplete Department

// Begin Autocomplete Section
var TMPSection;
$('#SECTION').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        var RESOURCE = [];

        $.post(URL_CONST.GET_AUTOCOMPLETE_SECTION, { _keyword: query, _depKeyword: $('#DEPARTMENT').val() }, function (data) {
            TMPSection = data.ObjectResult
            for (i = 0; i < data.ObjectResult.length; i++) {
                RESOURCE.push(data.ObjectResult[i].SECTION_NAME);
            }
            process(RESOURCE);
        });
    }
});

//$('#SECTION').change(function () {
//    var anchor = document.getElementById('SECTION');
//    for (i = 0; i < TMPDepartment.length; i++) {
//        if ($('#SECTION').val() == TMPDepartment[i].SECTION_CODE) {
//            anchor.title = TMPDepartment[i].SECTION_NAME;
//        }
//    }
//});
// End Autocomplete Section

// Begin Autocomplete Line
var TMPLine;
$('#LINE').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        var RESOURCE = [];

        $.post(URL_CONST.GET_AUTOCOMPLETE_LINE, { _keyword: query, _secKeyword: $('#DEPARTMENT').val() }, function (data) {
            TMPLine = data.ObjectResult
            for (i = 0; i < data.ObjectResult.length; i++) {
                RESOURCE.push(data.ObjectResult[i].LINE_NAME);
            }
            process(RESOURCE);
        });
    }
});

//$('#LINE').change(function () {
//    var anchor = document.getElementById('LINE');
//    for (i = 0; i < TMPLine.length; i++) {
//        if ($('#LINE').val() == TMPLine[i].LINE_CODE) {
//            anchor.title = TMPLine[i].LINE_NAME;
//        }
//    }
//});
// Begin Autocomplete Line

// Begin Autocomplete Position
//var TMPPosition;
//$('#POSITION').typeahead({
//    hint: true,
//    highlight: true,
//    minLength: 1,
//    source: function (query, process) {
//        var RESOURCE = [];

//        $.post(URL_CONST.GET_AUTOCOMPLETE_POSITION, { _keyword: query }, function (data) {
//            TMPPosition = data.ObjectResult;
//            for (i = 0; i < data.ObjectResult.length; i++) {
//                var displayValue = data.ObjectResult[i].POST_CODE + ' - ' + data.ObjectResult[i].POST_NAME;
//                TMPPosition[i].displayValue = displayValue;
//                RESOURCE.push(displayValue);
//            }
//            process(RESOURCE);
//        });
//    },
//    updater: function (item) {
//        for (i = 0; i < TMPPosition.length; i++) {
//            if (item === TMPPosition[i].displayValue) {
//                return TMPPosition[i].POST_CODE;
//            }
//        }
//        return item;
//    }
//});

//$('#POSITION').change(function () {
//    var anchor = document.getElementById('POSITION');
//    for (i = 0; i < TMPPosition.length; i++) {
//        if ($('#POSITION').val() == TMPPosition[i].POST_CODE) {
//            anchor.title = TMPPosition[i].POST_NAME;
//        }
//    }
//});
// End Autocomplete Position

// Begin Autocomplete Job
//var TMPJob;
//$('#JOB').typeahead({
//    hint: true,
//    highlight: true,
//    minLength: 0,
//    source: function (query, process) {
//        var RESOURCE = [];

//        $.post(URL_CONST.GET_AUTOCOMPLETE_JOB, { _keyword: query }, function (data) {
//            TMPJob = data.ObjectResult;
//            for (i = 0; i < data.ObjectResult.length; i++) {
//                //RESOURCE.push(data.ObjectResult[i].JOB_CODE);
//                var displayValue = data.ObjectResult[i].JOB_CODE + ' - ' + data.ObjectResult[i].JOB_NAME;
//                TMPJob[i].displayValue = displayValue;
//                RESOURCE.push(displayValue);
//            }
//            process(RESOURCE);
//        });
//    },
//    updater: function (item) {
//        for (i = 0; i < TMPJob.length; i++) {
//            if (item === TMPJob[i].displayValue) {
//                return TMPJob[i].JOB_CODE;
//            }
//        }
//        return item;
//    }
//});

//$('#JOB').change(function () {
//    var anchor = document.getElementById('JOB');
//    for (i = 0; i < TMPJob.length; i++) {
//        if ($('#JOB').val() == TMPJob[i].JOB_CODE) {
//            anchor.title = TMPJob[i].JOB_NAME;
//        }
//    }
//});
// End Autocomplete Job