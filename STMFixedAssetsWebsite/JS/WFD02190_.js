﻿search2190Control = {
    Pagin: $('#WFD02190_SearchResultTable').Pagin(20),
    DataTable: null
    //Search2190Config.ItemPerPage
}

$(function () {
    $('#WFD02190Loading').hide();
    $('#WFD02190_SearchDataRow').hide();

    //setWFD02190ControlsRequiredField();
    if (Search2190Config.DefaultCostCode != null || Search2190Config.DefaultCostCode != '') {
        $('#WFD02190CostCenter').val(Search2190Config.DefaultCostCode);
    }
    $('#WFD02190_SearchAssets').on('hidden.bs.modal', function () {
        f2190SearchAction.ClearScreen();
    });
    $('#WFD02190_SearchAssets').on('shown.bs.modal', function () {
        //f2190SearchAction.ClearScreen();
        $('#WFD02190CostCenter').focus();
        $('#WFD02190chkOnlyParent').prop('checked', true);
    });

    $('.entersearch').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            $('#WFD02190Search').trigger('click');
            return false;
        }
    });   
    
});


var f2190SearchAction = {
    ClearDefault:function(){

        Search2190Config.RoleMode = '';
        Search2190Config.searchOption = '';
        Search2190Config.AllowMultiple = true;

        Search2190Config.Default = {
            Company: { Value: "", Enable: true },
            CostCode: { Value: "", Enable: true },
            ResponsibleCostCode: { Value: "", Enable: true }, // Surasith change to FS in the future
            INVEST_REASON: { Value: "", Enable: true },
            Parent: { Value: "", Enable: true },
            AssetClass: { Value: "", Enable: true },
            AssetGroup: { Value: "", Enable: true },
            WBSProject: { Value: "", Enable: true },
            WBSBudget: { Value: "", Enable: true }
        }; 
            
    },
    _SetDefault: function () {
        
        if (Search2190Config.RoleMode != 'FW' && 
            Search2190Config.RoleMode != 'FS' && 
            Search2190Config.RoleMode != 'ACR' ){
            alert("Please set Role Mode (WFD02190)");
            throw new Error('Please set Role Mode (WFD02190)');
        }
        // Control, Value, status
        $('#WFD02190MultipleBoxCompany').val(Search2190Config.Default.Company.Value);
        $('#WFD02190MultipleBoxCompany').prop("disabled", !Search2190Config.Default.Company.Enable);

        $('#WFD02190CostCenter').val(Search2190Config.Default.CostCode.Value);
        $('#WFD02190CostCenter').prop("disabled", !Search2190Config.Default.CostCode.Enable);

        $('#WFD02190ResponsibleCostCenter').val(Search2190Config.Default.ResponsibleCostCode.Value);
        $('#WFD02190ResponsibleCostCenter').prop("disabled", !Search2190Config.Default.ResponsibleCostCode.Enable);

        $('#WFD02190cmbBOI').select2("val", Search2190Config.Default.INVEST_REASON.Value);
        $('#WFD02190cmbBOI').prop("disabled", !Search2190Config.Default.INVEST_REASON.Enable);

        $('#WFD02190chkOnlyParent').prop('checked', Search2190Config.Default.Parent.Value == "Y");
        $("#WFD02190chkOnlyParent").prop('disabled', !Search2190Config.Default.Parent.Enable);

        $('#WFD02190txtAssetClass').val(Search2190Config.Default.AssetClass.Value);
        $('#WFD02190txtAssetClass').prop("disabled", !Search2190Config.Default.AssetClass.Enable);

        $('#WFD02190cmbAssetGroup').select2("val",Search2190Config.Default.AssetGroup.Value);
        $('#WFD02190cmbAssetGroup').prop("disabled", !Search2190Config.Default.AssetGroup.Enable);


        $('#WFD02190txtWBSProject').val(Search2190Config.Default.WBSProject.Value);
        $('#WFD02190txtWBSProject').prop("disabled", !Search2190Config.Default.WBSProject.Enable);

        $('#WFD02190txtWBSBudget').val(Search2190Config.Default.WBSBudget.Value);
        $('#WFD02190txtWBSBudget').prop("disabled", !Search2190Config.Default.WBSBudget.Enable);

    },
    Initialize: function () {
        
        f2190SearchAction.ClearScreen();

        f2190SearchAction._SetDefault();

        f2190SearchAction.LoadLocation();

        $('#lblWFD02190SearchTitle').html("Fixed asset search dialog");
        if (Search2190Config.searchOption == "CNS") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search source assets for reclassification");
        }
        if (Search2190Config.searchOption == "CND") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search target assets for reclassification");
        }
        if (Search2190Config.searchOption == "CN") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search assets for reclassification");
        }

        if (Search2190Config.searchOption == "GN") //New Assets No for reclassification
        {
            $('#lblWFD02190SearchTitle').html("Search assets for info change");
        }

        if (Search2190Config.searchOption == "KN") //New Asset No for Settlement
        {
            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
        }
        if (Search2190Config.searchOption == "PN") //New Asset No (select Parent)
        {
            //$("#select").select2("val", "CA");
            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
            //$("#WFD02190cmbAssetGroup").prop('disabled', true);

            //$('#WFD02190chkOnlyParent').prop('checked', true);
            //$("#WFD02190chkOnlyParent").prop('disabled', true);

            //$("#WFD02190txtAssetSub").prop('disabled', true);
        }
        if (Search2190Config.searchOption == "KA") //Settlement - AUC
        {
            //$("#WFD02190txtWBSBudget").val('C%');
            ////Remain Same as previous

            //$('#WFD02190cmbAssetGroup').select2("val", "AUC");
            //$("#WFD02190cmbAssetGroup").prop('disabled', true);
        }
        if (Search2190Config.searchOption == "KE") //Settlement - AUC
        {
            
            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
            //$("#WFD02190cmbAssetGroup").prop('disabled', true);

            //Remain Default Asset class
        }
        if (Search2190Config.searchOption == "T") //Transfer
        {

            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
            //$("#WFD02190cmbAssetGroup").prop('disabled', true);

           

        }
        if (Search2190Config.searchOption == "D") //Dispose
        {

            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
            //$("#WFD02190cmbAssetGroup").prop('disabled', true);

            //$('#WFD02190chkOnlyParent').prop('checked', true);

        }
        if (Search2190Config.searchOption == "M") //Dispose
        {
            //$('#WFD02190cmbAssetGroup').select2("val", "RMA");
            //$("#WFD02190cmbAssetGroup").prop('disabled', true);

            //$('#WFD02190chkOnlyParent').prop('checked', true);
            //$("#WFD02190chkOnlyParent").prop('disabled', true);
        }


    },
    _GetSearchParameter : function(){
        return {
            GUID : GLOBAL.GUID,
            REQUEST_TYPE : GLOBAL.REQUEST_TYPE,
            COMPANY: $("#WFD02190MultipleBoxCompany").val(),
            ASSET_NO: $("#WFD02190txtAssetNo").val(),
            ASSET_SUB: $("#WFD02190txtAssetSub").val(),
            ASSET_NAME: $("#WFD02190txtAssetName").val(),
            ASSET_CLASS: $("#WFD02190txtAssetClass").val(),
            ASSET_GROUP: $("#WFD02190cmbAssetGroup").val(),
            WBS_PROJECT: $("#WFD02190txtWBSProject").val(),
            WBS_BUDGET: $("#WFD02190txtWBSBudget").val(),
            ASSET_PLATE_NO: $("#WFD02190txtAssetPlateNo").val(),
            ONLY_PARENT_ASSETS: $("#WFD02190chkOnlyParent").is(':checked') ? "Y" : "N",
            COST_CODE: $("#WFD02190CostCenter").val(),
            LOCATION: $("#WFD02190ddpLocation").val(),
            RESP_COST_CODE: $("#WFD02190ResponsibleCostCenter").val(),
            INVEST_REASON: $("#WFD02190cmbBOI").val(),
            DATE_IN_SERVICE_START: $("#WFD02190CapitalizeDateFrom").val(),
            DATE_IN_SERVICE_TO: $("#WFD02190CapitalizeDateTo").val()
        };
    },
    LoadLocation: function (_default) {
        ajax_method.Post('/Common/Location_AutoComplete', { COMPANY: $('#WFD02190MultipleBoxCompany').val() }, true, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02190ddpLocation').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE));
            });
            //Set Default
            $('#WFD02190ddpLocation').select2('val', '');
        });
    },
    Search: function () {
        ClearMessageC();
        search2190Control.Pagin.ClearPaginData();

        var _data = f2190SearchAction._GetSearchParameter();
        var pagin = search2190Control.Pagin.GetPaginData();
        console.log("Search");
        console.log(_data);
        $('#WFD02190Loading').show();
        console.log(Search2190Config.SearchUrl);
        ajax_method.SearchPost(Search2190Config.SearchUrl, _data, pagin, true, function (datas, pagin) {
            
            if (datas == null || datas.length == 0) {
                //control.SearchDataRow.hide();
                search2190Control.Pagin.Clear();
                $('#WFD02190Loading, #WFD02190_SearchDataRow').hide();
                return ;
            }
            //WFD02190SetParmeter(datas);

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#WFD02190_SearchResultTable'))) {
                var table = $('#WFD02190_SearchResultTable').DataTable();
                table.destroy();
            }
            search2190Control.DataTable = $('#WFD02190_SearchResultTable').DataTable({
                data: datas,
                "columns": [
                    { "data": null },
                    { "data": "ASSET_NO", 'orderable': false },
                    { "data": "ASSET_SUB", 'className': 'text-center' },
                    { "data": "ASSET_NAME", 'className': 'text-center' },
                    { "data": null }, //Asset class
                    { "data": null }, //Cost Code 
                    { "data": null }, //WBS
                    { "data": null },
                    { "data": null },
                    { "data": "BOI_NO" },
                    { "data": "LOCATION" },
                    { "data": "DATE_IN_SERVICE" },
                    { "data": "STATUS" }
                ],
                'columnDefs': [{
                    'targets': 0,
                    'orderable': false,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.CheckBoxRender(data);
                    }
                },
                {
                    'targets': 4,
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.ASSET_CLASS, data.ASSET_CLASS_HINT);
                    }
                },
                {
                    'targets': 5,
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.COST_CODE, data.COST_NAME_HINT);
                    }
                },
                {
                    'targets': 6,
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.WBS_PROJECT, data.WBS_PROJECT_HINT);
                    }
                },
                {
                    'targets': 7,
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.INVEST_REASON, data.INVEST_REASON_HINT);
                    }
                },
                {
                    'targets': 8,
                    'render': function (data, type, full, meta) {
                        return fSearch2190Render.TooltipRender(data.MINOR_CATEGORY, data.MINOR_CATEGORY_HINT);
                    }
                },
                { 'targets': [4, 5, 6], className: 'text-center' }],
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false
            });

            search2190Control.Pagin.Init(pagin, WFD02190sorting, WFD02190changePage, 'itemPerPage');

            //control.SearchDataRow.show();
            $('#WFD02190_SearchDataRow').show();
            $('#WFD02190_selectAll').prop('checked', false);
            $('#WFD02190Loading').hide();
        }, null);

        
    },
    ClearScreen: function () {
        // Textbox
<<<<<<< .mine
        $('#WFD02190_SearchAssets .entersearch:enabled').val('');
        $('#WFD02190_SearchAssets .select2:enabled').select2('val','');

        $('#WFD02190chkOnlyParent, #WFD02190_selectAll').prop('checked', false);
||||||| .r346
        $('#WFD02190_SearchAssets .entersearch').val('');
        $('#WFD02190_SearchAssets .select2').val('');

        $('#WFD02190chkOnlyParent, #WFD02190_selectAll').prop('checked', false);
=======
        //$('#WFD02190_SearchAssets .entersearch').val('');
        //$('#WFD02190_SearchAssets .select2').val('');       
        //$('#WFD02190chkOnlyParent, #WFD02190_selectAll').prop('checked', _CheckPartParent);
>>>>>>> .r361
       
        $('#AlertMessageWFD02190').html('');
        
        if ($.fn.DataTable.isDataTable($('#WFD02190_SearchResultTable'))) {
            var table = $('#WFD02190_SearchResultTable').DataTable();
            table.destroy();
        }
        $('#WFD02190_SearchDataRow').hide();
      
        
        $('#AlertMessageWFD02190').html('');

        $('#WFD02190txtAssetNo').val('');
        $('#WFD02190txtAssetSub').val('');
        $('#WFD02190chkOnlyParent').prop('checked', Search2190Config.Default.Parent.Value == "Y");
        $("#WFD02190chkOnlyParent").prop('disabled', !Search2190Config.Default.Parent.Enable);
        $('#WFD02190txtAssetName').val('');
        $('#WFD02190txtAssetClass').val('');
        //$('#WFD02190cmbAssetGroup').val('');
        $('#WFD02190CostCenter').val('');
        $('#WFD02190ResponsibleCostCenter').val('');
        $('#WFD02190txtWBSProject').val('');
        $('#WFD02190txtWBSBudget').val('');
        $('#WFD02190txtAssetPlateNo').val('');
        $('#WFD02190CapitalizeDateFrom').val('');
        $('#WFD02190CapitalizeDateTo').val('');

        $('#WFD02190cmbAssetGroup').select2("val", '');
        $('#WFD02190ddpLocation').select2("val", '');
        $('#WFD02190cmbBOI').select2("val", '');
    },
    CloseScreen: function () {
        f2190SearchAction.ClearScreen();
        $('#WFD02190_SearchAssets').modal('hide');
    },
    SelectAll: function (_chk) {
        $('#WFD02190_SearchResultTable').find('[name="sel"]').each(function (e) {
            $(this).prop('checked', _chk);
        });
    },
    _GetClientAssetList: function () {
        console.log("GetClientAssetList");
        if (search2190Control.DataTable == null) {
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List

        $("#WFD02190_SearchResultTable tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            //Check checkbox is checked.
            var _chk = $(this).find("input[name='sel']").is(':checked');
            if (!_chk) //no select
                return;
            var _row = [];

            _row = search2190Control.DataTable.row(this).data(); // Get source data

            _assetList.push({
                COMPANY: _row.COMPANY,
                ASSET_NO: _row.ASSET_NO,
                ASSET_NAME:_row.ASSET_NAME,
                ASSET_SUB: _row.ASSET_SUB,
                ASSET_CLASS : _row.ASSET_CLASS,
                ASSET_CLASS_HINT : _row.ASSET_CLASS_HINT,
                MINOR_CATEGORY : _row.MINOR_CATEGORY
            });
        });

        return _assetList;
    },

    Save: function () {
        //Load Selected Parameter and Check and Send to Main screen
        var _asset = f2190SearchAction._GetClientAssetList();
        

        try
        {
            if (_asset.length == 0) {
                $('#AlertMessageWFD02190').html(Search2190Message.NoSelected);
                return;
            }
            if (!Search2190Config.AllowMultiple && _asset.length > 1) {
                $('#AlertMessageWFD02190').html(Search2190Message.SelectOneRecord);
                return;
            }
            //Recheck ?
            if (Search2190Config.customCallback != null) {

                Search2190Config.customCallback(_asset, function () {
                    $('#WFD02190_SearchAssets').modal('hide');
                });
                return;
            }

            fAction.AddAsset(_asset, function () {
                $('#WFD02190_SearchAssets').modal('hide');
            });
        }
        catch(e)
        {
            console.error(e);
        }
    },
    DetailCheck: function () {
        var numberOfChecked = $('#WFD02190_SearchResultTable [name="sel"]:checked').length;
        var totalCheckboxes = $('#WFD02190_SearchResultTable [name="sel"]').length;
        $('#WFD02190_SearchResultTable #WFD02190_selectAll').prop('checked', totalCheckboxes > 0 && totalCheckboxes == numberOfChecked);

    }

}
var fSearch2190Render = {
    CheckBoxRender: function (data) {
        var _strHtml = '';
        var _strDisable = '';
        var _strChecked = '';
        _strHtml = '<input name="sel" style="width:20px; height:28px;" type="checkbox"' + _strDisable + ' ' + _strChecked + ' >';
        return _strHtml;
    },
    TooltipRender: function (display, toolTipText) {
        var _strHtml = '';
        if (toolTipText == null || toolTipText.length == 0) {
            _strHtml = display;
            return _strHtml;
        }
        _strHtml = '<p title="" data-original-title="' + replaceAll(toolTipText, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + display + '..' + '</p>'
        return _strHtml;
    }

}

//Event Control
$('#WFD02190Search').click(function (e) {

    ClearMessageC();
    search2190Control.Pagin.ClearPaginData();
    
    f2190SearchAction.Search();
}),
$('#WFD02190Clear').click(function (e) {
    f2190SearchAction.ClearScreen();
})
$('#WFD02190Close').click(function (e) {
    f2190SearchAction.CloseScreen();
})
$('#WFD02190OK').click(function (e) {
    f2190SearchAction.Save();
})
$('#WFD02190Cancel').click(function (e) {
    f2190SearchAction.CloseScreen();
})
$('#WFD02190_selectAll').change(function (e) {
   // alert("XXXX");
    var chk = $(this).is(':checked');
    f2190SearchAction.SelectAll(chk);
})



var WFD02190sorting = function (PaginData) {    
    f2190SearchAction.Search();
}

// Callback function after change pagination on table
var WFD02190changePage = function (PaginData) {
    f2190SearchAction.Search();
}


var getCompanyFromAnyPage = function (value) {
    $('#WFD02190MultipleBoxCompany').val(value);
}


