﻿var Controls = {
    WFD02630DDL_COMPANY: $('#COMPANY'),
    WFD02630DDL_YEAR: $('#YEAR'),
    WFD02630DDL_ROUND: $('#ROUND'),
    WFD02630DDL_ASSET_LOCATION: $('#ASSET_LOCATION'),
    WFD02630Clear: $('#WFD02630Clear'),
    WFD02630Search: $('#WFD02630Search'),
    WFD02630SearchDataRow: $('#SearchDataRow'),
    WFD02630DATE_AS_OF: $('#WFD02630DATE_AS_OF'),
    WFD02630ACTUAL: $('#WFD02630ACTUAL'),
    WFD02630TOTAL: $('#WFD02630TOTAL'),
    WFD02630PROGRESS_BAR_PERCEN: $('#WFD02630PROGRESS_BAR_PERCEN'),

    WFD02630BG: $('#WFD02630BG'),
    WFD02630ICON: $('#WFD02630ICON'),
    WFD02630IconImg: $('#WFD02630IconImg'),
    WFD02630Description: $('#WFD02630Description'),
    WFD02630PERSEN: $('#WFD02630PERSEN'),
    WFD02630TEXTPERSEN: $('#WFD02630TEXTPERSEN') 
};

var WFD02630_Holiday = {
    DayOfHoliday: []
};


var requiredMainControl_WFD02630 = [Controls.WFD02630DDL_COMPANY,Controls.WFD02630DDL_YEAR, Controls.WFD02630DDL_ROUND, Controls.WFD02630DDL_ASSET_LOCATION];
$(function () {
    $('.select2').select2({
        width: '120px'
    });
    Controls.WFD02630SearchDataRow.hide();
    setControlsRequiredField(requiredMainControl_WFD02630, true);
    GetDdlCompany();
    Controls.WFD02630DDL_COMPANY.change(function () {
        GetDdlYear();

        setControlsRequiredField(requiredMainControl_WFD02630, true);
    });
    Controls.WFD02630DDL_YEAR.change(function () {
        GetDdlRound();
        //GetDdlAssetLocation();
        setControlsRequiredField(requiredMainControl_WFD02630, true);
    });
    Controls.WFD02630DDL_ROUND.change(function () {
        GetDdlAssetLocation();
        setControlsRequiredField(requiredMainControl_WFD02630, true);
    });
    Controls.WFD02630Search.click(WFD2630SearchFN);
    Controls.WFD02630Clear.click(Clear);
  
    GetDefault();
});

var Clear = function () {
    WFD02630_Holiday.DayOfHoliday = [];
    $('.select2').select2({
        width: '120px'
    });
    Controls.WFD02630SearchDataRow.hide();
    GetDdlCompany();
    GetDdlYear();
    GetDefault();
}
//var Clear = function () {
//    Controls.WFD02630SearchDataRow.hide();
//    Controls.WFD02630DDL_YEAR.select2({
//        width: '120px'
//    }).select2("val", '');
//}

var GetDdlCompany = function () {

    ajax_method.Post(URL_CONST.GET_COMPANY_DDL, '', false, function (result) {
        if (result != null) {
            Controls.WFD02630DDL_COMPANY.html('');
            var optionhtml1 = '<option value="">' + "- Select -" + '</option>';
            Controls.WFD02630DDL_COMPANY.append(optionhtml1);

            $.each(result.data, function (i) {
                var optionhtml = '<option value="' + result.data[i].COMPANY + '">' + result.data[i].COMPANY + '</option>';

                Controls.WFD02630DDL_COMPANY.append(optionhtml);
            });
        }

    }, null);
};


var GetDdlYear = function () {
    var con = {
        company: Controls.WFD02630DDL_COMPANY.val()
       
    };

    Controls.WFD02630DDL_YEAR.select2({
        width: '120px'
    }).select2("val", '');
    Controls.WFD02630DDL_YEAR.html('');
    var optionhtml1 = '<option value="">' + "- Select -" + '</option>';

    ajax_method.Post(URL_CONST.GET_YEAR_DDL, con, false, function (result) {
        if (result != null) {
            //Controls.WFD02630DDL_YEAR.html('');
            //var optionhtml1 = '<option value="">' + "- Select -" + '</option>';

            Controls.WFD02630DDL_YEAR.append(optionhtml1);

            $.each(result.data, function (i) {
                var optionhtml = '<option value="' + result.data[i].YEAR + '">' + result.data[i].YEAR + '</option>';
                Controls.WFD02630DDL_YEAR.append(optionhtml);
                Controls.WFD02630DDL_YEAR.select2("val", result.data[i].YEAR);
            });
        } else {
            Controls.WFD02630DDL_YEAR.append(optionhtml1);
        }

    }, null);
};
var GetDdlRound = function () {
    var con = {
        company: Controls.WFD02630DDL_COMPANY.val(),
        year: Controls.WFD02630DDL_YEAR.val()
    };
    Controls.WFD02630DDL_ROUND.select2({
        width: '120px'
    }).select2("val", '');
    Controls.WFD02630DDL_ROUND.html('');
    var optionhtml1 = '<option value="">' + "- Select -" + '</option>';
    ajax_method.Post(URL_CONST.GET_ROUND_DDL, con, false, function (result) {
        if (result != null) {


            Controls.WFD02630DDL_ROUND.append(optionhtml1);

            $.each(result.data, function (i) {
                var optionhtml = '<option value="' + result.data[i].ROUND + '">' + result.data[i].ROUND + '</option>';
                Controls.WFD02630DDL_ROUND.append(optionhtml);
                Controls.WFD02630DDL_ROUND.select2("val", result.data[i].ROUND);
            });

        } else {
            Controls.WFD02630DDL_ROUND.append(optionhtml1);
        }

    }, null);
};
var GetDdlAssetLocation = function () {
    var con = {
        company: Controls.WFD02630DDL_COMPANY.val(),
        year: Controls.WFD02630DDL_YEAR.val(),
        round: Controls.WFD02630DDL_ROUND.val()
    };

    Controls.WFD02630DDL_ASSET_LOCATION.select2({
        width: '120px'
    }).select2("val", '');
    Controls.WFD02630DDL_ASSET_LOCATION.html('');

    var optionhtml1 = '<option value="">' + "- Select -" + '</option>';

    ajax_method.Post(URL_CONST.GET_ASSET_LOCATION_DDL, con, false, function (result) {
        if (result != null) {


            Controls.WFD02630DDL_ASSET_LOCATION.append(optionhtml1);

            $.each(result.data, function (i) {
                var optionhtml = '<option value="' + result.data[i].CODE + '">' + result.data[i].ASSET_LOCATION + '</option>';

                Controls.WFD02630DDL_ASSET_LOCATION.append(optionhtml);
                Controls.WFD02630DDL_ASSET_LOCATION.select2("val", result.data[i].CODE);
            });

        } else {
            Controls.WFD02630DDL_ASSET_LOCATION.append(optionhtml1);

            Controls.WFD02630DDL_ASSET_LOCATION.select2({
                width: '120px'
            }).select2("val", '');
        }

    }, null);


};



function RemoveAllCLASS_NAME() {
    Controls.WFD02630PROGRESS_BAR_PERCEN.removeClass("progress-bar-green");
    Controls.WFD02630PROGRESS_BAR_PERCEN.removeClass("progress-bar-yellow");
    Controls.WFD02630PROGRESS_BAR_PERCEN.removeClass("progress-bar-aqua");
    Controls.WFD02630PROGRESS_BAR_PERCEN.removeClass("progress-bar-red");
}
function RemoveAllICON() {
    Controls.WFD02630ICON.removeClass("fa-circle-thin");
    Controls.WFD02630ICON.removeClass("fa-caret-up");
    Controls.WFD02630ICON.removeClass("fa-close");
    }
function RemoveAllBGClass() {
    Controls.WFD02630BG.removeClass("bg-green");
    Controls.WFD02630BG.removeClass("bg-yellow");
    Controls.WFD02630BG.removeClass("bg-aqua");
    Controls.WFD02630BG.removeClass("bg-red");
    Controls.WFD02630BG.removeClass("bg-green");
           
}

var WFD2630SearchFN = function () {
    WFD02630_Holiday.DayOfHoliday = [];
    var con = {
        COMPANY: Controls.WFD02630DDL_COMPANY.val(),
        YEAR: Controls.WFD02630DDL_YEAR.val(),
        ROUND: Controls.WFD02630DDL_ROUND.val(),
        ASSET_LOCATION: Controls.WFD02630DDL_ASSET_LOCATION.val()
    };
    ajax_method.Post(URL_CONST.GET_HOLIDAY, con, true, function (result) {
        if (result.data != null) {
            $.each(result.data, function (i, v) {
                WFD02630_Holiday.DayOfHoliday.push(v);
            });

            var con = {
                COMPANY: Controls.WFD02630DDL_COMPANY.val(),
                YEAR: Controls.WFD02630DDL_YEAR.val(),
                ROUND: Controls.WFD02630DDL_ROUND.val(),
                ASSET_LOCATION: Controls.WFD02630DDL_ASSET_LOCATION.val()
            };
            ajax_method.Post(URL_CONST.GET_DATADETAIL, con, true, function (result) {
                if (result != null) {
                    if (result.DateAsOf != null && result.OverAll != null && result.Calendar != null) {

                        RemoveAllICON();
                        RemoveAllBGClass();
                        RemoveAllCLASS_NAME();

                        Controls.WFD02630SearchDataRow.show();
                        //gen Dateasof
                        Controls.WFD02630DATE_AS_OF.text(result.DateAsOf.DATE_AS_OF + " : Actual / Plan");
                        Controls.WFD02630ACTUAL.text(result.DateAsOf.ACTUAL);
                        Controls.WFD02630TOTAL.text(result.DateAsOf.TOTAL);


                        Controls.WFD02630PROGRESS_BAR_PERCEN.addClass(result.DateAsOf.CLASS_NAME);
                        Controls.WFD02630PROGRESS_BAR_PERCEN.css("width", result.DateAsOf.PROGRESS_BAR_PERCEN);

                        //gen Overall
                        Controls.WFD02630Description.text(result.OverAll.ACTUAL + "/" + result.OverAll.TOTAL);
                        Controls.WFD02630TEXTPERSEN.text(result.OverAll.PROGRESS_BAR_PERCEN);
                        Controls.WFD02630PERSEN.css("width", result.OverAll.PROGRESS_BAR_PERCEN);




                        Controls.WFD02630BG.addClass(result.OverAll.CLASS_NAME);

                        $('#WFD02630BorderIcon').removeClass('border-icon');
                        if (result.OverAll.IMAGE_TYPE != '') {
                            $('#WFD02630BorderIcon').addClass('border-icon');
                        }
                        //Controls.WFD02630ICON.addClass(result.OverAll.IMAGE_TYPE);
                        Controls.WFD02630IconImg.attr('src', result.OverAll.IMGSRC);

                        $('#calendar').fullCalendar('destroy');
                        $('#calendar').fullCalendar({
                            header: {
                                left: 'prev,next',
                                center: 'title',
                                right: null
                            },
                            defaultDate: result.TargetDate.TARGET_DATE_FROM,
                            defaultView: 'month',
                            events:
                                result.Calendar
                            //  [
                            //{
                            //    title: 'Sila L.',
                            //    description: '100%(100/100)',
                            //    start: '2017/02/17',
                            //    end: null,
                            //    borderColor: "#000000",
                            //    url: 'http://google.com/',
                            //    className: "text-des-calendar-complete",

                            //},
                            // {
                            //     title: 'Sila L.',
                            //     description: '100%(100/100)',
                            //     start: '2017/02/01',
                            //     end: '2017/02/19',
                            //     borderColor: "#000000",
                            //     url: 'http://google.com/',
                            //     className: "text-des-calendar-onprocess"
                            // }
                            //  ]
                            ,
                            eventRender: function (event, element) {
                                console.log(event);
                                var clsSubmitAll = "text-title-calendar";
                                if (event.IS_SEND_REQUEST == "Y") {
                                    clsSubmitAll = "bgblack";
                                }

                                var content = '<div class="text-center ' + clsSubmitAll + '" style="border-bottom: 0.5px solid black;">' + event.title + '</div><div class="text-center ' + event.className + '">' + event.description + '</div>';
                                element.find('.fc-content').html(content);
                            },
                            dayRender: function (date, cell) {
                                Holiday(convertDate(date._d), cell);
                            },
                            editable: false,
                            droppable: false, // this allows things to be dropped onto the calendar !!!

                        });
                    }
                    else {
                        Controls.WFD02630SearchDataRow.hide();
                    }
                }

            });
        }
    });

};


function Holiday(dates, cell) {

    if ( WFD02630_Holiday.DayOfHoliday.length > 0)
    { // Get new data
        for (var i = 0; i < WFD02630_Holiday.DayOfHoliday.length; i++) {

            if (convertDate(parseJsonDate(WFD02630_Holiday.DayOfHoliday[i].HOLIDATE_DATE)) == dates) {
                cell.css("background-color", "#D9D9D9");
            }
        }
    }
    // Set backgroud color
}



function parseJsonDate(jsonDateString) {
    return new Date(parseInt(jsonDateString.replace('/Date(', '')));
}
function convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}
var setControlsRequiredField = function (controls, isRequired) {
    for (var i = 0; i < controls.length; i++) {
        if (isRequired == true) {
            if (controls[i].hasClass('select2')) {
                controls[i].Select2Require(true);
            } else {
                controls[i].addClass('require');
            }
        } else {
            if (controls[i].hasClass('select2')) {
                controls[i].Select2Require(false);
            } else {
                controls[i].removeClass("require");
            }
        }
    }
};
var GetDefault = function () {

    ajax_method.Post(URL_CONST.GET_DEFAULT, '', true, function (result) {
        if (result != null) {
            //ValueDefault = {
            //    YEAR: result.data[0].YEAR,
            //    ROUND: result.data[0].ROUND,
            //    ASSET_LOCATION: result.data[0].ASSET_LOCATION
            //};
            Controls.WFD02630DDL_COMPANY.val(result.data[0].COMPANY).change();
            Controls.WFD02630DDL_YEAR.val(result.data[0].YEAR).change();
            Controls.WFD02630DDL_ROUND.val(result.data[0].ROUND).change();
            Controls.WFD02630DDL_ASSET_LOCATION.val(result.data[0].ASSET_LOCATION).change();
            //WFD2630SearchFN();
        }

    }, null);


}