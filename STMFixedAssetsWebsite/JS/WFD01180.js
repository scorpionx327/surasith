﻿$(function () {
    //Initialize
});

var Ctrl01180 = {
    SearchTable: $('#tbSearchResult'),
    SearchPanel: $('#WFD01180_SearchResult'),
    Pagin: $('#tbSearchResult').Pagin()
};

var control = {
    DataTable: null
}

var company;
var pagin;
var Select_COMPANY = '';
var Select_ASSET_CLASS = '';
var Select_MINOR_CATEGORY = '';
var TmpData;
var Select;
var rownNo = 0;
var myObj = [];
var DataCHKAssetClass;
var MSG_AREA_ADD = "AlertMessageWFD01180_add";
var MSG_AREA_EDIT = "AlertMessageWFD01180_edit";

$(document).ready(function () {
    $('#WFD01180Search').width(100);
    $('#WFD01180Clear').width(100);
    $('#WFD01180Add').width(100);
    $('#WFD01180Edit').width(100);
    $('#WFD01180Delete').width(100);

    $('#WFD01180_EDIT_Company').prop('disabled', true);
    $('#WFD01180_EDIT_ASSET_CLASS').prop('disabled', true);
    $('#WFD01180_EDIT_MINOR_CATEGORY').prop('disabled', true);
    $('#WFD01180MultipleBoxCompany').multiselect(multiSelectOptionWidth100);

    LoadComboboxOnSearchScreen($('#WFD01180_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: getCompanyValue() }));
    LoadComboboxOnSearchScreen($('#WFD01180_ADD_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: ($('#WFD01180_ADD_Company').val()) }));

    pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
    LoadCHKAssetClass();
    $('#WFD01180_SearchResult').hide();
    ////////////////////////////////////////////////////////////////

    //$('#WFD01180_ADD_ASSET_CLASS').change(function () {
    //    alert($('#WFD01180_ADD_ASSET_CLASS').val());
    //});

    $('#WFD01180_ADD_Local_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Local_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Local_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Local_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_ADD_Local_Life_Year').val('');
        //}
    });

    $('#WFD01180_ADD_Tax_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Tax_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Tax_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Tax_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_ADD_Tax_Life_Year').val('');
        //}
    });

    $('#WFD01180_ADD_Consolidate_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Consolidate_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Consolidate_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Consolidate_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_ADD_Consolidate_Life_Year').val('');
        //}
    });

    $('#WFD01180_ADD_Management_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Management_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Management_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Management_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_ADD_Management_Life_Year').val('');
        //}
    });

    $('#WFD01180_ADD_Leasing_Local_N_A_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Leasing_Local_N_A_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Leasing_Local_N_A_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Leasing_Local_N_A_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_ADD_Leasing_Local_N_A_Year').val('');
        //}
    });

    $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val('');
        //}
    });
    //////////////////////////////////////////////////////////////////

    $('#WFD01180_ADD_Local_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Local_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Local_Life_Month').val();
        //if (_month < 0)
        //{
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Local_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Local_Life_Month').val('');
        //}
    });

    $('#WFD01180_ADD_Tax_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Tax_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Tax_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Tax_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Tax_Life_Month').val('');
        //}
    });

    $('#WFD01180_ADD_Consolidate_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Consolidate_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Consolidate_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Consolidate_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Consolidate_Life_Month').val('');
        //}
    });

    $('#WFD01180_ADD_Management_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Management_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Management_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Management_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Management_Life_Month').val('');
        //}
    });

    $('#WFD01180_ADD_Leasing_Local_N_A_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Leasing_Local_N_A_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Leasing_Local_N_A_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Leasing_Local_N_A_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Leasing_Local_N_A_Month').val('');
        //}
    });

    $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val('');
        //}
    });

    $('#WFD01180_ADD_Local_Life_Year').prop('disabled', true);
    $('#WFD01180_ADD_Tax_Life_Year').prop('disabled', true);
    $('#WFD01180_ADD_Consolidate_Life_Year').prop('disabled', true);
    $('#WFD01180_ADD_Management_Life_Year').prop('disabled', true);
    $('#WFD01180_ADD_Leasing_Local_N_A_Year').prop('disabled', true);
    $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').prop('disabled', true);

    $('#WFD01180_ADD_Local_Life_Month').prop('disabled', true);
    $('#WFD01180_ADD_Tax_Life_Month').prop('disabled', true);
    $('#WFD01180_ADD_Consolidate_Life_Month').prop('disabled', true);
    $('#WFD01180_ADD_Management_Life_Month').prop('disabled', true);
    $('#WFD01180_ADD_Leasing_Local_N_A_Month').prop('disabled', true);
    $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').prop('disabled', true);

    $('#WFD01180Edit').prop('disabled', true);
    $('#WFD01180Delete').prop('disabled', true);
    //////////////////////////////////////////////////////////////////////////////////////

    var ADD_Life_Loacl_Book = null;
    $('#WFD01180_ADD_Local_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Local_Life_Year').val('');
            $('#WFD01180_ADD_Local_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Local_Life_Month').val('');
            $('#WFD01180_ADD_Local_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Local_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Local_Life_Year').prop('disabled', false);
            $('#WFD01180_ADD_Local_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_ADD_Local_Life_Year').prop('disabled', true);
            $('#WFD01180_ADD_Local_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Local_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Local_Life_Year').val('');
            $('#WFD01180_ADD_Local_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Local_Life_Month').val('');
            $('#WFD01180_ADD_Local_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_ADD_Tax_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Tax_Life_Year').val('');
            $('#WFD01180_ADD_Tax_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Tax_Life_Month').val('');
            $('#WFD01180_ADD_Tax_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Tax_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Tax_Life_Year').prop('disabled', false);
            $('#WFD01180_ADD_Tax_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_ADD_Tax_Life_Year').prop('disabled', true);
            $('#WFD01180_ADD_Tax_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Tax_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Tax_Life_Year').val('');
            $('#WFD01180_ADD_Tax_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Tax_Life_Month').val('');
            $('#WFD01180_ADD_Tax_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_ADD_Consolidate_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Consolidate_Life_Year').val('');
            $('#WFD01180_ADD_Consolidate_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Consolidate_Life_Month').val('');
            $('#WFD01180_ADD_Consolidate_Life_month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Consolidate_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Consolidate_Life_Year').prop('disabled', false);
            $('#WFD01180_ADD_Consolidate_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_ADD_Consolidate_Life_Year').prop('disabled', true);
            $('#WFD01180_ADD_Consolidate_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Consolidate_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Consolidate_Life_Year').val('');
            $('#WFD01180_ADD_Consolidate_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Consolidate_Life_Month').val('');
            $('#WFD01180_ADD_Consolidate_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_ADD_Management_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Management_Life_Year').val('');
            $('#WFD01180_ADD_Management_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Management_Life_Month').val('');
            $('#WFD01180_ADD_Management_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Management_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Management_Life_Year').prop('disabled', false);
            $('#WFD01180_ADD_Management_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_ADD_Management_Life_Year').prop('disabled', true);
            $('#WFD01180_ADD_Management_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Management_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Management_Life_Year').val('');
            $('#WFD01180_ADD_Management_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Management_Life_Month').val('');
            $('#WFD01180_ADD_Management_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_ADD_Leasing_Local_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').val('');
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').prop('disabled', true);

            $('#WFD01180_ADD_Leasing_Local_N_A_Month').val('');
            $('#WFD01180_ADD_Leasing_Local_N_A_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Leasing_Local_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').prop('disabled', false);
            $('#WFD01180_ADD_Leasing_Local_N_A_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').prop('disabled', true);
            $('#WFD01180_ADD_Leasing_Local_N_A_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Leasing_Local_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').val('');
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').prop('disabled', true);

            $('#WFD01180_ADD_Leasing_Local_N_A_Month').val('');
            $('#WFD01180_ADD_Leasing_Local_N_A_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_ADD_Leasing_Consolidation_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val('');
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val('');
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Leasing_Consolidation_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').prop('disabled', false);
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').prop('disabled', true);
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_ADD_Leasing_Consolidation_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val('');
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').prop('disabled', true);

            $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val('');
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Local_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Local_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
            //var _month = $('#WFD01180_EDIT_Local_Life_Year').val();
            //if (_month < 0) {
            //    alert('Month must between 0 - 11');
            //    $('#WFD01180_EDIT_Local_Life_Year').val('');
            //}
            //if (_month > 9999) {
            //    alert('_month > 9999');
            //    $('#WFD01180_EDIT_Local_Life_Year').val('');
            //}
    });

    $('#WFD01180_EDIT_Tax_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Tax_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Tax_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Tax_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_EDIT_Tax_Life_Year').val('');
        //}
    });

    $('#WFD01180_EDIT_Consolidate_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Consolidate_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Consolidate_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Consolidate_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_EDIT_Consolidate_Life_Year').val('');
        //}
    });

    $('#WFD01180_EDIT_Management_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Management_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Management_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Management_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_EDIT_Management_Life_Year').val('');
        //}
    });

    $('#WFD01180_EDIT_Leasing_Local_N_A_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Leasing_Local_N_A_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val('');
        //}
    });

    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 9999) {
                alert('_month > 9999');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val('');
        //}
        //if (_month > 9999) {
        //    alert('_month > 9999');
        //    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val('');
        //}
    });
    /////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Local_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Local_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }

        //var _month = $('#WFD01180_EDIT_Local_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Local_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Local_Life_Month').val('');
        //}
    });

    $('#WFD01180_EDIT_Tax_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Tax_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Tax_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Tax_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Tax_Life_Month').val('');
        //}
    });

    $('#WFD01180_EDIT_Consolidate_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Consolidate_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Consolidate_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Consolidate_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Consolidate_Life_Month').val('');
        //}
    });

    $('#WFD01180_EDIT_Management_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Management_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Management_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Management_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Management_Life_Month').val('');
        //}
    });

    $('#WFD01180_EDIT_Leasing_Local_N_A_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Leasing_Local_N_A_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val('');
        //}
    });

    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').keypress(function (e) {
        return isNumberKey(e);
    });

    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').blur(function () {
        if (!$.isNumeric(this.value)) {
            this.value = '';
        } else {
            var _month = this.value;
            if (_month < 0) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
            if (_month > 11) {
                alert('Month must between 0 - 11');
                this.value = '';
            }
        }
        //var _month = $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val();
        //if (_month < 0) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val('');
        //}
        //if (_month > 11) {
        //    alert('Month must between 0 - 11');
        //    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val('');
        //}
    });

    $('.scrap-value').keypress(function (e) {
        return isDecimalKey(e);
    });

    $('#WFD01180_EDIT_Leasing_Local_N_A_Year').prop('disabled', true);
    $('#WFD01180_EDIT_Local_Life_Year').prop('disabled', true);
    $('#WFD01180_EDIT_Tax_Life_Year').prop('disabled', true);
    $('#WFD01180_EDIT_Consolidate_Life_Year').prop('disabled', true);
    $('#WFD01180_EDIT_Management_Life_Year').prop('disabled', true);
    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').prop('disabled', true);

    $('#WFD01180_EDIT_Leasing_Local_N_A_Month').prop('disabled', true);
    $('#WFD01180_EDIT_Local_Life_Month').prop('disabled', true);
    $('#WFD01180_EDIT_Tax_Life_Month').prop('disabled', true);
    $('#WFD01180_EDIT_Consolidate_Life_Month').prop('disabled', true);
    $('#WFD01180_EDIT_Management_Life_Month').prop('disabled', true);
    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', true);
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Local_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Local_Life_Year').val('');
            $('#WFD01180_EDIT_Local_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Local_Life_Month').val('');
            $('#WFD01180_EDIT_Local_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Local_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Local_Life_Year').prop('disabled', false);
            $('#WFD01180_EDIT_Local_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_EDIT_Local_Life_Year').prop('disabled', true);
            $('#WFD01180_EDIT_Local_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Local_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Local_Life_Year').val('');
            $('#WFD01180_EDIT_Local_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Local_Life_Month').val('');
            $('#WFD01180_EDIT_Local_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Tax_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Tax_Life_Year').val('');
            $('#WFD01180_EDIT_Tax_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Tax_Life_Month').val('');
            $('#WFD01180_EDIT_Tax_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Tax_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Tax_Life_Year').prop('disabled', false);
            $('#WFD01180_EDIT_Tax_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_EDIT_Tax_Life_Year').prop('disabled', true);
            $('#WFD01180_EDIT_Tax_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Tax_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Tax_Life_Year').val('');
            $('#WFD01180_EDIT_Tax_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Tax_Life_Month').val('');
            $('#WFD01180_EDIT_Tax_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Consolidate_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Consolidate_Life_Year').val('');
            $('#WFD01180_EDIT_Consolidate_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Consolidate_Life_Month').val('');
            $('#WFD01180_EDIT_Consolidate_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Consolidate_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Consolidate_Life_Year').prop('disabled', false);
            $('#WFD01180_EDIT_Consolidate_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_EDIT_Consolidate_Life_Year').prop('disabled', true);
            $('#WFD01180_EDIT_Consolidate_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Consolidate_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Consolidate_Life_Year').val('');
            $('#WFD01180_EDIT_Consolidate_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Consolidate_Life_Month').val('');
            $('#WFD01180_EDIT_Consolidate_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Management_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Management_Life_Year').val('');
            $('#WFD01180_EDIT_Management_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Management_Life_Month').val('');
            $('#WFD01180_EDIT_Management_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Management_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Management_Life_Year').prop('disabled', false);
            $('#WFD01180_EDIT_Management_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_EDIT_Management_Life_Year').prop('disabled', true);
            $('#WFD01180_EDIT_Management_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Management_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Management_Life_Year').val('');
            $('#WFD01180_EDIT_Management_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Management_Life_Month').val('');
            $('#WFD01180_EDIT_Management_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Leasing_Local_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val('');
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val('');
            $('#WFD01180_EDIT_Leasing_Local_N_A_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Leasing_Local_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').prop('disabled', false);
            $('#WFD01180_EDIT_Leasing_Local_N_A_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').prop('disabled', true);
            $('#WFD01180_EDIT_Leasing_Local_N_A_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Leasing_Local_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val('');
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val('');
            $('#WFD01180_EDIT_Leasing_Local_N_A_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////
    $('#WFD01180_EDIT_Leasing_Consolidation_Base_On_Contact').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val('');
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').prop('disabled', true);

            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val('');
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Leasing_Consolidation_Life').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').prop('disabled', false);
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', false);
        }
        else {
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').prop('disabled', true);
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', true);
        }
    });

    $('#WFD01180_EDIT_Leasing_Consolidation_N_A').click(function () {
        if ($(this).is(":checked")) {
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val('');
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', true);

            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val('');
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', true);
        }
    });
    //////////////////////////////////////////////////////////////////////////////////////

    $('#WFD01180MultipleBoxCompany').change(function () {
        LoadComboboxOnSearchScreen($('#WFD01180_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: getCompanyValue() }));
    });

    $('#WFD01180Search').click(function (event) {
        $('#WFD01180Loading').show();
        //Validation
        if ($('#WFD01180MultipleBoxCompany').val() === null) {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Company"), MSG_AREA_ADD);
            $('#WFD01180Loading').hide();
            return;
        }

        $('#WFD01180Edit').prop('disabled', true);
        $('#WFD01180Delete').prop('disabled', true);
        
        Ctrl01180.Pagin.ClearPaginData();

        fAction.LoadData(function () {
            $('#WFD01180Loading').hide();
        });
        
    });

    $('#WFD01180Clear').click(function () {
        $('#WFD01180Loading').show();
        location.reload();
    });

    $('#WFD01180_ADD_Company').change(function () {
        LoadComboboxOnSearchScreen($('#WFD01180_ADD_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: ($('#WFD01180_ADD_Company').val()) }));
    });

    $('#WFD01180Add').click(function (event) {
        //LoadComboboxScreen($('#WFD01180_ADD_Company'), MainUrl.GetCompany);
        //LoadComboboxScreen2($('#WFD01180_ADD_ASSET_CLASS'), MainUrl.GetAssetClass);

        //LoadComboboxScreen2($('#WFD01180_ADD_Physical_check'), MainUrl.GetAssetClass);
        //LoadComboboxScreen2($('#WFD01180_ADD_Plate_Type'), MainUrl.GetPlateType);
        //LoadComboboxScreen2($('#WFD01180_ADD_Plate_Size'), MainUrl.GetPlateSize);
        ClearMessageC();
        isClear();

        if ($('#WFD01180MultipleBoxCompany').val() != null && $('#WFD01180MultipleBoxCompany').val().length == 1) {
            $('#WFD01180_ADD_Company').select2("val", $('#WFD01180MultipleBoxCompany').val());
        }
        
        //$('#WFD01180_ADD_ASSET_CLASS').select2("val", $('#WFD01180_ASSET_CLASS').val());

        //LoadAssetComboboxScreen($('#WFD01180_ADD_ASSET_CLASS'), MainUrl.GetAssetClass, $('#WFD01180_ASSET_CLASS').val());

        //$('#WFD01180_ADD_MINOR_CATEGORY').val($('#WFD01180_MINOR_CATEGORY').val());
        $('#WFD01180_ADD_Physical_check').select2("val", 'N');

        $('#WFD01180_ADD_Plate_Type').select2("val", '');
        $('#WFD01180_ADD_Plate_Size').select2("val", '');

        $('#myModalAddInfo').modal();
    });


    //var modalConfirm = function (confirm_text, callback) {

    //    $('#myModalLabel').text(confirm_text);
    //    $("#confirm-modal").modal('show');

    //    $("#modal-btn-si").on("click", function () {
    //        callback(true);
    //        $("#confirm-modal").modal('hide');
    //    });

    //    $("#modal-btn-no").on("click", function () {
    //        callback(false);
    //        $("#confirm-modal").modal('hide');
    //    });
    //};

    $('#btnAddInfoSave').click(function (e) {

        //modalConfirm(Message.MSTD0006ACFM,
        //    function (confirm) {
        //        if (confirm) {
        //            ChkprecistionScreen();
        //        }
        //    }
        //);
        if (confirm(Message.MSTD0006ACFM)) {
            ChkprecistionScreen();
        } 
        /*loadConfirmAlert(Message.MSTD0006ACFM, function (result) {
            if (!result) {
                return;
            }
            ChkprecistionScreen();
        })*/
    })

    $('#btnAddInfoCancel').click(function (e) {
        //modalConfirm("Are you sure you want to Cancel Add Operation ?",
        //    function (confirm) {
        //        if (confirm) {
        //            isClear();
        //            $('#myModalAddInfo').modal('hide');
        //        }
        //    }
        //);
        if (confirm("Are you sure you want to Cancel Add Operation ?")) {
            isClear();
            $('#myModalAddInfo').modal('hide');
        }
        /*loadConfirmAlert("Are you sure you want to Cancel Add Operation ?", function (result) {
            if (!result) {
                return;
            }
            isClear();
            $('#myModalAddInfo').modal('hide');
        })*/
    })

    $('#WFD01180_EDIT_Company').change(function () {
        LoadComboboxOnSearchScreen($('#WFD01180_EDIT_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: JSON.stringify($('#WFD01180_EDIT_Company').val()) }));
    });

    $('#WFD01180Edit').click(function (event) {
        ClearMessageC();
        var MyTable = $('#tbSearchResult').DataTable();
        var rows = $(MyTable.$('input[type="checkbox"]').map(function () {
            return $(this).prop("checked") ? $(this).closest('tr') : null;
        }));

        if (rows.length == 0)
        {
            alert('A single record must be selected to Edit operation.');
        } else if (rows.length > 1)
        {
            alert('A single record must be selected to Edit operation.');
        } else
        {
            var _input = rows[0].context;
            var _input_name = _input.id;
            var res = _input_name.split("-");

            myObj = GetSelectRow(res[1], res[2], res[3]);
            //$('#WFD01180_EDIT_Company').select2("val", myObj[0].COMPANY);
            $('#WFD01180_EDIT_Company').val(myObj[0].COMPANY);
            $('#WFD01180_EDIT_Physical_check').select2("val", myObj[0].INVENTORY_INDICATOR);
            //$('#WFD01180_EDIT_ASSET_CLASS').select2("val", myObj[0].ASSET_CLASS);
            $('#WFD01180_EDIT_ASSET_CLASS').val(myObj[0].ASSET_CLASS);
            $('#WFD01180_EDIT_ASSET_DESC').text(myObj[0].ASSET_DESCRIPTION);
            $('#WFD01180_EDIT_Plate_Type').select2("val", myObj[0].STICKER_TYPE);
            $('#WFD01180_EDIT_Plate_Size').select2("val", myObj[0].STICKER_SIZE);

            $('#WFD01180_EDIT_MINOR_CATEGORY').val(myObj[0].MINOR_CATEGORY);
            $('#WFD01180_EDIT_MINOR_CATEGORY_DESC').text(myObj[0].MINOR_CATEGORY_DESCRIPTION);
            //LoadComboboxOnSearchScreen($('#WFD01180_EDIT_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: JSON.stringify($('#WFD01180_EDIT_Company').val()) }));

            //$('#WFD01180_EDIT_MINOR_CATEGORY').select2("val", myObj[0].MINOR_CATEGORY);
            //$('#WFD01180_EDIT_MI0R_CATEGORY').val(myObj[0].MINOR_CATEGORY);
            //$('#WFD01180_EDIT_MI0R_CATEGORY').val(myObj[0].MINOR_CATEGORY_DESCRIPTION);

            $('#WFD01180_EDIT_Local_Depre_Key').val(myObj[0].DPRE_KEY_01);
            $('#WFD01180_EDIT_Local_Life_TXT').val(myObj[0].USEFUL_LIFE_01);
            if (myObj[0].USEFUL_LIFE_TYPE_01 == 'BC') {
                $('#WFD01180_EDIT_Local_Base_On_Contact').prop('checked', true);
            } else if (myObj[0].USEFUL_LIFE_TYPE_01 == 'VL') {
                $('#WFD01180_EDIT_Local_Life').prop('checked', true);
                $('#WFD01180_EDIT_Local_Life_Year').prop('disabled', false).val(myObj[0].USEFUL_LIFE_YEAR_01);
                $('#WFD01180_EDIT_Local_Life_Month').prop('disabled', false).val(myObj[0].USEFUL_LIFE_PERD_01);
            } else if (myObj[0].USEFUL_LIFE_TYPE_01 == 'NA') {
                $('#WFD01180_EDIT_Local_N_A').prop('checked', true);
            }
            //USEFUL_LIFE_TYPE_01: LIFE_TYPE_01,
            $('#WFD01180_EDIT_Local_Scrap_Value').val(myObj[0].SCRAP_VALUE_01);

            if (myObj[0].DEACT_DEPRE_AREA_15 == '1') {
                $('#WFD01180_EDIT_Tax_Deactivate').prop("checked", true);
            } else {
                $('#WFD01180_EDIT_Tax_Deactivate').prop("checked", false);
            }
            $('#WFD01180_EDIT_Tax_Depre_Key').val(myObj[0].DPRE_KEY_15);
            $('#WFD01180_EDIT_Tax_Life_TXT').val(myObj[0].USEFUL_LIFE_15);

            if (myObj[0].USEFUL_LIFE_TYPE_15 == 'BC') {
                $('#WFD01180_EDIT_Tax_Base_On_Contact').prop('checked', true);
            } else if (myObj[0].USEFUL_LIFE_TYPE_15 == 'VL') {
                $('#WFD01180_EDIT_Tax_Life').prop('checked', true);
                $('#WFD01180_EDIT_Tax_Life_Year').prop('disabled', false).val(myObj[0].USEFUL_LIFE_YEAR_15);
                $('#WFD01180_EDIT_Tax_Life_Month').prop('disabled', false).val(myObj[0].USEFUL_LIFE_PERD_15);
            } else if (myObj[0].USEFUL_LIFE_TYPE_15 == 'NA') {
                $('#WFD01180_EDIT_Tax_N_A').prop('checked', true);
            }
            //USEFUL_LIFE_TYPE_15: LIFE_TYPE_15,
            $('#WFD01180_EDIT_Tax_Scrap_Value').val(myObj[0].SCRAP_VALUE_15);

             $('#WFD01180_EDIT_Consolidate_Depre_Key').val(myObj[0].DPRE_KEY_31);
             $('#WFD01180_EDIT_Consolidate_Life_TXT').val(myObj[0].USEFUL_LIFE_31);
             if (myObj[0].USEFUL_LIFE_TYPE_31 == 'BC') {
                 $('#WFD01180_EDIT_Consolidate_Base_On_Contact').prop('checked', true);
             } else if (myObj[0].USEFUL_LIFE_TYPE_31 == 'VL') {
                 $('#WFD01180_EDIT_Consolidate_Life').prop('checked', true);
                 $('#WFD01180_EDIT_Consolidate_Life_Year').prop('disabled', false).val(myObj[0].USEFUL_LIFE_YEAR_31);
                 $('#WFD01180_EDIT_Consolidate_Life_Month').prop('disabled', false).val(myObj[0].USEFUL_LIFE_PERD_31);
             } else if (myObj[0].USEFUL_LIFE_TYPE_31 == 'NA') {
                 $('#WFD01180_EDIT_Consolidate_N_A').prop('checked', true);
             }
            //USEFUL_LIFE_TYPE_31: LIFE_TYPE_31,
             $('#WFD01180_EDIT_Consolidate_Scrap_Value').val(myObj[0].SCRAP_VALUE_31);

            $('#WFD01180_EDIT_Management_Depre_Key').val(myObj[0].DPRE_KEY_41);
            $('#WFD01180_EDIT_Management_Life_TXT').val(myObj[0].USEFUL_LIFE_41);
            if (myObj[0].USEFUL_LIFE_TYPE_41 == 'BC') {
                $('#WFD01180_EDIT_Management_Base_On_Contact').prop('checked', true);
            } else if (myObj[0].USEFUL_LIFE_TYPE_41 == 'VL') {
                $('#WFD01180_EDIT_Management_Life').prop('checked', true);
                $('#WFD01180_EDIT_Management_Life_Year').prop('disabled', false).val(myObj[0].USEFUL_LIFE_YEAR_41);
                $('#WFD01180_EDIT_Management_Life_Month').prop('disabled', false).val(myObj[0].USEFUL_LIFE_PERD_41);
            } else if (myObj[0].USEFUL_LIFE_TYPE_41 == 'NA') {
                $('#WFD01180_EDIT_Management_N_A').prop('checked', true);
            }
            //USEFUL_LIFE_TYPE_41: LIFE_TYPE_41,
            $('#WFD01180_EDIT_Management_Scrap_Value').val(myObj[0].SCRAP_VALUE_41);

            $('#WFD01180_EDIT_Leasing_Local_Depre_Key').val(myObj[0].DPRE_KEY_81);
            $('#WFD01180_EDIT_Leasing_Local_Life_TXT').val(myObj[0].USEFUL_LIFE_81);
            if (myObj[0].USEFUL_LIFE_TYPE_81 == 'BC') {
                $('#WFD01180_EDIT_Leasing_Local_Base_On_Contact').prop('checked', true);
            } else if (myObj[0].USEFUL_LIFE_TYPE_81 == 'VL') {
                $('#WFD01180_EDIT_Leasing_Local_Life').prop('checked', true);
                $('#WFD01180_EDIT_Leasing_Local_N_A_Year').prop('disabled', false).val(myObj[0].USEFUL_LIFE_YEAR_81);
                $('#WFD01180_EDIT_Leasing_Local_N_A_Month').prop('disabled', false).val(myObj[0].USEFUL_LIFE_PERD_81);
            } else if (myObj[0].USEFUL_LIFE_TYPE_81 == 'NA') {
                $('#WFD01180_EDIT_Leasing_Local_N_A').prop('checked', true);
            }
            //USEFUL_LIFE_TYPE_81: LIFE_TYPE_81,
            $('#WFD01180_EDIT_Leasing_Local_Scrap_Value').val(myObj[0].SCRAP_VALUE_81);

            $('#WFD01180_EDIT_Leasing_Consolidation_Depre_Key').val(myObj[0].DPRE_KEY_91);
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_TXT').val(myObj[0].COMPANY);
            if (myObj[0].USEFUL_LIFE_TYPE_91 == 'BC') {
                $('#WFD01180_EDIT_Leasing_Consolidation_Base_On_Contact').prop('checked', true);
            } else if (myObj[0].USEFUL_LIFE_TYPE_91 == 'VL') {
                $('#WFD01180_EDIT_Leasing_Consolidation_Life').prop('checked', true);
                $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').prop('disabled', false).val(myObj[0].USEFUL_LIFE_YEAR_91);
                $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').prop('disabled', false).val(myObj[0].USEFUL_LIFE_PERD_91);
            } else if (myObj[0].USEFUL_LIFE_TYPE_91 == 'NA') {
                $('#WFD01180_EDIT_Leasing_Consolidation_N_A').prop('checked', true);
            }
            //USEFUL_LIFE_TYPE_81: LIFE_TYPE_81,
            $('#WFD01180_EDIT_Leasing_Consolidation_Scrap_Value').val(myObj[0].SCRAP_VALUE_91);


            $('#myModalEditInfo').modal();
        }
    });

    $('#btnEditInfoSave').click(function (e) {
        if (confirm(Message.MSTD0006ACFM)) {
            UpdDeprecistionScreen(MainUrl.UpdDeprecistion);
        }
        /*loadConfirmAlert(Message.MSTD0006ACFM, function (result) {
            if (!result) {
                return;
            }
            UpdDeprecistionScreen(MainUrl.UpdDeprecistion);
        })*/
    })

    $('#btnEditInfoCancel').click(function (e) {
        if (confirm("Are you sure you want to Cancel Edit Operation ?")) {
            isClear();
            $('#myModalEditInfo').modal('hide');
        }
        /*loadConfirmAlert("Are you sure you want to Cancel Edit Operation ?", function (result) {
            if (!result) {
                return;
            }
            isClear();
            $('#myModalEditInfo').modal('hide');
        })*/
    })

    $('#WFD01180Delete').click(function (event) {
        var MyTable = $('#tbSearchResult').DataTable();
        var rows = $(MyTable.$('input[type="checkbox"]').map(function () {
            return $(this).prop("checked") ? $(this).closest('tr') : null;
        }));

        if (rows.length == 0) {
            alert('A single record must be selected to delete operation.');
        } else if (rows.length > 1) {
            alert('A single record must be selected to delete operation.');
        } else {
            loadConfirmAlert("Are you sure you want to delete the record ?", function (result) {
                if (!result) {
                    return;
                }
                //Clear Screen
                var _input = rows[0].context;
                var _input_name = _input.id;
                var res = _input_name.split("-");

                myObj = GetSelectRow(res[1], res[2], res[3]);
                DelDeprecistionScreen(MainUrl.DelDeprecistion);
            })
        }
    });

    Ctrl01180.SearchPanel.hide();

    $('#WFD01180_ASSET_CLASS').select2({ dropdownCssClass: 'auto-drop', dropdownAutoWidth: true });
});

var LoadComboboxOnSearchScreen = function(combobox, url, data){  
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        data: data,
        success: function (data) {           
            combobox.empty();
            combobox.val('').change();
            //combobox.append($("<option>&nbsp;</option>").val('').html(null));
            combobox.append($("<option>&nbsp;</option>").val(''));


            if (myObj.length > 0) {
                $.each(data.ObjectResult, function (key, value) {
                    if (value.CODE == myObj[0].MINOR_CATEGORY) {
                        combobox.append($("<option selected ></option>").val(value.CODE).html(value.CODE + " : " + value.VALUE));
                    } else {
                        combobox.append($("<option></option>").val(value.CODE).html(value.CODE + " : " + value.VALUE));
                    }
                });
            } else {
                $.each(data.ObjectResult, function (key, value) {
                    if ((value.CODE == $('#WFD01180_MINOR_CATEGORY').val()))
                    {
                        combobox.append($("<option selected ></option>").val(value.CODE).html(value.CODE + " : " + value.VALUE));
                    } else {
                        combobox.append($("<option></option>").val(value.CODE).html(value.CODE + " : " + value.VALUE));
                    }
                });
            }     
        }
    });
}

var LoadComboboxScreen = function (combobox, url) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        success: function (data) {
            combobox.empty();
            combobox.val('').change();
            $.each(data.ObjectResult, function (key, value) {
                if (value.Selected) {
                    combobox.append($("<option selected ></option>").val(value.CODE).html(value.CODE));
                } else {
                    combobox.append($("<option></option>").val(value.CODE).html(value.CODE));
                }
            });
        }
    });
}

var LoadAssetComboboxScreen = function (combobox, url, _Asset) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        success: function (data) {
            combobox.empty();
            combobox.val('').change();
            $.each(data.ObjectResult, function (key, value) {
                if (value.CODE == _Asset) {
                    combobox.append($("<option selected ></option>").val(value.CODE).html(value.CODE));
                } else {
                    combobox.append($("<option></option>").val(value.CODE).html(value.CODE));
                }
            });
        }
    });
}

var LoadCHKAssetClass = function () {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: MainUrl.CHKAssetClass,
        dataType: "json",
        success: function (data) {
            DataCHKAssetClass = data.ObjectResult;
        }
    });
}

var LoadComboboxScreenEdit = function (combobox, url,v) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        success: function (data) {
            combobox.empty();
            combobox.val('').change();
            $.each(data.ObjectResult, function (key, value) {
                if (value.CODE == v) {
                    combobox.append($("<option selected ></option>").val(value.CODE).html(value.CODE));
                } else {
                    combobox.append($("<option></option>").val(value.CODE).html(value.CODE));
                }
            });
        }
    });
}

var LoadComboboxScreen2 = function (combobox, url) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        success: function (data) {
            combobox.empty();
            combobox.val('').change();
            $.each(data.ObjectResult, function (key, value) {
                if (value.Selected) {
                    combobox.append($("<option selected ></option>").val(value.CODE).html(value.VALUE));
                } else {
                    combobox.append($("<option></option>").val(value.CODE).html(value.VALUE));
                }
            });
        }
    });
}

var GetUSEFUL_LIFE = function (data) {
    for (var i = 0; i < data.length; i++) {

        data[i].USEFUL_LIFE_01 = '';
        data[i].USEFUL_LIFE_15 = '';
        data[i].USEFUL_LIFE_31 = '';
        data[i].USEFUL_LIFE_41 = '';
        data[i].USEFUL_LIFE_81 = '';
        data[i].USEFUL_LIFE_91 = '';

        if (data[i].USEFUL_LIFE_TYPE_01 != "VL") {
            data[i].USEFUL_LIFE_PERD_01 = "";
            data[i].USEFUL_LIFE_YEAR_01 = "";
        }
        
        if (data[i].USEFUL_LIFE_TYPE_01 == "BC") {
            data[i].USEFUL_LIFE_01 = "Base on Contract";
        } else if (data[i].USEFUL_LIFE_TYPE_01 == "VL") {
            data[i].USEFUL_LIFE_01 = "";
        } else if (data[i].USEFUL_LIFE_TYPE_01 == "NA") {
            data[i].USEFUL_LIFE_01 = "N/A";
        } else {
            data[i].USEFUL_LIFE_01 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_15 != "VL") {
            data[i].USEFUL_LIFE_PERD_15 = "";
            data[i].USEFUL_LIFE_YEAR_15 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_15 == "BC") {
            data[i].USEFUL_LIFE_15 = "Base on Contract";
        } else if (data[i].USEFUL_LIFE_TYPE_15 == "VL") {
            data[i].USEFUL_LIFE_15 = "";
        } else if (data[i].USEFUL_LIFE_TYPE_15 == "NA") {
            data[i].USEFUL_LIFE_15 = "N/A";
        } else {
            data[i].USEFUL_LIFE_15 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_31 != "VL") {
            data[i].USEFUL_LIFE_PERD_31 = "";
            data[i].USEFUL_LIFE_YEAR_31 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_31 == "BC") {
            data[i].USEFUL_LIFE_31 = "Base on Contract";
        } else if (data[i].USEFUL_LIFE_TYPE_31 == "VL") {
            data[i].USEFUL_LIFE_31 = "";
        } else if (data[i].USEFUL_LIFE_TYPE_31 == "NA") {
            data[i].USEFUL_LIFE_31 = "N/A";
        } else {
            data[i].USEFUL_LIFE_31 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_41 != "VL") {
            data[i].USEFUL_LIFE_PERD_41 = "";
            data[i].USEFUL_LIFE_YEAR_41 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_41 == "BC") {
            data[i].USEFUL_LIFE_41 = "Base on Contract";
        } else if (data[i].USEFUL_LIFE_TYPE_41 == "VL") {
            data[i].USEFUL_LIFE_41 = "";
        } else if (data[i].USEFUL_LIFE_TYPE_41 == "NA") {
            data[i].USEFUL_LIFE_41 = "N/A";
        } else {
            data[i].USEFUL_LIFE_41 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_81 != "VL") {
            data[i].USEFUL_LIFE_PERD_81 = "";
            data[i].USEFUL_LIFE_YEAR_81 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_81 == "BC") {
            data[i].USEFUL_LIFE_81 = "Base on Contract";
        } else if (data[i].USEFUL_LIFE_TYPE_81 == "VL") {
            data[i].USEFUL_LIFE_81 = "";
        } else if (data[i].USEFUL_LIFE_TYPE_81 == "NA") {
            data[i].USEFUL_LIFE_81 = "N/A";
        } else {
            data[i].USEFUL_LIFE_81 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_91 != "VL") {
            data[i].USEFUL_LIFE_PERD_91 = "";
            data[i].USEFUL_LIFE_YEAR_91 = "";
        }

        if (data[i].USEFUL_LIFE_TYPE_91 == "BC") {
            data[i].USEFUL_LIFE_91 = "Base on Contract";
        } else if (data[i].USEFUL_LIFE_TYPE_91 == "VL") {
            data[i].USEFUL_LIFE_91 = "";
        } else if (data[i].USEFUL_LIFE_TYPE_91 == "NA") {
            data[i].USEFUL_LIFE_91 = "N/A";
        } else {
            data[i].USEFUL_LIFE_91 = "";
        }
    }
    return data;
}

var InsDeprecistionScreen = function (url) {
    ClearMessageC();

    if ($('#WFD01180_ADD_Company').val() == "")
    {
        AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Company"), MSG_AREA_ADD);
        $('#WFD01180_ADD_Company').focus();
        return;
    }

    if ($('#WFD01180_ADD_ASSET_CLASS').val() == "")
    {
        
        AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Asset Class"), MSG_AREA_ADD);
        $('#WFD01180_ADD_ASSET_CLASS').focus();
        return;
    }

    if ($('#WFD01180_ADD_MINOR_CATEGORY').val() == "")
    {
        AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Minor Category"), MSG_AREA_ADD);
        $('#WFD01180_ADD_MINOR_CATEGORY').focus();
        //fnErrorDialog("Error", 'Minor Category is null!');
        return;
    }

    /*
    var tmp = $.grep(DataCHKAssetClass, function (n) {
        return (n.CODE == $('#WFD01180_ADD_ASSET_CLASS').val());
    });
    if (tmp.length > 0) {
        if(tmp[0].VALUE.toLowerCase() == 'right of use' || tmp[0].VALUE.toLowerCase() == 'leasing'){
            if ($('#WFD01180_ADD_Leasing_Local_Depre_Key').val() == '') {
                //AlertTextErrorMessage(Message.MSTD0031AERR.format("Minor Category"), MSG_AREA_ADD);
                fnErrorDialog("Error", 'Asset Class ' + tmp[0].VALUE + ' Required 81 Leasing Book - Local');
                return;
            }

            if ($('#WFD01180_ADD_Leasing_Consolidation_Depre_Key').val() == '') {
                fnErrorDialog("Error", 'Asset Class ' + tmp[0].VALUE + ' Required 91 Leasing Book - Global');
                return;
            }
        }
    }
    */

    var radioValue_Local_Life = $("input[id='WFD01180_ADD_Local_Life']:checked").val();
    if (radioValue_Local_Life) {
        if ($('#WFD01180_ADD_Local_Life_Year').val() == '' && $('#WFD01180_ADD_Local_Life_Month').val() == '')
        {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 01 Local Accounting Book"), MSG_AREA_ADD);
            $('#WFD01180_ADD_Company').focus(); //focus for go to top
            $('#WFD01180_ADD_Local_Life_Year').focus();
            //fnErrorDialog("Error", 'Check local life Year/Month required Year or Month');
            return;
        }    
    }

    var radioValue_Tax_Life = $("input[id='WFD01180_ADD_Tax_Life']:checked").val();
    if (radioValue_Tax_Life) {
        if ($('#WFD01180_ADD_Tax_Life_Year').val() == '' && $('#WFD01180_ADD_Tax_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 15 Local Tax Book"), MSG_AREA_ADD);
            $('#WFD01180_ADD_Company').focus(); //focus for go to top
            $('#WFD01180_ADD_Tax_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Tax life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Consolidate_Life = $("input[id='WFD01180_ADD_Consolidate_Life']:checked").val();
    if (radioValue_Consolidate_Life) {
        if ($('#WFD01180_ADD_Consolidate_Life_Year').val() == '' && $('#WFD01180_ADD_Consolidate_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 31 Consolidation Book"), MSG_AREA_ADD);
            $('#WFD01180_ADD_Company').focus(); //focus for go to top
            $('#WFD01180_ADD_Consolidate_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Consolidate life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Management_Life = $("input[id='WFD01180_ADD_Management_Life']:checked").val();
    if (radioValue_Management_Life) {
        if ($('#WFD01180_ADD_Management_Life_Year').val() == '' && $('#WFD01180_ADD_Management_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 41 Management Reporting"), MSG_AREA_ADD);
            $('#WFD01180_ADD_Company').focus(); //focus for go to top
            $('#WFD01180_ADD_Management_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Management life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Leasing_Local_Life = $("input[id='WFD01180_ADD_Leasing_Local_Life']:checked").val();
    if (radioValue_Leasing_Local_Life) {
        if ($('#WFD01180_ADD_Leasing_Local_N_A_Year').val() == '' && $('#WFD01180_ADD_Leasing_Local_N_A_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 81 Leasing Book - Local"), MSG_AREA_ADD);
            $('#WFD01180_ADD_Company').focus(); //focus for go to top
            $('#WFD01180_ADD_Leasing_Local_N_A_Year').focus();
            //fnErrorDialog("Error", 'Check Leasing local Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Leasing_Consolidation_Life = $("input[id='WFD01180_ADD_Leasing_Consolidation_Life']:checked").val();
    if (radioValue_Leasing_Consolidation_Life) {
        if ($('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val() == '' && $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 91 Leasing Book - Global"), MSG_AREA_ADD);
            $('#WFD01180_ADD_Company').focus(); //focus for go to top
            $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Leasing consolidation life Year/Month required Year or Month');
            return;
        }
    }

    
    if (true)
    {
        var LIFE_TYPE_01;
        if ($("#WFD01180_ADD_Local_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_01 = 'BC';
        } else if ($("#WFD01180_ADD_Local_Life").prop("checked") == true) {
            LIFE_TYPE_01 = 'VL';
        } else if ($("#WFD01180_ADD_Local_N_A").prop("checked") == true) {
            LIFE_TYPE_01 = 'NA';
        }

        var LIFE_TYPE_15;
        if ($("#WFD01180_ADD_Tax_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_15 = 'BC';
        } else if ($("#WFD01180_ADD_Tax_Life").prop("checked") == true) {
            LIFE_TYPE_15 = 'VL';
        } else if ($("#WFD01180_ADD_Tax_N_A").prop("checked") == true) {
            LIFE_TYPE_15 = 'NA';
        }

        var LIFE_TYPE_31;
        if ($("#WFD01180_ADD_Consolidate_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_31 = 'BC';
        } else if ($("#WFD01180_ADD_Consolidate_Life").prop("checked") == true) {
            LIFE_TYPE_31 = 'VL';
        } else if ($("#WFD01180_ADD_Consolidate_N_A").prop("checked") == true) {
            LIFE_TYPE_31 = 'NA';
        }

        var LIFE_TYPE_41;
        if ($("#WFD01180_ADD_Management_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_41 = 'BC';
        } else if ($("#WFD01180_ADD_Management_Life").prop("checked") == true) {
            LIFE_TYPE_41 = 'VL';
        } else if ($("#WFD01180_ADD_Management_N_A").prop("checked") == true) {
            LIFE_TYPE_41 = 'NA';
        }

        var LIFE_TYPE_81;
        if ($("#WFD01180_ADD_Leasing_Local_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_81 = 'BC';
        } else if ($("#WFD01180_ADD_Leasing_Local_Life").prop("checked") == true) {
            LIFE_TYPE_81 = 'VL';
        } else if ($("#WFD01180_ADD_Leasing_Local_N_A").prop("checked") == true) {
            LIFE_TYPE_81 = 'NA';
        }

        var LIFE_TYPE_91;
        if ($("#WFD01180_ADD_Leasing_Consolidation_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_91 = 'BC';
        } else if ($("#WFD01180_ADD_Leasing_Consolidation_Life").prop("checked") == true) {
            LIFE_TYPE_91 = 'VL';
        } else if ($("#WFD01180_ADD_Leasing_Consolidation_N_A").prop("checked") == true) {
            LIFE_TYPE_91 = 'NA';
        }

        var tDeactDepreArea_15 = $('#WFD01180_ADD_Tax_Deactivate').prop('checked') ? "Y" : "N";

        var _data = {
            COMPANY: $('#WFD01180_ADD_Company').val(),
            ASSET_CLASS: $('#WFD01180_ADD_ASSET_CLASS').val(),
            MINOR_CATEGORY: $('#WFD01180_ADD_MINOR_CATEGORY').val(),
            ASSET_DESCRIPTION: $('#WFD01180_ADD_MINOR_CATEGORY').val(),
            STICKER_TYPE: $('#WFD01180_ADD_Plate_Type').val(),
            STICKER_SIZE: $('#WFD01180_ADD_Plate_Size').val(),
            INVENTORY_INDICATOR: $('#WFD01180_ADD_Physical_check').val(),
            MINOR_CATEGORY_DESCRIPTION: '',

            DPRE_KEY_01: $('#WFD01180_ADD_Local_Depre_Key').val(),
            USEFUL_LIFE_YEAR_01: $('#WFD01180_ADD_Local_Life_Year').val(),
            USEFUL_LIFE_PERD_01: $('#WFD01180_ADD_Local_Life_Month').val(),
            USEFUL_LIFE_TYPE_01: LIFE_TYPE_01,
            SCRAP_VALUE_01: $('#WFD01180_ADD_Local_Scrap_Value').val(),

            DEACT_DEPRE_AREA_15: tDeactDepreArea_15,
            DPRE_KEY_15: $('#WFD01180_ADD_Tax_Depre_Key').val(),
            USEFUL_LIFE_YEAR_15: $('#WFD01180_ADD_Tax_Life_Year').val(),
            USEFUL_LIFE_PERD_15: $('#WFD01180_ADD_Tax_Life_Month').val(),
            USEFUL_LIFE_TYPE_15: LIFE_TYPE_15,
            SCRAP_VALUE_15: $('#WFD01180_ADD_Tax_Scrap_Value').val(),

            DPRE_KEY_31: $('#WFD01180_ADD_Consolidate_Depre_Key').val(),
            USEFUL_LIFE_YEAR_31: $('#WFD01180_ADD_Consolidate_Life_Year').val(),
            USEFUL_LIFE_PERD_31: $('#WFD01180_ADD_Consolidate_Life_Month').val(),
            USEFUL_LIFE_TYPE_31: LIFE_TYPE_31,
            SCRAP_VALUE_31: $('#WFD01180_ADD_Consolidate_Scrap_Value').val(),

            DPRE_KEY_41: $('#WFD01180_ADD_Management_Depre_Key').val(),
            USEFUL_LIFE_YEAR_41: $('#WFD01180_ADD_Management_Life_Year').val(),
            USEFUL_LIFE_PERD_41: $('#WFD01180_ADD_Management_Life_Month').val(),
            USEFUL_LIFE_TYPE_41: LIFE_TYPE_41,
            SCRAP_VALUE_41: $('#WFD01180_ADD_Management_Scrap_Value').val(),

            DPRE_KEY_81: $('#WFD01180_ADD_Leasing_Local_Depre_Key').val(),
            USEFUL_LIFE_YEAR_81: $('#WFD01180_ADD_Leasing_Local_N_A_Year').val(),
            USEFUL_LIFE_PERD_81: $('#WFD01180_ADD_Leasing_Local_N_A_Month').val(),
            USEFUL_LIFE_TYPE_81: LIFE_TYPE_81,
            SCRAP_VALUE_81: $('#WFD01180_ADD_Leasing_Local_Scrap_Value').val(),

            DPRE_KEY_91: $('#WFD01180_ADD_Leasing_Consolidation_Depre_Key').val(),
            USEFUL_LIFE_YEAR_91: $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val(),
            USEFUL_LIFE_PERD_91: $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val(),
            USEFUL_LIFE_TYPE_91: LIFE_TYPE_91,
            SCRAP_VALUE_91: $('#WFD01180_ADD_Leasing_Consolidation_Scrap_Value').val(),

            USER_BY: 'System'
        }

        ajax_method.Post(url, _data, false, function (result) {
            console.log(result);
            if (result.IsError) {
                //fnErrorDialog("Error", result.Message);
                AletTextInfoMessage('MSTD0001AEER : ' + result.Message);
                return;
            }

            //fnCompleteDialog('Saving data is completed successfully.');
            $('#myModalAddInfo').modal('hide');
            isClear();

            //fnErrorDialog(result.Message);
            setTimeout(function() {
                AletTextInfoMessage('MSTD0101AINF : Saving data is completed successfully.');
            }, 1000);
            
            fAction.LoadData();

            //if (callback == null)
            //    return;

            //callback();
        });
    }
}

var UpdDeprecistionScreen = function (url) {
    /*
    if ($('#WFD01180_EDIT_Company').val() == "") {
        fnErrorDialog("Error", 'Company is null!');
        return;
    }

    if ($('#WFD01180_EDIT_ASSET_CLASS').val() == "") {
        fnErrorDialog("Error", 'Asset Class is null!');
        return;
    }

    if ($('#WFD01180_EDIT_MINOR_CATEGORY').val() == "") {
        fnErrorDialog("Error", 'Minor Category is null!');
        return;
    }

    var tmp = $.grep(DataCHKAssetClass, function (n) {
        return (n.CODE == $('#WFD01180_EDIT_ASSET_CLASS').val());
    });

    if (tmp.length > 0) {
        if ($('#WFD01180_EDIT_Leasing_Local_Depre_Key').val() == '') {
            fnErrorDialog("Error", 'Asset Class ' + tmp[0].VALUE + ' Required 81 Leasing Book - Local');
            return;
        }

        if ($('#WFD01180_EDIT_Leasing_Consolidation_Depre_Key').val() == '') {
            fnErrorDialog("Error", 'Asset Class ' + tmp[0].VALUE + ' Required 91 Leasing Book - Global');
            return;
        }
    }
    */

    var radioValue_Local_Life = $("input[id='WFD01180_EDIT_Local_Life']:checked").val();
    if (radioValue_Local_Life) {
        if ($('#WFD01180_EDIT_Local_Life_Year').val() == '' && $('#WFD01180_EDIT_Local_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 01 Local Accounting Book"), MSG_AREA_EDIT);
            $('#WFD01180_EDIT_Physical_check').focus(); //focus for go to top
            $('#WFD01180_EDIT_Local_Life_Year').focus();
            //fnErrorDialog("Error", 'Check local life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Tax_Life = $("input[id='WFD01180_EDIT_Tax_Life']:checked").val();
    if (radioValue_Tax_Life) {
        if ($('#WFD01180_EDIT_Tax_Life_Year').val() == '' && $('#WFD01180_EDIT_Tax_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 15 Local Tax Book"), MSG_AREA_EDIT);
            $('#WFD01180_EDIT_Physical_check').focus(); //focus for go to top
            $('#WFD01180_EDIT_Tax_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Tax life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Consolidate_Life = $("input[id='WFD01180_EDIT_Consolidate_Life']:checked").val();
    if (radioValue_Consolidate_Life) {
        if ($('#WFD01180_EDIT_Consolidate_Life_Year').val() == '' && $('#WFD01180_EDIT_Consolidate_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 31 Consolidation Book"), MSG_AREA_EDIT);
            $('#WFD01180_EDIT_Physical_check').focus(); //focus for go to top
            $('#WFD01180_EDIT_Consolidate_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Consolidate life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Management_Life = $("input[id='WFD01180_EDIT_Management_Life']:checked").val();
    if (radioValue_Management_Life) {
        if ($('#WFD01180_EDIT_Management_Life_Year').val() == '' && $('#WFD01180_EDIT_Management_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 41 Management Reporting"), MSG_AREA_EDIT);
            $('#WFD01180_EDIT_Physical_check').focus(); //focus for go to top
            $('#WFD01180_EDIT_Management_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Management life Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Leasing_Local_Life = $("input[id='WFD01180_EDIT_Leasing_Local_Life']:checked").val();
    if (radioValue_Leasing_Local_Life) {
        if ($('#WFD01180_EDIT_Leasing_Local_N_A_Year').val() == '' && $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 81 Leasing Book - Local"), MSG_AREA_EDIT);
            $('#WFD01180_EDIT_Physical_check').focus(); //focus for go to top
            $('#WFD01180_EDIT_Leasing_Local_N_A_Year').focus();
            //fnErrorDialog("Error", 'Check Leasing local Year/Month required Year or Month');
            return;
        }
    }

    var radioValue_Leasing_Consolidation_Life = $("input[id='WFD01180_EDIT_Leasing_Consolidation_Life']:checked").val();
    if (radioValue_Leasing_Consolidation_Life) {
        if ($('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val() == '' && $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val() == '') {
            AlertTextErrorMessage("MSTD0031AERR : " + Message.MSTD0031AERR.format("Year/Month in 91 Leasing Book - Global"), MSG_AREA_EDIT);
            $('#WFD01180_EDIT_Physical_check').focus(); //focus for go to top
            $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').focus();
            //fnErrorDialog("Error", 'Check Leasing consolidation life Year/Month required Year or Month');
            return;
        }
    }

    if (true)
    {
        var LIFE_TYPE_01;
        if ($("#WFD01180_EDIT_Local_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_01 = 'BC';
        } else if ($("#WFD01180_EDIT_Local_Life").prop("checked") == true) {
            LIFE_TYPE_01 = 'VL';
        } else if ($("#WFD01180_EDIT_Local_N_A").prop("checked") == true) {
            LIFE_TYPE_01 = 'NA';
        }

        var LIFE_TYPE_15;
        if ($("#WFD01180_EDIT_Tax_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_15 = 'BC';
        } else if ($("#WFD01180_EDIT_Tax_Life").prop("checked") == true) {
            LIFE_TYPE_15 = 'VL';
        } else if ($("#WFD01180_EDIT_Tax_N_A").prop("checked") == true) {
            LIFE_TYPE_15 = 'NA';
        }

        var LIFE_TYPE_31;
        if ($("#WFD01180_EDIT_Consolidate_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_31 = 'BC';
        } else if ($("#WFD01180_EDIT_Consolidate_Life").prop("checked") == true) {
            LIFE_TYPE_31 = 'VL';
        } else if ($("#WFD01180_EDIT_Consolidate_N_A").prop("checked") == true) {
            LIFE_TYPE_31 = 'NA';
        }

        var LIFE_TYPE_41;
        if ($("#WFD01180_EDIT_Management_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_41 = 'BC';
        } else if ($("#WFD01180_EDIT_Management_Life").prop("checked") == true) {
            LIFE_TYPE_41 = 'VL';
        } else if ($("#WFD01180_EDIT_Management_N_A").prop("checked") == true) {
            LIFE_TYPE_41 = 'NA';
        }

        var LIFE_TYPE_81;
        if ($("#WFD01180_EDIT_Leasing_Local_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_81 = 'BC';
        } else if ($("#WFD01180_EDIT_Leasing_Local_Life").prop("checked") == true) {
            LIFE_TYPE_81 = 'VL';
        } else if ($("#WFD01180_EDIT_Leasing_Local_N_A").prop("checked") == true) {
            LIFE_TYPE_81 = 'NA';
        }

        var LIFE_TYPE_91;
        if ($("#WFD01180_EDIT_Leasing_Consolidation_Base_On_Contact").prop("checked") == true) {
            LIFE_TYPE_91 = 'BC';
        } else if ($("#WFD01180_EDIT_Leasing_Consolidation_Life").prop("checked") == true) {
            LIFE_TYPE_91 = 'VL';
        } else if ($("#WFD01180_EDIT_Leasing_Consolidation_N_A").prop("checked") == true) {
            LIFE_TYPE_91 = 'NA';
        }

        var tDeactDepreArea_15 = $('#WFD01180_EDIT_Tax_Deactivate').prop('checked') ? "1" : "0";

        var _data = {
            COMPANY: myObj[0].COMPANY,
            ASSET_CLASS: myObj[0].ASSET_CLASS,
            MINOR_CATEGORY: myObj[0].MINOR_CATEGORY,
            ASSET_DESCRIPTION: $('#WFD01180_EDIT_MINOR_CATEGORY').val(),
            STICKER_TYPE: $('#WFD01180_EDIT_Plate_Type').val(),
            STICKER_SIZE: $('#WFD01180_EDIT_Plate_Size').val(),
            INVENTORY_INDICATOR: $('#WFD01180_EDIT_Physical_check').val(),
            MINOR_CATEGORY_DESCRIPTION: '',

            DPRE_KEY_01: $('#WFD01180_EDIT_Local_Depre_Key').val(),
            USEFUL_LIFE_YEAR_01: $('#WFD01180_EDIT_Local_Life_Year').val(),
            USEFUL_LIFE_PERD_01: $('#WFD01180_EDIT_Local_Life_Month').val(),
            USEFUL_LIFE_TYPE_01: LIFE_TYPE_01,
            SCRAP_VALUE_01: $('#WFD01180_EDIT_Local_Scrap_Value').val(),

            DEACT_DEPRE_AREA_15: tDeactDepreArea_15,
            DPRE_KEY_15: $('#WFD01180_EDIT_Tax_Depre_Key').val(),
            USEFUL_LIFE_YEAR_15: $('#WFD01180_EDIT_Tax_Life_Year').val(),
            USEFUL_LIFE_PERD_15: $('#WFD01180_EDIT_Tax_Life_Month').val(),
            USEFUL_LIFE_TYPE_15: LIFE_TYPE_15,
            SCRAP_VALUE_15: $('#WFD01180_EDIT_Tax_Scrap_Value').val(),

            DPRE_KEY_31: $('#WFD01180_EDIT_Consolidate_Depre_Key').val(),
            USEFUL_LIFE_YEAR_31: $('#WFD01180_EDIT_Consolidate_Life_Year').val(),
            USEFUL_LIFE_PERD_31: $('#WFD01180_EDIT_Consolidate_Life_Month').val(),
            USEFUL_LIFE_TYPE_31: LIFE_TYPE_31,
            SCRAP_VALUE_31: $('#WFD01180_EDIT_Consolidate_Scrap_Value').val(),

            DPRE_KEY_41: $('#WFD01180_EDIT_Management_Depre_Key').val(),
            USEFUL_LIFE_YEAR_41: $('#WFD01180_EDIT_Management_Life_Year').val(),
            USEFUL_LIFE_PERD_41: $('#WFD01180_EDIT_Management_Life_Month').val(),
            USEFUL_LIFE_TYPE_41: LIFE_TYPE_41,
            SCRAP_VALUE_41: $('#WFD01180_EDIT_Management_Scrap_Value').val(),

            DPRE_KEY_81: $('#WFD01180_EDIT_Leasing_Local_Depre_Key').val(),
            USEFUL_LIFE_YEAR_81: $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val(),
            USEFUL_LIFE_PERD_81: $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val(),
            USEFUL_LIFE_TYPE_81: LIFE_TYPE_81,
            SCRAP_VALUE_81: $('#WFD01180_EDIT_Leasing_Local_Scrap_Value').val(),

            DPRE_KEY_91: $('#WFD01180_EDIT_Leasing_Consolidation_Depre_Key').val(),
            USEFUL_LIFE_YEAR_91: $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val(),
            USEFUL_LIFE_PERD_91: $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val(),
            USEFUL_LIFE_TYPE_91: LIFE_TYPE_91,
            SCRAP_VALUE_91: $('#WFD01180_EDIT_Leasing_Consolidation_Scrap_Value').val(),

            USER_BY: 'System'
        }

        ajax_method.Post(url, _data, false, function (result) {
            console.log(result);
            if (result.IsError) {
                //fnErrorDialog("Error", result.Message);
                AletTextInfoMessage('MSTD0001AEER : ' + result.Message);
                return;
            }

            //fnCompleteDialog('Saving data is completed successfully.');
            $('#myModalEditInfo').modal('hide');
            isClear();

            //fnErrorDialog(result.Message);
            setTimeout(function () {
                AletTextInfoMessage('MSTD0101AINF : Saving data is completed successfully.');
            }, 1000);

            fAction.LoadData();

            //if (callback == null)
            //    return;

            //callback();
        });
    }
}

var DelDeprecistionScreen = function (url) {
    $('#WFD01180Loading').show();
    var _data = {
        COMPANY: myObj[0].COMPANY,
        ASSET_CLASS: myObj[0].ASSET_CLASS,
        MINOR_CATEGORY: myObj[0].MINOR_CATEGORY
    }

    ajax_method.Post(url, _data, false, function (result) {
        console.log(result);
        if (result.IsError) {
            fnErrorDialog("Error", result.Message);
            return;
        }

        fAction.LoadData(function () {
            AlertTextErrorMessage("MSTD0085AINF : " + Message.MSTD0085AINF.format("Deletion"));
            $('#WFD01180Loading').hide();
        });

        //if (callback == null)
        //    return;

        //callback();
    });
}

var ChkprecistionScreen = function () {
    var _data = {
        COMPANY: $('#WFD01180_ADD_Company').val(),
        ASSET_CLASS: $('#WFD01180_ADD_ASSET_CLASS').val(),
        MINOR_CATEGORY: $('#WFD01180_ADD_MINOR_CATEGORY').val()
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: MainUrl.ChkDepreciation,
        dataType: "json",
        data: JSON.stringify(_data),
        success: function (data) {
            if (data.ObjectResult.length > 0) {
                ClearMessageC();
                AlertTextErrorMessage("MSTD0010AERR : " + Message.MSTD0010AERR.format("Company", data.ObjectResult[0].COMPANY + ", Asset Class = " + data.ObjectResult[0].ASSET_CLASS + ", Minor category = " + data.ObjectResult[0].MINOR_CATEGORY, "Depreciation Master"), MSG_AREA_ADD);
                $('#WFD01180_ADD_Company').focus();
                //fnErrorDialog("Error", ' Duplicate key from Company, Asset Class, Minor Catory');
                return;
            } else {
                InsDeprecistionScreen(MainUrl.InsDeprecistion);
            }
        }
    });
}

var GetSelectData = function (cb, COMPANY, ASSET_CLASS, MINOR_CATEGORY) {
    var found_names = $.grep(TmpData, function (v) {
        return v.COMPANY === COMPANY && v.ASSET_CLASS === ASSET_CLASS && v.MINOR_CATEGORY === MINOR_CATEGORY;
    });

    if (cb.checked == true) {
        myObj.push(found_names);
    } else {
        myObj.splice($.inArray(found_names, myObj), 1);
        //myObj = jQuery.grep(myObj, function (v) {
        //    return v.COMPANY != COMPANY && v.ASSET_CLASS != ASSET_CLASS && v.MINOR_CATEGORY != MINOR_CATEGORY;
            //return value != found_names;
        //});
    }
}

var GetSelectRow = function (COMPANY, ASSET_CLASS, MINOR_CATEGORY) {
    var found_names = $.grep(TmpData, function (v) {
        return v.COMPANY === COMPANY && v.ASSET_CLASS === ASSET_CLASS && v.MINOR_CATEGORY === MINOR_CATEGORY;
    });

    return found_names;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isDecimalKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function getCompanyValue() {
    return GetMultipleCompany($('#WFD01180MultipleBoxCompany'));
}

function isClear()
{
    $('#WFD01180_ADD_Local_N_A').prop("checked", true);
    $('#WFD01180_ADD_Tax_N_A').prop("checked", true);
    $('#WFD01180_ADD_Consolidate_N_A').prop("checked", true);
    $('#WFD01180_ADD_Management_N_A').prop("checked", true);
    $('#WFD01180_ADD_Leasing_Local_N_A').prop("checked", true);
    $('#WFD01180_ADD_Leasing_Consolidation_N_A').prop("checked", true);

    $('#WFD01180_ADD_Company').select2('val','');
    $('#WFD01180_ADD_ASSET_CLASS').val('');

    $('#WFD01180_ADD_ASSET_CLASS').select2("val", '');

    $('#WFD01180_ADD_MINOR_CATEGORY').val('');
    myObj = [];
    LoadComboboxOnSearchScreen($('#WFD01180_ADD_MINOR_CATEGORY'), MainUrl.GetMinorCategory, JSON.stringify({ COMPANY: ($('#WFD01180_ADD_Company').val()) }));
    //$('#WFD01180_ADD_MINOR_CATEGORY').val('');
    $('#WFD01180_ADD_Plate_Type').val('');
    $('#WFD01180_ADD_Plate_Size').val('');
    $('#WFD01180_ADD_Physical_check').val('');

    $('#WFD01180_ADD_Local_Depre_Key').val('');
    $('#WFD01180_ADD_Local_Scrap_Value').val('1.000');

    $('#WFD01180_ADD_Tax_Deactivate').prop("checked", false);
    $('#WFD01180_ADD_Tax_Depre_Key').val('');
    $('#WFD01180_ADD_Tax_Scrap_Value').val('1.000');

    $('#WFD01180_ADD_Consolidate_Depre_Key').val('');
    $('#WFD01180_ADD_Consolidate_Scrap_Value').val('1.000');

    $('#WFD01180_ADD_Management_Depre_Key').val('');
    $('#WFD01180_ADD_Management_Scrap_Value').val('1.000');

    $('#WFD01180_ADD_Leasing_Local_Depre_Key').val('');
    $('#WFD01180_ADD_Leasing_Local_Scrap_Value').val('1.000');

    $('#WFD01180_ADD_Leasing_Consolidation_Depre_Key').val('');
    $('#WFD01180_ADD_Leasing_Consolidation_Scrap_Value').val('1.000');

    $('#WFD01180_ADD_Local_Life_Year').val('');
    $('#WFD01180_ADD_Local_Life_Month').val('');

    $('#WFD01180_ADD_Tax_Life_Year').val('');
    $('#WFD01180_ADD_Tax_Life_Month').val('');

    $('#WFD01180_ADD_Consolidate_Life_Year').val('');
    $('#WFD01180_ADD_Consolidate_Life_Month').val('');

    $('#WFD01180_ADD_Management_Life_Year').val('');
    $('#WFD01180_ADD_Management_Life_Month').val('');

    $('#WFD01180_ADD_Leasing_Local_N_A_Year').val('');
    $('#WFD01180_ADD_Leasing_Local_N_A_Month').val('');

    $('#WFD01180_ADD_Leasing_Consolidation_Life_Year').val('');
    $('#WFD01180_ADD_Leasing_Consolidation_Life_Month').val('');

    $('#WFD01180_EDIT_Company').val('');
    $('#WFD01180_EDIT_ASSET_CLASS').val('');
    $('#WFD01180_EDIT_ASSET_DESC').text('');
    //$('#WFD01180_EDIT_ASSET_CLASS').select2("val", '');
    $('#WFD01180_EDIT_MINOR_CATEGORY').val('');
    $('#WFD01180_EDIT_MINOR_CATEGORY_DESC').text('');
    
   // $('#WFD01180_EDIT_MINOR_CATEGORY').val('');
    $('#WFD01180_EDIT_Plate_Type').val('');
    $('#WFD01180_EDIT_Plate_Size').val('');
    $('#WFD01180_EDIT_Physical_check').val('');

    $('#WFD01180_EDIT_Local_Depre_Key').val('');
    $('#WFD01180_EDIT_Local_Scrap_Value').val('');

    $('#WFD01180_EDIT_Tax_Deactivate').prop("checked", false);
    $('#WFD01180_EDIT_Tax_Depre_Key').val('');
    $('#WFD01180_EDIT_Tax_Scrap_Value').val('');

    $('#WFD01180_EDIT_Consolidate_Depre_Key').val('');
    $('#WFD01180_EDIT_Consolidate_Scrap_Value').val('');

    $('#WFD01180_EDIT_Management_Depre_Key').val('');
    $('#WFD01180_EDIT_Management_Scrap_Value').val('');

    $('#WFD01180_EDIT_Leasing_Local_Depre_Key').val('');
    $('#WFD01180_EDIT_Leasing_Local_Scrap_Value').val('');

    $('#WFD01180_EDIT_Leasing_Consolidation_Depre_Key').val('');
    $('#WFD01180_EDIT_Leasing_Consolidation_Scrap_Value').val('');

    $('#WFD01180_EDIT_Local_Life_Year').val('');
    $('#WFD01180_EDIT_Local_Life_Month').val('');

    $('#WFD01180_EDIT_Tax_Life_Year').val('');
    $('#WFD01180_EDIT_Tax_Life_Month').val('');

    $('#WFD01180_EDIT_Consolidate_Life_Year').val('');
    $('#WFD01180_EDIT_Consolidate_Life_Month').val('');

    $('#WFD01180_EDIT_Management_Life_Year').val('');
    $('#WFD01180_EDIT_Management_Life_Month').val('');

    $('#WFD01180_EDIT_Leasing_Local_N_A_Year').val('');
    $('#WFD01180_EDIT_Leasing_Local_N_A_Month').val('');

    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Year').val('');
    $('#WFD01180_EDIT_Leasing_Consolidation_Life_Month').val('');

    $('#AlertMessageWFD01180').html('');
    $('#AlertMessageEDITWFD01180').html('');
}

var fAction = {
    LoadData: function (callback) {
        console.log("LoadData is callled");
        var _data = {
            //COMPANY: JSON.stringify($('#WFD01180MultipleBoxCompany').val()),
            COMPANY: getCompanyValue(),
            ASSET_CLASS: $('#WFD01180_ASSET_CLASS').val(),
            MINOR_CATEGORY: $('#WFD01180_MINOR_CATEGORY').val()
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: MainUrl.GetDepreciation,
            dataType: "json",
            data: JSON.stringify(_data),
            success: function (data) {

                ClearMessageC();

                if (data == null || data.ObjectResult == null || data.ObjectResult.length == 0) {
                    AlertTextErrorMessage("MSTD0059AERR : " + Message.MSTD0059AERR); //No data found
                } 
                if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
                if (!pagin.OrderType) pagin.OrderType = 'asc';
                pagin = Ctrl01180.Pagin.GetPaginData();

                TmpData = data.ObjectResult;

                pagin.Totalitem = TmpData.length;
                pagin.TotalPage = Math.ceil(pagin.Totalitem / pagin.ItemPerPage);
                SetDatatable(pagin);
            },
            complete: function () {
                if (callback == null)
                    return;
                callback();
            }
        });
    },
    ShowSearchPopup: function (callback) {

    },
    Clear: function (callback) {

    },
    Export: function (callback) {

    },
    AddAsset: function (_data, callback) { //It's call from dialog

    },
    DeleteAsset: function (_AssetNo, _AssetSub, _Company, _DocNo) {

    },
    DownloadTemplate: function (callback) {
        //Under implement
    },
    UploadExcel: function (_fileName) {
        var batch = BatchProcess({
            BatchId: "BFD02AAA",
            Description: "Upload AAA Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function () {
            fAction.LoadData();
        });
    },
    PrepareGenerateFlow: function (callback) {

    },
    PrepareSubmit: function (callback) {

    },
    PrepareApprove: function (callback) {

    },
    PrepareReject: function (callback) {
        if (callback == null)
            return;

        callback();
    },
    GetClientAssetList: function () {
        console.log("GetClientAssetList");
        if (control.DataTable == null) {
            console.log("NULL");
            return;
        }

        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() != null && row.find('td:eq(1)').text() != "") {
                var _rowData = [];
                if (control.DataTable != null) {
                    _rowData = control.DataTable.row(this).data();
                    console.log(_rowData);
                }
            }
        });
    }
}

var SetDatatable = function (_pagin) {
    if (TmpData.length > 0) {
        $('#WFD01180Edit').prop('disabled', false);
        $('#WFD01180Delete').prop('disabled', false);

        if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
            var table = $('#tbSearchResult').DataTable();
            table.destroy();
        }

        var _start = (_pagin.CurrentPage * _pagin.ItemPerPage) - _pagin.ItemPerPage;
        var _end = (_pagin.CurrentPage * _pagin.ItemPerPage) - 1;

        if (_end > (TmpData.length-1))
        {
            _end = (TmpData.length - 1);
        }

        GetUSEFUL_LIFE(TmpData);

        var _Tmp;
        let result = []
        for (let i = _start; i <= _end; i++) {
            result.push(TmpData[i]);
        }

        

        control.DataTable = $('#tbSearchResult').DataTable({
            data: result,
            "columns": [
                { "data": null }, // 0
                { "data": "COMPANY", 'orderable': false, "class": "text-left" },
                { "data": "ASSET_CLASS", 'orderable': false, "class": "text-center" },
                { "data": "ASSET_DESCRIPTION", 'orderable': false, "class": "text-left" },
                { "data": "MINOR_CATEGORY", 'orderable': false, "class": "text-left" },
                { "data": "MINOR_CATEGORY_DESCRIPTION", 'orderable': false, "class": "text-left" },
                { "data": "DPRE_KEY_01", 'orderable': false, "class": "text-left" },
                { "data": "USEFUL_LIFE_01", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_YEAR_01", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_PERD_01", 'orderable': false, "class": "text-center" },
                { "data": "SCRAP_VALUE_01", 'orderable': false, "class": "text-right" },
                { "data": "DPRE_KEY_15", 'orderable': false, "class": "text-left" },
                { "data": "USEFUL_LIFE_15", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_YEAR_15", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_PERD_15", 'orderable': false, "class": "text-center" },
                { "data": "SCRAP_VALUE_15", 'orderable': false, "class": "text-right" },
                { "data": "DPRE_KEY_31", 'orderable': false, "class": "text-left" },
                { "data": "USEFUL_LIFE_31", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_YEAR_31", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_PERD_31", 'orderable': false, "class": "text-center" },
                { "data": "SCRAP_VALUE_31", 'orderable': false, "class": "text-right" },
                { "data": "DPRE_KEY_41", 'orderable': false, "class": "text-left" },
                { "data": "USEFUL_LIFE_41", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_YEAR_41", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_PERD_41", 'orderable': false, "class": "text-center" },
                { "data": "SCRAP_VALUE_41", 'orderable': false, "class": "text-right" },
                { "data": "DPRE_KEY_81", 'orderable': false, "class": "text-left" },
                { "data": "USEFUL_LIFE_81", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_YEAR_81", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_PERD_81", 'orderable': false, "class": "text-center" },
                { "data": "SCRAP_VALUE_81", 'orderable': false, "class": "text-right" },
                { "data": "DPRE_KEY_91", 'orderable': false, "class": "text-left" },
                { "data": "USEFUL_LIFE_91", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_YEAR_91", 'orderable': false, "class": "text-center" },
                { "data": "USEFUL_LIFE_PERD_91", 'orderable': false, "class": "text-center" },
                { "data": "SCRAP_VALUE_91", 'orderable': false, "class": "text-right" },
            ],
            'columnDefs': [
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'text-center',
                    'data': 2,
                    'ordering': false,
                    'render': function (data, type, full, meta) {
                        return fRender.CheckBoxRender(data);
                    }
                }
            ],
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "No data found",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "emptyTable": "No data available in table"
            },
            'ordering': false,
            "searching": false,
            "paging": false,
            paging: false,
            "retrieve": true,
            "bInfo": false,
            "autoWidth": false
        });

        Ctrl01180.SearchTable = $('#tbSearchResult');
        Ctrl01180.SearchPanel = $('#WFD01180_SearchResult');
        Ctrl01180.Pagin = $('#tbSearchResult').Pagin();

        pagin.Totalitem = TmpData.length;
        pagin.TotalPage = Math.ceil(pagin.Totalitem / pagin.ItemPerPage);

        Ctrl01180.Pagin.Init(pagin, sorting, changePage, 'itemPerPage');
        Ctrl01180.SearchPanel.show();
        $('#tbSearchResult').show();
    }
    else {
        $('#tbSearchResult').hide();

        $('#WFD01180Edit').prop('disabled', true);
        $('#WFD01180Delete').prop('disabled', true);
        Ctrl01180.SearchPanel.hide();
        Ctrl01180.Pagin.Clear();
    }
}

var sorting = function (PaginData) {
    Ctrl01180.Pagin.ClearPaginData();
    SetDatatable(PaginData);
}

var changePage = function (PaginData) {
    SetDatatable(PaginData);
}


var fRender = {
    CheckBoxRender: function (data) {
        var _strHtml = '';
        var _strDisable = '';
        var _strChecked = '';
        //_strHtml = '<input id="myCheck-' + '-' + data.COMPANY + '-' + data.ASSET_CLASS + '-' + data.MINOR_CATEGORY + '" style="width:20px; height:28px;"  type="checkbox" onclick="GetSelectData(this,\'' + data.COMPANY + '\',\'' + data.ASSET_CLASS + '\',\'' + data.MINOR_CATEGORY + '\');">';
        //_strHtml = '<input id="myCheck' + data.COMPANY + data.ASSET_CLASS + data.MINOR_CATEGORY + '" style="width:20px; height:28px;"  type="checkbox" onclick="GetSelectData(this,\'' + (rownNo + 1) + '\');">';
        _strHtml = '<input id="myCheck' + '-' + data.COMPANY + '-' + data.ASSET_CLASS + '-' + data.MINOR_CATEGORY + '" style="width:20px;"  type="checkbox">';
        return _strHtml;
    },
    HyperLinkRender: function (data) {
        return data.ASSET_NO;
    },
    TextBoxRender: function (data) {
        return '';
    },
    LabelRender: function (data) {
        return ''
    },
    datepickerRender: function (data) {
        return '';
    },
    ButtonRender: function (data) {
        console.log(data);

        return '';
    },
    ButtonRetireRender: function (data) {
        console.log(data);
        var _text = '';

        return '';
    },
    ButtonPhotoRender: function (data) {
        console.log(data);
        return '';
    },
    ButtonMeasureRender: function (data) {
        console.log(data);
        return '';
    },
    ButtonBoiDocumentRender: function (data) {
        return '';
    },
    ComboStoreRender: function (d, t, r) {
        console.log(r);
        var $select = $("<select></select>", {
            "id": r.ASSET_NO + "_STORE",
            "value": d,
            "class": "form-control"
        });
        $.each(ArrDisposalStore, function (k, v) {
            var $option = $("<option></option>", {
                "text": v.VALUE,
                "value": v.CODE
            });
            if (d === v.CODE) {
                $option.attr("selected", "selected")
            }
            $select.append($option);
        });

        return $select.prop("outerHTML");
    },
    ButtonInfoRender: function (data, type, row) {
        console.log(data);
        var _strHtml = '';
        return _strHtml;
    },
    DeleteButtonRender: function (data) {
        var _strHtml = '';
        return _strHtml;
    },

}