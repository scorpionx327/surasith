﻿var UMControls = {
    UMSearchResult: $('#UMSearchResult'),
    UMSearchResultTable: $('#UMSearchResultTable'),
    UMCloseButton: $('#UMClose'),
    UMPagin: $('#UMSearchResultTable').Pagin(10),
    UMDataTable: null,
    UMSearchDataRow: $('#UMSearchDataRow'),
    UMCloseModal: $('#UMCloseModal')
};

var downloadUM = function (d) {

    var fileExistedFlag = true;
    var con = {
        FULLPATH: d.attributes['1'].value
    }

    ajax_method.Post(URL_NAV_CONST.CHECK_FILE_EXISTED, con, false, function (result) {
        if (result === false) {
            fileExistedFlag = false;
        }
    }, null);

    if (fileExistedFlag) {
        location.href = '/UserManual/Download?FULLPATH=' + d.attributes['1'].value;
    }
}

var UserManualSearch = function () {
    var pagin = UMControls.UMPagin.GetPaginData();

    ajax_method.SearchPost(URL_NAV_CONST.SEARCH_USER_MANUAL, null, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 0;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable(UMControls.UMSearchResultTable)) {
                var table = UMControls.UMSearchResultTable.DataTable();
                table.destroy();
            }
            UMControls.UMDataTable = UMControls.UMSearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { "data": null }
                ],
                'columnDefs': [
                    {
                        'targets': 0
                    , 'orderable': false
                    , 'className': 'textleft'
                    , 'render': function (data, type, full, meta) {
                        return '<span onclick="downloadUM(this)" value="' + data.VALUE + '">' + '<a href="#" target="_self">' + data.REMARKS + '</a></span>';
                    }
                    }],
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false
            }).draw();

            UMControls.UMSearchDataRow.show();
        } else {
            UMControls.UMSearchDataRow.hide();
            UMControls.UMPagin.Clear();
        }
    }, null);
}