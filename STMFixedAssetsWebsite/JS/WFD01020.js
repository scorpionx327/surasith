﻿//Script Index
var Ctrl = {

    ComboCategory: $('#cbbCategory'),
    ComboSubCategory: $('#cbbSubCategory'),
  
    ButtonSearch: $('#WFD01020Search'),
    ButtonClear: $('#WFD01020Clear'),
    ButtonAdd: $('#WFD01020Add'),

    SearchTable: $('#tableSystemMaster'),
    SearchPanel: $('#SearchResult'),
    Pagin: $('#tableSystemMaster').Pagin()
    
};

var SearchSysMaster = function () {
    ClearMessageC();
    var con = {
        CODE: null,
        VALUE: null,
        REMARKS: null,
        CATEGORY: Ctrl.ComboCategory.val(),
        SUB_CATEGORY: Ctrl.ComboSubCategory.val(),
        ACTIVE_FLAG: null,
        CREATE_BY: null,
        CREATE_DATE: null,
        UPDATE_BY: null,
        UPDATE_DATE: null
    };

    var pagin = Ctrl.Pagin.GetPaginData();
    ajax_method.SearchPost(URL_CONST.SearchURL, con, pagin, true, function (datas, pagin) {
      
        if (datas != null && datas.length > 0) {
            //if (pagin.OrderColIndex == null)
            //    pagin.OrderColIndex = 1;

            //if (!pagin.OrderType)
            //    pagin.OrderType = "asc";

            if ($.fn.DataTable.isDataTable(Ctrl.SearchTable)) {
                var table = Ctrl.SearchTable.DataTable();
                table.destroy();
            }

            Ctrl.SearchTable.DataTable({
                data: datas,
                "columns": [
                  { data: "CATEGORY"  },
                  { data: "SUB_CATEGORY" },
                  { data: "CODE" },
                  { data: null },
                  { data: "ACTIVE_FLAG" },
                  { data: null },
                  { data: null }
                ],
                "columnDefs": [
                    {
                        "targets": 0,
                        "orderable": false
                    },
                    {
                        "targets": 1,
                        "orderable": false
                    },
                    {
                        "targets": 2,                                 
                        "orderable": false
                    },
                    {
                        'targets': 3,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return SetTooltipInDataTableByColumn(data.VALUE, 30);
                        }
                    },                    
                    {
                        "targets": 4,                       
                        "className": "text-center",
                        "orderable": false
                    },
                    {
                        'targets': 5,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return SetTooltipInDataTableByColumn(data.REMARKS, 30);
                        }
                    },
                    {
                        "targets": 6,
                        "searchable": false,
                        "orderable": false,
                        "className": "text-center",
                        "render": function (data, type, full, meta) {
                            var html = '';
                            if (WFD01020_Param.IsEnableButtonWFD01020Edit == "Y")
                            {
                                html = '<button class="btn btn-info" name="WFD01020Edit" type="button" onclick=\'OpenSystemMasterScreen("' +
                                            data.CATEGORY + '", "' + data.SUB_CATEGORY + '", "' + data.CODE + '")\'><i class="fa fa-edit"></i>' +
                                               URL_CONST.EditText + '</button>';
                            }
                            return html;
                             
                        }
                    }
                ],
                //"order": [pagin.OrderColIndex, pagin.OrderType],
                //"paging": false,
                'order': [],
                "searching": false,
                "paging": false,
                paging: false,
                "retrieve": true,
                "bInfo": false,
                "autoWidth": false
            });

            //var page = Ctrl.SearchTable.Pagin();
            //page.Init(pagin, sorting, changePage, "itemPerPage");
            Ctrl.Pagin.Init(pagin, sorting, changePage, 'itemPerPage');
            Ctrl.SearchPanel.show();
        } else {
            Ctrl.SearchPanel.hide();
            Ctrl.Pagin.Clear();               
        }
    }, null);
}

var sorting = function (PaginData) {
    Ctrl.Pagin.ClearPaginData();
    SearchSysMaster();
}

var changePage = function (PaginData) {
    SearchSysMaster();
}

var OpenSystemMasterScreen = function (Category, SubCategory, Code) {
    var url = URL_CONST.ManageSystem + "?CATEGORY=" + Category + "&SUB_CATEGORY=" + SubCategory + "&CODE=" + Code;
    window.open(url, "_self");
}

var LoadComboboxOnSearchScreen = function(combobox, url, data){  
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        dataType: "json",
        data: data,
        success: function (data) {           
            combobox.empty();
            combobox.val('').change();
            $.each(data.data.SelectItemOptions, function (key, value) {
                if (value.Selected) {
                    combobox.append($("<option selected ></option>").val(value.Value).html(value.Text));
                } else {
                    combobox.append($("<option></option>").val(value.Value).html(value.Text));
                }
            });            
        }
    });
}

var InitSearchSys = function () {

    Ctrl.SearchPanel.hide();

    Ctrl.ButtonSearch.click(    
        function () {
            Ctrl.Pagin.ClearPaginData();
            SearchSysMaster();
    });

    Ctrl.ButtonClear.click(
       function () {
           ClearMessageC();
           Ctrl.SearchPanel.hide();
           URL_CONST.Mode = "";
           URL_CONST.Category = "";
           URL_CONST.SubCategory = "";
           LoadComboboxOnSearchScreen(Ctrl.ComboCategory, URL_CONST.CategoryURL, null);
    });

    Ctrl.ButtonAdd.click(function () {

        ClearMessageC();

        var con = {
            CODE: null,
            VALUE: null,
            REMARKS: null,
            CATEGORY: Ctrl.ComboCategory.val(),
            SUB_CATEGORY: Ctrl.ComboSubCategory.val(),
            ACTIVE_FLAG: null,
            CREATE_BY: null,
            CREATE_DATE: null,
            UPDATE_BY: null,
            UPDATE_DATE: null
        };

        ajax_method.Post(URL_CONST.ValidateSysURL, con, true, function (result) {
            if (result) {
                OpenSystemMasterScreen(Ctrl.ComboCategory.val(),Ctrl.ComboSubCategory.val(),'');
            }
        }, null);              
    });

    Ctrl.ComboCategory.change(function () {
        LoadComboboxOnSearchScreen(Ctrl.ComboSubCategory,
                                    URL_CONST.SubCategoryURL,
                                        JSON.stringify({ data: Ctrl.ComboCategory.val() }));
    });

    LoadComboboxOnSearchScreen(Ctrl.ComboCategory, URL_CONST.CategoryURL, null);
}

//Script Manage User
var GetSysMasterItem = function () {
    return {
        CODE: $('#txtCode').val(),
        VALUE: $('#txtValue').val(),
        REMARKS: $('#txtRemark').val(),
        CATEGORY: $('#txtCategory').val(),
        SUB_CATEGORY: $('#txtSubCategory').val(),
        ACTIVE_FLAG: ($('#chkActive').is(":checked")? 'Y': 'N'),
        CREATE_BY: URL_CONST.EmployeeCode,
        CREATE_DATE: new Date(),
        UPDATE_BY: URL_CONST.EmployeeCode,
        UPDATE_DATE: new Date()
    }
}

var SaveSysMasterItem = function (url, Data) {

    ClearMessageC();
    ajax_method.Post(url, Data, false, function (result) {
        if (result) {
            ReadOnlySysMasterItem(true);
            URL_CONST.State = "UPDATE";
        }
    }, null);
}

var ReadOnlySysMasterItem = function (value) {  
    $("#txtCode").removeClass("require");
    //$("#txtValue").removeClass("require");
    $("#txtCategory").prop("readonly", value);
    $('#txtSubCategory').prop("readonly", value);
    $('#txtCode').prop("readonly", value);
    //$('#txtValue').prop("readonly", value);
    //$('#txtRemark').prop("readonly", value);
    //$('#chkActive').prop("disabled", value);
    //$('#WFD01020Save').prop("disabled", value);
    //$('#WFD01020Cancel').prop("disabled", value);
}

var InitSysMasterItem = function () {   
   
    $('#WFD01020Save').click(function () {
        var params = GetSysMasterItem();
        var url = null;
        ClearMessageC();

        if (URL_CONST.State == 'ADD')
            url = URL_CONST.InsertSystemMasterURL;

        if (URL_CONST.State == 'UPDATE')
            url = URL_CONST.UpdateSystemMasterURL;

        SaveSysMasterItem(url, params);
    });

    $('#WFD01020Cancel').click(function () {
        //EditSysMasterItem();
        var url = URL_CONST.InitURL + "?CATEGORY=" + URL_CONST.Category + "&SUB_CATEGORY=" + URL_CONST.SubCategory + "&CODE=" + URL_CONST.Code + "&MODE=BACK";
        window.open(url, "_self");
    });

    EditSysMasterItem();   
}

var EditSysMasterItem = function (Data) {

    var Data = {
        CODE: (URL_CONST.State == 'UPDATE' ? URL_CONST.Code : null),
        VALUE: null,
        REMARKS: null,
        CATEGORY: URL_CONST.Category,
        SUB_CATEGORY: URL_CONST.SubCategory,
        ACTIVE_FLAG: null,
        CREATE_BY: null,
        CREATE_DATE: null,
        UPDATE_BY: null,
        UPDATE_DATE: null
    };
    
    ajax_method.Post(URL_CONST.GetSystemMasterURL, Data, true, function (result) {    
        SetSysMasterItem(result, URL_CONST.State);
    }, null);
}

var SetSysMasterItem = function (Data, State) {

    $("#txtCategory").val(Data.CATEGORY);
    $('#txtSubCategory').val(Data.SUB_CATEGORY);
    $('#txtCode').val(Data.CODE);
    $('#txtValue').val(Data.VALUE);
    $('#txtRemark').val(Data.REMARKS);  
    $('#chkActive').prop("checked",
        (Data.ACTIVE_FLAG == 'Y' ? true : false));

    if (State== 'ADD'){
        $('#txtCode').prop("readonly", false);
    }
    if (State == 'UPDATE') {
        $("#txtCode").removeClass("require");
        $('#txtCode').prop("readonly", true);
    }
}

//Start_Init_Event
$(function () {
    
    //ComboCategory: $('#cbbCategory'),
    //ComboSubCategory: $('#cbbSubCategory'),
    Ctrl.ComboCategory.Select2Require(true)
    Ctrl.ComboSubCategory.Select2Require(true);
    if (URL_CONST.Mode == 'SEARCH' || URL_CONST.Mode == '') {
        InitSearchSys();
    }
    if (URL_CONST.Mode == 'ITEM') {
        InitSysMasterItem();
    }

    if (URL_CONST.Mode == 'BACK') {
        InitSearchSys();
    }
});