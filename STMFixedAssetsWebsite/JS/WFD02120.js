﻿//var TMPBudgets;
//var TMPRespCostCenter = [];
//var TMPCostCenter = [];
//var TMPAssetClass = [];
//var TMPCostProject;
//var TMPAssetPlateNo;
// All control in main screen
var Controls = {
    WFD02120txtCostCode: $('#WFD02120txtCostCode'),
    WFD02120txtAssetNo: $('#WFD02120txtAssetNo'),
    WFD02120txtAssetName: $('#WFD02120txtAssetName'),
    WFD02120txtAssetStatus: $('#WFD02120txtAssetStatus'),
    WFD02120txtPONo: $('#WFD02120txtPONo'),
    WFD02120txtSupplier: $('#WFD02120txtSupplier'),
    WFD02120txtAssetCategory: $('#WFD02120txtAssetCategory'),
    WFD02120chkFully: $('#WFD02120txtFully'),
    WFD02120chkOnlyParent: $('#WFD02120txtOnlyParent'),
    WFD02120txtDateInServiceFrom: $('#WFD02120txtDateInServiceFrom'),
    WFD02120txtDateInServiceTo: $('#WFD02120txtDateInServiceTo'),
    WFD02120txtSubType: $('#WFD02120txtSubType'),
    WFD02120txtAssetType: $('#WFD02120txtAssetType'),
    WFD02120txtPrintTag: $('#WFD02120txtPrintTag'),
    WFD02120txtPhotoStatus: $('#WFD02120txtPhotoStatus'),
    WFD02120txtMapStatus: $('#WFD02120txtMapStatus'),
    SearchResult: $('#WFD02120SearchResult'),
    SearchResultTable: $('#SearchResultTable'),
    SearchButton: $("#WFD02120Search"),
    ClearButton: $('#WFD02120Clear'),
    Pagin: $('#SearchResultTable').Pagin(URL_CONST.ITEM_PER_PAGE),
    DataTable: null,
    CBSelectAll: $('#selectAll'),
    SearchDataRow: $('#SearchDataRow'),
    WFD02120ExportToExcel: $('#WFD02120ExportToExcel'),
    WFD02120PrintToReport: $('#WFD02120PrintToReport'),
    WFD02120PrintTag: $('#WFD02120PrintTag'),
    PRINT_LOCATION_DIALOG: $('#change-print-location-dialog'),
    WFD02120txtPrintLocation: $('#WFD02120txtPrintLocation'),

    //---- Cost center dialog
    COMSCCSearchCostCodeModal: $('#SearchCostCenter'),
    //Get code center code 
    WFD02120GetCostCenter: $('#WFD02120GetCostCenter'),
    WFD02120_COST_CODE: $('#WFD02120_COST_CODE'),
    WFD02120_COST_NAME: $('#WFD02120_COST_NAME'),

    WFD02120MultipleBoxCompany: $('#WFD02120MultipleBoxCompany'),
    WFD02120txtAssetNo: $('#WFD02120txtAssetNo'),
    WFD02120txtAssetSub: $('#WFD02120txtAssetSub'),
    WFD02120chkOnlyParent: $('#WFD02120chkOnlyParent'),
    WFD02120txtAssetName: $('#WFD02120txtAssetName'),
    WFD02120txtAssetClass: $('#WFD02120txtAssetClass'),
    WFD02120cmbAssetGroup: $('#WFD02120cmbAssetGroup'),
    WFD02120CostCenter: $('#WFD02120txtCostCode'),
    WFD02120txtResponsibleCostCenter: $('#WFD02120txtResponsibleCostCenter'),
    WFD02120cmbLocation: $('#WFD02120cmbLocation'),
    WFD02120WBSCostProject: $('#WFD02120WBSCostProject'),
    WFD02120WBSBudget: $('#WFD02120WBSBudget'),
    WFD02120cmbBOI: $('#WFD02120cmbBOI'),
    WFD02120CapitalizeDateFrom: $('#WFD02120CapitalizeDateFrom'),
    WFD02120CapitalizeDateTo: $('#WFD02120CapitalizeDateTo'),
    WFD02120AssetPlateNo: $('#WFD02120AssetPlateNo'),
    WFD02120cmbAssetStatus: $('#WFD02120cmbAssetStatus'),
    WFD02120cmbPrintTagStatus: $('#WFD02120cmbPrintTagStatus'),
    WFD02120cmbPhotoStatus: $('#WFD02120cmbPhotoStatus'),
    WFD02120cmbMapStatus: $('#WFD02120cmbMapStatus'),
    WFD02120txtCostPending: $('#WFD02120txtCostPending')
};

var WFD02120GetModelCostCenter = function () {
    Controls.WFD02120txtCostCode.addClass('COMSCCCostCodeModal');
    Controls.WFD02120_COST_NAME.addClass('COMSCCCostNameModal');
    COMSCC_InitialParameter(); // Suphachai L. Incase Check BOI User for dialog Cost Center
    Controls.COMSCCSearchCostCodeModal.appendTo("body").modal('show');
}



// Search condition data 
var DefaultScreenData = {
    WFD02120txtCostCode: '',
    WFD02120txtAssetNo: '',
    WFD02120txtAssetName: '',
    WFD02120txtAssetStatus: '',
    WFD02120txtPONo: '',
    WFD02120txtSupplier: '',
    WFD02120txtAssetCategory: '',
    WFD02120chkFully: '',
    WFD02120chkOnlyParent: '',
    WFD02120txtDateInServiceFrom: '',
    WFD02120txtDateInServiceTo: '',
    WFD02120txtSubType: '',
    WFD02120txtAssetType: '',
    WFD02120txtPrintTag: '',
    WFD02120txtPhotoStatus: '',
    WFD02120txtMapStatus: '',

    WFD02120MultipleBoxCompany: '',
    WFD02120txtAssetNo: '',
    WFD02120txtAssetSub: '',
    WFD02120chkOnlyParent: '',
    WFD02120txtAssetName: '',
    WFD02120txtAssetClass: '',
    WFD02120cmbAssetGroup: '',
    WFD02120CostCenter: '',
    WFD02120txtResponsibleCostCenter: '',
    WFD02120cmbLocation: '',
    WFD02120WBSCostProject: '',
    WFD02120WBSBudget: '',
    WFD02120cmbBOI: '',
    WFD02120CapitalizeDateFrom: '',
    WFD02120CapitalizeDateTo: '',
    WFD02120AssetPlateNo: '',
    WFD02120cmbAssetStatus: '',
    WFD02120cmbPrintTagStatus: '',
    WFD02120cmbPhotoStatus: '',
    WFD02120cmbMapStatus: '',
    WFD02120txtCostPending: ''
};

var batchProcessLFD02180ExportExcel = BatchProcess({
    BatchId: 'LFD02180', // 'LFD02180' 
    Description: 'Export data to Excel',
    UserId: WFD02120_UserLogIn.UserLoginID

});

var batchProcessLFD02150 = BatchProcess({
    BatchId: 'LFD02150', // 'LFD02150' 
    Description: 'Asset number list with photo report',
    UserId: WFD02120_UserLogIn.UserLoginID

});

var defaultValue = {};

// Load screen event
$(function () {
    defaultValue.company = $('#WFD02120MultipleBoxCompany').val();

    $('.int-input').IntegerInput();
    initialDefault();

    Controls.SearchButton.click(function (e) {

        //$('#WFD02120txtCostCode').val(GetSearchCriteria($('#WFD02120txtCostCode')));
        //$('#WFD02120txtAssetNo').val(GetSearchCriteria($('#WFD02120txtAssetNo')));
        //$('#WFD02120txtAssetName').val(GetSearchCriteria($('#WFD02120txtAssetName')));
        //$('#WFD02120txtPONo').val(GetSearchCriteria($('#WFD02120txtPONo')));
        //$('#WFD02120txtSupplier').val(GetSearchCriteria($('#WFD02120txtSupplier')));
        PARAMETERS.SEARCH_MODE = ''; //Search Mode = manual search
        OnSearchBtnClick();
    });
    Controls.ClearButton.click(OnWFD02120ClearBtnClick);


    Controls.WFD02120PrintTag.click(PrintTag);
    Controls.WFD02120ExportToExcel.click(OnWFD02120ExportToExcelBtnClick);
    Controls.WFD02120txtPrintLocation.click(OnWFD02120txtPrintLocationBtnClick);
    Controls.WFD02120PrintToReport.click(OnWFD02120PrintToReportBtnClick);
    Controls.WFD02120GetCostCenter.click(WFD02120GetModelCostCenter);


    Controls.CBSelectAll.change(selectAllItem);
    Controls.SearchDataRow.hide();

    // Check batch process is running
    batchProcessLFD02180ExportExcel.Initial();
    batchProcessLFD02150.Initial();

    if ((WFD02120_UserLogIn.IsFAAdmin == "N") && (WFD02120_UserLogIn.IsSystemAdmin == "N")) {
        $("#WFD02120txtAssetStatus option[value='N']").remove();
        //$('#WFD02120txtAssetStatus').select2("val", "Y");
        $('#WFD02120txtAssetStatus').prop('disabled', false);
    }

    var _autosearch = false;
    if (PARAMETERS.SEARCH_MODE != '') {
        _autosearch = true;
    }
    if (PARAMETERS.PRINT_STATUS != '') {
        $('#WFD02120txtPrintTag').select2("val", PARAMETERS.PRINT_STATUS);
        _autosearch = true;
    }

    if (PARAMETERS.PHOTO_STATUS != '') {
        $('#WFD02120txtAssetStatus').select2("val", "Y");
        _autosearch = true;
    }
    if (PARAMETERS.PARENT != '') {
        console.log("PARAMETERS.PARENT");
        $('#WFD02120chkOnlyParent').prop('checked', PARAMETERS.PARENT == "P");
        if (PARAMETERS.PARENT == "P") {
            $('#WFD02120chkOnlyParent').attr("disabled", true);
        }
        else {
            $('#WFD02120chkOnlyParent').attr("disabled", false);
        }
        _autosearch = true;
    } else {
        $('#WFD02120chkOnlyParent').attr("disabled", false);
    }

    if (PARAMETERS.SUB_TYPE != '') {
        $('#WFD02120txtSubType').select2("val", PARAMETERS.SUB_TYPE.split(','));
        _autosearch = true;
    }
    else {
        var selectedValues = new Array();
    }

    if (_autosearch) {
        OnSearchBtnClick();
    }

    $('#WFD02120txtCostCode, #WFD02120txtAssetNo, #WFD02120txtAssetName, #WFD02120txtSupplier').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            Controls.SearchButton.click();
            return false;
        }
    });

    //OnSearchBtnClick();
});

// Load default condition data to screen input 
var initialDefault = function () {
    //ajax_method.Post(URL_CONST.GET_DEFAULT_SCREEN_VAL, null, true
    //    , function (result) {
    //        $('#Condition').bindJSON(result);
    //    }, null);
    console.log("initialDefault");
    //$('#WFD02120MultipleBoxCompany').select2("val", WFD02120_UserLogIn.IsCompany);

    //if (!$('#WFD02120MultipleBoxCompany').is("disabled")) {
    //    $("#WFD02120MultipleBoxCompany").multiselect('clearSelection');
    //}

    $("#WFD02120MultipleBoxCompany").multiselect('clearSelection');
    if (defaultValue.company) {
        $("#WFD02120MultipleBoxCompany").multiselect('select', defaultValue.company);
    }

    $('#WFD02120txtCostCode').val('');
    $('#WFD02120txtAssetNo').val('');
    $('#WFD02120txtAssetName').val('');
    $('#WFD02120txtPONo').val('');
    $('#WFD02120txtSupplier').val('');
    $('#WFD02120chkFully').prop('checked', false);
    $('#WFD02120chkOnlyParent').prop('checked', true);
    $('#WFD02120txtDateInServiceFrom').val('');
    $('#WFD02120txtDateInServiceTo').val('');

    $('#WFD02120txtAssetSub').val('0000').prop('readonly', true);

    $('#WFD02120txtAssetClass').val('');
    $('#WFD02120cmbAssetGroup').select2('val', '');
    $('#WFD02120txtResponsibleCostCenter').val('');
    $('#WFD02120cmbLocation').select2('val', '');
    $('#WFD02120WBSBudget').val('');
    $('#WFD02120cmbBOI').select2('val', '');
    $('#WFD02120CapitalizeDateFrom').val('');
    $('#WFD02120CapitalizeDateTo').val('');
    $('#WFD02120AssetPlateNo').val('');

    $('#WFD02120WBSCostProject').val('');

    $('#WFD02120cmbAssetStatus').select2('val', '');
    $('#WFD02120cmbPrintTagStatus').select2('val', '');
    $('#WFD02120cmbPhotoStatus').select2('val', '');
    $('#WFD02120cmbMapStatus').select2('val', '');
    $('#WFD02120txtCostPending').val('');

    enabledButtonAfterSearchResult(false);
}

// Get search condition data from screen input
var GetSearchCondition = function () {
    return {
        Company: GetMultipleCompany($('#WFD02120MultipleBoxCompany')),
        AssetNo: $('#WFD02120txtAssetNo').val(),
        AssetSub: $('#WFD02120txtAssetSub').val(),
        AssetName: $('#WFD02120txtAssetName').val(),
        AssetClass: $('#WFD02120txtAssetClass').val(),
        AssetGroup: $('#WFD02120cmbAssetGroup').val(),
        CostCenterCode: ValueCostCode(),//$('#WFD02120CostCenter').val(),
        ResponsibleCostCenter: $('#WFD02120txtResponsibleCostCenter').val(),
        Location: $('#WFD02120cmbLocation').val(),
        WBSProject: $('#WFD02120WBSCostProject').val(),
        WBSBudget: $('#WFD02120WBSBudget').val(),
        BOI: $('#WFD02120cmbBOI').val(),
        CapitalizeDateFrom: $('#WFD02120CapitalizeDateFrom').val(),
        CapitalizeDateTo: $('#WFD02120CapitalizeDateTo').val(),
        AssetPlateNo: $('#WFD02120AssetPlateNo').val(),
        AssetStatus: $('#WFD02120cmbAssetStatus').val(),
        PrintStatus: $('#WFD02120cmbPrintTagStatus').val(),
        PhotoStatus: $('#WFD02120cmbPhotoStatus').val(),
        MapStatus: $('#WFD02120cmbMapStatus').val(),
        CostPending: $('#WFD02120txtCostPending').val(),
        SearchMode: PARAMETERS.SEARCH_MODE,
    }
}

// Callbac function after click search button
var OnSearchBtnClick = function () {
    Controls.Pagin.ClearPaginData();
    ClearMessageC();
    DefaultScreenData = GetSearchCondition();
    console.log("OnSearchBtnClick");
    WFD02120Search();

    // Add event dblclick on rows
    $(document).off("dblclick").on("dblclick", "#SearchResultTable tbody tr", function () {
        var $this = $(this);
        //var $tr = $this.closest("tr");
        var row = Controls.DataTable.row($this).data();

        //var costCode = row.find('td:eq(8)').text();
        //var company = row.find('td:eq(2)').text();
        //var assetno = row.find('td:eq(5)').text();
        //var assetSub = row.find('td:eq(6)').text();

        WFD02130URL_Link(row.COMPANY, row.ASSET_NO, row.ASSET_SUB, row.COST_CODE);
    });

    //$('#SearchResultTable tbody').on('dblclick', 'tr', function () {
    //    var $this = $(this);
    //    var row = $this.closest("tr");
    //    var costCode = row.find('td:eq(2)').text();
    //    var assetno = row.find('td:eq(5)').text();
    //    WFD02130URL_Link(assetno, costCode);
    //});
}

var DELAY = 700, clicks = 0, timer = null, confirms = 0;
//Export to Excel 
var OnWFD02120ExportToExcelBtnClick = function () {
    clicks++

    if (clicks === 1) {
        timer = setTimeout(loadConfirmAlert(WFD02120_Message.ConfirmExportToExcel, function (result) {
            if (result) {
                DefaultScreenData = GetSearchCondition();
                WFD02120ExportToExcel();
            }

            clicks = 0;
        }), DELAY);
    } else {
        clearTimeout(timer);
        clicks = 0;
    }
}
var OnWFD02120PrintToReportBtnClick = function () {
    var selected = $("#SearchResultTable input:checked").size();
    var allSelected = $("#SearchResultTable thead input:checked").size();
    var confirmMessage = WFD02120_Message.ConfirmPrintToReport;
    var isAllRows = true;

    if (selected >= 1) {
        isAllRows = false;
    }

    if (!isAllRows) {
        DefaultScreenData = GetSearchCondition();
        RegisterTheSelectedToTemp();
    } else {
        loadConfirmAlert(confirmMessage, function (result) {
            if (result) {
                DefaultScreenData = GetSearchCondition();
                WFD02120PrintToReport("");
            }
        });
    }
}

var RegisterTheSelectedToTemp = function () {
    var datas = getSelectPlan();

    ajax_method.Post(URL_CONST.WFD02120RegisterTemp, datas, true, function (result) {
        WFD02120PrintToReport(result);
    }, null);
}

// Print Tag Q

var PrintTag = function () {
    //Validate printTag
    var datas = getSelectPlan();

    if (datas.length > 0) {

        //var errMsgs = [];
        //WFD02120_UserLogIn.IsFAAdmin === "N";

        //$.each(datas, function (i, value) {
        //    if ((!value.PLATE_TYPE) || (value.PLATE_TYPE === "S" && (!value.BARCODE_SIZE))) {
        //        var msg = WFD02120_Message.PrintTag_Validate;
        //        msg = msg.replace("{0}", value.ASSET_NO);
        //        msg = msg.replace("{1}", value.ASSET_SUB);
        //        errMsgs.push(msg);
        //    } 
        //});

        //if (errMsgs.length > 0) {
        //    AlertMessageC([{ Type: 3, Messages: errMsgs }]);
        //    return;
        //}

        ajax_method.Post(URL_CONST.WFD02120ValidationPrintTag, datas, false, function (result) {
            if (result) {
                loadConfirmAlert(WFD02120_Message.ConfirmPrintTag, function (r) {
                    if (r) {
                        var isHasStricker = false;
                        $.each(datas, function (i, value) {
                            if (value.PLATE_TYPE === "S") {
                                isHasStricker = true;
                                return false;
                            }
                        });

                        if (isHasStricker) {
                            ChangePrintLocation();
                            $('#WFD02120txtPrintLocation').Select2Require(true);
                        } else {
                            WFD02120SavePrintLocation();
                        }
                    }
                });
            }
        }, null);

        //      clicks++;

        //      if (clicks === 1) {
        //	timer = setTimeout(loadConfirmAlert(WFD02120_Message.ConfirmPrintTag, function (result) {
        //          if (result) {

        //              if()

        //              ChangePrintLocation();
        //              $('#WFD02120txtPrintLocation').Select2Require(true);
        //          }

        //	clicks = 0; 
        //	}), DELAY);
        //} else {
        //	clearTimeout(timer);
        //	clicks = 0;
        //}
    } else {
        AlertTextErrorMessage(WFD02120_Message.PrintTag_Error_NoSelect);
    }

}

function ChangePrintLocation() {
    Controls.PRINT_LOCATION_DIALOG.appendTo("body").modal('show');
    var Param = {
        EMP_CODE: ''//Set on controller
    }
    ajax_method.Post(URL_CONST.GetPrintLocation, Param, true, function (result) {
        if (result.PrintLocation != "") {
            Controls.WFD02120txtPrintLocation.select2('val', result.PRINT_LOCATION);
        }
    })
}

function OnWFD02120txtPrintLocationBtnClick() {
    WFD02120txtPrintLocation: $('#WFD02120txtPrintLocation').val();
    if ($('#WFD02120txtPrintLocation').val() == "") {
        $('#change-print-location-dialog').modal('hide');
        AlertTextErrorMessage(WFD02120_Message.PrintTag_Err_NoInput);

    } else {
        $('#change-print-location-dialog').modal('hide');
        WFD02120SavePrintLocation();
    }

}

var WFD02120SavePrintLocation = function () {
    var datas = getSelectPlan();
    ajax_method.Post(URL_CONST.WFD02120PrintTag, datas, true, function (result) {

        if (result != null) {
            WFD02120Search();
        }

    }, null);
}

// Callback function after click clear button
var OnWFD02120ClearBtnClick = function () {
    initialDefault();
    //controls..addClass('require')
    Controls.SearchDataRow.hide();
    Controls.Pagin.Clear();
    ClearMessageC();
    if ((WFD02120_UserLogIn.IsFAAdmin == "N") && (WFD02120_UserLogIn.IsSystemAdmin == "N")) {
        //$('#WFD02120txtAssetStatus').select2("val", "Y");
        $('#WFD02120txtAssetStatus').prop('disabled', false);
    }
}
// Callback function after click soirting column in table
var sorting = function (PaginData) {

    console.log(PaginData);
    WFD02120Search();
}

// Callback function after change pagination on table
var changePage = function (PaginData) {
    console.log('changePage');
    WFD02120Search();
}

// Search plan function
var WFD02120Search = function () {
    var pagin = Controls.Pagin.GetPaginData();
    var url = '';
    if (PARAMETERS.SEARCH_MODE != '') {
        url = URL_CONST.WFD02120SEARCHAUTO
    } else {
        url = URL_CONST.WFD02120SEARCH
    }

    ajax_method.SearchPost(url, DefaultScreenData, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {
            console.log("Record Found");
            console.log(datas.length);


            if (pagin.OrderColIndex == null) {
                pagin.OrderColIndex = 1;
            }
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }

            Controls.DataTable = Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { "data": null, 'orderable': false },
                    { "data": "ROW_NO", 'orderable': false },
                    { "data": "COMPANY" },
                    { "data": null }, //3
                    { "data": null },
                    { "data": "ASSET_NO" },
                    { "data": "ASSET_SUB" },
                    { "data": "ASSET_NAME" },
                    { "data": null }, //8 
                    { "data": null }, //9
                    { "data": null },//BOI (INVEST_REASON)
                    //{
                    //    "data": "INVEST_REASON",
                    //    'className': 'text-center',
                    //    "render": function (data, type, full, meta) {
                    //        if (data.INVEST_REASON_HINT == null)
                    //            return data;
                    //        return '<span data-toggle="tooltip" title="' + data.INVEST_REASON_HINT + '">' + data + '</span>';
                    //    }
                    //},
                    { "data": null },
                    { "data": null }, //12
                    { "data": "DATE_IN_SERVICE" },
                    { "data": "MAP_STATUS" },
                    { "data": "PRINT_STATUS" },
                    { "data": "PHOTO_STATUS" }
                ],
                'columnDefs': [
                    {
                        'targets': 0,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return '<input type="checkbox" style="margin-left:-1px;" name="key" onclick="selectRowChange()" data-key="'
                                + $('<div/>').text(data.ASSET_NO).html() + '" data-cc="' + $('<div/>').text(full.COST_CODE).html() + '">'
                                // + '<a href="' + '/WFD02130/index?COMPANY=' + $('<div/>').text(full.COMPANY).html() + '&ASSET_NO=' + $('<div/>').text(full.ASSET_NO).html() + '&ASSET_SUB=' + $('<div/>').text(full.ASSET_SUB).html() + '&COST_CODE=' + $('<div/>').text(full.COST_CODE).html() + '&MODE=EDIT' + '"id="WFD02130link' + $('<div/>').text(full.ASSET_NO).html() + '_' + $('<div/>').text(full.COST_CODE).html() + '"  target="WFD02130Detail"></a>';
                                + '<a href="' + '/WFD02130/index?COMPANY=' + $('<div/>').text(full.COMPANY).html() + '&ASSET_NO=' + $('<div/>').text(full.ASSET_NO).html() + '&ASSET_SUB=' + $('<div/>').text(full.ASSET_SUB).html() + '&COST_CODE=' + $('<div/>').text(full.COST_CODE).html() + '&MODE=EDIT' + '"id="WFD02130link' + full.COMPANY + '_' + full.ASSET_NO + '_' + full.ASSET_SUB + '_' + full.COST_CODE + '"  target="WFD02130Detail"></a>';
                            //+ '<a href="' + '@Url.Action("index","WFD02130",new {ASSET_NO="' + $('<div/>').text(full.ASSET_NO).html() + '",COST_CODE="' + $('<div/>').text(full.COST_CODE).html() + '",MODE="EDIT"})' + '" id="WFD02130link' + $('<div/>').text(full.ASSET_NO).html() + '_' + $('<div/>').text(full.COST_CODE).html() + '"  target="WFD02130Detail"></a>';
                        }
                    },
                    {
                        'targets': 3,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.ASSET_CLASS_HINT + '">' + data.ASSET_CLASS + '</span>';
                        }
                    },
                    {
                        'targets': 4,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.MINOR_CATEGORY_HINT + '">' + data.MINOR_CATEGORY + '</span>';
                        }
                    },
                    {
                        'targets': 8,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    {
                        'targets': 9,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.RESP_COST_NAME_HINT + '">' + data.RESP_COST_CODE + '</span>';
                        }
                    },
                    {
                        'targets': 10,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.INVEST_REASON_HINT + '">' + data.INVEST_REASON + '</span>';
                        }
                    },
                    {
                        'targets': 11,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.BOI_NO == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.BOI_NO_HINT + '">' + data.BOI_NO + '</span>';
                        }
                    },
                    {
                        'targets': 12,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.WBS_PROJECT == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.WBS_PROJECT_HINT + '">' + data.WBS_PROJECT + '</span>';
                        }
                    },
                    { 'targets': [1], className: 'text-right' },
                    { 'targets': [3, 4, 5, 6, 8, 9, 10, 13, 14, 15, 16], className: 'text-center' }
                    //,{ 'targets': [9], className: 'text-right' }
                ],
                //'order': [],
                //"paging": false,
                //searching: false,
                //paging: false,
                //retrieve: false,
                //"bInfo": false,
                //"autoWidth": false,
                'order': [],
                "searching": false,
                "paging": false,
                "retrieve": true,
                "bInfo": false,
                "autoWidth": false
            }).draw();

            Controls.Pagin.Init(pagin, sorting, changePage, 'CustomPage', 'WFD02120');
            Controls.SearchDataRow.show();
            enabledButtonAfterSearchResult(true);

        } else {
            Controls.SearchDataRow.hide();
            Controls.Pagin.Clear();
            enabledButtonAfterSearchResult(false);
        }
        Controls.CBSelectAll.prop('checked', false);
    }, null, true, "divFWD02120SearchResult");


}

function numberFormat(num) {
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

var selectRowChange = function () {
    var isSelect = true;
    Controls.SearchResultTable.find('tr').each(function (e) {
        var cb = $(this).find('[name="key"]');
        if (e > 0 && !cb.is(':checked')) {
            isSelect = false;
        }
    });

    Controls.CBSelectAll.prop('checked', isSelect);
}

function WFD02130URL_Link(company, assetno, assetSub, costCode) {
    //$('#WFD02130link' + assetno + '_' + costCode).attr('href', '/WFD02130/index?ASSET_NO=GTT-A01N0002&COST_CODE=APD0000Z&MODE=EDIT');
    document.getElementById('WFD02130link' + company + '_' + assetno + '_' + assetSub + '_' + costCode).click();
}

var enabledButtonAfterSearchResult = function (enabled) {
    Controls.WFD02120ExportToExcel.prop('disabled', !enabled);
    Controls.WFD02120PrintToReport.prop('disabled', !enabled);
    Controls.WFD02120PrintTag.prop('disabled', !enabled);
}

var WFD02120ExportToExcel = function () {
    //debugger;
    batchProcessLFD02180ExportExcel.Clear();
    // Seq ,Name, Value
    batchProcessLFD02180ExportExcel.Addparam(1, 'COMPANY', GetMultipleCompany($('#WFD02120MultipleBoxCompany')))
    batchProcessLFD02180ExportExcel.Addparam(2, 'ASSET_NO', $('#WFD02120txtAssetNo').val())
    batchProcessLFD02180ExportExcel.Addparam(3, 'ASSET_SUB', $('#WFD02120txtAssetSub').val())
    batchProcessLFD02180ExportExcel.Addparam(4, 'ASSET_NAME', $('#WFD02120txtAssetName').val())
    batchProcessLFD02180ExportExcel.Addparam(5, 'ASSET_CLASS', $('#WFD02120txtAssetClass').val())
    batchProcessLFD02180ExportExcel.Addparam(6, 'ASSET_GROUP', $('#WFD02120cmbAssetGroup').val())
    batchProcessLFD02180ExportExcel.Addparam(7, 'COST_CODE', ValueCostCode())
    batchProcessLFD02180ExportExcel.Addparam(8, 'RESP_COST_CODE', $('#WFD02120txtResponsibleCostCenter').val())
    batchProcessLFD02180ExportExcel.Addparam(9, 'LOCATION', $('#WFD02120cmbLocation').val())
    batchProcessLFD02180ExportExcel.Addparam(10, 'WBS_PROJECT', $('#WFD02120WBSCostProject').val())
    batchProcessLFD02180ExportExcel.Addparam(11, 'WBS_BUDGET', $('#WFD02120WBSBudget').val())
    batchProcessLFD02180ExportExcel.Addparam(12, 'BOI', $('#WFD02120cmbBOI').val())
    batchProcessLFD02180ExportExcel.Addparam(13, 'CAPITAL_DATE_FROM', $('#WFD02120CapitalizeDateFrom').val())
    batchProcessLFD02180ExportExcel.Addparam(14, 'CAPITAL_DATE_TO', $('#WFD02120CapitalizeDateTo').val())
    batchProcessLFD02180ExportExcel.Addparam(15, 'ASSET_PLATE_NO', $('#WFD02120AssetPlateNo').val())
    batchProcessLFD02180ExportExcel.Addparam(16, 'ASSET_STATUS', $('#WFD02120cmbAssetStatus').val())
    batchProcessLFD02180ExportExcel.Addparam(17, 'PRINT_STATUS', $('#WFD02120cmbPrintTagStatus').val())
    batchProcessLFD02180ExportExcel.Addparam(18, 'PHOTO_STATUS', $('#WFD02120cmbPhotoStatus').val())
    batchProcessLFD02180ExportExcel.Addparam(19, 'MAP_STATUS', $('#WFD02120cmbMapStatus').val())
    batchProcessLFD02180ExportExcel.Addparam(20, 'COST_PENDING', $('#WFD02120txtCostPending').val())
    batchProcessLFD02180ExportExcel.Addparam(21, 'IsFAAdmin', WFD02120_UserLogIn.IsFAAdmin)
    batchProcessLFD02180ExportExcel.StartBatch();

}

var WFD02120PrintToReport = function (aplId) {
    batchProcessLFD02180ExportExcel.Clear();
    batchProcessLFD02150.Clear();
    // Seq ,Name, Value
    batchProcessLFD02150.Addparam(1, 'COMPANY', GetMultipleCompany($('#WFD02120MultipleBoxCompany')))
    batchProcessLFD02150.Addparam(2, 'EMP_CODE', WFD02120_UserLogIn.UserLoginID)
    batchProcessLFD02150.Addparam(3, 'COST_CODE', ValueCostCode())
    batchProcessLFD02150.Addparam(4, 'ASSET_NO', $('#WFD02120txtAssetNo').val())
    batchProcessLFD02150.Addparam(5, 'ASSET_NAME', $('#WFD02120txtAssetName').val())
    batchProcessLFD02150.Addparam(6, 'ASSET_STATUS', $('#WFD02120cmbAssetStatus').val())
    batchProcessLFD02150.Addparam(7, 'PO_NO', DefaultScreenData.WFD02120txtPONo)
    batchProcessLFD02150.Addparam(8, 'SUPPLIER_NAME', DefaultScreenData.WFD02120txtSupplier)

    batchProcessLFD02150.Addparam(9, 'SUP_TYPE', DefaultScreenData.WFD02120txtSubType)
    batchProcessLFD02150.Addparam(10, 'ASSET_CATEGORY', DefaultScreenData.WFD02120txtAssetCategory)

    if (DefaultScreenData.WFD02120chkFully) {
        batchProcessLFD02150.Addparam(11, 'FULLY', 'Y')
    } else {
        batchProcessLFD02150.Addparam(11, 'FULLY', '')
    }
    if (DefaultScreenData.WFD02120chkOnlyParent) {
        batchProcessLFD02150.Addparam(12, 'ONLY_PARENT', 'Y')
    } else {
        batchProcessLFD02150.Addparam(12, 'ONLY_PARENT', '')
    }
    batchProcessLFD02150.Addparam(13, 'DATE_IN_SERVICE_FROM', $('#WFD02120CapitalizeDateFrom').val())
    batchProcessLFD02150.Addparam(14, 'DATE_IN_SERVICE_TO', $('#WFD02120CapitalizeDateTo').val())
    batchProcessLFD02150.Addparam(15, 'ASSET_TYPE', DefaultScreenData.WFD02120txtAssetType)
    batchProcessLFD02150.Addparam(16, 'PRINT_STATUS', $('#WFD02120cmbPrintTagStatus').val())
    batchProcessLFD02150.Addparam(17, 'PHOTO_STATUS', $('#WFD02120cmbPhotoStatus').val())
    batchProcessLFD02150.Addparam(18, 'MAP_STATUS', $('#WFD02120cmbMapStatus').val())
    batchProcessLFD02150.Addparam(19, 'IS_FA_ADMIN', WFD02120_UserLogIn.IsFAAdmin)
    batchProcessLFD02150.Addparam(20, 'IS_SYS_ADMIN', WFD02120_UserLogIn.IsSystemAdmin)
    batchProcessLFD02150.Addparam(21, 'APL_ID', aplId)

    //batchProcessLFD02150.Addparam(8, 'SUB_TYPE', DefaultScreenData.WFD02120txtSubType)
    //batchProcessLFD02150.Addparam(10, 'ASSET_CATEGORY', DefaultScreenData.WFD02120txtAssetCategory)
    //batchProcessLFD02150.Addparam(1, 'COMPANY', WFD02120_UserLogIn.UserLoginID)


    //batchProcessLFD02180ExportExcel.Addparam(19, 'IS_FA_ADMIN', WFD02120_UserLogIn.IsFAAdmin)
    //batchProcessLFD02180ExportExcel.Addparam(20, 'IS_SYS_ADMIN', WFD02120_UserLogIn.IsSystemAdmin)


    batchProcessLFD02150.StartBatch();

}

var ExportResult = function () {
    var datas = getSelectPlan();
    //batchProcess.TestDialog();

    var data = {
        data: datas,
        processName: 'Export to result'
    };

    ajax_method.Post(URL_CONST.VALIDATE_EXPORT_ORACLE, data, true, function (result) {

        if (result != null) {
            batchProcessWFD02611.StartBatch(FinishExport);
        }

    }, null);
}

var FinishExport = function () {
    showAllCheckBoxColumn();
}

var validateExport = function (datas) {
    var isValid = true;

    if (datas.length < 1) {
        isValid = false;
    } else if (datas.length > 1) {
        isValid = false;
    }

    return isValid;
}

// Check all row checkbox
var selectAllItem = function () {
    var chk = $(this).is(':checked');

    setSelectRows(chk);
}

////SearchResultTable
//$('#SearchResultTable tbody').on('dblclick', 'tr', function () {
//    $('#myModalInfo').show();

//});

var setSelectRows = function (checked) {
    Controls.SearchResultTable.find('[name="key"]').each(function (e) {
        $(this).prop('checked', checked);
    });
}

var hideProcessDataCheckboxColumn = function (data) {
    Controls.SearchResultTable.find('tr').each(function (e) {
        var d = Controls.DataTable.row(this).data();
        if (!(typeof (d) === "undefined") && d.ASSET_NO == data.ASSET_NO) {
            $(this).find('[name="key"]').hide();
        }
    });
}
var showAllCheckBoxColumn = function () {
    Controls.SearchResultTable.find('tr').each(function (e) {
        $(this).find('[name="key"]').show()
    });
}
// Get select plan data
var getSelectPlan = function () {
    var datas = [];
    Controls.SearchResultTable.find('tr').each(function (e) {
        var cb = $(this).find('[name="key"]');
        if (cb.is(':checked')) {
            datas.push(Controls.DataTable.row(this).data());
            datas[datas.length - 1].EMP_CODE = WFD02120_UserLogIn.UserLoginID;
            datas[datas.length - 1].PRINT_LOCATION = $('#WFD02120txtPrintLocation').val();
        }
    });

    return datas;
}

function formatNumber(n) {
    return n.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

var ValueCostCode = function () {
    var res = $('#WFD02120txtCostCode').val().split("-");
    if (res.length > 0) {
        return res[0];
    }
}

// Auto complete
var searchHelper = {
    assetClass: typeaheadSearchHelper(URL_CONST.GetAutoCompleteAssetClass, 'CODE', 'VALUE', true),
    plateNo: typeaheadSearchHelper(URL_CONST.GetAutoCompleteAssetPlate, 'INVEN_NO', 'INVEN_NO', false),
    wbsBudget: typeaheadSearchHelper(URL_CONST.GetAutoCompleteWBSBudget, 'WBS_CODE', 'DESCRIPTION', true),
    wbsCostProject: typeaheadSearchHelper(URL_CONST.GetAutoCompleteWBSProject, 'WBS_CODE', 'DESCRIPTION', true),
    responsibleCostCenter: typeaheadSearchHelper(URL_CONST.GetAutoCompleteRespCostCenter, 'COST_CODE', 'COST_NAME', true),
    costCenter: typeaheadSearchHelper(URL_CONST.GetAutoCompleteCostCenter, 'COST_CODE', 'COST_NAME', true)
};

$('#WFD02120txtAssetClass').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        searchHelper.assetClass.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.assetClass.updateValue
});

$('#WFD02120AssetPlateNo').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.plateNo.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    }
});

$('#WFD02120WBSBudget').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.wbsBudget.loadData({ COMPANY: GetMultipleCompany($('#WFD02120MultipleBoxCompany')), _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.wbsBudget.updateValue
});

$('#WFD02120WBSCostProject').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.wbsCostProject.loadData({ COMPANY: GetMultipleCompany($('#WFD02120MultipleBoxCompany')), _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.wbsCostProject.updateValue
});

$('#WFD02120txtResponsibleCostCenter').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.responsibleCostCenter.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.responsibleCostCenter.updateValue
});

$('#WFD02120txtCostCode').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    source: function (query, process) {
        searchHelper.costCenter.loadData({ _keyword: query }, function (options) {
            process(options);
        });
    },
    updater: searchHelper.costCenter.updateValue
});

//

$('#WFD02120chkOnlyParent').click(function () {
    if ($(this).prop('checked')) {
        $('#WFD02120txtAssetSub').val('0000').prop('readonly', true);
    } else {
        $('#WFD02120txtAssetSub').val('').prop('readonly', false);
    }
});