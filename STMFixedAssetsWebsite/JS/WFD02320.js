﻿var Controls = {
    FIND: $('#FIND'),
    COMPANY: $('#WFD02120MultipleBoxCompany'),
    SearchButton: $("#WFD02320Search"),
    WFD02320PrintAllButton: $("#WFD02320PrintAll"),
    Pagin: $('#table').Pagin(URL_CONST.ITEM_PER_PAGE),
    SearchResultTable: $('#table'),
    SearchDataRow: $('#SearchDataRow'),
    PRINT_TAQ_QUEUE_DIALOG: $('#print-tag-queue-dialog'),
    CHANGE_PRINT_DIALOG: $('#change-print-dialog'),
    TablePrintTag: $('#table_printtagque')
};
var dataPrintAll = null;
var print_mode;
var cost_center;
var resp_cost_center;
var create_by ;
var dataListOfAsset = null;
$(function () {
    //Search();
    Controls.SearchButton.click(OnSearchBtnClick);
    Controls.WFD02320PrintAllButton.click(PrintAll);
    Controls.SearchDataRow.hide();
    $('.modal ').appendTo($('body'));

    $('#FIND').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            Controls.SearchButton.click();
            return false;
        }
    });
});

var getDefaultSceenValue = function () {

    var _com = '';
    if (Controls.COMPANY.val() != null) {
        _com = (Controls.COMPANY.val() + '');
        _com = _com.replace(/,/g, '#');
    }
    return {
        COMPANY: _com,
        FIND: Controls.FIND.val(),
        EMP_CODE: GLOBAL.USER_BY
    }
}
var Search = function () {
    var pagin = Controls.Pagin.GetPaginData();
    var con = getDefaultSceenValue();
    console.log(con);
    ajax_method.SearchPost(URL_CONST.SEARCH, con, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 5;
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }

            dataPrintAll = datas //save object search
            //Controls.DataTable =
            Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                  { data: 'ROWNUMBER', className: 'text-right', 'orderable': false, },
                  { data: 'COMPANY', name: 'COMPANY' },
                  { data: 'REQUEST_BY_CODE', name: 'REQUEST_BY_CODE', className: 'text-center' },
                  { data: 'REQUEST_BY_NAME', name: 'REQUEST_BY_NAME' },
                  { data: 'COST_CENTER', name: 'COST_CENTER', className: 'text-center' },
                  { data: 'COST_CENTER_NAME', name: 'COST_CENTER_NAME' },
                  { data: null, name: 'RESP_COST_CODE' },
                  { data: 'TOTAL_PRINT_ITEM', name: 'TOTAL_PRINT_ITEM', className: 'text-right' },
                  { data: null, name: 'button', 'orderable': false }
                ],
                'columnDefs': [
                {
                    'targets': 6,
                    'searchable': false,
                    'orderable': false,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        console.log(data);
                        if (data.RESP_COST_CODE == null)
                            return '';

                        return '<span data-toggle="tooltip" title="' + data.RESP_COST_NAME + '">' + data.RESP_COST_CODE + '</span>';
                    }

                },
                 {
                     'targets': 8,
                     'searchable': false,
                     'orderable': false,
                     'className': 'text-center',
                     'render': function (data, type, full, meta) {
                         return ' <button class="btn btn-sm btn-info" '+ 
                                'data-costcenter_name="' + data.COST_CENTER + ' - ' + data.COST_CENTER_NAME +
                             '"  data-company="' + data.COMPANY +
                             '"  data-costcenter="' + data.COST_CENTER +
                             '"  data-resp_cost_code="' + data.RESP_COST_CODE +
                             '" data-code="' + data.REQUEST_BY_CODE +
                             '" data-total="' + data.TOTAL_PRINT_ITEM +
                             '"  data-createby="' + data.REQUEST_BY_CODE + ' - ' + data.REQUEST_BY_NAME +
                             '" type="button" onclick="Print(this)"><i class="fa fa-print"></i> Print Tag</button>';
                     }

                 }],
                'order': [],
                "paging": false,
                searching: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                fixedHeader: true
            });

            //Controls.DataTable.on('order.dt search.dt', function () {
            //    Controls.DataTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            //        cell.innerHTML = i + 1;
            //    });
            //}).draw();
            //var page = Controls.SearchResultTable.Pagin();
            //page.Init(pagin, sorting, changePage, 'itemPerPage');
            Controls.Pagin.Init(pagin, sorting, changePage, 'itemPerPage');
            Controls.SearchDataRow.show();
        }
        else {
            Controls.SearchDataRow.hide();
            Controls.Pagin.Clear();
            dataPrintAll = null;
        }

    }, null);

}
// Callbac function after click search button
var OnSearchBtnClick = function () {

    //$('#FIND').val(GetSearchCriteria($('#FIND')));
    Controls.Pagin.ClearPaginData();
    Search();
}

var sorting = function (PaginData) {
    
  //  console.log(PaginData);
    Search();
}
var changePage = function (PaginData) {
  //  console.log(PaginData);
    Search();
}
function Print(txt) {
    debugger
    print_mode = "tag";

    console.log($(txt).data('company'));
    console.log($(txt).data('costcenter'));
    console.log($(txt).data('resp_cost_code'));
    console.log($(txt).data('code'));

    GetTableDialog($(txt).data('company'), $(txt).data('costcenter'), $(txt).data('resp_cost_code'), $(txt).data('code'), '', 'N');
    cost_center = $(txt).data('costcenter');
    resp_cost_center = $(txt).data('resp_cost_code');
    create_by = $(txt).data('code');

    $('#WFD02320_SelectAll').attr("checked",false);

    Controls.PRINT_TAQ_QUEUE_DIALOG.modal('show');
}
var GenTableDialog = function (datas) {
    debugger

    Controls.TablePrintTag.DataTable().destroy();
    Controls.TablePrintTag.DataTable({
        data: datas,
        "columns": [
            {
                "data": null, 'className': 'text-center',
                'render': function (data, type, full, meta) {
                    if (data.PLATE_TYPE == 'P')
                        return '';

                    return '<input type="checkbox" id="chk" name="chk[]" class="chkSelect" onclick="DetailCheck(this);">';
                }, "orderable": false
            },
            {
                "data": null, 'className': 'barcode_size', 'render': function (data, type, full, meta) {
                    if (data.PLATE_TYPE == 'P')
                        return '';
                    return data.BARCODE_SIZE_DESC;
                }, 'orderable': false
            },
            { "data": "PLATE_TYPE_DESC", 'className': 'plate_type', 'orderable': false },
            {
                "data": null, 'className': 'company', 'render': function (data, type, full, meta) {
                    if (data.PLATE_TYPE == 'P')
                        return '';
                    return data.PRINT_COMPANY_LOCATION;
                }, 'orderable': false
            },
            { "data": "LOCATION", 'className': 'location', 'orderable': false },
            {
                "data": null, 'render': function (data, type, full, meta) {

                    if (data.PLATE_TYPE == 'P')
                    {
                        //ExportPlateToExcel
                        return '<a href="#" onclick="ExportPlateToExcel(' + data.ITEM_QTY + ');">Export to Excel</a>';
                    }

                    return '<input type="hidden" name="hdprint_location" value="' + data.PRINT_LOCATION + '">' +
                        '<a href="#" onclick="BindDataChangeDialog(this);" data-size="' + data.BARCODE_SIZE +
                        '" data-sizedesc="' + data.BARCODE_SIZE_DESC +
                        '" data-company="' + data.PRINT_COMPANY_LOCATION +
                        '" data-location="' + data.PRINT_LOCATION +
                        '" data-id="' + data.BARCODE_SIZE + meta.row +
                        '" id="' + data.BARCODE_SIZE + meta.row + '">' + data.PRINTER_NAME + '</a>';
                }, 'orderable': false
            },
            { "data": "ITEM_QTY", 'className': 'text-right itemqty', 'orderable': false }
        ],
        'order': [],
        "paging": false,
        "searching": false,
        "retrieve": true,
        "bInfo": false,
        "autoWidth": false,
        fixedHeader: true
    });
}

var GetTableDialog = function (company, costcode, resp_cost_code, createby, find, printall) {
    debugger
    var Param = {
        company: company,
        costcode: costcode,
        resp_cost_code:resp_cost_code,
        createby:createby,
        find: find,
        printall: printall
    }
    ajax_method.Post(URL_CONST.PRINT_DIALOG, Param, true, function (result) {
        debugger
        if (result == null) {
            return ;
        }
        dataListOfAsset = [];
        if (result.printQDialog.length > 0)
        {
            GenTableDialog(result.printQDialog);
        }
        if (result.printAllCount !=null)
        {
            $('#resp_cost_center_d').html(result.printAllCount.RESP_COST_COUNT);
            $('#cost_center_d').html(result.printAllCount.COST_COUNT);
            $('#request_d').html(result.printAllCount.CREATEDBY_COUNT);
            $('#total_d').html(result.printAllCount.ITEM_COUNT);
        }
        if (result.AssetList.length> 0) {
            $.each(result.AssetList, function (i) {
                if (result.AssetList[i].PLAT_TYPE == "P")
                    return;

                dataListOfAsset.push(result.AssetList[i]);
            });
        }  
        console.log(dataListOfAsset);
    }, null);
}

function PrintAll() {
    print_mode = "all";

    var _s = getDefaultSceenValue();

    GetTableDialog(_s.COMPANY, '', '', '', _s.FIND, 'Y');
    $('#WFD02320_SelectAll').attr("checked", false);
    Controls.PRINT_TAQ_QUEUE_DIALOG.modal('show');
}

var selectedBarCodesize = '',
    selectedLocation = '',
    selectedPrinter = '';

function BindDataChangeDialog(txt) {
    console.log(txt);
    $('#printsize_d').html($(txt).data('sizedesc'));
   
    $('#printer_change').html('');
    $('#asset').val($(txt).data('id'));

    selectedBarCodesize = $(txt).data('size');
    selectedLocation = $(txt).data('location');
    selectedPrinter = $(txt).text();
    console.log("--------------");
    console.log(selectedBarCodesize);
    console.log(selectedLocation);
    console.log(selectedPrinter);


    $('#ddpWFD02320PrinterCompany').select2("val", $(txt).data('company'));
    $('#printer_change').select2('val', '');
    $('#ddpWFD02320PrinterLocation').select2('val','');


    Controls.CHANGE_PRINT_DIALOG.modal('show');
}
function Save() {
    debugger
    if ($('#printer_change').val() == null || $('#printer_change').val() == "") {
        fnErrorDialog("Error", Message.RECORDISNOTSELECTED);
        return;
    }
    
    if ($('#printer_change').val() == selectedPrinter) {
        //select a printer ;
        $('#' + $('#asset').val()).html($('#printer_change').val());
        $('#change-print-dialog').modal('hide');
        return;
    }
  
    var _msg = Message.ConfirmChangePrinter.replace('{0}', selectedPrinter).replace('{1}', $('#printer_change').val());
    loadConfirmAlert(_msg, function (result) {
        if (!result) {
            return;
        }
        $('#' + $('#asset').val()).html($('#printer_change').val());
        $('#change-print-dialog').modal('hide');
    });

    
}
function PrintTag() {
    debugger
    var param = [];

    _assetCnt = 0;
    if (print_mode == "tag") {
        $('#table_printtagque').find('tbody').find('tr').each(function (e) {
            var col1 = $(this).find('input[type="checkbox"]').is(':checked');
            var col2 = $(this).find("td:eq(1)").text(); // Barcode size
            var col3 = $(this).find("td:eq(3)").text(); // Company
            var col4 = $(this).find('input[name="hdprint_location"]').val(); // Location
            var col5 = $(this).find("td:eq(5)").text(); // Printer Name
            var col6 = $(this).find("td:eq(6)").text(); // Qty
            if (col1) {
                param.push({
                    'COST_CENTER': cost_center,
                    'RESP_COST_CODE': resp_cost_center,
                    'COMPANY': col3,
                    'BARCODE_SIZE': col2,
                    'PRINT_LOCATION': col4,
                    'PRINTER_NAME': col5
                });
                _assetCnt += parseInt(col6);
            }

        });
    } else if (print_mode == "all") {
    
        $('#table_printtagque').find('tbody').find('tr').each(function (e) {
            var col1 = $(this).find('input[type="checkbox"]').is(':checked');
            var col2 = $(this).find("td:eq(1)").text(); // Barcode size
            var col3 = $(this).find("td:eq(3)").text(); // Company
            var col4 = $(this).find('input[name="hdprint_location"]').val(); // Location
            var col5 = $(this).find("td:eq(5)").text(); // Printer Name
            var col6 = $(this).find("td:eq(6)").text(); // Qty
            if (col1) {
                param.push({
                    'COST_CENTER': '',
                    'RESP_COST_CODE': '',
                    'COMPANY': col3,
                    'BARCODE_SIZE': col2,
                    'PRINT_LOCATION': col4,
                    'PRINTER_NAME': col5
                });
                _assetCnt += parseInt(col6);
            }

        });
      
    }
    var data = {
        SelectedList : param,
        AssetList: dataListOfAsset
    }
    console.log(data);
    if (data.SelectedList.length == 0) {
        fnErrorDialog("Error", Message.RECORDISNOTSELECTED);
        return;
    }
    var _msg = Message.ConfirmPrint.replace('{0}', _assetCnt);
    loadConfirmAlert(_msg, function (result) {
        if (!result) {
            return;
        }

        ajax_method.Post(URL_CONST.PRINT, data, true, function (result) {
            $('#print-tag-queue-dialog').modal('hide');

            if (result != null) {
                //console.log(result)

                var msg = result.msg;

                setTimeout(function () {
                    AletTextInfoMessage(msg);
                }, 2000);
                Search();
            }
        }, null);

    });

    
    
}

$('#ddpWFD02320PrinterCompany').change(function () {
    //Load Printer Location
    
    ajax_method.Post('/WFD02320/GetPrinterLocation', { COMPANY: $(this).val() }, true, function (result) {
        $('#ddpWFD02320PrinterLocation').html('');
        $('#ddpWFD02320PrinterLocation').select2("val", "");
        $('#printer_change').select2("val", "");
        if (result == null || result.length == 0) {
            return;
        }
        $.each(result, function () {
            console.log(this);
            $('#ddpWFD02320PrinterLocation').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.VALUE));
        });
        if (result.length == 1) {
            $('#ddpWFD02320PrinterLocation').select2("val", result[0].VALUE);
            return;
        }
        $('#ddpWFD02320PrinterLocation').select2("val", selectedLocation);

    });

})
$('#ddpWFD02320PrinterLocation').change(function () {
    //Load Printer Location
    $('#printer_change').html('');
    $('#printer_change').select2("val", "");;
    
    ajax_method.Post('/WFD02320/GetPrinterName', { PRINTER_LOCATION: $(this).val(), BARCODE_SIZE: selectedBarCodesize }, true, function (result) {
        if (result == null || result.length == 0) {
            return;
        }

        console.log("ddpWFD02320PrinterLocation");
        console.log(selectedPrinter);

        $.each(result, function () {
            $('#printer_change').append($("<option />")
                .attr('data-description', this.VALUE)
                .val(this.VALUE)
                .text(this.VALUE));
        });
        if (result.length == 1) {
            $('#printer_change').select2("val", result[0].VALUE);
            return;
        }
        $('#printer_change').select2("val",selectedPrinter);
    });

})
$('#WFD02320_SelectAll').click(function (e) {
    $('.chkSelect').prop('checked', $(this).is(':checked'));
})
function DetailCheck(sender) {
    var row = $(sender).closest("tr");
       
    $('#WFD02320_SelectAll').prop('checked', _act = $('.chkSelect:checked').length == $('.chkSelect').length);
}
function ExportPlateToExcel(_cnt) {
    loadConfirmAlert(Message.ConfirmExportToExcel.replace('{0}', _cnt), function (result) {
        if (!result) {
            return;
        }

        Controls.PRINT_TAQ_QUEUE_DIALOG.modal('hide');

        var _data = getDefaultSceenValue();
        //shoud include batch.js
        var batch = BatchProcess({
            BatchId: "LFD021C0",
            Description: "Export Asset No to Excel",
            UserId: GLOBAL.USER_BY
        });
        //batch.Addparam(1, 'PRINT_FLAG', (print_mode == "all" ? "Y" : "N")); //Y : All, N : Selected
        //batch.Addparam(2, 'COMPANY', _data.COMPANY);
        //batch.Addparam(3, 'FIND', _data.FIND);
        //batch.Addparam(4, 'REQUEST_BY', create_by);
        //batch.Addparam(5, 'COST_CODE', cost_center);
        //batch.Addparam(6, 'RESP_COST_CODE', resp_cost_center);

        batch.Addparam(1, 'COMPANY', _data.COMPANY);
        batch.Addparam(2, 'COST_CODE', cost_center);

        //batch.StartBatch();
        batch.StartBatch(function (_appID, _Status) {
            if (_Status != 'S') {
                return;
            }

            OnSearchBtnClick();
        });

    });
}