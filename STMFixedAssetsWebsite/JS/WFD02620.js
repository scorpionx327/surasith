﻿//WFD02620btnSave
var IsFAAdmin_WFD02620 = false;
var IsAfterSave = true;

var IsUpdateDataFromUploadFile = false;
var WFD02620_Param =
{
    PLAN_STATUS: ''
};

// Setting section's control
var SettingSectionControls_WFD02620 = {
    BLOCK: $('#WFD02620SettingBlock'),
    CREATE_BY: $('#WFD02620CREATE_BY'),
    COMPANY: $('#WFD02620COMPANY'),
    YEAR: $('#WFD02620YEAR'),
    ROUND: $('#WFD02620ROUND'),
    ASSET_LOCATION: $('#WFD02620ASSET_LOCATION'),
    QTY_OF_HANDHELD: $('#WFD02620QTY_OF_HANDHELD'),
    //SUB_TYPE: $('#WFD02620SUB_TYPE'),
    TARGET_DATE_FROM: $('#WFD02620TARGET_DATE_FROM'),
    TARGET_DATE_TO: $('#WFD02620TARGET_DATE_TO'),
    DATA_AS_OF: $('#WFD02620DATA_AS_OF'),
    BREAK_TIME_MINUTE: $('#WFD02620BREAK_TIME_MINUTE'),
    ASSET_STATUS: $('#WFD02620ASSET_STATUS'),

       
    ASSET_CLASS: $('#WFD02620ASSET_CLASS'),
    MINOR_CATEGORY: $('#WFD02620MINOR_CATEGORY'),
    COST_CENTER: $('#WFD02620COST_CENTER'),
    RESPONSIBLE_COST_CENTER: $('#WFD02620RESPONSIBLE_COST_CENTER'),
    BOI: $('#WFD02620BOI'),
    INVENTORY_CHECK: $('#WFD02620INVENTORY_CHECK')
    
};

var ButtonControls_WFD02620 = {
    SettingSave: $('#WFD02620btnSave'),
    SettingCancel: $('#WFD02620btnCancel'),
    ResetAssignment: $('#WFD02620btnResetAssignment'),
    Edit: $('#WFD02620btnEdit'),
    EditButtonGroup: $('.edit-btn-group'),
    AssignButtonGroup: $('.assign-btn-group'),
    BottomButtonGroup: $('.bottom-btn-group'),
    AnnounceButton: $('#WFD02620AnnounceButton'),
    CompleteButton: $('#WFD02620CompleteButton'),
    Print: $('#WFD02620Print')
};

// Asset file upload's controls
var FileUplaodControls = {
    Textbox: $('#WFD02620FileUploadTB'),
    File: $('#WFD02620FileUploadFile'),
    Button: $('#WFD02620FileUploadButton')
};

var WFD02620_ScreenClassModeControls = {
    PrimaryInput: $('.wfd02620-primary-input'),
    Input: $('.wfd02620-input'),
    InputDTButton: $('.wfd02620-input-dtbutton'),
    Add: $('.wfd02620-addmode'),
    Edit: $('.wfd02620-editmode')
};

var batchProcessWFD02620 = BatchProcess({
    UserId: WFD02620_User.EMP_CODE,
    BatchId: 'BFD0261A',
    IsDownload: 'N'
});

// Require fields default   
var requiredMainControl_WFD0262 = [SettingSectionControls_WFD02620.COMPANY,SettingSectionControls_WFD02620.YEAR, SettingSectionControls_WFD02620.ROUND, SettingSectionControls_WFD02620.ASSET_LOCATION
    , SettingSectionControls_WFD02620.QTY_OF_HANDHELD, SettingSectionControls_WFD02620.TARGET_DATE_FROM, SettingSectionControls_WFD02620.TARGET_DATE_TO
    , SettingSectionControls_WFD02620.BREAK_TIME_MINUTE];

// Required fields if there is no asset file upload
var requireWhenNoUploadFile = [SettingSectionControls_WFD02620.MINOR_CATEGORY, SettingSectionControls_WFD02620.DATA_AS_OF, SettingSectionControls_WFD02620.ASSET_STATUS, SettingSectionControls_WFD02620.ASSET_LOCATION];

var AssignmentSectionControls_WFD0262 = {
    SummaryBlock: $('#summaryBlock')

};

var SCREEN_MODES_WFD0262 = {
    ADD: 'ADD',
    EDIT: 'EDIT',
    VIEW: 'VIEW'
};




$(function () {
    if (WFD02620_User.IS_FA_ADMIN == "1") WFD02620_User.IS_FA_ADMIN = true;
    else WFD02620_User.IS_FA_ADMIN = false;
    // WFD02620_User from index.cshtml
    if (WFD02620_User.IS_FA_ADMIN) IsFAAdmin_WFD02620 = true;
    else IsFAAdmin_WFD02620 = false;

    $('.int-input').IntegerInput();
    $(".select2").select2({ width: '100%' });

    setControlsRequiredField(requiredMainControl_WFD0262, true);
    setControlsRequiredField(requireWhenNoUploadFile, FileUplaodControls.File.val() == '');
    
    
    bindingAssetFileUploadControlEvents_WFD02620();// Binding Asset upload file's  controls event

    // Button event
    FileUplaodControls.Button.click(onWFD02620FileUploadButtonClick);
    ButtonControls_WFD02620.SettingSave.click(onWFD02620btnSaveClick);
    ButtonControls_WFD02620.SettingCancel.click(onWFD02620BtnCancelClick);

    // Variabled from file WFD02620_Assignment.js
    WFD02620_AssignmentControls.SaveAssignmentButton.click(saveAssignment_WFD02620);
    WFD02620_AssignmentControls.CancelAssignmentButton.click(InitialEditMode_WFD02620);

    ButtonControls_WFD02620.ResetAssignment.click(ResetAssignment_WFD02620);
    ButtonControls_WFD02620.Edit.click(EditAssignmentHeader_WFD02620);

    ButtonControls_WFD02620.AnnounceButton.click(onAnnounceButtonClick);
    ButtonControls_WFD02620.CompleteButton.click(onCompleteButtonClick);

    ButtonControls_WFD02620.Print.click(onPrintClick);




    // Screen mode
    InitialScreeModelWFD02620();

    // Batch
    InitialBatch();

    FileUplaodControls.File.change(function () {
        var fileExtension = 'XLSX';

        //var ext = this.value.match(/\.(.+)$/)[1]; not support folder include .
        var ext = this.value.substr((this.value.lastIndexOf('.') + 1));

        var file = FileUplaodControls.File[0].files[0];
        var arr = fileExtension.split(",");

        if (!CheckExtension(ext, arr, file.name, fileExtension)) {
            this.value = '';
        } 
    });

  
    $("#WFD02620COMPANY").change(function () {
        WFD02620GetMinorCategory();
    });
   // $("#WFD02620COMPANY").select2("val", "STM");

});



//#region   Batch function

var InitialBatch = function () {
    batchProcessWFD02620.Initial();
};

var uploadFile = function (isUpdate) {

    var fileUpload = $("#WFD02620FileUploadFile").get(0);
    var files = fileUpload.files;

    // Create FormData object  
    var fileData = new FormData();

    // Looping over all files and add it to FormData object  
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }

    var data = {
        data: WFD02620_GetSettingSectionData(),
        uploadFileName: files[0].name,
        isUpdate: isUpdate
    };

    ajax_method.Post(WFD02620_URL.GenBatchParam, data, true, function (res) {

        if (res != null) {

            ScreenKey_WFD02620 = {
                Conpany: res.COMPANY,
                Year: res.YEAR,
                Round: res.ROUND,
                AssetLocation: res.ASSET_LOCATION
            };

            ajax_method.PostFile(WFD02620_URL.UploadAssetListFile, fileData, function (result) {

                if (result.ObjectResult != null) {

                    res.IS_UPDATE = isUpdate;
                    res.UPLOAD_FILE_NAME = result.ObjectResult;

                    StartBatch(res);

                }

            }, null);

        }

    }, null);
};

var StartBatch = function (batchParam) {

    batchProcessWFD02620.Clear();
    batchProcessWFD02620.Addparam(1, 'UPLOAD_FILE_NAME', batchParam.UPLOAD_FILE_NAME);
    batchProcessWFD02620.Addparam(2, 'IS_UPDATE', batchParam.IS_UPDATE);
    batchProcessWFD02620.Addparam(3, 'UPDATE_BY', batchParam.UPDATE_BY);
    batchProcessWFD02620.Addparam(4, 'COMPANY', batchParam.COMPANY);
    batchProcessWFD02620.Addparam(5, 'YEAR', batchParam.YEAR);
    batchProcessWFD02620.Addparam(6, 'ROUND', batchParam.ROUND);
    batchProcessWFD02620.Addparam(7, 'ASSET_LOCATION', batchParam.ASSET_LOCATION);
    batchProcessWFD02620.Addparam(8, 'QTY_OF_HANDHELD', batchParam.QTY_OF_HANDHELD);
    batchProcessWFD02620.Addparam(9, 'TARGET_DATE_FROM', batchParam.TARGET_DATE_FROM);
    batchProcessWFD02620.Addparam(10, 'TARGET_DATE_TO', batchParam.TARGET_DATE_TO);
    batchProcessWFD02620.Addparam(11, 'BREAK_TIME_MINUTE', batchParam.BREAK_TIME_MINUTE);

    batchProcessWFD02620.StartBatch(finishUploadProcess);
};

var finishUploadProcess = function (AppId, status) {

    if (status == 'S') {

        IsUpdateDataFromUploadFile = true;
        SCREEN_PARAMS.SCREEN_MODE = SCREEN_MODES_WFD0262.EDIT; // SCREEN_PARAMS from index.cshtml
        InitialScreeModelWFD02620();

    }

};

//#endregion

//#region   Print

var onPrintClick = function () {
    var divToPrint = document.getElementById("TBCalendarBlock");

    var str = '<html><head><link rel="stylesheet" type="text/css" href="../InitializeComponents/Content/Components/bootstrap/css/bootstrap.min.css" />'
        + '<script src="../InitializeComponents/Content/Components/plugins/jQuery/jquery.min.js"></script>'
        + '<style type="text/css" media="print">'
        + '     @page { size: landscape; } '
        + '</style>'
        + '<link rel="stylesheet" type="text/css" href="../InitializeComponents/Content/style-content/site-bootstrap.css" />'
        + '<link rel="stylesheet" type="text/css" href="../InitializeComponents/Content/style-content/table.css" />'
        + '<link rel="stylesheet" type="text/css" href="../InitializeComponents/Content/style-content/Page/WFD02620.css" />'
        + '<script> $(function(){ setTimeout(function () { window.print(); }, 500); }); </script>'
        + '</head><body>'
        + divToPrint.outerHTML + '</body></html>';

    var windowWidth = 1100;
    var windowHeight = 600;
    var myleft = (screen.width) ? (screen.width - windowWidth) * 0.5 : 100;
    var mytop = (screen.height) ? (screen.height - windowHeight) * 0.5 : 100;
    var feature = 'left=' + myleft + ',top=' + eval(mytop - 50) + ',width=' + windowWidth + ',height=' + windowHeight + ',';
    feature += 'menubar=yes,status=no,location=no,toolbar=no,scrollbars=yes';

    var wnd = window.open("about:blank", "", feature);
    wnd.document.write(str);
    wnd.document.close();
};



//#endregion

//#region   Save
var DELAY = 700, clicks = 0, timer = null;
var onWFD02620btnSaveClick = function () {
    clicks++;

    if (clicks === 1) {
        timer = setTimeout(loadConfirmAlert(WFD02620_Message.ConfirmSaveHeader, function (res) {
            if (res) {
                if (SCREEN_PARAMS.SCREEN_MODE == SCREEN_MODES_WFD0262.ADD) { // SCREEN_PARAMS from index.cshtml
                    WFD02620_AddSave();
                } else {
                    WFD02620_EditSave();
                }
            }
            clicks = 0;
        }), DELAY);
    } else {
        clearTimeout(timer);
        clicks = 0;
    }
};

var WFD02620_AddSave = function () {

    if (FileUplaodControls.File.val() != '') {
        uploadFile('N');
    } else {
        WFD02620_AddProcess();
    }

};
var WFD02620_AddProcess = function () {

    var dataB = WFD02620_GetSettingSectionData();
    var data = ajax_method.ConvertDateBeforePostBack(dataB);

    ajax_method.Post(WFD02620_URL.AddStockTakeRequest, data, true, function (result) {

        if (result != null) {
            ScreenKey_WFD02620.Company = result.Company;
            ScreenKey_WFD02620.Year = result.Year;
            ScreenKey_WFD02620.Round = result.Round;
            ScreenKey_WFD02620.AssetLocation = result.AssetLocation;

            SCREEN_PARAMS.SCREEN_MODE = SCREEN_MODES_WFD0262.EDIT; // SCREEN_PARAMS from index.cshtml
            InitialScreeModelWFD02620();
        }

    }, null, true, 'WFD02620SettingBlock');

};
var WFD02620_GetSettingSectionData = function () {
    var targetDateFrom = null;
    var targetDateTo = null;
    var dataAsOf = null;
    if (SettingSectionControls_WFD02620.TARGET_DATE_FROM.datepicker('getDate') != null) {
        targetDateFrom = SettingSectionControls_WFD02620.TARGET_DATE_FROM.datepicker('getDate').toUTCString();
    }
    if (SettingSectionControls_WFD02620.TARGET_DATE_TO.datepicker('getDate') != null) {
        targetDateTo = SettingSectionControls_WFD02620.TARGET_DATE_TO.datepicker('getDate').toUTCString();
    }
    if (SettingSectionControls_WFD02620.DATA_AS_OF.datepicker('getDate') != null) {
        dataAsOf = SettingSectionControls_WFD02620.DATA_AS_OF.datepicker('getDate').toUTCString();
    }

    var str = SettingSectionControls_WFD02620.ROUND.val();
    var pad = "00";
    var _round = pad.substring(0, pad.length - str.length) + str;

    return {
        CREATE_BY: WFD02620_User.EMP_CODE, // WFD02620_User from index.cshtml
        EMP_NAME: SettingSectionControls_WFD02620.CREATE_BY.val(),
        STOCK_TAKE_KEY: '',
        EMP_LASTNAME: '',
        COMPANY: SettingSectionControls_WFD02620.COMPANY.val(),
        YEAR: SettingSectionControls_WFD02620.YEAR.val(),
        ROUND: _round,
        ASSET_LOCATION: SettingSectionControls_WFD02620.ASSET_LOCATION.val(),
        QTY_OF_HANDHELD: SettingSectionControls_WFD02620.QTY_OF_HANDHELD.val(),
        //SUB_TYPE: WFD02620_ConvertListToString(SettingSectionControls_WFD02620.SUB_TYPE.val()),
        TARGET_DATE_FROM: targetDateFrom,
        TARGET_DATE_TO: targetDateTo,
        DATA_AS_OF: dataAsOf,
        BREAK_TIME_MINUTE: SettingSectionControls_WFD02620.BREAK_TIME_MINUTE.val(),
        ASSET_STATUS: SettingSectionControls_WFD02620.ASSET_STATUS.val(),
        TARGET_DATE_FROM_STR: SettingSectionControls_WFD02620.TARGET_DATE_FROM.val(),
        TARGET_DATE_TO_STR: SettingSectionControls_WFD02620.TARGET_DATE_TO.val(),
        DATA_AS_OF_STR: SettingSectionControls_WFD02620.DATA_AS_OF.val(),

        ASSET_CLASS: getMultiControlValue($('#WFD02620ASSET_CLASS')),
        MINOR_CATEGORY: getMultiControlValue($('#WFD02620MINOR_CATEGORY')),
        COST_CENTER: getMultiControlValue($('#WFD02620COST_CENTER')),
        RESPONSIBLE_COST_CENTER: getMultiControlValue($('#WFD02620RESPONSIBLE_COST_CENTER')),
        BOI: SettingSectionControls_WFD02620.BOI.val(),
        INVENTORY_CHECK: SettingSectionControls_WFD02620.INVENTORY_CHECK.val(),
    };
};

var WFD02620_EditSave = function () {
    if (FileUplaodControls.File.val() != '') {
        uploadFile('Y');
    } else {
        WFD02620_EditSaveProcess();
    }
};
var WFD02620_EditSaveProcess = function () {
    var data = WFD02620_GetSettingSectionData();
    data.STOCK_TAKE_KEY = WFD02620_Assignment_Models.HData.STOCK_TAKE_KEY;
    data.UPDATE_DATE = WFD02620_Assignment_Models.HData.UPDATE_DATE;

    var sData = ajax_method.ConvertDateBeforePostBack(data);

    ajax_method.Post(WFD02620_URL.EditStockTakeRequest, sData, true, function (res) {

        if (res != null) {
            ScreenKey_WFD02620 = {
                Company: res.Company,
                Year: res.Year,
                Round: res.Round,
                AssetLocation: res.AssetLocation
            };
            IsAfterSave = false;
            InitialScreeModelWFD02620();
        }

    }, null, true, 'WFD02620SettingBlock');
};

var saveAssignment_WFD02620 = function () {
    clicks++;

    if (clicks === 1) {
        timer = setTimeout(loadConfirmAlert(WFD02620_Message.ConfirmSaveAssignment, function (res) {
            if (res) {
                console.log("Confirm");
                var data = {
                    HData: WFD02620_Assignment_Models.HData,
                    sv: JSON.parse(JSON.stringify(WFD02620_Assignment_Models.SVDatas)),
                    holidays: JSON.parse(JSON.stringify(WFD02620_Assignment_Models.Holidays))
                };

                ajax_method.Post(WFD02620_URL.SaveAssigmentDatas, data, true, function (res) {
                    // console.log("Save");
                    // console.log(res);
                    if (res == true) {
                        var data = {
                            COMPANY: WFD02620_Assignment_Models.HData.COMPANY,
                            YEAR: WFD02620_Assignment_Models.HData.YEAR,
                            ROUND: WFD02620_Assignment_Models.HData.ROUND,
                            ASSET_LOCATION: WFD02620_Assignment_Models.HData.ASSET_LOCATION
                        };
                        ajax_method.Post(WFD02620_URL.GetSVAssignment, data, true, function (res) {

                            if (res != null) {
                                console.log("Reload successfully");
                                // Objective : Download Update Date
                                WFD02620_Assignment_Models.SVDatas = ConvertSVDatas_WFD02620_Assignment(res);
                                setAssignmentScreenUI(); //No need to set again 

                                editModeButtonInitial(WFD02620_Assignment_Models.SVDatas);
                            }

                        }, null);


                    }

                }, null);

            }

            clicks = 0;
        }), DELAY);
    } else {
        clearTimeout(timer);
        clicks = 0;
    }
};

//#endregion

//#region   Announce / Complete

var onAnnounceButtonClick = function () {
    clicks++;

    if (clicks === 1) {
        timer = setTimeout(loadConfirmAlert(WFD02620_Message.ConfirmAnnounce, function (res) {
            if (res) {

                var data = {
                    STOCK_TAKE_KEY: WFD02620_Assignment_Models.HData.STOCK_TAKE_KEY,
                    COMPANY: WFD02620_Assignment_Models.HData.COMPANY,
                    YEAR: WFD02620_Assignment_Models.HData.YEAR,
                    ROUND: WFD02620_Assignment_Models.HData.ROUND,
                    ASSET_LOCATION: WFD02620_Assignment_Models.HData.ASSET_LOCATION
                };

                ajax_method.Post(WFD02620_URL.Announce, data, true, function (res) {

                }, null);

            }

            clicks = 0;
        }), DELAY);
    } else {
        clearTimeout(timer);
        clicks = 0;
    }
};


var onCompleteButtonClick = function () {
    clicks++;

    if (clicks === 1) {
        timer = setTimeout(loadConfirmAlert(WFD02620_Message.ConfirmComplete, function (res) {
            if (res) {

                var data = {
                    STOCK_TAKE_KEY: WFD02620_Assignment_Models.HData.STOCK_TAKE_KEY,
                    COMPANY: WFD02620_Assignment_Models.HData.COMPANY,
                    YEAR: WFD02620_Assignment_Models.HData.YEAR,
                    ROUND: WFD02620_Assignment_Models.HData.ROUND,
                    ASSET_LOCATION: WFD02620_Assignment_Models.HData.ASSET_LOCATION
                };

                ajax_method.Post(WFD02620_URL.Completed, data, true, function (res) {

                    if (res == true) {
                        //viewModeButtonInitial();
                    }

                }, null);

            }

            clicks = 0;
        }), DELAY);
    } else {
        clearTimeout(timer);
        clicks = 0;
    }

};

//#endregion

//#region   Reset assignment / Edit button click

var ResetAssignment_WFD02620 = function () {

    loadConfirmAlert(WFD02620_Message.ConfirmResetAssignment, function (res) {
        if (res) {

            var data = {
                COMPANY: WFD02620_Assignment_Models.HData.COMPANY,
                YEAR: WFD02620_Assignment_Models.HData.YEAR,
                ROUND: WFD02620_Assignment_Models.HData.ROUND,
                ASSET_LOCATION: WFD02620_Assignment_Models.HData.ASSET_LOCATION
            };
            ajax_method.Post(WFD02620_URL.ResetAssignment, data, true, function (res) {

                if (res != null) {
                    WFD02620_Param.PLAN_STATUS = 'D';
                    // function in WFD02620_Assignment.js
                    WFD02620_Assignment_Models.SVDatas = ConvertSVDatas_WFD02620_Assignment(res);
                    setAssignmentScreenUI();

                    editModeButtonInitial(WFD02620_Assignment_Models.SVDatas);
                }

            }, null);

        }

    });
};

var EditAssignmentHeader_WFD02620 = function () {

    WFD02620_ScreenClassModeControls.Edit.hide();
    ButtonControls_WFD02620.EditButtonGroup.show();

    WFD02620_ScreenClassModeControls.Input.prop('disabled', false);
    WFD02620_ScreenClassModeControls.InputDTButton.prop('disabled', false);
    WFD02620_ScreenClassModeControls.PrimaryInput.prop('disabled', true);

    setControlsRequiredField(requiredMainControl_WFD0262, true);
    setControlsRequiredField(requireWhenNoUploadFile, FileUplaodControls.File.val() == '');
    WFD02620_ScreenClassModeControls.PrimaryInput.removeClass('require');
};

var onWFD02620BtnCancelClick = function () {

    InitialScreeModelWFD02620();
    ClearMessageC();
};

//#endregion

//#region   Initial required field

var setControlsRequiredField = function (controls, isRequired) {
    for (var i = 0; i < controls.length; i++) {
        if (isRequired == true) {
            if (controls[i].hasClass('select2')) {
                controls[i].Select2Require(true);
            } else {
                controls[i].addClass('require');
            }
        } else {
            if (controls[i].hasClass('select2')) {
                controls[i].Select2Require(false);
            } else {
                controls[i].removeClass("require");
            }
        }
    }
};

//#endregion

//#region   Screen mode

var InitialScreeModelWFD02620 = function () {
    if (SCREEN_PARAMS.SCREEN_MODE == SCREEN_MODES_WFD0262.ADD) { // SCREEN_PARAMS from index.cshtml
        InitialAddMode_WFD02620();
    } else {
        InitialEditMode_WFD02620();
    }


};

//#region   Add mode

var InitialAddMode_WFD02620 = function () {

    setScreenOnAddMode_WFD02620();
    WFD02620_ScreenClassModeControls.Edit.hide();
    setControlsRequiredField(requiredMainControl_WFD0262, true);
    setControlsRequiredField(requireWhenNoUploadFile, FileUplaodControls.File.val() == '');

    $('#WFD02620ASSET_LOCATION').Select2Require(true);
    //$('#WFD02620SUB_TYPE').Select2Require(true);
    $('#WFD02620DATA_AS_OF').Select2Require(true);
    $('#WFD02620ASSET_STATUS').Select2Require(true);

    $('#WFD02620ASSET_CLASS').Select2Require(true);
    $('#WFD02620MINOR_CATEGORY').Select2Require(true);
    $('#WFD02620COST_CENTER').Select2Require(true);
    $('#WFD02620RESPONSIBLE_COST_CENTER').Select2Require(true);
    $('#WFD02620BOI').Select2Require(true);
    $('#WFD02620INVENTORY_CHECK').Select2Require(true);

};

var setScreenOnAddMode_WFD02620 = function () {
    SettingSectionControls_WFD02620.CREATE_BY.val(WFD02620_User.FULL_NAME); // WFD02620_User from index.cshtml
    SettingSectionControls_WFD02620.COMPANY.select2("val", "");
    SettingSectionControls_WFD02620.YEAR.val('');
    SettingSectionControls_WFD02620.ROUND.val('');
    SettingSectionControls_WFD02620.ASSET_LOCATION.select2("val", "");
    SettingSectionControls_WFD02620.QTY_OF_HANDHELD.val('');
    //SettingSectionControls_WFD02620.IN.select2("val", "");
    SettingSectionControls_WFD02620.TARGET_DATE_FROM.val('');
    SettingSectionControls_WFD02620.TARGET_DATE_TO.val('');
    SettingSectionControls_WFD02620.DATA_AS_OF.val('');
    SettingSectionControls_WFD02620.BREAK_TIME_MINUTE.val('');
    SettingSectionControls_WFD02620.ASSET_STATUS.select2("val", "");
    FileUplaodControls.Textbox.val('');

    SettingSectionControls_WFD02620.ASSET_CLASS.val('');
    SettingSectionControls_WFD02620.MINOR_CATEGORY.val('');
    SettingSectionControls_WFD02620.COST_CENTER.val('');
    SettingSectionControls_WFD02620.RESPONSIBLE_COST_CENTER.val('');
    SettingSectionControls_WFD02620.BOI.select2("val", "");
    SettingSectionControls_WFD02620.INVENTORY_CHECK.select2("val", "");
};

//#endregion

//#region   Edit mode

var InitialEditMode_WFD02620 = function () {


    if (IsUpdateDataFromUploadFile) {
        InitialEditMode_WFD02620_IncaseWithoutAssetLocation();
    } else {
        InitialEditMode_WFD02620_IncaseWithAssetLocation();
    }
    //Add By Surasith Thatdee : Not allow normal user click complete and announce
    if (WFD02620_User.IS_FA_ADMIN == false) {
        ButtonControls_WFD02620.AnnounceButton.hide();
        ButtonControls_WFD02620.CompleteButton.hide();
    }
};


var InitialEditMode_WFD02620_IncaseWithoutAssetLocation = function () {

    WFD02620_ScreenClassModeControls.Edit.show();
    WFD02620_ScreenClassModeControls.Add.hide();

    ajax_method.PostClearMesage(WFD02620_URL.GetParamWithoutAssetLocation, ScreenKey_WFD02620, true, function (result) {
        if (result != null) {
            ScreenKey_WFD02620.Company = result.COMPANY;
            ScreenKey_WFD02620.Year = result.YEAR;
            ScreenKey_WFD02620.Round = result.ROUND;
            ScreenKey_WFD02620.AssetLocation = result.ASSET_LOCATION;



            ajax_method.PostClearMesage(WFD02620_URL.GetStockTakeData, ScreenKey_WFD02620, true, function (result) {

                if (result != null) {

                    setSettingSectionData_WFD02620(result.stockTakeH);

                    WFD02620_Param.PLAN_STATUS = result.stockTakeH.PLAN_STATUS;

                    if (result.stockTakeH.PLAN_STATUS != 'D') {
                        setModeButtonInitial(result.stockTakeH.PLAN_STATUS);
                    } else {
                        editModeButtonInitial(result.stockTakeDPerSV);
                    }

                    if (result.stockTakeH.PLAN_STATUS == 'S') {
                        $('#WFD02620_setting').trigger("click");
                    }

                    // function in file  WFD02620_Assignment.js
                    setAssignmentData_WFD02620_Assignment(result.stockTakeH //, result.stockTakeD
                        , result.stockTakeDPerSV, result.stockTakeHolidays, result.masterSetting
                        , result.summaryMinorCate, IsFAAdmin_WFD02620);
                    IsAfterSave = true;

                }
            }, null, IsAfterSave, 'WFD02620_AssignmentBox');
        }
    }, null, IsAfterSave, 'WFD02620_AssignmentBox');


};

var InitialEditMode_WFD02620_IncaseWithAssetLocation = function () {

    WFD02620_ScreenClassModeControls.Edit.show();
    WFD02620_ScreenClassModeControls.Add.hide();


    ajax_method.PostClearMesage(WFD02620_URL.GetStockTakeData, ScreenKey_WFD02620, true, function (result) {

        if (result != null) {

            setSettingSectionData_WFD02620(result.stockTakeH);
            WFD02620_Param.PLAN_STATUS = result.stockTakeH.PLAN_STATUS;

            if (result.stockTakeH.PLAN_STATUS != 'D' && result.stockTakeH.PLAN_STATUS != 'C') {
                setModeButtonInitial(result.stockTakeH.PLAN_STATUS);
            } else {
                editModeButtonInitial(result.stockTakeDPerSV);
            }

            if (result.stockTakeH.PLAN_STATUS == 'S') {
                $('#WFD02620_setting').trigger("click");
            }

            // function in file  WFD02620_Assignment.js
            setAssignmentData_WFD02620_Assignment(result.stockTakeH //, result.stockTakeD
                , result.stockTakeDPerSV, result.stockTakeHolidays, result.masterSetting
                , result.summaryMinorCate, IsFAAdmin_WFD02620);
            IsAfterSave = true;

        }
    }, null, IsAfterSave, 'WFD02620_AssignmentBox');

};

var editModeButtonInitial = function (sv) {
    ButtonControls_WFD02620.EditButtonGroup.hide();
    if (WFD02620_User.IS_FA_ADMIN) {
        if (isEditAssinment(sv)) {
            ButtonControls_WFD02620.ResetAssignment.prop('disabled', false);
            ButtonControls_WFD02620.Edit.prop('disabled', true);
        } else {
            ButtonControls_WFD02620.ResetAssignment.prop('disabled', true);
            ButtonControls_WFD02620.Edit.prop('disabled', false);
        }
    } else {
        ButtonControls_WFD02620.AssignButtonGroup.hide();
        ButtonControls_WFD02620.BottomButtonGroup.hide();
    }
};
var setModeButtonInitial = function (planStatus) {

    switch (planStatus) {
        case 'S':
            ButtonControls_WFD02620.SettingSave.hide();
            ButtonControls_WFD02620.SettingCancel.hide();
            ButtonControls_WFD02620.ResetAssignment.hide();
            ButtonControls_WFD02620.Edit.hide();
            ButtonControls_WFD02620.AnnounceButton.hide();
            ButtonControls_WFD02620.CompleteButton.hide();
            break;
        case 'F':
            ButtonControls_WFD02620.SettingSave.hide();
            ButtonControls_WFD02620.SettingCancel.hide();
            ButtonControls_WFD02620.ResetAssignment.hide();
            ButtonControls_WFD02620.Edit.hide();
            ButtonControls_WFD02620.AnnounceButton.hide();
            ButtonControls_WFD02620.CompleteButton.hide();
            break;
    }




};

var viewModeButtonInitial = function () {
    ButtonControls_WFD02620.AnnounceButton.hide();
    ButtonControls_WFD02620.AssignButtonGroup.hide();
    ButtonControls_WFD02620.BottomButtonGroup.hide();
    ButtonControls_WFD02620.CompleteButton.hide();
    ButtonControls_WFD02620.Edit.hide();
    ButtonControls_WFD02620.EditButtonGroup.hide();
    ButtonControls_WFD02620.ResetAssignment.hide();
    ButtonControls_WFD02620.SettingCancel.hide();
    ButtonControls_WFD02620.SettingSave.hide();

    WFD02620_AssignmentControls.SaveAssignmentButton.hide();
    WFD02620_AssignmentControls.CancelAssignmentButton.hide();
};
var isEditAssinment = function (datas) {
    var isEdit = false;

    for (var i = 0; i < datas.length; i++) {
        if (datas[i].DATE_FROM != null || datas[i].DATE_TO != null || datas[i].TIME_START != null || datas[i].TIME_END != null) {
            isEdit = true; break;
        }
    }
    return isEdit;
};

var setSettingSectionData_WFD02620 = function (hdata) {

    SettingSectionControls_WFD02620.COMPANY.val(hdata.COMPANY).trigger("change");;
    SettingSectionControls_WFD02620.YEAR.val(hdata.YEAR);
    SettingSectionControls_WFD02620.ROUND.val(hdata.ROUND);
    SettingSectionControls_WFD02620.ASSET_LOCATION.val(hdata.ASSET_LOCATION).trigger("change");
    SettingSectionControls_WFD02620.QTY_OF_HANDHELD.val(hdata.QTY_OF_HANDHELD);
    //SettingSectionControls_WFD02620.SUB_TYPE.select2('val', hdata.SUB_TYPE.split(','));//.trigger("change");
    SettingSectionControls_WFD02620.TARGET_DATE_FROM.datepicker('setDate', ConvertToJsonDate(hdata.TARGET_DATE_FROM));
    SettingSectionControls_WFD02620.TARGET_DATE_TO.datepicker('setDate', ConvertToJsonDate(hdata.TARGET_DATE_TO));
    SettingSectionControls_WFD02620.DATA_AS_OF.datepicker('setDate', ConvertToJsonDate(hdata.DATA_AS_OF));
    SettingSectionControls_WFD02620.BREAK_TIME_MINUTE.val(hdata.BREAK_TIME_MINUTE);
    SettingSectionControls_WFD02620.ASSET_STATUS.val(hdata.ASSET_STATUS).trigger("change");
    SettingSectionControls_WFD02620.CREATE_BY.val(hdata.EMP_NAME + ' ' + hdata.EMP_LASTNAME); // WFD02620_User from index.cshtml
    
    SettingSectionControls_WFD02620.ASSET_CLASS.val(hdata.ASSET_CLASS).trigger("change");
    SettingSectionControls_WFD02620.MINOR_CATEGORY.val(hdata.MINOR_CATEGORY).trigger("change");
    SettingSectionControls_WFD02620.COST_CENTER.val(hdata.COST_CENTER).trigger("change");
    SettingSectionControls_WFD02620.RESPONSIBLE_COST_CENTER.val(hdata.RESPONSIBLE_COST_CENTER).trigger("change");
    SettingSectionControls_WFD02620.BOI.val(hdata.BOI).trigger("change");
    SettingSectionControls_WFD02620.INVENTORY_CHECK.val(hdata.INVENTORY_CHECK).trigger("change");

    WFD02620_ScreenClassModeControls.Input.prop('disabled', true);
    WFD02620_ScreenClassModeControls.InputDTButton.prop('disabled', true);

    $('.require').removeClass('require');
};

//#endregion



//#endregion

//#region   Input enabled


var enabledInput_WFD02620 = function () {

    WFD02620_ScreenClassModeControls.Input.prop('disabled', false);
    WFD02620_ScreenClassModeControls.PrimaryInput.prop('disabled', true);
    WFD02620_ScreenClassModeControls.InputDTButton.show();

    setControlsRequiredField();
};

//#endregion

//#region   Asset from upload process  event 

// Binding Asset upload file's  controls event
var bindingAssetFileUploadControlEvents_WFD02620 = function () {
    FileUplaodControls.Textbox.blur(onWFD02620FileUploadTBBlur);
    FileUplaodControls.File.change(onWFD02620FileUploadFileChange);
};

// Textbox file path focus out event : 
// if textbox empty -> clear file input
// else fill textbox with file path
var onWFD02620FileUploadTBBlur = function () {
    if (FileUplaodControls.Textbox.val() == '') {
        FileUplaodControls.File.val('');
    } else {
        FileUplaodControls.Textbox.val(getFilePathFromWFD02620FileUploadFile());
    }
};
// File input value change event : Set file path to textbox
var onWFD02620FileUploadFileChange = function () {
    FileUplaodControls.Textbox.val(getFilePathFromWFD02620FileUploadFile());
    if (FileUplaodControls.Textbox.val() == '') {
        setControlsRequiredField(requireWhenNoUploadFile, true);
    } else {
        setControlsRequiredField(requireWhenNoUploadFile, false);
    }
};

// get file path from file input
var getFilePathFromWFD02620FileUploadFile = function () {
    return FileUplaodControls.File.val().replace(/C:\\fakepath\\/i, '');
};

// Browse file button click event : Open select file dialog
var onWFD02620FileUploadButtonClick = function () {
    FileUplaodControls.File.click();
};

//#endregion

//#region   Common function

var WFD02620_ConvertListToString = function (arr) {

    if (arr != null && arr.length > 0) {

        var str = '';

        for (var i = 0; i < arr.length; i++) {
            str += arr[i] + ',';
        }

        return str.slice(0, -1);
    } else {
        return '';
    }
};

//#endregion

var WFD02620GetMinorCategory = function () {

    console.log(WFD02620_URL.GetMinorCategory_DDL);
    ajax_method.Post(WFD02620_URL.GetMinorCategory_DDL, { COMPANY: $('#WFD02620COMPANY').val() }, true, function (result) {
        if (result != null) {
         
            SettingSectionControls_WFD02620.MINOR_CATEGORY.html('');
           
            var optionhtml1 = '<option value="">' + " " + '</option>';
            $.each(result, function (i) {

                //SettingSectionControls_WFD02620.MINOR_CATEGORY.append(optionhtml1);
                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].CODE + " : " + result[i].VALUE + '</option>';
            
                SettingSectionControls_WFD02620.MINOR_CATEGORY.append(optionhtml);
            });
            $('#WFD02620MINOR_CATEGORY').multiselect('rebuild');
           // $('#WFD02620MINOR_CATEGORY').multiselect();
        }

    }, null);

};

var getMultiControlValue = function (id) {
    var _val = '';
    if (id.val() != null) {
        _val = (id.val() + '');
        _val = _val.replace(/,/g, '#');
    }
    return _val
}

//var GetMinorCategory = function () {
//    var con = {
//        company: Controls.COMPANY.val()
    
//    };

//    Controls.MINOR_CATEGORY.select2({
//        width: '120px'
//    }).select2("val", '');
//    Controls.MINOR_CATEGORY.html('');

//    var optionhtml1 = '<option value="">' + " " + '</option>';

//    ajax_method.Post(WFD02620_URL.GetMinorCategory_DDL, con, false, function (result) {
//        if (result != null) {


//            Controls.MINOR_CATEGORY.append(optionhtml1);

//            $.each(result.data, function (i) {
//                var optionhtml = '<option value="' + result.data[i].CODE + '">' + result.data[i].VALUE + '</option>';

//                Controls.MINOR_CATEGORY.append(optionhtml);
//            });

//        } else {
//            Controls.MINOR_CATEGORY.append(optionhtml1);

//            //Controls.MINOR_CATEGORY.select2({
//            //    width: '120px'
//            //}).select2("val", '');
//        }

//    }, null);


//};