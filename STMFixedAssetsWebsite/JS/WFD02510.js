﻿
$(function () {

  //  fScreenMode.EnableTMCDocument(false); //Enable when generate approval flow
    


});
var ColIndex = {
    Sel: 0,
    Index : 1,
    AssetNo : 2,
    AssetSub : 3,
    AssetName : 4,
    DateInService : 5,
    Cost : 6,
    BookValue :7,
    Retire:8,
    Reason : 9,
    Photo:10,
    Countermeasure : 11,
    InvestReason : 12,
    BOINo : 13,
    BOIAttachment : 14,
    Store: 15,  
    Info : 16,
    Status : 17,
    Delete : 18
}
var control = {
    DataTable: null,
    
    WFD02510MessageAlertReasonOther: $('#WFD02510MessageAlertReasonOther'),
    WFD02510_SetOtherReason: $('#WFD02510_SetOtherReason'),
      
    WFD02510_DISP_REASON_OTHER: $('#WFD02510_DISP_REASON_OTHER'),

    selectedRow : null,
    TMCAttachCount : 0,
    AllowUploadTMC : false,
    AllowUploadTop : false,
    AllowChangeFlow : false,

    EnableMass:false
}
var constants = {
    Mass : "M",
    Normal : "N",

    Sale: "SALES",
    Donat : "DONATE",
    Scrap: "SCRAP",
    TMC: "TMC", //contain with TMC, allow uplaod
    CHANGE_FLOW : "C" //start with C, allow change flow
}
var ArrDisposalReason;
//var ArrDisposalStore;
var TempFileWFD02510 = [];

var fPopup = {
    GetRetirePopup: function (button) {
        //debugger;

        control.selectedRow = fAction.GetSelectedRow(button);
        if (control.selectedRow == null)
            return;
        var row = control.DataTable.row(control.selectedRow).data();

        // clear value
        $("#AlertMessageWFD02510").html('');
        $("#WFD02510_Retire_Quantity, #WFD02510_Retire_Percentage").prop("checked", false);
        $('#WFD02510_Retire_Quantity_TXT, #WFD02510_Retire_Percentage_TXT').val('').prop('disabled', true);


        $('#WFD02510_COST_VALUE').val(currencyFormat(row.COST));
        $('#WFD02510_BOOK_VALUE').val(currencyFormat(row.BOOK_VALUE));

        $('#WFD02510_COST_VALUE').attr('readonly', 'true');
        $('#WFD02510_BOOK_VALUE').attr('readonly', 'true');

        var maxQty = row.QTY.toFixed(3).toString();
        //var minQty = row.QTY > 0 ? '0.001' : '0.000';
        //$("#WFD02510_Retire_Percentage_TXT").autoNumeric('init', { vMin: '0.01', vMax: '100.00' });
        //$("#WFD02510_Retire_Quantity_TXT").autoNumeric('init', { vMin: minQty, vMax: maxQty });

        $('#lblQty').text(maxQty);

        if (maxQty <= 0) {
            $("#WFD02510_Retire_Quantity").prop("disabled", true);
        }

        //alert(1);
        switch (row.ADJ_TYPE) {
            case 'Q':
                $('#WFD02510_Retire_Quantity_TXT').val(parseFloat(row.RETIRE).toFixed(3));
                $("#WFD02510_Retire_Quantity").prop("checked", true);
                $('#WFD02510_Retire_Percentage_TXT').prop('disabled', true);
                $('#WFD02510_Retire_Quantity_TXT').prop('disabled', false);
                $('#WFD02510_Retire_Percentage_TXT').val('');
                break;
            case 'P':
                $('#WFD02510_Retire_Percentage_TXT').val(parseFloat(row.RETIRE).toFixed(2));
                $("#WFD02510_Retire_Percentage").prop("checked", true);
                $('#WFD02510_Retire_Percentage_TXT').prop('disabled', false);
                $('#WFD02510_Retire_Quantity_TXT').prop('disabled', true);
                $('#WFD02510_Retire_Quantity_TXT').val('');
                break;
            default:
                break;
        }

        $('#WFD02510_Retire_Quantity_TXT').off('keypress').on('keypress', function (e) {
            return isNumberKey(e);
        });

        $('#WFD02510_Retire_Percentage_TXT').off('keypress').on('keypress', function (e) {
            return isNumberKey(e);
        });
        //control.ASSET_NO = row.ASSET_NO;
        //control.ASSET_SUB = row.ASSET_SUB;
        //control.COMPANY = row.COMPANY;

       
    },

    ShowBOIUploadDialog: function (button) {
        var row = control.DataTable.row($(button).parents('tr')).data();
        console.log('----------------');
        console.log(row.ASSET_NO);
        var _DocUpload = 'BOI';
       
        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            LINE_NO: 0,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _DocUpload,
            FUNCTION_ID: GLOBAL.FUNCTION_ID,
            DOC_UPLOAD: GLOBAL.FUNCTION_ID + "_" + _DocUpload,
            DEL_FLAG: row.IsBOIUpload  ? "Y" : "N"
        }
        //$('#lblUploadFileText').html("Upload BOI Document Asset No. :{0}".format(controlUpload.ASSET_NO));
        $('#lblUploadFileText').html("Upload BOI Document Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB);
        $('#CommonUpload').appendTo("body").modal('show');

    },
    ShowDetailUploadDialog: function (_Text, button, _Type) {
        var row = control.DataTable.row($(button).parents('tr')).data();        
        console.log('----------------');
        console.log(row.ASSET_NO);
        var _DocUpload = '';
        if (GLOBAL.FLOW_TYPE == null || GLOBAL.FLOW_TYPE == '') {
            _DocUpload = _Type;
        } else {
            _DocUpload = 'DN_' + _Type;//GLOBAL.FLOW_TYPE + '_' + _Type;
        }
        //Submit / 
        var _enable = (GLOBAL.DOC_NO == null || GLOBAL.DOC_NO == '' || PERMISSION.AllowResubmit == 'Y' ||
            (PERMISSION.IsAECUser == 'Y' && row.AllowEdit));
                               // AEC User can edit for error/new item but not edit for GEN/Success item

        //Photo allow for submit mode / requestor
        controlUpload = {
            GUID: GLOBAL.GUID,  
            DOC_NO: GLOBAL.DOC_NO,
            LINE_NO: 0,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _DocUpload,
            FUNCTION_ID: GLOBAL.FUNCTION_ID,
            DOC_UPLOAD: GLOBAL.FUNCTION_ID + "_" + _DocUpload,
            DEL_FLAG: (_enable) ? "Y" : "N"
        }

        var title = _Text + " Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB;
        $('#lblUploadFileText').html(title);
        // 
        $('#CommonUpload').appendTo("body").modal('show');

    },

    ShowMainUploadDialog: function (_Text, _Type,_Allow) {
        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: '',
            ASSET_SUB: '',
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _Type,
            FUNCTION_ID: "WFD02510",
            LINE_NO : 0,
            DOC_UPLOAD: "WFD02510" + "_" + _Type,
            DEL_FLAG: _Allow ? "Y" : "N" //Allow only Submit Mode, Fixed: Only Requestor can upload (Doc status = "00")
        }
        $('#lblUploadFileText').html(_Text);
        $('#CommonUpload').appendTo("body").modal('show');

    },

    ShowLossCaseDialog: function (button) {

        control.selectedRow = fAction.GetSelectedRow(button);
        if (control.selectedRow == null)
            return;
        var row = control.DataTable.row(control.selectedRow).data();

       

        //Clear Popup 
        $('#WFD02510_LOSS_COUNTER_MEASURE').val("");
        //$('#WFD02510_ATTACH_COUNTERMEASURE_FILE').val();

        $('#WFD02510_USER_MISTAKE_FLAG_Y, #WFD02510_USER_MISTAKE_FLAG_N').prop("checked", false);
        $('#WFD02510_COUNTERMEASURE').val();

        //Update new 
        if (row.USER_MISTAKE == "Y")
            $('#WFD02510_USER_MISTAKE_FLAG_Y').prop("checked", true);
        if (row.USER_MISTAKE == "N")
            $('#WFD02510_USER_MISTAKE_FLAG_N').prop("checked", true);

        $('#WFD02510_COUNTERMEASURE').val(row.COMPENSATION);

        //Requestor
        var _enable = (GLOBAL.DOC_NO == null || GLOBAL.DOC_NO == '' || PERMISSION.AllowResubmit == 'Y') ||
            (PERMISSION.IsAECUser == "Y" && row.AllowEdit);

        fUpload251.GetFileFromDB('DivWFD02510LossCaseFileList', 'LC', row, _enable);

        if (_enable) {
            $('#WFD02510_COUNTERMEASURE, #WFD02510_USER_MISTAKE_FLAG_Y, #WFD02510_USER_MISTAKE_FLAG_N').prop("disabled", false);
            $('#WFD02510_ATTACH_COUNTERMEASURE_BTN').show();
            $('#btnMeasureSave').show();
        }
        else {
            $('#WFD02510_COUNTERMEASURE, #WFD02510_USER_MISTAKE_FLAG_Y, #WFD02510_USER_MISTAKE_FLAG_N').prop("disabled", true);
            $('#WFD02510_ATTACH_COUNTERMEASURE_BTN').hide();
            $('#btnMeasureSave').hide();
        }

        var counter = false
        if (row.USER_MISTAKE == "Y")
            counter = true;

        if ($('#WFD02510_USER_MISTAKE_FLAG_Y').is(':disabled'))
            counter = false;

        $('#WFD02510_COUNTERMEASURE').prop("disabled", !counter);


        $('#WFD02510DivLossCase').appendTo("body").modal('show');
        TempFileWFD02510 = [];
    },

    ShowDetail: function (button) {
        var _disposaleType = fScreenVal.dispType() ;
        control.selectedRow = fAction.GetSelectedRow(button);
        if (control.selectedRow == null)
            return;
        
        var row = control.DataTable.row(control.selectedRow).data();

       
        //not allow to edit for ACM (IsDetailView)
        //&& row.ROW_STATE <= PERMISSION.ActionRoleIndex
        var _enable = (row.AllowEditRetirement ) || (row.ENABLE_DETAIL && PERMISSION.IsMainApprover == "Y");
        //if (PERMISSION.AllowResend == 'Y' && row.AllowEditRetirement) {
        //    _enable = true;
        //}

        if (row.IsDetailView) {
            _enable = false; 
        }
        

        if (_disposaleType == constants.Scrap) {
            fPopup._ShowScrapPopup(_enable);
        }
        if (_disposaleType == constants.Donat) {
            fPopup._ShowDonatePopup(_enable);
        }
        if (_disposaleType == constants.Sale) {
            fPopup._ShowSalePopup(_enable);
        }
    },
    _ShowScrapPopup: function (_enable) {
        var _row = control.DataTable.row(control.selectedRow).data();

        $('#WFD02510_SCRAPPING_DATE').val(_row.DETAIL_DATE);
        $('#WFD02510_SCRAPPING_REMARK').val(_row.DETAIL_REMARK);

        if (!_enable) {
            $('#btnScrappingInfoUpload, #btnScrappingInfoSave').hide(); //button
        }
        else {
            $('#btnScrappingInfoUpload, #btnScrappingInfoSave').show(); //button
        }
        $('#WFD02510_SCRAPPING_DATE, #WFD02510_SCRAPPING_REMARK').prop("disabled", !_enable);


        fUpload251.GetFileFromDB('DivWFD02510ScrappingInfoFileList', 'SCRAP', _row, _enable);
        
        $('#divScrappingInfo h4.modal-title').html(("Scrapping  Info of {0} : {1}").format(_row.ASSET_NO, _row.ASSET_NAME));
        $('#divScrappingInfo').modal();
    },
    _ShowSalePopup: function (_enable) {
        var _row = control.DataTable.row(control.selectedRow).data();

        $('#WFD02510_SALE_SOLD_TO').val(_row.SOLD_TO );
        $('#WFD02510_SALE_DATE').val(_row.DETAIL_DATE);
        $('#WFD02510_INVOICE_NO').val(_row.INVOICE_NO);
        $('#WFD02510_PRICE').val( _row.PRICE);
        $('#WFD02510_COST_OF_REMOVAL').val(_row.REMOVAL_COST);
        $('#WFD02510_SALE_REMARK').val(_row.DETAIL_REMARK);

        if (!_enable) {
            $('#btnSalesInfoSave').hide(); //button
        }
        else {
            $('#btnSalesInfoSave').show(); //button
        }
        $('#WFD02510_SALE_SOLD_TO, #WFD02510_SALE_DATE, #WFD02510_INVOICE_NO, #WFD02510_PRICE, #WFD02510_COST_OF_REMOVAL, WFD02510_SALE_REMARK').prop("disabled", !_enable);



        $('#divSalesInfo h4.modal-title').html(("Sale  Info of {0} : {1}").format(_row.ASSET_NO, _row.ASSET_NAME));
        $('#divSalesInfo').modal();
    },
    _ShowDonatePopup: function (_enable) {
        var _row = control.DataTable.row(control.selectedRow).data();
       
        $('#WFD02510_DONATION_DATE').val(_row.DETAIL_DATE);
        $('#WFD02510_DONATION_REMARK').val(_row.DETAIL_REMARK);
        
        if (!_enable) {
            $('#btnDonationInfoUpload, #btnDonationInfoSave').hide(); //button
        }
        else {
            $('#btnDonationInfoUpload, #btnDonationInfoSave').show(); //button
        }
        $('#WFD02510_DONATION_DATE, #WFD02510_DONATION_REMARK').prop("disabled", !_enable);

        fUpload251.GetFileFromDB('DivWFD02510DonationInfoFileList', 'DONATE', _row, _enable);


        $('#divDonationInfo h4.modal-title').html(("Donation  Info of {0} : {1}").format(_row.ASSET_NO, _row.ASSET_NAME));
        $('#divDonationInfo').modal();
    }
}


var fAction = {
    Initialize: function (callback) {
        //Initialize

        $('#DivTopManagement').hide();

        ajax_method.SearchPost(UrlWFD02510.GetDisposalReason, GLOBAL, null, true, function (reason, pagin) {
            if (reason != null) {
                ArrDisposalReason = reason;
                fAction.initDisposalReasonCombo();
            }
            console.log("GetDisposalReason");
            if (callback != null)
                callback();

            //ajax_method.SearchPost(UrlWFD02510.GetDisposalStore, GLOBAL, null, true, function (Store, pagin) {
            //    if (Store != null) {
            //        ArrDisposalStore = Store;
            //    }

            //    if (callback != null)
            //        callback();

            //}, null);

        }, null);

        
    },

    initDisposalReasonCombo: function() {
        if ($("#WFD02510MassDisposal").is(":visible")) {
            var reasons = getDisposalReason();

            var $select = $('#WFD02510_DISP_MASS_REASON').empty();

            var html = '<option value="">&nbsp;</option>';
            $.each(reasons, function (i, reason) {
                html += '<option value="' + reason.CODE + '">' + reason.VALUE + '</option>';
            });

            $select.append(html);

            $select.select2('val', '');
        }
    },

    SelectedCostCenter: "",
    SelectedRespCostCenter: "",    
    LoadData: function (callback) {
        //Get Header 
        ajax_method.Post(UrlWFD02510.GetRetirementHeader, GLOBAL, true, function (result) {
            if (result != null) {
              
                $('#WFD02510_NORMAL_DISPOSAL').prop("checked", result.ROUTE_TYPE == constants.Normal);
                $('#WFD02510_MASS_DISPOSAL').prop("checked", result.ROUTE_TYPE == constants.Mass);

                $('#WFD02510_DISPOSAL_BY_SALE').prop("checked", result.DISP_TYPE == constants.Sale);
                $('#WFD02510_DISPOSAL_BY_SCRAP').prop("checked", result.DISP_TYPE == constants.Scrap);
                $('#WFD02510_DISPOSAL_BY_DONATE').prop("checked", result.DISP_TYPE == constants.Donat);

                control.TMCAttachCount = result.TMC_ATTACH_CNT;


                if (result.TOP_MANAGEMENT_FLAG == 'Y') {

                    var _uploadTop = (PERMISSION.AllowResubmit == 'Y') || (GLOBAL.STATUS == '00'); 

                    fAction.SetUploadButton('#WFD02510btnUploadTopManagement', _uploadTop);
                    control.AllowUploadTop = _uploadTop;
                    $('#DivTopManagement').show();
                }
                else {
                    $('#DivTopManagement').hide();
                }

                var _uploadMD = (PERMISSION.AllowResubmit == 'Y') || (GLOBAL.STATUS == '00'); 
                fAction.SetUploadButton('#WFD02510_DISP_MASS_ATTACH_DOC_BTN', _uploadMD);
            }

            console.log("LoadData is callled");
            var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();


            //debugger;
            console.log(GLOBAL);
            console.log(Url.GetAsset);
            ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
                console.log(datas);
                fAction.SelectedCostCenter = ""; //Clear Selected
                fAction.SelectedRespCostCenter = ""; //Clear Selected

                if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                    var table = $('#tbSearchResult').DataTable();
                    table.clear().draw().destroy();
                }


                if (datas == null || datas.length == 0) {
                    $('#divSearchRequestResult').hide();
                    //fScreenMode.EnableTMCDocument(false);

                    if (callback != null)
                        callback();

                    return;
                }

                fAction.SelectedCostCenter = datas[0].COST_CODE;
                fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;



                var _bBOI = (PERMISSION.IsBOIState == "A" && (PERMISSION.IsBOIUser == "Y" || PERMISSION.IsAECUser == "Y" )) || PERMISSION.IsBOIState == "Y";
                var _bStore = (PERMISSION.IsStoreState == "A" && PERMISSION.IsStoreUser == "Y") || PERMISSION.IsStoreState == "Y";
                var _bDetail = (datas[0].ENABLE_DETAIL) || $('#WFD02510_MASS_DISPOSAL').is(':checked');

                //2019.12.14 Incase Mass transfer and AEC is requestor
                if ($('#WFD02510_MASS_DISPOSAL').is(':checked') && $('#WFD01170ddpRequestorRole').val() == GLOBAL.ACRCode) {
                    _bDetail = true;
                }
               

                if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
                if (!pagin.OrderType) pagin.OrderType = 'asc';

                control.DataTable = $('#tbSearchResult').DataTable({
                    data: datas,
                    "columns": [
                        { "data": null }, // 0                    
                        { "data": "ROW_INDX", 'orderable': false, "class": "text-center", }, // 1                   
                        { "data": null }, // 2
                        { "data": "ASSET_SUB", 'orderable': false, "class": "text-center" }, // 3
                        {
                            "data": "ASSET_NAME", 'orderable': false, "class": "text-left",
                            'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                        }, // 4
                        { "data": "DATE_IN_SERVICE", 'orderable': false, "class": "text-center" }, // 5
                        { "data": "RETIRE_COST", 'orderable': false, "class": "text-right" },//6
                        { "data": "RETIRE_BOOK_VALUE", 'orderable': false, "class": "text-right" },//7
                        { "data": null, 'orderable': false },	//7    8              
                        { "data": null, 'orderable': false, "class": "text-enter" }, //9
                        { "data": null, 'orderable': false, "class": "text-center" },	//10
                        { "data": null, 'orderable': false, "class": "text-center" }, //11
                        { "data": "INVEST_REASON", 'orderable': false, "class": "text-center" }, //12
                        { "data": "BOI_NO", 'orderable': false, "class": "text-center" }, //13
                        { "data": null, 'orderable': false, "class": "text-center" }, //14
                        { "data": null, 'orderable': false, "class": "text-center" }, //15
                        { "data": null, 'orderable': false }, // 16
                        { "data": null, 'orderable': false }, //17
                        { "data": null } // 18
                    ],
                    'columnDefs': [
                        {
                            'targets': 0,
                            'searchable': false,
                            'orderable': false,
                            "visible": (PERMISSION.AllowResend == "Y"),
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                return fRender.CheckBoxRender(data);
                            }
                        },
                        {
                            'targets': 2, //ASSET_NO Show hyperlink
                            'searchable': false,
                            'orderable': false,
                            'render': function (data, type, full, meta) {
                                return fRender.HyperLinkRender(full);
                            }
                        },//
                        {
                            'targets': 6, //COST
                            'orderable': false,
                            'className': 'text-right',
                            'render': function (data, type, full, meta) {
                                return currencyFormat(data);
                            }

                        },
                        {
                            'targets': 7, //BOOK_VALUE
                            'orderable': false,
                            'className': 'text-right',
                            'render': function (data, type, full, meta) {
                                return currencyFormat(data);
                            }

                        },
                        {
                            'targets': 8,//RETIRE BUTTON
                            'searchable': false,
                            'orderable': false,
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                console.log("RETIRE");
                                //console.log(full);
                                return fRender.ButtonRetireRender(full);
                            }
                        },
                        {
                            'targets': 9,//DISP_REASON
                            'searchable': false,
                            'orderable': false,
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                return fRender.ComboReasonRender(full);
                            }
                        },
                        {
                            'targets': 10,//PHOTO_ATTACH
                            'searchable': false,
                            'orderable': false,
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                console.log("PHOTO_ATTACH");
                                //console.log(full);
                                return fRender.ButtonPhotoRender(full);
                            }
                        },
                        {
                            'targets': 11,//MEASURE
                            'searchable': false,
                            'orderable': false,
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                console.log("ButtonMeasureRender");
                                //console.log(full);
                                return fRender.ButtonMeasureRender(full);
                            }
                        },
                        {
                            'targets': 14,//BOI_ATTACH_DOC
                            'searchable': false,
                            'orderable': false,
                            "visible": (_bBOI),
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                console.log("BOI_ATTACH_DOC");
                                //console.log(full);
                                return fRender.ButtonBoiDocumentRender(full);
                            }
                        },
                        {
                            'targets': 15,//STORE_CHECK_FLAG
                            'searchable': false,
                            'orderable': false,
                            "visible": (_bStore),
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                return '';
                            }
                        },
                        {
                            'targets': 16,//DETAIL
                            'searchable': false,
                            'orderable': false,
                            "visible": (_bDetail),
                            'className': 'text-center',
                            'render': function (d, t, r) {
                                return fRender.ButtonInfoRender(d, t, r);
                            }
                        },
                        {
                            'targets': 17, //Status
                            'orderable': true,
                            'className': 'text-left',
                            "render": function (data, type, full, meta) {

                                if (data.STATUS_HINT == null)
                                    return data.STATUS;

                                return '<span data-toggle="tooltip" title="' + data.STATUS_HINT + '">' + data.STATUS + '</span>'; //Test
                            }
                        },
                        {
                            'targets': 18, //Delete
                            'searchable': false,
                            'orderable': false,
                            'className': 'text-center',
                            'render': function (data, type, full, meta) {
                                return fRender.DeleteButtonRender(full);
                            }

                        }
                    ],
                    'order': [],
                    searching: false,
                    paging: false,
                    retrieve: true,
                    "bInfo": false,
                    fixedHeader: {
                        header: true,
                        footer: false
                    },
                    "initComplete": function (settings, json) {
                        console.log('DataTables has finished its initialisation.');
                    },
                    "createdRow": function (row, data, index) {
                        console.log("createdRow");
                    }
                }).columns.adjust().draw();

                //Incase New Mode
                if (GLOBAL.DOC_NO == '') {
                    if (datas.length == 0) {
                        fScreenMode.Submit_InitialMode(function () {
                            fMainScreenMode.Submit_InitialMode();
                        });
                    }
                    if (datas.length > 0) {
                        fScreenMode.Submit_AddAssetsMode(function () {
                            fMainScreenMode.Submit_AddAssetsMode();
                        });
                    }

                }

                $('#divSearchRequestResult').show();
                if (callback == null)
                    return;

                callback();

            }, null);


        });
    },

    Clear: function (callback) {

        fAction.UpdateAsset(function () { //Update first.
            //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
            ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
                console.log(result);
                if (result.IsError) {
                    return;
                }

                fAction.LoadData(function () {
                    fScreenMode.Submit_InitialMode(function () {
                        fMainScreenMode.Submit_InitialMode();
                    });
                });
                //  SuccessNotification(CommonMessage.ClearSuccess);

                if (callback == null)
                    return;

                callback();
            }); // ClearAssetsList
        }); // UpdateAsset
    },

    Export: function (callback) {

        fAction.UpdateAsset(function () {
            var batch = BatchProcess({
                BatchId: "LFD02550",
                Description: "Export Dispose Request",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'GUID', GLOBAL.GUID);
            batch.Addparam(2, 'DOC_NO', GLOBAL.DOC_NO);
            batch.StartBatch();

            if (callback == null)
                return;

            callback();
        });
    },

    AddAsset: function (_list, callback) { //It's call from dialog

        //console.log(Url.AddAsset);

        if (_list == null || _list.length == 0)
            return;

        _list.forEach(function (_data) {
            // do something with `item`
            _data.GUID = GLOBAL.GUID;
            _data.USER_BY = GLOBAL.USER_BY;
        });

        ajax_method.Post(Url.AddAsset, _list, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }

            fAction.LoadData();

            if (callback == null)
                return;

            callback();
        });
    },
    UpdateAsset: function (callback) {
        debugger

        var _data = fAction.GetClientAssetList(false); // Get All
        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            if (result.IsError) {
                //fnErrorDialog("UpdateAssetList", result.Message);
                return;
            }
            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList

    },
    DeleteAsset: function (_AssetNo, _AssetSub) {
        //console.log(_AssetNo);
        //console.log(Url.DeleteAsset);
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: GLOBAL.COMPANY,
            ASSET_NO: _AssetNo,
            ASSET_SUB: _AssetSub
        }
        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(_AssetNo, _AssetSub), function (result) {
            if (!result) {
                return;
            }
            fAction.UpdateAsset(function () {
                ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
                    if (result.IsError) {
                        return;
                    }
                    console.log("Delete");
                    fAction.LoadData();
                    // SuccessNotification(CommonMessage.ClearSuccess);
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Deletion process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                }); // Delete
            }); // Update
        }) ; //Confirm
    }, //Delete Function

    GetSelectedRow: function (sender) {

        if(control.DataTable == null){
            return ;
        }

        return control.DataTable.row($(sender).parents('tr')) ;

    },

    UpdateRetire: function (callBack) {

        console.log("UpdateRow");
        //debugger;

        
        var _row = control.DataTable.row(control.selectedRow).data();
        var _bFoundFlag = false;

        //var r = $('input[type=radio][name=Retire]:checked').val();
        var r = _GetAdjType();
        var _RETIRE = 0;
        var _RETIRE_COST = 0;
        var _RETIRE_BOOK_VALUE = 0;
        var _buttonRetire = '';
        var _COST = _row.COST;
        var _BOOK = _row.BOOK_VALUE;
        var _RETIRE_QTY = 0;

        if (r == 'Q') {
            _RETIRE = parseFloat($('#WFD02510_Retire_Quantity_TXT').val()).toFixed(3);
            _RETIRE_QTY = _RETIRE;

            _RETIRE_COST = _row.QTY > 0 ? (_COST * _RETIRE) / _row.QTY : 0;
            _RETIRE_BOOK_VALUE = _row.QTY > 0 ? (_BOOK * _RETIRE) / _row.QTY : 0;
            _buttonRetire = $('#WFD02510_Retire_Quantity_TXT').val() + '/' + _row.QTY;
        }
        else {

            _RETIRE = parseFloat($('#WFD02510_Retire_Percentage_TXT').val()).toFixed(2);

            _RETIRE_COST = (_COST * _RETIRE) / 100;
            _RETIRE_BOOK_VALUE = (_BOOK * _RETIRE) / 100;
            _buttonRetire = $('#WFD02510_Retire_Percentage_TXT').val() + '%';
        }

        _row.RETIRE_COST = _RETIRE_COST;
        _row.RETIRE_BOOK_VALUE = _RETIRE_BOOK_VALUE;
        _row.RETIRE = _RETIRE;
        _row.RETIREMENT_TYPE = r;
        _row.ADJ_TYPE = r;
        _row.RETIRE_QTY = _RETIRE_QTY;


        control.DataTable.row(control.selectedRow).data(_row).draw(false);

        fAction.UpdateAsset(function () {
            if (callBack) {
                callBack();
            }

            control.selectedRow = null;
        });
    },

    UpdateDetailInfo: function (_scr) {
        console.log("UpdateRow");
       
        var _row = control.DataTable.row(control.selectedRow).data();
        _row.EDIT_FLAG = 'Y';
        switch (_scr) {
            case constants.Sale:
                _row.SOLD_TO = $('#WFD02510_SALE_SOLD_TO').val();
                _row.DETAIL_DATE = $('#WFD02510_SALE_DATE').val();
                _row.INVOICE_NO = $('#WFD02510_INVOICE_NO').val();
                _row.PRICE = $('#WFD02510_PRICE').val();
                _row.REMOVAL_COST = $('#WFD02510_COST_OF_REMOVAL').val();
                _row.DETAIL_REMARK = $('#WFD02510_SALE_REMARK').val();
                break;
            case constants.Scrap:
                _row.DETAIL_DATE = $('#WFD02510_SCRAPPING_DATE').val();
                _row.DETAIL_REMARK = $('#WFD02510_SCRAPPING_REMARK').val();
                break;
            case constants.Donat:
                _row.DETAIL_DATE = $('#WFD02510_DONATION_DATE').val();
                _row.DETAIL_REMARK = $('#WFD02510_DONATION_REMARK').val();
                break;
        }

        control.DataTable.row(control.selectedRow).data(_row).draw(false);
        control.selectedRow = null;

    },
    GetUserMistake: function () {
        var userMistake = "";
        if ($('#WFD02510_USER_MISTAKE_FLAG_Y').is(":checked")) {
            userMistake = "Y";
        }
        if ($('#WFD02510_USER_MISTAKE_FLAG_N').is(":checked")) {
            userMistake = "N";
        }
        return userMistake;
    },
    ValidateUpdateCM: function () {
        var userMistake = fAction.GetUserMistake();
        if (userMistake == "") {
            AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format("", "Consider as user's mistake"), "#AlertMessageWFD02510LossCase");
            return false;
        }

        if ($('#WFD02510_USER_MISTAKE_FLAG_Y').is(":checked") && $('#WFD02510_COUNTERMEASURE').val() == "") {
            AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format("", "Compensation"), "#AlertMessageWFD02510LossCase");
            return false;
        }

        return true;
    },
    UpdateCM: function (_cmFileName) {
        var _row = control.DataTable.row(control.selectedRow).data();

        _row.LOSS_COUNTER_MEASURE = _cmFileName;
        _row.USER_MISTAKE = fAction.GetUserMistake();
        
        _row.COMPENSATION = $('#WFD02510_COUNTERMEASURE').val();
        control.DataTable.row(control.selectedRow).data(_row).draw(false);
        control.selectedRow = null;

        $('#WFD02510DivLossCase').modal('hide');
    },


    UpdateReasonOther: function () {
        //debugger;
        var _DISP_REASON_OTHER = $('#WFD02510_DISP_REASON_OTHER').val();
        var _DISP_REASON = 'OTHER';


        //Normal
        if (control.selectedRow != null) {

            var _row = control.DataTable.row(control.selectedRow).data();
            _row.DISP_REASON_OTHER = _DISP_REASON_OTHER;
            _row.DISP_REASON = _DISP_REASON;
            control.DataTable.row(control.selectedRow).data(_row).draw(false);

            return;
        }

        $("#tbSearchResult tr").each(function () {

            
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }

            var _row = [];

            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data

            _row.DISP_REASON_OTHER = _DISP_REASON_OTHER;
            _row.DISP_REASON = _DISP_REASON;
            control.DataTable.row(row).data(_row).draw(false);
        });
       
    },

    DownloadTemplate: function (callback) {
        //Under implement
    },
    ValidateUploadExcel: function () {
        return fAction.ValidateAddAssetDetail();
    },
    UploadExcel: function (_fileName) {
        //call Batch with filename
        var batch = BatchProcess({
            BatchId: "BFD02511",
            Description: "Upload Dispose Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'RequestAssetRetirement', ($('#WFD02510_DISPOSAL_BY_DONATE').is(':checked')) ? 'DONATE' : ($('#WFD02510_DISPOSAL_BY_SALE').is(':checked')) ? 'SALES' : ($('#WFD02510_DISPOSAL_BY_SCRAP').is(':checked') ? "SCRAP" : ''));
        batch.Addparam(5, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function (_appID, _Status) {

            if (_Status != 'S') {
                return;
            }
            fAction.LoadData();
        },true);
    },
 
    PrepareGenerateFlow: function (callback) {

        fAction.UpdateAsset(function () {
            ajax_method.Post(Url.PrepareFlow, { data: GLOBAL }, false, function (result) {
                if (result.IsError) {
                    return;
                }
                if (callback == null)
                    return;

                callback();
            }); //ajax_method.PrepareFlow
        }); //ajax_method.UpdateAssetList
    },

    PrepareSubmit: function (callback) {

        fAction.UpdateAsset(callback); 
    },
    PrepareApprove: function (callback) {
        fAction.UpdateAsset(callback); 
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },
    ValidateAddAssetDetail: function () {
        ClearMessageC();

        var _msg = [{ Type: 3, Messages: [] }];
        if (fScreenVal.RouteType() == null || fScreenVal.RouteType() == "") {
            _msg[0].Messages.push("MSTD0031AERR : Approval Route Type should not be empty.");
        }
        if (fScreenVal.dispType() == null || fScreenVal.dispType() == "") {
            _msg[0].Messages.push("MSTD0031AERR : Request asset retirement should not be empty.");
        }
        if (_msg[0].Messages.length > 0) {
            AlertMessageC(_msg);
            return false;
        }

        return true;
    },
    ShowSearchPopup: function (callback) {
        if (!fAction.ValidateAddAssetDetail()) {
            return;
        }

         //Set search parameter
        fAction.UpdateAsset(function () { //Update first.
            f2190SearchAction.ClearDefault();

            Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();

            Search2190Config.searchOption = "D";
            Search2190Config.AllowMultiple = true;

            Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
            Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
            Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedRespCostCenter, Enable: fAction.SelectedRespCostCenter == "" };


            //Assign Default and Initial screen
            f2190SearchAction.Initialize();

            $('#WFD02190_SearchAssets').modal();
        }); // UpdateAsset
    },

    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");

        var _header = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            COMPANY: GLOBAL.COMPANY,
            DISP_TYPE: fScreenVal.dispType(),
            DISP_MASS_REASON: $('#WFD02510_DISP_MASS_REASON').val(),
            ROUTE_TYPE: fScreenVal.RouteType(),
            CURRENT_APPR_INDX: PERMISSION.ActionRoleIndex
        }

        if (control.DataTable == null) {
            console.log("NULL");

            return _header;
        }
        // var table = $('#tbSearchResult').DataTable();
        var _assetList = []; //array List

        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }
            

            _row = control.DataTable.row(this).data(); // Get source data

            if (_requireCheck && !_row.SEL)
                return;
            //var _idReason = '#WFD02510_RESON_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
            var _idReason = '#WFD02510_RESON_' + _row.COMPANY + "_" + _row.ASSET_NO + "_" + _row.ASSET_SUB;

            

            _assetList.push({
                GUID: GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                COMPANY: _row.COMPANY,
                ASSET_NO: _row.ASSET_NO,
                ASSET_SUB: _row.ASSET_SUB,
                COST: _row.COST,
                BOOK_VALUE: _row.BOOK_VALUE,

                RETIRE: _row.RETIRE,
                RETIRE_COST:_row.RETIRE_COST,
                RETIRE_BOOK_VALUE: _row.RETIRE_BOOK_VALUE,

                DISP_REASON: $(_idReason).val() ,
                DISP_REASON_OTHER: _row.DISP_REASON_OTHER,

                RETIRE_TYPE: _row.RETIRE_TYPE,

                DISP_TYPE: fScreenVal.dispType(),
                ROUTE_TYPE : fScreenVal.RouteType(),
                BOI_NO: _row.BOI_NO,
                INVEST_REASON: _row.INVEST_REASON,
                
                DETAIL_DATE: _row.DETAIL_DATE,
                DETAIL_REMARK: _row.DETAIL_REMARK,
                SOLD_TO: _row.SOLD_TO,
                PRICE: _row.PRICE,
                INVOICE_NO: _row.INVOICE_NO,
                REMOVAL_COST: _row.REMOVAL_COST,
                ADJ_TYPE: _row.ADJ_TYPE,
                RETIRE_QTY: _row.RETIRE_QTY,
                EDIT_FLAG: _row.EDIT_FLAG,
                USER_MISTAKE: _row.USER_MISTAKE,
                PHOTO_ATTACH: _row.PHOTO_ATTACH,
                COMPENSATION: _row.COMPENSATION,
                LOSS_COUNTER_MEASURE: _row.LOSS_COUNTER_MEASURE
            });
        });

        _header.DetailList =  _assetList ;
       
        


        return _header;

    },

    SetDisplayDisposalType: function (button) {
        
        control.selectedRow = fAction.GetSelectedRow(button);
        var row = control.DataTable.row(control.selectedRow).data();

        
        var _idReson = 'WFD02510_RESON_' + row.COMPANY + "_" + row.ASSET_NO + "_" + row.ASSET_SUB;
        var _id = 'WFD02510_MEASURE_' + row.COMPANY + "_" + row.ASSET_NO + "_" + row.ASSET_SUB;

        if ($('#' + _idReson).val() == 'LOSS') {
            $('#' + _id).css("visibility", "visible");
        } else {
            $('#' + _id).css("visibility", "hidden");
        }

        row.DISP_REASON = $('#' + _idReson).val();
        control.DataTable.row(control.selectedRow).data(row);
        if ($('#' + _idReson).val() == 'OTHER') {
            fAction.SetOtherReason(row);
        }
    },

    SetOtherReason: function (_result) {
        //debugger;
        var _DISP_REASON_OTHER = _result["DISP_REASON_OTHER"];
        control.WFD02510MessageAlertReasonOther.hide();
        control.WFD02510_SetOtherReason.appendTo("body").modal('show');
        TempFileWFD02510 = [];
        control.WFD02510_DISP_REASON_OTHER.val(_DISP_REASON_OTHER);
        

    },

    SetEnableDisposalReason: function () {
        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() != "" && row.find('td:eq(1)').text() != null) {
                var _rowData = [];
                if (control.DataTable != null) {
                    _rowData = control.DataTable.row(this).data();
                    var _idReson = 'WFD02510_RESON_' + _rowData["COMPANY"] + "_" + _rowData["ASSET_NO"] + "_" + _rowData["ASSET_SUB"];
                    $('#' + _idReson).addClass("require");
                    $('#' + _idReson).prop("disabled", false);
                }
            }
        });
    },

    setEnableButton: function (id) {
        $(id).removeClass('disabled');
        $(id).prop('disabled', false);
    },

    setDisableButton: function (id) {
        $(id).addClass('disabled');
        $(id).removeClass('active');
        $(id).removeClass('require');
        $(id).prop('disabled', true);
    },

    setControlsRequiredField: function (controls, isRequired) {
        for (var i = 0; i < controls.length; i++) {
            console.log($(controls[i]));
            if (isRequired == true) {
                if ($(controls[i]).hasClass('select2')) {
                    $(controls[i]).Select2Require(true);
                } else {
                    $(controls[i]).addClass('require');
                }
            } else {
                if ($(controls[i]).hasClass('select2')) {
                    $(controls[i]).Select2Require(false);
                } else {
                    $(controls[i]).removeClass("require");
                }
            }
        }
    },

    ClickUploadFile: function (_type, _DivShowFile, _alert) {
        if ($('#' + _DivShowFile + ' div.attach').length >= controlUpload.FILE_LIMIT) {
            AlertTextErrorMessagePopup("Not allow upload greater than {0} files".format(controlUpload.FILE_LIMIT), "#"+_alert);
            return;
        }

        var _ControlFile = '';
        switch (_type) {
            case 'LC':
                _ControlFile = 'WFD02510_ATTACH_COUNTERMEASURE_FILE';
                break;
            case 'MD':
                _ControlFile = 'WFD02510_DISP_MASS_ATTACH_DOC_FILE';
                break;
            case 'MM':
                _ControlFile = 'WFD02510_MINUTE_OF_BIDDING_FILE';
                break;
            case 'SCRAP':
                _ControlFile = 'WFD02510_SCRAP_INFO_FILE';
                break;
            case 'DONATE':
                _ControlFile = 'WFD02510_DONATE_INFO_FILE';
                break;
        }
        document.getElementById(_ControlFile).click();
    },

    SetButtonAndUploadFile: function (_ControlFile, _type, _DivShowFile) {
        if (document.getElementById(_ControlFile).value != '' && document.getElementById(_ControlFile).value != null) {
            fUpload251.SaveFileToServer(_ControlFile, _type, _DivShowFile);
        }

        $('#' + _ControlFile).val('');
    },
    EnableMassMode: function (_Role) {
        //
        // Suraasith T. allow for all role
        ajax_method.Post('/WFD02510/WFD02510_IsMassPermission', { ACT_ROLE: _Role }, false, function (result) {
            if (!result.IsError) {
                $('#WFD02510_MASS_DISPOSAL').prop("disabled", false);
                
                control.EnableMass = true;
                return;
            }
            $('#WFD02510_MASS_DISPOSAL').prop("checked", false);
            $('#WFD02510_MASS_DISPOSAL').prop("disabled", true);
            $('#WFD02510_NORMAL_DISPOSAL').prop("checked", true);
            control.EnableMass = true;
        });
    },
    
    UpdateMassReason: function () {
        var _massReason = $('#WFD02510_DISP_MASS_REASON').val();

        $("#tbSearchResult tr").each(function () {


            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }

            var _row = [];

            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data

            _row.DISP_REASON = _massReason;
            control.DataTable.row(row).data(_row).draw(false);
        });
    },

    SetUploadButton:function(sender, _isUpload){
        if(_isUpload)
            $(sender).html('<i class="fa fa-upload"></i>  Upload ');
        else
            $(sender).html('<i class="fa fa-download"></i> Download');
    },
    GenerateFlow2510: function () {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(MainUrl.GenerateFlow, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                // alert(result.Message);
                return;
            }

            //Display
            $('#WFD01170Approver').html(result.Message);

            $('#WFD01170btnApprove, #WFD01170btnReject').prop("disabled", false);
           
        });
    },
    DetailCheck: function (sender) {
        var row = $(sender).closest("tr");
        var _row = control.DataTable.row(row).data();
        _row.SEL = $(sender).is(':checked');

        control.DataTable.row(row).data(_row).draw(false);

        $('#WFD02510_SelectAll').prop('checked', _act = $('.chkGEN:checked').length == $('.chkGEN').length);
    },
    Resend: function (callback) {
        var _header = fMainAction.GetParameter();
        var _data = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, { header: _header, data: _data }, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList -> Send


    },
    
    onDisposaleTypeChange: function (value) {
        fAction.initDisposalReasonCombo();

        if (control.DataTable) {
            var tableData = control.DataTable.data();
            ////Re draw all row
            $.each(tableData, function (i, tableRow) {
                //ALLOW_EDIT 
                if (tableRow.ALLOW_EDIT == "Y") {
                    control.DataTable.row(i).data(tableRow).draw(false);
                }
            });
        }
    }
}

function getDisposalReason() {
    var disposaleType = fScreenVal.dispType();
    var reasons = [];

    //Filter from disposaleType
    $.each(ArrDisposalReason, function (i, value) {
        var keys = [];
        if (value.REMARKS) {
            keys = value.REMARKS.split("|");
        }

        if (!disposaleType || keys.indexOf(disposaleType) !== -1) {
            reasons.push(value);
        }
    });

    return reasons;
}

var fRender = {
    CheckBoxRender: function (data) {
        var _strHtml = '';
        
        var _disabled = (data.AllowCheck ? '' : " disabled ");

        var _strChecked = data.SEL ? '  checked ' : '' ;
        _strHtml = '<input style="width:20px; height:28px;" name="chkGEN" class="chkGEN" value="' + data.ASSET_NO + '#' + data.ASSET_SUB + '" onclick="fAction.DetailCheck(this);" type="checkbox" ' + _disabled + _strChecked + ' >';
        return _strHtml;
    },
    HyperLinkRender: function (data) {

        var _text = data.ASSET_NO;
        if (data.IS_MACHINE_LICENSE == "Y") {
            _text = data.ASSET_NO + "<span style=\"color:red;font-weight:bold\">*</span>";
        }

        return '<a href="../WFD02130?COMPANY=' + data.COMPANY + '&ASSET_NO=' + data.ASSET_NO + '&ASSET_SUB=' + data.ASSET_SUB + '" target="_blank">' + _text + '</a>';
    },
    TextBoxRender: function (data) {
        var _disabled = (data.AllowEdit ? '' : "disabled");
        return '<input type="text" class="form-control" value="' + data.REASON + '" ' + _disabled + ' />'
    },
    LabelRetireRender: function (data) {
        return '<h3><span class="w-100 label label-success">' + data.RETIRE + '</span></h3>'
    },
    datepickerRender: function (data) {
        return '<input type="text" onfocus="showCalendarControl(this);" style="cursor:pointer;" value="' + data.DATE_IN_SERVICE + '"/>';
    },

    ButtonRetireRender: function (data) {
        console.log(data);

        var _disabled = ( (PERMISSION.AllowResend == 'Y' && data.AllowEdit) || ( PERMISSION.AllowEditBeforeGenFlow && data.AllowEdit) ? '' : "disabled");

        var _text = '';
        if (data.RETIRE != null) {
            _text = data.RETIRE;
        }

        var _partial = "btn-info";

        if (data.ADJ_TYPE != '' && data.ADJ_TYPE != null && data.ADJ_TYPE != 'null') {
            if (data.ADJ_TYPE == 'Q') {
                _text = data.RETIRE_QTY + "/" + data.QTY;
                if (data.RETIRE_QTY < data.QTY) _partial = "btn-warning";
            }
            else if (data.ADJ_TYPE == 'P') {
                var costPercentage = (data.COST > 0 ? (data.RETIRE_COST / data.COST) * 100 : 0).toFixed(2);
                _text = costPercentage + "%";

                if (costPercentage < 100) _partial = "btn-warning";
            }

        }
        else {
            var costPercentage = (data.COST > 0 ? (data.RETIRE_COST / data.COST) * 100 : 0).toFixed(2);
            _text = costPercentage + "%";

        }

        var _id = 'WFD02510_ButtonRetire_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<div class="btn-group">' +
                '<button type="button" ' + _disabled + ' class="btn btn-block ' + _partial + ' retire" data-toggle="modal" data-target="#myModalCost" style = "width: 100%;" onclick="fPopup.GetRetirePopup(this)">' +
                '<span class="glyphicon" id="' + _id + '">' + _text + '</span></button>' +
                '</div>';
    },
    LabelRender: function (data, Type, value) {
        console.log(data);
        var _id = 'WFD02510_' + Type + '_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<div class="btn-group">' +
                '<label class="control-label" id="' + _id + '">' + value + '</label>' +
                '</div>';
    },
    ButtonPhotoRender: function (data) {
        console.log(data);
        var _text = data.PHOTO_ATTACH;
        if (data.PHOTO_ATTACH != null) {
            _text = data.PHOTO_ATTACH;
        }

        //return '<div class="btn-group">' +
        //        '<button type="button" class="btn btn-info" onclick="fAction.ShowCostDialog(this,\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\');return false;">' +
        //        '<span class="glyphicon glyphicon-search"></span></button>' +
        //        '</div>';
        return '<div class="btn-group">' +
        '<button type="button" class="btn btn-info" style = "width: 100%;" onclick="fPopup.ShowDetailUploadDialog(\'Upload Photo\',this,\'PHO\')">' +
        '<span class="glyphicon glyphicon-search"></span></button>' +
        '</div>';
    },

    ButtonMeasureRender: function (data) {
        console.log(data);
        var _text = data.USER_MISTAKE;
        if (data.USER_MISTAKE != null) {
            _text = data.USER_MISTAKE;
        }
        //var _idReson = 'WFD02510_RESON_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        var _id = 'WFD02510_MEASURE_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;

        var _strHtml = '';
        if (data.DISP_REASON == 'LOSS') {
            _strHtml = '<div class="btn-group">' +
            '<button type="button"  id="' + _id + '" class="btn btn-info" onclick="fPopup.ShowLossCaseDialog(this)" style = "width: 100%;" >' +
            '<span class="glyphicon glyphicon-search"></span></button>' +
            '</div>';
        } else {
            _strHtml = '<div class="btn-group">' +
           '<button type="button" id="' + _id + '" class="btn btn-info" onclick="fPopup.ShowLossCaseDialog(this)" style = "width: 100%; visibility: hidden;" >' +
           '<span class="glyphicon glyphicon-search"></span></button>' +
           '</div>';
        }
        return _strHtml;

    },
    ButtonBoiDocumentRender: function (data) {
        console.log(data);
        var _text = data.BOI_ATTACH_DOC;
        if (data.BOI_ATTACH_DOC != null) {
            _text = data.BOI_ATTACH_DOC;
        }

        return '<div class="btn-group">' +
        '<button type="button" class="btn btn-info" style = "width: 100%;" onclick="fPopup.ShowBOIUploadDialog(this)">' +
        '<span class="glyphicon glyphicon-search"></span></button>' +
        '</div>';
    },    
    ComboReasonRender: function (data) {
        var comboData = getDisposalReason();

        var _id = 'WFD02510_RESON_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        var _AssetNo = data.ASSET_NO;
        //var _CostCode = data.COST_CODE;
        var _ValueChoosed = data.DISP_REASON;
        var _Require = data.REQUIRE_FLAG_DISP_REASON;
        var _Disable = data.DISABLE_DISP_REASON;
       
        var _strDisplayTooltip = '';

        var _disabled = (data.AllowEdit ? '' : "disabled");

        var _strHtmlDDL = '<select id=\'' + _id + '\'" ' + _disabled + ' class="Reason form-control select2" style="width:100%;" onchange="fAction.SetDisplayDisposalType(this);" >';
        if (comboData.length > 0) {
            _strHtmlDDL = _strHtmlDDL + '<option value="" title="Select">Select</option>';
            for (i = 0; i < comboData.length; i++) {
                if (comboData[i].CODE == _ValueChoosed) {
                    _strHtmlDDL = _strHtmlDDL + '<option selected="selected" value="' + comboData[i].CODE + '" title="' + comboData[i].VALUE + '">' + comboData[i].VALUE + '</option>';
                    if ((comboData[i].CODE != 'OTHER' && comboData[i].CODE != 'LOSS') && (comboData[i].VALUE.length != null && comboData[i].VALUE.length > 20)) {
                        _strDisplayTooltip = '<p title="" data-original-title="' + comboData[i].VALUE + '" data-toggle="tooltip" data-placement="top"> ';
                    }
                } else {
                    _strHtmlDDL = _strHtmlDDL + '<option value="' + comboData[i].CODE + '" title="' + comboData[i].VALUE + '">' + comboData[i].VALUE + '</option>';
                }
            }

        }
        if (_strDisplayTooltip != '') {
            _strHtmlDDL = _strDisplayTooltip + _strHtmlDDL + '</p>';
        }
        return _strHtmlDDL;
    },

    

    ButtonInfoRender: function (data, type, row) {
        console.log(data);
        var _strHtml = '';

        var _disabled = " disabled"; //(data.ENABLE_DETAIL && PERMISSION.ActionMark.indexOf(constants.TMC) >= 0) ? "" : " disabled ";

        if (!data.ENABLE_DETAIL) {

            //2019.12.15 //&& $('#WFD01170ddpRequestorRole').val() == GLOBAL.ACRCode
            if ($('#WFD02510_MASS_DISPOSAL').is(':checked'))
            {
                data.ENABLE_DETAIL = true;
            }
            else
            {
                return '';
            }
        }

        if (data.ENABLE_DETAIL) {
            _disabled = '';
        }
        
        var _color = "btn btn-info";
        if (data.ROW_STATE > PERMISSION.ActionRoleIndex)
        {
            _color = "btn btn-success";
        }
        if (data.ROW_STATE == PERMISSION.ActionRoleIndex) {
            _color = "btn btn-info";
        }
        if (data.ROW_STATE < PERMISSION.ActionRoleIndex) {
            _color = "btn btn-warning";
        }
        var _id = 'WFD02510_DETAIL_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;

        _strHtml = '<div class="btn-group">' +
            '<button type="button"  id="' + _id + '" class="' + _color + '"' + _disabled + ' onclick="fPopup.ShowDetail(this);" style = "width: 100%;" >' +
            'Detail</button>' +
            '</div>';

        return _strHtml;
    },

    DeleteButtonRender: function (data) {

        var _hide = (data.AllowDelete ? '' : "hide");
        var _disbled = (PERMISSION.AllowEditBeforeGenFlow ? '' : "disabled");
        var _strHtml = '';
        var _id = "btnDelete_" + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="' + _id + '" class="btn btn-danger btn-xs delete ' + _hide + '" data-title="Delete" type="button" ' + _disbled + ' ' +
                'onclick=fAction.DeleteAsset(\'' +
                 data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\')>' +
                 '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },


}

var fScreenVal = {

    dispType: function () {
        var _type = '';
        if ($('#WFD02510_DISPOSAL_BY_SALE').is(':checked')) {
            _type = $('#WFD02510_DISPOSAL_BY_SALE').val();
        }
        if ($('#WFD02510_DISPOSAL_BY_SCRAP').is(':checked')) {
            _type = $('#WFD02510_DISPOSAL_BY_SCRAP').val();
        }
        if ($('#WFD02510_DISPOSAL_BY_DONATE').is(':checked')) {
            _type = $('#WFD02510_DISPOSAL_BY_DONATE').val();
        }

        return _type;
    },
    RouteType: function () {
        var _type = '';
        if ($('#WFD02510_NORMAL_DISPOSAL').is(':checked')) {
            _type = $('#WFD02510_NORMAL_DISPOSAL').val();
        }
        if ($('#WFD02510_MASS_DISPOSAL').is(':checked')) {
            _type = $('#WFD02510_MASS_DISPOSAL').val();
        }
        return _type;
    },

    commaSeparateNumber: function (val) {
        if (val != null) {
            val = currencyFormat(val);
            //while (/(\d+)(\d{3})/.test(val.toString())) {
            //    val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            //}
        }
        return val;
    }
}

var fUpload251 = {
    DeleteFileTemp: function (_FileId, _FileName, _FileType, _Type) {
        var _row = control.DataTable.row(control.selectedRow).data();
        var model = new FormData();
        model.append('DOC_NO', GLOBAL.DOC_NO);
        model.append('GUID', GLOBAL.GUID);
        model.append('LINE_NO', 0);
        model.append('ASSET_NO', _row.ASSET_NO);
        model.append('ASSET_SUB', _row.ASSET_SUB);
        model.append('COMPANY', $('#WFD01170ddpCompany').val());
        model.append('TYPE', _Type);
        model.append('FUNCTION_ID', "WFD02510");
        model.append('DOC_UPLOAD', "WFD02510" + '_' + controlUpload.TYPE);
        model.append('TEMP_FILE_DOC_NAME', _FileId);
        model.append('FILE_NAME', _FileName);
        model.append('FILE_TYPE', _FileType);
        ajax_method.PostFile(URLComUpload.DeleteFileTemp, model, function (result) {
            if (result.ObjectResult) {// true 
                var id = '#' + _FileId;
                //$(id).removeClass('col-md-2');
                $(id).remove();
                //debugger;
                for (var i = 0; i < TempFileWFD02510.length; i++) {
                    if (TempFileWFD02510[i].FILE_ID == _FileId) {
                        TempFileWFD02510.splice(i, 1);
                    }
                }
            }
        }, null);
    },

    RenderFileUpload: function (result, _flagDel, _DivShowFile, _Type) {
        //console.log(result.ObjectResult.data);
        var data = result;
        var _DivFileList = document.getElementById(_DivShowFile);//DivWFD02510LossCaseFileList
        //_DivFileList.innerHTML = "";
        //var _row = document.createElement('div');
        //_row.className = 'row';

        var _divThumbnail = document.createElement('div');
        _divThumbnail.className = 'thumbnail';

        var _divFile = document.createElement('div');
        _divFile.className = 'col-md-2';

        var _divImgHtml = '';
        var _del = '';

        if (data != null) {

            for (i = 0; i < data.length; i++) {
                //if (i % 6 == 0) {
                //    _DivFileList.appendChild(_row);
                //    _row = document.createElement('div');
                //    _row.className = 'row';
                //}

                _divFile = document.createElement('div');
                _divFile.className = 'col-md-3 attach';
                _divFile.style = 'padding: 5px;';

                _divThumbnail = document.createElement('div');
                _divThumbnail.className = 'thumbnail';
                _divFile.id = data[i].FILE_ID;

                _divImgHtml = '<a href="' + data[i].FILE_PATH_DOWNLOAD + '" target="_blank">' +
                    '<img title="' + data[i].FILE_NAME_ORIGINAL + '" src="' + data[i].PATH_FILE_TYPE + '" alt="Lights" style="width:100%"/>' +
                    '</a>';
                if (_flagDel == 'Y') {
                    _del =
                        '<div class="caption" style="text-align:right">' +
                        '<i class="fa fa-remove" onclick="fUpload251.DeleteFileTemp(\'' + data[i].FILE_ID + '\',\'' + data[i].FILE_NAME + '\',\'' + data[i].FILE_TYPE + '\',\'' + _Type + '\');" ></i></button>' +
                        '</div>';
                } else {
                    _del = '';
                }

                _divImgHtml = _divImgHtml + _del;

                _divThumbnail.innerHTML = _divImgHtml;

                _divFile.appendChild(_divThumbnail);

                _DivFileList.appendChild(_divFile);
            }
            // _DivFileList.appendChild(_row);
        }
    },

    SaveFileToServer: function (_ControlFile, _type, _DivShowFile) {

        var _row = control.DataTable.row(control.selectedRow).data();

        var fileInput = document.getElementById(_ControlFile);

        var _existsCnt = $("#"+_DivShowFile +' div.attach').length;

        if (_existsCnt + fileInput.files.length > 5) {
            AlertTextErrorMessagePopup("Not allow upload greater than {0} files".format(5), "#AlertMessageCommonUpload");
            return;
        }

        var model = new FormData();
       
        model.append('DOC_NO', GLOBAL.DOC_NO);
        model.append('GUID', GLOBAL.GUID);//Start
        model.append('LINE_NO', 0);
        model.append('ASSET_NO', _row.ASSET_NO);
        model.append('ASSET_SUB', _row.ASSET_SUB);
        model.append('COMPANY', $('#WFD01170ddpCompany').val());
        model.append('USER_BY', GLOBAL.USER_BY);
        model.append('TYPE', _type);
        model.append('FUNCTION_ID', "WFD02510");
        model.append('DOC_UPLOAD', "WFD02510" + "_" + _type);
        model.append('DISP_TYPE', fScreenVal.dispType());
        if (fileInput.files.length > 0) {
            model.append('TEMP_FILE_DOC', fileInput.files[0]);
            ajax_method.PostMultiFile(URLComUpload.CheckUploadFile, model, function (result) {
                if (result.ObjectResult) {// true 
                    //debugger;
                    ajax_method.PostMultiFile(URLComUpload.UploadAssetFile, model, function (result) {
                        //debugger;
                        fUpload251.RenderFileUpload(result.ObjectResult.data, 'Y', _DivShowFile, _type);
                        if (TempFileWFD02510 != null && TempFileWFD02510.length > 0) {
                            TempFileWFD02510.push.apply(TempFileWFD02510, result.ObjectResult.data);
                            //TempFileInsert.push(result.ObjectResult.data);
                        } else {
                            TempFileWFD02510 = result.ObjectResult.data;
                        }
                        $('#' + _ControlFile).val('');
                    }, null);
                }
            }, null);
        }
    },

    GetFileFromDB: function (_DivShowFile, _Type, row, enable) {
        var paraUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _Type,
            FUNCTION_ID: "WFD02510",
            DOC_UPLOAD: "WFD02510" + "_" + _Type
        }
        var _DivFileList = document.getElementById(_DivShowFile);
        _DivFileList.innerHTML = "";
        ajax_method.Post(URLComUpload.GetFileAttchDoc, paraUpload, false, function (result) {
            //debugger;
            if (result.data != null) {
                fUpload251.RenderFileUpload(result.data, enable ? 'Y' : 'N', _DivShowFile, _Type);

            }
        }, null);
    },
}

var fScreenMode = {

    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        $('#WFD02510_SelectAll').prop("disabled", true);
        $('#WFD02510_SelectAll').hide();

        $('#WFD02510MassDisposal').hide();

        $('#WFD02510_DISPOSAL_BY_SALE, #WFD02510_DISPOSAL_BY_SCRAP, #WFD02510_DISPOSAL_BY_DONATE').prop('disabled', true);
        $('#WFD02510_NORMAL_DISPOSAL, #WFD02510_MASS_DISPOSAL').prop('disabled', true);
        $('#WFD02510_DISP_MASS_REASON').prop('disabled', true);
        $('#DivTMCDocument').hide();
    },

    
    Submit_InitialMode: function (callback) {

        fScreenMode._ClearMode();
       // fScreenMode._GridLayout_Submit();

        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd').prop("disabled", false);

        $('button.delete, button.boi, button.detail').prop("disabled", true);
        $('input.datepicker, input.chkGEN, select.reason').prop("disabled", true);

        
        $('#WFD02510_DISPOSAL_BY_SALE, #WFD02510_DISPOSAL_BY_SCRAP, #WFD02510_DISPOSAL_BY_DONATE').prop('disabled', false);
        $('#WFD02510_NORMAL_DISPOSAL').prop('disabled', false);
        if(control.EnableMass)
            $('#WFD02510_MASS_DISPOSAL').prop('disabled', false);

        // Remove becuase it's clear when user remove all assets, (It'should not clear)
        // $('#WFD02510_DISPOSAL_BY_SALE, #WFD02510_DISPOSAL_BY_SCRAP, #WFD02510_DISPOSAL_BY_DONATE').prop('checked', false);

        $('#WFD02510_MASS_DISPOSAL').prop("checked", false);
        $('#WFD02510_NORMAL_DISPOSAL').prop('checked', true);

        $('#WFD02510_DISP_MASS_REASON').prop('disabled', true);

        $('#WFD02510MassDisposal').show();
        
        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {

        fScreenMode._ClearMode();

        $('button.retire, button.delete').prop('disabled', false);

        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", false);

        //2019.12.15, requestor must select type befure add asset
        $('#WFD02510_DISPOSAL_BY_SALE, #WFD02510_DISPOSAL_BY_SCRAP, #WFD02510_DISPOSAL_BY_DONATE').prop('disabled', true);
        $('#WFD02510_NORMAL_DISPOSAL').prop('disabled', true);

        if (control.EnableMass)
            $('#WFD02510_MASS_DISPOSAL').prop('disabled', true);

        $('#WFD02510MassDisposal').show();

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {

        fScreenMode._ClearMode();
        
        $('button.retire, button.delete').prop('disabled', true);

        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').prop("disabled", false);

        $('#WFD02510MassDisposal').show();

        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) {
        fScreenMode._ClearMode();
        console.log("#Approve_Mode");
        //Document

        $('#WFD02510MassDisposal').show();

        if (PERMISSION.AllowResend == "Y") {
            $('#WFD02510_SelectAll').show();
            $('#WFD02510_SelectAll').removeClass('disabled').prop("disabled", false);

        }
        else {
            $('#WFD02510_SelectAll').hide();
            $('#WFD02510_SelectAll').prop("disabled", true);

        }


        // $('#WFD02510DisposalType').hide(); 2020.01.25 IS) Sunon confirm show all mode same as transfer

        $('#btnRequestExport').show();
        $('#btnRequestExport').removeClass('disabled').prop("disabled", false);

        control.AllowUploadTMC = PERMISSION.ActionMark.indexOf(constants.TMC) >= 0 ;

        if ((control.TMCAttachCount > 0) || (control.AllowUploadTMC && (PERMISSION.IsFSUser == "Y" || PERMISSION.IsAECUser == "Y") )) { //FS Mode and View Mode
            fAction.SetUploadButton('#WFD02510btnUploadTMCDocument', control.AllowUploadTMC);           
            $('#DivTMCDocument').show();
        }
        control.AllowChangeFlow = PERMISSION.ActionMark.indexOf(constants.CHANGE_FLOW) == 0 ;
        
        if (control.AllowChangeFlow) {
            $('#WFD02510_DISPOSAL_BY_SALE, #WFD02510_DISPOSAL_BY_SCRAP').prop("disabled", false);
            //Allow to gen flow
            
        }
        
        if (callback == null)
            return;
        callback();
    },
   
    Approve_Mode_RegenFlow: function () {
        if (!control.AllowChangeFlow) {
            return;
        }

       // $("#WFD01170btnGenerateApprover, #WFD01170btnResetApprover").unbind("click");

        $('#WFD02510MassDisposal').show();

        //Override parent
        $('#WFD01170btnGenerateApprover').show();
        $('#WFD01170btnResetApprover').show();

        $('#WFD01170btnGenerateApprover').removeClass('disabled').prop("disabled", false);
        $('#WFD01170btnResetApprover').prop("disabled", true);

        $('#WFD01170btnApprove').prop("disabled", true);
        $('#WFD01170btnReject').prop("disabled", true);

        $('#WFD01170btnGenerateApprover').click(function () {
            fAction.PrepareGenerateFlow(function () {
                fAction.GenerateFlow2510();
            });
            
        });

        
    }
}

$('#btnRetireSave').click(function (e) {

    if ($('#myModalCost input:checked').length == 0) {
        AlertTextErrorMessagePopup('MCOM0017AERR : Please select one of choice', "#AlertMessageWFD02510");
        return;
    }

    var adjType = _GetAdjType();
    var _row = control.DataTable.row(control.selectedRow).data();

    if (adjType == 'Q') {
        if ($('#WFD02510_Retire_Quantity_TXT').val() == "") {
            var _msg = "MSTD0031AERR :  {0} should not be empty".replace('{0}', 'Quantity');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02510");
            $('#WFD02510_Retire_Quantity_TXT').focus();
            return;
        }
        else if (_row.QTY > 0 && (parseFloat($('#WFD02510_Retire_Quantity_TXT').val()) <= 0.000)) {
            var _msg = "MSTD0031AERR :  {0} should be more than 0.000".replace('{0}', 'Quantity');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02510");
            $('#WFD02510_Retire_Quantity_TXT').focus();
            return;
        }
    } else {
        if ($('#WFD02510_Retire_Percentage_TXT').val() == "") {
            var _msg = "MSTD0031AERR :  {0} should not be empty".replace('{0}', 'Percentage');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02510");
            $('#WFD02510_Retire_Percentage_TXT').focus();
            return;
        }
        else if (parseFloat($('#WFD02510_Retire_Percentage_TXT').val()) <= 0.00) {
            var _msg = "MSTD0031AERR :  {0} should be more than 0.00".replace('{0}', 'Percentage');
            AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02510");
            $('#WFD02510_Retire_Percentage_TXT').focus();
            return;
        }
    }
    fAction.UpdateRetire(function () {
        $('#myModalCost').modal('hide');
    });
})

$('#btnRetireCancel').click(function (e) {
    $('#myModalCost').modal('hide');
})



$('#WFD02510_Retire_Quantity').click(function (e) {

    $('#WFD02510_Retire_Percentage_TXT, #WFD02510_Retire_Quantity_TXT').val('').prop('disabled', true);

    if ($('#WFD02510_Retire_Quantity').is(':checked')) {
        $('#WFD02510_Retire_Percentage').prop('checked', false);
        $('#WFD02510_Retire_Quantity_TXT').prop('disabled', false);
    };

});

$('#WFD02510_Retire_Percentage').click(function (e) {

    $('#WFD02510_Retire_Percentage_TXT, #WFD02510_Retire_Quantity_TXT').val('').prop('disabled', true);

    if ($('#WFD02510_Retire_Percentage').is(':checked')) {
        $('#WFD02510_Retire_Quantity').prop('checked', false);
        $('#WFD02510_Retire_Percentage_TXT').prop('disabled', false);
    };

});

$('#WFD02510_DISP_REASON_OTHER_SAVE').click(function (e) {
    fAction.UpdateReasonOther();
    control.WFD02510_SetOtherReason.modal('hide');
})

$('#WFD02510_DISP_REASON_OTHER_CANCEL').click(function (e) {
    control.WFD02510_SetOtherReason.modal('hide');
})

$('#btnDonationInfoSave').click(function (e) {

    ////Validate On Server
    //if ($('#WFD02510_DONATION_DATE').val() == "") {
    //    var _msg = "MSTD0031AERR :  {0} should not be empty".replace('{0}', 'Percentage');
    //    AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02510Donate");
    //    $('#WFD02510_DONATION_DATE').focus();
    //    return;
    //}
    //if ($("div [id^='DONATE']").length == 0)
    //{
    //    var _msg = "MSTD0031AERR :  {0} should not be empty".replace('{0}', 'Donate Attachment file');
    //    AlertTextErrorMessagePopup(_msg, "#AlertMessageWFD02510Donate");
    //    return;
    //}
  
        //WFD02510_DONATE_INFO_FILE 
    fUpload.InsertFileCallBack(TempFileWFD02510, function (_fileName) {
        fAction.UpdateDetailInfo(constants.Donat);
        $('#divDonationInfo').modal('hide');
    });
    
})

$('#btnScrappingInfoSave').click(function (e) {

    fUpload.InsertFileCallBack(TempFileWFD02510, function (_fileName) {
       fAction.UpdateDetailInfo(constants.Scrap);
        $('#divScrappingInfo').modal('hide');
    });

    
})

$('#btnSalesInfoSave').click(function (e) {
    fAction.UpdateDetailInfo(constants.Sale);
    $('#divSalesInfo').modal('hide');
})

$('#btnScrappingInfoUpload').click(function (e) {
    fAction.ClickUploadFile('SCRAP', 'DivWFD02510ScrappingInfoFileList', 'AlertMessageWFD02510Scrap');
})

$('#btnDonationInfoUpload').click(function (e) {
    fAction.ClickUploadFile('DONATE', 'DivWFD02510DonationInfoFileList', 'AlertMessageWFD02510Donate');
})

$('#btnMeasureSave').click(function (e) {
    //Validation
    if (fAction.ValidateUpdateCM()) {
        fUpload.InsertFileCallBack(TempFileWFD02510, function (_fileName) {
            fAction.UpdateCM(_fileName);
        });
    }
})



$('#WFD02510_NORMAL_DISPOSAL').click(function (e) {
    if ($(this).is(':checked')) {
        $('#WFD02510_MASS_DISPOSAL').prop('checked', false);
        //$('#WFD02510MassDisposal').hide();
    }
})
$('#WFD02510_MASS_DISPOSAL').click(function (e) {

    //$('#WFD02510MassDisposal').hide();
    if ($(this).is(':checked')) {
        $('#WFD02510_NORMAL_DISPOSAL').prop('checked', false);

        //Show
        //$('#WFD02510MassDisposal').show();
    }
})
$('#WFD02510_DISP_MASS_REASON').change(function () {
    if($(this).is(":disabled"))
    {
        return;
    }

    if ($(this).val() == "OTHER")
    {
        control.selectedRow = null;
        control.WFD02510_SetOtherReason.modal('show');
        return;
    }
    
    fAction.UpdateMassReason();
})

$('#WFD02510_DISPOSAL_BY_SALE').click(function (e) {
    if ($(this).is(':checked')) {
        $('#WFD02510_DISPOSAL_BY_DONATE').prop('checked', false);
        $('#WFD02510_DISPOSAL_BY_SCRAP').prop('checked', false);

        fScreenMode.Approve_Mode_RegenFlow();

        fAction.onDisposaleTypeChange(this.value);
    } else {
        fAction.onDisposaleTypeChange(null);
    }
})
$('#WFD02510_DISPOSAL_BY_SCRAP').click(function (e) {
    if ($(this).is(':checked')) {
        $('#WFD02510_DISPOSAL_BY_SALE').prop('checked', false);
        $('#WFD02510_DISPOSAL_BY_DONATE').prop('checked', false);

        fScreenMode.Approve_Mode_RegenFlow();

        fAction.onDisposaleTypeChange(this.value);
    } else {
        fAction.onDisposaleTypeChange(null);
    }
})
$('#WFD02510_DISPOSAL_BY_DONATE').click(function (e) {
    if ($(this).is(':checked')) {
        $('#WFD02510_DISPOSAL_BY_SALE').prop('checked', false);
        $('#WFD02510_DISPOSAL_BY_SCRAP').prop('checked', false);

        fAction.onDisposaleTypeChange(this.value);
    } else {
        fAction.onDisposaleTypeChange(null);
    }
})


$('#WFD02510_COUNTERMEASURE').keypress(function (e) {
    return isNumberKey(e);
});
$('#WFD02510_USER_MISTAKE_FLAG_Y').click(function (e) {
    $('#WFD02510_USER_MISTAKE_FLAG_N').prop('checked', false); // UnChecks it
    $('#WFD02510_COUNTERMEASURE').prop("disabled", !$(this).is(':checked'));
})
$('#WFD02510_USER_MISTAKE_FLAG_N').click(function (e) {
    $('#WFD02510_USER_MISTAKE_FLAG_Y').prop('checked', false); // UnChecks it
    $('#WFD02510_COUNTERMEASURE').prop("disabled", $(this).is(':checked'));
    if($(this).is(':checked'))
        $('#WFD02510_COUNTERMEASURE').val();

})
//*******  Function Of Modal **********
function _GetCost() {

    if ($('#WFD02510_Retire_Quantity').is(":checked")) {
        return $('#WFD02510_Retire_Quantity_TXT').val();
    }
    
    if ($('#WFD02510_Retire_Percentage').is(":checked")) {
        return $('#WFD02510_Retire_Percentage_TXT').val();
    }

    return 0;
}

function _GetAdjType() {
    if ($('#WFD02510_Retire_Quantity').is(":checked")) {
        return 'Q';
    }
    if ($('#WFD02510_Retire_Percentage').is(":checked")) {
        return 'P';
    }
    
};

function checkEnabledOKButton() {

    var adjType = _GetAdjType();
    var amontCost = _GetCost();

    if (adjType != '') {

        var costValueOriginal = $('#WFD02510_COST_VALUE').val();
        var numCostValueOriginal = parseFloat(costValueOriginal);
        var numAmountCost = parseFloat(amontCost);

        var canRetire = true;

        if (adjType == 'Q') {
            var numQty = parseFloat($('#lblQty').text());
            if (numAmountCost > numQty) {
                canRetire = false;
            }

        } else {
            if (numAmountCost > 100) {
                canRetire = false;
            }
        }

        if (canRetire) {
            $("#AlertMessageWFD02510").html('');
            $("#btnRetireSave").prop('disabled', false);
        } else {
            AlertTextErrorMessagePopup('MCOM0017AERR : Amount greater than Cost Value', "#AlertMessageWFD02510");
            $("#btnRetireSave").prop('disabled', true);
        }

    } else {
        AlertTextErrorMessagePopup('MCOM0017AERR : Please select one of choice', "#AlertMessageWFD02510");
        $("#btnRetireSave").prop('disabled', true);
    }

    


}

//*************************
$('#WFD02510btnUploadTMCDocument').click(function () {
    // $('#lblUploadFileText').html("Upload TMC Document");
    var text = "Upload TMC Document";
    //var text = "Upload TMC Document Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB;
    fPopup.ShowMainUploadDialog(text, "TMC", control.AllowUploadTMC);
})
$('#WFD02510btnUploadTopManagement').click(function () {
    fPopup.ShowMainUploadDialog("Upload Top Management Document", "TOP", control.AllowUploadTop || GLOBAL.STATUS == '00');
})
$('#WFD02510_PRICE').keypress(function (e) {
    return isNumberKey(e);
});

$('#WFD02510_COST_OF_REMOVAL').keypress(function (e) {
    return isNumberKey(e);
});

$('#WFD02510_DISP_MASS_ATTACH_DOC_BTN').click(function () {
    var _uploadMD = (PERMISSION.AllowResubmit == 'Y') || (GLOBAL.STATUS == '00'); 
    fPopup.ShowMainUploadDialog("Upload Mass Attachment Document", "MD", _uploadMD);

})
$('#WFD02510_SelectAll').click(function (e) {
    $('.chkGEN:enabled').prop('checked', $(this).is(':checked'));

    $('.chkGEN:enabled').each(function () {
        fAction.DetailCheck(this);
    });
})