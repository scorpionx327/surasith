﻿var WFD02210Controls = {
    WFD02210AddAssets: $('#WFD02210Add'),
    WFD02210Clear: $('#WFD02210Clear'),
    WFD02210ExportToExcel: $('#WFD02210ExportToExcel'),

    WFD02210RemarkGenerate: $('#WFD02210RemarkGenerate'),
    DOC_NO: $('#DOC_NO'),
    WFD02210_GUID: $('#WFD02210_GUID'),

    WFD02190SearchAssetsModal: $('#WFD02190_SearchAssets'),     //Id Div CIP
    SearchResult: $('#WFD02210_SearchResult'),                  //Id Div Table
    SearchResultTable: $('#WFD02210_SearchResultTable'),        //Id Table    
    Pagin: $('#WFD02210_SearchResultTable').Pagin(URL_WFD02210.ITEM_PER_PAGE),            //Id Table      
    DataTable: null,
    SearchDataRow: $('#WFD02210_SearchDataRow')                 //Id Block Div 

};

$(function () {
    if (SCREEN_PARAMS.REQUEST_TYPE == 'C') {
        WFD02210Controls.SearchResult.hide();
        WFD02210Controls.WFD02210AddAssets.click(WFD02210AddAssetsData);
        WFD02210Controls.WFD02210Clear.click(WFD02210Clear);
        $('#WFD02190_SearchAssets').on('hidden.bs.modal', function () {
            WFD02210_UpdateAssetData(WFD02210_GetDataAssets);// WFD02210_GetDataAssets('0');
        });
        WFD02210SetDisplayScreen();
    }
});
//==================== Start Set Display Screen ====================//

var WFD02210SetDisplayScreen = function () {
    if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
        WFD02210Controls.WFD02210AddAssets.show();
        WFD02210Controls.WFD02210Clear.show();
        WFD02210Controls.WFD02210ExportToExcel.show();
        WFD02210setDisableButton('#WFD02210Clear');
        WFD02210setDisableButton('#WFD02210ExportToExcel');
    } else {
        WFD02210_GetDataAssets('0');
        WFD02210Controls.WFD02210AddAssets.hide();
        WFD02210Controls.WFD02210Clear.hide();
        WFD02210Controls.WFD02210ExportToExcel.show();
    }
    if (SCREEN_PARAMS.MODE == 'V') {
        WFD02210_DisableGenerate();
    }
}

var WFD02210SetDisableBtnDelInTable = function () {
    WFD02210Controls.SearchResultTable.find('[id^="WFD02210Del_"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).removeClass('active');
        $(this).prop('disabled', true);
    });
}

var WFD02210SetDisableCompleteDateInTable = function () {
    WFD02210Controls.SearchResultTable.find('[id^="WFD02210COMPLETE_DATE"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).removeClass("require");
        $(this).prop('disabled', true);
    });
    WFD02210Controls.SearchResultTable.find('[id^="WFD02210BTN_COMPLETE_DATE"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).prop('disabled', true);
    });
}

var WFD02210SetEnableCompleteDateInTable = function () {
    WFD02210Controls.SearchResultTable.find('[id^="WFD02210COMPLETE_DATE"]').each(function (e) {
        $(this).removeClass('disabled');
        $(this).addClass("require");
        $(this).prop('disabled', false);
    });
    WFD02210Controls.SearchResultTable.find('[id^="WFD02210BTN_COMPLETE_DATE"]').each(function (e) {
        $(this).removeClass('disabled');
        $(this).prop('disabled', false);
    });
}

var WFD02210SetEnableBtnDelInTable = function () {
    WFD02210Controls.SearchResultTable.find('[id^="WFD02210Del_"]').each(function (e) {
        $(this).removeClass('disabled');
        $(this).prop('disabled', false);
    });
}

var WFD02210_DisableGenerate = function () {
    WFD02210setDisableButton('#WFD02210Clear');
    WFD02210setDisableButton('#WFD02210Add');
    WFD02210SetDisableBtnDelInTable();
    WFD02210SetDisableCompleteDateInTable();
    if (SCREEN_PARAMS.MODE == 'V') {
        WFD02210Controls.WFD02210RemarkGenerate.hide();
    } else {
        WFD02210Controls.WFD02210RemarkGenerate.show();
    }    
}

var WFD02210_EnableGenerate = function () {
    WFD02210setEnableButton('#WFD02210Clear');
    WFD02210setEnableButton('#WFD02210Add');
    WFD02210SetEnableBtnDelInTable();
    WFD02210SetEnableCompleteDateInTable();
    WFD02210Controls.WFD02210RemarkGenerate.hide();
}

var WFD02210setDisableButton = function (id) {
    $(id).addClass('disabled');
    $(id).removeClass('active');
    $(id).prop('disabled', true);
}

var WFD02210setEnableButton = function (id) {
    $(id).removeClass('disabled');
    $(id).prop('disabled', false);
}

var WFD02210_SetClassTrRed = function () {
    $("#WFD02210_SearchResultTable tr").each(function () {
        var $this = $(this);
        var row = $this.closest("tr");
        if (row.find('td:eq(1)').text() != "" && row.find('td:eq(1)').text() != null) {
            var _rowData = [];
            if (WFD02210Controls.DataTable != null) {
                _rowData = WFD02210Controls.DataTable.row(this).data();
                if (_rowData["RETIRED_FLAG"] != null && _rowData["RETIRED_FLAG"] == 'Y') {
                    WFD02210Controls.SearchResultTable.removeClass('table-striped');
                    WFD02210Controls.SearchResultTable.removeClass('table-hover');
                    row.removeClass('odd');
                    row.removeClass('even');
                    row.addClass('RetiredRead');
                }
            }
        }
    });
}

//==================== End Set Display Screen ====================//


//==================== Start Get Data For Operation ====================//
var WFD02210_GetAssetDataModel = function () {
    var datas = [];
    $("#WFD02210_SearchResultTable tr").each(function () {
        var $this = $(this);
        var row = $this.closest("tr");
        if (row.find('td:eq(1)').text() != "" && row.find('td:eq(1)').text() != null) {
            var _rowData = [];
            if (WFD02210Controls.DataTable != null) {
                _rowData = WFD02210Controls.DataTable.row(this).data();
                datas.push({
                    "DOC_NO": SCREEN_PARAMS.DOC_NO,
                    "ASSET_NO": _rowData["ASSET_NO"],
                    "COST_CODE": _rowData["COST_CODE"],
                    "COMPLETE_DATE": $('#WFD02210COMPLETE_DATE' + _rowData["ASSET_NO"]).val() //row.find('td:eq(5)').text()$('#DatePicker').GetCDateTime(),
                });
            }
        }
    });

    var WFD02210Save = {
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        USER_BY: SCREEN_PARAMS.USER_BY,
        EMP_CODE: SCREEN_PARAMS.USER_BY,
        GUID: SCREEN_PARAMS.GUID,
        UPDATE_DATE: SCREEN_PARAMS.UPDATE_DATE,
        ASSET_LIST: datas
    }
    return WFD02210Save;
}

var WFD02210GetDefaultSceenValue = function () {
    return {
        GUID: SCREEN_PARAMS.GUID,
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        USER_BY: SCREEN_PARAMS.EMP_CODE
    }
}
//==================== End Get Data For Operation ====================//

//==================== Start Data In Table ====================//
var WFD02210_SetButtonDelInTable = function (data) {
    var _strHtml = '';
    if (data.ALLOW_DELETE) {
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="WFD02210Del_"' + data.ASSET_NO + ' class="btn btn-danger btn-xs" data-title="Delete" type="button" onclick=WFD02210_DELETE(\'' + data.ASSET_NO + '\',\'' + data.COST_CODE + '\')><span class="glyphicon glyphicon-trash"></span></button></p>';
    } else {
        _strHtml = ' ';
    }
    return _strHtml;
}

var WFD02210_SetCompleteDateInTable = function (data) {
    var _strHtml = '';
    var _style = '';
    var _disable = '';
    var _background = '';
    if (SCREEN_PARAMS.MODE == 'V' || SCREEN_PARAMS.IS_REQ == 'N' || SCREEN_PARAMS.ALLOW_APPROVE != 'Y') {
        _style = '';
        _disable = 'disabled="disabled"';
        _background = 'background-color:#f5f5f5';
    } else {
        _style = 'require';
        _disable = '';
    }
    var _dateComplete = '';
    if (data.COMPLETE_DATE == null) {
        _dateComplete = '';
    } else {
        _dateComplete = data.COMPLETE_DATE;
    }
    _strHtml = '<div class="input-group date">'
            + '<input type="text" class="form-control datepicker ' + _style + '" id="WFD02210COMPLETE_DATE' + data.ASSET_NO + '" name="WFD02210COMPLETE_DATE' + data.ASSET_NO + '" value="' + _dateComplete + '" ' + _disable + '>'
            + '<span class="input-group-btn" style="cursor:pointer;">'
            + '<button id="WFD02210BTN_COMPLETE_DATE' + data.ASSET_NO + '" class="btn" onclick="ClickIconDate(\'WFD02210COMPLETE_DATE' + data.ASSET_NO + '\')" style="' + _background + '" ' + _disable + '>'
            + '<i class="fa fa-calendar"></i>'
            + '</button>'
            + '</span>'
            + '</div>';
    return _strHtml;
}

var WFD02210_SetTooltipColumnASSET_NAME = function (data) {
    var _strHtml = '';
    _strHtml = '<p title="" data-original-title="' + data.ASSET_NAME + '" data-toggle="tooltip" data-placement="top"> ' + data.ASSET_NAME_SHOW + '</p>';
    return _strHtml;
}

var WFD02210_GetDataAssets = function (rest) {
    if (rest != '-99') {
        WFD02210Data = WFD02210GetDefaultSceenValue();
        WFD01270DataModel = WFD01270RequestHeaderData();
        var pagin = WFD02210Controls.Pagin.GetPaginData();
        ajax_method.SearchPost(URL_WFD02210.GET_ASSETS_CIP_DATA, WFD02210Data, pagin, true, function (datas, pagin) {
            if (datas != null && datas.length > 0) {
                WFD02210Controls.SearchResult.show();
                SCREEN_PARAMS.WFD02190_COST_CODE = datas[0].COST_CODE;
                WFD02210setEnableButton('#WFD02210Clear');
                if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
                if (!pagin.OrderType) pagin.OrderType = 'asc';

                if ($.fn.DataTable.isDataTable(WFD02210Controls.SearchResultTable)) {
                    var table = WFD02210Controls.SearchResultTable.DataTable();
                    table.destroy();
                }
                WFD02210Controls.DataTable = WFD02210Controls.SearchResultTable.DataTable({
                    data: datas,
                    "columns": [
                        { "data": "CIP_NO", 'orderable': false },
                        { "data": "ASSET_NO_LINK", 'orderable': false },
                        { "data": null, 'orderable': false },
                        { "data": "PLAN_COST", 'orderable': false },
                        { "data": "ACTUAL_COST", 'orderable': false },
                        { "data": null },
                        { "data": null }
                    ],
                    'columnDefs': [
                    { 'targets': [3, 4], className: 'text-right' },
                    {
                        'targets': 6,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return WFD02210_SetButtonDelInTable(data);
                        }
                    },//WFD02210_SetTooltipColumnASSET_NAME
                    {
                        'targets': 2,
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return SetTooltipInDataTableByColumn(data.ASSET_NAME, 80);
                        }
                    },
                    {
                        'targets': 5,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return WFD02210_SetCompleteDateInTable(data);
                        }

                    }],
                    'order': [],
                    searching: false,
                    paging: false,
                    retrieve: true,
                    "bInfo": false,
                    "autoWidth": false
                });

                $('.datepicker').datepicker({
                    dateFormat: 'dd-M-yy',
                    //comment the beforeShow handler if you want to see the ugly overlay 
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 99999999999999);
                        }, 0);
                    }
                });

                WFD02210Controls.SearchDataRow.show();

                WFD02210_SetClassTrRed();
            } else {
                var table = WFD02210Controls.SearchResultTable.DataTable();
                table.destroy();
                WFD02210Controls.DataTable = null;
                SCREEN_PARAMS.WFD02190_COST_CODE = '';
                //WFD02210Controls.DataTable = null;
                WFD02210Controls.SearchResult.hide();
                WFD02210Controls.Pagin.Clear();
                WFD02210setDisableButton('#WFD02210Clear');
                WFD02210setDisableButton('#WFD02210ExportToExcel');
            }
            CheckPageLoaded = 'Y';
        }, null);
    }
}
//==================== End Data In Table ====================//

//==================== Start Even Button ====================//
var WFD02210AddAssetsData = function () {
    WFD02190LoadDefault(SCREEN_PARAMS.WFD02190_COST_CODE);
    WFD02210Controls.WFD02190SearchAssetsModal.appendTo("body").modal('show');
}

var WFD02210Clear = function () {
    loadConfirmAlert(WFD01270_Message.ConfirmClear, function (result) {
        if (result) {
            WFD02210Data = WFD02210GetDefaultSceenValue();
            ajax_method.Post(URL_WFD02210.CLEAR_ASSETS_CIP_DATA, WFD02210Data, false, function (result) {
                if (result != null) {
                    //WFD02210_UpdateAssetData(WFD02210_GetDataAssets);
                    WFD02210_GetDataAssets('0');
                    //$.bootstrapGrowl("Clear process is completed.", {
                    //    type: 'success',
                    //    align: 'right',
                    //    width: 'auto',
                    //    allow_dismiss: false
                    //});
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Clear process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                    return;
                }
            }, null);
        }
    });
}

var WFD02210ExportToExcel = function () {

}

var WFD02210_UpdateAssetData = function (callback) {
    var ArrData = WFD02210_GetAssetDataModel();
    if (ArrData.ASSET_LIST != null && ArrData.ASSET_LIST.length > 0) {
        ajax_method.Post(URL_WFD02210.UPDATE_ASSETS_CIP_DATA, ArrData, false, function (result) {
            if (typeof callback == 'function') {
                callback(result);
            }
        }, null);
    } else {
        WFD02210_GetDataAssets('0');
    }
}

var WFD02210_DELETE = function (_AssetNo, _CostCose) {
    var _ArrTransferReason = '';
    var WFD02210DetailAsset = [
        {
            ASSET_NO: _AssetNo,
            COST_CODE: _CostCose
        }
    ];
    var WFD02210Model = {
        GUID: SCREEN_PARAMS.GUID,
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        USER_BY: SCREEN_PARAMS.EMP_CODE,
        ASSET_LIST: WFD02210DetailAsset
    }
    var _strMessageDel = WFD01270_Message.ConfirmDelete;
    _strMessageDel = _strMessageDel.replace("{1}", _AssetNo);
    loadConfirmAlert(_strMessageDel, function (result) {
        if (result) {
            var ArrData = WFD02210_GetAssetDataModel();
            if (ArrData.ASSET_LIST != null && ArrData.ASSET_LIST.length > 0) {
                ajax_method.Post(URL_WFD02210.UPDATE_ASSETS_CIP_DATA, ArrData, false, function (result) {
                    if (result != '-99') {
                        ajax_method.Post(URL_WFD02210.DELETE_ASSET_NO, WFD02210Model, false, function (result) {
                            if (result != "-99") {
                                _ArrTransferReason = result;
                                $.notify({
                                    icon: 'glyphicon glyphicon-ok',
                                    message: "Deletion process is completed."
                                }, {
                                    type: 'success',
                                    delay: 300,
                                });
                                WFD02210_GetDataAssets('0');
                                return;
                            }
                        }, null);
                    }
                }, null);
            }
        }
    });
}

$('#WFD02210ExportToExcel').click(function (e) {
    loadConfirmAlert(WFD01270_Message.ConfirmExportToExcel, function (result) {
        if (result) {
            var batch = BatchProcess({
                BatchId: "LFD02240",
                Description: "Export CIP Request",
                UserId: SCREEN_PARAMS.EMP_CODE
            });
            batch.Addparam(1, 'DOC_NO', SCREEN_PARAMS.DOC_NO);
            batch.StartBatch();
        }
    });
});
//==================== End Button ====================//