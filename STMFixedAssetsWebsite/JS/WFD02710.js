﻿var control = {
    DataTable: null,
    DisableBtns: []
}

$(function () {
    //$('#divSearchRequestResult').hide();
});
var i = 0;
var fAction = {
    History: [],
    AucGroups : [],
    SelectedCostCenter: "",
    SelectedRespCostCenter: "",
    TargetAssetList: {},
    GetDocNo: function () {
        return GLOBAL.DOC_NO ? GLOBAL.DOC_NO : GLOBAL.TEMP_DOC_NO;
    },
    //Should define all request screen.
    Initialize: function (callback) {
        if (callback != null)
            callback();
    },

    LoadData: function (callback) {
        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (rs, pagin) {
            fAction.AucGroups = [];
            fAction.SelectedCostCenter = ""; //Clear Selected
            fAction.SelectedRespCostCenter = ""; //Clear Selected
            fAction.TargetAssetList = null;
            if (rs == null)
                return;
            datas = rs.RowList;

            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                //TODO POP DON'T KNOW WHY
                try {
                    table.clear().draw().destroy();
                } catch (e) {
                    table.clear().draw().destroy();
                }

                $('#tbSearchResult thead').empty();
            }

            if (datas == null || datas.length == 0) {
                //$('#divSearchRequestResult').hide();
                if (callback != null)
                    callback();
                return;
            }
            if (datas.length > 0) {
                console.log(datas[0]);
                fAction.SelectedCostCenter = datas[0].COST_CODE;
                fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;
                GLOBAL.TEMP_DOC_NO = datas[0].DOC_NO;
                GLOBAL.COMPANY = datas[0].COMPANY;
                $('#WFD01170ddpCompany').select2('val', GLOBAL.COMPANY);
            }

            var _bAllowGenerateAssetNo = false;
            $.each(rs.ColumnList, function (key, value) {
                if (value.ASSET_SUB == null || value.ASSET_SUB == '') {
                    _bAllowGenerateAssetNo = true;
                }
            });

            if (_bAllowGenerateAssetNo && PERMISSION.AllowGenAssetNo == "Y") {
                $('#btnGenerateAssetNo').show();
                $('#btnGenerateAssetNo').prop("disabled", false);
            }
            else {
                $('#btnGenerateAssetNo').hide();
                $('#btnGenerateAssetNo').prop("disabled", true);
            }

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            //Generate Header
            var _header = [
                { "data": null, "class": "td-border-l p-0 text-center", 'orderable': false },
                {
                    "data": "ROW_INDX", 'orderable': false, "class": "text-center p-x-2", 'render': function (data, type, full, meta) {
                        if (data == 0)
                            return "";
                        return data;
                    }
                },
                { "data": "AUC_NO", "class": "text-center p-x-2 td-s-tooltip", 'orderable': false },
                { "data": "AUC_SUB", "class": "text-center p-x-2 td-border-r", 'orderable': false },
                //{
                //    "data": "AUC_NAME", 'orderable': false, "class":"td-s-tooltip",
                //    'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                //},
                { "data": "PO_NO", 'orderable': false },
                { "data": "INV_NO", 'orderable': false },
                { "data": "INV_LINE", "class": "text-right", 'orderable': false },
                { "data": "INV_DESC", 'orderable': false },
                { "data": "AUC_AMOUNT", "class": "text-right", 'orderable': false },
                { "data": "AUC_REMAIN", "class": "text-right td-border-r", 'orderable': false },
                { "data": null, 'orderable': false }
            ];

            $('#tbSearchResult thead').append('<tr></tr>');
            $('#tbSearchResult thead').append('<tr></tr>');
            var $headerRow1 = $('#tbSearchResult thead tr:eq(0)');
            var $headerRow2 = $('#tbSearchResult thead tr:eq(1)');

            $headerRow2.append('<th style="border-top: 1px solid white;border-left: 1px solid white;border-bottom: 1px solid;min-width:40px;"></th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:32px;">No.</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:115px;width:115px;">AUC No.</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;width:36px;min-width:36px;">Asset<br />Sub</th>')
                //.append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:125px;width:110px;">Description</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:75px;">PO No.</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:75px;">Invoice No.</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:25px;">Line</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:125px;width:110px;">Invoice Description</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:90px;">AUC Amount</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:70px;">Remaining</th>')
                .append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:30px;">Average<br />To</th>');

            fAction.TargetAssetList = rs.ColumnList;
            fRender.TargetAssetListRender(fAction.TargetAssetList);

            var hasColumnList = rs.ColumnList.length > 0;

            var _colDefs = [
                {
                    'targets': 0,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fRender.CheckBoxRender(data);
                    }
                },
                {
                    'targets': 2, //ASSET_NO Show hyperlink
                    'render': function (data, type, full, meta) {
                        return '<span data-toggle="tooltip" title="' + full.AUC_NAME +'">' + fRender.HyperLinkRender(full) + '</span>';
                        //return fRender.HyperLinkRender(full);
                    }
                },
                {
                    'targets': 3, //ASSET_SUB Show hyperlink
                    'render': function (data, type, full, meta) {
                        if (full.GRP_ROW_INDX <= 1)
                            return data;
                        return '';
                    }
                },
                //{
                //    'className': 'td-s-tooltip',
                //    'targets': 4, //ASSET_NAME Show hyperlink
                //    'render': function (data, type, full, meta) {
                //        if (full.GRP_ROW_INDX <= 1)
                //            return SetTooltipInDataTableByColumn(data, 8);
                //        return '';
                //    }
                //},
                {
                    'targets': 10, //AverageAssetNoRender
                    "visible": hasColumnList,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return fRender.AverageAssetNoRender(full);
                    }
                },
                //{
                //    'targets': 8,
                //    'render': function (data, type, full, meta) {
                //        if (full.GRP_ROW_INDX <= 1)
                //            return SetTooltipInDataTableByColumn(data, 12);
                //        return '';
                //    }
                //},
                {
                    'targets': 8,
                    "render": function (data, type, full, meta) {
                        if (data == null)
                            return '';
                        return fRender.commaSeparateNumber(data.toFixed(2));
                    }
                },
                {
                    'targets': 9,
                    "render": function (data, type, full, meta) {
                        if (data == null)
                            return '';
                        return fRender.commaSeparateNumber(data.toFixed(2));
                    }
                }
            ];

            if (hasColumnList) {
                $headerRow1.append('<th style="border:0;background-color:white;" colspan="' + _header.length + '"></th>');
            }

            var dateDisable = (PERMISSION.IsAECUser === 'Y' || (GLOBAL.STATUS === "00" && PERMISSION.AllowEditBeforeGenFlow)) ? "" : " disabled";

            $.each(rs.ColumnList, function (key, value) {
                $headerRow2.append('<th class="td-s-tooltip" style="border-top: 1px solid white;border-bottom: 1px solid;">' + fRender.TargetAssetHeaderRender(value) + '</th>');

                var dateHtml = '';
                var dateStr = value.CAP_DATE ? value.CAP_DATE : '';

                var _id = 'Calendar2710_' + value.ASSET_NO + value.ASSET_SUB;
                var _dt = '<div class="input-group date" style="width:120px;">' +
                    '<input autocomplete="off" type="text" id="' + _id + '" asset-no="' + value.ASSET_NO + '" asset-sub="' + value.ASSET_SUB + '" name="Calendar2710" onchange="onCapDateChange(this)" class="form-control datepicker" value="' + dateStr + '" ' + dateDisable + '/>' +
                    '<span class="input-group-btn" style="cursor:pointer;">' +
                    '<button class="btn" id="' + _id + '_BTN" onclick="ClickIconDate(\'' + _id + '\')" style="background-color:white;"' + dateDisable + '>' +
                    '<i class="fa fa-calendar"></i>' +
                    '               </button>' +
                    '</span>' +
                    '</div>';
                dateHtml += '<th style="background-color:white;border:0;">' + _dt + '</th>';
                $headerRow1.append(dateHtml);

                _header.push({ "data": null, 'orderable': false });
                _colDefs.push(
                    {
                        'targets': _header.length - 1, //Settlement 
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        "width": "120px",
                        'render': function (data, type, full, meta) {
                            var colName = ['c' + key];
                            data[colName] = "0";

                            //var _mapdata = rs.MappingList.find(x => x.RowKey == full.INVOICE_GROUP_KEY &&
                            //    x.ColumnKey == value.COL_INDX);
                            var _mapdata = $.grep(rs.MappingList, function (n, i) {
                                return n.RowKey == full.INVOICE_GROUP_KEY &&
                                    n.ColumnKey == value.COL_INDX;
                            });

                            if (_mapdata == null || _mapdata.length == 0)
                                return fRender.SettlementButtonRender(full, value.ASSET_NO, value.ASSET_SUB, null, null, colName);
                            if (_mapdata[0].SETTLE_AMOUNT == null)
                                return fRender.SettlementButtonRender(full, value.ASSET_NO, value.ASSET_SUB, null, null, colName);

                            data[colName] = _mapdata[0].SETTLE_AMOUNT;

                            return fRender.SettlementButtonRender(full, value.ASSET_NO, value.ASSET_SUB, fRender.commaSeparateNumber(_mapdata[0].SETTLE_AMOUNT.toFixed(2)), _mapdata[0].ADJ_TYPE, colName);
                        }
                    }
                );

            });
            _header.push({
                "data": "STATUS", 'orderable': false,
                'render': function (data, type, full, meta) {
                    //return fRender.CheckBoxRender(data);
                    if (full.STATUS !== "ERROR")
                        return full.STATUS;

                    return '<span data-toggle="tooltip" data-html="true" title="<div style=&quot;min-width:80px;&quot;>' + full.MESSAGE + '</div>">' + full.STATUS + '</span>';
                }
            });
            _header.push({ "data": null, 'orderable': false });

            if (hasColumnList) {
                $headerRow1.append('<th style="background-color:white;border:0;"></th>');
                $headerRow1.append('<th style="background-color:white;border:0;"><div style="width:34px;"></div></th>');
            }

            $headerRow2.append('<th style="border-top: 1px solid white;border-bottom: 1px solid;min-width:30px;">Status</th>');
            $headerRow2.append('<th class="td-border-r" style="border-top: 1px solid white;border-bottom: 1px solid;min-width:20px;"></th>');

            _colDefs.push(
                {
                    'targets': _header.length - 1, //Delete
                    'className': 'text-center td-border-r',
                    'render': function (data, type, full, meta) {
                        return fRender.DeleteButtonRender(full);
                    }
                }
            );

            //$('#divSearchRequestResult').show();

            if (!hasColumnList) {
                $headerRow1.remove();
            }

            control.DataTable = $('#tbSearchResult').DataTable({
                data: datas,
                "columns": _header,
                'columnDefs': _colDefs,
                'order': [],
                "initComplete": function (settings, json) {
                    //var disable = (PERMISSION.IsAECUser === 'Y' || (GLOBAL.STATUS === "00" && PERMISSION.AllowEditBeforeGenFlow)) ? "" : " disabled";

                    //_html = '<th colspan="12">&nbsp;</th>';
                    //$.each(rs.ColumnList, function (key, value) {
                    //    var dateStr = value.CAP_DATE ? value.CAP_DATE : '';

                    //    var _id = 'Calendar2710_' + value.ASSET_NO + value.ASSET_SUB;
                    //    var _dt = '<div class="input-group date">' +
                    //        '<input type="text" id="' + _id + '" asset-no="' + value.ASSET_NO + '" asset-sub="' + value.ASSET_SUB + '" name="Calendar2710" onchange="onCapDateChange(this)" class="form-control datepicker" value="' + dateStr + '" ' + disable + '/>' +
                    //        '<span class="input-group-btn" style="cursor:pointer;">' +
                    //        '<button class="btn" id="' + _id + '_BTN" onclick="ClickIconDate(\'' + _id + '\')" style="background-color:white;"' + disable + '>' +
                    //        '<i class="fa fa-calendar"></i>' +
                    //        '               </button>' +
                    //        '</span>' +
                    //        '</div>'
                    //    _html += '<th style="width:120px;">' + _dt + '</th>';
                    //});
                    //_html += '<th colspan="2"></th>';

                    //$('#rowHeader').html(_html);

                    if (fAction.AucGroups.length > 0) {
                        $('.' + fAction.AucGroups[fAction.AucGroups.length - 1] + '_g').addClass('tr-last-td');
                    }

                    //Remapping
                    $('.datepicker').datepicker({
                        // dateFormat: 'dd-M-yy',
                        dateFormat: 'dd.mm.yy',
                        //comment the beforeShow handler if you want to see the ugly overlay
                        beforeShow: function () {
                            setTimeout(function () {
                                $('.ui-datepicker').css('z-index', 99999999999999);
                            }, 0);
                        }
                    });
                },
                scrollY: "400px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                fixedColumns: {
                    leftColumns: 10
                },
                "rowCallback": function (row, data, index) {
                    //if (data.GRP_ROW_INDX == 0) {
                    //    $(row).addClass(data.AUC_GROUP_KEY);
                    //}

                    if (data.GRP_ROW_INDX === 0) {
                        $(row).addClass(data.AUC_GROUP_KEY + "_g");//add group class
                        fAction.AucGroups.push(data.AUC_GROUP_KEY);
                    } else {
                        $(row).addClass(data.AUC_GROUP_KEY);//child class
                    }
                    //_map = $.grep(fAction.History, function (n, i) {
                    //    return n.AUC_GROUP_KEY == data.AUC_GROUP_KEY;
                    //});

                    //if (_map.length > 0) {
                    //    if (data.GRP_ROW_INDX == 0) {
                    //        $(row).hide();
                    //    }
                    //    else {
                    //        $(row).show();
                    //    }
                    //    return;
                    //}
                    var isGroup = data.GRP_ROW_INDX === 0;
                    var isShow = false;

                    var i = fAction.History.indexOf(data.AUC_GROUP_KEY);
                    if (i !== -1) {
                        isShow = isGroup ? false : true;
                    } else {
                        //isShow = !data.HAS_GROUP;
                        //if (!isShow) {
                        //    isShow = isGroup ? true : false;
                        //}
                        isShow = (isGroup || !data.HAS_GROUP) ? true : false;
                    }

                    if (isShow) {
                        $(row).show();
                    } else {
                        $(row).hide();
                    }

                    //var initRow = function () {
                    //    if ((data.HAS_GROUP && data.GRP_ROW_INDX === 0)
                    //        || !data.HAS_GROUP && data.GRP_ROW_INDX === 1) {
                    //        $(row).show();
                    //    } else {
                    //        $(row).hide();
                    //    }
                    //};

                    //initRow();

                    if (data.STATUS === "ERROR") {
                        $(row).css('background-color', '#F9966B');
                    }

                    $.each(fAction.TargetAssetList, function (i, value) {
                        if (value.STATUS === "ERROR") {
                            var colInx = (10 + i) + 1;
                            $(row).find('td:eq(' + colInx + ')').css('background-color', '#F9966B');
                        }
                    });
                },
                searching: false,
                retrieve: true
            });

            setTimeout(function () {
                control.DataTable.columns.adjust();
                //fixedColumns().relayout();
            },1);

            //POP FIX INFO TABLE WITH GROUP AUC
            $('#tbSearchResult_info').text('Total AUC : ' + rs.AssetAUCCount);

            if (GLOBAL.DOC_NO == '' || GLOBAL.DOC_NO == null) {
                if (datas.length == 0) {
                    //$('#divSearchRequestResult').hide();
                    fScreenMode.Submit_InitialMode(function () {
                        fMainScreenMode.Submit_InitialMode();
                    });
                }
                if (datas.length > 0) {
                    fScreenMode.Submit_AddAssetsMode(function () {
                        fMainScreenMode.Submit_AddAssetsMode();
                    });
                }
                fScreenMode._GridVisible(false, false);
            }
            else {
                fScreenMode.Submit_InitialMode(function () {
                    //fMainScreenMode.Approve_Mode();
                    fScreenMode.Approve_Mode(function () {
                        fMainScreenMode.Approve_Mode();
                    });
                });
            }

            if (callback == null)
                return;

            callback();

        }, null);
    },

    Clear: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var data = { GUID: GLOBAL.GUID, DOC_NO: fAction.GetDocNo() };
        ajax_method.Post(Url.ClearAssetsList, data, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }
            fAction.LoadData();
            //  SuccessNotification(CommonMessage.ClearSuccess);
            fScreenMode.Submit_InitialMode();

            if (callback == null)
                return;

            callback();

            //location.reload();
        });
    },
    Export: function (callback) {
        fAction.UpdateAsset(function () {
            var batch = BatchProcess({
                BatchId: "LFD02712",
                Description: "Export Settlement Request",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'GUID', GLOBAL.GUID);
            batch.Addparam(2, 'DOC_NO', fAction.GetDocNo());
            batch.StartBatch();

            if (callback == null)
                return;

            callback();
        });
    },
    AddAsset: function (_list, callback) { //It's call from dialog

        if (_list == null || _list.length == 0)
            return;

        _list.forEach(function (_data) {
            // do something with `item`
            _data.GUID = GLOBAL.GUID;
            _data.USER_BY = GLOBAL.USER_BY;
        });

        ajax_method.Post(Url.AddAsset, _list, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }

            fAction.LoadData();

            if (callback == null)
                return;

            callback();
        });
    },
    UpdateAsset: function (callback) {
        if (callback == null)
            return;

        callback();

        //var _data = fAction.GetClientAssetList(false); // Get All
        //ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
        //    if (result.IsError) {
        //        //fnErrorDialog("UpdateAssetList", result.Message);
        //        return;
        //    }
        //    if (callback == null)
        //        return;

        //    callback();

        //}); //ajax_method.UpdateAssetList
    },
    DeleteAsset: function (_AUCNo, _AUCSub, INV_NO, INV_LINE) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var _data = {
            DOC_NO: fAction.GetDocNo(),
            GUID: GLOBAL.GUID,
            ASSET_NO: _AUCNo,
            ASSET_SUB: _AUCSub,
            COMPANY: GLOBAL.COMPANY
        };

        INV_NO = (INV_NO && INV_NO !== "null") ? INV_NO : null;
        INV_LINE = (INV_LINE && INV_LINE !== "null") ? INV_LINE : null;

        var confirmMsg = "";

        if (INV_NO && INV_LINE) {
            confirmMsg = CommonMessage.ConfirmDelete.replace("{1}", "Invoice No. " + INV_NO + " Line No. " + INV_LINE);
        } else {
            confirmMsg = fMainAction.getConfirmDeleteAssetDetail(_AUCNo, _AUCSub);
        }

        loadConfirmAlert(confirmMsg, function (result) {
            if (!result) {
                return;
            }
            fAction.UpdateAsset(function () {

                _data.COMPANY = GLOBAL.COMPANY;
                _data.DOC_NO = fAction.GetDocNo();
               
                ajax_method.Post(Url.DeleteAsset, { data: _data, invNo: INV_NO, invLine: INV_LINE }, false, function (result) {
                    if (result.IsError) {
                        return;
                    }
                    console.log("Delete");
                    fAction.LoadData();
                    // SuccessNotification(CommonMessage.ClearSuccess);
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Deletion process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                });
            }); // UpdateAsset 
        }); // loadConfirmAlert
    },
    UploadExcel: function (_fileName) {
        //call Batch with filename
        var batch = BatchProcess({
            BatchId: "BFD02731",
            Description: "Upload Settlement Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function (_appID, _Status) {

            if (_Status != 'S') {
                return;
            }
            //fAction.LoadData();
            //location.reload();
            fAction.LoadData();
        }, true);
    },
    PrepareGenerateFlow: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        fAction.UpdateAsset(function () {
            
            ajax_method.Post(Url.PrepareFlow, {
                data: {
                    GUID: GLOBAL.GUID,
                    DOC_NO: fAction.GetDocNo(),
                    FLOW_TYPE: GLOBAL.FLOW_TYPE,
                    ACT_ROLE: GLOBAL.ACT_ROLE
                }
            }, false, function (result) {
                if (result.IsError) {
                    return;
                }
                if (callback == null)
                    return;

                callback();
            }); //ajax_method.PrepareFlow
        }); //ajax_method.UpdateAssetList

    },

    PrepareSubmit: function (callback) {

        fAction.UpdateAsset(callback);
    },
    PrepareApprove: function (callback) {
        fAction.UpdateAsset(callback);
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },
    ShowSearchPopup: function (callback) {
        //Set search parameter

        // fAction.UpdateAsset(function () { //Update first.
        f2190SearchAction.ClearDefault();

        var _selected = fAction.GetClientAssetList(false);


        Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();

        Search2190Config.searchOption = "K";
        Search2190Config.AllowMultiple = true;

        Search2190Config.Default.AssetGroup = { Value: "AUC", Enable: false };

        Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
        Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
        Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedRespCostCenter, Enable: fAction.SelectedRespCostCenter == "" };
        if (_selected && _selected.length > 0) {
            Search2190Config.Default.WBSBudget = { Value: _selected[0].WBS_BUDGET, Enable: false };
        }

        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();
        // }); // UpdateAsset
    },

    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");
        if (control.DataTable == null) {
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List

        $("#tbSearchResult tbody tr").each(function () {
            //var $this = $(this);
            //var row = $this.closest("tr");
            //if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
            //    return;
            //}
            //var _row = [];
            //if (control.DataTable == null) {
            //    return;
            //}
            //var _reason = $(this).find("input[name='txtReason']").val();

            var $checkBox = $(this).find("input.chkGEN");

            if ($checkBox.length > 0) {
                var _bSel = $checkBox.is(":checked");

                if (_requireCheck && !_bSel) // Required check but no selected 
                    return;

                var _row = control.DataTable.row(this).data(); // Get source data

                _assetList.push({
                    GUID: GLOBAL.GUID,
                    DOC_NO: fAction.GetDocNo(),
                    COMPANY: _row.COMPANY,
                    ASSET_NO: _row.ASSET_NO,
                    ASSET_SUB: _row.ASSET_SUB,
                    AMOUNT_POSTED: _row.AMOUNT_POSTED,
                    PERCENTAGE: _row.PERCENTAGE,
                    WBS_BUDGET: _row.WBS_BUDGET,
                    FINAL_ASSET_CLASS: _row.FINAL_ASSET_CLASS
                });
            }
        });

        return _assetList;
    },

    DetailCheck: function (sender, id, AUC_GROUP_KEY) {
        var isCheck = $(sender).is(':checked');
        $('.chk_' + AUC_GROUP_KEY).prop('checked', isCheck);

        $('#WFD02710_SelectAll').prop('checked', _act = $('.chkGEN:checked').length == $('.chkGEN').length);
    },


    ClientReload: function (selectedRow) {

    },
    AverageCost: function (callback) {
        //GOTO SERVER

        var _selectedList = [];
        $('.AssetNo:checked').each(function (index) {
            var _sender = $(this);

            //var _mapdata = fAction.TargetAssetList.find(x => x.COL_INDX == _sender.val());

            // _mapdata = $.grep(fAction.TargetAssetList.find(x => x.COL_INDX == _sender.val());
            _mapdata = $.grep(fAction.TargetAssetList, function (n, i) {
                return n.COL_INDX == _sender.val();
            });

            if (_mapdata == null || _mapdata.length == 0)
                return; //continue
            _selectedList.push({
                ASSET_NO: _mapdata[0].ASSET_NO,
                ASSET_SUB: _mapdata[0].ASSET_SUB
            });
        });

        //WFD02710AUCModel data, List<WFD02710TargetModel> selectedList
        ajax_method.Post(Url2710.AverageCost, { datas: fPopup.selectedRows, selectedList: _selectedList }, false, function (result) {
            //ajax_method.Post(Url2710.AverageCost, { data: fPopup.selectedRow, selectedList: _selectedList }, false, function (result) {
            if (result.IsError) {
                return;
            }
            $('#divAverageDialog #chkAverageSelectAll').prop("checked", false);
            fAction.LoadData();

            if (callback == null)
                return;

            callback();
        }); //ajax_method.UpdateCost
    },
    UpdateCost: function (callback) {
        var cost = fPopup._GetCost();
        var adjType = fPopup._GetAdjType();

        $.each(fPopup.selectedRows, function (i, item) {
            item.ADJ_TYPE = adjType;
        });
        //if (row.GRP_ROW_INDX === 0) {
        //    $.each(control.DataTable.data(), function (i, item) {

        //        if ((item.AUC_GROUP_KEY === row.AUC_GROUP_KEY) && item.GRP_ROW_INDX !== 0) {
        //            var data = initRow(item);
        //            fPopup.selectedRows.push(data);
        //        }
        //    });
        //} else {
        //    var data = initRow(row);
        //    fPopup.selectedRows.push(data);
        //}
        ajax_method.Post(Url2710.UpdateSettlement, { datas: fPopup.selectedRows, costStr: cost }, false, function (result) {
            if (result == null || result.IsError) {
                return;
            }
            fPopup.selectedRow = null;
            fPopup.selectedCol = null;
            //Update Row
            fAction.LoadData();
            if (callback == null)
                return;

            callback();
        }); //ajax_method.UpdateCost

    },
    //UpdateCost: function (callback) {

    //    var _amount = 0;

    //    var _data = {
    //        GRP_ROW_INDX: fPopup.selectedRow.GRP_ROW_INDX,
    //        GUID: GLOBAL.GUID,
    //        COMPANY: GLOBAL.COMPANY,
    //        AUC_NO: fPopup.selectedRow.AUC_NO,
    //        AUC_SUB: fPopup.selectedRow.AUC_SUB,
    //        SAP_DOC: fPopup.selectedRow.SAP_DOC,
    //        PO_NO: fPopup.selectedRow.PO_NO,
    //        INV_NO: fPopup.selectedRow.INV_NO,
    //        INV_LINE: fPopup.selectedRow.INV_LINE,
    //        TARGET_ASSET_NO: fPopup.selectedCol.TARGET_ASSET_NO,
    //        TARGET_ASSET_SUB: fPopup.selectedCol.TARGET_ASSET_SUB,
    //        SETTLE_AMOUNT: parseFloatWithComma(fPopup._GetCost()),
    //        AUC_REMAIN: fPopup.selectedRow.AUC_REMAIN,
    //        ADJ_TYPE: fPopup._GetAdjType()
    //    }
    //    ajax_method.Post(Url2710.UpdateSettlement, { data: _data }, false, function (result) {
    //        if (result == null || result.IsError) {
    //            return;
    //        }

    //        fPopup.selectedRow = null;
    //        fPopup.selectedCol = null;

    //        //Update Row
    //        fAction.LoadData();



    //        if (callback == null)
    //            return;

    //        callback();
    //    }); //ajax_method.UpdateCost

    //},
    EnableMassMode: function () {

    },

    AverageCostDetailCheck: function (sender) {

        $('#divAverageDialog #chkAverageSelectAll').prop('checked', _act = $('.AssetNo:checked').length == $('.AssetNo').length);

        if ($('.AssetNo:checked').length <= 0) {
            $('#btnAverageOK').attr("disabled", true);
        }
        else {
            $('#btnAverageOK').attr("disabled", false);
        }
    },
    RequesetAUCInfo: function (callback) {
        var batch = BatchProcess({
            BatchId: "BFD02713",
            Description: "Request AUC Info Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'GUID', GLOBAL.GUID);
        batch.Addparam(2, 'DOC_NO', fAction.GetDocNo());
        batch.StartBatch(function () {
            //location.reload();
            fAction.LoadData();
        }, true);

        if (callback == null)
            return;

        callback();
    },
    GenerateAssetNo: function (callback) {
        var batch = BatchProcess({
            BatchId: "BFD021A3",
            Description: "Request Asset No Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'GUID', GLOBAL.GUID);
        batch.Addparam(2, 'DOC_NO', fAction.GetDocNo());
        batch.StartBatch();

        if (callback == null)
            return;

        callback();
    },
    DeleteTargetAsset: function (_AssetNo, _AssetSub) {
        var _data = {
            GUID: GLOBAL.GUID,
            COMPANY: GLOBAL.COMPANY,
            ASSET_NO: _AssetNo,
            ASSET_SUB: _AssetSub
        }
        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(_AssetNo, _AssetSub), function (result) {
            if (!result) {
                return;
            }
            ajax_method.Post(Url2710.DeleteTargetAsset, _data, false, function (result) {
                if (result.IsError) {
                    return;
                }
                //location.reload();
                fAction.LoadData();
            });

        }); // loadConfirmAlert
    },

    ShowDetail: function (AUC_GROUP_KEY) {

        $('.' + AUC_GROUP_KEY + '_g').hide();
        $('.' + AUC_GROUP_KEY).show();

        control.DataTable.columns.adjust();

        fAction.History.push(AUC_GROUP_KEY);

        //var _selectedRow = control.DataTable.row($(sender).parents('tr'));
        //var _data = control.DataTable.row(_selectedRow).data();

        //$(sender).parents('tr').hide();
        //fAction.History = $.grep(fAction.History, function (n, i) {
        //    return n.AUC_GROUP_KEY != _data.AUC_GROUP_KEY;
        //});

        //fAction.History.push({ AUC_GROUP_KEY: _data.AUC_GROUP_KEY, visible: true });
        //$('.' + _data.AUC_GROUP_KEY).hide();
        //$("#tbSearchResult tr").each(function () {
        //    var $this = $(this);
        //    var row = $this.closest("tr");
        //    if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
        //        return;
        //    }

        //    if (control.DataTable == null) {
        //        return;
        //    }

        //    _row = control.DataTable.row(this).data(); // Get source data

        //    if (_row.AUC_GROUP_KEY == _data.AUC_GROUP_KEY) {
        //        console.log(_row.GRP_ROW_INDX);
        //        if (_row.GRP_ROW_INDX > 0)
        //            $(this).show();
        //    }
        //});
    },
    HideDetail: function (AUC_GROUP_KEY) {

        $('.' + AUC_GROUP_KEY + '_g').show();
        $('.' + AUC_GROUP_KEY).hide();

        control.DataTable.columns.adjust();

        fAction.History = $.grep(fAction.History, function (n, i) {
            return n !== AUC_GROUP_KEY;
        });

        //var _selectedRow = control.DataTable.row($(sender).parents('tr'));
        //var _data = control.DataTable.row(_selectedRow).data();

        //Remove when hide
        //fAction.History = $.grep(fAction.History, function (n, i) {
        //    return n.AUC_GROUP_KEY != _data.AUC_GROUP_KEY;
        //});

        //$('.' + _data.AUC_GROUP_KEY).show();
        //$("#tbSearchResult tr").each(function () {
        //    var $this = $(this);
        //    var row = $this.closest("tr");
        //    if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
        //        return;
        //    }

        //    if (control.DataTable == null) {
        //        return;
        //    }

        //    _row = control.DataTable.row(this).data(); // Get source data

        //    console.log(_row.GRP_ROW_INDX);

        //    if (_row.AUC_GROUP_KEY == _data.AUC_GROUP_KEY) {
        //        console.log(_row.GRP_ROW_INDX);
        //        if (_row.GRP_ROW_INDX > 0)
        //            $(this).hide();
        //        else {
        //            $(this).show();
        //        }
        //    }
        //});
    },
    SetMinus: function () {

    },
    Resend: function (callback) {
        var _header = fMainAction.GetParameter();
        var _list = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, { header: _header, list: _list }, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList -> Send


    },

    UpdateCapDate: function (tempAssetNo, tempSubNo, capDateStr) {
        var data = {
            guid: GLOBAL.GUID,
            docNo: fAction.GetDocNo(),
            tempAssetNo: tempAssetNo,
            tempSubNo: tempSubNo,
            capDateStr: capDateStr
        };

        ajax_method.Post(Url.UpdateCapDate, data, false, function (result) {
            if (result.IsError) {
                return;
            }
        });
    }
}

var fPopup = {
    selectedRow: {},
    selectedCol: {},
    selectedRows: [],
    ShowCostDialog: function (TargetAssetNo, TargetSub, SettleAmount, AdjType, button, colName) {
        var row = control.DataTable.row($(button).parents('tr')).data();

        //fPopup.selectedRow = row;
        //fPopup.selectedRow.GUID = GLOBAL.GUID;

        //fPopup.selectedCol = {
        //    TARGET_ASSET_NO: TargetAssetNo,
        //    TARGET_ASSET_SUB: TargetSub
        //};

        var initRow = function (r, amount) {
            var d = {
                GRP_ROW_INDX: r.GRP_ROW_INDX,
                GUID: GLOBAL.GUID,
                COMPANY: GLOBAL.COMPANY,
                AUC_NO: r.AUC_NO,
                AUC_SUB: r.AUC_SUB,
                SAP_DOC: r.SAP_DOC,
                PO_NO: r.PO_NO,
                INV_NO: r.INV_NO,
                INV_LINE: r.INV_LINE,
                TARGET_ASSET_NO: TargetAssetNo,
                TARGET_ASSET_SUB: TargetSub,
                AUC_REMAIN: r.AUC_REMAIN,
                SETTLE_AMOUNT: amount,
                //ADJ_TYPE: adjType,
                AUC_AMOUNT: r.AUC_AMOUNT
            };
            return d;
        };

        fPopup.selectedRows = [];

        if (row.GRP_ROW_INDX === 0) {
            //Default When group 
            AdjType = 'M';
            SettleAmount = (SettleAmount && SettleAmount !== 'null') ? SettleAmount : '0';

            $.each(control.DataTable.data(), function (i, item) {
                if ((item.AUC_GROUP_KEY === row.AUC_GROUP_KEY) && item.GRP_ROW_INDX !== 0) {
                    var amount = item[colName];
                    var data = initRow(item, amount);
                    fPopup.selectedRows.push(data);
                }
            });
        } else {
            var r = initRow(row, parseFloatWithComma(SettleAmount));
            fPopup.selectedRows.push(r);
        }

        // case existing
        if (AdjType != '' || AdjType != undefined) {
            $('#hddOldAdjType').val(AdjType);
            $('#hddOldSettleAmount').val(SettleAmount);
        } else {
            $('#hddOldAdjType').val('');
            $('#hddOldSettleAmount').val('');
        }
        $('#hddOldRemainCost').val(row.AUC_REMAIN);

        $('#txtCostAmount, #txtCostAll, #txtCostPercentage, #txtAmountByPercentage').val('');

        $('#lblRemainCost').text(currencyFormat(row.AUC_REMAIN));
        $('#txtCostValueOriginal').val(currencyFormat(row.AUC_AMOUNT));

        $('#AlertMessageWFD02710Cost').html('');
        $('#btnCostDialogOK').prop('disabled', false);
        $('#chkAll, #chkAmount, #chkPercentage').prop('checked', false);
        $('#txtCostAll, #txtCostAmount, #txtCostPercentage').prop('disabled', true);

        $("#txtCostAmount").autoNumeric('init', { aSep: ',', vMin: '0.00', vMax: '9999999999.99' });
        //$("#txtCostPercentage").autoNumeric('init', { vMin: '0.01', vMax: '100.00' });

        if (AdjType == 'A') {
            $('#chkAll').prop('checked', true);
            $('#txtCostAll').val(SettleAmount);
        }
        else if (AdjType == 'P') {
            $('#chkPercentage').prop('checked', true);
            $('#txtCostPercentage').prop('disabled', false);
            $('#txtAmountByPercentage').val(SettleAmount);

            // Amount2Percentage
            var numCostValueOriginal = row.AUC_AMOUNT;
            var numSettleAmount = parseFloatWithComma(SettleAmount);
            var costPercentage = (numSettleAmount / numCostValueOriginal) * 100;
            $('#txtCostPercentage').val(costPercentage.toFixed(2));

            reCalcRemainCost();
            //var numCostValueOriginal = row.AUC_REMAIN + numSettleAmount;
            //$('#txtCostPercentage').val(costPercentage.toFixed(2));
            // Amount2Percentage
            //var numCostValueOriginal = parseFloatWithComma($('#txtCostValueOriginal').val());
            //var numSettleAmount = parseFloatWithComma(SettleAmount);
            //var costPercentage = (numSettleAmount / numCostValueOriginal) * 100;
            //$('#txtCostPercentage').val(costPercentage.toFixed(2));
        }
        //else if (AdjType == 'M' || row.GRP_ROW_INDX === 0) {
        else if (AdjType == 'M') {
            $('#chkAmount').prop('checked', true);
            $('#txtCostAmount').prop('disabled', false);
            $('#txtCostAmount').val(SettleAmount);
        }
        //else if (row.GRP_ROW_INDX === 0) {
        //    if (SettleAmount === "null") {
        //        SettleAmount = "0.00";
        //    }
        //    $('#chkAmount').prop('checked', true);
        //    $('#txtCostAmount').prop('disabled', false);
        //    $('#txtCostAmount').val(SettleAmount);
        //}

        //Original
        /* if (row.AUC_AMOUNT == targetObj.SETTLE_AMOUNT) {
             $('#chkAll').prop('checked', true);
             $('#txtCostAll').val(row.AUC_AMOUNT);
 
         }
         else if (targetObj.PERCENTAGE > 0) {
             $('#chkPercentage').prop('checked', true);
             $('#txtCostPercentage').val(targetObj.PERCENTAGE);
             $('#txtCostPercentageAmount').val(targetObj.SETTLE_AMOUNT);
             $('#txtCostPercentage').prop('disabled', false);
         }
         else if (row.AUC_AMOUNT > targetObj.SETTLE_AMOUNT &&
             targetObj.SETTLE_AMOUNT != null) {
 
             $('#chkAmount').prop('checked', true);
             $('#txtCostAmount').val(targetObj.SETTLE_AMOUNT);
             $('#txtCostAmount').prop('disabled', false);
         }
         */
        $('#divCostDialog').modal();

        $('#txtCostPercentage').off('keypress').on('keypress', function (e) {
            return isNumberKey(e);
        });
    },

    _GetCost: function () {

        if ($('#chkAll').is(":checked")) {
            return $('#txtCostAll').val();
        }
        if ($('#chkAmount').is(":checked")) {
            return $('#txtCostAmount').val();
        }
        if ($('#chkPercentage').is(":checked")) {
            return $('#txtAmountByPercentage').val();
        }

        return 0;
    },
    _GetAdjType: function () {
        if ($('#chkAll').is(":checked")) {
            return 'A';
        }
        if ($('#chkAmount').is(":checked")) {
            return 'M';
        }
        if ($('#chkPercentage').is(":checked")) {
            return 'P';
        }
    },

    ShowAverageAssetDialog: function (button) {
        fPopup.selectedRows = [];

        var row = control.DataTable.row($(button).parents('tr')).data();
        //fPopup.selectedRow = row;
        //fPopup.selectedRow.GUID = GLOBAL.GUID;
        var initRow = function (row) {
            row.GUID = GLOBAL.GUID;
        };

        if (row.GRP_ROW_INDX === 0) {
            $.each(control.DataTable.data(), function (i, item) {
                if ((item.AUC_GROUP_KEY === row.AUC_GROUP_KEY) && item.GRP_ROW_INDX !== 0) {
                    initRow(item);
                    fPopup.selectedRows.push(item);
                }
            });

        } else {
            initRow(row);
            fPopup.selectedRows.push(row);
        }

        $('#divAverageDialog #btnAverageOK').attr("disabled", true);
        $('#divAverageDialog .AssetNo').prop("checked", false);
        $('#divAverageDialog #chkAverageSelectAll').prop("checked", false);
        $('#divAverageDialog').modal();
    },

    Open2115Popup: function (button) {

        var _popupWidth = $(window).width() - 20;
        var left = ($(window).width() / 2) - (_popupWidth / 2);
        top = ($(window).height() / 2) - (600 / 2);

        var _wbs = "", _auc = "", _sub = "", _final_class;
        var _rs = fAction.GetClientAssetList(false);
        if (_rs.length > 0) {
            _wbs = _rs[0].WBS_BUDGET;
            _final_asset = _rs[0].FINAL_ASSET_CLASS;
        }

        popup = window.open(MainUrl.NewAssetsPopup +
            "?GUID=" + GLOBAL.GUID +
            "&COMPANY=" + $('#WFD01170ddpCompany').val() +
            //"&DOC_NO=" + (GLOBAL.DOC_NO ? GLOBAL.DOC_NO : GLOBAL.TEMP_DOC_NO) +
            "&LINE_NO=0" +
            "&INVEST_REASON=" +
            "&RESP_COST_CODE=" +
            "&FLOW_TYPE=" + GLOBAL.FLOW_TYPE +
            "&MODE=N&OPTION=N" +
            "&ASSET_CLASS=" + _final_asset +
            "&ROLEMODE=" + $('#WFD01170ddpRequestorRole').val() +
            "&WBS_BUDGET=" + _wbs,
            "popup", "width=" + _popupWidth + ", height=600, top=" + top + ", left=" + left + ", scrollbars=yes");

    },

    ShowSearchExistingPopup: function (sender) {
        //Set search parameter

        f2190SearchAction.ClearDefault();


        //var _selectedList = fAction.GetClientAssetList(false); //false

        ////BOI same as original
        //if (_selectedList.length > 0) {
        //    Search2190Config.Default.CostCode = { Value: _selectedList[0].NEW_COST_CODE, Enable: false };
        //    Search2190Config.Default.ResponsibleCostCode = { Value: _selectedList[0].NEW_RESP_COST_CODE, Enable: false };
        //    Search2190Config.Default.INVEST_REASON = { Value: _selectedList[0].INVEST_REASON, Enable: false };

        //}

        Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();
        Search2190Config.searchOption = "KND";
        Search2190Config.AllowMultiple = true; //Not allow to select > 1

        Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
        Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: false };


        Search2190Config.customCallback = function (_targetList, callback) {
            console.log(_targetList);

            if (callback != null) {
                callback();
            }

            if (_targetList.length == 0)
                return;

            _targetList.forEach(function (_data) {
                // do something with `item`
                _data.GUID = GLOBAL.GUID;
                _data.USER_BY = GLOBAL.USER_BY;
            });

            //Update and Reload
            ajax_method.Post(Url2710.InsertExistsToTargetAsset, { data: _targetList }, false, function (result) {
                if (result.IsError) {
                    return;
                }

                fAction.LoadData();
                //location.reload();

            }); //ajax_method.UpdateCost

        };

        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();
    },
   
    OpenTargetAssetNo: function (_tempFlag, _Company, _RefNo, _AssetNo, _AssetSub, _wbs, _docNo) {
        if (_tempFlag == "Y") {
            var _popupWidth = $(window).width() - 20;
            var left = ($(window).width() / 2) - (_popupWidth / 2);
            top = ($(window).height() / 2) - (600 / 2);

            popup = window.open(MainUrl.NewAssetsPopup +
                "?GUID=" + GLOBAL.GUID +
                "&COMPANY=" + _Company +
                "&DOC_NO=" + _docNo +
                "&LINE_NO=" + _RefNo +
                "&INVEST_REASON=" +
                "&RESP_COST_CODE=" +
                "&FLOW_TYPE=" + GLOBAL.FLOW_TYPE +
                "&MODE=N&OPTION=E" +
                "&ROLEMODE=" + $('#WFD01170ddpRequestorRole').val() +
                "&ASSET_NO=" + _AssetNo +
                "&ASSET_SUB=" + _AssetSub,
                "popup", "width=" + _popupWidth + ", height=600, top=" + top + ", left=" + left + ", scrollbars=yes");
            return;
        }
        window.open("../WFD02130?COMPANY=" + _Company + "&ASSET_NO=" + _AssetNo + "&ASSET_SUB=" + _AssetSub, "_blank");
    }
}
//Call this function when PG set screen mode
var fScreenMode = {

    _GridVisible: function (_bVisible) {
        //control.DataTable.column(control.ColumnIndex.Sel).visible(_bVisible);  // CheckBox
        //control.DataTable.column(control.ColumnIndex.Depreciation).visible(_bVisible);  // Account Principle
        //control.DataTable.column(control.ColumnIndex.Status).visible(_bVisible); // Status
    },
    _ClearMode: function () {
        console.log('WFD02710_ClearMode');
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').hide();
        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').prop("disabled", true);
    },

    Submit_InitialMode: function (callback) {
        console.log('WFD02710_Submit_InitialMode');
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd').prop("disabled", false);


        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').show();
        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').prop("disabled", true);


        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        console.log('WFD02710_Submit_AddAssetsMode');
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').removeClass('disabled').prop("disabled", false);

        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').show();
        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').prop("disabled", false);

        $.each(control.DisableBtns, function (i, $btn) {
            $btn.prop("disabled", false);
        });

        control.DisableBtns = [];

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();
        console.log('WFD02710_Submit_GenerateFlow');
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').removeClass('disabled').prop("disabled", false);

        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').show();
        $('#btnRequestAddNewAsset, #btnRequestAddCurrentAsset, #btnRequestRequestAUCInvoice').prop("disabled", true);

        var $btns = $('button.td-btn-cost');
        $.each($btns, function (i, btn) {
            var $btn = $(btn);
            if ($btn.is(':enabled')) {
                $btn.prop("disabled", true);
                control.DisableBtns.push($btn);
            }
        });

        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) {
        //fScreenMode._ClearMode();
        //console.log("#Approve_Mode");
        ////Document
        // if (GLOBAL.STATUS === '20') { //should be check user permission
        //    fScreenMode._Approve_AEC();
        //}

        //if (callback == null)
        //    return;
        //callback();

        console.log("#_ClearMode");
        fScreenMode._ClearMode();

        $('#btnRequestExport').show();
        $('#btnRequestExport').prop("disabled", false);



        if (PERMISSION.AllowResend == "Y" || !$('#WFD01170btnResend').is(":disabled")) {
            $('#WFD02710_SelectAll').show();
            $('#WFD02710_SelectAll').prop("disabled", false);
        }

        //    $('button.CostDialog').prop("disabled", false);


        if (callback == null)
            return;
        callback();
        return;

    },

    //_Approve_AEC: function () {

    //    fScreenMode._GridVisible(true);
    //    //20
    //    $('#btnRequestExport').show();
    //    $('#btnRequestExport').removeClass('disabled').prop("disabled", false);

    //    $('button.CostDialog').prop("disabled", false);
    //    $('input.Remark').prop("disabled", false);

    //    $('.chkGEN, #WFD02710_SelectAll').prop("disabled", false);
    //}

}

var fRender = {
    CheckBoxRender: function (data) {
        if (data.GRP_ROW_INDX !== 0 && data.HAS_GROUP) {
            return "";
        }

        var _disabled = "";
        if (!data.AllowCheck)
            //return '';
            _disabled = "disabled";

        var _strHtml = '';
        var _strDisable = '';
        var _strChecked = '';
        if (data.IS_ROW_FOOTER != 'Y') {
            _strHtml = '<input style="width:20px; height:28px;" class="chkGEN chk_' + data.AUC_GROUP_KEY +'" ' + _disabled + ' value="' + data.LINE_NO + '" onclick="fAction.DetailCheck(this,\'' + data.LINE_NO + '\',\'' + data.AUC_GROUP_KEY + '\');" type="checkbox"' + _strDisable + ' ' + _strChecked + ' >';
        }
        return _strHtml;
    },

    AverageAssetNoRender: function (data) {

        var _disabled = "";
        if (!data.AllowEdit)
            //return '';
            _disabled = "disabled";


        if (data.AUC_AMOUNT == null || data.AUC_AMOUNT == 0) {
            _disabled = "disabled";
        }

        if (data.AUC_REMAIN <= 0) {
            _disabled = "disabled";
        }

        //console.log($.("#tbSearchResult > thead > tr: nth - child(2) > th: nth - child(13) > div"));

        var _id = 'WFD02710_ButtonAvgAsset_' + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        var _text = '...';
        return '<div class="btn-group">' +
            '<button id="' + uuidv4() +'" type="button" ' + _disabled + ' class="btn btn-info btn-block td-btn-cost" onclick="fPopup.ShowAverageAssetDialog(this);">' +
            _text + '</button>' +
            '</div>';
    },


    HyperLinkRender: function (data) {
        var _button = "";

        if (data.GRP_ROW_INDX == 1) {
            if (data.HAS_GROUP) {
                _button = '<i class="hand fa fa-minus" aria-hidden="true" onclick="fAction.HideDetail(\'' + data.AUC_GROUP_KEY + '\');"></i>';
            }
            return '<a href="../WFD02130?COMPANY=' +
                data.COMPANY + '&ASSET_NO=' + data.AUC_NO + '&ASSET_SUB=' + data.AUC_SUB + '" target="_blank">' +
                data.AUC_NO + '</a>&nbsp;' + _button;

        }
        if (data.GRP_ROW_INDX == 0) {
            _button = '<i class="hand fa fa-plus" aria-hidden="true" onclick="fAction.ShowDetail(\'' + data.AUC_GROUP_KEY + '\');"></i>';
            return '<a href="../WFD02130?COMPANY=' +
                data.COMPANY + '&ASSET_NO=' + data.AUC_NO + '&ASSET_SUB=' + data.AUC_SUB + '" target="_blank">' +
                data.AUC_NO + '</a>&nbsp;' + _button;

        }
        return '';

    },
    TextBoxRender: function (data) {


        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<input type="text" id="txtReason_' + _id + '" name="txtReason" class="form-control Remark" maxlength="50" value="' + (data.REASON == null ? "" : data.REASON) + '" />'
    },
    SettlementButtonRender: function (data, _TargetAssetNo, _TargetAssetSub, _Amount, _AdjType, colName) {
        // return fRender.SettlementButtonRender(full, full.INVOICE_GROUP_KEY, value.COL_INDX, _mapdata.SETTLE_AMOUNT);
        var _disabled = "";

        //if (!data.AllowEdit /*|| (_Amount == null || _Amount == 0)*/
        //    || isGroup) {
        //    //return '';
        //    _disabled = "disabled";
        //}

        if (!data.AllowEdit /*|| (_Amount == null || _Amount == 0)*/) {
            //return '';
            _disabled = "disabled";
        }
        else {
            if (_Amount == null || _Amount == 0) {
                _disabled = "disabled";
            }

            if ((data.AUC_AMOUNT == null || data.AUC_AMOUNT == 0 || data.AUC_REMAIN == null || data.AUC_REMAIN == 0) && (_Amount == 0 || _Amount == null)) {
                _disabled = "disabled";
            } else {
                _disabled = "";
            }

        }



        var _text = _Amount;
        if (_Amount == null || _Amount == 0) {
            _text = ' ... ';
            //_disabled = "disabled";
        }

        return '<button id="' + uuidv4() + '" type="button" ' + _disabled + ' class="btn btn-info btn-block td-btn-cost"' +
            'onclick="fPopup.ShowCostDialog(\'' + _TargetAssetNo + '\',\'' + _TargetAssetSub + '\',\'' + _Amount + '\',\'' + _AdjType + '\', this,\'' + colName + '\');return false;">' + _text + '</button>';


        //if (data.AMOUNT_POSTED != null) {
        //    _text = data.AMOUNT_POSTED;
        //}
        //var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        //return '<input type="hidden" id="hdOriginal_"' + _id + '" name="hdOriginal" value="' + data.BF_CUMU_ACQU_PRDCOST_01 + '" />' +
        //        '<input type="hidden" id="hdPercent_"' + _id + '" name="hdPercent" value="' + data.PERCENTAGE + '" />' +
        //        '<input type="hidden" id="hdAmount_"' + _id + '" name="hdAmount" value="' + _text + '"/>' +
        //        '<button type="button" id="btnAmountPosted_' + _id + '" name="btnAmountPosted" class="CostDialog btn btn-info" ' +
        //            'onclick="fAction.ShowCostDialog(this);return false;">' + _text + '</button>';

        //,\'' + data.COMPANY + '\',\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\'
    },
    DeleteButtonRender: function (data) {
        var _disabled = "";

        if (!data.AllowDelete)
            //return '';
            _disabled = "disabled";

        if (data.AUC_AMOUNT != data.AUC_REMAIN) {
            _disabled = "disabled";
        }

        var _strHtml = '';
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button class="btn btn-danger btn-xs" data-title="Delete" type="button" ' + _disabled +
            ' onclick=fAction.DeleteAsset("' + data.AUC_NO + '","' + data.AUC_SUB + '","' + data.INV_NO + '","' + data.INV_LINE + '")>' +
            '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },
    TargetAssetHeaderRender: function (data) {
        var closeBtn = '<span class="glyphicon glyphicon-remove" aria-hidden="true">';
        if (GLOBAL.STATUS != '00' || !PERMISSION.AllowEditBeforeGenFlow) {
            closeBtn = '';
        }
        var _strHtml = '';

        if (data.STATUS === "ERROR") {
            data.MESSAGE = data.MESSAGE ? data.MESSAGE : '';
            _strHtml += '<i data-toggle="tooltip"class="red-tooltip" data-html="true" title="<div><b>Status:</b><br><div>' + data.MESSAGE + '</div></div>" class="fa fa-exclamation" style="color: red;padding-right: 5px;" aria-hidden="true"></i>';
        }

        _strHtml += '<a href="#"  data-toggle="tooltip" data-html="true" title="<b>Asset Name:</b><br>' + data.TARGET_ASSET_NAME + '<br><b>Asset Class:</b><br>' + data.TARGET_ASSET_CLASS + '" data-placement="bottom" onclick="fPopup.OpenTargetAssetNo(\'' + data.TEMP_ASSETS + '\',\'' + data.COMPANY + '\',\'' + data.REF_LINE_NO + '\',\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\',\'' + data.WBS_BUDGET + '\',\'' + data.DOC_NO + '\');return false;">' + data.ColumnText + '</a>'
            + '<a href="#" onclick="fAction.DeleteTargetAsset(\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\');return false;">' + closeBtn + '</a></div>';
        return _strHtml;
    },
    TargetAssetListRender: function () {
        $('#divTargetAssetList').html(''); //Clear


        var _html = "";
        $.each(fAction.TargetAssetList, function (key, value) {
            _html += '<div class="form-group"><div class="col-sm-2 text-right">' +
                '   <input id="chk_' + value.COL_INDX + '" type="checkbox" class="AssetNo custom-control-input" tabindex="3" value="' + value.COL_INDX + '" style="width:24px;height:24px;" onclick="fAction.AverageCostDetailCheck(this);" />' +
                '</div>' +
                '<div class="col-sm-10">' +
                '   <label class="control-label" for="chk_' + value.COL_INDX + '"> ' + value.ASSET_NO + ' ' + value.ASSET_SUB + '</label>' +
                '</div></div>';
        });
        $('#divTargetAssetList').html(_html);
    },

    commaSeparateNumber: function (data) {
        if (data != null) {
            data = currencyFormat(data);
            //while (/(\d+)(\d{3})/.test(data.toString())) {
            //    data = parseFloat(data).toFixed(2).replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            //}
        }
        return data;
    }
}

$('#btnRequestAddNewAsset').click(function () {

    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }

    fPopup.Open2115Popup(this);
});

$('#btnRequestAddCurrentAsset').click(function () {

    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }

    fPopup.ShowSearchExistingPopup(this);
});

$('#btnRequestRequestAUCInvoice').click(function () {

    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }

    fAction.RequesetAUCInfo(function () {
        //alert("Request processing..");
    });
}),

    $('#WFD02710_SelectAll').click(function (e) {
        $('.chkGEN').prop('checked', $(this).is(':checked'));
    })
// Popup Control Event
$('#divAverageDialog #chkAverageSelectAll').click(function (e) {

    var _checked = $('#divAverageDialog #chkAverageSelectAll').is(":checked");
    $('#divAverageDialog .AssetNo').prop("checked", _checked);

    if ($('#divAverageDialog #chkAverageSelectAll').is(":checked")) {
        $('#btnAverageOK').attr("disabled", false);
    }
    else {
        $('#btnAverageOK').attr("disabled", true);
    }
})

$('#divAverageDialog #btnAverageOK').click(function (e) {

    fAction.AverageCost(function () {
        $('#divAverageDialog').modal('hide');
    });

})

// Popup Control Event
$('#divCostDialog #chkAll').click(function (e) {
    //recalculate for real remaining
    reCalcRemainCost();
    // clear control value
    $('#txtCostAmount, #txtCostAll, #txtCostPercentage, #txtCostPercentageAmount, #txtAmountByPercentage').val('');
    // default old value
    if ($('#hddOldAdjType').val() != '' && $('#hddOldAdjType').val() == 'A') {
        $('#txtCostAll').val($('#hddOldSettleAmount').val());
        $('#lblRemainCost').text($('#hddOldRemainCost').val());
    }
    console.log($('#txtCostAll').val() + " " + $('#hddOldAdjType').val() + " " + $('#lblRemainCost').text());
    $('#divCostDialog #chkAmount, #divCostDialog #chkPercentage').prop('checked', false);
    $('#divCostDialog #txtCostAmount, #divCostDialog #txtCostPercentage').prop('disabled', true);

    if ($('#divCostDialog #chkAll').is(':checked')) {
        $('#divCostDialog #txtCostAll').val($('#lblRemainCost').text());
    } else {
        $('#divCostDialog #txtCostAll').val('');
    }

    reCalcRemainCost();
})
$('#divCostDialog #chkAmount').click(function (e) {
    reCalcRemainCost();
    // clear control value
    $('#txtCostAmount, #txtCostAll, #txtCostPercentage, #txtCostPercentageAmount, #txtAmountByPercentage').val('');
    // default old value
    if ($('#hddOldAdjType').val() != '' && $('#hddOldAdjType').val() == 'M') {
        $('#txtCostAmount').val($('#hddOldSettleAmount').val());
        $('#lblRemainCost').text($('#hddOldRemainCost').val());
    }

    $('#divCostDialog #chkAll, #divCostDialog #chkPercentage').prop('checked', false);
    $('#divCostDialog #txtCostAmount, #divCostDialog #txtCostPercentage').prop('disabled', true);

    if ($('#chkAmount').is(':checked')) {
        $('#txtCostAmount').prop('disabled', false);
    } else {
        $('#txtCostAmount').val('');
    }

    reCalcRemainCost();

})
$('#divCostDialog #chkPercentage').click(function (e) {
    //reCalcRemainCost();
    // clear control value
    $('#txtCostAmount, #txtCostAll, #txtCostPercentage, #txtCostPercentageAmount, #txtAmountByPercentage').val('');
    // default old value
    if ($('#hddOldAdjType').val() != '' && $('#hddOldAdjType').val() == 'P') {
        $('#txtAmountByPercentage').val($('#hddOldSettleAmount').val());
        $('#lblRemainCost').text($('#hddOldRemainCost').val());

        //// Amount2Percentage
        var numSettleAmount = parseFloatWithComma($('#hddOldSettleAmount').val());
        var remainCost = parseFloatWithComma($('#hddOldRemainCost').val());
        var costPercentage = (numSettleAmount / (remainCost + numSettleAmount)) * 100;
        $('#txtCostPercentage').val(costPercentage.toFixed(2));
        //// Amount2Percentage
        //var numCostValueOriginal = parseFloatWithComma($('#txtCostValueOriginal').val());
        //var numSettleAmount = parseFloatWithComma($('#hddOldSettleAmount').val());
        //var costPercentage = (numSettleAmount / numCostValueOriginal) * 100;
        //$('#txtCostPercentage').val(costPercentage.toFixed(2));
    }

    $('#divCostDialog #chkAll, #divCostDialog #chkAmount').prop('checked', false);
    $('#divCostDialog #txtCostAmount, #divCostDialog #txtCostPercentage').prop('disabled', true);

    if ($('#divCostDialog #chkPercentage').is(':checked')) {
        $('#divCostDialog #txtCostPercentage').prop('disabled', false);

    } else {
        $('#divCostDialog #txtCostPercentage').val('');
    }

    calcPercentage2Amount();
    //reCalcRemainCost();
})

$('#divCostDialog #btnCostDialogOK').click(function (e) {
    if ($('#divCostDialog input:checked').length == 0) {
        AlertTextErrorMessagePopup('MCOM0017AERR : Please select one of choice', "#AlertMessageWFD02710Cost");
        window.scrollTo(0, 0);
        return;
    }

    fAction.UpdateCost(function () {
        $('#divCostDialog').modal('hide');
    });

})

$('#btnGenerateAssetNo').click(function () {
    fMainAction.GenerateAssetNo(true);
})

//POP FIX WHEN HIDE NAV BAR
$('.sidebar-toggle').on('click', function () {
    setTimeout(function () {
        control.DataTable.columns.adjust();
    }, 300);
});

function calcPercentage2Amount() {
    //reCalcRemainCost();
    ////alert($('#hddRemainCost').val());
    //var costValueOriginal = $('#hddRemainCost').val();
    //var costPercentage = $('#txtCostPercentage').val();

    //if (costPercentage != null) {
    //    var numCostValueOriginal = parseFloatWithComma(costValueOriginal);
    //    var numCostPercentage = parseFloatWithComma(costPercentage);

    //    var amountByPercentage = (numCostPercentage / 100) * numCostValueOriginal;
    //    $('#txtAmountByPercentage').val(currencyFormat(amountByPercentage.toFixed(2)));

    //} else {
    //    $('#txtAmountByPercentage').val('');
    //}
    var costPercentage = $('#txtCostPercentage').val();
    if (costPercentage !== null) {
        var numCostPercentage = parseFloatWithComma(costPercentage);
        var numCostValueOriginal = parseFloatWithComma($('#txtCostValueOriginal').val());

        var amountByPercentage = (numCostPercentage / 100) * numCostValueOriginal;
        $('#txtAmountByPercentage').val(currencyFormat(amountByPercentage.toFixed(2)));

    } else {
        $('#txtAmountByPercentage').val('');
    }

    reCalcRemainCost();
}

function reCalcRemainCost() {
    var oldAdjType = $('#hddOldAdjType').val();
    var oldSettleAmount = $('#hddOldSettleAmount').val();
    var oldRemainCost = $('#hddOldRemainCost').val();
    var numOldRemainCost = parseFloatWithComma(oldRemainCost);
    // case existing
    if (oldAdjType != '' && oldAdjType != "null") {
        numOldRemainCost = numOldRemainCost + parseFloatWithComma(oldSettleAmount);
    }

    var cost = fPopup._GetCost();
    if (cost != '') { //fond cost in input
        var remainCost = numOldRemainCost - parseFloatWithComma(cost);
        $('#lblRemainCost').text(currencyFormat(remainCost));
        if ($('#chkAll').is(":checked")) {
            $('#lblRemainCost').text(currencyFormat(0));
            $('#txtCostAll').val(currencyFormat(numOldRemainCost));
        }
        //else if ($('#chkPercentage').is(":checked")) {
        //    var amountByPercentage = parseFloatWithComma($('#txtAmountByPercentage').val());
        //    $('#lblRemainCost').text(currencyFormat(amountByPercentage));
        //var numAmountByPercentage = parseFloatWithComma($('#txtAmountByPercentage').val());
        //$('#hddRemainCost').val(currencyFormat(numAmountByPercentage + remainCost));
        //}
    } else {
        $('#lblRemainCost').text(currencyFormat(numOldRemainCost));
        $('#hddRemainCost').val(currencyFormat(numOldRemainCost));
    }
    $("#AlertMessageWFD02710Cost").html('');
    $('#btnCostDialogOK').prop('disabled', false);

    if ($('#lblRemainCost').text() != '') {
        if (parseFloat($('#lblRemainCost').text()) < 0) {
            AlertTextErrorMessagePopup('MCOM0017AERR : Amount greater than Cost Value or Remain Cost', "#AlertMessageWFD02710Cost");
            $('#btnCostDialogOK').prop('disabled', true);
            window.scrollTo(0, 0);
        }
    }
}

function onCapDateChange(input) {
    var $input = $(input);
    var dateStr = $input.val();
    var assetNo = $input.attr('asset-no');
    var assetSub = $input.attr('asset-sub');
    fAction.UpdateCapDate(assetNo, assetSub, dateStr);
};
