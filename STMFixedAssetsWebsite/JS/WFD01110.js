﻿var Controls = {
    WFD01110VIEW_MODE: $('.div_view'),
    WFD01110EDIT_MODE: $('.div_edit'),
    WFD01110EDIT_BTN: $('#WFD01110Edit'),
    WFD01110UPLOAD_BTN: $('#WFD01110Upload'),
    WFD01110CANCEL_BTN: $('#WFD01110Cancel'),
    WFD01110SAVE_BTN: $('#WFD01110Save'),
    WFD01110EDITOR: $('#ANNOUNCE_DESC'),
    WFD01110UPLOAD_DIALOG: $('#upload-dialog'),
    WFD01110SELECT_IMAGE: $('#Image_Upload'),
    WFD01110IMAGE_PREVIEW: $('#Image_Priview'),
    WFD01110TABLE_STOCK_TAKE: $('#tb_stock_take'),
    WFD01110TABLE_TRANSFER: $('#tb_transfer'),
    WFD01110TABLE_CIPCLOSE: $('#tb_cipclose'),
    WFD01110TABLE_DISPOSE: $('#tb_dispose'),
    WFD01110TABLE_REPRINT: $('#tb_reprint'),
    WFD01110TABLE_ONPROCESS: $('#tb_onprocess'),
    WFD01110TABLE_STOCKTAKING: $('#tb_stocktaking'),
    WFD01110UPLOADSAVE_BTN: $('#WFD01110UploadSave'),
    WFD01110UPLOADCANCEL_BTN: $('#WFD01110UploadCancel'),
    WFD01110Image_UploadTemp: $('#WFD01110Image_UploadTemp'),

    PaginTRANSFER: $('#tb_transfer').Pagin(URL_CONST.ITEM_PER_PAGE),
    //PaginCIPCLOSE: $('#tb_cipclose').Pagin(URL_CONST.ITEM_PER_PAGE),
    //PaginDISPOSE: $('#tb_dispose').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginREPRINT: $('#tb_reprint').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginONPROCESS: $('#tb_onprocess').Pagin(URL_CONST.ITEM_PER_PAGE),

    PaginSTOCKTAKING: $('#tb_stocktaking').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginSTOCKTAKE: $('#tb_stock_take').Pagin(URL_CONST.ITEM_PER_PAGE),

    WFD01110SELECTTYPE: $('#REQUEST_TPYE'),
    WFD01110SELECTTYPE_AEC: $('#REQUEST_TYPE_AEC'),
    WFD01110BTNIMAGEUPLOAD: $('#BtnImageUpload'),

    PaginNewAsset: $('#tb_asset_new').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginSettlement: $('#tb_settlement').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginReclass: $('#tb_reclassification').Pagin(URL_CONST.ITEM_PER_PAGE),

    PaginInfoChange: $('#tb_asset_change').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginRetirement: $('#tb_retirement').Pagin(URL_CONST.ITEM_PER_PAGE),

    PaginImpairment: $('#tb_imparment').Pagin(URL_CONST.ITEM_PER_PAGE),

    PaginPhysical: $('#tb_physical_count').Pagin(URL_CONST.ITEM_PER_PAGE),
    PaginOnAEC: $('#tb_onhandAEC').Pagin(URL_CONST.ITEM_PER_PAGE)

};
var MAIN_COMPANY = null;
$(function () {
    BindAnnuncement();

    $('.modal ').appendTo($('body'));
    Controls.WFD01110BTNIMAGEUPLOAD.on('click', function () {
        //Controls.WFD01110SELECT_IMAGE.val('');
        Controls.WFD01110SELECT_IMAGE.click();
    });
    //CKEDITOR.replace('ANNOUNCE_DESC');

    Controls.WFD01110UPLOADSAVE_BTN.click(WFD01110UPLOADSAVE_CLICK);
    Controls.WFD01110UPLOADCANCEL_BTN.click(WFD01110UPLOADCANCEL_CLICK);
    Controls.WFD01110EDIT_BTN.click(WFD01110EDIT_CLICK);
    Controls.WFD01110EDIT_MODE.hide();
    Controls.WFD01110CANCEL_BTN.click(WFD01110CANCEL_CLICK);
    Controls.WFD01110SAVE_BTN.click(WFD01110SAVE_CLICK);
    Controls.WFD01110UPLOAD_BTN.click(WFD01110UPLOAD_CLICK);
    Tabhome();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        if (target == "#tab_home") {
            Tabhome();
        }
        else if (target == "#tab_transfer") {
            Controls.PaginTRANSFER.ClearPaginData();
            requestPageing = Controls.PaginTRANSFER;
            TransferSearch();
        }
        else if (target == "#tab_asset_new") {
            Controls.PaginNewAsset.ClearPaginData();
            requestPageing = Controls.PaginNewAsset;
            AssetNewSearch();
        }
        else if (target == "#tab_settlement") {
            Controls.PaginSettlement.ClearPaginData();
            requestPageing = Controls.PaginSettlement;
            SettlementSearch();
        }
        else if (target == "#tab_reclassification") {
            Controls.PaginReclass.ClearPaginData();
            requestPageing = Controls.PaginReclass;
            ReclassificationSearch();
        }
        //AssetChange	
        else if (target == "#tab_asset_change") {
            Controls.PaginInfoChange.ClearPaginData();
            requestPageing = Controls.PaginInfoChange;
            AssetChangeSearch();
        }
        //Retirement	
        else if (target == "#tab_retirement") {
            Controls.PaginRetirement.ClearPaginData();
            requestPageing = Controls.PaginRetirement;

            RetirementSearch();
        }
        //Imparment	
        else if (target == "#tab_imparment") {
            Controls.PaginImpairment.ClearPaginData();
            requestPageing = Controls.PaginImpairment;
            ImparmentSearch();
        }
        //PhysicalCount							
        else if (target == "#tab_physical_count") {
            Controls.PaginPhysical.ClearPaginData();
            requestPageing = Controls.PaginPhysical;
            PhysicalCountSearch();
        }
        else if (target == "#tab_onhandaec") {

            Controls.WFD01110SELECTTYPE_AEC.select2({
                width: '100%'
            }).select2("val", '');
            Controls.WFD01110SELECTTYPE_AEC.html('');
            $.ajax({
                url: URL_CONST.GET_SELTET_TYPE,
                type: 'POST',
                dataType: 'json',
                success: function (json) {
                    Controls.WFD01110SELECTTYPE_AEC.append($('<option>').text('All').attr('value', ''));
                    $.each(json.ObjectResult.data, function (i, value) {
                        Controls.WFD01110SELECTTYPE_AEC.append($('<option>').text(value.VALUE).attr('value', value.CODE));

                    });
                }
            });

            Controls.PaginOnAEC.ClearPaginData();
            requestPageing = Controls.PaginOnAEC;
            OnHandAECSearch();
        }
        else if (target == "#tab_reprint") {
            Controls.PaginREPRINT.ClearPaginData();
            requestPageing = Controls.PaginREPRINT;
            ReprintSearch();
        }


        else if (target == "#tab_onprocess") {
            Controls.WFD01110SELECTTYPE.select2({
                width: '100%'
            }).select2("val", '');
            Controls.WFD01110SELECTTYPE.html('');
            $.ajax({
                url: URL_CONST.GET_SELTET_TYPE,
                type: 'POST',
                dataType: 'json',
                success: function (json) {
                    Controls.WFD01110SELECTTYPE.append($('<option>').text('All').attr('value', ''));
                    $.each(json.ObjectResult.data, function (i, value) {
                        Controls.WFD01110SELECTTYPE.append($('<option>').text(value.VALUE).attr('value', value.CODE));
                    });
                }
            });

            GetOnProcessTable(Controls.WFD01110TABLE_ONPROCESS, Controls.PaginONPROCESS, Controls.WFD01110SELECTTYPE.val(), $('#div_onprocess'));
        }
    });

    Controls.WFD01110SELECTTYPE_AEC.change(function () {
        Controls.PaginOnAEC.ClearPaginData();
        requestPageing = Controls.PaginOnAEC;
        OnHandAECSearch();
    });

    Controls.WFD01110SELECTTYPE.change(function () {
        GetOnProcessTable(Controls.WFD01110TABLE_ONPROCESS, Controls.PaginONPROCESS, Controls.WFD01110SELECTTYPE.val(), $('#div_onprocess'));
    });

    $('#MultipleBoxOnProcessCompany').change(function () {
        GetOnProcessTable(Controls.WFD01110TABLE_ONPROCESS, Controls.PaginONPROCESS, Controls.WFD01110SELECTTYPE.val(), $('#div_onprocess'));
    });

    Controls.WFD01110SELECT_IMAGE.change(function () {
        if ($("#Image_Upload").val() != null && $("#Image_Upload").val() != '') {
            var ext = this.value.substr((this.value.lastIndexOf('.') + 1));

            var file = Controls.WFD01110SELECT_IMAGE[0].files[0]
            var arr = PARAMETER.FILEEXTENSION.split(",");

            if (CheckExtensionPopup(ext, arr, file.name, PARAMETER.FILEEXTENSION, true)) {
                readURL(this);
            } else {

                this.value = '';
            }
        }
    });

    //Controls.WFD01110SELECT_IMAGE.change(function () {
    //    //var ext = this.value.match(/\.(.+)$/)[1]; not support folder include .
    //    if ($("#Image_Upload").val() != null && $("#Image_Upload").val() != '') {
    //        var ext = this.value.substr((this.value.lastIndexOf('.') + 1));

    //        var file = Controls.WFD01110SELECT_IMAGE[0].files[0]
    //        var arr = PARAMETER.FILEEXTENSION.split(",");

    //        if (CheckExtensionPopup(ext, arr, file.name, PARAMETER.FILEEXTENSION, true)) {
    //            readURL(this);
    //        } else {

    //            this.value = '';
    //        }
    //        var _fileVal = $("#Image_Upload").val();

    //        var clone = $(this).clone();
    //        clone.attr('id', 'field2');
    //        $('#Image_UploadTemp').html(clone);


    //        //var fileInput = document.getElementById('Image_Upload');
    //        //document.getElementById('Image_UploadTemp') = fileInput;
    //        $("#Image_Upload").val('');
    //    }
    //    //switch (ext.toLowerCase()) {
    //    //    case 'jpg':
    //    //    case 'jpeg':
    //    //    case 'png':
    //    //    case 'gif':
    //    //    case 'bmp':
    //    //    case 'jfif':
    //    //        readURL(this);
    //    //        break;
    //    //    default:
    //    //        fnErrorDialog('Error', 'File ' + file.name + ' is not expected file type image');
    //    //        this.value = '';
    //    //}

    //});
    if (URL_CONST.FAADMIN != "True") {
        Controls.WFD01110EDIT_BTN.hide();
        Controls.WFD01110UPLOAD_BTN.hide();
    }

    // Select tab depend on priority from system master by Pawares M. 20180718
    if (PARAMETER.PREVPAGE == "WFD01170") {
        $.ajax({
            type: "POST",
            url: URL_CONST.TAB_PRIORITY,
            data: null,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8;',
            success: function (result) {
                $.each(result.ObjectResult, function (key, value) {
                    var i;
                    for (i = 1; i < 5; i++) {
                        if (value.VALUE == i) {
                            if (value.CODE == 'D') {
                                if (PARAMETER.TABDISPOSE > 0 && PARAMETER.TABDISPOSE != null && PARAMETER.TABDISPOSE != '') {
                                    $('a[href="#tab_dispose"]').tab('show');
                                    return false;
                                }
                            }
                            else if (value.CODE == 'T' || value.CODE == 'M') {
                                if (PARAMETER.TABTRANSFER > 0 && PARAMETER.TABTRANSFER != null && PARAMETER.TABTRANSFER != '') {
                                    $('a[href="#tab_transfer"]').tab('show');
                                    return false;
                                }
                            }
                            else if (value.CODE == 'R' || value.CODE == 'L') {
                                if (PARAMETER.TABREPRINT > 0 && PARAMETER.TABREPRINT != null && PARAMETER.TABREPRINT != '') {
                                    $('a[href="#tab_reprint"]').tab('show');
                                    return false;
                                }
                            }
                            else if (value.CODE == 'S') {
                                if (PARAMETER.TABSTOCKTACKING > 0 && PARAMETER.TABSTOCKTACKING != null && PARAMETER.TABSTOCKTACKING != '') {
                                    $('a[href="#tab_stocktacking"]').tab('show');
                                    return false;
                                }
                            }
                            else if (value.CODE == 'C') {
                                if (PARAMETER.TABCIPCLOSE > 0 && PARAMETER.TABCIPCLOSE != null && PARAMETER.TABCIPCLOSE != '') {
                                    $('a[href="#tab_cipclose"]').tab('show');
                                    return false;
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    $('#MultipleBoxCompany').change(function () {
        TransferSearch();
    })

    $('#TransferType').change(function () {
        TransferSearch();
    })

    $('#AssetNew_MultipleBoxCompany').change(function () {
        AssetNewSearch();
    })

    $('#Settlement_MultipleBoxCompany').change(function () {
        SettlementSearch();
    })

    //Reclassification	
    $('#Reclassification_MultipleBoxCompany').change(function () {
        ReclassificationSearch();
    })

    //AssetChange	
    $('#AssetChange_MultipleBoxCompany').change(function () {
        AssetChangeSearch();
    })

    //Retirement	
    $('#Retirement_MultipleBoxCompany, #RetirementType').change(function () {
        RetirementSearch();
    })

    //Imparment	
    $('#Imparment_MultipleBoxCompany').change(function () {
        ImparmentSearch();
    })

    $('#MultipleBoxOnAECCompany').change(function () {
        OnHandAECSearch();
    })


    //PhysicalCount	
    $('#PhysicalCount_MultipleBoxCompany').change(function () {
        PhysicalCountSearch();
    })


});


function Tabhome() {

    SummaryAECList();

    if (URL_CONST.CHECKSTOCK != '') {
        Controls.WFD01110TABLE_STOCK_TAKE.DataTable().destroy();
        StockSearch();
    }

}
//function drawCallBack() {
//    $('#coutStock').text(this.fnSettings().fnRecordsTotal());
//}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            Controls.WFD01110IMAGE_PREVIEW.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function generateHtmlContent(result) {
    var htmlContent = ''
    for (var i = 0; i < result.data.length; i++) {

        htmlContent = '<div id="home-carousel"><div id="carousel-generic" class="carousel slide" data-ride="carousel">';
        htmlContent += '<ol class="carousel-indicators ">';

        for (var j = 0; j < result.data.length; j++) {
            var isActive = '';
            if (j < 1) {
                isActive = 'active'
            }
            htmlContent += '<li data-target="#carousel-generic" data-slide-to="' + [j] + '" class="' + isActive + '"></li>'
        }
        htmlContent += '</ol>';
        htmlContent += '<div class="carousel-inner  " role="listbox">';

        for (var k = 0; k < result.data.length; k++) {
            var isActive = '';
            if (k < 1) {
                isActive = 'active'
            }
            htmlContent += '<div class="item ' + isActive + '">';

            if (result.data[k].ANNOUNCE_TYPE == 'P') {
                htmlContent += '<img id="img-' + result.data[k].COMPANY + '" src="' + result.data[k].ANNOUNCE_URL + '" style="width:100%;height:470px"  />';
                htmlContent += '<div class="carousel-caption left-text"><h3>' + result.data[k].COMPANY + '</h3></div>';
            }

            else // (result.data[k].ANNOUNCE_TYPE == 'T')
            {
                htmlContent += '<img src="../Images/Cover/bg-wrigth.png" style="width:100%;height:470px"/><div class="carousel-caption left-text"><h3>' + result.data[k].COMPANY + '</h3>' + (result.data[k].ANNOUNCE_DESC ? result.data[k].ANNOUNCE_DESC : ''); +'</div>';
                htmlContent += '</div>';
            }
            htmlContent += '</div>';

        }
        htmlContent += '</div>';
        htmlContent += '</div>';

        if (result.data.length > 1) {
            htmlContent += '<a class="left carousel-control" href="#carousel-generic" role="button" data-slide="prev">';
            htmlContent += '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a>';
        }

        if (result.data.length > 1) {
            htmlContent += '<a class="right carousel-control" href="#carousel-generic" role="button" data-slide="next">';
            htmlContent += '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>';
        }
        //htmlContent += '</div></div>';
    }

    console.log('HTML Begin = [ ' + htmlContent + ' ] END !!!!!!!!!!!!')
    return htmlContent;
}

function BindAnnuncement(txt) {
    var content = "";
    ajax_method.Post(URL_CONST.ANNOUNCEMENT, null, true, function (result) {
        if (result.data != null) {
            var htmlContent = generateHtmlContent(result);
            Controls.WFD01110VIEW_MODE.html(htmlContent);
            //--Start Fix Show Current Text Edit Mode--by mrx//
            // CKEDITOR.instances.ANNOUNCE_DESC.setData(htmlContent);

            //--End Fix Show Current Text Edit Mode--//
            if (txt == "success")
                AletTextInfoMessage(URL_CONST.MESSAGESUCCESS);
        }

    }, null);
}

var initCKEDITOR = false;

var WFD01110EDIT_CLICK = function () {
    ClearMessageC();
    Controls.WFD01110EDIT_MODE.show();
    Controls.WFD01110EDIT_BTN.hide();
    Controls.WFD01110VIEW_MODE.hide();
    Controls.WFD01110UPLOAD_BTN.hide();
    //--Start Fix Show Current Text Edit Mode--by mrx//
    if (!initCKEDITOR) {
        initCKEDITOR = true;
        CKEDITOR.replace('ANNOUNCE_DESC');
    }

    CKEDITOR.instances.ANNOUNCE_DESC.setData('');

    var content = "";
    MAIN_COMPANY = null;
    MAIN_COMPANY = $('#home-carousel .active div h3').text();
    $('#span_company').text('(' + MAIN_COMPANY + ')');
    var con = {
        COMPANY: MAIN_COMPANY
    };
    ajax_method.Post(URL_CONST.ANNOUNCEMENT, con, true, function (result) {
        if (result.data != null) {

            for (var i = 0; i < result.data.length; i++) {
                if (result.data[i].ANNOUNCE_TYPE == 'P') {
                    content = null;
                }
                else if (result.data[i].ANNOUNCE_TYPE == 'T') {
                    content = result.data[i].ANNOUNCE_DESC;
                }
            }

            //--Start Fix Show Current Text Edit Mode--by mrx//
            CKEDITOR.instances.ANNOUNCE_DESC.setData(content);
            //--End Fix Show Current Text Edit Mode--//

        }

    }, null);
    //--End Fix Show Current Text Edit Mode--//
}


var WFD01110CANCEL_CLICK = function () {
    var message = URL_CONST.MESSAGECONFIRMCANCELT;

    loadConfirmAlert(message, function (result) {
        if (result) {
            //--Start Fix Show Current Text Edit Mode--by mrx//
            //CKEDITOR.instances.ANNOUNCE_DESC.setData(null);
            //--End Fix Show Current Text Edit Mode--//
            Controls.WFD01110EDIT_MODE.hide();
            Controls.WFD01110EDIT_BTN.show();
            Controls.WFD01110VIEW_MODE.show();
            Controls.WFD01110UPLOAD_BTN.show();

            clearHomeCarouselCompany();
        }
    });
}

var WFD01110SAVE_CLICK = function () {

    var message = URL_CONST.MESSAGECONFIRMSAVE;

    loadConfirmAlert(message, function (result) {
        if (result) {
            var con = {
                ANNOUNCE_DESC: CKEDITOR.instances.ANNOUNCE_DESC.getData(),
                COMPANY: MAIN_COMPANY
            }
            ajax_method.Post(URL_CONST.SAVE_TEXT, con, true, function (result) {
                Controls.WFD01110EDIT_MODE.hide();
                Controls.WFD01110EDIT_BTN.show();
                Controls.WFD01110VIEW_MODE.show();
                Controls.WFD01110UPLOAD_BTN.show();
                if (result.status) {
                    BindAnnuncement("success");
                }
                clearHomeCarouselCompany();
            }, null);
        }
    });
}
var WFD01110UPLOADSAVE_CLICK = function (event) {
    var message = URL_CONST.MESSAGECONFIRMSAVE;
    event.preventDefault();
    loadConfirmAlert(message, function (result) {
        if (result) {

            var fileUpload = $("#Image_Upload").get(0);
            var files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();

            // Looping over all files and add it to FormData object  
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }

            fileData.append("COMPANY", MAIN_COMPANY);

            if (files.length == 0) {
                AlertTextErrorMessagePopup('MCOM0025AERR : No image file selected', "#AlertMessageAreaPopup");
                return;
            }

            ajax_method.PostFile(URL_CONST.SAVE_PHOTO, fileData, function (result) {
                Controls.WFD01110UPLOAD_DIALOG.modal('hide');
                if (result.ObjectResult.status) {
                    BindAnnuncement("success");
                    $("#Image_Upload").val('');

                    //RELOAD IMAGE
                    $('#img-' + MAIN_COMPANY).attr("src", result.ObjectResult.imgUrl);
                }

                clearHomeCarouselCompany();

            }, null);
        }
    });
}
var WFD01110UPLOADCANCEL_CLICK = function () {
    var message = URL_CONST.MESSAGECONFIRMCANCELP;

    loadConfirmAlert(message, function (result) {
        if (result == true) {
            Controls.WFD01110UPLOAD_DIALOG.modal('hide');
            clearHomeCarouselCompany();
        }
    });
}
var WFD01110UPLOAD_CLICK = function () {

    MAIN_COMPANY = null;
    MAIN_COMPANY = $('#home-carousel .active div h3').text();
    $('#upload-title-desc').text('(' + MAIN_COMPANY + ')');
    ClearMessageC();
    ClearMessage("#AlertMessageAreaPopup");
    $("#Image_Upload").val('');
    Controls.WFD01110IMAGE_PREVIEW.attr('src', URL_CONST.DEFAULT_IMG);
    Controls.WFD01110UPLOAD_DIALOG.modal('show');
}
var requestData, requestTable, requestPageing, requestType, requestDiv;
function GetRequestTable(ptable, table_pageing, request_type, div_table) {
    ptable.DataTable().destroy();
    table_pageing.ClearPaginData();

    var con = {
        EMP_CODE: URL_CONST.EMP_CODE,
        REQUEST_TYPE: request_type
    };

    requestData = con;
    requestTable = ptable;
    requestPageing = table_pageing;
    requestType = request_type;
    requestDiv = div_table;

    RequestSearch();


}
function GetOnProcessTable(ptable, table_pageing, request_type, div_table) {
    ptable.DataTable().destroy();
    table_pageing.ClearPaginData();

    var con = {
        EMP_CODE: URL_CONST.EMP_CODE,
        REQUEST_TYPE: request_type,
        COMPANY: getMultiControlValue($('#MultipleBoxOnProcessCompany')),
    };

    requestData = con;
    requestTable = ptable;
    requestPageing = table_pageing;
    requestType = request_type;
    requestDiv = div_table;

    OnProcessSearch();


}
var Requestsorting = function (PaginData) {

    console.log(PaginData);
    RequestSearch();
}
var RequestchangePage = function (PaginData) {
    RequestSearch();
}
var RequestSearch = function () {
    var pagin = requestPageing.GetPaginData();
    ajax_method.SearchPost(URL_CONST.GET_REQUEST, requestData, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable(requestTable)) {
                var table = requestTable.DataTable();
                table.destroy();
            }

            requestTable.DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {

                            if (data.COST_CODE == null)
                                return null;

                            var _s = data.COST_CODE.replace("|", "<BR>");
                            return _s;
                        }
                    },
                    {
                        data: null, name: 'COST_NAME', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME == null)
                                return null;

                            var _s = data.COST_NAME;//.replace("|", "<BR>");
                            return SetTooltipInDataTableBySpan(_s, 30);
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    {
                        data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center'
                        //, 'render': function (data, type, full, meta) {
                        //    return dtConvFromJSON(data.REQUEST_DATE);
                        //}
                    },
                ],
                //
                'order': [],
                "paging": false,
                searching: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false
                //fixedHeader: true
            })
            //var page = requestTable.Pagin();
            //page.Init(pagin, Requestsorting, RequestchangePage, 'itemPerPage');
            requestPageing.Init(pagin, Requestsorting, RequestchangePage, 'itemPerPage');
            requestDiv.show();
        } else {
            requestDiv.hide();
            requestPageing.Clear();
        }
    }, null);
}

var ParamsSearchChange = function () {
    return {
        COMPANY: getMultiControlValue($('#MultipleBoxCompany')),
        TRNF_MAIN_TYPE: getMultiControlValue($('#TransferType'))

    }
}

var TransferSearch = function () {
    var con = ParamsSearchChange();
    var pagin = Controls.PaginTRANSFER.GetPaginData();

    Controls.WFD01110TABLE_TRANSFER.find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.TRANSFER, con, pagin, true, function (datas, pagin) {


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable(Controls.WFD01110TABLE_TRANSFER)) {
                var table = Controls.WFD01110TABLE_TRANSFER.DataTable();
                table.destroy();
            }

            Controls.WFD01110TABLE_TRANSFER.DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'TRNF_MAIN_TYPE', name: 'TRNF_MAIN_TYPE', 'className': 'text-center' },
                    {
                        data: null, name: 'TRANSFER_FROM', 'render': function (data, type, full, meta) {
                            if (data.TRANSFER_FROM_HINT == null)
                                return data.TRANSFER_FROM;
                            return '<span data-toggle="tooltip" title="' + data.TRANSFER_FROM_HINT + '">' + data.TRANSFER_FROM + '</span>';
                        }
                    },
                    {
                        data: null, name: 'TRANSFER_TO', 'render': function (data, type, full, meta) {
                            if (data.TRANSFER_TO_HINT == null)
                                return data.TRANSFER_TO;
                            return '<span data-toggle="tooltip" title="' + data.TRANSFER_TO_HINT + '">' + data.TRANSFER_TO + '</span>';
                        }
                    },

                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    {
                        data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center'
                    },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                // fixedHeader: true
            })
            //var page = Controls.WFD01110TABLE_TRANSFER.Pagin();
            //page.Init(pagin, Transfersorting, TransferchangePage, 'itemPerPage');

            Controls.PaginTRANSFER.Init(pagin, Transfersorting, TransferchangePage, 'itemPerPage');
            $('#div_transfer').show();
        } else {
            $('#div_transfer').hide();
            Controls.PaginTRANSFER.Clear();
        }
    }, null);
}
var Transfersorting = function (PaginData) {

    console.log(PaginData);
    TransferSearch();
}
var TransferchangePage = function (PaginData) {
    TransferSearch();
}

var StockSearch = function () {
    var pagin = Controls.PaginSTOCKTAKE.GetPaginData();
    ajax_method.SearchPost(URL_CONST.GETSTOCKTAKE, "", pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null || typeof pagin.OrderColIndex == 'undefined') pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable(Controls.WFD01110TABLE_STOCK_TAKE)) {
                var table = Controls.WFD01110TABLE_STOCK_TAKE.DataTable();
                table.destroy();
            }

            Controls.WFD01110TABLE_STOCK_TAKE.DataTable({
                data: datas,
                "columns": [
                    { "data": "ASSET_NO" },
                    { "data": "ASSET_NAME" },
                    { "data": "COST_CENTER_OWNER" },
                    { "data": "COST_CENTER_FOUND" },
                    { "data": "FOUND_BY" },
                ],
                'columnDefs': [
                    { 'targets': [0, 4], 'className': 'flex scol' },
                    { 'targets': [1, 2, 3], 'className': 'flex mcol' }
                ],

                "paging": false,
                searching: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                //fixedHeader: true
                //, "fnDrawCallback": drawCallBack,
            })
            //var page = Controls.WFD01110TABLE_STOCK_TAKE.Pagin();
            //page.Init(pagin, Stocksorting, StockchangePage, 'itemPerPage');
            Controls.PaginSTOCKTAKE.Init(pagin, Stocksorting, StockchangePage, 'itemPerPage');
            $('#div_stock_take').show();
            $('#coutStock').text(pagin.Totalitem);

        } else {
            $('#div_stock_take').hide();
            Controls.PaginSTOCKTAKE.Clear();
        }
    }, null);
}
var Stocksorting = function (PaginData) {

    console.log(PaginData);
    StockSearch();
}
var StockchangePage = function (PaginData) {
    StockSearch();
}

var OnProcessSearch = function () {
    var pagin = requestPageing.GetPaginData();
    ajax_method.SearchPost(URL_CONST.GET_ONPROCESS, requestData, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable(requestTable)) {
                var table = requestTable.DataTable();
                table.destroy();
            }

            requestTable.DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'REQUEST_TYPE_DESC', name: 'REQUEST_TYPE_DESC' },
                    { data: 'LAST_APPRV_DATE', name: 'LAST_APPRV_DATE', 'className': 'text-center' },
                    { data: 'DIFF_DATE', name: 'DIFF_DATE', 'className': 'text-right' },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],
                'order': [],
                "paging": false,
                searching: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                // fixedHeader: true
            })
            //var page = requestTable.Pagin();
            //page.Init(pagin, OnProcesssorting, OnProcesschangePage, 'itemPerPage');
            requestPageing.Init(pagin, OnProcesssorting, OnProcesschangePage, 'itemPerPage');
            requestDiv.show();
            //requestPageing.show();
        } else {
            requestDiv.hide();
            requestPageing.Clear();
        }
    }, null);
}
var OnProcesssorting = function (PaginData) {

    console.log(PaginData);
    OnProcessSearch();
}
var OnProcesschangePage = function (PaginData) {
    OnProcessSearch();
}

var SetTooltipInDataTableBySpan = function (_strText, _lengthSplit) {
    var res = _strText.split("|");
    var _strHtml = '';

    if (res.length === 1) {
        if (_strText != null && _strText.length > 0) {
            if (_strText.length > _lengthSplit) {
                //_strHtml = '<p title="" data-original-title="' + _strText + '" data-toggle="tooltip" data-placement="top"> ' + _strText.substring(0, _lengthSplit) + '..' + '</p>'
                _strHtml = '<p title="" data-original-title="' + replaceAll(_strText, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + _strText.substring(0, _lengthSplit) + '..' + '</p>'
            } else {
                _strHtml = _strText;
            }

        } else {
            _strHtml = _strText;
        }
        return _strHtml;
    }
    else {
        if (_strText != null && _strText.length > 0) {
            if (_strText.length > _lengthSplit) {
                for (var i = 0; i < res.length; i++) {
                    if (i > 0) { _strHtml += '</br>'; }
                    if (res[i].length < _lengthSplit) {
                        _strHtml += res[i];
                    }
                    else {
                        _strHtml += '<span title="" data-original-title="' + replaceAll(res[i], '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + res[i].substring(0, _lengthSplit) + '..' + '</span>';
                    }
                }
            } else {
                _strHtml = _strText;
            }

        } else {
            _strHtml = _strText;
        }
        return _strHtml;
    }
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

var getMultiControlValue = function (id) {
    var _val = '';
    if (id.val() != null) {
        _val = (id.val() + '');
        _val = _val.replace(/,/g, '#');
    }
    return _val
}

var AssetNewSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#AssetNew_MultipleBoxCompany'))
    };
    var pagin = requestPageing.GetPaginData();

    $('#tb_asset_new').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETASSETNEW, con, pagin, true, function (datas, pagin) {


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_asset_new'))) {
                var table = $('#tb_asset_new').DataTable();
                table.destroy();
            }

            $('#tb_asset_new').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;

                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })

            requestPageing.Init(pagin, AssetNewsorting, AssetNewchangePage, 'itemPerPage');

            // $('#tb_asset_new').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, AssetNewsorting, AssetNewchangePage, 'itemPerPage');
            $('#div_asset_new').show();
        } else {
            $('#div_asset_new').hide();
            requestPageing.Clear();
        }

    }, null);
}
var AssetNewsorting = function (PaginData) {

    AssetNewSearch();
}
var AssetNewchangePage = function (PaginData) {
    AssetNewSearch();
}


var SettlementSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#Settlement_MultipleBoxCompany'))
    };
    //var pagin = $('#tb_settlement').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();

    $('#tb_settlement').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETSETTLEMENT, con, pagin, true, function (datas, pagin) {


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_settlement'))) {
                var table = $('#tb_settlement').DataTable();
                table.destroy();
            }

            $('#tb_settlement').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    {
                        data: 'AMOUNT',
                        name: 'AMOUNT',
                        'className': 'text-right',
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },
                    { data: 'FINAL_CNT', name: 'FINAL_CNT', 'className': 'text-right' },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })
            requestPageing.Init(pagin, SettlementSearchsorting, SettlementSearchPage, 'itemPerPage');

            //$('#tb_settlement').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, SettlementSearchsorting, SettlementSearchPage, 'itemPerPage');
            $('#div_settlement').show();
        } else {
            $('#div_settlement').hide();
            requestPageing.Clear();
        }

    }, null);
}
var SettlementSearchsorting = function (PaginData) {

    SettlementSearch();
}
var SettlementSearchPage = function (PaginData) {
    SettlementSearch();
}

//Reclassification
var ReclassificationSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#Reclassification_MultipleBoxCompany'))
    };
    //var pagin = $('#tb_reclassification').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();
    $('#tb_reclassification').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETRECLASSIFICATION, con, pagin, true, function (datas, pagin) { // <<-----------------


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_reclassification'))) {
                var table = $('#tb_reclassification').DataTable();
                table.destroy();
            }

            $('#tb_reclassification').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    {
                        data: 'AMOUNT_POSTED', name: 'AMOUNT_POSTED', 'className': 'text-right',
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })

            requestPageing.Init(pagin, Reclassificationsorting, ReclassificationPage, 'itemPerPage');

            //$('#tb_reclassification').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, Reclassificationsorting, ReclassificationPage, 'itemPerPage');
            $('#div_reclassification').show();
        } else {
            $('#div_reclassification').hide();
            //$('#tb_reclassification').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
            requestPageing.Clear();
        }

    }, null);
}
var Reclassificationsorting = function (PaginData) {

    ReclassificationSearch();
}
var ReclassificationPage = function (PaginData) {
    ReclassificationSearch();
}

//Asset Change
var AssetChangeSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#AssetChange_MultipleBoxCompany'))
    };
    // var pagin = $('#tb_asset_change').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();
    $('#tb_asset_change').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETASSETCHANGE, con, pagin, true, function (datas, pagin) { // <<-----------------


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_asset_change'))) {
                var table = $('#tb_asset_change').DataTable();
                table.destroy();
            }

            $('#tb_asset_change').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })

            requestPageing.Init(pagin, AssetChangesorting, AssetChangePage, 'itemPerPage');

            //            $('#tb_asset_change').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, AssetChangesorting, AssetChangePage, 'itemPerPage');
            $('#div_asset_change').show();
        } else {
            $('#div_asset_change').hide();
            requestPageing.Clear();
            // $('#tb_asset_change').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
        }

    }, null);
}
var AssetChangesorting = function (PaginData) {

    AssetChangeSearch();
}
var AssetChangePage = function (PaginData) {
    AssetChangeSearch();
}

//Retirement
var RetirementSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#Retirement_MultipleBoxCompany')),
        DISP_TYPE: getMultiControlValue($('#RetirementType'))
    };
    // var pagin = $('#tb_retirement').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();
    $('#tb_retirement').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETRETIREMENT, con, pagin, true, function (datas, pagin) { // <<-----------------


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_retirement'))) {
                var table = $('#tb_retirement').DataTable();
                table.destroy();
            }

            $('#tb_retirement').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'DISP_TYPE', name: 'DISP_TYPE', 'className': 'text-center' },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },
                    {
                        data: 'TOTAL_NET_BOOK',
                        name: 'TOTAL_NET_BOOK',
                        'className': 'text-right',
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },
                    {
                        data: null, name: 'COST_CODE',
                        'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })

            requestPageing.Init(pagin, Retirementsorting, RetirementPage, 'itemPerPage');

            // $('#tb_retirement').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, Retirementsorting, RetirementPage, 'itemPerPage');
            $('#div_retirement').show();
        } else {
            $('#div_retirement').hide();
            requestPageing.Clear();
            // $('#tb_retirement').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
        }

    }, null);
}
var Retirementsorting = function (PaginData) {

    RetirementSearch();
}
var RetirementPage = function (PaginData) {
    RetirementSearch();
}


//Imparment
var ImparmentSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#Imparment_MultipleBoxCompany'))
    };
    // var pagin = $('#tb_imparment').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();
    $('#tb_imparment').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETIMPARMENT, con, pagin, true, function (datas, pagin) { // <<-----------------


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_imparment'))) {
                var table = $('#tb_imparment').DataTable();
                table.destroy();
            }

            $('#tb_imparment').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },

                    {
                        data: 'AMOUNT', name: 'AMOUNT', 'className': 'text-right',
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },
                    {
                        data: 'NEW_AMOUNT', name: 'NEW_AMOUNT', 'className': 'text-right',
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },

                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })

            requestPageing.Init(pagin, Imparmentsorting, ImparmentPage, 'itemPerPage');
            // $('#tb_imparment').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, Imparmentsorting, ImparmentPage, 'itemPerPage');
            $('#div_imparment').show();
        } else {
            $('#div_imparment').hide();
            requestPageing.Clear();
            // $('#tb_imparment').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
        }

    }, null);
}
var Imparmentsorting = function (PaginData) {

    ImparmentSearch();
}
var ImparmentPage = function (PaginData) {
    ImparmentSearch();
}

//Physical Count
var PhysicalCountSearch = function () {
    var con = {
        COMPANY: getMultiControlValue($('#PhysicalCount_MultipleBoxCompany'))
    };
    // var pagin = $('#tb_physical_count').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();
    $('#tb_physical_count').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETPHYSICALCOUNT, con, pagin, true, function (datas, pagin) { // <<-----------------


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 5; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_physical_count'))) {
                var table = $('#tb_physical_count').DataTable();
                table.destroy();
            }

            $('#tb_physical_count').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    { data: 'YEAR', name: 'YEAR', 'className': 'text-center' },
                    { data: 'ROUND', name: 'ROUND', 'className': 'text-center' },
                    { data: 'ASSET_LOCATION', name: 'ASSET_LOCATION', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            data.COST_CODE = data.COST_CODE.replace("#", "<BR>")
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;

                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })

            requestPageing.Init(pagin, OnProcesssorting, OnProcesschangePage, 'itemPerPage');
            // $('#tb_physical_count').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, PhysicalCountsorting, PhysicalCountPage, 'itemPerPage');
            $('#div_physical_count').show();
        } else {
            $('#div_physical_count').hide();
            requestPageing.Clear();
            // $('#tb_physical_count').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
        }

    }, null);
}
var PhysicalCountsorting = function (PaginData) {

    PhysicalCountSearch();
}
var PhysicalCountPage = function (PaginData) {
    PhysicalCountSearch();
}

//Reclassification
var SummaryAECList = function () {

    var pagin = $('#tb_request_summary').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();

    $('#tb_request_summary').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GETAECSUMMARY, null, pagin, true, function (datas, pagin) { // <<-----------------


        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable($('#tb_request_summary'))) {
                var table = $('#tb_request_summary').DataTable();
                table.destroy();
            }

            $('#tb_request_summary').DataTable({
                data: datas,
                "columns": [
                    { data: 'REQUEST_TYPE_NAME', name: 'REQUEST_TYPE_NAME', 'className': 'text-left', "orderable": false },
                    { data: 'STM_CNT', name: 'STM_CNT', 'className': 'text-right', "orderable": false },
                    { data: 'TDEM_CNT', name: 'TDEM_CNT', 'className': 'text-right', "orderable": false },
                    { data: 'TMT_CNT', name: 'TMT_CNT', 'className': 'text-right', "orderable": false },
                    { data: 'TOTAL', name: 'TOTAL', 'className': 'text-right', "orderable": false },

                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })
            $('#divSummaryAEC').show();
        } else {
            $('#divSummaryAEC').hide();
        }

    }, null);
}

var OnHandAECSearch = function () {

    var con = {
        COMPANY: getMultiControlValue($('#MultipleBoxOnAECCompany')),
        REQUEST_TYPE: getMultiControlValue($('#REQUEST_TYPE_AEC'))
    };

    var pagin = requestPageing.GetPaginData();
    //  var pagin = $('#tb_onhandAEC').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    $('#tb_onhandAEC').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GET_ONHANDAEC, con, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable($('#tb_onhandAEC'))) {
                var table = $('#tb_onhandAEC').DataTable();
                table.destroy();
            }

            $('#tb_onhandAEC').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'REQUEST_TYPE_DESC', name: 'REQUEST_TYPE_DESC' },
                    { data: 'LAST_APPRV_DATE', name: 'LAST_APPRV_DATE', 'className': 'text-center' },
                    { data: 'DIFF_DATE', name: 'DIFF_DATE', 'className': 'text-right' },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],
                'order': [],
                "paging": false,
                searching: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                // fixedHeader: true
            })
            //var page = requestTable.Pagin();

            requestPageing.Init(pagin, OnHandAECSorting, OnHandAECChangePage, 'itemPerPage');
            //    $('#tb_onhandAEC').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, OnHandAECSorting, OnHandAECChangePage, 'itemPerPage');
            $('#div_onhandAEC').show();
            //requestPageing.show();
        } else {
            $('#div_onhandAEC').hide();
            //  $('#tb_onhandAEC').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
            requestPageing.Clear();
        }
    }, null);
}
var OnHandAECSorting = function (PaginData) {

    console.log(PaginData);
    OnHandAECSearch();
}
var OnHandAECChangePage = function (PaginData) {
    OnHandAECSearch();
}

var ReprintSearch = function () {

    var con = {
        COMPANY: getMultiControlValue($('#MultipleBoxReprintCompany'))
    };
    // var pagin = $('#tb_reprint').Pagin(URL_CONST.ITEM_PER_PAGE).GetPaginData();
    var pagin = requestPageing.GetPaginData();

    $('#tb_reprint').find("tr:gt(0)").remove();

    ajax_method.SearchPost(URL_CONST.GET_REPRINT, con, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 2; //Default Order by DOC_NO
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable($('#tb_reprint'))) {
                var table = $('#tb_reprint').DataTable();
                table.destroy();
            }

            $('#tb_reprint').DataTable({
                data: datas,
                "columns": [
                    { data: 'ROWNUMBER', name: 'ROWNUMBER', 'className': 'text-center', "orderable": false },
                    { data: 'COMPANY', name: 'COMPANY', 'className': 'text-center' },
                    {
                        data: null, name: 'DOC_NO', 'render': function (data, type, full, meta) {
                            return '<a href="' + URL_CONST.REQUESTMASTER + '?RequestType=' + data.REQUEST_TYPE + '&DocNo=' + data.DOC_NO + '">' + data.DOC_NO + '</a>';
                        }
                    },
                    { data: 'ASSET_CNT', name: 'ASSET_CNT', 'className': 'text-right' },
                    {
                        data: null, name: 'COST_CODE', 'render': function (data, type, full, meta) {
                            if (data.COST_NAME_HINT == null)
                                return data.COST_CODE;
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME_HINT + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    { data: 'REQUESTOR', name: 'REQUESTOR' },
                    { data: 'CURRENT_STATUS', name: 'CURRENT_STATUS' },
                    { data: 'REQUEST_DATE', name: 'REQUEST_DATE', 'className': 'text-center' },
                ],

                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
            })
            //var page = requestTable.Pagin();

            requestPageing.Init(pagin, ReprintSorting, ReprintChangePage, 'itemPerPage');
            //  $('#tb_reprint').Pagin(URL_CONST.ITEM_PER_PAGE).Init(pagin, ReprintSorting, ReprintChangePage, 'itemPerPage');
            $('#div_reprint').show();
            //requestPageing.show();
        } else {
            $('#div_reprint').hide();
            requestPageing.Clear();
            // $('#tb_reprint').Pagin(URL_CONST.ITEM_PER_PAGE).Clear();
        }
    }, null);
}
var ReprintSorting = function (PaginData) {

    console.log(PaginData);
    ReprintSearch();
}
var ReprintChangePage = function (PaginData) {
    ReprintSearch();
}

//Home Carousel
var clearHomeCarouselCompany = function () {
    $('#span_company').text('');
};