﻿// All control in main screen
var TMPEmpCode;
var Controls = {
    MODEL: $('#MODEL'),
    TAG_NO: $('#TAG_NO'),



    UPDATE_DATE: $('#UPDATE_DATE'),


    //Button control
    WFD02130Save: $("#WFD02130Save"),
    WFD02130Close: $('#WFD02130Close'),
    WFD02130PrintTag: $('#WFD02130PrintTag'),
    //Load current image
    WFD02130ImageUpLoadPriview: $('#WFD02130ImageUpLoadPriview'),
    //Upload photo
    WFD02130UPLOAD_DIALOG: $('#upload-dialog'),
    WFD02130SELECT_IMAGE: $('#Image_Upload'),
    WFD02130IMAGE_PREVIEW: $('#WFD02130_Imgae_PreviewUpload'),
    WFD02130BTNIMAGEUPLOAD: $('#BtnImageUpload'),
    WFD02130UPLOADSAVE_BTN: $('#WFD02130UploadSave'),
    WFD02130UPLOADCANCEL_BTN: $('#WFD02130UploadCancel'),
    WFD02130UploadOpen: $('#WFD02130UploadOpen'),
    //Print tag
    PRINT_LOCATION_DIALOG: $('#WFD02130-change-print-location-dialog'),
    WFD02130txtPrintLocation: $('#WFD02130txtPrintLocation'),
    BOI_UPDATE_DATE: $('#BOI_UPDATE_DATE'),
    //BOI_ATTACH: $('#WFD02130FileUploadTB')
    WFD02130_UPLOAD_BOI_BTN: $('#WFD02130_UPLOAD_BOI_BTN')
};
//var WFD02130FileUplaodControls = {
//    Textbox: $('#WFD02130FileUploadTB'),
//    File: $('#WFD02130_UPLOAD_BOI_FILE'),
//    Button: $('#WFD02130_UPLOAD_BOI_BTN'),
//    DelFileBTN: $('#WFD02130_UPLOAD_BOI_DEL'),
//    DownloadFileBTN: $('#WFD02130_UPLOAD_BOI_DOWNLOAD'),
//    ViewFileBTN: $('#WFD02130_UPLOAD_BOI_VIEW')
//}
var WFD02130_Image_Cropper = {

    TagPhoto: null,
    Cropper: null

};

// Load screen event
$(function () {
    initialDefault(false, false);
    //button control
    Controls.WFD02130Save.click(WFD02130Save_click);
    Controls.WFD02130Close.click(WFD02130Close_click);
    Controls.WFD02130PrintTag.click(WFD02130PrintTag_click);
    Controls.WFD02130UPLOADSAVE_BTN.click(WFD02130UPLOADSAVE_CLICK)
    //WFD02130FileUplaodControls.Button.click(WFD02130UploadBOIFileClick);


});


var getSelectData = function () {
    var PrintLocation = '', BARCODE_SIZE = '', PLATE_TYPE = '';
    if ($('#WFD02130txtPrintLocation').val() != null) {
        PrintLocation = $('#WFD02130txtPrintLocation').val();
    }
    var myRadio = $('input[name=BARCODE]:checked');

    if (myRadio.length > 0) {
        if (myRadio[0].id != null) {
            switch (myRadio[0].id) {
                case 'BARCODE_SIZE_SMALL':
                    BARCODE_SIZE = 'S';
                    break;
                case 'BARCODE_SIZE_MEDIUM':
                    BARCODE_SIZE = 'M';
                    break;
                case 'BARCODE_SIZE_LARGE':
                    BARCODE_SIZE = 'L';
                    break;
            }
        }
    }

    var myRadios = $('input[name=PLATE_TYPE]:checked');
    if (myRadios.length > 0) {
        if (myRadios[0].id != null) {
            switch (myRadios[0].id) {
                case 'PLATE_TYPE_S':
                    PLATE_TYPE = 'S';
                    break;
                case 'PLATE_TYPE_P':
                    PLATE_TYPE = 'P';
                    break;
            }
        }
    }

    var datas = {
        COMPANY: URL_CONST.COMPANY,
        ASSET_NO: URL_CONST.ASSET_NO,
        ASSET_SUB: URL_CONST.ASSET_SUB,
        PRINT_CONDITION: $('#PRINT_CONDITION').text(),

        EMP_CODE: $('#EMP_CODE').val(),
        EMP_NAME: $('#EMP_NAME').text(),
        BARCODE_SIZE: BARCODE_SIZE,
        PLATE_TYPE: PLATE_TYPE,
        PRINT_LOCATION: PrintLocation,
        UPDATE_DATE: $('#UPDATE_DATE').text(),
        STOCKTAKE_FREQUENCY: $('#STOCKTAKE_FREQUENCY').val(),
        PROCESS_STATUS: $('#PROCESS_STATUS').val(),
        EMP_REMARK: $('#EMP_REMARK').val(),
        LOCATION_REMARK: $('#LOCATION_REMARK').val(),
        IMP_INVOICE_1: $('#IMP_INVOICE_1').val(),
        IMP_INVOICE_2: $('#IMP_INVOICE_2').val(),
        IMP_DATE: $('#IMP_DATE').val(),
        AORGOR_1: $('#AORGOR_1').val(),
        AORGOR_2: $('#AORGOR_2').val(),
        Free_Text: $('#Free_Text').val()
    }
    return datas;
}

// Callback function after click clear button
var WFD02130Save_click = function () {
    ClearMessageC();
    loadConfirmAlert(WFD02130_Message.ConfirmSaveLocation, function (result) {
        if (result) {
            var datas = getSelectData();
            ajax_method.Post(URL_CONST.UpdateAssetInfo, datas, true, function (result) {
                if (result != null) {
                    Controls.UPDATE_DATE.text(result.UPDATE_DATE);
                    WFD02130ShowToolTip(Controls.LOCATION_NAME);
                }
            }, null);

        }
    });
    // ----- SaveLocation_Err_NoInput
}



var WFD02130Close_click = function () {
    window.open('', '_self').close();
    //window.open('', '_self', ''); //bug fix
    //window.close();
    //Self.close();
    //ClearMessageC();
    //$('#LOCATION_NAME').focus();
}

// Load default condition data to screen input 
var initialDefault = function (f, showSaveSuccess, message) {
    //var param = { ASSET_NO: URL_CONST.ASSET_NO};
    var Param = {
        EMP_CODE: '',//Set on controller
        COMPANY: URL_CONST.COMPANY,
        ASSET_NO: URL_CONST.ASSET_NO,
        ASSET_SUB: URL_CONST.ASSET_SUB,
        PRINT_LOCATION: '',
        BARCODE_SIZE: '',
        TAG_PHOTO: '',
        LOCATION_NAME: '',
        UPDATE_DATE: ''
    }


    ajax_method.Post(URL_CONST.GetAssetList, Param, true, function (result) {
        if (result == null) {
            return;
        }
        //divTabAssetInformation
        $('div.assetdata input, div.assetdata  select,div.assetdata  label,div.assetdata span').each(
            function () {

                //console.log($(this).attr('id'));

                if ($(this).attr('id') == null)
                    return;
                _Mapping($(this), result);
            }
        );


        $('#INVEN_INDICATOR').prop("checked", result.INVEN_INDICATOR == 'Y');
        $('#COST_PENDING').prop("checked", result.COST_PENDING != null);
        $('#PRINT_COUNT').text(result.PRINT_COUNT);
        $('#PRINT_CONDITION').text(result.PRINT_CONDITION);


        $('#Free_Text').val(result.Free_Text);

        Controls.UPDATE_DATE.text(result.UPDATE_DATE);//BOI_UPDATE_DATE
        Controls.BOI_UPDATE_DATE.val(result.BOI_UPDATE_DATE);
        //Controls.BOI_ATTACH.val(result.BOI_ATTACH);

        if (result.TAG_PHOTO != '' && result.TAG_PHOTO != null) {
            Controls.WFD02130ImageUpLoadPriview.attr("src", result.TAG_PHOTO);
            Controls.WFD02130IMAGE_PREVIEW.attr("src", result.TAG_PHOTO);
        }

        WFD02130_Image_Cropper.TagPhoto = result.TAG_PHOTO;

        switch (result.BARCODE_SIZE) {
            case 'S':
                $("#BARCODE_SIZE_SMALL").prop("checked", true);
                break;
            case 'M':
                $("#BARCODE_SIZE_MEDIUM").prop("checked", true);
                break;
            case 'L':
                $("#BARCODE_SIZE_LARGE").prop("checked", true);
                break;
        };

        switch (result.PLATE_TYPE) {
            case 'S':
                $("#PLATE_TYPE_S").prop("checked", true);
                break;
            case 'P':
                $("#PLATE_TYPE_P").prop("checked", true);
                break;
        };

        WFD02130ShowToolTip($('#WBS_BUDGET'));
        WFD02130ShowToolTip($('#WBS_PROJECT'));

        if (PARAMETER.MODE == 'VIEW') {
            ViewMode();
            if (result.STATUS != 'Y') {

                //if (WFD02130FileUplaodControls.Button != null) {
                //    WFD02130FileUplaodControls.Button.prop("disabled", true);
                //}
                //if (WFD02130FileUplaodControls.DelFileBTN != null) {
                //    WFD02130FileUplaodControls.DelFileBTN.prop("disabled", true);
                //}
            }
        } else if (PARAMETER.MODE == 'EDIT') {
            EditMode(result);
            if (result.STATUS != 'Y') {
                Controls.WFD02130Save.prop("disabled", false);
                Controls.WFD02130Close.prop("disabled", false);
                Controls.WFD02130UploadOpen.prop("disabled", false);
                //Controls.WFD02130PrintTag.prop("disabled", false);
                if (GLOBAL.IS_AEC == "Y") {
                    Controls.WFD02130PrintTag.prop('disabled', false);
                } else {
                    if (result.PRINT_STATUS == "Y") {
                        Controls.WFD02130PrintTag.prop('disabled', true);
                        $('input[name=PLATE_TYPE]').prop('disabled', true);
                        $('input[name=BARCODE]').prop('disabled', true);
                    } else {
                        Controls.WFD02130PrintTag.prop('disabled', false);
                        $('input[name=PLATE_TYPE]').prop('disabled', false);
                        $('input[name=BARCODE]').prop('disabled', false);
                    }

                }
                Controls.WFD02130_UPLOAD_BOI_BTN.prop("disabled", false);
                //if (WFD02130FileUplaodControls.Button != null) {
                //    WFD02130FileUplaodControls.Button.prop("disabled", false);
                //}
                //if (WFD02130FileUplaodControls.DelFileBTN != null) {
                //    WFD02130FileUplaodControls.DelFileBTN.prop("disabled", false);
                //}
            }
        }
        //WFD02130_SetBOIDocument();

        if (f) {
            if (showSaveSuccess) {
                AletTextInfoMessage(message);//SaveSuccess
            } else {
                AlertTextErrorMessage(message);
            }
        }

    }, null);

}

function _Mapping(_sender, result) {
    var _fieldName = $(_sender).attr('id');
    if ($(_sender).is("select")) {
        $(_sender).select2('val', result[_fieldName]);
    }
    else if ($(_sender).is("label") || $(_sender).is("span")) {
        if (result[_fieldName] == null)
            return;

        $(_sender).text(result[_fieldName]);
    }

    else { //|| $(_sender).is("textarea")
        $(_sender).val(result[_fieldName]);

    }
}
// All controls in finishplan modal screen
$(function () {

    $('div.assetdata input, div.assetdata  select,div.assetdata  label,div.assetdata span').prop("disabled", true);

    $('.modal ').appendTo($('body'));
    Controls.WFD02130UploadOpen.on('click', WFD02130_OpenUploadImageModal);
    Controls.WFD02130BTNIMAGEUPLOAD.on('click', function () {
        Controls.WFD02130SELECT_IMAGE.click();
    });
    Controls.WFD02130SELECT_IMAGE.change(function () {

        var file = Controls.WFD02130SELECT_IMAGE[0].files[0];
        if (file == null) {
            return;
        }
        var ext = file.name.split('.').pop();
        var arr = PARAMETER.FILEEXTENSION.split(",");
        console.log(ext);
        console.log(arr);
        if (CheckExtensionPopup(ext, arr, file.name, PARAMETER.FILEEXTENSION, true)) {
            readURL(this);
        } else {
            this.value = '';
        }
    });
    Controls.WFD02130UPLOADCANCEL_BTN.on('click', function () {
        Controls.WFD02130UPLOAD_DIALOG.modal('hide');
    });

});

var WFD02130UPLOAD_CLICK = function () {
    Controls.WFD02130IMAGE_PREVIEW.attr('src', URL_CONST.DEFAULT_IMG);
    Controls.WFD02130UPLOAD_DIALOG.modal('show');
}

// Print Tag Q
var WFD02130PrintTag_click = function () {
    var plateType = $("input[name='PLATE_TYPE']:checked").val();
    //Validation
    if (!plateType) {
        AlertTextErrorMessage(WFD02130_Message.PlateType_Err_NoInput);
        return;
    }
    if (plateType === "S") {
        var plateSize = $("input[name='BARCODE']:checked").val();
        if (!plateSize) {
            AlertTextErrorMessage(WFD02130_Message.PlateSize_Err_NoInput);
            return;
        }
    }

    if (plateType === "S") {
        ChangePrintLocation();
        $('#WFD02130txtPrintLocation').Select2Require(true);
    } else {
        WFD02130SavePrintLocation();
    }
}

function ChangePrintLocation() {
    Controls.PRINT_LOCATION_DIALOG.appendTo("body").modal('show');

    var Param = {
        EMP_CODE: ''//Set on controller
    }
    ajax_method.Post(URL_CONST.GetPrintLocation, Param, true, function (result) {
        if (result.PrintLocation != "") {
            Controls.WFD02130txtPrintLocation.select2('val', result.PRINT_LOCATION);
        }
    })
}

function OnWFD02130txtPrintLocationBtnClick() {
    WFD02130txtPrintLocation: $('#WFD02130txtPrintLocation').val();
    if ($('#WFD02130txtPrintLocation').val() == "") {
        $('#WFD02130-change-print-location-dialog').modal('hide');
        AlertTextErrorMessage(WFD02130_Message.PrintTag_Err_NoInput);
    } else {
        $('#WFD02130-change-print-location-dialog').modal('hide');
        WFD02130SavePrintLocation();
    }
}

var WFD02130SavePrintLocation = function () {
    var datas = getSelectData();
    var showSaveSuccess = false;
    setTimeout(ajax_method.Post(URL_CONST.AddPrintTagAndUpdateBarCodeSize, datas, true, function (result) {
        //if (result == URL_CONST.CODE_CONTACT_ADMIN) {// Suphachai L.   CODESUCCESS
        //    showSaveSuccess = true;
        //}
        initialDefault(true, result.success, result.message);
    }, null), 200);
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //Controls.WFD02130IMAGE_PREVIEW.attr('src', e.target.result);
            WFD02130_BindingImageUploadPreview(e.target.result, 0);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

var WFD02130_BindingImageUploadPreview = function (result, waitTime) {
    WFD02130_Image_Cropper.TagPhoto = result;
    Controls.WFD02130IMAGE_PREVIEW.attr('src', result);
    WFD02130_InitCropper(waitTime);
}

var WFD02130_InitCropper = function (waitTime) {
    if (!isIe8()) {
        if (WFD02130_Image_Cropper.Cropper != null) {
            WFD02130_Image_Cropper.Cropper.cropper('destroy');
            WFD02130_Image_Cropper.Cropper = null;
        }

        if (WFD02130_Image_Cropper.TagPhoto != null && WFD02130_Image_Cropper.TagPhoto != '') {

            Controls.WFD02130IMAGE_PREVIEW.hide();


            setTimeout(function () {
                Controls.WFD02130IMAGE_PREVIEW.show();
                WFD02130_Image_Cropper.Cropper = Controls.WFD02130IMAGE_PREVIEW.cropper({
                    aspectRatio: integer(PARAMETER.IMAGE_WIDTH) / integer(PARAMETER.IMAGE_HEIGHT),
                    viewMode: 1,
                    dragMode: 'move',
                    autoCropArea: 1.0,
                    restore: false,
                    guides: false,
                    crossOrigin: true,
                    highlight: false,
                    cropBoxMovable: false,
                    cropBoxResizable: false
                });
            }, waitTime);
        }
    }

}

var WFD02130_OpenUploadImageModal = function () {
    $('#AlertMessageAreaPopup').html('');
    Controls.WFD02130UPLOAD_DIALOG.modal('show');
    if (WFD02130_Image_Cropper.TagPhoto == null) {
        WFD02130_BindingImageUploadPreview(PARAMETER.DEFAULT_IMAGE, 200);
    } else {
        WFD02130_BindingImageUploadPreview(WFD02130_Image_Cropper.TagPhoto, 200);
    }

}


var WFD02130UPLOADSAVE_CLICK = function () {

    if (isIe8()) {
        uploadNormal();
    } else {
        uploadCrop();
    }

}
var uploadNormal = function () {

    var fileUpload = Controls.WFD02130SELECT_IMAGE.get(0);
    var files = fileUpload.files;

    var con = new FormData();
    var datas = getSelectData();
    con.append("COMPANY", $('#COMPANY').val());
    con.append("ASSET_NO", $('#ASSET_NO').val());
    con.append("ASSET_SUB", $('#ASSET_SUB').val());
    con.append("UPDATE_DATE", Controls.UPDATE_DATE.text());
    con.append("base64Image", crop_canvas.toDataURL("image/png"));

    // Looping over all files and add it to FormData object  
    for (var i = 0; i < files.length; i++) {
        con.append("File", files[i]);
    }

    if (files.length == 0) {
        AlertTextErrorMessagePopup('MCOM0025AERR : No image file selected', "#AlertMessageAreaPopup");
        return;
    }
    ajax_method.PostFile(URL_CONST.UpdateTAG_Photo, con, function (result) {
        Controls.WFD02130UPLOAD_DIALOG.modal('hide');
        //if (result.ObjectResult.status) {
        //    BindAnnuncement("success");
        //}
    }, null);
}
var uploadCrop = function () {

    var fileUpload = Controls.WFD02130SELECT_IMAGE.get(0);
    var files = fileUpload.files;
    var contentType;

    if (files.length == 0) {
        AlertTextErrorMessagePopup('MCOM0025AERR : No image file selected', "#AlertMessageAreaPopup");
        return;
    }

    if (files.length > 0) {
        contentType = files[0].type;
    } else {
        contentType = "image/png";
    }

    console.log("OK");
    WFD02130_Image_Cropper.Cropper.cropper('getCroppedCanvas').toBlob(function (blob) {

        var con = new FormData();
        var datas = getSelectData();
        con.append("COMPANY", $('#COMPANY').val());
        con.append("ASSET_NO", $('#ASSET_NO').val());
        con.append("ASSET_SUB", $('#ASSET_SUB').val());
        con.append("UPDATE_DATE", Controls.UPDATE_DATE.text());
        con.append('base64Image', blob);
        con.append('extension', contentType.split('/').pop())

        ajax_method.PostFile(URL_CONST.UpdateTAG_Photo, con, function (result) {
            Controls.WFD02130UPLOAD_DIALOG.modal('hide');
            //if (result.ObjectResult.status) {
            //    BindAnnuncement("success");
            //}
            var Param = {
                EMP_CODE: '',//Set on controller
                COMPANY: URL_CONST.COMPANY,
                ASSET_NO: URL_CONST.ASSET_NO,
                ASSET_SUB: URL_CONST.ASSET_SUB,
                PRINT_LOCATION: '',
                BARCODE_SIZE: '',
                TAG_PHOTO: '',
                LOCATION_NAME: '',
                UPDATE_DATE: ''
            }

            ajax_method.Post(URL_CONST.GetAssetList, Param, true, function (result) {
                if (result != null) {
                    if (result.TAG_PHOTO != '' && result.TAG_PHOTO != null) {
                        Controls.WFD02130ImageUpLoadPriview.attr("src", result.TAG_PHOTO);
                        Controls.WFD02130IMAGE_PREVIEW.attr("src", result.TAG_PHOTO);
                        Controls.UPDATE_DATE.text(result.UPDATE_DATE);
                    }
                    WFD02130_Image_Cropper.TagPhoto = result.TAG_PHOTO;
                    AletTextInfoMessage(WFD02130_Message.SaveSuccess);
                }
            }, null);
        }, null);
    }, contentType);
}

function WFD02130ShowToolTip(ctrl) {
    ctrl.attr({
        "data-toggle": "tooltip",
        "data-placement": "top",
        "title": "",
        "data-original-title": ctrl.val()   // replaceAll(ctrl.val(), '"', '&quot;')
    })
}

function formatNumber(n) {
    return n.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

var ViewMode = function () {

    $('div.assetdata input, div.assetdata  select,div.assetdata  label,div.assetdata span').prop("disabled", true);

    Controls.WFD02130Save.prop("disabled", true);
    Controls.WFD02130UploadOpen.prop("disabled", true);
    Controls.WFD02130PrintTag.prop("disabled", true);
    Controls.WFD02130_UPLOAD_BOI_BTN.prop("disabled", true);

    initFixedAssetTag();
}

var EditMode = function (data) {
    $('#divAdditionInformation input, #divAdditionInformation select, #divAdditionInformation textarea').prop("disabled", false);

    $('#STOCKTAKE_FREQUENCY, #EMP_CODE, #LOCATION_REMARK').prop("disabled", false);

    if ($('#PROCESS_STATUS').val() == "Y")
        $('#PROCESS_STATUS').prop("disabled", false);

    Controls.WFD02130Save.prop("disabled", false);
    Controls.WFD02130UploadOpen.prop("disabled", false);
    //Controls.WFD02130PrintTag.prop("disabled", false);
    if (GLOBAL.IS_AEC == "Y") {
        Controls.WFD02130PrintTag.prop('disabled', false);
    } else {
        if (data.PRINT_STATUS == "Y") {
            Controls.WFD02130PrintTag.prop('disabled', true);
            $('input[name=PLATE_TYPE]').prop('disabled', true);
            $('input[name=BARCODE]').prop('disabled', true);
        } else {
            Controls.WFD02130PrintTag.prop('disabled', false);
            $('input[name=PLATE_TYPE]').prop('disabled', false);
            $('input[name=BARCODE]').prop('disabled', false);
        }
    }

    Controls.WFD02130_UPLOAD_BOI_BTN.prop("disabled", false);

    initFixedAssetTag();
}

$('input[name="PLATE_TYPE"]').on('change', function () {
    initFixedAssetTag();
});

function initFixedAssetTag() {
    var plateType = $("input[name='PLATE_TYPE']:checked").val();
    var $barCodeSize = $('input[name="BARCODE"]');

    if (plateType === "P") {
        $barCodeSize.prop('disabled', true);
    } else {
        $barCodeSize.prop('disabled', false);
    }
}

//===================== Upload BOI File =========================//
//var WFD02130_SetButtonAndUploadFile = function () {
//    var model = new FormData();
//    var fileInput = document.getElementById('WFD02130_UPLOAD_BOI_FILE');
//    if (fileInput.files.length > 0) {
//        model.append('BOIFile', fileInput.files[0]);
//        model.append('AttachFileName', fileInput.files[0].name);
//        model.append('ASSET_NO', Controls.ASSET_NO.val());
//        model.append('UPDATE_DATE', $('#UPDATE_DATE').text());
//        var ModelValidateFile = {
//            FunctionID: 'WFD02410',
//            Folder: 'BOI',
//            FileName: fileInput.files[0].name,
//            FileSize: fileInput.files[0].size
//        }
//        ajax_method.Post(URL_CONST.CHECK_UPLOAD_FILE, ModelValidateFile, false, function (result) {
//            if (result) {// true 
//                ajax_method.PostFile(URL_CONST.UPLOAD_FILE_BOI, model, function (result) {
//                    if (result.ObjectResult.FileName != null && result.ObjectResult.FileName != '') {
//                        WFD02130FileUplaodControls.Button.hide();
//                        WFD02130FileUplaodControls.DelFileBTN.show();
//                        WFD02130FileUplaodControls.DownloadFileBTN.hide();
//                        WFD02130FileUplaodControls.ViewFileBTN.show();
//                        WFD02130FileUplaodControls.Textbox.val(result.ObjectResult.FileName);
//                        document.getElementById('WFD02130_UPLOAD_BOI_VIEW').href = "/WFD02130/DownloadFileTemp?FILENAME=" + result.ObjectResult.FileName + "&FunctionID=WFD02410" + "&Type=BOI";
//                        var Param = {
//                            COMPANY: URL_CONST.COMPANY,
//                            ASSET_NO: URL_CONST.ASSET_NO,
//                            ASSET_SUB: URL_CONST.ASSET_SUB,
//                            COST_CODE: URL_CONST.COST_CODE,
//                        }
//                        ajax_method.Post(URL_CONST.GetAssetList, Param, true, function (result) {
//                            if (result != null) {
//                                Controls.UPDATE_DATE.text(result.UPDATE_DATE);//BOI_UPDATE_DATE
//                                Controls.BOI_UPDATE_DATE.val(result.BOI_UPDATE_DATE);
//                                AletTextInfoMessage(URL_CONST.SaveSuccess);
//                            }
//                        }, null);
//                    } else {
//                        WFD02130FileUplaodControls.File.val('');
//                    }

//                }, null);
//            }
//        }, null);
//    }
//}
//var WFD02130LoadBOIData = function () {
//    var ModelComment = {
//        ASSET_NO: URL_CONST.ASSET_NO,
//        UPDATE_DATE: $('#UPDATE_DATE').text(),
//        AttachFileName: WFD02130FileUplaodControls.Textbox.val(),
//    }
//    return ModelComment;
//}

//var WFD02130UploadBOIFileClick = function () {
//    ClearMessageC();
//    WFD02130FileUplaodControls.File.click();
//}

//var WFD02130CheckDownloadFile = function () {
//    var _valueFileName = WFD02130FileUplaodControls.Textbox.val();
//    if (_valueFileName != null && _valueFileName != '') {
//        WFD02130FileUplaodControls.DownloadFileBTN.href = "/WFD02130/DownloadFileTemp?FILENAME=" + _valueFileName + "&FunctionID=WFD02410" + "&_type=BOI";
//    }
//}

//var WFD02130DelUploadCommentFileClick = function () {
//    var _strMessageDel = WFD02130_Message.ConfirmDelete;
//    _strMessageDel = _strMessageDel.replace("{1}", " BOI Doc Asset No: " + URL_CONST.ASSET_NO);
//    var model = WFD02130LoadBOIData();
//    loadConfirmAlert(_strMessageDel, function (result) {
//        if (result) {
//            ajax_method.Post(URL_CONST.DELETE_FILE_BOI, model, false, function (result) {
//                if (result.FileName == '' || result.FileName == null) {
//                    WFD02130FileUplaodControls.Button.show();
//                    WFD02130FileUplaodControls.DelFileBTN.hide();
//                    WFD02130FileUplaodControls.DownloadFileBTN.hide();
//                    WFD02130FileUplaodControls.ViewFileBTN.hide();
//                    WFD02130FileUplaodControls.Textbox.val(result.FileName);
//                    WFD02130FileUplaodControls.File.val(result.FileName);
//                    var Param = {
//                        COMPANY: URL_CONST.COMPANY,
//                        ASSET_NO: URL_CONST.ASSET_NO,
//                        ASSET_SUB: URL_CONST.ASSET_SUB,
//                        COST_CODE: URL_CONST.COST_CODE,
//                    }
//                    ajax_method.Post(URL_CONST.GetAssetList, Param, true, function (result) {
//                        if (result != null) {
//                            Controls.UPDATE_DATE.text(result.UPDATE_DATE);//BOI_UPDATE_DATE
//                            Controls.BOI_UPDATE_DATE.val(result.BOI_UPDATE_DATE);
//                            AletTextInfoMessage(URL_CONST.MESSAGE_DEL_SUCCESS);
//                        }
//                    }, null);
//                }
//            });
//        }
//    });
//}

//var WFD02130_SetBOIDocument = function () {
//    var BtnFile = 'WFD02130_UPLOAD_BOI_BTN';
//    var File = 'WFD02130_UPLOAD_BOI_FILE';
//    var ViewFile = 'WFD02130_UPLOAD_BOI_VIEW';
//    var DelFile = 'WFD02130_UPLOAD_BOI_DEL';
//    var DownloadFile = 'WFD02130_UPLOAD_BOI_DOWNLOAD';
//    var FileName = Controls.BOI_ATTACH.val();
//    var FunctionID = 'WFD02410';
//    if (Controls.BOI_ATTACH.val() != '' && Controls.BOI_ATTACH.val() != null) {
//        if (PARAMETER.PerUploadBOIDoc == 'Y') {
//            document.getElementById(DelFile).style.display = 'inline-block';
//        } else {
//            document.getElementById(DelFile).style.display = 'none';
//        }
//        if (FileName.substring(0, 1) == 'D') {
//            FunctionID = 'WFD02510';
//        }
//        document.getElementById(BtnFile).style.display = 'none';
//        document.getElementById(ViewFile).style.display = 'inline-block';
//        document.getElementById(DownloadFile).style.display = 'none';
//        document.getElementById(ViewFile).href = "/WFD02130/DownloadFileTemp?FILENAME=" + Controls.BOI_ATTACH.val() + "&FunctionID=" + FunctionID + "&Type=BOI";
//        document.getElementById(DownloadFile).href = "/WFD02130/DownloadFileTemp?FILENAME=" + Controls.BOI_ATTACH.val() + "&FunctionID=" + FunctionID + "&Type=BOI";
//    } else {
//        document.getElementById(DelFile).style.display = 'none';
//        document.getElementById(ViewFile).style.display = 'none';
//        document.getElementById(DownloadFile).style.display = 'none';
//        if (PARAMETER.PerUploadBOIDoc == 'Y') {
//            document.getElementById(BtnFile).style.display = 'inline-block';
//        } else {
//            document.getElementById(BtnFile).style.display = 'none';
//        }
//    }
//}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

$('#EMP_CODE').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
    source: function (query, process) {
        EMPLOYEE = [];
        //map = {};
        $.post(URL_CONST.GetAutoCompleteEmployee, { _keyword: query, company: $('#COMPANY').val() }, function (data) {
            TMPEmpCode = data.ObjectResult;
            for (i = 0; i < data.ObjectResult.length; i++) {
                var displayValue = data.ObjectResult[i].EMP_CODE + ' - ' + data.ObjectResult[i].EMP_NAME;
                TMPEmpCode[i].displayValue = displayValue;
                EMPLOYEE.push(displayValue);
            }

            process(EMPLOYEE);
        });
    },
    updater: function (value) {
        for (i = 0; i < TMPEmpCode.length; i++) {
            if (value === TMPEmpCode[i].displayValue) {
                $('#EMP_NAME').val(TMPEmpCode[i].EMP_NAME);
                return TMPEmpCode[i].EMP_CODE;
            }
        }
        return value;
    }
});

//$('#EMP_CODE').change(function () {
//    var anchor = document.getElementById('EMP_CODE');
//    for (i = 0; i < TMPEmpCode.length; i++) {
//        if ($('#EMP_CODE').val() == TMPEmpCode[i].EMP_CODE) {
//            anchor.title = TMPEmpCode[i].EMP_NAME;
//        }
//    }
//});

function saveFileCallBack() {
    ajax_method.Post(URL_CONST.UPLOAD_FILE_BOI, {
        GUID: GLOBAL.GUID,
        DOC_NO: $('#COMPANY').val(),
        ASSET_NO: $('#ASSET_NO').val(),
        ASSET_SUB: $('#ASSET_SUB').val(),
        COMPANY: $('#COMPANY').val(),
        DOC_UPLOAD: "DEFAULT_BOI",
        //TYPE: "BOI",
        //LINE_NO: 0
    }, false, function (result) {
        if (result.success) {
            //set date updateDate
            Controls.BOI_UPDATE_DATE.val(result.updateDate);
        }
    }, null);
}

$('#WFD02130_UPLOAD_BOI_BTN').click(function (e) {
    controlUpload = {
        //GUID: $('#COMPANY').val() + $('#ASSET_NO').val() + $('#ASSET_SUB').val(),
        GUID: GLOBAL.GUID,
        DOC_NO: $('#COMPANY').val(),
        ASSET_NO: $('#ASSET_NO').val(),
        ASSET_SUB: $('#ASSET_SUB').val(),
        COMPANY: $('#COMPANY').val(),
        TYPE: "BOI",
        FUNCTION_ID: "DEFAULT",
        DOC_UPLOAD: "DEFAULT_BOI",
        LINE_NO: 0,
        FILE_LIMIT: 5,
        DEL_FLAG: 'Y',
        SAVE_CALLBACK: saveFileCallBack
    };

    //    ajax_method.PostFile(URL_CONST.UPLOAD_FILE_BOI, model, function (result) {
    //                    if (result.ObjectResult.FileName != null && result.ObjectResult.FileName != '') {
    //                        WFD02130FileUplaodControls.Button.hide();
    //                        WFD02130FileUplaodControls.DelFileBTN.show();
    //                        WFD02130FileUplaodControls.DownloadFileBTN.hide();
    //                        WFD02130FileUplaodControls.ViewFileBTN.show();
    //                        WFD02130FileUplaodControls.Textbox.val(result.ObjectResult.FileName);
    //                        document.getElementById('WFD02130_UPLOAD_BOI_VIEW').href = "/WFD02130/DownloadFileTemp?FILENAME=" + result.ObjectResult.FileName + "&FunctionID=WFD02410" + "&Type=BOI";
    //                        var Param = {
    //                            COMPANY: URL_CONST.COMPANY,
    //                            ASSET_NO: URL_CONST.ASSET_NO,
    //                            ASSET_SUB: URL_CONST.ASSET_SUB,
    //                            COST_CODE: URL_CONST.COST_CODE,
    //                        }
    //                        ajax_method.Post(URL_CONST.GetAssetList, Param, true, function (result) {
    //                            if (result != null) {
    //                                Controls.UPDATE_DATE.text(result.UPDATE_DATE);//BOI_UPDATE_DATE
    //                                Controls.BOI_UPDATE_DATE.val(result.BOI_UPDATE_DATE);
    //                                AletTextInfoMessage(URL_CONST.SaveSuccess);
    //                            }
    //                        }, null);
    //                    } else {
    //                        WFD02130FileUplaodControls.File.val('');
    //                    }

    //                }, null);

    $('#lblUploadFileText').html("Upload BOI Document Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB);
    //$('#lblUploadFileText').html("Upload BOI file");
    $('#CommonUpload').appendTo("body").modal('show');
});