﻿
var days_WFD02620_Assignment = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

var WorkingTime = JJWorkingTime();

var WFD02620_AssignmentControls = {
    SummaryBlock: $('#summaryBlock'),
    lockTBCol: $('#lockTBCol'),
    SVTable: $('#WFD02620_SVTable'),
    SVTableBody: $('#WFD02620_SVTable tbody'),
    CLD_Time_Col: $('#assignmentCalendarTableTimeCol'),
    CLD_Time_Header_Row: $('#assignmentCalendarTableTimeHeadRow'),
    CLD_TBody: $('#assignmentCalendarTableTBody'),
    SetWorkingTimeDialog: $('#SetWorkingTimeDialog'),
    SaveAssignmentButton: $('#WFD02620SaveAssignmentButton'),
    CancelAssignmentButton: $('#WFD02620CancelAssignmentButton')
};

var WFD02620_Assignment_Models = {
    HData: null,
    SVDatas: [],
    Holidays: [],
    MasterSettings: null,
    Summaries: [],
    ISFAAdmin: false
};

var setAssignmentData_WFD02620_Assignment = function (hData, svdatas, holidays, masterSetting, summaryMinorCate, isFaAdmin) {
    //console.log("BEFORE")
    //console.log(WFD02620_Assignment_Models.SVDatas);
    WFD02620_Assignment_Models.ISFAAdmin = isFaAdmin;
    WFD02620_Assignment_Models.HData = ConvertHData_WFD02620_Assignment(hData);
    WFD02620_Assignment_Models.SVDatas = ConvertSVDatas_WFD02620_Assignment(svdatas);
    WFD02620_Assignment_Models.Holidays = ConvertHolidayData_WFD02620_Assignment(holidays);
    WFD02620_Assignment_Models.Summaries = summaryMinorCate;
    WFD02620_Assignment_Models.MasterSettings = masterSetting;
    //console.log("AFTER")

    setAssignmentScreenUI();
};

var setAssignmentScreenUI = function () {

    //console.log(WFD02620_Assignment_Models.SVDatas);
    //console.log("start cal")
    setCommonData_WFD02620();

    setsummaryMinorCateUI_WFD02620_Assignment();

    setSVUI_WFD02620_Assignment();
    setCalendarUI_WFD02620_Assignment();

};
var setCommonData_WFD02620 = function () {
    var breakTimes = [];
    var starts = [];
    var ends = [];

    if (WFD02620_Assignment_Models.MasterSettings != null && WFD02620_Assignment_Models.MasterSettings.BREAK_TIME != null) {
        breakTimes = WFD02620_Assignment_Models.MasterSettings.BREAK_TIME.split('-'); // result example : ['11:31','12:30']
    } else {
        breakTimes = null;
    }

    if (WFD02620_Assignment_Models.MasterSettings != null && WFD02620_Assignment_Models.MasterSettings.START_HRS != null) {
        starts = WFD02620_Assignment_Models.MasterSettings.START_HRS.split(':'); // result example : ['07','30']
    } else {
        starts = null;
    }

    if (WFD02620_Assignment_Models.MasterSettings != null && WFD02620_Assignment_Models.MasterSettings.END_HRS != null) {
        ends = WFD02620_Assignment_Models.MasterSettings.END_HRS.split(':'); // result example : ['16','30']
    } else {
        ends = null;
    }

    if (breakTimes != null) {

        CLD_Time.BreakTimeStart.setHours(parseInt(breakTimes[0].split(':')[0]));
        CLD_Time.BreakTimeStart.setMinutes(parseInt(breakTimes[0].split(':')[1]));
        CLD_Time.BreakTimeStart.setSeconds(0);

        CLD_Time.BreakTimeEnd.setHours(parseInt(breakTimes[1].split(':')[0]));
        CLD_Time.BreakTimeEnd.setMinutes(parseInt(breakTimes[1].split(':')[1]));
        CLD_Time.BreakTimeEnd.setSeconds(0);

    } else {
        CLD_Time.BreakTimeStart.setHours(12);
        CLD_Time.BreakTimeStart.setMinutes(0);
        CLD_Time.BreakTimeStart.setSeconds(0);

        CLD_Time.BreakTimeEnd.setHours(13);
        CLD_Time.BreakTimeEnd.setMinutes(0);
        CLD_Time.BreakTimeEnd.setSeconds(0);
    }

    if (starts != null) {
        CLD_Time.Start.setHours(starts[0]);
        CLD_Time.Start.setMinutes(starts[1]);
        CLD_Time.Start.setSeconds(0);
    } else {
        CLD_Time.Start.setHours(0);
        CLD_Time.Start.setMinutes(0);
        CLD_Time.Start.setSeconds(0);
    }

    if (ends != null) {
        CLD_Time.End.setHours(ends[0]);
        CLD_Time.End.setMinutes(ends[1]);
        CLD_Time.End.setSeconds(0);
    } else {
        CLD_Time.End.setHours(0);
        CLD_Time.End.setMinutes(0);
        CLD_Time.End.setSeconds(0);
    }

    var holidayas = [];
    for (var i = 0; i < WFD02620_Assignment_Models.Holidays.length; i++) {

        holidayas.push(WFD02620_Assignment_Models.Holidays[i].HOLIDATE_DATE);

    }

    WorkingTime.SetSettings(CLD_Time.Start.TimeOfDay(), CLD_Time.End.TimeOfDay(), CLD_Time.BreakTimeStart.TimeOfDay(), CLD_Time.BreakTimeEnd.TimeOfDay(), holidayas);

};

//#region   Convert data

var ConvertHData_WFD02620_Assignment = function (data) {
    data.DATA_AS_OF = ConvertToJsonDate(data.DATA_AS_OF);
    data.TARGET_DATE_FROM = ConvertToJsonDate(data.TARGET_DATE_FROM);
    data.TARGET_DATE_TO = ConvertToJsonDate(data.TARGET_DATE_TO);
    data.UPDATE_DATE = ConvertToJsonDate(data.UPDATE_DATE);
    return data;
};
var ConvertSVDatas_WFD02620_Assignment = function (datas) {
    for (var i = 0; i < datas.length; i++) {
        datas[i].CREATE_DATE = ConvertToJsonDate(datas[i].CREATE_DATE);
        datas[i].UPDATE_DATE = ConvertToJsonDate(datas[i].UPDATE_DATE);
        datas[i].FA_DATE_FROM = ConvertToJsonDate(datas[i].FA_DATE_FROM);
        datas[i].FA_DATE_TO = ConvertToJsonDate(datas[i].FA_DATE_TO);
        datas[i].DATE_FROM = ConvertToJsonDate(datas[i].DATE_FROM);
        datas[i].DATE_TO = ConvertToJsonDate(datas[i].DATE_TO);
    }
    return datas;
};
var ConvertHolidayData_WFD02620_Assignment = function (datas) {
    for (var i = 0; i < datas.length; i++) {
        datas[i].HOLIDATE_DATE = ConvertToJsonDate(datas[i].HOLIDATE_DATE);
    }
    return datas;
};

//#endregion

//#region   Summary block

var setsummaryMinorCateUI_WFD02620_Assignment = function () {
    WFD02620_AssignmentControls.SummaryBlock.html('');

    if (WFD02620_Assignment_Models.Summaries != null && WFD02620_Assignment_Models.Summaries.length > 0) {
        var html = '';
        var total = 0;

        for (var i = 0; i < WFD02620_Assignment_Models.Summaries.length; i++) {

            html += genSummaryBlock_WFD02620_Assignment(WFD02620_Assignment_Models.Summaries[i]);
            total += WFD02620_Assignment_Models.Summaries[i].TOTAL_CNT;

        }

        html += genSummaryBlock_WFD02620_Assignment({ MINOR_CATEGORY: 'Total', TOTAL_CNT: total });

        WFD02620_AssignmentControls.SummaryBlock.html(html);
    }
};
var genSummaryBlock_WFD02620_Assignment = function (summaryMinorCate) {

    var html = '';
    if (summaryMinorCate.MINOR_CATEGORY == 'Total') html += '<div class="num-box" style="margin-left: 10px;">';
    else html += '<div class="num-box">';
    html += '<div class="num-box-header">' + summaryMinorCate.MINOR_CATEGORY + '</div>';
    html += '<div class="num-box-body">' + summaryMinorCate.TOTAL_CNT + '</div>';
    html += '</div>';

    return html;
};

//#endregion

//#region   Assignment Details

var setSVUI_WFD02620_Assignment = function () {
    WFD02620_AssignmentControls.SVTableBody.html('');
    var html = '';
    for (var i = 0; i < WFD02620_Assignment_Models.SVDatas.length; i++) {
        //console.log(WFD02620_Assignment_Models.SVDatas);
        html += genSVRow_WFD02620_Assignment(WFD02620_Assignment_Models.SVDatas[i], i);
    }
    WFD02620_AssignmentControls.SVTableBody.html(html);

    $('.WFD02620_UnLockCheckbox').change(function () {
        if ($(this).is(":checked")) {
            $(this).prop('checked', false);
        } else {
            var ind = $(this).attr('data-index');
            WFD02620_Assignment_Models.SVDatas[ind].IS_LOCK = 'N';
        }
    });
    //&& WFD02620_Assignment_Models.HData.PLAN_STATUS != 'C'
    if (WFD02620_User.IS_FA_ADMIN == true && WFD02620_Assignment_Models.HData.PLAN_STATUS != 'D') {
        WFD02620_AssignmentControls.lockTBCol.show();
        $('.WFD02620_UnLockCheckbox').parent().show();
    } else {
        WFD02620_AssignmentControls.lockTBCol.hide();
        $('.WFD02620_UnLockCheckbox').parent().hide();
    }

    if (WFD02620_AssignmentControls.SVTableBody.find('tr').length <= 0) {
        WFD02620_AssignmentControls.SaveAssignmentButton.hide();
        WFD02620_AssignmentControls.CancelAssignmentButton.hide();
    } else {
        setModeButtonInitialAssignmentControls(WFD02620_Param.PLAN_STATUS);
    }

    if (WFD02620_Assignment_Models.HData.ASSET_LOCATION == "I") {
        WFD02620_AssignmentControls.SVTable.find('th').first().html('S/V Name');
        $("#SVAssignmentTable th:nth-child(3)").html('S/V Name');
    } else {
        WFD02620_AssignmentControls.SVTable.find('th').first().html('Supplier');
        $("#SVAssignmentTable th:nth-child(3)").html('Supplier');
    }
};

var setModeButtonInitialAssignmentControls = function (planStatus) {
    WFD02620_AssignmentControls.SaveAssignmentButton.show();
    WFD02620_AssignmentControls.CancelAssignmentButton.show();

    switch (planStatus) {
        case 'C':
            if (IsFAAdmin_WFD02620 == false) {
                WFD02620_AssignmentControls.SaveAssignmentButton.hide();
                WFD02620_AssignmentControls.CancelAssignmentButton.hide();
            }
            break;
        case 'S':
            if (IsFAAdmin_WFD02620 == false) {
                WFD02620_AssignmentControls.SaveAssignmentButton.hide();
                WFD02620_AssignmentControls.CancelAssignmentButton.hide();
            }
            break;
        case 'F':
            WFD02620_AssignmentControls.SaveAssignmentButton.hide();
            WFD02620_AssignmentControls.CancelAssignmentButton.hide();
            break;
    }


};

var genSVRow_WFD02620_Assignment = function (data, index) {
    var html = '';

    if ((WFD02620_User.EMP_CODE == data.EMP_CODE && WFD02620_User.IS_FA_ADMIN == false) || WFD02620_User.IS_FA_ADMIN == true) {
        //console.log("TIME_START");
        //console.log(data.TIME_START);
        //console.log("TIME_END");
        //console.log(data.TIME_END);
        //console.log("DATE_FROM");
        //console.log(data.DATE_FROM);
        //console.log("DATE_TO");
        //console.log(data.DATE_TO);

        var workingTimeSummaryStr = calTimeFromStr_WFD02620_Assignment(data.TIME_START, data.TIME_END, data.DATE_FROM, data.DATE_TO, index);
        calHHTCNT(workingTimeSummaryStr, index);
        html += '<tr>';

        html += '<td>' + data.EMP_TITLE + ' ' + data.EMP_NAME + ' ' + data.EMP_LASTNAME + '</td>';
        html += '<td class="text-right">' + data.TOTAL_ASSET + '</td>';
        if (WFD02620_User.IS_FA_ADMIN || WFD02620_User.EMP_CODE == WFD02620_Assignment_Models.SVDatas[index].EMP_CODE) {
            if (WFD02620_Param.PLAN_STATUS == 'D') {
                html += '<td class="text-center"><a href="#" onclick="openSetWorkingTimeDialog_WFD02620_Assignment(' + index + ');">' + workingTimeSummaryStr + '</a></td>';

            } else {
                html += '<td class="text-center">' + workingTimeSummaryStr + '</td>';
            }
        } else {
            html += '<td class="text-center">' + workingTimeSummaryStr + '</td>';
        }


        html += '<td class="text-right">' + data.USAGE_HANDHELD + '</td>';

        if (data.IS_LOCK == 'Y') {
            html += '<td><input type="checkbox" data-index="' + index + '" class="WFD02620_UnLockCheckbox" checked style="margin-left: 7px;" /></td>';
        } else {
            html += '<td ><input type="checkbox" data-index="' + index + '" class="WFD02620_UnLockCheckbox" style="margin-left: 7px;" /></td>';
        }

        html += '</tr>';
    }

    return html;
};
var calHHTCNT = function (times, index) {
    if (times == "00:00") {
        WFD02620_Assignment_Models.SVDatas[index].USAGE_HANDHELD = 0;
    } else {

        var mins = (ToNumber(times.split(':')[0]) * 60) + ToNumber(times.split(':')[1]);
        var itemCnt = WFD02620_Assignment_Models.SVDatas[index].TOTAL_ASSET;
        WFD02620_Assignment_Models.SVDatas[index].USAGE_HANDHELD = Math.ceil(itemCnt / (mins * 1.5));
        if (WFD02620_Assignment_Models.SVDatas[index].USAGE_HANDHELD < 1) {
            WFD02620_Assignment_Models.SVDatas[index].USAGE_HANDHELD = 1;
        }
    }
};
var calTimeFromStr_WFD02620_Assignment_Val = function (fromStr, toStr, dateFrom, dateTo) {

    if (fromStr == null || toStr == null) {
        return null;
    } else {

        var str = '';
        var df = new Date(dateFrom); // temp date from only date value
        var dt = new Date(dateTo); // temp date to only date value

        var from = new Date(dateFrom); // temp date from with working time
        var to = new Date(dateTo); // temp date to with working time

        var startTimeH = ToNumber(WFD02620_Assignment_Models.MasterSettings.START_HRS.split(':')[0]);
        var startTimeM = ToNumber(WFD02620_Assignment_Models.MasterSettings.START_HRS.split(':')[1]);

        var sysStart = new Date(); // System start working time 
        var sysEnd = new Date(); // System end working time

        sysStart.setHours(WFD02620_Assignment_Models.MasterSettings.START_HRS.split(':')[0], WFD02620_Assignment_Models.MasterSettings.START_HRS.split(':')[1], 0, 0);
        sysEnd.setHours(WFD02620_Assignment_Models.MasterSettings.END_HRS.split(':')[0], WFD02620_Assignment_Models.MasterSettings.END_HRS.split(':')[1], 0, 0);

        from.setHours(fromStr.split(':')[0], fromStr.split(':')[1], 0);
        to.setHours(toStr.split(':')[0], toStr.split(':')[1], 0);

        var days = GetDayFromDateDiff_WFD02620(df, dt);
        var holidays = GetHolidayFromDateDiff_WFD02620(df, dt);
        var dateDiff = 0;
        var breakDiff = _diffBreakTime();


        if (days == 1) {
            if ($.format.date(from, "dd MMM yyyy") != $.format.date(to, "dd MMM yyyy")) {
                if (isHoliday(from)) {
                    from.setDate(to.getDate());
                    from.setMonth(to.getMonth());
                    from.setYear(to.getFullYear());
                    from.setHours(sysStart.getHours(), sysStart.getMinutes(), 0, 0);
                }
                if (isHoliday(to)) {
                    to.setDate(from.getDate());
                    to.setMonth(from.getMonth());
                    to.setYear(from.getFullYear());
                    to.setHours(sysEnd.getHours(), sysEnd.getMinutes(), 0, 0);
                }
            }
            if (from.getTimeMinute_WFD02620() >= CLD_Time.BreakTimeStart.getTimeMinute_WFD02620() && from.getTimeMinute_WFD02620() <= CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620()) {
                from.setHours(CLD_Time.BreakTimeEnd.getHours(), CLD_Time.BreakTimeEnd.getMinutes(), 0, 0);
            }
            if (to.getTimeMinute_WFD02620() >= CLD_Time.BreakTimeStart.getTimeMinute_WFD02620() && to.getTimeMinute_WFD02620() <= CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620()) {
                to.setHours(CLD_Time.BreakTimeStart.getHours(), CLD_Time.BreakTimeStart.getMinutes(), 0, 0);
            }

            dateDiff = to - from;
            if (_isPassBreakTime(from, to)) {
                dateDiff = dateDiff - breakDiff;
            }
        } else if (days > 1) {
            if (days > 2) {
                dateDiff += (sysEnd - sysStart - breakDiff) * (days - 2);
                days = days - 2;
            }

            var systemStartDate = new Date(from.getFullYear(), from.getMonth(), from.getDate(), sysStart.getHours(), sysStart.getMinutes(), 0, 0);
            var systemEndDayStartDate = new Date(from.getFullYear(), from.getMonth(), from.getDate(), sysEnd.getHours(), sysEnd.getMinutes(), 0, 0);

            var systemEndDate = new Date(to.getFullYear(), to.getMonth(), to.getDate(), sysEnd.getHours(), sysEnd.getMinutes(), 0, 0);
            var systemStartDayEndDate = new Date(to.getFullYear(), to.getMonth(), to.getDate(), sysStart.getHours(), sysStart.getMinutes(), 0, 0);

            if (from < systemStartDate) from = systemStartDate;
            if (to > systemEndDate) to = systemEndDate;

            dateDiff += (systemEndDayStartDate - from);
            dateDiff += (to - systemStartDayEndDate);
            if (_isPassBreakTime(from, systemEndDayStartDate)) {
                dateDiff = dateDiff - breakDiff;
            }
            if (_isPassBreakTime(systemStartDayEndDate, to)) {
                dateDiff = dateDiff - breakDiff;
            }
        }

        return dateDiff;
    }
}

var calTimeFromStr_WFD02620_Assignment = function (fromStr, toStr, dateFrom, dateTo, index) {


    if (fromStr == null || toStr == null) {
        return '00:00';
    }

    var from = new Date(dateFrom);
    var to = new Date(dateTo);

    from.setHours(fromStr.split(':')[0], fromStr.split(':')[1], 0);
    to.setHours(toStr.split(':')[0], toStr.split(':')[1], 0);

    var minDiff = WorkingTime.GetDifMinute(from, to);

    if (minDiff == 0) {
        return '00:00';
    }
    else {
        var hstr = (Math.floor(minDiff / 60)).toString();
        var mstr = (minDiff % 60).toString();
        var pat = "00"

        WFD02620_Assignment_Models.SVDatas[index].TIME_MINUTE = Math.round(minDiff);
        return showStrXDigit(hstr, '00') + ':' + showStrXDigit(mstr, '00');
    }
}
var _diffBreakTime = function () {
    try {
        var breakTimes = [];
        var breakTimeStart = new Date();
        var breakTimeEnd = new Date();
        breakTimes = WFD02620_Assignment_Models.MasterSettings.BREAK_TIME.split('-'); // result example : ['11:31','12:30']
        breakTimeStart.setHours(breakTimes[0].split(':')[0], breakTimes[0].split(':')[1], 0, 0);
        breakTimeEnd.setHours(breakTimes[1].split(':')[0], breakTimes[1].split(':')[1], 0, 0);

        return breakTimeEnd - breakTimeStart;
    } catch (err) {
        AlertTextErrorMessage(err);
        return 0;
    }
}
var _isPassBreakTime = function (dateFrom, dateTo) {
    try {
        var breakTimes = [];
        var breakTimeStart = new Date(dateFrom);
        var breakTimeEnd = new Date(dateFrom);
        breakTimes = WFD02620_Assignment_Models.MasterSettings.BREAK_TIME.split('-'); // result example : ['11:31','12:30']
        breakTimeStart.setHours(breakTimes[0].split(':')[0], breakTimes[0].split(':')[1], 0, 0);
        breakTimeEnd.setHours(breakTimes[1].split(':')[0], breakTimes[1].split(':')[1], 0, 0);

        if (dateFrom <= breakTimeStart && breakTimeEnd <= dateTo) {
            return true;
        } else {
            return false;
        }
    } catch (err) {
        AlertTextErrorMessage(err);
        return false;
    }
}
function showStrXDigit(txt, pattern) {
    var pat = pattern;
    return pat.substring(0, pat.length - txt.length) + txt;

}
function GetDayFromDateDiff_WFD02620(first, second) {
    var cnt = 0;
    while (first <= second) {

        if (!isHoliday(first)) {
            cnt += 1;
        }
        first = first.addDays(1);
    }
    return cnt;
}
function GetHolidayFromDateDiff_WFD02620(first, second) {
    var cnt = 0;
    while (first <= second) {

        if (isHoliday(first)) {
            cnt += 1;
        }
        first = first.addDays(1);
    }
    return cnt;
}

//#endregion

//#region   

// คำนวณว่ามีช่วงเวลาไหนที่มีการใช้งาน handheld เกินจำนวน handheld ที่มี
var setHandheld_IntersectionUsage_ScreenUI = function () {

    var datas = JSON.parse(JSON.stringify(WFD02620_Assignment_Models.SVDatas));//jQuery.extend(true, {}, WFD02620_Assignment_Models.SVDatas);
    if (datas.length <= 50) {
        var datas = ajax_method.ConvertDateBeforePostBack(datas);
    }
    var data = {
        datas: datas,
        MaxHHT: WFD02620_Assignment_Models.HData.QTY_OF_HANDHELD
    }
    ajax_method.Post(WFD02620_URL.CalculateIntersectUsageHHTMoreThanMaxHHT, data, true,
        function (res) {
            if (res != null) {
                for (var i = 0; i < res.length; i++) {
                    res[i].start = convertCSDate(res[i].start);
                    res[i].end = convertCSDate(res[i].end);
                }
                setUsageHHTMoreThanMaxScreenUI(res);
            }

        }, function (err) {
            console.error(err);
        }, false, 'TBCalendarBlock');
}

var setUsageHHTMoreThanMaxScreenUI = function (periods) {
    var errorClass = 'WrongProcessPeriod';

    for (var i = 0; i < periods.length; i++) {
        var dates = getDateInPeriod(periods[i].start, periods[i].end);
        for (var j = 0; j < dates.length; j++) {
            $('tr[data-date="' + $.format.date(dates[j], "ddMMyyyy") + '"]').find('div[data-type="time-bg-cell"]').each(function () {
                var df = $(this).attr('data-from').getDateTimeFromStr();
                var dt = $(this).attr('data-to').getDateTimeFromStr();
                var empCode = $(this).attr('data-emp-code');


                //if (periods[i].start <= df && periods[i].end >= dt) {
                if (((periods[i].start.getTime() <= df.getTime() && periods[i].end.getTime() >= dt.getTime())
                            || (periods[i].start.getTime() <= df.getTime() && periods[i].end.getTime() <= dt.getTime() && periods[i].end.getTime() >= df.getTime())
                            || (periods[i].start.getTime() >= df.getTime() && periods[i].end.getTime() <= dt.getTime())
                            || (periods[i].start.getTime() >= df.getTime() && periods[i].start.getTime() < dt.getTime() && periods[i].end.getTime() >= dt.getTime())
                        )) {

                    var empWorkingData = thereIsSVWorkingInPeriod(empCode, df, dt);




                    if (!isBreakTime(df, dt) && empWorkingData != null) {
                        var dateFrom = new Date(empWorkingData.DATE_FROM);
                        var dateTo = new Date(empWorkingData.DATE_TO);
                        var timeStart = empWorkingData.TIME_START.getTimeFromStr();
                        var timeEnd = empWorkingData.TIME_END.getTimeFromStr();

                        // Set time to date
                        dateFrom.setHours(timeStart.getHours(), timeStart.getMinutes(), 0, 0);
                        dateTo.setHours(timeEnd.getHours(), timeEnd.getMinutes(), 0, 0);


                        $(this).addClass(errorClass);
                        processBlock = $('div[data-type="process-cell"][data-from="' + $.format.date(df, "ddMMyyyy-HHmm") + '"][data-to="' + $.format.date(dt, "ddMMyyyy-HHmm") + '"]');

                        if (processBlock.attr('data-error') != 'hhtcnt') {
                            processBlock.attr('data-error', 'hhtcnt');
                            var title = empWorkingData.EMP_TITLE
                                    + ' ' + empWorkingData.EMP_NAME
                                    + ' ' + empWorkingData.EMP_LASTNAME
                                    + '<br/>' + $.format.date(dateFrom, "dd MMM yyyy HH:mm") + ' - '
                                    + ' ' + $.format.date(dateTo, "dd MMM yyyy HH:mm")
                                    + '<br/> Handheld usage: ' + empWorkingData.USAGE_HANDHELD
                                    + '<br /><br /><i style="color: #ff7c6e; font-style: normal;font-weight: bold;">There is usage handheld more than max handheld in period'
                                    + '<br />' + $.format.date(periods[i].start, "dd MMM yyyy HH:mm") + ' - ' + $.format.date(periods[i].end, "dd MMM yyyy HH:mm")
                                    + '<br />Usage total: ' + periods[i].HHT_CNT + '</i>';
                            processBlock.attr('title', title);
                        }
                    }
                }
            });
        }
    }
}
var getDateInPeriod = function (from, to) {
    var res = []
    var d = new Date(from.getFullYear(), from.getMonth(), from.getDate(), 0, 0, 0, 0);
    var t = new Date(to.getFullYear(), to.getMonth(), to.getDate(), 0, 0, 0, 0);

    while (d <= t) {
        res.push(d);
        d = d.addDays(1);
    }

    return res;
}
var thereIsSVWorkingInPeriod = function (empCode, from, to) {
    var isThere = false;
    var data = null;

    for (var i = 0; i < WFD02620_Assignment_Models.SVDatas.length; i++) {
        var ef = WFD02620_Assignment_Models.SVDatas[i].DATE_FROM;
        var et = WFD02620_Assignment_Models.SVDatas[i].DATE_TO;
        var ts = WFD02620_Assignment_Models.SVDatas[i].TIME_START;
        var te = WFD02620_Assignment_Models.SVDatas[i].TIME_END;

        if (ef != null && et != null && ts != null && te != null) {
            var f = new Date(ef.getFullYear(), ef.getMonth(), ef.getDate(), integer(ts.split(':')[0]), integer(ts.split(':')[1]), 0, 0);
            var t = new Date(et.getFullYear(), et.getMonth(), et.getDate(), integer(te.split(':')[0]), integer(te.split(':')[1]), 0, 0);


            to.setHours(to.getHours(), to.getMinutes(), 0, 0);
            from.setHours(from.getHours(), from.getMinutes(), 0, 0);

            //if (WFD02620_Assignment_Models.SVDatas[i].EMP_CODE == empCode
            //    && WFD02620_Assignment_Models.SVDatas[i].DATE_FROM <= to && WFD02620_Assignment_Models.SVDatas[i].DATE_TO >= from) {
            if (WFD02620_Assignment_Models.SVDatas[i].EMP_CODE == empCode
                && f.getTime() < to.getTime() && t.getTime() > from.getTime()) {
                isThere = true;
                data = WFD02620_Assignment_Models.SVDatas[i];
                break;
            }
        }


    }

    return data;
}

//#endregion

//#region   Set working time dialog

var WorkingTimeDialogVal_WFD02620_Assignment = {
    data: null,
    index: 0
}

var ConfigWorkingTimeDialogControls = {
    SVName: $('#WFD02620SetWorkingTimeSVName'),
    DateFrom: $('#WFD02620DATE_FROM'),
    DateTo: $('#WFD02620DATE_TO'),
    TimeStart: $('#WFD02620TIME_START'),
    TimeEnd: $('#WFD02620TIME_END'),
    ButtonOK: $('#btnWFD02620OKFP'),
    ButtonCancel: $('#btWFD02620nCancelFP'),
    PERIOD: $('#WFD02620DATEPERIOD')
}
var openSetWorkingTimeDialog_WFD02620_Assignment = function (index) {
    ClearMessageC();

    if (WFD02620_User.IS_FA_ADMIN || WFD02620_User.EMP_CODE == WFD02620_Assignment_Models.SVDatas[index].EMP_CODE) {
        WorkingTimeDialogVal_WFD02620_Assignment.data = WFD02620_Assignment_Models.SVDatas[index];
        WorkingTimeDialogVal_WFD02620_Assignment.index = index;


        //ConfigWorkingTimeDialogControls.TimeStart.timepicker({
        //    'minTime': CLD_Time.Start,
        //    'maxTime': CLD_Time.End,
        //    'timeFormat': 'H:i',
        //});
        //ConfigWorkingTimeDialogControls.TimeEnd.timepicker({
        //    'minTime': CLD_Time.Start,
        //    'maxTime': CLD_Time.End,
        //    'timeFormat': 'H:i',
        //});

        //setTimeout(function () {
        ConfigWorkingTimeDialogControls.PERIOD.periodpicker({
            yearsLine: true,
            title: false,
            inline: true,
            norange: false,
            cells: [1, 2],
            todayButton: true,
            fullsizeButton: false,
            resizeButton: false,
            navigate: true,
            animation: true,
            withoutBottomPanel: false,
            showTimepickerInputs: true,
            showDatepickerInputs: true,
            defaultEndTime: WorkingTimeDialogVal_WFD02620_Assignment.data.TIME_END,
            mousewheel: true,
            mousewheelYearsLine: true,
            //hideAfterSelecct: false,
            //hideOnBlur: false,
            //likeXDSoftDateTimePicker: false,
            //yearSizeInPixels: 200,
            //startMonth: moment(WorkingTimeDialogVal_WFD02620_Assignment.data.DATE_FROM).format('MM'),

            formatDate: 'DD-MMM-YYYY',
            formatDateTime: 'HH:mm DD-MMM-YYYY',
            formatDecoreDateTimeWithYear: 'HH:mm DD-MMM-YYYY',
            minDate: moment(WFD02620_Assignment_Models.HData.TARGET_DATE_FROM).format('DD-MMM-YYYY'),
            maxDate: moment(WFD02620_Assignment_Models.HData.TARGET_DATE_TO).add(1, 'day').format('DD-MMM-YYYY'),

            timepicker: true,
            timepickerOptions: {
                hours: true,
                minutes: true,
                seconds: false,
                ampm: false,
                useTimepickerLimits: true,
                //dragAndDrop: true,
                mouseWheel: true,
                defaultTime: WorkingTimeDialogVal_WFD02620_Assignment.data.TIME_START,
                twelveHoursFormat: false,
                steps: [1, 5, 1, 1]
            },

            //onAfterRegenerate: function () {
            //    ConfigWorkingTimeDialogControls.PERIOD.periodpicker('change');
            //},
            //onChange: function () {
            //    ConfigWorkingTimeDialogControls.PERIOD.periodpicker('change');
            //},
            //onAfterShow: function () {
            //    ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value', [WorkingTimeDialogVal_WFD02620_Assignment.data.TIME_START + ' ' + moment(WorkingTimeDialogVal_WFD02620_Assignment.data.DATE_FROM).format('DD-MMM-YYYY'), WorkingTimeDialogVal_WFD02620_Assignment.data.TIME_END + ' ' + moment(WorkingTimeDialogVal_WFD02620_Assignment.data.DATE_TO).format('DD-MMM-YYYY')]);
            //    ConfigWorkingTimeDialogControls.PERIOD.periodpicker('change');
            //}
        });
        //}, 500);

        setTimeout(function () {
            //ConfigWorkingTimeDialogControls.PERIOD.periodpicker('regenerate');
            ConfigWorkingTimeDialogControls.PERIOD.periodpicker('show')
            ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value', [WorkingTimeDialogVal_WFD02620_Assignment.data.TIME_START + ' ' + moment(WorkingTimeDialogVal_WFD02620_Assignment.data.DATE_FROM).format('DD-MMM-YYYY'), WorkingTimeDialogVal_WFD02620_Assignment.data.TIME_END + ' ' + moment(WorkingTimeDialogVal_WFD02620_Assignment.data.DATE_TO).format('DD-MMM-YYYY')]);
            //ConfigWorkingTimeDialogControls.PERIOD.periodpicker('change');
        }, 500);

        if (WFD02620_User.IS_FA_ADMIN != true) {
            //ConfigWorkingTimeDialogControls.TimeStart.change(function (e) {
            //    WFD02620_OnChangeWorkingDateTime(index);
            //});

            //ConfigWorkingTimeDialogControls.DateFrom.datepicker().on('change', function (e) {
            //    WFD02620_OnChangeWorkingDateTime(index);
            //});
            ConfigWorkingTimeDialogControls.PERIOD.on('change', function () {
                WFD02620_OnChangeWorkingDateTime(index);
            });

            //ConfigWorkingTimeDialogControls.TimeEnd.prop('disabled', true);
            //ConfigWorkingTimeDialogControls.DateTo.prop('disabled', true);
            //ConfigWorkingTimeDialogControls.DateTo.parent().find('.input-group-addon').each(function (e) {
            //    $(this).hide();
            //});
        } else {
            //ConfigWorkingTimeDialogControls.TimeEnd.prop('disabled', false);
            //ConfigWorkingTimeDialogControls.DateTo.prop('disabled', false);
            //ConfigWorkingTimeDialogControls.DateTo.parent().find('.input-group-addon').each(function (e) {
            //    $(this).show();
            //});
        }


        ConfigWorkingTimeDialogControls.SVName.val(
            WorkingTimeDialogVal_WFD02620_Assignment.data.EMP_TITLE
            + ' ' + WorkingTimeDialogVal_WFD02620_Assignment.data.EMP_NAME
            + ' ' + WorkingTimeDialogVal_WFD02620_Assignment.data.EMP_LASTNAME);

        ConfigWorkingTimeDialogControls.ButtonOK.off('click').on('click', SetWorkingTimeDialog_OKClick);

        WFD02620_AssignmentControls.SetWorkingTimeDialog.appendTo("body").modal('show');
    }
}

var WFD02620_OnChangeWorkingDateTime = function (index) {
    //var timeStart = ConfigWorkingTimeDialogControls.TimeStart.val();
    var timeStart = moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0]).format('HH:mm')
    //var dateFrom = getDatePickerDate(ConfigWorkingTimeDialogControls.DateFrom);
    var dateFrom = moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0]).format('DD-MMM-YYYY');
    var min = WFD02620_Assignment_Models.SVDatas[index].TIME_MINUTE;

    WFD02620_AutoSetDateTimeTo(timeStart, min, dateFrom);
}

var WFD02620_AutoSetDateTimeTo = function (fromStr, min, dateFrom) {
    var df = new Date(dateFrom); // temp date from only date value
    var date = new Date(dateFrom); // temp date to with working time
    df.setHours(0, 0, 0);
    date.setHours(fromStr.split(':')[0], fromStr.split(':')[1], 0);
    var date = WorkingTime.GetResultDateTimeAfterAddMinute(date, min);

    //ConfigWorkingTimeDialogControls.DateTo.datepicker("setDate", date);
    //ConfigWorkingTimeDialogControls.TimeEnd.val(showStrXDigit(date.getHours().toString(), '00') + ':' + showStrXDigit(date.getMinutes().toString(), '00'));
    ConfigWorkingTimeDialogControls.TimeEnd.val(showStrXDigit(date.getHours().toString(), '00') + ':' + showStrXDigit(date.getMinutes().toString(), '00'));
}

var IsDateTimeInBreak = function (date) {
    if (CLD_Time.BreakTimeStart.getTimeMinute_WFD02620() <= date.getTimeMinute_WFD02620()
            && date.getTimeMinute_WFD02620() <= CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620()) {
        return true;
    } else {
        return false;
    }
}
var IsDateTimeOutOfDay = function (date) {
    if (date.getTimeMinute_WFD02620() > CLD_Time.End.getTimeMinute_WFD02620()) {
        return true;
    } else {
        return false;
    }
}

var overBreakTime = function (startDate, endDate) {

    var start = startDate.getTimeMinute_WFD02620();
    var end = endDate.getTimeMinute_WFD02620();
    var bStart = CLD_Time.BreakTimeStart.getTimeMinute_WFD02620();
    var bEnd = CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620();

    if ((start < bStart && end > bStart && end <= bEnd) || (start < bEnd && end >= bEnd) || (start > bStart && end <= bEnd)) {
        return true;
    } else {
        return false;
    }

}

var calWorkingTime_WFD02620_Assignment = function (startDate, endDate, startTime, endTime, masterStartTime, masterEndTime) {

    var diffMs = null;
    var dt = new Date();
    var ms = 0;

    if (startDate.getDate() < endDate.getDate()) {

        while (startDate.getDate() < endDate.getDate()) {
            if (!isHoliday(startDate.getDate())) {
                diffMs = (masterEndTime - startTime);
                ms += Math.round(((diffMs % 86400000) % 3600000) / 60000);
            }

            startDate.addDays(1);
        }

        diffMs = (endTime - masterStartTime);
        ms += Math.round(((diffMs % 86400000) % 3600000) / 60000);

    } else {

        diffMs = (endTime - startTime);
        ms += Math.round(((diffMs % 86400000) % 3600000) / 60000);
    }

    return ms;
}

var CheckHHCount_WFD02620_Assignment = function (fromStr, toStr, dateFrom, dateTo) {

    //console.log("Test#1");
    if (fromStr == null || toStr == null) {
        return '00:00';
    }
    //console.log("Test#2");
    var from = new Date(dateFrom);
    var to = new Date(dateTo);
    //console.log(dateFrom);
    //console.log(dateTo);

    from.setHours(fromStr.split(':')[0], fromStr.split(':')[1], 0);
    to.setHours(toStr.split(':')[0], toStr.split(':')[1], 0);

    //console.log(fromStr);
    //console.log(toStr);
    var minDiff = WorkingTime.GetDifMinute(from, to);
    //console.log("Test#3");
    if (minDiff == 0) {
        return '00:00';
    }
    else {
        //console.log("Test#4");
        var hstr = (Math.floor(minDiff / 60)).toString();
        var mstr = (minDiff % 60).toString();
        var pat = "00"

        //WFD02620_Assignment_Models.SVDatas[index].TIME_MINUTE = Math.round(minDiff);
        return showStrXDigit(hstr, '00') + ':' + showStrXDigit(mstr, '00');
    }
}
var CheckHHCount = function (data, itemCnt) {
    var _ttl = CheckHHCount_WFD02620_Assignment(data.TimeStart, data.TimeEnd, data.DateFrom, data.DateTo);
    //console.log(_ttl);

    if (_ttl == "00:00") {
        return 0;
    } else {

        var mins = (ToNumber(_ttl.split(':')[0]) * 60) + ToNumber(_ttl.split(':')[1]);
        //console.log("Minute Count");
        //console.log(mins);

        var _hhCnt = Math.ceil(itemCnt / (mins * 1.5));
        //console.log("HH Count");
        //console.log(_hhCnt);

        if (_hhCnt == 0)
            return 1;

        return _hhCnt;

        //  if (WFD02620_Assignment_Models.SVDatas[index].USAGE_HANDHELD < 1) {
        //     WFD02620_Assignment_Models.SVDatas[index].USAGE_HANDHELD = 1;
        // }
    }
    console.log("OK");
};

var SetWorkingTimeDialog_OKClick = function () {

    var _vData = {
        //TimeStart: ConfigWorkingTimeDialogControls.TimeStart.val(),
        TimeStart: moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0]).format('HH:mm'),
        //TimeEnd: ConfigWorkingTimeDialogControls.TimeEnd.val(),
        TimeEnd: moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[1]).format('HH:mm'),
        //DateFrom: getDatePickerDate(ConfigWorkingTimeDialogControls.DateFrom),
        DateFrom: ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0],
        //DateTo: getDatePickerDate(ConfigWorkingTimeDialogControls.DateTo),
        DateTo: ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[1],
        TargetFrom: WFD02620_Assignment_Models.HData.TARGET_DATE_FROM,
        TargetTo: WFD02620_Assignment_Models.HData.TARGET_DATE_TO,
        TimeStartDay: WFD02620_Assignment_Models.MasterSettings.START_HRS,
        TimeEndDay: WFD02620_Assignment_Models.MasterSettings.END_HRS
    };
    //console.log(test);



    var data = {
        //TimeStart: ConfigWorkingTimeDialogControls.TimeStart.val(),
        TimeStart: moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0]).format('HH:mm'),
        //TimeEnd: ConfigWorkingTimeDialogControls.TimeEnd.val(),
        TimeEnd: moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[1]).format('HH:mm'),
        //DateFrom: ConfigWorkingTimeDialogControls.DateFrom.val(),
        DateFrom: moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0]).format('DD-MMM-YYYY'),
        //DateTo: ConfigWorkingTimeDialogControls.DateTo.val(),
        DateTo: moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[1]).format('DD-MMM-YYYY'),
        TargetFrom: WFD02620_Assignment_Models.HData.TARGET_DATE_FROM,
        TargetTo: WFD02620_Assignment_Models.HData.TARGET_DATE_TO,
        TimeStartDay: WFD02620_Assignment_Models.MasterSettings.START_HRS,
        TimeEndDay: WFD02620_Assignment_Models.MasterSettings.END_HRS
    };






    var holidayas = jQuery.extend(true, {}, WFD02620_Assignment_Models.Holidays);
    var model = {
        data: ajax_method.ConvertDateBeforePostBack(data),
        holidays: ajax_method.ConvertDateBeforePostBack(holidayas)
    };

    //var dateFrom = getDatePickerDate(ConfigWorkingTimeDialogControls.DateFrom);
    var dateFrom = ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0];
    //var dateTo = getDatePickerDate(ConfigWorkingTimeDialogControls.DateTo);
    var dateTo = ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[1];

    if (moment($('#periodtimefrom')[0].value, 'HH:mm').format('HH:mm') !== $('#periodtimefrom')[0].value) {
        AlertTextErrorMessage(WFD02620_Message.InvalidFormat.replace('{0}', 'Time From').replace('{1}', 'HH:mm'));
        return;
    }
    if (moment($('#perioddatefrom')[0].value, 'DD-MMM-YYYY').format('DD-MMM-YYYY') !== $('#perioddatefrom')[0].value) {
        AlertTextErrorMessage(WFD02620_Message.InvalidFormat.replace('{0}', 'Date From').replace('{1}', 'DD-MMM-YYYY'));
        return;
    }
    if (moment($('#perioddateto')[0].value, 'DD-MMM-YYYY').format('DD-MMM-YYYY') !== $('#perioddateto')[0].value) {
        AlertTextErrorMessage(WFD02620_Message.InvalidFormat.replace('{0}', 'Date to').replace('{1}', 'DD-MMM-YYYY'));
        return;
    }
    if (moment($('#periodtimeto')[0].value, 'HH:mm').format('HH:mm') !== $('#periodtimeto')[0].value) {
        AlertTextErrorMessage(WFD02620_Message.InvalidFormat.replace('{0}', 'Time to').replace('{1}', 'HH:mm'));
        return;
    }

    ajax_method.Post(WFD02620_URL.ValidateSetWorkingTimeDialog, model, true, function (result) {

        if (result === true) {
            //Add New By Surasith
            var index = WorkingTimeDialogVal_WFD02620_Assignment.index;
            var _hhCnt = CheckHHCount(_vData, WFD02620_Assignment_Models.SVDatas[index].TOTAL_ASSET);

            if (_hhCnt == 0) { //MFAS1506AERR
                AlertTextErrorMessage(WFD02620_Message.NoWorkingDay);
                return;
            }

            var i = WorkingTimeDialogVal_WFD02620_Assignment.index;

            //WFD02620_Assignment_Models.SVDatas[i].TIME_START = ConfigWorkingTimeDialogControls.TimeStart.val();
            WFD02620_Assignment_Models.SVDatas[i].TIME_START = moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[0]).format('HH:mm')
            //WFD02620_Assignment_Models.SVDatas[i].TIME_END = ConfigWorkingTimeDialogControls.TimeEnd.val();
            WFD02620_Assignment_Models.SVDatas[i].TIME_END = moment(ConfigWorkingTimeDialogControls.PERIOD.periodpicker('value')[1]).format('HH:mm')
            WFD02620_Assignment_Models.SVDatas[i].DATE_FROM = new Date(dateFrom);
            WFD02620_Assignment_Models.SVDatas[i].DATE_TO = new Date(dateTo);

            //console.log("START");
            //console.log(WFD02620_Assignment_Models.SVDatas);
            //alert("OK");
            WFD02620_AssignmentControls.SetWorkingTimeDialog.appendTo("body").modal('hide');

            setAssignmentScreenUI();
            //console.log("END");
            //console.log(WFD02620_Assignment_Models.SVDatas[i]);
            ConfigWorkingTimeDialogControls.PERIOD.periodpicker('destroy');
        }

    }, null);
}



//#endregion

//#region   Calendar

// Time for generate calendar
var CLD_Time = {
    Start: new Date(),
    End: new Date(),
    BreakTimeStart: new Date(),
    BreakTimeEnd: new Date(),
};

// Dates
var CLD_Dates = [];
var CLD_Time_Cols = [];

// Convert time constant
var WFD02620_TIME_CONST = {
    HOUR_TO_MINUTE: 60,
    MINUTE_TO_MILLISECOND: 60000
};

var setCalendarUI_WFD02620_Assignment = function () {
    setCalendarCLDTime_WFD02620_Assignment();
    setCalendarCLD_Dates_WFD02620_Assignment();
    setCalendarCLD_Time_Cols_WFD02620_Assignment();

    setCalendarTalbeUI_WFD02620_Assignment();
    bindingCalendarData_WFD02620_Assignment();
}

// Initial calendar table ui
var setCalendarTalbeUI_WFD02620_Assignment = function () {
    setCalendarHeaderUI_WFD02620_Assignment();
    setCalendarRowUI_WFD02620_Assignment();
}
var setCalendarHeaderUI_WFD02620_Assignment = function () {
    WFD02620_AssignmentControls.CLD_Time_Col.attr('colspan', CLD_Time_Cols.length - 1);

    var html = '';

    for (var i = 0; i < CLD_Time_Cols.length - 1; i++) {
        var diffMs = (CLD_Time_Cols[i + 1] - CLD_Time_Cols[i]);
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        html += '<th style="min-width: ' + diffMins + 'px;" class="text-center">'
            + timeStr(CLD_Time_Cols[i].getHours()) + ':' + timeStr(CLD_Time_Cols[i].getMinutes())
            + '-'
            + timeStr(CLD_Time_Cols[i + 1].getHours()) + ':' + timeStr(CLD_Time_Cols[i + 1].getMinutes())
            + '</th>';
    }

    WFD02620_AssignmentControls.CLD_Time_Header_Row.html(html);
}
var setCalendarRowUI_WFD02620_Assignment = function () {
    var html = '';

    for (var i = 0; i < CLD_Dates.length; i++) {
        html += genCalendarRowHtml_WFD02620_Assignment(CLD_Dates[i]);
    }

    WFD02620_AssignmentControls.CLD_TBody.html(html);
}
var ChangeRowHoliday = function (elem) {

    //if (WFD02620_User.IS_FA_ADMIN && WFD02620_Assignment_Models.HData.PLAN_STATUS != 'C') {
    if (WFD02620_User.IS_FA_ADMIN && WFD02620_Assignment_Models.HData.PLAN_STATUS == 'D') {

        var ds = $(elem).attr('data-date').split('');

        var isHoliday = $(elem).hasClass('holiday-row');
        //console.log(ds);
        //console.log(isHoliday);



        var date = new Date(parseInt(ds[4] + ds[5] + ds[6] + ds[7]), parseInt(ds[2] + ds[3]) - 1, parseInt(ds[0] + ds[1]), 0, 0);

        //console.log("date");
        //console.log(date);

        // date.setMonth(date.getMonth() - 1);

        if (isHoliday === true && WFD02620_Assignment_Models != null && WFD02620_Assignment_Models.Holidays != null && WFD02620_Assignment_Models.Holidays.length > 0) {
            console.log("List of Holiday");
            for (var i = 0; i < WFD02620_Assignment_Models.Holidays.length; i++) {
                console.log($.format.date(WFD02620_Assignment_Models.Holidays[i].HOLIDATE_DATE, "dd MMM yyyy"));
                if ($.format.date(WFD02620_Assignment_Models.Holidays[i].HOLIDATE_DATE, "dd MMM yyyy") == $.format.date(date, "dd MMM yyyy")) {
                    WFD02620_Assignment_Models.Holidays.splice(i, 1);

                    break;
                }

            }
        } else {

            WFD02620_Assignment_Models.Holidays.push({ STOCK_TAKE_KEY: WFD02620_Assignment_Models.HData.STOCK_TAKE_KEY, HOLIDATE_DATE: date });
            //console.log("WFD02620_Assignment_Models.Holidays");
            //console.log(WFD02620_Assignment_Models.Holidays);
        }


        setAssignmentScreenUI();
    }
}
//}
var genCalendarRowHtml_WFD02620_Assignment = function (date) {

    //console.log("genCalendarRowHtml_WFD02620_Assignment");
    var html = '';
    var datas = getSVInDay_WFD02620_Assignment(date);
    //console.log(datas);
    //console.log("TEST");


    var rowspan = datas.length;
    if (rowspan == 0) rowspan = 1;


    if (datas.length <= 0 || isHoliday(date)) {

        //console.log("#ChangeRowHoliday");
        rowspan = 1;
        if (isHoliday(date)) html += '<tr class="holiday-row" ondblclick="ChangeRowHoliday(this)" data-date="' + $.format.date(date, "ddMMyyyy") + '">';
        else html += '<tr ondblclick="ChangeRowHoliday(this)" data-date="' + $.format.date(date, "ddMMyyyy") + '">';

        html += '<td rowspan="' + rowspan + '" class="text-center">' + days_WFD02620_Assignment[date.getDay()] + '</td>';
        html += '<td rowspan="' + rowspan + '" class="text-center">' + $.format.date(date, "dd-MMM-yyyy") + '</td>';

        html += '<td rowspan="' + rowspan + '"></td>';
        html += '<td rowspan="' + rowspan + '"></td>';

        html += genCLD_TimeCell(date, '');

        html += '</tr>';
    } else {
        html += '<tr data-row-emp="' + datas[0].EMP_CODE + '" ondblclick="ChangeRowHoliday(this)"  data-date="' + $.format.date(date, "ddMMyyyy") + '">';
        html += '<td rowspan="' + rowspan + '" class="text-center">' + days_WFD02620_Assignment[date.getDay()] + '</td>';
        html += '<td rowspan="' + rowspan + '" class="text-center">' + $.format.date(date, "dd-MMM-yyyy") + '</td>';
        html += '<td data-type="svName" data-emp-code="' + datas[0].EMP_CODE + '" data-date="' + $.format.date(date, "ddMMyyyy") + '" >-</td>';
        html += '<td class="text-right">' + datas[0].USAGE_HANDHELD + '</td>';

        html += genCLD_TimeCell(date, datas[0].EMP_CODE);

        html += '</tr>';

        for (var i = 1; i < datas.length; i++) {
            html += '<tr data-row-emp="' + datas[i].EMP_CODE + '" ondblclick="ChangeRowHoliday(this)" data-date="' + $.format.date(date, "ddMMyyyy") + '">';

            html += '<td data-type="svName" data-emp-code="' + datas[i].EMP_CODE + '" data-date="' + $.format.date(date, "ddMMyyyy") + '" >-</td>';
            html += '<td class="text-right">' + datas[i].USAGE_HANDHELD + '</td>';

            html += genCLD_TimeCell(date, datas[i].EMP_CODE);

            html += '</tr>';
            //console.log(html);
        }
    }
    return html;
}


var genCLD_TimeCell = function (date, sv) {
    var html = '';
    for (var i = 0; i < CLD_Time_Cols.length - 1; i++) {

        var from = (CLD_Time_Cols[i].getHours() * WFD02620_TIME_CONST.HOUR_TO_MINUTE) + CLD_Time_Cols[i].getMinutes();
        var to = (CLD_Time_Cols[i + 1].getHours() * WFD02620_TIME_CONST.HOUR_TO_MINUTE) + CLD_Time_Cols[i + 1].getMinutes();
        var sub = (to - from) / 4;
        var svData = '';
        if (sv != '') svData = 'data-emp-code="' + sv + '"';

        var dateTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), CLD_Time_Cols[i].getHours(), CLD_Time_Cols[i].getMinutes(), 0, 0);

        var time1 = dateTime;
        var time2 = new Date(time1.getTime() + sub * WFD02620_TIME_CONST.MINUTE_TO_MILLISECOND);
        var time3 = new Date(time2.getTime() + sub * WFD02620_TIME_CONST.MINUTE_TO_MILLISECOND);
        var time4 = new Date(time3.getTime() + sub * WFD02620_TIME_CONST.MINUTE_TO_MILLISECOND);
        var time5 = new Date(time4.getTime() + sub * WFD02620_TIME_CONST.MINUTE_TO_MILLISECOND);

        html += '<td class="time-td" >'
        //if (!isHoliday(date) && !isBreakTime(CLD_Time_Cols[i], CLD_Time_Cols[i+1])) {

                + '<div class="time-cell-row">'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time1, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time2, "ddMMyyyy-HHmm") + '" ></div>'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time2, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time3, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time3, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time4, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time4, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time5, "ddMMyyyy-HHmm") + '"  ></div>'
                + '</div>'
                + '<div class="time-cell-row">'
                    + '<div class="time-cell-col process-cell" ' + sv + ' data-type="process-cell" data-from="' + $.format.date(time1, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time2, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col process-cell" ' + sv + ' data-type="process-cell" data-from="' + $.format.date(time2, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time3, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col process-cell" ' + sv + ' data-type="process-cell" data-from="' + $.format.date(time3, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time4, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col process-cell" ' + sv + ' data-type="process-cell" data-from="' + $.format.date(time4, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time5, "ddMMyyyy-HHmm") + '"  ></div>'
                + '</div>'
                + '<div class="time-cell-row">'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time1, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time2, "ddMMyyyy-HHmm") + '" ></div>'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time2, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time3, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time3, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time4, "ddMMyyyy-HHmm") + '"  ></div>'
                    + '<div class="time-cell-col" ' + svData + ' data-type="time-bg-cell" data-from="' + $.format.date(time4, "ddMMyyyy-HHmm") + '" data-to="' + $.format.date(time5, "ddMMyyyy-HHmm") + '"  ></div>'
                + '</div>'

        //}
        + '</td>';
    }
    return html;
}


// Binding data to calendar ui
var bindingCalendarData_WFD02620_Assignment = function () {

    bindingBreakTime();
    bindingSVDatas();
    bindingTimeWorking();
}
var bindingBreakTime = function () {

    $('div.time-cell-col').each(function () {

        var from = $(this).attr('data-from').getDateTimeFromStr();
        var to = $(this).attr('data-to').getDateTimeFromStr();

        if (isBreakTime(from, to)) {

            $(this).addClass('WFD02620-BreakTimeBG');
            $(this).closest("td").addClass('WFD02620-BreakTimeBG');
        }

    });

}
var bindingSVDatas = function () {
    for (var i = 0; i < WFD02620_Assignment_Models.SVDatas.length; i++) {

        $('td[data-type="svName"').each(function () {

            var date = $(this).attr('data-date').getDateFromStr();
            var empCode = $(this).attr('data-emp-code');

            if (WFD02620_Assignment_Models.SVDatas[i].DATE_FROM != null && WFD02620_Assignment_Models.SVDatas[i].DATE_TO != null
                 && IsSVWorkingInTargetDate(WFD02620_Assignment_Models.SVDatas[i].DATE_FROM, WFD02620_Assignment_Models.SVDatas[i].DATE_TO, date)
                 && empCode == WFD02620_Assignment_Models.SVDatas[i].EMP_CODE) {

                $(this).html(WFD02620_Assignment_Models.SVDatas[i].EMP_TITLE + ' '
                    + WFD02620_Assignment_Models.SVDatas[i].EMP_NAME + ' '
                    + WFD02620_Assignment_Models.SVDatas[i].EMP_LASTNAME);

            }
        });
    }
}
var bindingTimeWorking = function () {

    for (var i = 0; i < WFD02620_Assignment_Models.SVDatas.length; i++) {

        if (WFD02620_Assignment_Models.SVDatas[i].DATE_FROM != null
            && WFD02620_Assignment_Models.SVDatas[i].DATE_TO != null
            && WFD02620_Assignment_Models.SVDatas[i].TIME_START != null
            && WFD02620_Assignment_Models.SVDatas[i].TIME_END != null) {

            var dateFrom = new Date(WFD02620_Assignment_Models.SVDatas[i].DATE_FROM);
            var dateTo = new Date(WFD02620_Assignment_Models.SVDatas[i].DATE_TO);
            var timeStart = WFD02620_Assignment_Models.SVDatas[i].TIME_START.getTimeFromStr();
            var timeEnd = WFD02620_Assignment_Models.SVDatas[i].TIME_END.getTimeFromStr();

            // Set time to date
            dateFrom.setHours(timeStart.getHours(), timeStart.getMinutes(), 0, 0);
            dateTo.setHours(timeEnd.getHours(), timeEnd.getMinutes(), 0, 0);


            $('tr[data-row-emp="' + WFD02620_Assignment_Models.SVDatas[i].EMP_CODE + '"]').each(function () {



                $(this).find('div.process-cell').each(function () {

                    var from = $(this).attr('data-from').getDateTimeFromStr();
                    var to = $(this).attr('data-to').getDateTimeFromStr();

                    from.setHours(from.getHours(), from.getMinutes(), 0, 0);
                    to.setHours(to.getHours(), to.getMinutes(), 0, 0);

                    // Check this time period is on working time lines
                    if (((dateFrom.getTime() <= from.getTime() && dateTo.getTime() >= to.getTime())
                            || (dateFrom.getTime() <= from.getTime() && dateTo.getTime() <= to.getTime() && dateTo.getTime() > from.getTime())
                            || (dateFrom.getTime() >= from.getTime() && dateTo.getTime() <= to.getTime())
                            || (dateFrom.getTime() >= from.getTime() && dateFrom.getTime() < to.getTime() && dateTo.getTime() >= to.getTime())
                        ) && !isBreakTime(from, to)) {

                        // add working time class (change bg color of div)
                        $(this).addClass('WorkingTimePeriod');

                        // add boostrap tooltip
                        $(this).attr('data-toggle', 'tooltip');
                        $(this).attr('data-html', 'true');
                        $(this).attr('title'
                            , WFD02620_Assignment_Models.SVDatas[i].EMP_TITLE
                                + ' ' + WFD02620_Assignment_Models.SVDatas[i].EMP_NAME
                                + ' ' + WFD02620_Assignment_Models.SVDatas[i].EMP_LASTNAME
                                + '<br/>' + $.format.date(dateFrom, "dd MMM yyyy HH:mm") + ' - '
                                + ' ' + $.format.date(dateTo, "dd MMM yyyy HH:mm")
                                + '<br/> Handheld usage: ' + WFD02620_Assignment_Models.SVDatas[i].USAGE_HANDHELD);
                    }
                });
            });
        }
    }

    setHandheld_IntersectionUsage_ScreenUI();
}

// Check ว่า sv คนนี้ทำงานในวันที่กำหนดหรือไม่
// dateFrom, dateTo, targetDate : Date
var IsSVWorkingInTargetDate = function (dateFrom, dateTo, targetDate) {

    var f = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate());
    var t = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
    var g = new Date(targetDate.getFullYear(), targetDate.getMonth(), targetDate.getDate());

    if ((f <= g) && (t >= g)) {
        return true;
    } else {
        return false;
    }
}
// Check ว่า SV คนนี้ได้ทำงานในช่วงเวลที่กำหนดหรือไม่
// timeStart : string, timeEnd : string, from : number, to : number
var IsSVWorkingInTargetTimeline = function (timeStart, timeEnd, from, to) {

    var s = (ToNumber(timeStart.split(':')[0]) * WFD02620_TIME_CONST.HOUR_TO_MINUTE) + ToNumber(timeStart.split(':')[1]);
    var e = (ToNumber(timeEnd.split(':')[0]) * WFD02620_TIME_CONST.HOUR_TO_MINUTE) + ToNumber(timeEnd.split(':')[1]);

    if (s <= from && e >= to) {
        return true;
    } else {
        return false;
    }
}

// Initial data for calendar
var setCalendarCLDTime_WFD02620_Assignment = function () {
    try {

        var breakTimes = [];
        var starts = [];
        var ends = [];

        if (WFD02620_Assignment_Models.MasterSettings != null && WFD02620_Assignment_Models.MasterSettings.BREAK_TIME != null) {
            breakTimes = WFD02620_Assignment_Models.MasterSettings.BREAK_TIME.split('-'); // result example : ['11:31','12:30']
        } else {
            breakTimes = null;
        }

        if (WFD02620_Assignment_Models.MasterSettings != null && WFD02620_Assignment_Models.MasterSettings.START_HRS != null) {
            starts = WFD02620_Assignment_Models.MasterSettings.START_HRS.split(':'); // result example : ['07','30']
        } else {
            starts = null;
        }

        if (WFD02620_Assignment_Models.MasterSettings != null && WFD02620_Assignment_Models.MasterSettings.END_HRS != null) {
            ends = WFD02620_Assignment_Models.MasterSettings.END_HRS.split(':'); // result example : ['16','30']
        } else {
            ends = null;
        }

        if (breakTimes != null) {

            CLD_Time.BreakTimeStart.setHours(parseInt(breakTimes[0].split(':')[0]));
            CLD_Time.BreakTimeStart.setMinutes(parseInt(breakTimes[0].split(':')[1]));
            CLD_Time.BreakTimeStart.setSeconds(0);

            CLD_Time.BreakTimeEnd.setHours(parseInt(breakTimes[1].split(':')[0]));
            CLD_Time.BreakTimeEnd.setMinutes(parseInt(breakTimes[1].split(':')[1]));
            CLD_Time.BreakTimeEnd.setSeconds(0);

        } else {
            CLD_Time.BreakTimeStart.setHours(12);
            CLD_Time.BreakTimeStart.setMinutes(0);
            CLD_Time.BreakTimeStart.setSeconds(0);

            CLD_Time.BreakTimeEnd.setHours(13);
            CLD_Time.BreakTimeEnd.setMinutes(0);
            CLD_Time.BreakTimeEnd.setSeconds(0);
        }

        if (starts != null) {
            CLD_Time.Start.setHours(starts[0]);
            CLD_Time.Start.setMinutes(starts[1]);
            CLD_Time.Start.setSeconds(0);
        } else {
            CLD_Time.Start.setHours(0);
            CLD_Time.Start.setMinutes(0);
            CLD_Time.Start.setSeconds(0);
        }

        if (ends != null) {
            CLD_Time.End.setHours(ends[0]);
            CLD_Time.End.setMinutes(ends[1]);
            CLD_Time.End.setSeconds(0);
        } else {
            CLD_Time.End.setHours(0);
            CLD_Time.End.setMinutes(0);
            CLD_Time.End.setSeconds(0);
        }
    } catch (err) {

        AlertTextErrorMessage(err);

    }
}
var setCalendarCLD_Dates_WFD02620_Assignment = function () {
    var from = new Date(WFD02620_Assignment_Models.HData.TARGET_DATE_FROM.getTime());
    var to = new Date(WFD02620_Assignment_Models.HData.TARGET_DATE_TO.getTime());
    to.setDate(to.getDate() + 1);
    CLD_Dates = [];
    for (; from < to ; from.setDate(from.getDate() + 1)) {
        var data = new Date(from.getTime());
        data.setHours(0, 0, 0, 0);
        CLD_Dates.push(data);
    }
}
var setCalendarCLD_Time_Cols_WFD02620_Assignment = function () {

    var from = new Date(CLD_Time.Start.getTime());
    var to = new Date(CLD_Time.End.getTime());

    CLD_Time_Cols = [];

    while (from.getTime() < to.getTime()) {
        var dt = new Date(from.getTime());
        CLD_Time_Cols.push(dt);

        from.addHours(1);
        if (from.getTime() > to.getTime()) { from = new Date(to.getTime()); }
    }
    var dt = new Date(from.getTime());
    CLD_Time_Cols.push(dt);
}
// Get sv in target date : user in [ genCalendarRowHtml_WFD02620_Assignment ]
var getSVInDay_WFD02620_Assignment = function (date) {
    var datas = [];

    for (var i = 0; i < WFD02620_Assignment_Models.SVDatas.length; i++) {

        if (WFD02620_Assignment_Models.SVDatas[i].DATE_FROM != null && WFD02620_Assignment_Models.SVDatas[i].DATE_TO != null) {

            WFD02620_Assignment_Models.SVDatas[i].DATE_FROM.setHours(0, 0, 0, 0);
            WFD02620_Assignment_Models.SVDatas[i].DATE_TO.setHours(0, 0, 0, 0);

            if (WFD02620_Assignment_Models.SVDatas[i].DATE_FROM <= date && WFD02620_Assignment_Models.SVDatas[i].DATE_TO >= date) {
                datas.push(WFD02620_Assignment_Models.SVDatas[i]);
            }
        }
    }
    return datas;
}
// Check date is holiday : use in [ calWorkingTime_WFD02620_Assignment, genCalendarRowHtml_WFD02620_Assignment ]
var isHoliday = function (date) {
    var isHo = false;
    if (WFD02620_Assignment_Models != null && WFD02620_Assignment_Models.Holidays != null && WFD02620_Assignment_Models.Holidays.length > 0) {
        for (var i = 0; i < WFD02620_Assignment_Models.Holidays.length; i++) {
            //if ($.format.date(WFD02620_Assignment_Models.Holidays[i].HOLIDATE_DATE, "dd MMM yyyy") == $.format.date(date, "dd MMM yyyy")) {
            //    isHo = true;
            //    break;
            //}

            if (WFD02620_Assignment_Models.Holidays[i].HOLIDATE_DATE.EqualDate(date)) {
                isHo = true;
                break;
            }
        }
    }
    return isHo;
}
// Check time is in break time
var isBreakTime = function (startDate, endDate) {
    if (startDate.getTimeMinute_WFD02620() >= CLD_Time.BreakTimeStart.getTimeMinute_WFD02620()
        && endDate.getTimeMinute_WFD02620() <= CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620()) {
        return true;
    } else {
        return false;
    }
}



var isFirstOrEndInBreakTime = function (startDate, endDate) {
    if ((startDate.getTimeMinute_WFD02620() >= CLD_Time.BreakTimeStart.getTimeMinute_WFD02620()
            && (startDate.getTimeMinute_WFD02620() <= CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620()))
        || (endDate.getTimeMinute_WFD02620() >= CLD_Time.BreakTimeStart.getTimeMinute_WFD02620()
            && endDate.getTimeMinute_WFD02620() <= CLD_Time.BreakTimeEnd.getTimeMinute_WFD02620())) {
        return true;
    } else {
        return false;
    }
}

//#endregion


//#region   Common function


//Date.prototype.addMinutes = function (minutes) {
//    return new Date(date.getTime() + minutes * 60000);
//}
var addMinutes = function (date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
}
var getDate = function (date) {
    var d = new Date(date.getTime());
    d.setHours(0, 0, 0, 0);
    return d;
}
var setTime = function (date, otherDate) {
    var d = new Date(date.getTime());
    d.setHours(otherDate.getHours(), otherDate.getMinutes(), 0, 0);
    return d;
}

Date.prototype.getTimeMinute_WFD02620 = function () {
    var val = this.getHours() * 60;
    val += this.getMinutes();
    return val;
}
function timeStr(n) {
    return n > 9 ? "" + n : "0" + n;
}

String.prototype.getDateTimeFromStr = function () {
    var dt = new Date();

    dt.setDate($(this)[0] + $(this)[1]);
    dt.setMonth(ToNumber(($(this)[2] + $(this)[3])) - 1);
    dt.setYear($(this)[4] + $(this)[5] + $(this)[6] + $(this)[7]);

    dt.setHours(($(this)[9] + $(this)[10]));
    dt.setMinutes(($(this)[11] + $(this)[12]));
    dt.setSeconds(0);
    return dt;
}

String.prototype.getDateFromStr = function () {
    var dt = new Date((($(this)[4] + $(this)[5] + $(this)[6] + ToNumber($(this)[7])))
        , (ToNumber($(this)[2] + $(this)[3]) - 1)
        , (ToNumber($(this)[0] + $(this)[1])), 0, 0, 0, 0);

    return dt;
}



/* Gets the current value
 * @return Date The result or null if no date is present
 * @throws If the entered value is invalid
 */
function getDatePickerDate(datePicker) {
    datePicker = $(datePicker);

    var format = datePicker.datepicker("option", "dateFormat"),
        text = datePicker.val(),
        settings = datePicker.datepicker("option", "settings");

    return new Date($.datepicker.parseDate(format, text, settings));
}

function CloneJsonArrObj(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

var convertCSDate = function (d) {
    return new Date(parseInt(d.substr(6)));
}

var onWFD02620Cancel = function () {
    ConfigWorkingTimeDialogControls.PERIOD.periodpicker('destroy');
    WFD02620_AssignmentControls.SetWorkingTimeDialog.appendTo("body").modal('hide');
}
//#endregion