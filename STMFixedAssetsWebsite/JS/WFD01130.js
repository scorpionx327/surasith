﻿var Controls = {
    ASSET_NO: $('#ASSET_NO'),
    ASSET_NAME: $('#ASSET_NAME'),
    TRANSACTION_TYPE: $('#TRANSACTION_TYPE'),
    DATE_SERVICE_FROM: $('#DATE_SERVICE_FROM'),
    DATE_SERVICE_TO: $('#DATE_SERVICE_TO'),
    DOCUMENT_NO: $('#DOCUMENT_NO'),
    STATUS: $('#STATUS'),
    SearchButton: $("#WFD01130Search"),
    ClearButton: $('#WFD01130Clear'),
    Pagin: $('#table').Pagin(URL_CONST.ITEM_PER_PAGE),
    SearchResultTable: $('#table'),
    SearchDataRow: $('#SearchDataRow'),
    WFD01130btnCOST_CODE: $('#WFD01130BtnASSET_NO'),
    SearchAssetModal: $('#WFD02190_SearchAssets')
};

$(function () {
    var regex = new RegExp("^[A-Za-z0-9\-]{0,15}$");

    Controls.ASSET_NO.bind("keypress", function (event) {
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    $('#ASSET_SUB_NO').focusout(function () {
        //do something
        var d = {
            COMPANY: $('#cbb_company').val(),
            ASSET_NO: Controls.ASSET_NO.val(),
            ASSET_SUB: $('#ASSET_SUB_NO').val()
        }
        ajax_method.Post(URL_CONST.GET_ASSET_NAME, d, true
    , function (result) {
        if (result != null) {
            Controls.ASSET_NAME.val(result.ASSET_NAME);
        } else {
            Controls.ASSET_NAME.val('');
        }


    }, null);

    })
    var regexDoc = new RegExp("^[A-Za-z0-9\/]{0,10}$");
    Controls.DOCUMENT_NO.bind("keypress", function (event) {
        ClearMessageC();
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regexDoc.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    Controls.SearchButton.click(WFD01130OnSearchBtnClick);
    Controls.ClearButton.click(Clear);
    Controls.SearchDataRow.hide();
    Controls.WFD01130btnCOST_CODE.click(WFD01130getCostCenterModal);

    $('#ASSET_NO, #DOCUMENT_NO').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            //console.log(this.id);
            Controls.SearchButton.click();
            return false;
        }
    });

});

var WFD01130getDefaultSceenValue = function () {
    return {
        COMPANY: getMultiControlValue($('#cbb_company')),
        ASSET_SUB: $('#ASSET_SUB_NO').val(),
        ASSET_NO: Controls.ASSET_NO.val(),
        TRANSACTION_TYPE: Controls.TRANSACTION_TYPE.val(),
        DATE_SERVICE_FROM: Controls.DATE_SERVICE_FROM.val(),
        DATE_SERVICE_TO: Controls.DATE_SERVICE_TO.val(),
        DOCUMENT_NO: Controls.DOCUMENT_NO.val(),
        STATUS: Controls.STATUS.val()
    }
}
var Search = function () {
    ClearMessageC();
    var con = WFD01130getDefaultSceenValue();
    var pagin = Controls.Pagin.GetPaginData();

    ajax_method.SearchPost(URL_CONST.SEARCH, con, pagin, true, function (datas, pagin) {
        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';
  

            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }

            Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                  { data: 'COMPANY', name: 'COMPANY', "className": "text-center" },
                  { data: 'TRAN_TYPE', name: 'TRAN_TYPE' },
                  { data: 'TRANS_DATE', name: 'TRANS_DATE', "className": "text-center" },
                  { data: 'DETAIL', name: 'DETAIL' },
                  { data: 'LATEST_APPROVE_NAME', name: 'LATEST_APPROVE_NAME' },
                  { data: 'POSTING_DATE', name: 'POSTING_DATE', "className": "text-center" },
                  { data: null, name: 'DOC_NO' },//DOC_NO
                ],
                'columnDefs': [
                    {
                        'targets': 6,
                        'searchable': false,
                        'orderable': true,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return ' <button class="btn btn-sm btn-info docnobtn" data-mode="VIEW" data-request_type="' + data.REQUEST_TYPE + '" data-doc_no="' + data.DOC_NO + '" type="button" onclick="DocOpen(this)"><i class="fa fa-list-alt"></i>' + data.DOC_NO + '</button>';
                        }
                    }
                ],
                'order': [],
                //"paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false,
                fixedHeader: true
            });
            var page = Controls.SearchResultTable.Pagin();
            page.Init(pagin, sorting, changePage, 'itemPerPage');

            //Controls.Pagin.Init(pagin, sorting, changePage, 'itemPerPage');
            Controls.SearchDataRow.show();
        } else {
            Controls.SearchDataRow.hide();
            Controls.Pagin.Clear();
        }

    }, null);
}

var WFD01130OnSearchBtnClick = function () {
    Controls.Pagin.ClearPaginData();
    Search();
}

var Clear = function () {
    $('#ASSET_NO').val('');
    $('#ASSET_SUB_NO').val('');
    $('#ASSET_NAME').val('');
    $('#TRANSACTION_TYPE').select2("val", "");
    $('#DATE_SERVICE_FROM').val('');
    $('#DATE_SERVICE_TO').val('');
    $('#DOCUMENT_NO').val('');
    $('#STATUS').select2("val", "");
    Controls.SearchDataRow.hide();
    ClearMessageC();
}

var sorting = function (PaginData) {
    console.log(PaginData);
    
    Search();
}
var changePage = function (PaginData) {
    console.log(PaginData);
    Search();
}
function DocOpen(identifier) {
    var DOC_NO = $(identifier).data('doc_no');
    var MODE = $(identifier).data('mode');
    var REQUEST_TYPE = $(identifier).data('request_type');
    window.open(URL_CONST.DOC_OPEN + "?DocNo=" + DOC_NO + "&MODE=" + MODE + "&RequestType=" + REQUEST_TYPE , "Document_2017");
}
var fAction = {
    AddAsset: function (_list, callback) { //It's call from dialog

        //console.log(Url.AddAsset);

        if (_list == null || _list.length == 0)
            return;

        var _data = _list[0];
        $('#ASSET_NO').val(_data.ASSET_NO);
        $('#ASSET_SUB_NO').val(_data.ASSET_SUB);
        $('#ASSET_NAME').val(_data.ASSET_NAME);

        if (callback == null)
            return;

        callback();
       
    },
    
}

var WFD01130getCostCenterModal = function () {
    var company = $('#cbb_company').val();

    if (!company) {
        AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format('', 'Company'), "#AlertMessageArea");
        return false;
    }

    //Controls.ASSET_NO.addClass('pressedAssetNoModal');
    //Controls.ASSET_NAME.addClass('pressedAssetNameModal');


    //getCompanyFromAnyPage($('#cbb_company').val());
    //Controls.SearchAssetModal.appendTo("body").modal('show');
    f2190SearchAction.ClearDefault();

    Search2190Config.RoleMode = 'FAW'

    Search2190Config.searchOption = "HS";
    Search2190Config.AllowMultiple = false;

    Search2190Config.Default.Company = { Value: company, Enable: false };
    //Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
    //Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedRespCostCenter, Enable: fAction.SelectedRespCostCenter == "" };
    //Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: true };
    //Search2190Config.Default.Parent = { Value: "Y", Enable: false };

    //Assign Default and Initial screen
    f2190SearchAction.Initialize();

    $('#WFD02190_SearchAssets').modal();

}

var getMultiControlValue = function (id) {
    var _val = '';
    if (id.val() != null) {
        _val = (id.val() + '');
        _val = _val.replace(/,/g, '#');
    }
    return _val
}
