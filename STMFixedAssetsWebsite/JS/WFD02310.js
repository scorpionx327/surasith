﻿var control = {
    DataTable: null,
    Company: $('#WFD01170ddpCompany'), // Temp , still request best solution.
}
$(function () {
    $('#divSearchRequestResult').hide();
});
var fAction = {
    SelectedCostCenter : "",
    SelectedRespCostCenter : "",
    //Should define all request screen.
    Initialize: function (callback) {
        if (callback != null)
            callback();
    },
    LoadData: function (callback) {
        //console.log("LoadData is callled");
        //Do here
        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        //console.log(GLOBAL);
        //console.log(Url);
        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
         //   console.log(datas);
            fAction.SelectedCostCenter = ""; //Clear Selected
            fAction.SelectedRespCostCenter = ""; //Clear Selected
            
            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                table.clear().draw().destroy();
            }

            
            if (datas == null || datas.length == 0) {
                $('#divSearchRequestResult').hide();

                if (callback != null)
                    callback();

                return;
            }

            fAction.SelectedCostCenter = datas[0].COST_CODE;
            fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;
            

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            // To be discussion

            control.DataTable = $('#tbSearchResult').DataTable({
                data: datas,
                "columns": [
                    { "data": "ROW_INDX", 'orderable': false, "class": "text-center" }, // 0
                    { "data": null }, // Asset No : hyper link 1
                    { "data": "ASSET_SUB", 'orderable': false,"class": "text-center" }, // 2
                    { "data": "ASSET_NAME", 'orderable': false }, // 3
                    { "data": null }, // Asset class : hyper link 4
                    { "data": null }, // 5
                    { "data": null }, // 6
                    { "data": "PRINT_COUNT", 'orderable': false,"class": "text-right" }, // 7
                    { "data": null } // 8
                ],
                'columnDefs': [
                    {
                        'targets': 1, //ASSET_NO Show hyperlink
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.HyperLinkRender(full);
                        }
                    },//
                    {
                        'targets': 4,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            if (data.ASSET_CLASS == null)
                                return '';

                            return '<span data-toggle="tooltip" title="' + data.ASSET_CLASS_HINT + '">' + data.ASSET_CLASS + '</span>';
                        }
                    },
                    {
                        'targets': 5,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CheckBoxDamageRender(data);
                        }
                    },
                    {
                        'targets': 6,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CheckBoxLossRender(data);
                        }
                    },
                    {
                        'targets': 8, //Delete
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DeleteButtonRender(full);
                        }

                    }],
                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
               
                fixedHeader: {
                    header: true,
                    footer: false
                },
                "initComplete": function (settings, json) {
                    console.log('DataTables has finished its initialisation.');
                },
                "createdRow": function (row, data, index) {
                    console.log("createdRow");
                }
            }).columns.adjust().draw();

           // jQuery('.dataTable').wrap('<div class="dataTables_scroll" />');

            //control.DataTable.columns.adjust().draw();
            //Incase New Mode
            if (GLOBAL.DOC_NO == '') {
                if (datas.length == 0) {
                    fScreenMode.Submit_InitialMode(function () {
                        fMainScreenMode.Submit_InitialMode();
                    });
                }
                if (datas.length > 0) {
                    fScreenMode.Submit_AddAssetsMode(function () {
                        fMainScreenMode.Submit_AddAssetsMode();
                    });
                }
               
            }

            
            $('#divSearchRequestResult').show();

            
            if (callback == null)
                return;

            callback();

        }, null);


    },
    
    Clear: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }
            $('#WFD02310_LossSelectAll').prop('checked', false);
            $('#WFD02310_DamageSelectAll').prop('checked', false);
            console.log("Clear");
            fAction.LoadData(function () {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            });
            //  SuccessNotification(CommonMessage.ClearSuccess);

            if (callback == null)
                return;

            callback();
        });
    },
    Export: function (callback) {

        fAction.UpdateAsset(function () {
            var batch = BatchProcess({
                BatchId: "LFD02330",
                Description: "Export Reprint Request",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'GUID', GLOBAL.GUID);
            batch.Addparam(2, 'DOC_NO', GLOBAL.DOC_NO);
            batch.StartBatch();

            if (callback == null)
                return;

            callback();
        });
    },
    AddAsset: function (_list, callback) { //It's call from dialog

        //console.log(Url.AddAsset);

        if (_list == null || _list.length == 0)
            return;

        _list.forEach(function (_data) {
            // do something with `item`
            _data.GUID = GLOBAL.GUID;
            _data.USER_BY = GLOBAL.USER_BY;
        });
      

        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.AddAsset, _list, false, function (result) {
            console.log(result);
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }
            
            fAction.LoadData();

            if (callback == null)
                return;

            callback();

        });
    },
    UpdateAsset:function(callback){
        var _data = fAction.GetClientAssetList();
        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            if (result.IsError) {
                //fnErrorDialog("UpdateAssetList", result.Message);
                return;
            }
            if (callback == null)
                return;


            callback();

        }); //ajax_method.UpdateAssetList

    },
    DeleteAsset: function (_AssetNo, _AssetSub) {
        //console.log(_AssetNo);
        //console.log(Url.DeleteAsset);
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)

        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: control.Company.val(),
            ASSET_NO: _AssetNo,
            ASSET_SUB: _AssetSub
        };

        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(_AssetNo, _AssetSub), function (result) {
            if (!result) {
                return;
            }
            fAction.UpdateAsset(function () {
                ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
                    if (result.IsError) {
                        return;
                    }
                    console.log("Delete");
                    fAction.LoadData();
                    // SuccessNotification(CommonMessage.ClearSuccess);
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Deletion process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                });
            }); // UpdateAsset 
        }); // loadConfirmAlert
    },
    UploadExcel: function (_fileName) {
        var batch = BatchProcess({
            BatchId: "BFD02AAA",
            Description: "Upload AAA Batch",
            UserId: GLOBAL.USER_BY
        });
        batch.Addparam(1, 'Company', GLOBAL.COMPANY);
        batch.Addparam(2, 'GUID', GLOBAL.GUID);
        batch.Addparam(3, 'UploadFileName', _fileName);
        batch.Addparam(4, 'User', GLOBAL.USER_BY);
        batch.StartBatch(function () {
            fAction.LoadData();
        });
    },
    PrepareGenerateFlow: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        
        fAction.UpdateAsset(function(){
            // GLOBAL.FLOW_TYPE = 'MN';
            ajax_method.Post(Url.PrepareFlow, { data: GLOBAL}, false, function (result) {
                if (result.IsError) {
                    //fnErrorDialog("PrepareFlow", result.Message);
                    return;
                }

                if (callback == null)
                    return;

                callback();
            }); //ajax_method.PrepareFlow
        }); //ajax_method.UpdateAssetList

        
    },

    PrepareSubmit: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlow
    },
    PrepareApprove: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlows
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },
    EnableMassMode: function () {

    },
    ShowSearchPopup: function (callback) {
        //Set search parameter

        f2190SearchAction.ClearDefault();

        Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();

        Search2190Config.searchOption = "P"; //blank for reprint
        Search2190Config.AllowMultiple = true;

        Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
        Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
        Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedRespCostCenter, Enable: fAction.SelectedRespCostCenter == "" };
        // Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: false };

        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();
    },
    
    GetClientAssetList: function () {
        console.log("GetClientAssetList");
        if (control.DataTable == null){ 
            console.log("NULL");
            return;
        }
        var _assetList = []; //array List
      
        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "")
            {
                return ;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }
       
            _row = control.DataTable.row(this).data(); // Get source data

            _assetList.push({
                GUID: GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                COMPANY: _row.COMPANY,
                ASSET_NO: _row.ASSET_NO,
                ASSET_SUB: _row.ASSET_SUB,

                DAMAGE_FLAG: $(this).find('input[class="DAMAGE"]:checked').length > 0 ? 'Y' : 'N',
                LOSS_FLAG: $(this).find('input[class="LOSS"]:checked').length > 0 ? 'Y': 'N',
              
            });
            
        });
      
        return _assetList;
    },
    DetailLossCheck: function (sender) {

        var row = $(sender).closest("tr");

        row.find('[class="DAMAGE"]').prop("checked", false);

        $('#WFD02310_LossSelectAll').prop('checked', _act = $('.LOSS:checked').length == $('.LOSS').length);
        $('#WFD02310_DamageSelectAll').prop('checked', _act = $('.DAMAGE:checked').length == $('.DAMAGE').length);


    },
    DetailDamageCheck: function (sender) {
        var row = $(sender).closest("tr");
        row.find('[class="LOSS"]').prop("checked", false);

        $('#WFD02310_LossSelectAll').prop('checked', _act = $('.LOSS:checked').length == $('.LOSS').length);
        $('#WFD02310_DamageSelectAll').prop('checked', _act = $('.DAMAGE:checked').length == $('.DAMAGE').length);
    }
}

//Call this function when PG set screen mode
var fScreenMode = {

    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", true);

        //Delete button should exists this function, It's maintain by user permission and data status
        $('.DAMAGE, .LOSS').prop('disabled', true);
        $('#WFD02310_DamageSelectAll, #WFD02310_LossSelectAll').prop('disabled', true);
        if (GLOBAL.STATUS == '00') //Submit mode only
        {
            $('button.DAMAGE, button.LOSS').prop("disabled", true);

            
        }
    },

    Submit_InitialMode: function (callback) {

        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd').prop("disabled", false);


       // $('.DAMAGE, .LOSS, .DELETE').prop('disabled', false);
        $('#WFD02310_DamageSelectAll, #WFD02310_LossSelectAll').prop('disabled', false);

        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').removeClass('disabled').prop("disabled", false);


        $('.DAMAGE, .LOSS, button.delete').prop('disabled', false);
        $('#WFD02310_DamageSelectAll, #WFD02310_LossSelectAll').prop('disabled', false);

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();

        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').removeClass('disabled').prop("disabled", false);

        $('button.delete').prop('disabled', false);
        
        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) { //Should be call after data is loaded

        fScreenMode._ClearMode();


        $('#btnRequestExport').show();
        $('#btnRequestExport').prop("disabled", false);

        console.log("#Approve_Mode");
        // No check status for this request
        $('#WFD02310_DamageSelectAll, #WFD02310_LossSelectAll').prop('disabled', true).hide();
        
        //delete base on permission
        
        if (callback == null)
            return;
        callback();
    },
    _View_Mode: function () {

        

    }
}

var fRender = {
    CheckBoxDamageRender: function (data) {
        var _strHtml = '';
        var _strDisable = (data.AllowEdit ? '' : "disabled");
       
        var _strChecked = (data.DAMAGE_FLAG == "Y" ? 'checked' : '') ;
        
        _strHtml = '<input style="width:20px; height:28px;" class="DAMAGE" onclick="fAction.DetailDamageCheck(this);" type="checkbox"' +  _strChecked + ' '+_strDisable+' >';
        return _strHtml;
    },
    HyperLinkRender: function (data) {
      //  console.log("Load Detail");
        if (data.ASSET_NO == null)
            return '';
        return '<a href="../WFD02130/Index?COMPANY='+data.COMPANY+'&ASSET_NO='+data.ASSET_NO+'&ASSET_SUB='+data.ASSET_SUB+'" target="_blank">' + data.ASSET_NO + '</a>';
    },
    CheckBoxLossRender: function (data) {
        var _strHtml = '';
        var _strDisable = (data.AllowEdit ? '' : "disabled");
        
       
        var _strChecked = (data.LOSS_FLAG == "Y" ? 'checked' : '');
        _strHtml = '<input style="width:20px; height:28px;" class="LOSS" onclick="fAction.DetailLossCheck(this);" type="checkbox"' + _strChecked + ' '+_strDisable+' >';
        return _strHtml;
    },
   

   
    DeleteButtonRender: function (data) {
        var _hide = (data.AllowDelete ? '' : "hide");
        var _disbled = (PERMISSION.AllowEditBeforeGenFlow ? '' : "disabled");

        var _strHtml = '';
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="btnDelete_"' +
            data.ASSET_NO + ' class="' + _hide + ' btn btn-danger btn-xs DELETE" data-title="Delete" type="button" ' + _disbled + ' ' +
                 'onclick=fAction.DeleteAsset(\'' +
                 data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\')>' +
                 '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },
}



$('#WFD02310_DamageSelectAll').click(function (e) {

    $('.DAMAGE').prop('checked', $(this).is(':checked'));
    
    if ($(this).is(':checked')) {
        $('.LOSS, #WFD02310_LossSelectAll').prop('checked', false);
    }

});
$('#WFD02310_LossSelectAll').click(function (e) {
    $('.LOSS').prop('checked', $(this).is(':checked'));

    if ($(this).is(':checked')) {
        $('.DAMAGE, #WFD02310_DamageSelectAll').prop('checked', false);
    }

});