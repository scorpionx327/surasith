﻿var isFirefox = typeof InstallTrigger !== 'undefined';

var numericOneToNine = "[1-9]";
var numeric = "[0-9]";
var numericSign = "[-0-9]";
var decimal = "[0-9.]";
var numericComma = "[0-9,]";
var decimalComma = "[0-9.,]";
var date = "[0-9/]";
var alpha = "[a-zA-Z]";
var alphaNumeric = "[0-9a-zA-Z]";
var alphaWildcard = "[a-zA-Z*]";
var alphaNumericWildcard = "[0-9a-zA-Z*]";
var alphaSpace = "[a-zA-Z ]";
var alphaNumericSpace = "[0-9a-zA-Z ]";
var alphaSpaceWildcard = "[a-zA-Z *]";
var alphaNumericSpaceWildcard = "[0-9a-zA-Z *]";
var alphaNumericSlashDashUnder = "[a-zA-Z0-9-_\/]";
var alphaNumericSlashDashUnderWildcard = "[a-zA-Z0-9-*_\/]";

function currencyFormat(num) {
    if (num != null) {
        return parseFloat(num).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } else {
        return '';
    }
}

function parseFloatWithComma(str) {
    if (str == '' || str == 'null' || str == null || str == undefined) {
        return 0;
    } else {
        return parseFloat(str.replace(/,/g, ''));
    }
}

/*******************************************************************************
 * get cursor position in specific a textbox/textarea
 ******************************************************************************/
/*******************************************************************************
 * function doGetCaretPosition (ctrl) { var CaretPos = 0; // IE Support
 * 
 * if (document.selection) { ctrl.focus (); var Sel =
 * document.selection.createRange (); Sel.moveStart ('character',
 * -ctrl.value.length); CaretPos = Sel.text.length; } // Firefox support else if
 * (ctrl.selectionStart || ctrl.selectionStart == '0') CaretPos =
 * ctrl.selectionStart; return (CaretPos); }
 ******************************************************************************/

function doGetCaretPosition(ctrl) {
    var el = $(ctrl).get(0);
    var pos = 0;
    if ('selectionStart' in el) {
        pos = el.selectionStart;
    } else if ('selection' in document) {
        el.focus();
        var Sel = document.selection.createRange();
        var SelLength = document.selection.createRange().text.length;
        Sel.moveStart('character', -el.value.length);
        pos = Sel.text.length - SelLength;
    }
    return pos;
}

function doGetSelectedLengthCaretPosition(ctrl) {
    var el = $(ctrl).get(0);
    var SelLength = 0;
    if ('selectionStart' in el) {
        sta = el.selectionStart;
        end = el.selectionEnd;
        SelLength = end - sta;
    } else if ('selection' in document) {
        el.focus();
        var Sel = document.selection.createRange();
        SelLength = document.selection.createRange().text.length;
    }
    return SelLength;
}
/*******************************************************************************
 * end get cursor position in specific a textbox/textarea
 ******************************************************************************/


function numbersWithDecimalOnlyOnKeyPress(obj, decpoint, e) {
    var valid = performFiltering(/[^\d\.]/g, e);
    if (valid) {
        if (!e)
            e = window.event;
        if (e.keyCode)
            code = e.keyCode;
        else if (e.which)
            code = e.which;

        if (obj.value != null) {
            if (obj.value.indexOf(".") > -1) { // word already contain .
                if (isFirefox) {
                    var c = e.charCode;// 46 = . 0 = delete
                    var str = String.fromCharCode(c);
                    if (str === '.') {
                        valid = false;
                    }
                } else {
                    if (code == 46) {// key-in .
                        valid = false;
                    }
                }
            } else {
                if (decpoint && decpoint.length && valid) {
                    var indexCursor = doGetCaretPosition(obj);
                    var objLen = obj.value.length;
                    if (isFirefox) {
                        var c = e.charCode;// 46 = . 0 = delete
                        var str = String.fromCharCode(c);
                        if (str === '.') {
                            var strAfterDot = obj.value.substr(indexCursor,
									objLen);
                            if (strAfterDot) {
                                objLen = strAfterDot.length;
                                if (objLen > decpoint.length) {
                                    valid = false;
                                }
                            }
                        }
                    } else {
                        if (code == 46) {// key-in .
                            var strAfterDot = obj.value.substr(indexCursor,
									objLen);
                            if (strAfterDot) {
                                objLen = strAfterDot.length;
                                if (objLen > decpoint.length) {
                                    valid = false;
                                }
                            }
                        }
                    }

                }
            }
        }

        if (decpoint && decpoint.length && valid) {
            var indexCursor = doGetCaretPosition(obj);
            var a = obj.value.split(".");
            if (a.length > 1) {
                decValue = a[1]; // decimal value
                if (decValue && decValue != '') {
                    if (decValue.length > decpoint.length) {
                        valid = false;
                    } else if (decValue.length == decpoint.length) {
                        var indexdot = obj.value.indexOf(".");
                        if ((code >= 48 && code <= 57)
								&& indexCursor > indexdot) {
                            if (doGetSelectedLengthCaretPosition(obj) == 0) {
                                valid = false;
                            }
                        }
                    }
                }
            }
        }
        if (valid == false) {
            e.keyCode = 0;
            if (isFirefox == false) {
                if (event)
                    (event.preventDefault) ? event.preventDefault()
							: event.returnValue = false;
            } else {
                e.preventDefault();
            }
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function numbersWithDecimalOnlyOnKeyPress(obj, decpoint, e) {
    var valid = performFiltering(/[^\d\.]/g, e);
    if (valid) {
        if (!e)
            e = window.event;
        if (e.keyCode)
            code = e.keyCode;
        else if (e.which)
            code = e.which;

        if (obj.value != null) {
            if (obj.value.indexOf(".") > -1) { // word already contain .
                if (isFirefox) {
                    var c = e.charCode;// 46 = . 0 = delete
                    var str = String.fromCharCode(c);
                    if (str === '.') {
                        valid = false;
                    }
                } else {
                    if (code == 46) {// key-in .
                        valid = false;
                    }
                }
            } else {
                if (decpoint && decpoint.length && valid) {
                    var indexCursor = doGetCaretPosition(obj);
                    var objLen = obj.value.length;
                    if (isFirefox) {
                        var c = e.charCode;// 46 = . 0 = delete
                        var str = String.fromCharCode(c);
                        if (str === '.') {
                            var strAfterDot = obj.value.substr(indexCursor,
									objLen);
                            if (strAfterDot) {
                                objLen = strAfterDot.length;
                                if (objLen > decpoint.length) {
                                    valid = false;
                                }
                            }
                        }
                    } else {
                        if (code == 46) {// key-in .
                            var strAfterDot = obj.value.substr(indexCursor,
									objLen);
                            if (strAfterDot) {
                                objLen = strAfterDot.length;
                                if (objLen > decpoint.length) {
                                    valid = false;
                                }
                            }
                        }
                    }

                }
            }
        }

        if (decpoint && decpoint.length && valid) {
            var indexCursor = doGetCaretPosition(obj);
            var a = obj.value.split(".");
            if (a.length > 1) {
                decValue = a[1]; // decimal value
                if (decValue && decValue != '') {
                    if (decValue.length > decpoint.length) {
                        valid = false;
                    } else if (decValue.length == decpoint.length) {
                        var indexdot = obj.value.indexOf(".");
                        if ((code >= 48 && code <= 57)
								&& indexCursor > indexdot) {
                            if (doGetSelectedLengthCaretPosition(obj) == 0) {
                                valid = false;
                            }
                        }
                    }
                }
            }
        }
        if (valid == false) {
            e.keyCode = 0;
            if (isFirefox == false) {
                if (event)
                    (event.preventDefault) ? event.preventDefault()
							: event.returnValue = false;
            } else {
                e.preventDefault();
            }
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function pasteValidate(restrictionType, e, decpoint) {
    if (!e)
        e = window.event;

    if (window.clipboardData && window.clipboardData.getData) { // IE
        pasteData = window.clipboardData.getData('Text');
    } else if (e.clipboardData && e.clipboardData.getData) {
        pasteData = e.clipboardData.getData('text/plain');
    }

    if (decimal == restrictionType) {
        var arrData = pasteData.split('.');

        if (arrData.length > 2) {
            if (isFirefox == false) {
                (event.preventDefault) ? event.preventDefault()
						: event.returnValue = false;
            } else {
                e.preventDefault();
            }
        } else if (arrData.length > 1) {
            if (decpoint && decpoint.length) {
                var decValue = arrData[1]; // decimal value
                if (decValue && decValue != '') {
                    if (decValue.length > decpoint.length) {
                        if (isFirefox == false) {
                            (event.preventDefault) ? event.preventDefault()
									: event.returnValue = false;
                        } else {
                            e.preventDefault();
                        }
                    }
                }
            }
        }
    }

    var modData = pasteData.replace(new RegExp("[\\s-]+", "g"), "");
    var stError = modData.replace(new RegExp("^" + restrictionType + "+", "g"),
			"");
    if (stError == "") {
        return;
    } else {
        if (isFirefox == false) {
            (event.preventDefault) ? event.preventDefault()
					: event.returnValue = false;
        } else {
            e.preventDefault();
        }
    }
}

function addNumberWithCommas(obj, decpoint) {
    var value = obj.value.replace(/[^-?\d^.]/g, '');
    var decValue;
    if (arguments.length != 1 && value != "") {
        value = value.toString();
        a = value.split(".");
        if (a.length > 1) {
            decValue = a[1]; // decimal value
            if (decValue && decpoint && decpoint != '') {
                if (decValue.length <= decpoint.length) {
                    value = new Number(value);
                    value = value.toFixed(decpoint.length);
                    value = value.toString();
                    var b = value.split(".");
                    value = b[0]; // int value
                    decValue = b[1];
                } else {
                    // var tmpvalue = new Number("0."+decValue);
                    // var tmpvalue2 = roundUp(tmpvalue, decpoint.length)+"";
                    // var b = tmpvalue2.split(".");
                    // decValue = b[1]; // decimal value
                }
            }
        } else {
            value = a[0]; // int value
        }
        if (!decValue && decpoint && decpoint != '') {
            decValue = decpoint;
        }
    }
    var index = -1;
    if (value.length > 1 && value.charAt(0) == "0") {
        for (var i = 0; i < value.length; i++) {
            index++;
            if (value.charAt(i) == '.') {
                i = value.length;
                index--;
            } else if (value.charAt(i) != 0 && value.charAt(i) != ',') {
                i = value.length;
            }
        }
        value = value.substr(index, obj.value.length);
    }

    var objRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
    if (value !== '0') {
        if (value != null && value.indexOf(".") > -1) {
            value = value.substring(0, value.indexOf("."));
            objRegExp = new RegExp('(-?[0-9]+)([0-9]{3})[\.[0-9]]');
        }
    }

    while (objRegExp.test(value)) {
        value = value.replace(objRegExp, '$1,$2');
    }

    if (arguments.length != 1) {
        if (value !== "") {
            if (decValue) {
                value += "." + decValue;
            }
        }
    }
    obj.value = value;
}

window.removeNumberWithCommas = function removeNumberWithCommas(obj) {
    $(obj).val($(obj).val().replace(/,/g, ''));
};

function numbersOnlyOnKeyPress(obj, e, isAllowNegativeValue) {
    var valid = false;
    if (isAllowNegativeValue) {
        valid = performFiltering(/[^-?\d]/g, e);
    } else {
        valid = performFiltering(/[^\d]/g, e);
    }
    if (valid) {
        if (!e)
            e = window.event;
        if (e.keyCode)
            code = e.keyCode;
        else if (e.which)
            code = e.which;

        if (valid == false) {
            e.keyCode = 0;
            if (isFirefox == false) {
                if (event)
                    (event.preventDefault) ? event.preventDefault()
							: event.returnValue = false;
            } else {
                e.preventDefault();
            }
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function performFiltering(regex, e) {
    if (!e)
        e = window.event;
    if (e.keyCode)
        code = e.keyCode;
    else if (e.which)
        code = e.which;

    if (isFirefox) {
        // backspace = 8, tab = 9, delete = 46, left = 37, right = 39, paste=118
        if (code === 8 || code === 9 || code === 46 || code === 37
				|| code === 39 || code === 118) {
            return true;
        }
    }

    var strkeyin = String.fromCharCode(code);
    strkeyin = strkeyin.replace(regex, '');
    var code = strkeyin.charCodeAt(0);

    if (isNaN(code)) {
        e.keyCode = 0;
        if (isFirefox == false) {
            if (event)
                (event.preventDefault) ? event.preventDefault()
						: event.returnValue = false;
        } else {
            e.preventDefault();
        }
        return false;
    } else {
        return true;
    }
}

function validateDecimal(myTextbox, maxlen, additionalCommaLen, decpoint) {
    if (myTextbox != null) {
        if (myTextbox.addEventListener) {
            myTextbox.addEventListener('keypress', function (event) { numbersWithDecimalOnlyOnKeyPress(this, decpoint, event) }, false);
            myTextbox.addEventListener('paste', function (event) { pasteValidate(decimal, event, decpoint) }, false);
            myTextbox.addEventListener('focus', function (event) { removeNumberWithCommas(this); $(this).attr("maxlength", maxlen); }, false);
            myTextbox.addEventListener('blur', function (event) { addNumberWithCommas(this, decpoint); $(this).attr("maxlength", maxlen + additionalCommaLen); }, false);
        } else {
            myTextbox.attachEvent('onkeypress', function (event) { numbersWithDecimalOnlyOnKeyPress(myTextbox, decpoint, event) });
            myTextbox.attachEvent('onpaste', function (event) { pasteValidate(decimal, event, decpoint) });
            myTextbox.attachEvent('onfocus', function (event) { removeNumberWithCommas(myTextbox); $(this).attr("maxlength", maxlen); });
            myTextbox.attachEvent('onblur', function (event) { addNumberWithCommas(myTextbox, decpoint); $(this).attr("maxlength", maxlen + additionalCommaLen); });
        }
    }
}

function validateNumber(myTextbox, maxlen, additionalCommaLen, isAllowNegativeValue) {
    if (myTextbox != null) {
        if (myTextbox.addEventListener) {
            myTextbox.addEventListener('keypress', function (event) { numbersOnlyOnKeyPress(this, event, isAllowNegativeValue) }, false);
            myTextbox.addEventListener('paste', function (event) { pasteValidate(numeric, event, '') }, false);
            myTextbox.addEventListener('focus', function (event) { removeNumberWithCommas(this); $(this).attr("maxlength", maxlen); }, false);
            myTextbox.addEventListener('blur', function (event) { addNumberWithCommas(this, ''); $(this).attr("maxlength", maxlen + additionalCommaLen); }, false);
        } else {
            myTextbox.attachEvent('onkeypress', function (event) { numbersOnlyOnKeyPress(myTextbox, event, isAllowNegativeValue) });
            myTextbox.attachEvent('onpaste', function (event) { pasteValidate(numeric, event, '') });
            myTextbox.attachEvent('onfocus', function (event) { removeNumberWithCommas(myTextbox); $(this).attr("maxlength", maxlen); });
            myTextbox.attachEvent('onblur', function (event) { addNumberWithCommas(myTextbox, ''); $(this).attr("maxlength", maxlen + additionalCommaLen); });
        }
    }
}