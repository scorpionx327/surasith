﻿// Create by: Jirawat Jannet
// Create date: 03/02/2017

// All control in main screen
var Controls = {
    COMPANY: $('#COMPANY'),
    YEAR: $('#YEAR'),
    ROUND: $('#ROUND'),
    ASSET_LOCATION: $('#ASSET_LOCATION'),
    SearchResult: $('#SearchResult'),
    SearchResultTable: $('#SearchResultTable'),
    SearchButton: $("#WFD02610btnSearch"),
    ClearButton: $('#WFD02610btnClear'),
    Pagin: $('#SearchResultTable').Pagin(URL_CONST.ITEM_PER_PAGE),
    DataTable: null,
    CBSelectAll: $('#selectAll'),
    SearchDataRow: $('#SearchDataRow'),
    btnNewPlan: $('#WFD02610btnNewPlan'),
    btnFinishPlan: $('#WFD02610btnFinishPlan'),
    btnExportOracle: $('#WFD02610btnExportOracle'),
    btnExportResult: $('#WFD02610btnExportResult'),
    FinishPlanModal: $('#FinishPlanModal')
   
};

// All controls in finishplan modal screen
var FinishPlanControls = {
    lbCompany: $('#lbCompany'),
    lbPeriod: $('#lbPeriod'),
    lbAssetLocation: $('#lbAssetLocation'),
    lbTotalAsset: $('#lbTotalAsset'),
    lbScanResult: $('#lbScanResult'),
    lbNotFound: $('#lbNotFound'),
    taRemark: $('#taRemark'),
    btnOk: $('#btnOKFP'),
    btnCancel: $('#btnCancelFP'),
}

// Data for finish plan process
var FinishPlanData = {
    CheckData: null,
    ItemData: null
}

// Search condition data 
var DefaultScreenData = {
    WFD02610MultipleBoxCompany: '',
    YEAR: '',
    ROUND: '',
    ASSET_LOCATION: ''

};

// Export to oracle
var batchProcessBFD02680 = BatchProcess({
    UserId: '000001',
    BatchId: 'BFD02680'
});
// Export to result
var batchProcessLFD02690 = BatchProcess({
    UserId: '000001',
    BatchId: 'LFD02690'
});

// Load screen event
$(function () {

    if (IsFAAdmin == '1') {
        IsFAAdmin = true;
    } else {
        IsFAAdmin = false;
    }

    initialDefault();

    Controls.YEAR.IntegerSearchInput();
    Controls.ROUND.IntegerSearchInput();

    Controls.SearchButton.click(OnSearchBtnClick);
    Controls.ClearButton.click(ClearScreen);
    Controls.btnNewPlan.click(OnNewPlanBtnClick);
    Controls.btnFinishPlan.click(finishPlan);
    Controls.btnExportOracle.click(ExportOracle);
    Controls.btnExportResult.click(ExportResult);

    FinishPlanControls.btnOk.click(finishPlanSubmit);

    Controls.CBSelectAll.change(selectAllItem);
    Controls.SearchDataRow.hide();

    $('#COMPANY').change(BindingControls);

    // Check batch process is running
    batchProcessBFD02680.Initial();
    batchProcessLFD02690.Initial();

    Controls.btnNewPlan.prop('disabled', !IsFAAdmin);
    Controls.btnFinishPlan.prop('disabled', !IsFAAdmin);

    $('#YEAR, #ROUND').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
           
            //console.log(this.id);
            Controls.SearchButton.click();
            return false;
        }
    });
});

// Load default condition data to screen input 
initialDefault = function () {
    Controls.ASSET_LOCATION.select2("val", '');
    $('#YEAR').val('');
    $('#ROUND').val('');
}



var BindingControls = function () {
   
    var con = {
        COMPANY: $('#COMPANY').val()
    }
    initialDefault();
    ajax_method.Post(URL_CONST.GET_DEFAULT_SCREEN_VAL, con, true
        , function (result) {

            $('#Condition').bindJSON(result);
            Controls.COMPANY.val(result.COMPANY);//.change();
            Controls.ASSET_LOCATION.select2("val",result.ASSET_LOCATION);//.change();

            //OnSearchBtnClick();

        }, null);
};

// Get search condition data from screen input
var getDefaultSceenValue = function () { 
    return {
        COMPANY: $('#COMPANY').val(),
        YEAR: $('#YEAR').val(),
        ROUND: $('#ROUND').val(),
        ASSET_LOCATION: $('#ASSET_LOCATION').val()
    };
};

// Callbac function after click search button
var OnSearchBtnClick = function () {
    Controls.Pagin.ClearPaginData();
    DefaultScreenData = getDefaultSceenValue();
    Search();
};


var OnNewPlanBtnClick = function () {

    //alert('Open WFD02620 : Stock Take Request Detail Screen');
    window.location = '/WFD02620';

};

// Search plan function
var Search = function () {
    var pagin = Controls.Pagin.GetPaginData();

    ajax_method.SearchPost(URL_CONST.SEARCH, DefaultScreenData, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            if ($.fn.DataTable.isDataTable(Controls.SearchResultTable)) {
                var table = Controls.SearchResultTable.DataTable();
                table.destroy();
            }


            Controls.DataTable = Controls.SearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { "data": null },
                    { "data": "COMPANY" },
                    { "data": "YEAR" },
                    { "data": "ROUND" },
                    { "data": "ASSET_LOCATION_NAME" },
                    { "data": "PLAN_STATUS_TEXT" },
                    { "data": "TOTAL_COST_CENTER" },
                    { "data": null },// { "data": "TOTAL_ASSETS" },
                    { "data": null }, //{ "data": "SCAN_RESULT" },
                    { "data": "PLAN_DAY" },
                    { "data": "ACTUAL_DAY" },
                    { "data": "REMARK" },
                    { "data": null }
                ],
                'columnDefs': [{
                    'targets': 0,
                    'orderable': false,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return '<input type="checkbox" name="key" onclick="selectRowChange()" data-key="'
                            + $('<div/>').text(data.STOCK_TAKE_KEY).html() + '">';
                    }
                },
                {
                    'targets': 7,
                    'className': 'text-right',
                    'render': function (data, type, full, meta) {
                        return numberFormat(data.TOTAL_ASSETS);
                    }
                },
                {
                    'targets': 8,
                    'className': 'text-right',
                    'render': function (data, type, full, meta) {
                        return numberFormat(data.SCAN_RESULT);
                    }
                },
                {
                    'targets': 12,
                    'orderable': false,
                    'className': 'text-center',
                    'render': function (data, type, full, meta) {
                        return '<a href="#" name="detailLink" onclick="OnDetailLinkClick(this)" '
                            + 'data-company="' + $('<div/>').text(data.COMPANY).html() + '" '
                            + 'data-year="' + $('<div/>').text(data.YEAR).html() + '" '
                            + 'data-round="' + $('<div/>').text(data.ROUND).html() + '"  '
                            + 'data-asset-location="' + $('<div/>').text(data.ASSET_LOCATION).html() + '" >Detail</a>';
                    }
                }
                    , { 'targets': [1, 2, 3], className: 'text-center' }
                    , { 'targets': [6, 7, 8, 9, 10], className: 'text-right' }],
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false
            }).draw();

            Controls.Pagin.Init(pagin, sorting, changePage, '', 'WFD02610');


            Controls.SearchDataRow.show();
            enabledButtonAfterSearchResult(true);
        } else {
            Controls.SearchDataRow.hide();
            Controls.Pagin.Clear();
            enabledButtonAfterSearchResult(false);
        }
        Controls.CBSelectAll.prop('checked', false);
    }, null, true, 'WFD01250_BoxSearchResult');

};

function numberFormat(num) {
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

var selectRowChange = function () {
    var isSelect = true;
    Controls.SearchResultTable.find('tr').each(function (e) {
        var cb = $(this).find('[name="key"]');
        if (e > 0 && !cb.is(':checked')) {
            isSelect = false;
        }
    });

    Controls.CBSelectAll.prop('checked', isSelect);
};

var OnDetailLinkClick = function (ele) {
    var company = $(ele).attr('data-company');
    var year = $(ele).attr('data-year');
    var round = $(ele).attr('data-round');
    var assetLocation = $(ele).attr('data-asset-location');

    window.location = '/WFD02620?COMPANY=' + company + '&YEAR=' + year + '&ROUND=' + round + '&ASSET_LOCATION=' + assetLocation + '';
};

var enabledButtonAfterSearchResult = function (enabled) {

    if (IsFAAdmin) {
        Controls.btnFinishPlan.prop('disabled', !enabled);
    }
    Controls.btnExportOracle.prop('disabled', !enabled);
    Controls.btnExportResult.prop('disabled', !enabled);
};

// Callback function after click soirting column in table
var sorting = function (PaginData) {
    //PaginData.OrderType = PaginData.OrderType == 'asc' ? 'desc' : 'asc';
    Search();
};

// Callback function after change pagination on table
var changePage = function (PaginData) {
    Search();
};

// Callback function after click clear button
var ClearScreen = function () {

    initialDefault();
    Controls.SearchDataRow.hide();
    Controls.Pagin.Clear();

};

var ExportOracle = function () {
    var datas = getSelectPlan();

    var data = {
        data: datas,
        processName: 'Export to oracle',
        batchId: 'BFD02680'
    };

    ajax_method.Post(URL_CONST.VALIDATE_EXPORT_ORACLE, data, true, function (result) {

        if (result != null) {

            for (var i = 0; i < datas.length; i++) {
                hideProcessDataCheckboxColumn(datas[i]);
            }
            batchProcessBFD02680.Clear();
            batchProcessBFD02680.Addparam(1, 'APL_ID', result);

            batchProcessBFD02680.StartBatch(FinishExport);
        }

    }, null);

};
var ExportResult = function () {
    var datas = getSelectPlan();
    //batchProcess.TestDialog();

    var data = {
        data: datas,
        processName: 'Export to result',
        batchId: 'LFD02690'
    };

    ajax_method.Post(URL_CONST.VALIDATE_EXPORT_ORACLE, data, true, function (result) {

        if (result != null) {

            for (var i = 0; i < datas.length; i++) {
                hideProcessDataCheckboxColumn(datas[i]);
            }
            batchProcessLFD02690.Clear();
            batchProcessLFD02690.Addparam(1, 'APL_ID', result);
            batchProcessLFD02690.StartBatch(FinishExport);
        }

    }, null);
}

var FinishExport = function () {
    showAllCheckBoxColumn();
};

var validateExport = function (datas) {
    var isValid = true;

    if (datas.length < 1) {
        isValid = false;
    } else if (datas.length > 1) {
        isValid = false;
    }

    return isValid;
}

// Check all row checkbox
var selectAllItem = function () {
    var chk = $(this).is(':checked');

    setSelectRows(chk);
};

var setSelectRows = function (checked) {
    Controls.SearchResultTable.find('[name="key"]').each(function (e) {
        $(this).prop('checked', checked);
    });
    if (checked == false) { Controls.CBSelectAll.prop('checked', false); }
};

var hideProcessDataCheckboxColumn = function (data) {
    Controls.SearchResultTable.find('tr').each(function (e) {
        var d = Controls.DataTable.row(this).data();
        if (!(typeof (d) === "undefined") && d.STOCK_TAKE_KEY == data.STOCK_TAKE_KEY) {
            $(this).find('[name="key"]').hide();
        }
    });
};
var showAllCheckBoxColumn = function () {
    Controls.SearchResultTable.find('tr').each(function (e) {
        $(this).find('[name="key"]').show();
    });
};
// Get select plan data
var getSelectPlan = function () {
    var datas = [];
    Controls.SearchResultTable.find('tr').each(function (e) {
        var cb = $(this).find('[name="key"]');
        if (cb.is(':checked')) {
            datas.push(Controls.DataTable.row(this).data());
        }
    });

    return datas;
};

// #region  Finish Plan Function

// Callback function after click Finish plan button
var finishPlan = function () {
    var datas = getSelectPlan();
    ajax_method.Post(URL_CONST.GET_FINISH_PLAN_DATA, datas, true, function (result) {

        if (result != null) {
            bindFinishPlanModal(result.model, result.data);
        }

    }, null);
};
// Valdiate finishplan select row 
var validateFinishPlanSelectitem = function (datas) {
    var isValid = true;

    if (datas.length < 1) {
        isValid = false;
    } else if (datas.length > 1) {
        isValid = false;
    }

    return isValid;
};

// Binding and show finish plan modal
var bindFinishPlanModal = function (check, data) {
    FinishPlanData.CheckData = check;
    FinishPlanData.ItemData = data;

    FinishPlanControls.lbCompany.html(data.COMPANY);
    FinishPlanControls.lbAssetLocation.html(data.ASSET_LOCATION_NAME);
    FinishPlanControls.lbNotFound.html(numberFormat(check.UNCHECK_CNT));
    FinishPlanControls.lbPeriod.html(data.YEAR + '/' + data.ROUND);
    FinishPlanControls.lbScanResult.html(numberFormat(data.SCAN_RESULT));
    FinishPlanControls.lbTotalAsset.html(numberFormat(data.TOTAL_ASSETS));
    FinishPlanControls.taRemark.val(data.REMARK);

    if (check.UNCHECK_CNT > 0) {
        FinishPlanControls.taRemark.addClass('hilight');
    } else {
        FinishPlanControls.taRemark.removeClass('hilight');
    }

    Controls.FinishPlanModal.appendTo("body").modal('show');
};

// Callback function after click OK button in finish plan modal
var finishPlanSubmit = function () {
    if (validateFinishPlan()) {

        var data = getFinishPlanConditionData();

        ajax_method.Post(URL_CONST.FINISH_PLAN, data, true, function (result) {

            if (result != null) {
                setSelectRows(false);
            }
            Controls.FinishPlanModal.appendTo("body").modal('hide');

        }, function (err) {

            Controls.FinishPlanModal.appendTo("body").modal('hide');

        });
    }
};

// Get FinishPlanConditionData from finish plan modal

var getFinishPlanConditionData = function () {
    return {
        COMPANY: FinishPlanData.ItemData.COMPANY,
        YEAR: FinishPlanData.ItemData.YEAR,
        ROUND: FinishPlanData.ItemData.ROUND,
        ASSET_LOCATION: FinishPlanData.ItemData.ASSET_LOCATION,
        REMARK: FinishPlanControls.taRemark.val(),
        TOTAL_NOT_CHECK_ASSETS: FinishPlanData.CheckData.UNCHECK_CNT,
        USER: ''
    };
};

// Validate submit finish plan process
var validateFinishPlan = function () {
    var isValid = true;
    if (FinishPlanControls.taRemark.hasClass('hilight')) {
        if (FinishPlanControls.taRemark.val() == '') {
            isValid = false;
            alert('Please input remark.');
        }
    }
    return isValid;
};


// #endregion