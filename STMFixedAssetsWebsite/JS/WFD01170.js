﻿var fMainAction = {
    _GetApproverList : function(){
        var _allRowIndx = $('input[name="hdRoleIndx"]') ;
        //  console.log(_allRowIndx);
        var _approverList = "";
        for (i = 0; i < _allRowIndx.length; i++) {
            console.log($(_allRowIndx[i]).val());
            var _indx = $(_allRowIndx[i]).val() ;
            _approverList += $('#hdRoleIndx_' + _indx).val() + '#' + $('#hdApproveRole_' + _indx).val() + "#" + $('#ddlApprover_' + _indx).val() + "|"
        }
        return _approverList;
       
    },
    GetParameter: function () {
        data = {
            RequestHeaderData: GLOBAL,
            UserPermissionData: {
                IsDelegate : PERMISSION.IsDelegate,
                IsHigher : PERMISSION.IsHigher,
                IsInstead : PERMISSION.IsInstead,
            },
            CommentInfo: {
                CommentText: $('#txtCommentInfo').val(),
            },
            ApproverList: fMainAction._GetApproverList()
        };
        data.RequestHeaderData.PHONE_NO = $('#WFD01170txtPhoneNo').val();
        data.RequestHeaderData.UPDATE_DATE = $('#hdUpdateDate').val();
        return data;
    },
    _LoadRoleOfRequestor: function (_EmpCode, _DefaultRole) {
        ajax_method.Post(MainUrl.LoadRoleOfRequestor, { USER_BY: _EmpCode, ACT_ROLE : _DefaultRole}, true, function (result) {
            console.log(result);
            if (result == null) {
                return ;
            }

            if (result == null || result.length == 0) {
                return;
            }

            $.each(result, function () {
                console.log(this.CODE);
                $('#WFD01170ddpRequestorRole').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.VALUE));
            });
            if (_DefaultRole != null) {
                $('#WFD01170ddpRequestorRole').select2('val', _DefaultRole);
                try{
                    fAction.EnableMassMode(_DefaultRole);
                }
                catch(e)
                {
                    //No action
                    console.log(e);
                }
            } else if ($('#WFD01170ddpRequestorRole option').length == 1) {
                var _sel = $('#WFD01170ddpRequestorRole option:eq(0)').val();
                $('#WFD01170ddpRequestorRole').select2('val', _sel);
                $('#WFD01170ddpRequestorRole').prop("disabled", true);
                try {
                    fAction.EnableMassMode(_sel);
                }
                catch (e) {
                    //No action
                    console.log(e);
                }
                
            }
            else
            {
                var _sel = $('#WFD01170ddpRequestorRole option:eq(0)').val();
                $('#WFD01170ddpRequestorRole').select2('val', '');
                try {
                    fAction.EnableMassMode(_sel);
                }
                catch (e) {
                    //No action
                    console.log(e);
                }
            }

            $('#WFD01170ddpRequestorRole').on('select2:select', function (e) {
                var data = e.params.data;
                try {
                    fAction.EnableMassMode(data.id);
                }
                catch (e) {
                    //No action
                    console.log(e);
                }
            });

        })
    },
    LoadRequestor: function () {
        console.log(MainUrl.LoadRequestor);
        console.log(GLOBAL);
        ajax_method.Post(MainUrl.LoadRequestor, GLOBAL, true, function (result) {
            console.log(result);
            if (result == null) {
                return ;
            }
            var docCompany = GLOBAL.COMPANY;
            if (GLOBAL.SOURCE_DOC_NO != null && GLOBAL.SOURCE_DOC_NO != "") {
                var tmpSrcDoc = GLOBAL.SOURCE_DOC_NO.split('-');
                docCompany = tmpSrcDoc[0];
            }
            console.log("---------------------------");
            $('#WFD01170ddpCompany').select2('val', docCompany);

            $('#WFD01170txtDocNo').val(GLOBAL.DOC_NO);
            $('#WFD01170txtStatus').val(result.STATUS);
            
            $('#WFD01170txtRequestDate').val(result.REQUEST_DATE);
            $('#WFD01170txtCostName').val(result.COST_NAME);
            $('#WFD01170txtEmpCode').val(result.EMP_CODE);
            $('#WFD01170txtPostName').val(result.POS_NAME);
            $('#WFD01170txtEmpName').val(result.EMP_NAME);
            $('#WFD01170txtDeptName').val(result.DEPT_NAME);

            $('#WFD01170txtPhoneNo').val(result.PHONE_NO);
            $('#WFD01170txtSectName').val(result.SECT_NAME);
           // $('#WFD01170txtStatus').val(result.UPDATE_DATE);
          
            $('#WFD01170txtEmpName').attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result.EMP_NAME
                    });
            $('#WFD01170txtPostName').attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result.POS_NAME
                    });
            $('#WFD01170txtDeptName').attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result.DEPT_NAME
                    });
            $('#WFD01170txtSectName').attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result.SECT_NAME
                    });
            $('#WFD01170txtCostName').attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result.COST_NAME
            });

            if (GLOBAL.SOURCE_ROLE != null && GLOBAL.SOURCE_ROLE != "") {
                fMainAction._LoadRoleOfRequestor(result.SYS_EMP_CODE, GLOBAL.SOURCE_ROLE);
            } else {
                fMainAction._LoadRoleOfRequestor(result.SYS_EMP_CODE, result.REQUEST_ROLE);
            }

            //Load Comment History
            fMainAction.LoadCommentHistoryList();

            //Load workFlow Tracking
            fMainAction.LoadWorkflowTrackingProcessData();
         }, null);
    },
    LoadCommentHistoryList : function () {
        $("#CommentHistory").load("/WFD01170/CommentHistory", GLOBAL);
    },    
    LoadWorkflowTrackingProcessData :function () {
        $("#WorkflowTrackingProcess").load("/WFD01170/WorkflowTrackingProcess", GLOBAL);
    },

    GenerateFlowCallBack: function () {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(MainUrl.GenerateFlow, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
               // alert(result.Message);
                return;
            }

            //Display
            $('#WFD01170Approver').html(result.Message);

            $(".ddlApprover").unbind("change");
            $(".ddlApprover").change(function () {

                
                var _divname = $(this).find(':selected').attr('data-divname');

               
                var _tr = $(this).closest('tr');
                if (_divname == null) {
                    $(_tr).find('td.divname').text("");
                    return;
                }

                $(_tr).find('td.divname').text(_divname);

            });

            //Submit Mode
            if (GLOBAL.DOC_NO == '') {

                ajax_method.Post(MainUrl.GetTotalDocument, GLOBAL, false, function (result) {
                    if (result.Message != "") {
                        $('#WFD01170Multipledocument').html("<div class=\"box-body\"><div class=\"callout callout-info\"><p>" + result.Message + "</p></div></div>");
                    } else {
                        $('#WFD01170Multipledocument').html("");
                    }
                }, null);
            
                fScreenMode.Submit_GenerateFlow(function () {
                    PERMISSION.AllowEditBeforeGenFlow = false; //not allow to edit some field
                    fMainScreenMode.Submit_GenerateFlow();
                });

            }
            else
            {
                PERMISSION.AllowEditBeforeGenFlow = false; //not allow to edit some field
                fMainScreenMode.Approve_GenerateFlow();
            }
            
            
        });
    },
    RenderFlow: function () {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(MainUrl.RenderFlow, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }

            //Display
            $('#WFD01170Approver').html(result.Message);

            $(".ddlApprover").unbind("change");
            $(".ddlApprover").change(function () {

                var _divname = $(this).find(':selected').attr('data-divname');

                var _tr = $(this).closest('tr');

                $(_tr).find('td.divname').text(_divname);

            });

        });
    },
    ResetApproveFlow:function(){
        //Url Post ResetApproverFlow
        ajax_method.Post(MainUrl.ResetApproverFlow, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                alert(result.Message); //Test only
                return;
            }

            //Display
            $('#WFD01170Approver').html('');
            $('#WFD01170Multipledocument').html("");

            if (GLOBAL.DOC_NO == '') {
                fScreenMode.Submit_AddAssetsMode(function () {
                    PERMISSION.AllowEditBeforeGenFlow = true; //allow to edit some field
                    fMainScreenMode.Submit_AddAssetsMode();
                });
            } else {
                fMainScreenMode.Approve_ResetFlow();
            }
        });

        
        
    },
    ChangeApprover: function () {

        var _data = {
            RequestHeaderData: {
                GUID : GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                UPDATE_DATE : $('#hdUpdateDate').val()
            },
            ApproverList: "" // "1#FW#1235|2#MGR#1236|3#DGM#1237|4#GM#1238"
        }

        $('#WFD01170Approver input[name=hdRoleIndx]').each(function() {

            var _seq = $(this).val();
            var _role = $("#hdApproveRole_" + _seq).val();
            var _appr = $("#ddlApprover_" + _seq).val();

            if($("#ddlApprover_" + _seq).is(":disabled")){
                return;
            }

            if (_data.ApproverList != "") {
                _data.ApproverList += "|";
            }
            _data.ApproverList +=  _seq + "#" + _role + "#" + _appr ;
        });


        console.log(_data);
        

        ajax_method.Post(MainUrl.SaveApprover, _data, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }

            $("label[for='WFD01170ALERT_TITLE']").text("Change Approver is complete successfully");
            $("label[for='WFD01170ALERT_SUBJECT']").text("You document no as below :");
            var _DocNoLink = '<a href="../WFD01170/Index?RequestType=' + GLOBAL.REQUEST_TYPE + '&DocNo=' + GLOBAL.DOC_NO + '" target="_self">' + GLOBAL.DOC_NO + '</a>';

            $('#WFD01170_DisplayMessage').html(_DocNoLink);
            $('#WFD01170_AlertSumbit').modal({ backdrop: 'static', keyboard: false });

            // fMainAction.CountdownToRedirect();

        });
    },
    SubmitCallBack: function () {
        console.log("SubmitCallBack");
        console.log(fMainAction.GetParameter());
       
        ajax_method.Post(MainUrl.Submit, fMainAction.GetParameter(), false, function (result) {

            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                //Reset Flow 2019.12.13
                if (result.ResetFlow) {
                    
                    $('#WFD01170Approver').html('');
                    $('#WFD01170Multipledocument').html("");

                    if (GLOBAL.DOC_NO == '') {
                        fScreenMode.Submit_AddAssetsMode(function () {
                            PERMISSION.AllowEditBeforeGenFlow = true; //allow to edit some field
                            fMainScreenMode.Submit_AddAssetsMode();
                        });
                    } else {
                        fMainScreenMode.Approve_ResetFlow();
                    }
                }


                //fnErrorDialog("Submit Error", result.Message);
                return;
            }
            var _DocNoLink = "";
            fMainScreenMode.Submit_Success();
            _ArrDoc = result.Message.split("|");
            for (i = 0; i < _ArrDoc.length; i++) {
                if (_ArrDoc[i] != "") {
                    _Request = _ArrDoc[i].split(":");//= DocNo:RequestType
                    if (_Request.length > 0 && _Request[0] != "") {
                        _DocNoLink += '<a href="../WFD01170?RequestType=' + _Request[1] + '&DocNo=' + _Request[0] + '" target="_self">' + _Request[0] + '</a> <br />';
                    }
                }
            }
            fMainAction._DisplayResultPopup("Submit data is completed successfully", "", _DocNoLink);


        });
    },
    ApproveCallBack: function () {
        ajax_method.Post(MainUrl.Approve, fMainAction.GetParameter(), false, function (result) {
            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                //fnErrorDialog("Approve Error", result.Message);
                return;
            }

            fMainScreenMode.Submit_Success();

            var _DocNoLink = '<a href="../WFD01170?DocNo=' + result.Message + '" target="_self">' + result.Message + '</a> <br />';
            fMainAction._DisplayResultPopup("Approve data is completed successfully", "", _DocNoLink);

        });
    },
    RejectCallBack: function(){
        ajax_method.Post(MainUrl.Reject, fMainAction.GetParameter(), false, function (result) {
            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                //fnErrorDialog("Reject Error", result.Message);
                return;
            }

            fMainScreenMode.Submit_Success();

            var _DocNoLink = '<a href="../WFD01170?DocNo=' + result.Message + '" target="_self">' + result.Message + '</a> <br />';      
            fMainAction._DisplayResultPopup("Reject data is completed successfully", "", _DocNoLink);

        });
    },
    ReSubmitCallBack: function () {
        console.log("SubmitCallBack");
        console.log(fMainAction.GetParameter());

        ajax_method.Post(MainUrl.ReSubmit, fMainAction.GetParameter(), false, function (result) {

            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                //fnErrorDialog("Submit Error", result.Message);
                return;
            }

            fMainScreenMode.Submit_Success();

            var _DocNoLink = '<a href="../WFD01170/Index?DocNo=' + result.Message + '" target="_self">' + result.Message + '</a> <br />';
            fMainAction._DisplayResultPopup("Re-Submit data is completed successfully", result.Message, _DocNoLink);


        });
    },
    AcknowledgeCallBack: function () {
        console.log("AcknowledgeCallBack");
        console.log(fMainAction.GetParameter());

        ajax_method.Post(MainUrl.Acknowledge, fMainAction.GetParameter(), false, function (result) {

            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                return;
            }

            fMainScreenMode.Submit_Success();

            var _DocNoLink = '<a href="../WFD01170/Index?DocNo=' + result.Message + '" target="_self">' + result.Message + '</a> <br />';
            fMainAction._DisplayResultPopup("Acknowledge data is completed successfully", result.Message, null);


        });
    },
    CloseCallBack: function () {
        console.log("CloseCallBack");
        console.log(fMainAction.GetParameter());

        ajax_method.Post(MainUrl.CloseRequest, fMainAction.GetParameter(), false, function (result) {

            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                return;
            }

            fMainScreenMode.Submit_Success();

            var _DocNoLink = '<a href="../WFD01170/Index?DocNo=' + result.Message + '" target="_self">' + result.Message + '</a> <br />';
            fMainAction._DisplayResultPopup("Close request is completed successfully", result.Message, null);


        });
    },
    PrepareCopy: function (callback) {

        if (callback != null)
            callback();
    },
    CopyCallBack: function () {
        console.log("CopyCallBack");
        console.log(fMainAction.GetParameter());

        ajax_method.Post(MainUrl.CopyNew, fMainAction.GetParameter(), false, function (result) {

            if (result.IsError) {

                clicksCnt = 0;
                confirms = 0;

                //fnErrorDialog("Submit Error", result.Message);
                return;
            }
            
            fMainScreenMode.Submit_Success();

            window.location = MainUrl.HomePage + "WFD01170?RequestType=" + GLOBAL.REQUEST_TYPE + "&FlowType=" + GLOBAL.FLOW_TYPE + "&guid=" + result.Message + "&SourceDocNo=" + GLOBAL.DOC_NO + "&SourceRole=" + $('#WFD01170ddpRequestorRole').val() + "&amp;COPY_MODE=Y";

            //var _DocNoLink = '<a href="../WFD01170/Index?DocNo=' + result.Message + '" target="_self">' + result.Message + '</a> <br />';
            //fMainAction._DisplayResultPopup("Copy data to new request is completed successfully", result.Message, null);
            

        });
    },

    _DisplayResultPopup : function(_title, _subject, _docNoLink){
        $("label[for='WFD01170ALERT_TITLE']").text(_title);

        if (_docNoLink == null) {
            $("label[for='WFD01170ALERT_SUBJECT']").text("");
            $('#WFD01170txtDocNo').val('');
            $('#WFD01170_DisplayMessage').html('');
        }
        else{
            $("label[for='WFD01170ALERT_SUBJECT']").text("You document no as below :");
            
            //2020.01.25 Surasith T.
            //$('#WFD01170txtDocNo').val(_subject);
            var _text = _subject + _docNoLink;
            $('#WFD01170_DisplayMessage').html(_text);
        }
        $('#WFD01170_AlertSumbit').modal({ backdrop: 'static', keyboard: false });
        fMainAction.CountdownToRedirect();
    },

    DownloadTemplate: function () {
        var _data = { FUNCTION_ID: GLOBAL.FUNCTION_ID };
        if (GLOBAL.FLOW_TYPE == "RM" || GLOBAL.FLOW_TYPE == "AU") {
            _data = { FUNCTION_ID: GLOBAL.FUNCTION_ID + "_" + GLOBAL.FLOW_TYPE};
        }
        
        ajax_method.Post(MainUrl.GetDownloadTemplate, _data, false, function (result) {
            if (result.IsError) {
                fnErrorDialog("Download Template Error", result.Message);
                return;
            }
            if (GLOBAL.FLOW_TYPE == "RM" || GLOBAL.FLOW_TYPE == "AU") {
                window.location = MainUrl.DownloadTemplate + "_" + GLOBAL.FLOW_TYPE;
            } else {
                window.location = MainUrl.DownloadTemplate;
            }
            
            //bootbox.alert("DownloadTemplate Successfully!!!!!!!!!!!!!");

        });
    },
    CountdownToRedirect :function() {
        localConfig.CountDown = localConfig.CountDown - 1;
        if (localConfig.CountDown < 0) {
            $('#WFD01170_RedirectInformation').html("System is redirecting to main page" + "&nbsp");
            window.location = MainUrl.HomePage + "?PREVPAGE=WFD0127";
        } else {
            $('#WFD01170_RedirectInformation').html("System will redirect to main page in " + localConfig.CountDown + " seconds" + "&nbsp");
            window.setTimeout("fMainAction.CountdownToRedirect()", 1000);
        }
    },

    ShowCommentInfoUploadDialog: function (button, _Type) {
        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: '',
            ASSET_SUB: '',
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: "COMMENT",
            FUNCTION_ID: "WFD01170",
            DOC_UPLOAD: "WFD01170_COMMENT",
            LINE_NO: 0,
            FILE_LIMIT : 1,
            DEL_FLAG: PERMISSION.Mode != 'V' ? "Y" : "N"
        };

        $('#lblUploadFileText').html("Upload Comment file");
        $('#CommonUpload').appendTo("body").modal('show');

    },

    GenerateAssetNo: function (_all) {
        ClearMessageC();
        //Get
        if (_all == null)
            _all = false;

        var _data = {
            DOC_NO : GLOBAL.DOC_NO,
            GUID : GLOBAL.GUID,
            SelectedList: '' // "1#FW#1235|2#MGR#1236|3#DGM#1237|4#GM#1238"
        }

        var _Cnt = 0;
        $('#tbSearchResult .chkGEN').each(function () {

            if ($(this).is(":disabled")) {
                return;
            }
            if (!$(this).is(":checked")) {
                return;
            }
            if (_data.SelectedList != "") {
                _data.SelectedList += "|";
            }
            _Cnt++;
            _data.SelectedList += $(this).val();
        });

        if (!_all && _Cnt == 0) {
            AlertTextErrorMessage(CommonMessage.Noassetselected);
            return;
        }

        var _msg = CommonMessage.ConfirmGenerateAssetNo.format(_data.DOC_NO, _Cnt);
        
        if (_all) {
            _msg = CommonMessage.ConfirmGenerateAssetNo.format(_data.DOC_NO, "all");
        }
        //Show lline no + 
        loadConfirmAlert(_msg, function (result) {
            if (!result) {
                return;
            }
            ajax_method.Post(MainUrl.GenerateAssetNo, _data, false, function (result) {
                if (result.IsError) {
                    return;
                }
                //Show popup
                $("label[for='WFD01170ALERT_TITLE']").text("Request Asset No is successfully");
                $("label[for='WFD01170ALERT_SUBJECT']").text("Please wait result from SAP");
                $('#WFD01170txtDocNo').val(result.Message);

                $('#WFD01170_AlertSumbit').modal({ backdrop: 'static', keyboard: false });
                fMainAction.CountdownToRedirect();


            });
           
        }); // loadConfirmAlert
    },
    
    _GetGenCount: function () {
        ////$(control.DataTable.row(0).node()).closest('tr')
        var _cnt = 0;
        $('#tbSearchResult .chkGEN').each(function () {

            if ($(this).is(":disabled")) {
                return;
            }
            if (!$(this).is(":checked")) {
                return;
            }
            _cnt++;
        });
        return _cnt;
    },
    _GetGenList : function () {

        var _f = "";
        $('#tbSearchResult .chkGEN').each(function () {

            if ($(this).is(":disabled")) {
                return;
            }
            if (!$(this).is(":checked")) {
                return;
            }
            if (_f != "") {
                _f += "|";
            }
            _f += $(this).val();
        });
    },

    DoublleClickLock: function () {
        //Protect MA, Incase screen not finish loading but user click submit. it's marke asset list is lose
        if (localConfig.ClientIsLoaded == null || localConfig.ClientIsLoaded != 'Y') {
            alert("Please wait, Screen is loading");
            return false;
        }

        clicksCnt++;
        if (clicksCnt != 1) {
            AlertTextErrorMessagePopup('MSTD0000AWRN : Your oparated more than one times, please refresh screen and perform your procedure again.', "#AlertMessageArea");
            clearTimeout(timer);
            clicksCnt = 0;
            confirms = 0;
            window.scrollTo(0, 0);
            return false;
        }

        return true;
    },

    IsSelectCompanyAndRole: function () {
        ClearMessageC();
        if ($('#WFD01170ddpRequestorRole option:selected').val() == null || $('#WFD01170ddpRequestorRole option:selected').val() == "") {
            AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format('', 'Role'), "#AlertMessageArea");
            window.scrollTo(0, 0);
            return false;
        }

        if ($('#WFD01170ddpCompany option:selected').val() == null || $('#WFD01170ddpCompany option:selected').val() == "") {
            AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format('', 'Company'), "#AlertMessageArea");
            window.scrollTo(0, 0);
            return false;
        }

        return true;
    },
    getConfirmDeleteAssetDetail: function (assetNo, assetSub) {
        var msg = "Asset No. " + assetNo;
        if (assetSub) {
            msg += " Asset Sub " + assetSub;
        }
        return CommonMessage.ConfirmDelete.replace("{1}", msg);
    },
    getIsAECRole: function () {
        var isAECUser = 'N';
        var role = $('#WFD01170ddpRequestorRole').select2('val');
        if (role === "ACU") {
            isAECUser = 'Y';
        }

        return isAECUser;
    }
}

var fUploadAction = {
    //UploadComment : function () {
    //    //var _indx = '0';

    //    /*if (SCREEN_PARAMS.INDX != '' && SCREEN_PARAMS.INDX != null) {
    //        _indx = SCREEN_PARAMS.INDX;
    //    }*/

    //    var model = new FormData();

    //    //Cannot apply $ syntax
    //    var fileInput = document.getElementById('WFD01170fileUploadCommentInfo');
    //    //model.append('Index', _indx);
    //    //model.append('CommentStatus', SCREEN_PARAMS.COMMENT_STATUS);
    //    debugger;
    //    if (fileInput.files.length == 0) {
    //        return ;
    //    }
    //    model.append('CommentFile', fileInput.files[0]);
    //    model.append('AttachFileName', fileInput.files[0].name);
    //    model.append('DOC_NO', GLOBAL.DOC_NO);

    //    var ModelValidateFile = {
    //        FunctionID: 'WFD01170',
    //        Folder: 'COMMENT',
    //        FileName: fileInput.files[0].name,
    //        FileSize: fileInput.files[0].size
    //    }
    //    ajax_method.Post(MainUrl.CheckUploadFile, ModelValidateFile, false, function (result) {
    //        console.log(result);
    //        if (result.IsError) {// true 
    //            return ;
    //        }

    //        //Success
    //        ajax_method.PostFile(MainUrl.UploadCommentFile, model, function (result) {
    //            if (result.IsError) {
    //                $('#WFD01170fileUploadCommentInfo').val();
    //                return;
    //            }
    //            //WFD01270FileUplaodControls.Button.hide();
    //            //WFD01270FileUplaodControls.DelFileBTN.show();
    //            //WFD01270FileUplaodControls.DownloadFileBTN.hide();
    //            //WFD01270FileUplaodControls.ViewFileBTN.show();
    //            $('#WFD01170FileUploadTB').val(result.Message); //File Name
    //          //  WFD01270FileUplaodControls.Textbox.val(result.ObjectResult.FileName);
    //        //    document.getElementById('WFD01270btnViewCommentInfo').href = "/WFD01170/DownloadFileTemp?FILENAME=" + result.Message + "&FunctionID=WFD01170" + "&Type=COMMENT";
    //            $("#WFD01270btnViewCommentInfo").attr('href', "/WFD01170/DownloadFileTemp?FILENAME=" + result.Message + "&FunctionID=WFD01170" + "&Type=COMMENT")
               

    //        }, null);
                
    //    }, null);
        
    //},
    UploadExcel : function(){
        var model = new FormData();

        //Cannot apply $ syntax
        var fileInput = document.getElementById('WFD01170fileUploadRequestExcel');
        model.append('GUID', GLOBAL.GUID);
        model.append('RequestType', GLOBAL.REQUEST_TYPE);
        model.append('ExcelFile', fileInput.files[0]);
        
        var allowedExtensions = /(\.xlsx)$/i;

        if (!allowedExtensions.exec(fileInput.files[0].name)) {
            alert('Please upload file having extensions .xlsx only.');
        }

        if (fileInput.files.length == 0) {
            return;
        }

        var ModelValidateFile = {
            FunctionID: 'WFD01170',
            Folder: 'REQUEST',
            FileName: fileInput.files[0].name,
            FileSize: fileInput.files[0].size
        }

        

        ajax_method.Post(MainUrl.CheckUploadFile, ModelValidateFile, false, function (result) {
            console.log(result);
            if (result.IsError) {// true 
                return;
            }

            //Success -> FTP to DB
            ajax_method.PostFile(MainUrl.UploadRequest, model, function (result) {

                var rs = result.ObjectResult;
                if (rs.IsError) {
                   return;
                }



                if (rs.FileName == null) {
                    console.err("No File Name ");
                    return;
                }


                fAction.UploadExcel(rs.FileName);


                

                
            }, null);

        }, null);

    }
}


var fMainScreenMode = {
    
    _ClearMode: function () {
        //Document Status Button
        //$('#WFD01170btnSubmit, #WFD01170btnApprove, #WFD01170btnReject').hide();
        //$('#WFD01170btnSubmit, #WFD01170btnApprove, #WFD01170btnReject').addClass('disabled').removeClass('active').prop("disabled", true);
        $('#WFD01170btnSubmit').hide();
        $('#WFD01170btnSubmit').addClass('disabled').removeClass('active').prop("disabled", true);

        $("#WFD01170ddpCompany, #WFD01170ddpRequestorRole, #WFD01170txtPhoneNo").prop("disabled", true);

        

        //Set Class
        //Generate Flow Button
        $('#WFD01170btnSaveApprover, #WFD01170btnGenerateApprover, #WFD01170btnResetApprover').hide();
        $('#WFD01170btnSaveApprover, #WFD01170btnGenerateApprover, #WFD01170btnResetApprover').addClass('disabled').removeClass('active').prop("disabled", true);

        //Comment
        //$('#WFD01170btnUploadCommentInfo').hide();
        //$('#WFD01170btnUploadCommentInfo').addClass('disabled').removeClass('active').prop("disabled", true);
        //$('#txtCommentInfo').prop("disabled", true);

    },
    _SetChangeApprover : function(){
        if (PERMISSION.AllowChangeApprover == "Y") {
            $('#WFD01170btnSaveApprover').removeClass('disabled').prop("disabled", false);
            $('#WFD01170btnSaveApprover').show();
        }
        else {
            $('#WFD01170btnSaveApprover').addClass('disabled').prop("disabled", true);
            $('#WFD01170btnSaveApprover').hide();
        }
    },

    Submit_InitialMode: function () {
        //Set Asset Mode
        fMainScreenMode._ClearMode();

        //Show but not allow to click
        $('#WFD01170btnSubmit').show();

        var length = $('#WFD01170ddpCompany > option').length;
        if(length > 1){
            $("#WFD01170ddpCompany").removeClass('disabled').addClass('require').prop("disabled", false);

        }

        //Enable Control
        $("#WFD01170ddpRequestorRole, #WFD01170txtPhoneNo").removeClass('disabled').prop("disabled", false);

        $("#WFD01170txtPhoneNo").addClass('require');

        //Show but not allow to edit
        $('#WFD01170btnGenerateApprover, #WFD01170btnResetApprover').show();

        //Comment
        $('#WFD01170btnUploadCommentInfo').show();
        $('#WFD01170btnUploadCommentInfo').removeClass('disabled').prop("disabled", false);
        $('#txtCommentInfo').prop("disabled", false);

        $(':input:enabled:visible:first').focus();
    },

    Submit_AddAssetsMode: function () {
        
        //It's call from child screen.
        fMainScreenMode._ClearMode();

        //Show but not allow to click
        $('#WFD01170btnSubmit').show();
        $("#WFD01170txtPhoneNo").removeClass('disabled').prop("disabled", false);

        //Enable Generate Flow
        $('#WFD01170btnGenerateApprover, #WFD01170btnResetApprover').show();
        $('#WFD01170btnGenerateApprover').removeClass('disabled').prop("disabled", false);


    },
    Submit_GenerateFlow: function () {
        //It's call from child screen.
        fMainScreenMode._ClearMode();

        $('#WFD01170btnSubmit').show();
        $('#WFD01170btnSubmit').removeClass('disabled').prop("disabled", false);

        $("#WFD01170txtPhoneNo").prop("disabled", false);

        //Enable Reset Generate Flow
        $('#WFD01170btnGenerateApprover, #WFD01170btnResetApprover').show();
        $('#WFD01170btnResetApprover').removeClass('disabled').prop("disabled", false);
    },
    Submit_Success: function () {

        fMainScreenMode._ClearMode();
        //$('#WFD01170btnSubmit, #WFD01170btnApprove, #WFD01170btnReject').addClass('disabled').removeClass('active').prop("disabled", true);
    },
    Approve_Mode: function () {
        
        fMainScreenMode._ClearMode();
        $("#WFD01170ddpCompany, #WFD01170ddpRequestorRole, #WFD01170txtPhoneNo").removeClass('require');
        //Document
        if (PERMISSION.Mode == "V") {
            fMainScreenMode._View();
            return;
        }

        if (PERMISSION.Mode == "A") {
            fMainScreenMode._SetChangeApprover();

            if (PERMISSION.AllowSelectApprover == 'Y') {
                fMainScreenMode.Approve_GenerateFlow();
            }
        }
    },
    
    Approve_GenerateFlow : function(){
        //Enable Reset Generate Flow
        $('#WFD01170btnGenerateApprover, #WFD01170btnResetApprover').show();
        $('#WFD01170btnGenerateApprover').addClass('disabled').prop("disabled", true);
        $('#WFD01170btnResetApprover').removeClass('disabled').prop("disabled", false);

        $("#WFD01170ddpCompany, #WFD01170ddpRequestorRole, #WFD01170txtPhoneNo").removeClass('require');

        fMainScreenMode._SetChangeApprover();

       

    },
    Approve_ResetFlow: function() {
        $('#WFD01170btnGenerateApprover, #WFD01170btnResetApprover').show();
        $('#WFD01170btnResetApprover').addClass('disabled').prop("disabled", true);
        $('#WFD01170btnGenerateApprover').removeClass('disabled').prop("disabled", false);
        $('#WFD01170btnSaveApprover').hide();
    },
    _View: function () {
        console.log("_View");
        fMainScreenMode._SetChangeApprover();

        $('#divCommentInfo').hide();

        //Nothing to display
        //$('#WFD01170btnUploadCommentInfo').hide();
        //$('#WFD01170btnUploadCommentInfo').addClass('disabled').removeClass('active').prop("disabled", true);
        //$('#txtCommentInfo').prop("disabled", true);
    }

}

//Load Function
$(function () {
    fMainAction.LoadRequestor();
    
    console.log(GLOBAL.DOC_NO);
    if (GLOBAL.DOC_NO == '') {
        fScreenMode.Submit_InitialMode(function () {
            fMainScreenMode.Submit_InitialMode();
        })
    }
    else { //Approve Mode
        fMainAction.RenderFlow();
    }
    console.log("Main Formload");


    if ($('#WFD01170ddpRequestorRole option:selected').val() != "") {
        fAction.Initialize(function () { //Load Combobox
            fAction.LoadData(function () {
                //Call back function
                console.log("Load Finished");
                if (GLOBAL.DOC_NO != '') {
                    fScreenMode.Approve_Mode(function () {
                        fMainScreenMode.Approve_Mode();
                    })

                }
                localConfig.ClientIsLoaded = 'Y';//

            })
        });
    }

    
    
});


function LoadDataAbstract(callback, flowType, winPopup) {
    console.log("LoadDataAbstract");
    if (flowType == "KN") {
        winPopup.close();
        location.reload();
    }else{
        fAction.LoadData(callback);
    }
    
}
// Child button fAction is define in child function

//==================== Start Button ====================//
$('#btnRequestAdd').click(function (e) {
   
    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }
    fAction.ShowSearchPopup(function () {
        alert("btnRequestAdd");
    });
})
$('#btnRequestClear').click(function (e) {
    loadConfirmAlert(CommonMessage.ConfirmClear, function (result) {
        if (!result) {
            return;
        }

        var _selectedCompany = $('#WFD01170ddpCompany').val();
        //Clear Screen
        fAction.Clear(function () {
            console.log('X btnRequestClear ---------------------- X');
            console.log($('#WFD01170ddpCompany').val());
            $('#WFD01170ddpCompany').select2('val',_selectedCompany);
            //Set Original
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                message: "Clear process is completed."
            }, {
                type: 'success',
                delay: 500,
            });
        });

    }); //End of clear assets function
})
$('#btnRequestExport').click(function (e) {
    loadConfirmAlert(CommonMessage.ConfirmExportToExcel, function (result) {
        if (!result) {
            return;
        }
        fAction.Export(function () {
            //Complete
            console.log("Export Finish");
        });
    });
})

$('#btnRequestDownload').click(function (e) {
    fMainAction.DownloadTemplate(function () {
        alert("btnRequestDownload");
    });
})

$('#btnRequestUpload').click(function (e) {
    ClearMessageC();

    if (!fMainAction.IsSelectCompanyAndRole()) {
        return;
    }

    var validate = true;

    if (fAction.ValidateUploadExcel) {
        validate = fAction.ValidateUploadExcel();
    }

    if (validate) {
        $('#WFD01170fileUploadRequestExcel').click();
    }
})
$('#WFD01170fileUploadRequestExcel').change(function () {
    fUploadAction.UploadExcel();
    
    $(this).val('');
})

//==================== End Button ====================//

$('#WFD01170btnGenerateApprover').click(function (e) {
    ClearMessageC();
    GLOBAL.ACT_ROLE = $('#WFD01170ddpRequestorRole').val(); // Apply for all reqyeset
    fAction.PrepareGenerateFlow(function () {
        //Add Callback function when
        fMainAction.GenerateFlowCallBack();
    });
})
$('#WFD01170btnResetApprover').click(function (e) {
    fMainAction.ResetApproveFlow();
})

$('#WFD01170btnSaveApprover').click(function (e) {
    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;


    timer = setTimeout(loadConfirmAlert(CommonMessage.ChangeApprover, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }

        //Perform click
        fMainAction.ChangeApprover();
    }), DELAY);
})
var DELAY = 700, clicksCnt = 0, timer = null, confirms = 0;

$('#WFD01170btnSubmit').click(function (e) {

    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;

    
    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmSubmit, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }

        //Perform click
        fAction.PrepareSubmit(function () {
            //Add Callback function when
            fMainAction.SubmitCallBack();
        });

    }), DELAY);
   

    
})

$('#WFD01170btnApprove').click(function (e) {
    ClearMessageC();

    //Add Validation
    if (control.DataTable == null || control.DataTable.data().length == 0) {
        AlertTextErrorMessagePopup(CommonMessage.NoAssetApprove, "#AlertMessageArea");
        window.scrollTo(0, 0);
        return;
    }

    //Protect MA, Incase screen not finish loading but user click submit. it's marke asset list is lose
    if (!fMainAction.DoublleClickLock())
        return;

    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmApprove, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0;
            confirms = 0; //Reset
            return;
        }

        //Perform click
        fAction.PrepareApprove(function () {
            //Add Callback function when
            fMainAction.ApproveCallBack();
        });

    }), DELAY);

})

$('#WFD01170btnReject').click(function (e) {
    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;

    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmReject, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0;
            confirms = 0; //Reset
            return;
        }

        //Perform click
        fAction.PrepareReject(function () {
            //Add Callback function when
            fMainAction.RejectCallBack();
        });

    }), DELAY);

    
})

$('#WFD01170btnResend').click(function (e) {
    ClearMessageC();

    var _SelectedCnt = fMainAction._GetGenCount();
    if (_SelectedCnt == 0) {
        AlertTextErrorMessage(CommonMessage.Noassetselected);
        return;
    }

    if (!fMainAction.DoublleClickLock())
        return;

    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmResend.format(GLOBAL.DOC_NO, _SelectedCnt), function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0;
            confirms = 0; //Reset
            return;
        }
        
        //Perform click
        fAction.Resend(function () {

            //Insert Comment

            $("label[for='WFD01170ALERT_TITLE']").text("Resend data is completed successfully");
            $("label[for='WFD01170ALERT_SUBJECT']").text("You document no as below :");
            

            var _DocNoLink = '<a href="../WFD01170/Index?DocNo=' + GLOBAL.DOC_NO + '" target="_self">' + GLOBAL.DOC_NO + '</a> <br />';
            $('#WFD01170_DisplayMessage').html(_DocNoLink);


            $('#WFD01170_AlertSumbit').modal({ backdrop: 'static', keyboard: false });
            fMainAction.CountdownToRedirect();

        });
        

    }), DELAY);


})

$('#WFD01170btnReSubmit').click(function (e) {
    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;


    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmReSubmit, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }

        //Perform click
        fAction.PrepareApprove(function () {
            //Add Callback function when
            fMainAction.ReSubmitCallBack();
        });

    }), DELAY);



})

$('#WFD01170btnAcknowledge').click(function (e) {
    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;


    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmAckhowledge, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }

        fMainAction.AcknowledgeCallBack();


    }), DELAY);



})
$('#WFD01170btnClose').click(function (e) {
    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;


    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmClose, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }

        fMainAction.CloseCallBack();


    }), DELAY);



})
$('#WFD01170btnCopy').click(function (e) {
    ClearMessageC();
    if (!fMainAction.DoublleClickLock())
        return;


    timer = setTimeout(loadConfirmAlert(CommonMessage.ConfirmCopy, function (result) {

        confirms++; // Click confirm cnt
        if (confirms != 1) { //If click confirm 2 times
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }
        if (!result) { //If cancel confirm
            clicksCnt = 0; // Reset
            confirms = 0; //Reset
            return;
        }

        //Perform click
        fMainAction.PrepareCopy(function () {
            //Add Callback function when
            fMainAction.CopyCallBack();
        });

    }), DELAY);



})



$('#WFD01170btnUploadCommentInfo').click(function (e) {
    ClearMessageC();
    //$('#WFD01170fileUploadCommentInfo').click();
    fMainAction.ShowCommentInfoUploadDialog(this,'COMMENT_INFO');
})

//$('#WFD01170fileUploadCommentInfo').change(function () {
//    fUploadAction.UploadComment();
//})

$('#WFD01170_CloseRequester').click(function () {
    window.location = MainUrl.HomePage + "?PREVPAGE=WFD0127";
})

$('#WFD01170ddpCompany').on('select2:select', function (e) {

    var data = e.params.data;
    console.log(data);
    GLOBAL.COMPANY = data.id;

    fAction.Initialize(function () { //Load Combobox
        fAction.LoadData(function () {
            //Call back function
            console.log("Load Finished");
            if (GLOBAL.DOC_NO != '') {
                fScreenMode.Approve_Mode(function () {
                    fMainScreenMode.Approve_Mode();
                })

            }
            localConfig.ClientIsLoaded = 'Y';//

        })
    });

    
});