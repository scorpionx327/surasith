﻿var IsInit = "Y";// set initial value when first load

var WFD02160Ctr = {
    COMSCCSearchCostCodeModal: $('#SearchCostCenter')
    //text box
    , txtWFD02160MapLayoutFullPath: $('#txtWFD02160MapLayoutFullPath')
    , txtCostCode: $('#txtCostCode')
    , WFD02160_SEQ: $('#WFD02160_SEQ')
    , WFD02160_COMPANY: $('#WFD02160_COMPANY')

    //Get code center code 
    , WFD02160GetCostCenter: $('#WFD02160GetCostCenter')
    , WFD02160_COST_CODE: $('#WFD02160_COST_CODE')
    , WFD02160_COST_NAME: $('#WFD02160_COST_NAME')
    , WFD02160SubType: $('#WFD02160SubType')
    , GroupDropdownSubType: $('#GroupDropdownSubType')
    , GroupSubType: $('#GroupSubType')

    //Master
    , WFD01210COMPANY: $('#WFD02160_COMPANY')
    , WFD01210MinorCategory: $('#WFD01210MinorCategory')

    // Map Layout
    , imgWFD02160MapLayout: $('#imgWFD02160MapLayout')
    , WFD02160ItemSubType: $('#WFD02160ItemSubType') // , WFD02160ItemSubTypeTableBody: $('#WFD02160ItemSubType table tbody')
    , WFD02160ItemSubTypeTableBody: $('#WFD02160ItemSubType table tbody')

    //Button control
    , btnWFD02160View: $('#btnWFD02160View')
    , btnWFD02160Delete: $('#btnWFD02160Delete')
    , btnWFD02160Print: $('#btnWFD02160Print')
    , btnWFD02160Edit: $('#btnWFD02160Edit')
    , btnWFD02160Save: $('#btnWFD02160Save')
    , btnWFD02160Cancel: $('#btnWFD02160Cancel')

};

var WFD02160_Items = {
    AllData: []
};

// Asset file upload's controls
var WFD02160UplaodCtrol = {
    Textbox: $('#txtWFD02160FileUpload'),
    File: $('#WFD02160FileUploadFile'),
    BtnBrowse: $('#btnWFD02160Browse'),
    BtnUpload: $('#btnWFD02160Upload')
}

var batchLFD02170 = BatchProcess({
    BatchId: "LFD02170",
    Description: "Print out visualize Asset location map report",
    UserId: WFD02160_CONST.UserLogon

});

$(function () {
    batchLFD02170.Initial();

    WFD02160Ctr.WFD02160GetCostCenter.click(WFD02160GetModelCostCenter);
    bindingWFD02160FileUploadControlEvents();// Binding Asset upload file's  controls event
    WFD02160UplaodCtrol.BtnBrowse.click(onWFD02160FileUploadButtonClick);
    WFD02160UplaodCtrol.BtnUpload.click(onWFD02160UploadButtonClick);

    // button click
    WFD02160Ctr.btnWFD02160View.click(onWFD02160ViewButtonClick);
    WFD02160Ctr.btnWFD02160Delete.click(onWFD02160DeleteButtonClick);
    WFD02160Ctr.btnWFD02160Print.click(onWFD02160PrintButtonClick);
    WFD02160Ctr.btnWFD02160Edit.click(onWFD02160EditButtonClick);

    WFD02160Ctr.btnWFD02160Save.click(onWFD02160SaveButtonClick);

    WFD02160Ctr.btnWFD02160Cancel.click(onWFD02160CancelButtonClick);

    // Cost center dialog
    WFD02160Ctr.COMSCCSearchCostCodeModal.off('hidden.bs.modal').on('hidden.bs.modal', function () {
        var COMPANY = WFD02160Ctr.WFD02160_COMPANY.val()
        setValueTBCostCode();

    });
   
    //button mode
    InitialButton("init");
    WFD02160Ctr.txtCostCode.focus();
    defaultSearchByCostCode();
    GetCompany();
    GetMinorCategore();
 
});
$(function () {
    $("#sortable1, #sortable2").sortable({
        connectWith: ".connectedSortable"
    }).disableSelection();
});

var defaultSearchByCostCode = function () {
    if (WFD02160_CONST.DefaultCostCode != "") {
        setInitTBCostCode();
        onWFD02160ViewButtonClick('0');
    }
}

var setValueTBCostCode = function () {

    if (WFD02160Ctr.WFD02160_COST_CODE.val() != "") {
        WFD02160Ctr.txtCostCode.removeAttr("data-toggle");
        WFD02160Ctr.txtCostCode.removeAttr("data-placement");
        WFD02160Ctr.txtCostCode.removeAttr("title");
        WFD02160Ctr.txtCostCode.removeAttr("data-original-title");

        WFD02160Ctr.txtCostCode.html('');
        WFD02160Ctr.txtCostCode.val(WFD02160Ctr.WFD02160_COST_CODE.val());
        WFD02160Ctr.txtCostCode.attr({
            "data-toggle": "tooltip",
            "data-placement": "top",
            "data-original-title": WFD02160Ctr.WFD02160_COST_CODE.val() + "-" + WFD02160Ctr.WFD02160_COST_NAME.val()
        })

        WFD021600setDisableButton();
    }
}

var WFD021600setDisableButton = function () {
    WFD02160Ctr.btnWFD02160View.prop('disabled', false);
    WFD02160Ctr.btnWFD02160Delete.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Print.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Edit.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Save.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Cancel.prop('disabled', true);
    // upload
    WFD02160UplaodCtrol.BtnBrowse.prop('disabled', true);
    WFD02160UplaodCtrol.BtnUpload.prop('disabled', true);
}
var setInitTBCostCode = function () {



    if (WFD02160_CONST.DefaultCostCode != "") {
        WFD02160Ctr.txtCostCode.removeAttr("data-toggle");
        WFD02160Ctr.txtCostCode.removeAttr("data-placement");
        WFD02160Ctr.txtCostCode.removeAttr("title");
        WFD02160Ctr.txtCostCode.removeAttr("data-original-title");

        WFD02160Ctr.txtCostCode.html('');
        WFD02160Ctr.txtCostCode.val(WFD02160_CONST.DefaultCostCode);
        WFD02160Ctr.txtCostCode.attr({
            "data-toggle": "tooltip",
            "data-placement": "top",
            "data-original-title": WFD02160_CONST.DefaultCostCode + "-" + WFD02160_CONST.DefaultCostName
        });

    }

}

var setAllButtonenable = function () {
    WFD02160Ctr.btnWFD02160View.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Delete.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Print.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Edit.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Save.prop('disabled', true);
    WFD02160Ctr.btnWFD02160Cancel.prop('disabled', true);

    // upload
    WFD02160UplaodCtrol.BtnBrowse.prop('disabled', true);
    WFD02160UplaodCtrol.BtnUpload.prop('disabled', true);

    WFD02160Ctr.WFD02160GetCostCenter.prop('disabled', true);
    WFD02160Ctr.txtCostCode.prop('disabled', true);

    WFD02160Ctr.GroupDropdownSubType.children().prop('disabled', true);


    WFD02160Ctr.GroupSubType.hide();
}

function clearobjecttoInitial() {
    WFD02160Ctr.txtCostCode.removeAttr("data-toggle");
    WFD02160Ctr.txtCostCode.removeAttr("data-placement");
    WFD02160Ctr.txtCostCode.removeAttr("title");
    WFD02160Ctr.txtCostCode.removeAttr("data-original-title");
    $('#imgWFD02160MapLayout div').remove();
    onWFD02160LoadMapLayout(WFD02160_CONST.DefaultImage);


}

var InitialButton = function (mode) {
    setAllButtonenable();
    // clear tooltip


    switch (mode) {
        case "init":
            clearobjecttoInitial();

            if (IsInit == "Y") {
                setInitTBCostCode();
                IsInit = "N";
            }


            WFD02160Ctr.btnWFD02160View.prop('disabled', false);
            WFD02160Ctr.WFD02160GetCostCenter.prop('disabled', false);
            WFD02160Ctr.txtCostCode.prop('disabled', false);
            break;
        case "view":
            WFD02160Ctr.btnWFD02160View.prop('disabled', false);
            WFD02160Ctr.btnWFD02160Delete.prop('disabled', false);
            WFD02160Ctr.btnWFD02160Print.prop('disabled', false);
            WFD02160Ctr.btnWFD02160Edit.prop('disabled', false);
            // upload
            WFD02160UplaodCtrol.BtnBrowse.prop('disabled', false);
            WFD02160UplaodCtrol.BtnUpload.prop('disabled', false);

            // dialog cost center
            WFD02160Ctr.WFD02160GetCostCenter.prop('disabled', false);
            WFD02160Ctr.txtCostCode.prop('disabled', false);

            //Add by Surasith
            WFD02160Ctr.GroupDropdownSubType.children().prop('disabled', false);

            // Load sub type
            WFD02160Ctr.GroupSubType.show();
            GetSubType();
            GetItemBySubType();
            WFD02160Ctr.WFD02160SubType.change(GetItemBySubType);

            break;
        case "edit":
            WFD02160Ctr.GroupSubType.show();
            WFD02160Ctr.btnWFD02160Save.prop('disabled', false);
            WFD02160Ctr.btnWFD02160Cancel.prop('disabled', false);

            WFD02160Ctr.GroupDropdownSubType.children().prop('disabled', false);

            DragDrop();
            // Load sub type
            // GetSubType();
            // GetItemBySubType();
            // WFD02160Ctr.WFD02160SubType.change(GetItemBySubType);

            break;
        default:
            setAllButtonenable();
            break;
    }
}

function clearTempSubType() {

    WFD02160Ctr.WFD02160SubType.html('');//Clear temp subtype
    $('#WFD02160ItemSubType table tbody').html('');//Clear temp table in subtype
    WFD02160Ctr.WFD02160ItemSubType.html(''); //Clear header table

    $('#imgWFD02160MapLayout div').remove();// clear map 

}
// Cost center dialog
var WFD02160GetModelCostCenter = function () {
    WFD02160Ctr.WFD02160_COST_CODE.addClass('COMSCCCostCodeModal');
    WFD02160Ctr.WFD02160_COST_NAME.addClass('COMSCCCostNameModal');
    var COMPANY = WFD02160Ctr.WFD02160_COMPANY.val();
    $("#COMPANY_").val(COMPANY);
    WFD02160Ctr.COMSCCSearchCostCodeModal.appendTo("body").modal('show');
    //$('#WFD02160GetCostCenter').on('show.bs.modal', function (e) {
    //    var yourparameter = e.relatedTarget.dataset.yourparameter;
    //    // Do some stuff w/ it.
    //});
}

// Browse file button click event : Open select file dialog
var onWFD02160FileUploadButtonClick = function () {
    WFD02160UplaodCtrol.File.click();
}
// Binding Asset upload file's  controls event
var bindingWFD02160FileUploadControlEvents = function () {
    WFD02160UplaodCtrol.Textbox.blur(onWFD02160FileUploadTBBlur);
    WFD02160UplaodCtrol.File.change(onWFD02160FileUploadFileChange);
}

var onWFD02160FileUploadTBBlur = function () {
    if (WFD02160UplaodCtrol.Textbox.val() == '') {
        WFD02160UplaodCtrol.File.val('');
    } else {
        WFD02160UplaodCtrol.Textbox.val(getFilePathFromWFD02160FileUploadFile());
    }
}
// File input value change event : Set file path to textbox
var onWFD02160FileUploadFileChange = function () {
    WFD02160UplaodCtrol.Textbox.val(getFilePathFromWFD02160FileUploadFile());
}

// get file path from file input
var getFilePathFromWFD02160FileUploadFile = function () {
   
    return WFD02160UplaodCtrol.File.val().replace(/C:\\fakepath\\/i, '');
}

//---------- Upload file  
var onWFD02160UploadButtonClick = function () {
    ClearMessageC();
   
    if (WFD02160UplaodCtrol.Textbox.val() != '') {
        loadConfirmAlert(WFD02160_Message.ConfirmUploadPhoto, function (result) {
            if (!result) {
                return;
            }
            var fileUpload = WFD02160UplaodCtrol.File.get(0);
            var files = fileUpload.files;

            var ModelValidateFile = {
                FunctionID: 'WFD02160',
                Folder: 'MAP',
                FileName: files[0].name,
                FileSize: files[0].size
            }
            console.log("Suraist");
            console.log(files[0].size);
            ajax_method.Post(URLWFD02160_CONST.CHECK_UPLOAD_FILE, ModelValidateFile, false, function (result) {
                if (!result) {// true
                    return;
                }
                // Create FormData object  
                var con = new FormData();
                con.append("COST_CODE", WFD02160Ctr.txtCostCode.val());
                con.append("UPDATE_DATE", "ForUpdateConcerrency");
                // Looping over all files and add it to FormData object  
                for (var i = 0; i < files.length; i++) {
                    con.append('FILE', files[i]);
                }
                ajax_method.PostFile(URLWFD02160_CONST.UpdateMapLayout, con, function (result) {
                    if (result != null) {
                        if (result.Messages == null) {
                            WFD02160UplaodCtrol.Textbox.val('');//set defalse blank 
                            onWFD02160ViewButtonClick(WFD02160UploadSuccess);
                        }
                    }
                }, null);
            }, null);



        });
    }
    else {
        AlertTextErrorMessage(WFD02160_Message.NoSelectFile);
    }

}

//Load Map layout
var onWFD02160LoadMapLayout = function (mapPath) {
  
    console.log(mapPath);
    WFD02160Ctr.imgWFD02160MapLayout.removeClass("imgWFD02160MapLayout");
    WFD02160Ctr.imgWFD02160MapLayout.css({
        "name": "imgWFD02160MapLayout",
        "background": "url(" + mapPath + ")",
        "background-position-x": "0%",
        "background-position-y": "0%",
        "background-repeat": "no-repeat",
        "background-size": "100% 100%",
        "background-color": "transparent"
    });
}


//btn View click
var onWFD02160ViewButtonClick = function (callback) {
    InitialButton("init");
    clearTempSubType();

    //var param = { ASSET_NO: URL_CONST.ASSET_NO};
    var Param = {
        COST_CODE: WFD02160Ctr.txtCostCode.val(),
        PLANT_CODE: WFD02160Ctr.WFD02160_COMPANY.val()
    }
   
    ajax_method.Post(URLWFD02160_CONST.ViewMapLocation, Param, true, function (result) {
        if (result != null) {
          
            onWFD02160LoadMapLayout(result.MapLayOut.MAP_PATH);
           
    
            WFD02160Ctr.txtWFD02160MapLayoutFullPath.val(result.MapLayOut.MAP_LOCATION_FULL_PATH);

            InitialButton("view");
            WFD02160LoadMapPoint(callback); // Load map point
            //$('.boxMinor').removeClass('hidden');
          
            //onWFD02160ViewButtonClick
        }
    }, null);


}


var GetSubType = function () {
    ajax_method.Post(URLWFD02160_CONST.WFD02610_GetSubType, '', true, function (result) {
        if (result != null) {

            WFD02160Ctr.WFD02160SubType.html('');
            //WFD02160Ctr.WFD02160ItemSubType.html(''); //Clear temp
            $.each(result, function (i) {
                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].CODE + " " + '' + '</option>';

                console.log(optionhtml);
                WFD02160Ctr.WFD02160SubType.append(optionhtml);
            });
        }

    }, null);
   
}

var GetItemBySubType = function () {

    var sub_typeSelected = WFD02160Ctr.WFD02160SubType.val();
    var sub_typeSelected_ = WFD02160Ctr.WFD02160SubType.text();
    WFD02160Ctr.WFD02160ItemSubType.html(''); //Clear temp

    //if (sub_typeSelected != null && sub_typeSelected != 'All') {
    WFD02160Ctr.WFD02160ItemSubType.html(GenerateHeadertable(WFD02160Ctr.WFD02160SubType.val()))
    //}
    //}
    if (WFD02160_Items.AllData.length > 0) {
        // generate header table  
        //if (sub_typeSelected != null && sub_typeSelected != 'All') {2017-05-17
        //Load insert data in subtype        
        GenerateListPerSubtype(sub_typeSelected); // bind data in subtype table 
      
        //}
    }
    else {
        //get item
        var con = {
            COST_CODE: WFD02160Ctr.txtCostCode.val()
        }
        ajax_method.Post(URLWFD02160_CONST.ViewMapPoint, con, true, function (result) {
            if (result != null) {
                var content = '';
                var values = new Array();
                WFD02160_Items.AllData = [];
                $.each(result, function (i) {
                    WFD02160_Items.AllData.push(result[i]);
                });
                //if (sub_typeSelected != null && sub_typeSelected != 'All') {2017-05-17
                //Load insert data in subtype        
               // GenerateListPerSubtype(sub_typeSelected); // bind data in subtype table 
                //}
            }
        }, null);

    }
}


var GenerateHeadertable = function (subtype_name) {

    // subtype_name = [M/C] Machine Number 
    //E/C Asset No.
    if (subtype_name == 'M/C') {
        subtype_name = '  [M/C] Machine Number ';
    } else {
        if (subtype_name == null || subtype_name == 'All') {
            subtype_name = 'Asset No. ';
        } //else {
            //subtype_name = '  [' + subtype_name + '] Asset No.';
        //}
    }
    var content = '';
    //Pitiphat CR-B-011 201709 Start
    content += '<table class="table table-bordered table-hover table-th-center fixedHeaderTB"  style="font-size: 11px;">';
    content += '  <thead class="bg-gray-light" style="padding-right: 19px;">';
    content += '  <tr>';
    content += '  <td style="text-align: center; border-right-color: rgb(204, 204, 204); width: 50px;">Items</td>';
    content += '  <td style="text-align: center; border-right-color: rgb(204, 204, 204); width: 170px;">' + 'Asset No.' + '</td>';
    content += ' </tr>';
    content += ' <tbody>';
    content += '</tbody>';
    content += '</table>';
    //Pitiphat CR-B-011 201709 End
    return content;

}

function GenerateListPerSubtype(MINOR_CATEGORY) {
  
    //if ($('#WFD02160SubType').has('option').length > 0) {
    //    alert("55555");
    //}
  
    var MINOR_CATEGORY = WFD02160Ctr.WFD02160SubType.val();
    var MINOR_CATEGORY4 = WFD02160Ctr.WFD02160SubType.text();
    var MINOR_CATEGORY_ = $(".multiselect-selected-text").text();7
    var options = $('#WFD02160SubType > option:selected').length;
 
   
    var content = '';
    $('#WFD02160ItemSubType table tbody').html('');//Clear temp
    if (WFD02160_Items.AllData.length != 0) {
     
        WFD02160_Items.AllData.sort(function (a, b) {
            return a.ITEMS - b.ITEMS;
        });
    }
    for (var i = 0; i < WFD02160_Items.AllData.length; i++) {
        if (WFD02160_Items.AllData[i].SELECTED == 'N') {
            if (MINOR_CATEGORY_.match(/All.*/) && MINOR_CATEGORY4 != null && options == 0) {// == 'All selected') {
                var checkobject = WFD02160_Items.AllData[i];
                if (checkobject != undefined) {
                    switch (WFD02160_Items.AllData[i].BG_COLOR) {
                        case "red":
                            content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:red;" data-toggle="tooltip" data-placement="top" '
                            content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' +WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                            break;
                        case "orange":
                            content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:yellow;" data-toggle="tooltip" data-placement="top" '
                            content += ' title="' + WFD02160_Items.AllData[i].LOCATION_NAME + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                            break;
                        case "yellow":
                            content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:yellow;" data-toggle="tooltip" data-placement="top" '
                            content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                            break;
                        case "black":
                            content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:black;color:white;" data-toggle="tooltip" data-placement="top" '
                            content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                            break;
                        default:
                            content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" data-toggle="tooltip" data-placement="top" '
                            content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                            break;
                    }
                   // Pitiphat CR-B-011 201709
                    content += '<td class="text-center item" style="width: 50px;">' + leftPad(WFD02160_Items.AllData[i].ITEMS, 2) + '</td>';
                    content += '<td style="width: 170px;">' + WFD02160_Items.AllData[i].MACHINE_NUMBER + '</td>';
                    content += '</tr>';
                  
                }
            } else if (MINOR_CATEGORY != "" && MINOR_CATEGORY != null && options != 0) {
                var checkobject = WFD02160_Items.AllData[i];
                MINOR_CATEGORY.forEach(function (MINOR_CATEGORY) {
                    if (WFD02160_Items.AllData[i].MINOR_CATEGORY == MINOR_CATEGORY) {
                        if (checkobject != undefined) {
                            switch (WFD02160_Items.AllData[i].BG_COLOR) {
                                case "red":
                                    content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:red;" data-toggle="tooltip" data-placement="top" '
                                    content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                                    break;
                                case "orange":
                                    content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:yellow;" data-toggle="tooltip" data-placement="top" '
                                    content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                                    break;
                                case "yellow":
                                    content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:yellow;" data-toggle="tooltip" data-placement="top" '
                                    content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                                    break;
                                case "black":
                                    content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" style="background-color:black;color:white;" data-toggle="tooltip" data-placement="top" '
                                    content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                                    break;
                                default:
                                    content += '<tr  class="wfd02160-asset-item' + WFD02160_Items.AllData[i].Display + '" data-toggle="tooltip" data-placement="top" '
                                    content += ' title="' + WFD02160_Items.AllData[i].ASSET_NAME + ' ' + WFD02160_Items.AllData[i].MINOR_CATEGORY + '"  data-index="' + WFD02160_Items.AllData[i].ITEMS + '" data-item="' + WFD02160_Items.AllData[i].ITEMS + '" data-asset-no="' + WFD02160_Items.AllData[i].ASSET_NO + '" >';
                                    break;
                            }
                            //Pitiphat CR-B-011 201709
                            content += '<td class="text-center item" style="width: 50px;">' + leftPad(WFD02160_Items.AllData[i].ITEMS, 2) + '</td>';
                            content += '<td style="width: 170px;">' + WFD02160_Items.AllData[i].MACHINE_NUMBER + '</td>';
                            content += '</tr>';
                        }
                      
                    }
                });
            }//check subtype
           else if (options == 0) {
               MINOR_CATEGORY.forEach(function (MINOR_CATEGORY) {
                   if (WFD02160_Items.AllData[i].MINOR_CATEGORY == MINOR_CATEGORY) {
                       if (checkobject != undefined) {
                           content += '<td class="text-center item" style="width: 50px;">' + leftPad(WFD02160_Items.AllData[i].ITEMS, 2) + '</td>';
                           content += '<td style="width: 170px;">' + WFD02160_Items.AllData[i].MACHINE_NUMBER + '</td>';
                           content += '</tr>';
                       }

                   }
               });
            }
        }//check selected
    }
    $('#WFD02160ItemSubType table tbody').html(content);

    
    DragDrop();
}

function GenerateDataAlreadySelected() {
    if (WFD02160_Items.AllData.length != 0) {
        WFD02160_Items.AllData.sort(function (a, b) {
            return a.SEQ - b.SEQ || a.ITEMS.localeCompare(b.ITEMS);
        });
    }
    $.each(WFD02160_Items.AllData, function (i) {

        //Pitiphat CR-B-011 201709
        var content = '';
        var _iTems = '';
        var _asSetNo = '';
        var _loCationName = '';
        var _maChineNumber = '';
        var _bgColor = '';
        var _yPoint = '';
        var _xPoint = '';
        var _deGree = '';
        var _asSetName = '';

        if (WFD02160_Items.AllData[i].SELECTED == 'Y') {
            var checkobject = WFD02160_Items.AllData[i];
            if (checkobject != undefined) {

                //Pitiphat CR-B-011 201709 Start
                _iTems = WFD02160_Items.AllData[i].ITEMS;
                _asSetNo = WFD02160_Items.AllData[i].ASSET_NO;
                _loCationName = WFD02160_Items.AllData[i].LOCATION_NAME;
                _maChineNumber = WFD02160_Items.AllData[i].MACHINE_NUMBER;
                _bgColor = WFD02160_Items.AllData[i].BG_COLOR;
                _yPoint = WFD02160_Items.AllData[i].Y_POINT;
                _xPoint = WFD02160_Items.AllData[i].X_POINT;
                _deGree = WFD02160_Items.AllData[i].DEGREE;
                _asSetName = WFD02160_Items.AllData[i].ASSET_NAME;
                _minorCategory = WFD02160_Items.AllData[i].MINOR_CATEGORY;

                content = GenerateContentOnMap(_iTems, _asSetNo, _loCationName, _maChineNumber, _bgColor, _yPoint, _xPoint, _deGree, '1',_asSetName,_minorCategory);
                //Pitiphat CR-B-011 201709 End

                $('#imgWFD02160MapLayout').append(content);

            }

            if (WFD02160_Items.AllData[i].SEQ == null || WFD02160_Items.AllData[i].MACHINE_NUMBER == '') {
                _Seq = 0;
            } else {
                _Seq = WFD02160_Items.AllData[i].SEQ;
            }
            if (WFD02160Ctr.WFD02160_SEQ.val() == null || WFD02160Ctr.WFD02160_SEQ.val() == '') {
                _SeqOld = 0;
            } else {
                _SeqOld = WFD02160Ctr.WFD02160_SEQ.val();
            }
            if (parseInt(_SeqOld) < parseInt(_Seq)) {
                WFD02160Ctr.WFD02160_SEQ.val(_Seq);
            }

        }
    });
}

function RemoveItem(index, _valItem) {
    SetCurrentDataOnMap(index, 'N', _valItem);

    $("#imgWFD02160MapLayout").find('div.wfd02160-asset-item' + index).remove(); //Pitiphat CR-B-011 201709
    $("#imgWFD02160MapLayout").find('div.tooltip').remove();
    GenerateListPerSubtype(WFD02160Ctr.WFD02160SubType.val());
}


var DragDrop = function () {

    if (WFD02160Ctr.btnWFD02160Save.is(':disabled')) {
        return;
    }

    $("#WFD02160ItemSubType table tbody tr").draggable({
        containment: WFD02160Ctr.imgWFD02160MapLayout,
        helper: "clone",
        revert: "invalid",
        start: function (event, ui) {
            //c.tr = this;
            //c.helper = ui.helper;
            $("#WFD02160ItemSubType table tbody").find('div.tooltip').remove();
            $("#imgWFD02160MapLayout").find('div.tooltip').remove();

        },
        helper: function (e, ui) {

            $("#WFD02160ItemSubType table tbody").find('div.tooltip').remove();
            $("#imgWFD02160MapLayout").find('div.tooltip').remove();

            var item = $(this).clone(true);
            var index = item.attr('data-index');

            if (item[0].cells.length > 3) {
                item[0].cells[1].outerHTML = "";
            }
            if (item[0].cells.length > 2) {
                item[0].cells[2].outerHTML = "";
            }
           
            item.addClass('boxAssetHelper');
            item.append('<div class="wfd02160-remove" onclick="RemoveItem(' + index + ',\'' + index + '\')">x</div>');

            return $(this).clone(true).html(item); //Replaced $(ui) with $(this)

        }
    });

    $('#imgWFD02160MapLayout').droppable({
        drop: function (event, ui) {
            $("#WFD02160ItemSubType table tbody").find('tr.tooltip').remove();
            $("#imgWFD02160MapLayout").find('div.tooltip').remove();
            var $newPosX = parseInt(ui.offset.left) - parseInt($(this).offset().left);
            var $newPosY = parseInt(ui.offset.top) - parseInt($(this).offset().top);

            var item = $(ui.helper.context);

            var _valItem = item.attr('data-index');// item[0].cells[0].innerText;

            //Pitiphat CR-B-011 201709 Start
            var indexs = WFD02160_Items.AllData.map(function (o) { return o.ITEMS; }).indexOf(_valItem);

            var data = WFD02160_Items.AllData[indexs];

            if ((data.Y_POINT == null && data.X_POINT == null) || (data.Y_POINT == "" && data.X_POINT == "")) {
                var _iTems = data.ITEMS;
                var _asSetNo = data.ASSET_NO;
                var _loCationName = data.LOCATION_NAME;
                var _maChineNumber = data.MACHINE_NUMBER;
                var _bgColor = data.BG_COLOR;
                var _deGree = data.DEGREE;
                var _asSetName = data.ASSET_NAME;
                var _minorCategory = data.MINOR_CATEGORY;

                var content = GenerateContentOnMap(_iTems, _asSetNo, _loCationName, _maChineNumber, _bgColor, $newPosY, $newPosX, _deGree, '1', _asSetName,_minorCategory);

                $('#imgWFD02160MapLayout').append(content);
                $("#WFD02160ItemSubType table tbody").find('tr.wfd02160-asset-item' + _iTems).remove();
                onMapDragDrop();
            }
            else {

                item.appendTo($('#imgWFD02160MapLayout'));
                $(ui.helper.context).css('top', $newPosY);
                $(ui.helper.context).css('left', $newPosX);
            }
            //Pitiphat CR-B-011 201709 End

            data.X_POINT = $newPosX;
            data.Y_POINT = $newPosY;

            var _Seq;
            if (WFD02160Ctr.WFD02160_SEQ.val() == null || WFD02160Ctr.WFD02160_SEQ.val() == '') {
                _Seq = 0;
            } else {
                _Seq = WFD02160Ctr.WFD02160_SEQ.val();
            }
            _Seq = parseInt(_Seq) + parseInt(1);
            data.SEQ = _Seq;
            WFD02160Ctr.WFD02160_SEQ.val(_Seq);

            WFD02160_Items.AllData[indexs] = data;
            SetCurrentDataOnMap(indexs, 'Y', _valItem);

        },
    });
}
var onMapDragDrop = function () {

    $("#imgWFD02160MapLayout div").draggable({ //Pitiphat CR-B-011 201709
        //containment: WFD02160Ctr.imgWFD02160MapLayout,
        //revert: "invalid",
        //start: function (event, ui) {
            //c.tr = this;
            //c.helper = ui.helper;
            //$("#WFD02160ItemSubType table tbody").find('tr.tooltip').remove();
            ///$("#imgWFD02160MapLayout").find('div.tooltip').remove();
            ///$("#imgWFD02160MapLayout div").find('div.tooltip').remove();
            ///$("#imgWFD02160MapLayout div").find('tooltip').remove(); //Pitiphat CR-B-011 201709
          
        //}
    });

    //$('#imgWFD02160MapLayout').droppable({

    //    drop: function (event, ui) {
    //        $("#WFD02160ItemSubType table tbody").find('div.tooltip').remove();
    //        $("#imgWFD02160MapLayout").find('div.tooltip').remove();
            
    //        var $newPosX = parseInt(ui.offset.left) - parseInt($(this).offset().left);
    //        var $newPosY = parseInt(ui.offset.top) - parseInt($(this).offset().top);

    //        var item = $(ui.helper.context);

    //        var _valItem = item.attr('data-index');//item[0].cells[0].innerText;

    //        //Pitiphat CR-B-011 201709 Start
    //        var indexs = WFD02160_Items.AllData.map(function (o) { return o.ITEMS; }).indexOf(_valItem);

    //        var data = WFD02160_Items.AllData[indexs];

    //        if ((data.Y_POINT == null && data.X_POINT == null) || (data.Y_POINT == "" && data.X_POINT == "")) {
    //            var _iTems = data.ITEMS;
    //            var _asSetNo = data.ASSET_NO;
    //            var _loCationName = data.LOCATION_NAME;
    //            var _maChineNumber = data.MACHINE_NUMBER;
    //            var _bgColor = data.BG_COLOR;
    //            var _deGree = data.DEGREE;
    //            var _asSetName = data.ASSET_NAME;
    //            var _minorCategory = data.MINOR_CATEGORY;

    //            var content = GenerateContentOnMap(_iTems, _asSetNo, _loCationName, _maChineNumber, _bgColor, $newPosY, $newPosX, _deGree, '1',_asSetName,_minorCategory);

    //            $('#imgWFD02160MapLayout').append(content);
    //            $("#WFD02160ItemSubType table tbody").find('tr.wfd02160-asset-item' + _iTems).remove();
    //            alert('APCED003_');
    //            onMapDragDrop();
    //        }
    //        else {

    //            item.appendTo($('#imgWFD02160MapLayout'));
    //            $(ui.helper.context).css('top', $newPosY);
    //            $(ui.helper.context).css('left', $newPosX);
    //            alert('APCED003_2');
    //        }
    //        //Pitiphat CR-B-011 201709 End

    //        data.X_POINT = $newPosX;
    //        data.Y_POINT = $newPosY;

    //        var _Seq;
    //        if (WFD02160Ctr.WFD02160_SEQ.val() == null || WFD02160Ctr.WFD02160_SEQ.val() == '') {
    //            _Seq = 0;
    //        } else {
    //            _Seq = WFD02160Ctr.WFD02160_SEQ.val();
    //        }
    //        _Seq = parseInt(_Seq) + parseInt(1);
    //        data.SEQ = _Seq;
    //        WFD02160Ctr.WFD02160_SEQ.val(_Seq);

    //        WFD02160_Items.AllData[indexs] = data;
    //        SetCurrentDataOnMap(indexs, 'Y', _valItem);
    //        //   console.log('x: ' + $newPosY + ' y: ' + $newPosX);
    //        alert('APCED003_1');
    //    },
    //});
}

function SetCurrentDataOnMap(indexSelected, isSelected, _valItem) {
    if (WFD02160_Items.AllData.length > 0) {
        var indexs = WFD02160_Items.AllData.map(function (o) { return o.ITEMS; }).indexOf(_valItem);
        var data = WFD02160_Items.AllData[indexs];
        data.SELECTED = isSelected;
        if (isSelected == 'N') {
            data.X_POINT = '';
            data.Y_POINT = '';
            data.DEGREE = ''; //Pitiphat CR-B-011 201709
        }
        WFD02160_Items.AllData[indexs] = data;
    }
}

var getItemPosition = function () {
    console.log(WFD02160_Items.AllData);
    var datas = [];
    if (WFD02160_Items.AllData.length > 0) {
        for (var i = 0; i < WFD02160_Items.AllData.length; i++) {
            datas.push(WFD02160_Items.AllData[i]);
            datas[datas.length - 1].COST_CODE = WFD02160Ctr.txtCostCode.val();
            datas[datas.length - 1].EMP_CODE = WFD02160_CONST.UserLogon;
        }
    }
    console.log("DATA");
    console.log(datas);
    return datas;

    //return WFD02160_Items.AllData;
}

var WFD02160ChangeCostCode = function () {
    WFD02160Ctr.WFD02160_COST_CODE.val(WFD02160Ctr.txtCostCode.val());
    WFD021600setDisableButton();
}


//Edit button click
var onWFD02160EditButtonClick = function () {
    setValueTBCostCode();
    if (WFD02160Ctr.txtCostCode.val() != '') {

        InitialButton("edit");
        $('#imgWFD02160MapLayout div').remove(); //Pitiphat CR-B-011 201709
        GenerateDataAlreadySelected();
        onMapDragDrop();
    }
    else {
        AlertTextErrorMessage(WFD02160_Message.Err_NoInput);
    }
}

// Cancel button click
var onWFD02160CancelButtonClick = function () {
    ClearMessageC();
    loadConfirmAlert(WFD02160_Message.ConfirmCancel, function (result) {
        if (result) {
            onWFD02160ViewButtonClick('0');
        }
    });
}
// Print out report
var onWFD02160PrintButtonClick = function () {
    setValueTBCostCode();
    if (WFD02160Ctr.txtCostCode.val() != '') {
        //LFD02150_AssetNumberListWithPhoto
        batchLFD02170.Clear();
        batchLFD02170.Addparam(1, 'EMP_CODE', WFD02160_CONST.UserLogon)
        batchLFD02170.Addparam(2, 'COST_CODE', WFD02160Ctr.txtCostCode.val())
        batchLFD02170.StartBatch();
        InitialButton("view");
       
    }
    else {
        AlertTextErrorMessage(WFD02160_Message.Err_NoInput);
    }
}
//btn Delete 
var onWFD02160DeleteButtonClick = function () {
    ClearMessageC();
    setValueTBCostCode();
    if (WFD02160Ctr.txtCostCode.val() != '') {
        loadConfirmAlert(WFD02160_Message.ConfirmDelete, function (result) {
            if (result) {
                var Param = {
                    COST_CODE: WFD02160Ctr.txtCostCode.val(),
                    MAP_PATH: WFD02160Ctr.txtWFD02160MapLayoutFullPath.val()
                }
                ajax_method.Post(URLWFD02160_CONST.DeleteMapLayout, Param, true, function (result) {
                    if (result != null) {
                        onWFD02160ViewButtonClick(WFD02160DeleteSuccess);
                    }
                }, null);
            }
        });
    }
    else {
        AlertTextErrorMessage(WFD02160_Message.Err_NoInput);
    }
}
//btn Save 
function SavePage(remain) {

    if (remain == null || remain.length == 0) {
        onWFD02160ViewButtonClick(WFD02160SaveSuccess);
        return;
    }
    //console.log("Remain");
    //console.log(remain.length);
    var selected;

    if (remain.length >= 50)
        selected = remain.splice(1, 50);
    else {
        selected = remain;
        remain = null;
    }

    ajax_method.Post(URLWFD02160_CONST.SaveMapLayout, selected, true, function (result) {
        if (result != null) {
            SavePage(remain);
           
        }
    }, null)
}
var DELAY = 700, clicks = 0, timer = null
var onWFD02160SaveButtonClick = function () {
	clicks++
	
    ClearMessageC();
	if (clicks === 1) {
		timer = setTimeout(loadConfirmAlert(WFD02160_Message.ConfirmSave, function (result) {
		    //console.log(result);
		    
        if (result) {

            if (WFD02160Ctr.txtCostCode.val() != '') {
                var datas = getItemPosition();
                if (datas.length > 0) {
                    SavePage(datas);
                   
                } else {
                    alert('No select');
                }

            }
            else {
                AlertTextErrorMessage(WFD02160_Message.Err_NoInput);
            }
        }
			clicks = 0;
            }), DELAY);
	} else {
		    clearTimeout(timer);
            clicks = 0;
	}
	
 }

var WFD02160SaveSuccess = function () {
    AletTextInfoMessage(WFD02160_Message.SaveSuccess);
}

var WFD02160UploadSuccess = function () {
    AletTextInfoMessage(WFD02160_Message.UploadPhotoSuccess);
}

var WFD02160DeleteSuccess = function () {
    AletTextInfoMessage(WFD02160_Message.DeleteSuccess);
}
// Load data in view 
var WFD02160LoadMapPoint = function (callback) {
    //get item
    var con = {
        COST_CODE: WFD02160Ctr.txtCostCode.val()
    }
    ajax_method.Post(URLWFD02160_CONST.ViewMapPoint, con, true, function (result) {
        if (result != null) {
            var content = '';
            var values = new Array();
            WFD02160_Items.AllData = [];
            $.each(result, function (i) {
                WFD02160_Items.AllData.push(result[i]);
            });
            if (WFD02160_Items.AllData.length > 0) {
                GenerateViewMapPoint();
                GetItemBySubType();
            }
            if (typeof callback == 'function') {
                callback();
            }
        }
    }, null);
}

function GenerateViewMapPoint() {
    var _Seq = 0;
    var _SeqOld = 0;
    $('#imgWFD02160MapLayout div').remove(); //Pitiphat CR-B-011 201709
    if (WFD02160_Items.AllData.length > 0) {
        WFD02160_Items.AllData.sort(function (a, b) {
            return a.SEQ - b.SEQ || a.ITEMS.localeCompare(b.ITEMS);
        });
        $.each(WFD02160_Items.AllData, function (i) {

            //Pitiphat CR-B-011 201709
            var content = '';
            var _iTems = '';
            var _asSetNo = '';
            var _loCationName = '';
            var _maChineNumber = '';
            var _bgColor = '';
            var _yPoint = '';
            var _xPoint = '';
            var _deGree = '';
            var _asSetName = '';

            var checkobject = WFD02160_Items.AllData[i];
            if (checkobject != undefined) {
                if (WFD02160_Items.AllData[i].SELECTED == 'Y') {

                    //Pitiphat CR-B-011 201709 Start
                    _iTems = WFD02160_Items.AllData[i].ITEMS;
                    _asSetNo = WFD02160_Items.AllData[i].ASSET_NO;
                    _loCationName = WFD02160_Items.AllData[i].LOCATION_NAME;
                    _maChineNumber = WFD02160_Items.AllData[i].MACHINE_NUMBER;
                    _bgColor = WFD02160_Items.AllData[i].BG_COLOR;
                    _yPoint = WFD02160_Items.AllData[i].Y_POINT;
                    _xPoint = WFD02160_Items.AllData[i].X_POINT;
                    _deGree = WFD02160_Items.AllData[i].DEGREE;
                    _asSetName = WFD02160_Items.AllData[i].ASSET_NAME;
                    _minorCategory = WFD02160_Items.AllData[i].MINOR_CATEGORY;

                    content = GenerateContentOnMap(_iTems, _asSetNo, _loCationName, _maChineNumber, _bgColor, _yPoint, _xPoint, _deGree, '0',_asSetName,_minorCategory);
                    //Pitiphat CR-B-011 201709 End

                    $('#imgWFD02160MapLayout').append(content);


                    if (WFD02160_Items.AllData[i].SEQ == null || WFD02160_Items.AllData[i].MACHINE_NUMBER == '') {
                        _Seq = 0;
                    } else {
                        _Seq = WFD02160_Items.AllData[i].SEQ;
                    }
                    if (WFD02160Ctr.WFD02160_SEQ.val() == null || WFD02160Ctr.WFD02160_SEQ.val() == '') {
                        _SeqOld = 0;
                    } else {
                        _SeqOld = WFD02160Ctr.WFD02160_SEQ.val();
                    }
                    if (parseInt(_SeqOld) < parseInt(_Seq)) {
                        WFD02160Ctr.WFD02160_SEQ.val(_Seq);
                    }
                }// check selected
            }
        });
    }
}


function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

//Pitiphat CR-B-011 201709
function ChangeRotate(_this) {
    var _deGree = '';

    var _valItem = $(_this).attr('data-index');
    var indexs = WFD02160_Items.AllData.map(function (o) { return o.ITEMS; }).indexOf(_valItem);
    var data = WFD02160_Items.AllData[indexs];

    _deGree = data.DEGREE;

    if (_deGree == '90') {

        $(_this).removeClass("boxAssetVertical");
        $(_this).find("table").removeClass("boxAssetDegree90");
        $(_this).addClass("boxAsset");
        $(_this).find("table").addClass("boxAssetDegree0");
        $(_this).css({
            "height": "25px",
            "width": "76px",
            "font-size": "7px",
            "overflow": "hidden"
        });

        if (parseInt(data.X_POINT) >= parseInt('724')) {
            $(_this).css({
                "left": "724px",
                "top": data.Y_POINT + "px"
            });

            data.X_POINT = '724';
        }

        data.DEGREE = '0';
    }
    else {
        $(_this).removeClass("boxAsset");
        $(_this).find("table").removeClass("boxAssetDegree0");
        $(_this).addClass("boxAssetVertical");
        $(_this).find("table").addClass("boxAssetDegree90");
        $(_this).css({
            "height": "76px",
            "width": "25px",
            "font-size": "7px",
            "overflow": "hidden"
        });

        if (parseInt(data.Y_POINT) >= parseInt('374px')) {
            $(_this).css({
                "top": "374px",
                "left": data.X_POINT + "px"
            });

            data.Y_POINT = '374';
        }
        data.DEGREE = '90';
    }

    WFD02160_Items.AllData[indexs] = data;
};

//Pitiphat CR-B-011 201709
function SetTooltipSubString(_strText, _lengthSplit) {
    var _strHtml = '';
    if (_strText != null && _strText.length > 0) {
        if (_strText.length > _lengthSplit) {
            _strHtml = '<p title="" data-original-title="' + replaceAll(_strText, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + _strText.substring(0, _lengthSplit) + '..' + '</p>'
        }
        else if (_strText.length <= _lengthSplit) {
            _strHtml = '<p title="" data-original-title="' + replaceAll(_strText, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + _strText + '</p>'
        }
        else {
            _strHtml = _strText;
        }

    } else {
        _strHtml = _strText;
    }
    return _strHtml;
}

//Pitiphat CR-B-011 201709
function GenerateContentOnMap(_iTems, _asSetNo, _loCationName, _maChineNumber, _bgColor, _yPoint, _xPoint, _deGree, _staTus,_asSetName,_minorCategory) { //_staTus : 0 View, 1 Edit
    var content = '';
    var _divClass = '';
    var _tableClass = '';

    var $newPosX = _xPoint + "px";
    var $newPosY = _yPoint + "px";

    if (_deGree == '90') {
        _divClass = 'boxAssetVertical';
        _tableClass = 'boxAssetDegree90';
    }
    else {
        _divClass = 'boxAsset';
        _tableClass = 'boxAssetDegree0';
    }
    
    if (_staTus == '0') {
        //content += '<div style="background-color:#FF0000;">';
        content += '<div class="' + _divClass + '" ';
    }
    else if (_staTus == '1') {
        content += '<div class="' + _divClass + ' wfd02160-asset-item' + _iTems + '  ui-draggable ui-draggable-handle " ondblclick="ChangeRotate(this)" ';
    }
    
    content += ' style="top: ' + $newPosY + '; left: ' + $newPosX + ';border-style: none;" data-toggle="tooltip" data-placement="top"';
    //content += ' title="' + _loCationName + '"  data-index="' + _iTems + '" ';
    content += ' title="' + _asSetName + ' ' + _minorCategory + '"  data-index="' + _iTems + '" ';
    content += ' data-item="' + _iTems + '" data-asset-no="' + _asSetNo + '" >';
    content += '<table class="' + _tableClass + '">';
    switch (_bgColor) {
        case "red":
            content += '<tr style="background-color:red;border-style: dashed;" >';
            break;
        case "orange":
            content += '<tr style="background-color:yellow;border-style: dashed;" >';
            break;
        case "black":
            content += '<tr style="background-color:black;color:white;">';
            break;
        case "white":
            content += '<tr style="background-color:white;border-style: solid;" >';
            break;
        default:
            content += '<tr>'
            break;
    }
    if (_staTus == '0') {
        content += '<td style="text-align: center; line-height: 20px;">' + leftPad(_iTems, 2) + '&nbsp;' + '' + '</td>';//_maChineNumber
    }
    else if (_staTus == '1') {
        content += '<td class="text-center item" style="width: 20%;">' + leftPad(_iTems, 2) + '&nbsp;</td>';
        content += '<td style="width: 50%;">' + '' + '</td>';//_maChineNumber
        content += '<td style="width: 20%;" class="wfd02160-remove  ui-draggable ui-draggable-handle" onclick="RemoveItem(' + _iTems + ',\'' + _iTems + '\')">x</td>';
    }
    content += '</tr>';
    content += '</table>';
    content += '</div>';
    //content += '</div>';

    return content;
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

var GetCompany = function () {
  
    ajax_method.Post(URLWFD02160_CONST.GET_COMPANY_DDL, '', true, function (result) {
        if (result != null) {
            WFD02160Ctr.WFD01210COMPANY.html('');
            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
            WFD02160Ctr.WFD01210COMPANY.append(optionhtml1);

            $.each(result, function (i) {
                var optionhtml = '<option value="' + result[i].CODE + '">' + result[i].CODE + '</option>';

                WFD02160Ctr.WFD01210COMPANY.append(optionhtml);
            });
            //Controls.WFD01210DIVISTION.select2('open');
        }

    }, null);
  

}
var GetMinorCategore = function () {

    ajax_method.Post(URLWFD02160_CONST.GET_MinorCatagory_DDL, '', true, function (result) {
        if (result != null) {
            console.log(result);
            WFD02160Ctr.WFD01210MinorCategory.html('');
            console.log(WFD02160Ctr.WFD01210MinorCategory.html(''));
            var optionhtml1 = '<option value="">' + "&nbsp; " + '</option>';
            console.log(optionhtml1);
            WFD02160Ctr.WFD01210MinorCategory.append(optionhtml1);
            console.log(WFD02160Ctr.WFD01210MinorCategory.append(optionhtml1));
           
            $.each(result, function (i) {
                var optionhtml = '<option value="' + result[i].VALUE + '">' + result[i].VALUE + '</option>';

                WFD02160Ctr.WFD01210MinorCategory.append(optionhtml);
            });
            //Controls.WFD01210DIVISTION.select2('open');
        }

    }, null);

    //$(function () {
    //    var name = ['joe', 'mary', 'rose'];
    //    $.map(name, function (x) {
    //        return $('.multiselect').append("<option>" + x + "</option>");
    //    });

    //    $('.multiselect')
    //      .multiselect({
    //          allSelectedText: 'All',
    //          maxHeight: 200,
    //          includeSelectAllOption: true
    //      })
    //      .multiselect('selectAll', false)
    //      .multiselect('updateButtonText');
    //});
}