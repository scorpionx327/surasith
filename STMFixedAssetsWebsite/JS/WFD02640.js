﻿var WFD02640DialogCostcenter;
var WFD02640RequestType;
var WFD02640YEAR;
var WFD02640ROUND;
var WFD02640ASSET_LOCATION;
var WFD02640SV_CODE;
var WFD02640COST_CODE_SELECTED;
var WFD02640COST_CODE_SELECTED_AlreadySubmit;
var isPostBack = 0; // if isPostBack is = 0 is mean first load
var batchProcessLFD02660 = BatchProcess({
    BatchId: 'LFD02660', // 'LFD02660' 
    Description: 'Print out FA update scan status(count uncount) without cover page report',
    UserId: WFD02640SV_CODE

});
var ControlsWFD02640 = {
    WFD02640Comment_Dialog: $('#WFD02640Comment-dialog'),
    WFD02640ClassClickDetail: $('.asset_click'),
    WFD02640TextFile: $('#WFD02640TextFile'),
    WFD02640Attachment: $('#WFD02640Attachment'),
    WFD02640Comment: $('#WFD02640Comment'),
    WFD02640Okbtn: $('#WFD02640Okbtn'),
    WFD02640Report: $('.info-box-icon-detail'),
    WFD02640Browse: $('#WFD02640Browse'),
    WFD02640NoticeEmail: $('#WFD02640NoticeEmail'),
    WFD02640Document: $('#WFD01170txtDocNo'),
    WFD02640SvName: $('#WFD02640SvName'),
    lblWFD02640SvName: $('#lblWFD02640SvName'),
};
$(function () {

    batchProcessLFD02660.Initial();
    //get parameter
    if (GLOBAL.DOC_NO == '') {
        WFD02640RequestType = getUrlParameter('RequestType');
        WFD02640YEAR = getUrlParameter('YEAR');
        WFD02640ROUND = getUrlParameter('ROUND');
        WFD02640ASSET_LOCATION = getUrlParameter('ASSET_LOCATION');
        WFD02640SV_CODE = getUrlParameter('EMP_CODE');
    } else {
        WFD02640RequestType = GLOBAL.REQUEST_TYPE; //  getUrlParameter('RequestType');
        WFD02640YEAR = WFD02640_Parameter.YEAR;// getUrlParameter('YEAR');
        WFD02640ROUND = WFD02640_Parameter.ROUND;// getUrlParameter('ROUND');
        WFD02640ASSET_LOCATION = WFD02640_Parameter.ASSET_LOCATION; // getUrlParameter('ASSET_LOCATION');
        WFD02640SV_CODE = WFD02640_Parameter.SV_EMP_CODE;// getUrlParameter('EMP_CODE');
        if ((GLOBAL.STATUS == '90' || GLOBAL.STATUS == '99')) {
            // Incase Resubmit
            if (isPostBack == 0)
            {
                 SetInitialDataIncaseReSubmit(GLOBAL.DOC_NO);
            }
          
        }
    }


    WFD02640_onLoadScreen();
    ControlsWFD02640.WFD02640Comment_Dialog.appendTo($('body'));


    ControlsWFD02640.WFD02640NoticeEmail.on('click', function () {
        WFD02640_onNoticeButtonClick();
    });
    ControlsWFD02640.WFD02640Browse.on('click', function () {
        ControlsWFD02640.WFD02640Attachment.click();
    });


    ControlsWFD02640.WFD02640Attachment.on('change', function (e) {
        ControlsWFD02640.WFD02640TextFile.val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    ControlsWFD02640.WFD02640Okbtn.on('click', function () {
        WFD02640_SaveTemp();
    });



    

    ControlsWFD02640.lblWFD02640SvName.text('');
    if (WFD02640ASSET_LOCATION == "I") {
        ControlsWFD02640.lblWFD02640SvName.text('S/V Name :');
    } else {
        ControlsWFD02640.lblWFD02640SvName.text('Supplier :');
    }

    if (WFD02640_Parameter.IS_FA_ADMIN == 'Y') {
        ControlsWFD02640.WFD02640NoticeEmail.prop('disabled', false);
    } else {
        ControlsWFD02640.WFD02640NoticeEmail.prop('disabled', true);
    }



});

function SetInitialDataIncaseReSubmit (docNo)
{
    isPostBack = isPostBack + 1;
    var con = {
        DocNo: docNo
    };

    ajax_method.Post(WFD02640_URL.SetInitialDataIncaseReSubmit, con, true, function (result) {
    }, null);
}

var WFD02640_onNoticeButtonClick = function () {

    loadConfirmAlert(WFD02640_Message.ConfirmNotice, function (result) {
        if (result) {
            var con = {
                SV_CODE: WFD02640SV_CODE,
                YEAR: WFD02640YEAR,
                ROUND: WFD02640ROUND,
                EMP_CODE: WFD02640_Parameter.EMP_CODE,
                ASSET_LOCATION: WFD02640ASSET_LOCATION
            };

            ajax_method.Post(WFD02640_URL.NoticeEmail, con, true, function (result) {
                //function
                //if (result) {
                //    setTimeout(function () {
                //        AletTextInfoMessage(WFD02640_Message.NoticeSuccess);
                //    }, 2000);
                //}
            }, null);
        }
    });
}

var WFD02640_onReportButtonClick = function (costcenter) {
    loadConfirmAlert(WFD02640_Message.ConfirmReport, function (result) {
        if (result) {
            batchProcessLFD02660.Clear();
            batchProcessLFD02660.Addparam(1, 'YEAR', WFD02640YEAR)
            batchProcessLFD02660.Addparam(2, 'ROUND', WFD02640ROUND)
            batchProcessLFD02660.Addparam(3, 'ASSET_LOCATION', WFD02640ASSET_LOCATION)
            batchProcessLFD02660.Addparam(4, 'COST_CENTER_CODE', costcenter)
            batchProcessLFD02660.Addparam(5, 'DOCUMENT_NO', ControlsWFD02640.WFD02640Document.val())
            batchProcessLFD02660.StartBatch();

            //setTimeout(function () {
            //    AletTextInfoMessage(WFD02640_Message.ReportSuccess);
            //}, 2000);
        }


    });
}

var WFD02640_onLoadScreen = function () {

    var val_costList = new Array();
    var val_costList_have_docNo = new Array();

    var con = {
        COMPANY: GLOBAL.COMPANY,
        SV_CODE: WFD02640SV_CODE,
        EMP_CODE: WFD02640_Parameter.EMP_CODE,
        ISFAADMIN: WFD02640_Parameter.IS_FA_ADMIN,
        YEAR: WFD02640YEAR,
        ROUND: WFD02640ROUND,
        ASSET_LOCATION: WFD02640ASSET_LOCATION,
        DOCUMENT: GLOBAL.DOC_NO
    };
    ajax_method.Post(WFD02640_URL.onLoadScreen, con, true, function (result) {
        if (result != null) {
            if (result.SummaryTime != null && result.SummaryAsset != null && result.SummaryDetail) {
                ControlsWFD02640.WFD02640SvName.val(result.EMP_NAME)
                //time 
                $('#WFD02640TimePersen').html('<b>' + result.SummaryTime.PERSEN + '</b>'); // 100 % 
                $('#WFD02640TimeProgressBar').removeClass(result.SummaryTime.CLASS); //'progress-bar-red'
                $('#WFD02640TimeProgressBar').addClass(result.SummaryTime.CLASS); //'progress-bar-red'
                $('#WFD02640TimeProgressBar').css('width', result.SummaryTime.PERSEN); // 100 % 
                $('#WFD02640TimeStatus').html(result.SummaryTime.STATUS);//'Complete with delay'
                //asset
                //$('#WFD02640AssetIcon').removeClass(result.SummaryAsset.ICON); //fa fa-close
                //$('#WFD02640AssetIcon').addClass(result.SummaryAsset.ICON); //fa fa-close
                $('#WFD02640IconImg').attr('src', result.SummaryAsset.IMGSRC);

                $('#WFD02640AssetDescription').html(result.SummaryAsset.DESCRIPTION);//399/400
                $('#WFD02640AssetClass').removeClass(result.SummaryAsset.CLASS);//bg-red
                $('#WFD02640AssetClass').addClass(result.SummaryAsset.CLASS);//bg-red
                $('#WFD0260AssetProgressBar').css('width', result.SummaryAsset.PERSEN);//99%
                $('#WFD0260AssetDescription').html(result.SummaryAsset.PERSEN);//99%
                if (result.SummaryAsset.ICON != '') {
                    $('#WFD02640BorderIcon').removeClass('border-icon');
                    $('#WFD02640BorderIcon').addClass('border-icon');
                }
                //detail
                var content = "";

                result.SummaryDetail.forEach(function (item, index) {
                    var COMMENT = '';
                    var ATTACHMENT = '';
                    var CHECKED = '';
                    var DISABLED = '';
                    var ViewFile = '';
                    var RemoveFile = '';
                    var AddComment = '<button class="btn btn-xs btn-default asset_click" data-costcenter="' + item.COST_CENTER_CODE + '">Add comment</button>';
                    var checkAllow_dblClickinRow = 'asset_click';
                    if (item.COMMENT)
                        COMMENT = 'Comment : ' + item.COMMENT;

                    if (item.CHECKED == 'Y' )
                    {
                        val_costList_have_docNo.push(item.COST_CENTER_CODE);

                        CHECKED = 'CHECKED';
                      
                        DISABLED = 'disabled';
                        checkAllow_dblClickinRow = '';

                        AddComment = '';

                        if((GLOBAL.STATUS == '90' || GLOBAL.STATUS == '99'))
                        {
                            checkAllow_dblClickinRow = 'asset_click';
                            AddComment = '<button class="btn btn-xs btn-default asset_click" data-costcenter="' + item.COST_CENTER_CODE + '">Add comment</button>';

                            val_costList.push(item.COST_CENTER_CODE);
                        }
                    }
                    if (PERMISSION.MODE == 'V')
                    {
                      DISABLED = 'disabled';
                      checkAllow_dblClickinRow = '';
                      AddComment = '';
                    }

                    if (item.ATTACHMENT != '' && item.ATTACHMENT != null) {
                        ATTACHMENT = '<a download class="btn btn-sm btn-info" target="_blank" href="/WFD02640/DownloadFileTemp?FILENAME=' + item.ATTACHMENT + '&FunctionID=WFD02640&Type=STOCK"><span class="glyphicon glyphicon-download-alt"></span> Download file</a>';
                        ViewFile = ' ';
                        RemoveFile = ' ';
                      
                    }
                    if (PERMISSION.MODE == 'A' && (GLOBAL.STATUS == '90' || GLOBAL.STATUS == '99')) {//Re-Submit
                        if (item.ATTACHMENT != '' && item.ATTACHMENT != null) {
                            ATTACHMENT = '<a download class="btn btn-sm btn-info" target="_blank" style="display:none;" href="/WFD02640/DownloadFileTemp?FILENAME=' + item.ATTACHMENT + '&FunctionID=WFD02640&Type=STOCK"><span class="glyphicon glyphicon-download-alt"></span> Download file</a>';
                            ViewFile = '<p title="" data-original-title="View File" data-toggle="tooltip" data-placement="top"><a download class="btn btn-sm btn-white" target="_blank" href="/WFD02640/DownloadFileTemp?FILENAME=' + item.ATTACHMENT + '&FunctionID=WFD02640&Type=STOCK"><span class="glyphicon glyphicon-paperclip"></span></a></p>';

                            //RemoveFile = '<button id="' + item.COST_CENTER_CODE + 'RemoveFileTemp' + '" class="btn btn-xs btn-warning" type="button" onclick="WFD02640_DeleteFileTemp(\'' + item.COST_CENTER_CODE + '\')" class="btn btn-sm  btn-warning"><span class="glyphicon glyphicon-remove"></span> Remove file</button>';
                            RemoveFile = '<button id="' + item.COST_CENTER_CODE + 'RemoveFileTemp' + '" class="btn btn-sm btn-white" type="button" onclick="WFD02640_DeleteFileTemp(\'' + item.COST_CENTER_CODE + '\')" class="glyphicon glyphicon-remove" data-original-title="Remove File" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></button>';

                        }
                    } else {
                        if (item.ATTACHMENT != '' && item.ATTACHMENT != null) {
                            ATTACHMENT = '<a download class="btn btn-sm btn-info" target="_blank" href="/WFD02640/DownloadFileTemp?FILENAME=' + item.ATTACHMENT + '&FunctionID=WFD02640&Type=STOCK"><span class="glyphicon glyphicon-download-alt"></span> Download file</a>';
                            ViewFile = ' ';
                            RemoveFile = ' ';
                        }
                       
                    }
                    var costc = item.COST_CENTER_CODE;
                    var costN = item.COST_CENTER_NAME;
                    if (item.COST_CENTER_NAME.length > 20)
                    {
                        costN = costN.substring(0, 20) + '...';
                    }
                    var costdisplay = costN + '(' + costc + ')'; 

                    // Fix comment by Pawares M. 20180822
                    var commentDisWindow = $(window).width();
                    var commentDisDoc = $(document).width();
                    var commentFull = COMMENT;
                    var commentFLength = COMMENT.length;
                    var cutString = commentDisDoc / 15;

                    var commentTooltip = COMMENT; 

                    if (((commentDisWindow / 4) > commentFLength) && (cutString < commentFLength)) {
                        commentFull = commentFull.substring(0, cutString) + '...';
                    }

                    if (COMMENT == null || COMMENT == '') {
                        commentTooltip = '<span class="des-left" id="' + item.COST_CENTER_CODE + 'Comment">';
                    }
                    else {
                        commentTooltip = '<span class="des-left" data-original-title="' + item.COMMENT + '" data-toggle="tooltip" data-placement="top" id="' + item.COST_CENTER_CODE + 'Comment">';
                    }

                    //var optionhtml = '<option value="' + result.data[i].YEAR + '">' + result.data[i].YEAR + '</option>';
                    content += '<tr>'
                        + '<td class="text-center" style="width: 45px;"><input type="checkbox" class="WFD02640checkClass" name="WFD02640Ck[]" ' + CHECKED + ' ' + DISABLED + ' data-costcode="' + item.COST_CENTER_CODE + '" onclick="WFD02640_CheckBoxCheck(\'' + item.COST_CENTER_CODE + '\')" id="' + item.COST_CENTER_CODE + 'CheckBox"/></td>'
                                        + '<td>'
                                            + '<div class="info-box ' + item.STATUS + '" '
                                         //   + checkAllow_dblClickinRow +'" '
                                            + 'data-costcenter="' + item.COST_CENTER_CODE + '">'
                                            + '<span class="info-box-icon-detail" style="cursor:pointer" data-costcenter="' + item.COST_CENTER_CODE + '"><i class="fa fa-file"></i></span>'
                                            + '<div class="info-box-content">'
                                            + '<span class="info-box-text"><strong>&nbsp; &nbsp;</strong></span>'
                                            + '<div class="title">'
                                            + '<span class="text " data-toggle="tooltip" title="' + item.COST_CENTER_NAME + '">' + costdisplay + '</span>'
                                            + '<span class="total">' + item.TOTAL + '</span>'
                                            + '</div>'
                                            + '<div class="progress">'
                                            + '<div class="progress-bar" style="width: ' + item.PERSEN + '"></div>'
                                            + '</div>'
                                            + '<span class="progress-description">'
                                            + item.DESCRIPTION
                                            + '</span>'
                                            + '<div class="description">'
                        + commentTooltip
                        + commentFull
                                            + '</span>'
                                            + '<input type="text" class="form-control" id="' + item.COST_CENTER_CODE + 'TXTCOMMENT" name="' + item.COST_CENTER_CODE + 'TXTCOMMENT"  style="display:none" value="' + item.COMMENT + '">'
                                            + '<span class="des-right" id="' + item.COST_CENTER_CODE + 'RemoveFile">'
                                            + RemoveFile
                                            + '</span>'
                                            + '<span class="des-right" id="' + item.COST_CENTER_CODE + 'ViewFile">'
                                            + ViewFile
                                            + '</span>'
                                            + '<div><span class="des-right" id="' + item.COST_CENTER_CODE + 'Download">'
                                            + ATTACHMENT
                                            + '</span></div>'
                                            + '<span class="des-right NonChecked" id="' + item.COST_CENTER_CODE + 'AddComment">'
                                            + AddComment
                                            + '</span>'
                                            + '<input class="form-control" type="file" id="' + item.COST_CENTER_CODE + 'Attachment" name="' + item.COST_CENTER_CODE + 'Attachment" style="display:none" />'
                                            + '</div>'
                                            + '</div>'
                                            + '</div>'
                                        + '</td>'
                                    + '</tr>'

                });
                $('#WFD02640TableDetail').html(content);

                if(GLOBAL.DOC_NO !='')
                {
                    WFD02640COST_CODE_SELECTED =val_costList.join();
                } else
                {
                    WFD02640COST_CODE_SELECTED_AlreadySubmit = val_costList_have_docNo;
                }

                $('.info-box-icon-detail').on('click', function () {
                    var costcenter = $(this).data('costcenter');
                    WFD02640_onReportButtonClick(costcenter);
                });

                $('.asset_click').click(function () {
                 
                    ControlsWFD02640.WFD02640TextFile.val('');
                    ControlsWFD02640.WFD02640Attachment.val('');
                    ControlsWFD02640.WFD02640Comment.val('');
                  
                    if ($('#' + $(this).data('costcenter') + 'TXTCOMMENT').val() != "null" )
                    {
                        ControlsWFD02640.WFD02640Comment.val($('#' + $(this).data('costcenter') + 'TXTCOMMENT').val());
                    }                  
                    WFD02640DialogCostcenter = $(this).data('costcenter');
                    ControlsWFD02640.WFD02640Comment_Dialog.modal('show');
                });

                //$('#btnAddComment').click(function () {
                //    alert("TEST");
                //    $('#' + $(this).data('costcenter')).trigger("dblclick") ;
                //});

                $("input[name='WFD02640Ck[]']").click(function () {
                    var values = new Array();
                    $.each($("input[name='WFD02640Ck[]']:checked"), function () {
                        var checkHaveDocNo = false;
                        for (var i = 0; i < WFD02640COST_CODE_SELECTED_AlreadySubmit.length; i++) {
                            if(WFD02640COST_CODE_SELECTED_AlreadySubmit[i]== $(this).data('costcode'))
                            {
                                checkHaveDocNo = true;
                                i = WFD02640COST_CODE_SELECTED_AlreadySubmit.length; //exits loop                                
                            }
                        }
                        if (checkHaveDocNo == false)
                        {
                            values.push($(this).data('costcode'));
                        }
                      
                    });
                    WFD02640COST_CODE_SELECTED = values.join();
                   // console.log(WFD02640COST_CODE_SELECTED);
                });

            }
        }
        CheckPageLoaded = 'Y';
    }, null);
}

var WFD02640_SaveTemp = function () {
    var fileUpload = ControlsWFD02640.WFD02640Attachment.get(0);
    var files = fileUpload.files;
    var _removeFile = '';
    // Create FormData object  
    var con = new FormData();
    con.append('COMPANY', GLOBAL.COMPANY);
    con.append('DOCUMENT', ControlsWFD02640.WFD02640Document.val());
    con.append('YEAR', WFD02640YEAR);
    con.append('ROUND', WFD02640ROUND);
    con.append('ASSET_LOCATION', WFD02640ASSET_LOCATION);
    con.append('SV_CODE', WFD02640SV_CODE);
    con.append('COST_CENTER', WFD02640DialogCostcenter);
    con.append('COMMENT', ControlsWFD02640.WFD02640Comment.val());
    con.append('EMP_CODE', WFD02640_Parameter.EMP_CODE);
    con.append('GUID', GLOBAL.GUID);
    
    if (typeof WFD02640COST_CODE_SELECTED == 'undefined')
    {
        ControlsWFD02640.WFD02640Comment_Dialog.modal('hide');
    }

    if (WFD02640COST_CODE_SELECTED.indexOf(WFD02640DialogCostcenter) != -1) {
        con.append('SELECTED', 'Y');
    } else {
        con.append('SELECTED', 'N');
    }
    //con.append('SELECTED', 'N');

    var filename = '';
    if (files.length > 0) {
        con.append('FILE', files[0]);
        con.append('ATTACHMENT', files[0].name);
        var ext = files[0].name.split('.').pop();

        filename = WFD02640DialogCostcenter + WFD02640YEAR + WFD02640ROUND + WFD02640ASSET_LOCATION + '.' + ext
        var ModelValidateFile = {
            FunctionID: 'WFD02640',
            Folder: 'STOCK',
            FileName: files[0].name,
            FileSize: files[0].size
        }
        ajax_method.Post(WFD02640_URL.CHECK_UPLOAD_FILE, ModelValidateFile, false, function (result) {
            if (result) {// true 
                ajax_method.PostFile(WFD02640_URL.SaveTempRequestStock, con, function (result) {
                   // console.log(result);
                    if (result.ObjectResult) {
                        if (ControlsWFD02640.WFD02640Comment.val() != "") {
                            //$('#' + WFD02640DialogCostcenter + 'Comment').text('Comment:' + ControlsWFD02640.WFD02640Comment.val());
                            $('#' + WFD02640DialogCostcenter + 'TXTCOMMENT').val(ControlsWFD02640.WFD02640Comment.val());

                            // Fix comment by Pawares M. 20180822
                            var commentDisWindow = $(window).width();
                            var commentDisDoc = $(document).width();
                            var commentFull = ControlsWFD02640.WFD02640Comment.val();
                            var commentFLength = commentFull.length;
                            var cutString = commentDisDoc / 15;

                            if (((commentDisWindow / 4) > commentFLength) && (cutString < commentFLength)) {
                                commentFull = commentFull.substring(0, cutString) + '...';
                            }

                            $('#' + WFD02640DialogCostcenter + 'Comment').html('<span class="des-left" data-original-title="' + ControlsWFD02640.WFD02640Comment.val() + '" data-toggle="tooltip" data-placement="top">Comment : ' + commentFull + '</span>');


                        }
                        else {
                            $('#' + WFD02640DialogCostcenter + 'Comment').text('');
                            $('#' + WFD02640DialogCostcenter + 'TXTCOMMENT').val('');
                        }

                        if (ControlsWFD02640.WFD02640Attachment.val() != "") {
                            $('#' + WFD02640DialogCostcenter + 'Download').html('<p title="" data-original-title="Download File" data-toggle="tooltip" data-placement="top"><a download class="btn btn-sm btn-info" target="_blank" style="display:none;" href="/WFD02640/DownloadFileTemp?FILENAME=' + filename + '">Download file</a></p>');
                            $('#' + WFD02640DialogCostcenter + 'ViewFile').html('<p title="" data-original-title="View File" data-toggle="tooltip" data-placement="top"><a download class="btn btn-sm btn-white" target="_blank" href="/WFD02640/DownloadFileTemp?FILENAME=' + filename + '&FunctionID=WFD02640&Type=STOCK"><span class="glyphicon glyphicon-paperclip"></span></a></p>');
                            //$('#' + WFD02640DialogCostcenter + 'RemoveFile').html('<button id="' + WFD02640DialogCostcenter + 'RemoveFileTemp' + '" class="btn btn-xs btn-warning" type="button" onclick="WFD02640_DeleteFileTemp(\'' + WFD02640DialogCostcenter + '\')" class="btn btn-sm  btn-warning"><span class="glyphicon glyphicon-remove"></span> Remove file</button>');
                            //$('#' + WFD02640DialogCostcenter + 'Attachment').val(filename);

                            $('#' + WFD02640DialogCostcenter + 'RemoveFile').html('<button id="' + WFD02640DialogCostcenter + 'RemoveFileTemp' + '" class="btn btn-sm btn-white" type="button" onclick="WFD02640_DeleteFileTemp(\'' + WFD02640DialogCostcenter + '\')" data-original-title="Remove File" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></button>');



                            $('#' + WFD02640DialogCostcenter + 'Attachment').on('blur', function (e) {
                                $('#' + WFD02640DialogCostcenter + 'Attachment').val(filename);
                            });
                           
                        }
                        else {
                            $('#' + WFD02640DialogCostcenter + 'Download').html('');
                            //$('#' + WFD02640DialogCostcenter + 'Attachment').val('')
                        }
                    }

                    ControlsWFD02640.WFD02640Comment_Dialog.modal('hide');


                });
            } else {
                ControlsWFD02640.WFD02640Comment_Dialog.modal('hide');
            }
        });
    } else {
        ajax_method.PostFile(WFD02640_URL.SaveTempRequestStock, con, function (result) {
        //    console.log(result);
            if (result.ObjectResult) {
                if (ControlsWFD02640.WFD02640Comment.val() != "") {
                    //$('#' + WFD02640DialogCostcenter + 'Comment').text('Comment:' + ControlsWFD02640.WFD02640Comment.val());
                    $('#' + WFD02640DialogCostcenter + 'TXTCOMMENT').val(ControlsWFD02640.WFD02640Comment.val());

                    // Fix comment by Pawares M. 20180822
                    var commentDisWindow = $(window).width();
                    var commentDisDoc = $(document).width();
                    var commentFull = ControlsWFD02640.WFD02640Comment.val();
                    var commentFLength = commentFull.length;
                    var cutString = commentDisDoc / 15;

                    if (((commentDisWindow / 4) > commentFLength) && (cutString < commentFLength)) {
                        commentFull = commentFull.substring(0, cutString) + '...';
                    }

                    $('#' + WFD02640DialogCostcenter + 'Comment').html('<span class="des-left" data-original-title="' + ControlsWFD02640.WFD02640Comment.val() + '" data-toggle="tooltip" data-placement="top">Comment : ' + commentFull + '</span>');
                }
                else {
                    $('#' + WFD02640DialogCostcenter + 'Comment').text('');
                    $('#' + WFD02640DialogCostcenter + 'TXTCOMMENT').val('');
                }

                if (ControlsWFD02640.WFD02640Attachment.val() != "") {
                    $('#' + WFD02640DialogCostcenter + 'Download').html('<p title="" data-original-title="Download File" data-toggle="tooltip" data-placement="top"><a download class="btn btn-sm btn-info" target="_blank" style="display:none;" href="/WFD02640/DownloadFileTemp?FILENAME=' + filename + '">Download file</a></p>');
                    $('#' + WFD02640DialogCostcenter + 'ViewFile').html('<p title="" data-original-title="View File" data-toggle="tooltip" data-placement="top"><a download class="btn btn-sm btn-white" target="_blank" href="/WFD02640/DownloadFileTemp?FILENAME=' + filename + '&FunctionID=WFD02640&Type=STOCK"><span class="glyphicon glyphicon-paperclip"></span></a></p>');
                    //$('#' + WFD02640DialogCostcenter + 'RemoveFile').html('<button id="' + WFD02640DialogCostcenter + 'RemoveFileTemp' + '" class="btn btn-xs btn-warning" type="button" onclick="WFD02640_DeleteFileTemp(\'' + WFD02640DialogCostcenter + '\')" class="btn btn-sm  btn-warning"><span class="glyphicon glyphicon-remove"></span> Remove file</button>');

                    $('#' + WFD02640DialogCostcenter + 'RemoveFile').html('<button id="' + WFD02640DialogCostcenter + 'RemoveFileTemp' + '" class="btn btn-sm btn-white" type="button" onclick="WFD02640_DeleteFileTemp(\'' + WFD02640DialogCostcenter + '\')" data-original-title="Remove File" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></button>');


                }
                else {
                    $('#' + WFD02640DialogCostcenter + 'Download').html('');
                    //$('#' + WFD02640DialogCostcenter + 'Attachment').val('')
                }
            }

            ControlsWFD02640.WFD02640Comment_Dialog.modal('hide');


        });
    }




}

//var WFD02640GenerateApprover = function (callback) {
//   // console.log(WFD02640COST_CODE_SELECTED)
//    if (WFD02640COST_CODE_SELECTED == null || WFD02640COST_CODE_SELECTED =="") {
//        AlertTextErrorMessage(WFD02640_Message.ErrorGenApprover);
//        return;

var WFD02640_DisableGenerate = function () {
    $(".WFD02640checkClass").attr('disabled', 'disabled');
}

var WFD02640_EnableGenerate = function () {
    if ((GLOBAL.STATUS == '00') && (GLOBAL.DOC_NO == '')) {
        //   $(".WFD02640checkClass").removeAttr('disabled');//only case Submit
        $('#WFD02640TableDetail').find('.WFD02640checkClass:not(:checked)').removeAttr('disabled');
        var arr_WFD02640COST_CODE_SELECTED = WFD02640COST_CODE_SELECTED.split(',');
        for (var i = 0; i < arr_WFD02640COST_CODE_SELECTED.length; i++) {
            console.log("SELECTED:");
            console.log(arr_WFD02640COST_CODE_SELECTED[i]);
            $('#WFD02640TableDetail').find('.WFD02640checkClass').each(function () {
                var costCode = $(this).attr('data-costcode');
                if (arr_WFD02640COST_CODE_SELECTED[i] == costCode) {
                    //console.log(costCode);
                    $(this).removeAttr('disabled');//only case Submit
                }
            });
        }


    }

}

var WFD02640_GetAssetDataModel = function () {
    var datas = [];
    var _SummaryDetail = [];
    if (WFD02640COST_CODE_SELECTED != null) {
        var arr_WFD02640COST_CODE_SELECTED = WFD02640COST_CODE_SELECTED.split(',');
        $.each(arr_WFD02640COST_CODE_SELECTED, function (entry, item) {
            _SummaryDetail.push({
                "COST_CENTER_CODE": item,
                "CHECKED": 'Y'
            });
        });
    }

    var WFD02640Save = {
        COMPANY : GLOBAL.COMPANY,
        DOC_NO: GLOBAL.DOC_NO,
        USER_BY: GLOBAL.EMP_CODE,
        GUID: GLOBAL.GUID,
        UPDATE_DATE: GLOBAL.UPDATE_DATE,
        COST_CENTER_LIST: _SummaryDetail,
    }

    return WFD02640Save;
}

var WFD02640_DeleteFileTemp = function (Costcenter) {
    var _strMessageDel = WFD01270_Message.ConfirmDelete;
    _strMessageDel = _strMessageDel.replace("{1}", " Stock Doc Cost center: " + Costcenter);
    loadConfirmAlert(_strMessageDel, function (result) {
        if (result) {
            var ModelFile = {
                FunctionID: 'WFD02640',
                Folder: 'STOCK',
                FileName: $('#' + Costcenter + 'Attachment').val(),
                FileSize: 0
            }

            ajax_method.Post(WFD02640_URL.DELETE_FILE, ModelFile, false, function (result) {
                if (result.FileName == null || result.FileName == '') {
                    var ModelUpdate = {
                        COMPANY : GLOBAL.COMPANY,
                        DOCUMENT: GLOBAL.DOC_NO,
                        YEAR: WFD02640YEAR,
                        ROUND: WFD02640ROUND,
                        ASSET_LOCATION: WFD02640ASSET_LOCATION,
                        SV_CODE: WFD02640SV_CODE,
                        COST_CENTER: Costcenter,
                        EMP_CODE: WFD02640_Parameter.EMP_CODE,
                        GUID: GLOBAL.GUID,
                        ATTACHMENT: result.FileName
                    }
                    ajax_method.Post(WFD02640_URL.UPDATE_DELETE_FILE, ModelUpdate, false, function (result) {
                        if (result != '-99') {
                            $('#' + Costcenter + 'Download').html('');
                            $('#' + Costcenter + 'ViewFile').html('');
                            $('#' + Costcenter + 'RemoveFile').html('');
                         
                            //$.bootstrapGrowl("Delete File process is completed.", {
                            //    type: 'success',
                            //    align: 'right',
                            //    width: '200px',
                            //    allow_dismiss: false
                            //});
                            $.notify({
                                icon: 'glyphicon glyphicon-ok',
                                message: "Delete process is completed."
                            }, {
                                type: 'success',
                                delay: 500,
                            });
                        }
                    }, null);                    
                }
            }, null);
        }
    });
}

var clicks = 0;
var WFD02640_CheckBoxCheck = function (Costcenter) {

    clicks++

    if (clicks == 1) {
        if ($('#' + Costcenter + 'CheckBox').is(':checked')) {
            $('#' + Costcenter + 'AddComment').removeClass('NonChecked');

            fScreenMode.Submit_AddAssetsMode();
        }
        else {
            $('#' + Costcenter + 'AddComment').addClass('NonChecked');
            $('#' + Costcenter + 'Comment').text('');
            $('#' + Costcenter + 'TXTCOMMENT').val('');

            //$('#' + Costcenter + 'RemoveFileTemp').click();

            var ModelFile = {
                FunctionID: 'WFD02640',
                Folder: 'STOCK',
                FileName: $('#' + Costcenter + 'Attachment').val(),
                FileSize: 0
            }

            ajax_method.Post(WFD02640_URL.DELETE_FILE, ModelFile, false, function (result) {
                if (result.FileName == null || result.FileName == '') {
                    var ModelUpdate = {
                        COMPANY : GLOBAL.COMPANY,
                        DOCUMENT: GLOBAL.DOC_NO,
                        YEAR: WFD02640YEAR,
                        ROUND: WFD02640ROUND,
                        ASSET_LOCATION: WFD02640ASSET_LOCATION,
                        SV_CODE: WFD02640SV_CODE,
                        COST_CENTER: Costcenter,
                        EMP_CODE: WFD02640_Parameter.EMP_CODE,
                        GUID: GLOBAL.GUID,
                        ATTACHMENT: result.FileName
                    }
                    ajax_method.Post(WFD02640_URL.UPDATE_DELETE_FILE, ModelUpdate, false, function (result) {
                        if (result != '-99') {
                            $('#' + Costcenter + 'Download').html('');
                            $('#' + Costcenter + 'ViewFile').html('');
                            $('#' + Costcenter + 'RemoveFile').html('');
                        }
                    }, null);
                }
            }, null);

            // Create FormData object  
            var con = new FormData();
            con.append('COMPANY', GLOBAL.COMPANY);
            con.append('DOCUMENT', ControlsWFD02640.WFD02640Document.val());
            con.append('YEAR', WFD02640YEAR);
            con.append('ROUND', WFD02640ROUND);
            con.append('ASSET_LOCATION', WFD02640ASSET_LOCATION);
            con.append('SV_CODE', WFD02640SV_CODE);
            con.append('COST_CENTER', WFD02640DialogCostcenter);
            con.append('COMMENT', '');
            con.append('EMP_CODE', WFD02640_Parameter.EMP_CODE);
            con.append('GUID', GLOBAL.GUID);

            ajax_method.PostFile(WFD02640_URL.SaveTempRequestStock, con, function (result) {
                //    console.log(result);
                if (result.ObjectResult) { }

            });
        }

        clicks = 0;
    }
    else {
        //AlertTextErrorMessagePopup('MSTD0000AWRN : Business Message.', "#AlertMessageArea");
        clicks = 0;
    }
}

var fAction = {
    SelectedCostCenter: "",
    SelectedRespCostCenter: "",
    //Should define all request screen.
    LoadData: function (callback) {
        if (callback != null)
            callback();
    },

    Clear: function (callback) {
        if (callback != null)
            callback();
    },
    Export: function (callback) {
        
        if (callback == null)
            return;

        callback();
    },
   
    UpdateAsset: function (callback) {
        if (callback == null)
            return;

        callback();

    },
    PrepareGenerateFlow: function (callback) {

        if (WFD02640COST_CODE_SELECTED == null || WFD02640COST_CODE_SELECTED == "") {
            AlertTextErrorMessage(WFD02640_Message.ErrorGenApprover);
            return;

        }
        // Clear temp selected
        var con = new FormData();
        con.append('COMPANY', GLOBAL.COMPANY);
        con.append('DOCUMENT', ControlsWFD02640.WFD02640Document.val());
        con.append('YEAR', WFD02640YEAR);
        con.append('ROUND', WFD02640ROUND);
        con.append('ASSET_LOCATION', WFD02640ASSET_LOCATION);
        con.append('SV_CODE', WFD02640SV_CODE);
        con.append('EMP_CODE', WFD02640_Parameter.EMP_CODE);
        con.append('GUID', GLOBAL.GUID);

        //Clear Selected Checkbox on Temp table
        ajax_method.PostFile(WFD02640_URL.ClearAssetList, con, function (result) {
            var arr_WFD02640COST_CODE_SELECTED = WFD02640COST_CODE_SELECTED.split(',');
            var _index = 0;
            //Loop selected item
            
            var _SelectedCnt = arr_WFD02640COST_CODE_SELECTED.length;
            var _CompleteCnt = 0;
            $.each(arr_WFD02640COST_CODE_SELECTED, function (entry, item) {
                console.log("entry");
                console.log(entry);
                console.log("COST_CENTER => " + item);
                var con = new FormData();
                con.append('COMPANY', GLOBAL.COMPANY);
                con.append('DOCUMENT', GLOBAL.DOC_NO);
                con.append('YEAR', WFD02640YEAR);
                con.append('ROUND', WFD02640ROUND);
                con.append('ASSET_LOCATION', WFD02640ASSET_LOCATION);
                con.append('SV_CODE', WFD02640SV_CODE);
                con.append('COST_CENTER', item);
                con.append('EMP_CODE', WFD02640_Parameter.EMP_CODE);
                con.append('GUID', GLOBAL.GUID);
                con.append('SELECTED', 'Y');


                
                // Update Selected Assets
                ajax_method.PostFile(WFD02640_URL.UpdateAssetList, con, function (result) {
                    console.log("complete =>" + item);
                    _CompleteCnt++;

                    console.log(_CompleteCnt);
                    console.log(_SelectedCnt);
                    if (_CompleteCnt >= _SelectedCnt) { //All selected is updated.
                        console.log(Url.PrepareFlow)
                        ajax_method.Post(Url.PrepareFlow, { data: GLOBAL }, false, function (result) {
                            if (result.IsError) {
                                //fnErrorDialog("PrepareFlow", result.Message);
                                return;
                            }

                            if (callback == null)
                                return;

                            callback();
                        }); //ajax_method.PrepareFlow

                    }
                    //var con = {
                    //    'COMPANY' : GLOBA.COMPANY,
                    //    'DOCUMENT': GLOBAL.DOC_NO,
                    //    'YEAR': WFD02640YEAR,
                    //    'GUID': GLOBAL.GUID,
                    //    'ROUND': WFD02640ROUND,
                    //    'ASSET_LOCATION': WFD02640ASSET_LOCATION,
                    //    'COST_CENTER': item, //WFD02640COST_CODE_SELECTED, Modify by Surasith Becuase Found multiple record

                    //    'SELECTED': 'Y'
                    //}
                    /*
                    ajax_method.Post(WFD02640_URL.InsertAssetList, con, false, function (result) {

                        

                    }, null);
                    */
                });
            });
        });

        
    },

    PrepareSubmit: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlow
    },
    PrepareApprove: function (callback) {
        fAction.UpdateAsset(callback); //ajax_method.PrepareFlows
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },

    ShowSearchPopup: function (callback) {
        if (callback == null)
            return;

        callback();
    },
}

//Call this function when PG set screen mode
var fScreenMode = {

    _ClearMode: function () {
        
    },

    Submit_InitialMode: function (callback) {
        console.log("Submit_InitialMode");
        fScreenMode._ClearMode();

        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        
        console.log("Submit_AddAssetsMode");

        $('#WFD01170btnGenerateApprover, #WFD01170btnResetApprover').show();
        $('#WFD01170btnGenerateApprover').removeClass('disabled').prop("disabled", false);

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();

        
        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) { //Should be call after data is loaded

        fScreenMode._ClearMode();


        if (callback == null)
            return;
        callback();
    },
    _View_Mode: function () {



    }
}
