﻿//Surasith XXXXXXX

var control = {
    DataTable: null,
    Company: $('#WFD01170ddpCompany'), // Temp , still request best solution.
    DetailTransPopup: {
        ASSET_NO: "",
        ASSET_SUB: "",
        COMPANY: "",
        LOCATION_CD: null,
        FUTURE_USE: null,
        WBS_PROJECT: null,
        TO_EMPLOYEE: null,
        LOCATION_DETAIL: null
    },
    LOCATIONS: [],
    WBSPROJECTS: [],
    //NewAssetOwner
    WFD02410BtnNEW_OWNER_COST_NAME: $('#WFD02410BtnNEW_OWNER_COST_NAME'),
    CHECK_DIALOG_COST_CODE: $('#WFD02410_CHECK_DIALOG_COST_CODE'),
    NEW_OWNER_COST_CODE: $('#WFD02410_NEW_OWNER_COST_CODE'),
    NEW_OWNER_COST_NAME: $('#WFD02410_NEW_OWNER_COST_NAME'),
    NEW_OWNER_EMP_CODE: $('#WFD02410_NEW_OWNER_EMP_CODE'),
    NEW_OWNER_EMP_NAME: $('#WFD02410_NEW_OWNER_EMP_NAME'),
    NEW_OWNER_POS_NAME: $('#WFD02410_NEW_OWNER_POS_NAME'),
    NEW_OWNER_RESPONSIBILITY: $('#WFD02410_NEW_OWNER_RESPONSIBILITY'),
    NEW_OWNER_DEPT_NAME: $('#WFD02410_NEW_OWNER_DEPT_NAME'),
    NEW_OWNER_PHONE_NO: $('#WFD02410_NEW_OWNER_PHONE_NO'),
    NEW_OWNER_SECT_NAME: $('#WFD02410_NEW_OWNER_SECT_NAME'),
    WFD02410MessageAlertReasonOther: $('#WFD02410MessageAlertReasonOther'),
    WFD02410_SetOtherReason: $('#WFD02410_SetOtherReason'),
    WFD02410_TRNF_REASON_OTHER: $('#WFD02410_TRNF_REASON_OTHER'),
    ColumnIndex: {
        Sel: 0,
        EffectiveDate: 8,
        BOIDoc: 11,
        Status : 12
    },
    NewOwner: '',
    TMCAttachCount : 0,
    CostCenterDataTatable : null,
    CostPagin: $('#CostSearchResultTable').Pagin(URL_COMSCC.ITEM_PER_PAGE),
    selectedRow: null,
    selectedDDLTranferReason: null,
}



var constants = {
    ResponseTransfer : "RC",
    CostCenterTransfer : "CC",
    EmployeeTransfer : "EC",
    MassResponseTransfer: "TR",
    MassCostCenterTransfer: "TC",
    MassEmployeeTransfer: "TE",

    TMC : "TMC"
}

var _ArrTransferReason = [];

$(function () {
    $('#divSearchRequestResult').hide();
    if (GLOBAL.FLOW_TYPE == constants.EmployeeTransfer || GLOBAL.FLOW_TYPE == constants.MassEmployeeTransfer)//Employee
    {
        $('#WFD02410_NewAssetOwner').hide();
    }
    else
    {
        $('#WFD02410_NewAssetOwner').show();
    }
    
    
    //_ArrBOIStatut = GetBOIStatus(GLOBAL);
    fAction.GetNewAssetOwner((GLOBAL.FLOW_TYPE == constants.ResponseTransfer || GLOBAL.FLOW_TYPE == constants.MassResponseTransfer || GLOBAL.COPY_MODE == "Y"), null, true); //Load Header Incase New, View
});
var fAction = {
    SelectedCostCenter: "",
    SelectedRespCostCenter: "",

    Receiver : "",
    //Should define all request screen.    
    
    GetNewAssetOwner: function (_IsResponse, _selected, _isInitial) {
        var docNo = GLOBAL.DOC_NO;
        var empCode = GLOBAL.EMP_CODE;
        if (_isInitial != null && _isInitial && GLOBAL.COPY_MODE == "Y") {
            docNo = GLOBAL.SOURCE_DOC_NO;
            empCode = GLOBAL.USER_BY;
        }
        var WFD02410_Param = {
            GUID: GLOBAL.GUID,
            DOC_NO: docNo,
            COMPANY : GLOBAL.COMPANY, 
            USER_BY: empCode,
            REQUEST_EMP_CODE: empCode,
            NEW_COST_CODE: _selected == null ? null : (_IsResponse ? null : _selected.COST_CODE),
            NEW_RESP_COST_CODE: _selected == null ? null : (_IsResponse ? _selected.COST_CODE : null)
        };
        ajax_method.Post(UrlWFD02410.GetNewAssetOwner, WFD02410_Param, true
            , function (result) {
                if (result != null) {

                    control.NEW_OWNER_COST_CODE.val(result[0].COST_CODE);
                    control.NEW_OWNER_COST_NAME.val(result[0].COST_NAME);
                    control.NEW_OWNER_COST_NAME.attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result[0].COST_NAME
                    });
                  
                    $('#WFD02410_NEW_OWNER_RESP_COST_CODE').val(result[0].RESP_COST_CODE);
                    $('#WFD02410_NEW_OWNER_RESP_COST_NAME').val(result[0].RESP_COST_NAME);

                    $('#WFD02410_NEW_OWNER_RESP_COST_NAME').attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result[0].RESP_COST_NAME
                    });

                    if (result[0].REQUIRE_PHONE == "Y" && PERMISSION.Mode == "A") {
                        control.NEW_OWNER_PHONE_NO.prop("disabled", false).addClass('require');
                    }

                    control.TMCAttachCount = result[0].TMC_ATTACH_CNT;
                    control.NEW_OWNER_EMP_NAME.val(result[0].EMP_DISPLAY);
                    control.NEW_OWNER_POS_NAME.val(result[0].POST_NAME);
                    control.NEW_OWNER_RESPONSIBILITY.val(result[0].RESPONSIBILITY);
                    control.NEW_OWNER_DEPT_NAME.val(result[0].DEPT_NAME);
                    control.NEW_OWNER_PHONE_NO.val(result[0].NEW_PHONE_NO);
                    control.NEW_OWNER_SECT_NAME.val(result[0].SECT_NAME);
                    control.NEW_OWNER_EMP_CODE.val(result[0].EMP_CODE);
                    control.NewOwner = result[0].SYS_EMP_CODE ; //COMPANY.EMP_CODE
                    console.log(result[0].EMP_CODE);
                    console.log(control.NewOwner);

                    if (GLOBAL.DOC_NO != '') { // Approve Mode
                        $('#chkNormal, #chkMass').prop('disabled', true);
                        $('#chkNormal').prop('checked', result[0].TRNF_TYPE == "N");
                        $('#chkMass').prop('checked', result[0].TRNF_TYPE == "M");
                    }
                    
                    control.NEW_OWNER_EMP_NAME.attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title":  result[0].EMP_DISPLAY
                    });
                    control.NEW_OWNER_POS_NAME.attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result[0].POST_NAME
                    });
                    control.NEW_OWNER_RESPONSIBILITY.attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title":  result[0].RESPONSIBILITY
                    });
                    control.NEW_OWNER_DEPT_NAME.attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result[0].DEPT_NAME
                    });
                    control.NEW_OWNER_SECT_NAME.attr({
                        "data-toggle": "tooltip",
                        "data-placement": "top",
                        "title": result[0].SECT_NAME
                    });
                } else {

                    control.TMCAttachCount = 0;

                    $('#WFD02410_NEW_OWNER_RESP_COST_CODE').val('');
                    $('#WFD02410_NEW_OWNER_RESP_COST_NAME').val('');
                    control.NEW_OWNER_COST_CODE.val('');
                    control.NEW_OWNER_COST_NAME.val('');
                    control.NEW_OWNER_EMP_CODE.val('');
                    control.NEW_OWNER_EMP_NAME.val('');
                    control.NEW_OWNER_POS_NAME.val('');
                    control.NEW_OWNER_RESPONSIBILITY.val('');
                    control.NEW_OWNER_DEPT_NAME.val('');
                    control.NEW_OWNER_PHONE_NO.val('');
                    control.NEW_OWNER_SECT_NAME.val('');
                }
                control.CHECK_DIALOG_COST_CODE.val('N');

                fScreenMode.InitTMCDoc();

            }, null);
    },

    Initialize: function (callback) {
        $('#divSearchRequestResult').hide();
      
        fGetDropdown.GetWBSCosProject() ;
        fGetDropdown.GetLocation();
        $('#WFD02410_NEW_OWNER_EMP_CODE').val('');
        $('#WFD02410_NEW_OWNER_EMP_NAME').val('');
        $('#WFD02410_CHECK_DIALOG_COST_CODE').val('');
        $('#WFD02410_NEW_OWNER_COST_CODE').val('');
        $('#WFD02410_NEW_OWNER_COST_NAME').val('');
        $('#WFD02410_CHECK_DIALOG_RESP_COST_CODE').val('');
        $('#WFD02410_NEW_OWNER_RESP_COST_CODE').val('');
        $('#WFD02410_NEW_OWNER_POS_NAME').val('');
        $('#WFD02410_NEW_OWNER_DEPT_NAME').val('');
        $('#WFD02410_NEW_OWNER_PHONE_NO').val('');
        $('#WFD02410_NEW_OWNER_SECT_NAME').val('');
        $('#WFD02410_NEW_OWNER_RESP_COST_NAME').val('');    
        ajax_method.Post(UrlWFD02410.GetTransferReason, { FLOW_TYPE: GLOBAL.FLOW_TYPE }, false, function (result) {
            if (result != null) {
                _ArrTransferReason = result;
            }
            if (callback != null)
                callback();
        }, null);
                
         
       
    },

    LoadData: function (callback) {
        //console.log("LoadData is callled");
        //Do here

        var pagin = $('#tbSearchResult').Pagin(GLOBAL.ITEM_PER_PAGE).GetPaginData();
        console.log(GLOBAL);
        console.log(Url.GetAsset);
        ajax_method.SearchPost(Url.GetAsset, GLOBAL, pagin, true, function (datas, pagin) {
            console.log(datas);
            fAction.SelectedCostCenter = ""; //Clear Selected
            fAction.SelectedRespCostCenter = ""; //Clear Selected

            if ($.fn.DataTable.isDataTable($('#tbSearchResult'))) {
                var table = $('#tbSearchResult').DataTable();
                table.clear().draw().destroy();
            }

            
            if (datas == null || datas.length == 0) {
                $('#divSearchRequestResult').hide();

                if (callback != null)
                    callback();

                return;
            }

            fAction.SelectedCostCenter = datas[0].COST_CODE;
            fAction.SelectedRespCostCenter = datas[0].RESP_COST_CODE;

            var _bEffective = datas[0].TRNF_EFFECTIVE_DT != null;
            
            //2019.12.14 Incase Mass transfer and AEC is requestor
            if ($('#chkMass').is(':checked') && $('#WFD01170ddpRequestorRole').val() == GLOBAL.ACRCode) {
                _bEffective = true;
            }

            //
            var _bBOI = (PERMISSION.IsBOIState == "A" && ( PERMISSION.IsBOIUser == "Y" || PERMISSION.IsAECUser == "Y") ) || 
                                PERMISSION.IsBOIState == "Y";
            console.log(PERMISSION);

            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
            if (!pagin.OrderType) pagin.OrderType = 'asc';


            control.DataTable = $('#tbSearchResult').DataTable({
                data: datas,
                "columns": [
                    { "data": null }, // 0
                    { "data": "ROW_INDX", 'orderable': false, "class": "text-right" }, // 1
                    { "data": null }, // Asset No : hyper link 1
                    { "data": "ASSET_SUB", "class": "text-center", 'orderable': false },
                    {
                        "data": "ASSET_NAME", 'orderable': false,
                        'render': function (data) { return SetTooltipInDataTableByColumn(data, 15); }
                    }, // 3
                    { "data": "DATE_IN_SERVICE", "class": "text-center", 'orderable': false }, // 4
                    {
                        "data": "COST", 'orderable': false, "class": "text-right",
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },
                    {
                        "data": "BOOK_VALUE", 'orderable': false, "class": "text-right",
                        'render': function (data, type, full, meta) {
                            return currencyFormat(data);
                        }
                    },
                    { "data": "TRNF_REASON", 'orderable': false }, // 7
                    { "data": "TRNF_EFFECTIVE_DT", 'orderable': false }, // 8
                    {
                        "data": "BOI", 'orderable': false, "class": "text-center",
                        "render": function (data, type, full, meta) {
                            if (full.INVEST_REASON_HINT == null)
                                return data;

                            return '<span data-toggle="tooltip" title="' + full.INVEST_REASON_HINT + '">' + data + '</span>';
                        }
                    },// 9
                    {
                        "data": "BOI_NO", 'orderable': false, "class": "text-center",
                        "render": function (data, type, full, meta) {
                            if (full.BOI_NO_HINT == null)
                                return data;

                            return '<span data-toggle="tooltip" title="' + full.BOI_NO_HINT + '">' + data + '</span>';
                        }
                    },// 10
                    { "data": null }, // 11
                    { "data": "STATUS", 'orderable': false, "class": "text-center" }, // 12
                    { "data": null },// 13
                    { "data": null } // 14
                ],
                'columnDefs': [
                    {
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        "visible": (PERMISSION.AllowResend == "Y"),
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CheckBoxRender(data);
                        }
                    },
                    {
                        'targets': 2, //ASSET_NO Show hyperlink
                        'searchable': false,
                        'orderable': false,
                        'render': function (data, type, full, meta) {
                            return fRender.HyperLinkRender(full);
                        }
                    },//

                    {
                        'targets': 8, //TRNF_REASON
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            //   console.log("AMOUNT_POSTED");
                            //   console.log(full);
                            return fRender.DropdownTransferRender(_ArrTransferReason, full);
                        }

                    },
                    {
                        'targets': 9, //TRNF_EFFECTIVE_DT
                        'searchable': false,
                        'orderable': false,
                        'visible': (_bEffective),
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.CalendarRender(full);
                        }

                    },
                    {
                        'targets': 12, //BOI DOC
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'visible': (_bBOI),
                        'render': function (data, type, full, meta) {
                            //if (full.BOI == 'B') {
                            //    return fRender.ButtonUploadRender(full);
                            //} else {
                            //    return '';
                            //}
                            return fRender.ButtonUploadRender(full);
                        }

                    },
                    {
                        'targets': 14, //Detail
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.ButtonRender(full);
                        }

                    },
                    {
                        'targets': 15, //Delete
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return fRender.DeleteButtonRender(full);
                        }

                    }],
                'order': [],
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                fixedHeader: {
                    header: true,
                    footer: false
                },
                "initComplete": function (settings, json) {
                    console.log('DataTables has finished its initialisation.');
                },
                "createdRow": function (row, data, index) {
                    console.log("createdRow");
                }
            }).columns.adjust().draw();

            //Incase New Mode
            if (GLOBAL.DOC_NO == '') {
                if (datas.length == 0) {
                    fScreenMode.Submit_InitialMode(function () {
                        fMainScreenMode.Submit_InitialMode();
                    });
                }
                if (datas.length > 0) {
                    fScreenMode.Submit_AddAssetsMode(function () {
                        fMainScreenMode.Submit_AddAssetsMode();
                    });
                }
             //   fScreenMode._GridVisible(false, false,false,false); //Load Initial Mode
            }

            $('#divSearchRequestResult').show();

            $('.datepicker').datepicker({
                dateFormat: 'dd.mm.yy',
                minDate: datas[0].TRANS_START_DATE,
                //comment the beforeShow handler if you want to see the ugly overlay 
                beforeShow: function () {
                    setTimeout(function () {
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                }
            });

            //$('[data-toggle="tooltip"]').tooltip();
            if (callback == null)
                return;

            callback();

        }, null);


    },
  
    Clear: function (callback) {
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        ajax_method.Post(Url.ClearAssetsList, GLOBAL, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }
            fAction.LoadData(function () {
                fScreenMode.Submit_InitialMode(function () {
                    fMainScreenMode.Submit_InitialMode();
                });
            });
            //  SuccessNotification(CommonMessage.ClearSuccess);

            if (callback == null)
                return;

            callback();
        });
    },
 
    Export: function (callback) {
        fAction.UpdateAsset(function () {
            var batch = BatchProcess({
                BatchId: "LFD02450",
                Description: "Export Transfer Request",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'GUID', GLOBAL.GUID);
            batch.Addparam(2, 'DOC_NO', GLOBAL.DOC_NO);
            batch.StartBatch();

            if (callback == null)
                return;

            callback();
        });
    },

    AddAsset: function (_list, callback) { //It's call from dialog

        //console.log(Url.AddAsset);

        if (_list == null || _list.length == 0)
            return;

        _list.forEach(function (_data) {
            // do something with `item`
            _data.GUID = GLOBAL.GUID;
            _data.USER_BY = GLOBAL.USER_BY;
        });

        ajax_method.Post(Url.AddAsset, _list, false, function (result) {
            console.log(result);
            if (result.IsError) {
                return;
            }

            fAction.LoadData();

            if (callback == null)
                return;

            callback();
        });
    },
    UpdateAsset:function(callback){
        var _data = fAction.GetClientAssetList(false); // Get All
        ajax_method.Post(Url.UpdateAssetList, _data, false, function (result) {
            if (result.IsError) {
                //fnErrorDialog("UpdateAssetList", result.Message);
                return;
            }
            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList

    },
    DeleteAsset: function (_AssetNo, _AssetSub) {
        //console.log(_AssetNo);
        //console.log(Url.DeleteAsset);
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        var _data = {
            DOC_NO: GLOBAL.DOC_NO,
            GUID: GLOBAL.GUID,
            COMPANY: control.Company.val(),
            ASSET_NO: _AssetNo,
            ASSET_SUB: _AssetSub
        }
        loadConfirmAlert(fMainAction.getConfirmDeleteAssetDetail(_AssetNo, _AssetSub), function (result) {
            if (!result) {
                return;
            }
            fAction.UpdateAsset(function () {
                ajax_method.Post(Url.DeleteAsset, _data, false, function (result) {
                    if (result.IsError) {
                        return;
                    }
                    console.log("Delete");
                    fAction.LoadData();
                    // SuccessNotification(CommonMessage.ClearSuccess);
                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Deletion process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                });
            }); // UpdateAsset 
        }); // loadConfirmAlert
    },
    ValidateUploadExcel: function () {
        return fAction.ValidateAddAssetDetail();
    },
    UploadExcel: function (_fileName) {
        //Check Mandatory owner


        //call Batch with filename
        fAction.UpdateAsset(function () { 
            var batch = BatchProcess({
                BatchId: "BFD02411",
                Description: "Upload Transfer Batch",
                UserId: GLOBAL.USER_BY
            });
            batch.Addparam(1, 'Company', GLOBAL.COMPANY);
            batch.Addparam(2, 'GUID', GLOBAL.GUID);
            batch.Addparam(3, 'UploadFileName', _fileName);
            batch.Addparam(4, 'User', GLOBAL.USER_BY);
            batch.StartBatch(function (_appID, _Status) {

                if (_Status != 'S') {
                    return;
                }
                fAction.LoadData();
            }, true);
        });
    },
    PrepareGenerateFlow: function (callback) {

        fAction.UpdateAsset(function () {
            ajax_method.Post(Url.PrepareFlow, { data: GLOBAL }, false, function (result) {
                if (result.IsError) {
                    return;
                }
                if (callback == null)
                    return;

                callback();
            }); //ajax_method.PrepareFlow
        }); //ajax_method.UpdateAssetList
    },

    PrepareSubmit: function (callback) {
        
        //AlertTextErrorMessagePopup('MSTD0031AERR : Transfer Type should not be empty', "#AlertMessageArea");

        //AlertTextErrorMessage(Search2190Message.SelectOneRecord, "#AlertMessageWFD02190", '#WFD02190_SearchAssets');
        //return;

        // return;
        fAction.UpdateAsset(callback); 
    },
    PrepareApprove: function (callback) {
        // alert("Check File Upload");
        // return;
        fAction.UpdateAsset(callback); 
    },
    PrepareReject: function (callback) {
        //No any to prepare, We can call callback function for imprement request.
        if (callback == null)
            return;

        callback();
    },
    ValidateAddAssetDetail: function () {
        ClearMessageC();

        if (!$('#WFD02410_NEW_OWNER_EMP_CODE').val() && GLOBAL.FLOW_TYPE !== 'EC') {
            AlertTextErrorMessagePopup('MSTD0031AERR : New Owner should not be empty', "#AlertMessageArea");
            window.scrollTo(0, 0);
            return false;
        }
        //MSTD0031AERR : Transfer Type should not be empty.
        if (!$('#chkNormal').is(':checked') && !$('#chkMass').is(':checked')) {
            AlertTextErrorMessagePopup('MSTD0031AERR : Transfer Type should not be empty', "#AlertMessageArea");
            window.scrollTo(0, 0);
            return false;
        }

        return true;
    },
    ShowSearchPopup: function (callback) {
        if (!fAction.ValidateAddAssetDetail()) {
            return;
        }

        //Set search parameter
        fAction.UpdateAsset(function () {
            f2190SearchAction.ClearDefault();

            Search2190Config.RoleMode = $('#WFD01170ddpRequestorRole').val();

            Search2190Config.searchOption = "T";
            Search2190Config.AllowMultiple = true;

            Search2190Config.Default.Company = { Value: $('#WFD01170ddpCompany').val(), Enable: false };
            Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
            Search2190Config.Default.ResponsibleCostCode = { Value: fAction.SelectedRespCostCenter, Enable: fAction.SelectedRespCostCenter == "" };
            Search2190Config.Default.AssetGroup = { Value: "RMA", Enable: true };
            Search2190Config.Default.Parent = { Value: "Y", Enable: true }; //2020.01.26 change from false -> true support CR

            //Assign Default and Initial screen
            f2190SearchAction.Initialize();

            $('#WFD02190_SearchAssets').modal();
        });
    },

    ShowDetailTransDialog: function (button) {
        //  _Company, _AssetNo, _AssetSub
        //  $('#lblAssetNoOnDetailTransDialog').text(_AssetNo);
        var row = control.DataTable.row($(button).parents('tr')).data();
        //  console.log(button);
        //  var d = control.DataTable.row(sender).data();
        //  console.log(d.ASSET_NO);
        //  d.ASSET_NO = "DDDDD";
        console.log('----------------');
        console.log(row.ASSET_NO);
        //var _data = {
        //    COST_CENTER: "",
        //    COMPANY: $('#WFD01170ddpCompany').val()
        //}
        var title = "Asset No. : " + row.ASSET_NO + " Asset Sub : " + row.ASSET_SUB;

        $('#lblAssetNoOnDetailTransDialog').text(title);
        control.DetailTransPopup = {
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: row.COMPANY,
            LOCATION_CD: row.LOCATION_CD,
            LOCATION_NAME: row.LOCATION_NAME ? row.LOCATION_NAME : row.LOCATION_CD,
            FUTURE_USE: row.FUTURE_USE,
            WBS_PROJECT: row.WBS_PROJECT,
            WBS_PROJECT_DESC: row.WBS_PROJECT_DESC ? row.WBS_PROJECT_DESC : row.WBS_PROJECT,
            TO_EMPLOYEE: row.TO_EMPLOYEE,
            LOCATION_DETAIL: row.LOCATION_DETAIL,
        };

        $('#WFD02410ComBoLocation, #WFD02410FutureUse, #WFD02410ComBoWBSCosProject, #WFD02410ComBoToEmployee, #WFD02410LocationDetail').val('');

        $('#WFD02410ComBoLocation').val('');
        $('#WFD02410FutureUse').val(row.FUTURE_USE);
        $('#WFD02410ComBoWBSCosProject').val('');
        $('#WFD02410ComBoToEmployee').val('');
        $('#WFD02410LocationDetail').val(row.LOCATION_DETAIL);
       
        $('#WFD02410FutureUse').prop("disabled", true); // lock alway

        fGetDropdown.InitWBSProjectSelect2(control.DetailTransPopup.WBS_PROJECT, control.DetailTransPopup.WBS_PROJECT_DESC);
        fGetDropdown.InitLocationSelect2(control.DetailTransPopup.LOCATION_CD, control.DetailTransPopup.LOCATION_NAME);
      
        $('#WFD02410ComBoLocation').select2('val', row.LOCATION_CD);
        $('#WFD02410ComBoWBSCosProject').select2('val', row.WBS_PROJECT);

        fGetDropdown.GetToEmployee(row, function () {
            $('#divDetailTransDialog').modal();
        });

        //Allow to edit for submit, AEC, resubmit
        if (PERMISSION.AllowEditAll == "N") {
            $('#btnDetailTransDialogOK').prop("disabled", true);
            $('#WFD02410ComBoLocation, #WFD02410FutureUse, #WFD02410ComBoWBSCosProject, #WFD02410ComBoToEmployee, #WFD02410LocationDetail').prop("disabled", true);
            return;
        }
       
        $('#btnDetailTransDialogOK').prop("disabled", !row.AllowEdit);
        $('#WFD02410ComBoLocation,#WFD02410ComBoWBSCosProject, #WFD02410ComBoToEmployee, #WFD02410LocationDetail').prop("disabled", !row.AllowEdit);

        return;
    },
    
    GetFlowType : function()
    {
        if(GLOBAL.DOC_NO != '')
        {
            return GLOBAL.FLOW_TYPE ;
        }
        if (GLOBAL.FLOW_TYPE == constants.CostCenterTransfer)
            return $('#chkMass').is(':checked') ? constants.MassCostCenterTransfer : GLOBAL.FLOW_TYPE;
        if (GLOBAL.FLOW_TYPE == constants.ResponseTransfer)
            return $('#chkMass').is(':checked') ? constants.MassResponseTransfer : GLOBAL.FLOW_TYPE;
        if (GLOBAL.FLOW_TYPE == constants.EmployeeTransfer)
            return $('#chkMass').is(':checked') ? constants.MassEmployeeTransfer : GLOBAL.FLOW_TYPE;

        return "";
    },
    GetClientAssetList: function (_requireCheck) {
        console.log("GetClientAssetList");

        var _header = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            COMPANY: GLOBAL.COMPANY,
            NEW_COST_CODE: $('#WFD02410_NEW_OWNER_COST_CODE').val(),
            NEW_RESP_COST_CODE: $('#WFD02410_NEW_OWNER_RESP_COST_CODE').val(),
            FLOW_TYPE: fAction.GetFlowType(),
            TRNF_TYPE: ($('#chkNormal').is(':checked')) ? 'N' : ($('#chkMass').is(':checked') ? "M" : ""),
            RECEIVER: control.NewOwner,
            TEL_NO: $('#WFD02410_NEW_OWNER_PHONE_NO').val(),
            DetailList: [],
            UPDATE_DATE: $('#hdUpdateDate').val()
        }

        if (control.DataTable == null){ 
            console.log("NULL");
            return _header;
        }
        var _assetList = []; //array List

        $("#tbSearchResult tr").each(function () {
            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "")
            {
                return ;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }
            var _reason = $(this).find("select[name='ddpReason']").val();
           
            var _calendar =  $(this).find("input[name='Calendar2410']").val();
           
            var _bSel = $(this).find("input[name='chkGEN']").is(":checked");

            if (_requireCheck && !_bSel) // Required check but no selected 
                return;
            _row = control.DataTable.row(this).data(); // Get source data

            var _TRNF_REASON_OTHER = _row.TRNF_REASON_OTHER;
            if (_reason !== "OTHER") {
                _TRNF_REASON_OTHER = "";
            }

            _assetList.push({
                SEL : _bSel,
                GUID: GLOBAL.GUID,
                DOC_NO: GLOBAL.DOC_NO,
                COMPANY: _row.COMPANY,
                ASSET_NO: _row.ASSET_NO,
                ASSET_SUB: _row.ASSET_SUB,
                TRNF_REASON: _reason,
                TRNF_REASON_OTHER: _TRNF_REASON_OTHER,
                TRNF_EFFECTIVE_DT: _calendar,
                PLANT: _row.PLANT,
                LOCATION_CD: _row.LOCATION_CD,
                WBS_PROJECT: _row.WBS_PROJECT,
                TO_EMPLOYEE: _row.TO_EMPLOYEE,                
                LOCATION_DETAIL: _row.LOCATION_DETAIL,
            });
        });

      
        _header.DetailList = _assetList;

        //var _header = {
        //    GUID: GLOBAL.GUID,
        //    DOC_NO: GLOBAL.DOC_NO,
        //    COMPANY: GLOBAL.COMPANY,
        //    NEW_COST_CODE: $('#WFD02410_NEW_OWNER_COST_CODE').val(),
        //    NEW_RESP_COST_CODE: $('#WFD02410_NEW_OWNER_RESP_COST_CODE').val(),
        //    FLOW_TYPE: fAction.GetFlowType(),
        //    TRNF_TYPE: ($('#chkNormal').is(':checked')) ? 'N' : ($('#chkMass').is(':checked') ? "M" : ""),
        //    RECEIVER: control.NewOwner,
        //    TEL_NO: $('#WFD02410_NEW_OWNER_PHONE_NO').val(),
        //    DetailList: _assetList,
        //    UPDATE_DATE :  $('#hdUpdateDate').val()
        //}

        return _header;
    },
    EnableMassMode: function (_Role) {
        //
        ajax_method.Post('/WFD02410/WFD02410_IsMassPermission', { ACT_ROLE: _Role }, false, function (result) {
            if (result.IsError) {
                $('#WFD02410_ApproveRouteType').show();
                $('#chkNormal').prop("checked", true);
                $('#chkMass').prop("checked", false);

                $('#chkMass, #chkNormal').prop("disabled", true);
                return;
            }
            if (GLOBAL.DOC_NO == '') {
                $('#WFD02410_ApproveRouteType').show();
                //$('#chkNormal, #chkMass').prop("checked", false);
                $('#chkNormal, #chkMass').prop("disabled", false);
            }

        });
    },
    DetailCheck: function (sender) {
        var row = $(sender).closest("tr");

        $('#WFD02410_SelectAll').prop('checked', _act = $('.chkGEN:checked').length == $('.chkGEN').length);
    },
    UpdateRow: function (_data) {
        console.log("UpdateRow");

        var _bFoundFlag = false;
        //_Company, _AssetNo, _AssetSub, _AmountPosted, _Percent
        $("#tbSearchResult tr").each(function () {

            if (_bFoundFlag)
                return;

            var $this = $(this);
            var row = $this.closest("tr");
            if (row.find('td:eq(1)').text() == null || row.find('td:eq(1)').text() == "") {
                return;
            }
            var _row = [];
            if (control.DataTable == null) {
                return;
            }

            _row = control.DataTable.row(this).data(); // Get source data
            if (_row.COMPANY != control.DetailTransPopup.COMPANY ||
                _row.ASSET_NO != control.DetailTransPopup.ASSET_NO ||
                _row.ASSET_SUB != control.DetailTransPopup.ASSET_SUB) {
                return;
            }
            _row.LOCATION_CD = $('#WFD02410ComBoLocation').val();
            _row.FUTURE_USE = $('#WFD02410FutureUse').val();
            _row.WBS_PROJECT = $('#WFD02410ComBoWBSCosProject').val();
            _row.TO_EMPLOYEE = $('#WFD02410ComBoToEmployee').val();
            _row.LOCATION_DETAIL = $('#WFD02410LocationDetail').val();


            console.log("Update Row is success")
            _bFoundFlag = true;
        });

    },

    
    Resend: function (callback) {

        var _header = fMainAction.GetParameter();
        var _data = fAction.GetClientAssetList(true); // Get All
        ajax_method.Post(Url.Resend, { header: _header, data: _data }, false, function (result) {
            if (result.IsError) {
                return;
            }

            if (callback == null)
                return;

            callback();

        }); //ajax_method.UpdateAssetList -> Send

       
    },
    SetUploadButton:function(sender, _isUpload){
        if(_isUpload)
            $(sender).html('<i class="fa fa-upload"></i> Upload');
        else
            $(sender).html('<i class="fa fa-download"></i> Download');
    },

    UpdateReasonOther: function () {
        //debugger;
        var _TRNF_REASON_OTHER = $('#WFD02410_TRNF_REASON_OTHER').val();
        var _TRNF_REASON = 'OTHER';


        //Normal
        if (control.selectedRow != null && control.selectedDDLTranferReason != null) {
            //alert(control.selectedRow);
            var _row = control.DataTable.row(control.selectedRow).data();
            _row.TRNF_REASON_OTHER = _TRNF_REASON_OTHER;
            _row.TRNF_REASON = _TRNF_REASON;
            $('#pTooltip' + control.selectedDDLTranferReason).attr("title", _TRNF_REASON_OTHER);
            $('#pTooltip' + control.selectedDDLTranferReason).attr("data-original-title", _TRNF_REASON_OTHER);
            //control.DataTable.row(control.selectedRow).data(_row).draw(false);
            //$('.datepicker').datepicker({
            //    dateFormat: 'dd.mm.yy',
            //    minDate: _row.TRANS_START_DATE,
            //    //comment the beforeShow handler if you want to see the ugly overlay 
            //    beforeShow: function () {
            //        setTimeout(function () {
            //            $('.ui-datepicker').css('z-index', 99999999999999);
            //        }, 0);
            //    }
            //});
            return;
        }
    },
}

var fPopup = {
    ShowBOIUploadDialog: function (button) {

        var _Type = "BOI";
        var row = control.DataTable.row($(button).parents('tr')).data();
        console.log('----------------');
        console.log(row.ASSET_NO);
        var _DocUpload = '';
        _DocUpload = _Type;


        var _bIsUploadBOI = (PERMISSION.IsBOIState == "A" && (PERMISSION.IsBOIUser == "Y" || PERMISSION.IsAECUser == "Y"));

        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: row.ASSET_NO,
            ASSET_SUB: row.ASSET_SUB,
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _DocUpload,
            FUNCTION_ID: GLOBAL.FUNCTION_ID,
            DOC_UPLOAD: GLOBAL.FUNCTION_ID + "_" + _DocUpload,
            DEL_FLAG: _bIsUploadBOI ? "Y" : "N", 
            LINE_NO: '0'
        }
        //$('#lblUploadFileText').html("Upload BOI Document Asset No. : {0}".format(controlUpload.ASSET_NO));
        $('#lblUploadFileText').html("Upload BOI Document Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB);
        $('#CommonUpload').appendTo("body").modal('show');

    },
    ShowMainUploadDialog: function (_Text, _Type, _permission) {
        controlUpload = {
            GUID: GLOBAL.GUID,
            DOC_NO: GLOBAL.DOC_NO,
            ASSET_NO: '',
            ASSET_SUB: '',
            COMPANY: $('#WFD01170ddpCompany').val(),
            TYPE: _Type,
            FUNCTION_ID: GLOBAL.FUNCTION_ID,
            LINE_NO: 0,
            DOC_UPLOAD: GLOBAL.FUNCTION_ID + "_" + _Type,
            DEL_FLAG: _permission ? "Y" : "N" //Allow FS
        }

        $('#lblUploadFileText').html(_Text);
        $('#CommonUpload').appendTo("body").modal('show');

    },
}

//Call this function when PG set screen mode
var fScreenMode = {
   
    _ClearMode: function () {
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').hide();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", true);
        $('#WFD02410BtnNEW_OWNER_COST_NAME, #WFD02410BtnNEW_OWNER_RESP_COST_NAME').prop("disabled", true);
        
        $('#WFD02410_NEW_OWNER_COST_NAME, #WFD02410_NEW_OWNER_RESP_COST_NAME').removeClass('require');
       
        $('#WFD02410_SelectAll').prop("disabled", true);
        $('#WFD02410_SelectAll').hide();

        if (GLOBAL.STATUS == '00') //Submit mode only
        {
            $('button.delete, button.boi, button.detail').prop("disabled", true);
            $('input.datepicker, input.chkGEN, select.reason').prop("disabled", true);
            $('#chkMass, #chkNormal').prop("disabled", true);
        }

        $('#DivTMCDocument').hide();
    },

    Submit_InitialMode: function (callback) {

        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd').prop("disabled", false);

        $('#chkMass, #chkNormal, #WFD02410_NEW_OWNER_PHONE_NO').prop("disabled", false);

        $('button.delete, button.detail').prop("disabled", false);
        $('input.datepicker, select.reason').prop("disabled", false);

        $('#WFD02410BtnNEW_OWNER_RESP_COST_NAME').prop("disabled", false);
        $('#WFD02410BtnNEW_OWNER_COST_NAME').prop("disabled", GLOBAL.FLOW_TYPE == constants.ResponseTransfer || GLOBAL.FLOW_TYPE == constants.MassResponseTransfer);

        if (GLOBAL.FLOW_TYPE == constants.ResponseTransfer || GLOBAL.FLOW_TYPE == constants.MassResponseTransfer)
        {
            $('#WFD02410_NEW_OWNER_RESP_COST_NAME').removeClass('disabled').addClass('require');
        }
        if (GLOBAL.FLOW_TYPE == constants.CostCenterTransfer || GLOBAL.FLOW_TYPE ==  constants.MassCostCenterTransfer ) 
        {
            $('#WFD02410_NEW_OWNER_COST_NAME').removeClass('disabled').addClass('require');
        }
        
        if (callback == null)
            return;
        callback();
    },

    Submit_AddAssetsMode: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').prop("disabled", false);
        $('#WFD02410_NEW_OWNER_PHONE_NO').prop("disabled", false);
        $('button.delete, button.detail').prop("disabled", false);

        $('input.chkGEN').prop("disabled", false);
        $('input.datepicker, select.reason').prop("disabled", false);

        $('#chkMass, #chkNormal').prop("disabled", true); //Change to true 2019.12.14
         
         //Same as Initial
        $('#WFD02410BtnNEW_OWNER_RESP_COST_NAME').prop("disabled", false);
        $('#WFD02410BtnNEW_OWNER_COST_NAME').prop("disabled", GLOBAL.FLOW_TYPE == constants.ResponseTransfer || GLOBAL.FLOW_TYPE == constants.MassResponseTransfer);

        if (callback == null)
            return;
        callback();

    },
    Submit_GenerateFlow: function (callback) {
        fScreenMode._ClearMode();
        $('#btnRequestDownload, #btnRequestUpload, #btnRequestAdd, #btnRequestClear, #btnRequestExport').show();
        $('#btnRequestDownload, #btnRequestExport').prop("disabled", false);

        $('#WFD02410_NEW_OWNER_PHONE_NO').prop("disabled", false);

        //Enable becuase no impact flow
        $('button.detail').prop("disabled", false);
        $('input.datepicker, select.reason').prop("disabled", false);

        $('button.delete').show();

        if (callback == null)
            return;
        callback();
    },
    Approve_Mode: function (callback) {

        console.log("#_ClearMode");
        fScreenMode._ClearMode();

        $('#btnRequestExport').show();
        $('#btnRequestExport').prop("disabled", false);

        fScreenMode.InitTMCDoc();

        //if (PERMISSION.ActionMark == constants.TMC || control.TMCAttachCount > 0)
        //{ //FS Mode and View Mode

        //    fAction.SetUploadButton('#WFD02410btnUploadTMCDocument', PERMISSION.ActionMark == constants.TMC);
        //    $('#DivTMCDocument').show();
        //}

        //if (PERMISSION.AllowResubmit == "Y")
        //{
        //    $('button.detail').prop("disabled", false);
        //}

        if (callback == null)
            return;
        callback();
        return;
    },

    InitTMCDoc: function () {
        if (PERMISSION.ActionMark == constants.TMC || (control.TMCAttachCount > 0 && PERMISSION.AllowShowTMCDoc === "Y")) { //FS Mode and View Mode

            fAction.SetUploadButton('#WFD02410btnUploadTMCDocument', PERMISSION.ActionMark == constants.TMC);
            $('#DivTMCDocument').show();
        }
    }
}


var fRender = {
    CheckBoxRender: function (data) {
        var _strHtml = '';
        
        var _disabled = (data.AllowCheck ? '' : "disabled");

 
        _strHtml = '<input style="width:20px; height:28px;" name="chkGEN" class="chkGEN" value="' + data.ASSET_NO + '#' + data.ASSET_SUB + '" onclick="fAction.DetailCheck(this);" type="checkbox" ' + _disabled  + ' >';
        return _strHtml;
    },
    HyperLinkRender: function (data) {

        var _text = data.ASSET_NO;
        if (data.IS_MACHINE_LICENSE == "Y") {
            _text = data.ASSET_NO + "<span style=\"color:red;font-weight:bold\">*</span>";
        }

        return '<a href="../WFD02130?COMPANY=' + data.COMPANY + '&ASSET_NO=' + data.ASSET_NO + '&ASSET_SUB=' + data.ASSET_SUB + '" target="_blank">' + _text + '</a>';
    },
    
    DropdownTransferRender: function (_ArrTransferReason, data) {
        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        var _AssetNo = data.ASSET_NO;
        var _TransferChoosed = '';
        var _strDisplayTooltip = '';
        
        var _cssRequire = '';
        
        var _strDisplayTooltip = '';

        var _disabled = (data.AllowEdit ? '' : "disabled");

        var _strHtmlDDL = '<select id=\'WFD02410_TRA' + _id + '\'" name="ddpReason" class="reason form-control select2 ' + _cssRequire + '"' + _disabled + ' style="width:100%;" onchange="fRender.SetDisplayTranferReason(this,\'' + _id + '\');">';
        if (_ArrTransferReason.length == 0) {
            return _strHtmlDDL;
        }

        _strHtmlDDL = _strHtmlDDL + '<option value="">Select</option>';
        _strDisplayTooltip = '<p id="pTooltip' + _id + '" title="" data-original-title="" data-toggle="tooltip" data-placement="top" >';
        for (i = 0; i < _ArrTransferReason.length; i++) {
            if (_ArrTransferReason[i].CODE == data.TRNF_REASON) {               
                _strHtmlDDL = _strHtmlDDL + '<option selected="selected" value="' + _ArrTransferReason[i].CODE + '">' + _ArrTransferReason[i].VALUE + '</option>';
                if (_ArrTransferReason[i].VALUE.length != null && _ArrTransferReason[i].VALUE.length > 20 && _ArrTransferReason[i].CODE != 'OTHER') {
                    _strDisplayTooltip = '<p id="pTooltip' + _id + '" title="' + replaceAll(_ArrTransferReason[i].VALUE, '"', '&quot;') + '" data-original-title="' + replaceAll(_ArrTransferReason[i].VALUE, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top" >';
                } else {
                    if (data.TRNF_REASON == 'OTHER') {
                        _strDisplayTooltip = '<p id="pTooltip' + _id + '" title="' + data.TRNF_REASON_OTHER + '" data-original-title="' + data.TRNF_REASON_OTHER + '" data-toggle="tooltip" data-placement="top" >';
                    }                    
                }
            } else {                
                _strHtmlDDL = _strHtmlDDL + '<option value="' + _ArrTransferReason[i].CODE + '">' + _ArrTransferReason[i].VALUE + '</option>';
            }
        }
        
        if (_strDisplayTooltip != '') {
            _strHtmlDDL = _strDisplayTooltip + _strHtmlDDL + '</p>';
        }
        return _strHtmlDDL;

        //return WFD02410_DDL_TRANSFER_REASON(_ArrTransferReason, data);
    },
    CalendarRender: function (data) {

        var _disabled = (data.AllowEdit ? '' : "disabled");

        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;

     

        return '<div class="input-group date">' +
                    '<input type="text" id="Calendar2410_' + _id + '" name="Calendar2410" class="form-control datepicker" ' + _disabled + ' value="' + (data.TRNF_EFFECTIVE_DT == null ? '' : data.TRNF_EFFECTIVE_DT) + '" />' +
                        '<span class="input-group-btn" style="cursor:pointer;">' +
            '<button class="btn" id="Calendar2410_' + _id + '_BTN" onclick="ClickIconDate(\'Calendar2410_' + _id +'\')" >' +
                                '<i class="fa fa-calendar"></i>' +
            '               </button>' +
                        '</span>' +
                 '</div>'
    },
    ButtonRender: function (data) {
        console.log(data);

        var _disabled = "";
        if (GLOBAL.FLOW_TYPE == "RC") {
            _disabled = " disabled ";
        }


        var _text = ' ... ';
        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<div class="btn-group">' +
               '<button type="button" id="btnDetailTrans_' + _id + '" name="btnDetailTrans" class="detail btn btn-info" ' + _disabled +
                    'onclick="fAction.ShowDetailTransDialog(this);return false;">' + _text + '</button>' +
                '</div>';
    },

    ButtonUploadRender: function (data) {
        console.log(data);
        var _id = data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        return '<div class="btn-group">' +
                //'<input type="hidden" id="hdATTACH_DOC_"' + _id + '" name="hdATTACH_DOC" value="' + data.ATTACH_DOC + '" />' +
                //'<input type="hidden" id="hdATTACH_TYPE_"' + _id + '" name="hdATTACH_TYPE" value="' + data.ATTACH_TYPE + '" />' +
                '<button type="button" id="btnFileUpload_' + _id + '" name="btnFileUpload" class="boi btn btn-info" ' +
            'onclick="fPopup.ShowBOIUploadDialog(this);return false;">' +
            '<span class="glyphicon glyphicon-search"></span></button>' +
                '</div>';
        //,\'' + data.COMPANY + '\',\'' + data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\'
    },
    DeleteButtonRender: function (data) {

        var _hide = (data.AllowDelete ? '' : "hide");
        var _disbled = (PERMISSION.AllowEditBeforeGenFlow ? '' : "disabled");


        var _id = "btnDelete_" + data.COMPANY + "_" + data.ASSET_NO + "_" + data.ASSET_SUB;
        var _strHtml = '';
        _strHtml = '<p title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="' + _id + '" ' + _disbled + ' ' +
            ' class="btn btn-danger btn-xs delete ' + _hide + ' " data-title="Delete" type="button" ' + 
                 'onclick=fAction.DeleteAsset(\'' +
                 data.ASSET_NO + '\',\'' + data.ASSET_SUB + '\')>' +
                 '<span class="glyphicon glyphicon-trash"></span></button></p>';
        return _strHtml;
    },

    GetSelectedRow: function (sender) {

        if (control.DataTable == null) {
            return;
        }

        return control.DataTable.row($(sender).parents('tr'));

    },

    SetDisplayTranferReason: function (button, id) {
        control.selectedRow = fRender.GetSelectedRow(button);
        control.selectedDDLTranferReason = id;
        var row = control.DataTable.row(control.selectedRow).data();

        if ($('#WFD02410_TRA' + id).val() == 'OTHER') {
            fRender.SetOtherReason(row);
        } else {
            $('#pTooltip' + id).attr("title", "");
            $('#pTooltip' + id).attr("data-original-title", "");
        }
    },

    SetOtherReason: function (_result) {
        //debugger;
        var _TRNF_REASON_OTHER = _result["TRNF_REASON_OTHER"];
        control.WFD02410MessageAlertReasonOther.hide();
        control.WFD02410_SetOtherReason.appendTo("body").modal('show');
        //TempFileWFD02410 = [];
        control.WFD02410_TRNF_REASON_OTHER.val(_TRNF_REASON_OTHER);

    },
}

var fCostSearchAction = {
    ClearDefault: function () {
        ClearMessage("#AlertMessagecomCC2410");
        $('#FIND').val('');
        $('#CostSearchDataRow').hide();
    },
    Company : "",
    searchOption: "",
    callback : null,
    Search : function () {

        //CostCenterDataTatable
        var pagin = control.CostPagin.GetPaginData();

        var _data = {
            FIND: $('#FIND').val(),
            IsSearchRespCostCode : fCostSearchAction.IsSearchRespCostCode == "Y",
            COMPANY: fCostSearchAction.Company
        }
        ajax_method.SearchPost(UrlWFD02410.SearchCostCenter, _data, pagin, true, function (datas, pagin) {

            if (datas != null && datas.length > 0) {
                if (pagin.OrderColIndex == null) pagin.OrderColIndex = 0;
                if (!pagin.OrderType) pagin.OrderType = 'asc';

                if ($.fn.DataTable.isDataTable($('#CostSearchResultTable'))) {
                    var table = $('#CostSearchResultTable').DataTable();
                    table.destroy();
                }

                $('#CostSearchResultTable').DataTable({
                    data: datas,
                    "columns": [
                        { "data": "COST_CODE" },
                        { "data": "COST_NAME" , className: 'cursorPointer'},
                        { "data": "EMP_NAME", className: 'cursorPointer' },
                        { "data": "STATUS", className: 'cursorPointer' }
                    ],
                    'columnDefs': [
                    { 'targets': [0], className: 'text-center cursorPointer' }],
                    //
                    'order': [],
                    "paging": false,
                    searching: false,
                    paging: false,
                    retrieve: true,
                    "bInfo": false,
                    "autoWidth": false
                });

                control.CostPagin.Init(pagin, fCostSearchAction.Costsorting, fCostSearchAction.CostchangePage, 'itemPerPage');
                $('#CostSearchDataRow').show();

            } else {
                AlertTextErrorMessagePopup(Message.MSTD0059AERR, "#AlertMessagecomCC2410");
               
                $('#CostSearchDataRow').hide();
                control.CostPagin.Clear();
                window.scrollTo(0, 0);
            }

        }, null);
        
        $(document).off("dblclick", "#CostSearchResultTable tr");
        $(document).on("dblclick", "#CostSearchResultTable tr", function () {
            var $this = $(this);
            var row = $this.closest("tr");
            row.find('td:eq(0)');
            row.find('td:first').text();


            if (!$.fn.DataTable.isDataTable($('#CostSearchResultTable'))) {
                return;
            }
           
            _row = $('#CostSearchResultTable').DataTable().row(this).data(); // Get source data

            fCostSearchAction.callback(_row);

            $('#SearchCostCenter').modal('hide');
            return;
        });
    },
    Costsorting : function (PaginData) {
        fCostSearchAction.Search();
    },

    // Callback function after change pagination on table
     CostchangePage : function (PaginData) {
         fCostSearchAction.Search();
    }

}

var fGetDropdown = {
    GetWBSCosProject: function () {
        control.WBSPROJECTS = [];
        ajax_method.Post(UrlWFD02410.GetDropdownWBSCosProject, { COMPANY: GLOBAL.COMPANY }, false
                , function (result) {
                    if (result == null) {
                        return ;
                    }
                    control.WBSPROJECTS = result;
                    //$('#WFD02410ComBoWBSCosProject').html('');
                    //var optionhtml1 = '<option value="">' + "Select" + '</option>';
                    //$('#WFD02410ComBoWBSCosProject').append(optionhtml1);
                    //$.each(result, function (i) {
                    //    var optionhtml = '';
                    //    optionhtml = '<option value="' + result[i].WBS_CODE + '">' + result[i].DESCRIPTION + '</option>';
                    //    $('#WFD02410ComBoWBSCosProject').append(optionhtml);
                    //});

                //    $("#WFD02410ComBoWBSCosProject").select2("val", _value);
                    
                }, null);
    },
    InitWBSProjectSelect2: function (key, value) {
        $('#WFD02410ComBoWBSCosProject').html('');
        $('#WFD02410ComBoWBSCosProject').append('<option value="">' + "Select" + '</option>');

        var isHasItem = false;

        $.each(control.WBSPROJECTS, function (i, item) {
            var optionhtml = '';

            if ((key === item.WBS_CODE) && !isHasItem) {
                isHasItem = true;
            }

            optionhtml = '<option value="' + item.WBS_CODE + '">' + item.WBS_CODE + ' - ' + item.DESCRIPTION + '</option>';
            $('#WFD02410ComBoWBSCosProject').append(optionhtml);
        });

        if (key && !isHasItem) {
            $('#WFD02410ComBoWBSCosProject').append('<option value="' + key + '">' + key + ' - ' +  value + '</option>');
        }

        $("#WFD02410ComBoWBSCosProject").select2("destroy");
        $('#WFD02410ComBoWBSCosProject').select2({ dropdownCssClass: 'auto-drop', dropdownAutoWidth: true });
    },
    GetToEmployee: function (_selectedRow, callback) {

        
        var _data = {
            COMPANY: GLOBAL.COMPANY,
            COST_CODE: ((GLOBAL.FLOW_TYPE == constants.CostCenterTransfer || GLOBAL.FLOW_TYPE == constants.MassCostCenterTransfer)  ? $('#WFD02410_NEW_OWNER_COST_CODE').val() : _selectedRow.COST_CODE)
        };
        ajax_method.Post(UrlWFD02410.GetDropdownToEmployee, _data, false
                , function (result) {
                    if (result == null) {
                        return
                    }
                    $('#WFD02410ComBoToEmployee').html('');
                    var optionhtml1 = '<option value="">' + "Select" + '</option>';
                    $('#WFD02410ComBoToEmployee').append(optionhtml1);
                    $.each(result, function (i) {
                        var optionhtml = '';
                         optionhtml = '<option value="' + result[i].CODE + '">' + result[i].VALUE + '</option>';
                        $('#WFD02410ComBoToEmployee').append(optionhtml);
                    });

                    $("#WFD02410ComBoToEmployee").select2("val", _selectedRow.TO_EMPLOYEE);

                    if(callback !=null)
                        callback();
                   
                }, null);
    },
    GetLocation: function () {
        control.LOCATIONS = [];
        ajax_method.Post('Common/Location_AutoComplete', { COMPANY: GLOBAL.COMPANY }, false
                , function (result) {
                    if (result == null) {
                        return;
                    }
                    //REMOVE EMPTY OPTIONS
                    control.LOCATIONS = result;
                    control.LOCATIONS.splice(0,1);
                    //$('#WFD02410ComBoLocation').html('');
                    //var optionhtml1 = '<option value="">' + "Select" + '</option>';
                    //$('#WFD02410ComBoLocation').append(optionhtml1);
                    //$.each(result, function (i) {
                    //    var optionhtml = '';
                    //    optionhtml = '<option value="' + result[i].CODE + '">' + result[i].VALUE + '</option>';
                    //    $('#WFD02410ComBoLocation').append(optionhtml);
                    //});
                }, null);
    },
    InitLocationSelect2: function (key, value) {
        //var source = [];
        //source.push({ id: "", text: "Select" });
        //$.each(control.LOCATIONS, function (i, item) {
        //    source.push({ id: item.CODE, text: item.VALUE });
        //});

        $('#WFD02410ComBoLocation').html('');
        $('#WFD02410ComBoLocation').append('<option value="">' + "Select" + '</option>');

        var isHasItem = false;

        $.each(control.LOCATIONS, function (i, item) {
            var optionhtml = '';

            if ((key === item.CODE) && !isHasItem) {
                isHasItem = true;
            }

            optionhtml = '<option value="' + item.CODE + '">' + item.CODE + ' - ' + item.VALUE + '</option>';
            $('#WFD02410ComBoLocation').append(optionhtml);
        });

        if (key && !isHasItem) {
            $('#WFD02410ComBoLocation').append('<option value="' + key + '">' + key + ' - '  + value + '</option>');
        }

        $("#WFD02410ComBoLocation").select2("destroy");
        $('#WFD02410ComBoLocation').select2({ dropdownCssClass: 'auto-drop', dropdownAutoWidth: true });

        //function template(source) {

        //  var html ='<dl>' +
        //                '<dt>'+ source.id +'</dt>' +
        //                '<dd>' + source.text + '</dd>' +
        //            '</dl>';

        //    return html;
        //}

        //$("#WFD02410ComBoLocation").select2({
        //    data: source,
        //    placeholder: 'Select',
        //    dropdownAutoWidth: true,
        //    templateResult: template,
        //    escapeMarkup: function (markup) {
        //        return markup;
        //    }
        //});
    }
}

$('#chkNormal').click(function (e) {
    if ($(this).is(':checked')) {
        $('#chkMass').prop('checked', false);
    }
})
$('#chkMass').click(function (e) {
    if ($(this).is(':checked')) {
        $('#chkNormal').prop('checked', false);
    }
})

// Control Event
$('#btnDetailTransDialogOK').click(function (e) {
    fAction.UpdateRow();
    $('#divDetailTransDialog').modal('hide');
})

$('#WFD02410BtnNEW_OWNER_COST_NAME').click(function (e) {

    if (GLOBAL.DOC_NO != '')
        return;

    if ($('#WFD01170ddpCompany option:selected').val() == "") {
        AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format('', 'Company'), "#AlertMessageArea");
        window.scrollTo(0, 0);
        return;
    }

    fCostSearchAction.ClearDefault();
    fCostSearchAction.IsSearchRespCostCode = "N";
    fCostSearchAction.Company = $('#WFD01170ddpCompany').val();
    $('#lblCostCenterTitle').html("Search Cost Center");
    $('#lblHeader').html("FA Window Name")
    fCostSearchAction.callback = function (result) {
        fAction.GetNewAssetOwner(false, result);
        $('#divSearchCostCenter').modal('hide');
    };
    $('#divSearchCostCenter').modal();

})

$('#WFD02410BtnNEW_OWNER_RESP_COST_NAME').click(function (e) {

    if (GLOBAL.DOC_NO != '')
        return;

    if ($('#WFD01170ddpCompany option:selected').val() == "") {
        AlertTextErrorMessagePopup(CommonMessage.ShouldNotBeEmpty.format('', 'Company'), "#AlertMessageArea");
        window.scrollTo(0, 0);
        return;
    }

    fCostSearchAction.ClearDefault();
    fCostSearchAction.IsSearchRespCostCode = "Y";
    fCostSearchAction.Company = $('#WFD01170ddpCompany').val();
    $('#lblCostCenterTitle').html("Search Responsible Cost Center");
    $('#lblHeader').html("FA Supervisor")
    fCostSearchAction.callback = function (result) {
        if (GLOBAL.FLOW_TYPE == constants.ResponseTransfer || GLOBAL.FLOW_TYPE == constants.MassResponseTransfer) //Responsible CC
            fAction.GetNewAssetOwner(true, result);
        else {
            $('#WFD02410_NEW_OWNER_RESP_COST_CODE').val(result.COST_CODE);
            $('#WFD02410_NEW_OWNER_RESP_COST_NAME').val(result.COST_NAME);
            
        }
        $('#divSearchCostCenter').modal('hide');
    };
    $('#divSearchCostCenter').modal();

})

$('#btnCC2410Search').click(function (e) {

    
    fCostSearchAction.Search();
})

$('#btnCC2410Close').click(function (e) {
    $('#divSearchCostCenter').modal('hide');
})

$('#WFD02410btnUploadTMCDocument').click(function () {

    var text = "Upload TMC Document"; //"Upload TMC Document Asset No. : " + controlUpload.ASSET_NO + " Asset Sub : " + controlUpload.ASSET_SUB;
    fPopup.ShowMainUploadDialog(text, "TMC", PERMISSION.ActionMark == constants.TMC);
})

$('#WFD02410_TRNF_REASON_OTHER_SAVE').click(function (e) {
    fAction.UpdateReasonOther();
    control.WFD02410_SetOtherReason.modal('hide');
})

$('#WFD02410_TRNF_REASON_OTHER_CANCEL').click(function (e) {
    if (control.selectedRow != null && control.selectedDDLTranferReason != null) {
        //alert(control.selectedRow);
        var _row = control.DataTable.row(control.selectedRow).data();
        _row.TRNF_REASON_OTHER = _row.TRNF_REASON_OTHER;
        _row.TRNF_REASON = _row.TRNF_REASON;
        
        $('#pTooltip' + control.selectedDDLTranferReason).attr("title", _row.TRNF_REASON_OTHER);
        $('#pTooltip' + control.selectedDDLTranferReason).attr("data-original-title", _row.TRNF_REASON_OTHER);
        //control.DataTable.row(control.selectedRow).data(_row).draw(false);
        //$('.datepicker').datepicker({
        //    dateFormat: 'dd.mm.yy',
        //    minDate: _row.TRANS_START_DATE,
        //    //comment the beforeShow handler if you want to see the ugly overlay 
        //    beforeShow: function () {
        //        setTimeout(function () {
        //            $('.ui-datepicker').css('z-index', 99999999999999);
        //        }, 0);
        //    }
        //});
    }
    control.WFD02410_SetOtherReason.modal('hide');
})


$('#btnWFD02410_SetOtherReasonCloseModal').click(function (e) {
    if (control.selectedRow != null && control.selectedDDLTranferReason != null) {
        //alert(control.selectedRow);
        var _row = control.DataTable.row(control.selectedRow).data();
        _row.TRNF_REASON_OTHER = _row.TRNF_REASON_OTHER;
        _row.TRNF_REASON = _row.TRNF_REASON;
        $('#pTooltip' + control.selectedDDLTranferReason).attr("title", _row.TRNF_REASON_OTHER);
        $('#pTooltip' + control.selectedDDLTranferReason).attr("data-original-title", _row.TRNF_REASON_OTHER);
        //control.DataTable.row(control.selectedRow).data(_row).draw(false);
        //$('.datepicker').datepicker({
        //    dateFormat: 'dd.mm.yy',
        //    minDate: _row.TRANS_START_DATE,
        //    //comment the beforeShow handler if you want to see the ugly overlay 
        //    beforeShow: function () {
        //        setTimeout(function () {
        //            $('.ui-datepicker').css('z-index', 99999999999999);
        //        }, 0);
        //    }
        //});
    }
    control.WFD02410_SetOtherReason.modal('hide');
})