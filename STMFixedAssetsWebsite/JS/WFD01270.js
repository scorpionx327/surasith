﻿var WFD01270Controls = {
    WFD01270Submit: $('#WFD01270Submit'),
    WFD01270Approve: $('#WFD01270Approve'),
    WFD01270Reject: $('#WFD01270Reject'),
    //===========Approver==========//
    WFD01270GenerateApprover: $('#WFD01270GenerateApprover'),
    WFD01270ResetApprover: $('#WFD01270ResetApprover'),
    WFD01270SaveApprover:$('#WFD01270SaveApprover'),
    ApproverTable1: $('#WFD02190_ApproverTable1'),
    ApproverTable2: $('#WFD02190_ApproverTable2'),
    DataTable: null,

    WFD01270_Main: $('#WFD01270_Main'),
    WFD01270_NoAuthorize: $('#WFD01270_NoAuthorize'),

    //Master
    COMPANY: $('#WFD01270_COMPANY'),
    DOC_NO: $('#WFD01270_DOC_NO'),
    STATUS: $('#WFD01270_STATUS'),

    //Requester
    REQUEST_DATE: $('#WFD01270_REQUEST_DATE'),
    COST_NAME: $('#WFD01270_COST_NAME'),
    EMP_CODE: $('#WFD01270_EMP_CODE'),
    POS_NAME: $('#WFD01270_POS_NAME'),
    EMP_NAME: $('#WFD01270_EMP_NAME'),
    DEPT_NAME: $('#WFD01270_DEPT_NAME'),
    PHONE_NO: $('#WFD01270_PHONE_NO'),
    SECT_NAME: $('#WFD01270_SECT_NAME'),
    WFD01270_UPDATE_DATE: $('#WFD01270_UPDATE_DATE'),
    //Comment Info
    COMMENT_INFO_TEXT: $('#WFD01270_COMMENT_INFO'),

    // Include for click redirect to home screen and select tab same auto redirect by Pawares M. 20180712
    WFD01270_CloseRequester: $('#WFD01270_CloseRequester'),

    WFD01270_AlertSumbit: $('#WFD01270_AlertSumbit'),
    WFD01270_DisplayMessage: $('#WFD01270_DisplayMessage'),
    WFD01270_RedirectInformation: $('#WFD01270_RedirectInformation')
};

var CheckPageLoaded = 'N';

var WFD01270FileUplaodControls = {
    Textbox: $('#WFD01270FileUploadTB'),
    File: $('#WFD01270_UPLOAD_COMMENT_INFO_FILE'),
    Button: $('#WFD01270_UPLOAD_COMMENT_INFO_BTN'),
    DelFileBTN: $('#WFD01270_UPLOAD_COMMENT_INFO_DEL'),
    DownloadFileBTN: $('#WFD01270_UPLOAD_COMMENT_INFO_DOWNLOAD'),
    ViewFileBTN: $('#WFD01270_UPLOAD_COMMENT_INFO_VIEW')
}

// Require fields default   
var requiredMainControlWFD01270 = [WFD01270Controls.PHONE_NO, WFD01270Controls.COMPANY];

$(function () {
    WFD01270Controls.WFD01270Submit.hide();
    WFD01270Controls.WFD01270Approve.hide();
    WFD01270Controls.WFD01270Reject.hide();
    WFD01270Controls.WFD01270SaveApprover.hide(); 
    WFD01270Controls.WFD01270GenerateApprover.hide();
    WFD01270Controls.WFD01270ResetApprover.hide();

    setWFD01270ControlsRequiredField(requiredMainControlWFD01270, true);

    SetDisplayScreen();

    initialRequester();

    WFD01270InitialDataForApprove();

    WFD01270Controls.WFD01270Submit.click(WFD01270SubmitData);
    WFD01270Controls.WFD01270Approve.click(WFD01270ApproveData);
    WFD01270Controls.WFD01270Reject.click(WFD01270RejectData);

    WFD01270Controls.WFD01270GenerateApprover.click(WFD01270ClickGenerateApprover);
    WFD01270Controls.WFD01270ResetApprover.click(WFD01270ResetApproverData);

    WFD01270FileUplaodControls.Button.click(WFD01270UploadCommentFileClick);

    //Add New By Surasith T. 2019-07-01
    WFD01270Controls.WFD01270SaveApprover.click(WFD01270ClickSaveApprover);

    // Add function button click for redirect to home screen same as auto redirect by Pawares M. 20180712
    WFD01270Controls.WFD01270_CloseRequester.click(WFD01270CloseRequesterClick);

    if (SCREEN_PARAMS.MODE == "V") {
        WFD01270Controls.WFD01270GenerateApprover.hide();
        WFD01270Controls.WFD01270SaveApprover.hide();
        WFD01270Controls.WFD01270ResetApprover.hide();
        $('#WFD02190_ApproverTable1').prop('disabled', true);
        $('#WFD02190_ApproverTable2').prop('disabled', true);
        $('select').prop('disabled', 'none');

        $('ddlApprover_2').prop('disabled', 'none');
    }



});

var bStockViewStatus = false;
//==================== Start Set Display Screen ====================//
var SetDisplayScreen = function () {
    WFD01270Controls.WFD01270_NoAuthorize.hide();
    switch (SCREEN_PARAMS.MODE) {
        case 'N':   //-- N : No authorize
            WFD01270Controls.WFD01270_Main.hide();
            WFD01270Controls.WFD01270_NoAuthorize.show();
            document.getElementById('WFD01270_NoAuthorize').style.display = '';
            break;
        case 'A':   //-- A : Approve
            WFD01270Controls.WFD01270_Main.show();
            if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
                WFD01270Controls.WFD01270Approve.hide();
                WFD01270Controls.WFD01270Reject.hide();
                WFD01270Controls.WFD01270Submit.show();
                WFD01270Controls.WFD01270GenerateApprover.show();
                WFD01270Controls.WFD01270SaveApprover.show();
                WFD01270Controls.WFD01270ResetApprover.show();
                WFD01270setDisableButton('#WFD01270Submit');
                WFD01270setDisableButton('#WFD01270ResetApprover');
            } else {
                WFD01270Controls.WFD01270Submit.hide();
                WFD01270Controls.WFD01270SaveApprover.hide();
                WFD01270Controls.WFD01270GenerateApprover.hide();
                WFD01270Controls.WFD01270ResetApprover.hide();
                WFD01270setDisableButton('#WFD01270_PHONE_NO');
                $('#WFD01270_PHONE_NO').removeClass('require');
                WFD01270Controls.WFD01270Approve.show();
                WFD01270Controls.WFD01270Reject.show();
                if (SCREEN_PARAMS.DOC_STATUS == '90' || SCREEN_PARAMS.DOC_STATUS == '99') {
                    WFD01270Controls.WFD01270GenerateApprover.show();
                    WFD01270Controls.WFD01270SaveApprover.show();
                    WFD01270Controls.WFD01270ResetApprover.show();
                    WFD01270setDisableButton('#WFD01270GenerateApprover');
                    $("#WFD01270Approve").html('<i class="fa fa-check"></i> Re-Submit ');
                    WFD01270setEnableButton('#WFD01270ResetApprover');
                }
            }
            if (SCREEN_PARAMS.HIGHER == 'Y' || SCREEN_PARAMS.IS_ORG_CHANGED == 'Y') {
                document.getElementById('WFD01270_WARNING').style.display = '';
                if (SCREEN_PARAMS.IS_ORG_CHANGED == 'Y') {
                    $("#WFD01270_WARNING").html('<p>' + WFD01270_Message.OrgChanged + '</p>');//(OrgChanged)
                }
            } else {
                document.getElementById('WFD01270_WARNING').style.display = 'none';
            }


            //if (SCREEN_PARAMS.IS_ORG_CHANGED == 'Y') {
            //    document.getElementById('WFD01270_WARNING').style.display = '';
            //    $("#WFD01270_WARNING").html('<p>' + WFD01270_Message.OrgChanged + '</p>');
            //} else {
            //    document.getElementById('WFD01270_WARNING').style.display = 'none';
            //    $("#WFD01270_WARNING").html('<p>' + WFD01270_Message.OrgChanged + '</p>');
            //} //(OrgChanged)
            break;
        case 'V':   //-- V : View Only
            WFD01270Controls.WFD01270_Main.show();
            WFD01270Controls.WFD01270Submit.hide();
            WFD01270Controls.WFD01270Approve.hide();
            WFD01270Controls.WFD01270Reject.hide();
            WFD01270Controls.WFD01270GenerateApprover.hide();
            WFD01270Controls.WFD01270ResetApprover.hide();
            WFD01270setDisableButton('#WFD01270_PHONE_NO');
            $('#WFD01270_PHONE_NO').removeClass('require');
            WFD01270setDisableButton('#WFD01270_COMMENT_INFO');
            WFD01270setDisableButton('#WFD01270_UPLOAD_COMMENT_INFO_BTN');

            bStockViewStatus = true;

            break;
        case 'R':   //-- R : VERIFY
            WFD01270Controls.WFD01270_Main.show();
            WFD01270Controls.WFD01270Submit.hide();
            WFD01270Controls.WFD01270Approve.show();
            WFD01270Controls.WFD01270Reject.show();
            $("#WFD01270Approve").html('<i class="fa fa-check"></i> Verify & resend ');
            WFD01270setDisableButton('#WFD01270_PHONE_NO');
            $('#WFD01270_PHONE_NO').removeClass('require');
            WFD01270Controls.WFD01270GenerateApprover.show();
            WFD01270setDisableButton('#WFD01270GenerateApprover');
            WFD01270Controls.WFD01270ResetApprover.show();
            WFD01270setDisableButton('#WFD01270ResetApprover');
            break;
        default:
            WFD01270Controls.WFD01270_Main.hide();
            WFD01270Controls.WFD01270_NoAuthorize.show();
            break;
    }
    if (SCREEN_PARAMS.ALLOW_APPROVE != 'Y') {
        //WFD01270Controls.WFD01270Approve.show();
        WFD01270setDisableButton('#WFD01270Approve');
    }
    if (SCREEN_PARAMS.ALLOW_REJECT != 'Y') {
        //WFD01270Controls.WFD01270Reject.show();
        WFD01270setDisableButton('#WFD01270Reject');
    }
    $("#WFD02190_REQUEST_TYPE").val(SCREEN_PARAMS.REQUEST_TYPE);
}

var WFD01270setDisableButton = function (id) {
    $(id).addClass('disabled');
    $(id).removeClass('active');
    $(id).prop('disabled', true);
}

var WFD01270setEnableButton = function (id) {
    //$(id).addClass('active');
    $(id).removeClass('disabled');
    $(id).prop('disabled', false);
    //$(id).on("click");
}

var setWFD01270ControlsRequiredField = function (controls, isRequired) {
    for (var i = 0; i < controls.length; i++) {
        if (isRequired == true) {
            if (controls[i].hasClass('select2')) {
                controls[i].parent().find('.select2-container').addClass("require");
            } else {
                controls[i].addClass('require');
            }
        } else {
            if (controls[i].hasClass('select2')) {
                controls[i].parent().find('.select2-container').removeClass("require");
            } else {
                controls[i].removeClass("require");
            }
        }
    }
}

var WFD01270InitialDataForApprove = function () {
    if (SCREEN_PARAMS.DOC_NO != '' && SCREEN_PARAMS.DOC_NO !== null) {
        WFD01270GenerateApproverData("N");
        WFD01270GetCommentHistoryByDocNoData();
        WFD01270GetWorkflowTrackingProcessData();
    }
}

//==================== End Set Display Screen ====================//


//==================== Start Get Data For Operation ====================//
var getDefaultSceenWFD01270Value = function () {
    return {
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        REQUEST_TYPE: SCREEN_PARAMS.REQUEST_TYPE,
        EMP_CODE: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID
    }
}

var WFD02210DataApprover = function () {
    var datas = [];
    $("#WFD02190_ApproverTable1 tr").each(function () {
        var $this = $(this);
        var row = $this.closest("tr");
        if (row.find('td:eq(0)').text() != "" && row.find('td:eq(0)').text() != null) {
            datas.push({
                "INDX": row.find('td:eq(4)').text(),
                "ROLE": row.find('td:eq(5)').text(),
                "DIVISION": row.find('td:eq(6)').text(),
                "EMP_CODE": $(this).find('select option:selected').val(),
                "REQUIRE_FLAG": row.find('td:eq(3)').text()
            });
        }
    });
    $("#WFD02190_ApproverTable2 tr").each(function () {
        var $this = $(this);
        var row = $this.closest("tr");
        if (row.find('td:eq(0)').text() != "" && row.find('td:eq(0)').text() != null) {
            datas.push({
                "INDX": row.find('td:eq(4)').text(),
                "ROLE": row.find('td:eq(5)').text(),
                "DIVISION": row.find('td:eq(6)').text(),
                "EMP_CODE": $(this).find('select option:selected').val(),
                "REQUIRE_FLAG": row.find('td:eq(3)').text()
            });
        }
    });
    return datas;
}

var WFD01270GenerateApproverData = function (action) {
    ClearMessageC();
    var PARAMs = {
        COMPANY: "STM",
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        REQUEST_TYPE: SCREEN_PARAMS.REQUEST_TYPE,
        EMP_CODE: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID,
        USER_BY: SCREEN_PARAMS.EMP_CODE,
        COST_CENTER_TO: SCREEN_PARAMS.COST_CENTER_TO
    }
    var WFD01270_Main = WFD01270GetValueFromScreen();
    var _url = URL_WFD01270.GET_APPROVER;
    if (action == "Y")
        _url = URL_WFD01270.GENERATE_APPROVER;
    ajax_method.Post(_url, WFD01270_Main, false, function (result) {
        //alert(result.STR_HTML);
        if (result.STR_HTML != '') {
            $('#WFD01270Approver').html(result.STR_HTML);

            $('#WFD01270Approver select').change(function (e) {
                if ($(this).is(':disabled'))
                    return;

                var _chk = $(this);
                _chk.closest("tr").find('td').eq(2).html('').fadeIn('fast');

                var EMP_DATA = { Approver: $(this).val() };
                ajax_method.Post(URL_WFD01270.GET_APPROVER_INFO, EMP_DATA, false, function (empResult) {
                    console.log(empResult.DIV_NAME);
                    console.log(_chk);
                    if (empResult == null || empResult == "-99")
                        return;

                    _chk.closest("tr").find('td').eq(2).html(empResult.DIV_NAME).fadeIn('fast');
                });
            });

            if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
                WFD01270setEnableButton('#WFD01270Submit');
                WFD01270setEnableButton('#WFD01270ResetApprover');
                WFD01270setDisableButton('#WFD01270GenerateApprover');
                switch (SCREEN_PARAMS.REQUEST_TYPE) {
                    case 'C':
                        WFD02210_DisableGenerate();
                        break;
                    case 'T':
                        WFD02410_DisableGenerate();
                        break;
                    case 'D':
                        WFD02510_DisableGenerate();
                        break;
                    case 'P':
                        WFD02310_DisableGenerate();
                        break;
                    case 'R':
                        WFD02310_DisableGenerate();
                        break;
                    case 'L':
                        WFD02310_DisableGenerate();
                        break;
                    case 'S':
                        WFD02640_DisableGenerate();
                        break;
                    default:
                        break;
                }

                //$(".select2").select2();

            } else {
                WFD01270Controls.WFD01270Submit.hide();
                if (SCREEN_PARAMS.ALLOW_SEL_APPV == 'Y') {
                    WFD01270Controls.WFD01270ResetApprover.show();
                    WFD01270Controls.WFD01270GenerateApprover.show();
                    WFD01270setEnableButton('#WFD01270ResetApprover');
                    WFD01270setDisableButton('#WFD01270GenerateApprover');
                    WFD01270setDisableButton('#WFD01270SaveApprover');
                } else {
                    WFD01270Controls.WFD01270ResetApprover.hide();
                    WFD01270Controls.WFD01270GenerateApprover.hide();
                    WFD01270Controls.WFD01270SaveApprover.hide();
                }
                // -- Add by Uten for support incase resubmit 
                if (SCREEN_PARAMS.MODE == 'A' && (SCREEN_PARAMS.DOC_STATUS == '90' || SCREEN_PARAMS.DOC_STATUS == '99')) {
                    //WFD01270setEnableButton('#WFD01270ResetApprover');
                    //WFD01270setDisableButton('#WFD01270GenerateApprover');
                    switch (SCREEN_PARAMS.REQUEST_TYPE) {
                        case 'S':
                            WFD02640_DisableGenerate();
                            break;
                        default:
                            break;
                    }
                }
                // -- By Uten
            }
            if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
                ajax_method.Post("/WFD01270/GetTotalDocument", PARAMs, false, function (result) {
                    if (result != "") {
                        $('#WFD01270Multipledocument').html("<div class=\"box-body\"><div class=\"callout callout-info\"><p>" + result + "</p></div></div>");
                    } else {
                        $('#WFD01270Multipledocument').html("");
                    }
                }, null);
            }
        }
    }, null);


}
//WFD02310Controls.SearchResultTable.find('input[id^="DAMAGE"]').each(function (e) {


var WFD01270GetCommentHistoryByDocNoData = function () {
    var PARAMs = {
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        REQUEST_TYPE: SCREEN_PARAMS.REQUEST_TYPE,
        EMP_CODE: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID
    }
    $("#CommentHistory").load("/WFD01270/CommentHistory", PARAMs);

}

var WFD01270GetWorkflowTrackingProcessData = function () {
    var PARAMs = {
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        REQUEST_TYPE: SCREEN_PARAMS.REQUEST_TYPE,
        EMP_CODE: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID
    }
    $("#WFD01270WorkflowTrackingProcess").load("/WFD01270/WorkflowTrackingProcess", PARAMs);
}
//==================== End Get Data For Operation ====================//

//==================== Start Data In Table ====================//
var initialRequester = function () {
    WFD01270_Param = getDefaultSceenWFD01270Value();
    ajax_method.Post(URL_WFD01270.GET_REQUESTOR, WFD01270_Param, true
        , function (result) {
            if (result != null) {

                WFD01270Controls.COMPANY.val(SCREEN_PARAMS.COMPANY);
                WFD01270Controls.DOC_NO.val(SCREEN_PARAMS.DOC_NO);
                WFD01270Controls.STATUS.val(result.STATUS);

                //Change Requirement : Show blank when all cost center is in request processing
                if (bStockViewStatus && result.STATUS == "Wait for submit") {
                    WFD01270Controls.STATUS.val('');
                    //Add Warning Block
                    document.getElementById('WFD01270_WARNING').style.display = '';
                    $("#WFD01270_WARNING").html('<p>' + WFD02640_Message.AllCostCenterIsSelected + '</p>');

                }

                WFD01270Controls.REQUEST_DATE.val(result.REQUEST_DATE);
                WFD01270Controls.COST_NAME.val(result.COST_NAME);
                WFD01270Controls.EMP_CODE.val(result.EMP_CODE);
                WFD01270Controls.POS_NAME.val(result.POS_NAME);
                WFD01270Controls.EMP_NAME.val(result.EMP_NAME);
                WFD01270Controls.DEPT_NAME.val(result.DEPT_NAME);
                WFD01270Controls.PHONE_NO.val(result.PHONE_NO);
                WFD01270Controls.SECT_NAME.val(result.SECT_NAME);
                WFD01270Controls.WFD01270_UPDATE_DATE.val(result.UPDATE_DATE);
                SCREEN_PARAMS.UPDATE_DATE = result.UPDATE_DATE;

                WFD01270Controls.EMP_NAME.attr({
                    "data-toggle": "tooltip",
                    "data-placement": "top",
                    "title": result.EMP_NAME
                });
                WFD01270Controls.POS_NAME.attr({
                    "data-toggle": "tooltip",
                    "data-placement": "top",
                    "title": result.POS_NAME
                });
                WFD01270Controls.DEPT_NAME.attr({
                    "data-toggle": "tooltip",
                    "data-placement": "top",
                    "title": result.DEPT_NAME
                });
                WFD01270Controls.SECT_NAME.attr({
                    "data-toggle": "tooltip",
                    "data-placement": "top",
                    "title": result.SECT_NAME
                });
                WFD01270Controls.COST_NAME.attr({
                    "data-toggle": "tooltip",
                    "data-placement": "top",
                    "title": result.COST_NAME
                });
            }

        }, null);
}
//==================== End Data In Table ====================//

//==================== Start Even Button ====================//
var WFD01270RequestHeaderData = function () {
    var ListRequestHeaderData = {
        COMPANY: SCREEN_PARAMS.COMPANY,
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        REQUEST_TYPE: SCREEN_PARAMS.REQUEST_TYPE,
        EMP_CODE: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID,
        USER_BY: SCREEN_PARAMS.USER_BY,
        UPDATE_DATE: SCREEN_PARAMS.UPDATE_DATE,
        MODE: SCREEN_PARAMS.MODE,
        DELEGATE: SCREEN_PARAMS.DELEGATE,
        MAIN: SCREEN_PARAMS.MAIN,
        HIGHER: SCREEN_PARAMS.HIGHER,
        FA: SCREEN_PARAMS.FA,
        ALLOW_APPROVE: SCREEN_PARAMS.ALLOW_APPROVE,
        ALLOW_REJECT: SCREEN_PARAMS.ALLOW_REJECT
    }
    console.log(ListRequestHeaderData);
    return ListRequestHeaderData;
}

var WFD01270GetValueFromScreen = function () {
    var ListRequestHeaderData = {
        COMPANY:WFD01270Controls.COMPANY.val(),
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        REQUEST_TYPE: SCREEN_PARAMS.REQUEST_TYPE,
        EMP_CODE: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID,
        USER_BY: SCREEN_PARAMS.USER_BY,
        UPDATE_DATE: SCREEN_PARAMS.UPDATE_DATE,
        MODE: SCREEN_PARAMS.MODE,
        DELEGATE: SCREEN_PARAMS.DELEGATE,
        MAIN: SCREEN_PARAMS.MAIN,
        HIGHER: SCREEN_PARAMS.HIGHER,
        FA: SCREEN_PARAMS.FA,
        ALLOW_APPROVE: SCREEN_PARAMS.ALLOW_APPROVE,
        ALLOW_REJECT: SCREEN_PARAMS.ALLOW_REJECT,
        ALLOW_SEL_APPV: SCREEN_PARAMS.ALLOW_SEL_APPV,
        IS_BOI_STATE: SCREEN_PARAMS.IS_BOI_STATE,
        IS_STORE_STATE: SCREEN_PARAMS.IS_STORE_STATE,
        IS_REQ: SCREEN_PARAMS.IS_REQ,
        VIEW_BOI_STATE: SCREEN_PARAMS.VIEW_BOI_STATE,
        VIEW_STORE_STATE: SCREEN_PARAMS.VIEW_STORE_STATE,
        DOC_STATUS: SCREEN_PARAMS.DOC_STATUS,
        COST_CENTER_TO: SCREEN_PARAMS.COST_CENTER_TO
    }
    var ListRequestor = {
        PHONE_NO: WFD01270Controls.PHONE_NO.val()
    }

    var ListApproverList = WFD02210DataApprover();

    var ListCommentInfo = WFD01270LoadCommentInfoData();

    var ListDetailData = WFD01270LoadDetailData();

    var WFD01270Model = {
        RequestHeaderData: ListRequestHeaderData,
        Requestor: ListRequestor,
        ApproverList: ListApproverList,
        CommentInfo: ListCommentInfo,
        DetailData: ListDetailData,
    }

    return WFD01270Model;
}

var WFD01270LoadCommentInfoData = function () {
    var ModelComment = {
        CommentText: WFD01270Controls.COMMENT_INFO_TEXT.val(),
        CommentStatus: SCREEN_PARAMS.COMMENT_STATUS,
        AttachFileName: WFD01270FileUplaodControls.Textbox.val(),
    }
    return ModelComment;
}


var WFD01270LoadDetailData = function () {
    var DetailData;
    var _DetailCIP;
    var _DetailReprint;
    var _DetailTrnsfer;
    var _DetailDisposal;
    var _DetailStock;
    switch (SCREEN_PARAMS.REQUEST_TYPE) {
        case 'C':
            _DetailCIP = WFD02210_GetAssetDataModel();
            break;
        case 'T':
            _DetailTrnsfer = WFD02410_GetAssetDataModel();
            break;
        case 'D':
            _DetailDisposal = WFD02510_GetAssetDataModel();
            break;
        case 'P':
            _DetailReprint = WFD02310_GetAssetDataModel();
            break;
        case 'R':
            _DetailReprint = WFD02310_GetAssetDataModel();
            break;
        case 'L':
            _DetailReprint = WFD02310_GetAssetDataModel();
            break;
        case 'S':
            _DetailStock = WFD02640_GetAssetDataModel();
            break;
        default:
            break;
    }

    DetailData = {
        DetailCIP: _DetailCIP,
        DetailDisposal: _DetailDisposal,
        DetailTransfer: _DetailTrnsfer,
        DetailReprint: _DetailReprint,
        DetailStockTaking: _DetailStock
    }
    return DetailData;
}

var WFD01270SubmitData = function () {

    clicks++

    if (CheckPageLoaded == 'Y') {
        var _DocNoLink = '';
        var _ArrDoc;
        var _Request;
        ClearMessageC();
        SCREEN_PARAMS.COMMENT_STATUS = 'S';
        var WFD01270_Main = WFD01270GetValueFromScreen();

        if (clicks === 1) {
            timer = setTimeout(loadConfirmAlert(WFD01270_Message.ConfirmSubmit, function (result) {
                confirms++;

                if (confirms === 1) {
                    if (result) {
                        ajax_method.Post(URL_WFD01270.SUBMIT_DATA, WFD01270_Main, false, function (result) {
                            if (result != '-99') {
                                $("label[for='WFD01270ALERT_TITLE']").text("Submit data is completed successfully");
                                $("label[for='WFD01270ALERT_SUBJECT']").text("You document no as below :");
                                WFD01270Controls.DOC_NO.val(result);
                                _ArrDoc = result.split("|");
                                for (i = 0; i < _ArrDoc.length; i++) {
                                    if (_ArrDoc[i] != "") {
                                        _Request = _ArrDoc[i].split(":");//= DocNo:RequestType
                                        if (_Request.length > 0 && _Request[0] != "") {
                                            _DocNoLink += '<a href="../WFD01270?RequestType=' + _Request[1] + '&DocNo=' + _Request[0] + '" target="_self">' + _Request[0] + '</a> <br />';
                                        }
                                    }
                                }
                                WFD01270Controls.WFD01270_DisplayMessage.html(_DocNoLink);
                                //WFD01270Controls.WFD01270_AlertSumbit.appendTo("body").modal('show');
                                WFD01270setDisableButton('#WFD01270Reject');// Suphachai L. dialog show enable
                                WFD01270setDisableButton('#WFD01270Submit');
                                WFD01270setDisableButton('#WFD01270Approve');
                                WFD01270Controls.WFD01270_AlertSumbit.modal({ backdrop: 'static', keyboard: false });
                                countdown();
                            }
                        });
                    } else { confirms = 0; }
                }
                clicks = 0;
                confirms = 0;
            }), DELAY);
        } else {
            AlertTextErrorMessagePopup('MSTD0000AWRN : Your oparated (Submit/Re-Submit/Approve/Verify & Resent/Reject) more than one times, please refresh screen and perform your procedure again.', "#AlertMessageArea");
            clearTimeout(timer);
            clicks = 0;
            confirms = 0;
        }

    } else {
        alert("Please wait, Screen is loading");
    }
}



var WFD01270ClickSaveApprover = function () {
    ClearMessageC();
    
    var _data = {
        DOC_NO: WFD01270Controls.DOC_NO.val(),
        APPROVER_LIST: "1#FW#1235|2#MGR#1236|3#DGM#1237|4#GM#1238"
    }
    console.log(_data);
    console.log(URL_WFD01270.SAVE_APPROVER);
    ajax_method.Post(URL_WFD01270.SAVE_APPROVER, _data, false, function (result) {
        if (result != '-99') {
            $("label[for='WFD01270ALERT_TITLE']").text("Change Approver is complete successfully");
            $("label[for='WFD01270ALERT_SUBJECT']").text("You document no as below :");
            _DocNoLink += '<a href="../WFD01270?RequestType=' + SCREEN_PARAMS.REQUEST_TYPE + '&DocNo=' + WFD01270Controls.DOC_NO.val() + '" target="_self">' + WFD01270Controls.DOC_NO.val() + '</a>';
            WFD01270Controls.WFD01270_DisplayMessage.html(_DocNoLink);
            WFD01270Controls.WFD01270_AlertSumbit.modal({ backdrop: 'static', keyboard: false });
            countdown(); //Fixed IN18-4684, Phitak, 20180619
        }
    });

}

var WFD01270ClickGenerateApprover = function () {
    ClearMessageC();
    switch (SCREEN_PARAMS.REQUEST_TYPE) {
        case 'C':
            WFD01270GenerateApproverData("Y");
            break;
        case 'T':
            WFD01270GenerateApproverData("Y");
            break;
        case 'D':
            WFD01270GenerateApproverData("Y");
            break;
        case 'P':
            WFD01270GenerateApproverData("Y");
            break;
        case 'R':
            WFD01270GenerateApproverData("Y");
            break;
        case 'L':
            WFD01270GenerateApproverData("Y");
            break;
        case 'S':
            WFD02640GenerateApprover(WFD01270CheckrtBeforeGenerateApprover);
            break;
        default:
            break;
    }
}

var WFD01270CheckrtBeforeGenerateApprover = function (res) {
    var _returnSave = '-99';
    _returnSave = res;
    if (_returnSave != '-99') {
        WFD01270GenerateApproverData("Y");
    }
}

var WFD01270ApproveData_Final = function () {
    var _DocNoLink = '';
    ClearMessageC();
    SCREEN_PARAMS.COMMENT_STATUS = 'A';
    var WFD01270_Main = WFD01270GetValueFromScreen();
    ajax_method.Post(URL_WFD01270.APPROVE_DATA, WFD01270_Main, false, function (result) {
        if (result != '-99') {
            $("label[for='WFD01270ALERT_TITLE']").text("Approve data is completed successfully");
            $("label[for='WFD01270ALERT_SUBJECT']").text("You document no as below :");
            _DocNoLink += '<a href="../WFD01270?RequestType=' + SCREEN_PARAMS.REQUEST_TYPE + '&DocNo=' + WFD01270Controls.DOC_NO.val() + '" target="_self">' + WFD01270Controls.DOC_NO.val() + '</a>';
            WFD01270Controls.WFD01270_DisplayMessage.html(_DocNoLink);
            //WFD01270Controls.WFD01270_AlertSumbit.appendTo("body").modal('show');
            WFD01270setDisableButton('#WFD01270Reject');  // Suphachai L. dialog show enable
            WFD01270setDisableButton('#WFD01270Submit');
            WFD01270setDisableButton('#WFD01270Approve');
            WFD01270Controls.WFD01270_AlertSumbit.modal({ backdrop: 'static', keyboard: false });
            countdown(); //Fixed IN18-4684, Phitak, 20180619
        }
    });
}

var DELAY = 700, clicks = 0, timer = null, confirms = 0;
var WFD01270ApproveData = function () {

    clicks++

    if (CheckPageLoaded == 'Y') {
        var _DetailUpdate = WFD02410_GetAssetDataModel();
        var _DetailWFD02510Update = WFD02510_GetAssetDataModel();
        //--------------------------------------------------------------------------------
        // Add By Surasith T.
        var _msgConf = WFD01270_Message.ConfirmApprove;
        if (SCREEN_PARAMS.DOC_STATUS == '90') {
            _msgConf = WFD01270_Message.ConfirmReSubmit;
        }
        //--------------------------------------------------------------------------------
        if (clicks === 1) {
            timer = setTimeout(loadConfirmAlert(_msgConf, function (result) {
                confirms++;

                if (confirms === 1) {
                    if (result) {
                    switch (SCREEN_PARAMS.REQUEST_TYPE) {
                        case 'C':
                            WFD01270ApproveData_Final();
                            break;
                        case 'T':
                            ajax_method.Post(URL_WFD02410.UPDATE_ASSETS_TRANSFER_DATA, _DetailUpdate, false, function (result) {
                                if (result != '-99') {
                                    WFD01270ApproveData_Final();
                                }
                            });
                            break;
                        case 'D':
                            ajax_method.Post(URL_WFD02510.UPDATE_ASSETS_DISPOSAL_DATA, _DetailWFD02510Update, false, function (result) {
                                if (result != '-99') {
                                    WFD01270ApproveData_Final();
                                }
                            });
                            break;
                        case 'P':
                            WFD01270ApproveData_Final();
                            break;
                        case 'R':
                            WFD01270ApproveData_Final();
                            break;
                        case 'L':
                            WFD01270ApproveData_Final();
                            break;
                        case 'S':
                            WFD01270ApproveData_Final();
                            break;
                        default:
                            break;
                        }
                    } else { confirms = 0; }
                }

                clicks = 0;
                confirms = 0;
            }), DELAY);
        } else {
            AlertTextErrorMessagePopup('MSTD0000AWRN : Your oparated (Submit/Re-Submit/Approve/Verify & Resent/Reject) more than one times, please refresh screen and perform your procedure again.', "#AlertMessageArea");
            clearTimeout(timer);
            clicks = 0;
            confirms = 0;
        }
    } else {
        alert("Please wait, Screen is loading");
    }
}

var WFD01270RejectData = function () {

    clicks++

    if (CheckPageLoaded == 'Y') {
        var _DocNoLink = '';
        ClearMessageC();
        var WFD01270_Main = WFD01270GetValueFromScreen();
        if (clicks === 1) {
            timer = setTimeout(loadConfirmAlert(WFD01270_Message.ConfirmReject, function (result) {
                confirms++;

                if (confirms === 1) {
                    if (result) {
                        ajax_method.Post(URL_WFD01270.REJECT_DATA, WFD01270_Main, false, function (result) {
                            if (result != '-99') {
                                $("label[for='WFD01270ALERT_TITLE']").text("Reject data is completed successfully");
                                $("label[for='WFD01270ALERT_SUBJECT']").text("You document no as below :");
                                _DocNoLink += '<a href="../WFD01270?RequestType=' + SCREEN_PARAMS.REQUEST_TYPE + '&DocNo=' + WFD01270Controls.DOC_NO.val() + '" target="_self">' + WFD01270Controls.DOC_NO.val() + '</a>';
                                WFD01270Controls.WFD01270_DisplayMessage.html(_DocNoLink);
                                //WFD01270Controls.WFD01270_AlertSumbit.appendTo("body").modal('show');
                                WFD01270setDisableButton('#WFD01270Reject'); // Suphachai L. dialog show enable
                                WFD01270setDisableButton('#WFD01270Submit');
                                WFD01270setDisableButton('#WFD01270Approve');
                                WFD01270Controls.WFD01270_AlertSumbit.modal({ backdrop: 'static', keyboard: false });
                                countdown();
                            } else {
                                if ($('#WFD01270_COMMENT_INFO').val() == null || $('#WFD01270_COMMENT_INFO').val() == '') {
                                    $('#WFD01270_COMMENT_INFO').addClass('require');
                                    confirms = 0;
                                }
                            }
                        });
                    } else { confirms = 0; }
                }

                clicks = 0;
                confirms = 0;
            }), DELAY);
        } else {
            AlertTextErrorMessagePopup('MSTD0000AWRN : Your oparated (Submit/Re-Submit/Approve/Verify & Resent/Reject) more than one times, please refresh screen and perform your procedure again.', "#AlertMessageArea");
            clearTimeout(timer);
            clicks = 0;
            confirms = 0;
        }
    } else {
        alert("Please wait, Screen is loading");
    }
}

var WFD01270ResetApproverData = function () {
    ClearMessageC();
    loadConfirmAlert(WFD01270_Message.ConfirmResetApprover, function (result) {
        if (result) {
            $('#WFD01270Approver').html('');
            WFD01270setEnableButton('#WFD01270GenerateApprover');
            WFD01270setEnableButton('#WFD01270SaveApprover');
            WFD01270setDisableButton('#WFD01270ResetApprover');
            WFD01270setDisableButton('#WFD01270Submit');

            $('#WFD01270Multipledocument').html("");

            switch (SCREEN_PARAMS.REQUEST_TYPE) {
                case 'C':
                    WFD02210_EnableGenerate();
                    break;
                case 'T':
                    WFD02410_EnableGenerate();
                    break;
                case 'D':
                    WFD02510_EnableGenerate();
                    break;
                case 'P':
                    WFD02310_EnableGenerate();
                    break;
                case 'S':
                    WFD02640_EnableGenerate();
                    break;
                default:
                    break;
            }
        }
    });
}

//==================== End Button ====================//

//===================== Upload Comment File =========================//
var WFD01270_SetButtonAndUploadFile = function () {
    var _indx = '0';
    if (SCREEN_PARAMS.INDX != '' && SCREEN_PARAMS.INDX != null) {
        _indx = SCREEN_PARAMS.INDX;
    }
    var model = new FormData();
    var fileInput = document.getElementById('WFD01270_UPLOAD_COMMENT_INFO_FILE');
    model.append('Index', _indx);
    model.append('CommentStatus', SCREEN_PARAMS.COMMENT_STATUS);
    if (fileInput.files.length > 0) {
        model.append('CommentFile', fileInput.files[0]);
        model.append('AttachFileName', fileInput.files[0].name);
        model.append('DOC_NO', WFD01270Controls.DOC_NO.val());
        var ModelValidateFile = {
            FunctionID: 'WFD01270',
            Folder: 'COMMENT',
            FileName: fileInput.files[0].name,
            FileSize: fileInput.files[0].size
        }
        ajax_method.Post(URL_WFD01270.CHECK_UPLOAD_FILE, ModelValidateFile, false, function (result) {
            if (result) {// true 
                ajax_method.PostFile(URL_WFD01270.UPLOAD_FILE_COMMENT, model, function (result) {
                    if (result.ObjectResult.FileName != null && result.ObjectResult.FileName != '') {
                        WFD01270FileUplaodControls.Button.hide();
                        WFD01270FileUplaodControls.DelFileBTN.show();
                        WFD01270FileUplaodControls.DownloadFileBTN.hide();
                        WFD01270FileUplaodControls.ViewFileBTN.show();
                        WFD01270FileUplaodControls.Textbox.val(result.ObjectResult.FileName);
                        document.getElementById('WFD01270_UPLOAD_COMMENT_INFO_VIEW').href = "/WFD01270/DownloadFileTemp?FILENAME=" + result.ObjectResult.FileName + "&FunctionID=WFD01270" + "&Type=COMMENT";

                    } else {
                        WFD01270FileUplaodControls.File.val('');
                    }

                }, null);
            }
        }, null);
    }
}

var WFD01270UploadCommentFileClick = function () {
    ClearMessageC();
    WFD01270FileUplaodControls.File.click();
}

var WFD01270CheckDownloadFile = function () {
    var _valueFileName = WFD01270FileUplaodControls.Textbox.val();
    if (_valueFileName != null && _valueFileName != '') {
        WFD01270FileUplaodControls.DownloadFileBTN.href = "/WFD01270/DownloadFileTemp?FILENAME=" + _valueFileName + "&FunctionID=WFD01270" + "&_type=COMMENT";
    }
}

var WFD01270DelUploadCommentFileClick = function () {
    var model = WFD01270LoadCommentInfoData();
    ajax_method.Post(URL_WFD01270.DELETE_FILE_COMMENT, model, false, function (result) {
        if (result.FileName == '' || result.FileName == null) {
            WFD01270FileUplaodControls.Button.show();
            WFD01270FileUplaodControls.DelFileBTN.hide();
            WFD01270FileUplaodControls.DownloadFileBTN.hide();
            WFD01270FileUplaodControls.ViewFileBTN.hide();
            WFD01270FileUplaodControls.Textbox.val(result.FileName);
            WFD01270FileUplaodControls.File.val(result.FileName);
        }
    });

}

var sec = SCREEN_PARAMS.COUNTDOWN;
function countdown() {
    sec = sec - 1;
    if (sec < 0) {
        WFD01270Controls.WFD01270_RedirectInformation.html("System is redirecting to main page" + "&nbsp");
        window.location = SCREEN_PARAMS.MAINPAGE + "?PREVPAGE=WFD0127";
    } else {
        WFD01270Controls.WFD01270_RedirectInformation.html("System will redirect to main page in " + sec + " seconds" + "&nbsp");
        window.setTimeout("countdown()", 1000);
    }
}

// Click function of WFD01270CloseRequesterClick for redirect to home screen with auto select tab by Pawares M. 20180712
function WFD01270CloseRequesterClick() {
    window.location = SCREEN_PARAMS.MAINPAGE + "?PREVPAGE=WFD0127";
}


