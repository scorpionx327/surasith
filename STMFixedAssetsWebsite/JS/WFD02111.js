﻿var WFD02111Controls = {
    WFD02111AddAssets: $('#WFD02111AddAssets'),
    WFD02111Clear: $('#WFD02111Clear'),
    WFD02111ExportToExcel: $('#WFD02111ExportToExcel'),
    WFD02111RemarkGenerate: $('#WFD02111RemarkGenerate'),
    COMSCCSearchCostCodeModal: $('#SearchCostCenter'),
    WFD02115SearchAssetsModal: $('#xxxxxxxxxxxxxxAAAAAAAA'),
    WFD02111DwUpFile: $('#WFD02111DwUpFile'),
    WFD02115SearchAssetsModal: $('#WFD02115_SearchAssets'),


    SearchResult: $('#WFD02111_SearchResult'),                  //Id Div Table
    SearchResultTable: $('#WFD02111_SearchResultTable'),        //Id Table    
    Pagin: $('#WFD02111_SearchResultTable').Pagin(URL_WFD02111.ITEM_PER_PAGE),            //Id Table      
    DataTable: null,
    SearchDataRow: $('#WFD02111_SearchDataRow'),                 //Id Block Div 

    LINE_NO: $('#LINE_NO'),
    STATUS: $('#STATUS'),
    STATUS_HINT: $('#STATUS_HINT'),
    PARENT_ASSET_NO: $('#PARENT_ASSET_NO'),
    ASSET_DESC: $('#ASSET_DESC'),
    ASSET_CLASS: $('#ASSET_CLASS'),
    ASSET_CLASS_HINT: $('#ASSET_CLASS_HINT'),
    SERIAL_NO: $('#SERIAL_NO'),
    COST_CODE: $('#COST_CODE'),
    COST_NAME: $('#COST_NAME'),
    RESP_COST_CODE: $('#RESP_COST_CODE'),
    COST_NAME_HINT: $('#COST_NAME_HINT'),
    WBS_BUDGET: $('#WBS_BUDGET'),
    WBS_NAME: $('#WBS_NAME'),
    LOCATION: $('#LOCATION'),
    LOCATION_HINT: $('#LOCATION_HINT'),
    INVEST_REASON: $('#INVEST_REASON'),
    INVEST_REASON_HINT: $('#INVEST_REASON_HINT'),
    EVA_GRP_1: $('#EVA_GRP_1'),
    EVA_GRP_1_HINT: $('#EVA_GRP_1_HINT'),
    EVA_GRP_4: $('#EVA_GRP_4'),
    EVA_GRP_4_HINT: $('#EVA_GRP_4_HINT'),
    ASSET_NO: $('#ASSET_NO'),
    ASSET_SUB: $('#ASSET_SUB'),
    EMP_CODE: $('#EMP_CODE'),
    EMP_NAME: $('#EMP_NAME'),
    POSITION: $('#POSITION'),
    ROLE: $('#ROLE'),
    DEPT_NAME: $('#DEPT_NAME'),
    TEL_NO: $('#TEL_NO'),
    SECTION_NAME: $('#SECTION_NAME'),

};

$(function () {
    if (SCREEN_PARAMS.REQUEST_TYPE == 'A') {
        WFD02111Controls.WFD02111AddAssets.click(WFD02111AddAssetsData);
        WFD02111Controls.WFD02111DwUpFile.click(WFD02111DownloadAndUploadFile);
        WFD02111Controls.WFD02111Clear.click(WFD02111Clear);
        //WFD02111Controls.WFD02111ExportToExcel.click(WFD02111ExportToExcel);

        //WFD02111Controls.SearchResult.hide(); Test in Prod is not hide.

        $('#WFD02115_SearchAssets').on('hidden.bs.modal', function () {
            WFD02111_UpdateAssetData(WFD02111_GetDataAssets);
        });
        WFD02111SetDisplayScreen();
    }

    WFD02111_GetDataAssets('0');

});


//==================== Start Set Display Screen ====================//
var WFD02111SetDisplayScreen = function () {
    if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
        WFD02111Controls.WFD02111AddAssets.show();
        WFD02111Controls.WFD02111Clear.show();
        WFD02111Controls.WFD02111ExportToExcel.show();
        WFD02111setDisableButton('#WFD02111Clear');
        WFD02111setDisableButton('#WFD02111ExportToExcel');
    } else {
        WFD02111_GetDataAssets('0');
        WFD02111Controls.WFD02111AddAssets.hide();
        WFD02111Controls.WFD02111Clear.hide();
        WFD02111Controls.WFD02111ExportToExcel.show();
    }
    if (SCREEN_PARAMS.MODE == 'V') {
        WFD02111setDisableButton('#WFD02111Clear');
        //WFD02111setDisableButton('#WFD02111ExportToExcel');
        WFD02111_SetDisable();
    }
    WFD02111DetailItemCheck();
}

var WFD02111setDisableButton = function (id) {
    $(id).addClass('disabled');
    $(id).removeClass('active');
    $(id).prop('disabled', true);
}

var WFD02111setEnableButton = function (id) {
    $(id).removeClass('disabled');
    $(id).prop('disabled', false);
}

var WFD02111_EnableGenerate = function () {
    WFD02111setEnableButton('#WFD02111Clear');
    WFD02111setEnableButton('#WFD02111AddAssets');
    WFD02111SetEnableBtnDelInTable();
    WFD02111Controls.WFD02111RemarkGenerate.hide();
}

var WFD02111SetEnableBtnDelInTable = function () {
    WFD02111Controls.SearchResultTable.find('[id^="WFD02111Del_"]').each(function (e) {
        $(this).removeClass('disabled');
        $(this).prop('disabled', false);
    });
    WFD02111Controls.SearchResultTable.find('input[id^="DAMAGE"]').each(function (e) {
        $(this).removeClass('disabled');
        $(this).prop('disabled', false);
    });
    WFD02111Controls.SearchResultTable.find('input[id^="LOSS"]').each(function (e) {
        $(this).removeClass('disabled');
        $(this).prop('disabled', false);
    });
    WFD02111setEnableButton('#WFD02111_DamageSelectAll');
    WFD02111setEnableButton('#WFD02111_LossSelectAll');
}

var WFD02111_DisableGenerate = function () {
    WFD02111setDisableButton('#WFD02111Clear');
    WFD02111setDisableButton('#WFD02111AddAssets');
    WFD02111SetDisableBtnDelInTable();
    if (SCREEN_PARAMS.MODE == 'V') {
        WFD02111Controls.WFD02111RemarkGenerate.hide();
    } else {
        WFD02111Controls.WFD02111RemarkGenerate.show();
    }

}

var WFD02111SetDisableBtnDelInTable = function () {
    WFD02111Controls.SearchResultTable.find('[id^="WFD02111Del_"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).removeClass('active');
        $(this).prop('disabled', true);
    });
    WFD02111Controls.SearchResultTable.find('input[id^="DAMAGE"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).removeClass('active');
        $(this).prop('disabled', true);
    });
    WFD02111Controls.SearchResultTable.find('input[id^="LOSS"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).removeClass('active');
        $(this).prop('disabled', true);
    });
    WFD02111setDisableButton('#WFD02111_DamageSelectAll');
    WFD02111setDisableButton('#WFD02111_LossSelectAll');
}

var WFD02111_SetClassTrRed = function () {
    $("#WFD02111_SearchResultTable tr").each(function () {
        var $this = $(this);
        var row = $this.closest("tr");
        if (row.find('td:eq(1)').text() != "" && row.find('td:eq(1)').text() != null) {
            var _rowData = [];
            if (WFD02111Controls.DataTable != null) {
                _rowData = WFD02111Controls.DataTable.row(this).data();
                if (_rowData["RETIRED_FLAG"] != null && _rowData["RETIRED_FLAG"] == 'Y') {
                    WFD02111Controls.SearchResultTable.removeClass('table-striped');
                    WFD02111Controls.SearchResultTable.removeClass('table-hover');
                    row.removeClass('odd');
                    row.removeClass('even');
                    row.addClass('RetiredRead');
                }
            }
        }
    });
}
//==================== End Set Display Screen ====================//


//==================== Start Get Data For Operation ====================//
var _Loss = function (_Loss, _Damage) {
    _type = 'N';
    if ($('input[id="' + _Loss + '"]').is(':checked')) {
        _type = 'Y';
    }
    return _type;
}

var _Damage = function (_Loss, _Damage) {
    _type = 'N';
    if ($('input[id="' + _Damage + '"]').is(':checked')) {
        _type = 'Y';
    }
    return _type;
}

var WFD02111DetailItemCheck = function () {
    var numberOfChecked = $('#WFD02111_SearchResultTable [id^="DAMAGE"]:checked').length;
    var totalCheckboxes = $('#WFD02111_SearchResultTable [id^="DAMAGE"]').length;
    $('#WFD02111_SearchResultTable #WFD02111_DamageSelectAll').prop('checked', totalCheckboxes > 0 && totalCheckboxes == numberOfChecked);

    var numberOfCheckedLoss = $('#WFD02111_SearchResultTable [id^="LOSS"]:checked').length;
    var totalCheckboxesLoss = $('#WFD02111_SearchResultTable [id^="LOSS"]').length;
    $('#WFD02111_SearchResultTable #WFD02111_LossSelectAll').prop('checked', totalCheckboxesLoss > 0 && totalCheckboxesLoss == numberOfCheckedLoss);

}

var WFD02111_SetLoss = function (_ChkLoss, _ChkDamage) {
    //alert($('input[id="' + _ChkDamage + '"]').is(':checked'));
    if ($('input[id="' + _ChkLoss + '"]').is(':checked')) {
        $('#' + _ChkDamage).attr('checked', false);
        $('input[id=WFD02111_DamageSelectAll]').prop('checked', false);
    } else {
        $('input[id=WFD02111_LossSelectAll]').prop('checked', false);
    }
    WFD02111DetailItemCheck();
}

var WFD02111_SetDamage = function (_ChkDamage, _ChkLoss) {
    //alert($('input[id="' + _ChkLoss + '"]').is(':checked'));
    if ($('input[id="' + _ChkDamage + '"]').is(':checked')) {
        $('#' + _ChkLoss).attr('checked', false);
        $('input[id=WFD02111_LossSelectAll]').prop('checked', false);
    } else {
        $('input[id=WFD02111_DamageSelectAll]').prop('checked', false);
    }
    WFD02111DetailItemCheck();
}

var WFD02111_SetAllItemDamage = function () {
    var chk = $('input[id=WFD02111_DamageSelectAll]').is(':checked');
    //alert(chk);
    WFD02111Controls.SearchResultTable.find('input[id^="DAMAGE"]').each(function (e) {
        $(this).prop('checked', chk);
    });
    if (chk) {
        $('input[id=WFD02111_LossSelectAll]').prop('checked', false);
        WFD02111Controls.SearchResultTable.find('input[id^="LOSS"]').each(function (e) {
            $(this).prop('checked', false);
        });
    }
}

var WFD02111_SetAllItemLoss = function () {
    var chk = $('input[id=WFD02111_LossSelectAll]').is(':checked');
    WFD02111Controls.SearchResultTable.find('[id^="LOSS"]').each(function (e) {
        $(this).prop('checked', chk);
    });
    if (chk) {
        $('input[id=WFD02111_DamageSelectAll]').prop('checked', false);
        WFD02111Controls.SearchResultTable.find('[id^="DAMAGE"]').each(function (e) {
            $(this).prop('checked', false);
        });
    }
}

var WFD02111_SetDisable = function () {
    WFD02111Controls.SearchResultTable.find('[id^="WFD02111Del_"]').each(function (e) {
        $(this).addClass('disabled');
        $(this).removeClass('active');
        $(this).prop('disabled', true);
    });
}

var WFD02111_GetAssetDataModel = function () {
    var datas = [];
    $("#WFD02111_SearchResultTable tr").each(function () {
        var $this = $(this);
        var row = $this.closest("tr");
        if (row.find('td:eq(0)').text() != "" && row.find('td:eq(0)').text() != null) {
            var _rowData = [];
            if (WFD02111Controls.DataTable != null) {
                _rowData = WFD02111Controls.DataTable.row(this).data();
                datas.push({
                    "DOC_NO": SCREEN_PARAMS.DOC_NO,
                    "ASSET_NO": _rowData["ASSET_NO"],
                    "COST_CODE": _rowData["COST_CODE"],
                    "DAMAGE_FLAG": _Loss('DAMAGE' + _rowData["ASSET_NO"] + _rowData["COST_CODE"]),
                    "LOSS_FLAG": _Loss('LOSS' + _rowData["ASSET_NO"] + _rowData["COST_CODE"]),
                    "ALLOW_DELETE": _rowData["ALLOW_DELETE"],
                    "REQUIRE_DAMAGE_FLAG": _rowData["REQUIRE_DAMAGE_FLAG"],
                    "REQUIRE_LOSS_FLAG": _rowData["REQUIRE_LOSS_FLAG"],
                    "PRINT_COUNT": _rowData["PRINT_COUNT"]
                });
            }
        }
    });

    var WFD02111Save = {
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        USER_BY: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID,
        UPDATE_DATE: SCREEN_PARAMS.UPDATE_DATE,
        ASSET_LIST: datas
    }
    return WFD02111Save;
}

var WFD02111_GetParaModel = function () {
    var WFD02111_Para = {
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        USER_BY: SCREEN_PARAMS.EMP_CODE,
        GUID: SCREEN_PARAMS.GUID,
        UPDATE_DATE: SCREEN_PARAMS.UPDATE_DATE
    }
    return WFD02111_Para;
}


//==================== End Get Data For Operation ====================//

//==================== Start Data In Table ====================//
var WFD02111_SetButtonDelInTable = function (data) {
    var _strHtml_Del = '';
    var _strHtml_Edit = '';

    _strHtml_Edit = '<t title="" data-original-title="Edit" data-toggle="tooltip" data-placement="top"> <button id="WFD02310Del_"' + data.ASSET_NO + ' class="btn btn-info btn-xs" data-title="Edit" type="button" onclick=WFD02310_DELETE(\'' + data.ASSET_NO + '\',\'' + data.COST_CODE + '\')><span class="glyphicon glyphicon-edit"></span></button></t>';
    if (data.ALLOW_DELETE == 'Y') {
        _strHtml_Del = '<t title="" data-original-title="Delete" data-toggle="tooltip" data-placement="top"> <button id="WFD02310Del_"' + data.ASSET_NO + ' class="btn btn-danger btn-xs" data-title="Delete" type="button" onclick=WFD02310_DELETE(\'' + data.ASSET_NO + '\',\'' + data.COST_CODE + '\')><span class="glyphicon glyphicon-trash"></span></button></t>';
    } else {
        _strHtml_Del = ' ';
    }
    return _strHtml_Edit + _strHtml_Del;
}

var WFD02111_SetCheckBoxDamageInTable = function (data) {
    var _strHtml = '';
    var _strDisable = '';
    var _strChecked = '';
    if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
        _strDisable = '';
    } else {
        _strDisable = 'disabled="disabled"';
    }
    if (data.DAMAGE_FLAG == 'Y') {
        _strChecked = ' checked="checked"';
    } else {
        _strChecked = '';
    }
    _strHtml = '<input style="width:20px; height:28px;"  type="checkbox" onclick="WFD02111_SetDamage(\'DAMAGE' + data.ASSET_NO + data.COST_CODE + '\',\'LOSS' + data.ASSET_NO + data.COST_CODE + '\')" id="DAMAGE' + data.ASSET_NO + data.COST_CODE + '" ' + _strDisable + ' ' + _strChecked + ' >';
    return _strHtml;
}

var WFD02111_SetCheckBoxLossInTable = function (data) {
    var _strHtml = '';
    var _strDisable = '';
    var _strChecked = '';
    if (SCREEN_PARAMS.DOC_NO == '' || SCREEN_PARAMS.DOC_NO == null) {
        _strDisable = '';
    } else {
        _strDisable = 'disabled="disabled"';
    }
    if (data.LOSS_FLAG == 'Y') {
        _strChecked = ' checked="checked"';
    } else {
        _strChecked = '';
    }
    _strHtml = '<input style="width:20px; height:28px;"  type="checkbox" onclick="WFD02111_SetLoss(\'LOSS' + data.ASSET_NO + data.COST_CODE + '\',\'DAMAGE' + data.ASSET_NO + data.COST_CODE + '\')" id="LOSS' + data.ASSET_NO + data.COST_CODE + '" ' + _strDisable + ' ' + _strChecked + ' >';
    return _strHtml;
}

var WFD02111_SetButtonQuotation = function (data) {
    var _strQuotation = '';
    _strQuotation = '<t title="" data-original-title="Quotation" data-toggle="tooltip" data-placement="top"> <button class="btn btn-info btn-xs" data-title="Edit" type="button" onclick=WFD02111DownloadAndUploadFile(\'LOSS' + data.ASSET_NO + data.COST_CODE + '\',\'DAMAGE' + data.ASSET_NO + data.COST_CODE + '\')><span class="glyphicon glyphicon-search"></span></button></t>';
    return _strQuotation;
}

var WFD02111_GetDataAssets = function (rest) {
    if (rest != '-99') {
        WFD02111Data = WFD02111_GetAssetDataModel();
        WFD01270DataModel = WFD01270RequestHeaderData();
        var pagin = WFD02111Controls.Pagin.GetPaginData();
        WFD02111Controls.SearchResult.show();

        ajax_method.SearchPost(URL_WFD02111.GET_ASSETS_REPRINT_DATA, WFD02111Data, pagin, false, function (datas, pagin) {
            if (datas != null && datas.length > 0) {
                SCREEN_PARAMS.WFD02115_COST_CODE = datas[0].COST_CODE;
                WFD02111setEnableButton('#WFD02111Clear');
                if (pagin.OrderColIndex == null) pagin.OrderColIndex = 1;
                if (!pagin.OrderType) pagin.OrderType = 'asc';

                if ($.fn.DataTable.isDataTable(WFD02111Controls.SearchResultTable)) {
                    var table = WFD02111Controls.SearchResultTable.DataTable();
                    table.destroy();
                }
                WFD02111Controls.DataTable = WFD02111Controls.SearchResultTable.DataTable({
                    data: datas,
                    "columns": [
                        { "data": "LINE_NO", 'orderable': false },                       // 0
                        { "data": "PARENT_ASSET_NO", 'orderable': false },                 // 1    
                        { "data": "ASSET_DESC", 'orderable': false },            // 2
                        { "data": null, 'orderable': false },               // 3
                        { "data": null, 'orderable': false },     // 4
                        { "data": null, 'orderable': false },       // 5
                        { "data": null, 'orderable': false },              // 6                    
                        { "data": null, 'orderable': false },                              // 7
                        { "data": null, 'orderable': false },                              // 8
                        { "data": null, 'orderable': false },                              // 9
                        { "data": null, 'orderable': false },                              // 10
                        { "data": "ASSET_NO", 'orderable': false },                              // 11
                        { "data": "ASSET_SUB", 'orderable': false },                           // 12
                        { "data": null, 'orderable': false },                           // 13
                        { "data": null, 'orderable': false },                           // 14
                        { "data": null, 'orderable': false }                           // 15
                    ],
                    'columnDefs': [
                    {
                        //'targets': [ 1, 2, 14],
                        //className: 'text-center'
                    },
                     {
                         'targets': [0],
                         className: 'text-right'
                     },
                    {
                        'targets': 3,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.ASSET_CLASS_HINT + '">' + data.ASSET_CLASS + '</span>';
                        }
                    },
                    {
                        'targets': 4,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.EVA_GRP_1_HINT + '">' + data.EVA_GRP_1 + '</span>';
                        }
                    },
                    {
                        'targets': 5,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME + '">' + data.COST_CODE + '</span>';
                        }
                    },
                    {
                        'targets': 6,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.COST_NAME + '">' + data.RESP_COST_CODE + '</span>';
                        }
                    },
                    {
                        'targets': 7,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.WBS_NAME + '">' + data.WBS_BUDGET + '</span>';
                        }
                    },
                    {
                        'targets': 8,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.LOCATION_HINT + '">' + data.LOCATION + '</span>';
                        }
                    },
                    {
                        'targets': 9,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.INVEST_REASON_HINT + '">' + data.INVEST_REASON + '</span>';
                        }
                    },
                    {
                        'targets': 10,
                        'orderable': true,
                        'className': 'text-left',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.EVA_GRP_4_HINT + '">' + data.EVA_GRP_4 + '</span>';
                        }
                    },
                    {
                        'targets': 13,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return WFD02111_SetButtonQuotation(data);
                        }
                    },
                    {
                        'targets': 14,
                        'orderable': true,
                        'className': 'text-center',
                        "render": function (data, type, full, meta) {
                            return '<span data-toggle="tooltip" title="' + data.STATUS_HINT + '">' + data.STATUS + '</span>';
                        }
                    },
                    {
                        'targets': 15,
                        'searchable': false,
                        'orderable': false,
                        'className': 'text-center',
                        'render': function (data, type, full, meta) {
                            return WFD02111_SetButtonDelInTable(data);
                        }
                    }

                    ],
                    'order': [],
                    searching: false,
                    paging: false,
                    retrieve: true,
                    "bInfo": false,
                    "autoWidth": false,
                    fixedHeader: true
                });

                //var page = $('#WFD02111_SearchResultTable').Pagin();
                //page.Init(pagin, WFD02111sorting, WFD02111changePage, 'itemPerPage');

                //WFD02111Controls.Pagin.Init(pagin, WFD02111sorting, WFD02111changePage, 'itemPerPage');
                //$('select').select2();

                WFD02111Controls.SearchDataRow.show();
                WFD02111_SetClassTrRed();
            } else {
                var table = WFD02111Controls.SearchResultTable.DataTable();
                table.destroy();
                WFD02111Controls.DataTable = null;
                SCREEN_PARAMS.WFD02115_COST_CODE = '';
                WFD02111Controls.SearchResult.hide();
                WFD02111Controls.Pagin.Clear();
                WFD02111setDisableButton('#WFD02111Clear');
                WFD02111setDisableButton('#WFD02111ExportToExcel');

            }

            UpdateSelectAll();
            CheckPageLoaded = 'Y';
        }, null);

    }

}

//==================== End Data In Table ====================//

//==================== Start Even Button ====================//
var WFD02111AddAssetsData = function () {
    WFD02115LoadDefault(SCREEN_PARAMS.WFD02115_COST_CODE);
    WFD02111Controls.WFD02115SearchAssetsModal.appendTo("body").modal('show');
}


var WFD02111DownloadAndUploadFile = function (data) {
    WFD02115LoadDefault(SCREEN_PARAMS.WFD02115_COST_CODE);
    WFD02111Controls.WFD02115SearchAssetsModal.appendTo("body").modal('show');

}

var WFD02111Clear = function () {
    loadConfirmAlert(WFD01270_Message.ConfirmClear, function (result) {
        if (result) {
            WFD02111Data = WFD02111_GetParaModel();
            ajax_method.Post(URL_WFD02111.CLEAR_ASSETS_REPRINT_DATA, WFD02111Data, false, function (result) {
                if (result != '-99') {
                    WFD02111_GetDataAssets('0');

                    //Set checkbox clear
                    UpdateSelectAll();


                    $.notify({
                        icon: 'glyphicon glyphicon-ok',
                        message: "Clear process is completed."
                    }, {
                        type: 'success',
                        delay: 500,
                    });
                }
            }, null);
        }
    });
}

var WFD02111_UpdateAssetData = function (callback) {
    var ArrData = WFD02111_GetAssetDataModel();
    console.log("ASSET COUNT");
    console.log(ArrData.ASSET_LIST.length);
    if (ArrData.ASSET_LIST != null && ArrData.ASSET_LIST.length > 0) {
        ajax_method.Post(URL_WFD02111.UPDATE_ASSETS_REPRINT_DATA, ArrData, false, function (result) {
            if (typeof callback == 'function') {
                callback(result);
            }
        }, null);
    } else {
        WFD02111_GetDataAssets('0');
    }
}

var WFD02111_DELETE = function (_AssetNo, _CostCose) {
    var _StatusDelete = '';
    var WFD02111DetailAsset = [
        {
            ASSET_NO: _AssetNo,
            COST_CODE: _CostCose
        }
    ];
    var WFD02111Model = {
        GUID: SCREEN_PARAMS.GUID,
        DOC_NO: SCREEN_PARAMS.DOC_NO,
        USER_BY: SCREEN_PARAMS.USER_BY,
        ASSET_LIST: WFD02111DetailAsset
    }
    var _strMessageDel = WFD01270_Message.ConfirmDelete;
    _strMessageDel = _strMessageDel.replace("{1}", _AssetNo);
    loadConfirmAlert(_strMessageDel, function (result) {
        if (result) {
            var ArrData = WFD02111_GetAssetDataModel();
            if (ArrData.ASSET_LIST != null && ArrData.ASSET_LIST.length > 0) {
                ajax_method.Post(URL_WFD02111.UPDATE_ASSETS_REPRINT_DATA, ArrData, false, function (result) {
                    if (result != '-99') {
                        ajax_method.Post(URL_WFD02111.DELETE_ASSET_NO, WFD02111Model, false, function (result) {
                            if (result != "-99") {
                                _StatusDelete = result;
                                $.notify({
                                    icon: 'glyphicon glyphicon-ok',
                                    message: "Deletion process is completed."
                                }, {
                                    type: 'success',
                                    delay: 500,
                                });
                                WFD02111_GetDataAssets('0');
                                return;
                            }
                        }, null);
                    }
                }, null);
            }
        }
    });
}

$('#WFD02111ExportToExcel').click(function (e) {
    loadConfirmAlert(WFD01270_Message.ConfirmExportToExcel, function (result) {
        if (result) {
            var batch = BatchProcess({
                BatchId: "LFD02330",
                Description: "Export Reprint Request",
                UserId: SCREEN_PARAMS.EMP_CODE
            });
            batch.Addparam(1, 'DOC_NO', SCREEN_PARAMS.DOC_NO);
            batch.StartBatch();
        }
    });
});
//==================== End Button ====================//

function UpdateSelectAll() {
    var _total = 0;
    var _cnt = 0;
    $("#WFD02111_DamageSelectAll").prop("checked", false);
    $("#WFD02111_LossSelectAll").prop("checked", false);

    WFD02111Controls.SearchResultTable.find('input[id^="DAMAGE"]').each(function (e) {
        _total++;
        if ($(this).is(':checked')) {
            _cnt++;
        }

        if (_total > 0 && _total <= _cnt) {
            $("#WFD02111_DamageSelectAll").prop("checked", true);
        }


    });
    _total = 0;
    _cnt = 0;
    WFD02111Controls.SearchResultTable.find('input[id^="LOSS"]').each(function (e) {
        _total++;
        if ($(this).is(':checked')) {
            _cnt++;
        }

        if (_total > 0 && _total <= _cnt) {
            $("#WFD02111_LossSelectAll").prop("checked", true);
        }


    });


}