﻿var CostControls = {
    FIND: $('#FIND'),
    CostSearchResult: $('#CostSearchResult'),
    CostSearchResultTable: $('#CostSearchResultTable'),
    CostSearchButton: $("#COMSCCSearch"),
    CostCloseButton: $('#COMSCCClose'),
    CostPagin: $('#CostSearchResultTable').Pagin(URL_COMSCC.ITEM_PER_PAGE),
    CostDataTable: null,
    CostSearchDataRow: $('#CostSearchDataRow'),
    COMSCCCloseModal: $('#COMSCCCloseModal'),
    COMPANY_: $('#COMPANY_'),
    COMSCC_EMP_CODE: $('#COMSCC_EMP_CODE'),
    COMSCC_COST_CODE: $('#COMSCC_COST_CODE'),
    COMSCC_ALL_COST_CENTER: $('#COMSCC_ALL_COST_CENTER'),
    AssetOwnerFlag: $('#AssetOwnerFlag'),
    PageRequest: $('#COMSCCPageRequest'), // In case User BOI
    IsSearchRespCostCode: false,
    callback : null
};

$(function () {
    CostControls.CostSearchButton.click(CostOnSearchBtnClick);
    CostControls.CostCloseButton.click(CostClearScreen);
    CostControls.CostSearchDataRow.hide();
    CostControls.COMSCCCloseModal.click(CostClearScreen);
    if(URL_COMSCC.DefaultCostCode != null && URL_COMSCC.DefaultCostCode != '')
    {
        CostControls.FIND.val(URL_COMSCC.DefaultCostCode);
    }

    $('#FIND').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            CostControls.CostSearchButton.click();
            return false;
        }
    });
});

var COMSCCgetDefaultSceenValue = function () {
    return {
        FIND: CostControls.FIND.val(),
        EMP_CODE: CostControls.COMSCC_EMP_CODE.val(),
        COST_CODE: CostControls.COMSCC_COST_CODE.val(),
        COMPANY: CostControls.COMPANY_.val(),
        IsSearchRespCostCode : CostControls.IsSearchRespCostCode,
        ALL_COST_CENTER: CostControls.COMSCC_ALL_COST_CENTER.val(),
        PAGE_REQUEST: CostControls.PageRequest.val() // In case User BOI
    }
}

var COMSCC_InitialParameter = function () {
    CostControls.callback = null;
    //
    if (CostControls.IsSearchRespCostCode) {
        $('#lblCostCenterTitle').text("Search Responsible Cost Center Dialog");
    }
    else {
        $('#lblCostCenterTitle').text("Search Cost Center Dialog");
    }

    ajax_method.Post(URL_COMSCC.SET_INITIAL_PARAMETER, null, true
            , function (result) {
                if (result != null) {
                    CostControls.COMSCC_EMP_CODE.val(result.EMP_CODE);
                    CostControls.COMSCC_COST_CODE.val(result.COST_CODE);
                    CostControls.COMSCC_ALL_COST_CENTER.val(result.ALL_COST_CENTER);
                    CostControls.AssetOwnerFlag.val(result.AssetOwnerFlag);
                    CostControls.PageRequest.val(result.PAGE_REQUEST); // In case User BOI
                    if (result.AssetOwnerFlag != null && result.AssetOwnerFlag == 'Y') {
                        CostControls.FIND.val('');
                    }
                }
            }, null);
}


var cntClick = 0;
var CostOnSearchBtnClick = function () {
    
    //Search Criteria
    //$('#FIND').val(GetSearchCriteria($('#FIND')));
    CostControls.CostPagin.ClearPaginData();
    DefaultScreenData = COMSCCgetDefaultSceenValue();
    CostSearch();
    $(document).off("dblclick", "#CostSearchResultTable tr");
    $(document).on("dblclick", "#CostSearchResultTable tr", function () {
        var $this = $(this);
        var row = $this.closest("tr");
        row.find('td:eq(0)');
        row.find('td:first').text();

       
        // For 2115
        if (CostControls.callback != null) {
            _row = CostControls.CostDataTable.row(this).data(); // Get source data
            
            CostControls.callback(_row);
            $('#SearchCostCenter').modal('hide');
            CostClearScreen();

            return;
        }

        var buttonPressed = $('.COMSCCCostCodeModal');
        buttonPressed.val(row.find('td:first').text());
        buttonPressed.removeClass('COMSCCCostCodeModal');

        var CostName = $('.COMSCCCostNameModal');
        CostName.val(row.find('td:eq(1)').text());
        CostName.removeClass('COMSCCCostNameModal');

        $('#SearchCostCenter').modal('hide');
        CostClearScreen();
    });
}

var CostSearch = function () {
    var pagin = CostControls.CostPagin.GetPaginData();

    DefaultScreenData = COMSCCgetDefaultSceenValue();

    ajax_method.SearchPost(URL_COMSCC.SEARCH_COST_CODE, DefaultScreenData, pagin, true, function (datas, pagin) {

        if (datas != null && datas.length > 0) {
            if (pagin.OrderColIndex == null) pagin.OrderColIndex = 0;
            if (!pagin.OrderType) pagin.OrderType = 'asc';

            if ($.fn.DataTable.isDataTable(CostControls.CostSearchResultTable)) {
                var table = CostControls.CostSearchResultTable.DataTable();
                table.destroy();
            }

            CostControls.CostDataTable = CostControls.CostSearchResultTable.DataTable({
                data: datas,
                "columns": [
                    { "data": "COST_CODE" },
                    { "data": "COST_NAME" , className: 'cursorPointer'},
                    { "data": "EMP_NAME", className: 'cursorPointer' },
                    { "data": "STATUS", className: 'cursorPointer' }
                ],
                'columnDefs': [
                { 'targets': [0], className: 'text-center cursorPointer' }],
                //
                'order': [],
                "paging": false,
                searching: false,
                paging: false,
                retrieve: true,
                "bInfo": false,
                "autoWidth": false
            });
            //var page = $('#CostSearchResultTable').Pagin();
            //page.Init(pagin, Costsorting, CostchangePage, 'itemPerPage');

            CostControls.CostPagin.Init(pagin, Costsorting, CostchangePage, 'itemPerPage');


            CostControls.CostSearchDataRow.show();
        } else {
            CostControls.CostSearchDataRow.hide();
            CostControls.CostPagin.Clear();
        }

    }, null);
    
}
var Costsorting = function (PaginData) {
    
    CostSearch();
}   

// Callback function after change pagination on table
var CostchangePage = function (PaginData) {
    CostSearch();
}


// Callback function after click clear button
var CostClearScreen = function () {
    CostControls.CostSearchDataRow.hide();
    CostControls.CostPagin.Clear();
    CostControls.FIND.val('');
    CostControls.COMSCC_EMP_CODE.val('');
    CostControls.COMSCC_COST_CODE.val('');
    CostControls.COMSCC_ALL_COST_CENTER.val('');
    $('#AlertMessageCOMSCC').html('');
    $('#SearchCostCenter').modal('hide');
}
