﻿
$(function () {
    setTimeout(function () { $('#WFD02115Loading').hide(); }, 10000);
    window.scrollTo(0, 0);
    fScreenMode._ClearMode();
    var mode = '';

    //Add header lable info.
    var title = 'Fixed Asset Request Detail : ';
    if (Params.FLOW_TYPE == "RM") {
        $('#WFD02115FlowTypeLable').text(title + 'New Asset (RMA)');
    } else if (Params.FLOW_TYPE == "AU") {
        $('#WFD02115FlowTypeLable').text(title + 'New Asset (AUC)');
    } else if (Params.FLOW_TYPE == "ST") {
        $('#WFD02115FlowTypeLable').text(title + 'New Asset (Settlement)');
    } else if (Params.FLOW_TYPE == "GN" || Params.FLOW_TYPE == "GA") {
        $('#WFD02115FlowTypeLable').text(title + 'Asset Info Change');
    } else if (Params.FLOW_TYPE == "CN") {
        $('#WFD02115FlowTypeLable').text(title + 'Reclassification');
    } else if (Params.FLOW_TYPE == "KN") {
        $('#WFD02115FlowTypeLable').text(title + 'Settlement');
    } else {
        $('#WFD02115FlowTypeLable').text('Fixed Asset Request Detail');
    }

    if (Params.MODE == "N" && Params.LINE_NO == 0) { // New Mode
        if (Params.FLOW_TYPE == "RM") {
            fScreenMode.SetNewMode_NewAssetRequest();
            mode = 'NEW_RMA';
        } else if (Params.FLOW_TYPE == "AU") {
            fScreenMode.SetNewMode_NewAssetRequest();
            mode = 'NEW_AUC';
        } else if (Params.FLOW_TYPE == "ST") {
            fScreenMode.SetNewMode_NewAssetRequest();
            mode = 'NEW_SETTLE';
        } else if (Params.FLOW_TYPE == "GN") {
            fScreenMode.SetNewMode_AssetInfoChange();
            mode = 'NEW_INFO_CHG';

        } else if (Params.FLOW_TYPE == "CN") {
            fScreenMode.SetNewMode_NewAssetReclassification();
            mode = 'NEW_RECLASS';
        } else if (Params.FLOW_TYPE == "KN") {
            fScreenMode.SetNewMode_NewAssetSettlement();
            mode = 'NEW_SETTLE_KN';
        }

        fAction.Initialize();

        //Enable/Disable object
        fAction.enableObject(mode);

        $('#WFD02115Loading').hide();
        return;
    }

    if (Params.MODE == "N" && Params.LINE_NO > 0) { // New Mode
        doUpdateDepreciation = false;
        fAction.LoadData(function (dataLoaded) {
            if (Params.FLOW_TYPE == "RM") {
                fScreenMode.SetEditMode_NewAssetRequest();
                if (GLOBAL.ALLOW_EDIT == "N") {
                    fAction.enableObject('VIEW');
                } else {
                    fAction.enableObject('EDIT_RMA');
                }
            } else if (Params.FLOW_TYPE == "AU") {
                fScreenMode.SetEditMode_NewAssetRequest();
                if (GLOBAL.ALLOW_EDIT == "N") {
                    fAction.enableObject('VIEW');
                } else {
                    fAction.enableObject('EDIT_AUC');
                }
            } else if (Params.FLOW_TYPE == "ST") {
                fScreenMode.SetEditMode_NewAssetForSettlementRequest();
                if (GLOBAL.ALLOW_EDIT == "N") {
                    fAction.enableObject('VIEW');
                } else {
                    fAction.enableObject('EDIT_SETTLE'); //open from main screen (edit mode)
                }
            } else if (Params.FLOW_TYPE == "GN" || GLOBAL.REQUEST_TYPE == 'G') {
                fScreenMode.SetEditMode_AssetInfoChange(dataLoaded);
                if (GLOBAL.ALLOW_EDIT == "N") {
                    fAction.enableObject('VIEW');
                } else {
                    fAction.enableObject('EDIT_INFO_CHG');
                }

            } else if (Params.FLOW_TYPE == "CN") {
                $('#WFD02115Loading').hide();
                return;
            } else {
                alert("Not Implement LoadData for edit mode for this request");
            }
        });
        if (Params.FLOW_TYPE == "CN") {
            if (Params.OPTION == "E") {
                fScreenMode.SetEditMode_NewAssetReclassification();
                fAction.LoadData(function () {
                    fAction.enableObject('NEW_RECLASS');
                });

            } else {
                fScreenMode.SetNewMode_NewAssetReclassification();
                fAction.enableObject('NEW_RECLASS');
            }
        }

        doUpdateDepreciation = true;
        $('#WFD02115Loading').hide();
        return;
    }
    //Edit Mode
    if ((Params.DOC_NO != null && Params.DOC_NO != '') && (Params.LINE_NO != null && Params.LINE_NO != '')) {
        doUpdateDepreciation = false;
        fAction.LoadData(function () {

            //Load Original Data & set tool tip
            if (GLOBAL.REQUEST_TYPE == "G") {
                //fAction.LoadOriginal();

                if (GLOBAL.ALLOW_EDIT == "Y" || Params.MODE == "E") {
                    mode = 'EDIT_INFO_CHG_AFTER_SUBMIT';
                } else {
                    mode = 'VIEW';
                }
            } else if (Params.MODE == "V") {
                mode = 'VIEW';
            } else if (Params.MODE != "E") {
                $('#WFD02115Loading').hide();
                return;
            } else if (Params.FLOW_TYPE == "RM") {
                fScreenMode.SetEditMode_NewAssetRequest();
                mode = 'EDIT_RMA';
            } else if (Params.FLOW_TYPE == "AU") {
                fScreenMode.SetEditMode_NewAssetRequest();
                mode = 'EDIT_AUC';
            } else if (Params.FLOW_TYPE == "ST") {
                fScreenMode.SetEditMode_NewAssetForSettlementRequest();
                mode = 'EDIT_SETTLE';
            } else if (Params.FLOW_TYPE == "KN") {

                if (GLOBAL.ALLOW_EDIT == "Y") {
                    fScreenMode.SetEditMode_NewAssetSettlement();
                    //SetNewMode_NewAssetSettlement
                    mode = 'EDIT_SETTLE_KN';
                } else {
                    mode = 'VIEW';
                }
            }
        });

        if (Params.FLOW_TYPE == "CN") {
            if (Params.OPTION == "E") {
                fScreenMode.SetEditMode_NewAssetReclassification();
                fAction.LoadData(function () {
                    if (GLOBAL.ALLOW_EDIT === 'Y') {
                        mode = 'NEW_RECLASS';
                    } else {
                        mode = 'VIEW';
                    }
                });

            } else {
                fScreenMode.SetNewMode_NewAssetReclassification();
                mode = 'NEW_RECLASS';
            }
        }

        fAction.enableObject(mode);

        if (GLOBAL.REQUEST_TYPE == "G") {
            fAction.LoadOriginal();
        }

        doUpdateDepreciation = true;
    }
    $('#WFD02115Loading').hide();
});

var doUpdateDepreciation = true;
var doAddAssetProcess = false;
// Operation Action 
function format(item) { return item.text; };
var fAction = {
    SelectedCostCenter: '',
    AssetGroup: '',
    _GetParameter: function () {
        return {
            GUID: Params.GUID,
            COMPANY: $('#WFD02115_COMPANY').val(),
            DOC_NO: Params.DOC_NO,
            LINE_NO: Params.LINE_NO,
            ASSET_GROUP: $('#WFD02115_ASSET_GROUP').val(),
            PARENT_ASSET_NO: $('#WFD02115_PARENT_ASSET_NO').val(),
            ASSET_MAINSUB: $('#WFD02115_ASSET_MAIN_SUB').val(),
            WBS_BUDGET: $('#WFD02115_WBS_BUDGET').val(),
            INDICATOR_VALIDATE: (Params.FLOW_TYPE == "ST" || GLOBAL.REQUEST_TYPE == "C"
                || (Params.FLOW_TYPE === "CN" && $('#WFD02115_WBS_BUDGET').val() === null)) ? "N" : "Y",
            ASSET_CLASS: $('#WFD02115_ASSET_CLASS').val(),
            ASSET_NO: $('#WFD02115_ASSET_NO').val(),
            ASSET_SUB: $('#WFD02115_ASSET_SUB').val(),
            ASSET_DESC: $('#WFD02115_ASSET_DESC').val(),
            ADTL_ASSET_DESC: $('#WFD02115_ADTL_ASSET_DESC').val(),
            SERIAL_NO: $('#WFD02115_SERIAL_NO').val(),
            INVEN_NO: $('#WFD02115_INVEN_NO').val(),
            BASE_UOM: $('#WFD02115_BASE_UOM').val(),
            INVEN_INDICATOR: ($('#WFD02115_INVEN_INDICATOR').is(":checked") ? 'Y' : 'N'),
            INVEN_NOTE: $('#WFD02115_INVEN_NOTE').val(),
            COST_CODE: $('#WFD02115_COST_CODE').val(),
            RESP_COST_CODE: $('#WFD02115_RESP_COST_CODE').val(),
            LOCATION: $('#WFD02115_LOCATION').val(),
            ROOM: $('#WFD02115_ROOM').val(),
            LICENSE_PLATE: $('#WFD02115_LICENSE_PLATE').val(),
            WBS_PROJECT: $('#WFD02115_WBS_PROJECT').val(),
            INVEST_REASON: $('#WFD02115_BOI').val(),
            MINOR_CATEGORY: $('#WFD02115_MINOR_CATEGORY').val(),
            // ASSET_STATUS: $('#WFD02115_ASSET_STATUS').val(),
            MACHINE_LICENSE: $('#WFD02115_MACHINE_LICENSE').val(),
            BOI_NO: $('#WFD02115_BOI_NO').val(),
            FINAL_ASSET_CLASS: $('#WFD02115_FINAL_ASSET_CLASS').val(),
            // ASSET_SUPPER_NO: $('#WFD02115_ASSET_SUPPER_NO').val(),
            MANUFACT_ASSET: $('#WFD02115_MANUFACTURE_OF_ASSET').val(),
            LOCATION_REMARK: $('#WFD02115_LOCATION_REMARK').val(),
            ASSET_TYPE_NAME: $('#MACHINE_LICENSE_NAME').text(),
            PLATE_TYPE: $('#WFD02115_PLATE_TYPE').val(),
            BARCODE_SIZE: $('#WFD02115_BARCODE_SIZE').val(),
            LEASE_AGREE_DATE: $('#WFD02115_LEASE_AGREE_DATE').val(),
            LEASE_START_DATE: $('#WFD02115_LEASE_START_DATE').val(),
            LEASE_LENGTH_YEAR: $('#WFD02115_LEASE_LENGTH_YEAR').val(),
            LEASE_LENGTH_PERD: $('#WFD02115_LEASE_LENGTH_PERD').val(),
            LEASE_TYPE: $('#WFD02115_LEASE_TYPE').val(),
            LEASE_SUPPLEMENT: $('#WFD02115_LEASE_SUPPLEMENT').val(),
            LEASE_NUM_PAYMENT: $('#WFD02115_LEASE_NUM_PAYMENT').val(),
            LEASE_PAY_CYCLE: $('#WFD02115_LEASE_PAY_CYCLE').val(),
            LEASE_ADV_PAYMENT: $('#WFD02115_LEASE_ADV_PAYMENT').val(),
            LEASE_PAYMENT: strToFloat($('#WFD02115_LEASE_PAYMENT').val()),
            LEASE_ANU_INT_RATE: strToFloat($('#WFD02115_LEASE_ANU_INT_RATE').val()),
            DPRE_AREA_01: $('#WFD02115_DPRE_AREA_01').text(),
            DPRE_KEY_01: $('#WFD02115_DPRE_KEY_01').val(),
            PLAN_LIFE_YEAR_01: $('#WFD02115_PLAN_LIFE_YEAR_01').val(),
            PLAN_LIFE_PERD_01: $('#WFD02115_PLAN_LIFE_PERD_01').val(),
            SCRAP_VALUE_01: strToFloat($('#WFD02115_SCRAP_VALUE_01').val()),
            USEFUL_LIFE_TYPE_01: $('#WFD02115_USEFUL_LIFE_TYPE_01').val(),

            DPRE_AREA_15: $('#WFD02115_DPRE_AREA_15').text(),
            DPRE_KEY_15: $('#WFD02115_DPRE_KEY_15').val(),
            PLAN_LIFE_YEAR_15: $('#WFD02115_PLAN_LIFE_YEAR_15').val(),
            PLAN_LIFE_PERD_15: $('#WFD02115_PLAN_LIFE_PERD_15').val(),
            SCRAP_VALUE_15: strToFloat($('#WFD02115_SCRAP_VALUE_15').val()),
            USEFUL_LIFE_TYPE_15: $('#WFD02115_USEFUL_LIFE_TYPE_15').val(),
            DEACT_DEPRE_AREA_15: ($('#WFD02115_DEACT_DEPRE_AREA_15').is(":checked") ? 'Y' : 'N'),

            DPRE_AREA_31: $('#WFD02115_DPRE_AREA_31').text(),
            DPRE_KEY_31: $('#WFD02115_DPRE_KEY_31').val(),
            PLAN_LIFE_YEAR_31: $('#WFD02115_PLAN_LIFE_YEAR_31').val(),
            PLAN_LIFE_PERD_31: $('#WFD02115_PLAN_LIFE_PERD_31').val(),
            SCRAP_VALUE_31: strToFloat($('#WFD02115_SCRAP_VALUE_31').val()),
            USEFUL_LIFE_TYPE_31: $('#WFD02115_USEFUL_LIFE_TYPE_31').val(),

            DPRE_AREA_41: $('#WFD02115_DPRE_AREA_41').text(),
            DPRE_KEY_41: $('#WFD02115_DPRE_KEY_41').val(),
            PLAN_LIFE_YEAR_41: $('#WFD02115_PLAN_LIFE_YEAR_41').val(),
            PLAN_LIFE_PERD_41: $('#WFD02115_PLAN_LIFE_PERD_41').val(),
            SCRAP_VALUE_41: strToFloat($('#WFD02115_SCRAP_VALUE_41').val()),
            USEFUL_LIFE_TYPE_41: $('#WFD02115_USEFUL_LIFE_TYPE_41').val(),

            DPRE_AREA_81: $('#WFD02115_DPRE_AREA_81').text(),
            DPRE_KEY_81: $('#WFD02115_DPRE_KEY_81').val(),
            PLAN_LIFE_YEAR_81: $('#WFD02115_PLAN_LIFE_YEAR_81').val(),
            PLAN_LIFE_PERD_81: $('#WFD02115_PLAN_LIFE_PERD_81').val(),
            SCRAP_VALUE_81: strToFloat($('#WFD02115_SCRAP_VALUE_81').val()),
            USEFUL_LIFE_TYPE_81: $('#WFD02115_USEFUL_LIFE_TYPE_81').val(),

            DPRE_AREA_91: $('#WFD02115_DPRE_AREA_91').text(),
            DPRE_KEY_91: $('#WFD02115_DPRE_KEY_91').val(),
            PLAN_LIFE_YEAR_91: $('#WFD02115_PLAN_LIFE_YEAR_91').val(),
            PLAN_LIFE_PERD_91: $('#WFD02115_PLAN_LIFE_PERD_91').val(),
            SCRAP_VALUE_91: strToFloat($('#WFD02115_SCRAP_VALUE_91').val()),
            USEFUL_LIFE_TYPE_91: $('#WFD02115_USEFUL_LIFE_TYPE_91').val(),
            FLOW_TYPE: Params.FLOW_TYPE,

            Cnt: $('#INSERT_ASSET_ROWS').val()
        }

    },

    _Mapping: function (result, isExisting) {
        //loop control 
        //$('#frmWFD02115Submit input, #frmWFD02115Submit  select,#frmWFD02115Submit  label').each(
        //    function () {

        //       // console.log(this);
        //        if ($(this).data("field") == null) {
        //            return;
        //        }

        //        var _fieldName = $(this).data("field");

        //        if ((Params.FLOW_TYPE == "CN" || (Params.FLOW_TYPE == "KN"))
        //            && (_fieldName == "ASSET_NO" || _fieldName == "ASSET_SUB")) {

        //        } else {
        //            if ($(this).is("select")) {
        //                $(this).select2('val', result[_fieldName]);
        //            }
        //            else if ($(this).is("label")) {
        //                $(this).text((result[_fieldName] != null) ? result[_fieldName] : "");
        //            }
        //            else {
        //                $(this).val(result[_fieldName]);
        //            }
        //        }
        //    }
        //);

        //$('#WFD02115_INVEN_INDICATOR').prop("checked", (result.INVEN_INDICATOR == 'Y' ? true : false));
        //$('#WFD02115_DEACT_DEPRE_AREA_15').prop("checked", (result.DEACT_DEPRE_AREA_15 == 'Y' ? true : false));

        //$('#WFD02115_LEASE_AGREE_DATE').datepicker('setDate', (result.LEASE_AGREE_DATE));
        //$('#WFD02115_LEASE_START_DATE').datepicker('setDate', (result.LEASE_START_DATE));
        //$('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
        //$('#WFD02115_MINOR_CATEGORY').select2('val', result.MINOR_CATEGORY);
        //$('#WFD02115_USEFUL_LIFE_TYPE_01').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_01));
        //$('#WFD02115_USEFUL_LIFE_TYPE_15').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_15));
        //$('#WFD02115_USEFUL_LIFE_TYPE_31').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_31));
        //$('#WFD02115_USEFUL_LIFE_TYPE_41').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_41));
        //$('#WFD02115_USEFUL_LIFE_TYPE_81').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_81));
        //$('#WFD02115_USEFUL_LIFE_TYPE_91').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_91));
        //$('#WFD02115_CUMU_ACQU_PRDCOST_01').val(result.CUMU_ACQU_PRDCOST_01);

        fAction._MappingDetail(result, false, isExisting);
    },
    _CopyFromMaster: function (result, isExisting) {
        //loop control 
        //$('#frmWFD02115Submit input, #frmWFD02115Submit  select,#frmWFD02115Submit  label').each(
        //    function () {

        //        // console.log(this);
        //        if ($(this).data("field") == null) {
        //            return;
        //        }

        //        var _fieldName = $(this).data("field");

        //        if (((Params.FLOW_TYPE == "CN" || (Params.FLOW_TYPE == "KN"))
        //            && (_fieldName == "ASSET_NO" || _fieldName == "ASSET_SUB")) 
        //            || _fieldName == "INVEN_NO"
        //            ) {
        //            //Skip
        //        } else {
        //            if ($(this).is("select")) {
        //                $(this).select2('val', result[_fieldName]);
        //            }
        //            else if ($(this).is("label")) {
        //                $(this).text((result[_fieldName] != null) ? result[_fieldName] : "");
        //            }
        //            else {
        //                $(this).val(result[_fieldName]);
        //            }
        //        }

        //    }
        //);

        //$('#WFD02115_INVEN_INDICATOR').prop("checked", (result.INVEN_INDICATOR == 'Y' ? true : false));
        //$('#WFD02115_DEACT_DEPRE_AREA_15').prop("checked", (result.DEACT_DEPRE_AREA_15 == 'Y' ? true : false));

        //$('#WFD02115_LEASE_AGREE_DATE').datepicker('setDate', (result.LEASE_AGREE_DATE));
        //$('#WFD02115_LEASE_START_DATE').datepicker('setDate', (result.LEASE_START_DATE));
        //$('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
        //$('#WFD02115_MINOR_CATEGORY').select2('val', result.MINOR_CATEGORY);
        //$('#WFD02115_USEFUL_LIFE_TYPE_01').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_01));
        //$('#WFD02115_USEFUL_LIFE_TYPE_15').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_15));
        //$('#WFD02115_USEFUL_LIFE_TYPE_31').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_31));
        //$('#WFD02115_USEFUL_LIFE_TYPE_41').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_41));
        //$('#WFD02115_USEFUL_LIFE_TYPE_81').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_81));
        //$('#WFD02115_USEFUL_LIFE_TYPE_91').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_91));
        //$('#WFD02115_CUMU_ACQU_PRDCOST_01').val(result.CUMU_ACQU_PRDCOST_01);

        fAction._MappingDetail(result, true, isExisting);
    },
    _MappingDetail: function (result, isCopyFromMaster, isExisting) {
        console.log('MAP DATA');

        var ignoreFields = [];
        //if ((Params.FLOW_TYPE === "CN") || (Params.FLOW_TYPE === "KN")) {
        //    ignoreFields.push("ASSET_NO");
        //    ignoreFields.push("ASSET_SUB");
        //}
        if (GLOBAL.ALLOW_EDIT === 'Y' && ((Params.FLOW_TYPE === "CN") || (Params.FLOW_TYPE === "KN"))) {
            ignoreFields.push("ASSET_NO");
            ignoreFields.push("ASSET_SUB");
        }

        var notCopyValueFields = [];

        if (isCopyFromMaster) {
            notCopyValueFields.push("INVEN_NO");
        }

        if (isExisting) {
            notCopyValueFields.push('ASSET_MAINSUB');
            notCopyValueFields.push('PARENT_ASSET_NO');
            notCopyValueFields.push('PARENT_ASSET_NAME');
            notCopyValueFields.push('ASSET_CLASS');
            notCopyValueFields.push('MINOR_CATEGORY');

            notCopyValueFields.push('DPRE_KEY_01');
            notCopyValueFields.push('USEFUL_LIFE_TYPE_01');
            notCopyValueFields.push('PLAN_LIFE_YEAR_01');
            notCopyValueFields.push('PLAN_LIFE_PERD_01');
            notCopyValueFields.push('SCRAP_VALUE_01');

            notCopyValueFields.push('DPRE_KEY_15');
            notCopyValueFields.push('USEFUL_LIFE_TYPE_15');
            notCopyValueFields.push('PLAN_LIFE_YEAR_15');
            notCopyValueFields.push('PLAN_LIFE_PERD_15');
            notCopyValueFields.push('SCRAP_VALUE_15');

            notCopyValueFields.push('DPRE_KEY_31');
            notCopyValueFields.push('USEFUL_LIFE_TYPE_31');
            notCopyValueFields.push('PLAN_LIFE_YEAR_31');
            notCopyValueFields.push('PLAN_LIFE_PERD_31');
            notCopyValueFields.push('SCRAP_VALUE_31');

            notCopyValueFields.push('DPRE_KEY_41');
            notCopyValueFields.push('USEFUL_LIFE_TYPE_41');
            notCopyValueFields.push('PLAN_LIFE_YEAR_41');
            notCopyValueFields.push('PLAN_LIFE_PERD_41');
            notCopyValueFields.push('SCRAP_VALUE_41');

            notCopyValueFields.push('DPRE_KEY_81');
            notCopyValueFields.push('USEFUL_LIFE_TYPE_81');
            notCopyValueFields.push('PLAN_LIFE_YEAR_81');
            notCopyValueFields.push('PLAN_LIFE_PERD_81');
            notCopyValueFields.push('SCRAP_VALUE_81');

            notCopyValueFields.push('DPRE_KEY_91');
            notCopyValueFields.push('USEFUL_LIFE_TYPE_91');
            notCopyValueFields.push('PLAN_LIFE_YEAR_91');
            notCopyValueFields.push('PLAN_LIFE_PERD_91');
            notCopyValueFields.push('SCRAP_VALUE_91');

            if (!isCopyFromMaster) {
                notCopyValueFields.push('INVEN_NO');
            }
        }

        $('#frmWFD02115Submit input, #frmWFD02115Submit select,#frmWFD02115Submit label').each(
            function () {
                var fieldName = $(this).data("field");

                if (fieldName) {
                    if (ignoreFields.indexOf(fieldName) !== -1) {
                        return;
                    }

                    var value = '';
                    if (notCopyValueFields.indexOf(fieldName) === -1) {
                        value = result[fieldName];
                        console.log(fieldName + ' : ' + value);
                    }

                    if ($(this).is("select")) {
                        $(this).select2('val', value);
                    }
                    else if ($(this).is("label")) {
                        $(this).text(value != null ? value : "");
                    }
                    else {
                        $(this).val(value);
                    }
                }
            }
        );

        $('#WFD02115_INVEN_INDICATOR').prop("checked", (result.INVEN_INDICATOR == 'Y' ? true : false));
        $('#WFD02115_DEACT_DEPRE_AREA_15').prop("checked", (result.DEACT_DEPRE_AREA_15 == 'Y' ? true : false));

        $('#WFD02115_LEASE_AGREE_DATE').datepicker('setDate', (result.LEASE_AGREE_DATE));
        $('#WFD02115_LEASE_START_DATE').datepicker('setDate', (result.LEASE_START_DATE));

        //TODO POP BECAUSE SOME FIELD WHEN SET VALUE WILL EFFECT THIS (ASSET_CLASS OR MINOR_CATEGORY)
        if (isExisting) {
            $('#WFD02115_ASSET_CLASS').select2('val', '');
            $('#WFD02115_MINOR_CATEGORY').select2('val', '');
        } else {
            $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
            $('#WFD02115_MINOR_CATEGORY').select2('val', result.MINOR_CATEGORY);

            $('#WFD02115_USEFUL_LIFE_TYPE_01').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_01));
            $('#WFD02115_USEFUL_LIFE_TYPE_15').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_15));
            $('#WFD02115_USEFUL_LIFE_TYPE_31').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_31));
            $('#WFD02115_USEFUL_LIFE_TYPE_41').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_41));
            $('#WFD02115_USEFUL_LIFE_TYPE_81').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_81));
            $('#WFD02115_USEFUL_LIFE_TYPE_91').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_91));
        }
    },
    _LoadAssetClass: function (_default) {
        $('#WFD02115_ASSET_CLASS').html('');
        $('#WFD02115_ASSET_CLASS_NAME').text('');

        ajax_method.Post('/Common/FinalAssetClass_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }

            $.each(result, function () {
                if (this.CODE == "" || this.CODE == null) {
                    $('#WFD02115_ASSET_CLASS').append($("<option />")
                        .attr('data-description', '')
                        .val('')
                        .html('&nbsp;'));
                } else {
                    $('#WFD02115_ASSET_CLASS').append($("<option />")
                        .attr('data-description', this.VALUE)
                        .val(this.CODE)
                        .text(this.CODE + ' - ' + this.VALUE));
                }

            });

            //Set Default
            $('#WFD02115_ASSET_CLASS').select2('val', _default);

            if (_default != null) {
                fAction._LoadMinorCategory($('#hdMinorCategory').val());
            }

        });
    },
    _LoadFinalAssetClass: function (_default) {
        ajax_method.Post('/Common/FinalAssetClass_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {

                if (this.CODE == "" || this.CODE == null) {
                    $('#WFD02115_FINAL_ASSET_CLASS').append($("<option />")
                        .attr('data-description', '')
                        .val('')
                        .html('&nbsp;'));
                } else {
                    $('#WFD02115_FINAL_ASSET_CLASS').append($("<option />")
                        .attr('data-description', this.VALUE)
                        .val(this.CODE)
                        .text(this.CODE + ' - ' + this.VALUE));
                }


            });

            //Set Default
            $('#WFD02115_FINAL_ASSET_CLASS').select2('val', _default);

        });
    },
    _LoadAUCAssetClass: function (_default, callback) {
        $('#WFD02115_ASSET_CLASS').html('');
        $('#WFD02115_ASSET_CLASS_NAME').text('');

        ajax_method.Post('/Common/AUCAssetClass_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, false, function (result) {
            if (result == null || result.length == 0) {
                if (callback != null)
                    callback();
                return;
            }

            $.each(result, function () {
                $('#WFD02115_ASSET_CLASS').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE + ' - ' + this.VALUE));
            });
            //Set Default
            $('#WFD02115_ASSET_CLASS').select2('val', _default);

            if (callback != null)
                callback();
        });
    },
    _LoadWBSBudget: function (_default, callback, flowType) {
        if (flowType == null) {
            flowType = Params.FLOW_TYPE;
        }

        //Info change 
        var isFilterByFiscalYear;
        if (Params.FLOW_TYPE == "GN") {
            if (Params.CUMU_ACQU_PRDCOST_01 == 0) {
                isFilterByFiscalYear = 'Y';
            } else {
                isFilterByFiscalYear = 'N';
            }
        } else {
            isFilterByFiscalYear = 'Y';
        }
        $('#WFD02115_WBS_BUDGET').html('');
        $('#WFD02115_WBS_BUDGET_NAME').text('');
        $('#WFD02115_WBS_BALANCE').text('');
        ajax_method.Post('/WFD02115/WBSBudget_AutoComplete'
            , { COMPANY: $('#WFD02115_COMPANY').val(), FLOW_TYPE: flowType, SCREEN_MODE: Params.MODE, IS_FILTER_BY_FISCAL_YEAR: isFilterByFiscalYear }
            , false, function (result) {
                if (result == null || result.length == 0) {
                    return;
                }

                $.each(result, function () {
                    $('#WFD02115_WBS_BUDGET').append($("<option />")
                        .attr('data-description', this.DESCRIPTION)
                        .val(this.WBS_CODE)
                        .text(this.WBS_CODE + ' - ' + this.DESCRIPTION));
                });

                //Set Default
                if (_default != null) {
                    if (Params.FLOW_TYPE == "GN" && _default != null && _default.WBS_BUDGET != null) {
                        $('#WFD02115_WBS_BUDGET').append($("<option />")
                            .attr('data-description', '')
                            .val(_default.WBS_BUDGET)
                            .text(_default.WBS_BUDGET));
                    }
                    if (_default.WBS_BUDGET == null) {
                        $('#WFD02115_WBS_BUDGET').select2('val', '');
                    } else {
                        $('#WFD02115_WBS_BUDGET').select2('val', _default.WBS_BUDGET);
                    }
                    var desc = $('#WFD02115_WBS_BUDGET').find(':selected').attr('data-description');
                    $('#WFD02115_WBS_BUDGET_NAME').text(desc);


                } else {
                    $('#WFD02115_WBS_BUDGET').select2('val', '');
                    $('#WFD02115_WBS_BUDGET_NAME').text('');
                }


                //Should be load after list is inserted into combobox
                $('#WFD02115_WBS_BUDGET').on("change", function () {
                    var desc = $(this).find(':selected').attr('data-description');
                    $('#WFD02115_WBS_BUDGET_NAME').text(desc);

                    if (GLOBAL.REQUEST_TYPE == "G" && Params.FLOW_TYPE == "GN") {


                        //In case AUC system derive data Final Asset Class base on WBS Budget -- 2019/11/06
                        if (Params.CUMU_ACQU_PRDCOST_01 == 0) {
                            if ($('#WFD02115_ASSET_GROUP').val() == 'AUC') {
                                var finalAssetClassFromWBSBudget = ($('#WFD02115_WBS_BUDGET').val() != null) ? $('#WFD02115_WBS_BUDGET').val().substr(14, 5) : "";
                                $('#WFD02115_FINAL_ASSET_CLASS').select2('val', finalAssetClassFromWBSBudget);
                            } else {
                                //alert('RMA');
                                $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');
                                //if (_default != null) {
                                //CUMU_ACQU_PRDCOST_01 = 0 enable
                                //var _allowEditWBS = (_default.CUMU_ACQU_PRDCOST_01 == 0)
                                //$('#WFD02115_WBS_BUDGET').prop('disabled', !_allowEditWBS);
                                //$('#WFD02115_BASE_UOM').prop('disabled', !_allowEditWBS);
                                //}
                            }
                        }

                    }
                    if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU") {
                        fAction._LoadWBSProject(null);

                        //Add on 2019/12/02 BCT-IS : clear after change WBS Budget
                        $('#WFD02115_ASSET_MAIN_SUB').select2('val', '');
                        $('#WFD02115_PARENT_ASSET_NO').val('');
                        $('#WFD02115_PARENT_ASSET_NAME').text('');
                        $('#WFD02115_ASSET_CLASS').select2('val', '');
                        $('#WFD02115_MINOR_CATEGORY').select2('val', '');
                    }

                    //Editing mode only
                    fAction.LoadWBSBudgetInfo();
                });


            });
    },
    _LoadWBSProject: function (_default) {
        $('#WFD02115_WBS_PROJECT').html('');
        $('#WFD02115_WBS_PROJECT_NAME').text('');

        ajax_method.Post('/WFD02115/WBSProject_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val(), FLOW_TYPE: Params.FLOW_TYPE, WBS_BUDGET: $('#WFD02115_WBS_BUDGET').val() }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                $('#WFD02115_WBS_PROJECT').append($("<option />")
                    .attr('data-description', this.DESCRIPTION)
                    .val(this.WBS_CODE)
                    .text(this.WBS_CODE + ' - ' + this.DESCRIPTION));
            });

            if (Params.FLOW_TYPE == "GN" && _default != null) {
                $('#WFD02115_WBS_PROJECT').append($("<option />")
                    .attr('data-description', '')
                    .val(_default)
                    .text(_default));
            }

            //Set Default
            $('#WFD02115_WBS_PROJECT').select2('val', _default);



        });
    },
    _LoadMinorCategory: function (_default) {

        var _assetClass = "";
        /*
        if (fAction.AssetGroup == "AUC") {
            _assetClass = $('#WFD02115_FINAL_ASSET_CLASS').val();
        }
        else {
            */
        _assetClass = $('#WFD02115_ASSET_CLASS').val();
        //}

        if (_assetClass == "") {
            return;
        }

        $('#WFD02115_MINOR_CATEGORY').html('');
        $('#WFD02115_MINOR_CATEGORY').text('');
        ajax_method.Post('/Common/MinorCategory_DropDown', { COMPANY: $('#WFD02115_COMPANY').val(), ASSET_CLASS: _assetClass }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                if (this.CODE == "" || this.CODE == null) {
                    $('#WFD02115_MINOR_CATEGORY').append($("<option />")
                        .attr('data-description', this.VALUE)
                        .val('')
                        .html('&nbsp;'));
                } else {
                    $('#WFD02115_MINOR_CATEGORY').append($("<option />")
                        .attr('data-description', this.VALUE)
                        .val(this.CODE)
                        .text(this.CODE + ' - ' + this.VALUE));
                }

            });
            //Set Default
            $('#WFD02115_MINOR_CATEGORY').select2('val', _default);


            if (GLOBAL.REQUEST_TYPE == 'A' && Params.FLOW_TYPE == 'ST') {
                if (Params.LINE_NO == 0) {
                    fAction.LoadDepreciation();
                }
                //fScreenMode.SetNewMode_NewAssetLatestStep();
                return;
            }
        });

        if (!doAddAssetProcess) {
            if ($('#WFD02115_ASSET_GROUP').val() == 'RMA') {
                $('#WFD02115_BASE_UOM').select2('val', 'EA');
                $('#WFD02115_BASE_UOM').prop('disabled', true);
                //$('#WFD02115_ASSET_MAIN_SUB').prop('disabled', false);
            } else if ($('#WFD02115_ASSET_GROUP').val() == 'AUC') {
                if (Params.LINE_NO == 0) {
                    $('#WFD02115_BASE_UOM').select2('val', '');
                }

                //$('#WFD02115_ASSET_MAIN_SUB').prop('disabled', false);
            }
        }
    },
    _LoadLocation: function (_default) {
        ajax_method.Post('/Common/Location_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {
                if (this.CODE == "" || this.CODE == null) {
                    $('#WFD02115_LOCATION').append($("<option />")
                        .attr('data-description', '')
                        .val('')
                        .html('&nbsp;'));
                } else {
                    $('#WFD02115_LOCATION').append($("<option />")
                        .attr('data-description', this.VALUE)
                        .val(this.CODE)
                        .text(this.CODE + ' - ' + this.VALUE));
                }
            });

            if (Params.FLOW_TYPE == "GN" && _default != null) {
                $('#WFD02115_LOCATION').append($("<option />")
                    .attr('data-description', '')
                    .val(_default)
                    .text(_default));
            }
            //Set Default
            $('#WFD02115_LOCATION').select2('val', _default);
        });
    },
    _LoadBOITaxPrivilege: function (_BOI, _default) {
        /*if (_BOI == 'B') {
            _BOI = null;
        }
        */
        $('#WFD02115_BOI_NO').html('');

        ajax_method.Post('/Common/TaxPrivilegeAutoComplete', { COMPANY: $('#WFD02115_COMPANY').val(), _keyword: _BOI }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $('#WFD02115_BOI_NO').append($("<option />")
                .attr('data-description', '')
                .val('')
                .html('&nbsp;'));

            $.each(result, function () {
                $('#WFD02115_BOI_NO').append($("<option />")
                    .attr('data-description', this.VALUE)
                    .val(this.CODE)
                    .text(this.CODE + ' - ' + this.VALUE));
            });
            //Set Default
            if (_default != null && _default != "") {
                $('#WFD02115_BOI_NO').select2('val', _default);
            }
            if (Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "ST") {
                $('#WFD02115_BOI_NO').prop('disabled', false);
            }
        });
    },
    _LoadMachineLicense: function (_default) {
        ajax_method.Post('/Common/MachineLicense_AutoComplete', { COMPANY: $('#WFD02115_COMPANY').val() }, false, function (result) {
            if (result == null || result.length == 0) {
                return;
            }
            $.each(result, function () {

                if (this.CODE == "" || this.CODE == null) {
                    $('#WFD02115_MACHINE_LICENSE').append($("<option />")
                        .attr('data-description', '')
                        .val('')
                        .html('&nbsp;'));
                } else {
                    $('#WFD02115_MACHINE_LICENSE').append($("<option />")
                        .attr('data-description', this.VALUE)
                        .val(this.CODE)
                        .text(this.CODE + ' - ' + this.VALUE));
                }

            });
            //Set Default
            $('#WFD02115_MACHINE_LICENSE').select2('val', _default);
        });
    },

    _InitialWBSForRMA: function (_default) {
    },
    _InitialWBSForSettlement: function (_default) {
        if (GLOBAL.REQUEST_TYPE == "A" && Params.FLOW_TYPE == "ST") {

            $('#WFD02115_WBS_BUDGET').prop('disabled', true);
            $('#WFD02115_ASSET_CLASS').prop('disabled', false); //allow to change      
            $('#WFD02115_ASSET_GROUP').val('RMA');
            $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', false); //enable
            $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);

            //Enable should be show default
            $('#WFD02115_COST_CODE').prop('disabled', false);
            $('#WFD02115BtnCOST_CODE').prop('disabled', false);

            if (_default == null) {
                fAction._LoadAssetClass(null);
            } else {
                fAction._LoadAssetClass(_default.ASSET_CLASS);
            }


        }
    },

    LoadWBSBudgetInfo: function (assetGroup) {

        // If WBS Budget is blank
        if ($('#WFD02115_WBS_BUDGET').val() == '') {

            $('#WFD02115_WBS_BALANCE').text('');
            $('#WFD02115_ASSET_CLASS').select2('val', '');
            $('#WFD02115_MINOR_CATEGORY').select2('val', '');

            $('#WFD02115_RESP_COST_CODE').val('');
            $('#WFD02115_COST_CODE').val('');
            $('#WFD02115_ASSET_GROUP').val('');

            //return ;
        }


        var _data = {
            COMPANY_CODE: $('#WFD02115_COMPANY').val(),
            WBS: $('#WFD02115_WBS_BUDGET').val(),
        };
        ajax_method.Post(URL_CONST.WFD02115_GetDeriveWBSBudget, _data, false, function (result) {
            if (result == null) {
                //$('#WFD02115_WBS_BALANCE').text('');
                //console.log("WFD02115_GetWBSBudgetInfo  : No data found");
                return;
            }

            if (Params.FLOW_TYPE == "RM") {
                $('#WFD02115_WBS_ASSET_CLASS').val(result.ASSET_CLASS);
            }

            $('#WFD02115_WBS_BALANCE').text(commaSeparateNumber(result.AVAILABLE));

            if (Params.FLOW_TYPE != "GN") {
                $('#WFD02115_RESP_COST_CODE').val(result.RESP_COST_CENTER);
                $('#WFD02115_RESP_COST_NAME').text(result.RESP_COST_NAME);
                if (result.COM_IND_BUDGET != 'Y') {
                    $('#WFD02115_COST_CODE').val(result.COST_CENTER);
                    $('#WFD02115_COST_NAME').text(result.COST_NAME);
                } else {
                    $('#WFD02115_COST_CODE').val('');
                    $('#WFD02115_COST_NAME').text('');
                }
            }

            //Enable when Budget is common
            $('#WFD02115_COST_CODE, #WFD02115BtnCOST_CODE').prop('disabled', result.COM_IND_BUDGET != 'Y');

            if (Params.FLOW_TYPE == "RM") {
                $('#WFD02115_ASSET_GROUP').val("RMA");
            } else {
                if (assetGroup) {
                    $('#WFD02115_ASSET_GROUP').val(assetGroup);
                } else {
                    $('#WFD02115_ASSET_GROUP').val(result.ASSET_GROUP);
                }
            }

            //If WBS for RMA Assets
            if (GLOBAL.REQUEST_TYPE == 'A') {
                //result.ASSET_GROUP
                if (fAction.AssetGroup == "RMA") {
                    $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                    $('#WFD02115_MINOR_CATEGORY').select2('val', result.MINOR_CATEGORY);

                    //$('#WFD02115_RESP_COST_CODE').prop('disabled', false);

                    $('#WFD02115_DEFAULT_FINAL_ASSET_CLASS').val(result.ASSET_CLASS);

                    $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');

                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);

                    fAction._LoadAssetClass(result.ASSET_CLASS);
                }
                if (fAction.AssetGroup == "AUC") {

                    $('#WFD02115_DEFAULT_FINAL_ASSET_CLASS').val(result.ASSET_CLASS);
                    $('#WFD02115_FINAL_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false); //default and allow to user edit
                }

                $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', false); //enable

                return;
            }

            if (GLOBAL.REQUEST_TYPE == 'C') { //Reclass

                $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');

                $('#WFD02115_ASSET_CLASS').prop('disabled', false); //allow to user change
                $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true); // set blank and disables

            }
        })
    },
    LoadDepreciation: function () {

        if (!doUpdateDepreciation) {
            return;
        }

        //Clear Value
        $('#DPRE_MANI_1 input').val('');
        $('.aeconly input').val('');

        $('#WFD02115_PLATE_TYPE').select2('val', '');
        $('#WFD02115_BARCODE_SIZE').select2('val', '');
        $('#WFD02115_INVEN_INDICATOR').prop("checked", false);



        var _data = {
            COMPANY: $('#WFD02115_COMPANY').val(),
            ASSET_CLASS: $('#WFD02115_ASSET_CLASS').val(),
            MINOR_CATEGORY: $('#WFD02115_MINOR_CATEGORY').val(),
        };
        //User must select both of criteria
        if ((_data.ASSET_CLASS == null || _data.ASSET_CLASS == "") || (_data.MINOR_CATEGORY == null || _data.MINOR_CATEGORY == "")) {
            //console.log("LoadDepreciatioin : some field is blank");
            return;
        }


        ajax_method.Post(URL_CONST.WFD02115_GetDepreciationData, _data, false, function (result) {
            if (result == null) {
                //console.log("WFD02115_GetDepreciationData  : No data found");
                return;
            }

            $('#WFD02115_PLATE_TYPE').select2('val', result.STICKER_TYPE);
            $('#WFD02115_BARCODE_SIZE').select2('val', result.STICKER_SIZE);
            // INVENTORY_INDICATOR
            if (result.INVENTORY_INDICATOR == "Y") {
                $('#WFD02115_INVEN_INDICATOR').prop("checked", true);
            }

            // DPRE_AREA_01

            $('#WFD02115_DPRE_KEY_01').val(result.DPRE_KEY_01);
            $('#WFD02115_PLAN_LIFE_YEAR_01').val(result.USEFUL_LIFE_YEAR_01);
            $('#WFD02115_PLAN_LIFE_PERD_01').val(result.USEFUL_LIFE_PERD_01);
            $('#WFD02115_SCRAP_VALUE_01').val(result.SCRAP_VALUE_01);
            $('#WFD02115_USEFUL_LIFE_TYPE_01').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_01));

            $('#WFD02115_DPRE_KEY_15').val(result.DPRE_KEY_15);
            $('#WFD02115_PLAN_LIFE_YEAR_15').val(result.USEFUL_LIFE_YEAR_15);
            $('#WFD02115_PLAN_LIFE_PERD_15').val(result.USEFUL_LIFE_PERD_15);
            $('#WFD02115_SCRAP_VALUE_15').val(result.SCRAP_VALUE_15);
            $('#WFD02115_USEFUL_LIFE_TYPE_15').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_15));
            $('#WFD02115_DEACT_DEPRE_AREA_15').prop("checked", false);
            if (result.DEACT_DEPRE_AREA_15 == "Y") {
                $('#WFD02115_DEACT_DEPRE_AREA_15').prop("checked", true);
            }

            $('#WFD02115_DPRE_KEY_31').val(result.DPRE_KEY_31);
            $('#WFD02115_PLAN_LIFE_YEAR_31').val(result.USEFUL_LIFE_YEAR_31);
            $('#WFD02115_PLAN_LIFE_PERD_31').val(result.USEFUL_LIFE_PERD_31);
            $('#WFD02115_SCRAP_VALUE_31').val(result.SCRAP_VALUE_31);
            $('#WFD02115_USEFUL_LIFE_TYPE_31').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_31));

            $('#WFD02115_DPRE_KEY_41').val(result.DPRE_KEY_41);
            $('#WFD02115_PLAN_LIFE_YEAR_41').val(result.USEFUL_LIFE_YEAR_41);
            $('#WFD02115_PLAN_LIFE_PERD_41').val(result.USEFUL_LIFE_PERD_41);
            $('#WFD02115_SCRAP_VALUE_41').val(result.SCRAP_VALUE_41);
            $('#WFD02115_USEFUL_LIFE_TYPE_41').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_41));
            if (fAction.isLeasingOrRightOfUse()) {
                $('#WFD02115_DPRE_KEY_81').val(result.DPRE_KEY_81);
                $('#WFD02115_PLAN_LIFE_YEAR_81').val(result.USEFUL_LIFE_YEAR_81);
                $('#WFD02115_PLAN_LIFE_PERD_81').val(result.USEFUL_LIFE_PERD_81);
                $('#WFD02115_SCRAP_VALUE_81').val(result.SCRAP_VALUE_81);
                $('#WFD02115_USEFUL_LIFE_TYPE_81').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_81));

                $('#WFD02115_DPRE_KEY_91').val(result.DPRE_KEY_91);
                $('#WFD02115_PLAN_LIFE_YEAR_91').val(result.USEFUL_LIFE_YEAR_91);
                $('#WFD02115_PLAN_LIFE_PERD_91').val(result.USEFUL_LIFE_PERD_91);
                $('#WFD02115_SCRAP_VALUE_91').val(result.SCRAP_VALUE_91);
                $('#WFD02115_USEFUL_LIFE_TYPE_91').val(getUsefulLifeTypeDesc(result.USEFUL_LIFE_TYPE_91));
            }
        })
    },
    Initialize: function (_default) {
        if (_default != null) {
            Params.CUMU_ACQU_PRDCOST_01 = _default.CUMU_ACQU_PRDCOST_01;
        }
        fAction.AssetGroup = "";
        if (GLOBAL.REQUEST_TYPE != "G") { // G is Asset Info Change.
            fAction.AssetGroup = Params.FLOW_TYPE == "AU" ? "AUC" : "RMA";
        }

        $('#WFD02115_COMPANY').val(GLOBAL.COMPANY);
        $('#WFD02115_DOCNO').val(Params.DOC_NO);
        if (Params.FLOW_TYPE == "KN" && Params.LINE_NO == 0) {
            $('#WFD02115_ITEMNO').val(Params.NEW_LINE_NO);
        } else {
            $('#WFD02115_ITEMNO').val(Params.LINE_NO);
        }


        //Bind Combobox

        if ((Params.FLOW_TYPE == "CN" && Params.OPTION != "E") || (Params.FLOW_TYPE == "KN" && Params.OPTION != "E")) {
            return;
        }

        if (_default == null) {

            if (Params.FLOW_TYPE == "AU") {
                fAction._LoadAUCAssetClass(null);
            } else {
                fAction._LoadAssetClass(null);
            }
            fAction._LoadFinalAssetClass(null);

            if (GLOBAL.REQUEST_TYPE == "A" && Params.FLOW_TYPE == "RM") {
                //WFD02115_WBS_BUDGET
                fAction._LoadWBSBudget(null, fAction._InitialWBSForRMA(null));
            } else if (GLOBAL.REQUEST_TYPE == "G" && Params.FLOW_TYPE == "GN") {
                fAction._LoadWBSBudget(null, null, null);
            } else {
                //WFD02115_WBS_BUDGET
                fAction._LoadWBSBudget(null, fAction._InitialWBSForSettlement(null));
            }

            //WFD02115_WBS_PROJECT
            fAction._LoadWBSProject(null);

            //WFD02115_LOCATION
            fAction._LoadLocation(null);

            //WFD02115_BOI_NO
            fAction._LoadBOITaxPrivilege(null, null);

            //WFD02115_MACHINE_LICENSE
            fAction._LoadMachineLicense(null);
        }
        else {
            if (Params.FLOW_TYPE == "AU") {
                fAction._LoadAUCAssetClass(_default.ASSET_CLASS);
            } else if (Params.FLOW_TYPE == "GN") {
                if (_default.ASSET_GROUP == "AUC") {
                    fAction._LoadAUCAssetClass(_default.ASSET_CLASS);
                } else {
                    fAction._LoadAssetClass(_default.ASSET_CLASS);
                }
            } else {
                fAction._LoadAssetClass(_default.ASSET_CLASS);
            }
            fAction._LoadFinalAssetClass(_default.FINAL_ASSET_CLASS);

            //WFD02115_WBS_BUDGET
            if (GLOBAL.REQUEST_TYPE == "G" && Params.FLOW_TYPE == "GN") {
                if (_default.ASSET_GROUP == "AUC") {
                    fAction._LoadWBSBudget(_default, null, "AU");
                } else {
                    fAction._LoadWBSBudget(_default, null, "RM");
                }
            } else {
                if (Params.FLOW_TYPE === "KN") {
                    ajax_method.Post(URL_CONST.WFD02115_GetWBSData, { WBS: _default.WBS_BUDGET, COMPANY_CODE: Params.COMPANY }, false, function (result) {
                        if (!result) return;

                        $('#WFD02115_WBS_BUDGET').append($("<option />")
                            .attr('data-description', result.WBS_DESCRIPTION)
                            .val(_default.WBS_BUDGET)
                            .text(_default.WBS_BUDGET + ' - ' + result.WBS_DESCRIPTION));

                        $('#WFD02115_WBS_BUDGET').select2('val', _default.WBS_BUDGET);
                        $('#WFD02115_WBS_BUDGET_NAME').text(result.WBS_DESCRIPTION);
                    });

                    fAction._InitialWBSForSettlement(_default.ASSET_CLASS);
                } else {
                    fAction._LoadWBSBudget(_default, fAction._InitialWBSForSettlement(_default.ASSET_CLASS));
                }
            }

            //WFD02115_WBS_PROJECT
            fAction._LoadWBSProject(_default.WBS_PROJECT);

            //WFD02115_LOCATION
            fAction._LoadLocation(_default.LOCATION);

            //WFD02115_BOI_NO
            fAction._LoadBOITaxPrivilege(_default.INVEST_REASON, _default.BOI_NO);

            //WFD02115_MACHINE_LICENSE
            fAction._LoadMachineLicense(_default.MACHINE_LICENSE);
        }

        $('#WFD02115_ASSET_MAIN_SUB').trigger('change');

        //Load Data

    },

    LoadOriginal: function (callback) {
        ajax_method.Post(URL_CONST.WFD02115_GetOriginalAssetsData, Params, false, function (result) {
            if (result == null) {
                //console.log("No data found");
                return;
            }
            //console.log("LoadOriginal");
            //console.log(result);

            $('#frmWFD02115Submit input, #frmWFD02115Submit  select,#frmWFD02115Submit  label').each(
                function () {

                    // console.log(this);
                    if ($(this).data("field") == null) {
                        return;
                    }

                    var _fieldName = $(this).data("field");
                    if (_fieldName != "WFD02115_DOCNO"
                        && _fieldName != "ASSET_GROUP"
                        && _fieldName != "COMPANY"
                        && _fieldName != "ASSET_NO"
                        && _fieldName != "ASSET_SUB"
                        && _fieldName != "ASSET_MAINSUB"
                        && _fieldName != "PARENT_ASSET_NO"
                        && _fieldName != "USEFUL_LIFE_TYPE_01"
                        && _fieldName != "USEFUL_LIFE_TYPE_15"
                        && _fieldName != "USEFUL_LIFE_TYPE_31"
                        && _fieldName != "USEFUL_LIFE_TYPE_41"
                        && _fieldName != "USEFUL_LIFE_TYPE_81"
                        && _fieldName != "USEFUL_LIFE_TYPE_91") { //Skip some field
                        if ($(this).is("select") || $(this).is("input")) {
                            var _current = ($(this).val() == null) ? "" : $(this).val();
                            var _newValue = (result[_fieldName] == null) ? "" : result[_fieldName];

                            /*if (_fieldName == "USEFUL_LIFE_TYPE_01" ||
                                _fieldName == "USEFUL_LIFE_TYPE_15" ||
                                _fieldName == "USEFUL_LIFE_TYPE_31" ||
                                _fieldName == "USEFUL_LIFE_TYPE_41" ||
                                _fieldName == "USEFUL_LIFE_TYPE_81" ||
                                _fieldName == "USEFUL_LIFE_TYPE_91") {
                                _newValue = getUsefulLifeTypeDesc(_newValue);
                            }*/
                            if (_current != _newValue) {
                                var _displayValue = (_newValue == "") ? "empty" : _newValue;
                                if (_fieldName == "PLATE_TYPE" || _fieldName == "BARCODE_SIZE") {
                                    _displayValue = (result[_fieldName + '_DESC'] == null || result[_fieldName + '_DESC'] == '') ? "empty" : result[_fieldName + '_DESC'];
                                }


                                if ($(this).is("select")) {
                                    $(this).parent().find('span span.select2-selection').attr("style", "border-color: red;");
                                    $(this).parent().find('span span.select2-selection span.select2-selection__rendered').removeClass('different').addClass("different");
                                    $(this).parent().find('span span.select2-selection').attr('data-original-title', "Original is " + _displayValue).attr('data-toggle', 'tooltip').attr('data-placement', 'top');
                                } else if ($(this).is("input[type=checkbox]")) {
                                    $(this).css('outline', '2px solid red');
                                    if (_displayValue == "Y") {
                                        _displayValue = "Yes";
                                    } else {
                                        _displayValue = "No";
                                    }
                                    $(this).parent().attr('data-original-title', "Original is " + _displayValue).attr('data-toggle', 'tooltip').attr('data-placement', 'left');
                                } else {
                                    $(this).removeClass('different').addClass("different");
                                    $(this).attr('data-original-title', "Original is " + _displayValue).attr('data-toggle', 'tooltip').attr('data-placement', 'top');
                                }

                                //$(this).removeClass('different').addClass("different");
                                //$(this).attr('data-original-title', "Original is " + result[_fieldName]).attr('data-toggle', 'tooltip').attr('data-placement', 'top');

                            }

                        }
                    }

                }
            );

            //$('#WFD02115_INVEN_INDICATOR').prop("checked", (result.INVEN_INDICATOR == 'Y' ? true : false));
            //$('#WFD02115_LEASE_AGREE_DATE').datepicker('setDate', (result.LEASE_AGREE_DATE));
            //$('#WFD02115_LEASE_START_DATE').datepicker('setDate', (result.LEASE_START_DATE));


            if (callback != null)
                callback();
        })
    },
    LoadData: function (callback) {
        ajax_method.Post(URL_CONST.WFD02115_GetAssetsData, Params, false, function (result) {
            if (result == null) {
                //console.log("No data found");
                return;
            }

            //Load with default
            fAction.Initialize(result);

            fAction._Mapping(result);

            //Retrive WBS Balance only
            var _data = {
                COMPANY_CODE: $('#WFD02115_COMPANY').val(),
                WBS: $('#WFD02115_WBS_BUDGET').val(),
            };
            ajax_method.Post(URL_CONST.WFD02115_GetDeriveWBSBudget, _data, false, function (result) {
                if (result == null) {
                    return;
                }
                $('#WFD02115_WBS_BALANCE').text(commaSeparateNumber(result.AVAILABLE));
            });

            if (callback != null)
                callback(result);
        })
    },
    ShowSearchPopup: function (callback) {
        //Set search parameter

        f2190SearchAction.ClearDefault();

        Search2190Config.RoleMode = Params.ROLEMODE;

        if (Params.FLOW_TYPE != 'GN') {
            Search2190Config.searchOption = "COPY";
        } else {
            Search2190Config.searchOption = Params.FLOW_TYPE;
        }
        Search2190Config.AllowMultiple = false;

        Search2190Config.Default.Company = { Value: GLOBAL.COMPANY, Enable: false };

        if (fAction.AssetGroup != "")
            Search2190Config.Default.AssetGroup = { Value: fAction.AssetGroup, Enable: false };

        Search2190Config.customCallback = null;
        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();

    },
    ShowSearchParentPopup: function (callback) {

        //Set search parameter

        f2190SearchAction.ClearDefault();

        Search2190Config.RoleMode = Params.ROLEMODE;

        Search2190Config.searchOption = Params.FLOW_TYPE;
        Search2190Config.AllowMultiple = false;

        Search2190Config.Default.Company = { Value: GLOBAL.COMPANY, Enable: false };
        Search2190Config.Default.CostCode = { Value: fAction.SelectedCostCenter, Enable: fAction.SelectedCostCenter == "" };
        Search2190Config.Default.AssetGroup = { Value: fAction.AssetGroup, Enable: false };
        Search2190Config.Default.Parent = { Value: "Y", Enable: false };

        Search2190Config.customCallback = function (_list, callback) {
            if (_list == null || _list.length == 0) {
                fnErrorDialog('Error', "No selected data");
                return;
            }

            var _selected = _list[0];
            //console.log(_selected);
            $('#WFD02115_PARENT_ASSET_NO').val(_selected.ASSET_NO);
            $('#WFD02115_PARENT_ASSET_NAME').text((_selected.ASSET_NAME != null) ? _selected.ASSET_NAME : "");

            $('#hdMinorCategory').val(_selected.MINOR_CATEGORY);


            //Same as Parent
            $('#WFD02115_ASSET_CLASS').select2('val', _selected.ASSET_CLASS);
            //$('#WFD02115_FINAL_ASSET_CLASS').select2("val", _selected.FINAL_ASSET_CLASS); //RMA final should be blank
            //$('#WFD02115_ASSET_CLASS').trigger("change");

            fAction.LoadDepreciation();

            // fAction._LoadMinorCategory(_selected.MINOR_CATEGORY);

            Search2190Config.customCallback = null;
            if (callback == null)
                return;

            callback();


        };

        //Assign Default and Initial screen
        f2190SearchAction.Initialize();

        $('#WFD02190_SearchAssets').modal();

    },
    SaveData: function (callback) {
        //console.log(fAction._GetParameter());
        //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
        if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "ST") {
            if (!fAction.isLeasingOrRightOfUse()) {
                //Clear leaseing
                $('#WFD02115_LEASE_AGREE_DATE, #WFD02115_LEASE_START_DATE, #WFD02115_LEASE_LENGTH_YEAR, #WFD02115_LEASE_LENGTH_PERD').val('');
                $('#WFD02115_LEASE_TYPE').select2('val', '');
                $('#WFD02115_LEASE_SUPPLEMENT, #WFD02115_LEASE_NUM_PAYMENT, #WFD02115_LEASE_PAY_CYCLE, #WFD02115_LEASE_ADV_PAYMENT, #WFD02115_LEASE_PAYMENT, #WFD02115_LEASE_ANU_INT_RATE').val('');
            }
        }

        // Check input data.
        if (Validation()) {
            var saveData = fAction._GetParameter();

            if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "ST") {
                if (!fAction.isLeasingOrRightOfUse()) {
                    saveData.DPRE_AREA_81 = '';
                    saveData.DPRE_KEY_81 = '';
                    saveData.PLAN_LIFE_YEAR_81 = '';
                    saveData.PLAN_LIFE_PERD_81 = '';
                    saveData.SCRAP_VALUE_81 = '';
                    saveData.USEFUL_LIFE_TYPE_81 = '';

                    saveData.DPRE_AREA_91 = '';
                    saveData.DPRE_KEY_91 = '';
                    saveData.PLAN_LIFE_YEAR_91 = '';
                    saveData.PLAN_LIFE_PERD_91 = '';
                    saveData.SCRAP_VALUE_91 = '';
                    saveData.USEFUL_LIFE_TYPE_91 = '';
                }
            }
            if (Params.FLOW_TYPE == "GN") {
                if (fAction.isLeasingOrRightOfUse()) {
                    saveData.USEFUL_LIFE_TYPE_81 = (saveData.USEFUL_LIFE_TYPE_81 == "N/A") ? '' : saveData.USEFUL_LIFE_TYPE_81;
                    saveData.USEFUL_LIFE_TYPE_91 = (saveData.USEFUL_LIFE_TYPE_91 == "N/A") ? '' : saveData.USEFUL_LIFE_TYPE_91;
                }
            }

            ajax_method.Post(URL_CONST.WFD02115_SaveAssetsData, saveData, false, function (result) {
                var isError = false;
                //console.log(result);
                if (result.IsError) {
                    fnErrorDialog('Error', result.Message);
                    isError = true;
                    return;
                }
                // fnCompleteDialog('Save successfully!');

                if (Params.FLOW_TYPE == "CN") {
                    ajax_method.Post(URL_CONST.WFD02115_SaveAssetsReclassData, fAction._GetParameter(), false, function (result) {
                        if (result.IsError) {
                            fnErrorDialog('Error', result.Message);
                            isError = true;
                            return;
                        }
                    });
                }

                if (Params.FLOW_TYPE == "KN") {

                    var inputParams = fAction._GetParameter();
                    if (Params.OPTION != "E") {
                        inputParams.LINE_NO = Params.NEW_LINE_NO;
                    }

                    if (inputParams.DOC_NO == null || inputParams.DOC_NO == "") {
                        inputParams.DOC_NO = GLOBAL.COMPANY + '.' + GLOBAL.USER_LOGIN + '/9999'; //Generate Temp DOC_NO
                    }

                    ajax_method.Post(URL_CONST.InsertNewToTargetAssets, inputParams, false, function (result) {
                        if (result.IsError) {
                            fnErrorDialog('Error', result.Message);
                            isError = true;
                            return;
                        }
                    });
                }

                if (!isError) {
                    if (callback == null)
                        return;

                    callback();
                }

            });
        }
    },
    AddAsset: function (_list, callback) { //It's call from dialog
        doAddAssetProcess = true;

        try {
            //should have 1 records.
            if (_list == null || _list.length == 0) {
                fnErrorDialog('Error', "No selected data");
                doAddAssetProcess = false;
                return;
            }

            var _data = _list[0];
            //Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId)
            ajax_method.Post(URL_CONST.WFD02115_GetFixedAssetMaster, _data, false, function (result) {
                if (result == null) {
                    //console.log("AddAsset : No data found");
                    return;
                }
                doUpdateDepreciation = false;
                //console.log(result);
                Params.CUMU_ACQU_PRDCOST_01 = result.CUMU_ACQU_PRDCOST_01;

                if (Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "ST") {
                    result.ASSET_NO = "";
                    result.ASSET_SUB = "";
                    result.ORIGINAL_ASSET_NO = "";
                    result.ORIGINAL_ASSET_SUB = "";
                }

                if (Params.FLOW_TYPE == "ST") {
                    result.WBS_BUDGET = "";
                }

                if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "ST") {
                    fAction._CopyFromMaster(result, true);
                } else if (Params.FLOW_TYPE == "GN") {
                    fAction._Mapping(result, false);
                } else {
                    fAction._Mapping(result, true);
                }

                //Incase Asset Info Change (Add Mode)
                if (Params.MODE == "N" && GLOBAL.REQUEST_TYPE == "G") {
                    fScreenMode.SetNewMode_AssetInfoChangeAfterGet(result.CUMU_ACQU_PRDCOST_01 == 0);
                    if (result.ASSET_GROUP == "AUC") {
                        fAction._LoadAUCAssetClass(result.ASSET_CLASS);
                        fAction._LoadWBSBudget(result, null, "AU");
                    } else {
                        fAction._LoadAssetClass(result.ASSET_CLASS);
                        fAction._LoadWBSBudget(result, null, "RM");
                    }
                    if (result.WBS_BUDGET == null) {
                        $('#WFD02115_WBS_BUDGET').select2('val', '');
                    } else {
                        $('#WFD02115_WBS_BUDGET').select2('val', result.WBS_BUDGET);
                    }

                    $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
                    $('#WFD02115_MINOR_CATEGORY').select2('val', result.MINOR_CATEGORY);

                    $('#WFD02115_COST_CODE, #WFD02115BtnCOST_CODE').prop('disabled', true);
                    //$('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);

                    fAction.enableObject('EDIT_INFO_CHG');

                    fAction._LoadMinorCategory($('#hdMinorCategory').val());

                } else if (Params.MODE == "E" && GLOBAL.REQUEST_TYPE == "G") {
                    fAction.enableObject('EDIT_INFO_CHG_AFTER_SUBMIT');
                }

                if (GLOBAL.IsAEC == "Y") {
                    if (fAction.isLeasingOrRightOfUse()) {
                        $('.Leasing-control').show(true);
                        $('.Leasing-control').find('input,select').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
                    } else {
                        $('.Leasing-control').hide(true);
                        $('.Leasing-control').find('input,select').prop('disabled', true);
                    }
                }

                if (Params.FLOW_TYPE == "GN") {
                    if (result.WBS_BUDGET != null && isNullOrEmpty($('#WFD02115_WBS_BUDGET').val())) {
                        $('#WFD02115_WBS_BUDGET').append($("<option />")
                            .attr('data-description', '')
                            .val(result.WBS_BUDGET)
                            .text(result.WBS_BUDGET));
                        $('#WFD02115_WBS_BUDGET').select2('val', result.WBS_BUDGET);
                    }
                    if (result.WBS_PROJECT != null && isNullOrEmpty($('#WFD02115_WBS_PROJECT').val())) {
                        $('#WFD02115_WBS_PROJECT').append($("<option />")
                            .attr('data-description', '')
                            .val(result.WBS_PROJECT)
                            .text(result.WBS_PROJECT));
                        $('#WFD02115_WBS_PROJECT').select2('val', result.WBS_PROJECT);
                    }
                    if (result.LOCATION != null && isNullOrEmpty($('#WFD02115_LOCATION').val())) {
                        $('#WFD02115_LOCATION').append($("<option />")
                            .attr('data-description', '')
                            .val(result.LOCATION)
                            .text(result.LOCATION));
                        $('#WFD02115_LOCATION').select2('val', result.LOCATION);
                    }
                }

                //fAction._LoadBOITaxPrivilege(_default.INVEST_REASON, _default.BOI_NO);

                if (callback != null)
                    callback();

                doUpdateDepreciation = true;
            })

        } finally {
            doAddAssetProcess = false;
        }
    },

    isLeasingOrRightOfUse: function () {
        var str = $('#WFD02115_ASSET_CLASS_NAME').text();
        //str = (str!=null && str != '') ? str.toLowerCase : '';
        var rightOfUse = str.indexOf("Right-of-use");
        var leas = str.indexOf("Leasing");

        if (rightOfUse >= 0 || leas >= 0) {
            return true;
        } else {
            return false;
        }
    },
    isOutsourceTools: function () {
        var assetClass = $('#WFD02115_ASSET_CLASS').val();

        if (WFD02115_VARIABLES.ASSET_CLASS_OUTSOURCE != null) {
            for (var i = 0; i < WFD02115_VARIABLES.ASSET_CLASS_OUTSOURCE.length; i++) {
                if (WFD02115_VARIABLES.ASSET_CLASS_OUTSOURCE[i] == assetClass) {
                    return true;
                }
            }
        }
        return false;
    },
    enableObject: function (mode) {
        function formatSelected(state) {
            return state.id;
        };
        $("#WFD02115_MACHINE_LICENSE").select2({ dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });
        $("#WFD02115_FINAL_ASSET_CLASS").select2({ dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });
        $('#WFD02115_BASE_UOM').select2({ dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });

        console.log("MODE: " + mode);
        $("#WFD02115_WBS_BUDGET").select2({ containerCssClass: "require", dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });
        $("#WFD02115_WBS_PROJECT").select2({ containerCssClass: "require", dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });
        $("#WFD02115_ASSET_CLASS").select2({ containerCssClass: "require", dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });
        $("#WFD02115_MINOR_CATEGORY").select2({ containerCssClass: "require", templateSelection: formatSelected });
        if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "ST" || Params.FLOW_TYPE == "KN" || Params.FLOW_TYPE == "CN") {
            $('#WFD02115_LOCATION').select2({ containerCssClass: 'require', dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });
        }

        $('#WFD02115_BOI').select2({ containerCssClass: 'require', templateSelection: formatSelected });
        $("#WFD02115_BOI_NO").select2({ containerCssClass: "require", dropdownAutoWidth: true, dropdownCssClass: 'auto-drop', templateSelection: formatSelected });

        $("#WFD02115_ASSET_MAIN_SUB").select2({ containerCssClass: "require", templateSelection: formatSelected });

        //enable all Control
        $('#divWFD02115').find('input,select,button').prop('disabled', false);

        //Disable alway Control
        $('.disabledalway').prop('disabled', true);

        //AEC
        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').prop('disabled', true);
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
            $('.aeconly').removeAttr('disabled');
        } else {
            $('.aeconly').prop('disabled', false);
            $('.aeconly').hide();
            $('#DPRE_MANI_2').hide();
        }

        if (mode == 'VIEW') {
            //Disable all
            fScreenMode._ClearMode();
            $('#btnWFD02115Close').prop('disabled', false);

            if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
                $('.aeconly').prop('disabled', true);
                $('.aeconly').show();
                $('#DPRE_MANI_2').show();
            } else {
                $('.aeconly').prop('disabled', false);
                $('.aeconly').hide();
                $('#DPRE_MANI_2').hide();
            }

            if (GLOBAL.IsAECManager == "Y" || GLOBAL.IsAEC == 'Y') {
                if (fAction.isLeasingOrRightOfUse()) {
                    $('.Leasing-control').show(true);
                    $('.Leasing-control').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
                } else {
                    $('.Leasing-control').hide(true);
                    $('.Leasing-control').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
                }

            }
        }

        //############# RMA, AUC, Settlement ##################
        if (mode == 'NEW_RMA' || mode == 'NEW_AUC' || mode == 'NEW_SETTLE') {
            //header panel
            $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);

            //body panel
            $('#WFD02115_ASSET_GROUP').prop('disabled', true);
            //CR 2019/12/16 Disable Response Cost Center
            $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);

            if (mode === 'NEW_RMA') {
                $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
            } else if (mode == 'NEW_SETTLE') {
                $('#WFD02115_WBS_BUDGET').prop('disabled', true);

                $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
                $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', true);
                $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', true);

                $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', false);
            }

        } else if (mode == 'EDIT_RMA' || mode == 'EDIT_AUC' || mode == 'EDIT_SETTLE') {
            //header panel
            $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);

            //body panel
            $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', $('#WFD02115_ASSET_MAIN_SUB').val() == "MAIN");
            if (mode == 'EDIT_SETTLE' || $('#WFD02115_ASSET_GROUP').val() != "AUC") {
                $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
            }

            //CR 2019/12/16 Disable Response Cost Center
            $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);

            if (mode === 'NEW_RMA') {
                $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
            } else if (mode == 'EDIT_SETTLE') {
                $('#WFD02115_WBS_BUDGET').prop('disabled', true);
                $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', true);
                $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', true);

                $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', false);
            }

            $('#WFD02115_ASSET_CLASS').prop('disabled', $('#WFD02115_ASSET_MAIN_SUB').val() != "MAIN");

            if (fAction.isLeasingOrRightOfUse()) {
                $('#divLeasingInfo').show(true);
                $('#divLeasingInfo').find('input,select').prop('disabled', false);
                if (GLOBAL.IsAEC == 'Y') {
                    $('.Leasing-control').show(true);
                    $('.Leasing-control').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
                }
            } else {
                $('#divLeasingInfo').hide(true);
                $('#divLeasingInfo').find('input,select').prop('disabled', true);

                if (GLOBAL.IsAEC == 'Y') {
                    $('.Leasing-control').hide(true);
                    $('.Leasing-control').find('input,select').prop('disabled', true);
                }
            }
        }

        //############# Info Change ##################
        if (mode == 'NEW_INFO_CHG') {
            fScreenMode._ClearMode();
            $('#btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);
        } else if (mode == 'EDIT_INFO_CHG') {
            $('#WFD02115_WBS_PROJECT').prop('disabled', true);
            $('#WFD02115_LOCATION').prop('disabled', true);
            $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
            $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', true);
            $('#WFD02115_ASSET_GROUP, #WFD02115_INVEN_NO, #WFD02115_ROOM').prop('disabled', true);
            $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);
            $('#WFD02115_ASSET_CLASS').prop('disabled', true);
            $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);

            //CUMU_ACQU_PRDCOST_01 = 0 enable
            //$('#WFD02115_WBS_BUDGET').prop('disabled', Params.CUMU_ACQU_PRDCOST_01 != 0);
            //$('#WFD02115_WBS_BUDGET').prop('disabled', true);
            //$('#WFD02115_BASE_UOM').prop('disabled', Params.CUMU_ACQU_PRDCOST_01 != 0);
            $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', true);

            $('#WFD02115_COST_CODE, #WFD02115BtnCOST_CODE').prop('disabled', true);
            $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);//CR 2019/12/16 Disable Response Cost Center
            $('#WFD02115_DEACT_DEPRE_AREA_15').prop('disabled', true);//CR 2019/12/27 Disable Deactive 15

            if (Params.CUMU_ACQU_PRDCOST_01 == 0) {

                $('#WFD02115_WBS_BUDGET').prop('disabled', false);
                $('#WFD02115_BASE_UOM').prop('disabled', true);

                if ($('#WFD02115_ASSET_GROUP').val() == 'RMA') {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
                    $('#WFD02115_BASE_UOM').prop('disabled', true);
                } else {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);
                    $('#WFD02115_BASE_UOM').prop('disabled', false);
                }
            } else {
                if ($('#WFD02115_ASSET_GROUP').val() == 'RMA') {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
                } else if ($('#WFD02115_ASSET_GROUP').val() == 'AUC') {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);
                }
                $('#WFD02115_WBS_BUDGET').prop('disabled', true);
                $('#WFD02115_BASE_UOM').prop('disabled', true);
            }

            if (fAction.isLeasingOrRightOfUse()) {
                $('#divLeasingInfo').show(true);
                $('#divLeasingInfo').find('input,select').prop('disabled', false);
                if (GLOBAL.IsAEC == 'Y') {
                    $('.Leasing-control').show(true);
                    $('.Leasing-control').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
                }
            } else {
                $('#divLeasingInfo').hide(true);
                $('#divLeasingInfo').find('input,select').prop('disabled', true);

                if (GLOBAL.IsAEC == 'Y') {
                    $('.Leasing-control').hide(true);
                    $('.Leasing-control').find('input,select').prop('disabled', true);
                }
            }

        } else if (mode == 'EDIT_INFO_CHG_AFTER_SUBMIT') {
            //header panel
            $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);

            $('#btnGetExistsAssetInfo').prop('disabled', false);

            $('#WFD02115_WBS_PROJECT').prop('disabled', true);
            $('#WFD02115_LOCATION').prop('disabled', true);

            //$('#WFD02115_WBS_BUDGET').prop('disabled', true);
            $('#WFD02115_ASSET_GROUP').prop('disabled', true);
            $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', true);
            $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', true);

            $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);//CR 2019/12/16 Disable Response Cost Center
            $('#WFD02115_DEACT_DEPRE_AREA_15').prop('disabled', true);//CR 2019/12/27 Disable Deactive 15

            if (Params.CUMU_ACQU_PRDCOST_01 == 0) {

                $('#WFD02115_WBS_BUDGET').prop('disabled', false);
                $('#WFD02115_BASE_UOM').prop('disabled', true);

                if ($('#WFD02115_ASSET_GROUP').val() == 'RMA') {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
                } else {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);
                }
            } else {
                if ($('#WFD02115_ASSET_GROUP').val() == 'RMA') {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
                } else if ($('#WFD02115_ASSET_GROUP').val() == 'AUC') {
                    $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);
                }
                $('#WFD02115_WBS_BUDGET').prop('disabled', true);
                $('#WFD02115_BASE_UOM').prop('disabled', true);
            }

            if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
                $('.aeconly').show();
                $('#DPRE_MANI_2').show();
            } else {
                $('.aeconly').hide();
                $('#DPRE_MANI_2').hide();
            }

            if (fAction.isLeasingOrRightOfUse()) {
                $('#divLeasingInfo').show(true);
                $('#divLeasingInfo').find('input,select').prop('disabled', false);
                if (GLOBAL.IsAEC == 'Y') {
                    $('.Leasing-control').show(true);
                    $('.Leasing-control').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
                }
            } else {
                $('#divLeasingInfo').hide(true);
                $('#divLeasingInfo').find('input,select').prop('disabled', true);

                if (GLOBAL.IsAEC == 'Y') {
                    $('.Leasing-control').hide(true);
                    $('.Leasing-control').find('input,select').prop('disabled', true);
                }
            }

        }

        //############# Settlement KN ##################
        if (mode == "NEW_SETTLE_KN" || mode == "EDIT_SETTLE_KN") {
            //header panel
            $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);

            //body panel
            $('#WFD02115_WBS_BUDGET').prop('disabled', true);
            $('#WFD02115_ASSET_GROUP').prop('disabled', true);
            $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', $('#WFD02115_ASSET_MAIN_SUB').val() == "MAIN");

            $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);//CR 2019/12/16 Disable Response Cost Center

        }

        //############# Reclass ##################
        if (mode == "NEW_RECLASS") {
            //header panel
            $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);

            $('#WFD02115_ASSET_GROUP, #WFD02115_INVEN_NO, #WFD02115_ROOM').prop('disabled', true);

            $('#WFD02115_COST_CODE, #WFD02115BtnCOST_CODE').prop('disabled', true);
            $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);

            $('#WFD02115_WBS_BUDGET').prop('disabled', true);

            $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', true);
            $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', ($('#WFD02115_ASSET_MAIN_SUB').val() == "MAIN"));

            if (Params.OPTION !== "E") {
                //WHEN NEW MODE AND WBS BUDGET IS EMPTY
                var wbsBudget = $('#WFD02115_WBS_BUDGET').val();
                //var wbsBudget = $('#WFD02115_WBS_BUDGET').val();
                if (!wbsBudget && Params.IS_FIRST_ASSET) {//ONLY FIRST NEW LINE
                    $('#WFD02115_COST_CODE, #WFD02115BtnCOST_CODE').prop('disabled', false);
                }
                if (Params.IS_FIRST_ASSET) {
                    $('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', false);
                }
            }
        }
    },
    autoFillDpreValue: function () {
        $('#WFD02115_LEASE_LENGTH_YEAR').change(function () {
            if (fAction.isLeasingOrRightOfUse()) {
                if ($('#WFD02115_USEFUL_LIFE_TYPE_01').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_YEAR_01').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_15').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_YEAR_15').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_31').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_YEAR_31').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_41').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_YEAR_41').val($(this).val());
                //if (fAction.isLeasingOrRightOfUse()) {
                if ($('#WFD02115_USEFUL_LIFE_TYPE_81').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_YEAR_81').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_91').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_YEAR_91').val($(this).val());
                //}
            }
        });
        $('#WFD02115_LEASE_LENGTH_PERD').change(function () {
            if (fAction.isLeasingOrRightOfUse()) {
                if ($('#WFD02115_USEFUL_LIFE_TYPE_01').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_PERD_01').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_15').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_PERD_15').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_31').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_PERD_31').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_41').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_PERD_41').val($(this).val());
                //if (fAction.isLeasingOrRightOfUse()) {
                if ($('#WFD02115_USEFUL_LIFE_TYPE_81').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_PERD_81').val($(this).val());
                if ($('#WFD02115_USEFUL_LIFE_TYPE_91').val() == DPRE_TYPE.BC)
                    $('#WFD02115_PLAN_LIFE_PERD_91').val($(this).val());
                //}
            }
        });

        if (GLOBAL.IsAEC != "Y") {

            $('#WFD02115_DPRE_KEY_01').change(function () {
                if (fAction.isOutsourceTools()) {
                    $('#WFD02115_DPRE_KEY_15').val($(this).val());
                    $('#WFD02115_DPRE_KEY_31').val($(this).val());
                    $('#WFD02115_DPRE_KEY_41').val($(this).val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        $('#WFD02115_DPRE_KEY_81').val($(this).val());
                        $('#WFD02115_DPRE_KEY_91').val($(this).val());
                    }

                    $('#WFD02115_USEFUL_LIFE_TYPE_15').val($('#WFD02115_USEFUL_LIFE_TYPE_01').val());
                    $('#WFD02115_USEFUL_LIFE_TYPE_31').val($('#WFD02115_USEFUL_LIFE_TYPE_01').val());
                    $('#WFD02115_USEFUL_LIFE_TYPE_41').val($('#WFD02115_USEFUL_LIFE_TYPE_01').val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        $('#WFD02115_USEFUL_LIFE_TYPE_81').val($('#WFD02115_USEFUL_LIFE_TYPE_01').val());
                        $('#WFD02115_USEFUL_LIFE_TYPE_91').val($('#WFD02115_USEFUL_LIFE_TYPE_01').val());
                    }

                    $('#WFD02115_PLAN_LIFE_YEAR_15').val($('#WFD02115_PLAN_LIFE_YEAR_01').val());
                    $('#WFD02115_PLAN_LIFE_YEAR_31').val($('#WFD02115_PLAN_LIFE_YEAR_01').val());
                    $('#WFD02115_PLAN_LIFE_YEAR_41').val($('#WFD02115_PLAN_LIFE_YEAR_01').val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        $('#WFD02115_PLAN_LIFE_YEAR_81').val($('#WFD02115_PLAN_LIFE_YEAR_01').val());
                        $('#WFD02115_PLAN_LIFE_YEAR_91').val($('#WFD02115_PLAN_LIFE_YEAR_01').val());
                    }

                    $('#WFD02115_PLAN_LIFE_PERD_15').val($('#WFD02115_PLAN_LIFE_PERD_01').val());
                    $('#WFD02115_PLAN_LIFE_PERD_31').val($('#WFD02115_PLAN_LIFE_PERD_01').val());
                    $('#WFD02115_PLAN_LIFE_PERD_41').val($('#WFD02115_PLAN_LIFE_PERD_01').val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        $('#WFD02115_PLAN_LIFE_PERD_81').val($('#WFD02115_PLAN_LIFE_PERD_01').val());
                        $('#WFD02115_PLAN_LIFE_PERD_91').val($('#WFD02115_PLAN_LIFE_PERD_01').val());
                    }

                    $('#WFD02115_SCRAP_VALUE_15').val($('#WFD02115_SCRAP_VALUE_01').val());
                    $('#WFD02115_SCRAP_VALUE_31').val($('#WFD02115_SCRAP_VALUE_01').val());
                    $('#WFD02115_SCRAP_VALUE_41').val($('#WFD02115_SCRAP_VALUE_01').val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        $('#WFD02115_SCRAP_VALUE_81').val($('#WFD02115_SCRAP_VALUE_01').val());
                        $('#WFD02115_SCRAP_VALUE_91').val($('#WFD02115_SCRAP_VALUE_01').val());
                    }
                }
            });

            $('#WFD02115_PLAN_LIFE_YEAR_01').change(function () {
                if (fAction.isOutsourceTools()) {
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_15').val() == DPRE_TYPE.BC)
                        $('#WFD02115_PLAN_LIFE_YEAR_15').val($(this).val());
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_31').val() == DPRE_TYPE.BC)
                        $('#WFD02115_PLAN_LIFE_YEAR_31').val($(this).val());
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_41').val() == DPRE_TYPE.BC)
                        $('#WFD02115_PLAN_LIFE_YEAR_41').val($(this).val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        if ($('#WFD02115_USEFUL_LIFE_TYPE_81').val() == DPRE_TYPE.BC)
                            $('#WFD02115_PLAN_LIFE_YEAR_81').val($(this).val());
                        if ($('#WFD02115_USEFUL_LIFE_TYPE_91').val() == DPRE_TYPE.BC)
                            $('#WFD02115_PLAN_LIFE_YEAR_91').val($(this).val());
                    }
                }
            });

            $('#WFD02115_PLAN_LIFE_PERD_01').change(function () {
                if (fAction.isOutsourceTools()) {
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_15').val() == DPRE_TYPE.BC)
                        $('#WFD02115_PLAN_LIFE_PERD_15').val($(this).val());
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_31').val() == DPRE_TYPE.BC)
                        $('#WFD02115_PLAN_LIFE_PERD_31').val($(this).val());
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_41').val() == DPRE_TYPE.BC)
                        $('#WFD02115_PLAN_LIFE_PERD_41').val($(this).val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        if ($('#WFD02115_USEFUL_LIFE_TYPE_81').val() == DPRE_TYPE.BC)
                            $('#WFD02115_PLAN_LIFE_PERD_81').val($(this).val());
                        if ($('#WFD02115_USEFUL_LIFE_TYPE_91').val() == DPRE_TYPE.BC)
                            $('#WFD02115_PLAN_LIFE_PERD_91').val($(this).val());
                    }
                }
            });

            $('#WFD02115_SCRAP_VALUE_01').change(function () {
                if (fAction.isOutsourceTools()) {
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_15').val() == DPRE_TYPE.BC)
                        $('#WFD02115_SCRAP_VALUE_15').val($(this).val());
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_31').val() == DPRE_TYPE.BC)
                        $('#WFD02115_SCRAP_VALUE_31').val($(this).val());
                    if ($('#WFD02115_USEFUL_LIFE_TYPE_41').val() == DPRE_TYPE.BC)
                        $('#WFD02115_SCRAP_VALUE_41').val($(this).val());
                    if (fAction.isLeasingOrRightOfUse()) {
                        if ($('#WFD02115_USEFUL_LIFE_TYPE_81').val() == DPRE_TYPE.BC)
                            $('#WFD02115_SCRAP_VALUE_81').val($(this).val());
                        if ($('#WFD02115_USEFUL_LIFE_TYPE_91').val() == DPRE_TYPE.BC)
                            $('#WFD02115_SCRAP_VALUE_91').val($(this).val());
                    }
                }
            });
        }
    },
}
//Set Status of Control
var fScreenMode = {
    _ClearMode: function () {

        $('.aeconly').hide();
        $('#DPRE_MANI_2').hide();
        $('#divWFD02115').find('input,select,button').prop('disabled', true);
        $('#divWFD02115Header').find('input,select,button').prop('disabled', true);

        $('.duplicaterow').hide();

    },
    SetNewMode_AssetInfoChange: function () {
        $('#btnGetExistsAssetInfo').click();
    },
    SetNewMode_AssetInfoChangeAfterGet: function (_allowEditWBS) {

        fAction.LoadWBSBudgetInfo();

        $('#divWFD02115').find('input,select').prop('disabled', false);
        $('.aeconly').prop('disabled', GLOBAL.IsAEC != "Y");
        $('.disabledalway').prop('disabled', true);

        $('#WFD02115_FINAL_ASSET_CLASS, #WFD02115_ASSET_MAIN_SUB, #WFD02115_PARENT_ASSET_NO').prop('disabled', true);

        $('#WFD02115_ASSET_GROUP, #WFD02115_INVEN_NO, #WFD02115_ROOM').prop('disabled', true);
        $('#btnWFD02115OK, #btnWFD02115Close, #btnGetExistsAssetInfo').prop('disabled', false);
        $('#WFD02115_DOCNO, #WFD02115_ITEMNO, #WFD02115_ORIGIN_ASSET_NO, #WFD02115_ORIGIN_ASSET_SUB_NO').prop('disabled', true);

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
        }

        $('#WFD02115_ASSET_CLASS').prop('disabled', true);
        $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', false);

        //$('#WFD02115_COST_CODE, #WFD02115BtnCOST_CODE').prop('disabled', true);
        //$('#WFD02115_RESP_COST_CODE, #WFD02115BtnRESPONSIBLE_COST_CENTER').prop('disabled', true);

        //CUMU_ACQU_PRDCOST_01 = 0 enable
        //$('#WFD02115_WBS_BUDGET').prop('disabled', !_allowEditWBS);
        $('#WFD02115_WBS_BUDGET').prop('disabled', true);
        $('#WFD02115_BASE_UOM').prop('disabled', !_allowEditWBS);

    },
    SetEditMode_AssetInfoChange: function (dataLoaded) {
        fScreenMode._ClearMode();

        var result = dataLoaded;
        //var result = fAction._GetParameter();

        //fAction._Mapping(result);

        //Incase Asset Info Change (Add Mode)
        if (Params.MODE == "N" && GLOBAL.REQUEST_TYPE == "G") {
            fScreenMode.SetNewMode_AssetInfoChangeAfterGet(result.CUMU_ACQU_PRDCOST_01 == 0);

            if ($('#WFD02115_ASSET_GROUP').val() == "AUC") {
                fAction._LoadAUCAssetClass(result.ASSET_CLASS);
            } else {
                fAction._LoadAssetClass(result.ASSET_CLASS);
            }

            $('#WFD02115_ASSET_CLASS').select2('val', result.ASSET_CLASS);
            $('#WFD02115_MINOR_CATEGORY').select2('val', result.MINOR_CATEGORY);

            $('#WFD02115_ASSET_MAIN_SUB').prop('disabled', true);
        }
    },

    SetAECMode_AssetInfoChange: function () {
        fScreenMode._ClearMode();
        $('#divWFD02115').find('input,select').prop('disabled', false);
        $('.disabledalway').prop('disabled', true);
        $('#WFD02115BtnRESPONSIBLE_COST_CENTER, #WFD02115BtnCOST_CODE').prop('disabled', false);

        $('#btnWFD02115OK,#btnWFD02115Close').prop('disabled', false);
    },

    SetNewMode_NewAssetRequest: function () {
        fScreenMode._ClearMode();

        if (GLOBAL.IsAEC == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
        }

        if (Params.FLOW_TYPE == "ST") {
            $('#WFD02115_ASSET_MAIN_SUB').select2('val', 'MAIN');
        }

        if (GLOBAL.REQUEST_TYPE == 'A' || GLOBAL.REQUEST_TYPE == 'K') {
            $('.duplicaterow').show();
        }
    },

    SetEditMode_NewAssetRequest: function () {
        fScreenMode._ClearMode();

        //$('#WFD02115_WBS_BUDGET').trigger('change');
        $('#WFD02115_ASSET_MAIN_SUB').trigger('change');
        $('#WFD02115_BOI').trigger('change');

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
        }

        ////Retrive WBS Balance only
        //var _data = {
        //    COMPANY_CODE: $('#WFD02115_COMPANY').val(),
        //    WBS: $('#WFD02115_WBS_BUDGET').val(),
        //};
        //ajax_method.Post(URL_CONST.WFD02115_GetDeriveWBSBudget, _data, false, function (result) {
        //    if (result == null) {
        //        return;
        //    }
        //    $('#WFD02115_WBS_BALANCE').text(commaSeparateNumber(result.AVAILABLE));
        //});
    },
    SetEditMode_NewAssetForSettlementRequest: function () {
        fScreenMode._ClearMode();

        $('#WFD02115_ASSET_MAIN_SUB').select2('val', 'MAIN');

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
        }

    },
    SetNewMode_NewAssetReclassification: function () {
        fScreenMode._ClearMode();

        $('#WFD02115_ORIGIN_ASSET_NO').val(Params.ORIGINAL_ASSET_NO);
        $('#WFD02115_ORIGIN_ASSET_SUB_NO').val(Params.ORIGINAL_ASSET_SUB);
        $('#WFD02115_COMPANY').val(Params.COMPANY);

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
        }

        ajax_method.Post(URL_CONST.WFD02115_GetFixedAssetMasterForReclass, Params, false, function (result) {
            if (result == null) {
                return;
            }
            //console.log(result);

            //Load with default
            fAction._LoadAssetClass(result.ASSET_CLASS);

            //fAction._LoadFinalAssetClass(result.FINAL_ASSET_CLASS);

            //WFD02115_WBS_BUDGET
            fAction._LoadWBSBudget(result, null);


            //LoadWBSBudgetInfo
            fAction.LoadWBSBudgetInfo();

            //WFD02115_WBS_PROJECT
            fAction._LoadWBSProject(result.WBS_PROJECT);

            //WFD02115_LOCATION
            fAction._LoadLocation(result.LOCATION);

            //WFD02115_BOI_NO
            fAction._LoadBOITaxPrivilege(result.INVEST_REASON, result.BOI_NO);

            //WFD02115_MACHINE_LICENSE
            fAction._LoadMachineLicense(result.MACHINE_LICENSE);

            $('#WFD02115_ASSET_MAIN_SUB').trigger('change');

            //fAction.Initialize(result);
            fAction._Mapping(result, true);

            //Generate Temp Asset No.
            $('#WFD02115_ASSET_NO').val("TEMP" + Params.LINE_NO);
            $('#WFD02115_ASSET_SUB').val('');

            //RESP_COST_CODE
            //COST_CODE
            $('#WFD02115_COST_CODE').val(Params.COST_CODE);
            $('#WFD02115_RESP_COST_CODE').val(Params.RESP_COST_CODE);
        })

    },
    SetEditMode_NewAssetReclassification: function () {
        fScreenMode._ClearMode();

        $('#WFD02115_ORIGIN_ASSET_NO').val(Params.ORIGINAL_ASSET_NO);
        $('#WFD02115_ORIGIN_ASSET_SUB_NO').val(Params.ORIGINAL_ASSET_SUB);
        $('#WFD02115_COMPANY').val(Params.COMPANY);

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();
        }

        ajax_method.Post(URL_CONST.WFD02115_GetFixedAssetMasterForReclass, Params, false, function (result) {
            if (result == null) {
                return;
            }
            //console.log(result);

            //Load with default
            fAction._LoadAssetClass(result.ASSET_CLASS);

            //fAction._LoadFinalAssetClass(result.FINAL_ASSET_CLASS);

            //WFD02115_WBS_BUDGET
            fAction._LoadWBSBudget(result, null);

            //LoadWBSBudgetInfo
            fAction.LoadWBSBudgetInfo();

            //WFD02115_WBS_PROJECT
            fAction._LoadWBSProject(result.WBS_PROJECT);

            //WFD02115_LOCATION
            fAction._LoadLocation(result.LOCATION);

            //WFD02115_BOI_NO
            fAction._LoadBOITaxPrivilege(result.INVEST_REASON, result.BOI_NO);

            //WFD02115_MACHINE_LICENSE
            fAction._LoadMachineLicense(result.MACHINE_LICENSE);

            $('#WFD02115_ASSET_MAIN_SUB').trigger('change');

            //fAction.Initialize(result);
            fAction._Mapping(result);

            //Generate Temp Asset No.
            $('#WFD02115_ASSET_NO').val("TEMP" + Params.LINE_NO);
            $('#WFD02115_ASSET_SUB').val('');
        })

    },
    SetNewMode_NewAssetSettlement: function () {
        fScreenMode._ClearMode();
        $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');

        $('#WFD02115_ORIGIN_ASSET_NO').val(Params.ORIGINAL_ASSET_NO);
        $('#WFD02115_ORIGIN_ASSET_SUB_NO').val(Params.ORIGINAL_ASSET_SUB);
        $('#WFD02115_COMPANY').val(Params.COMPANY);

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();

        }

        ajax_method.Post(URL_CONST.WFD02115_GetWBSData, { WBS: Params.WBS_BUDGET, COMPANY_CODE: Params.COMPANY }, false, function (result) {
            if (result == null) {
                return;
            }

            fAction._LoadAssetClass(Params.ASSET_CLASS);

            fAction._LoadFinalAssetClass(null);

            //WFD02115_WBS_BUDGET
            //result.WBS_BUDGET = Params.WBS_BUDGET; //Set default as WBS_BUDGET from query string (URL)
            //fAction._LoadWBSBudget(result, null);

            $('#WFD02115_WBS_BUDGET').append($("<option />")
                .attr('data-description', result.WBS_DESCRIPTION)
                .val(Params.WBS_BUDGET)
                .text(Params.WBS_BUDGET + ' - ' + result.WBS_DESCRIPTION));

            $('#WFD02115_WBS_BUDGET').select2('val', Params.WBS_BUDGET);
            $('#WFD02115_WBS_BUDGET_NAME').text(result.WBS_DESCRIPTION);

            //LoadWBSBudgetInfo
            fAction.LoadWBSBudgetInfo('RMA');

            //WFD02115_WBS_PROJECT
            fAction._LoadWBSProject(null);

            //WFD02115_LOCATION
            fAction._LoadLocation(null);

            //WFD02115_BOI_NO
            fAction._LoadBOITaxPrivilege(null, null);

            //WFD02115_MACHINE_LICENSE
            fAction._LoadMachineLicense(null);

            //$('#WFD02115_ASSET_MAIN_SUB').trigger('change');

            //fAction.Initialize(result);
            //fAction._Mapping(result);

            //Generate Temp Asset No.
            $('#WFD02115_ASSET_NO').val("TEMP" + Params.NEW_LINE_NO);
            //$('#WFD02115_ASSET_SUB').val('');

        });

    },
    SetEditMode_NewAssetSettlement: function () {
        fScreenMode._ClearMode();
        $('#WFD02115_FINAL_ASSET_CLASS').select2('val', '');

        $('#WFD02115_ASSET_NO').val(Params.ORIGINAL_ASSET_NO);
        $('#WFD02115_ASSET_SUB_NO').val(Params.ORIGINAL_ASSET_SUB);
        $('#WFD02115_COMPANY').val(Params.COMPANY);

        if (GLOBAL.IsAEC == "Y" || GLOBAL.IsAECManager == "Y") {
            $('.aeconly').show();
            $('#DPRE_MANI_2').show();

            if (!fAction.isLeasingOrRightOfUse()) {
                $('.Leasing-control').hide(true);
            }
        }
    }
}

$('#WFD02115_WBS_PROJECT').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    $('#WFD02115_WBS_PROJECT_NAME').text(desc);
});

$('#btnWFD02115OK').click(function (e) {

    e.preventDefault();

    $('#btnWFD02115OK').prop('disabled', true);
    $('#btnWFD02115OK').prop('disabled', false);

    fAction.SaveData(function () {
        $('#btnWFD02115OK').prop('disabled', false);
        opener.LoadDataAbstract(function () {
            window.close();
        }, Params.FLOW_TYPE, window);
    });
})
//Close dialog
$('#btnWFD02115Close').click(function (e) {
    //Show confirm message to close
    e.preventDefault();
    window.close();
})

//Search existing assets
$('#btnGetExistsAssetInfo').click(function (e) {
    e.preventDefault();
    fAction.ShowSearchPopup(function () {

    });
})

$('#WFD02115BtnPARENT_ASSET_NO').click(function (e) {
    e.preventDefault();

    fAction.ShowSearchParentPopup(function () {

    });
});
$('#WFD02115BtnCOST_CODE').click(function () {

    $('#SearchCostCenter').modal('show');
    CostControls.IsSearchRespCostCode = false;

    if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "ST") {
        CostControls.COMPANY_.val(Params.COMPANY);
        URL_COMSCC.SET_INITIAL_PARAMETER = URL_COMSCC.SET_INITIAL_PARAMETER_TEMP + '?AllCostCode=N&AssetOwnerFlag=Y&PageRequest=WFD02115';
    } else if (Params.FLOW_TYPE == "CN") {
        CostControls.COMPANY_.val(Params.COMPANY);
        URL_COMSCC.SET_INITIAL_PARAMETER = URL_COMSCC.SET_INITIAL_PARAMETER_TEMP + '?AllCostCode=Y&AssetOwnerFlag=Y&PageRequest=WFD02115_CN';
    }

    COMSCC_InitialParameter();

    CostControls.callback = function (_data) {
        $('#WFD02115_COST_CODE').val(_data.COST_CODE);
        $('#WFD02115_COST_NAME').text(_data.COST_NAME);
    };

});
$('#WFD02115BtnRESPONSIBLE_COST_CENTER').click(function () {

    $('#SearchCostCenter').modal('show');
    CostControls.IsSearchRespCostCode = true;

    if (Params.FLOW_TYPE == "ST") {
        CostControls.COMPANY_.val(Params.COMPANY);
        URL_COMSCC.SET_INITIAL_PARAMETER = URL_COMSCC.SET_INITIAL_PARAMETER_TEMP + '?AllCostCode=N&AssetOwnerFlag=Y&PageRequest=WFD02115&CostCode=' + $('#WFD02115_COST_CODE').val();
    } else if (Params.FLOW_TYPE == "CN") {
        CostControls.COMPANY_.val(Params.COMPANY);
        URL_COMSCC.SET_INITIAL_PARAMETER = URL_COMSCC.SET_INITIAL_PARAMETER_TEMP + '?AllCostCode=Y&AssetOwnerFlag=Y&PageRequest=WFD02115_CN';
    }
    COMSCC_InitialParameter();

    CostControls.callback = function (_data) {
        $('#WFD02115_RESP_COST_CODE').val(_data.COST_CODE);
        $('#WFD02115_RESP_COST_NAME').text(_data.COST_NAME);
    };
});

$('#WFD02115_ASSET_CLASS').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_ASSET_CLASS_NAME').text(desc);

    if (Params.FLOW_TYPE == "RM") {
        if ($('#WFD02115_ASSET_CLASS').val() != null && $('#WFD02115_ASSET_CLASS').val() != "" && $('#WFD02115_ASSET_CLASS').val() != $('#WFD02115_WBS_ASSET_CLASS').val()) {
            $('#WFD02115_ASSET_CLASS_WARN').show();
        } else {
            $('#WFD02115_ASSET_CLASS_WARN').hide();
        }

    }

    /*
    - If Asset Class Name have value '%Right-of-use%' or '%leasing%' >> unhide Leasing Panel                              
    - Set the following to required                              
        No. Lease Payments                            
        Leasing Payment Cycle                            
        Leasing Advance Payments                            
        Lease payment  
    */
    if (fAction.isLeasingOrRightOfUse()) {
        $('#divLeasingInfo').show(true);
        $('#divLeasingInfo').find('input,select').prop('disabled', false);
        if (GLOBAL.IsAEC == 'Y') {
            $('.Leasing-control').show(true);
            $('.Leasing-control').find('input,select').not('#WFD02115_USEFUL_LIFE_TYPE_81, #WFD02115_USEFUL_LIFE_TYPE_91').prop('disabled', false);
        }
    } else {
        $('#divLeasingInfo').hide(true);
        $('#divLeasingInfo').find('input,select').prop('disabled', true);

        if (GLOBAL.IsAEC == 'Y') {
            $('.Leasing-control').hide(true);
            $('.Leasing-control').find('input,select').prop('disabled', true);
        }
    }
    if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "ST") {
        fAction.autoFillDpreValue();
    }
    if (fAction.isOutsourceTools()) {
        loadDpreKeyAutocomplete();
    } else {
        detroyDpreKeyAutocomplete();
    }
    //}

    fAction._LoadMinorCategory($('#hdMinorCategory').val());

    if ($(this).is(':disabled'))
        return;
});
$('#WFD02115_FINAL_ASSET_CLASS').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_FINAL_ASSET_CLASS_NAME').text(desc);

    fAction._LoadMinorCategory($('#hdMinorCategory').val());

});

$('#WFD02115_MINOR_CATEGORY').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_MINOR_CATEGORY_NAME').text(desc);

    if ($(this).is(':disabled') || $(this).val() == "") {
        return;
    }


    fAction.LoadDepreciation();
})
$('#WFD02115_LOCATION').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_LOCATION_NAME').text(desc);
})
$('#WFD02115_BOI_NO').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_TAX_PRIVILEGE_NAME').text(desc);
})
$('#WFD02115_MACHINE_LICENSE').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_MACHINE_LICENSE_NAME').text(desc);
})

$('#WFD02115_BASE_UOM').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_BOM_NAME').text(desc);
});
$('#WFD02115_BOI').change(function () {
    var desc = $(this).find(':selected').attr('data-description');
    desc = (desc != null) ? desc : '';
    $('#WFD02115_BOI_HINT').text(desc);

    if ($(this).is(':disabled'))
        return;

    $('#WFD02115_BOI_NO').prop('disabled', false);

    fAction._LoadBOITaxPrivilege($('#WFD02115_BOI').val(), $('#WFD02115_BOI_NO').val());
})

//Trigger Event change
$('#WFD02115_ASSET_MAIN_SUB').on("change", function () {
    try {
        if ($(this).is(':disabled'))
            return;


        $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', true);

        if ($('#WFD02115_ASSET_MAIN_SUB').val() == "") {
            return;
        }

        if (GLOBAL.REQUEST_TYPE == "A" && Params.FLOW_TYPE == "ST") {
            if ($('#WFD02115_WBS_BUDGET').val() == "") {
                $('#WFD02115_ASSET_MAIN_SUB').select2('val', "MAIN"); //Select Main
            }
        }

        $('#WFD02115_ASSET_DESC').prop('disabled', false); //enable WFD02115_ASSET_DESC

        if ($('#WFD02115_ASSET_MAIN_SUB').val() == "MAIN") {
            $('#WFD02115_PARENT_ASSET_NO').val(''); //parent
            $('#WFD02115_PARENT_ASSET_NAME').text('');
            $('#WFD02115_ASSET_CLASS').prop('disabled', false); //enable asset class

            $('#WFD02115_FINAL_ASSET_CLASS').prop('disabled', $('#WFD02115_ASSET_GROUP').val() != "AUC"); //allow edit incase class is AUC
            //$('#WFD02115_PARENT_ASSET_NO').removeClass("required");
            $('#WFD02115_PARENT_ASSET_NO').prop('required', true);

            //Enable Minor cate also incase class not empty
            if ($('#WFD02115_ASSET_GROUP').val() == "AUC") {
                $('#WFD02115_FINAL_ASSET_CLASS').trigger("change");
            } else {
                $('#WFD02115_ASSET_CLASS').trigger("change");
            }
        }
        else {
            $('#WFD02115_PARENT_ASSET_NO, #WFD02115BtnPARENT_ASSET_NO').prop('disabled', false); //enable parenet

            //$('#WFD02115_PARENT_ASSET_NO').removeClass("required").addClass("required");
            //$('#WFD02115_PARENT_ASSET_NO').prop('required', true);
        }

        //Add on 2019/12/02 BCT-IS
        if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "AU") {
            if ($('#WFD02115_ASSET_MAIN_SUB').val() == "MAIN") {


                $('#WFD02115_ASSET_CLASS').prop('disabled', false);
                //$('#WFD02115_MINOR_CATEGORY').prop('disabled', false);
            } else {
                $('#WFD02115_ASSET_CLASS').select2('val', '');
                $('#WFD02115_MINOR_CATEGORY').select2('val', '');
                $('#WFD02115_PARENT_ASSET_NO').val('');
                $('#WFD02115_PARENT_ASSET_NAME').text('');

                $('#WFD02115_ASSET_CLASS').prop('disabled', true);
                //$('#WFD02115_MINOR_CATEGORY').prop('disabled', true);
            }
        }
    } catch (e) {
        console.error(e);
    }

});

$('.numberOnly').keypress(function (e) {
    return isNumberKey(e);
});

$('.decimalOnly').keypress(function (e) {
    return isDecimalKey(e);
});

var Validation = function () {

    var bool = true;
    var messages = [];
    var index = 0;

    if (Params.FLOW_TYPE == "AU" || Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "CN") {
        if (isNullOrEmpty('WFD02115_WBS_BUDGET') && isEnabled('WFD02115_WBS_BUDGET')) {
            messages[index] = 'MSTD0031AERR : WBS (Budget) should not be empty.';
            index++;
            bool = false;
        }
    }

    if (isNullOrEmpty('WFD02115_WBS_PROJECT') && isEnabled('WFD02115_WBS_PROJECT')) {
        messages[index] = 'MSTD0031AERR : WBS Cost (Project) should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_ASSET_MAIN_SUB') && isEnabled('WFD02115_ASSET_MAIN_SUB')) {
        messages[index] = 'MSTD0031AERR : Asset Main / Sub should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_PARENT_ASSET_NO') && isEnabled('WFD02115_PARENT_ASSET_NO')) {
        messages[index] = 'MSTD0031AERR : Parent Asset No. should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_ASSET_DESC') && isEnabled('WFD02115_ASSET_DESC')) {
        messages[index] = 'MSTD0031AERR : Asset Description should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_ASSET_CLASS') && isEnabled('WFD02115_ASSET_CLASS')) {
        messages[index] = 'MSTD0031AERR : Asset Class should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_MINOR_CATEGORY') && isEnabled('WFD02115_MINOR_CATEGORY')) {
        messages[index] = 'MSTD0031AERR : Minor Category should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_COST_CODE') && isEnabled('WFD02115_COST_CODE')) {
        messages[index] = 'MSTD0031AERR : Cost Center Charge should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_RESP_COST_CODE') && isEnabled('WFD02115_RESP_COST_CODE')) {
        messages[index] = 'MSTD0031AERR : Responsible Cost Center should not be empty.';
        index++;
        bool = false;
    }

    if (Params.FLOW_TYPE == "RM" || Params.FLOW_TYPE == "ST" || Params.FLOW_TYPE == "KN" || Params.FLOW_TYPE == "CN") {
        if (isNullOrEmpty('WFD02115_LOCATION') && isEnabled('WFD02115_LOCATION')) {
            messages[index] = 'MSTD0031AERR : Location / Supplier Code should not be empty.';
            index++;
            bool = false;
        }
    }


    if (isNullOrEmpty('WFD02115_BOI') && isEnabled('WFD02115_BOI')) {
        messages[index] = 'MSTD0031AERR : BOI should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_BOI_NO') && isEnabled('WFD02115_BOI_NO')) {
        messages[index] = 'MSTD0031AERR : Tax Privilege should not be empty.';
        index++;
        bool = false;
    }

    //if (isNullOrEmpty('WFD02115_LOCATION_REMARK') && isEnabled('WFD02115_LOCATION_REMARK')) {
    //    messages[index] = 'MSTD0031AERR : Remark for Location should not be empty.';
    //    index++;
    //    bool = false;
    //}

    if (!isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_01') && isEnabled('WFD02115_PLAN_LIFE_PERD_01')
        && ($('#WFD02115_PLAN_LIFE_PERD_01').val() < 0 || $('#WFD02115_PLAN_LIFE_PERD_01').val() > 11)) {
        messages[index] = 'MSTD0000AERR : 01 Local Book : Month must between 0 - 11';
        $('#WFD02115_PLAN_LIFE_PERD_01').focus();
        index++;
        bool = false;
    }

    if (!isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_15') && isEnabled('WFD02115_PLAN_LIFE_PERD_15')
        && ($('#WFD02115_PLAN_LIFE_PERD_15').val() < 0 || $('#WFD02115_PLAN_LIFE_PERD_15').val() > 11)) {
        messages[index] = 'MSTD0000AERR : 15 Tax book : Month must between 0 - 11';
        $('#WFD02115_PLAN_LIFE_PERD_15').focus();
        index++;
        bool = false;
    }

    if (!isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_31') && isEnabled('WFD02115_PLAN_LIFE_PERD_31')
        && ($('#WFD02115_PLAN_LIFE_PERD_31').val() < 0 || $('#WFD02115_PLAN_LIFE_PERD_31').val() > 11)) {
        messages[index] = 'MSTD0000AERR : 31 Consolidation Book: Month must between 0 - 11';
        $('#WFD02115_PLAN_LIFE_PERD_31').focus();
        index++;
        bool = false;
    }

    if (!isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_41') && isEnabled('WFD02115_PLAN_LIFE_PERD_41')
        && ($('#WFD02115_PLAN_LIFE_PERD_41').val() < 0 || $('#WFD02115_PLAN_LIFE_PERD_41').val() > 11)) {
        messages[index] = 'MSTD0000AERR : 41 Management Reporting: Month must between 0 - 11';
        $('#WFD02115_PLAN_LIFE_PERD_41').focus();
        index++;
        bool = false;
    }

    if (!isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_81') && isEnabled('WFD02115_PLAN_LIFE_PERD_81')
        && ($('#WFD02115_PLAN_LIFE_PERD_81').val() < 0 || $('#WFD02115_PLAN_LIFE_PERD_81').val() > 11)) {
        messages[index] = 'MSTD0000AERR : 81 Leasing-Local : Month must between 0 - 11';
        $('#WFD02115_PLAN_LIFE_PERD_81').focus();
        index++;
        bool = false;
    }

    if (!isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_91') && isEnabled('WFD02115_PLAN_LIFE_PERD_91')
        && ($('#WFD02115_PLAN_LIFE_PERD_91').val() < 0 || $('#WFD02115_PLAN_LIFE_PERD_91').val() > 11)) {
        messages[index] = 'MSTD0000AERR : 91 Leasing-Consolidation : Month must between 0 - 11';
        $('#WFD02115_PLAN_LIFE_PERD_91').focus();
        index++;
        bool = false;
    }

    /***************** Leasing **********************/
    if (fAction.isLeasingOrRightOfUse()) {

        if (isNullOrEmpty('WFD02115_LEASE_LENGTH_YEAR') && isEnabled('WFD02115_LEASE_LENGTH_YEAR')) {
            messages[index] = 'MSTD0031AERR : Lease Length in Years should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_LEASE_LENGTH_PERD') && isEnabled('WFD02115_LEASE_LENGTH_PERD')) {
            messages[index] = 'MSTD0031AERR : Lease Length in Months should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_LEASE_NUM_PAYMENT') && isEnabled('WFD02115_LEASE_NUM_PAYMENT')) {
            messages[index] = 'MSTD0031AERR : No. Lease Payments should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_LEASE_PAY_CYCLE') && isEnabled('WFD02115_LEASE_PAY_CYCLE')) {
            messages[index] = 'MSTD0031AERR : Lease Payment Cycle should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_LEASE_ADV_PAYMENT') && isEnabled('WFD02115_LEASE_ADV_PAYMENT')) {
            messages[index] = 'MSTD0031AERR : Lease Advance Payments should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_LEASE_PAYMENT') && isEnabled('WFD02115_LEASE_PAYMENT')) {
            messages[index] = 'MSTD0031AERR : Lease payment should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_LEASE_ANU_INT_RATE') && isEnabled('WFD02115_LEASE_ANU_INT_RATE')) {
            messages[index] = 'MSTD0031AERR : Lease Annual Interest Rate should not be empty.';
            index++;
            bool = false;
        }
    }

    /***************** Dpreciation **********************/
    if (isNullOrEmpty('WFD02115_DPRE_KEY_01') && isEnabled('WFD02115_DPRE_KEY_01')) {
        messages[index] = 'MSTD0031AERR : Depreciation key of ' + DPRE_AREA.DESC_01 + ' should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_PLAN_LIFE_YEAR_01') && isEnabled('WFD02115_PLAN_LIFE_YEAR_01')) {
        messages[index] = 'MSTD0031AERR : Useful Life in Years of ' + DPRE_AREA.DESC_01 + ' should not be empty.';
        index++;
        bool = false;
    }

    if (isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_01') && isEnabled('WFD02115_PLAN_LIFE_PERD_01')) {
        messages[index] = 'MSTD0031AERR : Useful Life in Months of ' + DPRE_AREA.DESC_01 + ' should not be empty.';
        index++;
        bool = false;
    }

    if (GLOBAL.IsAEC == "Y") {
        if (isNullOrEmpty('WFD02115_DPRE_KEY_15') && isEnabled('WFD02115_DPRE_KEY_15')) {
            messages[index] = 'MSTD0031AERR : Depreciation key of ' + DPRE_AREA.DESC_15 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_PLAN_LIFE_YEAR_15') && isEnabled('WFD02115_PLAN_LIFE_YEAR_15')) {
            messages[index] = 'MSTD0031AERR : Useful Life in Years of ' + DPRE_AREA.DESC_15 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_15') && isEnabled('WFD02115_PLAN_LIFE_PERD_15')) {
            messages[index] = 'MSTD0031AERR : Useful Life in Months of ' + DPRE_AREA.DESC_15 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_DPRE_KEY_31') && isEnabled('WFD02115_DPRE_KEY_31')) {
            messages[index] = 'MSTD0031AERR : Depreciation key of ' + DPRE_AREA.DESC_31 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_PLAN_LIFE_YEAR_31') && isEnabled('WFD02115_PLAN_LIFE_YEAR_31')) {
            messages[index] = 'MSTD0031AERR : Useful Life in Years of ' + DPRE_AREA.DESC_31 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_31') && isEnabled('WFD02115_PLAN_LIFE_PERD_31')) {
            messages[index] = 'MSTD0031AERR : Useful Life in Months of ' + DPRE_AREA.DESC_31 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_DPRE_KEY_41') && isEnabled('WFD02115_DPRE_KEY_41')) {
            messages[index] = 'MSTD0031AERR : Depreciation key of ' + DPRE_AREA.DESC_41 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_PLAN_LIFE_YEAR_41') && isEnabled('WFD02115_PLAN_LIFE_YEAR_41')) {
            messages[index] = 'MSTD0031AERR : Useful Life in Years of ' + DPRE_AREA.DESC_41 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_41') && isEnabled('WFD02115_PLAN_LIFE_PERD_41')) {
            messages[index] = 'MSTD0031AERR : Useful Life in Months of ' + DPRE_AREA.DESC_41 + ' should not be empty.';
            index++;
            bool = false;
        }

        if (fAction.isLeasingOrRightOfUse()) {
            if (isNullOrEmpty('WFD02115_DPRE_KEY_81') && isEnabled('WFD02115_DPRE_KEY_81')) {
                messages[index] = 'MSTD0031AERR : Depreciation key of ' + DPRE_AREA.DESC_81 + ' should not be empty.';
                index++;
                bool = false;
            }

            if (isNullOrEmpty('WFD02115_PLAN_LIFE_YEAR_81') && isEnabled('WFD02115_PLAN_LIFE_YEAR_81')) {
                messages[index] = 'MSTD0031AERR : Useful Life in Years of ' + DPRE_AREA.DESC_81 + ' should not be empty.';
                index++;
                bool = false;
            }

            if (isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_81') && isEnabled('WFD02115_PLAN_LIFE_PERD_81')) {
                messages[index] = 'MSTD0031AERR : Useful Life in Months of ' + DPRE_AREA.DESC_81 + ' should not be empty.';
                index++;
                bool = false;
            }

            if (isNullOrEmpty('WFD02115_DPRE_KEY_91') && isEnabled('WFD02115_DPRE_KEY_91')) {
                messages[index] = 'MSTD0031AERR : Depreciation key of ' + DPRE_AREA.DESC_91 + ' should not be empty.';
                index++;
                bool = false;
            }

            if (isNullOrEmpty('WFD02115_PLAN_LIFE_YEAR_91') && isEnabled('WFD02115_PLAN_LIFE_YEAR_91')) {
                messages[index] = 'MSTD0031AERR : Useful Life in Years of ' + DPRE_AREA.DESC_91 + ' should not be empty.';
                index++;
                bool = false;
            }

            if (isNullOrEmpty('WFD02115_PLAN_LIFE_PERD_91') && isEnabled('WFD02115_PLAN_LIFE_PERD_91')) {
                messages[index] = 'MSTD0031AERR : Useful Life in Months of ' + DPRE_AREA.DESC_91 + ' should not be empty.';
                index++;
                bool = false;
            }
        }
    }

    if (!bool) {

        $('#sumValidation').html('');
        var text = '';
        for (var i = 0; i < messages.length; i++) {
            text = text + '<li><i class="fa fa-times-circle" aria-hidden="true"></i>' + messages[i] + '</li>';
        }
        $('#AlertMessageAreaWFD02115').html('<div id="" class="message-area"><div class="alert"><ul>' + text + '</ul></div></div>');
        $('#AlertMessageAreaWFD02115').show(true);
        window.scrollTo(0, 0);
        //$('#btnGetExistsAssetInfo').focus();
        return bool;
    }
    return true;
}
var currencyFormat = function (num) {
    return (
        num
            .toFixed(2) // always two decimal digits
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    ) // use . as a separator
}
var commaSeparateNumber = function (val) {
    //if (val != null) {
    //    val = currencyFormat(val);
    //while (/(\d+)(\d{3})/.test(val.toString())) {
    //    val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    //}

    //}
    //return val;
    if (val != null) {
        return parseFloat(val).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } else {
        return '';
    }
}

var isNumberKey = function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
var isDecimalKey = function isDecimalKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

var getUsefulLifeTypeDesc = function getUsefulLifeTypeDesc(val) {
    var ret = "";
    if (val == "BC") {
        ret = DPRE_TYPE.BC;
    } else if (val == "VL") {
        ret = DPRE_TYPE.VL;
    } else {
        ret = DPRE_TYPE.NA;
    }
    return ret;
}

var isNullOrEmpty = function isNullOrEmpty(id) {
    return ($('#' + id).val() == null || $('#' + id).val() == '');
}

var isEnabled = function isEnabled(id) {
    return ($('#' + id).prop('disabled') == false);
}

var loadDpreKeyAutocomplete = function () {
    var depreKey = [];
    $.post(URL_CONST.WFD02115_GetDepreciationAutocomplete, { _company: GLOBAL.COMPANY }, function (datas) {
        if (datas != null) {
            for (var i = 0; i < datas.ObjectResult.length; i++) {
                var data = datas.ObjectResult[i];
                var dpre = new Object();
                dpre.key = data.CODE;
                dpre.name = data.NAME;
                if (data.VALUE != null) {
                    var tmp = data.VALUE.split("|");
                    dpre.depreKey = getUsefulLifeTypeDesc(tmp[0]);
                    dpre.year = tmp[1];
                    dpre.month = tmp[2];
                    dpre.scrap = tmp[3];
                    dpre.deact15 = tmp[4];
                    //dpre.physicalCheck = tmp[5];
                    //dpre.plateType = tmp[6];
                    //dpre.plateSize = tmp[7];
                } else {
                    dpre.depreKey = getUsefulLifeTypeDesc('NA');
                    dpre.year = '';
                    dpre.month = '';
                    dpre.scrap = '';
                    dpre.deact15 = '';
                    //dpre.physicalCheck = '';
                    //dpre.plateType = '';
                    //dpre.plateSize = '';
                }
                depreKey.push(dpre);
            }
        }
    });


    $('#WFD02115_DPRE_KEY_01, #WFD02115_DPRE_KEY_15, #WFD02115_DPRE_KEY_31, #WFD02115_DPRE_KEY_41, #WFD02115_DPRE_KEY_81, #WFD02115_DPRE_KEY_91').typeahead({
        highlight: true,
        //showHintOnFocus: true,
        //autoSelect: true,
        minLength: 0,
        source: function (query, process) {
            var depreKeyLists = [];

            for (i = 0; i < depreKey.length; i++) {
                var displayValue = depreKey[i].key + ' - ' + depreKey[i].name;
                depreKey[i].displayValue = displayValue;
                depreKey[i].objectId = $(this).html();
                //WFD02115_PLAN_LIFE_YEAR_01
                depreKeyLists.push(displayValue);
            }

            process(depreKeyLists);
        },
        updater: function (item) {

            for (i = 0; i < depreKey.length; i++) {
                if (item === depreKey[i].displayValue) {
                    var dpreArea = $('#' + $(this)[0].$element[0].id).data('dpreArea');;
                    if (dpreArea != null) {
                        $('#WFD02115_USEFUL_LIFE_TYPE_' + dpreArea).val(depreKey[i].depreKey);
                        $('#WFD02115_PLAN_LIFE_YEAR_' + dpreArea).val(depreKey[i].year);
                        $('#WFD02115_PLAN_LIFE_PERD_' + dpreArea).val(depreKey[i].month);
                        $('#WFD02115_SCRAP_VALUE_' + dpreArea).val(depreKey[i].scrap);

                        //if (dpreArea == "01") {
                        //    $('#WFD02115_INVEN_INDICATOR').prop("checked", (depreKey[i].physicalCheck == 'Y' ? true : false));
                        //    $('#WFD02115_PLATE_TYPE').select2('val', depreKey[i].plateType);
                        //    $('#WFD02115_BARCODE_SIZE').select2('val', depreKey[i].plateSize);
                        //}
                        if (Params.FLOW_TYPE != "GN") {
                            if (dpreArea == "15") {
                                $('#WFD02115_DEACT_DEPRE_AREA_15').prop("checked", (depreKey[i].deact15 == 'Y' ? true : false));

                            }
                        }
                    }
                    return depreKey[i].key;
                }
            }
            return item;
        }
    }).on('focus', function () {
        var $this = $(this);
        var val = $this.val();
        if (!val) {
            $this.typeahead('lookup');
        }
    });
};

var detroyDpreKeyAutocomplete = function () {
    $('#WFD02115_DPRE_KEY_01, #WFD02115_DPRE_KEY_15, #WFD02115_DPRE_KEY_31, #WFD02115_DPRE_KEY_41, #WFD02115_DPRE_KEY_81, #WFD02115_DPRE_KEY_91').typeahead('destroy');
};

var strToFloat = function (txt) {
    if (txt) {
        return parseFloat(txt);
    }
    return '';
};