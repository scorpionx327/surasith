﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas.Config
{
    public class BatchProcess
    {
        public static string TestBatch = @"E:\_Fujitsu Test program\CommonBatch\CommonBatch.exe";
        public static string BatchProcessName = "STMAssetCMBatch";
    }

   
}
public class BatchProcessList
{
    public static int getUserAppId(string UserId)
    {
        var datas = batches.Where(m => m.UserId == UserId).First();
        return datas.AppId;
    }
    public static bool isThereUserBatchProcessing(string UserID)
    {
        if (batches.Where(m => m.UserId == UserID).Count() > 0) return true;
        else return false;
    }
    public static List<BatchUserModel> batches { get; set; }
}