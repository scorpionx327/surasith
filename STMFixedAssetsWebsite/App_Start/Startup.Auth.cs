﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Net.Client.SC2;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

namespace th.co.toyota.stm.fas
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app, Sc2Options sc2Opts)
        {
            //Set the Anti Forgery to Name
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;

            //Enable default authentication chain
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            //Enables SC2
            app.UseCasAuthentication(sc2Opts);

            //Set the default authentication
            //[Local comment], [IFT, BCT, UT, Prod Uncomment] #CheckDeploy
            string deployProfile = ConfigurationManager.AppSettings["deployProfile"];
            if (!"Offline".Equals(deployProfile) && !"PU".Equals(deployProfile))
            {
                AuthenticateAllRequests(app, "SC2");
            }
            

    }

        private static void AuthenticateAllRequests(IAppBuilder app, params string[] authenticationTypes)
        {
            app.Use((context, continuation) =>
            {
                if (context.Authentication.User != null &&
                    context.Authentication.User.Identity != null &&
                    context.Authentication.User.Identity.IsAuthenticated)
                {
                    return continuation();
                }
                else
                {
                    context.Authentication.Challenge(authenticationTypes);
                    return Task.Delay(0);
                }
            });
        }
    }
}