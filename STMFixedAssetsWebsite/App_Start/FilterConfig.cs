﻿using System.Web;
using System.Web.Mvc;
using Net.Client.SC2;
using Net.Client.SC2.Filter;

namespace th.co.toyota.stm.fas
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters, Sc2Options sc2Opts)
        {
            filters.Add(new HandleErrorAttribute());

            //SC2
            //Add for Global authorization check..use [AllowAnonymous] on controller to whitelist
            if (!sc2Opts.UseSc2Offline)
            {
                filters.Add(new System.Web.Mvc.AuthorizeAttribute());
            }
            filters.Add(new SC2Filter(sc2Opts));
        }
    }
}
