﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Net.Client.SC2;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

[assembly: OwinStartupAttribute(typeof(th.co.toyota.stm.fas.Startup))]
namespace th.co.toyota.stm.fas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //START: SC2
            Sc2Options sc2AuthenticationOptions = new Sc2Options()
            {
                //[Local, IFT, BCT, UT https://sc2-dev.tmap-em.toyota-asia.com/cas], [Prod https://sc2.tmap-em.toyota-asia.com/cas #CheckDeploy
                Sc2CasServerUrlBase = "https://sc2-dev.tmap-em.toyota-asia.com/cas"
            };

            ConfigureAuth(app, sc2AuthenticationOptions);
            //END: SC2
        }
       
    }
}