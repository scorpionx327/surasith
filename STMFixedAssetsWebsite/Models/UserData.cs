﻿using Net.Client.SC2;
using Net.Client.SC2.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models;
using th.co.toyota.stm.fas.Models.Common;

namespace th.co.toyota.stm.fas
{
    public class GlobalUser
    {

        public static void SetOfflineSession_offline(string _userName)
        {
            var _usr = (new DAO.WFD01210DAO()).GetEmployeeByEmpCodeDAO(
            new Models.WFD01210.WFD01210SearchModel { EMP_CODE = _userName }); //3747 0181 0339 4311 0427 3747 0641 0338 0181
            


            //# CheckDeploy mrx pkhonnoy 3747 1542 1784  1942 4389 4246 5115258 2180 4253



 
            Sc2Options sc2VerificationOptions = new Sc2Options()
            {
                Sc2MainServerUrlBase = "http://localhost:8082/main",
                AppSystemDn = "sc2-dev.tmap-em.toyota.co.th.asia",
                UseSc2Authorization = false,
                UseSc2Offline = true,



                offlineUser = new UserInfo()
                {
                    UserId = _usr.SYS_EMP_CODE,
                    FirstName = _usr.EMP_NAME,
                    LastName = string.Format("{0}({1})",_usr.EMP_LASTNAME, _usr.ROLE),
                    Company = _usr.COMPANY,
                    CompanyType = "supplier",
                    Department = "Gaming Services",
                    Division = "BC",
                    Email = "yulan@support.pennypop.com",
                    EmployeeNo = "0728",
                    Location = "SF",
                    Section = "",
                    TelephoneNumber = "9111111",
                    Title = "Mr.",
                    Region = "America",
                    Country = "US",
                    Language = "en_us",
                    UserAlias = "uten S."
                },

                offlineAcl = new AccessControlList()
                {
                    SystemDn = "fd1.tmap-em.toyota.co.th.asia",
                    SystemUrl = "http://localhost",
                    MapScreenAcl = new List<KeyValuePair<AccessControlScreen, string>>() {
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01110",
                                                ScreenId = "WFD01110",
                                                Group = "WFD011",
                                                SubGroup = "FD011",
                                                ScreenResource = ""

                                            }, "screen.aspx"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01210",
                                                ScreenId = "WFD01210",
                                                Group = "WFD0121",
                                                SubGroup = "FD012",
                                                ScreenResource = "WFD01210"

                                            }, "WFD01210"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01120",
                                                ScreenId = "WFD01120",
                                                Group = "WFD0112",
                                                SubGroup = "FD011",
                                                ScreenResource = "WFD01120"

                                            }, "WFD01120"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01130",
                                                ScreenId = "WFD01130",
                                                Group = "WFD0113",
                                                SubGroup = "FD011",
                                                ScreenResource = "WFD01130"

                                            }, "WFD01130"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01010",
                                                ScreenId = "WFD01010",
                                                Group = "WFD0101",
                                                SubGroup = "FD010",
                                                ScreenResource = "WFD01010"

                                            }, "WFD01010"),
                                        new KeyValuePair<AccessControlScreen, string>(
                                            new AccessControlScreen()
                                            {
                                                FunctionId = "WFD01010",
                                                ScreenId = "WFD01010",
                                                Group = "WFD0101",
                                                SubGroup = "FD010",
                                                ScreenResource = "WFD01010"

                                            }, "WFD01010"),
                                    },
                    MapButtonAcl = new List<KeyValuePair<AccessControlButton, int>>()
                                    {
                                        new KeyValuePair<AccessControlButton, int>(
                                            new AccessControlButton()
                                            {
                                                FunctionId = "WFD02120",
                                                ScreenId = "WFD02120",
                                                ButtonId = "WFD02120Clear",
                                                AccessRight = 3
                                            }, 1),
                                        new KeyValuePair<AccessControlButton, int>(
                                            new  AccessControlButton()
                                            {
                                                FunctionId = "WFD02120",
                                                ScreenId = "WFD02120",
                                                ButtonId = "WFD02120Search",
                                                AccessRight = 1
                                            }, 1)

                                    },
                    MapFunctionId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("XXX123", "XXX123")
                                    },

                    MapScreenId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("WFD01110", "WFD01110"),
                                        new KeyValuePair<string, string>("WFD01210", "WFD01210"),
                                        new KeyValuePair<string, string>("WFD01120", "WFD01120"),
                                        new KeyValuePair<string, string>("WFD01130", "WFD01130"),
                                        new KeyValuePair<string, string>("WFD01010", "WFD01010"),
                                        new KeyValuePair<string, string>("WFD01250", "WFD01250"),
                                        new KeyValuePair<string, string>("WFD01020", "WFD01020"),
                                        new KeyValuePair<string, string>("WFD02120", "WFD02120"),
                                        new KeyValuePair<string, string>("WFD02160", "WFD02160"),
                                        new KeyValuePair<string, string>("WFD02320", "WFD02320"),
                                        new KeyValuePair<string, string>("WFD01270", "WFD01270"),
                                        new KeyValuePair<string, string>("WFD02610", "WFD02610"),
                                        new KeyValuePair<string, string>("WFD02620", "WFD02620"),
                                        new KeyValuePair<string, string>("WFD02630", "WFD02630")
                                    },
                    MapGroupId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("XXX123Group", "XXX123Group")
                                    },
                    MapSubGroupId = new List<KeyValuePair<string, string>>()
                                    {
                                        new KeyValuePair<string, string>("XXX123SubGroup", "XXX123SubGroup")
                                    },
                    RoleList = new List<string>()
                                    {
                                        "Admin.tmap-em.toyota.co.th.asia",
                                        "Operator.tmap-em.toyota.co.th.asia"
                                    }
                }
            };
            HttpContext.Current.Session[SC2Defaults.SessionUserInfo] = sc2VerificationOptions.offlineUser;
            HttpContext.Current.Session[SC2Defaults.SessionSystemAcl] = sc2VerificationOptions.offlineAcl;

        }
        public static void SetSC2Session()
        {
            Log4NetFunction log4net = new Log4NetFunction();
            UserPrincipleBO _userBO = new UserPrincipleBO();
            UserPrinciple usersc2 = new UserPrinciple();
            CommonBO commonBO = new CommonBO();

            if ((HttpContext.Current.Session != null) && (HttpContext.Current.Session[SC2Defaults.SessionUserInfo] != null))
            {
                usersc2 = (UserPrinciple)HttpContext.Current.Session[Constants.SESSION_USER_PROFILE];
                if (usersc2 == null)
                {
                    //[Local Uncomment], [IFT, BCT, UT, Prod Comment] #CheckDeploy
                    if (!commonBO.IsSc2OnlineMode())
                    {
                        SetOfflineSession_offline("STM.1542");
                    }

                    usersc2 = new UserPrinciple();
                    usersc2.User = new UserInfo();
                    usersc2.Acl = new AccessControlList();

                    UserInfo userInfo = new UserInfo();
                    AccessControlList _acl = new AccessControlList();
                    if (HttpContext.Current.Session[SC2Defaults.SessionUserInfo] != null)
                    {
                        userInfo = (UserInfo)HttpContext.Current.Session[SC2Defaults.SessionUserInfo];
                        if (userInfo != null)
                        {
                            
                            usersc2.User = (UserInfo)userInfo;

                        }

                    }
                    if (HttpContext.Current.Session[SC2Defaults.SessionSystemAcl] != null)
                    {
                        _acl = (AccessControlList)HttpContext.Current.Session[SC2Defaults.SessionSystemAcl];
                        if (_acl != null)
                            usersc2.Acl = (AccessControlList)_acl;
                    }
                    
                    if ((HttpContext.Current.Session[SC2Defaults.SessionUserInfo] != null) && (HttpContext.Current.Session[SC2Defaults.SessionSystemAcl] != null))
                    {
                        if (commonBO.IsSc2OnlineMode())
                        {
                            usersc2.Company = UserPrincipleBO.GetCompanyFromSC2(usersc2.User.Location);
                            usersc2.TFASTEmployeeNo = string.Format("{0}.{1}", usersc2.Company, usersc2.User.EmployeeNo);

                            var _usr = (new DAO.WFD01210DAO()).GetEmployeeByEmpCodeDAO(new Models.WFD01210.WFD01210SearchModel { EMP_CODE = usersc2.TFASTEmployeeNo }); //3747 0181 0339 4311 0427 3747 0641 0338 0181
                            if (_usr == null)
                            {
                                usersc2.IsAECUser = false;
                                log4net.WriteELogFile("GetEmployeeByEmpCodeDAO : Employee not found in system : " + usersc2.TFASTEmployeeNo);
                                //throw (new Exception("GetEmployeeByEmpCodeDAO : Employee not found in system : " + usersc2.TFASTEmployeeNo));
                            }
                            else
                            {
                                usersc2.IsAECUser = _usr.FAADMIN == "Y";
                            }
                            usersc2.IsAECManager = UserPrincipleBO.IsAECManager(usersc2.TFASTEmployeeNo); ;
                            usersc2.IsSystemAdmin = UserPrincipleBO.isSystemAdmin(usersc2.Acl.RoleList);
                            usersc2.CompanyList = UserPrincipleBO.GetCompanyList(usersc2.TFASTEmployeeNo);
                        }
                        else
                        {
                            usersc2.IsAECUser = UserPrincipleBO.IsAECUser(usersc2.User.UserId);
                            usersc2.IsSystemAdmin = UserPrincipleBO.isSystemAdmin(usersc2.Acl.RoleList);
                            usersc2.CompanyList = UserPrincipleBO.GetCompanyList(usersc2.User.UserId);
                        }

                        HttpContext.Current.Session[Constants.SESSION_USER_PROFILE] = usersc2;
                    }

                    
                }
            }
        }
        public static UserPrinciple UserSc2
        {
            set
            {
                if (HttpContext.Current.Session != null)
                {
                    UserPrinciple usersc2 = (UserPrinciple)HttpContext.Current.Session[Constants.SESSION_USER_PROFILE];
                    if (usersc2 == null)
                    {
                        SetSC2Session();
                    }
                }

            }
            get
            {
                if (HttpContext.Current.Session != null)
                {
                    UserPrinciple usersc2 = new UserPrinciple();
                    usersc2 = (UserPrinciple)HttpContext.Current.Session[Constants.SESSION_USER_PROFILE];
                    if (usersc2 == null)
                    {
                        usersc2 = new UserPrinciple();
                        SetSC2Session();
                        usersc2 = (UserPrinciple)HttpContext.Current.Session[Constants.SESSION_USER_PROFILE];
                        if (usersc2 != null)
                        {
                            return usersc2;
                        }
                    }
                    else return HttpContext.Current.Session[Constants.SESSION_USER_PROFILE] as UserPrinciple;
                }

                return null;
            }
        }
        public static bool isSc2Connected()
        {
            bool _valReturn = false;
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session[SC2Defaults.SessionUserInfo] != null)
                {
                    _valReturn = true;
                }
            }

            return _valReturn;
        }
        protected static bool isAllowScreenId(string scrId)
        {
            CommonBO commonBO = new CommonBO();
            if (!commonBO.IsSc2OnlineMode())
            {
                return true; //Test by Surasith 2019-11-04
            }
            else
            {
                var accessControlList = UserSc2.Acl;
                return accessControlList.MapScreenId.Contains(new KeyValuePair<string, string>(scrId, scrId));
            }
        }

        protected static bool isMatchedRequestUrl(string actionRef)
        {
            string requestUrl = HttpContext.Current.Request.RawUrl;
            if (actionRef.Equals(requestUrl))
            {
                return true;
            }

            return false;
        }

        public static string generateMenuGroupSingleItem(string screenId, string actionRef, string textTitle, string className)
        {
            string treeView = "treeview";
            if (isMatchedRequestUrl(actionRef))
            {
                treeView = "active treeview";
            }
            StringBuilder output = new StringBuilder();
            output.Append("<li class=\"" + treeView + "\">");
            output.Append("<a href=\""+ actionRef +"\"><i class=\"" + className + "\"></i><span>" + textTitle + "</span>");
            output.Append("</a></li>");

            if (isAllowScreenId(screenId))
            {
                return output.ToString();
            }

            return "";
        }

        private static string FindHrefs(string str)
        {
            Regex rr = new Regex("<a (.*\\s)?href=\"?'?(.*?)\"?'?(\\s.*)?>");
            Match m = rr.Match(str);
            if (m != null)
            return m.Groups[2].ToString();
            return "";
        }

        public static bool IsMatchedRequestUrlParent(string page)
        {
            var isActive = false;
            //Check Controller match
            var controller = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
            isActive = page == controller;

            return isActive;
        }

        public static string generateMenuGroup(string textTitle, string className, List<string> listItem, bool isTreeActive = false)
        {
            //bool isTreeActive = false;

            if (!isTreeActive)
            {
                foreach (var item in listItem)
                {
                    if (isMatchedRequestUrl(FindHrefs(item)))
                    {
                        isTreeActive = true;
                        break;
                    }
                }
            }

            string treeView = "treeview";
            if (isTreeActive) 
                treeView = "active treeview";

            StringBuilder output = new StringBuilder();
            output.Append("<li class=\""+ treeView  + "\">");
            output.Append("<a href=\"#\">");
            output.Append("<i class=\"" + className + "\"></i>");
            output.Append("<span style=\"z-index:1\">"+ textTitle +"</span>");
            output.Append("<span class=\"pull-right-container\"><i class=\"fa fa-angle-right pull-right\"></i></span></a>");
            output.Append("<ul class=\"treeview-menu\">");
            output.Append(string.Join("", listItem.ToArray()));
            output.Append("</ul></li>");

            if ("".Equals(string.Join("", listItem.ToArray())))
            {
                return "";
            }
            return output.ToString();
        }

        public static string generateMenuItem(string screenId, string actionRef, string textTitle, string className)
        {
            if (isAllowScreenId(screenId))
            {
                string activeClass = isMatchedRequestUrl(actionRef) ? "active" : "";
                string output = "<li class=\"" + activeClass + "\"><a href=\"" + actionRef + "\"><i class=\"" + className + "\"></i>" + textTitle + "</a></li>";

                return output;
            }

            return "";         
        }
    }
}