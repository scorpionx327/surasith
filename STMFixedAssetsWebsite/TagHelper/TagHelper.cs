﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Reflection;
using Microsoft.Security.Application;
using th.co.toyota.stm.fas.BusinessObject.Common;
using System.Configuration;

namespace th.co.toyota.stm.fas
{
    public static class TagHelper
    {
        public static MvcHtmlString TextBox(this HtmlHelper helper, string id, object value, object argument, bool Enabled = true)
        {
            bool isThereClass = false;
            var builder = new TagBuilder("input");
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(argument))
            {
                if (property.Name.Contains("class"))
                    isThereClass = true;
                builder.Attributes.Add(property.Name.Replace('_', '-'), string.Format("{0}", property.GetValue(argument)));
            }
            if (!isThereClass) builder.Attributes.Add("class", "form-control");
            builder.Attributes.Add("id", id);

            
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        }
        public enum BUTTON_TYPES
        {
            ADD, DELETE, EDIT, SAVE, CANCEL, SEARCH, CLEAR, DOWNLOAD, DOWNLOAD_ALL
                , RESEND, PRINT, EXPORT, EMAIL, DEFAULT, OK, CLOSE, UPLOAD, VIEW, SUBMIT, BACK
        }
        public static MvcHtmlString Button(this HtmlHelper helper, string id, string Message, object obj, BUTTON_TYPES type = BUTTON_TYPES.DEFAULT, bool Enable = true, string icon = "")
        {
            if (obj == null)
                return null;
            var builder = new TagBuilder("button");
            bool isThereType = false;

            builder.Attributes.Add("id", id);

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(obj))
            {
                builder.Attributes.Add(property.Name.Replace('_', '-'), string.Format("{0}", property.GetValue(obj)));
                if (property.Name.ToLower() == "type") isThereType = true;
            }

            if (!Enable)
            {
                builder.Attributes.Add("disabled", "disabled");
            }
            if (!isThereType) builder.Attributes.Add("type", "button");
            switch (type)
            {
                case BUTTON_TYPES.DEFAULT:
                    icon = string.Empty;
                    break;
                case BUTTON_TYPES.ADD:
                    icon = "plus";
                    break;
                case BUTTON_TYPES.DELETE:
                    icon = "trash";
                    break;
                case BUTTON_TYPES.EDIT:
                    icon = "edit";
                    break;
                case BUTTON_TYPES.SAVE:
                    icon = "save";
                    break;
                case BUTTON_TYPES.CANCEL:
                    icon = "remove";
                    break;
                case BUTTON_TYPES.SEARCH:
                    icon = "search";
                    break;
                case BUTTON_TYPES.CLEAR:
                    icon = "repeat";
                    break;
                case BUTTON_TYPES.DOWNLOAD:
                    icon = "download";
                    break;
                case BUTTON_TYPES.DOWNLOAD_ALL:
                    icon = "download";
                    break;
                case BUTTON_TYPES.RESEND:
                    icon = "send";
                    break;
                case BUTTON_TYPES.EMAIL:
                    icon = "envelope";
                    break;
                case BUTTON_TYPES.PRINT:
                    icon = "print";
                    break;
                case BUTTON_TYPES.EXPORT:
                    icon = "file";
                    break;
                case BUTTON_TYPES.OK:
                    icon = "check";
                    break;
                case BUTTON_TYPES.CLOSE:
                    icon = "power-off";
                    break;
                case BUTTON_TYPES.UPLOAD:
                    icon = "upload";
                    break;
                case BUTTON_TYPES.VIEW:
                    icon = "file-text-o";
                    break;
                case BUTTON_TYPES.SUBMIT:
                    icon = "check-square-o";
                    break;
                default:
                    break;
            }
            if (!string.IsNullOrEmpty(icon))
            {
                var _inner = new TagBuilder("i");
                _inner.Attributes.Add("class", string.Format("fa fa-{0}", icon));
                builder.InnerHtml = string.Format("{0} ", _inner.ToString(TagRenderMode.Normal));
            }

            builder.InnerHtml += Message;

            //Sc2 Access control button //[Local comment], [IFT, BCT, UT, Prod Uncomment]  #CheckDeploy
            CommonBO commonBO = new CommonBO();
            if (commonBO.IsSc2OnlineMode())
            {
                bool isSeeButton = UserPrincipleBO.IsAccessButton(id, GlobalUser.UserSc2.Acl);
                if (!isSeeButton)
                {

                    builder.Attributes.Add("data-visible", "false");
                }

            }

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        }
        public static MvcHtmlString WFD01170Button(this HtmlHelper helper, string id, string Message, object obj, BUTTON_TYPES type = BUTTON_TYPES.DEFAULT,string _Visible = "N")
        {
            if (obj == null)
                return null;
            var builder = new TagBuilder("button");
            bool isThereType = false;

            builder.Attributes.Add("id", id);
            
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(obj))
            {
                builder.Attributes.Add(property.Name.Replace('_', '-'), string.Format("{0}", property.GetValue(obj)));
                if (property.Name.ToLower() == "type") isThereType = true;
            }
            if(_Visible != "Y")
                builder.Attributes.Add("style", "display:none");

            string icon = string.Empty;
            if (!isThereType) builder.Attributes.Add("type", "button");
            switch (type)
            {
                case BUTTON_TYPES.DEFAULT:
                    icon = string.Empty;
                    break;
                case BUTTON_TYPES.ADD:
                    icon = "plus";
                    break;
                case BUTTON_TYPES.DELETE:
                    icon = "trash";
                    break;
                case BUTTON_TYPES.EDIT:
                    icon = "edit";
                    break;
                case BUTTON_TYPES.SAVE:
                    icon = "save";
                    break;
                case BUTTON_TYPES.CANCEL:
                    icon = "remove";
                    break;
                case BUTTON_TYPES.SEARCH:
                    icon = "search";
                    break;
                case BUTTON_TYPES.CLEAR:
                    icon = "repeat";
                    break;
                case BUTTON_TYPES.DOWNLOAD:
                    icon = "download";
                    break;
                case BUTTON_TYPES.DOWNLOAD_ALL:
                    icon = "download";
                    break;
                case BUTTON_TYPES.RESEND:
                    icon = "send";
                    break;
                case BUTTON_TYPES.EMAIL:
                    icon = "envelope";
                    break;
                case BUTTON_TYPES.PRINT:
                    icon = "print";
                    break;
                case BUTTON_TYPES.EXPORT:
                    icon = "file";
                    break;
                case BUTTON_TYPES.OK:
                    icon = "check";
                    break;
                case BUTTON_TYPES.CLOSE:                    
                    icon = "power-off";
                    break;
                case BUTTON_TYPES.UPLOAD:
                    icon = "upload";
                    break;
                case BUTTON_TYPES.VIEW:
                    icon = "file-text-o";
                    break;
                case BUTTON_TYPES.SUBMIT:
                    icon = "check-square-o";
                    break;
                default:
                    break;
            }
            if (!string.IsNullOrEmpty(icon))
            {
                var _inner = new TagBuilder("i");
                _inner.Attributes.Add("class", string.Format("fa fa-{0}", icon));
                builder.InnerHtml = string.Format("{0} ", _inner.ToString(TagRenderMode.Normal));
            }

            builder.InnerHtml += Message;

            //Sc2 Access control button //[Local comment], [IFT, BCT, UT, Prod Uncomment]  #CheckDeploy
            CommonBO commonBO = new CommonBO();
            if (commonBO.IsSc2OnlineMode())
            {
                bool isSeeButton = UserPrincipleBO.IsAccessButton(id, GlobalUser.UserSc2.Acl);
                if (!isSeeButton)
                {

                    builder.Attributes.Add("data-visible", "false");
                }
            }

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        }

        /// <summary>
        /// Generate textbox
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static MvcHtmlString CommonTextBox(this HtmlHelper helper, string id, object value = null, object attribute = null)
        {
            bool isViewMode = false;
            if (attribute != null)
            {
                PropertyInfo prop = attribute.GetType().GetProperty("isViewMode");
                if (prop != null)
                {
                    object val = prop.GetValue(attribute, null);
                    if (val != null)
                    {
                        bool mode = false;
                        if (bool.TryParse(val.ToString(), out mode))
                            isViewMode = mode;
                    }
                }
            }

            if (isViewMode)
            {
                var spanBuilder = new TagBuilder("div");
                spanBuilder.MergeAttribute("class", "label-view");
                // Akat K. 2011-08-01 : Use in case of ViewMode have to change data depend on button click
                spanBuilder.MergeAttribute("id", id);

                if (value != null)
                {
                    string txt = value.ToString();
                    string htmlEndcode = Encoder.HtmlEncode(txt);

                    // allow for BR tag (<br/>)
                    //htmlEndcode = htmlEndcode.Replace("&lt;br/&gt;", "<br/>").Replace("&lt;BR/&gt;", "BR/").Replace("&lt;br&gt;", "<br>").Replace("&lt;BR&gt;","<BR>");
                    htmlEndcode = htmlEndcode.Replace("&#10;", "<br/>");

                    if (txt.Trim() != string.Empty)
                        spanBuilder.InnerHtml = htmlEndcode;
                    else
                        spanBuilder.InnerHtml = "&nbsp;";
                }
                else
                    spanBuilder.InnerHtml = "&nbsp;";

                return MvcHtmlString.Create(spanBuilder.ToString(TagRenderMode.Normal));
            }
            else
            {
                var inputBuilder = new TagBuilder("input");
                inputBuilder.MergeAttribute("type", "text");
                inputBuilder.MergeAttribute("id", id);
                inputBuilder.MergeAttribute("name", id);

                if (value != null)
                    inputBuilder.MergeAttribute("value", value.ToString());
                if (attribute != null)
                    ControlCreator.SetHtmlTagAttribute(inputBuilder, attribute);

                return MvcHtmlString.Create(inputBuilder.ToString(TagRenderMode.SelfClosing));
            }
        }

        /// <summary>
        /// Generate textbox Title
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static MvcHtmlString CommonTextBoxTitle(this HtmlHelper helper, string id, object value = null, object attribute = null)
        {
            bool isViewMode = false;
            if (attribute != null)
            {
                PropertyInfo prop = attribute.GetType().GetProperty("isViewMode");
                if (prop != null)
                {
                    object val = prop.GetValue(attribute, null);
                    if (val != null)
                    {
                        bool mode = false;
                        if (bool.TryParse(val.ToString(), out mode))
                            isViewMode = mode;
                    }
                }
            }

            if (isViewMode)
            {
                var spanBuilder = new TagBuilder("div");
                spanBuilder.MergeAttribute("class", "label-view");
                // Akat K. 2011-08-01 : Use in case of ViewMode have to change data depend on button click
                spanBuilder.MergeAttribute("id", id);

                if (value != null)
                {
                    string txt = value.ToString();
                    string htmlEndcode = Encoder.HtmlEncode(txt);

                    // allow for BR tag (<br/>)
                    //htmlEndcode = htmlEndcode.Replace("&lt;br/&gt;", "<br/>").Replace("&lt;BR/&gt;", "BR/").Replace("&lt;br&gt;", "<br>").Replace("&lt;BR&gt;","<BR>");
                    htmlEndcode = htmlEndcode.Replace("&#10;", "<br/>");

                    if (txt.Trim() != string.Empty)
                        spanBuilder.InnerHtml = htmlEndcode;
                    else
                        spanBuilder.InnerHtml = "&nbsp;";
                }
                else
                    spanBuilder.InnerHtml = "&nbsp;";

                return MvcHtmlString.Create(spanBuilder.ToString(TagRenderMode.Normal));
            }
            else
            {
                var inputBuilder = new TagBuilder("input");
                inputBuilder.MergeAttribute("type", "text");
                inputBuilder.MergeAttribute("id", id);
                inputBuilder.MergeAttribute("name", id);
                inputBuilder.MergeAttribute("title", "");

                if (value != null)
                    inputBuilder.MergeAttribute("value", value.ToString());
                if (attribute != null)
                    ControlCreator.SetHtmlTagAttribute(inputBuilder, attribute);

                return MvcHtmlString.Create(inputBuilder.ToString(TagRenderMode.SelfClosing));
            }
        }

        /// <summary>
        /// Generate textarea
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static MvcHtmlString CommonTextArea(this HtmlHelper helper, string id, string value = null, object attribute = null)
        {
            var inputBuilder = new TagBuilder("textarea");
            inputBuilder.MergeAttribute("id", id);
            inputBuilder.MergeAttribute("name", id);

            if (value != null)
                inputBuilder.InnerHtml = value;
            if (attribute != null)
                ControlCreator.SetHtmlTagAttribute(inputBuilder, attribute);

            return MvcHtmlString.Create(inputBuilder.ToString(TagRenderMode.Normal));
        }

        //public static SelectList GetSelectList(List<DropdownListItem> listItem, object defaultValue)
        //{
        //    if (listItem == null || listItem.Count == 0)
        //    {
        //        listItem.Add(new DropdownListItem() { Code = string.Empty, Display = string.Empty });
        //    }
        //    return new SelectList(listItem,"Code","Display", defaultValue);

        //}
        //public static MvcHtmlString Submit(string Message, object obj, bool Enable = true)
        //{
        //    if (obj == null)
        //        return null;
        //    var builder = new TagBuilder("input");
        //    foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(obj))
        //    {
        //        builder.Attributes.Add(property.Name.Replace('_', '-'), string.Format("{0}", property.GetValue(obj)));
        //    }
        //    if (!Enable)
        //    {
        //        builder.Attributes.Add("disabled", "disabled");
        //    }

        //    builder.InnerHtml += Message;
        //    return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        //}

        //public static MvcHtmlString ShowMessage(Models.ModelBase model)
        //{
        //    if (model == null || model.MessageInfo == null)
        //    {
        //        return null;
        //    }
        //    if (model.MessageInfo.MessageInfoList.Count == 0 && string.IsNullOrEmpty(model.MessageInfo.MessageText))
        //        return null;

        //    MessageInfo message = model.MessageInfo;
        //    System.Text.StringBuilder _sb = new System.Text.StringBuilder();
        //    if (!string.IsNullOrEmpty(message.MessageText))
        //    {
        //        TagBuilder _tb = new TagBuilder("p");
        //        _tb.InnerHtml = message.MessageText ;
        //        _sb.AppendFormat(_tb.ToString(TagRenderMode.Normal));
        //    }

        //    foreach (var _msg in message.MessageInfoList)
        //    {
        //        TagBuilder _tb = new TagBuilder("p");
        //        _tb.InnerHtml = _msg.ErrorMessage ;
        //        _sb.AppendFormat(_tb.ToString(TagRenderMode.Normal));
        //    }




        //    var builder = new TagBuilder("div");
        //    builder.Attributes["class"] = message.MessageInfoType == MessageInfoType.Successfully ? "alert alert-success" : "alert alert-danger";
        //    builder.InnerHtml = string.Format("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button><strong>{0}</strong>", message.MessageInfoType == MessageInfoType.Successfully ? "Successfully" : "Error!");
        //    builder.InnerHtml += _sb.ToString() ;
        //    return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        //}

        //public static MvcHtmlString ShowErrorPopup(Models.ModelBase model)
        //{
        //    if (model == null || model.MessageInfo == null)
        //    {
        //        return null;
        //    }
        //    if (model.MessageInfo.MessageInfoList.Count == 0 && string.IsNullOrEmpty(model.MessageInfo.MessageText))
        //        return null;

        //    MessageInfo message = model.MessageInfo;
        //    System.Text.StringBuilder _sb = new System.Text.StringBuilder();
        //    if (!string.IsNullOrEmpty(message.MessageText))
        //    {
        //        TagBuilder _tb = new TagBuilder("p");
        //        _tb.InnerHtml = message.MessageText.Replace("\r\n","<BR>");
        //        _sb.AppendFormat(_tb.ToString(TagRenderMode.Normal));
        //    }

        //    foreach (var _msg in message.MessageInfoList)
        //    {
        //        TagBuilder _tb = new TagBuilder("p");
        //        _tb.InnerHtml = _msg.ErrorMessage.Replace("\r\n", "<BR>");
        //        _sb.AppendFormat(_tb.ToString(TagRenderMode.Normal));
        //    }

        //    if (model.MessageInfo.MessageInfoType == MessageInfoType.Successfully)
        //    {
        //        return MvcHtmlString.Create(string.Format("fnCompleteDialog(\"{0}\")", _sb.ToString()));
        //    }

        //    return MvcHtmlString.Create(string.Format("fnErrorDialog(\"Error\", \"{0}\")",_sb.ToString()));
        //}

        //public static MvcHtmlString DisplayActiveStatus(string _status)
        //{
        //    string _cls = string.Empty;
        //    switch (_status)
        //    {
        //        case "N":
        //            _cls = "label label-warning";
        //            break;
        //        default:
        //            _cls = "label label-success";
        //            break;
        //    }
        //    var builder = new TagBuilder("span");
        //    builder.Attributes["class"] = _cls;
        //    builder.InnerHtml = _status == "Y" ? "Active" : "Disable";
        //    return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));

        //}
    }
   
}