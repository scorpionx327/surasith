﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using th.co.toyota.stm.fas.BusinessObject.Common;
using th.co.toyota.stm.fas.Models.Common;
using th.co.toyota.stm.fas.BusinessObject;

namespace th.co.toyota.stm.fas
{

    public static partial class DropdownListHelper
    {
        /// <summary>
        /// Asset location combobox
        /// </summary>
        public static MvcHtmlString SystemCombobox(this HtmlHelper helper, string id, string Category, string SubCategory, bool displayValue, object attribute = null, bool includeIdx0 = true, string firstElement = null, bool isDisplayCodeWithValue = false)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(Category, SubCategory);

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = isDisplayCodeWithValue ? d.CODE + " - " + d.VALUE : displayValue ? d.CODE : d.VALUE;
                    item.Description = d.VALUE;
                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static MvcHtmlString SystemComboboxLocation(this HtmlHelper helper, string id, string Category, string SubCategory, bool displayValue, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(Category, GlobalUser.UserSc2.DefaultCompany);

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = displayValue ? d.CODE : d.VALUE;
                    item.Description = d.VALUE;
                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static string GetLabelFromSystemMaster(this HtmlHelper helper, string Category, string SubCategory, string code)
        {
            SystemBO bo = new SystemBO();
            var datas = bo.SelectSystemDatas(Category, SubCategory, code);
            string html = "";
            if (datas != null)
            {
                html = datas.VALUE;
            }
            return html;
        }

        //public static MvcHtmlString FinalAssetClassCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(null, null);

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}

        public static MvcHtmlString AssetClassFlowCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.ASSET_CLASS.ToString(), SYSTEM_SUB_CATEGORY.ASSET_CLASS.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        //public static MvcHtmlString PlateTypeComboboxAssetClassCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.ASSET_CLASS.ToString(), SYSTEM_SUB_CATEGORY.ASSET_CLASS.ToString());

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}

        //public static MvcHtmlString MachineLicenseCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.MACHINE_LICENSE.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}


        //public static MvcHtmlString MinorCategoryCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.MINOR_CATEGORY.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}

        //public static MvcHtmlString PlateTypeCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();

        //    /*
        //        TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
        //        */
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.MINOR_CATEGORY.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString()); //---->> test data

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {

        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}


        //public static MvcHtmlString LeaseTypeCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();

        //    /*
        //        TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
        //        */
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.MINOR_CATEGORY.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString()); //---->> test data

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {

        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}



        //public static MvcHtmlString BaseUnitOfMeasureCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.UOM.ToString(), null);

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}


        //public static MvcHtmlString LocationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.LOCATION.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.REMARKS))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}

        //public static MvcHtmlString TaxPrivilegeCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.BOI_NO.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas.OrderBy(m => m.CODE))
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.CODE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "All", attribute, includeIdx0);
        //}

        public static MvcHtmlString CompanyMultipleComboboxNew(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

            ItemValue<string> item;

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                bool allowCompany = GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE);

                //  if (GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE))
                //  {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = d.CODE;
                //item.Disabled = !GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE);
                item.Disabled = !allowCompany;
                item.Selected = allowCompany;
                ls.Add(item);
                //  }
            }

            var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, (ls.FindAll(x => x.Selected).Count > 1));
            return _html;

            //if (ls.FindAll(x => !x.Disabled).Count == 1)
            //{
            //    string _code = string.Format("{0}",ls.Find(x => !x.Disabled).Value);
            //    ls.Find(x => !x.Disabled).Selected = true;
            //    var _rs = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", _code, attribute, false, false);

            //    return _rs;
            //}

            //if (ls.FindAll(x => !x.Disabled).Count == 1)
            //{
            //    ls.Find(x => !x.Disabled).Selected = true;
            //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, false);
            //}

            //var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, !(ls.Count == 1));
            //return _html;
        }

        public static MvcHtmlString CompanyMultipleComboboxWFD01110(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

            ItemValue<string> item;

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
            }

            //Get Company
            var _baseCom = (new WFD01110BO().GetCompanyList(GlobalUser.UserSc2.TFASTEmployeeNo));

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                //  if (GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE))
                //  {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = d.CODE;
                item.Disabled = !_baseCom.Exists(x => x.COMPANY == d.CODE);
                ls.Add(item);
                //  }
            }
            //if (ls.FindAll(x => !x.Disabled).Count == 1)
            //{
            //    string _code = string.Format("{0}",ls.Find(x => !x.Disabled).Value);
            //    ls.Find(x => !x.Disabled).Selected = true;
            //    var _rs = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", _code, attribute, false, false);

            //    return _rs;
            //}
            if (ls.FindAll(x => !x.Disabled).Count == 1)
            {
                ls.Find(x => !x.Disabled).Selected = true;
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, false);
            }

            var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, !(ls.Count == 1));
            return _html;
        }

        public static MvcHtmlString CompanyMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

            ItemValue<string> item;

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                //  if (GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE))
                //  {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = d.CODE;
                item.Disabled = !GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE);
                ls.Add(item);
                //  }
            }
            if (ls.FindAll(x => !x.Disabled).Count == 1)
            {
                ls.Find(x => !x.Disabled).Selected = true;
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, false);
            }

            var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, !(ls.Count == 1));
            return _html;
        }


        public static MvcHtmlString CompanyDefaultMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

            ItemValue<string> item;

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "All", attribute, includeIdx0);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                //  if (GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE))
                //  {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = d.CODE;
                item.Disabled = !GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE);
                if (item.Value.ToString() == GlobalUser.UserSc2.User.Company.ToString())
                {
                    item.Selected = true;
                }
                ls.Add(item);
                //  }
            }
            if (ls.FindAll(x => !x.Disabled).Count == 1)
            {
                ls.Find(x => !x.Disabled).Selected = true;
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, false);
            }
            var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "All", attribute, includeIdx0, !(ls.Count == 1));
            return _html;
        }
        public static MvcHtmlString MinorMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.MINOR_CATEGORY.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = string.Format("{0} : {1}", d.CODE, d.VALUE);

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "NOT_SHOW", attribute, includeIdx0);
        }
        public static MvcHtmlString CompanyCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

            ItemValue<string> item;

            //Should be check permission
            //GlobalUser.UserSc2.IsFAAdmin

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "Select", attribute, includeIdx0);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                if (GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.CODE;
                    ls.Add(item);
                }

            }
            if (string.IsNullOrEmpty(GlobalUser.UserSc2.DefaultCompany) && ls.Count == 0)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "Select", attribute, false);
            }

            if (ls.Count == 1)
            {
                ls[0].Selected = true;

            }
            if (ls.Count > 1)
            {
                item = new ItemValue<string>();
                item.Value = string.Empty;
                item.Display = "Select";
                ls.Insert(0, item);
                ls[0].Selected = true;
            }
            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", GlobalUser.UserSc2.DefaultCompany, attribute, false, !(ls.Count == 1));
        }

        public static MvcHtmlString CompanyComboboxWFD011170(this HtmlHelper helper, string id, string Company, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.COMPANY.ToString());

            ItemValue<string> item;

            //Should be check permission
            //GlobalUser.UserSc2.IsFAAdmin

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "Select", attribute, includeIdx0);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                if (GlobalUser.UserSc2.CompanyList.Exists(x => x == d.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.CODE;
                    ls.Add(item);
                }
                else if (Company == d.CODE)
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.CODE;
                    ls.Add(item);
                }
            }


            if (string.IsNullOrEmpty(GlobalUser.UserSc2.DefaultCompany) && ls.Count == 0)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "Select", attribute, false);
            }

            if (ls.Count == 1)
            {
                ls[0].Selected = true;

            }
            if (ls.Count > 1)
            {
                item = new ItemValue<string>();
                item.Value = string.Empty;
                item.Display = "Select";
                ls.Insert(0, item);
                ls[0].Selected = true;
            }
            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", GlobalUser.UserSc2.DefaultCompany, attribute, false, !(ls.Count == 1));
        }


        public static MvcHtmlString DisposalPurposeReasonCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.APPROVE_FLOW.ToString(), SYSTEM_SUB_CATEGORY.DISPOSAL_PURPOSE.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString AssetLocationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.COMMON.ToString(), SYSTEM_SUB_CATEGORY.ASSET_LOCATION.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString DisposalReasonCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.APPROVE_FLOW.ToString(), SYSTEM_SUB_CATEGORY.DISPOSAL_REASON.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString RequestTypeFlowOperation(this HtmlHelper helper, string id, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.REQUEST_FLOW_TYPE.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString RoleCombobox(this HtmlHelper helper, string id, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            //var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.RESPONSIBILITY.ToString(), SYSTEM_SUB_CATEGORY.USERROLE_CODE.ToString());
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.RESPONSIBILITY.ToString(), "USERROLE_CODE");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE).ThenBy(m => m.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString ApproveRoleCombobox(this HtmlHelper helper, string id, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            DAO.SFAS.WFD01150DAO bo = new DAO.SFAS.WFD01150DAO();


            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.GetApproveRoleList();

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;
                    //item.Description = d.ROLE_TYPE;
                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString DivisionCombobox(this HtmlHelper helper, string id, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.APPROVE_FLOW.ToString(), SYSTEM_SUB_CATEGORY.APP_DIVISION.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString ApproveConditionCombobox(this HtmlHelper helper, string id, string REQUEST_FLOW_TYPE, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas("SYSTEM_CONFIG", "APPROVE_FLOW_CONDITION");
            bool enabled = true;
            datas = datas.Where(m => m.CODE == REQUEST_FLOW_TYPE).ToList();

            ItemValue<string> item;

            if (datas != null && datas.Count > 0)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.REMARKS;

                    ls.Add(item);
                }
            }
            else
            {
                enabled = false;
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0, enabled);
        }
        public static MvcHtmlString OperatorCombobox(this HtmlHelper helper, string id, object attribute = null
            , bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.APPROVE_FLOW.ToString(), SYSTEM_SUB_CATEGORY.OPERATOR.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString MassDisposalReasonCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.FAS_TYPE.ToString(), SYSTEM_SUB_CATEGORY.SUB_TYPE.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString AssetStatusCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.COMMON.ToString(), SYSTEM_SUB_CATEGORY.ASSET_STATUS.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas)
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString PlateTypeDepreciationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatasOrderByCode(SYSTEM_CATEGORY.FAS_TYPE.ToString(), SYSTEM_SUB_CATEGORY.PLATE_TYPE.ToString()); //---->> test data

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString PlateSizeDepreciationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.FAS_TYPE.ToString(), SYSTEM_SUB_CATEGORY.BARCODE_SIZE.ToString()); //---->> test data

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString PhysicalCheckDepreciationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYS_CONFIRM.ToString(), SYSTEM_SUB_CATEGORY.YN.ToString()); //---->> test data

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString Minor_CategoryeDepreciationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatasOrderByCode("EVA_GRP_1", "STM"); //---->> test data

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = string.Format("{0} : {1}", d.CODE, d.VALUE);

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString AssetClassComboboxDepreciationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatasOrderByCode(SYSTEM_CATEGORY.ASSET_CLASS.ToString(), SYSTEM_SUB_CATEGORY.ASSET_CLASS.ToString());

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.CODE + " : " + d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString RequestFlowTypeCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.SYSTEM_CONFIG.ToString(), SYSTEM_SUB_CATEGORY.REQUEST_FLOW_TYPE.ToString()); //---->> test data

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString RequestFlowTypeRoleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            /*
                TB_M_DEPRECIATION where COMPANY = [company], ASSET_CLASS = [scr].asset_class and Minor_Category = [scr].Minor_Category
                */
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.APPRV_FLOW_MASTER.ToString(), SYSTEM_SUB_CATEGORY.ROLE_CODE.ToString()); //---->> test data

            ItemValue<string> item;

            if (datas != null)
            {

                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
    }

    public static class WFD02120DropdownList
    {
        public static MvcHtmlString AssetClassCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas("ASSET_CLASS", "ASSET_CLASS");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = string.Format("{0} : {1}", d.CODE, d.VALUE);

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static MvcHtmlString AssetTypeCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas("FAS_TYPE", "ASSET_TYPE");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static MvcHtmlString PrintTagCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatasOrderByRemark("FAS_STATUS", "PRINT_STATUS");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.VALUE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static MvcHtmlString MapStatusCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatasOrderByRemark("FAS_STATUS", "PHOTO_STATUS");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.VALUE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static MvcHtmlString PhotoStatusCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatasOrderByRemark("FAS_STATUS", "MAP_STATUS");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.VALUE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }
        public static MvcHtmlString PrintLocationCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas("PRINT_CONFIG", "PRINTER_LOCATION");
            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);

        }

        public static MvcHtmlString StatusAssetCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatasOrderByRemark("FAS_STATUS", "ASSET_STATUS");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas)
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        //public static MvcHtmlString AssetGroupCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();
        //    var datas = bo.SelectSystemDatas("ASSET", "ASSET_GROUP");

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas)
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}
        public static MvcHtmlString BOICombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();

            var datas = bo.SelectSystemDatas("ASSET", "BOI");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas)
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        //public static MvcHtmlString BOICombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        //{
        //    SystemBO bo = new SystemBO();
        //    List<ItemValue<string>> ls = new List<ItemValue<string>>();

        //    var datas = bo.SelectSystemDatas("ASSET", "BOI");

        //    ItemValue<string> item;

        //    if (datas != null)
        //    {
        //        foreach (var d in datas)
        //        {
        //            item = new ItemValue<string>();
        //            item.Value = d.CODE;
        //            item.Display = d.VALUE;

        //            ls.Add(item);
        //        }
        //    }

        //    return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        //}

    }

    public static class BaseCombobox
    {
        public static MvcHtmlString GenBaseCombobox(string id, List<ItemValue<string>> ls, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
    }


    public static class WFD01120DropdownList
    {
        public static MvcHtmlString WFD01120ComBoTypeOfReport(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            WFD01120BO bo = new WFD01120BO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.GetReportType();
            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas)
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE; //d.SUB_CATEGORY;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }
            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString WFD01120ComBoReportName(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            WFD01120BO bo = new WFD01120BO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.GetReportNameForCombo();
            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }
            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString ResponsibilityMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            //var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.RESPONSIBILITY.ToString(), SYSTEM_SUB_CATEGORY.USERROLE_CODE.ToString());
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.RESPONSIBILITY.ToString(), "USERROLE_CODE");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.VALUE).ThenBy(m => m.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
        }

        public static MvcHtmlString SpecialRoleMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            //var datas = bo.SelectSpecialRoleDatas(SYSTEM_CATEGORY.ROLE_MAPING.ToString(), SYSTEM_SUB_CATEGORY.ROLE_PRIORITY.ToString());
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.RESPONSIBILITY.ToString(), "DISPLAY_SPECIAL_ROLE");

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.CODE))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
        }

        public static MvcHtmlString TransferTypeMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.REQUEST.ToString(), SYSTEM_SUB_CATEGORY.TRANS_MAIN_TYPE.ToString());

            ItemValue<string> item;

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "All", attribute, includeIdx0);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                item = new ItemValue<string>();
                item.Value = d.CODE;
                item.Display = d.VALUE;
                item.Selected = true;
                ls.Add(item);
            }

            var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", "All", attribute, includeIdx0, !(ls.Count == 1));
            return _html;
        }
        public static MvcHtmlString WFD02620CostCenterCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            WFD02620BO bo = new WFD02620BO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.GetCostCenterForCombo();

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas)
                {
                    item = new ItemValue<string>();
                    item.Value = d.COST_CODE;
                    item.Display = string.Format("{0} : {1}", d.COST_CODE, d.COST_NAME);

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }

        public static MvcHtmlString WFD02620ResponsibleCostCenterCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            WFD02620BO bo = new WFD02620BO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.GetResponsibleCostCenterForCombo();

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas)
                {
                    item = new ItemValue<string>();
                    item.Value = d.COST_CODE;
                    item.Display = d.COST_NAME; // cost code + cost name from stored procedures

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString InventoryCheckCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true, string firstElement = null)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.STOCK_TAKE.ToString(), SYSTEM_SUB_CATEGORY.INVENT_CHECK.ToString());

            ItemValue<string> item;

            if (datas != null)
            {
                foreach (var d in datas.OrderBy(m => m.REMARKS))
                {
                    item = new ItemValue<string>();
                    item.Value = d.CODE;
                    item.Display = d.VALUE;

                    ls.Add(item);
                }
            }

            return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", firstElement, attribute, includeIdx0);
        }
        public static MvcHtmlString RetirementTypeMultipleCombobox(this HtmlHelper helper, string id, object attribute = null, bool includeIdx0 = true)
        {
            SystemBO bo = new SystemBO();
            List<ItemValue<string>> ls = new List<ItemValue<string>>();
            var datas = bo.SelectSystemDatas(SYSTEM_CATEGORY.REQUEST.ToString(), SYSTEM_SUB_CATEGORY.RETIREMENT_TYPE.ToString());

            ItemValue<string> item;

            if (datas == null)
            {
                return ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false);
            }

            foreach (var d in datas.OrderBy(m => m.CODE))
            {
                item = new ItemValue<string>();
                item.Value = d.VALUE;
                item.Display = d.VALUE;
                item.Selected = true;
                ls.Add(item);
            }

            var _html = ControlCreator.CommonComboBoxWithCustomFirstElement<ItemValue<string>>(id, ls, "Display", "Value", string.Empty, attribute, false, !(ls.Count == 1));
            return _html;
        }




    }


}