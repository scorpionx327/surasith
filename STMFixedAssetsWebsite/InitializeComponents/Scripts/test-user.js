﻿

$(function () {

    //$('#TEST_USER_ID').focusout(updateTestUser);
    //$('#TEST_USER_FA_ADMIN').change(updateTestUser);


});

var updateTestUser = function () {
    var data = {
        UserId: $('#TEST_USER_ID').val(),
        IsFaAdmin: $('#TEST_USER_FA_ADMIN').is(":checked")
    };

    ajax_method.Post('/Test/SetUserData', data, true, function (res) {
        
        console.log(res);
        if (res != null) {
            $('#lbTestUserId').html(res.userId);
            if (res.IsFaAdmin) $('#bTestIsFaAdmin').html('True');
            else $('#bTestIsFaAdmin').html('False');
        }


    }, null);
}