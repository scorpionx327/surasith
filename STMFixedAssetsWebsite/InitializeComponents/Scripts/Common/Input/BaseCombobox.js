﻿

$.fn.BaseComobox = function (options) {

    var cbDiv = $(this);
    var GenBaseComboboxUrl = '/Base/GenBaseCombobox';

    var settings = $.extend({
        Id: '',
        Url: '',
        Style: '',
        IsRequired: false,
    }, options);

    return {

        Initial: function (data, callback) {
            var mol = {
                category: 'FAS_TYPE',
                subcategory: 'ASSET_CATEGORY',
                data: data
            }

            ajax_method.Post(settings.Url, mol, true, function (res) {

                var model = {
                    ls: res,
                    id: settings.Id,
                    style: settings.Style
                };

                ajax_method.Post(GenBaseComboboxUrl, model, true, function (res) {

                    cbDiv.html(res);
                    if (res.indexOf('select2') != -1) {
                        $('#' + settings.Id).select2({ width: '100%' });
                    }

                    if (typeof callback == 'function') {
                        callback();
                    }

                }, function (err) {

                });

                

            }, function (err) {
                console.error(err);
            }, false);

        },

        SetData: function (val) {
            $('#' + settings.Id).select2("val", val);
        },
        GetData: function () {
            return $('#' + settings.Id).select2("val");
        }

    };

}