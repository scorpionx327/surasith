﻿

//$.fn.GetCDateTime = function () {
//    return {
//        DateFormat: "dd-MMM-yyyy",
//        Text: $(this).val(),
//        ValidFormat: false,
//        Value: null,
//    };
//}

var SetTooltipInDataTableByColumn = function (_strText, _lengthSplit) {
    var _strHtml = '';
    if (_strText != null && _strText.length > 0)
    {
        if (_strText.length > _lengthSplit) {
            _strHtml = '<p title="" data-original-title="' + replaceAll(_strText, '"', '&quot;') + '" data-toggle="tooltip" data-placement="top"> ' + _strText.substring(0, _lengthSplit) + '..' + '</p>'
            //_strHtml = '<p title="" data-original-title="' + _strText + '" data-toggle="tooltip" data-placement="top"> ' + _strText.substring(0, _lengthSplit) + '..' + '</p>'
        }
        else {
            _strHtml = _strText;
        }

    } else {
        _strHtml = _strText;
    }
    return _strHtml;
}//data-html="true"
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function htmlSubstring(s, n) {
    var m, r = /<([^>\s]*)[^>]*>/g,
        stack = [],
        lasti = 0,
        result = '';

    //for each tag, while we don't have enough characters
    while ((m = r.exec(s)) && n) {
        //get the text substring between the last tag and this one
        var temp = s.substring(lasti, m.index).substr(0, n);
        //append to the result and count the number of characters added
        result += temp;
        n -= temp.length;
        lasti = r.lastIndex;

        if (n) {
            result += m[0];
            if (m[1].indexOf('/') === 0) {
                //if this is a closing tag, than pop the stack (does not account for bad html)
                stack.pop();
            } else if (m[1].lastIndexOf('/') !== m[1].length - 1) {
                //if this is not a self closing tag than push it in the stack
                stack.push(m[1]);
            }
        }
    }

    //add the remainder of the string, if needed (there are no more tags in here)
    result += s.substr(lasti, n);

    //fix the unclosed tags
    while (stack.length) {
        result += '</' + stack.pop() + '>';
    }

    if (s != null && s.length > 0) {
        if (s.length > n) {
            result = '<p title="" data-html="true" data-original-title="' + s + '" data-toggle="tooltip" data-placement="top"> ' + result + '..' + '</p>';
        } else {
            result = s;
        }
    }

    return result;
}
//<a href="#" data-toggle="tooltip" title="Some tooltip text!">Hover over me</a>

//<!-- Generated markup by the plugin -->
//<div class="tooltip tooltip-top" role="tooltip">
//  <div class="tooltip-arrow"></div>
//  <div class="tooltip-inner">
//    Some tooltip text!
//  </div>
//</div>