﻿

var BatchProcess = function (options) {
    var settings = $.extend({
        UserId: '',
        BatchId: '',
        IsDownload: 'Y'
    }, options);

    var ControlId = {
        CancelButton: settings.BatchId + 'cancel',
        Block: 'BatchDialogArea'
    };

    var functionCallBack = {
        FinishProcess: null
    };

    var AppId = -1;
    var IsCancelBatch = false;
    var Parameters = [];
    var MainE = this;
    var errorMessage = '';

    var endStatus = ['E', 'S', 'C','N']; //N is No Data Found

    var CallBackUrl = {
        GetBatchStatus: '/BatchProcess/GetBatchStatus',
        GetBatchUserAppId: '/BatchProcess/GetBatchUserAppId',
        StartBatch: '/BatchProcess/StartBatch',
        CancelBatch: '/BatchProcess/CancelBatch',
        DownloadFileUrl: '/BatchProcess/DownloadFile'
    };


    return {
        Initial: function () {
            Parameters = [];
            AppId = -1;
            IsCancelBatch = false;
            this._ChkBatchUserAppId();

        },
        StartBatch: function (finishFNCallBack, isNotDownloadFile) {
            var data = {
                BatchId: settings.BatchId,
                UserId: settings.UserId,
                Description: settings.Description,
                Parameters: Parameters
            };

            functionCallBack.FinishProcess = finishFNCallBack;

            var ts = this;

            ajax_method.Post(CallBackUrl.StartBatch, data, true, function (result) {

                if (result != null) {
                    AppId = result;
                    ts._SetBatchStatus(isNotDownloadFile);
                }

            }, null);

        },
        Addparam: function (ParamSeq, name, value) {
            Parameters.push({ ParamSeq: ParamSeq, ParamName: name, ParamValue: value });
        },
        Clear: function () {
            AppId = -1;
            Parameters = [];
            $('#' + ControlId.Block).html('');
            $('#' + ControlId.Block).hide();
        },
        TestDialog: function () {

            var ts = this;

            ts._initialUI('Wating . . .');

            //setTimeout(function () {

            //    ts._initialUI('Processing . . .');

            //    setTimeout(function () {

            //        ts._initialUI('Create Report . . .');

            //        setTimeout(function () {

            //            ts._initialUI('Finish . . .');

            //            setTimeout(function () {

            //                $('#' + ControlId.Block).hide();

            //            }, 1500);

            //        }, 1500);

            //    }, 1500);


            //}, 1500);


        },

        _DownloadFile: function () {
            window.location = '/BatchProcess/DownloadFileName?AppId=' + AppId;
        },
        _CancelBatch: function () {
            IsCancelBatch = true;
            $('#' + ControlId.CancelButton).prop('disabled', IsCancelBatch);
            var data = {
                appId: AppId
            };

            ajax_method.Post(CallBackUrl.CancelBatch, data, true, function (result) {

            }, null);
        },
        _ChkBatchUserAppId: function () {

            var data = { batchId: settings.BatchId, userId: settings.UserId };

            var ts = this;
            ajax_method.Post(CallBackUrl.GetBatchUserAppId, data, true, function (result) {
                console.log("_ChkBatchUserAppId");
                console.log(result);
                if (result != null) {
                    errorMessage = result.message;

                    if (result.data != -1) {
                        AppId = result.data;
                        ts._SetBatchStatus();
                    }
                } else {
                    $('#' + ControlId.Block).hide();
                }

            }, function (err) {
                console.error(err);
            });
        },
        _SetBatchStatus: function (isNotDownloadFile) {

            var data = {
                appId: AppId
            };
            var ts = this;
            ajax_method.Post(CallBackUrl.GetBatchStatus, data, true, function (result) {
                console.log("errorMessage");
                console.log(result);
              
                if (ts._isEndBatch(result.STATUS)) {

                    if (!isNotDownloadFile) {
                        if (result.STATUS == 'S' && settings.IsDownload == "Y") {
                            ts._DownloadFile();
                        }
                    }
                   
                    ts._initialUI(result.MESSAGE);

                    setTimeout(function () {
                        $('#' + ControlId.Block).hide();
                        if (functionCallBack.FinishProcess != null) {
                            functionCallBack.FinishProcess(AppId, result.STATUS);
                            functionCallBack.FinishProcess = null
                        }

                        if (result.STATUS == 'E') {
                            errorMessage = "MCOM0024AERR : Process is error. Please see detail in Log Minitoring screen. App ID = [APP_ID]";
                            AlertTextErrorMessage(errorMessage.replace('[APP_ID]', AppId));
                        }
                        if (result.STATUS == 'N') {

                            AlertTextErrorMessage(result.MESSAGE);

                        }

                        if (result.STATUS = 'C') {
                            IsCancelBatch = false;
                        }

                        return;
                    }, 2000);
                } else {
                    ts._initialUI(result.MESSAGE);

                    setTimeout(function () {
                        ts._SetBatchStatus(isNotDownloadFile);
                    }, 1000);
                }

            });
        },
        _initialUI: function (status) {
            var html = this._getUIHtml(status);
            $('#' + ControlId.Block).html(html);
            $('#' + ControlId.Block).show();
            $('#' + ControlId.CancelButton).prop('disabled', IsCancelBatch);
            this._InitUIEvent();
        },
        _getUIHtml: function (status) {
            var html = '';
            if (isIe8()) {
            html += '<div class="BatchDialog">';
            html += '<div class="BatchDialog-header">' + settings.BatchId + '</div>';
            html += '<div class="BatchDialog-body">';

            html += '   <label class="BatchDialog-status">';
            html += '       <img src="../InitializeComponents/Images/Icon/batch-loading.gif" />' + status;
            html += '   </label>';

            html += '   <div class="BatchDialog-Button">';
            html += '       <button id="' + ControlId.CancelButton + '" class="btn btn-warning">Cancel</button>';
            html += '   </div>';

            html += '</div>';
            html += '</div>';
            } else {
                html += '<div class="batch-progress">';
                html += '    <div class="batch-progress-dialog">';
                html += '        <button id="' + ControlId.CancelButton + '"><i class="fa fa-close" aria-hidden="true"></i>Cancel</button>';
                html += '        <div class="batch-progress-dialog-logo">';
                html += '            <img src="../Images/rolling.gif" />';
                html += '        </div>';
                html += '        <div class="batch-progress-dialog-content">';
                html += '            <i>' + settings.BatchId + '</i>';
                html += '            <label data-toggle="tooltip" title="' + status + '">';
                html += '                ' + status;
                html += '            </label>';
                html += '            <div class="progress">';
                html += '                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width:100%;">';
                html += '                    <span class="sr-only">60% Complete</span>';
                html += '                </div>';
                html += '            </div>';
                html += '        </div>';
                html += '';
                html += '    </div>';
                html += '</div>';
            }

            return html;
        },
        _InitUIEvent: function () {
            MainE = this;
            $('#' + ControlId.CancelButton).click(this._CancelBatch);
        },
        _isEndBatch: function (status) {
            for (var i = 0; i < endStatus.length; i++) {
                if (endStatus[i] == status) return true;
            }
            return false;
        }
    };
}