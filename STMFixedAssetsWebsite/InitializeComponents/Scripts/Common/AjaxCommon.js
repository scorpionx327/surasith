﻿

var ajax_method = $.extend({

    // For search data and using pagination : Http method post
    SearchPost: function (url, conditionData, paginData, isAsync, successFunc, errorFunc, isClearMessage, BoxLoadingId) {
        var data = {
            con: conditionData,
            page: paginData
        }
        console.log(conditionData);
        console.log(data);
        this._AJax("POST", url, data, isAsync, 
            function (result) {

                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result.ObjectResult, result.Pagin);
                }

            }, errorFunc, true, isClearMessage, BoxLoadingId);
    },
    // Ajax callback : Http method Post
    Post: function (url, data, isAsync, successFunc, errorFunc, IsClearMessage, BoxLoadingId) {
        this._AJax("POST", url, data, isAsync,
            function (result) {
                console.log(result);
                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result.ObjectResult);
                }

            }, errorFunc, true, IsClearMessage, BoxLoadingId);
    },
    PostClearMesage: function (url, data, isAsync, successFunc, errorFunc, isClearMessage) {
        this._AJax("POST", url, data, isAsync,
            function (result) {

                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result.ObjectResult);
                }

            }, errorFunc, true, isClearMessage);
    },
    PostBatchProcess: function (url, data, isAsync, successFunc, errorFunc) {
        this._AJax("POST", url, data, isAsync,
            function (result) {

                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result.ObjectResult);
                }

            }, errorFunc, false);
    },

    // Ajax post file to controller
    PostFile: function (url, data, successFunc, errorFunc) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8;',
            success: function (result) {
                if (result.DivMessages != null) {
                    divAlert = result.DivMessages;
                    ClearMessage(divAlert);
                    if (result.Messages != null) {
                        AlertMessage(result.Messages, divAlert);
                    }
                } else {
                    ClearMessageC();
                    if (result.Messages != null) {
                        AlertMessageC(result.Messages);
                    }
                }
                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result);
                }

                loading(false);
            },
            error: function (result) {

                if (result.DivMessages != null) {
                    divAlert = result.DivMessages;
                } else {
                    divAlert = "#AlertMessageArea";
                }
                ClearMessage(divAlert);
                if (errorFunc != null && typeof (errorFunc) != 'undefined') {
                    errorFunc(result);
                }
                loading(false);

            },
            async: true,
            contentType: false,
            processData: false,
        });

    },

    PostMultiFile: function (url, data, successFunc, errorFunc) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: false,//'json',
            contentType: false,//'application/json; charset=UTF-8;',
            success: function (result) {
                if (result.DivMessages != null) {
                    divAlert = result.DivMessages;
                    ClearMessage(divAlert);
                    if (result.Messages != null) {
                        AlertMessage(result.Messages, divAlert);
                    }
                } else {
                    ClearMessageC();
                    if (result.Messages != null) {
                        AlertMessageC(result.Messages);
                    }
                }
                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result);
                }

                loading(false);
            },
            error: function (result) {

                if (result.DivMessages != null) {
                    divAlert = result.DivMessages;
                } else {
                    divAlert = "#AlertMessageArea";
                }
                ClearMessage(divAlert);
                if (errorFunc != null && typeof (errorFunc) != 'undefined') {
                    errorFunc(result);
                }
                loading(false);

            },
            async: true,
            contentType: false,
            processData: false,
        });

    },
    // Ajax callback : Http method Get
    Get: function (url, data, isAsync, successFunc, errorFunc) {

        this._AJax("GET", url, data, isAsync, successFunc, errorFunc, true);

    },

    // Ajax callback : Http method Put
    Put: function (url, data, isAsync, successFunc, errorFunc) {

        this._AJax("PUT", url, data, isAsync, successFunc, errorFunc, true);

    },

    // Ajax callback : Http method Delete
    Delete: function (url, data, isAsync, successFunc, errorFunc) {

        this._AJax("DELETE", url, data, isAsync, successFunc, errorFunc, true);

    },
    ConvertDateBeforePostBack: function (obj) {

        if (obj.length === undefined) {

            for (var j in obj) {
                if (obj[j] instanceof Date && obj[j] !='Invalid Date') {
                    obj[j] = obj[j].toISOString();
                }
            }

        } else {

            var L = obj.length;
            for (var i = 0; i < L; i++) {
                var ob = obj[i];
                for (var j in ob) {
                    if (ob[j] instanceof Date) {
                        ob[j] = ob[j].toISOString();
                    }
                }
            }

        }
        
        return obj;
    },
    _AJax: function (httpMethod, url, data, isAsync, successFunc, errorFunc, isLoading, IsClearMessage, BoxLoadingId) {

        IsClearMessage = typeof IsClearMessage !== 'undefined' ? IsClearMessage : true;
        var objJson = null
        var boxLoading = typeof BoxLoadingId !== 'undefined' ? $('#' + BoxLoadingId) : null;
        if (data != null)
            objJson = $.toJSON(data);
        objJson = ajax_method.ConvertIncorrectText(objJson);

        loading(isLoading);

        if (boxLoading != null) {
            boxLoading.BoxLoading(true);
        }

        $.ajax({
            type: httpMethod,
            url: url,
            data: objJson,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (result.DivMessages != null) {
                    divAlert = result.DivMessages;
                    if (IsClearMessage == true) {
                        ClearMessage(divAlert);
                    }
                    if (result.Messages != null) {
                        AlertMessage(result.Messages, divAlert);
                    }
                } else {
                    if (IsClearMessage == true) {
                        ClearMessageC();
                    }   
                    if (result.Messages != null) {
                        AlertMessageC(result.Messages);
                    }
                }

               

                if (successFunc != null && typeof (successFunc) == 'function') {
                    successFunc(result);
                }

                loading(false);
                if (boxLoading != null) {
                    boxLoading.BoxLoading(false);
                }
            },
            error: function (result) {
                if (result.DivMessages != null) {
                    divAlert = result.DivMessages;
                } else {
                    divAlert = "#AlertMessageArea";
                }
                ClearMessage(divAlert);
                if (errorFunc != null && typeof (errorFunc) != 'undefined') {
                    errorFunc(result);
                }
                loading(false);
                if (boxLoading != null) {
                    boxLoading.BoxLoading(false);
                }
            },
            async: (isAsync == undefined ? true : isAsync),
            //contentType: contentType,
            //processData: processData,
        });
    },

    ConvertIncorrectText: function (txt) {
        var ntxt = txt;
        if (ntxt != undefined) {
            //Convert &amp; to &
            ntxt = ntxt.replace(/&amp;/g, "&");

            /* --- Merge --- */
            ntxt = ntxt.replace(/&lt;/g, "<");
            ntxt = ntxt.replace(/&gt;/g, ">");
            /* ------------- */
        }

        return ntxt;
    },

    isFunction: function (functionToCheck) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }



});