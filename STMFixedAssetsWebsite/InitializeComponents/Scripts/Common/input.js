﻿
$(function () {

    $('.text-uppercase').keyup(function () {
        this.value = this.value.toUpperCase();
    });

});

$.fn.IntegerInput = function () {
    $(this).keydown(function (e) {
        console.log(e.keyCode);
        if ((/^([0-9])$/.test(e.key)) || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 17 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || e.keyCode == 46) {
            return;
        }else{
            e.preventDefault();
        }
       
    });
}
$.fn.IntegerSearchInput = function () {
    $(this).keydown(function (e) {
        console.log(e.keyCode);
        if ((/^([0-9])$/.test(e.key)) || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13 || e.keyCode == 17 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || e.keyCode == 46) {
            return;
        } else {
            e.preventDefault();
        }

    });
}

$.fn.NormalTextInput = function () {
    $(this).keydown(function (e) {
        if ((/^[A-Za-z0-9]{0,1}[A-Za-z0-9]{0,8}$/.test(e.key)) || e.keyCode == 9 || e.keyCode == 17) {
            return;
        } else {
            e.preventDefault();
        }

    });
}


$.fn.Select2Require = function (isRequired) {

    if (isRequired == true) {
        $(this).parent().find('span').find('.select2-selection').addClass('require');
    } else if (isRequired == false) {
        $(this).parent().find('span').find('.select2-selection').removeClass('require');
    }

}