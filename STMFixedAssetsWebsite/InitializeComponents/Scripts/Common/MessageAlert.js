﻿
var ClearMessageC = function () {
    $('.message-area').html('');
}
var ClearMessage = function (idDiv) {
    $(idDiv).html('');
}

var AlertMessageC = function (Messages) {
    var ele = getMessageAreaElement();
    ele.html('');

    for (var i = 0; i < Messages.length; i++) {
        var html = AddSTMFAMessageBlock(Messages[i]);
        ele.append(html);
    }
    $("html, body").animate({ scrollTop: 0 }, "fast");
}
var getMessageAreaElement = function () {
    var ele = $('#AlertMessageArea');
    if ($('.message-area').length > 1) {

        $('.message-area').each(function () {
            var modal = $(this).closest('.modal');
            if (modal.length >= 1 && modal.css('display') == 'block') {
                ele = $(this);
            }
        });
    }
    return ele;
}

var AlertTextErrorMessage = function (text) {
    var html = '';
    var ele = getMessageAreaElement();
    html = AlertMessageC([{ Type: 3, Messages: [text] }]);

}

var AlertMessageCPopup = function (Messages,div, divBody) {
    var ele = $(div);
    ele.html('');

    for (var i = 0; i < Messages.length; i++) {
        var html = AddSTMFAMessageBlock(Messages[i]);
        ele.append(html);
    }
    if (divBody) {
        var $divBody = $(divBody);
        //SCROLL TO TOP BODY
        if ($divBody.length > 0) {
            $divBody.scrollTop(0);
        }
    }
}
var AlertTextErrorMessagePopup = function (text, div, divBody) {
    console.log(text);
    console.log(div);
    var html = '';
    var ele = $(div);
    ele.html('');
    html = AddSTMFAMessageBlock({ Type: 3, Messages: [text] });
    ele.append(html);

    if (divBody) {
        var $divBody = $(divBody);
        //SCROLL TO TOP BODY
        if ($divBody.length > 0) {
            $divBody.scrollTop(0);
        }
    }
}

var AletTextInfoMessage = function (text) {
    var html = '';
    var ele = getMessageAreaElement();
    html = AlertMessageC([{ Type: 1, Messages: [text] }]);
}

var AlertMessage = function (Messages, idDiv) {
    $(idDiv).html('');

    for (var i = 0; i < Messages.length; i++) {
        var html = AddSTMFAMessageBlock(Messages[i]);
        $(idDiv).append(html);
    }
    $("html, body").animate({ scrollTop: 0 }, "fast");
}
var AddSTMFAMessageBlock = function (Message) {
    var html = '';
    var typeClass = GetSTMFAMessageTypeCssClass(Message.Type);
    var iconClass = getSTMFMessageIcon(Message.Type);
    if (typeClass == '') return;

    var iconHtml = '<i class="' + iconClass + '" aria-hidden="true"></i>';

    //html += '<div class="alert alert-' + typeClass + '">';
    html += '<div class="alert">';
    //html += '<i class="' + iconClass + '" aria-hidden="true"></i>';
    //html += '<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>';
    html += GetMessageList(Message.Messages, iconHtml);
    html += '</div>';

    return html;
}
var GetMessageList = function (Messages, iconHtml) {
    var html = '';
    html += '<ul>';
    for (var i = 0; i < Messages.length; i++) {
        html += '<li>' + iconHtml + Messages[i] + '</li>';
    }
    html += '</ul>';

    return html;
}
var GetSTMFAMessageTypeCssClass = function (type) {
    switch (type) {
        case 1: // info
            return 'info';
        case 2: // warning
            return 'warning';
        case 3: // error
        case 5: // Not found
            return 'danger';
        default:
            return '';
    }
}
var getSTMFMessageIcon = function (type) {
    switch (type) {
        case 1: // info
            return 'fa fa-info-circle';
        case 2: // warning
            return 'fa fa-warning';
        case 3: // error
        case 5: // Not found
            return 'fa fa-times-circle';
        default:
            return '';
    }
}