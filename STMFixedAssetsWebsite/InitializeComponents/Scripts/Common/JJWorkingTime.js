﻿
/*

 * คำนวณและตั้งค่าวันเวลาในช่วงเวลาทำงาน
 * ใช้กับหน้า WFD02620

*/
var JJWorkingTime = function (options) {

    var settings = $.extend({
        workingTimeStart: 0, // in minute
        workingTimeEnd: 0, // in minute
        breakTimeStart: 0, // in minute
        breakTimeEnd: 0, // in minute
        holidays: []
    }, options);


    return {

        SetSettings: function (workingTimeStart, workingTimeEnd, breakTimeStart, breakTimeEnd, holidays) {
            settings.workingTimeStart = workingTimeStart;
            settings.workingTimeEnd = workingTimeEnd;
            settings.breakTimeStart = breakTimeStart;
            settings.breakTimeEnd = breakTimeEnd;
            settings.holidays = holidays;
        },

        GetDifMinute: function (from, to) {
            var minute = 0;
            var date = new Date(from);

            while (date.Date() <= to.Date())
            {
                var f = new Date(date);
                var t;


                if (this._isHoliDay(date)) {
                    date = date.addDays(1).Date();
                    continue;
                }
                else if (date.EqualDate(to))
                    t = date.Date().setTimeOfDay(to.TimeOfDay());
                else if (date.Date() < to.Date()) t = date.Date().setTimeOfDay(settings.workingTimeEnd);
                else return 0;


                minute += this._getMinuteZone1(f, t);
                minute += this._getMinuteZone2(f, t);

                date = date.addDays(1).Date();
            }

            return minute;

        }, _getDifMinute: function (t1, t2) {
            if (t2 <= t1) return 0;
            return t2 - t1;
        }, _getMinuteZone1: function (from, to) {
            if (from >= to) return 0;
            var t1, t2;


            if (from.TimeOfDay() >= settings.workingTimeStart) t1 = from.TimeOfDay();
            else t1 = settings.workingTimeStart;

            if (to.TimeOfDay() <= settings.breakTimeStart) t2 = to.TimeOfDay();
            else t2 = settings.breakTimeStart;

            return this._getDifMinute(t1, t2);
        }, _getMinuteZone2: function (from, to) {
            if (from >= to) return 0;

            var t1, t2;

            if (from.TimeOfDay() >= settings.breakTimeEnd) t1 = from.TimeOfDay();
            else t1 = settings.breakTimeEnd;

            if (to.TimeOfDay() <= settings.workingTimeEnd) t2 = to.TimeOfDay();
            else t2 = settings.workingTimeEnd;

            return this._getDifMinute(t1, t2);
        },

        GetResultDateTimeAfterAddMinute: function (date, minutes) {

            while (minutes > 0) {
                //console.log(date + ' : ' + minutes);
                if (date.TimeOfDay() < settings.workingTimeStart) date = date.Date() + settings.workingTimeStart;
                else if (date.TimeOfDay() >= settings.breakTimeStart && date.TimeOfDay() < settings.breakTimeEnd) date = date.Date().setTimeOfDay(settings.breakTimeEnd);
                else if (date.TimeOfDay() >= settings.workingTimeEnd || this._isHoliDay(date)) date = date.Date().addDays(1).setTimeOfDay(settings.workingTimeStart);
                else {
                    date = date.AddMinutes(1);
                    minutes -= 1;
                }
            }

            return date;
        },
        _isHoliDay: function (date) {
            var isHoliday = false;
            for (var i = 0; i < settings.holidays.length; i++) {
                if (settings.holidays[i].EqualDate(date)) {
                    isHoliday = true;
                    break;
                }
            }
            return isHoliday;
        }
    };
};

Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.AddMinutes = function (minutes) {
    return new Date(this.getTime() + minutes * 60000);
}

Date.prototype.TimeOfDay = function () {
    return (this.getHours() * 60) + this.getMinutes();
}

Date.prototype.Date = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
}

Date.prototype.setTimeOfDay = function (time) {
    var d = new Date(this);
    d.setHours(Math.floor(time / 60), (time % 60), 0);
    return d;
}

Date.prototype.EqualDate = function (date) {
    var valid = true;

    if (this.getDate() != date.getDate()) valid = false;
    if (this.getMonth() != date.getMonth()) valid = false;
    if (this.getFullYear() != date.getFullYear()) valid = false;

    return valid;
}

String.prototype.GetTimeOfDayFromStr = function () {
    var d = this.getTimeFromStr();
    return (d.getHours() * 60) + d.getMinutes();
}

String.prototype.getTimeFromStr = function () {
    var dt = new Date();

    dt.setHours(($(this)[0] + $(this)[1]));
    dt.setMinutes(($(this)[3] + $(this)[4]));
    dt.setSeconds(0);

    return dt;
}