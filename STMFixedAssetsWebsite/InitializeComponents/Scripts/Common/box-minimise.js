﻿

$(function () {

    // หด-ขยาย box
    $('.btn-box-minimise-tool').click(function (n) {

        // หด
        if ($(this).find('i').hasClass('fa-angle-double-left')) {
            var box = null
            var bigBox = null;

            var defaultWDClass = [];
            var defaultBigBoxWDClass = [];

            if ($(this).closest(".box-minimise").length > 0) { box = $(this).closest(".box-minimise")[0]; };
            if (box == null) return;

            var sBoxs = $(box).siblings();
            if (sBoxs.length == 1) {
                bigBox = sBoxs[0];
            }

            var clss = $(box).attr('class').split(' ');
            for (var i = 0; i < clss.length; i++) {
                if (clss[i].indexOf('col-') >= 0) {
                    defaultWDClass.push(clss[i]);
                }
            }

            var oldClass = '';
            for (var i = 0; i < defaultWDClass.length; i++) {
                oldClass += defaultWDClass[i] + ' ';
                $(box).removeClass(defaultWDClass[i]);
            }
            $(box).find('.box-title').addClass('box-minimise-hide');
            $(box).find('.box-body').addClass('box-minimise-hide');
            $(box).addClass('col-lg-1');

            $(this).find('i').removeClass('fa fa-angle-double-left');
            $(this).find('i').addClass('fa fa-angle-double-right');
            $(this).attr('data-original-title', 'Show');

            $(box).attr('data-old-cls', oldClass);

            if (bigBox != null) {
                var bbClss = $(bigBox).attr('class').split(' ');
                for (var i = 0; i < bbClss.length; i++) {
                    if (bbClss[i].indexOf('col-') >= 0) {
                        defaultBigBoxWDClass.push(bbClss[i]);
                    }
                }

                var bbOldClass = '';
                for (var i = 0; i < defaultBigBoxWDClass.length; i++) {
                    bbOldClass += defaultBigBoxWDClass[i] + ' ';
                    $(bigBox).removeClass(defaultBigBoxWDClass[i]);
                }

                $(bigBox).addClass('col-lg-11');
                $(bigBox).attr('data-old-cls', bbOldClass);
            }
            
        }
        // ขยาย
        else if ($(this).find('i').hasClass('fa-angle-double-right')) {
            var box = null
            var bigBox = null;

            var oldClass = '';
            var bbOldClass = '';

            if ($(this).closest(".box-minimise").length > 0) { box = $(this).closest(".box-minimise")[0]; };
            if (box == null) return;

           

            oldClass = $(box).attr('data-old-cls');

            $(box).find('.box-minimise-hide').removeClass('box-minimise-hide');
            $(box).removeClass('col-lg-1');
            $(box).addClass(oldClass);

            $(this).find('i').removeClass('fa fa-angle-double-right');
            $(this).find('i').addClass('fa fa-angle-double-left');
            $(this).attr('data-original-title', 'Hide');

            // Set big box to old property
            var sBoxs = $(box).siblings();
            if (sBoxs.length == 1) {
                bigBox = sBoxs[0];
            }

            if (bigBox != null) {
                bbOldClass = $(bigBox).attr('data-old-cls');
                $(bigBox).removeClass('col-lg-11');
                $(bigBox).addClass(bbOldClass);
            }


        }

        
        
    });


    // ปรับความสูงตาม element ที่ set ไว้
    $('.map-height').each(function (e) {

        var tarGetId = $(this).attr('data-map-height-id');
        if (!(typeof tarGetId === "undefined")) {
            var target = $('#' + tarGetId);

            setMapHeight($(this), target);

            target.resize(function () {

                console.log('height: '  + $(this).height());

            });
            
        }
    });
});

var setMapHeight = function (e1, e2) {
    var h1 = e1.height();
    var h2 = e2.height();
    if (h1 > h2) {
        e2.css('height', (h1 + 3) + 'px');
        //e2.height(h1);
    } else {
        e1.css('height', (h2 + 3) + 'px');
        //e1.height(h2);
    }
}