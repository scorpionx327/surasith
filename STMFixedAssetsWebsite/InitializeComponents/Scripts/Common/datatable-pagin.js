﻿// Create by Jirawat Jannet
// Create date 2017-0202
// Use this function after initial DataTable

$.fn.Pagin = function (ItemPerPage) {

    var PaginData = {
        OrderColIndex: '',
        OrderType: '',
        CurrentPage: 1,
        ItemPerPage: ItemPerPage,
        Totalitem: 1,
        TotalPage: 1
    };

    //var itemPerPages = [10, 50, 100, 200, 500]; // -1 = All
    var itemPerPages = [10, 15, 20, 25];

    var controlId = {
        thisId: $(this).attr('id'),
        cbbItemPerPage: $(this).attr('id') + '-item-per-page',
        cbbChangePage: $(this).attr('id') + '-change-page',
        pCbbitemPerPageArea: '',
        pPaginArea: $(this).attr('id') + '-pagin-area',
        lbTotalitem: $(this).attr('id') + '-total-item',
        btnFirst: $(this).attr('id') + '-first-page',
        btnBack: $(this).attr('id') + '-back-page',
        btnNext: $(this).attr('id') + '-next-page',
        btnLast: $(this).attr('id') + '-last-page',
    };

    var ScreenId = '';

    var callbackEvent = {
        Sorting: null,
        ChangePage: null
    };

    var controlClass = {
        Disabled: 'p-disabled'
    }

    var text = {
        TotalRecord: 'Total record:',
        Item: 'item',
        All: 'Show all item',
        ItemPerPage: '',// 'items per page',
    };

    var This = this;


    return {
        Init: function (paginData, callbackSorting, callbackPageChange, cbbItemPerPageAreaId) {
            PaginData = paginData;
            callbackEvent.ChangePage = callbackPageChange;
            callbackEvent.Sorting = callbackSorting;
            
            this._ChangeSortIcon(PaginData.OrderColIndex, PaginData.OrderType);
            this._initPaginUI(cbbItemPerPageAreaId);
            this._initSortingEvent();
        },
        GetPaginData: function () {
            if (PaginData.ItemPerPage == null) PaginData.ItemPerPage = 10;
            return PaginData;
        },
        ClearPaginData: function () {
            if (PaginData.ItemPerPage == null) PaginData.ItemPerPage = 10;
            PaginData.CurrentPage = 1;
        },
        Clear: function () {

            $('#' + controlId.pPaginArea).remove();
            if (controlId.pCbbitemPerPageArea != '' && $('#' + controlId.pCbbitemPerPageArea).length > 0) {
                $('#' + controlId.pCbbitemPerPageArea).html('');
            }

            PaginData = {
                OrderColIndex: '',
                OrderType: '',
                CurrentPage: 1,
                ItemPerPage: ItemPerPage,
                Totalitem: 1,
                TotalPage: 1
            };

        },
        // Change header sorting icon
        _ChangeSortIcon: function (colIndex, sortType) {
            if (!isIe8()) {
                $('#' + controlId.thisId + ' thead th.sorting_desc').removeClass('sorting_desc');
                $('#' + controlId.thisId + ' thead th.sorting_asc').removeClass('sorting_asc');
                $('#' + controlId.thisId + ' thead th.sorting').removeClass('sorting');

                $('#' + controlId.thisId + ' thead th').each(function (inx, element) {
                    if (!($(this).hasClass('sorting_disabled'))) {

                        if (inx == colIndex) {
                            if (sortType == 'asc') {
                                $(this).addClass('sorting_asc');
                            } else {
                                $(this).addClass('sorting_desc');
                            }
                        } else {
                            $(this).addClass('sorting');
                        }
                    }
                });
            }

        },
        _initSortingEvent: function () {
            if (!isIe8()) {
                $('#' + controlId.thisId + ' thead th').each(function (inx, element) {
                    if (!(element.className.indexOf('sorting_disabled') >= 0)) {
                        $(this).off('click').on("click", function (res) {
                            var orderType = 'asc';

                            if (res.currentTarget.className.indexOf('asc') >= 0) orderType = 'asc';
                            if (res.currentTarget.className.indexOf('desc') >= 0) orderType = 'desc';
                            orderType = orderType == 'asc' ? 'desc' : 'asc';

                            PaginData.OrderColIndex = inx;
                            PaginData.OrderType = orderType;
                            callbackEvent.Sorting(PaginData);
                        });
                    }


                });

                    //$('#' + controlId.thisId).off('order.dt');
                    //$('#' + controlId.thisId).on('order.dt', function (a, b, c, d) {
                    //    var colIndex = parseInt(Object.keys(d)[0]);
                    //    var orderType = d[Object.keys(d)[0]];
                    //    if (callbackEvent.Sorting != null && typeof (callbackEvent.Sorting) == 'function') {
                    //        PaginData.OrderColIndex = colIndex;
                    //        PaginData.OrderType = orderType;
                    //        callbackEvent.Sorting(PaginData);
                    //    }
                    //});
            }
            //$('#' + controlId.thisId).unbind('order.dt');
            
        },
        _initPaginUI: function (cbbItemPerPageAreaId) {
            if (PaginData == null) return;
            controlId.pCbbitemPerPageArea = cbbItemPerPageAreaId;

            $('#' + controlId.pPaginArea).remove();

            var html = '';
            var value = $('#' + controlId.thisId).get(0).style.width;
            var hasPx = value.indexOf('px') >= 0;
            var hasPct = value.indexOf('%') >= 0;
            var _withPagin = $('#' + controlId.thisId).width() + 15;
            if (hasPx) {
                html += '<div id="' + controlId.pPaginArea + '" class="row" style="padding-top:5px; width: ' + _withPagin + 'px;">';
            } else {
                html += '<div id="' + controlId.pPaginArea + '" class="row" style="padding-top:5px;">';
            }           

            //Edit by Thanawut T. 2017/04/05 Fix for change scroll bar position of Datatable
           
            //html += '<div id="' + controlId.pPaginArea + '" class="row">';
            //End Edit by Thanawut T.
            //html += '<div class="col-lg-5 vertical">';
            //html += text.TotalRecord + ' ' + PaginData.Totalitem + ' ' + text.Item;
            //if (PaginData.Totalitem > 1) html += 's';
            //html += '</div>';
            //html += '<div class="col-lg-7 text-right">';

            //Edit by Pattanapong.p 2019/09/24 Show item-per-page
            html += '<div class="col-sm-6 col-md-6 vertical">';
            html += text.TotalRecord + ' ' + PaginData.Totalitem + ' ' + text.Item;
            if (PaginData.Totalitem > 1) html += 's';
            html += '</div>';

            //html += '<div class="col-sm-2 text-right" style="padding: 10px;">';
            //html += '<table>'
            //html += '<tr>'
            //html += '<td>View'
            //html += '</td>'
            //html += '<td>'
            //html += '<select id="' + controlId.cbbItemPerPage + '" class="">';
            //for (var i = 0 ; i < itemPerPages.length; i++) {
            //    if (itemPerPages[i] == -1) {
            //        html += '<option value="' + itemPerPages[i] + '">' + text.All + '</option>';
            //    }
            //    else {
            //        if (PaginData.ItemPerPage != null && (itemPerPages[i] == PaginData.ItemPerPage))//selected="selected"
            //        {
            //            html += '<option selected="selected" value="' + itemPerPages[i] + '">' + itemPerPages[i] + ' ' + text.ItemPerPage + '</option>';
            //        } else {
            //            html += '<option value="' + itemPerPages[i] + '">' + itemPerPages[i] + ' ' + text.ItemPerPage + '</option>';
            //        }
            //    }
            //}
            //html += '</select>';
            //html += '</td>'
            //html += '<td>Items/Page'
            //html += '</td>'
            //html += '</tr>'
            //html += '</table>'
            //html += '</div>';


            /////////////////////////////////
            html += '<div class="col-sm-3 col-md-3 form-horizontal text-right"> <div class="form-group">';
            html += '<label class="col-sm-4 control-label" style="padding-right: 0px;">View</label>';
            html += '<div class="col-sm-4">';
            html += '<select id="' + controlId.cbbItemPerPage + '" class="form-control">';
            for (var i = 0 ; i < itemPerPages.length; i++) {
                if (itemPerPages[i] == -1) {
                    html += '<option value="' + itemPerPages[i] + '">' + text.All + '</option>';
                }
                else {
                    if (PaginData.ItemPerPage != null && (itemPerPages[i] == PaginData.ItemPerPage))//selected="selected"
                    {
                        html += '<option selected="selected" value="' + itemPerPages[i] + '">' + itemPerPages[i] + ' ' + text.ItemPerPage + '</option>';
                    } else {
                        html += '<option value="' + itemPerPages[i] + '">' + itemPerPages[i] + ' ' + text.ItemPerPage + '</option>';
                    }
                }
            }
            html += '</select>';


            html += '</div>';
            html += '<label class="col-sm-3 control-label" style="padding-left: 0px;">Items/Page</label>';
            html += '</div></div>';
            
            /////////////////////////////////

            html += '<div class="col-sm-3 col-md-3 text-right" style="padding: 2px;">';
            //////////////////////////////////Edit by Pattanapong.p 2019/09/24 Show item-per-page///////////////////////////////////

            html += '<ul class="pagination">';
            html += '<li><a href="#/" id="' + controlId.btnFirst + '"><i class="glyphicon glyphicon-step-backward"></i></a></li>';
            html += '<li><a href="#/" id="' + controlId.btnBack + '"><i class="glyphicon glyphicon-chevron-left"></i></a></li>';
            html += '<li><select class="pagin-cbb" id="' + controlId.cbbChangePage + '">';
            for (var i = 1; i <= PaginData.TotalPage; i++) {
                html += '<option value="' + i + '">' + i + '</option>';
            }
            html += '</select></li>';

            html += '<li><a href="#/" id="' + controlId.btnNext + '"><i class="glyphicon glyphicon-chevron-right"></i></a></li>';
            html += '<li><a href="#/" id="' + controlId.btnLast + '"><i class="glyphicon glyphicon-step-forward"></i></a></li>';

            html += '</ul></div></div>';

            //Edit by Thanawut T. 2017/04/05 Fix for change scroll bar position of Datatable
            var ctrlClass = $('#' + controlId.thisId).parent().parent().parent().parent().attr('class');
            $('#' + controlId.thisId).parent().parent().parent().parent().after('<div class="' + ctrlClass + '" style="overflow: hidden;">' + html + '</div>');
            //$('#' + controlId.thisId).after(html);
            //End Edit by Thanawut T.
            $('#' + controlId.cbbChangePage).val(PaginData.CurrentPage);


            if (controlId.pCbbitemPerPageArea) {
                this._initItemPerPageCombobox();
            }

            this._initEnabled();
            this._initControlEvent();
        },
        _initItemPerPageCombobox: function () {
            
            var html = '';
            //if (PaginData.ItemPerPage != null) {
            //    itemPerPages[0] = PaginData.ItemPerPage;
            //}
            html += '<select id="' + controlId.cbbItemPerPage + '" class="pagin-cbb">';
            for (var i = 0 ; i < itemPerPages.length; i++) {
                if (itemPerPages[i] == -1) {
                    html += '<option value="' + itemPerPages[i] + '">' + text.All + '</option>';
                }                    
                else {
                    if (PaginData.ItemPerPage != null && (itemPerPages[i] == PaginData.ItemPerPage))//selected="selected"
                    {
                        html += '<option selected="selected" value="' + itemPerPages[i] + '">' + itemPerPages[i] + ' ' + text.ItemPerPage + '</option>';
                    }else{
                        html += '<option value="' + itemPerPages[i] + '">' + itemPerPages[i] + ' ' + text.ItemPerPage + '</option>';
                    }                    
                }                    
            }
            html += '</select>';

            $('#' + controlId.pCbbitemPerPageArea).html(html);
            $('#' + controlId.cbbItemPerPage).val(PaginData.ItemPerPage);
        },
        _initEnabled: function () {
            this._enabled(controlId.btnFirst);
            this._enabled(controlId.btnBack);
            this._enabled(controlId.btnNext);
            this._enabled(controlId.btnLast);
            this._enabled(controlId.cbbChangePage);

            if (PaginData.CurrentPage <= 1) {
                this._disabled(controlId.btnFirst);
                this._disabled(controlId.btnBack);
            }

            if (PaginData.CurrentPage >= PaginData.TotalPage) {
                this._disabled(controlId.btnNext);
                this._disabled(controlId.btnLast);
            }

            if (PaginData.TotalPage <= 1) {
                this._disabled(controlId.btnFirst);
                this._disabled(controlId.btnBack);
                this._disabled(controlId.btnNext);
                this._disabled(controlId.btnLast);
                this._disabled(controlId.cbbChangePage);
            }
        },
        _initControlEvent: function () {

            var parent = this;

            $('#' + controlId.btnFirst).off('click').click(function () { parent._first(parent); });
            $('#' + controlId.btnBack).off('click').click(function () { parent._back(parent); });
            $('#' + controlId.btnNext).off('click').click(function () { parent._next(parent); });
            $('#' + controlId.btnLast).off('click').click(function () { parent._last(parent); });
            $('#' + controlId.cbbChangePage).off('change').change(function () { parent._jump(parent, this); });
            $('#' + controlId.cbbItemPerPage).off('change').change(function () { parent._changeItemPerPage(parent, this); });
        },
        _changeItemPerPage: function (parent, thisEl) {
            PaginData.ItemPerPage = parseInt($(thisEl).find('option:selected').val());
            parent._initEnabled();
            callbackEvent.ChangePage(PaginData);
        },
        _first: function (parent) {
            if ($(this).hasClass(controlClass.Disabled)) return;
            if (PaginData.CurrentPage <= 1) return;
            
            PaginData.CurrentPage = 1;
            $('#' + controlId.cbbChangePage).val(PaginData.CurrentPage);
            parent._initEnabled();
            callbackEvent.ChangePage(PaginData);
        },
        _back: function (parent) {
            if ($(this).hasClass(controlClass.Disabled)) return;
            if (PaginData.CurrentPage <= 1) return;

            PaginData.CurrentPage = PaginData.CurrentPage - 1;
            $('#' + controlId.cbbChangePage).val(PaginData.CurrentPage);
            parent._initEnabled();
            callbackEvent.ChangePage(PaginData);
        },
        _next: function (parent) {
            if ($(this).hasClass(controlClass.Disabled)) return;
            if (PaginData.CurrentPage >= PaginData.TotalPage) return;

            PaginData.CurrentPage = PaginData.CurrentPage + 1;
            $('#' + controlId.cbbChangePage).val(PaginData.CurrentPage);
            parent._initEnabled();
            callbackEvent.ChangePage(PaginData);
        },
        _last: function (parent) {
            if ($(this).hasClass(controlClass.Disabled)) return;
            if (PaginData.CurrentPage >= PaginData.TotalPage) return;

            PaginData.CurrentPage = PaginData.TotalPage;
            $('#' + controlId.cbbChangePage).val(PaginData.CurrentPage);
            parent._initEnabled();
            callbackEvent.ChangePage(PaginData);
        },
        _jump: function (parent, thisEl) {
            if ($(this).hasClass(controlClass.Disabled)) return;

            PaginData.CurrentPage = parseInt($(thisEl).find('option:selected').val());
            parent._initEnabled();
            callbackEvent.ChangePage(PaginData);
        },
        _disabled: function (id) {
            $('#' + id).addClass(controlClass.Disabled);
            $('#' + id).prop('disabled', true);
        },
        _enabled: function (id) {
            $('#' + id).removeClass(controlClass.Disabled);
            $('#' + id).prop('disabled', false);
        }
    }

    if (cbbitemPerPageAreaId.trim()) {
        


    }


}