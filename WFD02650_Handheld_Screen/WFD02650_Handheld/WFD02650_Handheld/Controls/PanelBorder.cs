﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WFD02650_Handheld.Controls
{
    public class PanelBorder : Panel
    {

        protected override void OnPaint(PaintEventArgs e) 
        {
            e.Graphics.DrawRectangle(new Pen(Color.Gray), 0, 0,
                                     e.ClipRectangle.Width - 1,
                                     e.ClipRectangle.Height - 1
                                    );
        }

    }
}
