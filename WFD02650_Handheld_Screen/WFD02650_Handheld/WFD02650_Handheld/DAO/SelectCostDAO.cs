﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace WFD02650_Handheld.DAO
{
    public class SelectCostDAO : Database
    {
        public SelectCostDAO() : base() { }

        public DataTable GetCostCenterDatas()
        {
            try
            {
                return executeDataToDataTable("SELECT COST_CODE, COST_NAME FROM TB_R_STOCK_TAKE_D GROUP BY COST_CODE, COST_NAME");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
