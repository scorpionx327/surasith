﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WFD02650_Handheld.Models;
using System.Data;
using System.Globalization;

namespace WFD02650_Handheld.DAO
{
    public class MainWorkingScreenDAO : Database
    {

        public bool IsValidBarcode(string COST_CODE, string BARCODE)
        {
            string sql = string.Format(" select count(1) cnt from DB_SCAN where COST_CODE<>'{0}' and BARCODE='{1}' "
                , COST_CODE, BARCODE);

            DataTable dt = executeDataToDataTable(sql);

            int cnt=  0;
            if (dt.Rows.Count > 0) cnt = int.Parse(dt.Rows[0]["cnt"].ToString());
            return cnt > 0;
        }
         
        public void updateScanBarcode(DBScanModel item, string CheckStatus)
        {
            StringBuilder s = new StringBuilder();
            s.Append(" update DB_SCAN ");
            s.Append(string.Format("  set  CHECK_STATUS='{0}' ", CheckStatus));
            s.Append(string.Format("       , START_COUNT_TIME=CONVERT(Datetime, '{0}', 120) ", item.START_COUNT_TIME.Value.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-Us"))));
            s.Append(string.Format("       , END_COUNT_TIME=CONVERT(Datetime, '{0}', 120) ", item.END_COUNT_TIME.Value.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-Us"))));
            s.Append(string.Format("       , DATE=CONVERT(Datetime, '{0}', 120) ", item.DATE.Value.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-Us"))));
            s.Append(string.Format("  WHERE STOCK_TAKE_KEY='{0}' and BARCODE='{1}' ", item.STOCK_TAKE_KEY, item.BARCODE));

            ExecuteNonQuery(s.ToString());
        }

        public void updateScanStatus(DBScanModel item, string CheckStatus)
        {
            StringBuilder s = new StringBuilder();
            s.Append(" update DB_SCAN ");
            s.Append(string.Format("  set  CHECK_STATUS='{0}' ", CheckStatus));
            s.Append(string.Format("  WHERE STOCK_TAKE_KEY='{0}' and BARCODE='{1}' ", item.STOCK_TAKE_KEY, item.BARCODE));

            ExecuteNonQuery(s.ToString());
        }

        public void LockCostEmp(string EMP_CODE, string COST_CODE)
        {
            string sql = string.Format("UPDATE DB_COST_EMP set IS_LOCK='Y' WHERE EMP_CODE='{0}' and COST_CODE='{1}'"
                , EMP_CODE, COST_CODE);
            ExecuteNonQuery(sql);
        }

        public void addDBInvalid(DB_InvalidModel data)
        {
            List<DB_InvalidModel> datas = new List<DB_InvalidModel>();
            datas.Add(data);
            DBSet ds = new DBSet();
            DataTable dt = ToDataTable<DB_InvalidModel>(datas, ds.DB_INVALID);
            InsertData(dt);
        }
        public void updateScanInvalidUploadFlag(DB_InvalidModel item, string UploadFlag)
        {
            StringBuilder s = new StringBuilder();
            s.Append(" update DB_INVALID ");
            s.Append(string.Format("  set  IS_UPLOAD='{0}' ", UploadFlag));
            s.Append(string.Format("  WHERE EMP_CODE='{0}' and BARCODE='{1}' ", item.EMP_CODE, item.BARCODE));

            ExecuteNonQuery(s.ToString());
        }
        public DBScanModel GetInvalidScanData(string barcode, string costCode)
        {
            try
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(" select  ");
                sql.Append("   *  ");
                sql.Append(" from DB_INVALID ");
                sql.Append(" where BARCODE='{0}' ");
                sql.Append("        and COST_CODE><>'{1}' ");
                return executeDataToList<DBScanModel>(string.Format(sql.ToString(), barcode, costCode)).FirstOrDefault();
            }
            catch 
            {
                return null;
            }
        }


        public int isThereDataForUpload()
        {
            try
            {
                DataTable dt = executeDataToDataTable("SELECT count(1) as cnt FROM DB_SCAN WHERE CHECK_STATUS='W'");
                return int.Parse(dt.Rows[0]["cnt"].ToString());
            }
            catch 
            {
                return 0;
            }
        }
    }
}