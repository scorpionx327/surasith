﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WFD02650_Handheld.Models;
using System.Data;

namespace WFD02650_Handheld.DAO
{
    public class SystemSettingDAO : Database
    {
        public SystemSettingDAO() : base() { }

        public void Update(SystemSettingModel data)
        {
            string sql = string.Format("update SYSTEM_SETTING set SERVER_IP='{0}'"
                , data.SERVER_IP);
            ExecuteNonQuery(sql);
        }

        public SystemSettingModel Get()
        {
            try
            {
                return executeDataToList<SystemSettingModel>("SELECT * FROM SYSTEM_SETTING").First();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void Add(SystemSettingModel data)
        {
            List<SystemSettingModel> datas = new List<SystemSettingModel>();
            datas.Add(data);
            DBSet ds = new DBSet();
            DataTable dt = ToDataTable<SystemSettingModel>(datas, ds.SYSTEM_SETTING);
            InsertData(dt);
        }
    }
}
