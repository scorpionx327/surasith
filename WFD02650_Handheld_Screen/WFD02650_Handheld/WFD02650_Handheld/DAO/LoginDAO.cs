﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WFD02650_Handheld.Models;

namespace WFD02650_Handheld.DAO
{
    public class LoginDAO : Database
    {
        public LoginDAO() : base() { }

        public void LoginOffline(ref string EMP_CODE, ref string IS_FA_ADMIN, ref string ASSET_LOCATION)
        {
            string sql = string.Format("SELECT EMP_CODE, IS_FA_ADMIN, ASSET_LOCATION FROM DB_COST_EMP");
            DataTable dt = executeDataToDataTable(sql);


            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["EMP_CODE"].ToString().Trim() == EMP_CODE.Trim())
                    {
                        EMP_CODE = row["EMP_CODE"].ToString().Trim();
                        IS_FA_ADMIN = row["IS_FA_ADMIN"].ToString().Trim();
                        ASSET_LOCATION = row["ASSET_LOCATION"].ToString().Trim();
                        break;
                    }
                }
            }
            else
            {
                EMP_CODE = IS_FA_ADMIN = ASSET_LOCATION = string.Empty;
            }
        }

        
    }
}