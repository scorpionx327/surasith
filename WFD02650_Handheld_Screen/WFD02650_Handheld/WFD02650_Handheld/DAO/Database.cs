﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data.SqlServerCe;
using System.Data;
using System.ComponentModel;
using System.Linq;

namespace WFD02650_Handheld.DAO
{
    public class Database : GenerateSQL
    {
        private string connectionString = "Data Source=" + System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\db.sdf;";
        private SqlCeConnection _connection;
        private SqlCeTransaction _transaction;

        public Database()
        {
            _connection = new SqlCeConnection(connectionString);
        }

        #region Open / Close Connection

        public void OpenConnection()
        {
            if (_connection.State != System.Data.ConnectionState.Open) _connection.Open();
        }
        public void CloseConnection()
        {
            if (_connection.State != System.Data.ConnectionState.Closed) _connection.Close();
        }

        #endregion

        #region Transaction

        public void beginTransaction()
        {
            try
            {
                OpenConnection();
                if (_connection.State == System.Data.ConnectionState.Open)
                    _transaction = _connection.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void commitTransaction()
        {
            try
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void rollbackTransaction()
        {
            try
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _transaction.Rollback();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion


        public void InsertData(DataTable dt)
        {
            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    InsertDataRow(row);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Inserts the DataRow for the connection, returning the identity
        public void InsertDataRow(DataRow row)
        {
            try
            {
                SqlCeCommand command = CreateInsertCommand(row);
                command.Connection = _connection;
                command.CommandType = System.Data.CommandType.Text;
                OpenConnection();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (_transaction == null) CloseConnection();
            }
        }

        public void ExecuteNonQuery(string sql)
        {
            try
            {
                OpenConnection();
                using (SqlCeCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if (_transaction == null)
                    CloseConnection();
            }
        }

        public List<T> executeDataToList<T>(string sql)
        {
            
            List<T> result = new List<T>();
            bool haveData = false;
            try
            {
                OpenConnection();
                using (SqlCeCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    SqlCeDataReader dr = cmd.ExecuteReader();

                    result = _DataReaderMapToList<T>(dr);
                    if (result.Count > 0) haveData = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (_transaction == null)
                    CloseConnection();
            }

            if (haveData)
                return result;
            else
                return null;
        }

        public DataTable executeDataToDataTable(string sql, DataTable dt, string[] cols)
        {
            bool haveData = false;
            try
            {
                OpenConnection();
                using (SqlCeCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    SqlCeDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        DataRow r = dt.Rows.Add();
                        foreach (var c in cols)
                        {
                            r[c] = dr[c];
                        }
                    }
                }
                CloseConnection();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (_transaction == null)
                    CloseConnection();
            }

            if (haveData)
                return dt;
            else
                return null;
        }

        public DataTable executeDataToDataTable(string sql)
        {
            DataTable dt = new DataTable();
            bool haveData = false;
            try
            {
                OpenConnection();
                using (SqlCeCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    SqlCeDataReader dr = cmd.ExecuteReader();
                    dt.Load(dr);
                    if (dt.Rows.Count > 0) haveData = true;
                }
                CloseConnection();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (_transaction == null)
                    CloseConnection();
            }

            if (haveData)
                return dt;
            else
                return null;
        }

        #region Convert data
        protected T _DataReaderMap<T>(SqlCeDataReader dr)
        {
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
            }
            return obj;
        }

        protected List<T> _DataReaderMapToList<T>(SqlCeDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (ColumnExists(dr, prop.Name))
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            //  -- prop.SetValue(obj, dr[prop.Name], null);  
                            if (prop.PropertyType == typeof(String))
                            {
                                prop.SetValue(obj, GetString(dr, prop.Name, false), null);
                                continue;
                            }
                            if (prop.PropertyType == typeof(Decimal) || (prop.PropertyType == typeof(Decimal?)))
                            {
                                prop.SetValue(obj, GetDecimal(dr, prop.Name), null);
                                continue;
                            }
                            //DateTime
                            if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                            {
                                prop.SetValue(obj, GetDate(dr, prop.Name), null);
                                continue;
                            }
                            //Int
                            if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?))
                            {
                                prop.SetValue(obj, GetInt(dr, prop.Name), null);
                                continue;
                            }
                        }
                    }
                }
                list.Add(obj);
            }
            return list;
        }

        private bool ColumnExists(SqlCeDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i) == columnName)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
        #region Covert value

        private static System.DateTime? GetDate(SqlCeDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDateTime(objRow[fieldName]);
                }
            }
            catch
            {
                return null;
            }
            return null;
        }

        private static decimal GetDecimal(SqlCeDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToDecimal(objRow[fieldName]);
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static int GetInt(SqlCeDataReader objRow, string fieldName)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return int.Parse(objRow[fieldName].ToString());
                }
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        private static string GetString(SqlCeDataReader objRow, string fieldName, bool isWithNullable)
        {
            try
            {
                if (!(objRow == null | System.DBNull.Value.Equals(objRow[fieldName])))
                {
                    return Convert.ToString(objRow[fieldName]);
                }
            }
            catch
            {
                if (isWithNullable)
                { return null; }
                return string.Empty;
            }
            if (isWithNullable)
            { return null; }
            return string.Empty;
        }

        public static DataTable ToDataTable<T>(IList<T> data, DataTable table)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
           
            object[] values = new object[table.Columns.Count];
            foreach (T item in data)
            {
                DataRow row = table.Rows.Add();
                foreach (DataColumn c in table.Columns)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (c.ColumnName == props[i].Name)
                        {
                            try
                            {
                                row[c.ColumnName] = props[i].GetValue(item);
                            }
                            catch
                            {
                                row[c.ColumnName] = DBNull.Value;
                            }
                            break;
                        }
                    }
                }
            }
            return table;
        }

        public static string BuildInsertSQL(DataTable table)
        {
            StringBuilder sql = new StringBuilder("INSERT INTO " + table.TableName + " (");
            StringBuilder values = new StringBuilder("VALUES (");
            bool bFirst = true;
            bool bIdentity = false;
            string identityType = null;

            foreach (DataColumn column in table.Columns)
            {
                if (column.AutoIncrement)
                {
                    bIdentity = true;

                    switch (column.DataType.Name)
                    {
                        case "Int16":
                            identityType = "smallint";
                            break;
                        case "SByte":
                            identityType = "tinyint";
                            break;
                        case "Int64":
                            identityType = "bigint";
                            break;
                        case "Decimal":
                            identityType = "decimal";
                            break;
                        default:
                            identityType = "int";
                            break;
                    }
                }
                else
                {
                    if (bFirst)
                        bFirst = false;
                    else
                    {
                        sql.Append(", ");
                        values.Append(", ");
                    }

                    sql.Append(column.ColumnName);
                    values.Append("@");
                    values.Append(column.ColumnName);
                }
            }
            sql.Append(") ");
            sql.Append(values.ToString());
            sql.Append(")");

            if (bIdentity)
            {
                sql.Append("; SELECT CAST(scope_identity() AS ");
                sql.Append(identityType);
                sql.Append(")");
            }

            return sql.ToString(); ;
        }

        #endregion
    }
}
