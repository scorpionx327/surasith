﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlServerCe;
using System.IO;
using WFD02650_Handheld.Models;
using WFD02650_Handheld.WFD02650;
using System.Data;
using WFD02650_Handheld.Views;

namespace WFD02650_Handheld.DAO
{
    public class StockTakePlanDAO : Database
    {

        public void ClearAllData()
        {
            string DB_COST_EMP = "DELETE FROM DB_COST_EMP";
            string DB_SCAN = "DELETE FROM DB_SCAN";
            string DB_INVALID = "DELETE FROM DB_INVALID";

            ExecuteNonQuery(DB_COST_EMP);
            ExecuteNonQuery(DB_SCAN);
            ExecuteNonQuery(DB_INVALID);
        }

        #region Check

        public bool IsTherePlanData()
        {
            try
            {
                DataTable dbcostemp = executeDataToDataTable("select count(1) as cnt from DB_COST_EMP");
                DataTable db_scan = executeDataToDataTable("select count(1) as cnt from DB_SCAN");

                if (int.Parse(dbcostemp.Rows[0]["cnt"].ToString()) <= 0) return false;
                if (int.Parse(db_scan.Rows[0]["cnt"].ToString()) <= 0) return false;

                return true;
            }
            catch 
            {
                return false;   
            }
        }

        #endregion

        #region Save plan

        public bool SavePlan(TB_R_STOCK_TAKE_H hData, TB_R_STOCK_TAKE_D[] dDatas, TB_R_STOCK_TAKE_D_PER_SV[] dPerSVDatas
            , string EMP_CODE, string IS_FA_ADMIN)
        {
            try
            {
                ClearAllData();
                AddDBCostEmp(genDBCostEmpDatas(hData, dDatas.ToList(), dPerSVDatas.ToList(), EMP_CODE, IS_FA_ADMIN));
                AddDBScan(genDBScanModel(hData, dDatas.ToList(), dPerSVDatas.ToList(), EMP_CODE));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private List<DB_CostEmpModel> genDBCostEmpDatas(TB_R_STOCK_TAKE_H hData, List<TB_R_STOCK_TAKE_D> dDatas
            , List<TB_R_STOCK_TAKE_D_PER_SV> dPerSVDatas, string empCode, string IS_FA_ADMIN)
        {
            List<DB_CostEmpModel> datas = new List<DB_CostEmpModel>();


            if (hData.ASSET_LOCATION == "I")
            {
                foreach (var d in dDatas.GroupBy(m => new { m.COST_CODE, m.COST_NAME, m.SV_EMP_CODE }).ToList())
                {
                    var sv = dPerSVDatas.Where(m => m.EMP_CODE == d.Key.SV_EMP_CODE).First();
                    DateTime from = sv.DATE_FROM.Value;
                    DateTime to = sv.DATE_TO.Value;
                    from = from.Date + new TimeSpan(int.Parse(sv.TIME_START.Split(':')[0]), int.Parse(sv.TIME_START.Split(':')[1]), 0);
                    to = to.Date + new TimeSpan(int.Parse(sv.TIME_END.Split(':')[0]), int.Parse(sv.TIME_END.Split(':')[1]), 0);
                    datas.Add(new DB_CostEmpModel()
                    {
                        COST_CODE = d.Key.COST_CODE,
                        COST_NAME = d.Key.COST_NAME,
                        DATETIME_FROM = from,
                        DATETIME_TO = to,
                        EMP_CODE = empCode,
                        IS_LOCK = sv.IS_LOCK,
                        PLAN_STATUS = hData.PLAN_STATUS,
                        STOCK_TAKE_KEY = hData.STOCK_TAKE_KEY,
                        PLAN_UPDATE_DATE = hData.UPDATE_DATE,
                        IS_FA_ADMIN = IS_FA_ADMIN,
                        ASSET_LOCATION = hData.ASSET_LOCATION
                    });
                }
            }
            else if (hData.ASSET_LOCATION == "O")
            {
                foreach (var d in dDatas.GroupBy(m => new { m.SV_EMP_CODE, m.SUPPLIER_NAME }).ToList())
                {
                    var sv = dPerSVDatas.Where(m => m.EMP_CODE == d.Key.SV_EMP_CODE).First();
                    DateTime from = sv.DATE_FROM.Value;
                    DateTime to = sv.DATE_TO.Value;
                    from = from.Date + new TimeSpan(int.Parse(sv.TIME_START.Split(':')[0]), int.Parse(sv.TIME_START.Split(':')[1]), 0);
                    to = to.Date + new TimeSpan(int.Parse(sv.TIME_END.Split(':')[0]), int.Parse(sv.TIME_END.Split(':')[1]), 0);
                    datas.Add(new DB_CostEmpModel()
                    {
                        COST_CODE = d.Key.SV_EMP_CODE,
                        COST_NAME = d.Key.SUPPLIER_NAME,
                        DATETIME_FROM = from,
                        DATETIME_TO = to,
                        EMP_CODE = empCode,
                        IS_LOCK = sv.IS_LOCK,
                        PLAN_STATUS = hData.PLAN_STATUS,
                        STOCK_TAKE_KEY = hData.STOCK_TAKE_KEY,
                        PLAN_UPDATE_DATE = hData.UPDATE_DATE,
                        IS_FA_ADMIN = IS_FA_ADMIN
                    });
                }
            }

            

            return datas;
        }

        private List<DBScanModel> genDBScanModel(TB_R_STOCK_TAKE_H hData, List<TB_R_STOCK_TAKE_D> dDatas
            , List<TB_R_STOCK_TAKE_D_PER_SV> dPerSVDatas, string EMP_CODE)
        {
            List<DBScanModel> datas = new List<DBScanModel>();

            foreach (var d in dDatas)
            {
                var sv = dPerSVDatas.Where(m => m.STOCK_TAKE_KEY == d.STOCK_TAKE_KEY).First();
                datas.Add(new DBScanModel()
                {
                    ASSET_NAME = d.ASSET_NAME,
                    ASSET_NO = d.ASSET_NO,
                    BARCODE = d.BARCODE,
                    CHECK_STATUS = d.CHECK_STATUS,
                    COST_CODE = d.COST_CODE,
                    COST_NAME = d.COST_NAME,
                    COUNT_TIME = d.COUNT_TIME,
                    DATE = d.SCAN_DATE,
                    EMP_CODE = d.EMP_CODE,
                    END_COUNT_TIME = d.END_COUNT_TIME,
                    LOCATION = d.ASSET_LOCATION,
                    PLANT_CD = d.PLANT_CD,
                    PLANT_NAME = d.PLANT_NAME,
                    ROUND = d.ROUND,
                    START_COUNT_TIME = d.START_COUNT_TIME,
                    STOCK_TAKE_KEY = d.STOCK_TAKE_KEY,
                    SUB_TYPE = d.SUB_TYPE,
                    YEAR = d.YEAR
                });
            }

            return datas;
        }

        #endregion

        #region Add data

        private void AddDBCostEmp(List<DB_CostEmpModel> datas)
        {
            DBSet ds = new DBSet();
            DataTable dt = ToDataTable<DB_CostEmpModel>(datas, ds.DB_COST_EMP);
            InsertData(dt);
        }

        private void AddDBScan(List<DBScanModel> datas)
        {
            DBSet ds = new DBSet();
            DataTable dt = ToDataTable<DBScanModel>(datas, ds.DB_SCAN);
            InsertData(dt);
        }

        #endregion

        #region Get data

        public List<DB_CostEmpModel> GetDBCostEmpDatas()
        {
            return executeDataToList<DB_CostEmpModel>("SELECT * FROM DB_COST_EMP");
        }
        public DB_CostEmpModel GetDBCostEmpDatas(string COST_CODE)
        {
            var datas = executeDataToList<DB_CostEmpModel>(string.Format("SELECT * FROM DB_COST_EMP WHERE COST_CODE='{0}'", COST_CODE));
            if (datas.Count > 0)
            {
                return datas.First();
            }
            else
            {
                return null;
            }
        }

        public List<DB_InvalidModel> GetDBInvalidDatas()
        {
            return executeDataToList<DB_InvalidModel>("SELECT * FROM DB_INVALID");
        }

        public List<DBScanModel> GetDBScanDatas()
        {
            return executeDataToList<DBScanModel>("SELECT * FROM DB_SCAN");
        }

        public List<DBScanModel> GetDBScanDatas(string COST_CODE, string IS_FA_ADMIN)
        {
            return executeDataToList<DBScanModel>(string.Format("SELECT * FROM DB_SCAN WHERE COST_CODE='{0}' or '{1}' = 'Y'"
                , COST_CODE, IS_FA_ADMIN));
        }

        public string GetAssetLocation()
        {
            DataTable dt = executeDataToDataTable("select top 1 LOCATION as ASSET_LOCATION from DB_SCAN");
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0]["ASSET_LOCATION"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion
    }
}
